
#if !defined(AFX_MERGEMOD_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_)
#define AFX_MERGEMOD_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_

// Unassessed - government property 2020
//#define  MOD_UA_APN           0
//#define  MOD_UA_ENTITY        1
//#define  MOD_UA_NAME          2
//#define  MOD_UA_LOCATION      3
//#define  MOD_UA_ACRES         4
//#define  MOD_UA_REM1          5
//#define  MOD_UA_REM2          6

// Unassessed 2021
#define  MOD_UA_APN           0
#define  MOD_UA_NAME          1
#define  MOD_UA_LOCATION      2
#define  MOD_UA_ACRES         3
#define  MOD_UA_REM1          4
#define  MOD_UA_REM2          5

#define  MOD_CHAR_FEEPRCL                 0
#define  MOD_CHAR_DOCNUM                  1
#define  MOD_CHAR_CATTYPE                 2
#define  MOD_CHAR_BLDGSEQNO               3
#define  MOD_CHAR_UNITSEQNO               4
#define  MOD_CHAR_YRBLT                   5
#define  MOD_CHAR_BLDGTYPE                6
#define  MOD_CHAR_EFFYR                   7
#define  MOD_CHAR_BUILDINGSIZE            8
#define  MOD_CHAR_BUILDINGUSEDFOR         9
#define  MOD_CHAR_STORIESCNT              10
#define  MOD_CHAR_UNITSCNT                11
#define  MOD_CHAR_CONDITION               12
#define  MOD_CHAR_BEDROOMS                13
#define  MOD_CHAR_BATHROOMS               14
#define  MOD_CHAR_QUALITYCLASS            15
#define  MOD_CHAR_HALFBATHS               16
#define  MOD_CHAR_CONSTRUCTION            17
#define  MOD_CHAR_FOUNDATION              18
#define  MOD_CHAR_STRUCTURALFRAME         19
#define  MOD_CHAR_STRUCTURALFLOOR         20
#define  MOD_CHAR_EXTERIORTYPE            21
#define  MOD_CHAR_ROOFCOVER               22
#define  MOD_CHAR_ROOFTYPEFLAT            23
#define  MOD_CHAR_ROOFTYPEHIP             24
#define  MOD_CHAR_ROOFTYPEGABLE           25
#define  MOD_CHAR_ROOFTYPESHED            26
#define  MOD_CHAR_INSULATIONCEILINGS      27
#define  MOD_CHAR_INSULATIONWALLS         28
#define  MOD_CHAR_INSULATIONFLOORS        29
#define  MOD_CHAR_WINDOWPANESINMOD        30
#define  MOD_CHAR_WINDOWPANEDOUBLE        31
#define  MOD_CHAR_WINDOWPANETRIPLE        32
#define  MOD_CHAR_WINDOWTYPE              33
#define  MOD_CHAR_LIGHTING                34
#define  MOD_CHAR_COOLINGCENTRALAC        35
#define  MOD_CHAR_COOLINGEVAPORATIVE      36
#define  MOD_CHAR_COOLINGROOMWALL         37
#define  MOD_CHAR_COOLINGWINDOW           38
#define  MOD_CHAR_HEATING                 39
#define  MOD_CHAR_FIREPLACE               40
#define  MOD_CHAR_GARAGE                  41
#define  MOD_CHAR_PLUMBING                42
#define  MOD_CHAR_SOLAR                   43
#define  MOD_CHAR_BUILDER                 44
#define  MOD_CHAR_BLDGDESIGNEDFOR         45
#define  MOD_CHAR_MODELDESC               46
#define  MOD_CHAR_UNFINAREASSF            47
#define  MOD_CHAR_ATTACHGARAGESF          48
#define  MOD_CHAR_DETACHGARAGESF          49
#define  MOD_CHAR_CARPORTSF               50
#define  MOD_CHAR_DECKSSF                 51
#define  MOD_CHAR_PATIOSF                 52
#define  MOD_CHAR_CEILINGHEIGHT           53
#define  MOD_CHAR_FIRESPINKLERS           54
#define  MOD_CHAR_AVGWALLHEIGHT           55
#define  MOD_CHAR_BAY                     56
#define  MOD_CHAR_DOCK                    57
#define  MOD_CHAR_ELEVATOR                58
#define  MOD_CHAR_ESCALATOR               59
#define  MOD_CHAR_ROLLUPDOOR              60
#define  MOD_CHAR_FIELD1                  61
#define  MOD_CHAR_FIELD2                  62
#define  MOD_CHAR_FIELD3                  63
#define  MOD_CHAR_FIELD4                  64
#define  MOD_CHAR_FIELD5                  65
#define  MOD_CHAR_FIELD6                  66
#define  MOD_CHAR_FIELD7                  67
#define  MOD_CHAR_FIELD8                  68
#define  MOD_CHAR_FIELD9                  69
#define  MOD_CHAR_FIELD10                 70
#define  MOD_CHAR_FIELD11                 71
#define  MOD_CHAR_FIELD12                 72
#define  MOD_CHAR_FIELD13                 73
#define  MOD_CHAR_FIELD14                 74
#define  MOD_CHAR_FIELD15                 75
#define  MOD_CHAR_FIELD16                 76
#define  MOD_CHAR_FIELD17                 77
#define  MOD_CHAR_FIELD18                 78
#define  MOD_CHAR_FIELD19                 79
#define  MOD_CHAR_FIELD20                 80
#define  MOD_CHAR_LANDFIELD1              81
#define  MOD_CHAR_LANDFIELD2              82
#define  MOD_CHAR_LANDFIELD3              83
#define  MOD_CHAR_LANDFIELD4              84
#define  MOD_CHAR_LANDFIELD5              85
#define  MOD_CHAR_LANDFIELD6              86
#define  MOD_CHAR_LANDFIELD7              87
#define  MOD_CHAR_LANDFIELD8              88
#define  MOD_CHAR_ACRES                   89
#define  MOD_CHAR_NEIGHBORHOODCODE        90
#define  MOD_CHAR_ZONING                  91
#define  MOD_CHAR_TOPOGRAPHY              92
#define  MOD_CHAR_VIEWCODE                93
#define  MOD_CHAR_POOLSPA                 94
#define  MOD_CHAR_WATERSOURCE             95
#define  MOD_CHAR_SUBDIVNAME              96
#define  MOD_CHAR_SEWERCODE               97
#define  MOD_CHAR_UTILITIESCODE           98
#define  MOD_CHAR_ACCESSCODE              99
#define  MOD_CHAR_LANDSCAPE               100
#define  MOD_CHAR_PROBLEMCODE             101
#define  MOD_CHAR_FRONTAGE                102
#define  MOD_CHAR_LOCATION                103
#define  MOD_CHAR_PLANTEDACRES            104
#define  MOD_CHAR_ACRESUNUSEABLE          105
#define  MOD_CHAR_WATERSOURCEDOMESTIC     106
#define  MOD_CHAR_WATERSOURCEIRRIGATION   107
#define  MOD_CHAR_HOMESITES               108
#define  MOD_CHAR_PROPERTYCONDITIONCODE   109
#define  MOD_CHAR_ROADTYPE                110
#define  MOD_CHAR_HASWELL                 111
#define  MOD_CHAR_HASCOUNTYROAD           112
#define  MOD_CHAR_HASVINEYARD             113
#define  MOD_CHAR_ISUNSECUREDBUILDING     114
#define  MOD_CHAR_HASORCHARD              115
#define  MOD_CHAR_HASGROWINGIMPRV         116
#define  MOD_CHAR_SITECOVERAGE            117
#define  MOD_CHAR_PARKINGSPACES           118
#define  MOD_CHAR_EXCESSLANDSF            119
#define  MOD_CHAR_FRONTFOOTAGESF          120
#define  MOD_CHAR_MULTIPARCELECON         121
#define  MOD_CHAR_LANDUSECODE1            122
#define  MOD_CHAR_LANDUSECODE2            123
#define  MOD_CHAR_LANDSQFT                124
#define  MOD_CHAR_TOTALROOMS              125
#define  MOD_CHAR_NETLEASABLESF           126
#define  MOD_CHAR_BLDGFOOTPRINTSF         127
#define  MOD_CHAR_OFFICESPACESF           128
#define  MOD_CHAR_NONCONDITIONSF          129
#define  MOD_CHAR_MEZZANINESF             130
#define  MOD_CHAR_PERIMETERLF             131
#define  MOD_CHAR_ASMT                    132
#define  MOD_CHAR_ASMTCATEGORY            133
#define  MOD_CHAR_EVENTDATE               134
#define  MOD_CHAR_SALESPRICE              135      // Confirmed
#define  MOD_CHAR_CONFIRMATIONCODE        136

#define  MOD_MH_FEEPARCEL		            0
#define  MOD_MH_QUALCLS		               1
#define  MOD_MH_YEARBUILT		            2
#define  MOD_MH_BLDGGSIZE		            3
#define  MOD_MH_NUMBEDROOMS		         4
#define  MOD_MH_NUMFULLBATHS	            5
#define  MOD_MH_ASMT				            6
#define  MOD_MH_BLDGTYPE			         7

static XLAT_CODE  asCond[] =
{
   // Value, lookup code, value length
   "",  "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "1",   "1", 1,             // 1 Fireplace 
   "2",   "2", 1,             // 2 Fireplaces
   "F",   "Y", 1,             // Fireplace 
   "I",   "I", 1,             // Fireplace Insert
   "S",   "W", 1,             // Wood Stove
   "",   "", 0
};

static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "C",  "Z", 1,              // Central
   "M",  "V", 1,              // Monitor
   "B",  "F", 1,              // Baseboard
   "W",  "D", 1,              // Wall
   "E",  "F", 1,              // Electric
   "F",  "C", 1,              // Floor
   "P",  "G", 1,              // Heat pump
   "O",  "X", 1,              // Other
   "R",  "N", 1,              // Gas
   "S",  "9", 1,              // Unknown?
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "",   "", 0
};

static XLAT_CODE  asSewer[] =
{
   // Value, lookup code, value length
   "P", "P", 1,               // Public
   "S", "S", 1,               // Septic
   "",   "",  0
};

static XLAT_CODE  asWaterSrc[] =
{
   // Value, lookup code, value length
   "M", "P", 1,               // Municipal
   "W", "W", 1,               // Private Well
   "",  "",  0
};

static IDX_TBL5  asXferTypes[] =
{
   "1",  "F", 'Y', 1, 1,      // Prop58
   "2",  "F", 'Y', 1, 1,      // Date of Death-Spouse
   "3",  "F", 'Y', 1, 1,      // Property Settlement-Spouses
   "4",  "F", 'Y', 1, 1,      // Solely Between Spouses
   "6",  "F", 'Y', 1, 1,      // Prop 60
   "A",  "F", 'Y', 1, 1,      // General Sale
   "FR", "F", 'Y', 2, 1,      // Full Revalue
   "FV", "F", 'Y', 2, 1,      // Full value
   "F",  "F", 'Y', 1, 1,      // Joint Tenants
   "O",  "F", 'Y', 1, 1,      // Sale of Tax Deeded Property
   "PR", "P", 'Y', 2, 1,      // Partial Revalue
   "P",  "F", 'Y', 1, 1,      // Foreclosure of Sale in Lieu of
   "R",  "F", 'Y', 1, 1,      // No Beneficial Change
   "S",  "F", 'Y', 1, 1,      // Perfection of Title
   "U",  "F", 'Y', 1, 1,      // Trust
   "V",  "F", 'Y', 1, 1,      // Retained Life Estate
   "W",  "F", 'Y', 1, 1,      // Orig Trans Create/Delete
   "",   "",  'Y', 0, 0
};

IDX_TBL5 MOD_DocCode[] =
{  // DocCode, Index, Non-sale, DocCodelen, Indexlen
   "00", "74", 'Y', 2, 2,     // Undetermined Change 100% - 
                              // This code can be DEED (2016R0003248) or AFFIDAVIT (20170000011) or QC DEED (2016R0002011)
   "01", "1 ", 'N', 2, 2,     // General Sale 100%
   "02", "74", 'Y', 2, 2,     // No Beneficial Change Partial Interest
   "03", "57", 'N', 2, 2,     // General Sale Partial Interest
   "04", "40", 'Y', 2, 2,     // Easement
   "05", "60", 'Y', 2, 2,     // Property Settlement - Spouses
   "06", "60", 'Y', 2, 2,     // Property Settlement - Spouses Partial Int
   "07", "18", 'Y', 2, 2,     // Transfer Solely Between Spouses
   "08", "18", 'Y', 2, 2,     // Transfer Solely Between Spouses Partial Int
   "09", "74", 'Y', 2, 2,     // Split/Combo
   "10", "75", 'Y', 2, 2,     // Prop 58 100%
   "11", "75", 'Y', 2, 2,     // Prop 58 Partial Interest
   "13", "62", 'Y', 2, 2,     // Perfection of Title
   "14", "75", 'Y', 2, 2,     // DEED(2016R0001725)/ORDER(2016R0001740)
                              // XferType=4/blank
   "16", "6 ", 'Y', 2, 2,     // AFFIDAVIT 
   "17", "61", 'Y', 2, 2,     // Trust 100%
   "18", "61", 'Y', 2, 2,     // Trust partial
   "19", "19", 'Y', 2, 2,     // Retained Life Estate
   "21", "19", 'Y', 2, 2,     // Original Transfer Create/Delete
   "24", "74", 'Y', 2, 2,     // Lessors Interest 35 Years or More
   "25", "18", 'Y', 2, 2,     // Date of Death - Spouse 100%
   "26", "18", 'Y', 2, 2,     // Date of Death - Spouse
   "27", "77", 'Y', 2, 2,     // Foreclosure
   "32", "3 ", 'Y', 2, 2,     // Joint Tenants
   "34", "3 ", 'Y', 2, 2,     // Tenants in Common
   "38", "19", 'Y', 2, 2,     // Community Property Right of Survivorship
   "48", "67", 'Y', 2, 2,     // Sale of Tax Deeded Property
   "50", "78", 'Y', 2, 2,     // Foreclosure of Sale in Lieu of
   "52", "75", 'Y', 2, 2,     // Termination of Life Estate
   "95", "27", 'Y', 2, 2,     // Trustees Deed
   "","",0,0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 MOD_Exemption[] = 
{
   "E01", "H", 3,1,
   "E30", "V", 3,1,     // VETERAN
   "E40", "W", 3,1,     // WELFARE
   "E42", "X", 3,1,     // ?
   "E44", "W", 3,1,     // WELFARE 
   "E45", "W", 3,1,     // WELFARE 
   "E47", "W", 3,1,     // WELFARE 
   "E48", "W", 3,1,     // ?
   "E50", "C", 3,1,     // CHURCH 
   "E60", "R", 3,1,     // RELIGIOUS 
   "E63", "R", 3,1,     // RELIGIOUS
   "E64", "R", 3,1,     // RELIGIOUS
   "E80", "D", 3,1,     // DISABLED VETERAN
   "E98", "X", 3,1,     // OTHER
   "","",0,0
};

#endif // !defined(AFX_MERGEMOD_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_)
