#ifndef  _RECDEF_H_
#define  _RECDEF_H_

#include "R01.h"

#define  BLANK32        "                                "
#define  MAX_NAMES      5

#define  OFF_3K_CO_ID        1-1
#define  OFF_3K_CO_NUM       4-1
// Roll
#define  OFF_3K_ASSMT        6-1
#define  OFF_3K_FEE_PRCL     18-1
#define  OFF_3K_TRA          30-1
#define  OFF_3K_DESC         36-1
#define  OFF_3K_ZONE         86-1
#define  OFF_3K_USE          94-1
#define  OFF_3K_NBHCD        102-1
#define  OFF_3K_INACRES      106-1
#define  OFF_3K_RDOCNUM      115-1
#define  OFF_3K_RDOCDT       127-1
#define  OFF_3K_OUTFIXAPN    137-1     // Fixed 8-char of APN - Lake only
#define  OFF_3K_HASSEPTIC    145-1     // Y/N
#define  OFF_3K_TAXFULL      146-1
#define  OFF_3K_NAME         149-1
#define  OFF_3K_CAREOF       199-1
#define  OFF_3K_DBA          249-1
#define  OFF_3K_M_STR        299-1
#define  OFF_3K_M_CITY       349-1
#define  OFF_3K_M_ST         384-1
#define  OFF_3K_M_ZIP        386-1
#define  OFF_3K_S_STR        396-1
#define  OFF_3K_S_STRNUM     420-1
#define  OFF_3K_S_SFX        430-1
#define  OFF_3K_S_DIR        434-1
#define  OFF_3K_S_UNIT       436-1
#define  OFF_3K_S_COMM       440-1
#define  OFF_3K_S_ZIP        444-1
// Lien values
#define  OFF_3K_LAND         454-1
#define  OFF_3K_HOMESITE     463-1
#define  OFF_3K_IMPR         472-1
#define  OFF_3K_GROW         481-1
#define  OFF_3K_FIXT         490-1
#define  OFF_3K_FIXT_RP      499-1
#define  OFF_3K_PERS         508-1
#define  OFF_3K_MHPERS       517-1
// Exe
#define  OFF_3K_EXCNT        526-1
#define  OFF_3K_STAT1        527-1
#define  OFF_3K_EXCD1        528-1
#define  OFF_3K_ISHO1        531-1
#define  OFF_3K_MAXAMT1      532-1
#define  OFF_3K_PCT1         541-1
#define  OFF_3K_STAT2        544-1
#define  OFF_3K_EXCD2        545-1
#define  OFF_3K_ISHO2        548-1
#define  OFF_3K_MAXAMT2      549-1
#define  OFF_3K_PCT2         558-1
#define  OFF_3K_STAT3        561-1
#define  OFF_3K_EXCD3        562-1
#define  OFF_3K_ISHO3        565-1
#define  OFF_3K_MAXAMT3      566-1
#define  OFF_3K_PCT3         575-1
#define  OFF_3K_STAT4        578-1
#define  OFF_3K_EXCD4        579-1
#define  OFF_3K_ISHO4        582-1
#define  OFF_3K_MAXAMT4      583-1
#define  OFF_3K_PCT4         592-1
#define  OFF_3K_STAT5        595-1
#define  OFF_3K_EXCD5        596-1
#define  OFF_3K_ISHO5        599-1
#define  OFF_3K_MAXAMT5      600-1
#define  OFF_3K_PCT5         609-1
// Char
#define  OFF_3K_POOLS        612-1
#define  OFF_3K_USECAT       614-1
#define  OFF_3K_QUALCLS      616-1
#define  OFF_3K_HASSEWER     622-1
#define  OFF_3K_YRBLT        623-1
#define  OFF_3K_BDGSZ        627-1
#define  OFF_3K_GARSZ        636-1
#define  OFF_3K_HEAT         645-1
#define  OFF_3K_COOL         647-1
#define  OFF_3K_HTSRC        649-1
#define  OFF_3K_CLSRC        651-1
#define  OFF_3K_BEDS         654-1
#define  OFF_3K_FBATHS       658-1
#define  OFF_3K_HBATHS       662-1
#define  OFF_3K_FIREPL       666-1
// Sales
#define  OFF_3K_TOTSALES     670-1
#define  OFF_3K_SALECNT      672-1

#define  OFF_3K_SALESTART    673-1
#define  OFF_3K_DOCNO1       673-1
#define  OFF_3K_EVENTDT1     685-1
#define  OFF_3K_DOCCD1       695-1
#define  OFF_3K_SELLER1      697-1
#define  OFF_3K_BUYER1       747-1
#define  OFF_3K_CONFPRC1     797-1
#define  OFF_3K_DOCTAX1      810-1
#define  OFF_3K_GRPSL1       821-1
#define  OFF_3K_GRPASMT1     822-1
#define  OFF_3K_TRANTYP1     834-1
#define  OFF_3K_ADJRSNC1     836-1
#define  OFF_3K_CONFCD1      838-1
#define  OFF_3K_DOCNO2       840-1
#define  OFF_3K_EVENTDT2     852-1
#define  OFF_3K_DOCCD2       862-1
#define  OFF_3K_SELLER2      864-1
#define  OFF_3K_BUYER2       914-1
#define  OFF_3K_CONFPRC2     964-1
#define  OFF_3K_DOCTAX2      977-1
#define  OFF_3K_GRPSL2       988-1
#define  OFF_3K_GRPASMT2     989-1
#define  OFF_3K_TRANTYP2     1001-1
#define  OFF_3K_ADJRSNC2     1003-1
#define  OFF_3K_CONFCD2      1005-1
#define  OFF_3K_DOCNO3       1007-1
#define  OFF_3K_EVENTDT3     1019-1
#define  OFF_3K_DOCCOD3      1029-1
#define  OFF_3K_SELLER3      1031-1
#define  OFF_3K_BUYER3       1081-1
#define  OFF_3K_CONFPR3      1131-1
#define  OFF_3K_DOCTAX3      1144-1
#define  OFF_3K_GRPSL3       1155-1
#define  OFF_3K_GRPASMT3     1156-1
#define  OFF_3K_TRANTYP3     1168-1
#define  OFF_3K_ADJRSNC3     1170-1
#define  OFF_3K_CONFCD3      1172-1
// Sequential number
#define  OFF_3K_RECNO        1174-1
// Tax
#define  OFF_3K_TAXAMT1      1180-1
#define  OFF_3K_TAXAMT2      1191-1
#define  OFF_3K_PEN1         1202-1
#define  OFF_3K_PEN2         1213-1
#define  OFF_3K_PENDT1       1224-1
#define  OFF_3K_PENDT2       1234-1
#define  OFF_3K_PAIDAMT1     1244-1
#define  OFF_3K_PAIDAMT2     1255-1
#define  OFF_3K_TOTPAID1     1266-1
#define  OFF_3K_TOTPAID2     1277-1
#define  OFF_3K_TOTFEES      1288-1
#define  OFF_3K_PAIDDT1      1299-1
#define  OFF_3K_PAIDDT2      1309-1
#define  OFF_3K_TAXYEAR      1319-1
#define  OFF_3K_ROLLCAT      1323-1
// Flags, calculated/derived amounts
#define  OFF_3K_MAILBRK      1325-1    // "B" OR "N" OR BLANK IF NO MAIL STRT
#define  OFF_3K_MAILCA       1326-1    // MAIL ADDR IN CALIFORNIA - Y" OR "N"
#define  OFF_3K_MAILUSA      1327-1    // MAIL ADDR IN USA - "Y" OR "N"
#define  OFF_3K_HAVESITUS    1328-1    // "S" = HAVE SITUS
#define  OFF_3K_SITUSBRK     1329-1    // "B" = SITUS BROKEN
#define  OFF_3K_HAVESALE     1330-1    // "S" = HAVE SALE AMT
#define  OFF_3K_EXCPFLG      1331-1    // "E" = SALE AMT > GROSS AMT
#define  OFF_3K_GROSS        1332-1
#define  OFF_3K_TOTEX        1341-1
#define  OFF_3K_NET          1350-1
#define  OFF_3K_LANDIMP      1359-1
#define  OFF_3K_IMPPERC      1368-1    // IMP % OF LAND + IMP
#define  OFF_3K_DRV_ACRES    1371-1    // 9(6).999
#define  OFF_3K_LOTSQFT      1381-1
#define  OFF_3K_SALEAMT      1390-1
#define  OFF_3K_EXCPAMT      1399-1    // (SALEAMT - GROSS), IF (SALEAMT > GROSS)
#define  OFF_3K_LVALACRE     1408-1    // 9(7).99 - LAND VALUE / ACRE
#define  OFF_3K_LVALSQFT     1418-1    // 9(7).99 - LAND VALUE / LOT SQ FT
#define  OFF_3K_IVALSQFT     1428-1    // 9(7).99 - IMPROV VAL / IMP SQ FT
#define  OFF_3K_SVALSQFT     1438-1    // 9(7).99 - SALE VAL / IMP SQ FT
// Situs - formatted
#define  OFF_3K_FS_STRNO     1448-1
#define  OFF_3K_FS_FRA       1455-1
#define  OFF_3K_FS_DIR       1458-1
#define  OFF_3K_FS_STR       1460-1
#define  OFF_3K_FS_SFX       1490-1
#define  OFF_3K_FS_UNIT      1495-1
#define  OFF_3K_FS_TONUM     1503-1
#define  OFF_3K_FS_CITY      1509-1
#define  OFF_3K_FM_STRNO     1526-1
#define  OFF_3K_FM_FRA       1533-1
#define  OFF_3K_FM_DIR       1536-1
#define  OFF_3K_FM_STR       1538-1
#define  OFF_3K_FM_SFX       1568-1
#define  OFF_3K_FM_UNIT      1573-1
#define  OFF_3K_FM_TONUM     1581-1
#define  OFF_3K_FM_CITY      1587-1
#define  OFF_3K_FM_ST        1604-1
#define  OFF_3K_FM_ZIP       1606-1    // Zip+Zip4
#define  OFF_3K_ADRFLG       1615-1
// Massage name
#define  OFF_3K_MSGNAME1     1616-1    // Name1 after massage
#define  OFF_3K_NORMNAME1    1666-1    // Normalize name1
#define  OFF_3K_NRMCLNNAME1  1691-1    // Cleaned and normalize name1
#define  OFF_3K_SWAPNAME1    1741-1    // Swapped name1 first middle last
#define  OFF_3K_SALCODE      1791-1    // Salutation code
#define  OFF_3K_SALUTATION   1793-1
#define  OFF_3K_NAMESFXCD    1797-1
#define  OFF_3K_NAMESFX      1799-1
#define  OFF_3K_NAMEFLG      1802-1
// Current assessed values
#define  OFF_3K_CLAND        1803-1
#define  OFF_3K_CHOMESITE    1812-1
#define  OFF_3K_CIMPR        1821-1
#define  OFF_3K_CGROW        1830-1
#define  OFF_3K_CFIXT        1839-1
#define  OFF_3K_CFIXT_RP     1848-1
#define  OFF_3K_CPERS        1857-1
#define  OFF_3K_CMHPERS      1866-1
// Tax 2-6
#define  OFF_3K_TX2AMT1      1875-1
#define  OFF_3K_TX2AMT2      1886-1
#define  OFF_3K_TX2PEN1      1897-1
#define  OFF_3K_TX2PEN2      1908-1
#define  OFF_3K_TX2PENCGDT1  1919-1
#define  OFF_3K_TX2PENCHDT2  1929-1
#define  OFF_3K_TX2PAIDAMT1  1939-1
#define  OFF_3K_TX2PAIDAMT2  1950-1
#define  OFF_3K_TX2TOTPD1    1961-1
#define  OFF_3K_TX2TOTPD2    1972-1
#define  OFF_3K_TX2TOTAL     1983-1
#define  OFF_3K_TX2PDDT1     1994-1
#define  OFF_3K_TX2PDDT2     2004-1
#define  OFF_3K_TX2YEAR      2014-1
#define  OFF_3K_TX2CAT       2018-1
#define  OFF_3K_TX3AMT1      2020-1
#define  OFF_3K_TX3AMT2      2031-1
#define  OFF_3K_TX3PEN1      2042-1
#define  OFF_3K_TX3PEN2      2053-1
#define  OFF_3K_TX3PENCGDT1  2064-1
#define  OFF_3K_TX3PENCGDT2  2074-1
#define  OFF_3K_TX3PAIDAMT1  2084-1
#define  OFF_3K_TX3PAIDAMT2  2095-1
#define  OFF_3K_TX3TOTPD1    2106-1
#define  OFF_3K_TX3TOTPD2    2117-1
#define  OFF_3K_TX3TOTAL     2128-1
#define  OFF_3K_TX3PDDT1     2139-1
#define  OFF_3K_TX3PDDT2     2149-1
#define  OFF_3K_TX3YEAR      2159-1
#define  OFF_3K_TX3CAT       2163-1
#define  OFF_3K_TX4AMT1      2165-1
#define  OFF_3K_TX4AMT2      2176-1
#define  OFF_3K_TX4PEN1      2187-1
#define  OFF_3K_TX4PEN2      2198-1
#define  OFF_3K_TX4PENCGDT1  2209-1
#define  OFF_3K_TX4PENCGDT2  2219-1
#define  OFF_3K_TX4PAIDAMT1  2229-1
#define  OFF_3K_TX4PAIDAMT2  2240-1
#define  OFF_3K_TX4TOTPD1    2251-1
#define  OFF_3K_TX4TOTPD2    2262-1
#define  OFF_3K_TX4TOTAL     2273-1
#define  OFF_3K_TX4PDDT1     2284-1
#define  OFF_3K_TX4PDDT2     2294-1
#define  OFF_3K_TX4YEAR      2304-1
#define  OFF_3K_TX4CAT       2308-1
#define  OFF_3K_TX5AMT1      2310-1
#define  OFF_3K_TX5AMT2      2321-1
#define  OFF_3K_TX5PEN1      2332-1
#define  OFF_3K_TX5PEN2      2343-1
#define  OFF_3K_TX5PENCGDT1  2354-1
#define  OFF_3K_TX5PENCGDT2  2364-1
#define  OFF_3K_TX5PAIDAMT1  2374-1
#define  OFF_3K_TX5PAIDAMT2  2385-1
#define  OFF_3K_TX5TOTPD1    2396-1
#define  OFF_3K_TX5TOTPD2    2407-1
#define  OFF_3K_TX5TOTAL     2418-1
#define  OFF_3K_TX5PDDT1     2429-1
#define  OFF_3K_TX5PDDT2     2439-1
#define  OFF_3K_TX5YEAR      2449-1
#define  OFF_3K_TX5CAT       2453-1
#define  OFF_3K_TX6AMT1      2455-1
#define  OFF_3K_TX6AMT2      2466-1
#define  OFF_3K_TX6PEN1      2477-1
#define  OFF_3K_TX6PEN2      2488-1
#define  OFF_3K_TX6PENCGDT1  2499-1
#define  OFF_3K_TX6PENCGDT2  2509-1
#define  OFF_3K_TX6PAIDAMT1  2519-1
#define  OFF_3K_TX6PAIDAMT2  2530-1
#define  OFF_3K_TX6TOTPD1    2541-1
#define  OFF_3K_TX6TOTPD2    2552-1
#define  OFF_3K_TX6TOTAL     2563-1
#define  OFF_3K_TX6PPDT1     2574-1
#define  OFF_3K_TX6PPDT2     2584-1
#define  OFF_3K_TX6YEAR      2594-1
#define  OFF_3K_TX6CAT       2598-1
#define  OFF_3K_HASWELL      2600-1    // Y/N
#define  OFF_3K_G_G_COUNT    2601-1
#define  OFF_3K_1DT          2602-1
#define  OFF_3K_1DOC         2610-1
#define  OFF_3K_1TYPE        2626-1
#define  OFF_3K_1AMT         2636-1
#define  OFF_3K_1GRANTOR1    2646-1
#define  OFF_3K_1GRANTOR2    2666-1
#define  OFF_3K_1GRANTEE1    2686-1
#define  OFF_3K_1GRANTEE2    2706-1
#define  OFF_3K_1APN_MCH     2726-1
#define  OFF_3K_1NAME_MCH    2727-1
#define  OFF_3K_2DT          2728-1
#define  OFF_3K_2DOC         2736-1
#define  OFF_3K_2TYPE        2752-1
#define  OFF_3K_2AMT         2762-1
#define  OFF_3K_2GRANTOR1    2772-1
#define  OFF_3K_2GRANTOR2    2792-1
#define  OFF_3K_2GRANTEE1    2812-1
#define  OFF_3K_2GRANTEE2    2832-1
#define  OFF_3K_2APN_MCH     2852-1
#define  OFF_3K_2NAME_MCH    2853-1
#define  OFF_3K_3DT          2854-1
#define  OFF_3K_3DOC         2862-1
#define  OFF_3K_3TYPE        2878-1
#define  OFF_3K_3AMT         2888-1
#define  OFF_3K_3GRANTOR1    2898-1
#define  OFF_3K_3GRANTOR2    2918-1
#define  OFF_3K_3GRANTEE1    2938-1
#define  OFF_3K_3GRANTEE2    2958-1
#define  OFF_3K_3APN_MCH     2978-1
#define  OFF_3K_3NAME_MCH    2979-1
#define  OFF_3K_APN_DASH     2980-1
#define  OFF_3K_LASTSALEDT   2995-1
#define  OFF_3K_LASTSALEAMT  3003-1
// Appraisal
#define  MON_OFF_3K_NCODE     3013-1
#define  MON_OFF_3K_APPRID    3017-1
#define  MON_OFF_3K_NCSUFFIX  3021-1
#define  MON_OFF_3K_AREADESIG 3041-1
#define  MON_OFF_3K_AREADESC  3045-1
#define  MON_OFF_3K_APPRNAME  3095-1

#define  SIZ_3K_CO_ID        3
#define  SIZ_3K_CO_NUM       2
#define  SIZ_3K_ASSMT        9
#define  SIZ_3K_APN          9
#define  SIZ_3K_TRA          6
#define  SIZ_3K_DESC         50
#define  SIZ_3K_ZONE         8
#define  SIZ_3K_USE          8
#define  SIZ_3K_NBHCD        4
#define  SIZ_3K_INACRES      9
#define  SIZ_3K_RDOCNUM      12
#define  SIZ_3K_RDOCDT       10
#define  SIZ_3K_OUTFIXAPN    8
#define  SIZ_3K_TAXFULL      3
#define  SIZ_3K_NAME         50
#define  SIZ_3K_CAREOF       50
#define  SIZ_3K_DBA          50
#define  SIZ_3K_M_STR        50
#define  SIZ_3K_M_CITY       35
#define  SIZ_3K_M_ST         2
#define  SIZ_3K_M_ZIP        10
#define  SIZ_3K_S_STR        24
#define  SIZ_3K_S_STRNUM     10
#define  SIZ_3K_S_SFX        4
#define  SIZ_3K_S_DIR        2
#define  SIZ_3K_S_UNIT       4
#define  SIZ_3K_S_COMM       4
#define  SIZ_3K_S_ZIP        10
#define  SIZ_3K_LAND         9
#define  SIZ_3K_HOMESITE     9
#define  SIZ_3K_IMPR         9
#define  SIZ_3K_GROW         9
#define  SIZ_3K_FIXT         9
#define  SIZ_3K_FIXTRL       9
#define  SIZ_3K_PERS         9
#define  SIZ_3K_MHPERS       9

#define  SIZ_3K_EXCNT        1
#define  SIZ_3K_STATUS       1
#define  SIZ_3K_EXECD        3
#define  SIZ_3K_HOFLG        1
#define  SIZ_3K_EXEAMT       9
#define  SIZ_3K_EXEPCT       3

#define  SIZ_3K_POOLS        2
#define  SIZ_3K_USECAT       2
#define  SIZ_3K_QUALCLS      6
#define  SIZ_3K_YRBLT        4
#define  SIZ_3K_BDGSZ        9
#define  SIZ_3K_GARSZ        9
#define  SIZ_3K_HEAT         2
#define  SIZ_3K_COOL         2
#define  SIZ_3K_HTSRC        2
#define  SIZ_3K_CLSRC        3
#define  SIZ_3K_BEDS         4
#define  SIZ_3K_FBATHS       4
#define  SIZ_3K_HBATHS       4
#define  SIZ_3K_FIREPL       4

#define  SIZ_3K_TOTSALES     2

#define  SIZ_3K_SLCNT        1
#define  SIZ_3K_SDOCNUM      12
#define  SIZ_3K_SALEDATE     10
#define  SIZ_3K_SALECODE     2
#define  SIZ_3K_SELLER       50
#define  SIZ_3K_BUYER        50
#define  SIZ_3K_CONFPRICE    13
#define  SIZ_3K_DOCTAX       11
#define  SIZ_3K_GRPSALE      1
#define  SIZ_3K_GRPASMT      12
#define  SIZ_3K_TRANSTYPE    2
#define  SIZ_3K_AR_CODE      2
#define  SIZ_3K_CONFCODE     2

#define  SIZ_3K_RECNO        6

#define  SIZ_3K_TAXAMT       11
#define  SIZ_3K_PENAMT       11
#define  SIZ_3K_PENDATE      10
#define  SIZ_3K_PAIDAMT      11
#define  SIZ_3K_TOTPAID      11
#define  SIZ_3K_TOTALFEES    11
#define  SIZ_3K_PAIDDATE     10
#define  SIZ_3K_TAXYEAR      4
#define  SIZ_3K_ROLLCAT      2

#define  SIZ_3K_OUTMSW       1
#define  SIZ_3K_MAILBUT      1
#define  SIZ_3K_MAILUSA      1
#define  SIZ_3K_SITFLAG      1
#define  SIZ_3K_BRKFLAG      1
#define  SIZ_3K_SALEFLAG     1
#define  SIZ_3K_EXCPFLAG     1
#define  SIZ_3K_GROSS        9
#define  SIZ_3K_TOTEX        9
#define  SIZ_3K_NET          9
#define  SIZ_3K_LANDIMP      9
#define  SIZ_3K_IMPPERC      3
#define  SIZ_3K_DRV_ACRES   10
#define  SIZ_3K_LOTSQFT      9
#define  SIZ_3K_SALEAMT      9
#define  SIZ_3K_EXCPAMT      9
#define  SIZ_3K_LVALACRE     10
#define  SIZ_3K_LVALSQFT     10
#define  SIZ_3K_IVALSQFT     10
#define  SIZ_3K_SVALSQFT     10

#define  SIZ_3K_FS_STRNO     7
#define  SIZ_3K_FS_FRA       3
#define  SIZ_3K_FS_DIR       2
#define  SIZ_3K_FS_STR       30
#define  SIZ_3K_FS_SFX       5
#define  SIZ_3K_FS_UNIT      8
#define  SIZ_3K_FS_TONUM     6
#define  SIZ_3K_FS_CITY      17
#define  SIZ_3K_FM_STRNO     7
#define  SIZ_3K_FM_FRA       3
#define  SIZ_3K_FM_DIR       2
#define  SIZ_3K_FM_STR       30
#define  SIZ_3K_FM_SFX       5
#define  SIZ_3K_FM_UNIT      8
#define  SIZ_3K_FM_TONUM     6
#define  SIZ_3K_FM_CITY      17
#define  SIZ_3K_FM_ST        2
#define  SIZ_3K_FM_ZIP       9    // Zip+Zip4
#define  SIZ_3K_OUTADRFL     1

#define  SIZ_3K_MSGNAME1     50
#define  SIZ_3K_NORMNAME1    25
#define  SIZ_3K_NRMCLNNAME1  50
#define  SIZ_3K_SWAPNAME1    50
#define  SIZ_3K_SALCODE      2
#define  SIZ_3K_SALUTATION   4
#define  SIZ_3K_NAMESFXCD    2
#define  SIZ_3K_NAMESFX      3
#define  SIZ_3K_NAMEFLG      1

#define  SIZ_3K_CLAND        9
#define  SIZ_3K_CHOMESITE    9
#define  SIZ_3K_CSTRUC       9
#define  SIZ_3K_CGROW        9
#define  SIZ_3K_CFIXT        9
#define  SIZ_3K_CFIXTRL      9
#define  SIZ_3K_CPERS        9
#define  SIZ_3K_CMHPERS      9

#define  SIZ_3K_HAVE_WELL    1
#define  SIZ_3K_G_G_COUNT    1

#define  SIZ_3K_GDOCDATE     8
#define  SIZ_3K_GDOCNUM      16
#define  SIZ_3K_GDOCTYPE     10
#define  SIZ_3K_GSALEPRICE   10
#define  SIZ_3K_GRANTOR      20

#define  SIZ_3K_APN_DASH     15
#define  SIZ_3K_LASTSALEDT   8
#define  SIZ_3K_LASTSALEAMT  10

//#define  MON_SIZ_3K_BOOKPAGE              5
#define  MON_SIZ_3K_NCODE                 4
#define  MON_SIZ_3K_APPRID                4
#define  MON_SIZ_3K_NCSUFFIX              20
#define  MON_SIZ_3K_AREADESIG             4
#define  MON_SIZ_3K_AREADESC              50
#define  MON_SIZ_3K_APPRNAME              50
//#define  MON_SIZ_3K_DTS                   1
//#define  MON_SIZ_3K_USERID                8

#define  MAX_EXE_RECS        5
#define  MAX_TAX_RECS        6
#define  MAX_SALE_RECS       3
#define  MAX_GRGR_RECS       3

typedef struct _tMBExe
{
   char  Status;                 // Asmt status
   char  Code[SIZ_3K_EXECD];     // Exemption code
   char  HO_Flg;                 // HO flag
   char  MaxAmt[SIZ_3K_EXEAMT];  // Max exemption amount
   char  Percent[SIZ_3K_EXEPCT]; // Percent exempt
} MB_EXE;

typedef struct _tMBSale
{
   char  DocNum[SIZ_3K_SDOCNUM];
   char  SaleDate[SIZ_3K_SALEDATE];
   char  SaleCode[SIZ_3K_SALECODE];
   char  Seller[SIZ_3K_SELLER];
   char  Buyer[SIZ_3K_BUYER];
   char  SalePrice[SIZ_3K_CONFPRICE];
   char  DocTax[SIZ_3K_DOCTAX];
   char  GroupSale;
   char  GroupAsmt[SIZ_3K_GRPASMT];
   char  TransType[SIZ_3K_TRANSTYPE];
   char  AdjReasonCode[SIZ_3K_AR_CODE];
   char  ConfirmCode[SIZ_3K_CONFCODE];
} MB_SALE;

typedef struct _tMBTax
{
   char  TaxAmt1[SIZ_3K_TAXAMT];
   char  TaxAmt2[SIZ_3K_TAXAMT];
   char  PenAmt1[SIZ_3K_PENAMT];
   char  PenAmt2[SIZ_3K_PENAMT];
   char  PenChrgDt1[SIZ_3K_PENDATE];
   char  PenChrgDt2[SIZ_3K_PENDATE];
   char  PaidAmt1[SIZ_3K_PAIDAMT];
   char  PaidAmt2[SIZ_3K_PAIDAMT];
   char  TotPaid1[SIZ_3K_TOTPAID];
   char  TotPaid2[SIZ_3K_TOTPAID];
   char  TotFees[SIZ_3K_TOTALFEES];
   char  PaidDt1[SIZ_3K_PAIDDATE];
   char  PaidDt2[SIZ_3K_PAIDDATE];
   char  TaxYear[SIZ_3K_TAXYEAR];
   char  RollCat[SIZ_3K_ROLLCAT];
} MB_TAX;

typedef struct _tMBGrGr
{
   char  Apn[SIZ_3K_APN];
   char  DocDate[SIZ_3K_GDOCDATE];
   char  DocNum[SIZ_3K_GDOCNUM];
   char  DocType[SIZ_3K_GDOCTYPE];
   char  SalePrice[SIZ_3K_GSALEPRICE];
   char  Grantors[2][SIZ_3K_GRANTOR];
   char  Grantees[2][SIZ_3K_GRANTOR];
   char  ApnMatch;
   char  OwnerMatch;
   char  CRLF[2];
} MB_GRGR;
#define  GRGR_SIZE   (sizeof(MB_GRGR)-(SIZ_3K_APN+2))

#define  OFF_GR_DOCNUM        0
#define  OFF_GR_APN           16
#define  OFF_GR_DOCDATE       36
#define  OFF_GR_TITLE         44
#define  OFF_GR_TAX           114
#define  OFF_GR_SALE          124
#define  OFF_GR_NUMPG         134
#define  OFF_GR_NAMECNT       138
#define  OFF_GR_AMATCH        142
#define  OFF_GR_OMATCH        143
#define  OFF_GR_MORENAM       144
#define  OFF_GR_NAME          145
#define  OFF_GR_REF           345

#define  SIZ_GR_DOCNUM        16
#define  SIZ_GR_APN           20
#define  SIZ_GR_DOCDATE       8
#define  SIZ_GR_TITLE         70
#define  SIZ_GR_TAX           10
#define  SIZ_GR_SALE          10
#define  SIZ_GR_NUMPG         4
#define  SIZ_GR_NAMECNT       4
#define  SIZ_GR_NAME          50
#define  SIZ_GR_REF           100
#define  SIZ_GR_LEGAL         50
#define  SIZ_GR_PRCLCNT       3
#define  SIZ_GR_SOURCETABLE   2
#define  SIZ_GR_DOCTYPE       8

typedef struct _tAlaGrGr
{
   char  DocNum[SIZ_GR_DOCNUM];
   char  APN[SIZ_GR_APN];      
   char  DocDate[SIZ_GR_DOCDATE];
   char  DocTitle[SIZ_GR_TITLE];
   char  Tax[SIZ_GR_TAX];
   char  SalePrice[SIZ_GR_SALE];
   char  NumPages[SIZ_GR_NUMPG];
   char  NameCnt[SIZ_GR_NAMECNT];
   char  APN_Matched;
   char  Owner_Matched;
   char  MoreName;
   char  Grantor[2][SIZ_GR_NAME];
   char  Grantee[2][SIZ_GR_NAME];
   char  ReferenceData[SIZ_GR_REF];
   char  CRLF[2];
   char  filler;
} ALAGRGR;

typedef struct _tNRec
{
   char  NameType[1];
   char  Name[SIZ_GR_NAME];
} NREC;

typedef struct _tARec
{  // 90 bytes
   char  strNum[SIZ_M_STRNUM];
   char  strSub[SIZ_M_STR_SUB];
   char  strDir[SIZ_M_DIR];
   char  strName[SIZ_M_STREET+6];
   char  strSfx[SIZ_M_SUFF+1];
   char  Unit[SIZ_M_UNITNO+2];
   char  filler[6];
   char  City[SIZ_M_CITY];
   char  State[SIZ_M_ST];
   char  Zip[SIZ_M_ZIP];
   char  Zip4[SIZ_M_ZIP4];
} AREC;

/*
#define  OFF_GR_DOCNUM  1
#define  OFF_GR_APN     17
#define  OFF_GR_DOCDATE 37
#define  OFF_GR_TITLE   45
#define  OFF_GR_TAX     115
#define  OFF_GR_SALE    125
#define  OFF_GR_NUMPG   135
#define  OFF_GR_NAMECNT 139
#define  OFF_GR_AMATCH  143
#define  OFF_GR_OMATCH  144
#define  OFF_GR_MORENAM 145
#define  OFF_GR_GRTOR   146
#define  OFF_GR_GRTEE   397
#define  OFF_GR_REF     648
*/
#define  SIZ_GR_UNFMTAPN   40
typedef struct _tGrgrDef
{  // RecSize=1163
   char  DocNum[SIZ_GR_DOCNUM];              // 1
   char  APN[SIZ_GR_APN];                    // 17
   char  DocDate[SIZ_GR_DOCDATE];            // 37
   char  DocTitle[SIZ_GR_TITLE];             // 45
   char  Tax[SIZ_GR_TAX];                    // 115
   char  SalePrice[SIZ_GR_SALE];             // 125
   char  NumPages[SIZ_GR_NUMPG];             // 135
   char  NameCnt[SIZ_GR_NAMECNT];            // 139
   char  APN_Matched;                        // 143
   char  Owner_Matched;                      // 144
   char  MoreName;
   NREC  Grantors[MAX_NAMES];                // 146
   NREC  Grantees[MAX_NAMES];                // 401
   char  ReferenceData[SIZ_GR_REF];          // 656
   AREC  Situs;                              // 756
   char  Legal[SIZ_GR_LEGAL];                // 846
   char  CurOwner[SIZ_GR_NAME];              // 896
   char  CurCareOf[SIZ_GR_NAME];             // 946
   AREC  CurMailing;                         // 996
   char  CurDocDate[SIZ_TRANSFER_DT];        // 1086
   char  CurDocNum[SIZ_TRANSFER_DOC];        // 1094
   char  ParcelCount[SIZ_GR_PRCLCNT];        // 1106
   char  SourceTable[SIZ_GR_SOURCETABLE];    // 1109
   char  Unformatted_APN[SIZ_GR_UNFMTAPN];   // 1111
   char  DocType[SIZ_GR_DOCTYPE];            // 1151 Added 11/04/2018
   char  NoneSale;                           // 1159 Not sale transaction Y/N
   char  NoneXfer;                           // 1160 Misc document (not known transfer related)
   char  MultiApn;                           // 1161 Y/N
   char  CRLF[2];                            // 1162
} GRGR_DEF;

/***********************************************************************************
 *
 ***********************************************************************************/

typedef struct _tOwnerName
{
   char  acName1[64];
   char  acName2[64];
   char  acSwapName[64];
   char  acCareOf[64];
   char  acDba[64];
   char  acVest[16];
   char  acTitle[16];
   char  acOF[32];
   char  acOM[32];
   char  acOL[32];
   char  acSF[32];
   char  acSM[32];
   char  acSL[32];
} OWNER;

/***********************************************************************************
 *
 * Lien extract record layout
 * This is start out with CCX.  As other counties may include different values,
 * this structure may need more changes.  See Crest.rec
 *
 ***********************************************************************************/

#define  SIZ_LIEN_APN         14
#define  SIZ_LIEN_EXECODE     2
#define  SIZ_LIEN_EXECODEX    3
#define  SIZ_LIEN_EXECODEM    4
#define  SIZ_LIEN_TAXCODE     3
#define  SIZ_LIEN_AMT         10
#define  SIZ_LIEN_EXEAMT      10
#define  SIZ_LIEN_LAND        10
#define  SIZ_LIEN_IMPR        10
#define  SIZ_LIEN_PERS        10
#define  SIZ_LIEN_FIXT        10
#define  SIZ_LIEN_HO_FLG      1
#define  SIZ_LIEN_EXCL_TYPE   1
#define  SIZ_LIEN_PERS_KEY    1
#define  SIZ_LIEN_PERS_EXE    9
#define  SIZ_LIEN_FIXT_EXE    9
#define  SIZ_LIEN_HO_CNT      3
#define  SIZ_LIEN_HO_EXE      9
#define  SIZ_LIEN_REL_EXE     9
#define  SIZ_LIEN_GROSS       10
#define  SIZ_LIEN_OTHERS      10
#define  SIZ_LIEN_RATIO       3
#define  SIZ_LIEN_YEAR        4
#define  SIZ_LIEN_GROWING     10
#define  SIZ_LIEN_EXEVAL      9
#define  SIZ_LIEN_LATEPEN     9

typedef struct _tExePair
{
   char  Code[SIZ_LIEN_EXECODEM];
   char  Amt[SIZ_LIEN_EXEAMT];
} EXE_PAIR;

typedef struct _tMBLienExtr
{  // 87-byte record
   char  BusProp[SIZ_LIEN_FIXT];          // 
   char  PP_MobileHome[SIZ_LIEN_FIXT];    // 
   char  GrowImpr[SIZ_LIEN_FIXT];         // 
   char  ExeCode1[SIZ_LIEN_EXECODEX];
   char  ExeCode2[SIZ_LIEN_EXECODEX];
   char  ExeCode3[SIZ_LIEN_EXECODEX];
   char  FixtureRP[SIZ_LIEN_FIXT];        // 
} MB_LIENEXTR;

typedef struct _tCresLienExtr
{  // 87-byte record
   char  PP_MobileHome[SIZ_LIEN_FIXT];
   char  LeaseVal[SIZ_LIEN_FIXT];
   char  PersFixtr[SIZ_LIEN_FIXT];
   char  Exe_Code1[SIZ_LIEN_EXECODE];
   char  Exe_Amt1[SIZ_LIEN_EXEVAL];
   char  Exe_Code2[SIZ_LIEN_EXECODE];
   char  Exe_Amt2[SIZ_LIEN_EXEVAL];
   char  Exe_Code3[SIZ_LIEN_EXECODE];
   char  Exe_Amt3[SIZ_LIEN_EXEVAL];
   char  Exe_Code4[SIZ_LIEN_EXECODE];
   char  Exe_Amt4[SIZ_LIEN_EXEVAL];
   char  filler[13];
} CRES_LIENEXTR;

typedef struct _tAlaLienExtr
{
   char  Hpp[SIZ_LIEN_AMT];           // Household PP
   char  CLCA_Land[SIZ_LIEN_AMT];     // Land Conservation
   char  CLCA_Impr[SIZ_LIEN_AMT];     // Impr Conservation
   char  filler[9];
} ALA_LIENEXTR;

typedef struct _tCcxLienExtr
{
   char  Exe_Code1[SIZ_LIEN_EXECODE];
   char  Exe_Amt1[SIZ_LIEN_EXEVAL];
   char  Exe_Code2[SIZ_LIEN_EXECODE];
   char  Exe_Amt2[SIZ_LIEN_EXEVAL];
   char  Exe_Code3[SIZ_LIEN_EXECODE];
   char  Exe_Amt3[SIZ_LIEN_EXEVAL];
   char  Tax_CodeA;
   char  Tax_CodeB;
} CCX_LIENEXTR;

typedef struct _tEdxLienExtr
{
   char  Plants[SIZ_LIEN_AMT];
   char  Timber[SIZ_LIEN_AMT];
   char  Mineral[SIZ_LIEN_AMT];
   char  OthImpr[SIZ_LIEN_AMT];
   char  BusInv[SIZ_LIEN_AMT];
   char  Exe_Code1[SIZ_LIEN_EXECODE];
   char  Exe_Amt1[SIZ_LIEN_EXEVAL];
   char  Exe_Code2[SIZ_LIEN_EXECODE];
   char  Exe_Amt2[SIZ_LIEN_EXEVAL];
   char  Exe_Code3[SIZ_LIEN_EXECODE];
   char  Exe_Amt3[SIZ_LIEN_EXEVAL];
} EDX_LIENEXTR;

typedef struct _tFreLienExtr
{
   char  MH_Val[SIZ_LIEN_AMT];
   char  HOExe[SIZ_LIEN_EXEVAL];
   char  ClsExe[SIZ_LIEN_EXEVAL];
   char  ClsExe_Cnt;
} FRE_LIENEXTR;

#define  KER_SIZ_ALTAPN             9
#define  KER_SIZ_TE_NO              10
typedef struct _tKerLienExtr
{
   char  Te_No[KER_SIZ_TE_NO];
   char  AltApn[KER_SIZ_ALTAPN];
   char  acMineral[SIZ_LIEN_AMT];
   char  Exe_Code1[SIZ_LIEN_EXECODE];
} KER_LIENEXTR;

typedef struct _tKinLienExtr
{
   char  GrowImpr[SIZ_LIEN_FIXT];         // Tree/vines
   char  Exe_Code1[SIZ_LIEN_EXECODE];
   char  Exe_Amt1[SIZ_LIEN_EXEVAL];
   char  Exe_Code2[SIZ_LIEN_EXECODE];
   char  Exe_Amt2[SIZ_LIEN_EXEVAL];
   char  Exe_Code3[SIZ_LIEN_EXECODE];
   char  Exe_Amt3[SIZ_LIEN_EXEVAL];
} KIN_LIENEXTR;

typedef struct _tLaxLienExtr
{
   char  acExclType[SIZ_LIEN_EXCL_TYPE];
   char  acPP_Key[SIZ_LIEN_PERS_KEY];
   char  acPP_Exe[SIZ_LIEN_EXEVAL];
   char  acME_Exe[SIZ_LIEN_EXEVAL];
   char  acHOCnt[SIZ_LIEN_HO_CNT];
   char  acHOExe[SIZ_LIEN_EXEVAL];
   char  acRelExe[SIZ_LIEN_EXEVAL];       // Religious Exemption
   char  Exe_Code1[SIZ_LIEN_EXECODE];     
   char  Exe_Code2[SIZ_LIEN_EXECODE];
} LAX_LIENEXTR;

typedef struct _tMenLienExtr
{
   char  LivingImpr[SIZ_LIEN_AMT];        // Living Impr
   char  acWil_TV[SIZ_LIEN_AMT];          // Williamson trees/vines
   char  acWil_Land[SIZ_LIEN_AMT];        // Williamson land
   char  acOtherExe[SIZ_LIEN_REL_EXE];
} MEN_LIENEXTR;

typedef struct _tMnoLienExtr
{  // Exe code: R=Religion, W=Welfare, V=Veteran, O=Other
   char  MH_Val[SIZ_LIEN_AMT];     
   char  Other_Val[SIZ_LIEN_AMT];     
   char  Exe_Code1;
   char  Exe_Amt1[SIZ_LIEN_EXEVAL];
   char  Exe_Code2;
   char  Exe_Amt2[SIZ_LIEN_EXEVAL];
   char  TaxExempt;                       // Y=Tax exempt, public prop
} MNO_LIENEXTR;

typedef struct _tMpaLienExtr
{
   char  Timber[SIZ_LIEN_AMT];
   char  Wil_Land[SIZ_LIEN_AMT];          // Williamson land
   char  Wil_Mkt[SIZ_LIEN_AMT];           // Williamson market value
   char  TV_Val[SIZ_LIEN_AMT];            // Trees/Vines
} MPA_LIENEXTR;

typedef struct _tMrnLienExtr
{
   char  Bus_Inv[SIZ_LIEN_FIXT];
   char  Exe_Code2[SIZ_LIEN_EXECODE];
   char  Exe_Amt2[SIZ_LIEN_EXEVAL];
   char  Exe_Code3[SIZ_LIEN_EXECODE];
   char  Exe_Amt3[SIZ_LIEN_EXEVAL];
   char  Exe_Code4[SIZ_LIEN_EXECODE];
   char  Exe_Amt4[SIZ_LIEN_EXEVAL];
} MRN_LIENEXTR;

typedef struct _tNevLienExtr
{  // 87-byte record
   char  LotAcre[SIZ_LOT_ACRES];
   char  LotSqft[SIZ_LOT_SQFT];
   char  filler[87-(SIZ_LOT_ACRES+SIZ_LOT_SQFT)];
} NEV_LIENEXTR;

typedef struct _tOrgLienExtr
{
   char  OtherImpr[SIZ_LIEN_FIXT];
   char  Exe_Code1;
   char  Exe_Amt1[SIZ_LIEN_EXEVAL];
   char  Exe_Code2;
   char  Exe_Amt2[SIZ_LIEN_EXEVAL];
   //char  filler[57];
} ORG_LIENEXTR;

typedef struct _tRivLienExtr
{  
   char  XrefApn[SIZ_LIEN_APN]; 
   char  Bus_Inv[SIZ_LIEN_FIXT];       
   char  LivingImpr[SIZ_LIEN_AMT];           // Living Impr or Trees/Vines
   char  DelqYr[SIZ_LIEN_YEAR]; 
   char  TaxCode[SIZ_LIEN_TAXCODE];
   char  Exe_Code1[SIZ_LIEN_EXECODE];
   char  Exe_Amt1[SIZ_LIEN_EXEVAL];
   char  Exe_Code2[SIZ_LIEN_EXECODE];
   char  Exe_Amt2[SIZ_LIEN_EXEVAL];
   char  Exe_Code3[SIZ_LIEN_EXECODE];
   char  Exe_Amt3[SIZ_LIEN_EXEVAL];
   //char  filler[16];
} RIV_LIENEXTR;

typedef struct _tSacLienExtr
{
   char  Bus_Inv[SIZ_LIEN_FIXT];
   char  HO_Exe[SIZ_LIEN_EXEVAL];
   char  Rel_Exe[SIZ_LIEN_EXEVAL];  
   char  Vet_Exe[SIZ_LIEN_EXEVAL];  
   char  ActionCode[2];
} SAC_LIENEXTR;

typedef struct _tSbxLienExtr
{
   char  LivImpr[SIZ_LIEN_FIXT];             // Growing impr
   char  PP_MH[SIZ_LIEN_FIXT];      
   char  UnitVal[SIZ_LIEN_FIXT];             // Other impr
   char  Oth_Exe[SIZ_LIEN_EXEVAL];  
   char  NonTaxCode[SIZ_LIEN_EXECODE];
   char  Exe_Code1[SIZ_LIEN_EXECODE];
} SBX_LIENEXTR;

typedef struct _tSclLienExtr
{
   char  ParcelFlag1;                        // Parcel flag (0=assessed, 1=non-assessed)
   char  ParcelFlag2;                        // Parcel flag2 (P=public, S=SBE, N=assessed) (All 'N')
} SCL_LIENEXTR;

typedef struct _tScrLienExtr
{
   EXE_PAIR Exe[3];                          // 14x3
   char  LivImpr[SIZ_LIEN_FIXT];             // Growing impr
} SCR_LIENEXTR;

typedef struct _tSdxLienExtr
{
   char  Tax_Status;
   char  Exe_Code1;
   char  Exe_Amt1[SIZ_LIEN_EXEVAL];
   char  Exe_Code2;
   char  Exe_Amt2[SIZ_LIEN_EXEVAL];
   char  Exe_Code3;
   char  Exe_Amt3[SIZ_LIEN_EXEVAL];
   //char  filler[8];
} SDX_LIENEXTR;

typedef struct _tSfxLienExtr
{
   char  HO_Exe[SIZ_LIEN_EXEVAL];
   char  Misc_Exe[SIZ_LIEN_EXEVAL];  
   char  Roll_Status;
   //char  filler[20];
} SFX_LIENEXTR;

typedef struct _tSjxLienExtr
{
   char  HomeSite[SIZ_LIEN_FIXT];
   char  Tree[SIZ_LIEN_FIXT];     // Trees/Vines
} SJX_LIENEXTR;

typedef struct _tSloLienExtr
{
   char  Exe_Code1[SIZ_LIEN_EXECODE];
   char  Exe_Amt1[SIZ_LIEN_EXEVAL];
   char  Exe_Code2[SIZ_LIEN_EXECODE];
   char  Exe_Amt2[SIZ_LIEN_EXEVAL];
   char  Exe_Code3[SIZ_LIEN_EXECODE];
   char  Exe_Amt3[SIZ_LIEN_EXEVAL];
   char  Exe_Code4[SIZ_LIEN_EXECODE];
   char  Exe_Amt4[SIZ_LIEN_EXEVAL];
   char  Exe_Code5[SIZ_LIEN_EXECODE];
   char  Exe_Amt5[SIZ_LIEN_EXEVAL];
   char  Exe_Code6[SIZ_LIEN_EXECODE];
   char  Exe_Amt6[SIZ_LIEN_EXEVAL];
} SLO_LIENEXTR;

typedef struct _tSmxLienExtr
{
   char  Mineral[SIZ_LIEN_FIXT];  
   char  Root[SIZ_LIEN_FIXT];  
   char  Exe_Code[SIZ_LIEN_EXECODEM];
} SMX_LIENEXTR;

typedef struct _tSolLienExtr
{
   char  Tree[SIZ_LIEN_FIXT];
   char  Mineral[SIZ_LIEN_FIXT];
   char  HO_Exe[SIZ_LIEN_EXEVAL];  
   char  Mil_Exe[SIZ_LIEN_EXEVAL];  
   char  Wel_Exe[SIZ_LIEN_EXEVAL];  
   char  Exe_Codes[3][SIZ_LIEN_EXECODE];
} SOL_LIENEXTR;

typedef struct _tSutLienExtr
{
   char  LivingImpr[SIZ_LIEN_FIXT];    // Trees/Vines or LivingImpr
   char  HO_Exe[SIZ_LIEN_EXEVAL];  
   char  Other_Exe[SIZ_LIEN_EXEVAL];  
   char  DV_Exe[SIZ_LIEN_EXEVAL];  
   char  LDV_Exe[SIZ_LIEN_EXEVAL];  
} SUT_LIENEXTR;

#define TULSIZ_EXECODE     3
typedef struct _tTulLienExtr
{  
   char  PrevApn[SIZ_LIEN_APN];        // 104 - or Orig Asmt 
   char  FeeAsmt[SIZ_LIEN_APN];        // 118
   char  Bus_Inv[SIZ_LIEN_FIXT];       // 132
   char  GrowImpr[SIZ_LIEN_FIXT];  
   char  Exe_Code1[TULSIZ_EXECODE];
   char  Exe_Amt1[SIZ_LIEN_EXEVAL];
   char  Exe_Code2[TULSIZ_EXECODE];
   char  Exe_Amt2[SIZ_LIEN_EXEVAL];
   char  Exe_Code3[TULSIZ_EXECODE];
   char  Exe_Amt3[SIZ_LIEN_EXEVAL];
} TUL_LIENEXTR;

#define VENSIZ_EXECODE     6
typedef struct _tVenLienExtr
{
   char  PrevApn[SIZ_LIEN_APN];
   char  Tree[SIZ_LIEN_FIXT];
   char  Mineral[SIZ_LIEN_FIXT];  
   char  Exe_Code1[VENSIZ_EXECODE];
   char  Exe_Amt1[SIZ_LIEN_EXEVAL];
   char  Exe_Code2[VENSIZ_EXECODE];
   char  Exe_Amt2[SIZ_LIEN_EXEVAL];
   char  Exe_Code3[VENSIZ_EXECODE];
   char  Exe_Amt3[SIZ_LIEN_EXEVAL];
   char  filler[8];     // 87-79
} VEN_LIENEXTR;

#define  OFF_LIEN_APN         1
#define  OFF_LIEN_YEAR        15
#define  OFF_LIEN_EXECODE     19
#define  OFF_LIEN_EXEAMT      21
#define  OFF_LIEN_LAND        31
#define  OFF_LIEN_IMPR        41
#define  OFF_LIEN_PERS        51
#define  OFF_LIEN_FIXT        61
#define  OFF_LIEN_HO_FLG      71
#define  OFF_LIEN_GROSS       72
#define  OFF_LIEN_OTHERS      82
#define  OFF_LIEN_RATIO       92
#define  OFF_LIEN_TRA         95
#define  OFF_LIEN_SPCLFLAG    103
#define  OFF_LIEN_EXTRA       104
#define  OFF_LIEN_TAXCODE     191

// Special flag
#define  LX_FULLEXE_FLG       '1'
#define  LX_PROP8_FLG         '2'
#define  LX_OTHER_1           '4'
#define  LX_OTHER_2           '8'

// Notes: This should be expand to 256 bytes and
// adding full exemption flag
typedef struct _tLienExtr
{
   // 256-byte record
   char  acApn[SIZ_LIEN_APN];             // 1
   char  acYear[SIZ_LIEN_YEAR];           // 15
   char  acExCode[SIZ_LIEN_EXECODE];      // 19 - See extra section for more info
   char  acExAmt[SIZ_LIEN_EXEAMT];        // 21 - Total Exe
   char  acLand[SIZ_LIEN_LAND];           // 31
   char  acImpr[SIZ_LIEN_IMPR];           // 41
   char  acPP_Val[SIZ_LIEN_PERS];         // 51 - Personal property
   char  acME_Val[SIZ_LIEN_FIXT];         // 61 - Fixture or manufacturing equipment
   char  acHO[SIZ_LIEN_HO_FLG];           // 71 - 1=Y, 2=N
   char  acGross[SIZ_LIEN_GROSS];         // 72
   char  acOther[SIZ_LIEN_OTHERS];        // 82
   char  acRatio[SIZ_LIEN_RATIO];         // 92
   char  acTRA[SIZ_TRA];                  // 95
   char  SpclFlag;                        // 103 - See definition above

   union
   {
      ALA_LIENEXTR   Ala;
      CCX_LIENEXTR   Ccx;
      CRES_LIENEXTR  Cres;
      EDX_LIENEXTR   Edx;
      FRE_LIENEXTR   Fre;
      KER_LIENEXTR   Ker;
      KIN_LIENEXTR   Kin;
      LAX_LIENEXTR   Lax;
      MB_LIENEXTR    MB;
      MEN_LIENEXTR   Men;
      MNO_LIENEXTR   Mno;
      MPA_LIENEXTR   Mpa;
      MRN_LIENEXTR   Mrn;
      NEV_LIENEXTR   Nev;
      ORG_LIENEXTR   Org;
      RIV_LIENEXTR   Riv;
      SAC_LIENEXTR   Sac;
      SBX_LIENEXTR   Sbx;
      SCL_LIENEXTR   Scl;
      SCR_LIENEXTR   Scr;
      SDX_LIENEXTR   Sdx;
      SFX_LIENEXTR   Sfx;
      SJX_LIENEXTR   Sjx;
      SLO_LIENEXTR   Slo;
      SMX_LIENEXTR   Smx;
      SOL_LIENEXTR   Sol;
      SUT_LIENEXTR   Sut;
      TUL_LIENEXTR   Tul;
      VEN_LIENEXTR   Ven;
      char  filler[87];
   } extra;

   char  acTaxCode[SIZ_LIEN_TAXCODE];        // 191
   char  acPrevApn[SIZ_LIEN_APN];            // 194
   char  acNetTaxValue[SIZ_LIEN_GROSS];      // 208
   char  acLatePen[SIZ_LIEN_LATEPEN];        // 218 - SOL
   char  acAltApn[SIZ_LIEN_APN];             // 227 - SCR
   char  AsmtType;                           // 241
   char  filler[12];                         // 242
   char  AsmtStatus;                         // 254
   char  LF[2];
} LIENEXTR;

typedef struct _tLienExt1
{
   // 144-byte record
   char  acApn[SIZ_LIEN_APN];
   char  acExCode[SIZ_LIEN_EXECODE];
   char  acExAmt[SIZ_LIEN_EXEAMT];        // Total Exe
   char  acLand[SIZ_LIEN_LAND];
   char  acImpr[SIZ_LIEN_IMPR];
   char  acPP_Val[SIZ_LIEN_PERS];         // Personal property
   char  acME_Val[SIZ_LIEN_FIXT];         // Fixture or manufacturing equipment
   char  acHO[SIZ_LIEN_HO_FLG];

   union
   {
      char  filler[39];
   } extra;

   // Added for additional info
   char  acGross[SIZ_LIEN_GROSS];
   char  acOther[SIZ_LIEN_OTHERS];
   char  acRatio[SIZ_LIEN_RATIO];
   char  acYear[SIZ_LIEN_YEAR];
   char  acTRA[SIZ_TRA];
   char  SpclFlag;                        // See definition above
   char  LF[2];
} LIENEXT1;

typedef struct _tBppRec
{  
   char  Apn            [SIZ_LIEN_APN ];  // 1
   char  Fixtr          [SIZ_LIEN_FIXT];  // 15
   char  PersProp       [SIZ_LIEN_FIXT];  // 25
   char  PP_MH          [SIZ_LIEN_FIXT];  // 35
   char  Fixtr_RP       [SIZ_LIEN_FIXT];  // 45
   char  HH_PP          [SIZ_LIEN_FIXT];  // 55
   char  CRLF[2];                         // 65
} BPP_REC;

#define  CNTY_ALA             0x01000000
#define  CNTY_ALP             0x02000000
#define  CNTY_AMA             0x03000000
#define  CNTY_BUT             0x04000000
#define  CNTY_CAL             0x05000000
#define  CNTY_COL             0x06000000
#define  CNTY_CCX             0x07000000
#define  CNTY_DNX             0x08000000
#define  CNTY_EDX             0x09000000
#define  CNTY_FRE             0x0A000000
#define  CNTY_GLE             0x0B000000
#define  CNTY_HUM             0x0C000000
#define  CNTY_IMP             0x0D000000
#define  CNTY_INY             0x0E000000
#define  CNTY_KER             0x0F000000
#define  CNTY_KIN             0x10000000
#define  CNTY_LAK             0x11000000
#define  CNTY_LAS             0x12000000
#define  CNTY_LAX             0x13000000
#define  CNTY_MAD             0x14000000
#define  CNTY_MRN             0x15000000
#define  CNTY_MPA             0x16000000
#define  CNTY_MEN             0x17000000
#define  CNTY_MER             0x18000000
#define  CNTY_MOD             0x19000000
#define  CNTY_MNO             0x1A000000
#define  CNTY_MON             0x1B000000
#define  CNTY_NAP             0x1C000000
#define  CNTY_NEV             0x1D000000
#define  CNTY_ORG             0x1E000000
#define  CNTY_PLA             0x1F000000
#define  CNTY_PLU             0x20000000
#define  CNTY_RIV             0x21000000
#define  CNTY_SAC             0x22000000
#define  CNTY_SBT             0x23000000
#define  CNTY_SBD             0x24000000
#define  CNTY_SDX             0x25000000
#define  CNTY_SFX             0x26000000
#define  CNTY_SJX             0x27000000
#define  CNTY_SLO             0x28000000
#define  CNTY_SMX             0x29000000
#define  CNTY_SBX             0x2A000000
#define  CNTY_SCL             0x2B000000
#define  CNTY_SCR             0x2C000000
#define  CNTY_SHA             0x2D000000
#define  CNTY_SIE             0x2E000000
#define  CNTY_SIS             0x2F000000
#define  CNTY_SOL             0x30000000
#define  CNTY_SON             0x31000000
#define  CNTY_STA             0x32000000
#define  CNTY_SUT             0x33000000
#define  CNTY_TEH             0x34000000
#define  CNTY_TRI             0x35000000
#define  CNTY_TUL             0x36000000
#define  CNTY_TUO             0x37000000
#define  CNTY_VEN             0x38000000
#define  CNTY_YOL             0x39000000
#define  CNTY_YUB             0x3A000000

#define  GRP_ALA              1
#define  GRP_CRES             2
#define  GRP_MB               3
#define  GRP_EDX              9
#define  GRP_FRE              10
#define  GRP_KER              15
#define  GRP_LAX              19
#define  GRP_MPA              20
#define  GRP_MRN              21
#define  GRP_MEN              23
#define  GRP_MNO              26
#define  GRP_NEV              29
#define  GRP_ORG              30
#define  GRP_RIV              33
#define  GRP_SAC              34
#define  GRP_SDX              37
#define  GRP_SFX              38
#define  GRP_SJX              39
#define  GRP_SBX              42
#define  GRP_SCL              43
#define  GRP_SOL              48
#define  GRP_SUT              51
#define  GRP_TUL              54
#define  GRP_VEN              56

#endif