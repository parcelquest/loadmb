#ifndef  _MERGENEV_H_
#define  _MERGENEV_H_

// Copy from HUM
#define  NEV_CHAR_FEEPARCEL            0
#define  NEV_CHAR_POOLSPA              1
#define  NEV_CHAR_CATTYPE              2
#define  NEV_CHAR_QUALITYCLASS         3
#define  NEV_CHAR_YRBLT                4
#define  NEV_CHAR_BUILDINGSIZE         5
#define  NEV_CHAR_ATTACHGARAGESF       6
#define  NEV_CHAR_DETACHGARAGESF       7
#define  NEV_CHAR_CARPORTSF            8
#define  NEV_CHAR_HEATING              9
#define  NEV_CHAR_COOLINGCENTRALAC     10
#define  NEV_CHAR_COOLINGEVAPORATIVE   11
#define  NEV_CHAR_COOLINGROOMWALL      12
#define  NEV_CHAR_COOLINGWINDOW        13
#define  NEV_CHAR_STORIESCNT           14
#define  NEV_CHAR_UNITSCNT             15
#define  NEV_CHAR_TOTALROOMS           16
#define  NEV_CHAR_EFFYR                17
#define  NEV_CHAR_PATIOSF              18
#define  NEV_CHAR_BEDROOMS             19
#define  NEV_CHAR_BATHROOMS            20
#define  NEV_CHAR_HALFBATHS            21
#define  NEV_CHAR_FIREPLACE            22
#define  NEV_CHAR_ASMT                 23
#define  NEV_CHAR_BLDGSEQNUM           24
#define  NEV_CHAR_HASWELL              25
#define  NEV_CHAR_LOTSQFT              26

// NEV has no fireplace data - 20180618
static XLAT_CODE  asFirePlace[] =
{  
   // Value, lookup code, value length
   "1", "1", 1,               // 1
   "2", "2", 1,               // 2
   "3", "3", 1,               // 3+
   "N", "N", 1,               // No
   "Y", "Y", 1,               // Yes
   "",   "", 0
};

// NEV has no sewer data - 20180618
static XLAT_CODE  asSewer[] =
{
   // Value, lookup code, value length 
   "E", "E", 1,               // Engineered system
   "O", "Z", 1,               // Other than sewer or septic
   "S", "Y", 1,               // Sewer
   "T", "S", 1,               // Septic tank
   "",   "",  0
};

// NEV has no water data - 20180618
static XLAT_CODE  asWaterSrc[] = 
{
   // Value, lookup code, value length
   "1", "P", 2,               // Community
   "2", "W", 2,               // Well
   "4", "Y", 2,               // Other
   "",   "",  0
};

static XLAT_CODE  asHeating[] =
{  
   // Value, lookup code, value length
   "B", "8", 1,               // Baseboard
   "C", "Z", 1,               // Central 
   "E", "7", 1,               // Electric
   "F", "C", 1,               // Floor
   "P", "6", 1,               // Portable (new)
   "W", "D", 1,               // Wall Unit
   "",   "",  0
};

// NEV - 20180629
static XLAT_CODE  asPool[] =
{ 
   // Value, lookup code, value length
   "1",  "P", 1,              // 1 Pool
   "2",  "P", 1,              // 2 Pools
   "B",  "C", 1,              // Both
   "IP", "I", 2,              // Indoor Pool
   "LP", "P", 2,              // Lap Pool
   "P",  "P", 1,              // Permanent type swimming Pool
   "PS", "C", 2,              // Pool/Spa
   "S",  "S", 1,              // Spa
   "T",  "Y", 1,              // Hot tub/spa
   "",   "",  0
};


IDX_TBL5 NEV_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "00", "75", 'Y', 2, 2,
   "01", "1 ", 'N', 2, 2,     // 100% transfer
   "03", "1 ", 'N', 2, 2,     // 
   "06", "1 ", 'N', 2, 2,     // 
   "08", "75", 'Y', 2, 2,     // 
   "10", "75", 'Y', 2, 2,     // 
   "17", "75", 'Y', 2, 2,     // 
   "18", "75", 'Y', 2, 2,     // 
   "","",0,0,0
};

USEXREF  Nev_UseTbl[] =
{
   "00","120",              //VACANT-RESIDENTIAL TO 2.5 AC
   "01","115",              //MOBILEHOME ON UP TO 2.5 ACS.
   "02","100",              //NON-RES IMPRVMNT TO 2.5 ACS.
   "05","120",              //VACANT - MULTI-RESIDENTIAL  
   "11","100",              //RESIDENTIAL IMPRVD TO 2.5 AC
   "12","100",              //MULTI-RESID. 2-3 UNITS      
   "13","100",              //MULTI-RESID. 4 PLUS UNITS   
   "14","100",              //CONDMINIUMS/TOWNHOUSES     '
   "15","813",              //POSS. INT.-FOREST SER. CABIN
   "16","115",              //MOBILEHOMES                 
   "17","791",              //OPEN SPACE CONTRACTS        
   "21","175",              //RURAL VACANT 2.5-20 ACRES   
   "22","175",              //RURAL IMPRVD 2.5-20 ACRES   
   "23","175",              //RURAL SUB-ECONOMIC UNIT     
   "24","175",              //RURAL ECONOMIC UNIT 20+ACRES
   "25","175",              //RURAL RESTRICTED - CLCA     
   "26","175",              //RURAL RESTRICTED - CLCA     
   "28","115",              //MOBILEHOME - 2.5+ ACRES     
   "29","100",              //NON-RES IMPRVMNT -2.5+ ACRES
   "30","835",              //COMMERCIAL VACANT           
   "31","202",              //COMMERCIAL -MINOR IMPRVMNT  
   "32","219",              //COMMERCIAL -MAJOR IMPRVMNT  
   "33","110",              //MOTELS                      
   "34","234",              //SERVICE STATIONS            
   "35","114",              //MOBILEHOME PARKS            
   "40","190",              //INDUSTRIAL VACANT           
   "41","400",              //INDUSTRIAL IMPROVED         
   "50","535",              //TIMBER PRESERVE ZONES       
   "51","536",              //TIMBER RIGHTS               
   "60","600",              //RECREATIONAL PROPERTIES     
   "70","783",              //MINERAL RIGHTS              
   "75","801",              //GRAZING RIGHTS              
   "80","100",              //RESIDENTIAL - TIMESHARE     
   "", ""
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 NEV_Exemption[] = 
{
   "E01", "H", 3,1,
   "E02", "D", 3,1,     // DISABLED VET (LOW INCOME)
   "E03", "D", 3,1,     // DISABLED VET (BASIC)
   "E05", "V", 3,1,     // VETERAN
   "E06", "R", 3,1,     // CHARITABLE & RELIGIOUS/WELFARE
   "E09", "C", 3,1,     // CHURCH
   "E10", "R", 3,1,
   "E12", "S", 3,1, 
   "E13", "I", 3,1,
   "E17", "S", 3,1,
   "E18", "E", 3,1,
   "E19", "E", 3,1,
   "E20", "E", 3,1,
   "E21", "X", 3,1,     // LESSOR/LESSEE
   "E22", "X", 3,1,     // LESSOR/LESSEE LAND
   "E23", "X", 3,1,     // LESSOR/LESSEE STRUCTURE
   "E24", "P", 3,1,     // PUBLIC SCHOOL
   "E25", "P", 3,1,     // PUBLIC SCHOOL - LAND
   "E26", "P", 3,1,
   "E27", "P", 3,1,
   "E30", "X", 3,1,     // LOW VALUE SECURED
   "E31", "X", 3,1,     // LOW VALUE UNSECURED
   "E34", "M", 3,1,     // MUSEUM & LIBRARY
   "E35", "M", 3,1,     // MUSEUM & LIBRARY - LAND
   "E36", "M", 3,1,     // MUSEUM & LIBRARY - STRUCTURE
   "E37", "M", 3,1,     // MUSEUM & LIBRARY - PERSONAL PROPERTY
   "E40", "W", 3,1,     // WELFARE
   "E41", "W", 3,1,     // WELFARE UNSECURED
   "E42", "W", 3,1,     // WELFARE - LAND
   "E43", "W", 3,1,     // WELFARE - STRUCTURE
   "E44", "W", 3,1,     // WELFARE - PERSONAL PROPERTY
   "E45", "W", 3,1,     // WELFARE - REPORTING ONLY
   "E50", "R", 3,1,     // RELIGIOUS 
   "E51", "R", 3,1,     // RELIGIOUS UNSECURED
   "E52", "R", 3,1,     // RELIGIOUS - LAND
   "E56", "R", 3,1,     // RELIGIOUS - STRUCTURE
   "E57", "R", 3,1,     // RELIGIOUS - PERSONAL PROPERTY
   "E61", "E", 3,1,     // CHURCH UNSECURED
   "E62", "E", 3,1,     // BOE NOT OVER 40,000
   "E87", "C", 3,1,     // CHURCH - PERSONAL PROPERTY
   "E88", "C", 3,1,     // CHURCH - LAND
   "E89", "C", 3,1,     // CHURCH - STRUCTURE
   "E90", "C", 3,1,     // CHURCH
   "E91", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E92", "Y", 3,1,     // SOLDIERS & SAILORS
   "E98", "X", 3,1,     // OTHER
   "","",0,0
};

#endif
