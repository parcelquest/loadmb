#ifndef _MERGESIS_H
#define _MERGESIS_H

#define  LSIZ_APN                     12
#define  LSIZ_TRA                     6
#define  LSIZ_USECODE                 4
#define  LSIZ_VALUE                   9
#define  LSIZ_OTHEXE_CODE             3
#define  LSIZ_VALSET_TYPE             2
#define  LSIZ_TAXABILITY              3
#define  LSIZ_DATE                    8

/* not use
typedef struct _tSisLien
{  // 150-byte
   char Apn[LSIZ_APN];
   char Tra[LSIZ_TRA];                          // 13
   char UseCode[LSIZ_USECODE];                  // 19
   char Land[LSIZ_VALUE];                       // 23
   char Structure[LSIZ_VALUE];                  // 32
   char Growing[LSIZ_VALUE];                    // 41
   char Fixtures[LSIZ_VALUE];                   // 50
   char FixturesRealProp[LSIZ_VALUE];           // 59
   char PPBusiness[LSIZ_VALUE];                 // 68
   char PPMH[LSIZ_VALUE];                       // 77
   char Homesite[LSIZ_VALUE];                   // 86
   char HO_Exe[LSIZ_VALUE];                     // 95
   char OtherExe[LSIZ_VALUE];                   // 104
   char OtherExeCode[LSIZ_OTHEXE_CODE];         // 113
   char EnrolledValueSetType[LSIZ_VALSET_TYPE]; // 116
   char PPPenaltyIncluded;                      // 118
   char Taxability[LSIZ_TAXABILITY];            // 119
   char RCStatus;                               // 122
   char BaseDate[LSIZ_DATE];                    // 123
   char MHPPIncluded;                           // 131
   char IsAnotherValueSet;                      // 132
   char ValueDate[LSIZ_DATE];                   // 133
   char TaxRollDate[LSIZ_DATE];                 // 141
   char CrLf[2];                                // 149
} SIS_LIEN;

*/
// _ROLL
/* 
#define SIS_ROLL_ASMT            0
#define SIS_ROLL_FEEPARCEL       1
#define SIS_ROLL_ASMTDESC        2  // Type of bussiness
#define SIS_ROLL_TRA             3
#define SIS_ROLL_ZONING          4
#define SIS_ROLL_USECODE         5
#define SIS_ROLL_NBHCODE         6
#define SIS_ROLL_ACRES           7
#define SIS_ROLL_DOCNUM          8
#define SIS_ROLL_DOCDATE         9
#define SIS_ROLL_OWNER           10
#define SIS_ROLL_DBA             11
#define SIS_ROLL_M_ADDR1         12
#define SIS_ROLL_M_ADDR2         13
#define SIS_ROLL_M_ADDR3         14
#define SIS_ROLL_M_ADDR4         15
#define SIS_ROLL_LAND            16
#define SIS_ROLL_IMPR            17
#define SIS_ROLL_FIXTRS          18
#define SIS_ROLL_FIXTR_RP        19
#define SIS_ROLL_BUSPROP         20
#define SIS_ROLL_PPMOBILHOME     21
#define SIS_ROLL_HO_EXE          22
#define SIS_ROLL_OTH_EXE         23
#define SIS_ROLL_NET_ASSD_VAL    24
#define SIS_ROLL_OTH_EXECODE     25
#define SIS_ROLL_POOLS           26
#define SIS_ROLL_QUALITY         27
#define SIS_ROLL_YRBLT           28
#define SIS_ROLL_YREFF           29
#define SIS_ROLL_BLDGSQFT        30
#define SIS_ROLL_GARSQFT         31
#define SIS_ROLL_GARTYPE         32
#define SIS_ROLL_HEATING         33
#define SIS_ROLL_COOLING         34
#define SIS_ROLL_BEDS            35
#define SIS_ROLL_UNFIN_SQFT      36
#define SIS_ROLL_FBATHS          37
#define SIS_ROLL_HBATHS          38
#define SIS_ROLL_ROOMS           39
#define SIS_ROLL_FP              40
#define SIS_ROLL_FLOORS          41
#define SIS_ROLL_S_STRNAME       42
#define SIS_ROLL_S_STRNUM        43
#define SIS_ROLL_S_STRTYPE       44
#define SIS_ROLL_S_STRDIR        45
#define SIS_ROLL_S_UNITNO        46
#define SIS_ROLL_S_COMMUNITY     47
#define SIS_ROLL_S_ZIP           48
#define SIS_ROLL_SECTION         49
#define SIS_ROLL_TOWNSHIP        50
#define SIS_ROLL_RANGE           51
#define SIS_ROLL_LEGAL           52
#define SIS_ROLL_LDN             53
#define SIS_ROLL_ROLLYEAR        54
#define SIS_ROLL_TPZ             55
#define SIS_ROLL_APZ             56
#define SIS_ROLL_STATUS          57
#define SIS_ROLL_STATUS1         58
#define SIS_ROLL_NOTES           59
#define SIS_ROLL_NOTES1          60
*/
// New layout 20091021
#define SIS_ROLL_ASMT            0
#define SIS_ROLL_FEEPARCEL       1
#define SIS_ROLL_TRA             2
#define SIS_ROLL_ZONING          3
#define SIS_ROLL_USECODE         4
#define SIS_ROLL_NBHCODE         5
#define SIS_ROLL_ACRES           6
#define SIS_ROLL_DOCNUM          7
#define SIS_ROLL_DOCDATE         8
#define SIS_ROLL_OWNER           9
#define SIS_ROLL_DBA             10
#define SIS_ROLL_M_ADDR1         11
#define SIS_ROLL_M_ADDR2         12
#define SIS_ROLL_M_ADDR3         13
#define SIS_ROLL_M_ADDR4         14
#define SIS_ROLL_LAND            15
#define SIS_ROLL_IMPR            16
#define SIS_ROLL_FIXTRS          17
#define SIS_ROLL_FIXTR_RP        18
#define SIS_ROLL_BUSPROP         19
#define SIS_ROLL_PPMOBILHOME     20
#define SIS_ROLL_HO_EXE          21
#define SIS_ROLL_OTH_EXE         22
#define SIS_ROLL_NET_ASSD_VAL    23
#define SIS_ROLL_OTH_EXECODE     24
#define SIS_ROLL_POOLS           25
#define SIS_ROLL_QUALITY         26
#define SIS_ROLL_YRBLT           27
#define SIS_ROLL_YREFF           28
#define SIS_ROLL_BLDGSQFT        29
#define SIS_ROLL_GARSQFT         30
#define SIS_ROLL_GARTYPE         31
#define SIS_ROLL_HEATING         32
#define SIS_ROLL_COOLING         33
#define SIS_ROLL_BEDS            34
#define SIS_ROLL_UNFIN_SQFT      35
#define SIS_ROLL_FBATHS          36
#define SIS_ROLL_HBATHS          37
#define SIS_ROLL_ROOMS           38
#define SIS_ROLL_FP              39
#define SIS_ROLL_FLOORS          40
#define SIS_ROLL_S_STRNAME       41
#define SIS_ROLL_S_STRNUM        42
#define SIS_ROLL_S_STRTYPE       43
#define SIS_ROLL_S_STRDIR        44
#define SIS_ROLL_S_UNITNO        45
#define SIS_ROLL_S_COMMUNITY     46
#define SIS_ROLL_S_ZIP           47
#define SIS_ROLL_SECTION         48
#define SIS_ROLL_TOWNSHIP        49
#define SIS_ROLL_RANGE           50
#define SIS_ROLL_LEGAL           51
#define SIS_ROLL_LDN             52
#define SIS_ROLL_ROLLYEAR        53
#define SIS_ROLL_TPZ             54
#define SIS_ROLL_APZ             55
#define SIS_ROLL_STATUS          56
#define SIS_ROLL_STATUS1         57
#define SIS_ROLL_NOTES           58
#define SIS_ROLL_NOTES1          59
#define SIS_ROLL_MODEL_DESC      60
#define SIS_ROLL_FLDS            61

#define  SIS_MH_ASMT             0
#define  SIS_MH_ASMTYEAR         1
#define  SIS_MH_DECALNUM         2
#define  SIS_MH_DOCNUM           3
#define  SIS_MH_EVENTDATE        4
#define  SIS_MH_YEARBUILT        5
#define  SIS_MH_EFFECTIVEAGE     6
#define  SIS_MH_MAKE             7
#define  SIS_MH_MODEL            8
#define  SIS_MH_SIZESQFT         9
#define  SIS_MH_TOTALROOMS       10
#define  SIS_MH_BEDROOMS         11
#define  SIS_MH_BATHROOMS        12
#define  SIS_MH_ASMTSTATUS       13

typedef struct _tSisCharRec
{  // 128 bytes
   char  Asmt[MBSIZ_CHAR_ASMT];                 // 1
   char  FeeParcel[MBSIZ_CHAR_ASMT];            // 13
   char  NumPools[MBSIZ_CHAR_POOLS];            // 25
   char  LandUseCat[MBSIZ_CHAR_USECAT];         // 27
   char  QualityClass[MBSIZ_CHAR_QUALITY];      // 29
   char  YearBuilt[MBSIZ_CHAR_YRBLT];           // 35
   char  YearEff[MBSIZ_CHAR_YRBLT];             // 39
   char  BuildingSize[MBSIZ_CHAR_BLDGSQFT];     // 43
   char  SqFTGarage[MBSIZ_CHAR_GARSQFT];        // 52
   char  Heating[MBSIZ_CHAR_HEATING];           // 61
   char  Cooling[MBSIZ_CHAR_COOLING];           // 63
   char  HeatingSource[MBSIZ_CHAR_HEATSRC];     // 65
   char  CoolingSource[MBSIZ_CHAR_COOLSRC];     // 67
   char  NumBedrooms[MBSIZ_CHAR_BEDS];          // 69
   char  NumFullBaths[MBSIZ_CHAR_FBATHS];       // 72
   char  NumHalfBaths[MBSIZ_CHAR_HBATHS];       // 75
   char  NumFireplaces[MBSIZ_CHAR_FP];          // 78
   char  Stories[MBSIZ_CHAR_NUMFLOORS];         // 80
   char  LotSqft[MBSIZ_CHAR_LANDSQFT];          // 83
   char  TotalRooms[MBSIZ_CHAR_TOTALROOMS];     // 92
   char  ParkType[MBSIZ_CHAR_PARKTYPE];         // 96
   char  ParkSpaces[MBSIZ_CHAR_PARKSPACES];     // 100
   char  TRS[SIZ_TRS];                          // 104
   char  HasSeptic;                             // 118
   char  HasSewer;                              // 119
   char  HasWell;                               // 120
   char  filler[6];                             // 121
   char  CRLF[2];                               // 127
} SIS_CHAR;

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SIS_Exemption[] = 
{
   "E01", "H", 3, 1,    // HOMEOWNERS EXEMPTION
   "E02", "V", 3, 1,    // INTER-COUNTY VETERAN EXEMPTION
   "E04", "V", 3, 1,    // VETERAN EXEMPTION
   "E05", "C", 3, 1,    // CHURCH EXEMPTION
   "E06", "W", 3, 1,    // WELFARE EXEMPTION
   "E07", "E", 3, 1,    // CEMETERY EXEMPTION
   "E08", "S", 3, 1,    // SCHOOL EXEMPTION
   "E09", "M", 3, 1,    // MUSEUM EXEMPTION
   "E10", "L", 3, 1,    // LIBRARY EXEMPTION
   "E11", "D", 3, 1,    // DISABLED VET EXEMPT - LIMITED INCOME
   "E12", "V", 3, 1,    // VETERAN ORGANIZATION EXEMPT
   "E13", "D", 3, 1,    // DISABLED VET EXEMPT - NO INCOME LIMIT
   "E14", "D", 3, 1,    // DISABLED VET EXEMPT PARTIAL
   "E20", "X", 3, 1,    // LOW-INCOME TRIBAL HOUSING
   "E25", "X", 3, 1,    // HOMEOWNERS 25% PENALTY
   "E35", "R", 3, 1,    // RELIGIOUS EXEMPTION
   "E47", "X", 3, 1,    // HISTORICAL AIRCRAFT
   "E58", "X", 3, 1,    // LOW VALUE EXEMPT - PI (SPECIAL W/ IMPS) [<2,001]
   "E59", "X", 3, 1,    // LOW VALUE EXEMPT - PI (<2,001)
   "E60", "X", 3, 1,    // LOW VALUE EXEMPT - (<1,001)
   "E61", "X", 3, 1,    // LOW VALUE EXEMPT - PP/FIXT (<2,001)
   "E62", "X", 3, 1,    // LOW VALUE EXEMPT - PP M/H (<2,001)
   "E63", "V", 3, 1,    // PARTIAL VETERAN ORGANIZATION
   "E64", "V", 3, 1,    // PARTIAL VETERAN ORGANIZATION
   "E65", "V", 3, 1,    // PARTIAL VETERAN ORGANIZATION
   "E70", "W", 3, 1,    // PARTIAL WELFARE EXEMPT
   "E71", "W", 3, 1,    // PARTIAL WELFARE EXEMPT
   "E72", "W", 3, 1,    // PARTIAL WELFARE EXEMPT
   "E73", "I", 3, 1,    // PARTIAL HOSPITAL WELFARE EXEMPT
   "E74", "I", 3, 1,    // PARTIAL HOSPITAL WELFARE EXEMPT
   "E75", "I", 3, 1,    // PARTIAL HOSPITAL WELFARE EXEMPT
   "E76", "I", 3, 1,    // HOSPITAL WELFARE EXEMPT
   "E80", "C", 3, 1,    // PARTIAL CHURCH EXEMPT
   "E81", "C", 3, 1,    // PARTIAL CHURCH EXEMPT
   "E82", "C", 3, 1,    // PARTIAL CHURCH EXEMPT
   "E90", "R", 3, 1,    // PARTIAL RELIGIOUS EXEMPT
   "E91", "R", 3, 1,    // PARTIAL RELIGIOUS EXEMPT
   "E92", "R", 3, 1,    // PARTIAL RELIGIOUS EXEMPT
   "E98", "X", 3, 1,    // OTHER EXEMP
   "","",0,0
};

#endif