#ifndef  _MERGEEDX_H_
#define  _MERGEEDX_H_

// EL_DORADO_COUNTY_SECURED_2018_ROLL_COMPLETE.TXT
#define  EDX_LDR_OLDAPN                0
#define  EDX_LDR_OLD_FMTAPN            1
#define  EDX_LDR_NEW_FMTAPN            2
#define  EDX_LDR_TRA                   3
#define  EDX_LDR_STATUS                4
#define  EDX_LDR_AGENCY                5
#define  EDX_LDR_CLUSTER               6
#define  EDX_LDR_USECODE_1             7
#define  EDX_LDR_USECODE_2             8
#define  EDX_LDR_DATE_ADDED            9
#define  EDX_LDR_ACREAGE               10
#define  EDX_LDR_CUR_REC_BOOK_PAGE     11
#define  EDX_LDR_CUR_REC_DATE          12
#define  EDX_LDR_HIST1_REC_BOOK_PAGE   13
#define  EDX_LDR_HIST1_REC_DATE        14
#define  EDX_LDR_HIST2_REC_BOOK_PAGE   15
#define  EDX_LDR_HIST2_REC_DATE        16
#define  EDX_LDR_NAME_1                17
#define  EDX_LDR_NAME_2                18
#define  EDX_LDR_TYPE_CODE             19
#define  EDX_LDR_MAILING_STREET        20
#define  EDX_LDR_MAILING_ZIP_CODE      21
#define  EDX_LDR_S_STREET_NUMBER       22
#define  EDX_LDR_S_STREET_NAME         23
#define  EDX_LDR_S_UNIT_TYPE           24
#define  EDX_LDR_S_UNIT_NUMBER         25
#define  EDX_LDR_S_STREET_DIRECTION    26
#define  EDX_LDR_S_STREET_TYPE         27
#define  EDX_LDR_S_HOUSE_SUFFIX        28
#define  EDX_LDR_S_AREA                29
#define  EDX_LDR_LEADLINE              30
#define  EDX_LDR_SUBDIVISION_NUMBER    31
#define  EDX_LDR_CUR_TRACT             32
#define  EDX_LDR_CUR_DISTRICT          33
#define  EDX_LDR_CUR_PRIMARY_ZONING    34
#define  EDX_LDR_CUR_ZONING_CHG_DATE   35
#define  EDX_LDR_CUR_SECONDARY_ZONING  36
#define  EDX_LDR_CUR_TRPA_AMOUNT       37
#define  EDX_LDR_HIST_TRACT            38
#define  EDX_LDR_HIST_DISTRICT         39
#define  EDX_LDR_HIST_PRIMARY_ZONING   40
#define  EDX_LDR_HIST_SECONDARY_ZONING 41
#define  EDX_LDR_PARCEL_CREATE_DATE    42
#define  EDX_LDR_CREATE_CODE           43
#define  EDX_LDR_LAND_VALUE            44
#define  EDX_LDR_TIMBER_VALUE          45
#define  EDX_LDR_MINERAL_GRAZING_VALUE 46
#define  EDX_LDR_STRUCTURE_VALUE       47
#define  EDX_LDR_EQUIPMENT_VALUE       48
#define  EDX_LDR_PLANTS_TREE_VALUE     49
#define  EDX_LDR_IMPR_OTHER_VALUE      50
#define  EDX_LDR_BUSINV_VALUE          51
#define  EDX_LDR_BUSINV_PEN_PCT        52
#define  EDX_LDR_PP_OTHER              53
#define  EDX_LDR_PP_PEN_PCT            54
#define  EDX_LDR_FILLER1               55
#define  EDX_LDR_VALUE_CHANGE_DATE     56
#define  EDX_LDR_APPRAISAL_DATE        57
#define  EDX_LDR_APPRAISER_INITIALS    58
#define  EDX_LDR_CAAP_CODE             59
#define  EDX_LDR_GENERAL_PLAN_CODE     60
#define  EDX_LDR_DTT_AMOUNT            61
#define  EDX_LDR_DTT_CODE              62
#define  EDX_LDR_REAPPRIASAL_CODE      63
#define  EDX_LDR_1911_BOND_FLAG        64
#define  EDX_LDR_1915_BOND_FLAG        65
#define  EDX_LDR_OTHER_BOND_FLAG       66
#define  EDX_LDR_BUILDING_PERMIT_FLAG  67
#define  EDX_LDR_DELINQUENT_TAX_FLAG   68
#define  EDX_LDR_MOBILE_HOME_FLAG      69
#define  EDX_LDR_ETAL_NAME_FLAG        70
#define  EDX_LDR_BUSINESS_FORM_TYPE    71
#define  EDX_LDR_DIR_BILL              72
#define  EDX_LDR_ROLL_YEAR             73
#define  EDX_LDR_EXEMPTION_1_CODE      74
#define  EDX_LDR_EXEMPTION_1_AMOUNT    75
#define  EDX_LDR_EXEMPTION_1_YEAR      76
#define  EDX_LDR_EXEMPTION_2_CODE      77
#define  EDX_LDR_EXEMPTION_2_AMOUNT    78
#define  EDX_LDR_EXEMPTION_2_YEAR      79
#define  EDX_LDR_EXEMPTION_3_CODE      80
#define  EDX_LDR_EXEMPTION_3_AMOUNT    81
#define  EDX_LDR_EXEMPTION_3_YEAR      82
#define  EDX_LDR_QUALITY_CLASS         83
#define  EDX_LDR_STORIES               84
#define  EDX_LDR_BEDROOMS              85
#define  EDX_LDR_BATHROOMS             86
#define  EDX_LDR_TOTAL_ROOMS           87
#define  EDX_LDR_SQ_FT_IMPROV          88
#define  EDX_LDR_TOT_LIVING_UNITS      89
#define  EDX_LDR_YR_BLT                90
#define  EDX_LDR_YR_EFF                91
#define  EDX_LDR_STRUCT_FAIL_CODE      92
#define  EDX_LDR_BUILDING_COND         93
#define  EDX_LDR_FUNCT_PLAN_CODE       94
#define  EDX_LDR_BUILD_USE             95
#define  EDX_LDR_TOT_USABLE_SQFT       96
#define  EDX_LDR_WATER_SOURCE          97
#define  EDX_LDR_ACCESS                98
#define  EDX_LDR_TOPOGRAPHY            99
#define  EDX_LDR_GROUND_COVER          100
#define  EDX_LDR_WATERFRONT            101
#define  EDX_LDR_VIEW                  102
#define  EDX_LDR_NAT_GAS_SERV          103
#define  EDX_LDR_SEWER_SERV            104
#define  EDX_LDR_PART_COMPL_CODE       105
#define  EDX_LDR_COUNT                 106

// Old/New APN
#define  EDX_APN_OLD                   0
#define  EDX_FEEPARCEL_OLD             1
#define  EDX_APN_NEW                   2
#define  EDX_FEEPARCEL_NEW             3

// EDX_Owner.csv (copy from TUO)
#define  EDX_OWNR_ASMT                 0
#define  EDX_OWNR_ISPRIMARY            1
#define  EDX_OWNR_TITLE                2
#define  EDX_OWNR_OWNERFIRST           3
#define  EDX_OWNR_OWNERMIDDLE          4
#define  EDX_OWNR_OWNERLAST            5
#define  EDX_OWNR_OWNERSHIPPCT         6
#define  EDX_OWNR_DOCNUM               7
#define  EDX_OWNR_TITLETYPE            8
#define  EDX_OWNR_HWCODE               9
#define  EDX_OWNR_DTS                  10
#define  EDX_OWNR_OWNERFULL            11

// Copy from HUM
#define  EDX_CHAR_FEEPARCEL            0
#define  EDX_CHAR_POOLSPA              1
#define  EDX_CHAR_CATTYPE              2
#define  EDX_CHAR_QUALITYCLASS         3
#define  EDX_CHAR_YRBLT                4
#define  EDX_CHAR_BUILDINGSIZE         5
#define  EDX_CHAR_ATTACHGARAGESF       6
#define  EDX_CHAR_DETACHGARAGESF       7
#define  EDX_CHAR_CARPORTSF            8
#define  EDX_CHAR_HEATING              9
#define  EDX_CHAR_COOLINGCENTRALAC     10
#define  EDX_CHAR_COOLINGEVAPORATIVE   11
#define  EDX_CHAR_COOLINGROOMWALL      12
#define  EDX_CHAR_COOLINGWINDOW        13
#define  EDX_CHAR_STORIESCNT           14
#define  EDX_CHAR_UNITSCNT             15
#define  EDX_CHAR_TOTALROOMS           16
#define  EDX_CHAR_EFFYR                17
#define  EDX_CHAR_PATIOSF              18
#define  EDX_CHAR_BEDROOMS             19
#define  EDX_CHAR_BATHROOMS            20
#define  EDX_CHAR_HALFBATHS            21
#define  EDX_CHAR_FIREPLACE            22
#define  EDX_CHAR_ASMT                 23
#define  EDX_CHAR_BLDGSEQNUM           24
#define  EDX_CHAR_HASWELL              25
#define  EDX_CHAR_LOTSQFT              26

// EDX has no fireplace data - 20180618
static XLAT_CODE  asFirePlace[] =
{  
   // Value, lookup code, value length
   "1", "1", 1,               // 1
   "2", "2", 1,               // 2
   "3", "3", 1,               // 3+
   "N", "N", 1,               // No
   "Y", "Y", 1,               // Yes
   "",   "", 0
};

// EDX has no sewer data - 20180618
static XLAT_CODE  asSewer[] =
{
   // Value, lookup code, value length 
   "E", "E", 1,               // Engineered system
   "O", "Z", 1,               // Other than sewer or septic
   "S", "Y", 1,               // Sewer
   "T", "S", 1,               // Septic tank
   "",   "",  0
};

// EDX has no water data - 20180618
static XLAT_CODE  asWaterSrc[] = 
{
   // Value, lookup code, value length
   "1", "P", 2,               // Community
   "2", "W", 2,               // Well
   "4", "Y", 2,               // Other
   "",   "",  0
};

static XLAT_CODE  asHeating[] =
{  
   // Value, lookup code, value length
   "B", "F", 1,               // Baseboard
   "C", "Z", 1,               // Central 
   "E", "F", 1,               // Electric
   "F", "C", 1,               // Floor
   "P", "6", 1,               // Portable (new)
   "W", "D", 1,               // Wall Unit
   "",   "",  0
};

// EDX has no pool data - 20180618
static XLAT_CODE  asPool[] =
{ 
   // Value, lookup code, value length
   "1",  "P", 1,              // 1 Pool
   "2",  "P", 1,              // 2 Pools
   "3",  "C", 1,              // Pool/Spa
   "99", "P", 2,              // Pool
   "DB", "J", 2,              // Doughboy (new)
   "FG", "F", 2,              // Fiberglass
   "GU", "G", 2,              // Gunite
   "VI", "V", 2,              // Vinyl
   "",   "",  0
};

IDX_TBL5 EDX_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "01", "1 ", 'N', 2, 2,     // TRANSFER (Y)
   "02", "57", 'N', 2, 2,     // Partial Interest Transfer
   "03", "75", 'Y', 2, 2,     // DEATH -NO REAPPRAISAL (N)
   "04", "13", 'N', 2, 2,     // DEATH DATE - 100% REAPPRAISAL (Y)
   "05", "58", 'N', 2, 2,     // DEATH DATE - PARTIAL REAPPRAISAL (Y)
   "06", "13", 'N', 2, 2,     // PROP 58 100% REAPPRAISAL (Y)
   "07", "58", 'N', 2, 2,     // PROP 58 PARTIAL REAPPRAISAL (Y)
   "08", "67", 'N', 2, 2,     // TAX DEED REAPPRAISAL (Y)
   "09", "61", 'N', 2, 2,     // TRUST-REAPPRAISAL (Y)
   "10", "75", 'N', 2, 2,     // TIMESHARE- 100% REAPPRAISAL (Y)
   "11", "75", 'Y', 2, 2,     // 100% TRANSFER NO REAPPRAISAL (N)
   "12", "75", 'Y', 2, 2,     // PARTIAL TRANSFER NO REAPPRAISAL (N)
   "13", "18", 'Y', 2, 2,     // INTERSPOUSAL (N)
   "14", "61", 'Y', 2, 2,     // TRUST (N)
   "15", "13", 'Y', 2, 2,     // PROP 58 NO REAPPRAISAL (N)
   "16", "19", 'Y', 2, 2,     // CHANGE FORM OF TITLE (N)
   "17", "9 ", 'Y', 2, 2,     // CORRECT/PERFECT TITLE (N)
   "18", "19", 'Y', 2, 2,     // CAL VET-NO APPRAISAL (N)
   "19", "19", 'N', 2, 2,     // NEW MOBILE HOME-VOLUNTARY CONV (Y)
   "20", "74", 'Y', 2, 2,     // RESCISSIONS (Y - roll back value)
   "21", "13", 'N', 2, 2,     // END J/T OR LIFE EST 100% REAPP (Y)
   "22", "13", 'N', 2, 2,     // END J/T OR LIFE EST PART REAPP (Y)
   "23", "19", 'Y', 2, 2,     // ADD J/T (ORIG TRANS REMAINS) (N)
   "24", "19", 'Y', 2, 2,     // RETURN TO ORIG TRANSFEROR-(N)
   "25", "19", 'Y', 2, 2,     // RETAIN LIFE EST-NO REAPP (N)
   "26", "13", 'N', 2, 2,     // CREATE LIFE ESTATE-REAPPRAISAL (Y)
   "27", "19", 'Y', 2, 2,     // TERMINATE LIFE ESTATE-NO REAPP (N)
   "28", "75", 'N', 2, 2,     // TIMESHARE PARTIAL TRANSFER-REAPPRAISAL (Y)
   "29", "3 ", 'Y', 2, 2,     // JOINT TENANCY WITH A TRUST (N)
   "30", "19", 'Y', 2, 2,     // LEGAL ENTITY CHANGE (Y)
   "31", "75", 'Y', 2, 2,     // TRANSFER TO PUBLIC ENTITY (N)
   "32", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-NEW COMML/IND (Y)
   "33", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-NEW MH (Y)
   "34", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-NEW MISC (Y)
   "35", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-ADDS/ALT,SFR (Y)
   "36", "74", 'Y', 2, 2,     // NEW CONST-ADDS/ALT,MULTI (Y)
   "37", "74", 'Y', 2, 2,     // NEW CONST-ADDS/ALT,COMML/IND (Y)
   "38", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-ADDS/ALT,MH (Y)
   "39", "74", 'Y', 2, 2,     // AGRICULTURE BUILDINGS (Y)
   "40", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-REMOVAL/IMP (Y)
   "41", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-NEW MULTI (Y)
   "42", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-SOLAR (Y)
   "43", "74", 'Y', 2, 2,     // LIEN DATE UPDATE (Y)
   "44", "74", 'Y', 2, 2,     // CIP  RESIDENTIAL (Y)
   "45", "74", 'Y', 2, 2,     // CIP MULTI (Y)
   "46", "74", 'Y', 2, 2,     // CIP COMML/IND (Y)
   "47", "19", 'Y', 2, 2,     // PROP 8 (Y)
   "48", "19", 'Y', 2, 2,     // BOARD ORDER CHANGE (Y)
   "49", "18", 'Y', 2, 2,     // DOMESTIC PARTNER NO REAPPRAISAL (N)
   "50", "40", 'Y', 2, 2,     // CONSERVATION EASEMENTS (Y)
   "51", "74", 'Y', 2, 2,     // LAND CONSERVATION AGRMT (Y)
   "52", "74", 'Y', 2, 2,     // AMENDED LAND CONSERVATION AGRM (Y)
   "53", "74", 'Y', 2, 2,     // NON-RENEW LAND CONSERVATION AG (Y)
   "54", "74", 'Y', 2, 2,     // ANNUAL REVIEW TPZ (Y)
   "55", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-POOLS (Y)
   "56", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-PATIO COVERS (Y)
   "57", "74", 'Y', 2, 2,     // APPEAL (Y)
   "58", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-DECKS (Y)
   "59", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-GARAGE CONV (Y)
   "60", "19", 'Y', 2, 2,     // PROPOSITION 60 (Y)
                              // Propositions 60/90 amended section 2 of Article XIIIA of the California Constitution
                              // to allow a person who is over age 55 to sell his or her principal place of residence
                              // and transfer its base year value to a replacement dwelling of equal or lesser value
                              // that is purchased or newly constructed within two years of the sale.
   "61", "44", 'N', 2, 2,     // LEASE-REAPPRAISAL (Y)
   "62", "44", 'N', 2, 2,     // CANCEL LEASE-REAPPRAISAL (Y)
   "63", "44", 'Y', 2, 2,     // LEASE-NO REAPPRAISAL (N)
   "64", "44", 'Y', 2, 2,     // CANCEL LEASE-NO REAPPRAISAL (N)
   "65", "74", 'Y', 2, 2,     // BUSINESS AUDIT (Y)
   "66", "74", 'Y', 2, 2,     // PENALTY (Y)
   "67", "74", 'Y', 2, 2,     // CALAMITY FLOOD (Y)
   "68", "74", 'Y', 2, 2,     // CALAMITY (Y)
   "69", "74", 'Y', 2, 2,     // REMOVAL OF IMPROVEMENTS (Y)
   "70", "74", 'Y', 2, 2,     // NO ACTION BUILDING PERMIT (Y)
   "71", "74", 'Y', 2, 2,     // SPLIT (N)
   "72", "74", 'Y', 2, 2,     // SPLIT (Y)
   "73", "74", 'Y', 2, 2,     // COMBINATION (Y)
   "74", "74", 'Y', 2, 2,     // COMBINATION (N)
   "75", "74", 'Y', 2, 2,     // ROAD ABANDONMENT (Y)
   "76", "80", 'Y', 2, 2,     // ORDER OF POSSESSION (Y)
   "77", "57", 'Y', 2, 2,     // DIVISION OF INTERESTS (Y)
   "78", "74", 'Y', 2, 2,     // PAGE TRANSFER/BLOCKED PAGE (N)
   "79", "40", 'Y', 2, 2,     // EASEMENTS (Y)
   "80", "19", 'Y', 2, 2,     // PROP 8 REVIEW (Y)
   "81", "74", 'Y', 2, 2,     // MOBILE HOME ON FOUNDATION (Y)
   "82", "19", 'N', 2, 2,     // Timeshare partial reappraisable - bulk billed (Y)
   "83", "74", 'Y', 2, 2,     // NEW CONSTRUCTION - WELLS (Y)
   "84", "74", 'Y', 2, 2,     // MINING CLAIMS (Y)
   "85", "74", 'Y', 2, 2,     // MINES AND QUARRIES (Y)
   "86", "19", 'N', 2, 2,     // Timeshare 100% reappraisable - bulk billed project
   "87", "74", 'Y', 2, 2,     // AFFORDABLE HOUSING (Y)
   "88", "57", 'Y', 2, 2,     // POSSESSORY INTEREST (Y)
   "89", "74", 'Y', 2, 2,     // IREO (Y)
   "90", "77", 'N', 2, 2,     // FORECLOSURE-REAPPRAISAL (Y)
   "91", "25", 'N', 2, 2,     // SHERIFF'S DEED REAPPRAISAL (Y)
   "92", "19", 'Y', 2, 2,     // NEW MOBILE HOME (Y)
   "93", "19", 'Y', 2, 2,     // NEW CONSTRUCTION - NEW SFR (Y)
   "94", "74", 'Y', 2, 2,     // HOLDING COMPANY (Y)
   "95", "74", 'Y', 2, 2,     // UNRECORDED CONTRACT (Y)
   "96", "74", 'Y', 2, 2,     // MISCELLANEOUS (N - NO REAPP)
   "97", "74", 'Y', 2, 2,     // HOX-CLAIM REQUEST (N - NO REAPP)
   "98", "13", 'Y', 2, 2,     // PROBLEM DEED NO REAPP (N)
   "99", "13", 'N', 2, 2,     // PROBLEM DEED-REAPPRAISAL (Y)
   "","",0,0,0
};

USEXREF  Edx_UseTbl[] =
{
   "00","120",              //VACANT-RESIDENTIAL TO 2.5 AC
   "01","115",              //MOBILEHOME ON UP TO 2.5 ACS.
   "02","100",              //NON-RES IMPRVMNT TO 2.5 ACS.
   "05","120",              //VACANT - MULTI-RESIDENTIAL  
   "11","100",              //RESIDENTIAL IMPRVD TO 2.5 AC
   "12","100",              //MULTI-RESID. 2-3 UNITS      
   "13","100",              //MULTI-RESID. 4 PLUS UNITS   
   "14","100",              //CONDMINIUMS/TOWNHOUSES     '
   "15","813",              //POSS. INT.-FOREST SER. CABIN
   "16","115",              //MOBILEHOMES                 
   "17","791",              //OPEN SPACE CONTRACTS        
   "21","175",              //RURAL VACANT 2.5-20 ACRES   
   "22","175",              //RURAL IMPRVD 2.5-20 ACRES   
   "23","175",              //RURAL SUB-ECONOMIC UNIT     
   "24","175",              //RURAL ECONOMIC UNIT 20+ACRES
   "25","175",              //RURAL RESTRICTED - CLCA     
   "26","175",              //RURAL RESTRICTED - CLCA     
   "28","115",              //MOBILEHOME - 2.5+ ACRES     
   "29","100",              //NON-RES IMPRVMNT -2.5+ ACRES
   "30","835",              //COMMERCIAL VACANT           
   "31","202",              //COMMERCIAL -MINOR IMPRVMNT  
   "32","219",              //COMMERCIAL -MAJOR IMPRVMNT  
   "33","110",              //MOTELS                      
   "34","234",              //SERVICE STATIONS            
   "35","114",              //MOBILEHOME PARKS            
   "40","190",              //INDUSTRIAL VACANT           
   "41","400",              //INDUSTRIAL IMPROVED         
   "50","535",              //TIMBER PRESERVE ZONES       
   "51","536",              //TIMBER RIGHTS               
   "60","600",              //RECREATIONAL PROPERTIES     
   "70","783",              //MINERAL RIGHTS              
   "75","801",              //GRAZING RIGHTS              
   "80","100",              //RESIDENTIAL - TIMESHARE     
   "", ""
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 EDX_Exemption[] = 
{
   "E01", "H", 3,1,     // HOMEOWNERS EXEMPTION
   "E02", "V", 3,1,     // REGULAR VETERAN
   "E03", "X", 3,1,     // NOT VALID EXEMPTION CODE - USE WITH 801/802 BOE
   "E04", "W", 3,1,     // WELFARE (ORGANIZATIONAL)
   "E05", "C", 3,1,     // CHURCH
   "E06", "U", 3,1,     // COLLEGE
   "E07", "E", 3,1,     // CEMETARY
   "E08", "D", 3,1,     // DISABLED VETERAN
   "E09", "X", 3,1,     // CIVIC
   "E10", "P", 3,1,     // PUBLIC SCHOOLS
   "E11", "X", 3,1,     // LESSEE/LESSOR
   "E12", "R", 3,1,     // RELIGIOUS
   "E13", "M", 3,1,     // FREE MUSEUM/FREE LIBRARY
   "E14", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E16", "S", 3,1,     // WELFARE - PRIV PAR SCHOOL
   "E17", "I", 3,1,     // HOSPITAL
   "E18", "D", 3,1,     // LOW INCOME DISABLED VETERAN
   "E40", "W", 3,1,     // PARTIAL WEL-LAND
   "E41", "W", 3,1,     // PARTIAL WEL-IMPRV
   "E42", "W", 3,1,     // PARTIAL WELFARE PP
   "E43", "S", 3,1,     // PARTIAL WEL-SCHOOL LAND
   "E44", "S", 3,1,     // PARTIAL WEL SCHOOL IMPROVEMENT
   "E45", "S", 3,1,     // PARTIAL WEL SCHOOL PP
   "E46", "D", 3,1,     // PARTIAL DISABLED VET 100000
   "E47", "D", 3,1,     // PARTIAL DISABLED VET 150000
   "E50", "C", 3,1,     // PARTIAL CHURCH LAND
   "E51", "C", 3,1,     // PARTIAL CHURCH IMPROVEMENT
   "E52", "C", 3,1,     // PARTIAL CHURCH PP
   "E60", "R", 3,1,     // PARTIAL RELIGIOUS LAND
   "E61", "R", 3,1,     // PARTIAL RELIGIOUS IMPR
   "E62", "R", 3,1,     // PARTIAL RELIGIOUS PP
   "E63", "I", 3,1,     // PARTIAL HOSPITAL LAND
   "E64", "I", 3,1,     // PARTIAL HOSPITAL IMPR
   "E65", "I", 3,1,     // PARTIAL HOSPITAL PP
   "E88", "X", 3,1,     // UNSECURED LOW VALUE EXEMPTION
   "E98", "X", 3,1,     // OTHER
   "E99", "X", 3,1,     // LATE FILE FLAG
   "","",0,0
};

#endif
