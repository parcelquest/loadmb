
#if !defined(AFX_LOADMB_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_)
#define AFX_LOADMB_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"
#include "hlAdo.h"

#define  OPEN_ERR       0xF0000001
#define  READ_ERR       0xF0000002
#define  WRITE_ERR      0xF0000004

#define  UPDATE_R01     0
#define  CREATE_R01     1
#define  CREATE_LIEN    2
#define  CLEAR_R01      4

#define  LOAD_LIEN      0x10000000
#define  LOAD_UPDT      0x01000000
#define  LOAD_GRGR      0x00100000
#define  LOAD_SALE      0x00010000
#define  LOAD_ATTR      0x00001000
#define  LOAD_ASSR      0x00000100
#define  LOAD_DAILY     0x00000010
#define  UPDT_TAX       0x00000001

#define  EXTR_LIEN      0x20000000
#define  EXTR_CSAL      0x02000000
#define  EXTR_VALUE     0x00200000
#define  EXTR_SALE      0x00020000
#define  EXTR_ATTR      0x00002000
#define  EXTR_REGN      0x00000200
#define  EXTR_PRP8      0x00000020

#define  MERG_LIEN      0x40000000
#define  MERG_CSAL      0x04000000
#define  MERG_GRGR      0x00400000
#define  MERG_GISA      0x00040000
#define  MERG_ATTR      0x00004000
#define  MERG_SADR      0x00000400

#define  UPDT_XSAL      0x00080000
#define  UPDT_ASSR      0x00000800

// 2019
#define  L12_TAXYEAR                   0
#define  L12_ROLLCATEGORY              1
#define  L12_ASMT                      2
#define  L12_FEEPARCEL                 3
#define  L12_STATUS                    4
#define  L12_TRA                       5
#define  L12_TAXABILITY                6
#define  L12_OWNER                     7
#define  L12_ASSESSEE                  8
#define  L12_MAILADDRESS1              9
#define  L12_MAILADDRESS2              10
#define  L12_MAILADDRESS3              11
#define  L12_MAILADDRESS4              12
#define  L12_SITUS1                    13
#define  L12_SITUS2                    14
#define  L12_PARCELDESCRIPTION         15
#define  L12_LANDVALUE                 16
#define  L12_STRUCTUREVALUE            17
#define  L12_FIXTURESVALUE             18
#define  L12_GROWINGVALUE              19
#define  L12_FIXTURESRPVALUE           20
#define  L12_MHPPVALUE                 21
#define  L12_PPVALUE                   22
#define  L12_HOX                       23
#define  L12_OTHEREXEMPTION            24
#define  L12_OTHEREXEMPTIONCODE        25
#define  L12_LANDUSE1                  26
#define  L12_LANDSIZE                  27
#define  L12_ACRES                     28
// Added 2020 - "2020 Calaveras Secured CD.txt"
#define  L12_BUILDINGTYPE              29
#define  L12_QUALITYCLASS              30
#define  L12_BUILDINGSIZE              31
#define  L12_YEARBUILT                 32
#define  L12_TOTALAREA                 33
#define  L12_BEDROOMS                  34
#define  L12_BATHS                     35
#define  L12_HALFBATHS                 36
#define  L12_TOTALROOMS                37
#define  L12_GARAGE                    38
#define  L12_GARAGESIZE                39
#define  L12_HEATING                   40
#define  L12_AC                        41
#define  L12_FIREPLACE                 42
#define  L12_POOLSPA                   43
#define  L12_STORIES                   44
#define  L12_UNITS                     45
#define  L12_CURRENTDOCNUM             46
#define  L12_ISAGPRESERVE              47
#define  L12_FLDS                      48

// 2018
#define  L7_TAXYEAR                    0
#define  L7_ROLLCATEGORY               1
#define  L7_ASMT                       2
#define  L7_FEEPARCEL                  3
#define  L7_ASMTSTATUS                 4
#define  L7_TRA                        5
#define  L7_TAXABILITYFULL             6
#define  L7_OWNER                      7
#define  L7_ASSESSEENAME               8
#define  L7_MAILADDRESS1               9
#define  L7_MAILADDRESS2               10
#define  L7_MAILADDRESS3               11
#define  L7_MAILADDRESS4               12
#define  L7_BARCODEZIP                 13
#define  L7_SITUS1                     14
#define  L7_SITUS2                     15
#define  L7_PARCELDESCRIPTION          16
#define  L7_LANDVALUE                  17
#define  L7_STRUCTUREVALUE             18
#define  L7_FIXTURESVALUE              19
#define  L7_GROWING                    20
#define  L7_FIXTURESRP                 21
#define  L7_MHPPVALUE                  22
#define  L7_PPVALUE                    23
#define  L7_HOX                        24
#define  L7_OTHEREXEMPTION             25
#define  L7_OTHEREXEMPTIONCODE         26
#define  L7_LANDUSE1                   27
#define  L7_ACRES                      28

// 2017
#define  L3_TAXYEAR                    0
#define  L3_ROLLCATEGORY               1
#define  L3_ASMT                       2
#define  L3_FEEPARCEL                  3
#define  L3_ASMTSTATUS                 4
#define  L3_TRA                        5
#define  L3_TAXABILITYFULL             6
#define  L3_OWNER                      7
#define  L3_ASSESSEENAME               8
#define  L3_MAILADDRESS1               9
#define  L3_MAILADDRESS2               10
#define  L3_MAILADDRESS3               11
#define  L3_MAILADDRESS4               12
#define  L3_BARCODEZIP                 13
#define  L3_SITUS1                     14
#define  L3_SITUS2                     15
#define  L3_PARCELDESCRIPTION          16
#define  L3_LANDVALUE                  17
#define  L3_STRUCTUREVALUE             18
#define  L3_FIXTURESVALUE              19
#define  L3_GROWING                    20
#define  L3_FIXTURESRP                 21
#define  L3_MHPPVALUE                  22
#define  L3_PPVALUE                    23
#define  L3_HOX                        24
#define  L3_OTHEREXEMPTION             25
#define  L3_OTHEREXEMPTIONCODE         26
#define  L3_LANDUSE1                   27
#define  L3_LANDSIZE                   28
#define  L3_ACRES                      29
#define  L3_BUILDINGTYPE               30
#define  L3_QUALITYCLASS               31
#define  L3_BUILDINGSIZE               32
#define  L3_YEARBUILT                  33
#define  L3_TOTALAREA                  34
#define  L3_BEDROOMS                   35
#define  L3_BATHS                      36
#define  L3_HALFBATHS                  37
#define  L3_TOTALROOMS                 38
#define  L3_GARAGE                     39
#define  L3_GARAGESIZE                 40
#define  L3_HEATING                    41
#define  L3_AC                         42
#define  L3_FIREPLACE                  43
#define  L3_POOLSPA                    44
#define  L3_STORIES                    45
#define  L3_UNITS                      46
#define  L3_CURRENTDOCNUM              47
#define  L3_ISAGPRESERVE               48    // Added 2021

// 2023
#define  L32_TAXYEAR                   0
#define  L32_ROLLCATEGORY              1
#define  L32_ASMT                      2
#define  L32_FEEPARCEL                 3
#define  L32_ASMTSTATUS                4
#define  L32_TRA                       5
#define  L32_TAXABILITYFULL            6
#define  L32_OWNER                     7
#define  L32_ASSESSEENAME              8
#define  L32_MAILADDRESS1              9
#define  L32_MAILADDRESS2              10
#define  L32_MAILADDRESS3              11
#define  L32_MAILADDRESS4              12
#define  L32_BARCODEZIP                13
#define  L32_SITUS1                    14
#define  L32_SITUS2                    15
#define  L32_PARCELDESCRIPTION         16
#define  L32_LANDVALUE                 17
#define  L32_STRUCTUREVALUE            18
#define  L32_FIXTURESVALUE             19
#define  L32_GROWING                   20
#define  L32_FIXTURESRP                21
#define  L32_MHPPVALUE                 22
#define  L32_PPVALUE                   23
#define  L32_HOX                       24
#define  L32_OTHEREXEMPTION            25
#define  L32_OTHEREXEMPTIONCODE        26
#define  L32_LANDUSE1                  27
#define  L32_LANDSIZE                  28
#define  L32_ACRES                     29
#define  L32_QUALITYCLASS              30
#define  L32_BUILDINGSIZE              31
#define  L32_YEARBUILT                 32
#define  L32_TOTALAREA                 33
#define  L32_BEDROOMS                  34
#define  L32_BATHS                     35
#define  L32_HALFBATHS                 36
#define  L32_TOTALROOMS                37
#define  L32_GARAGE                    38
#define  L32_GARAGESIZE                39
#define  L32_HEATING                   40
#define  L32_AC                        41
#define  L32_FIREPLACE                 42
#define  L32_POOLSPA                   43
#define  L32_STORIES                   44
#define  L32_UNITS                     45
#define  L32_CURRENTDOCNUM             46

// LDR 2016
#define  L_TAXYEAR                     0
#define  L_ROLLCATEGORY                1
#define  L_ASMT                        2
#define  L_FEEPARCEL                   3
#define  L_STATUS                      4
#define  L_TRA                         5
#define  L_MAPCATEGORY                 6
#define  L_OWNER                       7
#define  L_ASSESSEE                    8
#define  L_MAILADDRESS1                9
#define  L_MAILADDRESS2                10
#define  L_MAILADDRESS3                11
#define  L_MAILADDRESS4                12
#define  L_ZIPMATCH                    13
#define  L_BARCODEZIP                  14
#define  L_SITUS1                      15
#define  L_SITUS2                      16
#define  L_PARCELDESCRIPTION           17
#define  L_CURRENTMARKETLANDVALUE      18
#define  L_CURRENTFIXEDIMPRVALUE       19
#define  L_CURRENTGROWINGIMPRVALUE     20
#define  L_CURRENTSTRUCTURALIMPRVALUE  21
#define  L_CURRENTPERSONALPROPVALUE    22
#define  L_CURRENTPERSONALPROPMHVALUE  23
#define  L_CURRENTNETVALUE             24
#define  L_BILLEDMARKETLANDVALUE       25
#define  L_BILLEDFIXEDIMPRVALUE        26
#define  L_BILLEDGROWINGIMPRVALUE      27
#define  L_BILLEDSTRUCTURALIMPRVALUE   28
#define  L_BILLEDPERSONALPROPVALUE     29
#define  L_BILLEDPERSONALPROPMHVALUE   30
#define  L_BILLEDNETVALUE              31
#define  L_EXEMPTIONCODE1              32
#define  L_EXEMPTIONAMT1               33
#define  L_EXEMPTIONCODE2              34
#define  L_EXEMPTIONAMT2               35
#define  L_EXEMPTIONCODE3              36
#define  L_EXEMPTIONAMT3               37
#define  L_USECODE                     38
#define  L_ACRES                       39
#define  L_BUILDINGTYPE                40
#define  L_QUALITYCLASS                41
#define  L_BUILDINGSIZE                42
#define  L_YEARBUILT                   43
#define  L_TOTALAREA                   44
#define  L_BEDROOMS                    45
#define  L_BATHS                       46
#define  L_HALFBATHS                   47
#define  L_TOTALROOMS                  48
#define  L_GARAGE                      49
#define  L_GARAGESIZE                  50
#define  L_HEATING                     51
#define  L_AC                          52
#define  L_FIREPLACE                   53
#define  L_POOLSPA                     54
#define  L_STORIES                     55
#define  L_UNITS                       56
#define  L_DTS                         56

// LDR 2014
//#define  L_ASMT                        0
//#define  L_ASMTROLLYEAR                1
//#define  L_FEEPARCEL                   2
//#define  L_STATUS                      3
//#define  L_TRA                         4
//#define  L_TAXABILITY                  5
//#define  L_ACRES                       6
//#define  L_USECODE                     7
//#define  L_CURRENTMARKETLANDVALUE      8
//#define  L_CURRENTFIXEDIMPRVALUE       9
//#define  L_CURRENTGROWINGIMPRVALUE     10
//#define  L_CURRENTSTRUCTURALIMPRVALUE  11
//#define  L_CURRENTPERSONALPROPVALUE    12
//#define  L_CURRENTPERSONALPROPMHVALUE  13
//#define  L_CURRENTNETVALUE             14
//#define  L_OWNER                       15
//#define  L_ASSESSEE                    16
//#define  L_MAILADDRESS1                17
//#define  L_MAILADDRESS2                18
//#define  L_MAILADDRESS3                19
//#define  L_MAILADDRESS4                20
//#define  L_EXEMPTIONCODE1              21
//#define  L_EXEMPTIONAMT1               22
//#define  L_EXEMPTIONCODE2              23
//#define  L_EXEMPTIONAMT2               24
//#define  L_EXEMPTIONCODE3              25
//#define  L_EXEMPTIONAMT3               26
//#define  L_SITUS1                      27
//#define  L_SITUS2                      28
//#define  L_PARCELDESCRIPTION           29
//#define  L_DTS                         30 

/* LDR - 2013
#define  L_ASMT                        0
#define  L_TAXYEAR                     1
#define  L_ROLLCHGNUM                  2
#define  L_MAPCATEGORY                 3
#define  L_ROLLCATEGORY                4
#define  L_ROLLTYPE                    5
#define  L_DESTINATIONROLL             6
#define  L_INSTALLMENTS                7
#define  L_BILLTYPE                    8
#define  L_FEEPARCEL                   9
#define  L_ORIGINATINGASMT             10
#define  L_XREFASMT                    11
#define  L_STATUS                      12
#define  L_TRA                         13
#define  L_TAXABILITY                  14
#define  L_ACRES                       15
#define  L_SIZEACRESFTTYPE             16
#define  L_INTDATEFROM                 17
#define  L_INTDATEFROM2                18
#define  L_INTDATETHRU                 19
#define  L_INTERESTTYPE                20
#define  L_USECODE                     21
#define  L_CURRENTMARKETLANDVALUE      22
#define  L_CURRENTFIXEDIMPRVALUE       23
#define  L_CURRENTGROWINGIMPRVALUE     24
#define  L_CURRENTSTRUCTURALIMPRVALUE  25
#define  L_CURRENTPERSONALPROPVALUE    26
#define  L_CURRENTPERSONALPROPMHVALUE  27
#define  L_CURRENTNETVALUE             28
#define  L_BILLEDMARKETLANDVALUE       29
#define  L_BILLEDFIXEDIMPRVALUE        30
#define  L_BILLEDGROWINGIMPRVALUE      31
#define  L_BILLEDSTRUCTURALIMPRVALUE   32
#define  L_BILLEDPERSONALPROPVALUE     33
#define  L_BILLEDPERSONALPROPMHVALUE   34
#define  L_BILLEDNETVALUE              35
#define  L_OWNER                       36
#define  L_ASSESSEE                    37 
#define  L_MAILADDRESS1                38
#define  L_MAILADDRESS2                39
#define  L_MAILADDRESS3                40
#define  L_MAILADDRESS4                41
#define  L_ZIPMATCH                    42
#define  L_BARCODEZIP                  43
#define  L_EXEMPTIONCODE1              44
#define  L_EXEMPTIONAMT1               45
#define  L_EXEMPTIONCODE2              46
#define  L_EXEMPTIONAMT2               47
#define  L_EXEMPTIONCODE3              48
#define  L_EXEMPTIONAMT3               49
#define  L_BILLDATE                    50
#define  L_DUEDATE1                    51
#define  L_DUEDATE2                    52
#define  L_TAXAMT1                     53
#define  L_TAXAMT2                     54
#define  L_PENAMT1                     55
#define  L_PENAMT2                     56
#define  L_COST1                       57
#define  L_COST2                       58
#define  L_PENCHRGDATE1                59
#define  L_PENCHRGDATE2                60
#define  L_PAIDAMT1                    61
#define  L_PAIDAMT2                    62
#define  L_PAYMENTDATE1                63
#define  L_PAYMENTDATE2                64
#define  L_TRANSDATE1                  65
#define  L_TRANSDATE2                  66
#define  L_COLLECTIONNUM1              67
#define  L_COLLECTIONNUM2              68
#define  L_UNSDELINQPENAMTPAID1        69
#define  L_SECDELINQPENAMTPAID2        70
#define  L_TOTALFEESPAID1              71
#define  L_TOTALFEESPAID2              72
#define  L_TOTALFEES                   73
#define  L_PMTCNLDATE1                 74
#define  L_PMTCNLDATE2                 75
#define  L_TRANSFERDATE                76
#define  L_PRORATIONFACTOR             77
#define  L_PRORATIONPCT                78
#define  L_DAYSTOFISCALYEAREND         79
#define  L_DAYSOWNED                   80
#define  L_OWNFROM                     81
#define  L_OWNTHRU                     82
#define  L_SITUS1                      83
#define  L_SITUS2                      84
#define  L_ASMTROLLYEAR                85
#define  L_PARCELDESCRIPTION           86
#define  L_BILLCOMMENTSLINE1           87
#define  L_BILLCOMMENTSLINE2           88
#define  L_DEFAULTNUM                  89
#define  L_DEFAULTDATE                 90
#define  L_REDEEMEDDATE                91
#define  L_SECDELINQFISCALYEAR         92
#define  L_PRIORTAXPAID1               93
#define  L_PENINTERESTCODE             94
#define  L_FOURPAYPLANNUM              95
#define  L_EXISTSLIEN                  96
#define  L_EXISTSCORTAC                97
#define  L_EXISTSCOUNTYCORTAC          98
#define  L_EXISTSBANKRUPTCY            99
#define  L_EXISTSREFUND                100
#define  L_EXISTSDELINQUENTVESSEL      101
#define  L_EXISTSNOTES                 102
#define  L_EXISTSCRITICALNOTE          103
#define  L_EXISTSROLLCHG               104
#define  L_ISPENCHRGCANCELED1          105
#define  L_ISPENCHRGCANCELED2          106
#define  L_ISPERSONALPROPERTYPENALTY   107
#define  L_ISAGPRESERVE                108
#define  L_ISCARRYOVER1STPAID          109
#define  L_ISCARRYOVERRECORD           110
#define  L_ISFAILURETOFILE             111
#define  L_ISOWNERSHIPPENALTY          112
#define  L_ISELIGIBLEFOR4PAY           113
#define  L_ISNOTICE4SENT               114
#define  L_ISPARTPAY                   115
#define  L_ISFORMATTEDADDRESS          116
#define  L_ISADDRESSCONFIDENTIAL       117
#define  L_SUPLCNT                     118
#define  L_ORIGINALDUEDATE1            119
#define  L_ORIGINALDUEDATE2            120
#define  L_ISPEN1REFUND                121
#define  L_ISPEN2REFUND                122
#define  L_ISDELINQPENREFUND           123
#define  L_ISREFUNDAUTHORIZED          124
#define  L_ISNODISCHARGE               125
#define  L_ISAPPEALPENDING             126
#define  L_OTHERFEESPAID               127
#define  L_FOURPAYREASON               128
#define  L_DATEDISCHARGED              129
#define  L_ISONLYFIRSTPAID             130
#define  L_ISALLPAID                   131
#define  L_DTS                         132
#define  L_USERID                      133
*/

// _ROLL
#define  MB_ROLL_ASMT                 0
#define  MB_ROLL_FEEPARCEL            1
#define  MB_ROLL_UNITS                2
#define  MB_ROLL_TRA                  3
#define  MB_ROLL_LEGAL                4
#define  MB_ROLL_ZONING               5
#define  MB_ROLL_USECODE              6
#define  MB_ROLL_NBHCODE              7
#define  MB_ROLL_ACRES                8
#define  MB_ROLL_DOCNUM               9
#define  MB_ROLL_DOCDATE              10
#define  MB_ROLL_TAXABILITY           11
#define  MB_ROLL_OWNER                12
#define  MB_ROLL_CAREOF               13
#define  MB_ROLL_DBA                  14
#define  MB_ROLL_M_ADDR               15
#define  MB_ROLL_M_CITY               16
#define  MB_ROLL_M_ST                 17
#define  MB_ROLL_M_ZIP                18
#define  MB_ROLL_LAND                 19
#define  MB_ROLL_HOMESITE             20
#define  MB_ROLL_IMPR                 21
#define  MB_ROLL_GROWING              22
#define  MB_ROLL_FIXTRS               23
#define  MB_ROLL_PERSPROP             24
#define  MB_ROLL_BUSPROP              25
#define  MB_ROLL_PPMOBILHOME          26
#define  MB_ROLL_M_ADDR1              27
#define  MB_ROLL_M_ADDR2              28
#define  MB_ROLL_M_ADDR3              29
#define  MB_ROLL_M_ADDR4              30

/*
#define  MB_ROLL_FEEPARCEL            0
#define  MB_ROLL_ASMT                 1
#define  MB_ROLL_ASMT_CAT             2
#define  MB_ROLL_OLD_DOCNUM           3
#define  MB_ROLL_OLD_DOCDATE          4
#define  MB_ROLL_STATUS               5
#define  MB_ROLL_STATUS_DATE          6
#define  MB_ROLL_KILL_DOCNUM          7
#define  MB_ROLL_KILL_DOCDATE         8
#define  MB_ROLL_INACTIVE_YEAR        9
#define  MB_ROLL_TRA                  10
#define  MB_ROLL_LEGAL                11
#define  MB_ROLL_TAXABLE1             12     // Same as L_TAXABILITY, but break into 3 fields
#define  MB_ROLL_TAXABLE2             13
#define  MB_ROLL_TAXABLE3             14
#define  MB_ROLL_BASEDATE             15
#define  MB_ROLL_ZONING               16
#define  MB_ROLL_ZONING2              17
#define  MB_ROLL_USECODE              18
#define  MB_ROLL_USECODE2             19
#define  MB_ROLL_UNITS                20
#define  MB_ROLL_NOTES                21
#define  MB_ROLL_NBHCODE              22
#define  MB_ROLL_ACRES                23
#define  MB_ROLL_SIZETYPE             24
#define  MB_ROLL_PP_CODE              25
#define  MB_ROLL_DOCNUM               26
#define  MB_ROLL_DOCDATE              27
#define  MB_ROLL_SUPL_CNT             28
#define  MB_ROLL_NA1                  29
#define  MB_ROLL_NA2                  30
#define  MB_ROLL_NA3                  31
#define  MB_ROLL_NA4                  32
#define  MB_ROLL_NA5                  33
#define  MB_ROLL_NA6                  34
#define  MB_ROLL_NA7                  35
#define  MB_ROLL_NA8                  36
#define  MB_ROLL_NA9                  37
#define  MB_ROLL_NA10                 38
#define  MB_ROLL_NA11                 39
#define  MB_ROLL_NA12                 40
#define  MB_ROLL_NA13                 41
#define  MB_ROLL_TAXABILITY           42
#define  MB_ROLL_TAXABLE4             43
*/
// _NAME
#define  MB_NAME_ASMT                 0
#define  MB_NAME_ADR_TYPE             1
#define  MB_NAME_OWNER                2
#define  MB_NAME_CAREOF               3
#define  MB_NAME_DBA                  4
#define  MB_NAME_M_ADDR               5
#define  MB_NAME_M_CITY               6
#define  MB_NAME_M_ST                 7
#define  MB_NAME_M_ZIP                8
#define  MB_NAME_M_ADDR1              9
#define  MB_NAME_M_ADDR2              10
#define  MB_NAME_M_ADDR3              11
#define  MB_NAME_M_ADDR4              12
#define  MB_NAME_ISFMT_ADDR           13
//#define  MB_NAME_ISCNFL_ADDR          14
//#define  MB_NAME_UPDATE_MODE          15
//#define  MB_NAME_BARCODE_ZIP          16
//#define  MB_NAME_DTS                  17
//#define  MB_NAME_USERID               18

// _VALUE
/*
#define  MB_VALU_ASMT                 0
#define  MB_VALU_SETTYPE              1
#define  MB_VALU_LAND                 2
#define  MB_VALU_LAND_DATE            3
#define  MB_VALU_HOMESITE             4
#define  MB_VALU_HOMESITE_DATE        5
#define  MB_VALU_IMPR                 6
#define  MB_VALU_IMPR_DATE            7
#define  MB_VALU_GROWING              8
#define  MB_VALU_GROWING_DATE         9
#define  MB_VALU_FIXTR                10

#define  MB_VALU_FIXTR_RP             11  // old script
#define  MB_VALU_PP_BUS               12

#define  MB_VALU_PERSPROP             11  // new script
#define  MB_VALU_BUSPROP              12

#define  MB_VALU_PP_MH                13
#define  MB_VALU_PP_PEN_INC           14
#define  MB_VALU_CURTYPE              15
#define  MB_VALU_PP_MH_DATE           16
#define  MB_VALU_DTS                  17
#define  MB_VALU_USERID               18
*/

// _EXE
#define  MB_EXE_STATUS                0
#define  MB_EXE_ASMT                  1
#define  MB_EXE_CODE                  2
#define  MB_EXE_HOEXE                 3
#define  MB_EXE_EXEAMT                4
#define  MB_EXE_EXEPCT                5

// _ASMT
#define  MB_ASMT_ACRES                0
#define  MB_ASMT_ASMT                 1
#define  MB_ASMT_LEGAL                2
#define  MB_ASMT_ASMTSTATUS           3
#define  MB_ASMT_CURRENTDOCDATE       4
#define  MB_ASMT_CURRENTDOCNUM        5
#define  MB_ASMT_DWELLINGUNITS        6
#define  MB_ASMT_FEEPARCEL            7
#define  MB_ASMT_LANDUSE1             8
#define  MB_ASMT_LANDUSE2             9
#define  MB_ASMT_NBHCODE              10
#define  MB_ASMT_SIZETYPE             11
#define  MB_ASMT_TAXABILITY           12
#define  MB_ASMT_TRA                  13
#define  MB_ASMT_ZONING1              14
#define  MB_ASMT_ZONING2              15

// _CHAR
/*
#define  MB_CHAR_FEE_PRCL             0
#define  MB_CHAR_POOLS                1
#define  MB_CHAR_USECAT               2
#define  MB_CHAR_QUALITY              3
#define  MB_CHAR_YRBLT                4
#define  MB_CHAR_BLDGSQFT             5
#define  MB_CHAR_GARSQFT              6
#define  MB_CHAR_HEATING              7
#define  MB_CHAR_COOLING              8
#define  MB_CHAR_HEATING_SRC          9
#define  MB_CHAR_COOLING_SRC          10
#define  MB_CHAR_BEDS                 11
#define  MB_CHAR_FBATHS               12
#define  MB_CHAR_HBATHS               13
#define  MB_CHAR_ROOMS                14
#define  MB_CHAR_FP                   15
#define  MB_CHAR_ASMT                 16
#define  MB_CHAR_HASSEPTIC            17
#define  MB_CHAR_HASSEWER             18
#define  MB_CHAR_HASWELL              19
*/
#define  MB_CHAR_FEE_PRCL             0
#define  MB_CHAR_POOLS                1
#define  MB_CHAR_USECAT               2
#define  MB_CHAR_QUALITY              3
#define  MB_CHAR_YRBLT                4
#define  MB_CHAR_BLDGSQFT             5
#define  MB_CHAR_GARSQFT              6
#define  MB_CHAR_HEATING              7
#define  MB_CHAR_COOLING              8
#define  MB_CHAR_BEDS                 9
#define  MB_CHAR_FBATHS               10
#define  MB_CHAR_HBATHS               11
#define  MB_CHAR_FP                   12
#define  MB_CHAR_ASMT                 13
#define  MB_CHAR_HASSEPTIC            14		// True/False
#define  MB_CHAR_HASSEWER             15
#define  MB_CHAR_HASWELL              16

#define  MBSIZ_CHAR_ASMT              12
#define  MBSIZ_CHAR_POOLS             2
#define  MBSIZ_CHAR_USECAT            2
#define  MBSIZ_CHAR_LANDUSE           8
#define  MBSIZ_CHAR_QUALITY           6
#define  MBSIZ_CHAR_YRBLT             4
#define  MBSIZ_CHAR_BLDGSQFT          9
#define  MBSIZ_CHAR_GARSQFT           9
#define  MBSIZ_CHAR_HEATING           2
#define  MBSIZ_CHAR_COOLING           2
#define  MBSIZ_CHAR_HEATSRC           2
#define  MBSIZ_CHAR_COOLSRC           2
#define  MBSIZ_CHAR_BEDS              3
#define  MBSIZ_CHAR_FBATHS            3
#define  MBSIZ_CHAR_HBATHS            3
#define  MBSIZ_CHAR_ROOMS             4
#define  MBSIZ_CHAR_FP                2
#define  MBSIZ_CHAR_HASSEPTIC         1
#define  MBSIZ_CHAR_HASSEWER          1
#define  MBSIZ_CHAR_HASWELL           1

// _SITUS
#define  MB_SITUS_ASMT                0
#define  MB_SITUS_STRNAME             1		// Might incluse StrType
#define  MB_SITUS_STRNUM              2
#define  MB_SITUS_STRTYPE             3
#define  MB_SITUS_STRDIR              4
#define  MB_SITUS_UNIT                5
#define  MB_SITUS_COMMUNITY           6
#define  MB_SITUS_ZIP                 7
//#define  MB_SITUS_SEQ                 8

// _SALES
#define  MB_SALES_ASMT                0
#define  MB_SALES_DOCNUM              1
#define  MB_SALES_DOCDATE             2
#define  MB_SALES_DOCCODE             3
#define  MB_SALES_SELLER              4
#define  MB_SALES_BUYER               5
//#define  MB_SALES_PRICE               6
#define  MB_SALES_TAXAMT              6
#define  MB_SALES_GROUPSALE           7
#define  MB_SALES_GROUPASMT           8
#define  MB_SALES_XFERTYPE            9
//#define  MB_SALES_ADJREASON           11
//#define  MB_SALES_CONFCODE            12

// _TAX
#define  MB_TAX_ASMT                  0
#define  MB_TAX_FEEPARCEL             1
#define  MB_TAX_TAXAMT1               2
#define  MB_TAX_TAXAMT2               3
#define  MB_TAX_PENAMT1               4
#define  MB_TAX_PENAMT2               5
#define  MB_TAX_PENDATE1              6
#define  MB_TAX_PENDATE2              7
#define  MB_TAX_PAIDAMT1              8
#define  MB_TAX_PAIDAMT2              9
#define  MB_TAX_TOTALPAID1            10
#define  MB_TAX_TOTALPAID2            11
#define  MB_TAX_TOTALFEES             12
#define  MB_TAX_PAIDDATE1             13
#define  MB_TAX_PAIDDATE2             14
#define  MB_TAX_YEAR                  15
#define  MB_TAX_ROLLCAT               16

typedef struct _tCharRec
{
   char  Asmt[MBSIZ_CHAR_ASMT];              //  1
   char  NumPools[MBSIZ_CHAR_POOLS];         // 13
   char  LandUseCat[MBSIZ_CHAR_USECAT];      // 15
   char  QualityClass[MBSIZ_CHAR_QUALITY];   // 17
   char  YearBuilt[MBSIZ_CHAR_YRBLT];        // 23
   char  BuildingSize[MBSIZ_CHAR_BLDGSQFT];  // 27
   char  SqFTGarage[MBSIZ_CHAR_GARSQFT];     // 36
   char  Heating[MBSIZ_CHAR_HEATING];        // 45
   char  Cooling[MBSIZ_CHAR_COOLING];        // 47
   char  HeatingSource[MBSIZ_CHAR_HEATSRC];  // 49
   char  CoolingSource[MBSIZ_CHAR_COOLSRC];  // 51
   char  NumBedrooms[MBSIZ_CHAR_BEDS];       // 53
   char  NumFullBaths[MBSIZ_CHAR_FBATHS];    // 56
   char  NumHalfBaths[MBSIZ_CHAR_HBATHS];    // 59
   char  NumFireplaces[MBSIZ_CHAR_FP];       // 62
   char  FeeParcel[MBSIZ_CHAR_ASMT];         // 64
   char  HasSeptic;                          // 76
   char  HasSewer;                           // 77
   char  HasWell;                            // 78
   char  TotalRooms[MBSIZ_CHAR_ROOMS];       // 79
   char  LandUse1[MBSIZ_CHAR_LANDUSE];       // 83
   char  filler[164];                        // 91
   char  CRLF[2];                            // 255
} MB_CHAR;

static XLAT_CODE  asHeating[] =
{
// Value, lookup code, value length
   "01", "B", 2,               // Forced Air Unit
   "02", "D", 2,               // Wall Furnace
   "03", "F", 2,               // Baseboard
   "04", "S", 2,               // Wood Stove/ Fireplace
   "05", "K", 2,               // Solar
   "06", "I", 2,               // Radiant
   "07", "G", 2,               // Heat pump
   "",   "",  0
};

static XLAT_CODE  asCooling[] =
{
   // Value, lookup code, value length
   "01", "C", 2,               // Central Air
   "02", "E", 2,               // Evaporative
   "03", "F", 2,               // Fans
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "01", "G", 2,               // Gunite
   "02", "F", 2,               // Fiberglass
   "03", "V", 2,               // Vinyl
   "04", "X", 2,               // PP Abv Ground
   "05", "S", 2,               // Spa
   "06", "Y", 2,               // Hot Tub
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "01", "L", 2,               // MASONRY
   "02", "Z", 2,               // ZERO CLEARANCE
   "03", "O", 2,               // HEATILATOR
   "04", "W", 2,               // WOOD STOVE
   "05", "S", 2,               // PELLET STOVE
   "06", "G", 2,               // GAS FP/STOVE
   "07", "O", 2,               // ?
   "08", "O", 2,               // ?
   "10", "O", 2,               // ?
   "11", "O", 2,               // ?
   "",   "",  0
};

static XLAT_CODE  asParkType[] =
{
   // Value, lookup code, value length
   "1", "I", 1,               // ATTACHED
   "2", "L", 1,               // DETACHED
   "3", "B", 1,               // BUILT-IN
   "4", "E", 1,               // BASEMENT
   "5", "C", 1,               // CARPORT
   "",   "",  0
};

IDX_TBL3 Cal_DocType[] =
{// DOCCODE, DOCTYPE, NONSALE  
   0,  "   ", 'Y',
   1,  "1  ", 'N',
   2,  "57 ", 'N',
   3,  "   ", 'Y',
   4,  "   ", 'Y',
   5,  "18 ", 'Y',
   6,  "51 ", 'Y',
   7,  "   ", 'Y',
   8,  "   ", 'Y',
   9,  "6  ", ' ',
   10, "67 ", 'N',
   11, "3  ", 'Y',
   12, "   ", ' ',
   13, "9  ", ' ',
   14, "   ", ' ',
   15, "41 ", ' ',
   16, "   ", ' ',
   17, "   ", 'Y',
   18, "40 ", 'Y',
   19, "77 ", 'N',
   20, "   ", 'Y',
   21, "   ", ' ',
   22, "55 ", ' ',
   23, "1  ", 'N',
   24, "75 ", ' ',
   25, "40 ", 'Y',
   26, "   ", 'Y',
   27, "   ", 'Y',
   28, "   ", 'Y',
   29, "   ", ' ',
   30, "   ", 'Y',
   31, "   ", ' ',
   32, "   ", ' ',
   33, "   ", 'Y',
   34, "   ", 'Y',
   35, "   ", 'Y',
   36, "   ", ' ',
   37, "   ", ' ',
   38, "   ", ' ',
   39, "   ", ' ',
   40, "   ", 'Y',
   41, "   ", ' ',
   42, "   ", ' ',
   43, "   ", ' ',
   44, "   ", 'Y',
   45, "   ", ' ',
   46, "   ", ' ',
   47, "   ", ' ',
   48, "   ", ' ',
   49, "   ", ' ',
   50, "1  ", 'N',
   51, "75 ", 'Y',
   52, "   ", ' ',
   53, "   ", ' ',
   54, "   ", ' ',
   55, "   ", ' ',
   56, "   ", ' ',
   57, "   ", ' ',
   58, "   ", ' ',
   59, "   ", ' ',
   60, "1  ", 'N',
   61, "   ", 'Y',
   62, "   ", 'Y',
   63, "   ", 'Y',
   64, "   ", 'Y',
   65, "   ", ' ',
   66, "   ", 'Y',
   67, "   ", ' ',
   68, "   ", ' ',
   69, "   ", ' ',
   70, "   ", ' ',
   71, "   ", ' ',
   72, "   ", ' ',
   73, "   ", ' ',
   74, "   ", ' ',
   75, "   ", ' ',
   76, "   ", ' ',
   77, "   ", ' ',
   78, "   ", ' ',
   79, "   ", ' ',
   80, "   ", ' ',
   81, "   ", ' ',
   82, "   ", ' ',
   83, "   ", ' ',
   84, "   ", ' ',
   85, "   ", ' ',
   86, "   ", ' ',
   87, "   ", ' ',
   88, "   ", ' ',
   89, "   ", ' ',
   90, "   ", ' ',
   91, "   ", 'Y',
   92, "   ", ' ',
   93, "   ", ' ',
   94, "   ", ' ',
   95, "   ", ' ',
   96, "   ", ' ',
   97, "   ", ' ',
   98, "   ", 'Y',
   99, "   ", 'Y'
};

IDX_TBL5 CAL_DocCode[] =
{// DOCCODE, DOCTYPE, NONSALE
   "01", "1 ", 'N', 2, 2,          // TRANSFER
   "02", "57", 'N', 2, 2,          // PARTIAL INTEREST TRANSFER
   "03", "74", 'Y', 2, 2,          // NON-REAPPRAISABLE EVENT
   "04", "74", 'Y', 2, 2,          // STAMP
   "05", "18", 'Y', 2, 2,          // INTERMARITAL
   "06", "51", 'Y', 2, 2,          // VESTING/NAME CHANGE
   "07", "74", 'Y', 2, 2,          // INTO TRUST
   "08", "74", 'Y', 2, 2,          // OUT OF TRUST
   "09", "6 ", 'Y', 2, 2,          // AFFIDAVIT OF DEATH
   "10", "67", 'N', 2, 2,          // TAX SALE
   "11", "3 ", 'Y', 2, 2,          // ADD/DELETE JT - NR
   "12", "74", 'Y', 2, 2,          // PROBLEM DEED
   "13", "9 ", 'Y', 2, 2,          // CORRECTION DEED - NR
   "14", "80", 'Y', 2, 2,          // JUDGEMENT/ORDER - NR
   "15", "41", 'Y', 2, 2,          // FD/ESTATE - NR
   "16", "74", 'Y', 2, 2,          // COSD
   "17", "74", 'Y', 2, 2,          // PROP 58
   "18", "40", 'Y', 2, 2,          // EASEMENT
   "19", "77", 'Y', 2, 2,          // FORECLOSURE
   "20", "74", 'Y', 2, 2,          // REVIEW - CHANGES TO CURRENT ROLL ONLY
   "21", "74", 'Y', 2, 2,          // ON SITES
   "22", "55", 'Y', 2, 2,          // PATENT
   "23", "1 ", 'N', 2, 2,          // MULTI-PARCEL TRANS
   "24", "75", 'N', 2, 2,          // TIMESHARE TRANSFER
   "25", "40", 'Y', 2, 2,          // CONSERVATION/WILDLIFE EASEMENT
   "26", "74", 'Y', 2, 2,          // SECRETARY OF VETERANS AFFAIRS
   "27", "74", 'Y', 2, 2,          // DEPARTMENT OF VETERANS AFFAIRS
   "28", "74", 'Y', 2, 2,          // SECRETARY OF HOUSING AND URBAN DEVELOPMENT
   "30", "74", 'Y', 2, 2,          // NOTICE OF NONACCEPTANCE OF A RECORDED DEED
   "31", "74", 'Y', 2, 2,          // PROBLEM DEED - W/SUPP
   "32", "74", 'Y', 2, 2,          // PROBLEM DEED PARTIAL INT - W/SUPP
   "33", "74", 'Y', 2, 2,          // FOR FINANCING
   "34", "74", 'Y', 2, 2,          // AFTER FINANCING
   "35", "74", 'Y', 2, 2,          // Registered Domestic Partners
   "40", "74", 'Y', 2, 2,          // CLERICAL
   "43", "74", 'Y', 2, 2,          // COMBINED SPLIT INTEREST VALUES
   "50", "1 ", 'N', 2, 2,          // REAPPRAISABLE CHANGE IN OWNERSHIP/ NO SUPP
   "51", "75", 'Y', 2, 2,          // TRANSFER W/IN PARK
   "54", "74", 'Y', 2, 2,          // VOLUNTARY CONVERSION
   "58", "74", 'Y', 2, 2,          // NEW MOBILE/ REMOVE MOBILE
   "60", "19", 'Y', 2, 2,          // SPLIT/COMBO
   "61", "74", 'Y', 2, 2,          // ROS
   "62", "74", 'Y', 2, 2,          // ROS/BLA - GENERATE SUPPLEMENTAL
   "63", "74", 'Y', 2, 2,          // ROS/BLA SUPPLEMENTAL GENERATED
   "64", "74", 'Y', 2, 2,          // SPLIT/COMBO - STAMP
   "65", "74", 'Y', 2, 2,          // COMBINED SPLIT INTERESTS
   "66", "74", 'Y', 2, 2,          // TRA CHANGE - SPLIT/COMBO
   "67", "74", 'Y', 2, 2,          // USA ADJUSTMENT
   "76", "74", 'Y', 2, 2,          // MISC ADDITION
   "80", "74", 'Y', 2, 2,          // NEW CONSTRUCTION COMPLETE
   "81", "74", 'Y', 2, 2,          // NEW DWELLING
   "90", "74", 'Y', 2, 2,          // PROP 8
   "91", "74", 'Y', 2, 2,          // PROP 8 ANNUAL REVIEW
   "98", "74", 'Y', 2, 2,          // SUPPLEMENTAL REVIEW
   "AB", "74", 'Y', 2, 2,          //
   "AD", "74", 'Y', 2, 2,          //
   "AS", "74", 'Y', 2, 2,          //
   "BA", "74", 'Y', 2, 2,          //
   "BD", "74", 'Y', 2, 2,          //
   "CC", "74", 'Y', 2, 2,          //
   "D ", "13", 'Y', 2, 2,          // DEED
   "E ", "40", 'Y', 2, 2,          // EASEMENT
   "ES", "74", 'Y', 2, 2,          //
   "FD", "41", 'Y', 2, 2,          // FIDUCIARY DEED
   "FS", "74", 'Y', 2, 2,          //
   "GD", "1 ", 'N', 2, 2,          // GRANT DEED
   "J ", "74", 'Y', 2, 2,          //
   "JD", "74", 'Y', 2, 2,          //
   "LE", "74", 'Y', 2, 2,          //
   "LP", "74", 'Y', 2, 2,          //
   "MH", "74", 'Y', 2, 2,          //
   "MI", "74", 'Y', 2, 2,          //
   "MR", "74", 'Y', 2, 2,          //
   "NP", "74", 'Y', 2, 2,          //
   "P ", "74", 'Y', 2, 2,          //
   "QC", "4 ", 'Y', 2, 2,          // QUIT CLAIM
   "QT", "74", 'Y', 2, 2,          //
   "RE", "74", 'Y', 2, 2,          //
   "RN", "74", 'Y', 2, 2,          //
   "RW", "74", 'Y', 2, 2,          //
   "SS", "74", 'Y', 2, 2,          //
   "T ", "74", 'Y', 2, 2,          //
   "TC", "74", 'Y', 2, 2,          //
   "TD", "27", 'Y', 2, 2,          // TRUSTEE'S DEED
   "WR", "28", 'Y', 2, 2,          // WARRANTY DEED
   "", "", '\0', 0, 0
};

IDX_TBL4 CAL_Exemption[] = 
{
   "E01", "H", 3,1,
   "E10", "V", 3,1,
   "E11", "D", 3,1,
   "E12", "D", 3,1, 
   "E13", "D", 3,1,
   "E14", "D", 3,1,
   "E21", "W", 3,1,     // WELFARE
   "E30", "X", 3,1,
   "E31", "R", 3,1,
   "E32", "C", 3,1,
   "E50", "P", 3,1,
   "E51", "P", 3,1,
   "E52", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E53", "M", 3,1,
   "E61", "E", 3,1,
   "E70", "W", 3,1,
   "E71", "W", 3,1,
   "E72", "W", 3,1,
   "E80", "C", 3,1,
   "E81", "C", 3,1,
   "E82", "C", 3,1,
   "E90", "R", 3,1,
   "E91", "R", 3,1,
   "E92", "R", 3,1,
   "E93", "R", 3,1,
   "E96", "X", 3,1,
   "E98", "X", 3,1,
   "","",0,0
};

int   MB_ConvChar(char *pInfile);
bool  sqlConnect(LPCSTR strDb, hlAdo *phDb);
int   execSqlCmd(LPCSTR strCmd);
int   execSqlCmd(LPCSTR strCmd, hlAdo *phDb);
int   MB_CreateLienRec(char *pOutbuf, char *pRollRec);
int   MB_ExtrLien(LPCSTR pCnty, LPCSTR pLDRFile);
int   MB_ExtrTR601(LPCSTR pCnty, LPCSTR pLDRFile=NULL, int iChkLastChar=0);
int   MB_MergeSale(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag, bool bUpdtXfer=true);

#endif // !defined(AFX_LOADMB_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_)
