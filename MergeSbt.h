#ifndef _MERGESBT_H_
#define _MERGESBT_H_

#define SBT_CHAR_FEEPARCEL                0
#define SBT_CHAR_POOLSPA                  1
#define SBT_CHAR_CATTYPE                  2
#define SBT_CHAR_QUALITYCLASS             3
#define SBT_CHAR_YRBLT                    4
#define SBT_CHAR_BUILDINGSIZE             5
#define SBT_CHAR_ATTACHGARAGESF           6
#define SBT_CHAR_DETACHGARAGESF           7
#define SBT_CHAR_CARPORTSF                8
#define SBT_CHAR_HEATING                  9
#define SBT_CHAR_COOLINGCENTRALAC         10
#define SBT_CHAR_COOLINGEVAPORATIVE       11
#define SBT_CHAR_COOLINGROOMWALL          12
#define SBT_CHAR_COOLINGWINDOW            13
#define SBT_CHAR_STORIESCNT               14
#define SBT_CHAR_UNITSCNT                 15
#define SBT_CHAR_TOTALROOMS               16
#define SBT_CHAR_EFFYR                    17
#define SBT_CHAR_PATIOSF                  18
#define SBT_CHAR_BEDROOMS                 19
#define SBT_CHAR_BATHROOMS                20
#define SBT_CHAR_HALFBATHS                21
#define SBT_CHAR_FIREPLACE                22
#define SBT_CHAR_ASMT                     23
#define SBT_CHAR_BLDGSEQNUM               24
#define SBT_CHAR_HASWELL                  25
#define SBT_CHAR_LOTSQFT                  26

static XLAT_CODE  asCooling[] =
{
   // Value, lookup code, value length
   "C",  "C", 1,              // Central A/C
   "F",  "M", 1,              // Floor
   "N",  " ", 1,              // None
   "O",  "X", 1,              // Other
   "W",  "L", 1,              // Wall unit
   "",   "",  0
};

static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "BB", "F", 2,              // Baseboard
   "C",  "Z", 1,              // Central
   "FA", "B", 2,              // Forced-Air Unit
   "FL", "C", 2,              // Floor 
   "N",  "L", 1,              // None
   "O",  "X", 1,              // Other
   "R",  "I", 1,              // Radiator
   "W",  "D", 1,              // Wall
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "AG", "X", 2,               // Above Ground Pool
   "B",  "G", 1,               // In-Ground Gunite Pool
   "F",  "F", 1,               // Built-in Fiberglass
   "PS", "C", 2,               // Built-in pool wih spa
   "V",  "V", 1,               // In Ground Vinyl Pool
   "N",  "N", 1,               // No Pool
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{  // 04/10/2018
   // Value, lookup code, value length
   "0", "N", 1,                // 0 fireplace
   "1", "1", 1,                // 1 fireplaces
   "2", "2", 1,                // 2 fireplaces
   "3", "3", 1,                // 3 fireplaces
   "CF","Y", 1,                // Corner Fireplace
   "D", "2", 1,                // Dual fireplace
   "FS","F", 1,                // Free Standing Fireplace
   "F", "Y", 1,                // Fireplace
   "P", "S", 1,                // Pellet Stove
   "WS","W", 1,                // Wood Burning Stove
   "",   "", 0
};

static STRSFX Sbt_SfxTbl[] = 
{
   "DR",  "DR",  "5  ", 2,
   "RD",  "RD",  "2  ", 2,
   "ST",  "ST",  "1  ", 2,
   "CT",  "CT",  "14 ", 2,
   "AVE", "AVE", "3  ", 3,
   "ST",  "ST",  "1  ", 2,
   "HWY", "HWY", "20 ", 3,
   "LN",  "LN",  "21 ", 2,
   "WAY", "WAY", "38 ", 3,
   "CIR", "CIR", "10 ", 3,
   "","","",0
};

IDX_TBL5 SBT_DocCode[] =
{// DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN
   "01", "1 ", 'N', 2, 2,     // GRANT DEED
   "02", "13", 'N', 2, 2,     // DEED
   "03", "13", 'N', 2, 2,     // DEED
   "10", "13", 'N', 2, 2,     // DEED
   "11", "13", 'N', 2, 2,     // DEED/GIFT DEED
   "12", "13", 'N', 2, 2,     // DEED
   "", "", '\0', 0, 0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SBT_Exemption[] = 
{
   "E01", "H", 3,1,
   "E02", "D", 3,1,     // WIDOW - DISABLED VETERAN
   "E03", "V", 3,1,     // VETERAN BOTH
   "E04", "D", 3,1,     // DISABLED VETERAN LOW INCOME
   "E05", "U", 3,1,     // COLLEGE
   "E06", "D", 3,1,     // WIDOW - DISABLED VET LOW INCOME
   "E07", "D", 3,1,     // DISABLED VETERAN
   "E08", "S", 3,1,     // SCHOOL
   "E09", "R", 3,1,     // RELIGIOUS
   "E10", "C", 3,1,     // CHURCH
   "E11", "W", 3,1,     // WELFARE
   "E12", "E", 3,1,     // CEMETERY
   "E13", "R", 3,1,     // PARTIAL RELIGIOUS & CEMETERY
   "E14", "C", 3,1,     // PARTIAL CHURCH LAND/STRUCTURE
   "E15", "M", 3,1,     // MUSEUM
   "E16", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E17", "W", 3,1,     // WELFARE PP/FIXT
   "E18", "W", 3,1,     // PARTIAL WELFARE LAND/STRUCTURE
   "E19", "R", 3,1,     // RELIGIOUS PP/FIXT
   "E20", "X", 3,1,     // LOW VALUE
   "E21", "X", 3,1,     // LEASED LAND
   "E22", "X", 3,1,     // PARTIAL WELFARE LAND
   "E23", "X", 3,1,     // SOLAR
   "E98", "X", 3,1,     // OTHER
   "P19", "X", 3,1,     // PROP 19 - FOR BYVT 19 REPORTING ONLY
   "","",0,0
};

#endif