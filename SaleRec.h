#ifndef SALE_OFF_APN

#define SALE_FLG_CONFIRM   0x31
#define SALE_FLG_NODOCNUM  0x32

#define SALE_FIX_SPCFLG    0x00000001
#define SALE_FIX_SALECODE  0x00000002
#define SALE_FIX_NOPRCLXFR 0x00000004
#define SALE_FIX_SALEPRICE 0x00000008
#define SALE_FIX_DOCNUM    0x00000010
#define SALE_FIX_DOCDATE   0x00000020
#define SALE_FIX_DOCTYPE   0x00000040
#define SALE_FIX_NONESALE  0x00000080
#define SALE_FIX_REMSCODE  0x00000100

#define SALE_USE_SALEREC1  0x00000001
#define SALE_USE_SCSALREC  0x00000010
#define SALE_USE_SCUPDXFR  0x00000100
#define SALE_USE_SCNOPRICE 0x00001000

#define GRGR_UPD_XFR       0x00010000    // ALA
#define GRGR_UPD_GD        0x00100000    // VEN
#define GRGR_UPD_OWNER     0x01000000
#define GRGR_UPD_SELLER    0x10000000

#define TYPE_SCSAL_REC     0x00000001
#define TYPE_GRGR_DOC      0x00000002
#define TYPE_GRGR_DEF      0x00000004
#define TYPE_GRGR_ORG      0x00000008

// These values can be combined using '|'
#define CLEAR_OLD_SALE     0x00000001
#define CLEAR_UPD_SALE     0x00000002
#define CLEAR_UPD_GRGR     0x00000004
#define CLEAR_OLD_XFER     0x00000008
#define DONT_CHK_DOCNUM    0x00000010
#define DONT_CHK_DOCDATE   0x00000020
#define DONT_CHK_PRICE     0x00000040
#define DONT_CHK_ANY       0x00000080
#define DONT_UPT_PRICE     0x00000100     // Do not update conf price (MEN, RIV)
#define VERIFY_DATE        0x00000200     // Verify DocDate (SFX)
#define UPDATE_OWNER       0x00001000
#define FMT_DOCNUM_X4      0x00002000     // DNX - Use YYYY from sale date to format DocNum as YYYY1234 (i.e. 123 -> YYYY0123)

#define CONV_SALE_REC1     1
#define CONV_SALE_REC2     2
#define CONV_SALE_REC3     3
#define CONV_SALE_REC4     4
#define CONV_CSAL_REC      5
#define CONV_GRGR_DEF      6
#define CONV_GRGR_ALA      7
#define CONV_GRGR_DOC      8
#define CONV_SALE_SLO      9
#define CONV_GRGR_ORG      10

#define SALE_OFF_APN       0
#define SALE_OFF_DOCNUM    14
#define SALE_OFF_DOCDATE   26
#define SALE_OFF_DOCTYPE   34
#define SALE_OFF_SALEPRICE 56
#define SALE_OFF_SALECODE  66

#define SALE_SIZ_APN       14
#define SALE_SIZ_DOCNUM    12
#define SALE_SIZ_DOCDATE   8
#define SALE_SIZ_DOCTYPE   22
#define SALE_SIZ_SALEPRICE 10
#define SALE_SIZ_SALECODE  2
#define SALE_SIZ_SELLER    48
#define SALE_SIZ_STAMPAMT  10
#define SALE_SIZ_NOPRCLXFR 2
#define SALE_SIZ_BUYER     52
#define SALE_SIZ_CAREOF    52
#define SALE_SIZ_M_ADR1    52
#define SALE_SIZ_M_ADR2    40
#define SALE_SIZ_M_ZIP     5
#define SALE_SIZ_NAME      50
#define SALE_SIZ_PCTXFER   3
#define SALE_SIZ_DOCCODE   3
#define SALE_SIZ_NAMECNT   2
#define SALE_SIZ_NBHCODE   9
#define SALE_SIZ_M_STRNUM  7
#define SALE_SIZ_M_STRSUB  3
#define SALE_SIZ_M_DIR     2
#define SALE_SIZ_M_STREET  24
#define SALE_SIZ_M_SUFF    5
#define SALE_SIZ_M_UNITNO  6
#define SALE_SIZ_M_CITY    17
#define SALE_SIZ_M_ST      2
#define SALE_SIZ_M_ZIP4    4
#define SALE_SIZ_DOCTITLE  24
#define SALE_SIZ_LOT       6
#define SALE_SIZ_BLOCK     6
#define SALE_SIZ_TRACT     6
#define SALE_SIZ_S_RAWADDR 80

#define CDA_SIZ_SELLER     24

// CD-Assessor field offset
#define CDA_OFF_DOC1NUM    873-1
#define CDA_OFF_DOC1DATE   855-1
#define CDA_OFF_DOC1TYPE   606-1
#define CDA_OFF_SALE1PRICE 583-1
#define CDA_OFF_SALE1CODE  593-1
#define CDA_OFF_SELLER     683-1

#define CDA_OFF_DOC2NUM    903-1
#define CDA_OFF_DOC2DATE   885-1
#define CDA_OFF_DOC2TYPE   744-1
#define CDA_OFF_SALE2PRICE 721-1
#define CDA_OFF_SALE2CODE  731-1

#define CDA_OFF_DOC3NUM    933-1
#define CDA_OFF_DOC3DATE   915-1
#define CDA_OFF_DOC3TYPE   803-1
#define CDA_OFF_SALE3PRICE 780-1
#define CDA_OFF_SALE3CODE  790-1

#define CDA_OFF_XFERDATE   825-1
#define CDA_OFF_XFERDOC    843-1

typedef struct t_Sale_Rec
{
   // 132-bytes without names
   // 232-byte with names
   char  acApn[SALE_SIZ_APN];
   char  acDocNum[SALE_SIZ_DOCNUM];
   char  acDocDate[SALE_SIZ_DOCDATE];
   char  acDocType[SALE_SIZ_DOCTYPE];
   char  acSalePrice[SALE_SIZ_SALEPRICE];
   char  acSaleCode[SALE_SIZ_SALECODE];
   char  acSeller[SALE_SIZ_SELLER];
   char  acStampAmt[SALE_SIZ_STAMPAMT];
   char  acNumOfPrclXfer[SALE_SIZ_NOPRCLXFR];
   char  NoneSale_Flg;
   char  acCOO_Flag[1];
#ifdef _NAME_INCLUDED
   char  acName1[SALE_SIZ_NAME];
   char  acName2[SALE_SIZ_NAME];
#endif
   char  CRLF[2];
} SALE_REC;
#define SALEREC_SIZE       sizeof(SALE_REC)

typedef struct t_Sale_Rec1
{
   // 232-byte with names
   char  acApn[SALE_SIZ_APN];
   char  acDocNum[SALE_SIZ_DOCNUM];
   char  acDocDate[SALE_SIZ_DOCDATE];
   char  acDocType[SALE_SIZ_DOCTYPE];
   char  acSalePrice[SALE_SIZ_SALEPRICE];
   char  acSaleCode[SALE_SIZ_SALECODE];
   char  acSeller[SALE_SIZ_SELLER];
   char  acStampAmt[SALE_SIZ_STAMPAMT];
   char  acNumOfPrclXfer[SALE_SIZ_NOPRCLXFR];
   char  NoneSale_Flg;
   char  acCOO_Flag[1];
   char  acName1[SALE_SIZ_NAME];
   char  acName2[SALE_SIZ_NAME];
   char  CRLF[2];
} SALE_REC1;

typedef struct t_Xfer_Rec
{
   char  acApn[SALE_SIZ_APN];                // 1
   char  acDoc1Num[SALE_SIZ_DOCNUM];         // 15
   char  acDoc1Date[SALE_SIZ_DOCDATE];       // 27
   char  acDoc1Type[SALE_SIZ_DOCTYPE];       // 35
   char  acSale1Price[SALE_SIZ_SALEPRICE];   // 57
   char  acSale1Code[SALE_SIZ_SALECODE];     // 67
   char  acDoc2Num[SALE_SIZ_DOCNUM];         // 69
   char  acDoc2Date[SALE_SIZ_DOCDATE];       // 81
   char  acDoc2Type[SALE_SIZ_DOCTYPE];       // 89
   char  acSale2Price[SALE_SIZ_SALEPRICE];   // 111
   char  acSale2Code[SALE_SIZ_SALECODE];     // 121
   char  acDoc3Num[SALE_SIZ_DOCNUM];         // 123
   char  acDoc3Date[SALE_SIZ_DOCDATE];       // 135
   char  acDoc3Type[SALE_SIZ_DOCTYPE];       // 143
   char  acSale3Price[SALE_SIZ_SALEPRICE];   // 165
   char  acSale3Code[SALE_SIZ_SALECODE];     // 175
   char  acXferDocNum[SALE_SIZ_DOCNUM];      // 177
   char  acXferDocDate[SALE_SIZ_DOCDATE];    // 189
   char  acSeller[SALE_SIZ_SELLER];          // 197
   char  CRLF[2];
} XFER_REC;

// Old cumsale rec
typedef struct t_CSale_Rec
{
   char  Apn[SALE_SIZ_APN];
   char  DocNum[SALE_SIZ_DOCNUM];
   char  DocDate[SALE_SIZ_DOCDATE];
   char  DocType[SALE_SIZ_DOCTYPE];
   char  SalePrice[SALE_SIZ_SALEPRICE];
   char  SaleCode[SALE_SIZ_SALECODE];
   char  Seller[SALE_SIZ_SELLER];
   char  StampAmt[SALE_SIZ_STAMPAMT];
   char  NumOfPrclXfer[SALE_SIZ_NOPRCLXFR];
   char  NoneSale_Flg;                       // Transfer-Default
   char  COO_Flag;                           // Change of ownership
   char  Name1[SALE_SIZ_BUYER];
   char  Name2[SALE_SIZ_BUYER];
   char  CareOf[SALE_SIZ_CAREOF];
   char  MailAdr1[SALE_SIZ_M_ADR1];
   char  MailAdr2[SALE_SIZ_M_ADR2];
   char  MailZip[SALE_SIZ_M_ZIP];
   char  CRLF[2];
} CSAL_REC;
#define CSALREC_SIZE       sizeof(CSAL_REC)

#define  SALE_FLD_APN            1
#define  SALE_FLD_DOCNUM         15
#define  SALE_FLD_DOCDATE        27
#define  SALE_FLD_DOCTYPE        35
#define  SALE_FLD_SALEPRICE      57
#define  SALE_FLD_SALECODE       67
#define  SALE_FLD_SELLER1        69
#define  SALE_FLD_STAMPAMT       117
#define  SALE_FLD_NOPRCLXFR      127
#define  SALE_FLD_NONSALE        129 // - Transfer-Default
#define  SALE_FLD_SPCFLG         130 // - 1=conf sale
#define  SALE_FLD_NAME1          131
#define  SALE_FLD_NAME2          183
#define  SALE_FLD_CAREOF         235
#define  SALE_FLD_M_ADR1         287
#define  SALE_FLD_M_ADR2         339
#define  SALE_FLD_M_ZIP          379
#define  SALE_FLD_SELLER2        384
#define  SALE_FLD_XFERDATE       432
#define  SALE_FLD_MULTI          440 // - Y/N
#define  SALE_FLD_PRI_APN        441
#define  SALE_FLD_PCTXFER        455
#define  SALE_FLD_DOCCODE        458
#define  SALE_FLD_CONFPRICE      461
#define  SALE_FLD_ADJPRICE       471
#define  SALE_FLD_OTH_APN        481
#define  SALE_FLD_NAMECNT        495
#define  SALE_FLD_FILLER         497
#define  SALE_FLD_ARCODE         507
#define  SALE_FLD_ETAL           508
#define  SALE_FLD_APNMATCH       509
#define  SALE_FLD_OWNMATCH       510
#define  SALE_FLD_CLEANUP        999

// Standard cumsale rec
typedef struct t_SCSale_Rec
{  // 512-bytes
   char  Apn[SALE_SIZ_APN];                  // 1
   char  DocNum[SALE_SIZ_DOCNUM];            // 15
   char  DocDate[SALE_SIZ_DOCDATE];          // 27
   char  DocType[SALE_SIZ_DOCTYPE];          // 35  - Standard DocType code in tblLkUp table [Deed Type Index]
   char  SalePrice[SALE_SIZ_SALEPRICE];      // 57
   char  SaleCode[SALE_SIZ_SALECODE];        // 67  - (F)ull, (P)artial, I(With lien), N(less lien), W(With other properties) 
   char  Seller1[SALE_SIZ_SELLER];           // 69
   char  StampAmt[SALE_SIZ_STAMPAMT];        // 117
   char  NumOfPrclXfer[SALE_SIZ_NOPRCLXFR];  // 127
   char  NoneSale_Flg;                       // 129 - Known non-sale record (Transfer, Quit claim, ...) for internal used only
   char  Spc_Flg;                            // 130 - 1=conf sale, 2=No DocNum, 3=DocNum from GrGr (VEN)
                                             //       3=DocNum from GrGr, 4=SFR, 5=Multi parcel, 6=Mobile home, 7=Land, 8=Dock, 9=Others (SBD)
   char  Name1[SALE_SIZ_BUYER];              // 131
   char  Name2[SALE_SIZ_BUYER];              // 183
   char  CareOf[SALE_SIZ_CAREOF];            // 235
   char  MailAdr1[SALE_SIZ_M_ADR1];          // 287
   char  MailAdr2[SALE_SIZ_M_ADR2];          // 339
   char  MailZip[SALE_SIZ_M_ZIP];            // 379
   char  Seller2[SALE_SIZ_SELLER];           // 384
   char  TransferDate[SALE_SIZ_DOCDATE];     // 432
   char  MultiSale_Flg;                      // 440 - Y/N
   char  PrimaryApn[SALE_SIZ_APN];           // 441
   char  PctXfer[SALE_SIZ_PCTXFER];          // 455
   char  DocCode[SALE_SIZ_DOCCODE];          // 458 - County specific DOCCODE (even within Megabyte counties)
   char  ConfirmedSalePrice[SALE_SIZ_SALEPRICE]; // 461
   char  AdjSalePrice[SALE_SIZ_SALEPRICE];   // 471
   char  OtherApn[SALE_SIZ_APN];             // 481
   char  NameCnt[SALE_SIZ_NAMECNT];          // 495
   char  Nbh_Code[SALE_SIZ_NBHCODE];         // 497 - Neighborhood code
   char  XferType;                           // 506 - T=Transfer (denote transfer record extract from old CD)
   char  ARCode;                             // 507 - Data source (A)ssessor/(R)ecorder
   char  Etal;                               // 508 - Y (known transfer with more than 2 owners)
   char  ApnMatched;                         // 509 - Y/N (for Butte county used in CD-Assessor only)
   char  OwnerMatched;                       // 510 - Y/N (for Butte county used in CD-Assessor only)
   char  CRLF[2];                            // 511
} SCSAL_REC;
#define SCSALREC_SIZE      sizeof(SCSAL_REC)

typedef struct t_SCSale_Ext
{  // 1024-bytes
   char  Apn[SALE_SIZ_APN];                  // 1
   char  DocNum[SALE_SIZ_DOCNUM];            // 15
   char  DocDate[SALE_SIZ_DOCDATE];          // 27
   char  DocType[SALE_SIZ_DOCTYPE];          // 35  - Standard DocType code in tblLkUp table [Deed Type Index]
   char  SalePrice[SALE_SIZ_SALEPRICE];      // 57
   char  SaleCode[SALE_SIZ_SALECODE];        // 67  - (F)ull, (P)artial, I(With lien), N(less lien), W(With other properties) 
   char  Seller1[SALE_SIZ_SELLER];           // 69
   char  StampAmt[SALE_SIZ_STAMPAMT];        // 117
   char  NumOfPrclXfer[SALE_SIZ_NOPRCLXFR];  // 127
   char  NoneSale_Flg;                       // 129 - Known non-sale record (Transfer, Quit claim, ...) for internal used only
   char  Spc_Flg;                            // 130 - 1=conf sale, 2=No DocNum, 3=DocNum from GrGr (VEN)
                                             //       3=DocNum from GrGr, 4=SFR, 5=Multi parcel, 6=Mobile home, 7=Land, 8=Dock, 9=Others (SBD)
   char  Name1[SALE_SIZ_BUYER];              // 131
   char  Name2[SALE_SIZ_BUYER];              // 183
   char  CareOf[SALE_SIZ_CAREOF];            // 235
   char  MailAdr1[SALE_SIZ_M_ADR1];          // 287
   char  MailAdr2[SALE_SIZ_M_ADR2];          // 339
   char  MailZip[SALE_SIZ_M_ZIP];            // 379
   char  Seller2[SALE_SIZ_SELLER];           // 384
   char  TransferDate[SALE_SIZ_DOCDATE];     // 432
   char  MultiSale_Flg;                      // 440 - Y/N
   char  PrimaryApn[SALE_SIZ_APN];           // 441
   char  PctXfer[SALE_SIZ_PCTXFER];          // 455
   char  DocCode[SALE_SIZ_DOCCODE];          // 458 - County specific DOCCODE (even within Megabyte counties)
   char  ConfirmedSalePrice[SALE_SIZ_SALEPRICE]; // 461
   char  AdjSalePrice[SALE_SIZ_SALEPRICE];   // 471
   char  OtherApn[SALE_SIZ_APN];             // 481
   char  NameCnt[SALE_SIZ_NAMECNT];          // 495
   char  Nbh_Code[SALE_SIZ_NBHCODE];         // 497 - Neighborhood code
   char  XferType;                           // 506 - T=Transfer (denote transfer record extract from old CD)
   char  ARCode;                             // 507 - Data source (A)ssessor/(R)ecorder
   char  Etal;                               // 508 - Y (known transfer with more than 2 owners)
   char  ApnMatched;                         // 509 - Y/N (for Butte county used in CD-Assessor only)
   char  OwnerMatched;                       // 510 - Y/N (for Butte county used in CD-Assessor only)
   char  NoneXfer_Flg;                       // 511 - known non transfer (i.e. Notice of Completion, CERTIFICATE AMOUNT DUE, ...)
   char  filler1;
   char  M_StrNum[SALE_SIZ_M_STRNUM];        // 513
   char  M_StrSub[SALE_SIZ_M_STRSUB];        // 520
   char  M_PreDir[SALE_SIZ_M_DIR];           // 523
   char  M_StrName[SALE_SIZ_M_STREET];       // 525
   char  M_StrSfx[SALE_SIZ_M_SUFF];          // 549
   char  M_PostDir[SALE_SIZ_M_DIR];          // 554
   char  M_UnitNo[SALE_SIZ_M_UNITNO];        // 556
   char  M_City[SALE_SIZ_M_CITY];            // 562
   char  M_St[SALE_SIZ_M_ST];                // 579
   char  MailZip4[SALE_SIZ_M_ZIP4];          // 581

   char  Lot[SALE_SIZ_LOT];                  // 585
   char  Tract[SALE_SIZ_TRACT];              // 591
   char  Block[SALE_SIZ_BLOCK];              // 597

   char  DocTitle[SALE_SIZ_DOCTITLE];        // 603 - Original DocTitle
   char  InsertDate[SALE_SIZ_DOCDATE];       // 627
   char  S_Addr1[SALE_SIZ_S_RAWADDR];        // 635
   char  S_Addr2[SALE_SIZ_S_RAWADDR];        // 715
   char  filler2[228];                       // 795
   char  CRLF[2];                            // 1023
} SCSAL_EXT;
#define SCSAL_EXT_SIZE      sizeof(SCSAL_EXT)

#define  OFF_GD_DOCNUM     1
#define  OFF_GD_APN        17
#define  OFF_GD_DOCDATE    37
#define  OFF_GD_TITLE      45
#define  OFF_GD_TAX        109
#define  OFF_GD_SALE       119
#define  OFF_GD_SALECODE   129
#define  OFF_GD_NAMECNT    133
#define  OFF_GD_GRANTOR    137
#define  OFF_GD_GRANTEE    241
#define  OFF_GD_REF        345
#define  OFF_GD_AMATCH     445
#define  OFF_GD_OMATCH     446
#define  OFF_GD_NONESALE   447
#define  OFF_GD_NONEXFER   448
#define  OFF_GD_DOCTYPE    449
#define  OFF_GD_NUMPAGES   453
#define  OFF_GD_CITYTAX    457
#define  OFF_GD_MULTIAPN   467

#define  SIZ_GD_DOCNUM     16
#define  SIZ_GD_APN        20
#define  SIZ_GD_DOCDATE    8
#define  SIZ_GD_TITLE      64
#define  SIZ_GD_TAX        10
#define  SIZ_GD_SALE       10
#define  SIZ_GD_SALECODE   4
#define  SIZ_GD_NAMECNT    4
#define  SIZ_GD_NAME       52
#define  SIZ_GD_REF        100
#define  SIZ_GD_DOCTYPE    4
#define  SIZ_GD_NUMPAGES   4
#define  SIZ_GD_FILLER     43

#define  SIZ_GD_LEGAL      50

typedef struct _tGrGrDoc
{  // 512-bytes
   char  DocNum[SIZ_GD_DOCNUM];
   char  APN[SIZ_GD_APN];
   char  DocDate[SIZ_GD_DOCDATE];
   char  DocTitle[SIZ_GD_TITLE];          // 45 - DocTitle
   char  DocTax[SIZ_GD_TAX];
   char  SalePrice[SIZ_GD_SALE];
   char  SaleCode[SIZ_GD_SALECODE];
   char  NameCnt[SIZ_GD_NAMECNT];         // 133
   char  Grantor[2][SIZ_GD_NAME];         // 137 - Grantor
   char  Grantee[2][SIZ_GD_NAME];         // 241 - Grantee
   char  ReferenceData[SIZ_GD_REF];
   char  ApnMatched;
   char  OwnerMatched;
   char  NoneSale;                        // 447 - None sale Y/N
   char  NoneXfer;                        // 448 - None xfer Y/N
   char  DocType[SIZ_GD_DOCTYPE];         // 449 - DocType
   char  NumPages[SIZ_GD_NUMPAGES];
   char  CityTax[SIZ_GD_TAX];
   char  MultiApn;                        // 467
   char  filler2[SIZ_GD_FILLER];          // 468
   char  CRLF[2];                         // 511
} GRGR_DOC;

typedef struct _tSloSale
{  // 1024-bytes
   char  Apn[SALE_SIZ_APN];               // 1
   char  DocNum[SALE_SIZ_DOCNUM];         // 15
   char  DocDate[SALE_SIZ_DOCDATE];       // 27
   char  DocType[SALE_SIZ_DOCTYPE];       // 35
   char  DocTax[SALE_SIZ_STAMPAMT];       // 57
   char  SalePrice[SALE_SIZ_SALEPRICE];   // 67
   char  SaleCode[SALE_SIZ_SALECODE];     // 77
   char  Grantor[6][SALE_SIZ_NAME];       // 79
   char  Grantee[6][SALE_SIZ_NAME];       // 379
   char  NameCnt[2];                      // 679
   char  filler[342];                     // 681
   char  CRLF[2];                         // 1023
} SLO_SALE;

#define  SIZ_OGR_DOCKEY    12
#define  SIZ_OGR_DOCNUM    16
#define  SIZ_OGR_DOCTAX    10
#define  SIZ_OGR_DOCCODE   6
#define  SIZ_OGR_PARCEL    16
#define  SIZ_OGR_NAME      52
#define  SIZ_OGR_ADDR1     52
#define  SIZ_OGR_CITY      30
#define  SIZ_OGR_STATE     2
#define  SIZ_OGR_ZIP       10
#define  SIZ_OGR_LOT       6
#define  SIZ_OGR_UNIT      6
#define  SIZ_OGR_TRACT     6
#define  SIZ_OGR_BLOCK     6
#define  SIZ_OGR_DOCDATE   8
#define  SIZ_OGR_DOCTITLE  24

#define  OFF_OGR_PARCEL    1-1
#define  OFF_OGR_DOCDATE   17-1
#define  OFF_OGR_DOCNUM    25-1

typedef struct _tOrgGrGr
{  // 512 bytes
   char  ParcelNo[SIZ_OGR_PARCEL];     // 1
   char  DocDate[SIZ_OGR_DOCDATE];     // 17
   char  DocNum[SIZ_OGR_DOCNUM];       // 25
   char  DocTitle[SIZ_OGR_DOCTITLE];   // 41
   char  DocTax[SIZ_OGR_DOCTAX];       // 65
   char  SalePrice[SIZ_GR_SALE];       // 75
   char  DocCode[SIZ_OGR_DOCCODE];     // 85
   char  Addr1[SIZ_OGR_ADDR1];         // 91 - Mail addr1
   char  City[SIZ_OGR_CITY];           // 143
   char  State[SIZ_OGR_STATE];         // 173
   char  Zip[SIZ_OGR_ZIP];             // 175
   char  Lot[SIZ_OGR_LOT];             // 185
   char  Unit[SIZ_OGR_UNIT];           // 191
   char  Tract[SIZ_OGR_TRACT];         // 197
   char  Block[SIZ_OGR_BLOCK];         // 203
   char  Grantor[2][SIZ_OGR_NAME];     // 209
   char  Grantee[2][SIZ_OGR_NAME];     // 313
   char  DocKey[SIZ_OGR_DOCKEY];       // 417
   char  ApnMatched;                   // 429
   char  OwnerMatched;                 // 430
   char  MoreName;                     // 431
   char  NoneSale_Flg;                 // 432
   char  IsXfer_Flg;                   // 433
   char  filler[69];                   // 434
   char  ProcDate[SIZ_OGR_DOCDATE];    // 503 - processing date
   char  CrLf[2];                      // 511
} ORG_GRGR;

extern FILE  *fdGrGr, *fdSale, *fdCSale;

int   MergeSale(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag=false);
int   MergeSale1(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag=false);
int   MergeSale2(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag=false);
int   MergeSale3(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag=true);
int   MergeSaleX(CSAL_REC *pSaleRec, char *pOutbuf, bool bSaleFlag=false);
int   MergeSaleXFile(char *pInfile, char *pOutfile, char *pXSalefile);
int   MergeXferFile(char *pInfile, char *pOutfile, char *pXferfile);
int   MergeCumSale(int iSkip, char *pCSaleFile=NULL);
int   MergeCumSale1(int iSkip, char *pCSaleFile=NULL, bool bResort=true, bool bNoPrice=false, bool bUpdtXfer=false);
int   MergeCumGrGr(int iSkip, char *pCSaleFile);
int   ApplyCumSale(int iSkip, char *pCSaleFile=NULL, bool bResort=true, int iType=SALE_USE_SALEREC1, int iClearSaleFlg=CLEAR_OLD_SALE);
int   ApplyCumSaleWP(int iSkip, char *pCSaleFile=NULL, bool bResort=true, int iType=SALE_USE_SCSALREC, int iClearSaleFlg=CLEAR_OLD_SALE);
int   ApplyCumSaleDN(int iSkip, char *pCSaleFile=NULL, bool bResort=true, int iType=SALE_USE_SALEREC1, int iClearSaleFlg=CLEAR_OLD_SALE);
int   ApplyCumSaleNR(int iSkip, char *pCSaleFile=NULL, int iType=SALE_USE_SCUPDXFR, int iUpdateFlg=0, int iFileCnt=1);

int   filterSale(char *pInfile, char *pOutfile, int fltFlag);
void  ClearOldSale(char *pOutbuf, bool bDelXfer=false);
void  ClearOneSale(char *pOutbuf, int iSaleNum);

char *findDocType(char *pDocTitle, char *pCode);
char *findDocType(char *pDocTitle, char *pCode, IDX_TBL2 *pTbl);
bool  findDocType(char *pDocTitle, char *pCode, IDX_TBL4 *pTbl);
int   findDocType(char *pDocTitle, IDX_TBL4 *pDocTbl);
int   findDocType(char *pDocTitle, IDX_TBL5 *pDocTbl);
int   findDocType(char *pDocTitle, IDX_SALE *pDocTbl);
int   findSortedDocType(char *pDocTitle, IDX_TBL5 *pDocTbl);

long  ExtrCSale(char *pInfile, char *pOutfile, int iRecSize=1900);
long  ExtrHSale(char *pInfile, char *pOutfile, int iRecSize=1900);
long  ExtrHSale1(char *pInfile, char *pOutfile, int iRecSize=1900);
long  ExtrXfer (char *pInfile, char *pOutfile, int iRecSize=1900);
int   convertSaleData(char *pCnty, char *pInfile, int iType, char *pOutfile=NULL);
int   convertXferData(char *pInfile, char *pOutfile);
int   ApplySCSalRec(char *pOutbuf, char *pSale, bool bNoPrice=false, bool bUpdtXfer=false, char cDataSrc='A');
int   ApplySCSalRec_NoDocChk(char *pOutbuf, char *pSale, bool bNoPrice=false, bool bUpdtXfer=false, char cDataSrc='A');
int   ApplySCSalRec_NoDateChk(char *pOutbuf, char *pSale, bool bNoPrice=false, bool bUpdtXfer=false, char cDataSrc='A');
int   ApplySCSalRecWP(char *pOutbuf, char *pSale, bool bUpdtXfer=false, char cDataSrc='A');
int   ApplySCSalRec_NoChk(char *pOutbuf, char *pSale, bool bUpdtXfer=false, char cDataSrc='A');

// Use this function to apply GD without price to R01
int   ApplySCSalRecGD(char *pOutbuf, char *pSale, char cDataSrc='R');

int   CombineSaleAndTransfer(char *pCnty);
int   CleanupSales(char *pInfile);
int   FixSalePrice(char *pInfile, bool bResort=false);
int   FixCumSale(char *pInfile, int iType, bool bRemove=false); // remove suspected bad records.
int   FixDocType(char *pInfile, IDX_TBL5 *pDocTbl, bool bRemove=false, bool bFormatStamp=false); // remove records w/o DocNum
int   FixSCSal(char *pInfile, int iType, int iOption=0, bool bRemove=false);
int   FixSCSalEx(char *pInfile, int iType, int iOption=0, bool bRemove=false);

int   createSaleImport(LPCSTR CountyCode, LPCSTR pInfile, LPCSTR pOutfile, int iType=1, bool bInclHdr=false);
int   createSaleImport(void (*fn)(LPSTR, LPSTR, LPSTR), LPCSTR CountyCode, LPCSTR pInfile, LPCSTR pOutfile, int iType=1, bool bInclHdr=false);
int   createWebSaleImport(LPCSTR CountyCode, LPCSTR pInfile, LPCSTR pOutfile, int iType=2, bool bInclHdr=false, IDX_TBL5 *pDocTbl=NULL);

int   MergeSaleFiles(char *pFile1, char *pFile2, char *pOutfile, bool bResort=true);
int   DedupCumSale(char *pInfile);
bool  ChkDocTax(char *pDocTax);
int   FixDocTax(char *pInfile);
int   SaleDedup(char *pInfile);
int   UpdateSaleUsingGrGr(char *pGrgrFile, int UpdFlds, bool bSaleOnly=false);
int   FixOtherApn(char *pInfile, int iLen);
int   SetMultiSale(char *pInfile, char *pSrtCmd, char *pCnty);
int   SetFullPartialSale(char *pInfile);
int   ResetSaleCode(char *pInfile, char *pOutfile);

void  SetChkYearFlg(bool bVal);
void  SetKeepMAFlg(bool bVal);
int   SC2SCExt(char *pInfile);

#endif