#ifndef _CHAR_REC_
#define _CHAR_REC_

//#define  CHAR_APN_S           1
//#define  CHAR_LOT_ACRES       15
//#define  CHAR_FILLER1         24
//#define  CHAR_LOT_SQFT        35
//#define  CHAR_YR_BLT          44
//#define  CHAR_YR_EFF          48
//#define  CHAR_FILLER2         52
//#define  CHAR_BLDG_SF         63
//#define  CHAR_UNITS           72
//#define  CHAR_STORIES         78
//#define  CHAR_BEDS            82
//#define  CHAR_BATH_F          84
//#define  CHAR_BATH_H          86
//#define  CHAR_ROOMS           88
//#define  CHAR_BLDG_CLASS      91
//#define  CHAR_BLDG_QUAL       92
//#define  CHAR_YR_ASSD         96
//#define  CHAR_FILLER3         100
//#define  CHAR_IMPR_COND       101
//#define  CHAR_FIRE_PL         110
//#define  CHAR_BLDGS           112
//#define  CHAR_AIR_COND        114
//#define  CHAR_HEAT            137
//#define  CHAR_GAR_SQFT        158
//#define  CHAR_PARK_TYPE       164
//#define  CHAR_PARK_SPACE      181
//#define  CHAR_VIEW            187
//#define  CHAR_STYLE           206
//#define  CHAR_IMPR_TYPE       219
//#define  CHAR_CONS_TYPE       237
//#define  CHAR_FNDN_TYPE       253
//#define  CHAR_ROOF_TYPE       267
//#define  CHAR_ROOF_MAT        282
//#define  CHAR_FRAME_TYPE      301
//#define  CHAR_EX_WALL_TYPE    309
//#define  CHAR_WATER           328
//#define  CHAR_SEWER           340
//#define  CHAR_POOL            354
//#define  CHAR_FILLER4         366
//#define  CHAR_CRLF            383

//#define  EXSIZ_LOT_ACRES      9
//#define  EXSIZ_LOT_SQFT       9
//#define  EXSIZ_YR_BLT         4
//#define  EXSIZ_YR_EFF         4
//#define  EXSIZ_BLDG_SF        9
//#define  EXSIZ_UNITS          6
//#define  EXSIZ_STORIES        4
//#define  EXSIZ_BEDS           2
//#define  EXSIZ_BATH_F         2
//#define  EXSIZ_BATH_H         2
//#define  EXSIZ_ROOMS          3
//#define  EXSIZ_BLDG_CLASS     1
//#define  EXSIZ_BLDG_QUAL      4
//#define  EXSIZ_YR_ASSD        4
//#define  EXSIZ_UNS_FLG        1
//#define  EXSIZ_IMPR_COND      9
//#define  EXSIZ_FIRE_PL        2
//#define  EXSIZ_BLDGS          2
//#define  EXSIZ_AIR_COND       23
//#define  EXSIZ_HEAT           21
//#define  EXSIZ_GAR_SQFT       6
//#define  EXSIZ_PARK_TYPE      17
//#define  EXSIZ_PARK_SPACE     6
//#define  EXSIZ_VIEW           19
//#define  EXSIZ_STYLE          13
//#define  EXSIZ_IMPR_TYPE      18
//#define  EXSIZ_CONS_TYPE      16
//#define  EXSIZ_FNDN_TYPE      14
//#define  EXSIZ_ROOF_TYPE      15
//#define  EXSIZ_ROOF_MAT       19
//#define  EXSIZ_FRAME_TYPE     8
//#define  EXSIZ_EX_WALL_TYPE   19
//#define  EXSIZ_WATER          12
//#define  EXSIZ_SEWER          14
//#define  EXSIZ_POOL           12
//#define  EXSIZ_LEGAL          40
//#define  EXSIZ_OTHER_ROOM     40

//typedef struct _tCharExt
//{  // 385-byte
//   char  Apn[SIZ_APN_S];                
//   char  LotAcre[EXSIZ_LOT_ACRES]; 
//   char  filler1[11];
//   char  LotSqft[EXSIZ_LOT_SQFT];       
//   char  YrBlt[EXSIZ_YR_BLT];            
//   char  YrEff[EXSIZ_YR_BLT];     
//   char  filler2[11];
//   char  BldgSqft[EXSIZ_BLDG_SF];      
//   char  Units[EXSIZ_UNITS];          
//   char  Stories[EXSIZ_STORIES];        
//   char  Beds[EXSIZ_BEDS];             
//   char  Bath[EXSIZ_BATH_F];             
//   char  HBath[EXSIZ_BATH_H];          
//   char  Rooms[EXSIZ_ROOMS];           
//   char  BldgClass[EXSIZ_BLDG_CLASS];                  
//   char  BldgQual[EXSIZ_BLDG_QUAL];                      
//   char  YrAssd[EXSIZ_YR_ASSD];
//   char  filler3[1];
//   char  ImprCond[EXSIZ_IMPR_COND];
//   char  FirePlace[EXSIZ_FIRE_PL];                     
//   char  Bldgs[EXSIZ_BLDGS];
//   char  AC[EXSIZ_AIR_COND];
//   char  Heat[EXSIZ_HEAT];                          
//   char  GarSqft[EXSIZ_GAR_SQFT];       
//   char  ParkType[EXSIZ_PARK_TYPE];
//   char  ParkSpace[EXSIZ_PARK_SPACE];
//   char  View[EXSIZ_VIEW];                      
//   char  Style[EXSIZ_STYLE];
//   char  ImprType[EXSIZ_IMPR_TYPE];
//   char  ConstType[EXSIZ_CONS_TYPE];                         
//   char  FndnType[EXSIZ_FNDN_TYPE];
//   char  RoofType[EXSIZ_ROOF_TYPE];                      
//   char  RoofMat[EXSIZ_ROOF_MAT];                      
//   char  FrameType[EXSIZ_FRAME_TYPE];
//   char  ExWallType[EXSIZ_EX_WALL_TYPE];
//   char  Water[EXSIZ_WATER];
//   char  Sewer[EXSIZ_SEWER];
//   char  Pool[EXSIZ_POOL]; 
//   char  filler[17];
//   char  CrLf[2];                         
//} CHAR_EXT;

#define  SIZ_CHAR_APN            14
#define  SIZ_CHAR_APN_D          20
#define  SIZ_CHAR_QCLS           6
#define  SIZ_CHAR_TRA            6
#define  SIZ_CHAR_BEDS           4
#define  SIZ_CHAR_BATHS          4
#define  SIZ_CHAR_SIZE2          2
#define  SIZ_CHAR_SIZE4          4
#define  SIZ_CHAR_SIZE8          8
#define  SIZ_CHAR_DATE           8
#define  SIZ_CHAR_YRBLT          4
#define  SIZ_CHAR_YREFF          4
#define  SIZ_CHAR_SQFT           10
#define  SIZ_CHAR_ROOMS          4
#define  SIZ_CHAR_UNITS          4
#define  SIZ_CHAR_AC             10
#define  SIZ_CHAR_ZONING         10
#define  SIZ_CHAR_MISC           20
#define  SIZ_CHAR_TRS            12
#define  SIZ_CHAR_COMMENTS       80
#define  SIZ_CHAR_BLDGTYPE       6
#define  SIZ_CHAR_QUALCLS        6
#define  SIZ_CHAR_DOCNUM         12
#define  SIZ_CHAR_SALEPRICE      10
#define  SIZ_CHAR_NBH_CODE       6
#define  SIZ_CHAR_LEGAL          80
#define  SIZ_CHAR_SUBDIV         80
#define  SIZ_CHAR_AMENITIES      80

typedef struct _tStdCharExtra
{  // 80 bytes - same as SIZ_CHAR_COMMENTS
   char DetGarSqft[SIZ_CHAR_SQFT];     // 431
   char CarportSqft[SIZ_CHAR_SQFT];    // 441
   char OfficeSqft[SIZ_CHAR_SQFT];     // 451
   char RecDate[SIZ_CHAR_SIZE8];       // 461
   char DocNum[SIZ_CHAR_DOCNUM];       // 469
   char SalePrice[SIZ_CHAR_SALEPRICE]; // 481
   char FirePlaceType[SIZ_CHAR_SIZE2]; // 491   YOL, YUB, MER
   char CarPort[SIZ_CHAR_SIZE4];       // 493   SBD
   char OpenSpace[SIZ_CHAR_SIZE4];     // 497   SBD
   char RVSpace[SIZ_CHAR_SIZE2];       // 501   SBD
   char PropType;                      // 503   C=commercial, R=Residential
   char TRA[SIZ_CHAR_TRA];             // 504   SDX
   char filler;                        // 510
} CHAREXTRA;

typedef struct _tSclExtra
{  // 80 bytes - same as SIZ_CHAR_COMMENTS
   char LeaseSqft[SIZ_CHAR_SQFT];      // 431
   char CarportSqft[SIZ_CHAR_SQFT];    // 441
   char OfficeSqft[SIZ_CHAR_SQFT];     // 451
   char RecDate[SIZ_CHAR_SIZE8];       // 461
   char DocNum[SIZ_CHAR_DOCNUM];       // 469
   char SalePrice[SIZ_CHAR_SALEPRICE]; // 481
   char FirePlaceType[SIZ_CHAR_SIZE2]; // 491   
   char CarPort[SIZ_CHAR_SIZE4];       // 493   
   char OpenSpace[SIZ_CHAR_SIZE4];     // 497   
   char RVSpace[SIZ_CHAR_SIZE2];       // 501   
   char Rec_Room;                      // 503   Y/N
   char Tennis;                        // 504
   char Sauna;                         // 505
   char Patio_Balcony;                 // 506
   char CableTV;                       // 507
   char Garbage;                       // 508
   char Electric;                      // 509
   char Gas;                           // 510
} SCL_EXTRA;

typedef struct _tSbdExtra
{  // 80 bytes - same as SIZ_CHAR_COMMENTS
   char Sfr_Shape[SIZ_CHAR_SIZE2];        // 431
   char NeighborHood[SIZ_CHAR_NBH_CODE];  // 433
   char Filler[72];                       // 439
} SBD_EXTRA;

// Offset of fields in STDCHAR
#define  SCROFF_APN                       0
#define  SCROFF_FEEPARCEL                 14
#define  SCROFF_QUALCLASS                 28
#define  SCROFF_CONDITION                 34
#define  SCROFF_BEDS                      36
#define  SCROFF_FBATHS                    40
#define  SCROFF_HBATHS                    44
#define  SCROFF_ROOMS                     48
#define  SCROFF_YRBLT                     52
#define  SCROFF_YREFF                     56
#define  SCROFF_PARKTYPE                  60 
#define  SCROFF_PARKSPACE                 62 
#define  SCROFF_POOL                      66
#define  SCROFF_SPA                       68
#define  SCROFF_STORIES                   70
#define  SCROFF_BLDGS                     74
#define  SCROFF_UNITS                     76
#define  SCROFF_BLDGSEQNUM                80
#define  SCROFF_UNITSEQNUM                82
#define  SCROFF_FIREPLACE                 84
#define  SCROFF_HEATING                   86
#define  SCROFF_COOLING                   88
#define  SCROFF_HEATINGSRC                92
#define  SCROFF_COOLINGSRC                94
#define  SCROFF_ZONING                    98
#define  SCROFF_LANDUSE                   108
#define  SCROFF_LANDUSECAT                116
#define  SCROFF_VIEW                      118
#define  SCROFF_FRONTAGE                  120 
#define  SCROFF_PUD                       122  
#define  SCROFF_TOPO                      124
#define  SCROFF_IRREGLOT                  126
#define  SCROFF_CORNER                    128
#define  SCROFF_POWER                     130
#define  SCROFF_GAS                       132
#define  SCROFF_ROOFTYPE                  134
#define  SCROFF_ROOFMAT                   136
#define  SCROFF_IMPRTYPE                  138
#define  SCROFF_CONSTTYPE                 140
#define  SCROFF_FNDNTYPE                  142
#define  SCROFF_FRAMETYPE                 144
#define  SCROFF_EXWALLTYPE                146
#define  SCROFF_STYLE                     148
#define  SCROFF_HASWELL                   150
#define  SCROFF_HASSEPTIC                 151
#define  SCROFF_HASWATER                  152
#define  SCROFF_HASSEWER                  153
#define  SCROFF_BLDGCLASS                 154
#define  SCROFF_BLDGQUAL                  155
#define  SCROFF_HASMHREC                  156
#define  SCROFF_ASMTSTATUS                157
#define  SCROFF_AGP                       158
#define  SCROFF_TPZ                       159
#define  SCROFF_BLDGTYPE                  160
#define  SCROFF_NBHCODE                   166 
#define  SCROFF_TRS                       172 
#define  SCROFF_BLDGSQFT                  184
#define  SCROFF_GARSQFT                   194
#define  SCROFF_CARPSQFT                  204
#define  SCROFF_LOTSQFT                   214
#define  SCROFF_LOTACRES                  224
#define  SCROFF_BSMTSQFT                  234
#define  SCROFF_1STFLRSQFT                244
#define  SCROFF_2NDFLRSQFT                254
#define  SCROFF_PORCHSQFT                 264
#define  SCROFF_DECKSQFT                  274
#define  SCROFF_FLATWRKSQFT               284
#define  SCROFF_GUESTSQFT                 294
#define  SCROFF_MISCSQFT                  304
#define  SCROFF_ACCESS                    314
#define  SCROFF_ROAD                      334
#define  SCROFF_GRNDCVR                   354
#define  SCROFF_PATIOTYPE                 374
#define  SCROFF_PATIOSQFT                 376
#define  SCROFF_HASELEVATOR               386
#define  SCROFF_BATH_1Q                   387
#define  SCROFF_BATH_3Q                   389
#define  SCROFF_BATH_2Q                   391
#define  SCROFF_BATH_4Q                   393
#define  SCROFF_MISCIMPRCODE              395
#define  SCROFF_SQFT_ABOVE2ND             397
#define  SCROFF_APN_D                     407
#define  SCROFF_VACANTLOT                 427
#define  SCROFF_WATER                     428
#define  SCROFF_SEWER                     429
#define  SCROFF_COMMENT                   430
#define  SCROFF_UPDATED_DATE              510
#define  SCROFF_NETRENTAL                 518
#define  SCROFF_SFR_SHAPE                 528
#define  SCROFF_HASGAS                    530
#define  SCROFF_HASELECTRIC               531
#define  SCROFF_LEASEAREA                 532
#define  SCROFF_BATH_1QX                  542
#define  SCROFF_BATH_3QX                  546
#define  SCROFF_BATH_2QX                  550
#define  SCROFF_BATH_4QX                  554
#define  SCROFF_RECAREA                   558
#define  SCROFF_NUMSTUDIO                 568
#define  SCROFF_ONEBED                    572
#define  SCROFF_TWOBEDS                   576
#define  SCROFF_OTHROOMS                  580
#define  SCROFF_UNITSIZE                  584
#define  SCROFF_AMENITIES                 594
#define  SCROFF_DETGARSQFT                604
#define  SCROFF_ELEVATOR                  615
#define  SCROFF_ESCALATOR                 617
#define  SCROFF_BLDGCODE                  619

#define  SCROFF_NOTUSED                   -1

typedef struct _tStdCharRec
{  // 1024
   char Apn[SIZ_CHAR_APN];             // 1
   char FeeParcel[SIZ_CHAR_APN];       // 15
   char QualityClass[SIZ_CHAR_QCLS];   // 29
   char ImprCond[SIZ_CHAR_SIZE2];      // 35
   char Beds[SIZ_CHAR_BEDS];           // 37
   char FBaths[SIZ_CHAR_BATHS];        // 41
   char HBaths[SIZ_CHAR_BATHS];        // 45
   char Rooms[SIZ_CHAR_ROOMS];         // 49
   char YrBlt[SIZ_CHAR_YRBLT];         // 53
   char YrEff[SIZ_CHAR_YREFF];         // 57
   char ParkType[SIZ_CHAR_SIZE2];      // 61
   char ParkSpace[SIZ_CHAR_SIZE4];     // 63 
   char Pool[SIZ_CHAR_SIZE2];          // 67
   char Spa[SIZ_CHAR_SIZE2];           // 69
   char Stories[SIZ_CHAR_SIZE4];       // 71 
   char Bldgs[SIZ_CHAR_SIZE2];         // 75    // Number of buildings
   char Units[SIZ_CHAR_UNITS];         // 77    // Number of units
   char BldgSeqNo[SIZ_CHAR_SIZE2];     // 81
   char UnitSeqNo[SIZ_CHAR_SIZE2];     // 83
   char Fireplace[SIZ_CHAR_SIZE2];     // 85
   char Heating[SIZ_CHAR_SIZE2];       // 87
   char Cooling[SIZ_CHAR_SIZE2];       // 89
   char HeatingSrc[SIZ_CHAR_SIZE4];    // 91
   char CoolingSrc[SIZ_CHAR_SIZE4];    // 95
   char Zoning[SIZ_CHAR_ZONING];       // 99 
   char LandUse[SIZ_CHAR_SIZE8];       // 109
   char LandUseCat[SIZ_CHAR_SIZE2];    // 117
   char View[SIZ_CHAR_SIZE2];          // 119
   char Frontage[SIZ_CHAR_SIZE2];      // 121
   char Pud[SIZ_CHAR_SIZE2];           // 123 
   char Topo[SIZ_CHAR_SIZE2];          // 125  
   char IrregLot[SIZ_CHAR_SIZE2];      // 127
   char Corner[SIZ_CHAR_SIZE2];        // 129
   char Power[SIZ_CHAR_SIZE2];         // 131
   char Gas[SIZ_CHAR_SIZE2];           // 133
   char RoofType[SIZ_CHAR_SIZE2];      // 135
   char RoofMat[SIZ_CHAR_SIZE2];       // 137
   char ImprType[SIZ_CHAR_SIZE2];      // 139
   char ConstType[SIZ_CHAR_SIZE2];     // 141                   
   char FndnType[SIZ_CHAR_SIZE2];      // 143
   char FrameType[SIZ_CHAR_SIZE2];     // 145
   char ExWallType[SIZ_CHAR_SIZE2];    // 147
   char Style[SIZ_CHAR_SIZE2];         // 149
   char HasWell;                       // 151
   char HasSeptic;                     // 152
   char HasWater;                      // 153
   char HasSewer;                      // 154
   char BldgClass;                     // 155
   char BldgQual;                      // 156
   char HasMHRec;                      // 157
   char AsmtStatus;                    // 158
   char AGP;                           // 159
   char TPZ;                           // 160
   char BldgType[SIZ_CHAR_BLDGTYPE];   // 161 - SCR - BldgQual
   char Nbh_Code[SIZ_CHAR_NBH_CODE];   // 167
   char TRS[SIZ_CHAR_TRS];             // 173
   char BldgSqft[SIZ_CHAR_SQFT];       // 185
   char GarSqft[SIZ_CHAR_SQFT];        // 195
   char CarpSqft[SIZ_CHAR_SQFT];       // 205
   char LotSqft[SIZ_CHAR_SQFT];        // 215
   char LotAcre[SIZ_CHAR_SQFT];        // 225
   char BsmtSqft[SIZ_CHAR_SQFT];       // 235
   char Sqft_1stFl[SIZ_CHAR_SQFT];     // 245
   char Sqft_2ndFl[SIZ_CHAR_SQFT];     // 255
   char PorchSqft[SIZ_CHAR_SQFT];      // 265
   char DeckSqft[SIZ_CHAR_SQFT];       // 275
   char FlatWrkSqft[SIZ_CHAR_SQFT];    // 285
   char GuestSqft[SIZ_CHAR_SQFT];      // 295 
   char MiscSqft[SIZ_CHAR_SQFT];       // 305 - Additional Area
   char Access[SIZ_CHAR_MISC];         // 315 
   char Road[SIZ_CHAR_MISC];           // 335
   char GrndCvr[SIZ_CHAR_MISC];        // 355
   char PatioType[SIZ_CHAR_SIZE2];     // 375
   char PatioSqft[SIZ_CHAR_SQFT];      // 377
   char HasElevator;                   // 387
   char Bath_1Q[SIZ_CHAR_SIZE2];       // 388
   char Bath_3Q[SIZ_CHAR_SIZE2];       // 390
   char Bath_2Q[SIZ_CHAR_SIZE2];       // 392
   char Bath_4Q[SIZ_CHAR_SIZE2];       // 394
   char MiscImpr_Code[SIZ_CHAR_SIZE2]; // 396 - KIN, MOD
   char Sqft_Above2nd[SIZ_CHAR_SQFT];  // 398
   char Apn_D[SIZ_CHAR_APN_D];         // 408
   char VacantLot;                     // 428 - CCX Y/N
   char Water;                         // 429 - water service
   char Sewer;                         // 430 - sewer service
   union 
   {
      char Comment[SIZ_CHAR_COMMENTS]; // 431 - SIS
      char SubDiv[SIZ_CHAR_SUBDIV];    //     - TUO
      char Legal[SIZ_CHAR_LEGAL];      //     - FRE
      CHAREXTRA sExtra;                //     - MON,AMA,COL,MER
   } Misc;
   char Date_Updated[SIZ_CHAR_DATE];   // 511 - county provided
   char NetRental[SIZ_CHAR_SQFT];      // 519 - SAC, SCL, ALA
   char Sfr_Shape[SIZ_CHAR_SIZE2];     // 529
   char HasGas;                        // 531
   char HasElectric;
   char LeaseArea[SIZ_CHAR_SQFT];      // 533 - SCL
   char Bath_1QX[SIZ_CHAR_SIZE4];      // 543
   char Bath_3QX[SIZ_CHAR_SIZE4];      // 547
   char Bath_2QX[SIZ_CHAR_SIZE4];      // 551
   char Bath_4QX[SIZ_CHAR_SIZE4];      // 555
   char RecArea[SIZ_CHAR_SQFT];        // 559 
   char NumStudio[SIZ_CHAR_BEDS];      // 569 - SMX
   char OneBed[SIZ_CHAR_BEDS];         // 573 - SMX
   char TwoBeds[SIZ_CHAR_BEDS];        // 577 - SMX
   char ThreeBeds[SIZ_CHAR_BEDS];      // 581 - SMX
   char FourBeds[SIZ_CHAR_BEDS];       // 585 - SMX
   char OthRooms[SIZ_CHAR_BEDS];       // 589
   char UnitSize[SIZ_CHAR_SQFT];       // 593 - IMP
   char DetGarSqft[SIZ_CHAR_SQFT];     // 603 - IMP
   char Elevator[SIZ_CHAR_SIZE2];      // 613 - IMP,SMX: number of elevators
   char Escalator[SIZ_CHAR_SIZE2];     // 615 - IMP
   char Solar[SIZ_CHAR_SIZE4];         // 617 - COL
   char BuildingCode[SIZ_CHAR_MISC];   // 621 - IMP
   char HasSolar;                      // 641 - SOL,RIV
   char HasUtilities;                  // 642 - SOL
   char HasDiningRoom;                 // 643 - SOL
   char HasFamilyRoom;                 // 644 - SOL
   char Amenities[SIZ_CHAR_AMENITIES]; // 645 - ALA, SMX
   ADR_REC  sAdr;                      // 725 - INY

   char filler[18];                    // 905
   char CRLF[2];                       // 1023
} STDCHAR;

int   MergeCharRec(char *pCnty);
long  ExtrHChar(char *pInfile, char *pOutfile);

#endif