#include "stdafx.h"
#include "logs.h"
#include "keycode.h"
#include "defines.h"
#include "see.h"

static char sMailTo[256], sMailFrom[256], sSmtp[256], sMailLog[256], sMailTech[256], 
				sSmtpUid[64], sSmtpPwd[32], sProxyDir[256], sProxyCert[256], sProxyExe[256];
static bool bSendSSL;
static int	iSmtpPort;

/******************************************************************************
 *
 * Populate mail global variables
 *
 ******************************************************************************/

int getConfig(LPCSTR pConfig)
{
	char  sTmp[256];
	int	iRet;

   GetPrivateProfileString("Mail", "ProxyCert", "", sProxyCert, 256, pConfig);
   GetPrivateProfileString("Mail", "ProxyExe", "", sProxyExe, 256, pConfig);
   GetPrivateProfileString("Mail", "ProxyDir", "", sProxyDir, 256, pConfig);
   GetPrivateProfileString("Mail", "Host", "", sSmtp, 256, pConfig);
   GetPrivateProfileString("Mail", "MailTo", "", sMailTo, 256, pConfig);
   GetPrivateProfileString("Mail", "MailFrom", "", sMailFrom, 256, pConfig);
   iRet = GetPrivateProfileString("Mail", "MailTech", "", sMailTech, 256, pConfig);
	if (iRet < 10)
		strcpy(sMailTech, sMailFrom);
   GetPrivateProfileString("Mail", "MailLog", "", sMailLog, 256, pConfig);
   GetPrivateProfileString("Mail", "SmtpUid", "accounts", sSmtpUid, 256, pConfig);
   GetPrivateProfileString("Mail", "SmtpPwd", "P@ssw0rd", sSmtpPwd, 256, pConfig);
   GetPrivateProfileString("Mail", "SmtpPort", "587", sTmp, 32, pConfig);
	iSmtpPort = atol(sTmp);

   GetPrivateProfileString("Mail", "SSL", "N", sTmp, 32, pConfig);
	if (sTmp[0] == 'Y')
		bSendSSL = true;
	else
		bSendSSL = false;
   return 0;
}

/******************************************************************************
 *
 *
 ******************************************************************************/

int mySendMail(LPCSTR pConfig, LPSTR pSubj, LPSTR pBody, LPSTR pMailTo)
{
	char	sTmp[256];

   int iRet = seeAttach(1, SEE_KEY_CODE);
   
   if (iRet < 0)
   {
      printf("seeAttach fails: Check key iRet");
      return iRet;
   }

   // Initialize
   if (!pConfig)
   {
      LogMsg("***** Mail Config file required");
      return -1;
   }

   iRet = getConfig(pConfig);

   // enable log file
   if (sMailLog[0])
      seeStringParam(0, SEE_LOG_FILE, sMailLog);

   if (pMailTo)
      strcpy(sMailTo, pMailTo);
   iRet = seeVerifyFormat(sMailTo);
   if (iRet < 0)
   {
		seeErrorText(0, iRet, (char *)sTmp, 255);
      LogMsg("***** Bad MailTo addr: Check %s [%s]", sMailTo, sTmp);
      return iRet;
   }

   iRet = seeVerifyFormat(sMailFrom);
   if (iRet < 0)
	{
		seeErrorText(0, iRet, (char *)sTmp, 255);
      LogMsg("***** Bad MailFrom addr: Check %s [%s]", sMailFrom, sTmp);
      return iRet;
	}
  
   if (bSendSSL)
	{
		// enable "SMTP Authentication"
		seeIntegerParam(0, SEE_ENABLE_ESMTP, 1);
		// specify the SMTP port
		seeIntegerParam(0, SEE_SMTP_PORT, iSmtpPort);
		// Set proxy params
		iRet = seeSetProxySSL(0, 0, sProxyDir, sProxyCert, sProxyExe);
		// authenticate smtp user
		iRet = seeSmtpConnectSSL(0, 8801, iSmtpPort, sSmtp, sSmtpUid, sSmtpPwd, sMailFrom, sMailTech, "");
	} else
		iRet = seeSmtpConnect(0, sSmtp, sMailFrom, sMailTech);
   if (iRet < 0)
   {
      LogMsg("***** seeSmtpConnect fails: Check %s [Errno: %d]", sSmtp, iRet);
   } else
   {
      LogMsg((char *)"Send email to: %s", sMailTo);
      iRet = seeSendEmail(0, sMailTo, NULL, NULL, pSubj, pBody, NULL);
      if (iRet < 0)
      {
         if (iRet == SEE_TIMED_OUT)
            LogMsg("***** seeSendEmail is timed out.  Verify inbox to make sure email is sent.");
         else
            LogMsg("***** seeSendEmail fails: Check %s [Errno: %d]", sMailTo, iRet);
      }
		seeClose(0);
   }

   seeRelease();

   return iRet;
}

/******************************************************************************
 *
 *
 ******************************************************************************/

int initMail(char *pConfig, bool bDebug)
{
   int	iRet;
   
   LogMsg("Initialize mail system ...");

   iRet = seeAttach(1, SEE_KEY_CODE);   
   if (iRet < 0)
   {
      LogMsg("***** Mail initialize failed: Check key code");
      return iRet;
   }

	iRet = getConfig(pConfig);

   // enable log file
   if (sMailLog[0])
   {
      if (bDebug)
         LogMsg("Setting mail log to : %s", sMailLog);
      seeStringParam(0, SEE_LOG_FILE, sMailLog);
   }

   if (bDebug)
      LogMsg("Mail to be sent from : %s", sMailFrom);

   iRet = seeVerifyFormat(sMailFrom);
   if (iRet<0)
      LogMsg("***** Mailfrom addr check failed: Check %s", sMailFrom);
   else
   {
      if (bDebug)
         LogMsg("Connecting to SMTP : %s", sSmtp);

      // Connect to mail server
		if (bSendSSL)
		{
			// enable "SMTP Authentication"
			seeIntegerParam(0, SEE_ENABLE_ESMTP, 1);
			// specify the SMTP port
			seeIntegerParam(0, SEE_SMTP_PORT, iSmtpPort);
			// Set proxy params
			iRet = seeSetProxySSL(0, 0, sProxyDir, sProxyCert, sProxyExe);
			// authenticate smtp user
			iRet = seeSmtpConnectSSL(0, 8801, iSmtpPort, sSmtp, sSmtpUid, sSmtpPwd, sMailFrom, sMailTech, NULL);
		} else
			iRet = seeSmtpConnect(0, sSmtp, sMailFrom, sMailTech);

      if (iRet < 0)
		{
         LogMsg("***** seeSmtpConnect fails: Check %s [Errno: %d]", sSmtp, iRet);
			seeRelease();
		}
   }

   if (bDebug)
      LogMsg("Mail Initialization complete ... %d", iRet);

   return iRet;
}

/******************************************************************************
 *
 *
 ******************************************************************************/

int sendMail(LPSTR pSubj, LPSTR pBody, LPSTR pMailTo, LPSTR pMailCc=NULL, LPSTR pMailBcc=NULL)
{
   int   iRet;

   iRet = seeVerifyFormat(pMailTo);
   if (iRet<0)
      LogMsg0("***** Mailto addr check failed: Check %s", pMailTo);
   else 
   {
      if (pMailCc && *pMailCc && pMailBcc && *pMailBcc)
         iRet = seeSendEmail(0, pMailTo, pMailCc, pMailBcc, pSubj, pBody, NULL);
      else if (pMailCc && *pMailCc)
         iRet = seeSendEmail(0, pMailTo, pMailCc, NULL, pSubj, pBody, NULL);
      else if (pMailBcc && *pMailBcc)
         iRet = seeSendEmail(0, pMailTo, NULL, pMailBcc, pSubj, pBody, NULL);
      else
         iRet = seeSendEmail(0, pMailTo, NULL, NULL, pSubj, pBody, NULL);

      if (iRet<0)
         LogMsg0("***** Unable to send mail to: %s", pMailTo);
      else
         LogMsg0("Send mail to: %s", pMailTo);
   }

   return iRet;
}

/******************************************************************************
 *
 *
 ******************************************************************************/

void closeMail()
{
	seeClose(0);
	seeRelease();
}

/******************************************************************************
 *
 *
 ******************************************************************************/

int TestMail(LPSTR pCfgFile)
{
	int	iRet;

	iRet = initMail(pCfgFile, true);
	if (iRet >=0)
	{
		iRet = sendMail("Test1", "Body 1", sMailTo);
		// Add CC
		iRet = sendMail("Test2", "Body 2", sMailTo, "SonyCC<sp_nguyen@yahoo.com>");
		// Add CC & BCC
		iRet = sendMail("Test3", "Body 3", sMailTo, "SonyCC<sp_nguyen@yahoo.com>", sMailTech);

		closeMail();
	}

	return iRet;
}