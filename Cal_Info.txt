Cal_Char:
"FeeParcel","NumPools","LandUseCategory","QualityClass","YearBuilt","BuildingSize","SqFTGarage","Heating","Cooling","NumBedrooms","NumFullBaths","NumHalfBaths","NumFireplaces","Asmt","HasSeptic","HasSewer","HasWell"
"002007001000","0       ",2,"      ",1900,.00,.00,"  ","  ",0,0,0,0,"002007001000",False,True,False

Cal_Asmt:
"Acres","Asmt","AsmtDescription","AsmtStatus","CurrentDocDate","CurrentDocNum","DwellingUnits","FeeParcel","LandUse1","LandUse2","NeighborhoodCode","SizeType","TaxabilityFull","TRA","Zoning1","Zoning2"
199.06,"002001001000","SEC 7 8 9 POR SEC 4 5 6 T7R16   POR 33 T8R16","A",2003-10-10 15:15:22.257000000,"1900R0000000",0,"002001001000","0090","0090","002 ","A","003","079043","        ","        "

Cal_Situs:
"Asmt","Street","StreetNum","StreetType","StreetDirection","SpaceApt","Community","Zip"
"002007001000","CABBAGE PATCH LOG RD","4710      "," ","  ","       ","TK"," "

Cal_Sales:
"Asmt","DocNum","EventDate","DocCode","TransferorName","TransfereeName","DocTranTax","IsGroupSale","GroupAsmt","TransferType"
"002002001000","1987R8400096",1987-11-25 00:00:00,"GD","","","0.00",False,"            ","  "

Cal_Roll:
"Asmt","FeeParcel","DwellingUnits","TRA","AsmtDescription","Zoning1","LandUse1","NeighborhoodCode","Acres","CurrentDocNum","CurrentDocDate","TaxabilityFull","AssesseeName","InCareOf","DBA","AddressStreet","AddressCity","AddressState","AddressZip","Land","Homesite","Structure","Growing","Fixtures","FixturesRealProperty","PPBusiness","PPMH","Address1","Address2","Address3","Address4"
"002001001000","002001001000","079043","SEC 7 8 9 POR SEC 4 5 6 T7R16   POR 33 T8R16","        ","0090","002 ",199.06,"1900R0000000",2003-10-10 15:15:22.257000000,"003","U S NATIONAL FOREST U S FOREST SERVICE","C/O CALAVERAS RANGER DISTRICT","","P O BOX 500","HATHAWAY PINES","CA","95233",0,0,0,0,0,0,0,0,"C/O CALAVERAS RANGER DISTRICT","P O BOX 500","HATHAWAY PINES CA 95233"," "

Cal_NameAddress:
"Asmt","AddressType","AssesseeName","InCareOf","DBA","AddressStreet","AddressCity","AddressState","AddressZip","Address1","Address2","Address3","Address4","IsFormattedAddress"
"            ","C","VALLEY SPRINGS INVESTORS LLC ETAL","","","","","  ","","","","","",True
"002001001000","C","U S NATIONAL FOREST U S FOREST SERVICE","C/O CALAVERAS RANGER DISTRICT","","P O BOX 500","HATHAWAY PINES","CA","95233","C/O CALAVERAS RANGER DISTRICT","P O BOX 500","HATHAWAY PINES CA 95233"," ",True

Cal_Exemption:
"AsmtStatus","Asmt","ExemptionCode","IsHomeowners","MaximumAmt"
"A","002018027000","E01",True,7000
"A","002019035000","E30",False,2

AGENCYCDCURRSEC_TR601
Asmt	TaxYear	RollChgNum	MapCategory	RollCategory	RollType	DestinationRoll	Installments	BillType	FeeParcel	OriginatingAsmt	XrefAsmt	Status	TRA	Taxability	Acres	SizeAcresFtType	date1	date2	date3	InterestType	UseCode	CurrentMarketLandValue	CurrentFixedImprValue	CurrentGrowingImprValue	CurrentStructuralImprValue	CurrentPersonalPropValue	CurrentPersonalPropMHValue	CurrentNetValue	BilledMarketLandValue	BilledFixedImprValue	BilledGrowingImprValue	BilledStructuralImprValue	BilledPersonalPropValue	BilledPersonalPropMHValue	BilledNetValue	Owner	Assessee	MailAddress1	MailAddress2	MailAddress3	MailAddress4	ZipMatch	BarCodeZip	ExemptionCode1	ExemptionAmt1	ExemptionCode2	ExemptionAmt2	ExemptionCode3	ExemptionAmt3	date4	date5	date6	TaxAmt1	TaxAmt2	PenAmt1	PenAmt2	Cost1	Cost2	date7	date8	PaidAmt1	PaidAmt2	date9	date10	date11	date12	CollectionNum1	CollectionNum2	UnsDelinqPenAmtPaid1	SecDelinqPenAmtPaid2	TotalFeesPaid1	TotalFeesPaid2	TotalFees	date13	date14	date15	ProrationFactor	ProrationPct	DaysToFiscalYearEnd	DaysOwned	date16	date17	Situs1	Situs2	AsmtRollYear	ParcelDescription	BillCommentsLine1	BillCommentsLine2	DefaultNum	date18	date19	SecDelinqFiscalYear	PriorTaxPaid1	PenInterestCode	FourPayPlanNum	ExistsLien	ExistsCortac	ExistsCountyCortac	ExistsBankruptcy	ExistsRefund	ExistsDelinquentVessel	ExistsNotes	ExistsCriticalNote	ExistsRollChg	IsPenChrgCanceled1	IsPenChrgCanceled2	IsPersonalPropertyPenalty	IsAgPreserve	IsCarryOver1stPaid	IsCarryOverRecord	IsFailureToFile	IsOwnershipPenalty	IsEligibleFor4Pay	IsNotice4Sent	IsPartPay	IsFormattedAddress	IsAddressConfidential	SuplCnt	date20	date21	IsPen1Refund	IsPen2Refund	IsDelinqPenRefund	IsRefundAuthorized	IsNoDischarge	IsAppealPending	OtherFeesPaid	FourPayReason	date22	IsOnlyFirstPaid	IsAllPaid	date23	UserID	