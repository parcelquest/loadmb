/**************************************************************************
 *
 * Options:
 *    -CSIE -L -Xs|-Ms -Xa -Xl (load lien)
 *    -CSIE -U -Xsi -Xa (load update)
 *
 * Notes:
 *    - Old DocNum (12-byte length) is re-sequencing and won't match with DataTree.  
 *    - New DocNum YYYYR123456 (11-byte length) is OK
 *    - Update APN format in tblCounty
 *    - Update tblCity (Run LoadCity SIE).  Change INI file for each server update
 *    - Update lookup table (Run LoadLkup) change cfg file for each server update
 *    - Update tblDpf set LookupTable='Fire Place' where CountyCode='SIE' and SqlFieldName='FIRE_PL'
 *
 * Revision
 * 02/18/2019 18.10.3   First version - Copy from MergeGle.cpp
 * 03/05/2019 18.10.5   Release to production
 * 08/19/2019 19.1.4    Fine tune Sie_Load_LDR() for first time usage in 2019 LDR.
 * 09/25/2019 19.3.1    Loading using default HdrRow from INI file.
 * 11/06/2019 19.5.1    Bug fix in loadSie().  iPrevApnLen not initialize crashed program.
 * 07/20/2020 20.2.1    Clean up code in Sie_MergeSitus() and modify Sie_Load_LDR()
 *                      to remove unneeded code.
 * 11/01/2020 20.4.2    Modify Sie_MergeRoll() & Sie_MergeMisc() to populate default PQZoning.
 * 06/24/2021 21.0.1    Modify Sie_MergeSitus() to replace Abbr2Code() with Abbr2CZ() to populate known zipcode.
 * 07/29/2021 21.1.5    Modify Sie_MergeLien() to skip unsecured records.  Bug fix in Sie_MergeMisc().
 * 08/26/2021 21.2.0    Default bUseConfSalePrice=false so we don't use confidential sale price.
 * 07/15/2022 22.0.2    Remove unused code in Sie_Load_LDR()
 * 02/19/2023 22.6.9    Modify to keep APN length as defined in CountyInfo.csv, not as PrevApnLength
 * 03/13/2023 22.6.11   Fix sort control statement in Sie_ConvStdChar().
 * 03/22/2024 23.7.3.1  Modify Sie_MergeStdChar() to populate LOT_ACRES/LOtSQFT
 * 07/24/2024 24.0.3    Modify Sie_MergeMAdr() to merge M_Zip into M_City if length of M_Zip != 5.
 *                      Modify Sie_MergeLien() to add ExeType and check for misalignment record.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"

#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "CharRec.h"
#include "PQ.h"
#include "MergeSie.h"
#include "Situs.h"
#include "Tax.h"

/******************************** Sie_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *
 *****************************************************************************/

void Sie_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iRet, iVet;
   char  acOwner[64], acName1[64], *pTmp;
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

#ifdef _DEBUG
   // MC DONALD JOHN E &   % MC DONALD ELIZABETH ETAL
   //if (!memcmp(pOutbuf, "044060021000", 9))
   //   iTmp = 0;
#endif

   // Remove multiple spaces
   strcpy(acName1, pNames);
   quoteRem(acName1);
   blankRem(acName1);

   // Update vesting
   iVet = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST);

   remChar(acName1, ',');
   strcpy(acOwner, acName1);

   // Remove ETAL or DVA
   if (pTmp = strrchr(acName1, ' '))
   {
      if (!strcmp(pTmp, " ETAL") || !strcmp(pTmp, " DVA") )
         *pTmp = 0;
      else if (pTmp = strstr(acName1, " ET AL"))
         *pTmp = 0;
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = -1;
   if (strchr(acName1, ' '))
   {
      if ((pTmp = strstr(acName1, " JT")) || (pTmp = strstr(acName1, " CP")))
      {
         if (!*(pTmp+3))
         {
            if (bDebug)
               LogMsg("--- %.12s: %s", pOutbuf, acName1);
            *pTmp = 0;
         }
      } if (pTmp = strstr(acName1, " SS"))
      {
         if (bDebug)
            LogMsg("--- %.12s: %s", pOutbuf, acName1);
         *pTmp = 0;
      }


      iRet = splitOwner(acName1, &myOwner, 3);
      if (myOwner.acVest[0] > ' ' && *(pOutbuf+OFF_VEST) == ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // If name is swapable, use it
      if (iRet >= 0)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   }

   if (iRet == -1)
   {
      // Couldn't split names
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME1);
   }

   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);
}


/******************************** Sie_MergeOwner *****************************
 *
 * The CareOf field may contain Owner2.  Check for & at the end of Assessee
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sie_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf, char *pDba, char *pMAdr1)
{
   int   iTmp, iRet, iACnt;
   char  acOwner[64], acName1[64], acName2[64], *pTmp;
   OWNER myOwner;
   boolean bUseCareOf=false, bHasName2=false;

   // Clear output buffer if needed
   removeNames(pOutbuf, true, true);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001140007000", 10))
   //   iTmp = 0;
#endif

   // Init names
   iACnt=0;
   strcpy(acName1, pNames);
   remChar(acName1, '*');
   blankRem(acName1);
   acName2[0] = 0;

   // Check DBA
   if (*pDba > ' ')
   {
      pTmp = pDba;
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   }

   // Check CareOf
   if (pCareOf && *pCareOf > ' ')
   {
      if (*pCareOf == '&')
      {
         bHasName2 = true;
         strcpy(acName2, pCareOf+2);
      } else
      {
         if (*pCareOf == '%')
            updateCareOf(pOutbuf, pCareOf+2, strlen(pCareOf));
         else
            updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
      }
   }

   // Check for Name2
   if (!bHasName2 && pMAdr1 && *pMAdr1 == '&')
   {
      bHasName2 = true;
      strcpy(acName2, pMAdr1+2);
   }

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST);
   if (!iTmp && acName2[0] > ' ')
      iTmp = updateVesting(myCounty.acCntyCode, acName2, pOutbuf+OFF_VEST);

   if (bHasName2 && !iTmp)
   {
      iTmp = MergeName1(acName1, acName2, acOwner);
      if (!iTmp)
         strcpy(acOwner, acName1);
   } else
      strcpy(acOwner, acName1);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = -1;
   if (strchr(acOwner, ' '))
   {
      if ((pTmp = strstr(acOwner, " JT")) || (pTmp = strstr(acOwner, " CP")))
      {
         if (!*(pTmp+3))
         {
            if (bDebug)
               LogMsg("--- %.12s: %s", pOutbuf, acOwner);
            *pTmp = 0;
         }
      } if (pTmp = strstr(acOwner, " SS"))
      {
         if (bDebug)
            LogMsg("--- %.12s: %s", pOutbuf, acOwner);
         *pTmp = 0;
      }

      iRet = splitOwner(acOwner, &myOwner, 5);

      // If name is swapable, use it
      if (iRet >= 0)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   }

   if (iRet == -1)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);

   // Save Name2 if exist
   remChar(acName2, ',');
   vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);

   // Save Name1
   remChar(acName1, ',');
   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);
}

/******************************** Sie_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Sie_MergeMAdr(char *pOutbuf)
{
   char    acTmp[256], acAddr1[64], *pTmp;
   int     iTmp;
   ADR_REC sMailAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0050200020", 10))
   //   iTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, false, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }

   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
      sMailAdr.strName[SIZ_M_STREET] = 0;
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], SIZ_M_CITY);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         // Take out '-' in zipcode if present
         strcpy(acTmp, apTokens[MB_ROLL_M_ZIP]);
         iTmp = strlen(acTmp);
         if (acTmp[5] == '-' && iTmp == 10)
         {
            strcpy(&acTmp[5], &acTmp[6]);
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         } else if (iTmp > 9)
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         else
            vmemcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
      } else
         iTmp = 0;

      if (iTmp == 9)
         sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP], apTokens[MB_ROLL_M_ZIP]+5);
      else
         sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

void Sie_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2, *pDba;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "005070005000", 9) || !memcmp(pOutbuf, "005230043000", 9) )
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   pDba = p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }

      if (!_memicmp(pLine1, "DBA ", 4) )
         pDba = pLine1;
      else if (!_memicmp(pLine2, "DBA ", 4))
         pDba = pLine2;
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
         {
            p1 = pLine2;
            p0 = pLine1;
         }
      }

      if (!_memicmp(pLine1, "DBA ", 4) )
         pDba = pLine1;
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
   }

   // Update DBA
   if (pDba)
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Special case
      int iRoadChg = replStr(acAddr1, "CO RD ", "COUNTY ROAD ");
      if (iRoadChg > 0)
         LogMsg("--> Rename CO RD to %s [%.12s]", acAddr1, pOutbuf);

      // Remove blank
      iTmp = blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, iTmp);

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
            vmemcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, SIZ_M_STR_SUB);
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));

         if (!iRoadChg)
         {
            if (sMailAdr.strSfx[0] > '0')
               memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
            if (sMailAdr.Unit[0] > ' ')
               memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
            if (pTmp = strstr(sMailAdr.strName, " PMB"))
               *pTmp = 0;
            vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
         } else
         {
            pTmp = strstr(acAddr1, "COUNTY");
            vmemcpy(pOutbuf+OFF_M_STREET, pTmp, SIZ_M_STREET);
         }
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   strcpy(acAddr2, p2);
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      // Check for foreign address
      if (sMailAdr.Zip[0] > ' ' && strlen(sMailAdr.Zip) != 5)
      {
         sprintf(acTmp, "%s %s %s", sMailAdr.City, sMailAdr.State, sMailAdr.Zip);
         iTmp = blankRem(acTmp);
         vmemcpy(pOutbuf+OFF_M_CITY, acTmp, SIZ_M_CITY, iTmp);
      } else
      {
         vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
         if (sMailAdr.State[0] > ' ')
            memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
         vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
         if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
            memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
      }
   }
}


/******************************** Sie_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Sie_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acAddr2[64], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do {
         pRec = fgets(acRec, 512, fdSitus);
      } while (pRec && (*pRec < ' ' || *pRec > '9'));
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      pTmp = pRec;
      if (*pTmp == '"')
         pTmp++;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   replNull(pRec, ' ');
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   else
      iRet = ParseStringIQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   if (*apTokens[MB_SITUS_STRNAME] == ' ' && *apTokens[MB_SITUS_STRNUM] == ' ')
      return 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001030016000", 12))
   //   iTmp = 0;
#endif

   // Merge data
   acAddr1[0] = 0;
   removeSitus(pOutbuf);
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);

   if (lTmp > 0)
   {
      // Save original StrNum
      vmemcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], SIZ_S_HSENO);

      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, SIZ_S_STR_SUB);

      strcpy(acAddr1, apTokens[MB_SITUS_STRNUM]);
      if (*apTokens[MB_SITUS_STRDIR] > ' ' && isDir(apTokens[MB_SITUS_STRDIR]))
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         vmemcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], SIZ_S_DIR);
      }
   }

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      if (acAddr1[0] > ' ')
         strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);
      vmemcpy(pOutbuf+OFF_S_STREET, apTokens[MB_SITUS_STRNAME], SIZ_S_STREET, strlen(apTokens[MB_SITUS_STRNAME]));

      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
      {
         Sfx2Code(acTmp, acCode);
         vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      } else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         iBadSuffix++;
      }
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apTokens[MB_SITUS_STRNAME]);
      vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      vmemcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, SIZ_S_SUFF);

      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNO);
   }

   iTmp = blankRem(acAddr1);
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D, iTmp);
   acAddr2[0] = 0;

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2CZ(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr2, acCode, pOutbuf);
      //Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr2, pOutbuf);
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr2[0] > ' ')
      {
         // Zip
         lTmp = atol(apTokens[MB_SITUS_ZIP]);
         if (lTmp > 10000 && lTmp < 99999)
         {
            vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], SIZ_S_ZIP);
            sprintf(acTmp, "%s CA %.5s", acAddr2, apTokens[MB_SITUS_ZIP]);
         } else if (acCode[0] == '9')
         {
            vmemcpy(pOutbuf+OFF_S_ZIP, acCode, SIZ_S_ZIP);
            sprintf(acTmp, "%s CA %.5s", acAddr2, acCode);
         } else
            sprintf(acTmp, "%s CA ", acAddr2);

         iTmp = blankRem(acTmp);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
      }
   }

   // Get city/zip from GIS extract
   //if (fdCity && *(pOutbuf+OFF_S_CITY) == ' ')
   //{
   //   char acZip[16], acCity[32];

   //   iTmp = getCityZip(pOutbuf, acCity, acZip, 9);
   //   if (!iTmp)
   //   {
   //      City2Code(acCity, acCode, pOutbuf);
   //      memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
   //      memcpy(pOutbuf+OFF_S_ZIP, acZip, strlen(acZip));
   //      sprintf(acAddr2, "%s CA %s", acCity, acZip);
   //      lUseGis++;
   //   }
   //}

   //iTmp = blankRem(acAddr2, SIZ_S_CTY_ST_D);
   //memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int Sie_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "007280027000", 9))
   //   acTmp[0] = 0;
#endif
   //
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   if (sSitusAdr.lStrNum > 0)
   {
      char *pTmp = strchr(acAddr1, ' ');
      *pTmp = 0;

      // Save original StrNum
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
      memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   }

   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/***************************** Sie_MergeStdChar ******************************
 *
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the one that has value.
 *
 *****************************************************************************/

int Sie_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft;
   int      iLoop, iBeds, iFBath, iHBath, iBldgNum, iTmp;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001051007", 9))
   //   iTmp = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;

   // Yrblt
   memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // YrEff
   iTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (iTmp > 1600 && iTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // LotSqft
   long lLotSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lLotSqft > 100)
   {
      memcpy(pOutbuf+OFF_LOT_SQFT, pChar->LotSqft, SIZ_LOT_SQFT);
      memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_ACRES);
   } 

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   } else
      memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);

   // Parking type
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // ParkSpace
   iTmp = atoin(pChar->ParkSpace, SIZ_CHAR_SIZE4);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d      ", iTmp);
      memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
   } else
      memset(pOutbuf+OFF_PARK_SPACE, ' ', SIZ_PARK_SPACE);

   // Heating
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];

   // Cooling
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

   // Rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   } else
      memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

   // Units
   iTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (pChar->Units[0] > ' ')
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   } else
      memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

   // Stories
   if (pChar->Stories[0] > ' ')
      memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
   else
      memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

   // Fireplace
   if (pChar->Fireplace[0] > ' ')
      memcpy(pOutbuf+OFF_FIRE_PL, pChar->Fireplace, SIZ_FIRE_PL);
   else
      memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);

   // HasSeptic or HasSewer
   *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Pools
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // BldgNum
   iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************** Sie_MergeChar ******************************
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

int Sie_MergeChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft;
   int      iLoop, iBeds, iFBath, iHBath, iFp;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;

   // Yrblt
   memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   } else if (*(pOutbuf+OFF_PARK_TYPE) > ' ')
   {
      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
   }

   // Parking type
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // Heating
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];

   // Cooling
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Fireplace
   iFp = atoin(pChar->Fireplace, SIZ_CHAR_SIZE2);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   } else
      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);

   // HasSeptic or HasSewer
   *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Pools
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   lCharMatch++;

   iLoop = 0;
   do
   {
      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         break;      // EOF
      }
      iLoop++;
   } while (!memcmp(pOutbuf, pRec, iApnLen));

   if (iLoop > 1)
   {
      int iTmp;

      iTmp = sprintf(acTmp, "%d", iLoop);
      memcpy(pOutbuf+OFF_UNITS, acTmp, iTmp);
   }

   return 0;
}

/********************************* Sie_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sie_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Remove null
   replNull(pRollRec, ' ');

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Sie_MergeRoll(): bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN starts with 100-999 except 910 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp > 99 && iTmp != 910))
      return 99;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // PREV_APN - Only apply to physical parcels
      if (memcmp(pOutbuf, "100", 3) < 0)
         vmemcpy(pOutbuf+OFF_PREV_APN, apTokens[iApnFld], 10);

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "46SIE", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   } else
   {
      memcpy(pOutbuf+OFF_CO_NUM, "46SIE", 5);

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   acTmp[SIZ_USE_CO] = 0;
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Owner
   try {
      Sie_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER], apTokens[MB_ROLL_CAREOF], apTokens[MB_ROLL_DBA], apTokens[MB_ROLL_M_ADDR1]);
   } catch(...) {
      LogMsg("***** Exeception occured in Sie_MergeOwner()");
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "990018287", 9) )
   //   iTmp = 0;
#endif
   // Mailing
   try {
      Sie_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Sie_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   // Recorded Doc
   iLen = iTrim(apTokens[MB_ROLL_DOCNUM]);
   pTmp = apTokens[MB_ROLL_DOCNUM];
   if (*(pTmp+4) == 'R' && iLen == 11)
   {
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp && !memcmp(acTmp, apTokens[MB_ROLL_DOCNUM], 4))
      {
         iTmp = atol(acTmp);
         if (iTmp > lLastRecDate)
            lLastRecDate = iTmp;
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], strlen(apTokens[MB_ROLL_DOCNUM]));
      }
   }

   return 0;
}

/********************************** Sie_Load_Roll *****************************
 *
 *
 *
 ******************************************************************************/

int Sie_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A)");
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"Z\") DEL(124) ");
   else
      lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) ");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) ");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) ");
   fdTax = fopen(acTmpFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (acRollRec[0] > '9' || acRollRec[0] < '0');

   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "007280027000", 9))
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Sie_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Sie_MergeSitus(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Sie_MergeStdChar(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acBuf, 0);

            iRollUpd++;
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         // Create new R01 record
         iRet = Sie_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

         if (!iRet)
         {
            if (bDebug)
               LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

            // Merge Situs
            if (fdSitus)
               lRet = Sie_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe2(acRec, 0);

            // Merge Char
            if (fdChar)
               lRet = Sie_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acRec, iHdrRows);

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      // Create new R01 record
      iRet = Sie_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Merge Situs
         if (fdSitus)
            lRet = Sie_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe2(acRec, 0);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Sie_MergeStdChar(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, iHdrRows);

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u\n", lLastRecDate);

   return 0;
}

/**************************** Sie_ConvStdChar ********************************
 *
 * 07/21/2017 Fix Garage Type
 * 08/02/2017 Remove TotalRooms since this field is not keyed.
 *
 *****************************************************************************/

int Sie_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   double   dTmp;
   STDCHAR  myCharRec;

   LogMsgD("Sie_ConvStdChar - Converting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 4096, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);

      if (!pRec)
         break;

      replNull(acBuf, ' ');
      _strupr(acBuf);

      // Parse string
      if (cDelim == '|')
         iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

      if (iFldCnt < SIE_CHAR_COLS)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[SIE_CHAR_ASMT], strlen(apTokens[SIE_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[SIE_CHAR_FEEPARCEL], strlen(apTokens[SIE_CHAR_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[SIE_CHAR_ASMT], acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      // Bldg#
      iTmp = atoi(apTokens[SIE_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // BldgSize
      int iBldgSize = atoi(apTokens[SIE_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Stories/NumFloors 1, 1.5, 2, 2.5, 3
      dTmp = atof(apTokens[SIE_CHAR_STORIESCNT]);
      if (dTmp > 0)
      {
         if (dTmp < 10.0)
            iRet = sprintf(acTmp, "%.1f", dTmp);
         else
            iRet = sprintf(acTmp, "%.1f", dTmp/10.0);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[SIE_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      }

      // Total rooms
      iTmp = atoi(apTokens[SIE_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[SIE_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[SIE_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
         memcpy(myCharRec.Bath_4Q, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[SIE_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
         memcpy(myCharRec.Bath_2Q, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "01152002", 9))
      //   iRet = 0;
#endif
      // Pool - No data
      //iTmp = blankRem(apTokens[SIE_CHAR_POOLSPA]);
      //if (*apTokens[SIE_CHAR_POOLSPA] > ' ')
      //{
      //   pRec = findXlatCode(apTokens[SIE_CHAR_POOLSPA], &asPool[0]);
      //   if (pRec)
      //      myCharRec.Pool[0] = *pRec;
      //}

      // QualityClass
      if (*apTokens[SIE_CHAR_QUALITYCLASS] == 'A')
         myCharRec.BldgQual = 'A';
      else if (*apTokens[SIE_CHAR_QUALITYCLASS] == 'E')
         myCharRec.BldgQual = 'E';
      else if (*apTokens[SIE_CHAR_QUALITYCLASS] == 'F')
         myCharRec.BldgQual = 'F';
      else if (*apTokens[SIE_CHAR_QUALITYCLASS] == 'G')
         myCharRec.BldgQual = 'G';
      else if (*apTokens[SIE_CHAR_QUALITYCLASS] == 'L')
         myCharRec.BldgQual = 'P';
      else if (*apTokens[SIE_CHAR_QUALITYCLASS] == 'V')
         myCharRec.BldgQual = 'V';
      else if (*apTokens[SIE_CHAR_QUALITYCLASS] > ' ' && memcmp(apTokens[SIE_CHAR_QUALITYCLASS], "MH", 2))
         LogMsg("*** Bad Quality: %s", apTokens[SIE_CHAR_QUALITYCLASS]);

      // YrBlt
      int iYrBlt = atoi(apTokens[SIE_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[SIE_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[SIE_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         if (iAttGar > 100)
         {
            myCharRec.ParkType[0] = 'I';
            if (iAttGar > 200 && iAttGar < 300)
               myCharRec.ParkSpace[0] = '1';
            else if (iAttGar > 400 && iAttGar < 600)
               myCharRec.ParkSpace[0] = '2';
            else if (iAttGar > 600 && iAttGar < 700)
               myCharRec.ParkSpace[0] = '3';
         }
      }

      // Detached SF
      int iDetGar = atoi(apTokens[SIE_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (iAttGar < 100)
         {
            memcpy(myCharRec.GarSqft, acTmp, iRet);
            myCharRec.ParkType[0] = 'L';
         }
      }

      // Carport
      int iCarport = atoi(apTokens[SIE_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
      }

      // LotSqft 
      ULONG lLotSqft = atol(apTokens[SIE_CHAR_LOTSQFT]);
      if (lLotSqft > 100)
      {
         iRet = sprintf(acTmp, "%u", lLotSqft);
         memcpy(myCharRec.LotSqft, acTmp, iRet);
         iRet = sprintf(acTmp, "%u", (ULONG)(lLotSqft*SQFT_MF_1000+0.5));
         memcpy(myCharRec.LotAcre, acTmp, iRet);
      }

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[SIE_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCodeA(apTokens[SIE_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      }

      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[SIE_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[SIE_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[SIE_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[SIE_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // FirePlace
      if (*(apTokens[SIE_CHAR_FIREPLACE]) > '0')
      {
         blankRem(apTokens[SIE_CHAR_FIREPLACE]);
         pRec = findXlatCodeA(apTokens[SIE_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
         {
            myCharRec.Fireplace[0] = *pRec;
            if (*pRec > '9')
               myCharRec.Misc.sExtra.FirePlaceType[0] = *pRec;
         }
      }

      // Water
      if (*apTokens[SIE_CHAR_HASWELL] > '0')
      {
         myCharRec.HasWater = 'W';
         myCharRec.HasWell = 'Y';
      }

      // PatioSf
      iTmp = atoi(apTokens[SIE_CHAR_PATIOSF]);
      if (iTmp > 10)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D,53,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/*************************** Sie_ConvertApnRoll *****************************
 *
 * Convert APN format from Cres to MB.  Keep old APN in PREV_APN.
 *
 ****************************************************************************/

int Sie_ConvertApnRoll(char *pInfile, char *pOutfile, int iRecordLen)
{
   FILE *fdIn, *fdOut;
   int   iTmp, iCnt=0;
   char  acBuf[2048], acTmp[32], acApn[32];

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "rb");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "wb");

   fread(acBuf, 1, iRecordLen, fdIn);
   fwrite(acBuf, 1, iRecordLen, fdOut);

   while (!feof(fdIn))
   {
      iTmp = fread(acBuf, 1, iRecordLen, fdIn);
      if (iTmp < iRecordLen)
         break;

      if (acBuf[OFF_STATUS] != 'R' && memcmp(acBuf, "050", 3) < 0)
      {
         // Unsecured or mobilehome
         if (acBuf[9] != '0')
            LogMsg("??? %.10s", acBuf);

         // Format new APN - ignore last digit of old APN
         sprintf(acApn, "%.10s00", acBuf);

         // Save old APN to previous APN
         memcpy(&acBuf[OFF_PREV_APN], &acBuf[0], 10);

         sprintf(acTmp, "%.3s-%.3s-%.3s-%.3s", acApn, &acApn[3], &acApn[6], &acApn[9]);
         memcpy(&acBuf[OFF_APN_D], acTmp, 15);

         memcpy(acBuf, acApn, 12);
         fwrite(acBuf, 1, iRecordLen, fdOut);
      } else if (acBuf[OFF_STATUS] == 'R')
         LogMsg("Drop retired parcel: %.12s", acBuf);
      else
         LogMsg("Drop old parcel: %.12s", acBuf);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert Roll APN completed with %d records", iCnt);

   return 0;
}

void Sie_ConvDocNum(char *pDocNum, char *pDocDate)
{
   int   iTmp;

   if (*pDocNum == ' ' || *(pDocNum+4) > '9')
      return;

   iTmp = atoin(pDocNum, 7);
   if (iTmp > 99999)
      return;

   //iTmp = sprintf(sTmp, "%.4s-%.4d", pDocDate, iTmp);
   //memcpy(pDocNum, sTmp, iTmp);
}

/******************************************************************************
 *
 * Convert to new APN format and keep only records up to 01/31/2018.
 * All new transaction can be taken from new sale file with new DocNum format.
 *
 ******************************************************************************/

int Sie_ConvertApnSale(char *pInfile, char *pOutfile)
{
   FILE *fdIn, *fdOut;
   int   iRecDate, iTmp, iCnt=0, iOut=0;
   char  acBuf[1024], acApn[32], *pBuf;
   SCSAL_REC *pSale = (SCSAL_REC *)&acBuf[0];

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");

   while (!feof(fdIn))
   {
      pBuf = fgets(acBuf, 1024, fdIn);
      if (!pBuf)
         break;

      // Check RecDate
      iRecDate = atoin(pSale->DocDate, 8);
      if (iRecDate < 20180201)
      {
         // Format new APN
         iTmp = sprintf(acApn, "%.10s00", acBuf);
         memcpy(acBuf, acApn, iTmp);

         // Reset sale code
         iTmp = atoin(pSale->SalePrice, SALE_SIZ_SALEPRICE);
         if (!iTmp)
            pSale->SaleCode[0] = ' ';

         fputs(acBuf, fdOut);
         iOut++;
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Number of records processed %d", iCnt);
   LogMsg("Number of output records    %d\n", iOut);

   return 0;
}

/***************************** MB_CreateSCSale *******************************
 *
 * Extract sale data from ???_Sales.csv and output to ???_SALE.DAT
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * DateFmt:
 *    0 : Default. Auto format by checking input date
 *    MM_DD_YYYY_1 (MON)
 *    YYYY_MM_DD   (HUM)
 *
 * DocTypeFmt:
 *    0 : Default. Set DocType=1 (GD) if DocCode=1 or sale price > 0
 *    1 : AMA, BUT, MAD, SON, SBT
 *    2 : PLA
 *    3 : SHA, STA
 *
 * DocNumFmt:
 *    0 : Default. Copy DocNum as is
 *    1 : Format DocNum second part of DocNum to 7 digits (i.e. 2010R1234 = 2010R0001234)
 *    2 : Format DocNum second part of DocNum to n digits (i.e. 2010R0034 = 2010R34) (COL)
 *    3 : Remove all nonnumeric after 5th character and format to 6 digits (SON)
 *    4 : Format to yyyyR9999999 if R is in pos 4 or 5 of original DocNum (SBT)
 *    5 : TEH
 *    6 : SIE - format as yyyy9999 (i.e. 2012-0001 = 20120001)
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Sie_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp, iLen, iDrop=0;
   double   dTmp;
   long     lOut=0, lCnt=0, lPrice, lTmp;

   LogMsg("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Remove null char
      replNull(acRec);

      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "052202011000", 9))
      //   iTmp = 0;
#endif

      // DocDate
      pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      if (pTmp)
      {
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
      } else
         lTmp = 0;

      // Only take records with sale price or sale date is newer than 20180131
      if (lTmp < 20180201)
      {
         iDrop++;
         continue;
      }
      if (lTmp > lLastRecDate && lTmp < lToday)
         lLastRecDate = lTmp;

      // Docnum
      myTrim(apTokens[MB_SALES_DOCNUM]);
      iLen = strlen(apTokens[MB_SALES_DOCNUM]);
      pTmp = apTokens[MB_SALES_DOCNUM];
      if (*(pTmp+4) == 'R')
      {
         if (iLen == 11)
            memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], iLen);
         else if (!memcmp(apTokens[MB_SALES_DOCNUM], "2018R16823", iLen))
            memcpy(SaleRec.DocNum, "2018R168823", 11);
         else
            LogMsg("*** Bad DocNum: %s %s [%s]", apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_DOCDATE], apTokens[MB_SALES_ASMT]);
      } else if (iLen == 10 && isdigit(*(pTmp+4)))
      {
         iLen = sprintf(acTmp, "%.4sR%s", pTmp, pTmp+4);
         memcpy(SaleRec.DocNum, acTmp, iLen);
      } else
      {
         LogMsg("*** Drop DocNum: %s %s [%s]", apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_DOCDATE], apTokens[MB_SALES_ASMT]);
         iDrop++;
         continue;
      }

      // Confirmed sale price
      dTmp = atof(apTokens[MB_SALES_PRICE]);
      if (dTmp > 0.0)
      {
         lPrice = (long)((dTmp*100)/100.0);
         iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      } 

      // Do not use confirmed sale price
      if (!bUseConfSalePrice)
         lPrice = 0;

      // Tax
      dTmp = atof(apTokens[MB_SALES_TAXAMT]);
      if (dTmp > 0.0)
      {
         // Save DocTax
         iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);

         // Check for bad DocTax
         if (lPrice == 0)
         {
            // Calculate sale price
            lPrice = (long)(dTmp * SALE_FACTOR);
         }

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Ignore sale price.",
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE]);
               lPrice = 0;
            }
         }
      } 

      // Ignore sale price if less than 1000
      if (lPrice > 100)
      {
         sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
         memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);
      }

      // Doc code - accept following code only
      int iDocCode = 0;
      if (isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         if (pDocTbl)
         {
            iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
            if (iTmp >= 0)
            {
               memcpy(SaleRec.DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
               SaleRec.NoneSale_Flg = pDocTbl[iTmp].flag;
            } else if (bDebug)
               LogMsg("*** Unknown DocCode: %s", apTokens[MB_SALES_DOCCODE]);
         }
      } else if (lPrice > 1000)
      {
         SaleRec.DocType[0] = '1';
      }

      // Save original DocCode
      vmemcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      } else if (*apTokens[MB_SALES_XFERTYPE] == 'F' && lPrice > 0)
         SaleRec.SaleCode[0] = 'F';

      // Seller
      blankRem(apTokens[MB_SALES_SELLER]);
      vmemcpy(SaleRec.Seller1, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);

      // Buyer
      blankRem(apTokens[MB_SALES_BUYER]);
      vmemcpy(SaleRec.Name1, apTokens[MB_SALES_BUYER], SALE_SIZ_BUYER);

      lOut++;
      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);
   }

   fclose(fdSale);
   fclose(fdOut);

   // Sort output file
   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(27,2,C,EQ,\"  \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -2;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            // Save old cumsale file
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);
            iTmp = rename(acCSalFile, acTmpFile);

            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -2;
      } else
         iTmp = rename(acOutFile, acCSalFile);

      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acCSalFile, acTmpFile);
      }
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp == -2)
      LogMsg("***** Error sorting output file");
   else if (iTmp)
      LogMsg("***** Error renaming to %s", acCSalFile);

   LogMsg("Number of Sale records processed: %d", lCnt);
   LogMsg("                       extracted: %d", lOut);
   LogMsg("                         dropped: %d", iDrop);
   LogMsg("                          output: %d", lTmp);
   LogMsg("         Latetest recording date: %d.\n", lLastRecDate);
   return iTmp;
}

/********************************* Sie_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sie_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L3_ASMT], iTokens);
      return -1;
   } else if (iTokens > L3_FLDS)
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L3_ASMT], iTokens);

   // Ignore APN starts with 100-999 except 910 (MH)
   iTmp = atoin(apTokens[L3_ASMT], 3);
   if (!iTmp || (iTmp > 99 && iTmp != 910))
      return 99;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "46SIE", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > '0')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SIE_Exemption);

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (iTokens > L3_ISAGPRESERVE)
   {
      if (*apTokens[L3_ISAGPRESERVE] == '1')
         *(pOutbuf+OFF_AG_PRE) = 'Y';
   }

   // Owner
   Sie_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Mailing
   Sie_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*(apTokens[L3_CURRENTDOCNUM]+4) == 'R')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   return 0;
}

/******************************** Sie_MergeMisc ******************************
 *
 * Copy various fields from roll file since LDR file doesn't have them.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Sie_MergeMisc(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     *apItems[64], *pTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 1024, fdRoll);
   }
         
   pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Zone rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 1024, fdRoll);
         lZoneSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   replNull(acRec);
   iRet = ParseStringIQ(acRec, cDelim, 64, apItems);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input Zone record for APN=%s", apItems[0]);
         iRet = -1;
      }

      return iRet;
   }

   // Merge data
   if (*apItems[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apItems[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apItems[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "017140001000", 9))
   //   iTmp = 0;
#endif

   // Update Transfer
   iTmp = atol(apItems[MB_ROLL_DOCDATE]);
   char acTmp[32];
   if (*(apItems[MB_ROLL_DOCNUM]+4) == 'R' && dateConversion(apItems[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1))
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, apItems[MB_ROLL_DOCNUM], strlen(apItems[MB_ROLL_DOCNUM]));
   }
                
   // Acres

   // Legal
   //updateLegal(pOutbuf, apItems[MB_ROLL_LEGAL]);

   lZoneMatch++;

   // Get next record
   pRec = fgets(acRec, 1024, fdRoll);

   return 0;
}

/******************************** Sie_Load_LDR ******************************
 *
 *
 ****************************************************************************/

int Sie_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int      iRet, lRet=0, lCnt=0, lTmp;

   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   HANDLE   fhOut;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
      if (!iRet)
         return -1;
   }

   // Open LDR file
   LogMsg("Open LDR file %s", acRollFile);
   fdLDR = fopen(acRollFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acRollFile);
      return -1;
   }

   // Use update roll to populate Taxability, Prop8, and Zoning
   GetIniString(myCounty.acCntyCode, "RollFile", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg("Open update roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Value file
   //sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (!_access(acTmpFile, 0))
   //{
   //   LogMsg("Open Lien file %s", acTmpFile);
   //   fdLienExt = fopen(acTmpFile, "r");
   //   if (fdLienExt == NULL)
   //   {
   //      LogMsg("***** Error opening lien file: %s\n", acTmpFile);
   //      return -7;
   //   }
   //} else
   //   fdLienExt = NULL;

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(1,13,C,A) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!feof(fdLDR))
   {
      // Create new R01 record
      iRet = Sie_MergeLien(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         //if (fdLienExt)
         //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Situs
         if (fdSitus)
            lRet = Sie_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Sie_MergeStdChar(acBuf);

         // Merge misc fields from roll update
         if (fdRoll)
            lRet = Sie_MergeMisc(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdLDR)
      fclose(fdLDR);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Zone matched:     %u", lZoneMatch);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Zone skiped:      %u", lZoneSkip);
   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/*********************************** loadSie ********************************
 *
 * Options:
 *
 *    -CSIE -U -Xsi -T [-Xa] -O   (Normal update)
 *    -CSIE -L -Xl -Ms|Xsi [-Xa] -O (LDR)
 *
 ****************************************************************************/

int loadSie(int iSkip)
{
   int   iRet=0, iPrevApnLen;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;
   iApnLen = myCounty.iApnLen;

   //char sInfile[_MAX_PATH], sOutfile[_MAX_PATH];
   //sprintf(sInfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   //sprintf(sOutfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   //iRet = Sie_ConvertApnRoll(sInfile, sOutfile, 1900);
   //sprintf(sInfile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Org");
   //sprintf(sOutfile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   //iRet = Sie_ConvertApnSale(sInfile, sOutfile);
   //strcpy(sInfile, "H:\\CO_PROD\\SSIE_CD\\raw\\teh_cityzip.csv");
   //strcpy(sOutfile, "H:\\CO_PROD\\SSIE_CD\\raw\\teh_cityzip.srt");
   //iRet = Sie_ConvertApnCity(sInfile, sOutfile);

   if (iLoadTax == TAX_LOADING)
   {
      // Load tax base
      iRet = MB_Load_TaxBase(bTaxImport, false, 2);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet = MB_Load_TaxCodeMstr(bTaxImport);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
   }

   if (!iLoadFlag)
      return iRet;

   // Extract lien file
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acValueFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acValueFile, 0);      // 2016
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode);
   }

   // Extract Sale file from Sie_Sales.txt to Sie_Sale.sls
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Do not use confirmed sale price
      //bUseConfSalePrice = false;

      // Doc date - input format MM/DD/YYYY
      iRet = Sie_CreateSCSale(MM_DD_YYYY_1, 0, 6, true, &SIE_DocCode[0]);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      iRet = Sie_ConvStdChar(acCharFile);
      if (iRet <= 0)
      {
         LogMsg("***** Error extracting attributes data from %s", acCharFile);
         return -1;
      }
   }

   // Open CityZip file
   //iRet = initSitus(acIniFile, myCounty.acCntyCode);
   //if (iRet < 0)
   //   return iRet;

   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      if (iLoadFlag & LOAD_LIEN)                   // -L
      {
         // Create Lien file
         LogMsg0("Load %s Lien file", myCounty.acCntyCode);
         iRet = Sie_Load_LDR(iSkip);

         // If not defined, use current apn length
         iPrevApnLen = GetPrivateProfileInt(myCounty.acCntyCode, "PrevApnLen", iApnLen, acIniFile);
         iRet = updatePrevApn(myCounty.acCntyCode, iPrevApnLen, iSkip);
      } else if (iLoadFlag & LOAD_UPDT)            // -U
      {
         LogMsg0("Load %s roll update file", myCounty.acCntyCode);
         iRet = Sie_Load_Roll(iSkip);
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Sie_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
   }

   iPrevApnLen = GetPrivateProfileInt(myCounty.acCntyCode, "PrevApnLen", iApnLen, acIniFile);
   if (bUpdPrevApn)
   {
      // If not defined, use current apn length
      iRet = updatePrevApn(myCounty.acCntyCode, iPrevApnLen, iSkip);
   }

   // 02/19/2023
   //if ((iLoadFlag & MERG_GISA) && iPrevApnLen > 0 && iPrevApnLen <= iApnLen)
   //   iApnLen = iPrevApnLen;

   return iRet;
}
