/**************************************************************************
 *
 * Notes:
 *    - Even though LAK is MB county, the data layout is not standard.  Be careful
 *      when copy or use function from other MB counties.
 *    - Review Lak_MergeLien() before 2010 LDR production.
 *
 * Options:
 *    -CLAK -L -Xl -Xs (load lien)
 *    -CLAK -U -Xs (load update)
 *    -CLAK -Ld -Xs or -L20090910 (load 20090910 daily file)
 *
 * Revision
 * 12/13/2006 1.7.6.1  Fix situs city problem
 * 09/27/2007 1.9.17.1 Fix Lak_MergeChar() and asCooling table.
 * 12/19/2007 1.9.20   Support new file naming for daily process.
 * 01/25/2008 1.10.3.1 Add code to support standard usecode
 * 02/29/2008 1.10.5   Add code to output update records.
 * 07/14/2008 8.1.1    LAK is now sending TR601 file format for LDR.  New function
 *                     MB_ExtrTR601() is created in LoadMB.cpp to extract lien values.
 * 07/14/2008 8.1.1    Add function Lak_Load_LDR(), Lak_MergeLien(), Lak_MergeSitus(),
 *                     Lak_MergeMAdr() for TR601 input format.
 * 07/16/2008 8.1.2    Translate numeric usecode from LDR data to regular county
 *                     usecode for display.  This is required change for LDR 2008.
 * 07/22/2008 8.2.2.1  Remove bad characters in input records. Fix bug in Lak_MergeMAdr()
 *                     that overwrites IMAPLINK. Scan for bad chars on R01 file.
 * 01/22/2009 8.5.5    Fix CareOf bug in Lak_MergeMAdr().  Ignore CareOf if its length < 7 chars.
 * 02/26/2009 8.6      Add -Mo option, modify Lak_MergeLien() to populate other values.
 * 03/30/2009 8.7.1    Use MB_MergeSale() to do sale update instead of MergeSale3() to fix 
 *                     problem that non-sale transaction overwrites sale transaction that  
 *                     occurs in the same day  since we only keep one transaction per day.
 * 05/21/2009 8.7.6    Remove reference to OldApn & NewApn files.  We no longer need them.
 * 07/15/2009 9.1.2    Modify Lak_MergeRoll() to update other values.
 * 09/16/2009 9.2.1    Adding option -Lyyyymmdd to allow loading specific day.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 03/25/2011 10.6.0   Add bGrGrAvail and send email notif on daily processing.
 * 06/07/2011 10.9.1   Exclude HomeSite from OtherVal.  Replace Lak_MergeExe() with MB_MergeExe()
 *                     and Lak_MergeTax() with MB_MergeTax(). Add -Xs option. Bug fix  LoadLak().
 * 07/05/2011 11.1.1   Add S_HSENO.  Found no range HSENO in SBX.
 *                     Update TRANSFER via MB_MergeSale().
 * 08/29/2011 11.3.1.2 LAK has new record layout.  Many thing will be changed.
 *                     First replace MergeLak.h with LoadMB.h and CharRec.h
 *                     Replace Lak_MergeSAdr() with Lak_MergeSitus(). Modify Lak_MergeChar()
 *                     to use STDCHAR instead of MB_CHAR. Modify Lak_MergeRoll() to support
 *                     new 4-digit USECODE. Use MB_MergeTaxG2() to update tax info. Replace
 *                     Lak_ConvertChar() with Lak_ExtrChar(). Sale data has new date format.
 *                     Use ApplyCumSale() to update sales instead of Lak_MergeSale().
 * 05/22/2012 11.15.0  Add option to load tax file into tax database.
 * 08/03/2012 12.2.4   Fix STD_USE in Lak_MergeLien().  Modify Lak_Load_LDR() to use
 *                     CUM CHAR file instead of county file.
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 08/07/2013 13.8.11  Lak_MergeSitus(): blank out UnitNo before update.
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 01/08/2014 13.19.0  Add DocCode[] table. Remove bad char in S_UNITNO.
 * 07/28/2015 15.1.0   Add DBA to Lak_MergeMAdr() and remove comma from M_CTY_ST_D.
 * 07/31/2015 15.2.0   Fix Lak_MergeMAdr() to remove old DBA before putting new one in.
 * 10/13/2015 15.4.0   Use MB_Load_TaxBase() instead of MB_Load_TaxRollInfo() to import 
 *                     data into Tax_Base & Tax_Owner tables.
 * 01/13/2016 15.10.1  Remove old situs in Cal_MergeSitus() before update to avoid remnant from old addr.
 * 08/02/2016 16.1.0   Add Lak_Load_LDR3() to support new LDR layout.  
 * 05/09/2017 16.15.4  Add Lak_ConvStdChar() & Lak_MergeStdChar() to support new CHAR layout.
 * 07/28/2017 17.2.1   Modify Lak_Load_LDR3() to use Lak_MergeStdChar().
 * 07/20/2019 19.1.0   Remove duplicate code in MergeLien3().
 * 07/24/2019          Add Agpreserve flag.
 * 04/22/2020 19.8.6   Modify Lak_MergeLien3() & Lak_MergeRoll() to hardcode USE_STD for APN=040195020000
 * 07/20/2020 20.2.1   Remove using lien extract file in Lak_Load_LDR3() and clean up code in Lak_MergeSitus().
 * 07/31/2020 20.2.4   Add -Mz option to merge zoning
 * 11/01/2020 20.4.2   Modify Lak_MergeRoll() & Lak_MergeStdChar() to populate default PQZoning.
 * 07/16/2021 21.0.9   Modify Lak_MergeStdChar() to populate QualityClass.  Remove Lak_ExtrChar() & Lak_MergeChar().
 *                     Modify Lak_ConvStdChar() to update new data for Fireplace, Heating, & Pool.
 * 09/12/2024 24.1.3   Modify Lak_MergeLien3() to add ExeType.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"

#include "LoadMB.h"
#include "CharRec.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "PQ.h"
#include "Tax.h"
#include "MergeLak.h"
//#include "MergeZoning.h"

/******************************** Lak_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Lak_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64], acOwners[64];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.' || *pTmp == ',' || *pTmp == '*')
         pTmp++;
      if (*pTmp == '-')
      {
         pTmp++;
         if (*pTmp == ' ')
            pTmp++;
      }
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;
   strcpy(acOwners, acTmp);

   acName2[0] = 0;
   acSave1[0] = 0;

   if (pTmp=strstr(acTmp, " 1/2 INT"))
      *pTmp = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp1 = strlen(acTmp);
      if (iTmp1 > SIZ_NAME1) iTmp1 = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp1);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp1);
      return;
   }

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
     if ( (pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " TR")))
        *pTmp = 0;

      // If TRUST appears after number, save from number forward
      if (strstr((char *)&acTmp[iTmp], " TRUST") || strstr((char *)&acTmp[iTmp], " LIVING") ||
        strstr((char *)&acTmp[iTmp], " REVOCABLE") )
      {
         iTmp--;
         strcpy(acSave1, (char *)&acTmp[iTmp]);
         acTmp[iTmp] = 0;
      }
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
      if ((pTmp=strstr(acName2, " ETAL")) || (pTmp=strstr(acName2, " ET AL")) )
         *pTmp = 0;
   } else if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Filter out words, things in parenthesis
   // MONDANI NELLIE M ESTATE OF

   if ((pTmp=strstr(acTmp, " SUCC TR")) || 
       (pTmp=strstr(acTmp, " SUCCESSOR")) ||
       (pTmp=strstr(acTmp, " SURVIVOR")) )
      *pTmp = 0;

   if ( (pTmp = strchr(acTmp, '('))  || (pTmp=strstr(acTmp, " CO TR")) || (pTmp=strstr(acTmp, " CO-TR")) ||
      (pTmp=strstr(acTmp, " COTR")) ||
      (pTmp=strstr(acTmp, " LIFE ESTATE")) ||
      (pTmp=strstr(acTmp, " TRUSTEE")) ||
      (pTmp=strstr(acTmp, " TR ETAL")) ||
      (pTmp=strstr(acTmp, " TR")) ||
      (pTmp=strstr(acTmp, " TRES")) || (pTmp=strstr(acTmp, " TTEE")) ||
      (pTmp=strstr(acTmp, " ESTATE OF")) ||
      (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTS OF")) ||
      (pTmp=strstr(acTmp, " ETAL"))  || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " JT")) || (pTmp=strstr(acTmp, " CP")) ||
      (pTmp=strstr(acTmp, " LIV")))
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " FAMILY TRUST &"))
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      // RUMAN RICKY L & RUMAN FAMILY TRUST &
      // RUMAN RICKY L & NANCY FAMILY TRUST & MARIE
      // VERMETTE JAMES & THERESA A & ZENDER ROBERT A &
      strcpy(acSave1, " FAMILY TRUST");
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST &"))
   {  // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      strcpy(acSave1, " LIVING TRUST");
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      strcpy(acSave1, " REV TRUST");
      strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      *(pTmp+6) = 0;
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
      {
         strcpy(acSave1, pTmp1);
         *pTmp1 = 0;
      } else
      {
         strcpy(acSave1, pTmp);
         *pTmp = 0;
      }

      strcpy(acName1, acTmp);
      strcpy(acName2, pTmp+9);
   } else if (pTmp=strstr(acTmp, " LAND TRUST"))
   {
      strcpy(acSave1, " LAND TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " FAMILY "))
             || (pTmp=strstr(acTmp, " INCOME TR")) || (pTmp=strstr(acTmp, " LIVING TR")) )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " ESTATE OF"))
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   if (pTmp = strstr(acSave1, " THE"))
      *pTmp = 0;

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
         strcpy(acName2, pTmp);
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Name1
   iTmp = strlen(acOwners);
   if (iTmp > SIZ_NAME1)
      iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acOwners, iTmp);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);

      strcpy(acTmp1, myOwner.acName1);
      if (myOwner.acVest[0] > ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // Concat what in saved buffer
      if (acSave1[0])
      {
         if (acTmp1[strlen(acTmp1)-1] == ' ' && acSave1[0] == ' ')
            strcat(acTmp1, (char *)&acSave1[1]);      // avoid double blank
         else
            strcat(acTmp1, acSave1);
      }

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
      {
         iTmp = strlen(acTmp1);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, iTmp);
      } else
      {
         pTmp = myOwner.acSwapName;
         if (!memcmp(pTmp, "JR ", 3) || !memcmp(pTmp, "SR ", 3))
            pTmp += 3;
         iTmp = strlen(pTmp);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, pTmp, iTmp);
      }
   } else
   {
      // Couldn't split names
      if (acSave1[0])
      {
         strcat(acName1, acSave1);
         acSave1[0] = 0;
         iTmp = strlen(acName1);
         if (iTmp > SIZ_NAME1)
            iTmp = SIZ_NAME1;
         memcpy(pOutbuf+OFF_NAME_SWAP, acName1, iTmp);
      } else
      {
         iTmp = strlen(pNames);
         if (iTmp > SIZ_NAME1)
            iTmp = SIZ_NAME1;
         memcpy(pOutbuf+OFF_NAME_SWAP, pNames, iTmp);
      }
   }
}

/******************************** Lak_MergeMAdr ******************************
 *
 * Merge Mail address from TR601 file
 *
 *****************************************************************************/

void Lak_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *pCareOf, *pDba, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64], acDba[128];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "056400065", 9) || !memcmp(pOutbuf, "007113047", 9))
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   pCareOf = pDba = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         pCareOf = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         pCareOf = pLine2;
         p1 = pLine3;
         if (!_memicmp(pLine1, "DBA ", 4) )
            pDba = pLine1+4;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         p1 = pLine3;
         sprintf(acDba, "%s %s", pLine1+4, pLine2);
         pDba = &acDba[0];
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         pCareOf = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         pDba = pLine1+4;
         p1 = pLine2;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
   }

   // Update DBA
   if (pDba)
   {
      iTmp = strlen(pDba);
      if (iTmp > SIZ_DBA) 
         iTmp = SIZ_DBA;
      memcpy(pOutbuf+OFF_DBA, pDba, iTmp);
   }

   // Check for C/O
   if (pCareOf)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
      /*
      if (!_memicmp(pCareOf, "C/O", 3))
         pTmp = pCareOf+4;
      else if (!_memicmp(pCareOf, "ATTN", 4))
         pTmp = pCareOf+5;
      else if (*pCareOf == '%')
         pTmp = pCareOf+1;

      while (*pTmp == ' ')
         pTmp++;
      iTmp = strlen(pTmp);
      if (iTmp > SIZ_CARE_OF)
         iTmp = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, pTmp, iTmp);
      */
   }

   if (p1)
   {
      // Remove known bad char
      remUChar((unsigned char *)p1, 0xAB, strlen(p1));

      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      } else
      {
         iTmp = strlen(acAddr1);
         if (iTmp > SIZ_M_STREET)
            iTmp = SIZ_M_STREET;
         memcpy(pOutbuf+OFF_M_STREET, acAddr1, iTmp);
      }
   }

   strcpy(acAddr2, p2);
   blankRem(acAddr2);
   remChar(acAddr2, ',');
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D)
      iTmp = SIZ_M_CTY_ST_D;
   memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      iTmp = strlen(sMailAdr.City);
      if (iTmp > SIZ_M_CITY) iTmp = SIZ_M_CITY;
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, iTmp);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      iTmp = strlen(sMailAdr.Zip);
      if (iTmp > SIZ_M_ZIP)
         iTmp = SIZ_M_ZIP;
      memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, iTmp);
      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}

/******************************** Lak_MergeSAdr ******************************
 *
 * Merge Situs address from TR601 file
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Lak_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128], *pTmp;
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004130013", 9))
   //   acTmp[0] = 0;
#endif
   // 
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   if (sSitusAdr.lStrNum > 0)
   {
      pTmp = strchr(acAddr1, ' ');
      *pTmp = 0;

      // Save original StrNum
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
      memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   }

   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/******************************** Lak_MergeMAdr ******************************
 *
 * Merge Mail address from roll update
 *
 *****************************************************************************/

void Lak_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64];
   int      iTmp;
   char *    pTmp;
   CString strIn;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ' && strlen(apTokens[MB_ROLL_CAREOF]) > 6)
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      iTmp = strlen(pTmp);
      if (iTmp > SIZ_DBA)
         iTmp = SIZ_DBA;
      memcpy(pOutbuf+OFF_DBA, pTmp, iTmp);
   } 

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   blankRem(acAddr1);
   if (acAddr1[0] <= ' ')
      return;

   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1_1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
     sMailAdr.strName[SIZ_M_STREET] = 0;
     pTmp = (char *)&sMailAdr.strName[0];
     if (*pTmp == '-') pTmp++;
     if (*pTmp == ' ') pTmp++;
     memcpy(pOutbuf+OFF_M_STREET, pTmp, strlen(pTmp));
     memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ' && strcmp(apTokens[MB_ROLL_M_CITY], "UNKNOWN"))
   {
      memcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], strlen(apTokens[MB_ROLL_M_CITY]));
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      iTmp = atoin(apTokens[MB_ROLL_M_ZIP], 5);
      if (memcmp(apTokens[MB_ROLL_M_ZIP], "000", 3) > 0)
      {
         iTmp = strlen(apTokens[MB_ROLL_M_ZIP]);
         if (iTmp > 9)
            iTmp = 9;
         memcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], iTmp);
      }
      iTmp = sprintf(acTmp, "%s %s %.5s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], pOutbuf+OFF_M_ZIP);
      if (iTmp > SIZ_M_CTY_ST_D)
         iTmp = sprintf(acTmp, "%s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST]);

      memcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, iTmp);
   }

}

/******************************** Lak_MergeSitus *****************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Lak_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acAddr2[64], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSitus);
      pRec = fgets(acRec, 512, fdSitus);
   }

   pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      pTmp = pRec;
      if (*pTmp == '"')
         pTmp++;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   else
      iRet = ParseStringIQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old Situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      // Save original StrNum
      vmemcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], SIZ_S_HSENO);

      iTmp = sprintf(acTmp, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      strcpy(acAddr1, apTokens[MB_SITUS_STRNUM]);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, SIZ_S_STR_SUB);

      if (*apTokens[MB_SITUS_STRDIR] > ' ' && isDir(apTokens[MB_SITUS_STRDIR]))
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
      }
   }

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);
      vmemcpy(pOutbuf+OFF_S_STREET, apTokens[MB_SITUS_STRNAME], SIZ_S_STREET);

      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
         Sfx2Code(acTmp, acCode);
      else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apTokens[MB_SITUS_STRNAME]);
      if (sAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);

      if (sAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, strlen(sAdr.strSfx));

      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      // Replace bad char
      replUnPrtChar(apTokens[MB_SITUS_UNIT], ' ', 0);

      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNO);
   }

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   acAddr2[0] = 0;
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr2);   
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr2[0] > ' ')
      {
         iTmp = strlen(acAddr2);
         strcat(acAddr2, " CA ");

         if (*(pOutbuf+OFF_M_ZIP) == '9' && !memcmp(pOutbuf+OFF_M_CITY, acAddr2, iTmp))
         {
            memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
            memcpy(acTmp, pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
            acTmp[SIZ_M_ZIP] = 0;
            strcat(acAddr2, acTmp);
         }
         blankRem(acAddr2);
      }
   }

   if (acAddr2[0] > ' ')
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/******************************** Lak_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************

int Lak_MergeSAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;
   ADR_REC  sAdr;

   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_ROLL_STRNUM]);
   if (lTmp > 0)
   {
      // Save original StrNum
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_ROLL_STRNUM], strlen(apTokens[MB_ROLL_STRNUM]));
#ifdef _DEBUG
      //if (isCharIncluded(apTokens[MB_ROLL_STRNUM], '-', 0))
      //   lRecCnt++;
#endif

      sprintf(acTmp, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
      pTmp = strchr(acTmp, ' ');
      *(pTmp+1) = 0;
      strcpy(acAddr1, acTmp);

      if (pTmp = strchr(apTokens[MB_ROLL_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));
   }
   strcpy(acTmp, apTokens[MB_ROLL_STRNAME]);
   strcat(acAddr1, acTmp);
   blankPad(acAddr1, SIZ_S_ADDR_D);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Remove any thing in parenthesis
   if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;

   // Parse address
   parseAdr1S(&sAdr, acTmp);
   if (sAdr.strName[0] > ' ')
   {
      if (sAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sAdr.strDir, strlen(sAdr.strDir));

      memcpy(pOutbuf+OFF_S_STREET, sAdr.strName, strlen(sAdr.strName));
      
      if (sAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, strlen(sAdr.strSfx));
      if (sAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, sAdr.Unit, strlen(sAdr.Unit));
   }

   // Situs city
   if (*apTokens[MB_ROLL_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_ROLL_COMMUNITY], acTmp, acAddr1);   
      blankPadz(acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
         sprintf(acTmp, "%s CA    ", myTrim(acAddr1));
      iTmp = strlen(acTmp);
      if (iTmp > SIZ_S_CTY_ST_D)
         iTmp = SIZ_S_CTY_ST_D;

      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
   }

   // Currently there is no situs zip availble
   lTmp = atol(apTokens[MB_ROLL_ZIP]);
   if (lTmp > 90000)
      memcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_ROLL_ZIP], SIZ_S_ZIP);

   return 0;
}
*/
/******************************** Lak_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Lak_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.12s", pRec+1);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002017014", 9))
   //   iRet = 0;
#endif

   while (!iTmp)
   {
      // Replace tab char with 0
      if (pRec)
         iRet = ParseStringNQ(pRec, ',', MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         /* Do not use Confidential Sale Price
         } else
         {
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            }
         */
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         MB_MergeSale(&sCurSale, pOutbuf, true, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Lak_MergeChar ******************************
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

//int Lak_MergeChar(char *pOutbuf)
//{
//   static   char acRec[512], *pRec=NULL;
//   char     acTmp[256], acCode[32];
//   long     lTmp, lBldgSqft, lGarSqft;
//   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iFp;
//   STDCHAR  *pChar;
//
//   iRet=iBeds=iFBath=iHBath=iFp=0;
//   lBldgSqft=lGarSqft=0;
//
//   // Get first Char rec for first call
//   if (!pRec && !lCharMatch)
//      pRec = fgets(acRec, 1024, fdChar);
//
//   do
//   {
//      if (!pRec)
//      {
//         fclose(fdChar);
//         fdChar = NULL;
//         return 1;      // EOF
//      }
//
//      // Compare Asmt
//      iLoop = memcmp(pOutbuf, pRec, iApnLen);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
//         pRec = fgets(acRec, 1024, fdChar);
//         lCharSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 1;
//
//   pChar = (STDCHAR *)pRec;
//
//   // Quality Class
//   memcpy(acTmp, pChar->QualityClass, SIZ_CHAR_QCLS);
//   acTmp[MBSIZ_CHAR_QUALITY] = 0;
//
//   acCode[0] = 0;
//   if (isalpha(acTmp[0]))
//   {
//      *(pOutbuf+OFF_BLDG_CLASS) = toupper(acTmp[0]);
//
//      iTmp = 0;
//      while (acTmp[iTmp] && !isdigit(acTmp[iTmp]))
//         iTmp++;
//
//      if (acTmp[iTmp] > '0')
//      {
//         // Limit to first 2 digits
//         if (acTmp[iTmp+1] == '.')
//            acTmp[iTmp+3] = 0;
//         else
//            acTmp[iTmp+2] = 0;
//
//         iRet = Quality2Code((char *)&acTmp[iTmp], acCode, NULL);
//      }
//   } else if (acTmp[0] > '0' && acTmp[0] <= '9')
//      iRet = Quality2Code(acTmp, acCode, NULL);
//
//   blankPad(acCode, SIZ_BLDG_QUAL);
//   memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
//
//   // Yrblt
//   lTmp = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
//   if (lTmp > 1700)
//      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
//   else
//      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);
//
//   // BldgSqft
//   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
//   if (lBldgSqft > 10)
//   {
//      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
//      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
//   } else
//      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);
//
//   // Garage Sqft
//   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
//   if (lGarSqft > 10)
//   {
//      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
//      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
//      *(pOutbuf+OFF_PARK_TYPE) = '2';
//   } else
//   {
//      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
//      *(pOutbuf+OFF_PARK_TYPE) = ' ';
//   }
//
//   // Heating
//   if (pChar->Heating[0] > ' ')
//   {
//      iTmp = 0;
//      while (asHeating[iTmp].iLen > 0)
//      {
//         if (!memcmp(pChar->Heating, asHeating[iTmp].acSrc, asHeating[iTmp].iLen))
//         {
//            *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
//            break;
//         }
//         iTmp++;
//      }
//   }
//   // Cooling 
//   if (pChar->Cooling[0] > ' ')
//   {
//      iTmp = 0;
//      while (asCooling[iTmp].iLen > 0)
//      {
//         if (!memcmp(pChar->Cooling, asCooling[iTmp].acSrc, asCooling[iTmp].iLen) )
//         {
//            *(pOutbuf+OFF_AIR_COND) = asCooling[iTmp].acCode[0];
//            break;
//         }
//         iTmp++;
//      }
//   }
//
//   // Pool
//   *(pOutbuf+OFF_POOL) = pChar->Pool[0];
//
//   // Beds
//   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
//   if (iBeds > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
//      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
//   } else
//      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);
//
//   // Bath
//   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
//   if (iFBath > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
//      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
//   } else
//      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);
//
//   // Half bath
//   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
//   if (iHBath > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
//      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
//   } else
//      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);
//
//   // Fireplace
//   iFp = atoin(pChar->Fireplace, SIZ_CHAR_SIZE2);
//   if (iFp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
//      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
//   } else
//      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);
//
//   // HasSeptic or HasSewer
//   if (pChar->HasSeptic > '0')
//      *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
//   else if (pChar->HasSewer > '0')
//      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;
//
//   // HasWell
//   *(pOutbuf+OFF_WATER) = pChar->HasWater;
//
//   // Pool - ignore
//   if (pChar->Pool[0] > '0')
//      *(pOutbuf+OFF_POOL) = pChar->Pool[0];
//
//   lCharMatch++;
//
//   // Get next Char rec
//   pRec = fgets(acRec, 1024, fdChar);
//
//   return 0;
//}

int Lak_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lTmp;
   int      iRet, iLoop, iBeds, iFBath, iHBath, iTmp;
   STDCHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=0;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, acRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "044510005", 9))
   //   iRet = 0;
#endif

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)&acRec[0];

   // Quality Class
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Impr condition
   *(pOutbuf+OFF_IMPR_COND) = pChar->ImprCond[0];

   // Yrblt
   long lYrBlt = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lYrBlt > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // YrEff
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 1900 && lTmp >= lYrBlt)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_EFF);
   else
      memcpy(pOutbuf+OFF_YR_EFF, BLANK32, SIZ_YR_EFF);

   // LotSqft
   long lLotSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lLotSqft > 100)
   {
      //sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lLotSqft);
      memcpy(pOutbuf+OFF_LOT_SQFT, pChar->LotSqft, SIZ_LOT_SQFT);
      memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_ACRES);
   } 

   // BldgSqft
   long lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 100)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   long lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 100)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   } else
      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);

   // Parking type
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // #Fireplace
   *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

   // Heating
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   
   // Cooling 
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Total rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   } else
      memcpy(pOutbuf+OFF_ROOMS, BLANK32, SIZ_ROOMS);
   
   // HasSeptic or HasSewer
   if (pChar->HasSeptic > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
   else if (pChar->HasSewer > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;
   else
      *(pOutbuf+OFF_SEWER) = ' ';

   // Pool
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Stories
   iTmp = atoin(pChar->Stories, SIZ_CHAR_SIZE4);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d.0 ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   } else
      memcpy(pOutbuf+OFF_STORIES, BLANK32, SIZ_STORIES);

   // Units
   if (pChar->Units[0] > '0')
      memcpy(pOutbuf+OFF_UNITS, pChar->Units, SIZ_CHAR_UNITS);

   // Zoning
   if (pChar->Zoning[0] > ' ' && *(pOutbuf+OFF_ZONE) == ' ')
   {
      memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         memcpy(pOutbuf+OFF_ZONE_X1, pChar->Zoning, SIZ_ZONE);
   }

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************** Lak_MergeExe *******************************
 *
 *
 *****************************************************************************

int Lak_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp, lGross;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, ',');
      pTmp += 2;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 0;
   }

   iRet = ParseStringNQ(pRec, ',', MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Gross - Exempt amt cannot be larger than gross
   lGross = atoin(pOutbuf+OFF_GROSS, SIZ_GROSS);
   
   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0 && lTmp <= lGross)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Lak_MergeTax ******************************
 *
 * Note:
 *
 *****************************************************************************

int Lak_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double   dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iRet;

   // Merge tax data
   while (!iTmp)
   {
      iTmp = ParseStringNQ(pRec, ',', MB_TAX_ROLLCAT+1, apTokens);
      if (iTmp < MB_TAX_ROLLCAT)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
         return -1;
      }

      // If same tax year, apply values
      //if (!memcmp(myCounty.acYearAssd, apTokens[MB_TAX_YEAR], 4))
      {
         dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
         dTax1 = atof(acTmp);
         dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
         dTax2 = atof(acTmp);

         if (dTax1 > 0.0 || dTax2 > 0.0)
         {
            dTmp = dTax1+dTax2;
            if (dTax1 == 0.0 || dTax2 == 0.0)
               dTmp *= 2;

            if (dTmp > 0.0)
            {
               sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
               memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
            }
         }
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/********************************* Lak_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Lak_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Input file may contain tab or unprintable char.  Replace it with space
   // Do not call this function if TAB is used for delimiter
   replCharEx(pRollRec, 31, 32);

   // Parse input record
   if (cDelim == ',')
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad input record for APN=%s (tokens=%d)", apTokens[iApnFld], iRet);
      return -1;
   }

   // Ignore APN starts with 800-999 except 910 (MH) or 915
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910 && iTmp != 915))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "17LAK", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "033305340", 9))
   //   iTmp = 0;
#endif

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Format APN - Lake format may change any time so we need to keep it out here
   iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Legal
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Megabyte:
   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memset(pOutbuf+OFF_USE_CO, ' ', SIZ_USE_CO);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      if (!memcmp(pOutbuf, "040195020000", 12))    // Special case per Denise request 4/22/2020
         memcpy(pOutbuf+OFF_USE_STD, "212", 3);
      else
         updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], strlen(apTokens[MB_ROLL_DOCNUM]));
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   }

   // Owner
   Lak_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);

   // Merge Situs - old layout before 08/20/2011
   //Lak_MergeSAdr(pOutbuf);

   // Mailing
   Lak_MergeMAdr(pOutbuf);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************* Lak_Load_Roll ******************************
 *
 * Process update roll
 * Return -1: Missing S01 file
 *        -2: No input file
 *        -3: Cannot create output file
 *         0: Process normal, no error
 *
 ******************************************************************************/

int Lak_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Load roll update %s", acRollFile);

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open updated roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) F(TXT)");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }
   
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw out file: %s\n", acOutFile);
      return -4;
   }

   // Skip header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Get 1st rec
   pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

#ifdef _DEBUG
      //if (!memcmp(acBuf, "006012680000", 9) || !memcmp((char *)&acRollRec[iSkipQuote], "030330020", 9))
      //   iTmp = 0;
#endif

      NextRollRec:
      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Lak_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Lak_MergeSitus(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Lak_MergeStdChar(acBuf);
               //lRet = Lak_MergeChar(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acBuf, iHdrRows);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Lak_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Lak_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Lak_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acRec, iHdrRows);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[iSkipQuote], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Lak_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Lak_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);

         // Merge Char
         if (fdChar)
            lRet = Lak_MergeStdChar(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, iHdrRows);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSitus)
      fclose(fdSitus);
   if (fdChar)
      fclose(fdChar);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lRecCnt);

   // Force email
   if (!lSitusMatch || !lCharMatch)
   {
      bSendMail = true;
      iRet = 1;
   } else
      iRet = 0;

   return iRet;
}


/****************************** Lak_ExtrChar() ******************************
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

//int Lak_ExtrChar()
//{
//   FILE     *fdIn, *fdOut;
//   char     acBuf[1024], acTmpFile[256], acTmp[256], *pRec;
//   int      iRet, iTmp, iCnt=0;
//   STDCHAR  myCharRec;
//
//   LogMsg("\nExtracting char file %s\n", acCharFile);
//
//   if (!(fdIn = fopen(acCharFile, "r")))
//   {
//      LogMsg("***** Error open input file %s.  Please check for file exist.", acCharFile);
//      return -1;
//   }
//
//   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   if (!(fdOut = fopen(acTmpFile, "w")))
//   {
//      fclose(fdIn);
//      LogMsg("***** Error creating output file %s", acTmpFile);
//      return -2;
//   }
//
//   // Skip header record
//   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
//      pRec = fgets(acBuf, 1024, fdIn);
//
//   while (!feof(fdIn))
//   {
//      pRec = fgets(acBuf, 1024, fdIn);
//
//      if (!pRec)
//         break;
//
//      if (cDelim == ',')
//         iRet = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
//      else
//         iRet = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
//      if (iRet < MB2_CHAR_BLDGTYPE)
//      {
//         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
//         continue;
//      }
//
//      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
//
//      // Apn
//      memcpy(myCharRec.Apn, apTokens[MB2_CHAR_ASMT], strlen(apTokens[MB2_CHAR_ASMT]));
//      
//      // Fee parcel
//      memcpy(myCharRec.FeeParcel, apTokens[MB2_CHAR_FEE_PRCL], strlen(apTokens[MB2_CHAR_FEE_PRCL]));
//      
//      // Pools
//      if (*apTokens[MB2_CHAR_POOLS] > '0')
//         myCharRec.Pool[0] = 'P';
//
//      // Use cat
//      memcpy(myCharRec.LandUseCat, apTokens[MB2_CHAR_USECAT], strlen(apTokens[MB2_CHAR_USECAT]));
//
//      // Quality Class
//      memcpy(myCharRec.QualityClass, apTokens[MB2_CHAR_QUALITY], strlen(apTokens[MB2_CHAR_QUALITY]));
//      memcpy(myCharRec.Heating, apTokens[MB2_CHAR_HEATING], strlen(apTokens[MB2_CHAR_HEATING]));
//      memcpy(myCharRec.Cooling, apTokens[MB2_CHAR_COOLING], strlen(apTokens[MB2_CHAR_COOLING]));
//      memcpy(myCharRec.HeatingSrc, apTokens[MB2_CHAR_HEATING_SRC], strlen(apTokens[MB2_CHAR_HEATING_SRC]));
//      memcpy(myCharRec.CoolingSrc, apTokens[MB2_CHAR_COOLING_SRC], strlen(apTokens[MB2_CHAR_COOLING_SRC]));
//
//      // YrBlt
//      iTmp = atoi(apTokens[MB2_CHAR_YRBLT]);
//      if (iTmp > 1600 && iTmp <= lLienYear)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.YrBlt, acTmp, iRet);
//      }
//
//      // BldgSqft
//      iTmp = atoi(apTokens[MB2_CHAR_BLDGSQFT]);
//      if (iTmp > 1)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.BldgSqft, acTmp, iRet);
//      }
//
//      // GarSqft
//      iTmp = atoi(apTokens[MB2_CHAR_GARSQFT]);
//      if (iTmp > 1)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.GarSqft, acTmp, iRet);
//      }
//
//      // Beds
//      iTmp = atoi(apTokens[MB2_CHAR_BEDS]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.Beds, acTmp, iRet);
//      }
//
//      // Baths
//      iTmp = atoi(apTokens[MB2_CHAR_FBATHS]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.FBaths, acTmp, iRet);
//      }
//
//      // Half baths
//      iTmp = atoi(apTokens[MB2_CHAR_HBATHS]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.HBaths, acTmp, iRet);
//      }
//
//      // Fireplace
//      iTmp = atoi(apTokens[MB2_CHAR_FP]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.Fireplace, acTmp, iRet);
//      }
//
//      blankRem(apTokens[MB2_CHAR_HASSEPTIC]);
//      if (*(apTokens[MB2_CHAR_HASSEPTIC]) > '0')
//      {
//         myCharRec.HasSeptic = 'Y';
//         myCharRec.HasSewer = 'S';
//      }
//
//      blankRem(apTokens[MB2_CHAR_HASSEWER]);
//      if (*(apTokens[MB2_CHAR_HASSEWER]) > '0')
//         myCharRec.HasSewer = 'Y';
//
//      blankRem(apTokens[MB2_CHAR_HASWELL]);
//      if (*(apTokens[MB2_CHAR_HASWELL]) > '0')
//      {
//         myCharRec.HasWell = 'Y';
//         myCharRec.HasWater = 'W';
//      }
//
//      // BldgSeqNum
//      iTmp = atoi(apTokens[MB2_CHAR_BLDGSEQNO]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
//      }
//
//      // UnitSeqNum
//      iTmp = atoi(apTokens[MB2_CHAR_UNITSEQNO]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.UnitSeqNo, acTmp, iRet);
//      }
//
//      // Building Type
//      //if (iRet > MB2_CHAR_BLDGTYPE)
//      //   memcpy(myCharRec.CoolingSrc, apTokens[MB2_CHAR_BLDGTYPE], strlen(apTokens[MB2_CHAR_BLDGTYPE]));
//
//      myCharRec.CRLF[0] = '\n';
//      myCharRec.CRLF[1] = '\0';
//      fputs((char *)&myCharRec.Apn[0], fdOut);
//
//      iCnt++;
//      if (!(++iCnt % 1000))
//         printf("\r%u", iCnt);
//   }
//
//   if (fdIn) fclose(fdIn);
//   if (fdOut) fclose(fdOut);
//
//   // Sort output on ASMT
//   if (iCnt > 100)
//   {
//      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
//      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
//   } else
//      iRet = 0;
//
//   printf("\n");
//   return iRet;
//}

/**************************** Lak_ConvStdChar ********************************
 *
 * Copy from MergeMer.cpp to convert new char file. 07/14/2015
 *
 *****************************************************************************/

int Lak_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec)
         break;

      if (*pRec < '0' || *pRec > '9')
         continue;

      // Remove NULL before parsing
      replStrAll(acBuf, "NULL", "");

      iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < LAK_CHAR_HASWELL)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[LAK_CHAR_ASMT], strlen(apTokens[LAK_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[LAK_CHAR_FEEPARCEL], strlen(apTokens[LAK_CHAR_FEEPARCEL]));

      // Format APN
      if (*apTokens[LAK_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[LAK_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[LAK_CHAR_FEEPARCEL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[LAK_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[LAK_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(acBuf, "006012680000", 9))
      //   iTmp = 0;
#endif

      // QualityClass 
      iTmp = remChar(apTokens[LAK_CHAR_QUALITYCLASS], ' ');    // Remove all blanks before process
      pRec = _strupr(apTokens[LAK_CHAR_QUALITYCLASS]);
      vmemcpy(myCharRec.QualityClass, apTokens[LAK_CHAR_QUALITYCLASS], SIZ_CHAR_QCLS, iTmp);

      if (*apTokens[LAK_CHAR_QUALITYCLASS] > ' ' && *apTokens[LAK_CHAR_QUALITYCLASS] <= 'Z')
      {
         acCode[0] = ' ';
         strcpy(acTmp, apTokens[LAK_CHAR_QUALITYCLASS]);
         if (isalpha(acTmp[0])) 
         {
            myCharRec.BldgClass = acTmp[0];

            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            else if (isdigit(acTmp[2]))
               iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
         } else if (isdigit(acTmp[0]))
            iRet = Quality2Code(acTmp, acCode, NULL);
         else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[LAK_CHAR_QUALITYCLASS], apTokens[LAK_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (*apTokens[LAK_CHAR_QUALITYCLASS] > ' ' && *apTokens[LAK_CHAR_QUALITYCLASS] != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[LAK_CHAR_QUALITYCLASS], apTokens[LAK_CHAR_ASMT]);

      // YrBlt
      int iYrBlt = atoi(apTokens[LAK_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[LAK_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[LAK_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[LAK_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // Stories/NumFloors
      iTmp = atoi(apTokens[LAK_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 20)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[LAK_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[LAK_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport SF
      int iCarport = atoi(apTokens[LAK_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "010007600000", 9))
      //   iRet = 0;
#endif

      // Pool 
      iTmp = blankRem(apTokens[LAK_CHAR_POOLSPA]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[LAK_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
         else if (*apTokens[LAK_CHAR_POOLSPA] > '0')
            LogMsg("Unknown Pool/Spa type: %s [%s]", apTokens[LAK_CHAR_POOLSPA], apTokens[LAK_CHAR_FEEPARCEL]);
      }

      // FirePlace 
      if (*apTokens[LAK_CHAR_FIREPLACE] > '0')
      {
         pRec = findXlatCodeA(apTokens[LAK_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
         else
            LogMsg("Unknown FirePlace: %s [%s]", apTokens[LAK_CHAR_FIREPLACE], apTokens[LAK_CHAR_FEEPARCEL]);
      }

      // Heating 
      iTmp = blankRem(apTokens[LAK_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCodeA(apTokens[LAK_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
         else
            LogMsg("Unknown Heating type: %s [%s]", apTokens[LAK_CHAR_HEATING], apTokens[LAK_CHAR_FEEPARCEL]);
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[LAK_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[LAK_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[LAK_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[LAK_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[LAK_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[LAK_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[LAK_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // Patio SF
      iTmp = atoi(apTokens[LAK_CHAR_PATIOSF]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // Haswell - 1, 0
      blankRem(apTokens[LAK_CHAR_HASWELL]);
      if (*(apTokens[LAK_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Lot Sqft - Currently no data
      //if (iFldCnt >= LAK_CHAR_LOTSQFT)
      //{
      //   iTmp = atoi(apTokens[LAK_CHAR_LOTSQFT]);
      //   if (iTmp > 1)
      //   {
      //      iRet = sprintf(acTmp, "%*d", SIZ_LOT_SQFT, iTmp);
      //      memcpy(myCharRec.LotSqft, acTmp, iRet);

      //      // Lot acres
      //      double dTmp;
      //      dTmp = (double)(iTmp*1000)/SQFT_PER_ACRE;
      //      iTmp = sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp+0.5));
      //      memcpy(myCharRec.LotAcre, acTmp, iTmp);
      //   }
      //}

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum, UnitSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/********************************* Lak_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Lak_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L_ASMT], strlen(apTokens[L_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L_FEEPARCEL], strlen(apTokens[L_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "17LAK", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values
   // Land
   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);
   long lFixt   = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L_TAXAMT1]);
   double dTax2 = atof(apTokens[L_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L_TRA], strlen(apTokens[L_TRA]));

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "042264030000", 9))
   //   iTmp = 0;
#endif
   // Legal
   updateLegal(pOutbuf, apTokens[L_PARCELDESCRIPTION]);

   // UseCode
   iTmp = strlen(apTokens[L_USECODE]);
   if (iTmp > 0)
   {
      memcpy(pOutbuf+OFF_USE_CO, apTokens[L_USECODE], iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L_USECODE], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Lak_MergeOwner(pOutbuf, apTokens[L_OWNER]);

   // Situs
   Lak_MergeSitus(pOutbuf, apTokens[L_SITUS1], apTokens[L_SITUS2]);

   // Mailing
   Lak_MergeMAdr(pOutbuf, apTokens[L_MAILADDRESS1], apTokens[L_MAILADDRESS2], apTokens[L_MAILADDRESS3], apTokens[L_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L_TAXABILITY], true, true);

   return 0;
}

/********************************* Lak_Load_LDR *****************************
 *
 * Load TR601 LDR into 1900-byte record.
 *
 ****************************************************************************/

int Lak_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     iRet, lRet=0, lCnt=0;

   LogMsg0("Load LDR file %s", acRollFile);

   lLastFileDate = getFileDate(acRollFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A)");
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open LDR file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating R01 file: %s\n", acTmpFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      lLDRRecCount++;

      // Create new R01 record
      iRet = Lak_MergeLien(acBuf, acRec);
      if (!iRet)
      {
         // Merge Char
         if (fdChar)
            lRet = Lak_MergeStdChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsgD("Total output records:       %u\n", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Lak_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Lak_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "17LAK", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&LAK_Exemption);

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      if (!memcmp(pOutbuf, "040195020000", 12))    // Special case per Denise request 4/22/2020
         memcpy(pOutbuf+OFF_USE_STD, "212", 3);
      else
         updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], 0, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Lak_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   //Lak_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Lak_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*apTokens[L3_CURRENTDOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 

   //// Garage size
   //dTmp = atof(apTokens[L3_GARAGESIZE]);
   //if (dTmp > 0.0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_GAR_SQFT, dTmp);
   //   memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   //   *(pOutbuf+OFF_PARK_TYPE) = '2';                 // GARAGE/CARPORT
   //}

   //// Number of parking spaces
   //if (*apTokens[L3_GARAGE] == '0' || *apTokens[L3_GARAGE] == 'N')
   //   *(pOutbuf+OFF_PARK_TYPE) = 'H';                 // None
   //else if (*apTokens[L3_GARAGE] > '0' && *apTokens[L3_GARAGE] <= '9')
   //{
   //   iTmp = atol(apTokens[L3_GARAGE]);
   //   sprintf(acTmp, "%d", iTmp);
   //   vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
   //   if (dTmp > 100)
   //      *(pOutbuf+OFF_PARK_TYPE) = 'Z';              // Garage
   //   else
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
   //} else
   //{
   //   if (*(apTokens[L3_GARAGE]) == 'C')
   //      *(pOutbuf+OFF_PARK_TYPE) = 'C';              // Carport
   //   else if (*(apTokens[L3_GARAGE]) == 'A')
   //   {
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L3_GARAGE], "DOU", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '2';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L3_GARAGE], "TRI", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '3';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L3_GARAGE], "SIN", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '1';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L3_GARAGE], "GC", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT
   //   else if (!_memicmp(apTokens[L3_GARAGE], "GS", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT ?
   //   else if (!_memicmp(apTokens[L3_GARAGE], "DE", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = 'D';              // Detached
   //   else if (*(apTokens[L3_GARAGE]) == 'S')
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Space ?
   //}

   //// YearBlt
   //lTmp = atol(apTokens[L3_YEARBUILT]);
   //if (lTmp > 1800 && lTmp < lToyear)
   //{
   //   iTmp = sprintf(acTmp, "%d", lTmp);
   //   memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   //}

   //// Total rooms
   //iTmp = atol(apTokens[L3_TOTALROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   //}

   //// Stories
   //iTmp = atol(apTokens[L3_STORIES]);
   //if (iTmp > 0 && iTmp < 100)
   //{
   //   sprintf(acTmp, "%d.0", iTmp);
   //   vmemcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   //}

   //// Units
   //iTmp = atol(apTokens[L3_UNITS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%d", iTmp);
   //   vmemcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   //}

   //// Beds
   //iTmp = atol(apTokens[L3_BEDROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BEDS, iTmp);
   //   memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   //}

   //// Baths
   //iTmp = atol(apTokens[L3_BATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_F, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   //}

   //// HBaths
   //iTmp = atol(apTokens[L3_HALFBATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_H, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   //}

   //// Heating
   //int iCmp;
   //if (*apTokens[L3_HEATING] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_HEATING], asHeating_LDR[iTmp].acSrc, asHeating_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_HEAT) = asHeating_LDR[iTmp].acCode[0];
   //}

   //// Cooling
   //if (*apTokens[L3_AC] == 'C')
   //   *(pOutbuf+OFF_AIR_COND) = 'C';
   //else if (*apTokens[L3_AC] > ' ')
   //   LogMsg("*** Unknown A/C: %s", apTokens[L3_AC]);

   //// Pool/Spa
   //if (*apTokens[L3_POOLSPA] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_POOLSPA], asPool_LDR[iTmp].acSrc, asPool_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_POOL) = asPool_LDR[iTmp].acCode[0];
   //}

   //// Fire place
   //if (*apTokens[L3_FIREPLACE] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   pTmp = _strupr(apTokens[L3_FIREPLACE]);
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_FIREPLACE], asFP_LDR[iTmp].acSrc, asFP_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_FIRE_PL) = asPool[iTmp].acCode[0];
   //}

   //// Quality Class
   //acTmp1[0] = 0;
   //if (*apTokens[L3_QUALITYCLASS] > '0' || strlen(apTokens[L3_QUALITYCLASS]) > 1)
   //{
   //   strcpy(acTmp, apTokens[L3_QUALITYCLASS]);
   //   remCharEx(acTmp, " ,'?");
   //   pTmp = _strupr(acTmp);

   //   //memcpy(myCharRec.QualityClass, pTmp, strlen(pTmp));
   //   if (acTmp[0] == 'A' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'A';
   //   else if (acTmp[0] == 'F' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'F';
   //   else if (!memcmp(pTmp, "POOR", 4) || !memcmp(pTmp, "LOW", 3))
   //      acTmp1[0] = 'P';
   //   else if (*pTmp == 'G')
   //      acTmp1[0] = 'G';
   //   else if (isalpha(*pTmp))
   //   {
   //      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
   //      if (isdigit(acTmp[1]))
   //         iRet = Quality2Code(&acTmp[1], acTmp1, NULL);
   //      else if (isdigit(acTmp[2]))
   //         iRet = Quality2Code(&acTmp[2], acTmp1, NULL);
   //      else if (isalpha(acTmp[1]))
   //      {
   //         switch (acTmp[1])
   //         {
   //            case 'L':
   //            case 'P':
   //               acTmp1[0] = 'P';
   //               break;
   //            case 'A':
   //               acTmp1[0] = 'A';
   //               break;
   //            case 'F':
   //               acTmp1[0] = 'F';
   //               break;
   //            case 'G':
   //               acTmp1[0] = 'G';
   //               break;
   //         }
   //      }
   //   } else if (isdigit(*pTmp))
   //   {
   //      iTmp = atol(pTmp);
   //      if (iTmp < 100)
   //         iRet = Quality2Code(pTmp, acTmp1, NULL);
   //   }

   //   if (acTmp1[0] > '0')
   //      *(pOutbuf+OFF_BLDG_QUAL) = acTmp1[0];
   //}

   return 0;
}

/******************************** Lak_Load_LDR3 *****************************
 *
 * Load LDR 2016-2020
 * 07/17/2020 Remove using lien extract since its data is from the same file.
 *
 ****************************************************************************/

int Lak_Load_LDR3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Value file
   //sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (!_access(acTmpFile, 0))
   //{
   //   LogMsg("Open Lien file %s", acTmpFile);
   //   fdLienExt = fopen(acTmpFile, "r");
   //   if (fdLienExt == NULL)
   //   {
   //      LogMsg("***** Error opening lien file: %s\n", acTmpFile);
   //      return -7;
   //   }
   //} else
   //   fdLienExt = NULL;

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
      if (cDelim == '|')
         strcat(acRec, "DEL(124) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      // Create new R01 record
      iRet = Lak_MergeLien3(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         //if (fdLienExt)
         //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Situs
         if (fdSitus)
            lRet = Lak_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Lak_MergeStdChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/*********************************** loadLak ********************************
 *
 * Options:
 *    -CLAK -L -Xl -Xs (load lien)
 *    -CLAK -U -Xs (load update)
 *    -CLAK -Ld -Xs or -L20090910 (load 20090910 daily file)
 *
 ****************************************************************************/

int loadLak(int iSkip)
{
   int   iRet=0;
   char  acTmpFile[256];

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;
   iApnLen = myCounty.iApnLen;

   if (iLoadTax == TAX_LOADING)           // -T or -Lt
   {
      // Load taxrollinfo.txt into SQL
      //iRet = MB_Load_TaxRollInfo(bTaxImport);

      // Load Lak_Tax.txt
      iRet = MB_Load_TaxBase(bTaxImport, true, 2, iHdrRows);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet = MB_Load_TaxCodeMstr(bTaxImport);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
   }

   if (!iLoadFlag)
      return iRet;
   /*
   char acTmp[64];
   if (iLoadFlag & LOAD_DAILY)
   {
      if (lProcDate > 0)
         sprintf(acTmp, "%d", lProcDate);
      else
         strcpy(acTmp, acToday);
   } else
      acTmp[0] = 0;

   replStr(acRollFile, "$date", acTmp);
   replStr(acSalesFile, "$date", acTmp);
   replStr(acExeFile, "$date", acTmp);
   replStr(acTaxFile, "$date", acTmp);
   strcpy(acTmpFile, acCharFile);
   replStr(acTmpFile, "$date", acTmp);
   replStr(acCharFile, "$date", "");
   replStr(acCharFile, ".csv", ".dat");

   if (_access(acRollFile, 0) || _access(acSalesFile, 0))
   {

      LogMsg("*** Input file not available %s/%s", acRollFile, acSalesFile);
      return 1;
   }
   */

   // Extract lien file - LDR2016
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acValueFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acValueFile, 0);      // 2016 
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode, NULL, LAST_CHAR_N);
   }

   // Create/Update cum sale file from TransferHistory.csv
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Doc date - new input format YYYY-MM-DD 08/22/2011
      // Old format MM/DD/YYYY upto ldr 2011
      // 01/06/2014
      iRet = MB_CreateSCSale(YYYY_MM_DD, 0, 0, false, (IDX_TBL5 *)&LAK_DocCode[0]);
      //iRet = MB_CreateSCSale(YYYY_MM_DD);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   } else
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Load Char file
      if (!_access(acCharFile, 0))
      {
         iRet = Lak_ConvStdChar(acCharFile);
         if (iRet <= 0)
         {
            LogMsg("***** Error converting Char file %s", acCharFile);
            return -1;
         }
      } else
      {
         LogMsg("*** WARNING: CHAR file does not exist: %s: ", acCharFile);
         LogMsg("    -Xa option is ignore.  Please verify input file");
      }
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      iRet = Lak_Load_LDR3(iSkip);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   } else 
   if (iLoadFlag & (LOAD_UPDT|LOAD_DAILY))         // -U or -Ld
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Lak_Load_Roll(iSkip);
      if (iRet = -2 && (iLoadFlag & LOAD_DAILY))
         bGrGrAvail = false;
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Sha_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   if (!iRet && bMergeOthers)
   {
      // Merge other values
      sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmpFile, 0))
         iRet = PQ_MergeOthers(myCounty.acCntyCode, acTmpFile, GRP_MB, iSkip);
      else
         LogMsg("***** Lien extract is missing: %s.  Please locate the file and retry", acTmpFile);
   }

   return iRet;
}

