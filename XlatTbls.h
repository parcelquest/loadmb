#ifndef PARC_TYPE
typedef struct _tParcelStatus
{
   int   iParcType;
   char  cParcStatus;
   char  cTimPre;     // Timber preserve
   char  cAgrPre;     // Agriculture preserve
} PARC_TYPE;

typedef struct _tVesting
{
   char  acVestSrc[8];
   char  acVestCode[8];
   int   iVestingLen;
} VEST_CODE;

typedef struct _tUseXref
{
   char  acCntyUse[4];
   char  acStdUse[4];
} USEXREF;

#define MAX_XLATCODE_SRC   32
#define MAX_XLATCODE_CODE  8
typedef struct _tXlatCode
{
   char  acSrc[MAX_XLATCODE_SRC];
   char  acCode[MAX_XLATCODE_CODE];
   int   iLen;
} XLAT_CODE;

char *getVestingCode_GLE(char *pVesting, char *pVestCode);
char *getVestingCode_LAS(char *pVesting, char *pVestCode);
char *getVestingCode_DNX(char *pVesting, char *pVestCode);
char *getVestingCode_TEH(char *pVesting, char *pVestCode);
char *getVestingCode_TRI(char *pVesting, char *pVestCode);
char *getVestingCode_ALP(char *pVesting, char *pVestCode);
char *getVestingCode_SIE(char *pVesting, char *pVestCode);
char *getVestingCode_TUO(char *pVesting, char *pVestCode);
char getParcStatus_ALP(char *pParcType);
char getParcStatus_DNX(char *pParcType);
char getParcStatus_GLE(char *pParcType);
char getParcStatus_LAS(char *pParcType);
char getParcStatus_TEH(char *pParcType);
char getParcStatus(PARC_TYPE *psPrclType, char *pParcType);

void getParcType(char *pStatus, char *pTimPre, char *pAgPre, char *pParcType, char *pCnty);

// Search for coded value from a sorted table, compare based on length of entry in table
char *findXlatCode(char *pValue, XLAT_CODE *pTable);
// Search for coded value from an unsorted table, compare based on length of entry in table 
char *findXlatCodeA(char *pValue, XLAT_CODE *pTable);
// Search for coded value from a sorted table, compare whole string
char *findXlatCodeS(char *pValue, XLAT_CODE *pTable);
// Search for coded value from unsorted table, compare single character 
char *findXlatCode(char cValue, XLAT_CODE *pTable);

// Search for standardized exemption type
char *findExeType(char *pExeCode, char *pExeType, IDX_TBL4 *pExeTypeTbl);
char *makeExeType(char *pExeType, char *pExeCode1, char *pExeCode2, char *pExeCode3, IDX_TBL4 *pExeXlat);

char *findVesting(char *pName, char *pVestCode);      // Return pointer to vesting in name
int  loadVesting(char *pCntyCode, char *pVestFile);
bool isVestChk(char *pVesting);
int  updateVesting(char *pCntyCode, char *pName, char *pVesting, char *pEtal=NULL);

#endif
