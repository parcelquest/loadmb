#ifndef _MERGETUL_H_
#define _MERGETUL_H_

#define TUL_CHAR_FEEPARCEL                0
#define TUL_CHAR_POOLSPA                  1
#define TUL_CHAR_CATTYPE                  2
#define TUL_CHAR_QUALITYCLASS             3
#define TUL_CHAR_YRBLT                    4
#define TUL_CHAR_BUILDINGSIZE             5
#define TUL_CHAR_ATTACHGARAGESF           6
#define TUL_CHAR_DETACHGARAGESF           7
#define TUL_CHAR_CARPORTSF                8
#define TUL_CHAR_HEATING                  9
#define TUL_CHAR_COOLINGCENTRALAC         10
#define TUL_CHAR_COOLINGEVAPORATIVE       11
#define TUL_CHAR_COOLINGROOMWALL          12
#define TUL_CHAR_COOLINGWINDOW            13
#define TUL_CHAR_STORIESCNT               14
#define TUL_CHAR_UNITSCNT                 15
#define TUL_CHAR_TOTALROOMS               16
#define TUL_CHAR_EFFYR                    17
#define TUL_CHAR_PATIOSF                  18
#define TUL_CHAR_BEDROOMS                 19
#define TUL_CHAR_BATHROOMS                20
#define TUL_CHAR_HALFBATHS                21
#define TUL_CHAR_FIREPLACE                22
#define TUL_CHAR_ASMT                     23
#define TUL_CHAR_BLDGSEQNUM               24
#define TUL_CHAR_HASWELL                  25
#define TUL_CHAR_LOTSQFT                  26
#define TUL_CHAR_PARKSPACE                27

#define TUL_L_ASMT                        0
#define TUL_L_TAXYEAR                     1
#define TUL_L_ROLLCATEGORY                2
#define TUL_L_FEEPARCEL                   3
#define TUL_L_ORIGINATINGASMT             4
#define TUL_L_STATUS                      5
#define TUL_L_TRA                         6
#define TUL_L_TAXABILITY                  7
#define TUL_L_ACRES                       8
#define TUL_L_USECODE                     9
#define TUL_L_CURRENTMARKETLANDVALUE      10
#define TUL_L_CURRENTFIXEDIMPRVALUE       11
#define TUL_L_CURRENTSTRUCTURALIMPRVALUE  12
#define TUL_L_CURRENTPERSONALPROPVALUE    13
#define TUL_L_CURRENTPERSONALPROPMHVALUE  14
#define TUL_L_CURRENTNETVALUE             15
#define TUL_L_BILLEDMARKETLANDVALUE       16
#define TUL_L_BILLEDFIXEDIMPRVALUE        17
#define TUL_L_BILLEDSTRUCTURALIMPRVALUE   18
#define TUL_L_BILLEDPERSONALPROPVALUE     19
#define TUL_L_BILLEDPERSONALPROPMHVALUE   20
#define TUL_L_BILLEDNETVALUE              21
#define TUL_L_OWNER                       22
#define TUL_L_ASSESSEE                    23
#define TUL_L_MAILADDRESS1                24
#define TUL_L_MAILADDRESS2                25
#define TUL_L_MAILADDRESS3                26
#define TUL_L_EXEMPTIONCODE1              27
#define TUL_L_EXEMPTIONAMT1               28
#define TUL_L_EXEMPTIONCODE2              29
#define TUL_L_EXEMPTIONAMT2               30
#define TUL_L_EXEMPTIONCODE3              31
#define TUL_L_EXEMPTIONAMT3               32
#define TUL_L_BILLDATE                    33
#define TUL_L_DUEDATE1                    34
#define TUL_L_DUEDATE2                    35
#define TUL_L_TAXAMT1                     36
#define TUL_L_TAXAMT2                     37
#define TUL_L_PENAMT1                     38
#define TUL_L_PENAMT2                     39
#define TUL_L_COST1                       40
#define TUL_L_COST2                       41
#define TUL_L_COLLECTIONNUM1              42
#define TUL_L_COLLECTIONNUM2              43
#define TUL_L_UNSDELINQPENAMTPAID1        44
#define TUL_L_SECDELINQPENAMTPAID2        45
#define TUL_L_TOTALFEESPAID1              46
#define TUL_L_TOTALFEESPAID2              47
#define TUL_L_TOTALFEES                   48
#define TUL_L_PRORATIONFACTOR             49
#define TUL_L_PRORATIONPCT                50
#define TUL_L_DAYSTOFISCALYEAREND         51
#define TUL_L_DAYSOWNED                   52
#define TUL_L_SITUS1                      53
#define TUL_L_SITUS2                      54
#define TUL_L_ASMTROLLYEAR                55
#define TUL_L_PARCELDESCRIPTION           56
#define TUL_L_BILLCOMMENTSLINE1           57
#define TUL_L_BILLCOMMENTSLINE2           58
#define TUL_L_DEFAULTNUM                  59
#define TUL_L_SECDELINQFISCALYEAR         60
#define TUL_L_PRIORTAXPAID1               61
#define TUL_L_PENINTERESTCODE             62
#define TUL_L_FOURPAYPLANNUM              63
#define TUL_L_EXISTSLIEN                  64
#define TUL_L_EXISTSCORTAC                65
#define TUL_L_EXISTSCOUNTYCORTAC          66
#define TUL_L_EXISTSBANKRUPTCY            67
#define TUL_L_EXISTSREFUND                68
#define TUL_L_EXISTSDELINQUENTVESSEL      69
#define TUL_L_EXISTSNOTES                 70
#define TUL_L_EXISTSCRITICALNOTE          71
#define TUL_L_EXISTSROLLCHG               72
#define TUL_L_ISPENCHRGCANCELED1          73
#define TUL_L_ISPENCHRGCANCELED2          74
#define TUL_L_ISPERSONALPROPERTYPENALTY   75
#define TUL_L_ISAGPRESERVE                76
#define TUL_L_ISCARRYOVER1STPAID          77
#define TUL_L_ISCARRYOVERRECORD           78
#define TUL_L_ISFAILURETOFILE             79
#define TUL_L_ISOWNERSHIPPENALTY          80
#define TUL_L_ISELIGIBLEFOR4PAY           81
#define TUL_L_ISNOTICE4SENT               82
#define TUL_L_ISPARTPAY                   83
#define TUL_L_ISFORMATTEDADDRESS          84
#define TUL_L_ISADDRESSCONFIDENTIAL       85
#define TUL_L_SUPLCNT                     86
#define TUL_L_ORIGINALDUEDATE1            87
#define TUL_L_ORIGINALDUEDATE2            88
#define TUL_L_ISPEN1REFUND                89
#define TUL_L_ISPEN2REFUND                90
#define TUL_L_ISDELINQPENREFUND           91
#define TUL_L_ISREFUNDAUTHORIZED          92
#define TUL_L_ISNODISCHARGE               93
#define TUL_L_ISAPPEALPENDING             94
#define TUL_L_OTHERFEESPAID               95
#define TUL_L_FOURPAYREASON               96
#define TUL_L_DATEDISCHARGED              97
#define TUL_L_ISONLYFIRSTPAID             98
#define TUL_L_ISALLPAID                   99
#define TUL_L_DTS                         100
#define TUL_L_USERID                      101

static XLAT_CODE  asHeating[] =
{  // 11/23/2022
   // Value, lookup code, value length
   "B", "B", 1,               // FAU (Force air)
   "C", "Z", 1,               // Central
   "P", "6", 1,               // Portable
   "E", "7", 1,               // Electric
   "F", "C", 1,               // Floor
   "WD","S", 2,               // Wood stove
   "W", "D", 1,               // Wall
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{  // 11/23/2022
   // Value, lookup code, value length
   "PS", "C", 2,              // Pool and spa
   "PWS","C", 3,              // Pool and spa
   "P",  "P", 1,              // Pool
   "S",  "S", 1,              // Spa
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{  // Not used 12/22/2022
   // Value, lookup code, value length
   "0", "N", 1,                // None
   "1", "L", 1,                // Masonry
   "2", "Z", 1,                // Zero Clearance
   "3", "W", 1,                // Wood Stove
   "4", "S", 1,                // Pellet Stove
   "5", "I", 1,                // Fireplace Insert
   "6", "G", 1,                // LPG Fireplace (Gas Log)
   "7", "O", 1,                // Other
   "",   "", 0
};

static STRSFX asSfxTbl[] = 
{
   "RD",  "ROAD","2",  4,
   "DR",  "DRIV","5",  4,
   "BLVD","BLVD","4",  4,
   "LN",  "LANE","21", 4,
   "ST",  "STRE","1",  4,
   "CIR", "CIRC","10", 4,
   "CIR", "CIR", "10", 3,
   "TRL", "TRAI","35", 4,
   "TRL", "TRL", "35", 3,
   "WAY", "WAY", "38", 3,
   "AVE", "AVEN","3",  3,
   "AVE", "AVE", "3",  3,
   "PL",  "PLAC","27", 2,
   "PL",  "PL",  "27", 2,
   "CT",  "COUR","14", 4,
   "CT",  "CT",  "14", 2,
   "PARK","PARK","26", 4,
   "TER", "TERR","33", 4,
   "LOOP","LOOP","22", 4,
   "PT",  "POIN","169",4,
   "PT",  "PT",  "169",2,
   "RD",  "RD",  "2",  2,
   "DR",  "DR",  "5",  2,
   "LN",  "LN",  "21", 2,
   "ST",  "ST",  "1",  2,
   "RUN", "RUN", "183",3,
   "CUT", "CUTO","15", 4,
   "HWY", "HWY", "20", 3,
   "PKWY","PKWY","28", 4,
   "TER", "TER", "33", 3,
   "","","",0
};

// Use to translate sale's doc code to standard DocType
IDX_TBL5 TUL_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "01", "1 ", 'N', 2, 2,        // Grand Deed - Transfer (DTT)
   "02", "75", 'Y', 2, 2,        // Partial Transfer
   "03", "75", 'Y', 2, 2,        // Partial Non-Reappraisable Event
   "04", "57", 'N', 2, 2,        // Non Sale 100% - Reappraise
   "05", "1 ", 'N', 2, 2,        // New Mobile Home
   "06", "74", 'Y', 2, 2,        // 100% CIO SP Not Equal to Market
   "86", "75", 'Y', 2, 2,        // 100% No reappraise
   "A1", "74", 'Y', 2, 2,        // CON - AG DAIRY IMPS/REVIEW
   "A2", "74", 'Y', 2, 2,        // CON - AG BLDG. IMPS/REVIEW
   "A3", "74", 'Y', 2, 2,        // CON - AG IRRIG. IMPS/REVIEW
   "A4", "74", 'Y', 2, 2,        // CON - AG GROWING IMPS/REVIEW
   "A5", "74", 'Y', 2, 2,        // CON - AG MACH./FROST/TRELLIS
   "A6", "74", 'Y', 2, 2,        // CON - AG MISC. (COMMENT REQ.)
   "C1", "74", 'Y', 2, 2,        // New Commerical Building
   "C2", "74", 'Y', 2, 2,        // New Tenant Improvments
   "C3", "74", 'Y', 2, 2,        // Commerical Building Remodel
   "C4", "74", 'Y', 2, 2,        // Commerical Building Addition
   "C5", "74", 'Y', 2, 2,        // Demo Commerical Building
   "C6", "74", 'Y', 2, 2,        // Commerical Calamity
   "C7", "74", 'Y', 2, 2,        // Commerical Calamity Repair
   "C8", "74", 'Y', 2, 2,        // Commerical Building change in use
   "CC", "74", 'Y', 2, 2,        // CERT. OF COMPLIANCE
   "CM", "74", 'Y', 2, 2,        // CALAMITY/MISFORTUNE REVIEW
   "CP", "74", 'Y', 2, 2,        // CONDOMINIUM PLAN
   "CS", "74", 'Y', 2, 2,        // COMBINATION/SEGREGATION
   "LA", "74", 'Y', 2, 2,        // MAP - LOT LINE ADJUSTMENT
   "LE", "74", 'Y', 2, 2,        // Leop - Reappraise
   "LR", "74", 'Y', 2, 2,        // REVIEW - LIEN DATE
   "M1", "74", 'Y', 2, 2,        // Add MH
   "M2", "74", 'Y', 2, 2,        // MH addition/Construction
   "MH", "74", 'Y', 2, 2,        // MOBILEHOME IMPROVEMENTS
   "MI", "74", 'Y', 2, 2,        // CON - MISC. IMPS (COMMENT REQ)
   "MM", "74", 'Y', 2, 2,        // MAP - MISC. (COMMENT REQUIRED)
   "OP", "74", 'Y', 2, 2,        // MAP - OFFICIAL PLAT
   "OR", "74", 'Y', 2, 2,        // MAP - OFFICIAL RECORDS
   "PM", "74", 'Y', 2, 2,        // MAP - PARCEL MAP
   "PS", "74", 'Y', 2, 2,        // Past Statute - Reappraise and Roll Correction
   "R1", "74", 'Y', 2, 2,        // CON - RES. NEW BUILDING
   "R2", "74", 'Y', 2, 2,        // CON - RES. GARAGE/CARPORT
   "R3", "74", 'Y', 2, 2,        // CON - RES. POOL AND/ OR SPA
   "R4", "74", 'Y', 2, 2,        // CON - RES. BLDG. ADDITION
   "R5", "74", 'Y', 2, 2,        // CON - RES. PATIO
   "R6", "74", 'Y', 2, 2,        // CON - RES. REMODEL/REHAB
   "R7", "74", 'Y', 2, 2,        // CON - RES. RELOCATION
   "R8", "74", 'Y', 2, 2,        // CON - RES. DEMOLITION DUE TO CALAMITY
   "R9", "74", 'Y', 2, 2,        // CON - RES. DEMOLITION
   "RA", "74", 'Y', 2, 2,        // CON - NEW MULTI RES BUILDING
   "RB", "74", 'Y', 2, 2,        // CON - CONVERT GARAGE/SHOP TO LIVING AREA
   "RC", "74", 'Y', 2, 2,        // CON - RES FIRE DAMAGE REPAIR
   "RD", "74", 'Y', 2, 2,        // CON - RES ADU
   "RE", "74", 'Y', 2, 2,        // MAP - REVIEW OR REDRAW
   "RF", "74", 'Y', 2, 2,        // CON - ADDITION MICS STRUCTURE
   "RM", "74", 'Y', 2, 2,        // MAP - RECORDED OFFICIAL MAP
   "RS", "74", 'Y', 2, 2,        // MAP - RECORD OF SURVEY
   "SA", "74", 'Y', 2, 2,        // MAP - STREET ABANDONMENT
   "TC", "74", 'Y', 2, 2,        // MAP - TRA CODE BOUNDARY CHG
   "UI", "74", 'Y', 2, 2,        // UNDIVIDED INTEREST
   "UM", "74", 'Y', 2, 2,        // MAP - UNRECORDED MAP
   "US", "74", 'Y', 2, 2,        // MAP - UNRECORDED SURVEY
   "","",0,0,0
};

// Use to translate GrGr's doc title to standard DocType
IDX_TBL5 TUL_DocTitle[] =
{  // DocTitle, Index, Non-sale, len1, len2
   "DEED ",    "1 ", 'N', 5, 2,  // GRAND DEED
   "ASGT",     "7 ", 'Y', 4, 2,  // ASSIGNMENT 
   "TAXL",     "46", 'N', 4, 2,  // TAX LEIN
   "NOTD",     "52", 'Y', 4, 2,  // NOTICE OF DEFAULT
   "AFF",      "6 ", 'Y', 3, 2,  // AFFIDAVIT
   "DEED-NC",  "13", 'Y', 7, 2,  // DEED - NO CHANGE OF OWNERSHIP
   "DEED-NO",  "13", 'N', 7, 2,  // DEED NO CHARGE FOR RECORDING
   "DEEDTX",   "67", 'N', 6, 2,  // TAX DEED
   "DEEDLF",   "78", 'Y', 6, 2,  // DEED IN LIEU OF FORCLOSURE
   "DEEDTR",   "27", 'Y', 6, 2,  // TRUSTEE'S DEED
   "TAXIRS",   "46", 'N', 6, 2,  // IRS TAX LEIN
   "ML",       "46", 'N', 2, 2,  // MECHANIC LIEN
   "","",' ',0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 TUL_Exemption[] = 
{
   "E01", "H", 3,1,
   "E10", "D", 3,1,
   "E12", "D", 3,1, 
   "E14", "X", 3,1,     // AIRCRAFT OF HISTORICAL SIGNIF
   "E15", "X", 3,1,     // RAILROAD EXEMPTION
   "E16", "E", 3,1,     // 
   "E18", "X", 3,1,     // BANK OR FINANCIAL EXEMPTION 
   "E21", "W", 3,1,
   "E31", "C", 3,1,
   "E32", "R", 3,1,
   "E50", "U", 3,1,
   "E51", "G", 3,1,     // GOVERNMENT ENTITIES EXEMPTION
   "E52", "P", 3,1,
   "E53", "M", 3,1,
   "E60", "P", 3,1,     // PARTIAL PUBLIC SCHOOL
   "E61", "P", 3,1,     // PARTIAL PUBLIC SCHOOL
   "E62", "P", 3,1,     // PARTIAL PUBLIC SCHOOL
   "E63", "P", 3,1,     // PARTIAL PUBLIC SCHOOL
   "E70", "W", 3,1,
   "E71", "W", 3,1,
   "E72", "W", 3,1,
   "E73", "W", 3,1,
   "E80", "C", 3,1,
   "E81", "C", 3,1,
   "E82", "C", 3,1,
   "E83", "C", 3,1,
   "E90", "C", 3,1,
   "E91", "C", 3,1, 
   "E92", "C", 3,1,
   "E93", "C", 3,1,
   "E94", "X", 3,1,     // LOW VALUE EXEMPTION UNSECURED
   "E95", "X", 3,1,     // LOW VALUE EXEMPTION SECURED
   "P19", "X", 3,1,     // Prop 19 - For BYVT 19 Reporting Only
   "","",0,0
};

#endif