/***************************************************************************
 *
 * Notes:
 *    - HUM is changing file format from CSV to TXT.  Tax file layout is also changed.
 *
 * Revision
 * 01/25/2008 1.10.3.1 Add code to support standard usecode
 * 02/29/2008 1.10.5   Add code to output update records.
 * 05/02/2008 1.10.8   County has removed ConfirmSalePrice from Hum_Sales.csv.
 *                     Modify Hum_MergeSale() to support the change.
 * 06/02/2008 1.11.1   Modify MergeOwner() to keep owner name as is.  Adding
 *                     'OF' to company list in ProdLib so names that contain 'OF'
 *                     won't be parsed (DLL version 2.2.4). Also update
 *                     asVestCode3, asVestCode4, and asVestCode5 tables.
 * 06/16/2008 1.12.2   Fig bug in Hum_MergeOwner() that drops off last character
 *                     and convert lower case to upper case
 * 07/11/2008 8.1.1    HUM is now sending TR601 file format for LDR.  Add function 
 *                     Hum_Load_LDR(), Hum_MergeLien(), Hum_MergeSitus(),
 *                     Hum_MergeMAdr() for TR601 input format.
 * 12/10/2008 8.5.1    Modify Hum_MergeMAdr() to remove quote around city name.
 * 12/10/2008 8.5.2    Fix Hum_MergeMAdr() to put Zip & Zip4 on M_CTY_ST_D field.  
 *                     This has been cleared out when we fix the bug on 12/10.
 * 03/30/2009 8.7.1    Use MB_MergeSale() to do sale update instead of MergeSale3() to fix 
 *                     problem that non-sale transaction overwrites sale transaction that  
 *                     occurs in the same day  since we only keep one transaction per day.
 * 05/21/2009 8.7.6    Remove reference to OldApn & NewApn files.  We no longer need them.
 * 07/14/2009 9.1.2    Fix Heating/Cooling bug in Hum_MergeChar().  Modify Hum_MergeLien()
 *                     to add other values and replace replChar() with replNull().
 * 07/18/2010 10.1.0   Add option -Xs to extract sale data.  Modify Hum_MergeLien() to add 
 *                     TAXABILITY and update LDRRecCount.  Rename CreateHumRoll() to Hum_Load_Roll()
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 06/01/2011 10.9.0   Exclude HomeSite from OtherVal.  Replace Hum_MergeExe() with MB_MergeExe()
 *                     and Hum_MergeTax() with MB_MergeTax(). Modify MergeSitus() too.
 * 07/12/2011 11.1.3   Add S_HSENO. Modify Hum_MergeSitus() by replacing parseMAdr1() with 
 *                     parseMAdr1_4() to pick up more UnitNo.
 * 05/24/2012 11.15.1  Add option to load tax file into tax database.
 *                     Update asHeating[] & asCooling[]. Add Hum_ExtrChar() to load Hum_Char.txt
 *                     to STDCHAR format. Remove preceeding space for careof in Hum_MergeMAdr().
 *                     Modify Hum_MergeSale() to load Hum_Sales.txt and add Hum_MergeSale1()
 *                     to load Hum_Sales.csv. Rename Hum_MergeChar() to Hum_MergeChar1() and
 *                     modify Hum_MergeChar() to merge CHARS using STDCHAR format.
 *                     Modify Hum_Load_Roll() to load *.txt input files and Hum_Load_Roll1()
 *                     to load *.csv files.
 * 07/21/2012 12.2.2   Fix sale date in Hum_MergeSale() since Hum_Sales.txt now has new format.
 *                     Modify loadHum() to load old char file Hum_Char.csv until new char file
 *                     Hum_Char.txt is corrected.
 * 04/11/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 07/17/2013 13.2.5   Modify Hum_Load_LDR() to correct chars issue. Adding HUM_DocCode[]
 *                     table and waiting for data from county.
 * 07/22/2013 13.4.7   Adding option to create unknown Usecode file. Updating HUM_DocCode[]
 *                     and calling MB_CreateSCSale() with DocCode table.
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 10/23/2013 13.17.4  Add -Z option and modify loadHum() to unzip data for processing.
 * 01/12/2014 13.19.0  Update DocCode[] table. Remove sale update from Hum_Load_Roll() and 
 *                     use ApplyCumSale() to update sale history. 
 * 11/06/2014 14.9.1   Add log msg to output number of parcels with LotSqft
 * 07/28/2015 15.1.0   Add DBA to Hum_MergeMAdr() and remove comma from M_CTY_ST_D.
 * 07/31/2015 15.2.0   Add new Hum_MergeOwner() that uses CareOf to populate Name2 if CareOf starts with '&'
 *                     and modify Hum_MergeMAdr() to use unformatted mailing when formatted addr is not populated.
 *                     Also check for CareOf and DBA in mailaddr when CareOf and DBA not populated.
 * 08/21/2015 15.2.5   Add Hum_ConvStdChar() to convert new CHAR format.  Modify Hum_MergeChar() to support new file.
 *                     Allow -Xa option to be independence from load LDR or roll update.
 * 08/24/2015 15.2.6   Use old char data if fail to convert new char file.
 * 08/25/2015 15.2.6.3 Modify Hum_ConvStdChar() & Hum_MergeChar() to fix bugs and add more fields.
 *                     Rename Hum_MergeChar() to Hum_MergeStdChar().
 * 08/31/2015 15.2.6.4 Fix Stories and FirePl in Hum_ConvStdChar() & Hum_MergeChar()
 * 10/13/2015 15.4.0   Use MB_Load_TaxBase() instead of MB_Load_TaxRollInfo() to import 
 *                     data into Tax_Base & Tax_Owner tables.
 * 01/13/2016 15.10.1  Remove old situs in Cal_MergeSitus() before update to avoid remnant from old addr.
 * 07/22/2016 16.0.6   Modify Hum_MergeSitus() to fix bad suffix.  Add Hum_Load_LDR3() to support new LDR layout.
 * 08/14/2016 16.3.0   Make legal upper case.
 * 02/07/2017 16.9.1   Add Hum_CleanupHistSale()
 * 09/12/2018 18.5.1   Add TAX_UPDATING option to update tax paid only
 * 10/05/2018 18.6.1   Modify _MergeRoll() & _MergeLien3() to update TRANSFER only if DOCNUM is valid.
 * 02/09/2019 18.10.1  Add -Mz option to load zoning data from Denise.
 * 07/16/2019 19.0.4   Add AgPreserve in Hum_MergeLien3().
 * 07/20/2019 19.1.0   Remove duplicate code, remove hyphen in APN & TRA in MergeLien3().
 *                     Modify Hum_Load_LDR3() not to use lien extract to update value.
 * 03/26/2020 19.8.0.1 Add MergeZoning.h and call MergeZoming() instead of MB_LoadZoning()
 * 07/20/2020 20.2.1   Modify Hum_MergeRoll() to reset OFF_ZONE.
 * 11/01/2020 20.4.2   Modify Hum_MergeRoll() to populate default PQZoning.
 * 07/16/2021 21.0.9   Modify Hum_MergeStdChar() to populate QualityClass.
 * 10/14/2021 21.2.8   Modify Hum_ConvStdChar(), Hum_MergeStdChar(), Hum_MergeRoll() 
 *                     to fix negative value in LotAcres & LotSqft.
 * 07/10/2023 23.0.5   Modify Hum_MergeLien3() to filter out LotSqft > 999999999.
 * 07/23/2024 24.0.3   Modify Hum_MergeLien3() to add ExeType.
 * 08/07/2024 24.0.5   Modify Merge_Roll() to include book 905 (mineral right)
 *                     Modify Hum_MergeOwner() to fix known bad char.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "CharRec.h"
#include "PQ.h"
#include "Tax.h"
#include "doZip.h"
#include "MergeHum.h"

extern long lLotSqftCount;

/**************************** Hum_ConvStdChar ********************************
 *
 * Copy from MergeYol.cpp to convert new char file. 08/21/2015
 * MER is similar without LandSqft at the end.
 * Other counties has similar format: MER, MNO, YOL, HUM, MAD, SBT
 * 
 *****************************************************************************/

int Hum_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   ULONG    lSqft;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_char.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort char file %s to %s", pInfile, acTmpFile);
   iRet = sortFile(pInfile, acTmpFile, "S(#1,C,A)");
   if (iRet < 500)
   {
      LogMsg("***** Input file is too small.");
      return 1;
   }

   if (!(fdIn = fopen(acTmpFile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Start loop
   while (!feof(fdIn))
   {
      // Get next record
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec || acBuf[0] > '9')
         break;

      replStrAll(acBuf, "|NULL", "|");
      iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < HUM_CHAR_HASWELL)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[HUM_CHAR_ASMT], strlen(apTokens[HUM_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[HUM_CHAR_FEEPARCEL], strlen(apTokens[HUM_CHAR_FEEPARCEL]));

      // Format APN
      if (*apTokens[HUM_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[HUM_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[HUM_CHAR_FEEPARCEL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[HUM_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[HUM_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - not avail
      //iTmp = blankRem(apTokens[HUM_CHAR_POOLSPA]);
      //if (iTmp > 1)
      //{
      //   pRec = findXlatCode(apTokens[HUM_CHAR_POOLSPA], &asPool[0]);
      //   if (pRec)
      //      myCharRec.Pool[0] = *pRec;
      //}

      // QualityClass 
      iTmp = remChar(apTokens[HUM_CHAR_QUALITYCLASS], ' ');    // Remove all blanks before process
      pRec = _strupr(apTokens[HUM_CHAR_QUALITYCLASS]);
      if (iTmp > SIZ_CHAR_QCLS)
         iTmp = SIZ_CHAR_QCLS;
      memcpy(myCharRec.QualityClass, apTokens[HUM_CHAR_QUALITYCLASS], iTmp);

      if (*apTokens[HUM_CHAR_QUALITYCLASS] > '0' && *apTokens[HUM_CHAR_QUALITYCLASS] <= 'Z')
      {
         acCode[0] = ' ';
         strcpy(acTmp, apTokens[HUM_CHAR_QUALITYCLASS]);
         if (isalpha(acTmp[0])) 
         {
            myCharRec.BldgClass = acTmp[0];

            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            else if (isdigit(acTmp[2]))
               iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
            else if (!_memicmp(acTmp, "ST", 2))
               memcpy(myCharRec.QualityClass, "ST", 2);     // Steel
         } else if (isdigit(acTmp[0]))
            iRet = Quality2Code(acTmp, acCode, NULL);
         else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[HUM_CHAR_QUALITYCLASS], apTokens[HUM_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (*apTokens[HUM_CHAR_QUALITYCLASS] > ' ' && *apTokens[HUM_CHAR_QUALITYCLASS] != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[HUM_CHAR_QUALITYCLASS], apTokens[HUM_CHAR_ASMT]);

      // YrBlt
      int iYrBlt = atoi(apTokens[HUM_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[HUM_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[HUM_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[HUM_CHAR_UNITSCNT]);
      //if (iTmp > 0 && iBldgSize > iTmp*100)
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 
      //else if (iTmp > 1)
      //   LogMsg("*** Questionable UnitsCnt: %d, BldgSqft=%d, Apn=%s", iTmp, iBldgSize, apTokens[HUM_CHAR_ASMT]);

      // Stories/NumFloors
      iTmp = atoi(apTokens[HUM_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 99)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[HUM_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[HUM_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport Sqft
      int iCarport = atoi(apTokens[HUM_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Patio SF
      iTmp = atoi(apTokens[HUM_CHAR_PATIOSF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }


#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "001042001000", 9))
      //   iRet = 0;
#endif

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[HUM_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[HUM_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[HUM_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[HUM_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[HUM_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[HUM_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[HUM_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[HUM_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[HUM_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace - 01, 02, 05, 06, 99
      if (*apTokens[HUM_CHAR_FIREPLACE] >= '0' && *apTokens[HUM_CHAR_FIREPLACE] <= '9')
      {
         pRec = findXlatCode(apTokens[HUM_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      } 

      // Haswell - not avail
      blankRem(apTokens[HUM_CHAR_HASWELL]);
      if (*(apTokens[HUM_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Lot Sqft
      if (iFldCnt >= HUM_CHAR_LOTSQFT)
      {
         lSqft = atol(apTokens[HUM_CHAR_LOTSQFT]);
         if (lSqft > 1)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(lSqft*1000)/SQFT_PER_ACRE;
            iTmp = sprintf(acTmp, "%u", (long)(dTmp+0.5));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum, UnitSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/****************************** Hum_ExtrChar() ******************************
 *
 * Load Hum_Char.txt to STDCHAR format.
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

int Hum_ExtrChar()
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmpFile[256], acTmp[256], acCode[64], *pRec, *pTmp;
   int      iRet, iTmp, iCmp, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nExtracting char file %s\n", acCharFile);

   if (!(fdIn = fopen(acCharFile, "r")))
   {
      LogMsg("***** Error open input file %s.  Please check for file exist.", acCharFile);
      return -1;
   }

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);

      if (!pRec)
         break;

      if (cDelim == ',')
         iRet = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iRet = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iRet < MB2_CHAR_BLDGTYPE)
      {
         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));

      // Apn
      memcpy(myCharRec.Apn, apTokens[MB2_CHAR_ASMT], strlen(apTokens[MB2_CHAR_ASMT]));
      
      // Fee parcel
      memcpy(myCharRec.FeeParcel, apTokens[MB2_CHAR_FEE_PRCL], strlen(apTokens[MB2_CHAR_FEE_PRCL]));
      
      // Use cat
      memcpy(myCharRec.LandUseCat, apTokens[MB2_CHAR_USECAT], strlen(apTokens[MB2_CHAR_USECAT]));

      // Quality Class
      if (*apTokens[MB2_CHAR_QUALITY] > ' ')
      {
         strcpy(acTmp, apTokens[MB2_CHAR_QUALITY]);
         pTmp = _strupr(acTmp);
         if (*(pTmp+1) == '-')
         {
            pTmp++;
            strcpy(pTmp, pTmp+1);
            if (*(pTmp+1) == '-')
               strcpy(pTmp+1, pTmp+2);
         }
         memcpy(myCharRec.QualityClass, acTmp, strlen(acTmp));
         acCode[0] = 0;
         if (isalpha(acTmp[0]))
         {
            myCharRec.BldgClass = acTmp[0];
            if (isdigit(acTmp[1]))
               iRet = Quality2Code(&acTmp[1], acCode, NULL);
            else if (isdigit(acTmp[2]))
               iRet = Quality2Code(&acTmp[2], acCode, NULL);
            else if (acTmp[1] == 'A')
               acCode[0] = 'A';
         } else if (isdigit(acTmp[0]))
         {
            iTmp = atol(acTmp);
            if (iTmp < 100)
               iRet = Quality2Code(acTmp, acCode, NULL);
         }

         if (acCode[0] > '0')
            myCharRec.BldgQual = acCode[0];
      }

      // YrBlt
      iTmp = atoi(apTokens[MB2_CHAR_YRBLT]);
      if (iTmp > 1600 && iTmp <= lLienYear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // BldgSqft
      iTmp = atoi(apTokens[MB2_CHAR_BLDGSQFT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // GarSqft
      iTmp = atoi(apTokens[MB2_CHAR_GARSQFT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
      }

      // Heating
      if (*apTokens[MB2_CHAR_HEATING] > ' ')
      {
         iTmp = 0;
         iCmp = -1;
         while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[MB2_CHAR_HEATING], asHeating[iTmp].acSrc, asHeating[iTmp].iLen)) < 0)
            iTmp++;

         if (!iCmp)
            myCharRec.Heating[0] = asHeating[iTmp].acCode[0];
      }

      // Cooling
      if (*apTokens[MB2_CHAR_COOLING] > ' ')
      {
         iTmp = 0;
         iCmp = -1;
         blankRem(apTokens[MB2_CHAR_COOLING]);
         while (asCooling[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[MB2_CHAR_COOLING], asCooling[iTmp].acSrc, asCooling[iTmp].iLen)) < 0)
            iTmp++;

         if (!iCmp)
         {
            myCharRec.Cooling[0] = asCooling[iTmp].acCode[0];
         }
      }

      // Heating src
      blankRem(apTokens[MB2_CHAR_HEATING_SRC]);
      memcpy(myCharRec.HeatingSrc, apTokens[MB2_CHAR_HEATING_SRC], strlen(apTokens[MB2_CHAR_HEATING_SRC]));

      // Cooling src
      blankRem(apTokens[MB2_CHAR_COOLING_SRC]);
      memcpy(myCharRec.CoolingSrc, apTokens[MB2_CHAR_COOLING_SRC], strlen(apTokens[MB2_CHAR_COOLING_SRC]));

      // Beds
      iTmp = atoi(apTokens[MB2_CHAR_BEDS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Baths
      iTmp = atoi(apTokens[MB2_CHAR_FBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half baths
      iTmp = atoi(apTokens[MB2_CHAR_HBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // Fireplace
      iTmp = atoi(apTokens[MB2_CHAR_FP]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Fireplace, acTmp, iRet);
      }

      blankRem(apTokens[MB2_CHAR_HASSEPTIC]);
      if (*(apTokens[MB2_CHAR_HASSEPTIC]) > '0')
      {
         myCharRec.HasSeptic = 'Y';
         myCharRec.HasSewer = 'S';
      } else
      {
         blankRem(apTokens[MB2_CHAR_HASSEWER]);
         if (*(apTokens[MB2_CHAR_HASSEWER]) > '0')
            myCharRec.HasSewer = 'Y';
      }

      blankRem(apTokens[MB2_CHAR_HASWELL]);
      if (*(apTokens[MB2_CHAR_HASWELL]) > '0')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Building Type - ??? data available, need standardized translation table
      //BldgType|01|Class A, Structural steel columns and beams
      //BldgType|02|Class B, Reinforced concrete columns and beams
      //BldgType|03|Class C, Masonry or concrete load-bearing walls
      //BldgType|04|Class D, wood or steel studs in bearing walls
      //BldgType|05|Class S, metal bents, columns, w/o fireproofing
      //BldgType|06|Other

      // BldgSeqNum
      iTmp = atoi(apTokens[MB2_CHAR_BLDGSEQNO]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      }

      // UnitSeqNum
      iTmp = atoi(apTokens[MB2_CHAR_UNITSEQNO]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.UnitSeqNo, acTmp, iRet);
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
   } else
      iRet = 0;

   printf("\n");
   return iRet;
}

/******************************** Hum_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 * 06/02/2008  Keep owner name as is.  Parse to create Mail name only.
 *
 *****************************************************************************/

void Hum_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64], acVesting[8], acOwner[SIZ_NAME1+2];
   OWNER myOwner;
   bool  bDecease = false;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);
   strcpy(acTmp1, pNames);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009172022", 9))
   //if (!memcmp(pOutbuf, "880000069", 9))
   //   iTmp = 0;
#endif

   // Replace comma with space
   if (pTmp = strrchr(acTmp1, ','))
      *pTmp = ' ';

   // Remove multiple spaces
   pTmp = acTmp1;
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = toupper(*pTmp);

      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '*' || *pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acName2[0] = 0;
   acSave1[0] = 0;
   acVesting[0] = 0;
   
   // If name ending with CR, it is a company
   // PT = Partnership
   // PL = Public parcel
   // SB = SBE parcel
 
   iTmp1 = iTmp -3;
   pTmp = (char *)&acTmp[iTmp1];
   if (!memcmp(pTmp, " CR", 3) || 
       !memcmp(pTmp, " PT", 3) ||
       !memcmp(pTmp, " PL", 3) ||
       !memcmp(pTmp, " SB", 3) )
   {
      *pTmp++ = 0;
      memcpy(pOutbuf+OFF_VEST, pTmp, 2);
      if (!memcmp(pTmp, "PL", 2) )
         *(pOutbuf+OFF_PUBL_FLG) = 'Y';

      iTmp1 = strlen(acTmp);
      if (iTmp1 > SIZ_NAME1) iTmp1 = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp1);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp1);

      return;
   } else if (!memcmp(pTmp, " ID", 3) )
   {  // Indicated decease
      memcpy(pOutbuf+OFF_VEST, pTmp+1, 2);
      *pTmp = 0;
      iTmp -= 3;
      bDecease = true;
   }

   // Check for vesting on last 6 letters
   pTmp = (char *)&acTmp[iTmp-6];
   iTmp1 = 0;
   while (asVestCode5[iTmp1][0])
   {
      if (!memcmp(pTmp, asVestCode5[iTmp1], 6))
      {
         memcpy(pOutbuf+OFF_VEST, asVestCode4[iTmp1], 4);
         *pTmp = 0;
         break;
      }
      iTmp1++;
   }
   if (*pTmp)
   {
      // Check for vesting on last 4 letters
      pTmp = (char *)&acTmp[iTmp-4];
      iTmp1 = 0;
      while (asVestCode4[iTmp1][0])
      {
         if (!memcmp(pTmp, asVestCode4[iTmp1], 4))
         {
            memcpy(pOutbuf+OFF_VEST, pTmp, 4);
            *pTmp = 0;
            break;
         }
         iTmp1++;
      }
   }
   if (*pTmp)
   {
      // Check for vesting on last 3 letters
      pTmp = (char *)&acTmp[iTmp-3];
      iTmp1 = 0;
      while (asVestCode3[iTmp1][0])
      {
         if (!memcmp(pTmp, asVestCode3[iTmp1], 3))
         {
            if (bDecease)
            {
               strcat(pTmp, "ID");
               memcpy(pOutbuf+OFF_VEST, pTmp+1, 4);
            } else
               memcpy(pOutbuf+OFF_VEST, pTmp+1, 2);
            *pTmp = 0;
            break;
         }
         iTmp1++;
      }
   }
   if (*pTmp)
   {
      // Find last word
      pTmp = strrchr(acTmp, ' ');
      if (pTmp)
      {
         // Prevent HWJTCP ...
         if (!memcmp(pTmp, " HWJT", 5))
         {
            memcpy(pOutbuf+OFF_VEST, pTmp+1, 4);
            *pTmp = 0;
         } else if (!memcmp(pTmp, " MMSE", 5))
         {
            memcpy(pOutbuf+OFF_VEST, pTmp+1, 4);
            *pTmp = 0;
         }
      }
   }
   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp1 = strlen(acTmp);
      if (iTmp1 > SIZ_NAME1) iTmp1 = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp1);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp1);
      return;
   }

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Save owner name
   strncpy(acOwner, acTmp, SIZ_NAME1);
   acOwner[SIZ_NAME1] = 0;

   // If TRUST appears after number, save from number forward
   if (acTmp[iTmp])
   {
      if (pTmp=strstr((char *)&acTmp[0], " PENSION & 401(K)") )
      {
         strcpy(acSave1, pTmp);
         *pTmp = 0;         
      } else if (strstr((char *)&acTmp[iTmp], " TRUST") )
      {
         iTmp--;
         strcpy(acSave1, (char *)&acTmp[iTmp]);
         acTmp[iTmp] = 0;
      }
   }

   // Filter out words
   if ((pTmp=strstr(acTmp, " ETAL")) || 
       (pTmp=strstr(acTmp, " ET AL")) || 
       (pTmp=strstr(acTmp, " & FBO")) ||
       (pTmp=strstr(acTmp, " TRUSTEE")) ||
       (pTmp=strstr(acTmp, " SUC CO TR")) ||
       (pTmp=strstr(acTmp, " SUC TR")) )
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " & TRUST "))
   {
      // Break into two name
      *pTmp = 0;
      strcpy(acName1, acTmp);

      //iTmp1 = strlen(pTmp+3);
      //if (iTmp1 > SIZ_NAME2) iTmp1 = SIZ_NAME2;
      //memcpy(pOutbuf+OFF_NAME2, pTmp+3, iTmp1);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // MARTIN PATSY L TRUST & WILDER EVERETT E & HEWITT B
      // Break names in two parts
      *pTmp = 0;        
      strcpy(acSave1, " TRUST");
      strcpy(acName1, acTmp);

      //iTmp1 = strlen(pTmp+9);
      //if (iTmp1 > SIZ_NAME2) iTmp1 = SIZ_NAME2;
      //memcpy(pOutbuf+OFF_NAME2, pTmp+9, iTmp1);
   }

   // Check for multiple '&' in name
   // If more than one "&", put second part into name2
   if (pTmp = strchr(acTmp, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         char *pTmp2;

         pTmp2 = strchr(pTmp1+2, ' ');
         if (pTmp2)
         {  // SCHWEIGERT STANLEY C & LAWLOR ANN S & GOODRICH J
            iTmp1 = strlen(pTmp2+1);
            if (iTmp1 == 1)
            {
               // Put 2nd and 3rd names into name2
               strcpy(acName2, pTmp+2);
               *pTmp = 0;
            } else
            {
               // Put 3rd names into name2
               strcpy(acName2, pTmp1+2);
               *pTmp1 = 0;
            }
         } else
         {
            // Put 2nd and 3rd names into name2
            strcpy(acName2, pTmp+2);
            *pTmp = 0;
         }
      }
   }
   
   if (pTmp=strstr(acTmp, " REVOCABLE FAMILY TRUST"))
   {
      strcpy(acSave1, " REVOCABLE FAMILY TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY REVOCABLE TRUST"))
   {
      strcpy(acSave1, " REVOCABLE LIVING TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE LIVING TRUST"))
   {
      strcpy(acSave1, " REVOCABLE LIVING TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY LIVING TRUST"))
   {
      strcpy(acSave1, " FAMILY LIVING TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY TRUST"))
   {  
      strcpy(acSave1, " FAMILY TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FRAMILY TRUST"))
   {  
      strcpy(acSave1, " FAMILY TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST"))
   {  
      strcpy(acSave1, " REV TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST"))
   {
      strcpy(acSave1, " LIVING TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REV TRUST"))
   {  
      strcpy(acSave1, " REV TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOC TRUST"))
   {  
      strcpy(acSave1, " REV TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " EXEMPTION TRUST"))
   {  
      strcpy(acSave1, " EXEMPTION TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LAND TRUST"))
   {  
      strcpy(acSave1, " LAND TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " PROPERTY TRUST"))
   {  
      strcpy(acSave1, " PROPERTY TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " SURVIVOR'S TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST UNDER"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST NO"))
   {
      strcpy(acSave1, " TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST DATE"))
   {
      strcpy(acSave1, " TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST TR"))
   {
      strcpy(acSave1, " TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST THE"))
   {  // Reverse the word order
      strcpy(acName1, "THE ");
      *(pTmp+6) = 0;
      strcat(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[strlen(acTmp)-6], " TRUST", 6))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      acTmp[strlen(acTmp)-6] = 0;
      strcpy(acName1, acTmp);
      strcpy(acSave1, " TRUST");
   } else if (pTmp=strstr(acTmp, " LLC LLC"))
   {  // Keep one only
      *pTmp = 0;
      strcpy(acName1, acTmp);
      strcat(acName1, pTmp+4);
   } else
      strcpy(acName1, acTmp);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(acTmp1, myOwner.acName1);
      //if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
      //{
      //   iTmp = strlen(myOwner.acName2);
      //   if (iTmp > SIZ_NAME2)
      //      iTmp = SIZ_NAME2;
      //   memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, iTmp);
      //}
      if (myOwner.acVest[0] > ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // Concat what in saved buffer if it is not " TRUST"
      if (acSave1[0])
      {
         if (acTmp1[strlen(acTmp1)-1] == ' ')
            strcat(acTmp1, (char *)&acSave1[1]);      // avoid double blank
         else
            strcat(acTmp1, acSave1);
      }

      // Save Name1
      //iTmp = strlen(acTmp1);
      //if (iTmp > SIZ_NAME1)
      //   iTmp = SIZ_NAME1;
      //memcpy(pOutbuf+OFF_NAME1, acTmp1, iTmp);

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
      {
         iTmp = strlen(acTmp1);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, iTmp);
      } else
      {
         iTmp = strlen(myOwner.acSwapName);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
      }
   } else
   {
      if (acSave1[0])
      {
         strcat(acName1, acSave1);
         acSave1[0] = 0;
         iTmp = strlen(acName1);
         if (iTmp > SIZ_NAME1)
            iTmp = SIZ_NAME1;
         //memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);
         memcpy(pOutbuf+OFF_NAME_SWAP, acName1, iTmp);
      } else
      {
         // Couldn't split names
         iTmp = strlen(pNames);
         if (iTmp > SIZ_NAME1)
            iTmp = SIZ_NAME1;
         //memcpy(pOutbuf+OFF_NAME1, pNames, iTmp);
         memcpy(pOutbuf+OFF_NAME_SWAP, pNames, iTmp);
         //acName2[0] = 0;
      }
   }

   /* Keep owner name as is - spn 06/02/2008
   // Process Name2 when there is more than one word and has not been populated
   if (acName2[0] && strchr((char *)&acName2[1], ' ') && myOwner.acName2[0] <= ' ')
   {
      if ((pTmp=strstr(acName2, " REVOC")) || (pTmp=strstr(acName2, " FAMILY "))
         || (pTmp=strstr(acName2, " LIVING TR")) )
      {
         iTmp = strlen(acName2);
         if (iTmp > SIZ_NAME2)
            iTmp = SIZ_NAME2;
         memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);
      } else
      {
         acSave1[0] = 0;
         if (pTmp=strstr(acName2, " TRUST"))
         {
            strcpy(acSave1, pTmp);
            *pTmp = 0;
         }

         iRet = splitOwner(acName2, &myOwner, 3);
         if (iRet >= 0)
            strcpy(acName2, myOwner.acName1);

         if (acSave1[0])
            strcat(acName2, acSave1);

         iTmp = strlen(acName2);
         if (iTmp > SIZ_NAME2)
            iTmp = SIZ_NAME2;
         memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);
      }
   }
   */
   iTmp = strlen(acOwner);
   if (acOwner[iTmp-1] == '&')
      iTmp -= 1;
   memcpy(pOutbuf+OFF_NAME1, acOwner, iTmp);
}

/******************************************************************************
 *
 * This function is needed to replace current MergeOwner() and properly assign CareOf
 * since CareOf is sometimes part of Owner Name.
 *
 ******************************************************************************/

void Hum_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[128], acName2[128], acVesting[8], acOwner[SIZ_NAME1+2];
   OWNER myOwner;
   bool  bDecease = false;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);
   acName2[0] = 0;
   acSave1[0] = 0;
   acVesting[0] = 0;

   strcpy(acName1, pNames);
   if (pTmp = strchr(acName1, 0xC1))
      *pTmp = 'A';

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "011101017000", 9))
   //   iTmp = 0;
#endif

   // Check CareOf
   strcpy(acTmp, _strupr(pCareOf));
   blankRem(acTmp);

   // If CareOf start with '&', it is part of Owner name, save it as Name2
   if (acTmp[0] == '&')
   {
      strcpy(acName2, &acTmp[2]);
      *pCareOf = 0;
   } else if (!memcmp(acTmp, "ET AL", 5))
   {
      strcat(acName1, " ");
      strcat(acName1, acTmp);
      *pCareOf = 0;
   }

   // Remove extra suffix
   if (pTmp = strstr(acName1, "LLC LLC"))
      *(pTmp+3) = 0;
   else if (pTmp = strstr(acName1, " LP LP"))
      *(pTmp+3) = 0;
   else if (pTmp = strstr(acName1, " TR TR"))
      *(pTmp+3) = 0;

   // Remove special chars
   remCharEx(acName1, ".,*");

   // Remove multiple spaces
   iTmp = blankRem(acName1);

  
   // If name ending with CR, it is a company
   // PT = Partnership
   // PL = Public parcel
   // SB = SBE parcel
 
   iTmp1 = iTmp -3;
   pTmp = (char *)&acName1[iTmp1];
   if (!memcmp(pTmp, " CR", 3) || 
       !memcmp(pTmp, " PT", 3) ||
       !memcmp(pTmp, " PL", 3) ||
       !memcmp(pTmp, " SB", 3) )
   {
      *pTmp++ = 0;
      memcpy(pOutbuf+OFF_VEST, pTmp, 2);
      if (!memcmp(pTmp, "PL", 2) )
         *(pOutbuf+OFF_PUBL_FLG) = 'Y';

      iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
      memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);
      return;
   } else if (!memcmp(pTmp, " ID", 3) )
   {  // Indicated decease
      memcpy(pOutbuf+OFF_VEST, pTmp+1, 2);
      *pTmp = 0;
      iTmp -= 3;
      bDecease = true;
   } else if (pTmp = strstr(acName1, " ID REC") )
   {  // Indicated decease
      memcpy(pOutbuf+OFF_VEST, pTmp+1, 2);
      *pTmp = 0;
      iTmp = strlen(acName1);
      bDecease = true;
   }
   
   // Check for vesting on last 6 letters
   pTmp = &acName1[iTmp-6];
   iTmp1 = 0;
   while (asVestCode5[iTmp1][0])
   {
      if (!memcmp(pTmp, asVestCode5[iTmp1], 6))
      {
         memcpy(pOutbuf+OFF_VEST, asVestCode4[iTmp1], 4);
         *pTmp = 0;
         break;
      }
      iTmp1++;
   }
   if (*pTmp)
   {
      // Check for vesting on last 4 letters
      pTmp = (char *)&acName1[iTmp-4];
      iTmp1 = 0;
      while (asVestCode4[iTmp1][0])
      {
         if (!memcmp(pTmp, asVestCode4[iTmp1], 4))
         {
            memcpy(pOutbuf+OFF_VEST, pTmp, 4);
            *pTmp = 0;
            break;
         }
         iTmp1++;
      }
   }
   if (*pTmp)
   {
      // Check for vesting on last 3 letters
      pTmp = (char *)&acName1[iTmp-3];
      iTmp1 = 0;
      while (asVestCode3[iTmp1][0])
      {
         if (!memcmp(pTmp, asVestCode3[iTmp1], 3))
         {
            if (bDecease)
            {
               strcat(pTmp, "ID");
               memcpy(pOutbuf+OFF_VEST, pTmp+1, 4);
            } else
               memcpy(pOutbuf+OFF_VEST, pTmp+1, 2);
            *pTmp = 0;
            break;
         }
         iTmp1++;
      }
   }
   if (*pTmp)
   {
      // Find last word
      pTmp = strrchr(acName1, ' ');
      if (pTmp)
      {
         // Prevent HWJTCP ...
         if (!memcmp(pTmp, " HWJT", 5))
         {
            memcpy(pOutbuf+OFF_VEST, pTmp+1, 4);
            *pTmp = 0;
         } else if (!memcmp(pTmp, " MMSE", 5))
         {
            memcpy(pOutbuf+OFF_VEST, pTmp+1, 4);
            *pTmp = 0;
         }
      }
   }
   // Check for year that goes before TRUST
   iTmp =0;
   while (acName1[iTmp])
   {
      if (acName1[iTmp] <= '9')
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
      memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);
      return;
   }

   // Update vesting
   updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Save owner name
   strncpy(acOwner, acName1, SIZ_NAME1);
   acOwner[SIZ_NAME1] = 0;
   strcpy(acTmp, acName1);

   // If TRUST appears after number, save from number forward
   if (acTmp[iTmp])
   {
      if (pTmp=strstr((char *)&acTmp[0], " PENSION & 401(K)") )
         *pTmp = 0;         
      else if (strstr((char *)&acTmp[iTmp], " TRUST") )
      {
         iTmp--;
         acTmp[iTmp] = 0;
      }
   }

   // Filter out words
   if ((pTmp=strstr(acTmp, " ETAL")) || 
       (pTmp=strstr(acTmp, " ET AL")) || 
       (pTmp=strstr(acTmp, " & FBO")) ||
       (pTmp=strstr(acTmp, " TRUSTEE")) ||
       (pTmp=strstr(acTmp, " SUC CO TR")) ||
       (pTmp=strstr(acTmp, " SUC TR")) )
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " & TRUST "))
   {
      // Break into two name
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // MARTIN PATSY L TRUST & WILDER EVERETT E & HEWITT B
      // Break names in two parts
      *pTmp = 0;        
      strcpy(acName1, acTmp);
   }

   // Check for multiple '&' in name
   // If more than one "&", put second part into name2
   if ((pTmp = strchr(acTmp, '&')) && strlen(acTmp) > SIZ_NAME1)
   {
      if (pTmp1 = strchr(pTmp+1, '&')) 
      {
         char *pTmp2;

         pTmp2 = strchr(pTmp1+2, ' ');
         if (pTmp2)
         {  // SCHWEIGERT STANLEY C & LAWLOR ANN S & GOODRICH J
            iTmp1 = strlen(pTmp2+1);
            if (iTmp1 == 1)
            {
               // Put 2nd and 3rd names into name2
               strcpy(acName2, pTmp+2);
               *pTmp = 0;
            } else
            {
               // Put 3rd names into name2
               strcpy(acName2, pTmp1+2);
               *pTmp1 = 0;
            }
         } else
         {
            // Put 2nd and 3rd names into name2
            strcpy(acName2, pTmp+2);
            *pTmp = 0;
         }
      }
   }
   
   if (pTmp=strstr(acTmp, " REVOCABLE FAMILY TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY REVOCABLE TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE LIVING TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY LIVING TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FRAMILY TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REV TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOC TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " EXEMPTION TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LAND TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " PROPERTY TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " SURVIVOR'S TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST UNDER"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST NO"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST DATE"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST TR"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST THE"))
   {  // Reverse the word order
      strcpy(acName1, "THE ");
      *(pTmp+6) = 0;
      strcat(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[strlen(acTmp)-6], " TRUST", 6))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      acTmp[strlen(acTmp)-6] = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LLC LLC"))
   {  // Keep one only
      *(pTmp+4) = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(acTmp1, myOwner.acName1);
      if (myOwner.acVest[0] > ' ' && *(pOutbuf+OFF_VEST) == ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
         iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
      else
         iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
      iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);

   iTmp = strlen(acOwner);
   if (acOwner[iTmp-1] == '&')
      iTmp -= 1;
   memcpy(pOutbuf+OFF_NAME1, acOwner, iTmp);

   if (acName2[0] > ' ')
      iTmp = vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);
}

/******************************** Hum_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Hum_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4, bool bAdrOnly=false)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *pCareOf, *pDba, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64], acDba[128];
   int   iTmp;

#ifdef _DEBUG
   // (!memcmp(pOutbuf, "001011009000", 9))
   //   iTmp = 0;
#endif

   // Initialize
   if (!bAdrOnly)
      removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   pCareOf = pDba = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         pCareOf = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         pCareOf = pLine2;
         p1 = pLine3;
         if (!_memicmp(pLine1, "DBA ", 4) )
            pDba = pLine1+4;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         p1 = pLine3;
         sprintf(acDba, "%s %s", pLine1+4, pLine2);
         pDba = &acDba[0];
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         pCareOf = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         pDba = pLine1+4;
         p1 = pLine2;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
   }

   // Update DBA
   if (pDba && !bAdrOnly)
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

   // Check for C/O
   if (pCareOf && !bAdrOnly)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   strcpy(acAddr2, p2);
   quoteRem(acAddr2);
   remChar(acAddr2, ',');
   iTmp = blankRem(acAddr2);
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D, iTmp);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}

void Hum_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004034005000", 9))
   //   iTmp = 0;
#endif

   // CareOf - can be part of owner name - 222153003000
   _strupr(apTokens[MB_ROLL_M_ADDR1]);
   _strupr(apTokens[MB_ROLL_M_ADDR2]);
   iTmp = blankRem(_strupr(apTokens[MB_ROLL_CAREOF]));
   pTmp = NULL;
   if (iTmp > 6)
   {
      if (!memcmp(apTokens[MB_ROLL_CAREOF], "C/O", 3) || !memcmp(apTokens[MB_ROLL_CAREOF], "C/0", 3) )
      {
         *(apTokens[MB_ROLL_CAREOF]+2) = 'O';
         pTmp = apTokens[MB_ROLL_CAREOF];
      } else if (!memcmp(apTokens[MB_ROLL_CAREOF], "ATTN", 4) ||
         !memcmp(apTokens[MB_ROLL_CAREOF], "ATT:", 4) ||
         !memcmp(apTokens[MB_ROLL_CAREOF], "ATT ", 4) )
         pTmp = apTokens[MB_ROLL_CAREOF];
   }
   if (!pTmp && !memcmp(apTokens[MB_ROLL_M_ADDR1], "C/O", 3))
      pTmp = apTokens[MB_ROLL_M_ADDR1];
   else if (!pTmp && !memcmp(apTokens[MB_ROLL_M_ADDR2], "C/O", 3))
      pTmp = apTokens[MB_ROLL_M_ADDR2];
   if (pTmp)
      updateCareOf(pOutbuf, pTmp, strlen(pTmp));

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   pTmp = NULL;
   if (!memcmp(apTokens[MB_ROLL_DBA], "DBA", 3) )
      pTmp = apTokens[MB_ROLL_DBA] + 4;
   else if (!memcmp(apTokens[MB_ROLL_M_ADDR1], "DBA", 3))
      pTmp = apTokens[MB_ROLL_M_ADDR1] + 4;
   else if (!memcmp(apTokens[MB_ROLL_M_ADDR2], "DBA", 3))
      pTmp = apTokens[MB_ROLL_M_ADDR2] + 4;

   if (pTmp)
   {
      if (*pTmp == ' ') pTmp++;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   }

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   quoteRem(acAddr1);
   blankRem(acAddr1);
   if (acAddr1[0] <= ' ')
   {
      // Mailaddr is not populated here, try unformatted version
      Hum_MergeMAdr(pOutbuf, apTokens[MB_ROLL_M_ADDR1], apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3], apTokens[MB_ROLL_M_ADDR4], true);
      return;
   }

   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      pTmp = apTokens[MB_ROLL_M_CITY];
      quoteRem(apTokens[MB_ROLL_M_CITY]);
      blankRem(apTokens[MB_ROLL_M_CITY]);

      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], SIZ_M_CITY);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      int iZip, iZip4=0;
      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {

         iZip = atoin(apTokens[MB_ROLL_M_ZIP], 5);
         iTmp = strlen(apTokens[MB_ROLL_M_ZIP]);
         if (iTmp == 9)
            iZip4 = atoin(apTokens[MB_ROLL_M_ZIP]+5, 4);

         sprintf(acTmp, "%.5d", iZip);
         memcpy(pOutbuf+OFF_M_ZIP, acTmp, 5);
         if (iZip4 > 0)
         {
            sprintf(acTmp, "%.4d", iZip4);
            memcpy(pOutbuf+OFF_M_ZIP4, acTmp, 4);
         }
      }

      if (iZip > 0)
      {
         if (iZip4 > 0)
            iTmp = sprintf(acTmp, "%s %s %.5d-%.4d", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], iZip, iZip4);
         else
            iTmp = sprintf(acTmp, "%s %s %.5d", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], iZip);
      } else
         iTmp = sprintf(acTmp, "%s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST]);

      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
   }

}

/******************************** Hum_MergeSitus *****************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Hum_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do
      {
         pRec = fgets(acRec, 512, fdSitus);
      } while (!isdigit(acRec[1]));
   }

   pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse situs input
   if (cDelim == ',')
      iTmp = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iTmp = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

   if (iTmp < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old Situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      sprintf(acTmp, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
      pTmp = strchr(acTmp, ' ');
      *(pTmp+1) = 0;
      strcpy(acAddr1, acTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
      }
   }

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      // Fix specific spelling - 07/21/2016
      char acStrSfx[16];
      if (!strcmp(apTokens[MB_SITUS_STRTYPE], "PNT"))
         strcpy(acStrSfx, "PT");
      else if (!strcmp(apTokens[MB_SITUS_STRTYPE], "PLA"))
         strcpy(acStrSfx, "PL");
      else if (!strcmp(apTokens[MB_SITUS_STRTYPE], "PR"))
         strcpy(acStrSfx, "PKWY");
      else
         strcpy(acStrSfx, apTokens[MB_SITUS_STRTYPE]);

      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, acStrSfx);
      vmemcpy(pOutbuf+OFF_S_STREET, apTokens[MB_SITUS_STRNAME], SIZ_S_STREET);

      // Encode suffix
      iTmp = GetSfxCodeX(acStrSfx, acTmp);
      if (iTmp > 0)
      {
         sprintf(acCode, "%d", iTmp);
      } else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         iBadSuffix++;
         acCode[0] = 0;
      }
      vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apTokens[MB_SITUS_STRNAME]);
      if (sAdr.strName[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      }
      if (sAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, strlen(sAdr.strSfx));

      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      memcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], strlen(apTokens[MB_SITUS_UNIT]));
   }

   iTmp = blankRem(acAddr1, SIZ_S_ADDR_D);
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D, iTmp);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1, pOutbuf);   
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
         sprintf(acTmp, "%s CA      ", myTrim(acAddr1));
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int Hum_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004130013", 9))
   //   acTmp[0] = 0;
#endif
   // 
   strcpy(acAddr1, pLine1);
   int iTmp = blankRem(acAddr1);
   if (iTmp < 5)
      return 1;

   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

#ifdef _DEBUG
   // Check for '-'
   //char *pTmp;
   //if (pTmp = isCharIncluded(acAddr1, '-', 0))
   //   pTmp++;
#endif

   // 2830 G ST #STE D-1
   // 3980 CEDAR APTS 1-4 ST
   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1_4(&sSitusAdr, acAddr1);

   memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));

   memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
   memcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/******************************** Hum_MergeSale ******************************
 *
 * Handle Hum_Sales.txt
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Hum_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   pTmp = pRec;

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg("Skip Sale rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   if (iTmp)
      return 1;

   while (!iTmp)
   {
      // Parse input
      if (pRec)
         iRet = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "222153003000", 9))
      //   iRet = 0;
#endif

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, YYYY_MM_DD);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         // Tax - Since HUM county removes confidential sale price from standard format,
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         pTmp = apTokens[MB_SALES_XFERTYPE];
         if (lPrice > 0 && *pTmp > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(pTmp, asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         //*(pOutbuf+OFF_MULTI_APN) = *apTokens[MB_SALES_GROUPSALE];
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         MB_MergeSale(&sCurSale, pOutbuf, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Hum_MergeSale1 *****************************
 *
 * Handle Hum_Sales.csv
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Hum_MergeSale1(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg("Skip Sale rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   while (!iTmp)
   {
      // Parse input
      if (pRec)
         iRet = ParseStringNQ(pRec, cDelim, MB1_SALES_CONFCODE+1, apTokens);
      if (iRet < MB1_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "001040006", 9))
      //   iRet = 0;
#endif

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB1_SALES_DOCNUM] > ' ' && *apTokens[MB1_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB1_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB1_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB1_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         // Tax - Since HUM county removes confidential sale price from standard format,
         dollar2Num(apTokens[MB1_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         //strcpy(sCurSale.acDocType, apTokens[MB1_SALES_DOCCODE]);

         // Transfer Type
         pTmp = apTokens[MB1_SALES_XFERTYPE];
         if (lPrice > 0 && *pTmp > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(pTmp, asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         //*(pOutbuf+OFF_MULTI_APN) = *apTokens[MB1_SALES_GROUPSALE];
         if (*apTokens[MB1_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         MB_MergeSale(&sCurSale, pOutbuf, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Hum_MergeChar1 *****************************
 *
 * Merge Hum_Char.dat in MB_CHAR format
 *
 * Note: need code table for Heating and Colling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

int Hum_MergeChar1(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], *pTmp;
   long     lTmp, lBldgSqft, lGarSqft;
   int      iRet, iTmp, iCmp, iLoop, iBeds, iFBath, iHBath, iFp;
   MB_CHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
   {
      pRec = fgets(acRec, 512, fdChar);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdChar);
         lCharSkip++;
      }
      //pTmp = pRec;
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (MB_CHAR *)pRec;

   // Quality Class
   memcpy(acTmp, pChar->QualityClass, MBSIZ_CHAR_QUALITY);
   acTmp[MBSIZ_CHAR_QUALITY] = 0;
   pTmp = strchr(acTmp, ' ');
   if (pTmp) *pTmp = 0;

   acCode[0] = 0;
   if (!_memicmp(acTmp, "AVG", 3))
      strcpy(acCode, "A");
   else if (isalpha(acTmp[0]))
   {
      *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0];
      if (isdigit(acTmp[1]))
         iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
      else if (pTmp && isdigit(*(pTmp+1)))
         iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
   } else if (acTmp[0] > '0' && acTmp[0] <= '9')
      iRet = Quality2Code(acTmp, acCode, NULL);

   blankPad(acCode, SIZ_BLDG_QUAL);
   memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);

   // Yrblt
   lTmp = atoin(pChar->YearBuilt, MBSIZ_CHAR_YRBLT);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = '2';
   } else
   {
      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = ' ';
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001033006", 9))
   //   iTmp = 0;
#endif
   // Heating
   if (pChar->Heating[0] > ' ')
   {
      iTmp = 0;
      iCmp = -1;
      while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(pChar->Heating, asHeating[iTmp].acSrc, asHeating[iTmp].iLen)) > 0)
         iTmp++;

      if (!iCmp)
      {
         *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
      }
   }

   // Cooling
   if (pChar->Cooling[0] > ' ')
   {
      iTmp = 0;
      iCmp = -1;
      while (asCooling[iTmp].iLen > 0 && (iCmp=memcmp(pChar->Cooling, asCooling[iTmp].acSrc, asCooling[iTmp].iLen)) > 0)
         iTmp++;

      if (!iCmp)
      {
         *(pOutbuf+OFF_AIR_COND) = asCooling[iTmp].acCode[0];
      }
   }

   // Beds
   iBeds = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Fireplace
   iFp = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   } else
      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);

   // HasSeptic or HasSewer
   if (pChar->HasSeptic > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
   else if (pChar->HasSewer > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWell;

   lCharMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdChar);

   return 0;
}

/******************************* Hum_MergeChar *******************************
 *
 * Merge Hum_Char.dat in STDCHAR format
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the one that has value.
 *
 *****************************************************************************/

int Hum_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   ULONG    lSqft, lAcres;
   int      iLoop, iBeds, iFBath, iHBath, iBldgNum, iRooms, iUnits;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   while (!iLoop)
   {
      // Quality Class
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
      *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
      memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

      // YrBlt
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

      // YrEff
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

      // BldgSqft
      lSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*u", SIZ_BLDG_SF, lSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      } else
         memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

      // Garage Sqft
      lSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
      } else
      {
         memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = ' ';
      }

      // LotSqft
      lSqft = atoln(pChar->LotSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10 && *(pOutbuf+OFF_LOT_SQFT) == ' ')
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lSqft);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
         
         lAcres = atoln(pChar->LotAcre, SIZ_CHAR_SQFT);
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lAcres);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }

      // PatioSqft
      lSqft = atoin(pChar->PatioSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_PATIO_SF, lSqft);
         memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
      } else
         memset(pOutbuf+OFF_PATIO_SF, ' ', SIZ_BLDG_SF);

      // Heating
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   
      // Cooling 
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

      // Total Rooms
      iRooms = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
      if (iRooms > 0)
      {
         sprintf(acTmp, "%*d", SIZ_ROOMS, iRooms);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      } else
         memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

      // Beds
      iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      } else
         memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

      // Bath
      iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      } else
         memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

      // Half bath
      iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
      if (iHBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      } else
         memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

      // Fireplace
      *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

      // HasSeptic or HasSewer
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

      // HasWell
      *(pOutbuf+OFF_WATER) = pChar->HasWater;

      // Pools
      *(pOutbuf+OFF_POOL) = pChar->Pool[0];

      // Units count
      iUnits = atoin(pChar->Units, SIZ_CHAR_UNITS);
      if (iUnits > 0)
      {
         sprintf(acTmp, "%*d", SIZ_UNITS, iUnits);
         memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
      } else
         memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

      // Stories
      if (pChar->Stories[0] > ' ')
         memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
      else
         memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

      // BldgSeqNum
      iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);

      lCharMatch++;

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (!pRec)
         break;
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (!iLoop && (iBeds > 0 && iBldgNum > 0))
         break;
   }

   return 0;
}

/******************************** Hum_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************

int Hum_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      iTmp = memcmp(pOutbuf, pTmp+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   iRet = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Hum_MergeTax ******************************
 *
 * Note:
 *
 *****************************************************************************

int Hum_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double   dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iRet;

   // Merge tax data
   while (!iTmp)
   {
      iTmp = ParseStringNQ(pRec, ',', MB_TAX_PAIDDATE2+1, apTokens);
      if (iTmp < MB_TAX_PAIDDATE2)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
         return -1;
      }

      // HUM has no tax year.  keep updating
      dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
      dTax1 = atof(acTmp);
      dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
      dTax2 = atof(acTmp);
      dTmp = dTax1+dTax2;
      if (dTax1 == 0.0 || dTax2 == 0.0)
         dTmp *= 2;

      if (dTmp > 0.0)
      {
         sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
         memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/********************************* Hum_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Hum_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse input
   //iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (cDelim == ',')
      iTmp = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iTmp = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);

   if (iTmp < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN starts with 800-999 except 905 (MR), 910, 911 or 914 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 905 && iTmp != 910 && iTmp != 911 && iTmp != 914))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "12HUM", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   _strupr(apTokens[MB_ROLL_LEGAL]);
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) != cInZoneDelim)
      {
         iTmp = sprintf(acTmp, "%c%s%c", cInZoneDelim, apTokens[MB_ROLL_ZONING], cInZoneDelim);
         memcpy(pOutbuf+OFF_ZONE_X1, acTmp, iTmp);
      }
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   if (acTmp[0] > ' ')
   {
      acTmp[SIZ_USE_CO] = 0;
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "053051009", 9))
   //   iTmp = 0;
#endif

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      lLotSqftCount++;

      // Lot Sqft
      lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%u          ", lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (ULONG)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%u          ", lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
   memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp && isNumber(apTokens[MB_ROLL_DOCNUM]+5))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], SIZ_TRANSFER_DOC);
      }
   }

   // Owner
   try {
      Hum_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER], apTokens[MB_ROLL_CAREOF]);
   } catch(...) {
      LogMsg("***** Exeception occured in Hum_MergeOwner()");
   }

   // Mailing
   try {
      Hum_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Hum_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************* Hum_Load_Roll1 *****************************
 *
 * Handle Hum_Roll.csv file and the like
 *
 ******************************************************************************/

int Hum_Load_Roll1(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return -2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   /*
   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -2;
   }
   */

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }

   /*
   // Open lien file
   fdLienExt = NULL;
   sprintf(acRec, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acRec, 0))
   {
      LogMsg("Open Lien file %s", acRec);
      fdLienExt = fopen(acRec, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acRec);
         return -7;
      }
   }
   */

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Skip header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Hum_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Hum_MergeSitus(acBuf);

            // Merge Lien
            /*
            lRet = 1;
            if (fdLienExt)
               lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

            // Merge Exe if not found in LienExt
            if (fdExe && lRet)
               lRet = MB_MergeExe(acBuf);

            // Remove old sale data
            if (bClearSales)
               ClearOldSale(acBuf);

            // Merge Sales
            if (fdSale)
               lRet = Hum_MergeSale1(acBuf);
            */

            // Merge Char
            if (fdChar)
               lRet = Hum_MergeChar1(acBuf);


            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acBuf);        // Hum_Tax.csv

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Hum_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Hum_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Hum_MergeChar1(acRec);

            // Merge Sales
            //if (fdSale)
            //   lRet = Hum_MergeSale1(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Hum_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Hum_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Hum_MergeChar1(acRec);

         // Merge Sales
         //if (fdSale)
         //   lRet = Hum_MergeSale1(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTax(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   //if (fdSale)
   //   fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   //LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   //LogMsg("Number of Exe skiped:       %u", lExeSkip);
   //LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u\n", lRecCnt);
   return 0;
}

/********************************* Hum_Load_Roll ******************************
 *
 * Handle Hum_Roll.txt file and the like
 * 01/11/2014 Remove Sale & Lien update
 *
 ******************************************************************************/

int Hum_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }
 
   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Skip header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Hum_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Hum_MergeSitus(acBuf);

             // Merge Char
            if (fdChar)
               lRet = Hum_MergeStdChar(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acBuf, 0);        // for new Hum_Tax.txt

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Hum_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Hum_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Hum_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acRec, 0);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Hum_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Hum_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Hum_MergeStdChar(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, 0);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u\n", lRecCnt);
   return 0;
}

/********************************* Hum_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Hum_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L_ASMT], iRet);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L_ASMT], strlen(apTokens[L_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L_FEEPARCEL], strlen(apTokens[L_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "12HUM", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values
   // Land
   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
   long lGrow  = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);
   long lPers  = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
   long lPP_MH = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L_TAXAMT1]);
   double dTax2 = atof(apTokens[L_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'


   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L_TRA], strlen(apTokens[L_TRA]));

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030330020", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   iTmp = strlen(apTokens[L_USECODE]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L_USECODE], SIZ_USE_CO, iTmp);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L_USECODE], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (ULONG)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Hum_MergeOwner(pOutbuf, apTokens[L_OWNER]);

   // Situs
   Hum_MergeSitus(pOutbuf, apTokens[L_SITUS1], apTokens[L_SITUS2]);

   // Mailing
   Hum_MergeMAdr(pOutbuf, apTokens[L_MAILADDRESS1], apTokens[L_MAILADDRESS2], apTokens[L_MAILADDRESS3], apTokens[L_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L_TAXABILITY], true, true);

   return 0;
}

/********************************* Hum_Load_LDR *****************************
 *
 * Load TR601 LDR into 1900-byte record.
 * LoadType=1: load csv files
 *          0: load txt files
 *
 ****************************************************************************/

int Hum_Load_LDR(int iFirstRec, int iLoadType)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     iRet, lRet=0, lCnt=0;

   lLastFileDate = getFileDate(acRollFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A)");
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return 1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return 2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating R01 file: %s\n", acOutFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      lLDRRecCount++;

      // Create new R01 record
      iRet = Hum_MergeLien(acBuf, acRec);
      if (!iRet)
      {
         // Merge Char
         if (fdChar)
            lRet = Hum_MergeStdChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Hum_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * For 2019, add AgPreserve
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Hum_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_ISAGPRESERVE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Format APN
   memcpy(pOutbuf+OFF_APN_D, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // APN
   iRet = remChar(apTokens[L3_ASMT], '-');
   memcpy(pOutbuf, apTokens[L3_ASMT], iRet);

   // Copy ALT_APN
   iRet = remChar(apTokens[L3_FEEPARCEL], '-');
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "12HUM", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   iRet = remChar(apTokens[L3_TRA], '-');
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&HUM_Exemption);

   // Legal
   remChar(apTokens[L3_PARCELDESCRIPTION], '"');
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
      if (lTmp > 999999999)
      {
         LogMsg("*** LotSqft is too big, no update");
      } else
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }

      // Format Acres
      lTmp = (ULONG)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (ULONG)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   remChar(apTokens[L3_OWNER], '"');
   Hum_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   //Hum_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   remChar(apTokens[L3_MAILADDRESS1], '"');
   remChar(apTokens[L3_MAILADDRESS2], '"');
   remChar(apTokens[L3_MAILADDRESS3], '"');
   Hum_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*apTokens[L3_CURRENTDOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp && isNumber(apTokens[L3_CURRENTDOCNUM]+5))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   return 0;
}

/******************************** Hum_Load_LDR3 *****************************
 *
 * Load LDR 2016
 *
 ****************************************************************************/

int Hum_Load_LDR3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Value file
   //sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (!_access(acTmpFile, 0))
   //{
   //   LogMsg("Open Lien file %s", acTmpFile);
   //   fdLienExt = fopen(acTmpFile, "r");
   //   if (fdLienExt == NULL)
   //   {
   //      LogMsg("***** Error opening lien file: %s\n", acTmpFile);
   //      return -7;
   //   }
   //} else
   //   fdLienExt = NULL;

   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SMP");
   lRet = sortFile(acSitusFile, acTmpFile, "S(1,13,C,A)");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(1,13,C,A) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      // Create new R01 record
      iRet = Hum_MergeLien3(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         //if (fdLienExt)
         //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Situs
         if (fdSitus)
            lRet = Hum_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Hum_MergeStdChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/*************************** Hum_CleanupHistSale ******************************
 *
 * Clean up sale file.  remove all record with bad DocNum or DocDate
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Hum_CleanupHistSale(char *pInfile)
{
   char     acInbuf[1024], acOutFile[_MAX_PATH], *pRec, sTmp1[32];
   long     lCnt=0, lOut=0, iTmp;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];

   LogMsg0("Fix sale history file %s", pInfile);
   if (_access(pInfile, 0))
   {
      LogMsg("***** Hum_CleanupHistSale(): Missing input file: %s", pInfile);
      return -1;
   }

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0282630300", 9))
      //   iTmp = 0;
#endif
      memcpy(sTmp1, &pInRec->DocNum, 12);
      myTrim(sTmp1, 12);
      if (sTmp1[4] == 'R')
      {
         if (!strpbrk(sTmp1, "-+*`./"))
         {
            if (!strpbrk(sTmp1, "DAT"))
            {
               iTmp = atoi(&sTmp1[5]);
               sprintf(sTmp1, "%.5d", iTmp);
               memcpy(&pInRec->DocNum[5], sTmp1, 5);
               fputs(acInbuf, fdOut);
               lOut++;
            } else
               LogMsg("Remove 2. %s", sTmp1);
         } else
            LogMsg("Remove 1. %s", sTmp1);
      } else if (strlen(sTmp1) == 9 && isdigit(sTmp1[4]) && !memcmp(sTmp1, pInRec->DocDate, 4))
      {
         pInRec->DocNum[4] = 'R';
         memcpy(&pInRec->DocNum[5], &sTmp1[4], 5);
         fputs(acInbuf, fdOut);
         lOut++;
      } else if (strlen(sTmp1) == 11 && sTmp1[5] == 'R')
      {
         iTmp = sprintf(sTmp1, "%.4s%.6s  ", pInRec->DocDate, &pInRec->DocNum[5]);
         memcpy(pInRec->DocNum, sTmp1, iTmp);
         fputs(acInbuf, fdOut);
         lOut++;
      } else
         LogMsg("Remove %.12s", pInRec->DocNum);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if (lOut != lCnt)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".org");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Rename output file
      iTmp = rename(acOutFile, pInfile);
   } else
      LogMsg("Good sale file, no record drop!");

   LogMsg("Total input records:     %u\n", lCnt);
   LogMsg("    records cleaned:     %u", lOut);

   return iTmp;
}

/*********************************** loadHum ********************************
 *
 * Options:
 *    -CHUM -L -Xs -Xl -Xa -Ms -Mz -Mr (load lien)
 *    -CHUM -U -Xs[i] [-Z] [-T] (load update)
 *
 ****************************************************************************/

int loadHum(int iSkip)
{
   int   iRet=0, iLoadType=0;
   char  acZipFile[_MAX_PATH], acTmp[_MAX_PATH], *pTmp;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;
   iApnLen = myCounty.iApnLen;
   lLotSqftCount = 0;

   //Hum_CleanupHistSale(acCSalFile);

   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      // Load taxrollinfo.txt into SQL
      //iRet = MB_Load_TaxRollInfo(bTaxImport, iHdrRows);

      // Load Hum_Tax.txt
      iRet = MB_Load_TaxBase(bTaxImport, true, 2, iHdrRows);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet = MB_Load_TaxCodeMstr(bTaxImport, 0);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport, 0);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport, iHdrRows);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
   } else if (iLoadTax == TAX_UPDATING)            // -Ut
   {
      iRet = MB_Update_TaxPaid(bTaxImport, 1, iHdrRows);

      // Load Redemption
      if (!iRet)
         iRet |= MB_Load_TaxRedemption(bTaxImport);

      // Update Delq flag in Tax_Base
      if (!iRet)
         iRet = updateDelqFlag(myCounty.acCntyCode);
   }

   if (!iLoadFlag)
      return iRet;

   // Unzip data
   GetIniString("HUM", "ZipFile", "", acZipFile, _MAX_PATH, acIniFile);
   if (bUnzip)                                     // -Z
   {
      if (!_access(acZipFile, 0))
      {
         strcpy(acTmp, acZipFile);
         pTmp = strrchr(acTmp, '\\');
         *pTmp = 0;

         // Unzip input files
         doZipInit();  
         setUnzipToFolder(acTmp);
         setReplaceIfExist(true);
         iRet = startUnzip(acZipFile);
         doZipShutdown();

         if (iRet)
         {
            LogMsg("***** Error unzipping %s.  Program terminated", acZipFile);
            return -1;
         }
      } else if (bSendMail)
      {
         LogMsg("***** Daily zip file not available %s.  Program terminated", acZipFile);
         return -1;
      }
   }

   if (strstr(acRollFile, ".csv"))
      iLoadType = 1;

   // Convert SALE_REC to SCSAL_REC
   //iRet = convertSaleData(myCounty.acCntyCode, acCSalFile, 1);

   // Create/Update cum sale file from Hum_Sales.csv
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // HUM sale file can have different date format, use all default settings
      // MB1_CreateSCSale() is similar to MB_CreateSCSale() except it handles
      // file w/o ConfirmedSalesPrice (Hum_Sales.csv)
      if (!iLoadType)
         // // Hum_Sales.txt
         iRet = MB_CreateSCSale(YYYY_MM_DD,0,5,false,(IDX_TBL5 *)&HUM_DocCode[0]);
      else
         // Hum_Sales.csv
         iRet = MB1_CreateSCSale(MM_DD_YYYY_1,0,0,false,(IDX_TBL5 *)&HUM_DocCode[0]);    

      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract lien file - LDR2016
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acValueFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acValueFile, 0);      // 2016 
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode);
   }

   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Load Char file
      if (!_access(acCharFile, 0))
      {
         if (strstr(acCharFile, ".txt"))
            iRet = Hum_ConvStdChar(acCharFile);
         else
         {
            cDelim = ',';
            iRet = MB_ConvChar(acCharFile);
            cDelim = '|';
         }

         if (iRet <= 0)
            LogMsg("*** WARNING: Error converting Char file %s.  Use existing data.", acCharFile);
      } else
      {
         LogMsg("*** WARNING: CHAR file does not exist: %s: ", acCharFile);
         LogMsg("    -Xa option is ignore.  Please verify input file");
      }
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Create Lien file
      LogMsg0("Load %s LDR file", myCounty.acCntyCode);
      //iRet = Hum_Load_LDR(iSkip, iLoadType);     // 2015
      iRet = Hum_Load_LDR3(iSkip);                 // 2016
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      if (!iLoadType)
         iRet = Hum_Load_Roll(iSkip);
      else
         iRet = Hum_Load_Roll1(iSkip);
      LogMsg("Total LotSqft populated: %d", lLotSqftCount);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Hum_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

    // Merge zoning
   //if (!iRet && (iLoadFlag & MERG_ZONE) )          // -Mz
   //{
   //   iRet = MergeZoning(myCounty.acCntyCode, iSkip);
   //}

   return iRet;
}