/**************************************************************************
 *
 * Revision
 * 09/01/2005 1.0.0     First version by Sony Nguyen
 *                      Adding last known sale fields (price, date, doc#, doctype)
 *                      Sale_Exp.dat will be added by MergeAdr to G01 file
 * 11/10/2010 10.4.1    Fix bug in Asr1_MergeSale() that crashes when sale price is too big.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "Tables.h"
#include "SaleRec.h"
#include "doOwner.h"
#include "doSort.h"
#include "formatApn.h"
#include "MergePla.h"
#include "MergeAssr.h"
#include "MBExtrn.h"

extern   FILE  *fdGrGr;
extern   bool  bFixedApn;
extern   int   iLoadFlag;

/******************************** Asr_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Asr1_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], acSave[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64];
   OWNER myOwner;

   // Clear output buffer if needed
   memset(pOutbuf+OFF_3K_MSGNAME1, ' ', (OFF_3K_CLAND)-(OFF_3K_MSGNAME1));

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acName2[0] = 0;
   acSave[0] = 0;
   acSave1[0] = 0;

   if (pTmp=strstr(acTmp, " 1/2 INT"))
      *pTmp = 0;

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // Save it - Only do it for individual trust
   if (acTmp[iTmp] && !strchr(acTmp, '&'))
   {
      iTmp--;
      strcpy(acSave, (char *)&acTmp[iTmp]);
      acTmp[iTmp] = 0;
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
      if ((pTmp=strstr(acName2, " ETAL")) || (pTmp=strstr(acName2, " ET AL")) )
         *pTmp = 0;
   } else if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Filter out words
   if ((pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " FAMILY TRUST &"))
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      *(pTmp+13) = 0;
      strcpy(acSave1, acTmp);

      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST &"))
   {  // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      *(pTmp+13) = 0;
      strcpy(acSave1, acTmp);

      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
         *(pTmp+6) = 0;
      else
         *pTmp = 0;        // Drop TRUST only if not family trust

      strcpy(acName1, acTmp);
      pTmp1 = pTmp+9;
      iTmp1 = strlen(pTmp1);
      for (iTmp = 0; iTmp < iTmp1; iTmp++)
      {
         if (acName1[iTmp] != *pTmp1)
            break;
         pTmp1++;
      }

      // Skip first word after & if it is the same as last name
      if (iTmp > 2 && *(pTmp1-1) == ' ')
      {
         strcat(acName1, " & ");
         // Search and remove TRUST in second part of name
         if (pTmp=strstr(pTmp1, " TRUST"))
            *pTmp = 0;
         strcat(acName1, pTmp1);
      } else
      {
         strcpy(acName2, pTmp+9);
      }
   } else if ((pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " FAMILY "))
             || (pTmp=strstr(acTmp, " INCOME TR")) || (pTmp=strstr(acTmp, " LIVING TR"))
             || (pTmp=strstr(acTmp, " LAND TRUST")) || (pTmp=strstr(acTmp, " FAM TRUST")))
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUSTE"))
   {  // Drop TRUSTEE
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (acSave[0])
      {
         strcpy(acTmp1, acSave);
         strcpy(acSave, pTmp);
         strcat(acSave, acTmp1);
      } else
      {
         if (isdigit(*(pTmp-1)) )
            while (isdigit(*(--pTmp)));

         strcpy(acSave, pTmp);
      }
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " ESTATE OF"))
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
         strcpy(acName2, pTmp);
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " SUCCESSOR")) )
      *pTmp = 0;
   if ((pTmp=strstr(acName2, " AS ")) || (pTmp=strstr(acName2, " SUCCESSOR")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(acTmp1, myOwner.acName1);

      // Concat what in saved buffer if it is not " TRUST"
      if (acSave[0] && strcmp(acSave, " TRUST"))
      {
         if (acTmp1[strlen(acTmp1)-1] == ' ')
            strcat(acTmp1, (char *)&acSave[1]);      // avoid double blank
         else
            strcat(acTmp1, acSave);
      }

      // Save Name1
      iTmp = strlen(acTmp1);
      if (iTmp > SIZ_3K_MSGNAME1)
         iTmp = SIZ_3K_MSGNAME1;
      memcpy(pOutbuf+OFF_3K_MSGNAME1, acTmp1, iTmp);

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
      {
         iTmp = strlen(acTmp1);
         if (iTmp > SIZ_3K_SWAPNAME1)
            iTmp = SIZ_3K_SWAPNAME1;
         memcpy(pOutbuf+OFF_3K_SWAPNAME1, acTmp1, iTmp);
      } else
      {
         iTmp = strlen(myOwner.acSwapName);
         if (iTmp > SIZ_3K_SWAPNAME1)
            iTmp = SIZ_3K_SWAPNAME1;
         memcpy(pOutbuf+OFF_3K_SWAPNAME1, myOwner.acSwapName, iTmp);
      }
   } else if (acSave1[0])
   {
      iTmp = strlen(acSave1);
      if (iTmp > SIZ_3K_MSGNAME1)
         iTmp = SIZ_3K_MSGNAME1;
      memcpy(pOutbuf+OFF_3K_MSGNAME1, acSave1, iTmp);

      iTmp = strlen(acSave1);
      if (iTmp > SIZ_3K_SWAPNAME1)
         iTmp = SIZ_3K_SWAPNAME1;
      memcpy(pOutbuf+OFF_3K_SWAPNAME1, acSave1, iTmp);
   } else
   {
      if (acSave[0])
      {
         strcat(acName1, acSave);
         acSave[0] = 0;
         iTmp = strlen(acName1);
         if (iTmp > SIZ_3K_MSGNAME1)
            iTmp = SIZ_3K_MSGNAME1;
         memcpy(pOutbuf+OFF_3K_MSGNAME1, acName1, iTmp);

         iTmp = strlen(acName1);
         if (iTmp > SIZ_3K_SWAPNAME1)
            iTmp = SIZ_3K_SWAPNAME1;
         memcpy(pOutbuf+OFF_3K_SWAPNAME1, acName1, iTmp);
      } else
      {
         // Couldn't split names
         iTmp = strlen(pNames);
         if (iTmp > SIZ_3K_MSGNAME1)
            iTmp = SIZ_3K_MSGNAME1;
         memcpy(pOutbuf+OFF_3K_MSGNAME1, pNames, iTmp);

         iTmp = strlen(pNames);
         if (iTmp > SIZ_3K_SWAPNAME1)
            iTmp = SIZ_3K_SWAPNAME1;
         memcpy(pOutbuf+OFF_3K_SWAPNAME1, pNames, iTmp);
      }
   }
}

/******************************** Asr_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Asr1_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64];
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear buffer
   //memset(pOutbuf+OFF_3K_FM_STRNO, ' ', (OFF_3K_ADRFLG)-(OFF_3K_FM_STRNO));
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001012010", 9))
   //   iTmp = 0;
#endif

   // Mail address
   if (!memcmp(apTokens[MB_ROLL_M_ADDR], "        ", 8))
   {
      memcpy(pOutbuf+OFF_3K_FM_STR, "NO MAIL ADDR ON FILE", 20);
      return;
   }

   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);

   // Parse mail address
   parseMAdr1_1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_FM_STRNO, sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_3K_FM_STRNO, acTmp, SIZ_3K_FM_STRNO);
      memcpy(pOutbuf+OFF_3K_FM_FRA, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_3K_FM_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_3K_FM_STR, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_3K_FM_SFX, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_3K_FM_UNIT, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] >= 'A')
   {
      iTmp = strlen(apTokens[MB_ROLL_M_CITY]);
      if (iTmp > SIZ_3K_FM_CITY)
         iTmp = SIZ_3K_FM_CITY;
      memcpy(pOutbuf+OFF_3K_FM_CITY, apTokens[MB_ROLL_M_CITY], strlen(apTokens[MB_ROLL_M_CITY]));
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_3K_FM_ST, apTokens[MB_ROLL_M_ST], 2);
      else
         memcpy(pOutbuf+OFF_3K_FM_ST, BLANK32, 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         iTmp = strlen(apTokens[MB_ROLL_M_ZIP]);
         if (iTmp > 9)
            iTmp = 9;
         memcpy(pOutbuf+OFF_3K_FM_ZIP, apTokens[MB_ROLL_M_ZIP], iTmp);
      }
   }

}

/******************************** Asr_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************/

int Asr1_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp, lTotExe=0;
   int      iRet=0, iTmp, iCnt;
   double   dTmp;
   MB_EXE   myExe;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      //pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf+OFF_3K_ASSMT, "001011019", 9))
   //   iTmp = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, ',');
      pTmp += 2;
      iTmp = memcmp(pOutbuf+OFF_3K_ASSMT, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      *(pOutbuf+OFF_3K_EXCNT) = '0';
      return 0;
   }

   iCnt = 0;
   while (!iTmp)
   {
      memset((void *)&myExe, ' ', sizeof(MB_EXE));
      iRet = ParseStringNQ(pRec, ',', MB_EXE_EXEPCT+1, apTokens);
      if (iRet < MB_EXE_EXEPCT)
      {
         LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         pRec = fgets(acRec, 512, fdExe);
         return -1;
      }

      // Asmt status
      myExe.Status = *apTokens[MB_EXE_STATUS];
      // Exe code
      memcpy(myExe.Code, apTokens[MB_EXE_CODE], strlen(apTokens[MB_EXE_CODE]));
      // HO Flag
      myExe.HO_Flg = *apTokens[MB_EXE_HOEXE];

      // Exe Amt
      lTmp = atol(apTokens[MB_EXE_EXEAMT]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_3K_EXEAMT, lTmp);
         memcpy(myExe.MaxAmt, acTmp, SIZ_3K_EXEAMT);
         lTotExe += lTmp;
      }

      // Exe percentage
      dTmp = atof(apTokens[MB_EXE_EXEPCT]);
      //if ((iTmp = strlen(apTokens[MB_EXE_EXEPCT])) > 0)
      if (dTmp > 0.0)
      {
         iTmp = sprintf(acTmp, "%.2f", dTmp);
         if (acTmp[0] == '0')
            memcpy(myExe.Percent, (char *)&acTmp[1], SIZ_3K_EXEPCT);
         else
            memcpy(myExe.Percent, (char *)&acTmp[0], SIZ_3K_EXEPCT);
      }

      // Copy to output record
      if (iCnt < MAX_EXE_RECS)
         memcpy(pOutbuf+OFF_3K_STAT1+sizeof(MB_EXE)*iCnt++, &myExe, sizeof(MB_EXE));

      // Get next record
      pRec = fgets(acRec, 512, fdExe);
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         break;
      }
      pTmp = strchr(acRec, ',');
      pTmp += 2;
      iTmp = memcmp(pOutbuf+OFF_3K_ASSMT, pTmp, iApnLen);
      lExeMatch++;
   }

   // Total Exe
   if (lTotExe >= 999999999)
      lTotExe = 0;
   else
   {
      sprintf(acTmp, "%*d", SIZ_3K_TOTEX, lTotExe);
      memcpy(pOutbuf+OFF_3K_TOTEX, acTmp, SIZ_3K_TOTEX);
   }

   // Net 
   lTmp = atoin(pOutbuf+OFF_3K_GROSS, SIZ_3K_GROSS);
   if (lTmp > 0)
   {
      lTmp = lTmp - lTotExe;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_3K_NET, lTmp);
         memcpy(pOutbuf+OFF_3K_NET, acTmp, SIZ_3K_NET);
      }
   }

   *(pOutbuf+OFF_3K_EXCNT) = iCnt|0x30;
   return 0;
}

/******************************** Asr_MergeChar ******************************
 *
 * Note: need code table for Heating and Colling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *   - Heating - 1..9 ???
 *   - Cooling - 01..07  ???
 *
 *****************************************************************************/

int Asr1_MergeChar(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft;
   int      iRet, iLoop, iBeds, iFBath, iHBath, iFp;
   MB_CHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
   {
      pRec = fgets(acRec, 512, fdChar);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf+OFF_3K_ASSMT, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_ROLL_ASMT],"003480091", 9))
   //   iTmp = 0;
#endif

   // If not match, return
   if (iLoop)
      return 0;

   pChar = (MB_CHAR *)pRec;

   // Pool
   if (pChar->NumPools[0] > ' ')
      memcpy(pOutbuf+OFF_3K_POOLS, pChar->NumPools, MBSIZ_CHAR_POOLS);

   // UseCat
   if (pChar->LandUseCat[0] > ' ')
      memcpy(pOutbuf+OFF_3K_USECAT, pChar->LandUseCat, MBSIZ_CHAR_USECAT);

   // Quality Class
   if (pChar->QualityClass[0] > ' ')
      memcpy(pOutbuf+OFF_3K_QUALCLS, pChar->QualityClass, MBSIZ_CHAR_QUALITY);

   // HasSeptic or HasSewer
   if (pChar->HasSewer == '1')
      *(pOutbuf+OFF_3K_HASSEWER) = 'Y';
   if (pChar->HasSeptic == '1')
      *(pOutbuf+OFF_3K_HASSEPTIC) = 'Y';

   // HasWell
   if (pChar->HasWell == '1')
      *(pOutbuf+OFF_3K_HASWELL) = 'Y';

   // Yrblt
   memcpy(pOutbuf+OFF_3K_YRBLT, pChar->YearBuilt, SIZ_3K_YRBLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_3K_BDGSZ, lBldgSqft);
      memcpy(pOutbuf+OFF_3K_BDGSZ, acTmp, SIZ_3K_BDGSZ);
   } else
      memcpy(pOutbuf+OFF_3K_BDGSZ, BLANK32, SIZ_3K_BDGSZ);

   // Garage Sqft
   lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_3K_GARSZ, lGarSqft);
      memcpy(pOutbuf+OFF_3K_GARSZ, acTmp, SIZ_3K_GARSZ);
   } else
      memcpy(pOutbuf+OFF_3K_GARSZ, BLANK32, SIZ_3K_GARSZ);

   // Heating
   if (pChar->Heating[0] > ' ')
      memcpy(pOutbuf+OFF_3K_HEAT, pChar->Heating, SIZ_3K_HEAT);
   // Cooling
   if (pChar->Cooling[0] > ' ')
      memcpy(pOutbuf+OFF_3K_COOL, pChar->Cooling, SIZ_3K_COOL);

   // Heating src
   if (pChar->HeatingSource[0] > ' ')
      memcpy(pOutbuf+OFF_3K_HTSRC, pChar->HeatingSource, MBSIZ_CHAR_HEATSRC);
   // Cooling src
   if (pChar->Cooling[0] > ' ')
      memcpy(pOutbuf+OFF_3K_CLSRC, pChar->CoolingSource, MBSIZ_CHAR_COOLSRC);

   // Beds
   iBeds = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_BEDS, iBeds);
      memcpy(pOutbuf+OFF_3K_BEDS, acTmp, SIZ_3K_BEDS);
   } else
      memcpy(pOutbuf+OFF_3K_BEDS, BLANK32, SIZ_3K_BEDS);

   // Bath
   iFBath = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_FBATHS, iFBath);
      memcpy(pOutbuf+OFF_3K_FBATHS, acTmp, SIZ_3K_FBATHS);
   } else
      memcpy(pOutbuf+OFF_3K_FBATHS, BLANK32, SIZ_3K_FBATHS);

   // Half bath
   iHBath = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_HBATHS, iHBath);
      memcpy(pOutbuf+OFF_3K_HBATHS, acTmp, SIZ_3K_HBATHS);
   } else
      memcpy(pOutbuf+OFF_3K_HBATHS, BLANK32, SIZ_3K_HBATHS);

   // Fireplace
   iFp = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_FIREPL, iFp);
      memcpy(pOutbuf+OFF_3K_FIREPL, acTmp, SIZ_3K_FIREPL);
   } else
      memcpy(pOutbuf+OFF_3K_FIREPL, BLANK32, SIZ_3K_FIREPL);

   lCharMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdChar);

   return 0;
}

/******************************** Asr1_MergeSale *****************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Asr1_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp, iCmp, iCnt, iTotalSales;
   long     lSaleDt, lTmp;
   double   dTmp, dDocTax, dPrice;
   MB_SALE  sCurSale;

   // Get rec
   if (!pRec)
   {
      // Skip header record - We don't skip header here because the file is already been resorted
      //pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      // Add 1 to Sale rec to skip double quote
      iCmp = memcmp(pOutbuf+5, pRec+1, iApnLen);
      if (iCmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iCmp > 0);

   // If not match, return
   if (iCmp)
   {
      *(pOutbuf+OFF_3K_SALECNT) = '0';
      return 0;
   }

   iCnt=iTotalSales = 0;
   lSaleDt= atoin(pOutbuf+OFF_3K_LASTSALEDT, 8);
   dPrice = 0;
   while (!iCmp)
   {
      // Clear old record
      memset((void *)&sCurSale, ' ', sizeof(MB_SALE));

      // Parse input rec
      iTmp = ParseStringNQ(pRec, ',', MB_SALES_XFERTYPE+1, apTokens);
      if (iTmp < MB_SALES_XFERTYPE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         iRet =  -1;
         break;
      }

#ifdef _DEBUG
      //if (!memcmp(pOutbuf+5, "470110090000", 9))
      //   LogMsgD("APN = 470110090000.  TaxAmt=%s, SaleAmt=%s", apTokens[MB_SALES_TAXAMT], apTokens[MB_SALES_PRICE]);
#endif

      if (iCnt > 0)
      {
         // Max 2 sales can be moved
         if (iCnt == MAX_SALE_RECS)
            iCnt--;

         // Move sale 1&2 to 2&3 ...
         memcpy(pOutbuf+OFF_3K_SALESTART+sizeof(MB_SALE), pOutbuf+OFF_3K_SALESTART, iCnt*sizeof(MB_SALE));
         // Clear sale 1
         memset(pOutbuf+OFF_3K_SALESTART, ' ', sizeof(MB_SALE));
      }

      // Merge data
      if (*apTokens[MB_SALES_DOCNUM] > ' ')
      {
         lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
         sprintf(acTmp, "%.5s%0.7ld", apTokens[MB_SALES_DOCNUM], lTmp);
         memcpy(sCurSale.DocNum, acTmp, SIZ_3K_SDOCNUM);
      } else
         memset(sCurSale.DocNum, ' ', SIZ_3K_SDOCNUM);

      pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         memcpy(sCurSale.SaleDate, acTmp, 8);

      strcpy(sCurSale.SaleCode, apTokens[MB_SALES_DOCCODE]);
      strcpy(sCurSale.Seller, apTokens[MB_SALES_SELLER]);
      strcpy(sCurSale.Buyer, apTokens[MB_SALES_BUYER]);
      sprintf(sCurSale.SalePrice, "%.*s%s", SIZ_3K_CONFPRICE-strlen(apTokens[MB_SALES_PRICE]), BLANK32, apTokens[MB_SALES_PRICE]);
      sprintf(sCurSale.DocTax, "%.*s%s", SIZ_3K_DOCTAX-strlen(apTokens[MB_SALES_TAXAMT]), BLANK32, apTokens[MB_SALES_TAXAMT]);

      if (*apTokens[MB_SALES_GROUPSALE] > ' ')
      {
         sCurSale.GroupSale = *apTokens[MB_SALES_GROUPSALE];
         strcpy(sCurSale.GroupAsmt, apTokens[MB_SALES_GROUPASMT]);
      }

      strcpy(sCurSale.TransType, apTokens[MB_SALES_XFERTYPE]);
   
      // Remove null char with blank - null is resulted from strcpy()
      replChar((char *)&sCurSale, 0, ' ', sizeof(MB_SALE));
      memcpy(pOutbuf+OFF_3K_SALESTART, &sCurSale, sizeof(MB_SALE));

      // Save most recent sale info
      iTmp = atoin(sCurSale.SaleDate, 8);
      if (iTmp > lSaleDt)
      {
         try
         {
            dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
            dDocTax = atof(acTmp);
            if (dDocTax > 0.0)
            {
               dPrice = (dDocTax * SALE_FACTOR);
               lSaleDt = iTmp;
            } else
            {
               dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
               dPrice = atof(acTmp);
               if (dPrice > 0.0)
                  lSaleDt = iTmp;
            }

            if (dPrice > 999999999.0)
            {
               LogMsgD("??? APN = 470110090000.  TaxAmt=%s, SaleAmt=%s.  Reset sale price to 0.", apTokens[MB_SALES_TAXAMT], apTokens[MB_SALES_PRICE]);
               dPrice = 0;
            }
         } catch (...)
         {
            LogMsgD("Exception occurs in Asr1_MergeSale().  TaxAmt=%s, SaleAmt=%s", apTokens[MB_SALES_TAXAMT], apTokens[MB_SALES_PRICE]);
            dPrice = 0;
         }
      }

      iTotalSales++;
      iCnt++;

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }
      iCmp = memcmp(pOutbuf+5, pRec+1, iApnLen);
   }

   lSaleMatch++;

   sprintf(acTmp, "%2d", iTotalSales);
   memcpy(pOutbuf+OFF_3K_TOTSALES, acTmp, 2);
   *(pOutbuf+OFF_3K_SALECNT) = iCnt|0x30;

   // Update most recent sale
   if (dPrice > 0.0 && dPrice < 999999999.0)
   {
      //iTmp = sprintf(acTmp, "%ld", lSaleDt);
      //memcpy(pOutbuf+OFF_3K_LASTSALEDT, acTmp, iTmp);
      //sprintf(acTmp, "%*ld", SIZ_3K_LASTSALEAMT, lPrice);
      //memcpy(pOutbuf+OFF_3K_LASTSALEAMT, acTmp, SIZ_3K_LASTSALEAMT);
      dPrice += 0.5;
      sprintf(acTmp, "%*ld", SIZ_3K_SALEAMT, (long)dPrice);
      memcpy(pOutbuf+OFF_3K_SALEAMT, acTmp, SIZ_3K_SALEAMT);

      // Gain of sale
      dTmp = atoin(pOutbuf+OFF_3K_GROSS, SIZ_3K_GROSS);
      if (dPrice > dTmp)
      {
         sprintf(acTmp, "%*ld", SIZ_3K_EXCPAMT, dPrice-dTmp);
         memcpy(pOutbuf+OFF_3K_EXCPAMT, acTmp, SIZ_3K_EXCPAMT);
      }
   }

   // Land value per lot acre
   long lLand = atoin(pOutbuf+OFF_3K_LAND, SIZ_3K_LAND);
   dTmp = atofn(pOutbuf+OFF_3K_DRV_ACRES, SIZ_3K_DRV_ACRES);
   if (dTmp > 0.0 && lLand > 0)
   {
      sprintf(acTmp, "%*.2f", SIZ_3K_LVALACRE, (double)lLand/dTmp);
      memcpy(pOutbuf+OFF_3K_LVALACRE, acTmp, SIZ_3K_LVALACRE);
   }

   // Land value per lot sqft
   lTmp = atoin(pOutbuf+OFF_3K_LOTSQFT, SIZ_3K_LOTSQFT);
   if (lTmp > 0 && lLand > 0)
   {
      sprintf(acTmp, "%*.2f", SIZ_3K_LVALSQFT, (double)lLand/lTmp);
      memcpy(pOutbuf+OFF_3K_LVALSQFT, acTmp, SIZ_3K_LVALSQFT);
   }

   // Impr value per impr sqft
   long lImpr = atoin(pOutbuf+OFF_3K_IMPR, SIZ_3K_IMPR);
   lTmp = atoin(pOutbuf+OFF_3K_BDGSZ, SIZ_3K_BDGSZ);
   if (lTmp > 0 && lImpr > 0)
   {
      sprintf(acTmp, "%*.2f", SIZ_3K_IVALSQFT, (double)lImpr/lTmp);
      memcpy(pOutbuf+OFF_3K_IVALSQFT, acTmp, SIZ_3K_IVALSQFT);
   }

   // Sale value per impr sqft
   if (lTmp > 0 && dPrice > 0.0)
   {
      sprintf(acTmp, "%*.2f", SIZ_3K_SVALSQFT, (double)dPrice/lTmp);
      memcpy(pOutbuf+OFF_3K_SVALSQFT, acTmp, SIZ_3K_SVALSQFT);
   }

   return iRet;
}

/******************************** Asr_MergeGrGr ******************************
 *
 * 
 *
 *****************************************************************************/

int Asr1_MergeGrGr(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   int      iRet=0, iTmp, iCmp, iCnt;
   long     lSaleDt, lPrice, lTmp;
   MB_GRGR  *pSaleRec;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdGrGr);
      // Get first rec
      pRec = fgets(acRec, 512, fdGrGr);
   }

   pSaleRec = (MB_GRGR *) &acRec[0];
   do
   {
      if (!pRec)
         return 1;      // EOF

      iCmp = memcmp(pOutbuf+5, pRec, SIZ_3K_APN);
      if (iCmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", SIZ_3K_APN, pRec);
         pRec = fgets(acRec, 512, fdGrGr);
         lSaleSkip++;
      }
   } while (iCmp > 0);

   // If not match, return
   if (iCmp)
   {
      *(pOutbuf+OFF_3K_G_G_COUNT) = '0';
      return 0;
   }

   iCnt = 0;
   lSaleDt = atoin(pOutbuf+OFF_3K_LASTSALEDT, SIZ_3K_LASTSALEDT);
   lPrice = 0;
   while (!iCmp)
   {
      if (iCnt < MAX_GRGR_RECS)
      {
         // Merge data
         memcpy(pOutbuf+OFF_3K_1DT+(iCnt*GRGR_SIZE), (char *)&pSaleRec->DocDate[0], GRGR_SIZE);

         // Save most recent sale info
         lTmp = atoin(pSaleRec->SalePrice, SIZ_3K_GSALEPRICE);
         if (lTmp > 0)
         {
            iTmp = atoin(pSaleRec->DocDate, SIZ_3K_LASTSALEDT);
            if (iTmp > lSaleDt)
            {
               lPrice = lTmp;
               lSaleDt = iTmp;
            }
         }

         iCnt++;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdGrGr);
      if (!pRec)
      {
         fclose(fdGrGr);
         fdGrGr = NULL;
         break;
      }
      iCmp = memcmp(pOutbuf+5, pRec, SIZ_3K_APN);
   }

   lGrGrMatch++;

   *(pOutbuf+OFF_3K_G_G_COUNT) = iCnt|0x30;

   // Update most recent sale
   if (lPrice > 0)
   {
      char  acTmp[32];
      iTmp = sprintf(acTmp, "%ld", lSaleDt);
      memcpy(pOutbuf+OFF_3K_LASTSALEDT, acTmp, iTmp);
      iTmp = sprintf(acTmp, "%*ld", SIZ_3K_LASTSALEAMT, lPrice);
      memcpy(pOutbuf+OFF_3K_LASTSALEAMT, acTmp, iTmp);
   }

   return iRet;
}

/******************************** Asr1_MergeLien *****************************
 *
 * Note:
 *
 *****************************************************************************/

int Asr1_MergeLien(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *apItems[32];
   int      iRet, iTmp, iLoop;
   long     lLand, lImpr, lGrowing, lFixt, lPP, lNet, lExe, lTmp;

   if (!fdValue)
      return 1;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdValue);
      // Get first rec
      pRec = fgets(acRec, 512, fdValue);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         return 1;      // EOF
      }

      iLoop = memcmp(pOutbuf+OFF_3K_ASSMT, pRec+1, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Lien rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdValue);
         lValueSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   iRet = 1;
   do 
   {
      iTmp = ParseStringNQ(pRec, ',', MB_LIEN_OTHEXE+1, apItems);
      if (iTmp < MB_LIEN_OTHEXE)
      {
         LogMsg0("*** Bad Lien record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         lValueSkip++;
      } else
      {
         // Merge Lien data
         lLand=lImpr=lGrowing=lFixt=lPP=lNet=lExe=0;

         // Land
         lLand = atol(apItems[MB_LIEN_LAND]);
         sprintf(acTmp, "%*d", SIZ_3K_LAND, lLand);
         memcpy(pOutbuf+OFF_3K_LAND, acTmp, SIZ_3K_LAND);
         memcpy(pOutbuf+OFF_3K_CLAND, acTmp, SIZ_3K_LAND);

         // Impr
         lImpr = atol(apItems[MB_LIEN_IMPR]);
         sprintf(acTmp, "%*d", SIZ_3K_IMPR, lImpr);
         memcpy(pOutbuf+OFF_3K_IMPR, acTmp, SIZ_3K_IMPR);
         memcpy(pOutbuf+OFF_3K_CIMPR, acTmp, SIZ_3K_IMPR);

         // Other values
         lGrowing = atol(apItems[MB_LIEN_GROWING]);
         sprintf(acTmp, "%*d", SIZ_3K_GROW, lGrowing);
         memcpy(pOutbuf+OFF_3K_GROW, acTmp, SIZ_3K_GROW);
         memcpy(pOutbuf+OFF_3K_CGROW, acTmp, SIZ_3K_GROW);

         // Fixtures 
         lFixt = atoi(apItems[MB_LIEN_FIXTRS]);
         sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixt);
         memcpy(pOutbuf+OFF_3K_FIXT, acTmp, SIZ_3K_IMPR);
         memcpy(pOutbuf+OFF_3K_CFIXT, acTmp, SIZ_3K_IMPR);

         // PP
         lPP = atoi(apItems[MB_LIEN_PP]);
         sprintf(acTmp, "%*d", SIZ_3K_IMPR, lPP);
         memcpy(pOutbuf+OFF_3K_PERS, acTmp, SIZ_3K_IMPR);
         memcpy(pOutbuf+OFF_3K_CPERS, acTmp, SIZ_3K_IMPR);

         // Gross
         lTmp = lLand+lImpr+lGrowing+lFixt+lPP;
         sprintf(acTmp, "%*d", SIZ_3K_GROSS, lTmp);
         memcpy(pOutbuf+OFF_3K_GROSS, acTmp, SIZ_3K_GROSS);

         // Net
         lNet = atoi(apItems[MB_LIEN_NET]);
         sprintf(acTmp, "%*d", SIZ_3K_GROSS, lTmp);
         memcpy(pOutbuf+OFF_3K_NET, acTmp, SIZ_3K_GROSS);

         // Land+Impr
         sprintf(acTmp, "%*d", SIZ_3K_IMPR, lLand+lImpr);
         memcpy(pOutbuf+OFF_3K_LANDIMP, acTmp, SIZ_3K_IMPR);

         // Impr%
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*d", SIZ_3K_IMPPERC, (LONGLONG)lImpr*100/(lLand+lImpr));
            memcpy(pOutbuf+OFF_3K_IMPPERC, acTmp, SIZ_3K_IMPPERC);
         }

         iRet = 0;
      }

      // Get next Lien record
      pRec = fgets(acRec, 512, fdValue);
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         break;         // EOF
      }

      iLoop = memcmp(pOutbuf+OFF_3K_ASSMT, pRec+1, iApnLen);
   } while (iTmp < MB_LIEN_OTHEXE && !iLoop);

   if (!iRet)
      lValueMatch++;

   return iRet;
}

/******************************** Asr1_MergeCurr *****************************
 *
 * Note:
 *
 *****************************************************************************/

int Asr1_MergeCurr(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *apItems[32];
   int      iRet, iTmp, iLoop;
   long     lLand, lImpr, lHomeSite, lGrow, lFixt, lPP, lFixtRp, lPPMh;

   if (!fdValue)
      return 1;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      //pRec = fgets(acRec, 512, fdValue);
      // Get first rec
      pRec = fgets(acRec, 512, fdValue);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         return 1;      // EOF
      }

      iLoop = memcmp(pOutbuf+OFF_3K_ASSMT, pRec+1, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip current rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdValue);
         lValueSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_CURR_ASMT], "015450050000", 9))
   //   iTmp = 0;
#endif

   iRet = 1;
   do 
   {
      iTmp = ParseStringNQ(pRec, ',', MB_CURR_OTHEXE+1, apItems);
      if (iTmp < MB_CURR_OTHEXE)
      {
         LogMsg0("*** Bad current record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         lValueSkip++;
      } else
      {
         // Merge Lien data
         lLand=lImpr=lGrow=lFixt=lFixtRp=lPP=lHomeSite=lPPMh=0;

         // Land
         lLand = atol(apItems[MB_CURR_LAND]);
         sprintf(acTmp, "%*d", SIZ_3K_LAND, lLand);
         memcpy(pOutbuf+OFF_3K_CLAND, acTmp, SIZ_3K_LAND);

         // Homesite
         lHomeSite = atol(apItems[MB_CURR_HOMESITE]);
         sprintf(acTmp, "%*d", SIZ_3K_LAND, lHomeSite);
         memcpy(pOutbuf+OFF_3K_CHOMESITE, acTmp, SIZ_3K_LAND);

         // Impr
         lImpr = atol(apItems[MB_CURR_IMPR]);
         sprintf(acTmp, "%*d", SIZ_3K_IMPR, lImpr);
         memcpy(pOutbuf+OFF_3K_CIMPR, acTmp, SIZ_3K_IMPR);

         // Grow
         lGrow = atol(apItems[MB_CURR_GROWING]);
         sprintf(acTmp, "%*d", SIZ_3K_GROW, lGrow);
         memcpy(pOutbuf+OFF_3K_CGROW, acTmp, SIZ_3K_GROW);

         // Fixtures 
         lFixt = atoi(apItems[MB_CURR_FIXTRS]);
         sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixt);
         memcpy(pOutbuf+OFF_3K_CFIXT, acTmp, SIZ_3K_IMPR);

         // Fixtures RP
         lFixtRp = atoi(apItems[MB_CURR_FIXTR_RP]);
         sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixtRp);
         memcpy(pOutbuf+OFF_3K_CFIXT_RP, acTmp, SIZ_3K_IMPR);

         // PP
         lPP = atoi(apItems[MB_CURR_PP_BUS]);
         sprintf(acTmp, "%*d", SIZ_3K_IMPR, lPP);
         memcpy(pOutbuf+OFF_3K_CPERS, acTmp, SIZ_3K_IMPR);

         // Mobile home PP
         lPPMh = atoi(apItems[MB_CURR_PPMOBILHOME]);
         sprintf(acTmp, "%*d", SIZ_3K_IMPR, lPPMh);
         memcpy(pOutbuf+OFF_3K_CMHPERS, acTmp, SIZ_3K_IMPR);

         iRet = 0;
      }

      // Get next current record
      pRec = fgets(acRec, 512, fdValue);
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         break;         // EOF
      }

      iLoop = memcmp(pOutbuf+OFF_3K_ASSMT, pRec+1, iApnLen);
   } while (iTmp < MB_CURR_OTHEXE && !iLoop);

   if (!iRet)
      lValueMatch++;

   return iRet;
}

/********************************* Asr_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Asr1_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace tab char with 0
   iRet = ParseStringNQ(pRollRec, ',', MB_ROLL_INACTIVE, apTokens);
   if (iRet < MB_ROLL_INACTIVE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   if ((*apTokens[MB_ROLL_ASMTSTATUS] != 'A'))
      return -1;


#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_ROLL_ASMT], "015450050", 9))
   //   iTmp = 0;
#endif

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iAsrRecLen);

      // County code 
      memcpy(pOutbuf+OFF_3K_CO_ID, myCounty.acCntyCode, 3);
      memcpy(pOutbuf+OFF_3K_CO_NUM, myCounty.acCntyID, 2);

      // Asmt, fee parcel
      memcpy(pOutbuf+OFF_3K_ASSMT, apTokens[MB_ROLL_ASMT], strlen(apTokens[MB_ROLL_ASMT]));
      memcpy(pOutbuf+OFF_3K_FEE_PRCL, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[MB_ROLL_ASMT], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_3K_APN_DASH, acTmp, iRet);

      // TRA
      memcpy(pOutbuf+OFF_3K_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));
      // Legal
      if (*apTokens[MB_ROLL_LEGAL] > ' ')
      {
         iTmp = strlen(apTokens[MB_ROLL_LEGAL]);
         if (iTmp > SIZ_3K_DESC)
            iTmp = SIZ_3K_DESC;
         memcpy(pOutbuf+OFF_3K_DESC, apTokens[MB_ROLL_LEGAL], iTmp);
      }
      
      if (iLoadFlag & LOAD_ASSR)
         Asr1_MergeLien(pOutbuf);
      else
         Asr1_MergeCurr(pOutbuf);
   } else
      Asr1_MergeCurr(pOutbuf);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%.*ld", SIZ_3K_INACRES, (long)(dTmp * 100.0001));
      memcpy(pOutbuf+OFF_3K_INACRES, acTmp, SIZ_3K_INACRES);
      sprintf(acTmp, "%*.3f", SIZ_3K_DRV_ACRES, dTmp);
      memcpy(pOutbuf+OFF_3K_DRV_ACRES, acTmp, SIZ_3K_DRV_ACRES);
      sprintf(acTmp, "%*ld", SIZ_3K_LOTSQFT, (long)(dTmp * SQFT_PER_ACRE));
      memcpy(pOutbuf+OFF_3K_LOTSQFT, acTmp, SIZ_3K_LOTSQFT);
   }

   // Land $ per acres
   // Land $ per sqft
   // Impr $ per sqft

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      iTmp = strlen(apTokens[MB_ROLL_ZONING]);
      if (iTmp > SIZ_3K_ZONE)
         iTmp = SIZ_3K_ZONE;
      memcpy(pOutbuf+OFF_3K_ZONE, apTokens[MB_ROLL_ZONING], iTmp);
   }

   // UseCode
   iTmp = strlen(apTokens[MB_ROLL_USECODE]);
   if (iTmp > SIZ_3K_USE)
      iTmp = SIZ_3K_USE;
   memcpy(pOutbuf+OFF_3K_USE, apTokens[MB_ROLL_USECODE], iTmp);

   // Neighborhood code
   iTmp = strlen(apTokens[MB_ROLL_NBHCODE]);
   if (iTmp > SIZ_3K_NBHCD)
      iTmp = SIZ_3K_NBHCD;
   memcpy(pOutbuf+OFF_3K_NBHCD, apTokens[MB_ROLL_NBHCODE], iTmp);

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > ' ')
   {
      lTmp = atoin(apTokens[MB_ROLL_DOCNUM]+5, 7);
      iTmp = sprintf(acTmp, "%.5s%0.7ld", apTokens[MB_ROLL_DOCNUM], lTmp);
      memcpy(pOutbuf+OFF_3K_RDOCNUM, acTmp, iTmp);
   } else
      memcpy(pOutbuf+OFF_3K_RDOCNUM, BLANK32, SIZ_3K_RDOCNUM);

   // Recording date
   pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      memcpy(pOutbuf+OFF_3K_RDOCDT, acTmp, strlen(acTmp));

   // Fix APN
   if (!bFixedApn)
      memset(pOutbuf+OFF_3K_OUTFIXAPN, ' ', SIZ_3K_OUTFIXAPN);
   else
   {
      // Create fixed APN - implement for LAK only
   }

   // Tax in full
   memcpy(pOutbuf+OFF_3K_TAXFULL, apTokens[MB_ROLL_TAXABILITY], strlen(apTokens[MB_ROLL_TAXABILITY]));

   // Owner
   memset(pOutbuf+OFF_3K_NAME, ' ', (OFF_3K_M_STR)-(OFF_3K_NAME));
   memcpy(pOutbuf+OFF_3K_NAME, apTokens[MB_ROLL_OWNER], strlen(apTokens[MB_ROLL_OWNER]));
   memcpy(pOutbuf+OFF_3K_CAREOF, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));
   memcpy(pOutbuf+OFF_3K_DBA, apTokens[MB_ROLL_DBA], strlen(apTokens[MB_ROLL_DBA]));

   // Mailing
   memset(pOutbuf+OFF_3K_M_STR, ' ', (OFF_3K_S_STR)-(OFF_3K_M_STR));
   memset(pOutbuf+OFF_3K_FM_STRNO, ' ', (OFF_3K_ADRFLG)-(OFF_3K_FM_STRNO));
   if (*apTokens[MB_ROLL_M_ADDR] > ' ')
   {
      memcpy(pOutbuf+OFF_3K_M_STR, _strupr(apTokens[MB_ROLL_M_ADDR]), strlen(apTokens[MB_ROLL_M_ADDR]));
      memcpy(pOutbuf+OFF_3K_M_CITY, _strupr(apTokens[MB_ROLL_M_CITY]), strlen(apTokens[MB_ROLL_M_CITY]));
      memcpy(pOutbuf+OFF_3K_M_ST, _strupr(apTokens[MB_ROLL_M_ST]), strlen(apTokens[MB_ROLL_M_ST]));
      memcpy(pOutbuf+OFF_3K_M_ZIP, apTokens[MB_ROLL_M_ZIP], strlen(apTokens[MB_ROLL_M_ZIP]));
      Asr1_MergeMAdr(pOutbuf);
      *(pOutbuf+OFF_3K_ADRFLG) = '1';
   } else
   {
      *(pOutbuf+OFF_3K_ADRFLG) = ' ';
   }

   // Situs
   // Clear old Situs
   memset(pOutbuf+OFF_3K_S_STR, ' ', (OFF_3K_LAND)-(OFF_3K_S_STR));
   memset(pOutbuf+OFF_3K_FS_STRNO, ' ', (OFF_3K_FM_STRNO)-(OFF_3K_FS_STRNO));

   // Merge data
   memcpy(pOutbuf+OFF_3K_S_STR, apTokens[MB_ROLL_STRNAME], strlen(apTokens[MB_ROLL_STRNAME]));
   memcpy(pOutbuf+OFF_3K_S_STRNUM, apTokens[MB_ROLL_STRNUM], strlen(apTokens[MB_ROLL_STRNUM]));
   memcpy(pOutbuf+OFF_3K_S_SFX, apTokens[MB_ROLL_STRTYPE], strlen(apTokens[MB_ROLL_STRTYPE]));
   memcpy(pOutbuf+OFF_3K_S_DIR, apTokens[MB_ROLL_STRDIR], strlen(apTokens[MB_ROLL_STRDIR]));
   memcpy(pOutbuf+OFF_3K_S_UNIT, apTokens[MB_ROLL_UNIT], strlen(apTokens[MB_ROLL_UNIT]));
   memcpy(pOutbuf+OFF_3K_S_COMM, apTokens[MB_ROLL_COMMUNITY], strlen(apTokens[MB_ROLL_COMMUNITY]));
   
   // Situs Zip - not avail in BUT
   memcpy(pOutbuf+OFF_3K_S_ZIP, apTokens[MB_ROLL_ZIP], strlen(apTokens[MB_ROLL_ZIP]));

   // Format Situs
   lTmp = atol(apTokens[MB_ROLL_STRNUM]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_FS_STRNO, lTmp);
      memcpy(pOutbuf+OFF_3K_FS_STRNO, acTmp, SIZ_3K_FS_STRNO);
      if (pTmp = strchr(apTokens[MB_ROLL_STRNUM], ' '))
         memcpy(pOutbuf+OFF_3K_FS_FRA, pTmp+1, strlen(pTmp+1));
   }
   memcpy(pOutbuf+OFF_3K_FS_DIR, apTokens[MB_ROLL_STRDIR], strlen(apTokens[MB_ROLL_STRDIR]));
   memcpy(pOutbuf+OFF_3K_FS_STR, apTokens[MB_ROLL_STRNAME], strlen(apTokens[MB_ROLL_STRNAME]));
   memcpy(pOutbuf+OFF_3K_FS_SFX, apTokens[MB_ROLL_STRTYPE], strlen(apTokens[MB_ROLL_STRTYPE]));
   memcpy(pOutbuf+OFF_3K_FS_UNIT, apTokens[MB_ROLL_UNIT], strlen(apTokens[MB_ROLL_UNIT]));
   if (*apTokens[MB_ROLL_COMMUNITY] > ' ')
   {
      char acCode[32];
      apTokens[MB_ROLL_COMMUNITY][3] = 0;
      Abbr2City(apTokens[MB_ROLL_COMMUNITY], acCode);   
      memcpy(pOutbuf+OFF_3K_FS_CITY, acCode, strlen(acCode));
   }

   // Format owner
   if (!memcmp(myCounty.acCntyCode, "PLA", 3))
      Asr1_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   else
      Asr1_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);

   return 0;
}

/********************************** MB_MergeAssr ******************************
 *
 * This function has not been tested.
 *
 ******************************************************************************/

int MB_MergeAssr1(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg("+++++ Updating Assr file for %s\n", myCounty.acCntyCode);

   iApnLen = myCounty.iApnLen;
   if (!memcmp(myCounty.acCntyCode, "LAK", 3))
      bFixedApn = true;
   else
      bFixedApn = false;

   sprintf(acRawFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, "CO");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open GrGr file
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open GrGr file %s", acTmpFile);
      fdGrGr = fopen(acTmpFile, "r");
      if (fdGrGr == NULL)
      {
         LogMsg("***** Error opening GrGr file: %s\n", acTmpFile);
         return 2;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }
   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return 2;
   }
   // Open Sales file - Need to be sorted 
   LogMsg("Open Sales file %s", acSalesFile);
   sprintf(acTmpFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "SMP");
   lRet = sortFile(acSalesFile, acTmpFile, "S(#1,C,A,#3,DAT,A) F(TXT)");
   fdSale = fopen(acTmpFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acTmpFile);
      return 2;
   }
   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "EMP");
   lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) F(TXT)");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return 2;
   }

   // Open current file
   LogMsg("Open current value file %s", acValueFile);
   sprintf(acTmpFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "LMP");
   lRet = sortFile(acValueFile, acTmpFile, "S(#1,C,A) F(TXT)");
   fdValue = fopen(acTmpFile, "r");
   if (fdValue == NULL)
   {
      LogMsg("***** Error opening current value file: %s\n", acTmpFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Drop header record
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iAsrRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iAsrRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   lCharMatch=lExeMatch=lSaleMatch=lValueMatch= 0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iAsrRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (lCnt >= 140720)
      //   printf("APN=%.20s <---> %.20s", (char *)&acBuf[5], (char *)&acRollRec[1]);
      //if (!memcmp((char *)&acBuf[5], "470110090000", 9))
      //   iTmp = 0;
#endif

      iTmp = memcmp((char *)&acBuf[5], (char *)&acRollRec[1], iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Asr1_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         if (!iRet)
         {
            // Merge Exe
            if (fdExe)
               lRet = Asr1_MergeExe(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Asr1_MergeChar(acBuf);

            // Merge Sales
            if (fdSale)
               lRet = Asr1_MergeSale(acBuf);

            // Merge GrGr
            if (fdGrGr)
               lRet = Asr1_MergeGrGr(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Asr1_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

         if (!iRet)
         {
            // Merge Exe
            if (fdExe)
               lRet = Asr1_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Asr1_MergeChar(acRec);

            // Merge Sales
            if (fdSale)
               lRet = Asr1_MergeSale(acRec);

            // Merge GrGr
            if (fdGrGr)
               lRet = Asr1_MergeGrGr(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_3K_RDOCDT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iAsrRecLen, &nBytesWritten, NULL);
            lCnt++;
         }

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;
         continue;
      }

      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_3K_RDOCDT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
         lCnt++;
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Finish remainder of roll file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Asr1_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Exe
         if (fdExe)
            lRet = Asr1_MergeExe(acRec);

         // Merge Char
         if (fdChar)
            lRet = Asr1_MergeChar(acRec);

         // Merge Sales
         if (fdSale)
            lRet = Asr1_MergeSale(acRec);

         // Merge GrGr
         if (fdGrGr)
            lRet = Asr1_MergeGrGr(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iAsrRecLen, &nBytesWritten, NULL);
         lCnt++;
      }

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdGrGr)
      fclose(fdGrGr);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdValue)
      fclose(fdValue);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Sort output file
   //sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) B(%d,R)", iAsrRecLen, iAsrRecLen);
   //lRet = sortFile(acTmpFile, acOutFile, acTmp);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   //LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of CurVal matched:   %u\n", lValueMatch);

   //LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Number of CurVal skiped:    %u\n", lValueSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u", lCnt);

   lAssrRecCnt = lCnt;
   return 0;
}

/******************************** MB_CreateAssr *****************************
 *
 *
 *
 ****************************************************************************/

int MB_CreateAssr1(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg("+++++ Creating Assr file for %s\n", myCounty.acCntyCode);

   iApnLen = myCounty.iApnLen;
   if (!memcmp(myCounty.acCntyCode, "LAK", 3))
      bFixedApn = true;
   else
      bFixedApn = false;

   // Use TMP file only if output needs resort
   //sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   sprintf(acOutFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, "CO");

   // Open GrGr file
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open GrGr file %s", acTmpFile);
      fdGrGr = fopen(acTmpFile, "r");
      if (fdGrGr == NULL)
      {
         LogMsg("***** Error opening GrGr file: %s\n", acTmpFile);
         return 2;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }
   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return 2;
   }
   // Open Sales file - Need to be sorted on APN and EventDate
   LogMsg("Open Sales file %s", acSalesFile);
   sprintf(acTmpFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "SMP");
   lRet = sortFile(acSalesFile, acTmpFile, "S(#1,C,A,#3,DAT,A) F(TXT)");
   fdSale = fopen(acTmpFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acTmpFile);
      return 2;
   }
   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "EMP");
   lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) F(TXT)");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return 2;
   }

   // Open Lien file
   LogMsg("Open Lien file %s", acValueFile);
   fdValue = fopen(acValueFile, "r");
   if (fdValue == NULL)
   {
      LogMsg("***** Error opening Value file: %s\n", acValueFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iAsrRecLen);
      bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
   }

   // Drop header record
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   lCharMatch=lExeMatch=lSaleMatch=lValueMatch= 0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new R01 record
      iRet = Asr1_MergeRoll(acBuf, acRec, iAsrRecLen, CREATE_R01|CREATE_LIEN);

      if (!iRet)
      {
         // Merge Exe
         if (fdExe)
            lRet = Asr1_MergeExe(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Asr1_MergeChar(acBuf);

         // Merge Sales
         if (fdSale)
            lRet = Asr1_MergeSale(acBuf);

         // Merge GrGr
         if (fdGrGr)
            lRet = Asr1_MergeGrGr(acBuf);

         iNewRec++;
#ifdef _DEBUG
         //iRet = replChar(acBuf, 0, ' ', iAsrRecLen);
         //if (iRet)
         //   iRet = 0;
#endif
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         sprintf(acTmp, "%*d", SIZ_3K_RECNO, ++lCnt);
         memcpy((char *)&acBuf[OFF_3K_RECNO], acTmp, SIZ_3K_RECNO);

         bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdGrGr)
      fclose(fdGrGr);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdValue)
      fclose(fdValue);
   if (fhOut)
      CloseHandle(fhOut);

   // Sort output file
   //LogMsg("Sort output file %s to %s", acTmpFile, acOutFile);
   //sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) B(%d,R)", iAsrRecLen, iAsrRecLen);
   //lRet = sortFile(acTmpFile, acOutFile, acTmp);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Value matched:    %u", lValueMatch);
   LogMsg("Number of GrGr matched:     %u\n", lGrGrMatch);

   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Number of Value skiped:     %u\n", lValueSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lCnt);

   lAssrRecCnt = lCnt;
   return 0;
}

