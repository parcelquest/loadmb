#ifndef _MERGE_SON_H
#define  _MERGE_SON_H           1

#define  SON_CHAR_FEEPARCEL           0
#define  SON_CHAR_POOLSPA             1
#define  SON_CHAR_CATTYPE             2
#define  SON_CHAR_QUALITYCLASS        3
#define  SON_CHAR_YRBLT               4
#define  SON_CHAR_BUILDINGSIZE        5
#define  SON_CHAR_ATTACHGARAGESF      6
#define  SON_CHAR_DETACHGARAGESF      7
#define  SON_CHAR_CARPORTSF           8
#define  SON_CHAR_HEATING             9
#define  SON_CHAR_COOLINGCENTRALAC    10
#define  SON_CHAR_COOLINGEVAPORATIVE  11
#define  SON_CHAR_COOLINGROOMWALL     12
#define  SON_CHAR_COOLINGWINDOW       13
#define  SON_CHAR_STORIESCNT          14
#define  SON_CHAR_UNITSCNT            15
#define  SON_CHAR_TOTALROOMS          16
#define  SON_CHAR_EFFYR               17
#define  SON_CHAR_PATIOSF             18
#define  SON_CHAR_BEDROOMS            19
#define  SON_CHAR_BATHROOMS           20
#define  SON_CHAR_HALFBATHS           21
#define  SON_CHAR_FIREPLACE           22
#define  SON_CHAR_ASMT                23
#define  SON_CHAR_BLDGSEQNUM          24
#define  SON_CHAR_HASWELL             25
#define  SON_CHAR_LOTSQFT             26

#define  SON_GRGR_DOCNUM        0
#define  SON_GRGR_RECDATE       1
#define  SON_GRGR_DOCTYPE       2
#define  SON_GRGR_APN           3
#define  SON_GRGR_TRA           4
#define  SON_GRGR_COUNTYTAX     5
#define  SON_GRGR_CITYTAX       6
#define  SON_GRGR_SALEPRICE     7
#define  SON_GRGR_GRANTOR       8
#define  SON_GRGR_GRANTEE       9
#define  SON_GRGR_FLDCNT        10

static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "00", "L", 2,               // None
   "01", "Z", 2,               // Central
   "02", "D", 2,               // Wall Unit
   "03", "C", 2,               // Floor Unit
   "04", "I", 2,               // Radiant
   "05", "E", 2,               // Hydronic
   "06", "F", 2,               // Electric Baseboard
   "07", "G", 2,               // Heat Pump
   "08", "R", 2,               // Fireplace/Stove
   "09", "X", 2,               // Other
   "99", "Q", 2,               // Structure heating type
   "",   "",  0
};

static XLAT_CODE  asCooling[] =
{
   // Value, lookup code, value length
   "00", "N", 2,               // None
   "01", "C", 2,               // Central
   "02", "H", 2,               // Heat pump
   "03", "X", 2,               // Other
   "04", "X", 2,               // Unknown
   "99", "Y", 2,               // Structure cooling type
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "00", "N", 2,               // None
   "01", "G", 2,               // Gunite
   "02", "W", 2,               // Plastic liner
   "03", "F", 2,               // Fiberglass
   "04", "P", 2,               // Other
   "99", "P", 2,               // Pool
   "C",  "P", 1,               // Other?
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "0",  "N", 1,               // None
   "1",  "L", 1,               // Masonary
   "2",  "Z", 1,               // Zero Clearance
   "3",  "W", 1,               // Wood Stove
   "4",  "O", 1,               // Other
   "5",  "U", 1,               // Other?
   "6",  "U", 1,               // Other?
   "7",  "U", 1,               // Other?
   "8",  "U", 1,               // Other?
   "9",  "U", 1,               // Other?
   "10", "U", 2,               // Other?
   "11", "U", 2,               // Other?
   "20", "U", 2,               // Other?
   "99", "O", 2,               // Fireplace type
   "",   "", 0
};

IDX_TBL5 SON_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "01", "1 ", 'N', 2, 2,     // *100% transfer
   "02", "13", 'Y', 2, 2,     // *SEC 11 - Taxable Gov Prop
   "03", "13", 'Y', 2, 2,     // *Joint Tenants
   "04", "13", 'Y', 2, 2,     // *Domestic Partners
   "05", "74", 'Y', 2, 2,     // *New Mobile home
   "06", "74", 'Y', 2, 2,     // *LEOPS
   "07", "74", 'Y', 2, 2,     // *Transfer of P.I. or Unsec Bldg
   "08", "13", 'Y', 2, 2,     // *Non-reappraisal transfer
   "09", "19", 'Y', 2, 2,     // *Non market transfer DEED/QUITCLAIM/DEC QUIET TITLE/TRUSTEES DEED
   "10", "74", 'Y', 2, 2,     // *Residential Memo
   "11", "74", 'Y', 2, 2,     // New Single Family Residence
   "12", "74", 'Y', 2, 2,     // New Multi�Family Residence
   "13", "74", 'Y', 2, 2,     // Misc. Residential Improvement
   "14", "74", 'Y', 2, 2,     // Sfr Addition Or Remodel
   "15", "74", 'Y', 2, 2,     // Misc. Multi�Family Add/Remodel
   "16", "74", 'Y', 2, 2,     // Relocation Of Res Structure
   "17", "74", 'Y', 2, 2,     // Unsec Residential Improvement
   "18", "74", 'Y', 2, 2,     // *Partially Complete Residential
   "19", "74", 'Y', 2, 2,     // Residential Demolition
   "20", "74", 'Y', 2, 2,     // *Com/Ind Memo
   "21", "74", 'Y', 2, 2,     // New Com/Ind Structure
   "23", "74", 'Y', 2, 2,     // Misc. Com/Ind Structure
   "24", "74", 'Y', 2, 2,     // Com/Ind Alteration Or Remodel
   "26", "74", 'Y', 2, 2,     // RELOCATION OF COM/IND IMPS
   "27", "74", 'Y', 2, 2,     // LEASEHOLD IMPROVEMENTS
   "28", "74", 'Y', 2, 2,     // PARTIALLY COMPLETE COM/IND
   "29", "74", 'Y', 2, 2,     // COM/IND DEMOLITION
   "30", "74", 'Y', 2, 2,     // LAND IMPROVEMENTS
   "35", "74", 'Y', 2, 2,     // *35+ years lease
   "38", "74", 'Y', 2, 2,     // PARTIALLY COMPLETE LAND
   "40", "74", 'Y', 2, 2,     // AGRICULTURAL MEMO
   "41", "74", 'Y', 2, 2,     // VINEYARD IMPROVEMENTS
   "42", "74", 'Y', 2, 2,     // PLANTING OF TREES/VINES
   "43", "74", 'Y', 2, 2,     // AGRICULTURAL OUTBUILDINGS
   "44", "74", 'Y', 2, 2,     // RECREATIONAL OUTBUILDINGS
   "48", "74", 'Y', 2, 2,     // PARTIALLY COMPLETE AG/REC
   "49", "74", 'Y', 2, 2,     // MFGH CONV/TRANS SAME YR
   "50", "74", 'Y', 2, 2,     // CALAMITY DAMAGE, MEMO
   "51", "19", 'Y', 2, 2,     // *Split/Combination
   "52", "74", 'Y', 2, 2,     // *Relocation
   "53", "74", 'Y', 2, 2,     // SUBDIVISION
   "54", "74", 'Y', 2, 2,     // MFG HOME PLACEMENT
   "55", "74", 'Y', 2, 2,     // NEW UNSEC ASSESSMENT
   "56", "74", 'Y', 2, 2,     // SPLIT PENDING
   "57", "75", 'Y', 2, 2,     // *Grand Parent/Grand Child Prop 193
   "58", "75", 'Y', 2, 2,     // *Parent/Child transfer
   "59", "74", 'Y', 2, 2,     // AG PRESERVE/TPZ
   "60", "74", 'Y', 2, 2,     // Prop 60 transfer (base value transfer within county)
   "61", "74", 'Y', 2, 2,     // ENROLL PREVIOUSLY EXEMPT TREES/VINES
   "62", "74", 'Y', 2, 2,     // CALAMITY DAMAGE CLAIM
   "64", "74", 'Y', 2, 2,     // NON SECTION 170 DAMAGE
   "68", "74", 'Y', 2, 2,     // *Calamity/Em Domain Base Value Transfer
   "70", "74", 'Y', 2, 2,     // *MINERAL RIGHTS CREATE/TRANSFER
   "74", "74", 'Y', 2, 2,     // ASSESSMENT APPEAL
   "75", "74", 'Y', 2, 2,     // PARCEL RENUMBERING
   "76", "74", 'Y', 2, 2,     // *Roll Correction
   "77", "74", 'Y', 2, 2,     // PROP 8 REAPPRAISAL
   "78", "74", 'Y', 2, 2,     // POSSESSORY INTEREST (ON-GOING)
   "79", "74", 'Y', 2, 2,     // PROP 8 DONE
   "80", "19", 'Y', 2, 2,     // *Tax to Non-Tax
   "81", "13", 'N', 2, 2,     // *Non-Tax to Tax
   "82", "13", 'Y', 2, 2,     // *Non-Tax to Non-Tax
   "89", "13", 'Y', 2, 2,     // *Split/Reappraisable
   "92", "13", 'Y', 2, 2,     // *DEED
   "93", "19", 'Y', 2, 2,     // *DOD TRANSFER
   "97", "74", 'Y', 2, 2,     // HOX REQUEST
   "98", "57", 'Y', 2, 2,     // *Partial interest transfer
   "99", "74", 'Y', 2, 2,     // *CLERICAL FUNCTIONS
   "","",0,0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SON_Exemption[] = 
{
   "E01", "H", 3, 1,    // HOMEOWNERS EXEMPTION
   "E02", "V", 3, 1,    // VET DIED ON DUTY-FOR SPOUSE
   "E03", "V", 3, 1,    // VETERAN OWN PROPERTY
   "E06", "D", 3, 1,    // DISABLED VETERAN 100000
   "E07", "D", 3, 1,    // DISABLED VET 150000
   "E08", "W", 3, 1,    // WELFARE-CHARITABLE
   "E09", "S", 3, 1,    // WELFARE SCHOOL/EDUCATIONAL
   "E10", "I", 3, 1,    // WELFARE-HOSPITAL
   "E11", "R", 3, 1,    // WELFARE-RELIGIOUS
   "E12", "U", 3, 1,    // COLLEGE
   "E16", "E", 3, 1,    // CEMETERY
   "E17", "C", 3, 1,    // CHURCH
   "E18", "M", 3, 1,    // MUSEUM
   "E19", "X", 3, 1,    // MISCELLANEOUS
   "E21", "P", 3, 1,    // PUBLIC SCHOOL
   "E22", "R", 3, 1,    // RELIGIOUS-ONE TIME FILING
   "E23", "X", 3, 1,    // HISTORICAL AIRCRAFT
   "E24", "X", 3, 1,    // 4% FISHING VESSEL
   "E26", "X", 3, 1,    // MISC-LEASE OPTION PARTIAL LAND
   "E27", "X", 3, 1,    // MISC-LEASE OPTION PARTIAL IMPS
   "E28", "X", 3, 1,    // MISC-LEASE OPTION PARTIAL PP
   "E30", "W", 3, 1,    // PART WELFARE EDUCATION LAND
   "E31", "W", 3, 1,    // PARTIAL WELFARE EDUCATION IMPR
   "E32", "W", 3, 1,    // PARTIAL WELFARE EDUCATION PP
   "E33", "I", 3, 1,    // PARTIAL WELFARE HOSPITAL LAND
   "E34", "I", 3, 1,    // PARTIAL WELFARE HOSPITAL IMPRV
   "E35", "I", 3, 1,    // PARTIAL WELFARE HOSPITAL PP
   "E36", "R", 3, 1,    // PARTIAL WELFARE RELIGIOUS LAND
   "E37", "R", 3, 1,    // PARTIAL WELFARE RELIGIOUS IMPR
   "E38", "R", 3, 1,    // PARTIAL WELFARE RELIGIOUS PP
   "E40", "W", 3, 1,    // PARTIAL WELFARE-LAND
   "E41", "W", 3, 1,    // PARTIAL WELFARE-IMPRV
   "E42", "W", 3, 1,    // PARTIAL WELFARE - PP
   "E43", "E", 3, 1,    // PARTIAL CEMETERY LAND
   "E44", "E", 3, 1,    // PARTIAL CEMETERY IMPROV
   "E45", "E", 3, 1,    // PARTIAL CEMETERY PP
   "E46", "P", 3, 1,    // PARTIAL PUBLIC SCHOOL LAND
   "E47", "P", 3, 1,    // PARTIAL PUBLIC SCHOOL IMPROV
   "E48", "P", 3, 1,    // PARTIAL PUBLIC SCHOOL PP
   "E50", "C", 3, 1,    // PARTIAL CHURCH-LAND
   "E51", "C", 3, 1,    // PARTIAL CHURCH-IMPROVEMENTS
   "E52", "C", 3, 1,    // PARTIAL CHURCH PP
   "E53", "U", 3, 1,    // PARTIAL COLLEGE-LAND
   "E54", "U", 3, 1,    // PARTIAL COLLEGE-IMPROVEMENTS
   "E55", "U", 3, 1,    // COLLEGE EXEMPTION - PP
   "E60", "R", 3, 1,    // PARTIAL RELIGIOUS-LAND
   "E61", "R", 3, 1,    // PARTIAL RELIGIOUS-IMPR
   "E62", "R", 3, 1,    // PARTIAL RELIGIOUS-PP
   "E70", "M", 3, 1,    // PARTIAL FREE MUSEUM/PUBLIC LIBRAR
   "E71", "M", 3, 1,    // PARTIAL FREE MUSEUM/PUBLIC LIBRA
   "E72", "M", 3, 1,    // PARTIAL FREE MUSEUM/PUBLIC LIBRARY
   "E98", "X", 3, 1,    // OTHER EXEMPTIONS (INCL SOLDIERS & SAILORS)
   "","",0,0
};

#endif