#include "resource.h"
#include "hlAdo.h"

#ifndef _STANISLAUS_
#define _STANISLAUS_  

#define  OPEN_ERR       0xF0000001
#define  READ_ERR       0xF0000002
#define  WRITE_ERR      0xF0000004

#define  UPDATE_R01     0
#define  CREATE_R01     1
#define  CREATE_LIEN    2
#define  CLEAR_R01      4

#define  LOAD_LIEN      0x10000000
#define  LOAD_UPDT      0x01000000
#define  LOAD_GRGR      0x00100000
#define  LOAD_SALE      0x00010000
#define  LOAD_ATTR      0x00001000
#define  LOAD_ASSR      0x00000100
#define  LOAD_DAILY     0x00000010

#define  EXTR_LIEN      0x20000000
#define  EXTR_CSAL      0x02000000
#define  EXTR_VALUE     0x00200000
#define  EXTR_SALE      0x00020000
#define  EXTR_ATTR      0x00002000
#define  EXTR_REGN      0x00000200
#define  EXTR_PRP8      0x00000020

#define  MERG_LIEN      0x40000000
#define  MERG_CSAL      0x04000000
#define  MERG_GRGR      0x00400000
#define  MERG_GISA      0x00040000
#define  MERG_ATTR      0x00004000
#define  MERG_SADR      0x00000400
#define  MERG_PUBL      0x00000040

#define  UPDT_XSAL      0x00080000
#define  UPDT_ASSR      0x00000800
#define  FIX_CSDOC      0x00000080
#define  FIX_CSTYP      0x00000008

// MON_LIEN.CSV and CAL_LIEN.CSV
#define  L_ASMT                        0
#define  L_TAXYEAR                     1
#define  L_ROLLCHGNUM                  2
#define  L_MAPCATEGORY                 3
#define  L_ROLLCATEGORY                4
#define  L_ROLLTYPE                    5
#define  L_DESTINATIONROLL             6
#define  L_INSTALLMENTS                7
#define  L_BILLTYPE                    8
#define  L_FEEPARCEL                   9
#define  L_ORIGINATINGASMT             10
#define  L_XREFASMT                    11
#define  L_STATUS                      12
#define  L_TRA                         13
#define  L_TAXABILITY                  14
#define  L_ACRES                       15
#define  L_SIZEACRESFTTYPE             16
#define  L_INTDATEFROM                 17
#define  L_INTDATEFROM2                18
#define  L_INTDATETHRU                 19
#define  L_INTERESTTYPE                20
#define  L_USECODE                     21
#define  L_CURRENTMARKETLANDVALUE      22
#define  L_CURRENTFIXEDIMPRVALUE       23
#define  L_CURRENTGROWINGIMPRVALUE     24
#define  L_CURRENTSTRUCTURALIMPRVALUE  25
#define  L_CURRENTPERSONALPROPVALUE    26
#define  L_CURRENTPERSONALPROPMHVALUE  27
#define  L_CURRENTNETVALUE             28
#define  L_BILLEDMARKETLANDVALUE       29
#define  L_BILLEDFIXEDIMPRVALUE        30
#define  L_BILLEDGROWINGIMPRVALUE      31
#define  L_BILLEDSTRUCTURALIMPRVALUE   32
#define  L_BILLEDPERSONALPROPVALUE     33
#define  L_BILLEDPERSONALPROPMHVALUE   34
#define  L_BILLEDNETVALUE              35
#define  L_OWNER                       36
#define  L_ASSESSEE                    37
#define  L_MAILADDRESS1                38
#define  L_MAILADDRESS2                39
#define  L_MAILADDRESS3                40
#define  L_MAILADDRESS4                41
#define  L_ZIPMATCH                    42
#define  L_BARCODEZIP                  43
#define  L_EXEMPTIONCODE1              44
#define  L_EXEMPTIONAMT1               45
#define  L_EXEMPTIONCODE2              46
#define  L_EXEMPTIONAMT2               47
#define  L_EXEMPTIONCODE3              48
#define  L_EXEMPTIONAMT3               49
#define  L_BILLDATE                    50
#define  L_DUEDATE1                    51
#define  L_DUEDATE2                    52
#define  L_TAXAMT1                     53
#define  L_TAXAMT2                     54
#define  L_PENAMT1                     55
#define  L_PENAMT2                     56
#define  L_COST1                       57
#define  L_COST2                       58
#define  L_PENCHRGDATE1                59
#define  L_PENCHRGDATE2                60
#define  L_PAIDAMT1                    61
#define  L_PAIDAMT2                    62
#define  L_PAYMENTDATE1                63
#define  L_PAYMENTDATE2                64
#define  L_TRANSDATE1                  65
#define  L_TRANSDATE2                  66
#define  L_COLLECTIONNUM1              67
#define  L_COLLECTIONNUM2              68
#define  L_UNSDELINQPENAMTPAID1        69
#define  L_SECDELINQPENAMTPAID2        70
#define  L_TOTALFEESPAID1              71
#define  L_TOTALFEESPAID2              72
#define  L_TOTALFEES                   73
#define  L_PMTCNLDATE1                 74
#define  L_PMTCNLDATE2                 75
#define  L_TRANSFERDATE                76
#define  L_PRORATIONFACTOR             77
#define  L_PRORATIONPCT                78
#define  L_DAYSTOFISCALYEAREND         79
#define  L_DAYSOWNED                   80
#define  L_OWNFROM                     81
#define  L_OWNTHRU                     82
#define  L_SITUS1                      83
#define  L_SITUS2                      84
#define  L_ASMTROLLYEAR                85
#define  L_PARCELDESCRIPTION           86
#define  L_BILLCOMMENTSLINE1           87
#define  L_BILLCOMMENTSLINE2           88
#define  L_DEFAULTNUM                  89
#define  L_DEFAULTDATE                 90
#define  L_REDEEMEDDATE                91
#define  L_SECDELINQFISCALYEAR         92
#define  L_PRIORTAXPAID1               93
#define  L_PENINTERESTCODE             94
#define  L_FOURPAYPLANNUM              95
#define  L_EXISTSLIEN                  96
#define  L_EXISTSCORTAC                97
#define  L_EXISTSCOUNTYCORTAC          98
#define  L_EXISTSBANKRUPTCY            99
#define  L_EXISTSREFUND                100
#define  L_EXISTSDELINQUENTVESSEL      101
#define  L_EXISTSNOTES                 102
#define  L_EXISTSCRITICALNOTE          103
#define  L_EXISTSROLLCHG               104
#define  L_ISPENCHRGCANCELED1          105
#define  L_ISPENCHRGCANCELED2          106
#define  L_ISPERSONALPROPERTYPENALTY   107
#define  L_ISAGPRESERVE                108
#define  L_ISCARRYOVER1STPAID          109
#define  L_ISCARRYOVERRECORD           110
#define  L_ISFAILURETOFILE             111
#define  L_ISOWNERSHIPPENALTY          112
#define  L_ISELIGIBLEFOR4PAY           113
#define  L_ISNOTICE4SENT               114
#define  L_ISPARTPAY                   115
#define  L_ISFORMATTEDADDRESS          116
#define  L_ISADDRESSCONFIDENTIAL       117
#define  L_SUPLCNT                     118
#define  L_ORIGINALDUEDATE1            119
#define  L_ORIGINALDUEDATE2            120
#define  L_ISPEN1REFUND                121
#define  L_ISPEN2REFUND                122
#define  L_ISDELINQPENREFUND           123
#define  L_ISREFUNDAUTHORIZED          124
#define  L_ISNODISCHARGE               125
#define  L_ISAPPEALPENDING             126
#define  L_OTHERFEESPAID               127
#define  L_FOURPAYREASON               128
#define  L_DATEDISCHARGED              129
#define  L_ISONLYFIRSTPAID             130
#define  L_ISALLPAID                   131
#define  L_DTS                         132
#define  L_USERID                      133

// Group MB1
#define  L1_ASMT                        0
#define  L1_TAXYEAR                     1
#define  L1_MAPCATEGORY                 2
#define  L1_ROLLCATEGORY                3
#define  L1_ROLLTYPE                    4
#define  L1_DESTINATIONROLL             5
#define  L1_INSTALLMENTS                6
#define  L1_BILLTYPE                    7
#define  L1_FEEPARCEL                   8
#define  L1_ORIGINATINGASMT             9
#define  L1_XREFASMT                    10
#define  L1_STATUS                      11
#define  L1_TRA                         12
#define  L1_TAXABILITY                  13
#define  L1_ACRES                       14
#define  L1_SIZEACRESFTTYPE             15
#define  L1_INTDATEFROM                 16
#define  L1_INTDATEFROM2                17
#define  L1_INTDATETHRU                 18
#define  L1_INTERESTTYPE                19
#define  L1_USECODE                     20
#define  L1_CURRENTMARKETLANDVALUE      21
#define  L1_CURRENTFIXEDIMPRVALUE       22
#define  L1_CURRENTGROWINGIMPRVALUE     23
#define  L1_CURRENTSTRUCTURALIMPRVALUE  24
#define  L1_CURRENTPERSONALPROPVALUE    25
#define  L1_CURRENTPERSONALPROPMHVALUE  26
#define  L1_CURRENTNETVALUE             27
#define  L1_BILLEDMARKETLANDVALUE       28
#define  L1_BILLEDFIXEDIMPRVALUE        29
#define  L1_BILLEDGROWINGIMPRVALUE      30
#define  L1_BILLEDSTRUCTURALIMPRVALUE   31
#define  L1_BILLEDPERSONALPROPVALUE     32
#define  L1_BILLEDPERSONALPROPMHVALUE   33
#define  L1_BILLEDNETVALUE              34
#define  L1_OWNER                       35
#define  L1_ASSESSEE                    36
#define  L1_MAILADDRESS1                37
#define  L1_MAILADDRESS2                38
#define  L1_MAILADDRESS3                39
#define  L1_MAILADDRESS4                40
#define  L1_ZIPMATCH                    41
#define  L1_BARCODEZIP                  42
#define  L1_EXEMPTIONCODE1              43
#define  L1_EXEMPTIONAMT1               44
#define  L1_EXEMPTIONCODE2              45
#define  L1_EXEMPTIONAMT2               46
#define  L1_EXEMPTIONCODE3              47
#define  L1_EXEMPTIONAMT3               48
#define  L1_BILLDATE                    49
#define  L1_DUEDATE1                    50
#define  L1_DUEDATE2                    51
#define  L1_TAXAMT1                     52
#define  L1_TAXAMT2                     53
#define  L1_PENAMT1                     54
#define  L1_PENAMT2                     55
#define  L1_COST1                       56
#define  L1_COST2                       57
#define  L1_PENCHRGDATE1                58
#define  L1_PENCHRGDATE2                59
#define  L1_PAIDAMT1                    60
#define  L1_PAIDAMT2                    61
#define  L1_PAYMENTDATE1                62
#define  L1_PAYMENTDATE2                63
#define  L1_TRANSDATE1                  64
#define  L1_TRANSDATE2                  65
#define  L1_COLLECTIONNUM1              66
#define  L1_COLLECTIONNUM2              67
#define  L1_UNSDELINQPENAMTPAID1        68
#define  L1_SECDELINQPENAMTPAID2        69
#define  L1_TOTALFEESPAID1              70
#define  L1_TOTALFEESPAID2              71
#define  L1_TOTALFEES                   72
#define  L1_PMTCNLDATE1                 73
#define  L1_PMTCNLDATE2                 74
#define  L1_TRANSFERDATE                75
#define  L1_PRORATIONFACTOR             76
#define  L1_PRORATIONPCT                77
#define  L1_DAYSTOFISCALYEAREND         78
#define  L1_DAYSOWNED                   79
#define  L1_OWNFROM                     80
#define  L1_OWNTHRU                     81
#define  L1_SITUS1                      82
#define  L1_SITUS2                      83
#define  L1_ASMTROLLYEAR                84
#define  L1_PARCELDESCRIPTION           85
#define  L1_BILLCOMMENTSLINE1           86
#define  L1_BILLCOMMENTSLINE2           87
#define  L1_DEFAULTNUM                  88
#define  L1_DEFAULTDATE                 89
#define  L1_REDEEMEDDATE                90
#define  L1_SECDELINQFISCALYEAR         91
#define  L1_PRIORTAXPAID1               92
#define  L1_PENINTERESTCODE             93
#define  L1_FOURPAYPLANNUM              94
#define  L1_EXISTSLIEN                  95
#define  L1_EXISTSCORTAC                96
#define  L1_EXISTSCOUNTYCORTAC          97
#define  L1_EXISTSBANKRUPTCY            98
#define  L1_EXISTSREFUND                99
#define  L1_EXISTSDELINQUENTVESSEL      100
#define  L1_EXISTSNOTES                 101
#define  L1_EXISTSCRITICALNOTE          102
#define  L1_EXISTSROLLCHG               103
#define  L1_ISPENCHRGCANCELED1          104
#define  L1_ISPENCHRGCANCELED2          105
#define  L1_ISPERSONALPROPERTYPENALTY   106
#define  L1_ISAGPRESERVE                107
#define  L1_ISCARRYOVER1STPAID          108
#define  L1_ISCARRYOVERRECORD           109
#define  L1_ISFAILURETOFILE             110
#define  L1_ISOWNERSHIPPENALTY          111
#define  L1_ISELIGIBLEFOR4PAY           112
#define  L1_ISNOTICE4SENT               113
#define  L1_ISPARTPAY                   114
#define  L1_ISFORMATTEDADDRESS          115
#define  L1_ISADDRESSCONFIDENTIAL       116
#define  L1_SUPLCNT                     117
#define  L1_ORIGINALDUEDATE1            118
#define  L1_ORIGINALDUEDATE2            119
#define  L1_ISPEN1REFUND                120
#define  L1_ISPEN2REFUND                121
#define  L1_ISDELINQPENREFUND           122
#define  L1_ISREFUNDAUTHORIZED          123
#define  L1_ISNODISCHARGE               124
#define  L1_ISAPPEALPENDING             125
#define  L1_OTHERFEESPAID               126
#define  L1_FOURPAYREASON               127
#define  L1_DATEDISCHARGED              128
#define  L1_ISONLYFIRSTPAID             129
#define  L1_ISALLPAID                   131
#define  L1_DTS                         132
#define  L1_USERID                      133
#define  L1_CORTAC                      134

#define  L2_ASMT                        0
#define  L2_TAXYEAR                     1
#define  L2_ROLLCHGNUM                  2
#define  L2_MAPCATEGORY                 3
#define  L2_ROLLCATEGORY                4
#define  L2_ROLLTYPE                    5
#define  L2_DESTINATIONROLL             6
#define  L2_INSTALLMENTS                7
#define  L2_BILLTYPE                    8
#define  L2_FEEPARCEL                   9
#define  L2_ORIGINATINGASMT             10
#define  L2_XREFASMT                    11
#define  L2_STATUS                      12
#define  L2_TRA                         13
#define  L2_TAXABILITY                  14
#define  L2_ACRES                       15
#define  L2_SIZEACRESFTTYPE             16
#define  L2_INTDATEFROM                 17
#define  L2_INTDATEFROM2                18
#define  L2_INTDATETHRU                 19
#define  L2_INTERESTTYPE                20
#define  L2_USECODE                     21
#define  L2_CURRENTMARKETLANDVALUE      22
#define  L2_CURRENTFIXEDIMPRVALUE       23
#define  L2_CURRENTGROWINGIMPRVALUE     24
#define  L2_CURRENTSTRUCTURALIMPRVALUE  25
#define  L2_CURRENTPERSONALPROPVALUE    26
#define  L2_CURRENTPERSONALPROPMHVALUE  27
#define  L2_CURRENTNETVALUE             28
#define  L2_BILLEDMARKETLANDVALUE       29
#define  L2_BILLEDFIXEDIMPRVALUE        30
#define  L2_BILLEDGROWINGIMPRVALUE      31
#define  L2_BILLEDSTRUCTURALIMPRVALUE   32
#define  L2_BILLEDPERSONALPROPVALUE     33
#define  L2_BILLEDPERSONALPROPMHVALUE   34
#define  L2_BILLEDNETVALUE              35
#define  L2_OWNER                       36
#define  L2_ASSESSEE                    37
#define  L2_MAILADDRESS1                38
#define  L2_MAILADDRESS2                39
#define  L2_MAILADDRESS3                40
#define  L2_MAILADDRESS4                41
#define  L2_ZIPMATCH                    42
#define  L2_BARCODEZIP                  43
#define  L2_EXEMPTIONCODE1              44
#define  L2_EXEMPTIONAMT1               45
#define  L2_EXEMPTIONCODE2              46
#define  L2_EXEMPTIONAMT2               47
#define  L2_EXEMPTIONCODE3              48
#define  L2_EXEMPTIONAMT3               49
#define  L2_BILLDATE                    50
#define  L2_DUEDATE1                    51
#define  L2_DUEDATE2                    52
#define  L2_TAXAMT1                     53
#define  L2_TAXAMT2                     54
#define  L2_PENAMT1                     55
#define  L2_PENAMT2                     56
#define  L2_COST1                       57
#define  L2_COST2                       58
#define  L2_PENCHRGDATE1                59
#define  L2_PENCHRGDATE2                60
#define  L2_PAIDAMT1                    61
#define  L2_PAIDAMT2                    62
#define  L2_PAYMENTDATE1                63
#define  L2_PAYMENTDATE2                64
#define  L2_TRANSDATE1                  65
#define  L2_TRANSDATE2                  66
#define  L2_COLLECTIONNUM1              67
#define  L2_COLLECTIONNUM2              68
#define  L2_UNSDELINQPENAMTPAID1        69
#define  L2_SECDELINQPENAMTPAID2        70
#define  L2_TOTALFEESPAID1              71
#define  L2_TOTALFEESPAID2              72
#define  L2_TOTALFEES                   73
#define  L2_PMTCNLDATE1                 74
#define  L2_PMTCNLDATE2                 75
#define  L2_TRANSFERDATE                76
#define  L2_PRORATIONFACTOR             77
#define  L2_PRORATIONPCT                78
#define  L2_DAYSTOFISCALYEAREND         79
#define  L2_DAYSOWNED                   80
#define  L2_OWNFROM                     81
#define  L2_OWNTHRU                     82
#define  L2_SITUS1                      83
#define  L2_SITUS2                      84
#define  L2_ASMTROLLYEAR                85
#define  L2_PARCELDESCRIPTION           86
#define  L2_BILLCOMMENTSLINE1           87
#define  L2_BILLCOMMENTSLINE2           88
#define  L2_DEFAULTNUM                  89
#define  L2_DEFAULTDATE                 90
#define  L2_REDEEMEDDATE                91
#define  L2_SECDELINQFISCALYEAR         92
#define  L2_PRIORTAXPAID1               93
#define  L2_PENINTERESTCODE             94
#define  L2_FOURPAYPLANNUM              95
#define  L2_EXISTSLIEN                  96
#define  L2_EXISTSCORTAC                97
#define  L2_EXISTSCOUNTYCORTAC          98
#define  L2_EXISTSBANKRUPTCY            99
#define  L2_EXISTSREFUND                100
#define  L2_EXISTSDELINQUENTVESSEL      101
#define  L2_EXISTSNOTES                 102
#define  L2_EXISTSCRITICALNOTE          103
#define  L2_EXISTSROLLCHG               104
#define  L2_ISPENCHRGCANCELED1          105
#define  L2_ISPENCHRGCANCELED2          106
#define  L2_ISPERSONALPROPERTYPENALTY   107
#define  L2_ISAGPRESERVE                108
#define  L2_ISCARRYOVER1STPAID          109
#define  L2_ISCARRYOVERRECORD           110
#define  L2_ISFAILURETOFILE             111
#define  L2_ISOWNERSHIPPENALTY          112
#define  L2_ISELIGIBLEFOR4PAY           113
#define  L2_ISNOTICE4SENT               114
#define  L2_ISPARTPAY                   115
#define  L2_ISFORMATTEDADDRESS          116
#define  L2_ISADDRESSCONFIDENTIAL       117
#define  L2_SUPLCNT                     118
#define  L2_ORIGINALDUEDATE1            119
#define  L2_ORIGINALDUEDATE2            120
#define  L2_ISPEN1REFUND                121
#define  L2_ISPEN2REFUND                122
#define  L2_ISDELINQPENREFUND           123
#define  L2_ISREFUNDAUTHORIZED          124
#define  L2_ISNODISCHARGE               125
#define  L2_ISAPPEALPENDING             126
#define  L2_OTHERFEESPAID               127
#define  L2_FOURPAYREASON               128
#define  L2_DATEDISCHARGED              129
#define  L2_ISONLYFIRSTPAID             130
#define  L2_ISALLPAID                   131
#define  L2_DTS                         132
#define  L2_USERID                      133
#define  L2_TAX_RATEUSED                134
#define  L2_TAX_ROLLCHGNUM              135

// 2016 MER, STA
#define  L3_TAXYEAR                    0
#define  L3_ROLLCATEGORY               1
#define  L3_ASMT                       2
#define  L3_FEEPARCEL                  3
#define  L3_ASMTSTATUS                 4
#define  L3_TRA                        5
#define  L3_TAXABILITYFULL             6
#define  L3_OWNER                      7
#define  L3_ASSESSEENAME               8
#define  L3_MAILADDRESS1               9
#define  L3_MAILADDRESS2               10
#define  L3_MAILADDRESS3               11
#define  L3_MAILADDRESS4               12
#define  L3_BARCODEZIP                 13
#define  L3_SITUS1                     14
#define  L3_SITUS2                     15
#define  L3_PARCELDESCRIPTION          16
#define  L3_LANDVALUE                  17
#define  L3_STRUCTUREVALUE             18
#define  L3_FIXTURESVALUE              19
#define  L3_GROWING                    20
#define  L3_FIXTURESRP                 21
#define  L3_MHPPVALUE                  22
#define  L3_PPVALUE                    23
#define  L3_HOX                        24
#define  L3_OTHEREXEMPTION             25
#define  L3_OTHEREXEMPTIONCODE         26
#define  L3_LANDUSE1                   27
#define  L3_LANDSIZE                   28
#define  L3_ACRES                      29
#define  L3_BUILDINGTYPE               30
#define  L3_QUALITYCLASS               31
#define  L3_BUILDINGSIZE               32
#define  L3_YEARBUILT                  33
#define  L3_TOTALAREA                  34
#define  L3_BEDROOMS                   35
#define  L3_BATHS                      36
#define  L3_HALFBATHS                  37
#define  L3_TOTALROOMS                 38
#define  L3_GARAGE                     39
#define  L3_GARAGESIZE                 40
#define  L3_HEATING                    41
#define  L3_AC                         42
#define  L3_FIREPLACE                  43
#define  L3_POOLSPA                    44
#define  L3_STORIES                    45
#define  L3_UNITS                      46
#define  L3_CURRENTDOCNUM              47
#define  L3_CURRENTDOCDATE             48
#define  L3_LANDUSE2                   49    // EDX
#define  L3_ISAGPRESERVE               49    // MER,STA 2019
#define  L3_COLS                       50

// _ROLL
#define  MB_ROLL_ASMT                 0
#define  MB_ROLL_FEEPARCEL            1
#define  MB_ROLL_TRA                  2
#define  MB_ROLL_LEGAL                3
#define  MB_ROLL_ZONING               4
#define  MB_ROLL_USECODE              5
#define  MB_ROLL_NBHCODE              6
#define  MB_ROLL_ACRES                7
#define  MB_ROLL_DOCNUM               8
#define  MB_ROLL_DOCDATE              9
#define  MB_ROLL_TAXABILITY           10
#define  MB_ROLL_OWNER                11
#define  MB_ROLL_CAREOF               12
#define  MB_ROLL_DBA                  13
#define  MB_ROLL_M_ADDR               14
#define  MB_ROLL_M_CITY               15
#define  MB_ROLL_M_ST                 16
#define  MB_ROLL_M_ZIP                17
#define  MB_ROLL_LAND                 18
#define  MB_ROLL_HOMESITE             19
#define  MB_ROLL_IMPR                 20
#define  MB_ROLL_GROWING              21
#define  MB_ROLL_FIXTRS               22

#define  MB_ROLL_FIXTR_RP             23  // Old script
//#define  MB_ROLL_FIXTR_BUS            24
#define  MB_ROLL_PP_BUS               24

//#define  MB_ROLL_PERSPROP             23  // New script
//#define  MB_ROLL_BUSPROP              24

#define  MB_ROLL_PPMOBILHOME          25
#define  MB_ROLL_M_ADDR1              26
#define  MB_ROLL_M_ADDR2              27
#define  MB_ROLL_M_ADDR3              28
#define  MB_ROLL_M_ADDR4              29
#define  STA_ROLL_COLS                30

// _EXE
#define  MB_EXE_STATUS                0
#define  MB_EXE_ASMT                  1
#define  MB_EXE_CODE                  2
#define  MB_EXE_HOEXE                 3
#define  MB_EXE_EXEAMT                4
#define  MB_EXE_EXEPCT                5

// _CHAR
/*
#define  MB_CHAR_FEE_PRCL             0
#define  MB_CHAR_POOLS                1
#define  MB_CHAR_USECAT               2
#define  MB_CHAR_LANDUSE              3
#define  MB_CHAR_QUALITY              4
#define  MB_CHAR_YRBLT                5
#define  MB_CHAR_BLDGSQFT             6
#define  MB_CHAR_GARSQFT              7
#define  MB_CHAR_HEATING              8
#define  MB_CHAR_COOLING              9
#define  MB_CHAR_HEATING_SRC          10
#define  MB_CHAR_COOLING_SRC          11
#define  MB_CHAR_BEDS                 12
#define  MB_CHAR_FBATHS               13
#define  MB_CHAR_HBATHS               14
#define  MB_CHAR_FP                   15
#define  MB_CHAR_ASMT                 16
#define  MB_CHAR_HASSEPTIC            17
#define  MB_CHAR_HASSEWER             18
#define  MB_CHAR_HASWELL              19
#define  MB_CHAR_BLDGSEQNO            20
#define  MB_CHAR_LOTACRES             21  
#define  MB_CHAR_LOTSQFT              22
#define  MB_CHAR_DOCNO                23
#define  MB_CHAR_ASMT_STATUS          24
#define  MB_CHAR_UNITSEQNO            25  
*/
// 9/30/2013
#define  MB_CHAR_FEE_PRCL             0
#define  MB_CHAR_POOLS                1
#define  MB_CHAR_USECAT               2
#define  MB_CHAR_LANDUSE              3
#define  MB_CHAR_QUALITY              4
#define  MB_CHAR_YRBLT                5
#define  MB_CHAR_BLDGSQFT             6
#define  MB_CHAR_GARSQFT              7
#define  MB_CHAR_HEATING              8
#define  MB_CHAR_COOLING              9
#define  MB_CHAR_BEDS                 10
#define  MB_CHAR_FBATHS               11
#define  MB_CHAR_HBATHS               12
#define  MB_CHAR_FP                   13
#define  MB_CHAR_ASMT                 14
#define  MB_CHAR_HASSEWER             15
#define  MB_CHAR_HASWELL              16
#define  MB_CHAR_BLDGSEQNO            17
#define  MB_CHAR_LOTACRES             18  
#define  MB_CHAR_LOTSQFT              19
#define  MB_CHAR_DOCNO                20
#define  MB_CHAR_ASMT_STATUS          21
#define  MB_CHAR_UNITSEQNO            22  

#define  MBSIZ_CHAR_ASMT              12
#define  MBSIZ_CHAR_POOLS             2
#define  MBSIZ_CHAR_USECAT            2
#define  MBSIZ_CHAR_QUALITY           6
#define  MBSIZ_CHAR_YRBLT             4
#define  MBSIZ_CHAR_BLDGSQFT          9
#define  MBSIZ_CHAR_GARSQFT           9
#define  MBSIZ_CHAR_HEATING           2
#define  MBSIZ_CHAR_COOLING           2
#define  MBSIZ_CHAR_HEATSRC           2
#define  MBSIZ_CHAR_COOLSRC           2
#define  MBSIZ_CHAR_BEDS              3
#define  MBSIZ_CHAR_FBATHS            3
#define  MBSIZ_CHAR_HBATHS            3
#define  MBSIZ_CHAR_FP                2
#define  MBSIZ_CHAR_HASSEPTIC         1
#define  MBSIZ_CHAR_HASSEWER          1
#define  MBSIZ_CHAR_HASWELL           1
// Sonoma
#define  MBSIZ_CHAR_DOCNO             12
#define  MBSIZ_CHAR_BLDGSEQNO         2
#define  MBSIZ_CHAR_UNITSEQNO         2
// Yolo
#define  MBSIZ_CHAR_NUMFLOORS         3
#define  MBSIZ_CHAR_LANDSQFT          9
#define  MBSIZ_CHAR_TOTALROOMS        4  
#define  MBSIZ_CHAR_PARKTYPE          4
#define  MBSIZ_CHAR_PARKSPACES        4
/*
#define  MBOFF_CHAR_ASMT              1
#define  MBOFF_CHAR_POOLS             13
#define  MBOFF_CHAR_USECAT            15
#define  MBOFF_CHAR_QUALITY           17
#define  MBOFF_CHAR_YRBLT             23
#define  MBOFF_CHAR_BLDGSQFT          27
#define  MBOFF_CHAR_GARSQFT           36
#define  MBOFF_CHAR_HEATING           45
#define  MBOFF_CHAR_COOLING           47
#define  MBOFF_CHAR_HEATSRC           49
#define  MBOFF_CHAR_COOLSRC           51
#define  MBOFF_CHAR_BEDS              53
#define  MBOFF_CHAR_FBATHS            56
#define  MBOFF_CHAR_HBATHS            59
#define  MBOFF_CHAR_FP                62
#define  MBOFF_CHAR_HASSEPTIC         64
#define  MBOFF_CHAR_HASSEWER          65
#define  MBOFF_CHAR_HASWELL           66
// Sonoma
#define  MBOFF_CHAR_DOCNO             67
#define  MBOFF_CHAR_BLDGSEQNO         79
#define  MBOFF_CHAR_UNITSEQNO         81
*/
// _SITUS
#define  MB_SITUS_ASMT                0
#define  MB_SITUS_STRNAME             1
#define  MB_SITUS_STRNUM              2
#define  MB_SITUS_STRTYPE             3
#define  MB_SITUS_STRDIR              4
#define  MB_SITUS_UNIT                5
#define  MB_SITUS_COMMUNITY           6
#define  MB_SITUS_ZIP                 7
#define  MB_SITUS_SEQ                 8

// _SALES
#define  MB_SALES_ASMT                0
#define  MB_SALES_DOCNUM              1
#define  MB_SALES_DOCDATE             2
#define  MB_SALES_DOCCODE             3
#define  MB_SALES_SELLER              4
#define  MB_SALES_BUYER               5
#define  MB_SALES_TAXAMT              6
#define  MB_SALES_GROUPSALE           7
#define  MB_SALES_GROUPASMT           8
#define  MB_SALES_XFERTYPE            9
#define  MB_SALES_ADJREASON           10
#define  MB_SALES_CONFCODE            11
#define  MB_SALES_DTS                 12

// _TAX
#define  MB_TAX_ASMT                  0
#define  MB_TAX_FEEPARCEL             1
#define  MB_TAX_TAXAMT1               2
#define  MB_TAX_TAXAMT2               3
#define  MB_TAX_PENAMT1               4
#define  MB_TAX_PENAMT2               5
#define  MB_TAX_PENDATE1              6
#define  MB_TAX_PENDATE2              7
#define  MB_TAX_PAIDAMT1              8
#define  MB_TAX_PAIDAMT2              9
#define  MB_TAX_TOTALPAID1            10
#define  MB_TAX_TOTALPAID2            11
#define  MB_TAX_TOTALFEES             12
#define  MB_TAX_PAIDDATE1             13
#define  MB_TAX_PAIDDATE2             14
#define  MB_TAX_YEAR                  15
#define  MB_TAX_ROLLCAT               16

typedef struct _tApnRange
{
   char  acApn[12];
   int   iLen;
} APN_RNG;

static XLAT_CODE  asSewer[] =
{  // 11/03/2015
   // Value, lookup code, value length
   "E", "E", 1,               // Engineered System
   "M", "P", 1,               // Municipal Sewer Hookup
   "O", "Z", 1,               // Other Than Sewer or Septic
   "S", "Y", 1,               // Sewer
   "T", "S", 1,               // Septic Tank
   "",   "", 0
};

static XLAT_CODE  asHeating[] =
{  // 11/03/2015
   // Value, lookup code, value length
   "C", "Z", 1,               // Central
   "W", "D", 1,               // Wall
   "Z", "T", 1,               // Zone
   "F", "C", 1,               // Floor
   "R", "I", 1,               // Radiator
   "S", "S", 1,               // Stove
   "DW","D", 2,               // Double wall 
   "BC","Z", 2,               // Baseboard-Central
   "B", "F", 1,               // Baseboard
   "EC","Z", 2,               // Electric-Central
   "E", "F", 1,               // Electric
   "GC","Z", 2,               // Gas-Central
   "GR","I", 2,               // Gas-Radiant
   "GW","D", 2,               // Gas-Wall
   "GZ","T", 2,               // Gas-Zone
   "G", "Z", 1,               // Gas
   "WC","Z", 2,               // Wall-Central 
   "",   "", 0
};

static XLAT_CODE  asCooling[] =
{  // 11/03/2015
   // Value, lookup code, value length
   "C",  "C", 1,              // Central
   "E",  "E", 1,              // Evaporative Cooler
   "W",  "L", 1,              // Wall
   "RWW","W", 3,              // Room/Wall - Window
   "RW", "L", 2,              // Room/Wall
   "ERW","E", 3,              // Evaporative - Room/Wall
   "CW", "C", 2,              // Central - Window
   "CRW","C", 3,              // Central - Room/Wall
   "CE", "C", 2,              // Central - Evaporative
   "O",  "X", 1,              // Other
   "",   "",  0
};

static XLAT_CODE  asHeatingSrc[] =
{
   // Value, lookup code, value length
   "C",  "Z", 1,              // Central
   "Z",  "T", 1,              // Zone
   "F",  "C", 1,              // Floor
   "GC", "Z", 2,              // Natural Gas, Central
   "R",  "I", 1,              // Radiator
   "DW", "D", 2,              // Double wall 
   "GZ", "T", 2,              // Natural Gas, Zone
   "S",  "S", 1,              // Stove
   "99", "Q", 2,              // Structure Heating Type
   "EC", "Z", 2,              // Electric, Central
   "BC", "Z", 2,              // Propane, Central
   "B",  "F", 1,              // Baseboard
   "WC", "Z", 2,              // Wood stove, Central
   "W",  "D", 1,              // Wall
   "E",  "F", 1,              // Electric
   "GW", "W", 2,              // Natural Gas, Wall
   "GR", "I", 2,              // Natural Gas
   "G",  "N", 1,              // Natural Gas
   "",   " ", 0
};

static XLAT_CODE  asCoolingSrc[] =
{
   // Value, lookup code, value length
   "CE", "C", 2,              // Central
   "C",  "C", 1,              // Central
   "E",  "Q", 1,              // Electric
   "RW", "L", 2,              // Wall
   "O",  "X", 1,              // Other
   "",   "", 0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length 
   "GS", "C", 2,              // Gunite Pool /w Spa
   "G",  "G", 1,              // Gunite (in ground)
   "S",  "S", 1,              // Spa
   "A",  "X", 1,              // Pool (above ground)
   "V",  "V", 1,              // Vinyl (in ground)
   "F",  "F", 1,              // Fiberglass (in ground)
   "N",  "N", 1,              // None
   "",   "",  0
};

IDX_TBL5 STA_DocCode[] =
{
   // DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN
   "01","1  ",'N',2,3,  // *Sale-Reappraise
   "02","13 ",'N',2,3,  // *Transfer-Reappraise
   "03","13 ",'Y',2,3,  // *Sale-No Reappraisal
   "04","13 ",'Y',2,3,  // *Transfer-No Reappraisal
   "05","57 ",'N',2,3,  // *Partial Transfer-Reappraise
   "06","57 ",'Y',2,3,  // *Partial Transfer-No Reappraisal
   //"07","19 ",'Y',2,3,  // Split/Combo
   "08","19 ",'Y',2,3,  // *Split/Combo w Sale-Reappraise
   //"09","19 ",'Y',2,3,  // Split/Combo w Transfer-No Reapp
   "10","19 ",'Y',2,3,  // *Mobile Home Transfer
   "11","75 ",'Y',2,3,  // *Government Transfer/No Supl
   "12","1  ",'N',2,3,  // *Direct Enrollment Transfer
   //"13","74 ",'Y',2,3,  // Clerical Function Prior Owner supp
   "14","19 ",'Y',2,3,  // *MH Transfer-No Supl
   //"15","74 ",'Y',2,3,  // New Single Family Dwelling
   //"16","74 ",'Y',2,3,  // New Garage/Carport
   //"17","74 ",'Y',2,3,  // New Swimming Pool/Spa
   //"18","74 ",'Y',2,3,  // New Patio/Deck
   //"19","74 ",'Y',2,3,  // Res Addn/Conversion
   //"20","74 ",'Y',2,3,  // Gar Addn/Conversion
   //"21","74 ",'Y',2,3,  // New Multiple Building
   //"22","74 ",'Y',2,3,  // Multiple Addn/Conv
   //"23","74 ",'Y',2,3,  // New Rural Building
   //"24","74 ",'Y',2,3,  // Rural Addn/Conversion
   //"25","74 ",'Y',2,3,  // Land Improvements
   //"26","74 ",'Y',2,3,  // New Industrial Building
   //"27","74 ",'Y',2,3,  // Industrial Addn/Conversion
   //"28","74 ",'Y',2,3,  // New Commercial Building
   //"29","74 ",'Y',2,3,  // Commercial Addn/Conversion
   //"30","74 ",'Y',2,3,  // Misc Comm/Ind Tenant Imp
   //"31","74 ",'Y',2,3,  // Demo/Removal
   //"32","74 ",'Y',2,3,  // Moved/Relocated Improvements
   //"33","74 ",'Y',2,3,  // Rehab/Repair
   //"34","74 ",'Y',2,3,  // Fire Damage
   //"35","74 ",'Y',2,3,  // Offsite Development
   //"36","74 ",'Y',2,3,  // New Mobile Home
   //"37","74 ",'Y',2,3,  // Mobile Home Addn/Conversion
   //"38","74 ",'Y',2,3,  // Miscellaneous
   //"39","74 ",'Y',2,3,  // Prop 8
   //"40","74 ",'Y',2,3,  // Clerical Function
   //"41","74 ",'Y',2,3,  // Land Conservation Agreement
   //"42","74 ",'Y',2,3,  // Non-Renew LCA
   //"43","74 ",'Y',2,3,  // Leasehold Improvements
   "46","1  ",'N',2,3,  // *Grant deed
   "52","27 ",'Y',2,3,  // *Transfer-Default-Trustee's Deed
   "53","13 ",'Y',2,3,  // *Deed
   "54","19 ",'Y',2,3,  // *Recission
   "57","75 ",'Y',2,3,  // Transfer on death
   "58","74 ",'Y',2,3,  // *Prop 58-Non Reappraisal
   "59","1  ",'N',2,3,  // *Grant deed
   "60","74 ",'Y',2,3,  // *Prop 60-Replacement Dwelling
   //"61","74 ",'Y',2,3,  // Calamity Application
   //"62","74 ",'Y',2,3,  // Calamity Restoration
   //"63","74 ",'Y',2,3,  // PI New/Renewal
   //"64","74 ",'Y',2,3,  // PI Lien Update
   //"70","74 ",'Y',2,3,  // Lien Date Update
   "80","75 ",'Y',2,3,  // P19 Denial Intergenerational
   "81","75 ",'Y',2,3,  // P19 Denial Base Year Transfer
   "82","75 ",'Y',2,3,  // P19 55 BYT Out of County
   "83","75 ",'Y',2,3,  // P19 55 BYT Within County
   "84","75 ",'Y',2,3,  // P19 Disabled Out of County
   "85","75 ",'Y',2,3,  // P19 Disabled Within County
   "86","75 ",'Y',2,3,  // P19 Calamity Out of County
   "87","75 ",'Y',2,3,  // P19 Calamity Within County
   "88","75 ",'Y',2,3,  // P19 Intergenerational Rural Partial
   "89","75 ",'Y',2,3,  // P19 Intergenerational Rural 100%
   "90","75 ",'Y',2,3,  // P19 Intergenerational Partial
   "91","75 ",'Y',2,3,  // P19 Intergenerational 100%
   "92","75 ",'Y',2,3,  // P19 Correction
   "93","74 ",'Y',2,3,  // Prop 193-Non Reappraisal
   "GD","1  ",'N',2,3,  // Grant Deed
   "GX","13 ",'Y',2,3,  // Deed
   "TX","13 ",'Y',2,3,  // Deed
   "ZD","74 ",'Y',2,3,  // Disability BYV transfer  inter-county
   "ZE","74 ",'Y',2,3,  // Disability BYV transfer  intra-county
   "ZG","75 ",'Y',2,3,  // Grandparent/Grandchild Intergenerational
   "ZH","74 ",'Y',2,3,  // Base year transfer  inter-county
   "ZN","74 ",'Y',2,3,  // Disaster/Wildfire BYV transfer intra-county
   "ZP","75 ",'Y',2,3,  // Parent/child Intergenerational
   "ZV","74 ",'Y',2,3,  // Disaster/Wildfire BYV transfer inter-county
   "ZW","74 ",'Y',2,3,  // Base year transfer  intra-county
   "","",0,0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 STA_Exemption[] = 
{
   "E01", "H", 3,1,
   "E09", "V", 3,1,
   "E10", "D", 3,1,
   "E11", "D", 3,1,
   "E12", "D", 3,1, 
   "E13", "V", 3,1,
   "E20", "R", 3,1,
   "E21", "R", 3,1,
   "E22", "R", 3,1,
   "E23", "R", 3,1,
   "E24", "R", 3,1,
   "E25", "C", 3,1,
   "E26", "C", 3,1,
   "E27", "C", 3,1,
   "E28", "C", 3,1,
   "E30", "W", 3,1,
   "E31", "W", 3,1,
   "E32", "W", 3,1,
   "E33", "W", 3,1,
   "E34", "W", 3,1,
   "E35", "W", 3,1,
   "E36", "W", 3,1,
   "E37", "W", 3,1,
   "E38", "W", 3,1,
   "E39", "W", 3,1,
   "E40", "M", 3,1,
   "E41", "M", 3,1,
   "E42", "M", 3,1,
   "E43", "M", 3,1,
   "E44", "M", 3,1,
   "E45", "X", 3,1,     // LOW VALUE PROPERTY
   "E46", "X", 3,1,     // LOW VALUE PERSONAL PROPERTY
   "E50", "L", 3,1,
   "E51", "L", 3,1,
   "E55", "I", 3,1, 
   "E60", "X", 3,1,     // LESSORS - UNSECURED (LEASED CHURCH)
   "E61", "X", 3,1,     // LIMITED PARTNERSHIP
   "E62", "X", 3,1,     // REHABILITATION SUPPLEMENT
   "E63", "X", 3,1,     // HOUSING SUPPLEMENT
   "E64", "X", 3,1,     // LOW INCOME SUPPLEMENT
   "E70", "S", 3,1,
   "E71", "S", 3,1,
   "E80", "E", 3,1,
   "E81", "E", 3,1,
   "E82", "E", 3,1,
   "E83", "E", 3,1,
   "E84", "E", 3,1,
   "E85", "E", 3,1,
   "E90", "U", 3,1,
   "E91", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E92", "U", 3,1,     // PRIVATELY OWNED COLLEGES-UNSECURED
   "E93", "X", 3,1,     // ESCAPE HOX W/25% PEN & INT
   "E98", "X", 3,1,     // OTHER
   "","",0,0
};

int   MB_MergeExe(char *pOutbuf);
int   MergeGrGrFile1(char *pCnty);
bool  sqlConnect(LPCSTR strDb, hlAdo *phDb);
int   execSqlCmd(LPCSTR strCmd);
int   execSqlCmd(LPCSTR strCmd, hlAdo *phDb);
int   MB_CreateSale();
int   MB_ExtrTR601(LPCSTR pCnty, LPCSTR pLDRFile=NULL, int iChkLastChar=0);
int   MB_ExtrTC601(LPCSTR pCnty, LPCSTR pLDRFile, int iChkLastChar=0, bool bSortInput=true);

int   MB_MergeSale(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag, bool bUpdtXfer=true);
int   MB_ExtrProp8Ldr(char *pCnty, char *pLDRFile=NULL);

#endif