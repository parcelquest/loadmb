#if !defined(AFX_UPDATE_H__948A2705_9E68_11D2_A0BF_00105A27C570__INCLUDED_)
#define AFX_UPDATE_H__948A2705_9E68_11D2_A0BF_00105A27C570__INCLUDED_

#ifndef IRCSIZ_APN
#define  IRCSIZ_APN        20
#define  UPDATE_ROLL_REC   0x31
#define  DELETE_ROLL_REC   0x32
#define  INSERT_ROLL_REC   0x34

typedef struct _tApnListRec
{
	char  Apn[IRCSIZ_APN];
   char  bUpdDel;          // 1=update, 2=delete, 3=new
   char  filler;
   char  CrLf[2];
} APNLIST;
#endif

extern HANDLE updOpen(char *pCnty);
extern void updClose();
extern BOOL updUpdate(char *pOrgRec, char *pUpdRec, int iLen);
extern BOOL updWrite(char *pUpdRec, int iLen);
extern int updFileCmp(char *pFile1, char *pFile2, char *pFileUpd, char *pApnList, char *pNewApn, int iRecordLen, int iApnLen=12);
extern int updCmpS01R01(char *pCnty, char *pRawTmpl, int iRecordLen=1900, int iApnLen=14);
extern int updCmpX01G01(char *pCnty, char *pRawTmpl, int iRecordLen=1934, int iApnLen=14);
extern int chkS01R01(char *pCnty, char *pRawTmpl, int iRecordLen=1900, int iApnLen=14, bool bCreateUpdate=false, bool bCreateApnFile=false);
extern int updFileRemove(char *pCnty, char *pRawTmpl);

#endif