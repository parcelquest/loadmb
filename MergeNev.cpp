/***************************************************************************
 *
 * 06/22/2017 17.16.0   Copy from MergeEdx.cpp and modify for NEV
 * 08/29/2018 18.4.1    Add -Xv option to generate value import (similar to BUT)
 * 09/05/2018 18.5.0    Change logic to generate value import similar to TUO.
 * 09/13/2018 18.5.2    Increase DUPOUT buffer in Nev_ConvStdChar() to fix sort error.
 * 09/15/2018 18.5.3    Modify Nev_MergeMAdr(() to correct known misspelling city name.
 *                      Fine tune Nev_MergeSitus() to fix UNIT and pickup Zipcode from mailing.
 *                      Modify Nev_MergeStdChar() to overwrite data only if there is new value.
 * 10/21/2018 18.6.2    Fix bug in Nev_MergeSitus() that overwrites buffer of SIZ_M_CTY_ST_D
 * 02/14/2019 18.10.2   Modify Nev_MergeSitus() to adapt to new change to situs file.
 * 07/31/2019 19.1.1    Fix QualityClass in Nev_ConvStdChar().  Fix bug in Nev_MergeSitus(char *).
 *                      Modify Nev_MergeStdChar() to update LotSqft only if roll file doesn't have it.
 *                      Create Nev_MergeLien3() and modify Nev_Load_LDR() to load new 2019 lien roll.
 *                      Auto update PREV_APN after loading LDR.
 * 08/06/2019 19.1.3    Modify Nev_MergeLien3() to format DocNum as YYYYR999999 to match with Merge_Roll().
 * 12/11/2019 19.6.2    Modify Nev_ConvStdChar() since county changes its QUALITYCLASS format (EDX & SJX have similar format).
 * 11/01/2020 20.4.2    Modify Nev_MergeRoll() to populate default PQZoning.
 * 02/10/2021 20.6.2    Modify Nev_MergeSitus() to fix known data problem due to street type "CLOS".  
 *                      It should be part of street name.
 * 06/15/2021 20.10.1   Modify Nev_MergeSitus() to use situs community before checking for mail addr.
 * 07/16/2021 21.0.9    Modify Nev_MergeStdChar() to populate QualityClass.
 * 08/03/2021 21.1.5    Fix PatioSqft in Nev_MergeStdChar().
 * 02/05/2024 23.6.0    Modify Nev_MergeSitus() & Nev_MergeMAdr() to populate UnitNox.
 * 07/23/2024 24.0.3    Modify Nev_MergeLien3() to add ExeType.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "CharRec.h"
#include "PQ.h"
#include "Situs.h"
#include "Tax.h"
#include "doZip.h"
#include "MB_Value.h"
#include "LoadValue.h"
#include "MergeNev.h"

extern long lLotSqftCount;

/**************************** Nev_ConvStdChar ********************************
 *
 * No PoolSpa, FirePlace
 * Copy from MergeHum.cpp to convert new char file.
 * 
 *****************************************************************************/

int Nev_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   double   dTmp;
   STDCHAR  myCharRec;

   LogMsg0("\nConverting char file %s", pInfile);

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_char.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort char file %s to %s", pInfile, acTmpFile);
   iRet = sortFile(pInfile, acTmpFile, "S(#1,C,A)");
   if (iRet < 500)
   {
      LogMsg("***** Input file is too small.");
      return 1;
   }

   if (!(fdIn = fopen(acTmpFile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Start loop
   while (!feof(fdIn))
   {
      // Get next record
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec || acBuf[0] > '9')
         break;

      replNull(acBuf);
      iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < NEV_CHAR_HASWELL)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[NEV_CHAR_ASMT], strlen(apTokens[NEV_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[NEV_CHAR_FEEPARCEL], strlen(apTokens[NEV_CHAR_FEEPARCEL]));

      // Format APN
      if (*apTokens[NEV_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[NEV_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[NEV_CHAR_FEEPARCEL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[NEV_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[NEV_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - prepare for future - currently not avail 20180618
      iTmp = blankRem(apTokens[NEV_CHAR_POOLSPA]);
      if (iTmp > 1)
      {
         pRec = findXlatCode(apTokens[NEV_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass 
      vmemcpy(myCharRec.QualityClass, _strupr(apTokens[NEV_CHAR_QUALITYCLASS]), SIZ_CHAR_QCLS);
      if (*apTokens[NEV_CHAR_QUALITYCLASS] > '0' && *apTokens[NEV_CHAR_QUALITYCLASS] <= 'Z')
      {
         acCode[0] = ' ';
         strcpy(acTmp, apTokens[NEV_CHAR_QUALITYCLASS]);
         myTrim(acTmp);
         if (isalpha(acTmp[0])) 
         {
            if (isdigit(acTmp[1]))
            {
               myCharRec.BldgClass = acTmp[0];
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            } else if (strlen(acTmp) < 3)
            {
               myCharRec.BldgClass = acTmp[0];
            } else
            {
               if (acTmp[0] == 'M' && acTmp[1] == 'H')
                  myCharRec.BldgClass = acTmp[0];
               else
                  myCharRec.BldgClass = acTmp[1];
               if (isdigit(acTmp[2]))
                  iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
               else
                  LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[NEV_CHAR_QUALITYCLASS], apTokens[NEV_CHAR_ASMT]);
            }
         } else if (isdigit(acTmp[0]))
         {
            iRet = Quality2Code((char *)&acTmp[0], acCode, NULL);
         } else if (strlen(acTmp) > 2)
            LogMsg("*** 2. Please check QUALITYCLASS: '%s' in [%s]", apTokens[NEV_CHAR_QUALITYCLASS], apTokens[NEV_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (*apTokens[NEV_CHAR_QUALITYCLASS] > ' ' && *apTokens[NEV_CHAR_QUALITYCLASS] != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[NEV_CHAR_QUALITYCLASS], apTokens[NEV_CHAR_ASMT]);

      // YrBlt
      int iYrBlt = atoi(apTokens[NEV_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[NEV_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[NEV_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[NEV_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // Stories/NumFloors
      dTmp = atof(apTokens[NEV_CHAR_STORIESCNT]);
      if (dTmp > 0.0 && dTmp < 99.9)
      {
         iRet = sprintf(acTmp, "%.1f", dTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[NEV_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[NEV_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport Sqft
      int iCarport = atoi(apTokens[NEV_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Patio SF
      iTmp = atoi(apTokens[NEV_CHAR_PATIOSF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "008040038000", 9))
      //   iRet = 0;
#endif

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[NEV_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[NEV_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[NEV_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[NEV_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[NEV_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[NEV_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[NEV_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[NEV_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[NEV_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace - 01, 02, 05, 06, 99
      if (*apTokens[NEV_CHAR_FIREPLACE] >= '0' && *apTokens[NEV_CHAR_FIREPLACE] <= '9')
      {
         pRec = findXlatCode(apTokens[NEV_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      } 

      // Haswell - not avail
      blankRem(apTokens[NEV_CHAR_HASWELL]);
      if (*(apTokens[NEV_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Lot Sqft
      if (iFldCnt >= NEV_CHAR_LOTSQFT)
      {
         ULONG    lSqft;
         lSqft = (ULONG)atol(apTokens[NEV_CHAR_LOTSQFT]);
         if (lSqft > 1)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(lSqft*SQFT_MF_1000);
            iTmp = sprintf(acTmp, "%u", (long)(dTmp+0.1));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum, UnitSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B3000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/******************************** Nev_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 ******************************************************************************/

void Nev_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf=NULL, char *pDba=NULL)
{
   int   iTmp, iRet;
   char  acTmp1[128], acTmp[128], *pTmp;
   char  acName1[128], acOwner[128];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, true, true);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "011101017000", 9))
   //   iTmp = 0;
#endif

   // CareOf
   if (pCareOf && *pCareOf > ' ')
      iTmp = updateCareOf(pOutbuf, pCareOf, SIZ_CARE_OF);

   // DBA
   if (pDba && *pDba > ' ')
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

   // Remove multiple spaces
   strcpy(acName1, pNames);
   iTmp = blankRem(acName1);
   
   // Check for year that goes before TRUST
   iTmp =0;
   while (acName1[iTmp])
   {
      if (isdigit(acName1[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
      memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);
      return;
   }

   // Update vesting
   updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Save owner name
   strncpy(acOwner, acName1, SIZ_NAME1);
   acOwner[SIZ_NAME1] = 0;
   strcpy(acTmp, acName1);

   // Filter out words
   if ((pTmp=strstr(acTmp, " ETAL")) || 
       (pTmp=strstr(acTmp, " ET AL")) || 
       (pTmp=strstr(acTmp, " & FBO")) ||
       (pTmp=strstr(acTmp, " TRUSTEE")) ||
       (pTmp=strstr(acTmp, " SUC CO TR")) ||
       (pTmp=strstr(acTmp, " SUC TR")) )
      *pTmp = 0;

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(acTmp1, myOwner.acName1);
      if (myOwner.acVest[0] > ' ' && *(pOutbuf+OFF_VEST) == ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
         iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
      else
         iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
      iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);

   iTmp = strlen(acOwner);
   if (acOwner[iTmp-1] == '&')
      iTmp -= 1;
   memcpy(pOutbuf+OFF_NAME1, acOwner, iTmp);
}

/******************************** Nev_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Nev_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4, bool bAdrOnly=false)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *pCareOf, *pDba, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64], acDba[128];
   int   iTmp;

#ifdef _DEBUG
   // (!memcmp(pOutbuf, "001011009000", 9))
   //   iTmp = 0;
#endif

   // Initialize
   if (!bAdrOnly)
      removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   pCareOf = pDba = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         pCareOf = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         pCareOf = pLine2;
         p1 = pLine3;
         if (!_memicmp(pLine1, "DBA ", 4) )
            pDba = pLine1+4;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         p1 = pLine3;
         sprintf(acDba, "%s %s", pLine1+4, pLine2);
         pDba = &acDba[0];
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         pCareOf = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         pDba = pLine1+4;
         p1 = pLine2;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
   }

   // Update DBA
   if (pDba && !bAdrOnly)
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

   // Check for C/O
   if (pCareOf && !bAdrOnly)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ')
         {
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
            vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
         }
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   strcpy(acAddr2, p2);
   quoteRem(acAddr2);
   remChar(acAddr2, ',');
   iTmp = blankRem(acAddr2);
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D, iTmp);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}

void Nev_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], acCity[32];
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, false, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004034005000", 9))
   //   iTmp = 0;
#endif

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   quoteRem(acAddr1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
      vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
   }

   // City/St - Zip
   strcpy(acCity, apTokens[MB_ROLL_M_CITY]);
   if (acCity[0] > ' ')
   {
      quoteRem(acCity);
      remChar(acCity, ',');
      blankRem(acCity);

      // Correct known error
      if (!memcmp(acCity, "PENN VALL", 9))
         strcpy(acCity, "PENN VALLEY");
      else if (!memcmp(acCity, "GRASS VALL", 10))
         strcpy(acCity, "GRASS VALLEY");
      else if (!memcmp(acCity, "SANTA CLAR", 10))
         strcpy(acCity, "SANTA CLARA");
      else if (!memcmp(acCity, "NEVADA CIT", 10))
         strcpy(acCity, "NEVADA CITY");
      else
      {
         if (strstr(acCity, "SAN JUA "))
            replStr(acCity, "SAN JUA ", "SAN JUAN");
      }
      vmemcpy(pOutbuf+OFF_M_CITY, acCity, SIZ_M_CITY);

      if (!memcmp(apTokens[MB_ROLL_M_ST], "A.", 2))
         memcpy(apTokens[MB_ROLL_M_ST], "CA", 2);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      int iZip, iZip4=0;
      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {

         iZip = atoin(apTokens[MB_ROLL_M_ZIP], 5);
         iTmp = strlen(apTokens[MB_ROLL_M_ZIP]);
         if (iTmp == 9)
            iZip4 = atoin(apTokens[MB_ROLL_M_ZIP]+5, 4);

         sprintf(acTmp, "%.5d", iZip);
         memcpy(pOutbuf+OFF_M_ZIP, acTmp, 5);
         if (iZip4 > 0)
         {
            sprintf(acTmp, "%.4d", iZip4);
            memcpy(pOutbuf+OFF_M_ZIP4, acTmp, 4);
         }
      }

      if (iZip > 0)
      {
         if (iZip4 > 0)
            iTmp = sprintf(acTmp, "%s %s %.5d-%.4d", acCity, apTokens[MB_ROLL_M_ST], iZip, iZip4);
         else
            iTmp = sprintf(acTmp, "%s %s %.5d", acCity, apTokens[MB_ROLL_M_ST], iZip);
      } else
         iTmp = sprintf(acTmp, "%s %s", acCity, apTokens[MB_ROLL_M_ST]);

      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
   }
}

/******************************** Nev_MergeSitus *****************************
 *
 * Street name includes direction and suffix.  Currently DIR & SUFFIX are not populated 9/14/2018
 * Street name now doesn't include suffix 2/13/2019
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Nev_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acAddr1[256], *pTmp;
   long     lStrNum;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do
      {
         pRec = fgets(acRec, 512, fdSitus);
      } while (!isdigit(acRec[1]));
   }

   pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001281003000", 9))
   //   iTmp = 0;
#endif

   // Parse situs input
   replNull(acRec);
   iTmp = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

   if (iTmp < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old Situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lStrNum = atol(myTrim(apTokens[MB_SITUS_STRNUM]));
   if (lStrNum > 0)
   {
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      iTmp = sprintf(acAddr1, "%d ", lStrNum);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));
   }

   // Direction
   if (*apTokens[MB_SITUS_STRDIR] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], SIZ_S_DIR);
      strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
      strcat(acAddr1, " ");
   }

   char acStrName[64], acUnit[16];
   ADR_REC  sSitusAdr;

   memset((char *)&sSitusAdr, 0, sizeof(ADR_REC));
   strcpy(acStrName, apTokens[MB_SITUS_STRNAME]);
   iTmp = strlen(acAddr1);
   if (iTmp > 0 && !memcmp(acAddr1, acStrName, iTmp))
   {
      strcpy(acAddr1, acStrName);
      strcpy(acStrName, &acStrName[iTmp]);
   } else
      strcat(acAddr1, acStrName);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001321074000", iApnLen))
   //   iTmp = 0;
#endif

   if (acStrName[0] != '*')
   {
      acUnit[0] = 0;
      if (*apTokens[MB_SITUS_STRTYPE] > ' ')
      {
         char sSfxStr[16];

         vmemcpy(pOutbuf+OFF_S_STREET, acStrName, SIZ_S_STREET);
         // Check for known issue
         if (!memcmp(apTokens[MB_SITUS_STRTYPE], "CLOS", 4))
         {
            strcpy(acTmp, " CLOSE");
            strcat(acStrName, acTmp);
            strcat(acAddr1, acTmp);
            *apTokens[MB_SITUS_STRTYPE] = 0;
         } else
         {
            iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], sSfxStr);    // Return 0 if not found
            if (iTmp > 0)
            {
               sprintf(acTmp, "%d", iTmp);
               vmemcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
               strcat(acAddr1, " ");
               strcat(acAddr1, sSfxStr);
            } else
               LogMsg("*** Unknown suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         }
      } else
      {
         // Parse StrName for suffix  & direction
         parseAdr1N_1(&sSitusAdr, acStrName);

         vmemcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, SIZ_S_DIR);
         vmemcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, SIZ_S_STREET);
         vmemcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, SIZ_S_SUFF);
      }

      if (sSitusAdr.Unit[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
         vmemcpy(pOutbuf+OFF_S_UNITNOX, sSitusAdr.UnitNox, SIZ_S_UNITNOX);
      } else if (acUnit[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_S_UNITNO, acUnit, SIZ_S_UNITNO);
         vmemcpy(pOutbuf+OFF_S_UNITNOX, acUnit, SIZ_S_UNITNOX);
         strcat(acAddr1, " ");
         strcat(acAddr1, acUnit);
      } else if (*myBTrim(apTokens[MB_SITUS_UNIT]) > ' ')
      {
         //_strupr(apTokens[MB_SITUS_UNIT]);
         if (!_memicmp(apTokens[MB_SITUS_UNIT], "UNIT ", 5))
         {
            if (*(apTokens[MB_SITUS_UNIT]+5) == '#')
               strcpy(acTmp, apTokens[MB_SITUS_UNIT]+5);
            else
               sprintf(acTmp, "#%s", apTokens[MB_SITUS_UNIT]+5);
         } else if (!_memicmp(apTokens[MB_SITUS_UNIT], "UNITS ", 6) || !_memicmp(apTokens[MB_SITUS_UNIT], "SUITE ", 6))
            sprintf(acTmp, "#%s", apTokens[MB_SITUS_UNIT]+6);
         else if (!_memicmp(apTokens[MB_SITUS_UNIT], "STE ", 4))
            sprintf(acTmp, "#%s", apTokens[MB_SITUS_UNIT]+4);
         else if (strlen(apTokens[MB_SITUS_UNIT]) < 4 && *(apTokens[MB_SITUS_UNIT]) != '-')
            strcpy(acTmp, apTokens[MB_SITUS_UNIT]);
         else
            acTmp[0] = 0;
         if (acTmp[0] > ' ')
         {
            vmemcpy(pOutbuf+OFF_S_UNITNO, acTmp, SIZ_S_UNITNO);
            vmemcpy(pOutbuf+OFF_S_UNITNOX, acTmp, SIZ_S_UNITNOX);
         }
      }
   }
   iTmp = blankRem(acAddr1, SIZ_S_ADDR_D);
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D, iTmp);

   // Situs city
   char acCity[32], acCode[16];

   acCode[0] = 0;
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      strcpy(acTmp, apTokens[MB_SITUS_COMMUNITY]);
      Abbr2Code(acTmp, acCode, acCity);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);

         // hardcode known zipcode
         if (!memcmp(acTmp, "TR", 2))
            memcpy(pOutbuf+OFF_S_ZIP, "96161", 5);
         else if (*apTokens[MB_SITUS_ZIP] == '9')
            vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], SIZ_S_ZIP);

         myTrim(acCity);
         iTmp = sprintf(acTmp, "%s CA %.5s", acCity, pOutbuf+OFF_S_ZIP);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      }
   }
   // If situs and mailing are the same, use mail city and zip
   if (!acCode[0] && 
       *(pOutbuf+OFF_M_STREET) > '0' && 
       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, 7) &&
       !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_M_STRNUM) &&
       memcmp(apTokens[MB_SITUS_COMMUNITY], "TR", 2) )
   {
      memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      blankRem(acCity, SIZ_M_CITY);
      City2Code(acCity, acCode, pOutbuf);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, pOutbuf+OFF_M_CTY_ST_D, SIZ_M_CTY_ST_D);
      }
   } 

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

// Just save to make change 02/14/2019
int Nev_MergeSitus_20190214(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do
      {
         pRec = fgets(acRec, 512, fdSitus);
      } while (!isdigit(acRec[1]));
   }

   pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001200022", 9))
   //   iTmp = 0;
#endif

   // Parse situs input
   replNull(acRec);
   iTmp = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

   if (iTmp < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old Situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(myTrim(apTokens[MB_SITUS_STRNUM]));
   if (lTmp > 0)
   {
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));
   }

   char acStrName[64];
   ADR_REC  sSitusAdr;

   strcpy(acStrName, apTokens[MB_SITUS_STRNAME]);
   iTmp = strlen(acAddr1);
   if (iTmp > 0 && !memcmp(acAddr1, acStrName, iTmp))
   {
      strcpy(acAddr1, acStrName);
      strcpy(acStrName, &acStrName[iTmp]);
   } else
      strcat(acAddr1, acStrName);

   if (acStrName[0] != '*')
   {
      // Parse StrName for suffix  & direction
      parseAdr1N_1(&sSitusAdr, acStrName);

      vmemcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, SIZ_S_DIR);
      vmemcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, SIZ_S_STREET);
      vmemcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, SIZ_S_SUFF);
      if (sSitusAdr.Unit[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
      else if (*myBTrim(apTokens[MB_SITUS_UNIT]) > ' ')
      {
         if (!_memicmp(apTokens[MB_SITUS_UNIT], "UNIT ", 5))
            sprintf(acTmp, "#%s", apTokens[MB_SITUS_UNIT]+5);
         else if (!_memicmp(apTokens[MB_SITUS_UNIT], "UNITS ", 6) || !_memicmp(apTokens[MB_SITUS_UNIT], "SUITE ", 6))
            sprintf(acTmp, "#%s", apTokens[MB_SITUS_UNIT]+6);
         else if (!_memicmp(apTokens[MB_SITUS_UNIT], "STE ", 4))
            sprintf(acTmp, "#%s", apTokens[MB_SITUS_UNIT]+4);
         else if (*apTokens[MB_SITUS_UNIT] == '#')
            strcpy(acTmp, apTokens[MB_SITUS_UNIT]);
         else if (pTmp = strstr(apTokens[MB_SITUS_UNIT]+2, "UNIT"))
         {
            *pTmp = 0;
            strcpy(acTmp, apTokens[MB_SITUS_UNIT]);
         } else
            acTmp[0] = 0;
         if (acTmp[0] > ' ')
         {
            vmemcpy(pOutbuf+OFF_S_UNITNO, acTmp, SIZ_S_UNITNO);
            vmemcpy(pOutbuf+OFF_S_UNITNOX, acTmp, SIZ_S_UNITNOX);
         }
      }
   }
   iTmp = blankRem(acAddr1, SIZ_S_ADDR_D);
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D, iTmp);

   // Situs city
   char acCity[32], acCode[16];

   // If situs and mailing are the same, use mail city and zip
   if (!memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, 7) &&
       !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_M_STRNUM) &&
       memcmp(apTokens[MB_SITUS_COMMUNITY], "TR", 2) )
   {
      memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      blankRem(acCity, SIZ_M_CITY);
      City2Code(acCity, acCode, pOutbuf);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, pOutbuf+OFF_M_CTY_ST_D, SIZ_M_CTY_ST_D);
      }
   } else if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      strcpy(acTmp, apTokens[MB_SITUS_COMMUNITY]);
      Abbr2Code(acTmp, acCode, acCity);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);

         // hardcode known zipcode
         if (!memcmp(acTmp, "TR", 2))
            memcpy(pOutbuf+OFF_S_ZIP, "96161", 5);
         else if (*apTokens[MB_SITUS_ZIP] == '9')
            vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], SIZ_S_ZIP);

         myTrim(acCity);
         iTmp = sprintf(acTmp, "%s CA %.5s", acCity, pOutbuf+OFF_S_ZIP);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      }
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int Nev_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004130013", 9))
   //   acTmp[0] = 0;
#endif
   // 
   strcpy(acAddr1, pLine1);
   int iTmp = blankRem(acAddr1);
   if (iTmp < 5)
      return 1;

   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

#ifdef _DEBUG
   // Check for '-'
   //char *pTmp;
   //if (pTmp = isCharIncluded(acAddr1, '-', 0))
   //   pTmp++;
#endif

   // 2830 G ST #STE D-1
   // 3980 CEDAR APTS 1-4 ST
   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1_4(&sSitusAdr, acAddr1);

   memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));

   memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
   memcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, sSitusAdr.UnitNox, SIZ_S_UNITNOX);
   }

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/****************************** Nev_MergeStdChar *****************************
 *
 * Merge Nev_Char.dat in STDCHAR format
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the one that has value.
 *
 *****************************************************************************/

int Nev_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lSqft;
   int      iLoop, iBeds, iFBath, iHBath, iBldgNum, iRooms, iUnits;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   while (!iLoop)
   {
      // Quality Class
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
      *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
      memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

      // YrBlt
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

      // YrEff
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

      // BldgSqft
      lSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      } 
      //else
      //   memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

      // Garage Sqft
      lSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
      //} else
      //{
      //   memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);
      //   *(pOutbuf+OFF_PARK_TYPE) = ' ';
      }

      // LotSqft - Use Acres from roll file instead
      lSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10 && *(pOutbuf+OFF_LOT_SQFT+7) == ' ')
      {
         sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lSqft);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
         
         lSqft = atoin(pChar->LotAcre, SIZ_CHAR_SQFT);
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lSqft);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      } 

      // PatioSqft
      lSqft = atoin(pChar->PatioSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_PATIO_SF, lSqft);
         memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
      } 
      //else
      //   memset(pOutbuf+OFF_PATIO_SF, ' ', SIZ_BLDG_SF);

      // Heating
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   
      // Cooling 
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

      // Total Rooms
      iRooms = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
      if (iRooms > 0)
      {
         sprintf(acTmp, "%*d", SIZ_ROOMS, iRooms);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      } 
      //else
      //   memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

      // Beds
      iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      } 
      //else
      //   memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

      // Bath
      iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      } 
      //else
      //   memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

      // Half bath
      iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
      if (iHBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      } 
      //else
      //   memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

      // Fireplace
      if (pChar->Fireplace[0] > ' ')
         *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

      // HasSeptic or HasSewer
      if (pChar->HasSewer > ' ')
         *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

      // HasWell
      if (pChar->HasWater > ' ')
         *(pOutbuf+OFF_WATER) = pChar->HasWater;

      // Pools
      if (pChar->Pool[0] > ' ')
         *(pOutbuf+OFF_POOL) = pChar->Pool[0];

      // Units count
      iUnits = atoin(pChar->Units, SIZ_CHAR_UNITS);
      if (iUnits > 0)
      {
         sprintf(acTmp, "%*d", SIZ_UNITS, iUnits);
         memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
      } 
      //else
      //   memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

      // Stories
      if (pChar->Stories[0] > ' ')
         memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
      //else
      //   memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

      // BldgSeqNum
      iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);

      lCharMatch++;

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (!pRec)
         break;
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (!iLoop && (iBeds > 0 && iBldgNum > 0))
         break;
   }

   return 0;
}

/********************************* Nev_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Nev_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse input
   iTmp = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad input record for APN=%s (tokens=%d)", apTokens[iApnFld], iTmp);
      return -1;
   }

   // Ignore APN starts with 800-999 except 910 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910 ))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Copy PREV_APN
      memcpy(pOutbuf+OFF_PREV_APN, pOutbuf, iApnLen);

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "29NEV", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   _strupr(apTokens[MB_ROLL_LEGAL]);
   if (pTmp = strchr(apTokens[MB_ROLL_LEGAL], 0x14))
      *pTmp = '/';
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   memset(pOutbuf+OFF_USE_CO, ' ', SIZ_USE_CO);
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   if (acTmp[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, strlen(acTmp), pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      lLotSqftCount++;

      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
      {
         if (*(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
         {
            memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
            memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
            sprintf(acTmp, "%.5s%.6d", apTokens[MB_ROLL_DOCNUM], atol(apTokens[MB_ROLL_DOCNUM]+5));
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, 11);
         } 
      }
   }

   // Owner
   try {
      Nev_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER], apTokens[MB_ROLL_CAREOF], apTokens[MB_ROLL_DBA]);
   } catch(...) {
      LogMsg("***** Exeception occured in Nev_MergeOwner()");
   }

   // Mailing
   try {
      Nev_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Nev_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************* Nev_Load_Roll ******************************
 *
 * Handle Nev_Roll.csv file and the like
 *
 ******************************************************************************/

int Nev_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Sort roll file
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A)");
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"Z\") DEL(124) ");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   fdTax = fopen(acTmpFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
      replNull(acRollRec);
      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Nev_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Nev_MergeSitus(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Nev_MergeStdChar(acBuf);


            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acBuf);        // Nev_Tax.csv

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Nev_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Nev_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Nev_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      replNull(acRollRec);
      iRet = Nev_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Nev_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Nev_MergeStdChar(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTax(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   //if (fdSale)
   //   fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u\n", lRecCnt);
   return 0;
}

/********************************* Nev_MergeLien *****************************
 *
 * For 2019 LDR AGENCYCDCURRSEC_TR601.TAB
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Nev_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_ISAGPRESERVE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // APN
   vmemcpy(pOutbuf, apTokens[L3_ASMT], iApnLen);

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Copy ALT_APN
   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], SIZ_ALT_APN);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "29NEV", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&NEV_Exemption);

   // Legal
   remChar(apTokens[L3_PARCELDESCRIPTION], '"');
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   remChar(apTokens[L3_OWNER], '"');
   Nev_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   //Nev_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   remChar(apTokens[L3_MAILADDRESS1], '"');
   remChar(apTokens[L3_MAILADDRESS2], '"');
   remChar(apTokens[L3_MAILADDRESS3], '"');
   Nev_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc
   if (*apTokens[L3_CURRENTDOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp && isNumber(apTokens[L3_CURRENTDOCNUM]+5))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         iTmp = sprintf(acTmp, "%.5s%.6d", apTokens[L3_CURRENTDOCNUM], atol(apTokens[L3_CURRENTDOCNUM]+5));
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, iTmp);
      }
   } 

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   return 0;
}

/******************************** Nev_Load_LDR ******************************
 *
 * Load LDR 2019
 *
 ****************************************************************************/

int Nev_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdLDR;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdLDR = fopen(acRollFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(1,13,C,A) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      // Create new R01 record
      iRet = Nev_MergeLien3(acBuf, acRec);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Nev_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Nev_MergeStdChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/*************************** Nev_CleanupHistSale ******************************
 *
 * Clean up sale file.  remove all record with bad DocNum or DocDate
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Nev_CleanupHistSale(char *pInfile)
{
   char     acInbuf[1024], acOutFile[_MAX_PATH], *pRec, sTmp1[32];
   long     lCnt=0, lOut=0, iTmp;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];

   LogMsg0("Fix sale history file %s", pInfile);
   if (_access(pInfile, 0))
   {
      LogMsg("***** Nev_CleanupHistSale(): Missing input file: %s", pInfile);
      return -1;
   }

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0282630300", 9))
      //   iTmp = 0;
#endif
      memcpy(sTmp1, &pInRec->DocNum, 12);
      myTrim(sTmp1, 12);
      if (sTmp1[4] == 'R')
      {
         if (!strpbrk(sTmp1, "-+*`./"))
         {
            if (!strpbrk(sTmp1, "DAT"))
            {
               iTmp = atoi(&sTmp1[5]);
               sprintf(sTmp1, "%.5d", iTmp);
               memcpy(&pInRec->DocNum[5], sTmp1, 5);
               fputs(acInbuf, fdOut);
               lOut++;
            } else
               LogMsg("Remove 2. %s", sTmp1);
         } else
            LogMsg("Remove 1. %s", sTmp1);
      } else if (strlen(sTmp1) == 9 && isdigit(sTmp1[4]) && !memcmp(sTmp1, pInRec->DocDate, 4))
      {
         pInRec->DocNum[4] = 'R';
         memcpy(&pInRec->DocNum[5], &sTmp1[4], 5);
         fputs(acInbuf, fdOut);
         lOut++;
      } else if (strlen(sTmp1) == 11 && sTmp1[5] == 'R')
      {
         iTmp = sprintf(sTmp1, "%.4s%.6s  ", pInRec->DocDate, &pInRec->DocNum[5]);
         memcpy(pInRec->DocNum, sTmp1, iTmp);
         fputs(acInbuf, fdOut);
         lOut++;
      } else
         LogMsg("Remove %.12s", pInRec->DocNum);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if (lOut != lCnt)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".org");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Rename output file
      iTmp = rename(acOutFile, pInfile);
   } else
      LogMsg("Good sale file, no record drop!");

   LogMsg("Total input records:     %u\n", lCnt);
   LogMsg("    records cleaned:     %u", lOut);

   return iTmp;
}

 /*******************************************************************************
 *
 * Test APN 006132028000, 001211014000
 *
 ********************************************************************************/

int Nev_ConvDocNum(char *pDocNum, char *pDocDate)
{
   int   iTmp1, iTmp2, iRet;
   char sTmp[32], *pTmp;

   memcpy(sTmp, pDocNum, 12);
   sTmp[12] = 0;

   if (*(pDocNum+2) == '-')
   {
      iTmp1 = sprintf(sTmp, "%.4sR%.6s ", pDocDate, pDocNum+3);
      memcpy(pDocNum, sTmp, iTmp1);
      iRet = 0;
   } else if (pTmp = strchr(sTmp, '/'))
   {
      *pTmp = 0;
      iTmp1 = atoi(sTmp);
      iTmp2 = atoi(pTmp+1);
      if (iTmp1 > 0 && iTmp2 > 0)
      {
         sprintf(sTmp, "%d/%d          ", iTmp1, iTmp2);
         memcpy(pDocNum, sTmp, 12);
         iRet = 0;
      } else
         iRet = -1;
   } else
      iRet = -1;

   return iRet;
}

int Nev_ConvertApnSale(char *pInfile, char *pOutfile)
{
   FILE *fdIn, *fdOut;
   int   iCnt=0;
   char  acBuf[2048], acApn[32], *pBuf;
   SCSAL_REC *pSale = (SCSAL_REC *)&acBuf[0];

   LogMsg("Convert Cum Sale file");
   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");

   while (!feof(fdIn))
   {
      pBuf = fgets(acBuf, 2048, fdIn);
      if (!pBuf)
         break;

      // Ignore test records
      //if (pSale->DocNum[0] == ' ' || pSale->DocNum[4] == ' ' || !memcmp(pSale->DocNum, "000", 3))
      if (pSale->DocNum[0] == ' ')
      {
         LogMsg("... Drop: %.132s", acBuf);
         continue;
      }

      if (strlen(acBuf) > 512)
      {
         LogMsg("Bad sale rec: %s", acBuf);
         continue;
      }

      // Format new APN
      sprintf(acApn, "0%.5s0%.5s", acBuf, &acBuf[5]);
      memcpy(pSale->OtherApn, acBuf, 12);
      memcpy(acBuf, acApn, 12);

      // Convert DocNum
      if (!Nev_ConvDocNum(pSale->DocNum, pSale->DocDate))
         fputs(acBuf, fdOut);
      else
      {
         iCnt--;
         LogMsg(">>> Bad DocNum: %.132s", acBuf);
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert Sale APN completed with %d records", iCnt);

   return 0;
}

int Nev_ConvertApn(char *pInfile, char *pOutfile)
{
   FILE *fdIn, *fdOut;
   int   iCnt=0;
   char  acBuf[2048], acApn[32], *pBuf;

   LogMsg("Convert Cum Sale file");
   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");

   while (!feof(fdIn))
   {
      pBuf = fgets(acBuf, 2048, fdIn);
      if (!pBuf)
         break;

      // Format new APN
      sprintf(acApn, "0%.5s0%.5s", acBuf, &acBuf[5]);
      memcpy(acBuf, acApn, 12);
      fputs(acBuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert APN completed with %d records", iCnt);

   return 0;
}

/*************************** Nev_ConvertApnRoll *****************************
 *
 * Convert old APN format to MB.  Keep old APN in PREV_APN.
 * 0101004000 (01-010-04-000) = 001010004000
 *
 ****************************************************************************/

int Nev_ConvertApnRoll(char *pInfile, char *pOutfile, int iRecordLen)
{
   FILE *fdIn, *fdOut;
   int   iCnt=0;
   char  acBuf[2048], acTmp[32], acApn[32];

   LogMsg("Open input file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "rb")))
   {
      LogMsg("***** Error opening %s", pInfile);
      return -1;
   }

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "wb");

   fread(acBuf, 1, iRecordLen, fdIn);
   fwrite(acBuf, 1, iRecordLen, fdOut);

   while (!feof(fdIn))
   {
      fread(acBuf, 1, iRecordLen, fdIn);

      // Format new APN
      sprintf(acApn, "0%.5s0%.5s", acBuf, &acBuf[5]);

      // Save old APN to previous APN
      memcpy(&acBuf[OFF_PREV_APN], &acBuf[0], 12);

      sprintf(acTmp, "0%.2s-%.3s-0%.2s-%.3s", acBuf, &acBuf[2], &acBuf[5], &acBuf[7]);
      memcpy(&acBuf[OFF_APN_D], acTmp, 15);

      memcpy(acBuf, acApn, 12);
      fwrite(acBuf, 1, iRecordLen, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert Roll APN completed with %d records", iCnt);

   return 0;
}

/***************************** Nev_CreateSCSale *****************************
 *
 *
 ****************************************************************************/

int Nev_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], acSaleRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC *pSale = (SCSAL_REC *)&acSaleRec[0];

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp;

   LogMsg("\nCreating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      // Remove null char
      iTmp = replNull(acRec);
      if (iTmp < 42)
         iTmp = 0;

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Reset output record
      memset(acSaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(pSale->Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

      // Docnum
      if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R')
      {
         sprintf(acTmp, "%.5s%.6d", apTokens[MB_SALES_DOCNUM], atol(apTokens[MB_SALES_DOCNUM]+5));
         memcpy(pSale->DocNum, acTmp, 11);
      } else if (*(apTokens[MB_SALES_DOCNUM]+4) == 'I')
         continue;
      else
         memcpy(pSale->DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));

      // Doc date
      pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      if (pTmp)
         memcpy(pSale->DocDate, acTmp, 8);

      // Group sale
      if (*apTokens[MB_SALES_GROUPSALE] == '1')
      {
         pSale->SaleCode[0] = 'P';
         pSale->MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            vmemcpy(pSale->PrimaryApn, apTokens[MB_SALES_GROUPASMT], iApnLen);
      }

#ifdef _DEBUG
      //if (!memcmp(pSale->Apn, "027071031000", 9))
      //   lPrice = 0;
#endif

      // Sale price
      lPrice = atol(apTokens[MB_SALES_PRICE]);

      // Tax
      dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
      if (acTmp[0] > '0')
      {
         dTmp = atof(acTmp);
         lTmp = (long)(dTmp * SALE_FACTOR);

         // Check for bad DocTax
         if (dTmp > 100000)
         {
            iTmp = ((int)dTmp/100)*100;
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", pSale->Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               LogMsg("??? Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f.  Need investigation.  Ignore price.", pSale->Apn, pSale->DocNum, dTmp);
               lPrice = 0;
            }
         } else
         {
            //lPrice = lTmp;
            iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
            memcpy(pSale->StampAmt, acTmp, iTmp);
         }

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
               LogMsg("*** Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d, \tTax=%.2f, \tDOCCODE=%s, DOCTYPE=%.3s", 
                  pSale->Apn, pSale->DocNum, pSale->DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE], pSale->DocType);
         }  

         // Ignore sale price if less than 1000
         if (lPrice >= 10000)
            sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
         else if (lPrice >= 1000)
            sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
         else
            memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
         memcpy(pSale->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
      }

      // Doc code 
      iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
      if (iTmp >= 0)
      {
         if (pDocTbl[iTmp].pCode[0] > '0')
         {
            memcpy(pSale->DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
            if (lPrice <= 100)
               pSale->NoneSale_Flg = pDocTbl[iTmp].flag;
         } else if (lPrice > 100)
            pSale->DocType[0] = '1';
         else
            pSale->NoneSale_Flg = 'Y';            
      } else if (lPrice > 100)
         pSale->DocType[0] = '1';
      else
      {
         pSale->NoneSale_Flg = 'Y';            
         memcpy(pSale->DocType, "74", 2);    // Misc.
      }

      // Save original DocCode
      vmemcpy(pSale->DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);

#ifdef _DEBUG
      //if (!memcmp(pSale->Apn, "001160016000", 9))
      //   iTmp = 0;
#endif

      // Full/Partial
      if (!memcmp(pSale->DocType, "57", 2))
         pSale->SaleCode[0] = 'P';

      // Transfer Type
      if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!_memicmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               pSale->SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      // Only output record with DocDate
      if (pSale->DocDate[0] > ' ')
      {
         // Seller
         strcpy(acTmp, apTokens[MB_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         vmemcpy(pSale->Seller1, acTmp, SALE_SIZ_SELLER);

         // Buyer
         strcpy(acTmp, apTokens[MB_SALES_BUYER]);
         iTmp = blankRem(acTmp);
         vmemcpy(pSale->Name1, acTmp, SALE_SIZ_BUYER);

         pSale->CRLF[0] = 10;
         pSale->CRLF[1] = 0;
         fputs(acSaleRec, fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc, Prev APN
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,481,1,C,D) F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -1;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            DeleteFile(acCSalFile);
   
            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -1;
      } else
         iTmp = rename(acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
         DeleteFile(acCSalFile);
      iTmp = rename(acOutFile, acCSalFile);
   }

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                       output:    %d.", lTmp);
   return iTmp;
}

/*********************************** loadNev ********************************
 *
 * Options:
 *    -CNEV -L -Xs -Xl (load lien)
 *    -CNEV -U -Xs[i] [-Z] [-T] (load update)
 *
 ****************************************************************************/

int loadNev(int iSkip)
{
   int   iRet=0;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;
   iApnLen = myCounty.iApnLen;
   lLotSqftCount = 0;

   // Loading Tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      int iTaxGrp = GetPrivateProfileInt(myCounty.acCntyCode, "TaxGroup", 2, acIniFile);

      // Load Nev_Tax.txt
      iRet = MB_Load_TaxBase(bTaxImport, true, iTaxGrp, iHdrRows);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet = MB_Load_TaxCodeMstr(bTaxImport, 0);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport, 0);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport, iHdrRows);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
      if (!iLoadFlag)
         return iRet;
   }

   // Load Value file - Copy from MergeBut.cpp
   if (iLoadFlag & EXTR_VALUE)                     // -Xv
   {
      char sDbName[64], sBYVFile[_MAX_PATH], sTmp[_MAX_PATH], sCVFile[_MAX_PATH];

#ifdef _DEBUG
      // Combine value record
      //sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Vmp");
      //iRet = MB_CombineValue(myCounty.acCntyCode, acValueFile, acTmpFile, 3);
#endif

      // Like TUO
      sprintf(sBYVFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Value");
      sprintf(sTmp, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = MB_ExtrValues(myCounty.acCntyCode, acValueFile, acExeFile, sTmp, sBYVFile, iHdrRows);
      
      // Reset reason and VST
      if (iRet > 0)
      {
         sprintf(sTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Vmp");
         iRet = MB_ResetPVReason(sBYVFile, sTmp);
      }
      if (iRet > 0)
      {
         sprintf(sDbName, "LDR%d", lLienYear);
         GetIniString("Data", "SqlValueFile", "", sTmp, _MAX_PATH, acIniFile);
         sprintf(sCVFile, sTmp, sDbName, myCounty.acCntyCode);
         lRecCnt = createValueImport(sBYVFile, sCVFile, false);
         if (lRecCnt > 0)
         {
            // Save output file for import
            strcpy(acValueFile, sCVFile);
            iRet = 0;
         } else
            iLoadFlag ^= EXTR_IVAL;      // Turn off import
      }
   }

   // Create/Update cum sale file from Nev_Sales.csv
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // NEV sale file can have different date format, use all default settings
      // MB1_CreateSCSale() is similar to MB_CreateSCSale() except it handles
      // file w/o ConfirmedSalesPrice (Nev_Sales.csv)
      // Nev_Sales.csv
      iRet = Nev_CreateSCSale(MM_DD_YYYY_1,0,0,true,(IDX_TBL5 *)&NEV_DocCode[0]);    

      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract lien file - LDR2016
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acValueFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acValueFile, 0);      // 2016 
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode);
   }

   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Load Char file
      if (!_access(acCharFile, 0))
      {
         iRet = Nev_ConvStdChar(acCharFile);
         if (iRet <= 0)
            LogMsg("*** WARNING: Error converting Char file %s.  Use existing data.", acCharFile);
      } else
      {
         LogMsg("*** WARNING: CHAR file does not exist: %s: ", acCharFile);
         LogMsg("    -Xa option is ignore.  Please verify input file");
      }
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Create Lien file
      LogMsg0("Load %s LDR file", myCounty.acCntyCode);
      iRet = Nev_Load_LDR(iSkip);     
      //if (!iRet)
      //{
      //   iRet = GetPrivateProfileInt(myCounty.acCntyCode, "PrevApnLen", iApnLen, acIniFile);
      //   iRet = updatePrevApn(myCounty.acCntyCode, iRet, iSkip);
      //}
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Nev_Load_Roll(iSkip);
      LogMsg("Total LotSqft populated: %d", lLotSqftCount);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Nev_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   if (bUpdPrevApn)                                // -Up
   {
      // If not defined, use current apn length
      iRet = GetPrivateProfileInt(myCounty.acCntyCode, "PrevApnLen", iApnLen, acIniFile);
      iRet = updatePrevApn(myCounty.acCntyCode, iRet, iSkip);
   }

   return iRet;
}
