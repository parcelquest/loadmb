#include "stdafx.h"
#include "Logs.h"
#include "XceedZipAPI.h"
#include "doZip.h"

// License string found in the LICENSE.TXT file
#define XCD_LICENSE_A   "SFX50-YZZ3C-E4WJP-Y4CA"
//#define XCD_LICENSE_A   ""
#define XCD_LICENSE_W   L""

bool     m_bPreserve;
bool     m_bSubfolder;
bool     m_bReplace;
bool     m_bZipLoaded;
CString  m_sFilesToExclude;
CString  m_sFilesToProcess;
CString  m_sUnzipFolder;
CString  m_sZipFilename;

HMODULE  hXceedZipDll;

/********************************************************************************/

void setUnzipToFolder(char *pStr)
{
   m_sUnzipFolder = pStr;
}

/********************************************************************************/

void setReplaceIfExist(bool bVal)
{
   m_bReplace = bVal;
}

/********************************************************************************/

void setPreservePath(bool bVal)
{
   m_bPreserve = bVal;
}

/********************************************************************************/

void setProcessSubfolder(bool bVal)
{
   m_bSubfolder = bVal;
}

/********************************************************************************/

void doZipInit()
{
   if (m_bZipLoaded == true)
      return;

   m_bZipLoaded = true;
   m_bPreserve = false;
   m_bSubfolder = false;
   m_bReplace = true;
   m_sFilesToExclude = _T("");
   m_sFilesToProcess = _T("");
   m_sUnzipFolder = _T("");
   m_sZipFilename = _T("");

   // Initialize the Xceed Zip Compression Library for Dll API access
   hXceedZipDll = LoadLibrary( "XceedZip.dll" );

   LPFNXCEEDZIPINITDLL lpfnInitDll = ( LPFNXCEEDZIPINITDLL ) GetProcAddress( hXceedZipDll, "XceedZipInitDLL" );

   if (lpfnInitDll)
   {
      lpfnInitDll();
   }

   //#error YOU MUST PROVIDE YOUR LICENSE KEY IN ORDER TO RUN THIS SAMPLE.
   //m_xZip.License( "SFX50-YZZ3C-E4WJP-Y4CA" );
   //m_xZip.SetBackgroundProcessing( TRUE );
}

/********************************************************************************/

void doZipShutdown()
{
   if (m_bZipLoaded == true)
   {
      LPFNXCEEDZIPSHUTDOWNDLL lpfnShutdown  = ( LPFNXCEEDZIPSHUTDOWNDLL ) GetProcAddress( hXceedZipDll, "XceedZipShutdownDLL" );

      if( lpfnShutdown )
      {
         lpfnShutdown();
      }

      FreeLibrary( hXceedZipDll );
      m_bZipLoaded = false;
   }
}

/********************************************************************************/

// Here, we'll use UNICODE functions only, and use a callback function
// for event notifications. This way, we will not mix up Ansi and Unicode
// support in event handling.

void CALLBACK MyCallback( WPARAM wXceedMessage, LPARAM lParam )
{
   //static HWND               hList   = NULL;
   static XceedZipFunctions* pFuncs  = NULL;

   switch( wXceedMessage )
   {
      case 0xffff:
         // It's a trick to set hList!
         //hList = ( HWND ) lParam;
         break;

      case 0xfffe:
         // And another trick to set pFuncs!
         pFuncs  = ( XceedZipFunctions* ) lParam;
         break;

      // A file is evaluated for unzipping
      case XM_UNZIPPREPROCESSINGFILE:
      {
         // For this particular event, we decided to create our handle using 
         // the wide version of XzCreateXceedZip (see DoUnzipExample)
         // So we typecast the lParam to the wide version of the structure
         xcdUnzipPreprocessingFileParamsW* pParams = ( xcdUnzipPreprocessingFileParamsW * ) lParam;
         // Here, you could change some information about this file, or reject
         // the file based on your own custom filtering conditions.

         // For this example, we are going to append a ".Unzipped" to all files!
         //wcscat( pParams->szDestFilename, L".Unzipped" );

         break;
      }

      // A file is skipped for some reason
      case XM_SKIPPINGFILE:
      {
         xcdSkippingFileParamsA*  pParams = ( xcdSkippingFileParamsA * ) lParam;

         // Display the skipped filename. We make sure to use Ansi strings to
         // work on Win95/98
         char  szMsg[300];
         CString sTmp;

         sTmp = pParams->szFilename;
         if (sTmp.Right(1) != "\\")
         {
            LogMsg("Skipping file %S", pParams->szFilename );

            // Get and display the reason. You can see that each structure contains
            // the handle to the instance that triggered the event (hZip).
            szMsg[ 0 ]  = ' ';
            // Even though we created this handle with the wide version, we can
            // call Ansi versions of other API's.
            if( pFuncs )
            {
               // As explained in "DoUnzipExample" below, we can call ansi functions
               // on a "wide-created" instance.
               pFuncs->lpfnXzGetErrorDescriptionA( pParams->hZip, xvtSkippingReason, pParams->xReason, szMsg+1, 299 );
               LogMsg("Reason: %s", szMsg);
            }
         }
         break;
      }

      // The FileStatus event is the triggered event along the actual
      // processing. That's why it's the best place for status messages.
      case XM_FILESTATUS:
      {
         xcdFileStatusParamsA*  pParams = ( xcdFileStatusParamsA * ) lParam;

         // When starting to process a file, we display it's name
         if ( pParams->lBytesProcessed == 0 && pFuncs )
         {
            char  szMsg[ 300 ] = { 0 };

            // Starting to process this file
            switch( pFuncs->lpfnXzGetCurrentOperation( pParams->hZip ) )
            {
               case xcoUnzipping:
               {
                  LogMsg("Unzipping --> %s", pParams->szFilename );
                  break;
               }
            }
         }

         // We could insert here code to update a file status progress bar.

         break;
      }
      // The GlobalStatus event gives the global progression
      case XM_GLOBALSTATUS:
      {
         xcdGlobalStatusParams*  pParam  = ( xcdGlobalStatusParams * ) lParam;
         break;
      }

      // A recoverable inconsistency was encountered
      case XM_WARNING:
      {
         xcdWarningParamsA* pParams = ( xcdWarningParamsA * ) lParam;

         // Display the affected file and the message. We can call the Ansi
         // function, even if the handle was created with the Wide creator.
         char  szWarning[ 200 ]  = { 0 };

         if( pFuncs )
         {
            pFuncs->lpfnXzGetErrorDescriptionA( pParams->hZip, xvtWarning, pParams->xWarning, szWarning, 200 );
         }

         LogMsg("*** Warning: %s (%s)", szWarning, pParams->szFilename);

         break;
      }
      // A file is about to be replaced
      case XM_REPLACINGFILE:
      {
         xcdReplacingFileParamsA* pParams = ( xcdReplacingFileParamsA * ) lParam;

         pParams->bReplaceFile = m_bReplace;
         break;
      }
   }
}

/********************************************************************************
 *
 *  Notes: to zip multiple files, separate input file names with '|' in pFilesToProcess.
 *
 *  Return 0 if process started successully 
 *
 ********************************************************************************/

int startZip(char *pZipFile, char *pFilesToProcess)
{
   int   iRet;

   // The GetProcAddress can do much more than get function pointers.
   // It would be better to call it GetExportedSymbolAddress. q;-)
   XceedZipFunctions * pFuncs  = ( XceedZipFunctions * ) GetProcAddress( hXceedZipDll, "g_xzFunctions" );

   if (pFuncs)
   {
      MyCallback( 0xfffe, ( LPARAM ) pFuncs );

      // Create an instance for this operation. Throughout this sample, we create
      // instances on demand. We could also create the required instance(s) at the
      // beginning, and free them at the end.
      HXCEEDZIP hZip  = pFuncs->lpfnXzCreateXceedZipA( XCD_LICENSE_A );

      if ( hZip )
      {
         pFuncs->lpfnXzSetXceedZipCallback( hZip, MyCallback );
         pFuncs->lpfnXzSetPreservePaths(hZip, m_bPreserve);
         pFuncs->lpfnXzSetProcessSubfolders(hZip, m_bSubfolder);
         pFuncs->lpfnXzSetZipFilenameA( hZip, pZipFile );
         pFuncs->lpfnXzSetFilesToProcessA(hZip, pFilesToProcess);

         iRet = pFuncs->lpfnXzZip( hZip );

         // You can also use the GetErrorDescription method through the
         // XzGetErrorDescription function, to get a default error message.
         char szMsg[ 200 ];
         if (iRet)
         {
            pFuncs->lpfnXzGetErrorDescriptionA( hZip, xvtError, iRet, szMsg, 200 );
            LogMsg("***** Zip error (%d): %s", iRet, szMsg);
            iRet = -3;
         } else
            LogMsg("Zipping successfully");

         // For each XzCreateXceedZip, there must be a XzDestroyXceedZip call
         pFuncs->lpfnXzDestroyXceedZip( hZip );
      } else
      {
         LogMsg("***** Error creating zip file.  Possible invalid license");
         iRet = -1;
      }

      // Clean-up our callback's static variables.
      MyCallback( 0xfffe, 0 );
   } else
   {
      LogMsg("***** Error creating zip file.  Please check for existence of XceedZip.dll");
      iRet = -2;
   }

   return iRet;
}

/********************************************************************************
 *
 *  Return 0 if process started successully
 *
 ********************************************************************************/

int startUnzip(char *pZipFile, char *pFilesToProcess)
{
   int nErr=0;

   // Get a pointer to the functions structure
   XceedZipFunctions * pFuncs  = ( XceedZipFunctions * ) GetProcAddress( hXceedZipDll, "g_xzFunctions" );

   if( pFuncs )
   {
      // We send dummy messages to our callback to set it's static variables!
      //MyCallback( 0xffff, ( LPARAM ) hList );
      MyCallback( 0xfffe, ( LPARAM ) pFuncs );

      // Create an instance for this operation. Why not work in UNICODE?
      // The MyCallback function will have to expect UNICODE strings in its params.
      HXCEEDZIP hZip  = pFuncs->lpfnXzCreateXceedZipA( XCD_LICENSE_A );

      if ( hZip )
      {
         pFuncs->lpfnXzSetXceedZipCallback( hZip, MyCallback );
         pFuncs->lpfnXzSetPreservePaths(hZip, m_bPreserve);
         if (pFilesToProcess && *pFilesToProcess)
            pFuncs->lpfnXzSetFilesToProcessA(hZip, pFilesToProcess);
         pFuncs->lpfnXzSetZipFilenameA( hZip, pZipFile);  
         pFuncs->lpfnXzSetUnzipToFolderA( hZip, m_sUnzipFolder); // Wide version

         nErr  = pFuncs->lpfnXzUnzip( hZip );
  
         // Windows 95 does not implement UNICODE APIs, so we must use SendMessageA
         // (SendMessage is mapped to SendMessageA when UNICODE isn't define)
         // Why not call XzGetErrorDescriptionA. It's not because we created the
         // instance with the wide version that we are stuck calling wide functions.
         // The only impact of creating the instance with the wide or ansi version
         // is the format of the string parameters in events.
         char  szMsg[ 200 ];
         pFuncs->lpfnXzGetErrorDescriptionA( hZip, xvtError, nErr, szMsg, 200 );
         LogMsg(szMsg);
         pFuncs->lpfnXzDestroyXceedZip( hZip );
      }

      // Clean-up our callback's static variables.
      //MyCallback( 0xffff, 0 );
      MyCallback( 0xfffe, 0 );
   }

   // Bypass skipped file error
   if (nErr == xerFilesSkipped)
   {
      LogMsg("WARNING: Some files may be skipped.  Please verify ...");
      nErr = 0;
   }

   return nErr;
}

/********************************************************************************
 *
 *  Return number of items placed on the list
 *
 ********************************************************************************/

int getZipFileContents(char *pZipFile, FILE_ITEM *pFileList, int iMaxCnt)
{
   int   iRet, iTmp=0;
   BOOL  bRet=false;
   FILE_ITEM *pList = pFileList;

   xcdListingFileParamsA xItemInfo;
   HXCEEDZIPITEMS        hItems;     
   XceedZipFunctions     *pFuncs = (XceedZipFunctions *) GetProcAddress(hXceedZipDll, "g_xzFunctions");
   
   if (pFuncs)
   {
      MyCallback( 0xfffe, ( LPARAM ) pFuncs );

      // Create an instance for this operation. Why not work in UNICODE?
      // The MyCallback function will have to expect UNICODE strings in its params.
      HXCEEDZIP hZip  = pFuncs->lpfnXzCreateXceedZipA( XCD_LICENSE_A );

      if ( hZip )
      {
         pFuncs->lpfnXzSetXceedZipCallback(hZip, MyCallback);
         pFuncs->lpfnXzSetZipFilenameA(hZip, pZipFile);  

         iRet = pFuncs->lpfnXzGetZipContents(hZip, &hItems);
         if (!iRet)
         {
            bRet = pFuncs->lpfnXziGetFirstItemA(hItems, &xItemInfo);
            while (bRet && iTmp < iMaxCnt)
            {
               strcpy(pList->szName, xItemInfo.szFilename);
               pList->lSize = xItemInfo.lSize;
               pList->stLastModified = xItemInfo.stLastModified;
               pList++;
               iTmp++;

               bRet = pFuncs->lpfnXziGetNextItemA(hItems, &xItemInfo);
            }
            pFuncs->lpfnXziDestroyXceedZipItems(hItems);
         }

         pFuncs->lpfnXzDestroyXceedZip(hZip);
      }
   }

   return iTmp;
}

/********************************* unzipAll *********************************
 *
 * If successful, return 0.  Otherwise error.
 *
 ****************************************************************************/

int unzipAll(LPCSTR pZipPath, LPSTR pUnzipFolder, bool bOverWrite)
{
   char     *pTmp, acZipFile[_MAX_PATH], acZipPath[_MAX_PATH];
   struct   _finddata_t  sFileInfo;
   int      iCnt, iRet;
   long     lHandle;

   sprintf(acZipPath, pZipPath);

   // Open Input file
   iRet = -1;
   lHandle = _findfirst(acZipPath, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acZipPath, '\\');
      if (pTmp)
      {
         *pTmp = 0;
         iRet = 0;
      }
   } else
   {
      LogMsg("*** No new file avail for processing: %s", pZipPath);
      return -1;
   }

   iCnt = 0;
   // Initialize Zip server
   doZipInit();

   // Set unzip folder
   setUnzipToFolder(pUnzipFolder);
   setReplaceIfExist(bOverWrite);

   while (!iRet)
   {
      // unzip file
      sprintf(acZipFile, "%s\\%s", acZipPath, sFileInfo.name);
      LogMsgD("Unzip %s", acZipFile);

      // Unzip
      iRet = startUnzip(acZipFile);
      if (iRet)
      {
         LogMsg("***** Error unzipping %s.  Program terminated", acZipFile);
         iCnt = -2;
         break;
      }

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
      iCnt++;
   }

   // Close handle
   _findclose(lHandle);

   // Unload Zip DLL
   doZipShutdown();

   if (iCnt > 0)
   {
      LogMsgD("Number of zip files processed: %u", iCnt);
      iRet = 0;
   }

   return iRet;
}

/********************************* unzipOne *********************************
 *
 * Unzip one file
 * If successful, return 0.  Otherwise error.
 *
 ****************************************************************************/

int unzipOne(LPCSTR pZipFile, LPSTR pUnzipFolder, bool bOverWrite)
{
   char        acZipFile[_MAX_PATH];
   FILE_ITEM   asFileList[256];
   int      iRet, iCnt;

   doZipInit();

   // Set unzip folder
   setUnzipToFolder(pUnzipFolder);
   setReplaceIfExist(bOverWrite);

   // unzip file
   strcpy(acZipFile, pZipFile);
   LogMsg("Unzip input file %s", acZipFile);

   // get file name
   iCnt = getZipFileContents(acZipFile, &asFileList[0], 256);

   // Unzip
   if (iCnt > 0)
   {
      try
      {
         iRet = startUnzip(acZipFile);
      } catch (...)
      {
         LogMsg("Exception in unzipOne().");
         iRet = 1;
      }
      if (iRet)
         LogMsg("***** Error unzipping %s.  Program terminated", acZipFile);
      else
         LogMsg("Number of files unzipped: %d\n", iCnt);
   } else
   {
      LogMsg("***** Error opening zip file.  Please check input file %s", acZipFile);
      iRet = -1;
   }

   // Unload Zip DLL
   doZipShutdown();

   return iRet;
}

// Rename input file by adding current date to end of file
//int unzipOne(LPSTR pZipFile, LPSTR pUnzipFolder, bool bRename, bool bOverWrite)
//{
//   char        acZipFile[_MAX_PATH], *pTmp;
//   FILE_ITEM   asFileList[256];
//   int      iRet, iCnt;
//
//   doZipInit();
//
//   // Set unzip folder
//   setUnzipToFolder(pUnzipFolder);
//   setReplaceIfExist(bOverWrite);
//
//   // unzip file
//   strcpy(acZipFile, pZipFile);
//   LogMsg("Unzip file %s", acZipFile);
//
//   // get file name
//   iCnt = getZipFileContents(acZipFile, &asFileList[0], 256);
//
//   // Unzip
//   if (iCnt > 0)
//   {
//      try
//      {
//         iRet = startUnzip(acZipFile);
//      } catch (...)
//      {
//         LogMsg("Exception in unzipOne().");
//         iRet = 1;
//      }
//      if (iRet)
//         LogMsg("***** Error unzipping %s.  Program terminated", acZipFile);
//      else if (bRename)
//      {
//         pTmp = strrchr(acZipFile, '.');
//         *pTmp = 0;
//         strcpy(pZipFile, acZipFile);
//      }
//   } else
//      iRet = -1;
//
//   // Unload Zip DLL
//   doZipShutdown();
//
//   return iRet;
//}

/********************************* unzipOne *********************************
 *
 * Special version that allows user to rename input zip file with specific extension
 *
 * If successful, return 0.  Otherwise error.
 *
 ****************************************************************************/

int unzipOne(LPSTR pZipFile, LPSTR pUnzipFolder, LPSTR pExtension, bool bOverWrite)
{
   char        acZipFile[_MAX_PATH];
   FILE_ITEM   asFileList[256];
   int         iRet, iCnt;

   doZipInit();

   // Set unzip folder
   setUnzipToFolder(pUnzipFolder);
   setReplaceIfExist(bOverWrite);

   // unzip file
   strcpy(acZipFile, pZipFile);
   LogMsg("Unzip file %s", acZipFile);

   // get file name
   iCnt = getZipFileContents(acZipFile, &asFileList[0], 256);

   // Unzip
   if (iCnt > 0)
   {
      try
      {
         iRet = startUnzip(acZipFile);
      } catch (...)
      {
         LogMsg("Exception in unzipOne().");
         iRet = 1;
      }
      if (iRet)
         LogMsg("***** Error unzipping %s.  Program terminated", acZipFile);
      else if (pExtension && *pExtension > ' ')
      {
         sprintf(acZipFile, "%s.%s", pZipFile, pExtension);
         iRet = rename(pZipFile, acZipFile);
      }
   } else
      iRet = -1;

   // Unload Zip DLL
   doZipShutdown();

   return iRet;
}

