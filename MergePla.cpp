/**************************************************************************
 *
 * Options:
 *    -CPLA -L -Xl -Xa -Ms -Mr -Dr -Mz [-O]  (load lien)
 *    -CPLA -U -Xs -Xa [-Mz] [-Mr] [-O]      (load update)
 *    -CPLA -T|-Ut [-O]                      (load tax)
 *
 * Revision
 * 12/23/2005 1.0.0    First version by Tung Bui
 * 04/26/2007 1.9.5    Remove '-' in mail zip
 * 07/16/2007 1.9.6    Change rule on LDR processing.  Now keep all records
 *                     even if there is no lien value.  This could be public parcel.
 * 11/05/2007 1.9.18   If there is FORECLOSURE in mail addr, chop it.  This fixes
 *                     the problem in ParcelQuest which hardcoded record length
 *                     at 700.  If record is longer than 700, you know what happened.
 * 01/26/2008 1.10.3.1 Add code to support standard usecode
 * 03/05/2008 1.10.5.1 Add code to output update records.  
 * 02/26/2009 8.6      Add -Mo option, modify Pla_MergeLien() to populate other values.
 *                     Need to update Pla_MergeCurr() before LDR.
 * 03/30/2009 8.7.1    Use MB_MergeSale() to do sale update instead of MergeSale3() to fix 
 *                     problem that non-sale transaction overwrites sale transaction that  
 *                     occurs in the same day  since we only keep one transaction per day.
 * 05/21/2009 8.7.6    Remove reference to OldApn & NewApn files.  We no longer need them.
 * 07/07/2009 9.1.1    Reverse LDR processing rule, take only records with lien values.
 *                     Value file needs resort.
 * 12/15/2009 9.2.9    Fix pool code table asPool[] translating 'S' to 'C'.
 * 07/06/2010 10.0.1   Add new function Pla_ExtrLien() to extract current value and store
 *                     in LIENEXTR format. Change Pla_MergeCurr() to add other values to R01.
 * 08/20/2010 10.3.2   Overwrite EXE_TOTAL with GROSS if it is greater than GROSS.
 * 10/23/2010 10.3.5   Set FULL_EXEMPT flag based on mail addr.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 05/14/2011 10.8.1   Add -Xs option to create cum sale.
 * 06/09/2011 10.9.1   Exclude HomeSite from OtherVal. Replace Pla_MergeExe() with MB_MergeExe().
 *                     Modify sort command for EXE file so it can be used with MB_MergeExe().
 * 06/30/2011 11.0.0   Modify Pla_Load_LDR() to process LDR using current value format.
 *                     Save original House Number in S_HSENO.
 * 07/01/2011 11.0.1   Remove special handling in Pla_Load_LDR() for 2011 LDR.
 * 08/18/2011 11.3.0   Call MB_CreateSCSale() with DocTypeFmt=2, special case for PLA.
 * 04/24/2012 11.14.2  Remove Pla_MergeOthers().  Add Pla_MergeExe() to replace MB_MergeExe().
 *                     This is done to fix bug which MB_MergeExe() overwrites TOTALEXE value.
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 01/04/2014 13.19.0  Add DocCode[] table. Remove sale/lien update from Pla_Load_Roll() and 
 *                     use ApplyCumSale() to update sale history. Also remove creation of assr file.
 * 01/20/2014 13.20.1  Modify DocCode[] to make FORCLOSURE non-sale event.
 * 07/07/2014 14.0.1   Modify Pla_ConvertChar() and rename to Pla_ConvChar1(). Add Pla_ConvChar2()
 *                     to support new CHAR layout. No more HeatingSrc, CoolingSrc, UnitSeqNo.
 * 07/15/2014 14.0.3   Modify Pla_ConvChar1() & Pla_ConvChar2() to output cumchar file to RawFile folder. 
 *                     Also modify Load_LDR() and Load_Roll() to use cumchar file.
 * 10/26/2014 14.6.0   Fix exception in Pla_ConvChar2(). This bug only occurs in Win2012.
 * 07/03/2015 15.0.3   Fix known bad char in Pla_MergeMAdr().
 * 07/31/2015 15.2.0   Add DBA to Pla_MergeMAdr()
 * 10/28/2015 15.4.3   Modify MergeRoll() to populate TRANSFER only if DOCNUM has the same year with DOCDATE.
 * 11/03/2015 15.5.0   Move all tables to .H file.
 * 12/02/2015 15.5.3   Move tables back to .CPP due to conflict in MergeAssr1.cpp
 * 01/04/2016 15.9.0   Add option to load TC file by calling Load_TC()
 * 02/14/2016 15.12.0  Modify Pla_MergeMAdr() to fix overwrite bug.
 * 02/15/2016 15.12.1  Fix blank M_ZIP in Pla_MergeMAdr()
 * 07/07/2016 16.0.3   Clean up merge situs section in Pla_MergeRoll().
 * 07/08/2016 16.0.3   Remove -La & -Ua option.  No longer support CDASSR.
 * 07/06/2017 17.1.3   Add Pla_ConvStdChar() & Pla_MergeStdChar() to support new CHAR file.
 *                     Modify Pla_MergeRoll() for new ROLL layout. Modify Pla_Load_Roll()
 *                     & Pla_Load_LDR() to use new Pla_MergeStdChar() function.  Add -Xa option.
 * 03/27/2018 17.9.0   Add -Ut option to update tax.
 * 02/28/2020 19.7.0   Modify Pla_MergeMAdr() to fix special case of "ST FRANCIS"
 * 03/12/2020 19.7.2   New CHAR file: Modify Pla_MergeStdChar() & Pla_ConvStdChar() to add new field UNITS.
 * 04/27/2020 19.9.0   Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 06/02/2020 19.9.4   Add Pla_CreateSCSale() and modify Pla_MergeCurr(), Pla_MergeRoll(), Pla_ConvStdChar() due to new layout.
 * 07/16/2020 20.1.5   Modify Pla_MergeLien() due to new layout, rename old version to Pla_MergeLien_2019().
 *                     Modify Pla_Load_LDR() & Pla_CreateLienRec() to use delimiter from INI file instead of hardcode.
 * 05/11/2021 20.8.7   Fix FirePlace in Pla_ConvStdChar().
 * 07/08/2021 21.0.6   Modify Pla_MergeStdChar() to populate QualityClass.
 * 08/10/2021 21.1.8   Modify Pla_MergeRoll() to keep all records from county, not filter out 'I' or 'D' records.
 * 08/26/2021 21.2.0   Default bUseConfSalePrice=false so we don't use confidential sale price.
 * 04/15/2022 21.8.2   Modify Pla_MergeRoll() to change records filter on 'I' & 'D' record per bulk customer request (see email from Travis).
 * 06/07/2023 22.9.3   Modify Pla_Load_Roll() to fix broken CSV file before processing.
 * 07/06/2023 23.0.4   Modify Pla_MergeRoll() to fix overload value of LotSqft.
 * 02/05/2024 23.6.0   Modify Pla_MergeRoll() & Pla_MergeMAdr() to populate UnitNox.
 * 05/13/2024 23.8.2   Remove exception of city starts with "ROS" in Pla_MergeMAdr().
 * 05/15/2024 23.8.3   Modify Pla_MergeMAdr() to populate M_UnitNox even when M_Unit is empty.
 * 09/10/2024 24.1.2   Modify Pla_MergeLien() to add Exetype.  Modify Pla_Load_LDR() & Pla_Load_Roll() 
 *                     to change SORT command on EXE file due to layout changed.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "CharRec.h"
#include "doOwner.h"
#include "doSort.h"
#include "formatApn.h"
#include "MergePla.h"
#include "PQ.h"

#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "Tax.h"

int MB_MergeExe(char *pOutbuf);
int MB_MergeSale(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag, bool bUpdtXfer=true);
int MB_CreateSCSale(int iDateFmt=0, int iDocTypeFmt=0, int iDocNumFmt=0, bool bAppend=false, IDX_TBL5 *pDocTbl=NULL);
int Pla_ConvChar1(char *pInfile);
int Pla_ConvChar2(char *pInfile);

/******************************** Pla_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Pla_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acOwners[64], acName1[64], acName2[64];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001011002", 9))
   //   iTmp = 0;
#endif

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.' || *pTmp == ',')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;
   strcpy(acOwners, acTmp);      // To be used later

   acName2[0] = 0;
   acSave1[0] = 0;

   if (pTmp=strstr(acTmp, " 1/2 INT"))
      *pTmp = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp1 = strlen(acTmp);
      if (iTmp1 > SIZ_NAME1) iTmp1 = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp1);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp1);
      return;
   }

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
     if ( (pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " TR")))
        *pTmp = 0;

      // If TRUST appears after number, save from number forward
      if (strstr((char *)&acTmp[iTmp], " TRUST") || strstr((char *)&acTmp[iTmp], " LIVING") ||
        strstr((char *)&acTmp[iTmp], " REVOCABLE") )
      {
         iTmp--;
         strcpy(acSave1, (char *)&acTmp[iTmp]);
         acTmp[iTmp] = 0;
      }
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
      if ((pTmp=strstr(acName2, " ETAL")) || (pTmp=strstr(acName2, " ET AL")) )
         *pTmp = 0;
   } else if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Filter out words, things in parenthesis
   // MONDANI NELLIE M ESTATE OF

   if ( (pTmp = strchr(acTmp, '('))  || (pTmp=strstr(acTmp, " CO TR")) || (pTmp=strstr(acTmp, " CO-TR")) ||
     (pTmp=strstr(acTmp, " COTR")) ||
     (pTmp=strstr(acTmp, " LIFE ESTATE")) ||
     (pTmp=strstr(acTmp, " TRUSTEE")) ||
     (pTmp=strstr(acTmp, " TR ETAL")) ||
     (pTmp=strstr(acTmp, " TR")) ||
     (pTmp=strstr(acTmp, " TRES")) || (pTmp=strstr(acTmp, " TTEE")) ||
     (pTmp=strstr(acTmp, " ESTATE OF")) ||
     (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTS OF")) ||
     (pTmp=strstr(acTmp, " ETAL"))  || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " JT")) || (pTmp=strstr(acTmp, " CP")) ||
     (pTmp=strstr(acTmp, " LIV")))
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " SUCCS-TR")) || (pTmp=strstr(acTmp, " SUCCESSOR")) ||
      (pTmp=strstr(acTmp, " SURVIVOR")) )
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);

   } else if (pTmp=strstr(acTmp, " FAMILY TRUST &"))
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      // RUMAN RICKY L & RUMAN FAMILY TRUST &
      // RUMAN RICKY L & NANCY FAMILY TRUST & MARIE
      // VERMETTE JAMES & THERESA A & ZENDER ROBERT A &
      strcpy(acSave1, " FAMILY TRUST");
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST &"))
   {  // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      strcpy(acSave1, " LIVING TRUST");
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      strcpy(acSave1, " REV TRUST");
      strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      *(pTmp+6) = 0;
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
      {
         strcpy(acSave1, pTmp1);
         *pTmp1 = 0;
      } else
      {
         strcpy(acSave1, pTmp);
         *pTmp = 0;
      }

      strcpy(acName1, acTmp);

      // Preparing Name2 - matching up last name
      /*
      pTmp1 = pTmp+9;
      iTmp1 = strlen(pTmp1);
      for (iTmp = 0; iTmp < iTmp1; iTmp++)
      {
         if (acName1[iTmp] != *pTmp1)
            break;
         pTmp1++;
      }

      // Skip first word after & if it is the same as last name
      if (iTmp > 2 && *(pTmp1-1) == ' ')
      {
         strcat(acName1, " & ");
         // Search and remove TRUST in second part of name since we already saved
         if (pTmp=strstr(pTmp1, " TRUST"))
            *pTmp = 0;
         strcat(acName1, pTmp1);
      } else
      {
         strcpy(acName2, pTmp+9);
      }
      */
      strcpy(acName2, pTmp+9);

   } else if (pTmp=strstr(acTmp, " LAND TRUST"))
   {
      strcpy(acSave1, " LAND TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " FAMILY "))
             || (pTmp=strstr(acTmp, " INCOME TR")) || (pTmp=strstr(acTmp, " LIVING TR")) )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " ESTATE OF"))
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   if (pTmp = strstr(acSave1, " THE"))
      *pTmp = 0;

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
         strcpy(acName2, pTmp);
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;
   if ((pTmp=strstr(acName2, " AS ")) || (pTmp=strstr(acName2, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Name1
   iTmp = strlen(acOwners);
   if (iTmp > SIZ_NAME1)
      iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acOwners, iTmp);

   // Parse owner for swapped name
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);

      strcpy(acTmp1, myOwner.acName1);
      if (myOwner.acVest[0] > ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // Concat what in saved buffer
      if (acSave1[0])
      {
         if (acTmp1[strlen(acTmp1)-1] == ' ' && acSave1[0] == ' ')
            strcat(acTmp1, (char *)&acSave1[1]);      // avoid double blank
         else
            strcat(acTmp1, acSave1);
      }

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
      {
         iTmp = strlen(acTmp1);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, iTmp);
      } else
      {
         iTmp = strlen(myOwner.acSwapName);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
      }
   } else
   {
      // Couldn't split names
      if (acSave1[0])
      {
         strcat(acName1, acSave1);
         acSave1[0] = 0;
         iTmp = strlen(acName1);
         if (iTmp > SIZ_NAME1)
            iTmp = SIZ_NAME1;
         memcpy(pOutbuf+OFF_NAME_SWAP, acName1, iTmp);
      } else
      {
         iTmp = strlen(pNames);
         if (iTmp > SIZ_NAME1)
            iTmp = SIZ_NAME1;
         memcpy(pOutbuf+OFF_NAME_SWAP, pNames, iTmp);
      }
   }
/* Temporary remove and use Name1 as is
   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(acTmp1, myOwner.acName1);
      if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
      {
         iTmp = strlen(myOwner.acName2);
         if (iTmp > SIZ_NAME2)
            iTmp = SIZ_NAME2;
         memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, iTmp);
      }
      if (myOwner.acVest[0] > ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // Concat what in saved buffer
      //if (acSave[0] && strcmp(acSave, " TRUST"))
      if (acSave1[0])
      {
         if (acTmp1[strlen(acTmp1)-1] == ' ' && acSave1[0] == ' ')
            strcat(acTmp1, (char *)&acSave1[1]);      // avoid double blank
         else
            strcat(acTmp1, acSave1);
      }

      // Save Name1
      iTmp = strlen(acTmp1);
      if (iTmp > SIZ_NAME1)
         iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp1, iTmp);

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
      {
         iTmp = strlen(acTmp1);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, iTmp);
      } else
      {
         iTmp = strlen(myOwner.acSwapName);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
      }
   } else
   {
      if (acSave1[0])
      {
         strcat(acName1, acSave1);
         acSave1[0] = 0;
         iTmp = strlen(acName1);
         if (iTmp > SIZ_NAME1)
            iTmp = SIZ_NAME1;
         memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);
         memcpy(pOutbuf+OFF_NAME_SWAP, acName1, iTmp);
      } else
      {
         // Couldn't split names
         iTmp = strlen(pNames);
         if (iTmp > SIZ_NAME1)
            iTmp = SIZ_NAME1;
         memcpy(pOutbuf+OFF_NAME1, pNames, iTmp);
         memcpy(pOutbuf+OFF_NAME_SWAP, pNames, iTmp);
         acName2[0] = 0;
      }
   }

   // Process Name2 when there is more than one word
   if (acName2[0] && strchr((char *)&acName2[1], ' ') && myOwner.acName2[0] <= ' ')
   {
      if ((pTmp=strstr(acName2, " REVOC")) || (pTmp=strstr(acName2, " FAMILY "))
         || (pTmp=strstr(acName2, " INCOME TR")) || (pTmp=strstr(acName2, " LIVING TR")) )
      {
         iTmp = strlen(acName2);
         if (iTmp > SIZ_NAME2)
            iTmp = SIZ_NAME2;
         memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);
      } else
      {
         acSave1[0] = 0;
         if (pTmp=strstr(acName2, " TRUST"))
         {
            strcpy(acSave1, pTmp);
            *pTmp = 0;
         }

         iRet = splitOwner(acName2, &myOwner, 3);
         if (iRet >= 0)
            strcpy(acName2, myOwner.acName1);

         if (acSave1[0])
            strcat(acName2, acSave1);

         iTmp = strlen(acName2);
         if (iTmp > SIZ_NAME2)
            iTmp = SIZ_NAME2;
         memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);
      }
   }
*/
}

/******************************** Pla_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Pla_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[256], *pTmp;
   int      iTmp;
   CString strIn;
   ADR_REC  sMailAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "069102009000", 9))
   //   iTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   } 

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   _strupr(myTrim(acAddr1));

   // Check for known bad char
   if (pTmp = strchr(acAddr1, 0xCD))
      *pTmp = 'I';

   if (!memcmp(acAddr1, "N/A", 3) ||
       !strcmp(acAddr1, "NA") ||
       !memcmp(apTokens[MB_ROLL_M_CITY], "N/A", 3) ||
       !memcmp(acAddr1, "VALUE ", 6) ||
       !memcmp(apTokens[MB_ROLL_M_CITY], "VALUE ", 6) ||
       !memcmp(acAddr1, "ASSESSED", 8) ||
       !memcmp(acAddr1, "EXEMPT", 6) ||
       !memcmp(acAddr1, "COMMON AREA", 11) 
      )
   {
      // If situs available, use it
      if (*(pOutbuf+OFF_S_STRNUM) > ' ')
      {
         memcpy(pOutbuf+OFF_M_STRNUM, pOutbuf+OFF_S_STRNUM, SIZ_M_STRNUM);
         memcpy(pOutbuf+OFF_M_DIR, pOutbuf+OFF_S_DIR, SIZ_M_DIR);
         memcpy(pOutbuf+OFF_M_STREET, pOutbuf+OFF_S_STREET, SIZ_M_STREET);
         memcpy(pOutbuf+OFF_M_UNITNO, pOutbuf+OFF_S_UNITNO, SIZ_M_UNITNO);
         memcpy(pOutbuf+OFF_M_UNITNOX, pOutbuf+OFF_S_UNITNOX, SIZ_M_UNITNOX);
         if (*apTokens[MB_ROLL_STRTYPE] > ' ')
            memcpy(pOutbuf+OFF_M_SUFF, apTokens[MB_ROLL_STRTYPE], strlen(apTokens[MB_ROLL_STRTYPE]));
         if (*apTokens[MB_ROLL_COMMUNITY] > ' ')
         {
            //if (!memcmp(apTokens[MB_ROLL_COMMUNITY], "ROS", 3))
            //   memcpy(pOutbuf+OFF_M_CITY, "ROSEVILLE", 9);
            //else
               memcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_COMMUNITY], strlen(apTokens[MB_ROLL_COMMUNITY]));
         }
         memcpy(pOutbuf+OFF_M_ST, "CA", 2);
         memcpy(pOutbuf+OFF_M_ZIP, pOutbuf+OFF_S_ZIP, SIZ_M_ZIP);
         memcpy(pOutbuf+OFF_M_ADDR_D, pOutbuf+OFF_S_ADDR_D, SIZ_M_ADDR_D);
         memcpy(pOutbuf+OFF_M_CTY_ST_D, pOutbuf+OFF_S_CTY_ST_D, SIZ_M_CTY_ST_D);

         // Check full exempt
         if (!memcmp(acAddr1, "EXEMPT FROM TAXATION", 15))
            *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   } else 
   {
      // Special case for E ST FRANCIS
      if (strstr(acAddr1, "ST FRANCIS"))
         replStr(acAddr1, "ST FRANCIS", "SAINT FRANCIS");

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "069102025000", 9))
      //   iTmp = 0;
#endif
      // Parse mail address
      iTmp = parseMAdr1_1(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > ' ')
            memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
      }

      if ((iTmp == 1) && (pTmp = strstr(acAddr1, "FORECLOSURE") ) )
         *pTmp = 0;

      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      if (sMailAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));

      iTmp = strlen(sMailAdr.strName);
      if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
      {
         // Skip the first 9 bytes and search for next space after box #
         pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
         if (pTmp)
            *pTmp = 0;

        memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
        sMailAdr.strSfx[SIZ_M_SUFF] = 0;
      } else
      {
         sMailAdr.strName[SIZ_M_STREET] = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      }

      // Unit #
      if (sMailAdr.UnitNox[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
      }

      // City/St - Zip
      if (*apTokens[MB_ROLL_M_CITY] > ' ')
      {
         char  acCity[64];

         // Sometimes ROSEVILLE was key as ROS
         //if (!memcmp(apTokens[MB_ROLL_M_CITY], "ROS", 3))
         //   strcpy(acCity, "ROSEVILLE");
         //else
         {
            strcpy(acCity, apTokens[MB_ROLL_M_CITY]);
            _strupr(myTrim(acCity));
         }

         vmemcpy(pOutbuf+OFF_M_CITY, acCity, SIZ_M_CITY);
         if (2 == strlen(apTokens[MB_ROLL_M_ST]))
            memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

         if (isdigit(*apTokens[MB_ROLL_M_ZIP]))
         {
            strcpy(acTmp, apTokens[MB_ROLL_M_ZIP]);
            iTmp = strlen(acTmp);
            if (acTmp[5] == '-' && iTmp == 10)
            {
               strcpy(&acTmp[5], &acTmp[6]);
               memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
            } else if (iTmp > 9)
               memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);   
            else
               vmemcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);   
         } else
            iTmp = 0;

         if (iTmp == 9)
            sprintf(acTmp, "%s %s %.5s-%.4s", acCity, apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP], apTokens[MB_ROLL_M_ZIP]+5);
         else
            sprintf(acTmp, "%s %s %s", acCity, apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
         iTmp = blankRem(acTmp);
         if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
            acTmp[iTmp-5] = 0;
         vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
      }
   }
}

/******************************** Pla_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

//int Pla_MergeSale(char *pOutbuf)
//{
//   static   char acRec[512], *pRec=NULL;
//   char     acTmp[256], *pTmp;
//   int      iRet=0, iTmp;
//   long     lCurSaleDt, lPrice;
//   double   dTmp;
//   SALE_REC sCurSale;
//
//   // Get rec
//   if (!pRec)
//   {
//      // Skip header record
//      //pRec = fgets(acRec, 512, fdSale);
//      // Get first rec
//      pRec = fgets(acRec, 512, fdSale);
//   }
//
//   do
//   {
//      if (!pRec)
//         return 1;      // EOF
//
//      // Add 1 to Sale rec to skip double quote
//      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
//      if (iTmp > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Sale rec %.*s", iApnLen, pRec+1);
//         pRec = fgets(acRec, 512, fdSale);
//         lSaleSkip++;
//      }
//   } while (iTmp > 0);
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "015180056", 9))
//   //   iRet = 0;
//#endif
//
//   while (!iTmp)
//   {
//      // Parse data
//      if (pRec)
//         iRet = ParseStringNQ(pRec, ',', MB_SALES_XFERTYPE+1, apTokens);
//      if (iRet < MB_SALES_XFERTYPE)
//      {
//         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
//         iRet =  -1;
//         break;
//      }
//
//      // Merge data - Only take sale rec that has both docnum and docdate
//      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
//      {
//         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));
//
//         // Docnum
//         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
//         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);
//
//         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
//         if (pTmp)
//         {
//            // Doc date
//            lCurSaleDt = atol(acTmp);
//            memcpy(sCurSale.acDocDate, acTmp, 8);
//         }
//
//         // Tax
//         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
//         lPrice = 0;
//         if (acTmp[0] > '0')
//         {
//            dTmp = atof(acTmp);
//            lPrice = (long)(dTmp * SALE_FACTOR);
//            if (lPrice < 100)
//               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
//            else
//               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
//            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
//         /* Do not use Confidential Sale Price
//         } else
//         {
//            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
//            if (acTmp[0] > '0')
//            {
//               lPrice = atol(acTmp);
//               if (lPrice < 100)
//                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
//               else
//                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
//               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
//            }
//         */
//         }
//
//         // DocType - need translation before production
//         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
//         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);
//
//         // Transfer Type
//         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
//         {
//            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
//            {
//               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
//               {
//                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
//                  break;
//               }
//               iTmp++;
//            }
//         } else
//            sCurSale.acSaleCode[0] = ' ';
//
//         // Group sale?
//         if (*apTokens[MB_SALES_GROUPSALE] > '0')
//            *(pOutbuf+OFF_MULTI_APN) = 'Y';
//         else
//            *(pOutbuf+OFF_MULTI_APN) = ' ';
//
//         // Seller
//         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
//         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);
//
//         MB_MergeSale(&sCurSale, pOutbuf, true, false);
//         iRet = 0;
//      }
//
//      // Get next sale record
//      pRec = fgets(acRec, 512, fdSale);
//      if (pRec)
//         iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
//      else
//         break;
//   }
//
//   lSaleMatch++;
//
//   // Update flag
//   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
//      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
//   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
//      *(pOutbuf+OFF_AR_CODE2) = 'A';
//   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
//      *(pOutbuf+OFF_AR_CODE3) = 'A';
//
//   return iRet;
//}

/******************************** Pla_MergeChar ******************************
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

//int Pla_MergeChar(char *pOutbuf)
//{
//   static   char acRec[512], *pRec=NULL;
//   char     acTmp[256], acCode[32], *pTmp;
//   long     lTmp, lBldgSqft, lGarSqft;
//   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iFp;
//   MB_CHAR  *pChar;
//
//   iRet=iBeds=iFBath=iHBath=iFp=0;
//   lBldgSqft=lGarSqft=0;
//
//   // Get first Char rec for first call
//   if (!pRec && !lCharMatch)
//      pRec = fgets(acRec, 512, fdChar);
//
//   do
//   {
//      if (!pRec)
//      {
//         fclose(fdChar);
//         fdChar = NULL;
//         return 1;      // EOF
//      }
//
//      // Compare Asmt
//      iLoop = memcmp(pOutbuf, pRec, iApnLen);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
//         pRec = fgets(acRec, 512, fdChar);
//         lCharSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 0;
//
//   pChar = (MB_CHAR *)pRec;
//
//   // Quality Class
//   if (pChar->QualityClass[0] < 'A')
//   {
//      memcpy(acTmp, pChar->QualityClass, MBSIZ_CHAR_QUALITY);
//      acTmp[MBSIZ_CHAR_QUALITY] = 0;
//      pTmp = acTmp;
//      while (*pTmp && *pTmp < 'A') pTmp++;
//      if (*pTmp >= 'A')
//      {
//         // Class
//         *(pOutbuf+OFF_BLDG_CLASS) = *pTmp++;
//
//         // Quality
//         if (strstr(pTmp, "AV"))
//            *(pOutbuf+OFF_BLDG_QUAL) = 'A';
//         else if (strstr(pTmp, "GD") || strstr(pTmp, "GO"))
//            *(pOutbuf+OFF_BLDG_QUAL) = 'G';
//         else if (strstr(pTmp, "EX"))
//            *(pOutbuf+OFF_BLDG_QUAL) = 'E';
//         else if (strstr(pTmp, "PO"))
//            *(pOutbuf+OFF_BLDG_QUAL) = 'P';
//         else if (strstr(pTmp, "FA"))
//            *(pOutbuf+OFF_BLDG_QUAL) = 'F';
//         else
//         {
//            while (*pTmp && *pTmp > '9') pTmp++;
//            iTmp = atoi(pTmp);
//            if (iTmp > 0)
//            {
//               iRet = Quality2Code(pTmp, acCode, NULL);
//               if (acCode[0] > ' ')
//                  *(pOutbuf+OFF_BLDG_QUAL) = acCode[0];
//            }
//         }
//      }
//   }
//
//   // Yrblt
//   lTmp = atoin(pChar->YearBuilt, MBSIZ_CHAR_YRBLT);
//   if (lTmp > 1800)
//      memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);
//   else
//      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);
//
//   // BldgSqft
//   lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
//   if (lBldgSqft > 10)
//   {
//      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
//      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
//   } else
//      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);
//
//   // Garage Sqft
//   lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
//   if (lGarSqft > 10)
//   {
//      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
//      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
//      *(pOutbuf+OFF_PARK_TYPE) = '2';
//   } else
//   {
//      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
//      *(pOutbuf+OFF_PARK_TYPE) = ' ';
//   }
//
//   // Heating
//   if (pChar->Heating[0] > ' ')
//   {
//      iTmp = 0;
//      while (asHeating[iTmp].iLen > 0)
//      {
//         if (pChar->Heating[0] == asHeating[iTmp].acSrc[0])
//         {
//            *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
//            break;
//         }
//         iTmp++;
//      }
//   }
//
//   // Cooling
//   if (pChar->Cooling[0] > ' ')
//   {
//      iTmp = 0;
//      while (asCooling[iTmp].iLen > 0)
//      {
//         if (pChar->Cooling[0] == asCooling[iTmp].acSrc[0])
//         {
//            *(pOutbuf+OFF_AIR_COND) = asCooling[iTmp].acCode[0];
//            break;
//         }
//         iTmp++;
//      }
//   }
//   // Pool
//   if (pChar->NumPools[0] > ' ')
//   {
//      iTmp = 0;
//      while (asPool[iTmp].iLen > 0)
//      {
//         if (pChar->NumPools[0] == asPool[iTmp].acSrc[0])
//         {
//            *(pOutbuf+OFF_POOL) = asPool[iTmp].acCode[0];
//            break;
//         }
//         iTmp++;
//      }
//   }
//   // Beds
//   iBeds = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
//   if (iBeds > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
//      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
//   } else
//      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);
//
//   // Bath
//   iFBath = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
//   if (iFBath > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
//      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
//   } else
//      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);
//
//   // Half bath
//   iHBath = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
//   if (iHBath > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
//      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
//   } else
//      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);
//
//   // Fireplace
//   iFp = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
//   if (iFp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
//      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
//   } else
//      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);
//
//   lCharMatch++;
//
//   // Get next Char rec
//   pRec = fgets(acRec, 512, fdChar);
//
//   return 0;
//}

/***************************** Pla_MergeStdChar ******************************
 *
 *  Merge STDCHAR format.
 *
 *****************************************************************************/

int Pla_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft;
   int      iLoop, iBeds, iFBath, iHBath;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

#ifdef _DEBUG
   //if (!memcmp(acRec, "006142045000", 9) || !memcmp(pOutbuf, "006142045000", 9))
   //   iLoop = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   // Yrblt
   memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   } else
      memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

   // Parking type
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // Heating
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];

   // Cooling
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // Fireplace
   *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

   // Pools
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Units
   int iUnits = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (iUnits > 0)
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, iUnits);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   } else
      memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************** Pla_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************

int Pla_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, ',');
      pTmp += 2;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   iRet = ParseStringNQ(pRec, ',', MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0 && lTmp < 999999999)
   {
      long lGross = atoin(pOutbuf+OFF_GROSS, SIZ_GROSS);

      if (lTmp > lGross)
      {
         LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Pla_MergeLien ******************************
 *
 * Note:
 *
 *****************************************************************************/

int Pla_MergeLien(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *apItems[32];
   int      iRet, iTmp;
   long     lLand, lImpr, lGrow, lFixt, lPP, lExe, lTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      //pRec = fgets(acRec, 512, fdValue);
      // Get first rec
      pRec = fgets(acRec, 512, fdValue);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Lien rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdValue);
         lValueSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = 1;
   do 
   {
      iTmp = ParseStringNQ(pRec, cLdrSep, MB_LIEN_OTHEXE+1, apItems);
      if (iTmp < MB_LIEN_OTHEXE)
      {
         LogMsg0("*** Bad Lien record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         lValueSkip++;
      } else
      {
         // Land
         lLand = atol(apItems[MB_LIEN_LAND]);
         if (lLand > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LAND, lLand); 
            memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
         }
         // Impr
         lImpr = atol(apItems[MB_LIEN_IMPR]);
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LAND, lImpr); 
            memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_LAND);
         }

         // Other values
         lGrow = atol(apItems[MB_LIEN_GROWING]);
         lFixt = atol(apItems[MB_LIEN_FIXTRS]);
         lPP = atol(apItems[MB_LIEN_PP]);
         lTmp = lGrow + lFixt + lPP;
         if (lTmp > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LAND, lTmp); 
            memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_LAND);

            if (lGrow > 0)
            {
               sprintf(acTmp, "%d         ", lGrow);
               memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
            }
            if (lFixt > 0)
            {
               sprintf(acTmp, "%d         ", lFixt);
               memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
            }
            if (lPP > 0)
            {
               sprintf(acTmp, "%d         ", lPP);
               memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
            }
         }

         // Gross
         long lGross = lTmp+lLand+lImpr;
         if (lGross > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LAND, lGross); 
            memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_LAND);

            // Ratio
            if (lImpr > 0)
            {
               sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
               memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
            }
         }

         lExe = atol(apItems[MB_LIEN_HOEXE]);
         if (lExe == 999999999) lExe = 0;

         if (lExe > 0)
         {
            *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
            memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
         } else
            *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

         lTmp = lExe + atol(apItems[MB_LIEN_OTHEXE]);
         if (lTmp > 0)
         {
            if (lTmp > lGross)
            {
               LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
               lTmp = lGross;
            }
            sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp); 
            memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
         }

         // Populate ExeType
         makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&PLA_Exemption);

         iRet = 0;
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdValue);
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         break;         // EOF
      }
   } while (iTmp < MB_LIEN_OTHEXE);

   if (!iRet)
      lValueMatch++;

   return iRet;
}

int Pla_MergeLien_2019(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *apItems[32];
   int      iRet, iTmp;
   long     lLand, lImpr, lGrow, lFixt, lPP, lExe, lTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      //pRec = fgets(acRec, 512, fdValue);
      // Get first rec
      pRec = fgets(acRec, 512, fdValue);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Lien rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdValue);
         lValueSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = 1;
   do 
   {
      iTmp = ParseStringNQ(pRec, ',', MB_LIEN_OTHEXE+1, apItems);
      if (iTmp < MB_LIEN_OTHEXE)
      {
         LogMsg0("*** Bad Lien record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         lValueSkip++;
      } else
      {
         // Land
         lLand = atol(apItems[MB_LIEN_LAND]);
         if (lLand > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LAND, lLand); 
            memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
         }
         // Impr
         lImpr = atol(apItems[MB_LIEN_IMPR]);
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LAND, lImpr); 
            memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_LAND);
         }

         // Other values
         lGrow = atol(apItems[MB_LIEN_GROWING]);
         lFixt = atol(apItems[MB_LIEN_FIXTRS]);
         lPP = atol(apItems[MB_LIEN_PP]);
         lTmp = lGrow + lFixt + lPP;
         if (lTmp > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LAND, lTmp); 
            memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_LAND);

            if (lGrow > 0)
            {
               sprintf(acTmp, "%d         ", lGrow);
               memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
            }
            if (lFixt > 0)
            {
               sprintf(acTmp, "%d         ", lFixt);
               memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
            }
            if (lPP > 0)
            {
               sprintf(acTmp, "%d         ", lPP);
               memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
            }
         }

         // Gross
         long lGross = lTmp+lLand+lImpr;
         if (lGross > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LAND, lGross); 
            memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_LAND);

            // Ratio
            if (lImpr > 0)
            {
               sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
               memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
            }
         }

         lExe = atol(apItems[MB_LIEN_HOEXE]);
         if (lExe == 999999999) lExe = 0;

         if (lExe > 0)
            *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
         else
            *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

         lTmp = lExe + atol(apItems[MB_LIEN_OTHEXE]);
         if (lTmp > 0)
         {
            if (lTmp > lGross)
            {
               LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
               lTmp = lGross;
            }
            sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp); 
            memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
         }
         iRet = 0;
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdValue);
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         break;         // EOF
      }
   } while (iTmp < MB_LIEN_OTHEXE);

   if (!iRet)
      lValueMatch++;

   return iRet;
}

/******************************* Pla_MergeCur *********************************
 *
 ******************************************************************************/

int Pla_MergeCurr(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *apItems[32];
   int      iRet, iTmp;
   long     lLand, lImpr, lExe, lTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      //pRec = fgets(acRec, 512, fdValue);
      // Get first rec
      pRec = fgets(acRec, 512, fdValue);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip value rec   %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdValue);
         lValueSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = 1;
   do 
   {
      iTmp = ParseStringNQ(pRec, cDelim, MB_CURR_OTHEXE_CD+1, apItems);
      if (iTmp < MB_CURR_OTHEXE_CD)
      {
         LogMsg0("*** Bad current value for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         lValueSkip++;
      } else
      {
         // Land
         lLand = atol(apItems[MB_CURR_LAND]);
         if (lLand > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LAND, lLand); 
            memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
         }
         // Impr
         lImpr = atol(apItems[MB_CURR_IMPR]);
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LAND, lImpr); 
            memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_LAND);
         }

         // Other values
         long lGrow = atoi(apItems[MB_CURR_GROWING]);
         long lFixt = atoi(apItems[MB_CURR_FIXTRS]);
         long lPP   = atoi(apItems[MB_CURR_PP_BUS]);
         long lHSite= atoi(apItems[MB_CURR_HOMESITE]);
         long lMH   = atoi(apItems[MB_CURR_PPMOBILHOME]);

         lTmp = lGrow+lFixt+lPP+lMH;
         if (lTmp > 0)
         {
            sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
            memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
            
            if (lGrow > 0)
            {
               sprintf(acTmp, "%d         ", lGrow);
               memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
            }
            if (lFixt > 0)
            {
               sprintf(acTmp, "%d         ", lFixt);
               memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
            }
            if (lPP > 0)
            {
               sprintf(acTmp, "%d         ", lPP);
               memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
            }
            if (lHSite > 0)
            {
               sprintf(acTmp, "%d         ", lHSite);
               memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
            }
            if (lMH > 0)
            {
               sprintf(acTmp, "%d         ", lMH);
               memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
            }
         }

         // Gross
         long lGross = lTmp+lLand+lImpr;
         if (lGross > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LAND, lGross); 
            memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_LAND);

            // Ratio
            if (lImpr > 0)
            {
               sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
               memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
            }
         }

         lExe = atol(apItems[MB_CURR_HOEXE]);
         if (lExe == 999999999) lExe = 0;

         if (lExe > 0)
            *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
         else
            *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

         lTmp = lExe + atol(apItems[MB_CURR_OTHEXE]);
         if (lTmp > 0)
         {
            if (lTmp > lGross)
            {
               LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
               lTmp = lGross;
            }
            sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp); 
            memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
         }
      
         if (*apItems[MB_CURR_OTHEXE_CD] > ' ')
            memcpy(pOutbuf+OFF_EXE_CD1, apItems[MB_CURR_OTHEXE_CD], strlen(apItems[MB_CURR_OTHEXE_CD]));

         iRet = 0;
      }

      // Get next current value record
      pRec = fgets(acRec, 512, fdValue);
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         break;         // EOF
      }
   } while (iTmp < MB_CURR_OTHEXE_CD);

   if (!iRet)
      lValueMatch++;

   return iRet;
}

/********************************* Pla_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Pla_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], acCode[64], acStr[64], acSfx[64], acAddr1[256], *pTmp;
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Remove unprintable char
   replNull(pRollRec);

   // Parse record
   iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_INACTIVE)
   {
      LogMsg("***** Error: Pla_MergeRoll()->bad input record for APN=%s (%d)", apTokens[iApnFld], iRet);
      return -1;
   }

   // Remove all parcels with AsmtStatus D or I except those that have a match in  
   // the closed roll (e.g. 335-010-056 and 335-230-008) - 04/15/2022
   if (iFlag && !(iFlag & CREATE_LIEN) && (*apTokens[MB_ROLL_ASMTSTATUS] != 'A'))
      return -1;

   // Ignore APN starts with 800-999 except 910 (MH), ignore 940 (OIL/GAS)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = *apTokens[MB_ROLL_ASMTSTATUS];

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "31PLA", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning - No Zoning since 2017 LDR
   //if (*apTokens[MB_ROLL_ZONING] > ' ')
   //{
   //   iTmp = strlen(apTokens[MB_ROLL_ZONING]);
   //   if (iTmp > SIZ_ZONE)
   //      iTmp = SIZ_ZONE;
   //   memcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], iTmp);
   //}

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   acTmp[SIZ_USE_CO] = 0;
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Megabyte:
   // Standard UseCode
   //if (acTmp[0] > ' ')
   //{
   //   _strupr(acTmp);
   //   iTmp = strlen(acTmp);
   //   memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
   //   if (iNumUseCodes > 0)
   //   {
   //      pTmp = Cnty2StdUse(acTmp);
   //      if (pTmp)
   //         memcpy(pOutbuf+OFF_USE_STD, pTmp, 3);
   //      else
   //      {
   //         memcpy(pOutbuf+OFF_USE_STD, USE_MISC, 3);
   //         logUsecode(acTmp, pOutbuf);
   //      }
   //   }
   //}

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001012058000", 9))
   //   iTmp = 0;
#endif

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (ULONG)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);

      // Look into legal to see if they have info
      pTmp = strstr(apTokens[MB_ROLL_LEGAL], " AC ");
      if (pTmp)
      {
         do {
            pTmp--;
         } while (isdigit(*pTmp) || *pTmp == '.');

         // get acres
         dTmp = atof(++pTmp);
         if (dTmp > 0.0)
         {
            // Lot Sqft
            lTmp = (long)(dTmp * SQFT_PER_ACRE);
            sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
            memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

            // Format Acres
            lTmp = (long)(dTmp * ACRES_FACTOR);
            sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
            memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
         }
      }
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      // 06/02/2020
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MMM_DD_YYYY);
      //pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp && !memcmp(acTmp, apTokens[MB_ROLL_DOCNUM], 4))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], strlen(apTokens[MB_ROLL_DOCNUM]));
      } else
      {
         memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
         memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
      }
   }

   // Owner
   Pla_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);

   // Merge Situs
   removeSitus(pOutbuf);
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_ROLL_STRNUM]);
   if (lTmp > 0)
   {
      // Save original StrNum
      vmemcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_ROLL_STRNUM], SIZ_S_HSENO);

      iTmp = sprintf(acTmp, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      strcpy(acAddr1, acTmp);

      if (pTmp = strchr(apTokens[MB_ROLL_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apTokens[MB_ROLL_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_ROLL_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[MB_ROLL_STRDIR], strlen(apTokens[MB_ROLL_STRDIR]));
      }
   }

   if (*apTokens[MB_ROLL_STRTYPE] > ' ')
   {
      strcpy(acSfx, apTokens[MB_ROLL_STRTYPE]);
      myTrim(acSfx);
      strcpy(acStr, apTokens[MB_ROLL_STRNAME]);
      myTrim(acStr);

      // If strName also has sfx, remove it
      pTmp = strrchr(acStr, ' ');
      if (pTmp && !strcmp(pTmp+1, acSfx))
         *pTmp = 0;

      strcat(acAddr1, acStr);
      strcat(acAddr1, " ");
      strcat(acAddr1, acSfx);

      vmemcpy(pOutbuf+OFF_S_STREET, acStr, SIZ_S_STREET);

      iTmp = GetSfxCodeX(acSfx, acTmp);
      if (iTmp > 0)
      {
         Sfx2Code(acTmp, acCode);
         memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      } else
      {
         LogMsg0("*** Invalid suffix: %s", acSfx);
         iBadSuffix++;
      }
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apTokens[MB_ROLL_STRNAME]);
      if (sAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      if (sAdr.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, SIZ_S_SUFF);

      strcat(acAddr1, apTokens[MB_ROLL_STRNAME]);
   }

   if (*apTokens[MB_ROLL_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_ROLL_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_ROLL_UNIT], SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, apTokens[MB_ROLL_UNIT], SIZ_S_UNITNOX);
   }

   // Save Addr1
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   if (*apTokens[MB_ROLL_COMMUNITY] > ' ')
   {
      if (*(apTokens[MB_ROLL_COMMUNITY]+3) == '\\' || *(apTokens[MB_ROLL_COMMUNITY]+3) == ';')
         *(apTokens[MB_ROLL_COMMUNITY]+3) = 0;
      Abbr2Code(apTokens[MB_ROLL_COMMUNITY], acTmp, acAddr1, pOutbuf);
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
      {
         sprintf(acTmp, "%s CA %s", myTrim(acAddr1), apTokens[MB_ROLL_ZIP]);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
      }
   }

   lTmp = atol(apTokens[MB_ROLL_ZIP]);
   if (lTmp > 90000)
      memcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_ROLL_ZIP], SIZ_S_ZIP);

   // Mailing
   Pla_MergeMAdr(pOutbuf);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/******************************** Pla_MergeExe *******************************
 *
 * Populate exemption codes and ExeType
 *
 *****************************************************************************/

int Pla_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   int      iTmp, iOffset;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, acRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, acRec);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   iOffset = OFF_EXE_CD1;
   *(pOutbuf+OFF_HO_FL) = '2';         // 'N'
   do
   {
      iTmp = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
      if (iTmp < MB_EXE_EXEPCT)
      {
         LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdExe);
         return -1;
      }

      // EXE code
      if (*apTokens[MB_EXE_CODE] > ' ')
      {
         memcpy(pOutbuf+iOffset, apTokens[MB_EXE_CODE], strlen(apTokens[MB_EXE_CODE]));
         iOffset += SIZ_EXE_CD1;
      }

      // HO Exe
      myLTrim(apTokens[MB_EXE_HOEXE]);
      if (*apTokens[MB_EXE_HOEXE] == '1' || *apTokens[MB_EXE_HOEXE] == 'T')
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'

      // Get next record
      pRec = fgets(acRec, 512, fdExe);
      if (!pRec)
         break;
   } while (iOffset <= OFF_EXE_CD3 && !memcmp(pOutbuf, acRec, iApnLen));

   lExeMatch++;

   return 0;
}

int Pla_MergeExe_old(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     *pTmp;
   int      iTmp, iOffset;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
      if (acRec[0] == '"')
         iTmp = 1;
      else
         iTmp = 0;
      if (iTmp != iSkipQuote)
      {
         LogMsg("*** Overwrite INI SkipQuote value to %d (from %d)", iTmp, iSkipQuote);
         iSkipQuote = iTmp;
      }
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      iTmp = memcmp(pOutbuf, pTmp+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   iOffset = OFF_EXE_CD1;
   do
   {
      iTmp = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
      if (iTmp < MB_EXE_EXEPCT)
      {
         LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdExe);
         return -1;
      }

      // EXE code
      if (*apTokens[MB_EXE_CODE] > ' ')
      {
         memcpy(pOutbuf+iOffset, apTokens[MB_EXE_CODE], strlen(apTokens[MB_EXE_CODE]));
         iOffset += SIZ_EXE_CD1;
      }

      // HO Exe
      myLTrim(apTokens[MB_EXE_HOEXE]);
      if (*apTokens[MB_EXE_HOEXE] == '1' || *apTokens[MB_EXE_HOEXE] == 'T')
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'

      // Get next record
      pRec = fgets(acRec, 512, fdExe);
      if (pRec)
      {
         // Asmt is on 2nd token
         pTmp = strchr(acRec, cDelim);
         pTmp++;
      } else
         break;
   } while (iOffset <= OFF_EXE_CD3 && !memcmp(pOutbuf, pTmp+iSkipQuote, iApnLen));

   lExeMatch++;

   return 0;
}

/********************************* Pla_Load_Roll ******************************
 *
 * This function has not been tested.
 *
 ******************************************************************************/

int Pla_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Initialize
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Fix broken records in roll file
   sprintf(acTmpFile, "%s\\%s\\%s_roll.fix", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = RebuildCsv(acRollFile, acTmpFile, cDelim, MB_ROLL_FLDS);
   lLastFileDate = getFileDate(acRollFile);

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "EMP");
   if (cDelim == '|')
      lRet = sortFile(acExeFile, acTmpFile, "S(#1,C,A) DEL(124) B(66,R) F(TXT)");
   else
      lRet = sortFile(acExeFile, acTmpFile, "S(#1,C,A) B(78,R) F(TXT)");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open current value file for new parcels
   LogMsg("Open Current value file %s", acValueFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "LMP");
   if (cDelim == '|')
      lRet = sortFile(acValueFile, acTmpFile, "S(#1,C,A) DEL(124) F(TXT)");
   else
      lRet = sortFile(acValueFile, acTmpFile, "S(#1,C,A) F(TXT)");
   fdValue = fopen(acTmpFile, "r");
   if (fdValue == NULL)
   {
      LogMsg("***** Error opening Current value file: %s\n", acTmpFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (acRollRec[0] < '0' || acRollRec[0] > '9');

   // Skip test records
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (!memcmp(acRollRec, "000000", 6));
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "006142045000", 9))
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Pla_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Exe
            if (fdExe)
               lRet = Pla_MergeExe(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Pla_MergeStdChar(acBuf);
               //lRet = Pla_MergeChar(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, (char *)&acRollRec[iSkipQuote], lCnt);

         // Create new R01 record
         iRet = Pla_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Lien values
            if (fdValue)
               lRet = Pla_MergeCurr(acRec);

            // Merge Exe
            if (fdExe)
               lRet = Pla_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Pla_MergeStdChar(acRec);
               //lRet = Pla_MergeChar(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);

         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", 
               iApnLen, acBuf, iApnLen, (char *)&acRollRec[iSkipQuote], lCnt);
         iRetiredRec++;

         continue;
      }

#ifdef _DEBUG
      //if (!memcmp(acBuf, "477310060", 9))
      //   iRet = replCharEx(acBuf, 31, ' ', iRecLen);
#endif

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, (char *)&acRollRec[iSkipQuote], lCnt);

      // Create new R01 record
      iRet = Pla_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Lien values
         if (fdValue)
            lRet = Pla_MergeCurr(acRec);

         // Merge Exe
         if (fdExe)
            lRet = Pla_MergeExe(acRec);

         // Merge Char
         if (fdChar)
            lRet = Pla_MergeStdChar(acRec);
            //lRet = Pla_MergeChar(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      
      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }


   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdExe)
      fclose(fdExe);
   if (fdValue)
      fclose(fdValue);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   //LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Curr matched:     %u\n", lValueMatch);

   //LogMsg("Number of Situs skipped:    %u", lSitusSkip);
   LogMsg("Number of Char skipped:     %u", lCharSkip);
   LogMsg("Number of Exe skipped:      %u", lExeSkip);
   LogMsg("Number of Curr skipped:     %u\n", lValueSkip);

   LogMsg("Last recording date:        %u\n", lLastRecDate);

   printf("\nTotal output records: %u\n", lRecCnt);
   return 0;
}

/******************************** Pla_Load_LDR ******************************
 *
 * 
 *
 ****************************************************************************/

int Pla_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     iRet, lRet, lCnt=0, lRollDrop, lValueDrop;

   LogMsg0("Loading LDR");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Sales file
   //LogMsg("Open Sales file %s", acSalesFile);
   //sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SMP");
   //lRet = sortFile(acSalesFile, acTmpFile, "S(#1,C,A,#3,DAT,A) F(TXT)");
   //fdSale = fopen(acTmpFile, "r");
   //if (fdSale == NULL)
   //{
   //   LogMsg("***** Error opening Sales file: %s\n", acTmpFile);
   //   return -2;
   //}

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "EMP");
   sprintf(acRec, "S(#1,C,A) DEL(%c) B(66,R) F(TXT)", cDelim);
   lRet = sortFile(acExeFile, acTmpFile, acRec);
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Lien file
   LogMsg("Open Lien value file %s", acValueFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "LMP");
   sprintf(acRec, "S(#1,C,A) DEL(%c) F(TXT)", cLdrSep);
   lRet = sortFile(acValueFile, acTmpFile, acRec);
   fdValue = fopen(acTmpFile, "r");
   if (fdValue == NULL)
   {
      LogMsg("***** Error opening Current value file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Drop header record
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   lLDRRecCount=lRollDrop=lValueDrop=iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdRoll) && fdValue)
   {
      // Create new R01 record
      iRet = Pla_MergeRoll(acBuf, acRec, MAX_RECSIZE, CREATE_R01|CREATE_LIEN);
      if (iRet == 0)
      {
         // Merge Exe
         if (fdExe)
            iRet = Pla_MergeExe(acBuf);

         // Merge Char
         if (fdChar)
            iRet = Pla_MergeStdChar(acBuf);
            //iRet = Pla_MergeChar(acBuf);

         // Merge Sales
         //if (fdSale)
         //   iRet = Pla_MergeSale(acBuf);

         // Merge values - if no lien value for the record, drop it
         // Rule change: Keep them even if no lien value
         // Rule change again for LDR 2009: drop records without lien value
         iRet = Pla_MergeLien(acBuf);

         if (!iRet)
         {
#ifdef _DEBUG
            //iRet = replCharEx(acBuf, 31, ' ', iRecLen);
            //if (iRet)
            //   iRet = 0;
#endif
            // Save last recording date
            lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            lLDRRecCount++;
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

            if (!bRet)
            {
               LogMsg("***** Error writing to output file at record %d\n", lCnt);
               lRet = WRITE_ERR;
               break;
            }
         } else
            lValueDrop++;
      } else
         lRollDrop++;

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   //if (fdSale)
   //   fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdValue)
      fclose(fdValue);
   if (fhOut)
      CloseHandle(fhOut);

   // Sort output file
   //LogMsg("Sort output file %s to %s", acTmpFile, acOutFile);
   //sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) B(%d,R)", iRecLen, iRecLen);
   //lRet = sortFile(acTmpFile, acOutFile, acTmp);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("      output records:       %u", lLDRRecCount);
   LogMsg("      roll record dropped:  %u", lRollDrop);
   LogMsg("      value record dropped: %u", lValueDrop);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u\n", iBadSuffix);

   //LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   //LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Lien matched:     %u\n", lValueMatch);

   //LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   //LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Number of Lien skiped:      %u\n", lValueSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   lRecCnt = lLDRRecCount;
   if (lRet == WRITE_ERR)
      return lRet;
   else
      return 0;
}

/******************************* Pla_ConvChar1() ****************************
 *
 * Process old Char format only.
 * Return > 1 if successful.  < 0 is file open error, 0 is sort error, 1 is wrong layout.
 *
 ****************************************************************************/

//int Pla_ConvChar1(char *pInfile)
//{
//   FILE     *fdIn, *fdOut;
//   char     acBuf[1024], acTmpFile[256], acTmp[256], *pRec;
//   int      iRet, iTmp, iCnt=0;
//   MB_CHAR  myCharRec;
//
//   LogMsgD("\nConverting char file using ConvChar1(): %s\n", pInfile);
//   if (!(fdIn = fopen(pInfile, "r")))
//      return -1;
//
//   // Skip first record - header
//   pRec = fgets(acBuf, 1024, fdIn);
//   // Test input file - if new format, return 1
//   iRet = ParseStringNQ(pRec, ',', MB_CHAR_UNITSEQNO+1, apTokens);
//   if (iRet < MB_CHAR_UNITSEQNO)
//   {
//      fclose(fdIn);
//      return 1;
//   }
//
//   LogMsgD("\nConverting char file %s\n", pInfile);
//   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   if (!(fdOut = fopen(acTmpFile, "w")))
//   {
//      fclose(fdIn);
//      LogMsg("***** Error creating output file %s", acTmpFile);
//      return -2;
//   }
//
//   while (!feof(fdIn))
//   {
//      pRec = fgets(acBuf, 1024, fdIn);
//
//      if (!pRec)
//         break;
//
//      iRet = ParseStringNQ(pRec, ',', MB_CHAR_UNITSEQNO+1, apTokens);
//      if (iRet < MB_CHAR_UNITSEQNO)
//         break;
//
//      memset((void *)&myCharRec, ' ', sizeof(MB_CHAR));
//      memcpy(myCharRec.Asmt, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));
//
//      iTmp = atoi(apTokens[MB_CHAR_POOLS]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.NumPools, acTmp, iRet);
//      } else if (*apTokens[MB_CHAR_POOLS] >= 'A')
//      {
//         iTmp = strlen(apTokens[MB_CHAR_POOLS]);
//         if (iTmp > MBSIZ_CHAR_POOLS) iTmp = MBSIZ_CHAR_POOLS;
//         memcpy(myCharRec.NumPools, apTokens[MB_CHAR_POOLS], iTmp);
//      }
//
//      memcpy(myCharRec.LandUseCat, apTokens[MB_CHAR_USECAT], strlen(apTokens[MB_CHAR_USECAT]));
//      memcpy(myCharRec.QualityClass, apTokens[MB_CHAR_QUALITY], strlen(apTokens[MB_CHAR_QUALITY]));
//      memcpy(myCharRec.YearBuilt, apTokens[MB_CHAR_YRBLT], strlen(apTokens[MB_CHAR_YRBLT]));
//      memcpy(myCharRec.BuildingSize, apTokens[MB_CHAR_BLDGSQFT], strlen(apTokens[MB_CHAR_BLDGSQFT]));
//      memcpy(myCharRec.SqFTGarage, apTokens[MB_CHAR_GARSQFT], strlen(apTokens[MB_CHAR_GARSQFT]));
//      memcpy(myCharRec.Heating, apTokens[MB_CHAR_HEATING], strlen(apTokens[MB_CHAR_HEATING]));
//      memcpy(myCharRec.Cooling, apTokens[MB_CHAR_COOLING], strlen(apTokens[MB_CHAR_COOLING]));
//      memcpy(myCharRec.HeatingSource, apTokens[MB_CHAR_HEATING_SRC], strlen(apTokens[MB_CHAR_HEATING_SRC]));
//      memcpy(myCharRec.CoolingSource, apTokens[MB_CHAR_COOLING_SRC], strlen(apTokens[MB_CHAR_COOLING_SRC]));
//
//      iTmp = atoi(apTokens[MB_CHAR_BEDS]);
//      if (iTmp > 0)
//         memcpy(myCharRec.NumBedrooms, apTokens[MB_CHAR_BEDS], strlen(apTokens[MB_CHAR_BEDS]));
//
//      iTmp = atoi(apTokens[MB_CHAR_FBATHS]);
//      if (iTmp > 0)
//         memcpy(myCharRec.NumFullBaths, apTokens[MB_CHAR_FBATHS], strlen(apTokens[MB_CHAR_FBATHS]));
//
//      iTmp = atoi(apTokens[MB_CHAR_HBATHS]);
//      if (iTmp > 0)
//         memcpy(myCharRec.NumHalfBaths, apTokens[MB_CHAR_HBATHS], strlen(apTokens[MB_CHAR_HBATHS]));
//
//      iTmp = atoi(apTokens[MB_CHAR_FP]);
//      if (iTmp > 0)
//         memcpy(myCharRec.NumFireplaces, apTokens[MB_CHAR_FP], strlen(apTokens[MB_CHAR_FP]));
//
//      memcpy(myCharRec.FeeParcel, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));
//
//      myCharRec.HasSeptic = ' ';
//      myCharRec.HasSewer = ' ';
//      myCharRec.HasWell = ' ';
//
//      myCharRec.CRLF[0] = '\n';
//      myCharRec.CRLF[1] = '\0';
//      fputs((char *)&myCharRec.Asmt[0], fdOut);
//
//      iCnt++;
//      if (!(++iCnt % 1000))
//         printf("\r%u", iCnt);
//   }
//
//   if (fdIn) fclose(fdIn);
//   if (fdOut) fclose(fdOut);
//
//   // Sort output on ASMT
//   if (iCnt > 100)
//   {
//      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
//      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A)");
//   } else
//   {
//      printf("\n");
//      iRet = 0;
//   }
//
//   return iRet;
//}

/****************************** Pla_ConvChar() ******************************
 *
 * Handle new CHAR file 7/7/2014
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

//int Pla_ConvChar2(char *pInfile)
//{
//   FILE     *fdIn, *fdOut;
//   char     acBuf[1024], acTmpFile[256], acOutBuf[1024], *pRec;
//   int      iRet, iTmp, iCnt=0;
//   MB_CHAR  *pCharRec = ( MB_CHAR *)&acOutBuf;
//
//   LogMsgD("\nConverting char file using ConvChar2(): %s\n", pInfile);
//   if (!(fdIn = fopen(pInfile, "r")))
//      return -1;
//
//   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   if (!(fdOut = fopen(acTmpFile, "w")))
//   {
//      fclose(fdIn);
//      LogMsg("***** Error creating output file %s", acTmpFile);
//      return -2;
//   }
//
//   // Skip first record - header
//   pRec = fgets(acBuf, 1023, fdIn);
//
//   while (!feof(fdIn))
//   {
//      pRec = fgets(acBuf, 1023, fdIn);
//      if (!pRec)
//         break;
//
//      iRet = ParseStringNQ(pRec, ',', PLA_CHR_FP+1, apTokens);
//      if (iRet < PLA_CHR_FP)
//         break;
//
//      memset(acOutBuf, ' ', sizeof(MB_CHAR));
//      memcpy(pCharRec->Asmt, apTokens[PLA_CHR_FEE_PRCL], strlen(apTokens[PLA_CHR_FEE_PRCL]));
//
//      if (isdigit(*apTokens[PLA_CHR_POOLS]))
//         pCharRec->NumPools[0] = 'P';
//      else 
//      {
//         switch (*apTokens[PLA_CHR_POOLS])
//         {
//            case 'A':
//               pCharRec->NumPools[0] = 'X';  // Above gound
//               break;
//            case 'G':
//               pCharRec->NumPools[0] = 'A';  // Concrete inground
//               break;
//            case 'H':
//               pCharRec->NumPools[0] = 'S';  // Spa only
//               break;
//            case 'S':
//               pCharRec->NumPools[0] = 'C';  // Spa included
//               break;
//            case 'V':
//               pCharRec->NumPools[0] = 'F';  // Vinyl/Fiberglass inground
//               break;
//         }
//      }
//
//      memcpy(pCharRec->LandUseCat, apTokens[PLA_CHR_USECAT], strlen(apTokens[PLA_CHR_USECAT]));
//      memcpy(pCharRec->QualityClass, apTokens[PLA_CHR_CLASS], strlen(apTokens[PLA_CHR_CLASS]));
//      memcpy(pCharRec->YearBuilt, apTokens[PLA_CHR_YRBLT], strlen(apTokens[PLA_CHR_YRBLT]));
//      memcpy(pCharRec->BuildingSize, apTokens[PLA_CHR_BLDGSQFT], strlen(apTokens[PLA_CHR_BLDGSQFT]));
//      memcpy(pCharRec->SqFTGarage, apTokens[PLA_CHR_GARSQFT], strlen(apTokens[PLA_CHR_GARSQFT]));
//      memcpy(pCharRec->Heating, apTokens[PLA_CHR_HEATING], strlen(apTokens[PLA_CHR_HEATING]));
//      memcpy(pCharRec->Cooling, apTokens[PLA_CHR_COOLING], strlen(apTokens[PLA_CHR_COOLING]));
//
//      iTmp = atoi(apTokens[PLA_CHR_BEDS]);
//      if (iTmp > 0)
//         memcpy(pCharRec->NumBedrooms, apTokens[PLA_CHR_BEDS], strlen(apTokens[PLA_CHR_BEDS]));
//
//      iTmp = atoi(apTokens[PLA_CHR_FBATHS]);
//      if (iTmp > 0)
//         memcpy(pCharRec->NumFullBaths, apTokens[PLA_CHR_FBATHS], strlen(apTokens[PLA_CHR_FBATHS]));
//
//      iTmp = atoi(apTokens[PLA_CHR_HBATHS]);
//      if (iTmp > 0)
//         memcpy(pCharRec->NumHalfBaths, apTokens[PLA_CHR_HBATHS], strlen(apTokens[PLA_CHR_HBATHS]));
//
//      iTmp = atoi(apTokens[PLA_CHR_FP]);
//      if (*apTokens[PLA_CHR_FP] > ' ')
//      {
//         if (isdigit(*apTokens[PLA_CHR_FP]))
//            memcpy(pCharRec->NumFireplaces, apTokens[PLA_CHR_FP], strlen(apTokens[PLA_CHR_FP]));
//         else 
//         {
//            switch(*apTokens[PLA_CHR_FP])
//            {
//               case 'P':
//                  pCharRec->NumFireplaces[0] = 'S';   // Pellet Stove
//                  break;
//               case 'B':
//                  pCharRec->NumFireplaces[0] = '2';   // Fireplace and Wood Stove
//                  break;
//               case 'W':
//                  pCharRec->NumFireplaces[0] = 'W';   // Wood Stove
//                  break;
//               case 'N':
//               case 'O':
//                  pCharRec->NumFireplaces[0] = 'N';   // No
//                  break;
//               default:
//                  pCharRec->NumFireplaces[0] = 'Y';
//                  break;
//            }
//         }
//      }
//
//      memcpy(pCharRec->FeeParcel, apTokens[PLA_CHR_FEE_PRCL], strlen(apTokens[PLA_CHR_FEE_PRCL]));
//
//      pCharRec->HasSeptic = ' ';
//      pCharRec->HasSewer = ' ';
//      pCharRec->HasWell = ' ';
//
//      pCharRec->CRLF[0] = '\n';
//      pCharRec->CRLF[1] = '\0';
//      fputs(acOutBuf, fdOut);
//
//      iCnt++;
//      if (!(++iCnt % 1000))
//         printf("\r%u", iCnt);
//   }
//
//   if (fdIn) fclose(fdIn);
//   if (fdOut) fclose(fdOut);
//
//   if (iCnt > 100)
//   {
//      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
//      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A)");
//   } else
//   {
//      printf("\n");
//      iRet = 0;
//   }
//
//   return iRet;
//}

/****************************** Pla_ConvChar() ******************************
 *
 * Handle new CHAR file 7/5/2017
 * Adding new field UNITS 3/12/2020
 * Modify on 5/28/2020 to remove UNITS/VIEW with new layout
 * Modify on 6/3/2020 to add UNITS/VIEW with new layout
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

int Pla_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acTmp[256], acBuf[1024], acOutBuf[1024], *pRec, *pTmp;
   int      iRet, iTmp, iCnt=0, lTmp;
   STDCHAR  *pCharRec = (STDCHAR *)&acOutBuf;

   LogMsg0("\nConverting char file %s", pInfile);

   // Sorting input file
   sprintf(acBuf, "%s\\%s\\%s_Char.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      iRet = sortFile(pInfile, acBuf, "S(#2,C,A,#1,C,A) F(TXT) DEL(124) ");
   else
      iRet = sortFile(pInfile, acBuf, "S(#2,C,A,#1,C,A) F(TXT) ");
   if (iRet <= 0)
      return -99;
   LogMsg("Open char file %s", acBuf);
   if (!(fdIn = fopen(acBuf, "r")))
      return -1;

   if (!(fdOut = fopen(acCChrFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acCChrFile);
      return -2;
   }

   // Skip first record - header
   pRec = fgets(acBuf, 1023, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1023, fdIn);
      if (!pRec)
         break;

      iRet = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iRet < PLA_CHR_FLDS)
         break;

      // Skip unsecured parcels
      iTmp = atoin(apTokens[PLA_CHR_ASMT], 3);
      if (!iTmp || (iTmp >= 800 && iTmp != 910))
         continue;

      memset(acOutBuf, ' ', sizeof(STDCHAR));
      memcpy(pCharRec->Apn, apTokens[PLA_CHR_ASMT], strlen(apTokens[PLA_CHR_ASMT]));
      memcpy(pCharRec->FeeParcel, apTokens[PLA_CHR_FEE_PRCL], strlen(apTokens[PLA_CHR_FEE_PRCL]));
      vmemcpy(pCharRec->LandUseCat, apTokens[PLA_CHR_USECAT], SIZ_CHAR_SIZE2);

      // Fire place
      if (*apTokens[PLA_CHR_FP] > ' ')
      {
         iTmp = atoin(apTokens[PLA_CHR_FP], 2);
         if (iTmp > 6)
         {
            pCharRec->Fireplace[0] = 'M';
         } else
         {
            pTmp = findXlatCodeA(_strupr(apTokens[PLA_CHR_FP]), &asFirePlace[0]);
            if (pTmp)
               pCharRec->Fireplace[0] = *pTmp;
            else
               LogMsg("*** Bad FirePlace: %s [%s]", apTokens[PLA_CHR_FP], apTokens[PLA_CHR_ASMT]);
         }
      }

      // Pool 
      if (*apTokens[PLA_CHR_POOLS] > ' ')
      {
         pTmp = findXlatCodeA(apTokens[PLA_CHR_POOLS], &asPool[0]);
         if (pTmp)
            pCharRec->Pool[0] = *pTmp;
      }

      // Heating 
      if (*apTokens[PLA_CHR_HEATING] > ' ')
      {
         pTmp = findXlatCodeA(apTokens[PLA_CHR_HEATING], &asHeating[0]);
         if (pTmp)
            pCharRec->Heating[0] = *pTmp;
      }

      // Cooling 
      if (*apTokens[PLA_CHR_COOLING] > ' ')
      {
         pTmp = findXlatCodeA(apTokens[PLA_CHR_COOLING], &asCooling[0]);
         if (pTmp)
            pCharRec->Cooling[0] = *pTmp;
      }

      // Quality Class
      if (*apTokens[PLA_CHR_CLASS] > ' ')
      {
         vmemcpy(pCharRec->QualityClass, _strupr(apTokens[PLA_CHR_CLASS]), SIZ_CHAR_QCLS);
         pTmp = apTokens[PLA_CHR_CLASS];
         // Strip prefix digits
         while (*pTmp && *pTmp < 'A') pTmp++;
         if (*pTmp >= 'A')
         {
            // Class
            pCharRec->BldgClass = *pTmp++;

            // Quality
            if (strstr(pTmp, "AV"))
               pCharRec->BldgQual = 'A';
            else if (strstr(pTmp, "GD") || strstr(pTmp, "GO"))
               pCharRec->BldgQual = 'G';
            else if (strstr(pTmp, "EX"))
               pCharRec->BldgQual = 'E';
            else if (strstr(pTmp, "PO"))
               pCharRec->BldgQual = 'P';
            else if (strstr(pTmp, "FA"))
               pCharRec->BldgQual = 'F';
            else
            {
               while (*pTmp && *pTmp > '9') pTmp++;
               iTmp = atoi(pTmp);
               if (iTmp > 0)
               {
                  iRet = Quality2Code(pTmp, acTmp, NULL);
                  if (acTmp[0] > ' ')
                     pCharRec->BldgQual= acTmp[0];
               }
            }
         }
      }

      // Garage Sqft
      long lGarSqft = atol(apTokens[PLA_CHR_GARSQFT]);
      if (lGarSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pCharRec->GarSqft, acTmp, SIZ_GAR_SQFT);
         pCharRec->ParkType[0] = '2';        // Garage/Carport
      }

      // Bldg Sqft
      long lBldgSqft = atol(apTokens[PLA_CHR_BLDGSQFT]);
      if (lBldgSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
         memcpy(pCharRec->BldgSqft, acTmp, SIZ_BLDG_SF);
      }

      // Yrblt
      lTmp = atol(apTokens[PLA_CHR_YRBLT]);
      if (lTmp > 1800 && lTmp <= lLienYear)
         memcpy(pCharRec->YrBlt, apTokens[PLA_CHR_YRBLT], SIZ_YR_BLT);

      iTmp = atoi(apTokens[PLA_CHR_BEDS]);
      if (iTmp > 0)
         memcpy(pCharRec->Beds, apTokens[PLA_CHR_BEDS], strlen(apTokens[PLA_CHR_BEDS]));

      iTmp = atoi(apTokens[PLA_CHR_FBATHS]);
      if (iTmp > 0)
         memcpy(pCharRec->FBaths, apTokens[PLA_CHR_FBATHS], strlen(apTokens[PLA_CHR_FBATHS]));

      iTmp = atoi(apTokens[PLA_CHR_HBATHS]);
      if (iTmp > 0)
         memcpy(pCharRec->HBaths, apTokens[PLA_CHR_HBATHS], strlen(apTokens[PLA_CHR_HBATHS]));

      // View 
      if (*apTokens[PLA_CHR_VIEW] > ' ')
      {
         pTmp = findXlatCodeA(apTokens[PLA_CHR_VIEW], &asView[0]);
         if (pTmp)
            pCharRec->View[0] = *pTmp;
      }

      iTmp = atoi(apTokens[PLA_CHR_UNITS]);
      if (iTmp > 0)
         memcpy(pCharRec->Units, apTokens[PLA_CHR_UNITS], strlen(apTokens[PLA_CHR_UNITS]));

      pCharRec->HasSeptic = ' ';
      pCharRec->HasSewer = ' ';
      pCharRec->HasWell = ' ';

      pCharRec->CRLF[0] = '\n';
      pCharRec->CRLF[1] = '\0';
      fputs(acOutBuf, fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   //if (iCnt > 100)
   //{
   //   LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
   //   iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A)");
   //} else
   //{
   //   printf("\n");
   //   iRet = 0;
   //}

   LogMsg("Number of records output: %d", iCnt);
   return iCnt;
}

/******************************** Pla_ExtrLien ******************************
 *
 * Need testing before use in LDR 2010.
 *
 ****************************************************************************/

int Pla_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64], *apItems[32];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   iRet = ParseStringNQ(pRollRec, cLdrSep, MB_LIEN_OTHEXE+1, apItems);
   if (iRet < MB_LIEN_OTHEXE)
   {
      LogMsg("***** Error: Pla_CreateLienRec()->bad input record for APN=%s", apItems[MB_CURR_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apItems[MB_CURR_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // APN
   memcpy(pLienExtr->acApn, apItems[MB_LIEN_ASMT], strlen(apItems[MB_LIEN_ASMT]));

   // Land
   long lLand = atol(apItems[MB_LIEN_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand); 
      memcpy(pLienExtr->acLand, acTmp, SIZ_LIEN_LAND);
   }

   // Impr
   long lImpr = atol(apItems[MB_LIEN_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lImpr); 
      memcpy(pLienExtr->acImpr, acTmp, SIZ_LIEN_LAND);
   }

   // Other values
   long lGrow = atoi(apItems[MB_LIEN_GROWING]);
   long lFixt = atoi(apItems[MB_LIEN_FIXTRS]);
   long lPP   = atoi(apItems[MB_LIEN_PP]);

   lTmp = lGrow+lFixt+lPP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_OTHERS);
      
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
      }
   }

   long lExe = atol(apItems[MB_LIEN_HOEXE]);
   if (lExe == 999999999) lExe = 0;

   if (lExe > 0)
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   lTmp = lExe + atol(apItems[MB_LIEN_OTHEXE]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int Pla_CreateCurrentRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64], *apItems[32];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   iRet = ParseStringNQ(pRollRec, ',', MB_CURR_OTHEXE_CD+1, apItems);
   if (iRet < MB_CURR_OTHEXE_CD)
   {
      LogMsg("***** Error: Pla_CreateCurrentRec()->bad input record for APN=%s", apItems[MB_CURR_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apItems[MB_CURR_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // APN
   memcpy(pLienExtr->acApn, apItems[MB_CURR_ASMT], strlen(apItems[MB_CURR_ASMT]));

   // Land
   long lLand = atol(apItems[MB_CURR_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand); 
      memcpy(pLienExtr->acLand, acTmp, SIZ_LIEN_LAND);
   }

   // Impr
   long lImpr = atol(apItems[MB_CURR_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lImpr); 
      memcpy(pLienExtr->acImpr, acTmp, SIZ_LIEN_LAND);
   }

   // Other values
   long lGrow = atoi(apItems[MB_CURR_GROWING]);
   long lFixt = atoi(apItems[MB_CURR_FIXTRS]);
   long lPP   = atoi(apItems[MB_CURR_PP_BUS]);
   long lHSite= atoi(apItems[MB_CURR_HOMESITE]);
   long lMH   = atoi(apItems[MB_CURR_PPMOBILHOME]);

   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_OTHERS);
      
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lHSite > 0)
      {
         // Overlaid homesite to BusProp
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lHSite);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
      }
   }

   long lExe = atol(apItems[MB_CURR_HOEXE]);
   if (lExe == 999999999) lExe = 0;

   if (lExe > 0)
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   lTmp = lExe + atol(apItems[MB_CURR_OTHEXE]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }

   if (*apItems[MB_CURR_OTHEXE_CD] > ' ')
      memcpy(pLienExtr->extra.MB.ExeCode1, apItems[MB_CURR_OTHEXE_CD], strlen(apItems[MB_CURR_OTHEXE_CD]));

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************** Pla_ExtrLien ******************************
 *
 * Extract value from Lien value file and put in standard LIENEXTR record.  
 *
 ****************************************************************************/

int Pla_ExtrLien(LPCSTR pCnty, LPCSTR pLDRFile=NULL)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg("\nExtract lien roll for %s", pCnty);

   // Open lien file
   if (!pLDRFile)
   {
      GetIniString(pCnty, "LienVal", "", acRec, _MAX_PATH, acIniFile);
      sprintf(acBuf, acRec, pCnty, pCnty);
   } else
      strcpy(acBuf, pLDRFile);

   LogMsg("Open LDR Value file %s", acBuf);
   sprintf(acTmpFile, "%s\\%s\\%s_LienVal.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Sort on ASMT
   iRet = sortFile(acBuf, acTmpFile, "S(#1,C,A)");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted value file: %s\n", acTmpFile);
      return 2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new record
      iRet = Pla_CreateLienRec(acBuf, acRec);
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/*****************************************************************************
 *
 *****************************************************************************/

int Pla_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], acDocNum[16], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp, iFldDif;
   double   dTax;
   long     lCnt=0, lPrice, lTmp;

   LogMsg0("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      sprintf(acTmp, "*** %s - Error opening Sales file.  Errno=%d", myCounty.acCntyCode, errno);
      //sprintf(acRec, "Please review log file \"%s\" for more info on %s", acLogFile, acSalesFile);
      //mySendMail(acIniFile, acTmp, acRec, NULL);

      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Replace null char with space
      replNull(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_FLDS+4, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_FLDS+4, apTokens);
      if (iTokens < MB_SALES_FLDS)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      iFldDif = iTokens - MB_SALES_FLDS;

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Ignore DocNum start with 1900
      if (!memcmp(apTokens[MB_SALES_DOCNUM], "1900R", 5))
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "052202011000", 9))
      //   iTmp = 0;
#endif
      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
      {
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }

      // Docnum
      acDocNum[0] = 0;
      if (pTmp = strchr(apTokens[MB_SALES_DOCNUM], '`'))
         *pTmp = 0;

      if (!iDocNumFmt)
      {
         // AMA, MON, NAP, PLA, SIS, SHA, LAK, MNO, YUB
         memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));
      }

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE+iFldDif]);
      if (*apTokens[MB_SALES_GROUPSALE+iFldDif] == '1' || *apTokens[MB_SALES_GROUPSALE+iFldDif] == 'T')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001130011000", 9))
      //   iTmp = 0;
#endif
      // Confirmed sale price
      dollar2Num(apTokens[MB_SALES_PRICE+iFldDif], acTmp);
      if (acTmp[0] > '0')
      {
         lPrice = atol(acTmp);
         if (lPrice < 1000)
         {
            // This doesn't look normal, but keep it for reference only
            iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            lPrice = 0;
         } else
            iTmp = sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      } 

      // Do not use confirmed sale price
      if (!bUseConfSalePrice)
         lPrice = 0;

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001160016000", 9))
      //   iTmp = 0;
#endif
      // Tax
      dTax = 0;
      dollar2Num(apTokens[MB_SALES_TAXAMT+iFldDif], acTmp);
      if (acTmp[0] > '0')
      {
         dTax = atof(acTmp);

         // Calculate sale price
         lTmp = (long)(dTax * SALE_FACTOR);
         iTmp = ((int)dTax/100)*100;

         // Check for bad DocTax
         if (dTax > 100000)
         {
            if (iTmp == (int)dTax)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTax);
               lPrice = iTmp;
               dTax = 0;
            } else if (lPrice > 100000 && lPrice == (lPrice/100)*100)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTax);
            } else
            {
               LogMsg("??? (1) Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f, CP=%d.  Need investigation.  Ignore price.", SaleRec.Apn, SaleRec.DocNum, dTax, lPrice);
               lPrice = 0;
            }
         } else if (lPrice > 0 && (long)dTax >= lPrice+5000)
         {
            // Even though sale tax is questionable, we still rely on it if it is reasonable
            if (iTmp == (int)dTax)
            {
               LogMsg("*** (2) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTax);
               lPrice = iTmp;
               dTax = 0;
            } else
            {
               if (lPrice == (lPrice/100)*100)
                  LogMsg("*** (2) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTax);
               else
               {
                  LogMsg("??? (2) Questionable Sale Tax Amt for %.12s: DocNum=%.12s, Price=%d, \ttax=%.2f).  Need investigation.", SaleRec.Apn, SaleRec.DocNum, lPrice, dTax);
                  lPrice = lTmp;
               }
            }
         } else if (iTmp == (int)dTax && iTmp > 10000)
         {
            if (lTmp != (lTmp/100)*100)
            {
               LogMsg("*** (3) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTax);
               lPrice = iTmp;
               dTax = 0;
            } else
               lPrice = lTmp;
         } else if (lTmp == (lTmp/100)*100)
            lPrice = lTmp;
         else if (lTmp > 1000 && lPrice == 0)
            lPrice = lTmp;

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE+iFldDif] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Ignore sale price.", 
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTax, apTokens[MB_SALES_DOCCODE]);
               lPrice = 0;
            }
         }  
      } 

      // Save DocTax
      iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTax);
      memcpy(SaleRec.StampAmt, acTmp, iTmp);

      // Ignore sale price if less than 1000
      if (lPrice >= 10000)
         sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
      else if (lPrice >= 1000)
         sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
      else
         memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
      memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);

      // Doc code - accept following code only
      int iDocCode = 0;
      if (isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         if (pDocTbl)
         {
            iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
            if (iTmp >= 0)
            {
               memcpy(SaleRec.DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
               if (lPrice < 1000)
                  SaleRec.NoneSale_Flg = pDocTbl[iTmp].flag;
            } else if (bDebug)
               LogMsg("*** Unknown DocCode: %s", apTokens[MB_SALES_DOCCODE]);
         } else
         {
            iDocCode = atoi(apTokens[MB_SALES_DOCCODE]);
            if (iDocTypeFmt == 2)  // PLA
            {
               switch (iDocCode)
               {
                  case 1:  // GD, Transfer reappr
                  case 5:  // New mobile home                  
                     SaleRec.DocType[0] = '1';
                     break;
                  case 2:  // Partial Transfer
                  case 10: // Timeshare - reappr
                  case 11: // Transfer - no reappr
                  case 12: // Partial Transfer - no reappr
                  case 15: // Strawman transfer
                     memcpy(SaleRec.DocType, "75", 2);
                     break;
                  case 3:
                     // Foreclosure
                     memcpy(SaleRec.DocType, "77", 2);
                     break;
                  case 4:
                     // Sheriff's deed
                     memcpy(SaleRec.DocType, "25", 2);
                     break;
                  case 8:
                     // Tax deed
                     memcpy(SaleRec.DocType, "67", 2);
                     break;
                  default:
                     break;
               }
            } 
         }
      } else if (!memcmp(apTokens[MB_SALES_DOCCODE], "GD", 2))
         SaleRec.DocType[0] = '1';

      // Save original DocCode
      vmemcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);

      // Transfer Type
      if (*apTokens[MB_SALES_XFERTYPE+iFldDif] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE+iFldDif], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         strcpy(acTmp, apTokens[MB_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Seller1, acTmp, SALE_SIZ_SELLER, iTmp);

         // Buyer
         if (iFldDif > 0 && !memcmp(apTokens[MB_SALES_BUYER], "ET AL", 5))
         {
            strcpy(acTmp, apTokens[MB_SALES_BUYER+1]);
            iTmp = blankRem(acTmp);
         } else
         {
            strcpy(acTmp, apTokens[MB_SALES_BUYER]);
            iTmp = blankRem(acTmp);
         }
         vmemcpy(SaleRec.Name1, acTmp, SALE_SIZ_BUYER, iTmp);

         SaleRec.CRLF[0] = 10;
         SaleRec.CRLF[1] = 0;
         fputs((char *)&SaleRec,fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(20,1,C,EQ,\" \",OR,31,1,C,EQ,\" \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -2;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            // Save old cumsale file
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);
            iTmp = rename(acCSalFile, acTmpFile);
   
            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -2;
      } else
         iTmp = rename(acOutFile, acCSalFile);

      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acCSalFile, acTmpFile);
      }
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp)
   {
      iErrorCnt++;
      if (iTmp == -2)
         LogMsg("***** Error sorting output file");
      else
         LogMsg("***** Error renaming to %s", acCSalFile);
   }

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                          output: %d.", lTmp);
   LogMsg("         Latetest recording date: %d.", lLastRecDate);
   return iTmp;
}

/*********************************** loadPla ********************************
 *
 * Notes: 
 * 1) No need to extract lien.  Use certified value file instead.
 * 2) LDR 2009 has dropped all records without lien value.
 * 3) LDR 2010, PLA do not send lien value file.  Use current value file instead.
 *
 * Options:
 *    -CPLA -L -Xl -Xa -[Xs|Ms] (load lien)
 *    -CPLA -U -[Xsi|Ms] [-Mr] [-Mo] (load update)
 *
 ****************************************************************************/

int loadPla(int iSkip)
{
   int   iRet=0;
   char  acTmpFile[_MAX_PATH], acTmp[_MAX_PATH];

   iApnLen = myCounty.iApnLen;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      TC_SetDateFmt(MM_DD_YYYY_1);
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))          // Rebuild CSV
   {
      sprintf(acTmpFile, "%s\\Pla\\Pla_Asmt.srt", acTmpPath);
      LogMsg("Check lien file for rebuild: %s", acTmpFile);
      if (_access(acTmpFile, 0) || chkFileDate(acRollFile, acTmpFile) == 1)
      {
         sprintf(acTmp, "%s\\Pla\\Pla_Asmt.txt", acTmpPath);
         iRet = RebuildCsv_IQ(acRollFile, acTmp, cDelim, MB_ROLL_FLDS);
         if (iRet < 100000) 
            return -1;

         // Resort LDR file
         char  sCmd[256];
         sprintf(sCmd, "S(#1,C,A) OMIT(#1,C,LT,\"0\",OR,#1,C,GE,\"A\") DEL(%d) F(TXT)", cDelim);
         iRet = sortFile(acTmp, acTmpFile, sCmd);
         if (iRet < 100000) 
            return -2;
      } else
      {
         LogMsg("Use existing lien file: %s", acTmpFile);
         iRet = 0;
      }
      strcpy(acRollFile, acTmpFile);
   }

   // Extract lien file
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = Pla_ExtrLien(myCounty.acCntyCode);

   // Fix DocType if needed
   if (iLoadFlag & FIX_CSTYP)
   {
      iRet = FixDocType(acCSalFile, (IDX_TBL5 *)&PLA_DocCode[0], true /* remove rec w/o DocNum */);
      if (iRet)
         return iRet;
   }

   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Load old char layout.  If failed, reload using new version
      //iRet = Pla_ConvChar1(acCharFile);
      //if (iRet == 1)
      //   iRet = Pla_ConvChar2(acCharFile);
      //if (iRet <= 0)
      //{
      //   LogMsg("***** Error converting Char file %s", acCharFile);
      //   return -1;
      //}

      // Load Char file
      if (!_access(acCharFile, 0))
      {
         iRet = Pla_ConvStdChar(acCharFile);
         if (iRet <= 0)
         {
            LogMsg("***** Error converting Char file %s", acCharFile);
            return -1;
         }
      } else
      {
         LogMsg("*** WARNING: CHAR file does not exist: %s: ", acCharFile);
         LogMsg("    -Xa option is ignore.  Please verify input file");
      }

   }

   // Create/Update cum sale file
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Do not use confirmed sale price
      //bUseConfSalePrice = false;

      // 05/28/2020 - Append to existing cum sale file
      iRet = Pla_CreateSCSale(MMM_DD_YYYY, 2, 0, true, (IDX_TBL5 *)&PLA_DocCode[0]);
      // 01/03/2014
      //iRet = MB_CreateSCSale(MM_DD_YYYY_1, 2, 0, false, (IDX_TBL5 *)&PLA_DocCode[0]);
      //iRet = MB_CreateSCSale(MM_DD_YYYY_1, 2, 0);
      if (iRet)
         return iRet;

      iLoadFlag |= MERG_CSAL;
   }

   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      //if (!_access(acCharFile, 0))
      //{
      //   // Load old char layout.  If failed, reload using new version
      //   iRet = Pla_ConvChar1(acCharFile);
      //   if (iRet == 1)
      //      iRet = Pla_ConvChar2(acCharFile);
      //   if (iRet <= 0)
      //   {
      //      LogMsg("***** Error converting Char file %s", acCharFile);
      //      return -1;
      //   }
      //}

      if (iLoadFlag & LOAD_LIEN)
      {
         // Create Lien file
         LogMsg0("Load %s Lien file", myCounty.acCntyCode);
         iRet = Pla_Load_LDR(iSkip);
      } else if (iLoadFlag & LOAD_UPDT)
      {
         LogMsg0("Load %s roll update file", myCounty.acCntyCode);
         iRet = Pla_Load_Roll(iSkip);
      }
   }

   // Merge other values
   if (!iRet && bMergeOthers)
   {
      iRet = PQ_MergeLienExt(myCounty.acCntyCode, GRP_MB, iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   // Clean up
   if (!iRet && bClean)
   {
      char acTmpFile[_MAX_PATH];
      int  iTmp;

      if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
      {
         sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "?MP");
         iTmp = delFile(acTmpFile);
         if (iTmp)
            LogMsg("*** Cannot remove file %s", acTmpFile);
      }
      //if (iLoadFlag & (LOAD_ASSR|UPDT_ASSR))
      //{
      //   sprintf(acTmpFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "?MP");
      //   iTmp = delFile(acTmpFile);
      //   if (iTmp)
      //      LogMsg("*** Cannot remove file %s", acTmpFile);
      //}
   }

   return iRet;
}
