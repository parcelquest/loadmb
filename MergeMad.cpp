/**************************************************************************
 *
 * Notes: 
 *    - Resort situs & sale files
 *    - Even though input files are CSV, it may contain unprintable chars.
 *
 * Revision
 * 02/28/2007 1.9.2    Sort roll file.
 * 09/27/2007 1.9.17.1 Fix Mad_ConvertChar() and Mad_MergeChar() to fix Heating,
 *                     Cooling, and Pool problem.  Also modify PQLkup.txt table.
 * 01/25/2008 1.10.3.1 Add code to support standard usecode
 * 02/29/2008 1.10.5   Add code to output update records.
 * 07/22/2008 8.2.2.2  Adding Mad_MergeLien, Mad_Load_LDR(), Mad_MergeMAdr(), and
 *                     Mad_MergeSitus() to process 2008 LDR in TR601 format.
 * 03/30/2009 8.7.1    Use MB_MergeSale() to do sale update instead of MergeSale3() to fix 
 *                     problem that non-sale transaction overwrites sale transaction that  
 *                     occurs in the same day  since we only keep one transaction per day.
 * 05/21/2009 8.7.6    Remove reference to OldApn & NewApn files.  We no longer need them.
 * 07/23/2009 9.1.3    Use replNull() instead of replChar() to replace NULL char in 
 *                     TR061 file.  MAD is now using NAPA layout.
 * 08/10/2009 9.1.6    Set LDR to group 1 and use L2_* definition group along with MAD & SBT.
 * 08/24/2010 10.3.3   County has changed LDR file names and format. Change to MB group 2.
 *                     Save EXE_CD and TAXCODE to R01 record. Create cumsale file.
 *                     Overwrite EXE_TOTAL with GROSS if it is greater than GROSS.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 05/23/2011 10.8.2   Fix bad character in Mad_MergeOwner().
 * 06/07/2011 10.9.1   Exclude HomeSite from OtherVal.  Replace Mad_MergeExe() with MB_MergeExe()
 *                     and Mad_MergeTax() with MB_MergeTax(). Use new MB_CreateSCSale().
 * 07/24/2012 12.2.3   Fix Mad_MergeMAdr() to merge CAREOF correctly.
 * 10/04/2012 12.3.1   Use ApplyCumSale() to update roll.
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 02/04/2014 13.20.2  Modify Mad_MergeOwner() to keep Name1 as it appears in county file.
 * 07/15/2014 14.0.3   Modify ConvertChar() to output cumchar file to RawFile folder. Also modify
 *                     Load_LDR() and Load_Roll() to use cumchar file.
 * 06/07/2015 14.16.1  Add Mad_ExtrValues() and Mad_ParseValues() to support -Xv for MAD.
 * 06/15/2015 15.0.1   Modify Mad_ExtrValues() to sort output file.
 * 07/31/2015 15.2.0   Add DBA to Mad_MergeMAdr(), rewrite Mad_ParseValues() to support new layout.
 *                     Add ValueHdr to INI file for number of skip hdr records.
 * 11/17/2015 15.6.3   Add Mad_ConvStdChar() and Mad_MergeStdChar() to support new CHAR file format.
 *                     Modify Mad_MergeMAdr() and Mad_MergeRoll() to use address lines for mailing.
 * 01/04/2016 15.9.0   Add option to load TC file by calling Load_TC()
 * 01/13/2016 15.10.1  Remove old situs in Cal_MergeSitus() before update to avoid remnant from old addr.
 * 07/17/2016 16.0.5   Add Mad_Load_LDR3() to support new LDR format.
 * 07/29/2017 17.2.2   Add Mad_ParseValues3() for 2017 value file format.
 * 03/27/2018 17.9.0   Add -Ut option to update tax.
 * 07/04/2018 18.0.1   Modify Mad_MergeSitus() to add "COVE" to StrName (i.e. DE ANN COVE)
 * 07/17/2019 19.0.4   Add AgPreserve and fix LotSize in Mad_MergeLien3().
 * 12/13/2019 19.6.4   Modify Mad_MergeRoll() to format DocNum as YYYYR9999999.
 * 04/27/2020 19.9.0   Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 04/29/2020 19.9.2   Modify loadMad(), Mad_Load_Roll() to work around formatted dollar amount.
 *                     Clean up MergeOwner() to keep original owner as is.
 * 06/14/2020 19.10.2  Add -Xn & -Mn options
 * 11/01/2020 20.4.2   Modify Mad_MergeRoll() to populate default PQZoning.
 * 07/02/2021 21.0.5   Add QualityClass to R01 in Mad_MergeStdChar().
 * 09/09/2021 21.2.1   Modify Mad_ConvStdChar() to fix QualityClass.
 * 03/12/2022 21.6.2   Modify program to support new data format which includes embeded null in record.
 *                     Modify Mad_ConvStdChar() to add ParkSpace (even though only 4 records populated).
 * 03/15/2022 21.7.1.1 Add exception to overwrite bad data in Mad_MergeRoll().
 * 07/16/2022 22.0.2   Change -Xv option to use MB_ExtrValues() instead of Mad_ExtrValues().
 *                     Comment out Mad_ExtrValues() and related functions.
 * 11/08/2022 22.2.8   Modify Mad_MergeMAdr() to remove leading spaces before parsing.
 * 06/28/2023 23.0.3   Use Mad_Value.csv likes we do in 2022. Don't use ValueNoticeExports.*
 * 07/04/2023 23.0.4   Modify loadMad() to force processing -Xl before -Xv since -Xv needs output from -Xl.
 * 07/01/2024 24.0.0   Bug fix in load_Mad() when using -Xv with -Xl at the same time.
 *                     Modify Mad_MergeLien3() to add ExeType.
 * 09/27/2024 24.1.6   Modify Merge_Roll() to fix bad ZONING.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "CharRec.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "PQ.h"

// Use NAPA format for lien roll
#define _NAPA_
#include "LoadMB.h"
#include "MB_Value.h"
#include "LoadValue.h"
#include "MergeMad.h"
#include "Tax.h"
#include "NdcExtr.h"

/*****************************************************************************
 * Value Set Type:
 *  1  Enrolled
 *  2  Adjusted Base for Enrolled
 *  3  Base Year
 *  4  Adj Base for Base Year
 *  5  Restricted
 *  8  Prop 8
 *  11 Future Enrolled
 *  12 Future Adjusted Base for Enrolled
 *  13 Future Base Year
 *  14 Future Adj Base for Base Year
 *  15 Future Restricted
 *  18 Future Prop 8
 *  52 CLCA Non-Renewal
 *  53 CLCA 423.3
 *  54 CLCA FSZ (423.4)
 *  55 CLCA 
 *  96 Timber Value
 *
 * Do not use this version
 *    - "G:\CO_DATA\MAD\2023\ValueNotices Prop 8 to base .txt" 
 *    - "G:\CO_DATA\MAD\2023\Value Notices Prop 8 only.txt"  
 *    - "G:\CO_DATA\MAD\2023\Value Notices.txt" 
 * Use ValueNoticeExports.ots to merge them into ValueNoticeExports.srt before processing
 *
 ****************************************************************************/

//int Mad_ParseValues(LPSTR pInbuf, LPSTR pOutbuf, FILE *fd)
//{
//   VALUE_REC   *pVal = (VALUE_REC *)pOutbuf;
//   int         iRet = 0, lVal, lLand, lImpr, lOther;
//   char        sTmp[32];
//
//#ifdef _DEBUG
//   //if (!memcmp(pInbuf, "075250001000", 9))
//   //   iRet = 0;
//#endif
//
//   // Parse input rec
//   iTokens = ParseStringIQ(pInbuf, cValDelim, 64, apTokens);
//
//   if (iTokens >= PV_SITUS2 && *apTokens[PV_VST] > ' ')
//   {
//      memset(pOutbuf, ' ', sizeof(VALUE_REC));
//      memcpy(pVal->Apn, apTokens[PV_APN], strlen(apTokens[PV_APN]));
//      iRet = atol(apTokens[PV_VST]);
//      switch (iRet)
//      {
//         case 3:
//            pVal->ValueSetType[0] = '2';
//            break;
//         case 1:
//         case 8:
//            pVal->ValueSetType[0] = *apTokens[PV_VST];
//            break;
//         default:
//            pVal->ValueSetType[0] = '9';  // Others
//      }
//
//      // Set reason
//      if (!_memicmp(apTokens[PV_ENROLLED], "PROP8", 5))
//         pVal->Reason[0] = '8';
//      else
//         memcpy(pVal->Reason, apTokens[PV_VST], strlen(apTokens[PV_VST]));
//
//      memcpy(pVal->TaxYear, myCounty.acYearAssd, 4);
//      lLand = atol(apTokens[PV_LAND]);
//      if (lLand > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lLand);
//         memcpy(pVal->Land, sTmp, iRet);
//      }
//
//      lImpr = atol(apTokens[PV_IMPR]);
//      if (lImpr > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lImpr);
//         memcpy(pVal->Impr, sTmp, iRet);
//      }
//
//      lVal = atol(apTokens[PV_GROWIMPR]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->GrowImpr, sTmp, iRet);
//      }
//      lOther = lVal;
//
//      lVal = atol(apTokens[PV_FIXTR]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->Fixtr, sTmp, iRet);
//         lOther += lVal;
//      }
//
//      lVal = atol(apTokens[PV_FIXTR_RP]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->Fixtr_RP, sTmp, iRet);
//      }
//      
//      lVal = atol(apTokens[PV_PPBUSINESS]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->Businv, sTmp, iRet);
//         lOther += lVal;
//      }
//
//      lVal = atol(apTokens[PV_PP_MH]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->PP_MH, sTmp, iRet);
//         lOther += lVal;
//      }
//
//      if (lOther > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lOther);
//         memcpy(pVal->Other, sTmp, iRet);
//      }
//
//      // Gross
//      lVal = lOther + lLand + lImpr;
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->TotalVal, sTmp, iRet);
//      }
//
//      // Net
//      dollar2Num(apTokens[PV_NETVALUE], sTmp);
//      lVal = atol(sTmp);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->NetVal, sTmp, iRet);
//      }
//
//      // Exemption
//      lVal = atol(apTokens[PV_EXE_AMT]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->Exe, sTmp, iRet);
//      }
//      
//      pVal->CRLF[0] = '\n';
//      pVal->CRLF[1] = 0;
//      fputs(pOutbuf, fd);
//      iRet = 1;
//   } else
//   {
//      LogMsg("*** Bad input record: %s", pInbuf);
//      iRet = -1;
//   }
//
//   return iRet;
//}

/*****************************************************************************
 * Value Set Type:
 *  1  Enrolled:   Only show the Enrolled column
 *  3  Base Year:  Show both Enrolled and Base Year columns
 *  5  Restricted: This one is like an other type so we should probably just show the Enrolled column
 *  8  Prop 8:     Show all three columns (Enrolled, Base Year, Prop 8) 
 *
 * Processing notes:
 *  1 : Create one record with VST = 1
 *  3 : Create 2 records with VST=1 & VST=2
 *  5 : Create upto 3 records with VST = 1, 2, 9
 *  8 : Create upto 3 records with VST = 1, 2, 8
 *
 * This version is for 2015-2016
 *
 * Return -1 if bad record.  0 otherwise.
 *
 ****************************************************************************/

//int Mad_ParseValues2(LPSTR pInbuf, LPSTR pOutbuf, FILE *fdOut)
//{
//   VALUE_REC   *pVal = (VALUE_REC *)pOutbuf;
//   int         iRet, lVal, lLand, lImpr, lOther;
//   int         iCnt = 0;
//   char        sTmp[32];
//   bool        bBaseYear, bProp8, bRestricted;
//
//#ifdef _DEBUG
//   //if (!memcmp(pInbuf, "075250001000", 9))
//   //   iRet = 0;
//#endif
//
//   // Parse input rec
//   iTokens = ParseStringNQ(pInbuf, ',', 64, apTokens);
//   if (iTokens < PV2_APRCODE)
//   {
//      LogMsg("*** Bad input record: %s", pInbuf);
//      return -1;
//   }
//
//   // Init
//   bBaseYear=bProp8=bRestricted = false;
//   memset(pOutbuf, ' ', sizeof(VALUE_REC));
//
//   // Populate APN
//   iRet = remChar(apTokens[PV2_APN], '-');
//   memcpy(pVal->Apn, apTokens[PV2_APN], iRet);
//
//   // Determine which record to create
//   iRet = atol(apTokens[PV2_VST]);
//   switch (iRet)
//   {
//      case 3:
//         bBaseYear = true;
//         break;
//      case 5:
//         bRestricted = true;
//         if (*apTokens[PV2_BASEVALUEEXISTS] == '1')
//            bBaseYear = true;
//         break;
//      case 1:
//          break;
//      case 8:  // Base year value may not be present 064-071-040-000, 064-071-041-000
//         bProp8 = true;
//         if (*apTokens[PV2_BASEVALUEEXISTS] == '1')
//            bBaseYear = true;
//         break;
//      default:
//         LogMsg("***** Invalid VST: %s", apTokens[PV2_VST]);
//   }
//
//   // Keep original reason
//   memcpy(pVal->Reason, apTokens[PV2_VST], strlen(apTokens[PV2_VST]));
//   pVal->ValueSetType[0] = '1';
//
//   memcpy(pVal->TaxYear, myCounty.acYearAssd, 4);
//   lLand = atol(apTokens[PV2_LAND]);
//   if (lLand > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lLand);
//      memcpy(pVal->Land, sTmp, iRet);
//   }
//
//   lImpr = atol(apTokens[PV2_STRUCTURE]);
//   if (lImpr > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lImpr);
//      memcpy(pVal->Impr, sTmp, iRet);
//   }
//
//   lVal = atol(apTokens[PV2_GROWING]);
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->GrowImpr, sTmp, iRet);
//   }
//   lOther = lVal;
//
//   lVal = atol(apTokens[PV2_FIXTURES]);
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->Fixtr, sTmp, iRet);
//      lOther += lVal;
//   }
//
//   lVal = atol(apTokens[PV2_FIXRP]);
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->Fixtr_RP, sTmp, iRet);
//   }
//      
//   lVal = atol(apTokens[PV2_PP]);
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->PersProp, sTmp, iRet);
//      lOther += lVal;
//   }
//
//   lVal = atol(apTokens[PV2_PPMH]);
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->PP_MH, sTmp, iRet);
//      lOther += lVal;
//   }
//
//   if (lOther > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lOther);
//      memcpy(pVal->Other, sTmp, iRet);
//   }
//
//   // Gross
//   lVal = lOther + lLand + lImpr;
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->TotalVal, sTmp, iRet);
//   }
//
//   // Net
//   dollar2Num(apTokens[PV2_NETVALUE], sTmp);
//   lVal = atol(sTmp);
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->NetVal, sTmp, iRet);
//   }
//
//   // Exemption
//   lVal = atol(apTokens[PV2_EXEMP]);
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->Exe, sTmp, iRet);
//   }
//
//   pVal->CRLF[0] = '\n';
//   pVal->CRLF[1] = 0;
//   fputs(pOutbuf, fdOut);
//   iCnt++;
//
//   if (bBaseYear)
//   {
//      memset(&pVal->ValueSetType[0], ' ', sizeof(VALUE_REC)-PV_SIZ_APN);
//      memcpy(pVal->TaxYear, myCounty.acYearAssd, 4);
//
//      pVal->ValueSetType[0] = '2';
//      lLand = atol(apTokens[PV2_BASELAND]);
//      if (lLand > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lLand);
//         memcpy(pVal->Land, sTmp, iRet);
//      } 
//
//      lImpr = atol(apTokens[PV2_BASESTRUCTURE]);
//      if (lImpr > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lImpr);
//         memcpy(pVal->Impr, sTmp, iRet);
//      } 
//
//      lVal = atol(apTokens[PV2_BASEGROWING]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->GrowImpr, sTmp, iRet);
//      }
//
//      lOther = lVal;
//      lVal = atol(apTokens[PV2_BASEFIXTURES]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->Fixtr, sTmp, iRet);
//         lOther += lVal;
//      } 
//
//      lVal = atol(apTokens[PV2_BASEFIXRP]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->Fixtr_RP, sTmp, iRet);
//      } 
//      
//      lVal = atol(apTokens[PV2_BASEPP]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->PersProp, sTmp, iRet);
//         lOther += lVal;
//      } 
//
//      lVal = atol(apTokens[PV2_BASEPPMH]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->PP_MH, sTmp, iRet);
//         lOther += lVal;
//      } else
//         memset(pVal->PP_MH, ' ', PV_SIZ_LAND);
//
//      // Gross
//      lVal = lOther + lLand + lImpr;
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->TotalVal, sTmp, iRet);
//      } 
//
//      // Net
//      dollar2Num(apTokens[PV2_BASENETVALUE], sTmp);
//      lVal = atol(sTmp);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->NetVal, sTmp, iRet);
//      } 
//
//      pVal->CRLF[0] = '\n';
//      pVal->CRLF[1] = 0;
//      fputs(pOutbuf, fdOut);
//      iCnt++;
//   }
//
//   if (bProp8)
//   {
//      memset(&pVal->ValueSetType[0], ' ', sizeof(VALUE_REC)-PV_SIZ_APN);
//      memcpy(pVal->TaxYear, myCounty.acYearAssd, 4);
//
//      pVal->ValueSetType[0] = '8';
//      lLand = atol(apTokens[PV2_PROP8LAND]);
//      if (lLand > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lLand);
//         memcpy(pVal->Land, sTmp, iRet);
//      }
//
//      lImpr = atol(apTokens[PV2_PROP8STRUCTURE]);
//      if (lImpr > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lImpr);
//         memcpy(pVal->Impr, sTmp, iRet);
//      } 
//
//      lVal = atol(apTokens[PV2_PROP8GROWING]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->GrowImpr, sTmp, iRet);
//      } 
//
//      lOther = lVal;
//      lVal = atol(apTokens[PV2_PROP8FIXTURES]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->Fixtr, sTmp, iRet);
//         lOther += lVal;
//      } 
//
//      lVal = atol(apTokens[PV2_PROP8FIXRP]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->Fixtr_RP, sTmp, iRet);
//      }
//      
//      lVal = atol(apTokens[PV2_PROP8PP]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->PersProp, sTmp, iRet);
//         lOther += lVal;
//      } 
//
//      lVal = atol(apTokens[PV2_PROP8PPMH]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->PP_MH, sTmp, iRet);
//         lOther += lVal;
//      } 
//
//      // Gross
//      lVal = lOther + lLand + lImpr;
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->TotalVal, sTmp, iRet);
//      } 
//
//      // Net
//      dollar2Num(apTokens[PV2_PROP8NETVALUE], sTmp);
//      lVal = atol(sTmp);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->NetVal, sTmp, iRet);
//      } 
//
//      pVal->CRLF[0] = '\n';
//      pVal->CRLF[1] = 0;
//      fputs(pOutbuf, fdOut);
//      iCnt++;
//   }
//
//   if (bRestricted)
//   {
//      memset(&pVal->ValueSetType[0], ' ', sizeof(VALUE_REC)-PV_SIZ_APN);
//      memcpy(pVal->TaxYear, myCounty.acYearAssd, 4);
//
//      pVal->ValueSetType[0] = '9';
//      lLand = atol(apTokens[PV2_RESLAND]);
//      if (lLand > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lLand);
//         memcpy(pVal->Land, sTmp, iRet);
//      } 
//
//      lImpr = atol(apTokens[PV2_RESSTRUCTURE]);
//      if (lImpr > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lImpr);
//         memcpy(pVal->Impr, sTmp, iRet);
//      } 
//
//      lVal = atol(apTokens[PV2_RESGROWING]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->GrowImpr, sTmp, iRet);
//      } 
//
//      lOther = lVal;
//      lVal = atol(apTokens[PV2_RESFIXTURES]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->Fixtr, sTmp, iRet);
//         lOther += lVal;
//      } 
//
//      lVal = atol(apTokens[PV2_RESFIXRP]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->Fixtr_RP, sTmp, iRet);
//      } 
//      
//      lVal = atol(apTokens[PV2_RESPP]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->PersProp, sTmp, iRet);
//         lOther += lVal;
//      } 
//
//      lVal = atol(apTokens[PV2_RESPPMH]);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->PP_MH, sTmp, iRet);
//         lOther += lVal;
//      } 
//
//      // Gross
//      lVal = lOther + lLand + lImpr;
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->TotalVal, sTmp, iRet);
//      } 
//
//      // Net
//      dollar2Num(apTokens[PV2_RESNETVALUE], sTmp);
//      lVal = atol(sTmp);
//      if (lVal > 0)
//      {
//         iRet = sprintf(sTmp, "%d", lVal);
//         memcpy(pVal->NetVal, sTmp, iRet);
//      }
//
//      pVal->CRLF[0] = '\n';
//      pVal->CRLF[1] = 0;
//      fputs(pOutbuf, fdOut);
//      iCnt++;
//   }
//
//   return iCnt;
//}

/*****************************************************************************
 * Value Set Type:
 *  1  Enrolled
 *  3  Base Year
 *  5  Restricted
 *
 * Processing notes:
 *  1 : Create one record with VST = 1.  If Enrolled="PROP8", add 1 rec with VST=8
 *  3 : Create 2 records with VST=1 & VST=2
 *  5 : Create upto 3 records with VST = 1, 2, 9
 *  8 : Create upto 3 records with VST = 1, 2, 8
 *
 *****************************************************************************/

//int Mad_ParseValues3(LPSTR pInbuf, LPSTR pOutbuf, FILE *fdOut)
//{
//   VALUE_REC   *pVal = (VALUE_REC *)pOutbuf;
//   int         iRet, lVal, lLand, lImpr, lOther;
//   int         iCnt = 0;
//   char        sTmp[32];
//   bool        bBaseYear, bProp8, bRestricted, bEnrolled;
//
//#ifdef _DEBUG
//   //if (!memcmp(pInbuf, "075250001000", 9))
//   //   iRet = 0;
//#endif
//
//   // Parse input rec
//   iTokens = ParseStringIQ(pInbuf, 9, 64, apTokens);
//   if (iTokens < PV_SITUS2 || *apTokens[PV_VST] == ' ')
//   {
//      LogMsg("*** Bad input record: %s", pInbuf);
//      return -1;
//   }
//
//   // Init
//   bBaseYear=bProp8=bRestricted= false;
//   bEnrolled = true;
//   memset(pOutbuf, ' ', sizeof(VALUE_REC));
//
//   // Populate APN
//   memcpy(pVal->Apn, apTokens[PV_APN], strlen(apTokens[PV_APN]));
//
//   // Determine which record to create
//   iRet = atol(apTokens[PV_VST]);
//   switch (iRet)
//   {
//      case 3:
//         bBaseYear = true;
//         if (!_memicmp(apTokens[PV_ENROLLED], "REST", 4))
//            bRestricted = true;
//         else
//            bEnrolled = false;
//         break;
//      case 5:
//         bRestricted = true;
//         break;
//      case 1:
//         if (!_memicmp(apTokens[PV_ENROLLED], "PROP8", 5))
//            bProp8 = true;
//         break;
//      default:
//         LogMsg("***** Invalid VST: %s", apTokens[PV_VST]);
//   }
//
//   if (bEnrolled)
//   {
//      // Set reason
//      if (bProp8)
//         pVal->Reason[0] = '8';
//      else
//         pVal->Reason[0] =  *apTokens[PV_VST];
//      pVal->ValueSetType[0] = '1';
//   }
//
//   memcpy(pVal->TaxYear, myCounty.acYearAssd, 4);
//   lLand = atol(apTokens[PV_LAND]);
//   if (lLand > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lLand);
//      memcpy(pVal->Land, sTmp, iRet);
//   }
//
//   lImpr = atol(apTokens[PV_IMPR]);
//   if (lImpr > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lImpr);
//      memcpy(pVal->Impr, sTmp, iRet);
//   }
//
//   lVal = atol(apTokens[PV_GROWIMPR]);
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->GrowImpr, sTmp, iRet);
//   }
//   lOther = lVal;
//
//   lVal = atol(apTokens[PV_FIXTR]);
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->Fixtr, sTmp, iRet);
//      lOther += lVal;
//   }
//
//   lVal = atol(apTokens[PV_FIXTR_RP]);
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->Fixtr_RP, sTmp, iRet);
//      lOther += lVal;
//   }
//      
//   lVal = atol(apTokens[PV_PPBUSINESS]);
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->PersProp, sTmp, iRet);
//      lOther += lVal;
//   }
//
//   lVal = atol(apTokens[PV_PP_MH]);
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->PP_MH, sTmp, iRet);
//      lOther += lVal;
//   }
//
//   if (lOther > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lOther);
//      memcpy(pVal->Other, sTmp, iRet);
//   }
//
//   // Gross
//   lVal = lOther + lLand + lImpr;
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->TotalVal, sTmp, iRet);
//   }
//
//   // Net
//   lVal = atol(apTokens[PV_NETVALUE]);
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->NetVal, sTmp, iRet);
//   }
//
//   // Exemption
//   lVal = atol(apTokens[PV_EXE_AMT]);
//   if (lVal > 0)
//   {
//      iRet = sprintf(sTmp, "%d", lVal);
//      memcpy(pVal->Exe, sTmp, iRet);
//   }
//
//   pVal->CRLF[0] = '\n';
//   pVal->CRLF[1] = 0;
//   if (bEnrolled)
//   {
//      fputs(pOutbuf, fdOut);
//      iCnt++;
//   } else
//      pVal->Reason[0] =  *apTokens[PV_VST];
//
//   if (bBaseYear)
//   {
//      pVal->ValueSetType[0] = '2';
//      fputs(pOutbuf, fdOut);
//      iCnt++;
//   }
//
//   if (bProp8)
//   {
//      pVal->ValueSetType[0] = '8';
//      fputs(pOutbuf, fdOut);
//      iCnt++;
//   }
//
//   if (bRestricted)
//   {
//      pVal->ValueSetType[0] = '9';
//      fputs(pOutbuf, fdOut);
//      iCnt++;
//   }
//
//   return iCnt;
//}

/******************************************************************************
 *
 * Extract base year values from value file for MAD
 *
 ******************************************************************************/

//int Mad_ExtrValues(LPCSTR pCnty, LPSTR pValueFile, LPSTR pOutFile, int iSkipRows)
//{
//   int   lCnt, lOut, iRet, iTmp;
//   char  sInbuf[4096], sOutbuf[1024], sTmpFile[_MAX_PATH], *pTmp;
//   FILE  *fdIn, *fdOut;
//
//   LogMsg0("Extract base year values from Value file for %.3s", pCnty);
//
//   // Open input
//   LogMsg("Opening ... %s", pValueFile);
//   fdIn = fopen(pValueFile, "r");
//   if (!fdIn)
//   {
//      LogMsg("***** Error opening %s (%d)", pValueFile, _errno);
//      return -1;
//   }
//
//   // Create output file
//   sprintf(sTmpFile, acRawTmpl, pCnty, pCnty, "vmp");
//   fdOut = fopen(sTmpFile, "w");
//   if (!fdOut)
//   {
//      LogMsg("***** Error creating %s (%d)", sTmpFile, _errno);
//      return -2;
//   }
//
//   // Skip header
//   for (iTmp = 0; iTmp < iSkipRows; iTmp++)
//      pTmp = fgets(sInbuf, 1024, fdIn);
//
//   lCnt=lOut=0;
//   // Loop through it
//   while (!feof(fdIn))
//   {
//      if (!(pTmp = fgets(sInbuf, 1024, fdIn)))
//         break;
//
//      if (isdigit(sInbuf[0]))
//      {
//         iRet = replNull(sInbuf);
//         if (lLienYear == 2022)
//            iRet = Mad_ParseValues(sInbuf, sOutbuf, fdOut);
//         else if (lLienYear == 2015 || lLienYear == 2016)
//            iRet = Mad_ParseValues2(sInbuf, sOutbuf, fdOut);
//         else if (lLienYear == 2023)
//            iRet = Mad_ParseValues(sInbuf, sOutbuf, fdOut);
//         else if (lLienYear >= 2017 && lLienYear < 2022)
//            iRet = Mad_ParseValues3(sInbuf, sOutbuf, fdOut);
//         else 
//            iRet = Mad_ParseValues(sInbuf, sOutbuf, fdOut);
//         if (iRet > 0)
//         {
//            lOut += iRet;
//         }
//      }
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   // CLosing
//   fclose(fdIn);
//   fclose(fdOut);
//
//   // Sort output
//   if (lOut > 100)
//   {
//      LogMsg("Sorting Value file %s --> %s", sTmpFile, pOutFile);
//      iRet = sortFile(sTmpFile, pOutFile, "S(1,12,C,A)");
//   } else
//   {
//      printf("\n");
//      iRet = 0;
//   }
//
//   LogMsg("Number of records processed: %d", lCnt);
//   LogMsg("Number of records output:    %d", iRet);
//
//   return iRet;
//}

/******************************** Mad_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Mad_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iRet;
   char  acName1[64], acName2[64], acOwners[64], acTmp[128], *pTmp, *pTmp1;
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001041009", 9))
   //   iTmp = 0;
#endif

   pTmp = strcpy(acTmp, pNames);

   // Check for bad character
   if (iTmp = remJSChar(acTmp))
      LogMsg0("*** Bad character in Owner1 at %.12s", pOutbuf);

   // Remove multiple spaces
   blankRem(acTmp);
   acName2[0] = 0;
   
   if (pTmp=strstr(acTmp, " 1/2 INT"))
      *pTmp = 0;
   
   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Check for year that goes before TRUST
   iTmp =0;   
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME1);
      return;
   }

   // Filter out words, things in parenthesis 
   if (pTmp = strchr(acTmp, '(')) 
      *pTmp = 0;

   // Remove alias
   if ((pTmp=strstr(acTmp, " AS ")) || (pTmp=strstr(acTmp, " AKA ")) )
      *pTmp = 0;

   // Save owner
   strcpy(acOwners, acTmp);
   vmemcpy(pOutbuf+OFF_NAME1, acOwners, SIZ_NAME1);

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
      // If TRUST appears after number, save from number forward
      if (strstr((char *)&acTmp[iTmp], " TRUST") )
      {
         iTmp--;
         acTmp[iTmp] = 0;
      }
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
   } 

   // MONDANI NELLIE M ESTATE OF
   if ( (pTmp=strstr(acTmp, " TRUSTE"))  ||
      (pTmp=strstr(acTmp, " ESTATE OF")) || (pTmp=strstr(acTmp, " LIFE ESTATE")) ||
      (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTS OF"))  || (pTmp=strstr(acTmp, " TR")) ||
     (pTmp=strstr(acTmp, " TR OF")) || (pTmp=strstr(acTmp, " ET AL"))  || (pTmp=strstr(acTmp, " ETAL"))  ||
     (pTmp=strstr(acTmp, " TRET AL")) || (pTmp=strstr(acTmp, " FAMILY")) ||
     (pTmp=strstr(acTmp, " LIVING TR")))
   {
      *pTmp = 0;
   }

   if (pTmp=strstr(acTmp, " CO-TR"))
   {
      *pTmp = 0;
   }

   if ((pTmp=strstr(acTmp, " SUCCS-TR")) || (pTmp=strstr(acTmp, " SUCCESSOR")) || 
      (pTmp=strstr(acTmp, " SURVIVOR")) )
   {
      *pTmp = 0; 
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY TRUST &"))
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      // RUMAN RICKY L & RUMAN FAMILY TRUST &
      // RUMAN RICKY L & NANCY FAMILY TRUST & MARIE
      // VERMETTE JAMES & THERESA A & ZENDER ROBERT A &
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST &"))
   {  // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE  
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      *(pTmp+6) = 0;
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
         *pTmp1 = 0;
      else
         *pTmp = 0;

      strcpy(acName1, acTmp);
      strcpy(acName2, pTmp+9);
   } else if (pTmp=strstr(acTmp, " LAND TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " FAMILY "))
             || (pTmp=strstr(acTmp, " INCOME TR")) || (pTmp=strstr(acTmp, " LIVING TR")) )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " ESTATE OF"))
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
         strcpy(acName2, pTmp);
      }
   }

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      // If name is not swapable, use Name1 instead
      if (iRet == -1)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwners, SIZ_NAME_SWAP);
      else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
   {
      // Couldn't split names
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwners, SIZ_NAME_SWAP);
   }
}

/******************************** Mad_MergeMAdr ******************************
 *
 * Merge Mail address, Mail addr might contains double quote like ""C"" ST.
 * Remove it before merging.
 *
 *****************************************************************************/

void Mad_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64];
   int      iTmp;
   char *    pTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      iTmp = strlen(pTmp);
      if (iTmp > SIZ_DBA)
         iTmp = SIZ_DBA;
      memcpy(pOutbuf+OFF_DBA, pTmp, iTmp);
   } 

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001015013", 9))
   //   iTmp = 0;
#endif

   // Parse mail address
   parseMAdr1_1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
      sMailAdr.strName[SIZ_M_STREET] = 0;
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      memcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], strlen(apTokens[MB_ROLL_M_CITY]));
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         iTmp = strlen(apTokens[MB_ROLL_M_ZIP]);
         if (iTmp > 9)
            iTmp = 9;
         memcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], iTmp);
      }

      iTmp = sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
      if (iTmp > SIZ_M_CTY_ST_D)
         iTmp = SIZ_M_CTY_ST_D;
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, iTmp);
   }

}

/******************************** Mad_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Mad_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4, char *pCareOf, char *pDba)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030260041", 9))
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   // Remove leading space
   myLTrim(pLine1);
   myLTrim(pLine2);
   myLTrim(pLine3);
   myLTrim(pLine4);

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   p0 = NULL;
   if (pLine4 && *pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         // Update DBA
         vmemcpy(pOutbuf+OFF_DBA, pLine1+4, SIZ_DBA);
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         // Update DBA
         strcpy(acTmp, pLine1+4);
         vmemcpy(pOutbuf+OFF_DBA, acTmp, SIZ_DBA);
         p1 = pLine2;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      _strupr(p0);
      updateCareOf(pOutbuf, p0, strlen(p0));
   } else if (pCareOf && *pCareOf > ' ' && memcmp(pCareOf, "DBA", 3))
   {
      _strupr(pCareOf);
      updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
   } 
   
   if (*(pOutbuf+OFF_DBA) == ' ' && pDba && *pDba > ' ')
      vmemcpy(pOutbuf+OFF_DBA, pDba+4, SIZ_DBA);

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      iTmp = blankRem(acAddr1, SIZ_M_ADDR_D);
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D, iTmp);

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
         if (sMailAdr.strSfx[0] > '0')
            vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      } else
      {
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
      }
   }

   strcpy(acAddr2, p2);
   if (pTmp = strstr(&acAddr2[2], "NEW YORK"))
   {
      replStr(pTmp, "NEW YORK", "NY");
   }
   iTmp = blankRem(acAddr2);

   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D, iTmp);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
   }
}

void Mad_MergeMAdr1(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "056400065", 9) || !memcmp(pOutbuf, "007113047", 9))
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
         {
            p1 = pLine1;
         } else
         {
            // If last word of line 3 is number and first word of line 1 is alpha, 
            // line 1 more likely be CareOf
            p0 = pLine1;
            p1 = pLine2;
         }
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));

      /*
      if (!_memicmp(p0, "C/O", 3))
         pTmp = p0+4;
      else if (!_memicmp(p0, "ATTN", 4))
         pTmp = p0+5;
      else if (*p0 == '%')
         pTmp = p0+1;
      else
         pTmp = p0;

      while (*pTmp == ' ')
         pTmp++;
      iTmp = strlen(pTmp);
      if (iTmp > SIZ_CARE_OF)
         iTmp = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, pTmp, iTmp);
      */
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      } else
      {
         iTmp = strlen(acAddr1);
         if (iTmp > SIZ_M_STREET)
            iTmp = SIZ_M_STREET;
         memcpy(pOutbuf+OFF_M_STREET, acAddr1, iTmp);
      }
   }

   strcpy(acAddr2, p2);
   blankRem(acAddr2);

   iTmp = strlen(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D)
      iTmp = SIZ_M_CTY_ST_D;
   memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);

   parseAdr2(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      iTmp = strlen(sMailAdr.City);
      if (iTmp > SIZ_M_CITY) iTmp = SIZ_M_CITY;
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, iTmp);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      iTmp = strlen(sMailAdr.Zip);
      if (iTmp > SIZ_M_ZIP)
         iTmp = SIZ_M_ZIP;
      memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, iTmp);

      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);

   }
}

/******************************** Mad_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Mad_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 512, fdSitus);
   }

   do 
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF 
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 0;

   if (pRec)
   {
      replNull(pRec);
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   }
   
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old Situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
#ifdef _DEBUG
      if (isCharIncluded(apTokens[MB_SITUS_STRNUM], '-', 0))
         lRecCnt++;
#endif
      // Save original StrNum
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      sprintf(acTmp, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
      pTmp = strchr(acTmp, ' ');
      *(pTmp+1) = 0;
      strcpy(acAddr1, acTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
      }
   }

   // Check StrSfx CODE
   if (!strcmp(apTokens[MB_SITUS_STRTYPE], "COVE"))
   {
      iTmp = sprintf(acTmp, "%s COVE", apTokens[MB_SITUS_STRNAME]);
      *apTokens[MB_SITUS_STRTYPE] = 0;
   } else
   {
      strcpy(acTmp, apTokens[MB_SITUS_STRNAME]);
      iTmp = strlen(apTokens[MB_SITUS_STRNAME]);
   }

   // StrName
   memcpy(pOutbuf+OFF_S_STREET, acTmp, iTmp);
   strcat(acAddr1, acTmp);

   // StrSfx
   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);

      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
         Sfx2Code(acTmp, acCode);
      else
      {
         LogMsg0("*** Invalid suffix: %s (StrName=%s) APN=%s", apTokens[MB_SITUS_STRTYPE], apTokens[MB_SITUS_STRNAME], apTokens[MB_SITUS_ASMT]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      memcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], strlen(apTokens[MB_SITUS_UNIT]));
   }

   blankPad(acAddr1, SIZ_S_ADDR_D);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1);   
      blankPadz(acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
         sprintf(acTmp, "%s CA    ", myTrim(acAddr1));
      iTmp = strlen(acTmp);
      if (iTmp > SIZ_S_CTY_ST_D)
         iTmp = SIZ_S_CTY_ST_D;
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int Mad_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004130013", 9))
   //   acTmp[0] = 0;
#endif
   // 
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   if (sSitusAdr.lStrNum > 0)
   {
      char *pTmp = strchr(acAddr1, ' ');
      *pTmp = 0;

      // Save original StrNum
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
      memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   }

   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   if (sSitusAdr.Zip[0] > ' ')
      memcpy(pOutbuf+OFF_S_ZIP, sSitusAdr.Zip, SIZ_S_ZIP);

   return 0;
}

/******************************** Mad_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Mad_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do 
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg("Skip Sale rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   while (!iTmp)
   {
      // Replace tab char with 0
      if (pRec)
         iRet = ParseStringNQ(pRec, ',', MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "001040006", 9))
      //   iRet = 0;
#endif

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         /* Do not use Confidential Sale Price
         } else
         {
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            }
         */
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         MB_MergeSale(&sCurSale, pOutbuf, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Mad_MergeChar ******************************
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

int Mad_MergeChar(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], *pTmp;
   long     lTmp, lBldgSqft, lGarSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iFp;
   MB_CHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 512, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   pChar = (MB_CHAR *)pRec;

   // Quality Class
   acCode[0] = 0;
   memcpy(acTmp, pChar->QualityClass, MBSIZ_CHAR_QUALITY);
   acTmp[MBSIZ_CHAR_QUALITY] = 0;
   if (pTmp = strchr(acTmp, ' ')) 
      *pTmp = 0;

   if (acTmp[1] == 'A')
   {
      if (isalpha(acTmp[0]))
      {
         *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0];
         strcpy(acCode, "A");
      }
   } else if (isalpha(acTmp[0])) 
   {
      *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0];

      // Remove hyphens
      remChar(acTmp, '-');
      if (isdigit(acTmp[1]))
         iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
      else if (pTmp && isdigit(*(pTmp+1)))
         iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
   } else if (acTmp[0] > '0' && acTmp[0] <= '9')
      iRet = Quality2Code(acTmp, acCode, NULL);

   blankPad(acCode, SIZ_BLDG_QUAL);
   memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);

   // Yrblt
   lTmp = atoin(pChar->YearBuilt, MBSIZ_CHAR_YRBLT);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = '2';
   } else
   {
      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = ' ';
   }

   // Heating
   if (pChar->Heating[0] > ' ')
   {
      iTmp = 0;
      while (asHeating[iTmp].iLen > 0)
      {
         if (!memcmp(pChar->Heating, asHeating[iTmp].acSrc, asHeating[iTmp].iLen))
         {
            *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }
   // Cooling 
   if (pChar->Cooling[0] > ' ')
   {
      iTmp = 0;
      while (asCooling[iTmp].iLen > 0)
      {
         if (!memcmp(pChar->Cooling, asCooling[iTmp].acSrc, asCooling[iTmp].iLen) )
         {
            *(pOutbuf+OFF_AIR_COND) = asCooling[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }
   // Pool 
   if (pChar->NumPools[0] > ' ')
   {
      iTmp = 0;
      while (asPool[iTmp].iLen > 0)
      {
         if (!memcmp(pChar->NumPools, asPool[iTmp].acSrc, asPool[iTmp].iLen) )
         {
            *(pOutbuf+OFF_POOL) = asPool[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }

   // Beds
   iBeds = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Fireplace
   iFp = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   } else
      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);
   

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 512, fdChar);

   return 0;
}

/******************************* Mad_MergeStdChar ****************************
 *
 * Return 0 if successful, < 0 if error, 1 if not found
 *
 *****************************************************************************/

int Mad_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   ULONG    lSqft;
   int      iLoop, iBeds, iFBath, iHBath, iBldgNum, iRooms, iUnits;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   while (!iLoop)
   {
      // Quality Class
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
      *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
      memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

      // YrBlt
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

      // YrEff
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

      // BldgSqft
      lSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      } else
         memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

      // Garage Sqft
      lSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
      } else
      {
         memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = ' ';
      }

      // LotSqft
      lSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
      if (lSqft > 100 && *(pOutbuf+OFF_LOT_SQFT+(SIZ_LOT_SQFT-1)) == ' ')
      {
         sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lSqft);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
         
         lSqft = atoin(pChar->LotAcre, SIZ_CHAR_SQFT);
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lSqft);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }

      // PatioSqft
      lSqft = atoin(pChar->PatioSqft, SIZ_CHAR_SQFT);
      if (lSqft > 100)
      {
         sprintf(acTmp, "%*d", SIZ_PATIO_SF, lSqft);
         memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
      } else
         memset(pOutbuf+OFF_PATIO_SF, ' ', SIZ_BLDG_SF);

      // Heating
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   
      // Cooling 
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

      // Total Rooms
      iRooms = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
      if (iRooms > 0)
      {
         sprintf(acTmp, "%*d", SIZ_ROOMS, iRooms);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      } else
         memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

      // Beds
      iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      } else
         memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

      // Bath
      iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      } else
         memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

      // Half bath
      iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
      if (iHBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      } else
         memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

      // Fireplace
      memcpy(pOutbuf+OFF_FIRE_PL, pChar->Fireplace, SIZ_FIRE_PL);

      // HasSeptic or HasSewer - no data
      //*(pOutbuf+OFF_SEWER) = pChar->HasSewer;

      // HasWell
      *(pOutbuf+OFF_WATER) = pChar->HasWater;

      // Pools
      *(pOutbuf+OFF_POOL) = pChar->Pool[0];

      // Parkspaces
      if (pChar->ParkSpace[0] > ' ')
         memcpy(pOutbuf+OFF_PARK_SPACE, pChar->ParkSpace, SIZ_PARK_SPACE);
      else
         memset(pOutbuf+OFF_PARK_SPACE, ' ', SIZ_PARK_SPACE);

      // Units count
      iUnits = atoin(pChar->Units, SIZ_CHAR_UNITS);
      if (iUnits > 0)
      {
         sprintf(acTmp, "%*d", SIZ_UNITS, iUnits);
         memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
      } else
         memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

      // Stories
      if (pChar->Stories[0] > ' ')
         memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
      else
         memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

      // BldgSeqNum
      iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);

      lCharMatch++;

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (!pRec)
         break;
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (!iLoop && (iBeds > 0 && iBldgNum > 0))
         break;
   }

   return 0;
}

/******************************** Mad_MergeExe *******************************
 *
 * Merge Exemption
 *
 *****************************************************************************

int Mad_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do 
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         return 1;      // EOF 
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, ',');
      pTmp += 2;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 0;
   }

   iRet = ParseStringNQ(pRec, ',', MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      long lGross = atoin(pOutbuf+OFF_GROSS, SIZ_GROSS);

      if (lTmp > lGross)
      {
         LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Mad_MergeTax ******************************
 *
 * Note: 
 *
 ****************************************************************************

int Mad_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double	dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iRet;

   // Merge tax data
   while (!iTmp)
   {
      iTmp = ParseStringNQ(pRec, ',', MB_TAX_ROLLCAT+1, apTokens);
      if (iTmp < MB_TAX_ROLLCAT)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
         return -1;
      }

      // If same tax year, apply values
      //if (!memcmp(myCounty.acYearAssd, apTokens[MB_TAX_YEAR], 4))
      {
         dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
         dTax1 = atof(acTmp);
         dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
         dTax2 = atof(acTmp);
         dTmp = dTax1+dTax2;
         if (dTax1 == 0.0 || dTax2 == 0.0)
            dTmp *= 2;

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/********************************* Mad_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mad_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Remove null
   replNull(pRollRec);

   // Exception
   if (pTmp = strstr(pRollRec, ")|("))
   {
      LogMsg("*** Bad roll record at %.12s ***", pRollRec);
      memset(pTmp, ' ', 3);
   }

   // Replace tab char with 0
   if (cDelim == '|')
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_M_ADDR4)
   {
      LogMsg("***** Error: Mad_MergeRoll()-> bad input record for APN=%s (Tokens=%d)", apTokens[iApnFld], iRet);
      return -1;
   }

   // Ignore APN starts with 800-999 except 910 (MH), ignore 925 (LEASE LAND)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "20MAD", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] >= 'A' && *apTokens[MB_ROLL_ZONING] <= 'Z')
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
   else
      memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);

   // Standard UseCode - check for leading blank too
   if (acTmp[0] && acTmp[1] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      //memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], strlen(apTokens[MB_ROLL_DOCNUM]));
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (*(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
         lTmp = atol(apTokens[MB_ROLL_DOCNUM]+5);
      else
         lTmp = 0;

      // Only populate if DocNum and DocDate match the year.
      if (pTmp && !memcmp(acTmp, apTokens[MB_ROLL_DOCNUM], 4) && lTmp > 0)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);

         // Format DocNum using new format
         iTmp = sprintf(acTmp, "%.5s%.7d", apTokens[MB_ROLL_DOCNUM], lTmp);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, iTmp);
      }
   }

   // Owner
   try 
   {
      if (strchr(apTokens[MB_ROLL_OWNER], '+'))
      {
         LogMsg0("*** Remove '+' in owner name at %.12s [%s]", pOutbuf, apTokens[MB_ROLL_OWNER]);
         remChar(apTokens[MB_ROLL_OWNER], '+');
      }
      Mad_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   } catch(...) {
      LogMsg("***** Exeception occured in Mad_MergeOwner()");
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "061520023000", 9))
   //   iTmp = 0;
#endif

   // Mailing
   try {
      if (memcmp(apTokens[MB_ROLL_M_ADDR1], "   ", 3))
         Mad_MergeMAdr(pOutbuf, apTokens[MB_ROLL_M_ADDR1], apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3], apTokens[MB_ROLL_M_ADDR4], apTokens[MB_ROLL_CAREOF], apTokens[MB_ROLL_DBA]);
      else if (*apTokens[MB_ROLL_M_ADDR] > '0')
         Mad_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Mad_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************** Mad_Load_Roll ******************************
 *
 * We have to merge lien value here because MAD sometimes drop the parcel and then
 * add it back later with new value.  We want to keep original lien value unless 
 * county request to use the new value. 11/16/2015 spn
 *
 ******************************************************************************/

int Mad_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acTmp[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Load roll update for %s", myCounty.acCntyCode);

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   lLastFileDate = getFileDate(acRollFile);

   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acTmp, "S(#1,C,A) DEL(%d) F(TXT)", cDelim);
   lRet = sortFile(acRollFile, acTmpFile, acTmp);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }
   
   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acTmp, "S(#1,C,A) DEL(%d) F(TXT)", cDelim);
   lRet = sortFile(acSitusFile, acTmpFile, acTmp);
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }
   // Open Sales file
   /*
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return 2;
   }
   */

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, "%s\\%s\\%s_exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acTmp, "S(#2,C,A) DEL(%d) F(TXT)", cDelim);
   lRet = sortFile(acExeFile, acTmpFile, acTmp);
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file - Check for bad dollar format (ignore if dollar is formatted)- 4/28/2020
   iRet = GetIniString("MAD", "DollarFmt", "N", acBuf, 16, acIniFile);
   if (acBuf[0] == 'N')
   {
      LogMsg("Open Tax file %s", acTaxFile);
      sprintf(acTmpFile, "%s\\%s\\%s_tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      sprintf(acTmp, "S(#1,C,A) DEL(%d) F(TXT)", cDelim);
      lRet = sortFile(acTaxFile, acTmpFile, acTmp);

      fdTax = fopen(acTmpFile, "r");
      if (fdTax == NULL)
      {
         LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
         return -2;
      }
   }

   // Open lien file
   fdLienExt = NULL;
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Lien file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
      // Skip quote
      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
#ifdef _DEBUG
         //if (!memcmp(acRollRec, "910005605000", 9))
         //   iTmp = 0;
#endif

         // Create R01 record from roll data
         iRet = Mad_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Mad_MergeSitus(acBuf);

            // Merge Lien
            lRet = 1;
            if (fdLienExt)
               lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

            // Merge Exe if not found in LienExt
            if (fdExe && lRet)
               lRet = MB_MergeExe(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Mad_MergeStdChar(acBuf);

            // Remove old sale data
            //if (bClearSales)
            //   ClearOldSale(acBuf);

            // Merge Sales
            //if (fdSale)
            //   lRet = Mad_MergeSale(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("***** New roll record : %.*s (%d) *****", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Mad_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Mad_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Mad_MergeStdChar(acRec);

            // Merge Sales
            //if (fdSale)
            //   lRet = Mad_MergeSale(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired 
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof && isdigit(acRollRec[1]))
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Mad_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Mad_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Mad_MergeStdChar(acRec);

         // Merge Sales
         //if (fdSale)
         //   lRet = Mad_MergeSale(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTax(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   //if (fdSale)
   //   fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   //LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   //LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u\n", lRecCnt);
   return 0;
}

/******************************** CreateMadRoll ****************************
 *
 * Tested on 9/1/2005 by SPN
 *
 ***************************************************************************

int CreateMadRoll(int iFirstRec)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   iApnLen = myCounty.iApnLen;
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "RMP");
   lRet = sortFile(acRollFile, acTmpFile, "S(1,13,C,A)");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return 2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return 2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SMP");
   lRet = sortFile(acSitusFile, acTmpFile, "S(1,13,C,A) ");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return 2;
   }
   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return 2;
   }
   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return 2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return 4;

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new R01 record
      iRet = Mad_MergeRoll(acBuf, acRec, MAX_RECSIZE, CREATE_R01|CREATE_LIEN);

      // Merge Situs
      if (fdSitus)
         lRet = Mad_MergeSitus(acBuf);

      // Merge Exe
      if (fdExe)
         lRet = Mad_MergeExe(acBuf);

      // Merge Char
      if (fdChar)
         lRet = Mad_MergeChar(acBuf);

      // Merge Sales
      if (fdSale)
         lRet = Mad_MergeSale(acBuf);

      // Merge Taxes
      if (fdTax)
         lRet = Mad_MergeTax(acBuf);

      if (!iRet)
      {
         iNewRec++;
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdSale)
      fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);

   // Sort output file
   //LogMsg("Sort output file %s to %s", acTmpFile, acOutFile);
   //sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) B(%d,R)", iRecLen, iRecLen);
   //lRet = sortFile(acTmpFile, acOutFile, acTmp);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}


/******************************* Mad_convertChar() **************************
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

//int Mad_ConvertChar(char *pInfile)
//{
//   FILE     *fdIn, *fdOut;
//   char     acOutBuf[1024], acInBuf[1024], acTmpFile[256], acTmp[256], *pRec;
//   int      iRet, iTmp, iCnt=0;
//   MB_CHAR  *pCharRec = (MB_CHAR *)&acOutBuf;
//
//   LogMsgD("\nConverting char file %s\n", pInfile);
//   if (!(fdIn = fopen(pInfile, "r")))
//      return -1;
//
//   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   if (!(fdOut = fopen(acTmpFile, "w")))
//   {
//      fclose(fdIn);
//      LogMsg("***** Error creating output file %s", acTmpFile);
//      return -2;
//   }
//
//   // Skip first record - header
//   pRec = fgets(acInBuf, 1024, fdIn);
//   while (!feof(fdIn))
//   {
//      pRec = fgets(acInBuf, 1024, fdIn);
//      if (!pRec)
//         break;
//
//      iRet = ParseStringNQ(pRec, cDelim, MB_CHAR_FP+1, apTokens);
//      if (iRet < MB_CHAR_FP)
//      {
//         LogMsg("***** Bad CHAR input for parcel %s", apTokens[0]);
//         continue;
//      }
//
//      memset(acOutBuf, ' ', sizeof(MB_CHAR));
//      memcpy(pCharRec->Asmt, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));
//
//      iTmp = atoi(apTokens[MB_CHAR_POOLS]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%.2d", iTmp);
//         memcpy(pCharRec->NumPools, acTmp, iRet);
//      } else if (*apTokens[MB_CHAR_POOLS] >= 'A')
//      {
//         iTmp = strlen(apTokens[MB_CHAR_POOLS]);
//         if (iTmp > MBSIZ_CHAR_POOLS) iTmp = MBSIZ_CHAR_POOLS;
//         memcpy(pCharRec->NumPools, apTokens[MB_CHAR_POOLS], iTmp);
//      }
//
//      memcpy(pCharRec->LandUseCat, apTokens[MB_CHAR_USECAT], strlen(apTokens[MB_CHAR_USECAT]));
//      memcpy(pCharRec->QualityClass, apTokens[MB_CHAR_QUALITY], strlen(apTokens[MB_CHAR_QUALITY]));
//      memcpy(pCharRec->YearBuilt, apTokens[MB_CHAR_YRBLT], strlen(apTokens[MB_CHAR_YRBLT]));
//      memcpy(pCharRec->BuildingSize, apTokens[MB_CHAR_BLDGSQFT], strlen(apTokens[MB_CHAR_BLDGSQFT]));
//      memcpy(pCharRec->SqFTGarage, apTokens[MB_CHAR_GARSQFT], strlen(apTokens[MB_CHAR_GARSQFT]));
//      memcpy(pCharRec->Heating, apTokens[MB_CHAR_HEATING], strlen(apTokens[MB_CHAR_HEATING]));
//      memcpy(pCharRec->Cooling, apTokens[MB_CHAR_COOLING], strlen(apTokens[MB_CHAR_COOLING]));
//      memcpy(pCharRec->HeatingSource, apTokens[MB_CHAR_HEATING_SRC], strlen(apTokens[MB_CHAR_HEATING_SRC]));
//      memcpy(pCharRec->CoolingSource, apTokens[MB_CHAR_COOLING_SRC], strlen(apTokens[MB_CHAR_COOLING_SRC]));
//
//      iTmp = atoi(apTokens[MB_CHAR_BEDS]);
//      if (iTmp > 0)
//         memcpy(pCharRec->NumBedrooms, apTokens[MB_CHAR_BEDS], strlen(apTokens[MB_CHAR_BEDS]));
//
//      iTmp = atoi(apTokens[MB_CHAR_FBATHS]);
//      if (iTmp > 0)
//         memcpy(pCharRec->NumFullBaths, apTokens[MB_CHAR_FBATHS], strlen(apTokens[MB_CHAR_FBATHS]));
//
//      iTmp = atoi(apTokens[MB_CHAR_HBATHS]);
//      if (iTmp > 0)
//         memcpy(pCharRec->NumHalfBaths, apTokens[MB_CHAR_HBATHS], strlen(apTokens[MB_CHAR_HBATHS]));
//
//      iTmp = atoi(apTokens[MB_CHAR_FP]);
//      if (iTmp > 0)
//         memcpy(pCharRec->NumFireplaces, apTokens[MB_CHAR_FP], strlen(apTokens[MB_CHAR_FP]));
//
//      memcpy(pCharRec->FeeParcel, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));
//
///*
//      if (*(apTokens[MB_CHAR_HASSEPTIC]) > '0')
//         pCharRec->HasSeptic = 'S';
//
//      if (*(apTokens[MB_CHAR_HASSEWER]) > '0')
//         pCharRec->HasSewer = 'Y';
//
//      if (*(apTokens[MB_CHAR_HASWELL]) > '0')
//         pCharRec->HasWell = 'W';
//
//*/
//      pCharRec->CRLF[0] = '\n';
//      pCharRec->CRLF[1] = '\0';
//      fputs(acOutBuf, fdOut);
//
//      iCnt++;
//      if (!(++iCnt % 1000))
//         printf("\r%u", iCnt);
//   }
//
//   printf("\n");
//   if (fdIn) fclose(fdIn);
//   if (fdOut) fclose(fdOut);
//
//   // Sort output on ASMT
//   if (iCnt > 100)
//   {
//      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
//      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A)");
//   } else
//   {
//      printf("\n");
//      iRet = 0;
//   }
//
//   return iRet;
//}

/********************************* Mad_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mad_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L2_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L2_ASMT], strlen(apTokens[L2_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L2_FEEPARCEL], strlen(apTokens[L2_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L2_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L2_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "20MAD", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values
   // Land
   long lLand = atoi(apTokens[L2_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L2_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow = atoi(apTokens[L2_CURRENTGROWINGIMPRVALUE]);
   long lFixt   = atoi(apTokens[L2_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L2_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[L2_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   long lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L2_TAXAMT1]);
   double dTax2 = atof(apTokens[L2_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L2_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L2_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L2_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      if (lTmp > lGross)
      {
         LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L2_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L2_EXEMPTIONCODE1], strlen(apTokens[L2_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L2_EXEMPTIONCODE2], strlen(apTokens[L2_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L2_EXEMPTIONCODE3], strlen(apTokens[L2_EXEMPTIONCODE3]));

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L2_TRA], strlen(apTokens[L2_TRA]));

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L2_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030330020", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L2_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   iTmp = strlen(apTokens[L2_USECODE]);
   if (iTmp > 0)
   {
      pTmp = _strupr(apTokens[L2_USECODE]);
      vmemcpy(pOutbuf+OFF_USE_CO, pTmp, SIZ_USE_CO, iTmp);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, pTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L2_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L2_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Mad_MergeOwner(pOutbuf, apTokens[L2_OWNER]);

   // Mailing
   Mad_MergeMAdr(pOutbuf, apTokens[L2_MAILADDRESS1], apTokens[L2_MAILADDRESS2], apTokens[L2_MAILADDRESS3], apTokens[L2_MAILADDRESS4], NULL, NULL);

   // Situs
   Mad_MergeSitus(pOutbuf, apTokens[L2_SITUS1], apTokens[L2_SITUS2]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L2_TAXABILITY], true, true);

   return 0;
}

/********************************* Mad_Load_LDR *****************************
 *
 * Load TR601 LDR into 1900-byte record.
 *
 ****************************************************************************/

int Mad_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     iRet, lRet=0, lCnt=0;

   LogMsg0("Loading LDR roll for %s", myCounty.acCntyCode);

   lLastFileDate = getFileDate(acRollFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A)");
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating R01 file: %s\n", acTmpFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      lLDRRecCount++;

      // Create new R01 record
      iRet = Mad_MergeLien(acBuf, acRec);
      if (!iRet)
      {
         // Merge Char
         if (fdChar)
            lRet = Mad_MergeStdChar(acBuf);

         // Merge Sales
         if (fdSale)
            lRet = Mad_MergeSale(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Sale matched:     %u\n", lSaleMatch);

   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Sale skiped:      %u\n", lSaleSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/**************************** Mad_ConvStdChar ********************************
 *
 * Copy from MergeMno.cpp to convert new char file. 11/16/2015
 * Other counties has similar format: MER, YOL, HUM, MNO, SBT
 *
 *****************************************************************************/

int Mad_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_char.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acTmp, "S(#1,C,A) OMIT(#1,C,LT,\"001\") DEL(%d) F(TXT)", cDelim);
   LogMsg("Sort char file %s to %s", pInfile, acTmpFile);
   // FeeParcel and Asmt are the same.  We can sort on either one.
   iRet = sortFile(pInfile, acTmpFile, acTmp);
   if (iRet < 500)
   {
      LogMsg("***** Input file is too small.");
      return 1;
   }

   if (!(fdIn = fopen(acTmpFile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Start loop
   while (!feof(fdIn))
   {
      // Get next record
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec)
         break;

      replNull(acBuf);
      if (!pRec || acBuf[0] > '9')
         break;
      if (cDelim == '|')
         iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < MAD_CHAR_HASWELL)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[MAD_CHAR_ASMT], strlen(apTokens[MAD_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[MAD_CHAR_FEEPARCEL], strlen(apTokens[MAD_CHAR_FEEPARCEL]));

      // Format APN
      if (*apTokens[MAD_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[MAD_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[MAD_CHAR_FEEPARCEL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[MAD_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[MAD_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool 
      iTmp = atoi(apTokens[MAD_CHAR_POOLSPA]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[MAD_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass 
      strcpy(acTmp, _strupr(apTokens[MAD_CHAR_QUALITYCLASS]));
      replStr(acTmp, "..", ".");
      replCharEx(acTmp, ",-", ' ', 0, false);
      iTmp = remChar(acTmp, ' ');    // Remove all blanks before process

      if (acTmp[0] >= '0' && acTmp[0] <= 'Z')
      {
         vmemcpy(myCharRec.QualityClass, _strupr(acTmp), SIZ_CHAR_QCLS, iTmp);
         acCode[0] = ' ';
         if (isalpha(acTmp[0])) 
         {
            if (acTmp[0] == 'X' && isalpha(acTmp[1]))
               myCharRec.BldgClass = acTmp[1];
            else
               myCharRec.BldgClass = acTmp[0];

            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            else if (isdigit(acTmp[2]))
               iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
            else if (!_memicmp(acTmp, "ST", 2))
               memcpy(myCharRec.QualityClass, "ST", 2);     // Steel
         } else if (isdigit(acTmp[0]))
         {
            iTmp = atoi(acTmp);
            if (iTmp > 0 && iTmp < 120)
               iRet = Quality2Code(acTmp, acCode, NULL);
         } else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", acTmp, apTokens[MAD_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (acTmp[0] > '0' && acTmp[0] != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", acTmp, apTokens[MAD_CHAR_ASMT]);

      // YrBlt
      int iYrBlt = atoi(apTokens[MAD_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[MAD_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[MAD_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[MAD_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // Stories/NumFloors
      iTmp = atoi(apTokens[MAD_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 99)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[MAD_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[MAD_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport Sqft
      int iCarport = atoi(apTokens[MAD_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Patio SF - MAD stores mixed Patio type and PatioSF in this field
      // We skip Patio Type (01, 02, 03, 04, 05)
      if (*apTokens[MAD_CHAR_PATIOSF] > '0')
      {
         iTmp = atoi(apTokens[MAD_CHAR_PATIOSF]);
         if (iTmp > 100)
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(myCharRec.PatioSqft, acTmp, iRet);
         }
      }

  #ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "001042001000", 9))
      //   iRet = 0;
#endif

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[MAD_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[MAD_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[MAD_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[MAD_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[MAD_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[MAD_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[MAD_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[MAD_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[MAD_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace - 01, 02, 05, 06, 99
      if (*apTokens[MAD_CHAR_FIREPLACE] >= '0' && *apTokens[MAD_CHAR_FIREPLACE] <= '9')
      {
         pRec = findXlatCode(apTokens[MAD_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      } 

      // Haswell -0, 1
      blankRem(apTokens[MAD_CHAR_HASWELL]);
      if (*(apTokens[MAD_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Lot Sqft
      if (iFldCnt > MAD_CHAR_LOTSQFT)
      {
         ULONG    lSqft;
         lSqft = (ULONG)atol(apTokens[MAD_CHAR_LOTSQFT]);
         if (lSqft > 1)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(lSqft*SQFT_MF_1000);
            iTmp = sprintf(acTmp, "%d", (long)(dTmp+0.5));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      // Parking spaces
      if (iFldCnt > MAD_CHAR_PARKSPACES)
      {
         iTmp = atol(apTokens[MAD_CHAR_PARKSPACES]);
         if (iTmp > 0)
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(myCharRec.ParkSpace, acTmp, iRet);
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}


/********************************* Mad_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mad_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "20MAD", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&MAD_Exemption);

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006220044000", 9))
   //   iTmp = 0;
#endif

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Mad_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   //Mad_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Mad_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4], NULL, NULL);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*apTokens[L3_CURRENTDOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 

   return 0;
}

/******************************** Mad_Load_LDR3 *****************************
 *
 * Load LDR 2016
 *
 ****************************************************************************/

int Mad_Load_LDR3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   int      iRet;
   long     lRet=0, lCnt=0, lTmp;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   HANDLE   fhOut;

   LogMsg0("Load %s LDR file", myCounty.acCntyCode);
   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Value file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Lien file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   } else
      fdLienExt = NULL;

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(1,13,C,A) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      // Create new R01 record
      iRet = Mad_MergeLien3(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         if (fdLienExt)
            lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Situs
         if (fdSitus)
            lRet = Mad_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Mad_MergeStdChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/*********************************** loadMad ********************************
 *
 * Options:
 *    -CMAD -L -Xs -Xl -Xv -Xa (load lien)
 *    -CMAD -U -Xsi -Xa        (load roll update)
 *
 ****************************************************************************/

int loadMad(int iSkip)
{
   int   iRet=0;
   char  acTmp[_MAX_PATH], acTmp1[_MAX_PATH];

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;
   iApnLen = myCounty.iApnLen;

   if (iLoadTax == TAX_LOADING)                    // -T 
   {
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Extract lien file - LDR2016
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acTmp, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acTmp, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acTmp, 0);      // 2016 
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode);
   }

   // 2022/07/06 - Copy from ALP to use with MAD_Values.csv
   // To use MB_ExtrValues(), lien extract (-Xl) has to be done first.
   if (iLoadFlag & EXTR_VALUE)                     // -Xv
   {
      char sDbName[64], sBYVFile[_MAX_PATH], sCVFile[_MAX_PATH], acTmp[256];
   
      iValHdr = GetPrivateProfileInt(myCounty.acCntyCode, "ValueHdr", 0, acIniFile);
      GetPrivateProfileString(myCounty.acCntyCode, "ValDelim", "|", acTmp, 32, acIniFile);
      cValDelim = acTmp[0];
   
      sprintf(sBYVFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Value");
      sprintf(acTmp, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = MB_ExtrValues(myCounty.acCntyCode, acValueFile, acExeFile, acTmp, sBYVFile, iValHdr);
   
      // Reset reason and VST
      if (iRet > 0)
      {
         sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Vmp");
         iRet = MB_ResetPVReason(sBYVFile, acTmp);
      }
   
      if (iRet > 0)
      {
         sprintf(sDbName, "LDR%d", lLienYear);
         GetIniString("Data", "SqlValueFile", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(sCVFile, acTmp, sDbName, myCounty.acCntyCode);
         lRecCnt = createValueImport(sBYVFile, sCVFile, false);
         if (lRecCnt > 0)
         {
            // Save output file for import
            strcpy(acValueFile, sCVFile);
            iRet = 0;
         } else
            iLoadFlag ^= EXTR_IVAL;                // Turn off import
      }
   }

   // Extract Sale file from Mad_Sales.csv to Mad_Sale.sls
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Temporary fix sale price problem - 04/28/2020
      // Check for bad dollar format (ignore if dollar is formatted)
      iRet = GetIniString("MAD", "DollarFmt", "N", acTmp1, 16, acIniFile);
      if (acTmp1[0] == 'Y')
      {
         sprintf(acTmp, "%s\\%s\\%s_sale.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
         iRet = FixDollarField(acSalesFile, acTmp);
         if (iRet > 0)
            strcpy(acSalesFile, acTmp);
      }

      iRet = MB_CreateSCSale(MM_DD_YYYY_1, 1, 1, false);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract NDC recorder sale 
   if (iLoadFlag & EXTR_NRSAL)                     // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSalesFile, _MAX_PATH, acIniFile);
      if (!_access(acSalesFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSalesFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSalesFile);
         return -1;
      }
   }

   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      iRet = Mad_ConvStdChar(acCharFile);
      if (iRet <= 0)
      {
         LogMsg("***** Error extracting attributes data from %s", acCharFile);
         return -1;
      }
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
      iRet = Mad_Load_LDR3(iSkip);
   else if (iLoadFlag & LOAD_UPDT)                 // -U
      iRet = Mad_Load_Roll(iSkip);

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Mad_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
   }

   // Apply NDC sale file to R01
   if (!iRet && (iLoadFlag & UPDT_XSAL))           // -Mn
   {
      // Apply Col_Ash.sls to R01 file
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSaleNR(iSkip, acCSalFile, SALE_USE_SCUPDXFR);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   return iRet;
}

