/*****************************************************************************
 *
 * 12/16/2007  Adding function ApplyCumSale() to merge cum sale to R01 by first
 *             remove all sales from R01.
 * 04/01/2010  Fix bug in ApplyCumSale() that might cause access violation.
 * 04/20/2010  Modify ApplySCSalRec() to allow updating TRANSFER but not sales
 *             on non-sale transactions
 * 05/13/2010  Modify MergeGrGr() to skip record without sale price or DocTitle
 *             starts with "CANCEL".
 * 06/26/2010  Fix ApplyCumSale() that drops on record before end of file.
 *             Fix MergeSaleRec(), set bEof=true when no more sale.
 *             Stop update transfer.  This should come from roll file only.
 * 12/10/2010  Modify ExtrHSale() to output SCSAL_REC instead of SALE_REC.  This 
 *             makes it compatible with current sale format.
 * 03/15/2011  Change logic to clear old sales in ApplyHistSale() and ApplyCumSales().
 *             ClearSaleFlg = 1 (clear sales whether there is sale update or not)
 *                          = 2 (clear sales only if sale update available)
 *                          = 0 (do not remove sales before update)
 * 04/27/2011  Modify ApplyCumSale() to add SalePrice to sorting command.
 * 06/28/2011  Adding ApplyCumSaleWP() to apply sale /w sale price only.
 * 08/12/2011  Add CleanupSales() to clean up (SCSAL_REC) file by removing sales that have
 *             same APN and date but no DocNum. Copy SalePrice over if avail.
 *             
 * 08/29/2011  Modify ApplySCSalRec() not to update Sale1 when new and current sale have
 *             the same date.
 * 09/19/2011  Add output file option to convertSaleData()
 * 09/29/2011  Add createSaleImport() to extract sales for import into spatial db.
 * 10/26/2011  Add FixSalePrice() to remove old sale price from prev transaction.
 *             Modify createSaleImport() to output TaxAmt.
 * 11/04/2011  Add output header to createSaleImport() and trim all trailing space.
 * 01/12/2012  Add convertSaleType6() to convert GRGR_DEF to SCSAL_REC.
 *             convertSaleType7() to convert ALAGRGR to SCSAL_REC,
 * 01/19/2012  Add ApplyCumSaleDN() to support SBX.
 * 01/27/2012  Modify ApplyCumSale() to add option to use and clear old GRGR data. 
 * 03/12/2012  Add FixCumSale() to remove bad sale records.
 * 03/25/2012  Modify FixCumSale() and add CombineSaleAndTransfer() which set XferType='T'
 *             for any transfer record inserted.
 * 04/04/2012  Modify ApplySCSalRec() to fix bug that creates duplicate sale when
 *             update transaction with the same date. (found in VEN)
 * 04/19/2012  Add convertSaleType9() to convert SLO sale.
 * 04/30/2012  Modify convertSaleType9() to set NoneSale_Flg='Y' only when DocType='4' (QC)
 * 05/16/2012  Populate OFF_MULTI_APN with MultiSale_Flg in ApplySCSalRec??().
 * 06/18/2012  Add new findDocType() that searches provided DocType translation list and return index if found.
 * 07/23/2012  Modify ApplySCSalRec() to ignore record missing DOCNUM or DOCDATE.
 * 07/26/2012  Add MergeGrGrDoc().
 * 08/03/2012  Fix MergeSale() not to update when new saledate=cursaledate.
 * 08/15/2012  Add ApplySCSalRec_NoDocChk() to ignore DOCNUM.
 * 09/02/2012  Fix ApplyCumSale().
 * 10/22/2012  Fix createSaleImport() to cleanup some bad characters "+*`./{}&%$" in DOCNUM.
 *             Add option SALE_FLD_DOCNUM+6 in FixCumSale() for STA.
 * 11/08/2012  Add option SALE_FLD_DOCNUM+7 in FixCumSale() for RIV
 * 11/16/2012  Modify ApplyHistSale() to add option SALE_USE_SCNOPRICE to update sale w/o sale price.
 * 12/06/2012  Modify FixCumSale() to fix DocType for SJX.
 * 01/02/2013  Add DocLink to sale import file
 * 01/03/2013  Fix bug in createSaleImport() that removes '/' from DocNum.
 * 01/07/2013  Add ApplySCSalRec_NoDateChk() and option DONT_CHK_DOCDATE to support 
 *             county that has no sale date (TRI).
 * 01/09/2013  Add new createSaleImport() that takes DocLink format function as input
 *             and create DocLink on the fly. This is for PQ docs only. createSaleOutrec()
 *             will accept DocLink as parameter.
 * 01/24/2013  Add findDocType() that supports IDX_TBL5 and modify createSaleOutrec()
 *             to output both DocType and DocCode.
 * 02/08/2013  Modify ApplySCSalRec() to update DocType & DocNum on same date with different DocNum.
 * 03/11/2013  Modify ApplySCSalRec() where same date but different DocNum, update whole
 *             record with new doc data, not just DocNum & DocType. KER
 * 05/22/2013  Modify FixCumSale() to fix DocNum for MNO & SFX
 * 05/31/2013  Modify ApplyHistSale() to add DONT_CHK_DOCNUM for MPA.
 * 07/09/2013  Add ApplySCSalRec_NoChk() to apply all transactions to R01 regardless
 *             of DocType. If multiple transactions on the same day, apply them all. GLE
 * 07/10/2013  Modify ApplySCSalRec().  See desc on function for detail.
 * 07/24/2013  Fix problem when CurSaleDt > LstSaleDt and CurDocNum < LstDocNum
 * 10/03/2013  Fix bug in createSaleImport() when typo of "1" as '|'.
 * 11/22/2013  Modify FixCumSale() to add option to fix TRI's DocNum.
 * 12/31/2013  Add FixDocType() to update DocType using translation table and reformat DocTax.
 * 01/13/2014  Modify FixDocType() to add option to reformat StampAmt.
 * 01/30/2014  Add MergeSaleFiles() to move sale price from one sale file to another.
 * 02/03/2014  Modify MergePrice() to add SaleCode and MultiSale_Flg.
 * 02/19/2014  Add createWebSaleImport() to create import filr for GRGR_DOC input
 * 03/28/2014  Add option to clear GRGR in ApplyCumSale() even if no sale update for that parcel.
 * 04/13/2014  Add DedupCumSale() and ChkDocTax().
 * 04/30/2014  Add option to clear xfer in ApplySCSalRec()
 *             Add ApplySCSalRec_ChkDocYear() and SetChkYearFlg() to better match DocNum for MON
 *             Modify Mon_FormatDocNum() to correct some known DocNum style.
 * 05/21/2014  Add FixDocTax() for VEN
 * 05/26/2014  Add SaleDedup() for VEN
 * 05/28/2014  Change sort sequence in MergeSaleFiles() and capture DocNum in MergePrice()
 *             This may affect INY & TUO.
 * 06/24/2014  Modify ApplyHistSale() when call with GRGR_UPD_XFR to apply update only if there
 *             is sale price.  Otherwise, only transfer is updated (ALA & VEN).
 * 07/02/2014  Move MergeGrGr() and MergeGrGrDoc() to MergeLax.cpp
 * 09/30/2014  Make sure all sale records with Multi_Apn flag set are applied correctly (RIV).
 * 10/09/2014  Standardize the use of MultiSale_Flg 
 * 10/14/2014  Modify CreateSaleImport() to create import for GRGR_DOC and GRGR_DEF.
 * 02/03/2015  Modify ApplyCumSale() to reset buffer pointer every time it's called.
 *             Remove MergeSaleRec() since it's no longer needed.
 * 07/10/2015  Add ApplySCSalRecGD() and FixSCSal().  Modify ApplyHistSale() to add option
 *             GRGR_UPD_GD to update GD to Sale1-3 even without sale price.
 * 10/07/2015  Add UpdateSaleUsingGrGr() to merge DocNum from GRGR_DOC to SCSALE_REC.
 * 12/02/2015  Add FixOtherApn() to update APN based on TE_NO for KER.
 * 02/15/2016  Fix CNTY_STA in FixSCSal() to format DocNum as 7 digits instead of 6.
 * 02/18/2016  Fix MergePrice() to accept second file as updated data if sale price > 0.
 * 11/17/2016  Add SetMultiSale() to set MultiSale_Flg='Y' when same DocNum occurs in different parcels.
 * 11/18/2016  Add SetFullPartialSale() to set SaleCode="F" or "P" if PctXfer > 0
 * 11/22/2016  Add buffer B8000 to all DUPO command using sortFile().
 * 11/27/2016  Fix bug in SetMultiSale()
 * 04/05/2017  Add ResetSaleCode() to fix sale code
 * 05/05/2017  Modify ApplySCSalRec(): if SalePrice is 0, check ConfSalePrice (YOL)
 * 07/09/2017  Modify findDocType() to return NULL when DocTitle is unknown.
 * 12/11/2017  Modify FixCumSale() to add SIE
 * 02/01/2018  Add findSortedDocType() to find in a sorted list
 * 02/03/2018  Add SetKeepMAFlg() to support VEN not to overwrite MultiApn flag
 * 02/09/2018  Modify ApplyHistSale(): use ARCode from sale rec if exist.
 * 03/23/2018  Modify createSaleImport() & createSaleOutrec() to add NonSale_Flg to output record.
 * 03/26/2018  Set NonSale_Flg='Y' in CombineSaleAndTransfer() for all transfers
 * 04/13/2018  Add ApplySCSalExt() to update owner info using SCSAL_EXT layout.
 *             Add option GRGR_UPD_OWNER to ApplyHistSale() to update owner.
 *             Increase all sale buffer to 2048 from 1024 bytes.
 * 04/24/2018  Modify FixCumSale() to add code that fixes DocNum & DocType for MOD.
 * 09/03/2018  Cleanup old sale amt & salecode if not avail on new sale
 * 03/07/2019  Modify ApplySCSalExt() to update owner info for sale transaction only (no QuitClaim).
 * 03/15/2019  Modify FixSCSal() to add SJX
 * 04/29/2019  Add new version of findDocType() using IDX_SALE DocType
 * 05/16/2019  Modify convertSaleData() & add convertSaleType10() to convert ORG_GRGR to SCSAL_EXT format.
 * 05/21/2019  Fix FixDocTax() to return 0 when nothing change.
 * 08/01/2019  Add SC2SCExt() to convert SCSAL_REC to SCSAL_EXT
 * 12/11/2019  Modify ApplySCSalRec() to overwrite Transfered DocNum on same date
 * 12/12/2019  Add ApplySCSalRec_ReformatDocNumX4() to reformat DocNum for DNX.
 * 02/25/2020  Modify ApplySCSalRec_NoDocChk() to skip record that only have RecDate.
 *             Add ApplyCumSaleNR(), ApplyHistSaleNR, and ApplySCSalRecNR for SBD.
 * 04/02/2020  Modify ApplyCumSaleNR() to support LAX.
 * 04/09/2020  Change all ApplySCSalRec*() from void to int and return 0 if sale1 is updated.
 *             Change log msg in ApplyCumSaleNR() & ApplyCumSale()
 * 04/10/2020  Fix bug in ApplyHistSale*()
 * 06/01/2020  Reset return code in ApplyCumSale(), ApplyCumSaleNR(), and ApplyCumSaleWP()
 * 06/17/2020  Modify ApplySCSalRecNR() & ApplyCumSaleNR() to add option to update owner.
 * 06/18/2020  Modify ApplySCSalRecNR() & ApplySCSalRec() to update salecode only if there is sale price.
 * 11/13/2020  Fix bug in createGrgrDocImportRec() that crashes when calling function to create DocLinks.
 *             Remove '|' from buyer/seller or replace it with 'I'.  Add createOrgGrgrImportRec().
 *             Modify createSaleImport() to support TYPE_GRGR_ORG.
 * 01/05/2021  Modify ApplySCSalRec(): when sale rec has the same date as last record, only update SALE1
 *             if it has sale price or DocType=1 or 13 (INY)
 * 01/17/2021  Modify convertSaleType10() to access ORG_DocCode[] from MergeOrg.h
 * 02/19/2021  Modify FixCumSale() to correct DocNum for TRI.
 * 06/03/2021  Fix bug in ApplySCSalRec() that insert null char after DocType (CCX).
 * 07/21/2021  Modify ApplySCSalRec() to filter out large value > 32-bit signed int (SFX).
 * 08/26/2021  Use bUseConfSalePrice to decide when to use confidential sale price.
 * 02/15/2022  Add FixSCSalEx().
 * 03/05/2022  Bug fix ApplySCSalRec_ReformatDocNumX4() for DNX.
 * 05/31/2022  Modify ApplyCumSale() to add VERIFY_DATE option which makes sure SALE1_DT
 *             and TRANSFER_DT are the same if they have the same DOCNUM (SFX).
 * 01/30/2023  Modify FixCumSale() to add option to remove all bad sale records where 'I'  
 *             is present in DocNum or DocNum and DocYear are not matched. 
 * 02/05/2023  Modify FixCumSale() to fix cum sale file for MEN by remove all bad DocNum records.
 * 02/10/2023  Modify FixCumSale() to fix cum sale file for SON.
 * 09/23/2023  Add ApplySCSalRec_ChkXfer().  Modify ApplyHistSale() and update Transfer
 *             when sale is newer only (SFX).
 * 05/22/2024  Modify ApplySCSalRec_ChkXfer() to set DataSrc even when no data update if DataSrc='A'
 * 01/30/2025  Modify ApplySCSalRec_NoDocChk() not to update TRANSFER if transaction is not newer than current.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "Logs.h"
#include "Utils.h"
#include "CountyInfo.h"
#include "R01.h"
#include "Recdef.h"
#include "Tables.h"
#include "SaleRec.h"
#include "OutRecs.h"
#include "doSort.h"
#include "Prodlib.h"

static   long  lSaleMatch, lSaleSkip, lSaleUpdate;
static   bool  bChkYear=false;
static   bool  bKeepMAFlag=false;      // Keep MultiApn flag

extern   COUNTY_INFO myCounty;
extern   XREFTBL     asDeed[];
extern   int         iNumDeeds;

extern   int   iRecLen, iApnLen, iNoMatch, iGrGrApnLen;
extern   bool  bDebug, bUseConfSalePrice;

extern   char  acSaleFile[], acCSalFile[], acGrGrTmpl[], acRawTmpl[], acRollKey[];
extern   char  acTmpPath[], acSaleKey[], acESalTmpl[], acSaleTmpl[], acIniFile[], acDocPath[];
extern   long  lRecCnt, lLastRecDate, lToday, lToyear, lLienDate;

/*****************************************************************************
 *
 * Set flag bKeepMAFlag.
 *
 *****************************************************************************/

void SetKeepMAFlg(bool bVal)
{
   bKeepMAFlag = bVal;
}

/*****************************************************************************
 *
 * Set flag bChkYear.
 *
 *****************************************************************************/

void SetChkYearFlg(bool bVal)
{
   bChkYear = bVal;
}

/*****************************************************************************
 *
 * Check for valid DocTax. DocTax must be > 10.0 and sale price is whole value.
 *
 *****************************************************************************/

bool ChkDocTax(char *pDocTax)
{
   double dTax;
   long   lPrice, lTmp;
   bool   bRet;

   dTax = atof(pDocTax);
   if (dTax < 10.0)
      return false;

   lPrice = (long)(dTax * SALE_FACTOR * 100);
   lTmp = (lPrice/100)*100;
   if (lTmp == lPrice)
      bRet = true;
   else
      bRet = false;

   return bRet;
}

/*****************************************************************************
 *
 *****************************************************************************/

void ClearOldSale(char *pOutbuf, bool bDelXfer)
{
   memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
   memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);
   memset(pOutbuf+OFF_SALE1_DOCTYPE, ' ', SIZ_SALE1_DOCTYPE);
   memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
   memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);
   memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

   memset(pOutbuf+OFF_SALE2_DOC, ' ', SIZ_SALE1_DOC);
   memset(pOutbuf+OFF_SALE2_DT, ' ', SIZ_SALE1_DT);
   memset(pOutbuf+OFF_SALE2_DOCTYPE, ' ', SIZ_SALE1_DOCTYPE);
   memset(pOutbuf+OFF_SALE2_AMT, ' ', SIZ_SALE1_AMT);
   memset(pOutbuf+OFF_SALE2_CODE, ' ', SIZ_SALE1_CODE);

   memset(pOutbuf+OFF_SALE3_DOC, ' ', SIZ_SALE1_DOC);
   memset(pOutbuf+OFF_SALE3_DT, ' ', SIZ_SALE1_DT);
   memset(pOutbuf+OFF_SALE3_DOCTYPE, ' ', SIZ_SALE1_DOCTYPE);
   memset(pOutbuf+OFF_SALE3_AMT, ' ', SIZ_SALE1_AMT);
   memset(pOutbuf+OFF_SALE3_CODE, ' ', SIZ_SALE1_CODE);

   if (bDelXfer)
   {
      memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_SALE1_DOC);
   }

   if (!bKeepMAFlag)
      *(pOutbuf+OFF_MULTI_APN) = ' ';

   memset(pOutbuf+OFF_AR_CODE1, ' ', 3);
}

/*****************************************************************************
 *
 *****************************************************************************/

void ClearOldGrGr(char *pOutbuf)
{
   if (*(pOutbuf+OFF_AR_CODE1) == 'R')
   {
      memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_SALE1_DOCTYPE, ' ', SIZ_SALE1_DOCTYPE);
      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);
      memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

      memset(pOutbuf+OFF_SALE2_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_SALE2_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_SALE2_DOCTYPE, ' ', SIZ_SALE1_DOCTYPE);
      memset(pOutbuf+OFF_SALE2_AMT, ' ', SIZ_SALE1_AMT);
      memset(pOutbuf+OFF_SALE2_CODE, ' ', SIZ_SALE1_CODE);

      memset(pOutbuf+OFF_SALE3_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_SALE3_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_SALE3_DOCTYPE, ' ', SIZ_SALE1_DOCTYPE);
      memset(pOutbuf+OFF_SALE3_AMT, ' ', SIZ_SALE1_AMT);
      memset(pOutbuf+OFF_SALE3_CODE, ' ', SIZ_SALE1_CODE);

      memset(pOutbuf+OFF_AR_CODE1, ' ', 3);
      if (!bKeepMAFlag)
         *(pOutbuf+OFF_MULTI_APN) = ' ';
   }
}

/*****************************************************************************
 *
 *****************************************************************************/

void ClearOneSale(char *pOutbuf, int iSaleNum)
{
   switch (iSaleNum)
   {
      case 1:
         memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
         memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);
         memset(pOutbuf+OFF_SALE1_DOCTYPE, ' ', SIZ_SALE1_DOCTYPE);
         memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
         memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);
         memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);
         break;
      case 2:
         memset(pOutbuf+OFF_SALE2_DOC, ' ', SIZ_SALE1_DOC);
         memset(pOutbuf+OFF_SALE2_DT, ' ', SIZ_SALE1_DT);
         memset(pOutbuf+OFF_SALE2_DOCTYPE, ' ', SIZ_SALE1_DOCTYPE);
         memset(pOutbuf+OFF_SALE2_AMT, ' ', SIZ_SALE1_AMT);
         memset(pOutbuf+OFF_SALE2_CODE, ' ', SIZ_SALE1_CODE);
         break;
      case 3:
         memset(pOutbuf+OFF_SALE3_DOC, ' ', SIZ_SALE1_DOC);
         memset(pOutbuf+OFF_SALE3_DT, ' ', SIZ_SALE1_DT);
         memset(pOutbuf+OFF_SALE3_DOCTYPE, ' ', SIZ_SALE1_DOCTYPE);
         memset(pOutbuf+OFF_SALE3_AMT, ' ', SIZ_SALE1_AMT);
         memset(pOutbuf+OFF_SALE3_CODE, ' ', SIZ_SALE1_CODE);
         break;
   }
}

/********************************* findDocType *******************************
 *
 *
 *****************************************************************************/

char *findDocType(char *pDocTitle, char *pCode)
{
   char *pRet=NULL;

   *pCode = 0;
   if (pDocTitle[0] >= 'A')
   {
      pRet = pCode;
      // Here are deeds found on Fresno
      if (!memcmp(pDocTitle, "GRANT ", 6) || !memcmp(pDocTitle, "GD", 2))
         strcpy(pCode, "1  ");
      else if (!memcmp(pDocTitle, "DEED", 4))
         strcpy(pCode, "13 ");
      else if (!memcmp(pDocTitle, "QUIT", 4) || !memcmp(pDocTitle, "QC", 2))
         strcpy(pCode, "4  ");
      else if (!memcmp(pDocTitle, "EASEMENT", 8) || !memcmp(pDocTitle, "E ", 2))
         strcpy(pCode, "40 ");
      else if (!memcmp(pDocTitle, "TRUSTEE", 7))
         strcpy(pCode, "27 ");
      else if (!memcmp(pDocTitle, "AFFIDAVIT", 9))
         strcpy(pCode, "6  ");
      else if (!memcmp(pDocTitle, "ASSIGNMENT", 10))
         strcpy(pCode, "7  ");
      else if (!memcmp(pDocTitle, "BILL OF SALE", 12))
         strcpy(pCode, "32 ");
      else if (!memcmp(pDocTitle, "CONVENANT", 9))
         // Treat CONVENANTS as GRANT DEED
         strcpy(pCode, "1  ");
      else if (!memcmp(pDocTitle, "LEASE", 5))
         strcpy(pCode, "44 ");
      else if (!memcmp(pDocTitle, "TAX DEED", 8))
         strcpy(pCode, "67 ");
      else if (!memcmp(pDocTitle, "CORRECTION DEED", 15))
         strcpy(pCode, "9  ");
      else if (!memcmp(pDocTitle, "SHERIFFS DEED", 13))
         strcpy(pCode, "25 ");
      else
      {
         LogMsg("Unknown DocTitle: %.16s", pDocTitle);
         pRet=NULL;
      }
   }

   return pRet;
}

char *findDocType(char *pDocTitle, char *pCode, IDX_TBL2 *pDocTbl)
{
   strcpy(pCode, "   ");
   if (pDocTitle[0] >= 'A')
   {
      // Here are deeds found on Fresno
      if (!memcmp(pDocTitle, "GRANT ", 6) || !memcmp(pDocTitle, "GD", 2))
         strcpy(pCode, "1  ");
      else if (!memcmp(pDocTitle, "DEED", 4))
         strcpy(pCode, "13 ");
      else if (!memcmp(pDocTitle, "QUITCLAIM", 9) || !memcmp(pDocTitle, "QUIT CLAIM", 10) 
               || !memcmp(pDocTitle, "QC", 2))
         strcpy(pCode, "4  ");
      else if (!memcmp(pDocTitle, "EASEMENT", 8) || !memcmp(pDocTitle, "E ", 2))
         strcpy(pCode, "40 ");
      else if (!memcmp(pDocTitle, "TRUSTEES DEED", 13) || !memcmp(pDocTitle, "TRUSTEE'S DEED", 14))
         strcpy(pCode, "27 ");
      else if (!memcmp(pDocTitle, "AFFIDAVIT", 9))
         strcpy(pCode, "6  ");
      else if (!memcmp(pDocTitle, "ASSIGNMENT", 10))
         strcpy(pCode, "7  ");
      else if (!memcmp(pDocTitle, "BILL OF SALE", 12))
         strcpy(pCode, "32 ");
      else if (!memcmp(pDocTitle, "CONVENANT", 9))
         // Treat CONVENANTS as GRANT DEED
         strcpy(pCode, "1  ");
      else if (!memcmp(pDocTitle, "LEASE", 5))
         strcpy(pCode, "44 ");
      else if (!memcmp(pDocTitle, "TAX DEED", 8))
         strcpy(pCode, "67 ");
      else if (!memcmp(pDocTitle, "CORRECTION DEED", 15))
         strcpy(pCode, "9  ");
      else if (!memcmp(pDocTitle, "SHERIFFS DEED", 13))
         strcpy(pCode, "25 ");
      else if (bDebug)
         LogMsg("Unknown DocTitle: %.16s", pDocTitle);
   } else
   {
      int iTmp = atol(pDocTitle);
      if (iTmp < 100)
         strcpy(pCode, pDocTbl[iTmp].pCode);
      else
         LogMsg("Unknown DocTitle: %.16s", pDocTitle);
   }

   return pCode;
}

bool findDocType(char *pDocTitle, char *pCode, IDX_TBL4 *pDocTbl)
{
   bool  bFound=false;

   strcpy(pCode, "   ");
   if (pDocTitle[0] >= '0')
   {
      while (pDocTbl->iNameLen > 0)
      {
         if (!_memicmp(pDocTitle, pDocTbl->pName, pDocTbl->iNameLen))
         {
            memcpy(pCode, pDocTbl->pCode, pDocTbl->iCodeLen);
            bFound = true;
            break;
         }
         pDocTbl++;
      }
   }

   if (!bFound && bDebug)
      LogMsg("Unknown DocTitle: %.20s", pDocTitle);

   return bFound;
}

/********************************* findDocType *******************************
 *
 * Return index of matched entry, otherwise -1
 *
 *****************************************************************************/

int findDocType(char *pDocTitle, IDX_TBL4 *pDocTbl)
{
   bool  bFound=false;
   int   iTmp  =0;

   if (pDocTitle[0] >= '0')
   {
      while (pDocTbl->iNameLen > 0)
      {
         if (!_memicmp(pDocTitle, pDocTbl->pName, pDocTbl->iNameLen))
         {
            bFound = true;
            break;
         }
         pDocTbl++;   
         iTmp++;
      }
   }

   if (!bFound)
   {
      if (bDebug)
         LogMsg("Unknown DocTitle: %.20s", pDocTitle);
      iTmp = -1;
   }

   return iTmp;
}

int findDocType(char *pDocTitle, IDX_TBL5 *pDocTbl)
{
   bool  bFound=false;
   int   iTmp  =0;

   if (pDocTitle[0] >= '0')
   {
      while (pDocTbl->iNameLen > 0)
      {
         if (!_memicmp(pDocTitle, pDocTbl->pName, pDocTbl->iNameLen))
         {
            bFound = true;
            break;
         }
         pDocTbl++;   
         iTmp++;
      }
   }

   if (!bFound)
   {
      if (bDebug)
         LogMsg("Unknown DocTitle: %.20s", pDocTitle);
      iTmp = -1;
   }

   return iTmp;
}

int findDocType(char *pDocTitle, IDX_SALE *pDocTbl)
{
   bool  bFound=false;
   int   iTmp  =0;

   if (pDocTitle[0] >= '0')
   {
      while (pDocTbl->DocTitleLen > 0)
      {
         if (!_memicmp(pDocTitle, pDocTbl->DocTitle, pDocTbl->DocTitleLen))
         {
            bFound = true;
            break;
         }
         pDocTbl++;   
         iTmp++;
      }
   }

   if (!bFound)
      iTmp = -1;

   return iTmp;
}

int findSortedDocType(char *pDocTitle, IDX_TBL5 *pDocTbl)
{
   bool  bFound=false;
   int   iTmp  =0, iCmp=1;

   if (pDocTitle[0] >= '0')
   {
      while (pDocTbl->iNameLen > 0 && iCmp > 0)
      {
         iCmp = _memicmp(pDocTitle, pDocTbl->pName, pDocTbl->iNameLen);
         if (!iCmp)
         {
            bFound = true;
            break;
         }
         pDocTbl++;   
         iTmp++;
      }
   }

   if (!bFound)
   {
      if (bDebug)
         LogMsg("Unknown DocTitle: %.20s", pDocTitle);
      iTmp = -1;
   }

   return iTmp;
}

/********************************** MergeSale ********************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update seller if that is the sale from assessor.  Ignore sale occured 
 * on the same date without sale amt.
 *
 *****************************************************************************/

int MergeSale(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag)
{
   long  lCurSaleDt, lLstSaleDt, lPrice;
   char  *pTmp, acTmp[32], acSalePrice[32];

   lCurSaleDt = atoin(pSaleRec->acDocDate, SALE_SIZ_DOCDATE);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SALE_SIZ_DOCDATE);

   if (lCurSaleDt <= lLstSaleDt)
      return -1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002530020000", 9))
   //   lPrice = 0;
#endif

   lPrice = atoin(pSaleRec->acSalePrice, SALE_SIZ_SALEPRICE); 
   if (lPrice > 0)
      sprintf(acSalePrice, "%*d", SALE_SIZ_SALEPRICE, lPrice);
   else
      memset(acSalePrice, ' ', SALE_SIZ_SALEPRICE);

   if (lCurSaleDt == lLstSaleDt)
   {
      // Sales occur on the same date, keep the one has price
      if (lPrice > 0)
      {
         memcpy(pOutbuf+OFF_SALE1_AMT, acSalePrice, SALE_SIZ_SALEPRICE);
         memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->acDocNum, SALE_SIZ_DOCNUM);
         if (pTmp=findDocType(pSaleRec->acDocType, acTmp))
            memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pTmp, strlen(pTmp));
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->acDocNum, SALE_SIZ_DOCNUM);
      }
      return 0;
   }

   // If there is no sale price, then it's only a transfer
   //lPrice = atoin(pSaleRec->acSalePrice, SALE_SIZ_SALEPRICE); 
   //if (lPrice > 0)
   //{
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SALE_SIZ_DOCNUM);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SALE_SIZ_DOCDATE);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE1_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SALE_SIZ_SALEPRICE);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SALE_SIZ_SALECODE);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SALE_SIZ_DOCNUM);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SALE_SIZ_DOCDATE);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SALE_SIZ_SALEPRICE);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SALE_SIZ_SALECODE);

      // Update current sale
      memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->acDocNum, SALE_SIZ_DOCNUM);
      memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->acDocDate, SALE_SIZ_DOCDATE);
      if (isdigit(pSaleRec->acDocType[0]))
         memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->acDocType, SIZ_SALE1_DOCTYPE);
      else if (pTmp=findDocType(pSaleRec->acDocType, acTmp))
         memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pTmp, strlen(pTmp));

      // Remove sale code
      if (pSaleRec->acSaleCode[0] >= '0')
         memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->acSaleCode, SALE_SIZ_SALECODE);
      else
         memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);

      if (bSaleFlag)
      {
         quoteRem(pSaleRec->acSeller, SIZ_SELLER);
         memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);
      } else
         memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

      memcpy(pOutbuf+OFF_SALE1_AMT, acSalePrice, SIZ_SALE1_AMT);
   //}

   // Update transfers
   //memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->acDocNum, SIZ_TRANSFER_DOC);
   //memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->acDocDate, SIZ_TRANSFER_DT);

   return 1;
}

/********************************** MergeSale1 *******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update seller if that is the sale from assessor.
 * This is the modified version of MergeSale() which will not update sale in 
 * the same date without SaleAmt.
 *
 *****************************************************************************/

int MergeSale1(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   char  *pTmp, acTmp[32];

   lCurSaleDt = atoin(pSaleRec->acDocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   if (lCurSaleDt < lLstSaleDt)
      return -1;

   lPrice = atoin(pSaleRec->acSalePrice, SIZ_SALE1_AMT); 
   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if ((lPrice == lLastAmt) || (lPrice == 0))
         return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->acDocNum, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->acDocDate, SIZ_SALE3_DT);
   if (isdigit(pSaleRec->acDocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->acDocType, 3);
   else if (pTmp=findDocType(pSaleRec->acDocType, acTmp))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pTmp, strlen(pTmp));

   // Remove sale code - 
   // Notes: SALE_SIZ_SALECODE is smaller than  SIZ_SALE1_CODE.  Don't swap them.
   if (pSaleRec->acSaleCode[0] >= '0')
      memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->acSaleCode, SALE_SIZ_SALECODE);
   else
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);

   if (bSaleFlag)
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);
   else if (lCurSaleDt > lLstSaleDt)
      memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

   // Check for questionable price
   if (lPrice > 5000)
      memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->acSalePrice, SIZ_SALE1_AMT);
   else
      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);

   // Update transfers
   //lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   //if (lCurSaleDt >= lLstSaleDt)
   //{
   //   memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->acDocNum, SIZ_TRANSFER_DOC);
   //   memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->acDocDate, SIZ_TRANSFER_DT);
   //}

   if (bSaleFlag)
      *(pOutbuf+OFF_AR_CODE1) = 'A';
   else
      *(pOutbuf+OFF_AR_CODE1) = 'R';

   return 1;
}

/********************************** MergeSale2 *******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update seller if that is the sale from assessor.
 * This is the modified version of MergeSale() which updates sale on following 
 * categories (designed for PLU):
 *    1) If docnum < doc1num, ignore this sale. 
 *    2) If docnum > doc1num and docdate = doc1date, no saleamt and doc1 has 
 *       saleamt, ignore this sale.
 *    3) If docnum > doc1num and docdate = doc1date, both has no saleamt, 
 *       update docnum.
 *    4) If docnum > doc1num and docdate = doc1date, has saleamt update docnum 
 *       and saleamt.
 *    5) If docnum > doc1num and docdate <> doc1date, insert new sale.
 *
 *****************************************************************************/

int MergeSale2(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   int   iTmp;
   char  *pTmp, acTmp[32];
   bool  bUpdtAll=true;

   // Check case #1
   iTmp = memcmp(pSaleRec->acDocNum, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   if (iTmp <= 0)
      return -1;

   lCurSaleDt = atoin(pSaleRec->acDocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   lPrice = atoin(pSaleRec->acSalePrice, SIZ_SALE1_AMT);
   lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   
   
   if (lCurSaleDt == lLstSaleDt)
   {
      // Check case #2
      if (!lPrice && lLastAmt)
         return 0;

      // Check case #3
      if (!lPrice && !lLastAmt)
         bUpdtAll = false;    // Update docnum and doctype only

      // case #4: update current sale
      iTmp = 0;

   } else // case #5
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE1+2) = *(pOutbuf+OFF_AR_CODE1+1);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE1+1) = *(pOutbuf+OFF_AR_CODE1);
      iTmp = 1;
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->acDocNum, SIZ_SALE1_DOC);
   if (isdigit(pSaleRec->acDocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->acDocType, 3);
   else if (pTmp=findDocType(pSaleRec->acDocType, acTmp))
      memcpy(pOutbuf+CDA_OFF_DOC1TYPE, pTmp, strlen(pTmp));

   if (bUpdtAll)
   {
      memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->acDocDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->acSalePrice, SIZ_SALE1_AMT);

      // Notes: SALE_SIZ_SALECODE is smaller than  SIZ_SALE1_CODE.  Don't swap them.
      if (pSaleRec->acSaleCode[0] >= '0')
         memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->acSaleCode, SALE_SIZ_SALECODE);
      else
         memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);

      if (bSaleFlag)
         memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);
      else
         memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

      //memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->acDocNum, SIZ_TRANSFER_DOC);
      //memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->acDocDate, SIZ_TRANSFER_DT);
   }

   if (bSaleFlag)
      *(pOutbuf+OFF_AR_CODE1) = 'A';    // Assessor
   else
      *(pOutbuf+OFF_AR_CODE1) = 'R';    // Recorder

   return iTmp;
}

/********************************* MergeSale3 *******************************
 *
 * ===> This is obsolete function.  Replace this with MB_MergeSale() for all
 *      Megabyte counties. 03/02/2009 SN
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * This special version to support MB counties.
 * Only update seller if that is the sale from assessor.
 *    1) If docnum < doc1num, ignore this sale. 
 *    2) If docdate < doc1date, ignore this sale
 *    3) If docdate = doc1date, update current sale regardless of sale price 
 *    4) If docdate > doc1date, insert new sale 
 *
 *****************************************************************************/

int MergeSale3(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   int   iTmp;
   char  *pTmp, acTmp[32];
   bool  bUpdtAll=true;

   // Check case #1
   iTmp = memcmp(pSaleRec->acDocNum, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   if (iTmp < 0)
      return -1;

   lCurSaleDt = atoin(pSaleRec->acDocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   // Check case #2
   if (lCurSaleDt < lLstSaleDt)
      return -1;

   lPrice = atoin(pSaleRec->acSalePrice, SIZ_SALE1_AMT);
   lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   
   // Check case #3
   if (lCurSaleDt == lLstSaleDt)
   {
      iTmp = 0;

   } else // case #4
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE1+2) = *(pOutbuf+OFF_AR_CODE1+1);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE1+1) = *(pOutbuf+OFF_AR_CODE1);
      iTmp = 1;
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->acDocNum, SIZ_SALE1_DOC);
   if (isdigit(pSaleRec->acDocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->acDocType, 3);
   else if (pTmp=findDocType(pSaleRec->acDocType, acTmp))
      memcpy(pOutbuf+CDA_OFF_DOC1TYPE, pTmp, strlen(pTmp));

   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->acDocDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->acSalePrice, SIZ_SALE1_AMT);

   memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->acSaleCode, SALE_SIZ_SALECODE);

   if (bSaleFlag)
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);
   else
      memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

   if (bSaleFlag)
   {
      // Only update transfer if current sale is newer
      //lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
      //if (lCurSaleDt >= lLstSaleDt)
      //{
      //   memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->acDocNum, SIZ_TRANSFER_DOC);
      //   memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->acDocDate, SIZ_TRANSFER_DT);
      //}

      *(pOutbuf+OFF_AR_CODE1) = 'A';    // Assessor
   } else
      *(pOutbuf+OFF_AR_CODE1) = 'R';    // Recorder

   return iTmp;
}

/*******************************************************************
 *
 * Filter out sale record that has APN and Sale price.
 * Return number of records output.
 *
 *******************************************************************/

int filterSale(char *pInfile, char *pOutfile, int fltFlag)
{
   SALE_REC *pRec;
   char     acRec[2048], acTmp[32], *pTmp;
   int      iRet=0, lTmp, iTmp;
   FILE     *fdIn, *fdOut;

   fdIn = fopen(pInfile, "r");
   if (!fdIn)
   {
      return -1;
   }
   fdOut = fopen(pOutfile, "w");
   if (!fdOut)
   {
      fclose(fdIn);
      return -2;
   }

   while (!feof(fdIn))
   {
      pRec = (SALE_REC *)fgets(acRec, 2048, fdIn);

      // Output record with APN and sale price only
      if (pRec && pRec->acApn[0] > ' ')
      {
         lTmp = atoin(pRec->acSalePrice, SALE_SIZ_SALEPRICE);
         if (lTmp > 0)
         {
            // Remove year in Doc# and make it left justify
            if (pRec->acDocNum[4] == '-')
               iTmp = 5;   // 2001-1234567
            else if (pRec->acDocNum[2] == '-')
               iTmp = 3;   // 94-1234567
            else
               iTmp = 0;   // 1234567

            memcpy(acTmp, (char *)&pRec->acDocNum[iTmp], SALE_SIZ_DOCNUM-iTmp);
            acTmp[SALE_SIZ_DOCNUM-iTmp] = 0;
            iTmp = 0;
            while (acTmp[iTmp] && acTmp[iTmp] <= '0')
               iTmp++;
            pTmp = (char *)&acTmp[iTmp];
            blankPad(pTmp, SALE_SIZ_DOCNUM);
            memcpy(pRec->acDocNum, pTmp, SALE_SIZ_DOCNUM);

            iRet++;
            fputs(acRec, fdOut);
         }
      }

   }

   fclose(fdIn);
   fclose(fdOut);

   return iRet;
}

// ????#ifndef _MERGESALE

/*****************************************************************************
 *
 * Extract all sales from current record.
 *
 * Return 0  = input file missing
 *        <0 = open file error
 *        >0 = successful and return number of output records
 *
 *****************************************************************************/

long ExtrCSale(char *pInfile, char *pOutfile, int iRecSize)
{
   int      iRet=0;
   long     lCnt=0;
   unsigned long nBytesRead, iLen;
   char     acBuf[2048], acOutbuf[512], *pInbuf, cFileCnt;

   HANDLE   fhIn;
   FILE     *fdOut;
   CSAL_REC *pRec = (CSAL_REC *)&acOutbuf;

   iLen = iRecSize;      // Default for R01 format
   // Check file is exist
   if (_access(pInfile, 0))
      return 0;

   LogMsg("Open input file %s", pInfile);
   if (INVALID_HANDLE_VALUE == (fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, 
       NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
      return -3;

   // Skip test record
   ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL);

   // Open Output file
   if (!(fdOut = fopen(pOutfile, "w")))
      return -4;

   pRec->CRLF[0] = '\n';
   pRec->CRLF[1] = 0;
   pInbuf = acBuf;
   cFileCnt = 1;
   while (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         pInfile[strlen(pInfile)-1] = cFileCnt | 0x30;
         if (!_access(pInfile, 0))
         {
            CloseHandle(fhIn);
            fhIn = 0;

            // Open next Input file
            LogMsg("Open input file %s", pInfile);
            fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }
            if (!ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
               break;
         } else
            break;
      }

      memset(acOutbuf, ' ', sizeof(CSAL_REC)-2);      // Do not overwrite CRLF
      if (acBuf[OFF_SALE3_DOC] > ' ' && acBuf[OFF_SALE3_DT] > ' ')
      {
         memcpy(pRec->Apn, pInbuf+OFF_APN_S, SALE_SIZ_APN);
         memcpy(pRec->DocNum, pInbuf+OFF_SALE3_DOC, SALE_SIZ_DOCNUM);
         memcpy(pRec->DocDate, pInbuf+OFF_SALE3_DT, SALE_SIZ_DOCDATE);
         memcpy(pRec->DocType, pInbuf+OFF_SALE3_DOCTYPE, SIZ_SALE1_DOCTYPE);
         memcpy(pRec->SalePrice, pInbuf+OFF_SALE3_AMT, SALE_SIZ_SALEPRICE);
         memcpy(pRec->SaleCode, pInbuf+OFF_SALE3_CODE, SALE_SIZ_SALECODE);
         fputs(acOutbuf, fdOut);
         memset(acOutbuf, ' ', sizeof(SALE_REC)-2);
         iRet++;
      }
      
      if (acBuf[OFF_SALE2_DOC] > ' ' && acBuf[OFF_SALE2_DT] > ' ')
      {
         memcpy(pRec->Apn, pInbuf+OFF_APN_S, SALE_SIZ_APN);
         memcpy(pRec->DocNum, pInbuf+OFF_SALE2_DOC, SALE_SIZ_DOCNUM);
         memcpy(pRec->DocDate, pInbuf+OFF_SALE2_DT, SALE_SIZ_DOCDATE);
         memcpy(pRec->DocType, pInbuf+OFF_SALE2_DOCTYPE, SIZ_SALE1_DOCTYPE);
         memcpy(pRec->SalePrice, pInbuf+OFF_SALE2_AMT, SALE_SIZ_SALEPRICE);
         memcpy(pRec->SaleCode, pInbuf+OFF_SALE2_CODE, SALE_SIZ_SALECODE);
         fputs(acOutbuf, fdOut);
         memset(acOutbuf, ' ', sizeof(SALE_REC)-2);
         iRet++;
      }

      if (acBuf[OFF_SALE1_DOC] > ' ' && acBuf[OFF_SALE1_DT] > ' ')
      {
         memcpy(pRec->Apn, pInbuf+OFF_APN_S, SALE_SIZ_APN);
         memcpy(pRec->Seller, pInbuf+OFF_SELLER, SIZ_SELLER);
         memcpy(pRec->DocNum, pInbuf+OFF_SALE1_DOC, SALE_SIZ_DOCNUM);
         memcpy(pRec->DocDate, pInbuf+OFF_SALE1_DT, SALE_SIZ_DOCDATE);
         memcpy(pRec->DocType, pInbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
         memcpy(pRec->SalePrice, pInbuf+OFF_SALE1_AMT, SALE_SIZ_SALEPRICE);
         memcpy(pRec->SaleCode, pInbuf+OFF_SALE1_CODE, SALE_SIZ_SALECODE);
         memcpy(pRec->Name1, pInbuf+OFF_NAME1, SIZ_NAME1);
         memcpy(pRec->Name2, pInbuf+OFF_NAME2, SIZ_NAME2);
         memcpy(pRec->CareOf, pInbuf+OFF_CARE_OF, SIZ_CARE_OF);
         if (*(pInbuf+OFF_M_ADDR_D) > ' ')
         {
            memcpy(pRec->MailAdr1, pInbuf+OFF_M_ADDR_D, SIZ_M_ADDR_D);
            memcpy(pRec->MailAdr2, pInbuf+OFF_M_CTY_ST_D, SIZ_M_CTY_ST_D);
         } else
         {
            memcpy(pRec->MailAdr1, pInbuf+OFF_M_STRNUM, SIZ_M_ADDR);
            memcpy(pRec->MailAdr2, pInbuf+OFF_M_CITY, SIZ_M_ADDR2);
         }
         memcpy(pRec->MailZip, pInbuf+OFF_M_ZIP, SIZ_M_ZIP);

         fputs(acOutbuf, fdOut);
         iRet++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fdOut)
      fclose(fdOut);

   return iRet;
}

char *fixNum(char *pOutbuf, char *pNumber, int iLen)
{
   char  *pRet = NULL;
   long  lPrice;

   lPrice = atoin(pNumber, iLen);
   *pOutbuf = '\0';
   if (lPrice > 0)
   {
      sprintf(pOutbuf, "%*d", iLen, lPrice);
      pRet = pOutbuf;
   } 

   return pRet;
}

/*****************************************************************************
 *
 * Extract all sales from current record.
 *
 * Return 0  = input file missing
 *        <0 = open file error
 *        >0 = successful and return number of output records
 *
 *****************************************************************************/

long ExtrHSale(char *pInfile, char *pOutfile, int iRecSize)
{
   int      iRet=0, iTmp;
   long     lCnt=0;
   unsigned long nBytesRead, iLen;
   char     acBuf[2048], acOutbuf[512], acTmp[64], *pInbuf, cFileCnt;

   HANDLE    fhIn;
   FILE      *fdOut;
   SCSAL_REC *pRec = (SCSAL_REC *)&acOutbuf;

   LogMsg("\nExtract history sale from %s using SCSAL_REC format to %s\n", pInfile, pOutfile);

   iLen = iRecSize;      // Default for R01 format
   // Check input file is exist
   if (_access(pInfile, 0))
   {
      LogMsg("***** Missing input file %s.", pInfile);
      return 0;
   }

   LogMsg("Open input file %s", pInfile);
   if (INVALID_HANDLE_VALUE == (fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, 
       NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
      return -3;

   // Skip test record
   ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL);

   // Open Output file
   if (!(fdOut = fopen(pOutfile, "w")))
      return -4;

   pRec->CRLF[0] = '\n';
   pRec->CRLF[1] = 0;
   pInbuf = acBuf;
   cFileCnt = 1;
   while (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         pInfile[strlen(pInfile)-1] = cFileCnt | 0x30;
         if (!_access(pInfile, 0))
         {
            CloseHandle(fhIn);
            fhIn = 0;

            // Open next Input file
            LogMsg("Open input file %s", pInfile);
            fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }
            if (!ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
               break;
         } else
            break;
      }

#ifdef _DEBUG
      //if (!memcmp(pInbuf, "009600434", 9))
      //   iTmp = 0;
#endif
      memset(acOutbuf, ' ', sizeof(SCSAL_REC)-2);      // Do not overwrite CRLF
      if ((acBuf[OFF_SALE3_DOC] > ' ' || acBuf[OFF_SALE3_DOC+7] > ' ') && acBuf[OFF_SALE3_DT] > ' ')
      {
         memcpy(pRec->Apn, pInbuf+OFF_APN_S, SALE_SIZ_APN);
         memcpy(pRec->DocNum, pInbuf+OFF_SALE3_DOC, SALE_SIZ_DOCNUM);
         memcpy(pRec->DocDate, pInbuf+OFF_SALE3_DT, SALE_SIZ_DOCDATE);
         memcpy(acTmp, pInbuf+OFF_SALE3_DOCTYPE, SIZ_SALE1_DOCTYPE);
         if (acTmp[0] >= 'A')
         {
            iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
            if (iTmp > 0)
            {
               iTmp = sprintf(acTmp, "%d", iTmp);
               memcpy(pRec->DocType, acTmp, iTmp);
            }
         } else if (isdigit(acTmp[0]))
            memcpy(pRec->DocType, pInbuf+OFF_SALE3_DOCTYPE, SIZ_SALE1_DOCTYPE);

         if (fixNum(acTmp, pInbuf+OFF_SALE3_AMT, SALE_SIZ_SALEPRICE))
         {
            memcpy(pRec->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
            memcpy(pRec->SaleCode, pInbuf+OFF_SALE3_CODE, SALE_SIZ_SALECODE);
         }

         //memcpy(pRec->acSalePrice, pInbuf+OFF_SALE3_AMT, SALE_SIZ_SALEPRICE);
         //memcpy(pRec->acSaleCode, pInbuf+OFF_SALE3_CODE, SALE_SIZ_SALECODE);
         fputs(acOutbuf, fdOut);
         memset(acOutbuf, ' ', sizeof(SCSAL_REC)-2);
         iRet++;
      }
      
      if ((acBuf[OFF_SALE2_DOC] > ' ' || acBuf[OFF_SALE2_DOC+7] > ' ') && acBuf[OFF_SALE2_DT] > ' ')
      {
         memcpy(pRec->Apn, pInbuf+OFF_APN_S, SALE_SIZ_APN);
         memcpy(pRec->DocNum, pInbuf+OFF_SALE2_DOC, SALE_SIZ_DOCNUM);
         memcpy(pRec->DocDate, pInbuf+OFF_SALE2_DT, SALE_SIZ_DOCDATE);
         memcpy(acTmp, pInbuf+OFF_SALE2_DOCTYPE, SIZ_SALE1_DOCTYPE);
         if (acTmp[0] >= 'A')
         {
            iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
            if (iTmp > 0)
            {
               iTmp = sprintf(acTmp, "%d", iTmp);
               memcpy(pRec->DocType, acTmp, iTmp);
            }
         } else if (isdigit(acTmp[0]))
            memcpy(pRec->DocType, pInbuf+OFF_SALE2_DOCTYPE, SIZ_SALE1_DOCTYPE);

         if (fixNum(acTmp, pInbuf+OFF_SALE2_AMT, SALE_SIZ_SALEPRICE))
         {
            memcpy(pRec->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
            memcpy(pRec->SaleCode, pInbuf+OFF_SALE2_CODE, SALE_SIZ_SALECODE);
         }
         //memcpy(pRec->acSalePrice, pInbuf+OFF_SALE2_AMT, SALE_SIZ_SALEPRICE);
         //memcpy(pRec->acSaleCode, pInbuf+OFF_SALE2_CODE, SALE_SIZ_SALECODE);
         fputs(acOutbuf, fdOut);
         memset(acOutbuf, ' ', sizeof(SCSAL_REC)-2);
         iRet++;
      }

      if ((acBuf[OFF_SALE1_DOC] > ' ' || acBuf[OFF_SALE1_DOC+7] > ' ') && acBuf[OFF_SALE1_DT] > ' ')
      {
         memcpy(pRec->Apn, pInbuf+OFF_APN_S, SALE_SIZ_APN);
         memcpy(pRec->Seller1, pInbuf+OFF_SELLER, SIZ_SELLER);
         memcpy(pRec->DocNum, pInbuf+OFF_SALE1_DOC, SALE_SIZ_DOCNUM);
         memcpy(pRec->DocDate, pInbuf+OFF_SALE1_DT, SALE_SIZ_DOCDATE);
         memcpy(acTmp, pInbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
         if (acTmp[0] >= 'A')
         {
            iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
            if (iTmp > 0)
            {
               iTmp = sprintf(acTmp, "%d", iTmp);
               memcpy(pRec->DocType, acTmp, iTmp);
            }
         } else if (isdigit(acTmp[0]))
            memcpy(pRec->DocType, pInbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);

         if (fixNum(acTmp, pInbuf+OFF_SALE1_AMT, SALE_SIZ_SALEPRICE))
         {
            memcpy(pRec->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
            memcpy(pRec->SaleCode, pInbuf+OFF_SALE1_CODE, SALE_SIZ_SALECODE);
         }
         //memcpy(pRec->SalePrice, pInbuf+OFF_SALE1_AMT, SALE_SIZ_SALEPRICE);
         //memcpy(pRec->SaleCode, pInbuf+OFF_SALE1_CODE, SALE_SIZ_SALECODE);

         fputs(acOutbuf, fdOut);
         iRet++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("      records extracted: %u", iRet);

   return iRet;
}

long ExtrHSale1(char *pInfile, char *pOutfile, int iRecSize)
{
   int      iRet=0, iTmp;
   long     lCnt=0, lDate;
   unsigned long nBytesRead, iLen;
   char     acBuf[2048], acOutbuf[512], acTmp[256], *pInbuf, cFileCnt;

   HANDLE   fhIn;
   FILE     *fdOut;
   SALE_REC1 *pRec = (SALE_REC1 *)&acOutbuf;

   LogMsg("\nExtract history sale from %s using SALE_REC1 format to %s\n", pInfile, pOutfile);

   iLen = iRecSize;      // Default for R01 format
   // Check input file is exist
   if (_access(pInfile, 0))
   {
      LogMsg("***** Missing input file %s.", pInfile);
      return 0;
   }

   LogMsg("Open input file %s", pInfile);
   if (INVALID_HANDLE_VALUE == (fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, 
       NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
      return -3;

   // Skip test record
   ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL);

   // Open Output file
   if (!(fdOut = fopen(pOutfile, "w")))
      return -4;

   pRec->CRLF[0] = '\n';
   pRec->CRLF[1] = 0;
   pInbuf = acBuf;
   cFileCnt = 1;
   while (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         pInfile[strlen(pInfile)-1] = cFileCnt | 0x30;
         if (!_access(pInfile, 0))
         {
            CloseHandle(fhIn);
            fhIn = 0;

            // Open next Input file
            LogMsg("Open input file %s", pInfile);
            fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }
            if (!ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
               break;
         } else
            break;
      }

      memset(acOutbuf, ' ', sizeof(SALE_REC1)-2);      // Do not overwrite CRLF
      lDate = atoin(&acBuf[OFF_SALE3_DT], 8);
      if ((acBuf[OFF_SALE3_DOC] > ' ' || acBuf[OFF_SALE3_DOC+7] > ' ') && (lDate > 1900) && (lDate < lToday))
      {
         memcpy(pRec->acApn, pInbuf+OFF_APN_S, SALE_SIZ_APN);
         memcpy(pRec->acDocNum, pInbuf+OFF_SALE3_DOC, SALE_SIZ_DOCNUM);
         memcpy(pRec->acDocDate, pInbuf+OFF_SALE3_DT, SALE_SIZ_DOCDATE);
         memcpy(acTmp, pInbuf+OFF_SALE3_DOCTYPE, SIZ_SALE1_DOCTYPE);
         if (acTmp[0] >= 'A')
         {
            iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
            if (iTmp > 0)
            {
               iTmp = sprintf(acTmp, "%d", iTmp);
               memcpy(pRec->acDocType, acTmp, iTmp);
            }
         } else if (isdigit(acTmp[0]))
            memcpy(pRec->acDocType, pInbuf+OFF_SALE3_DOCTYPE, SIZ_SALE1_DOCTYPE);

         if (fixNum(acTmp, pInbuf+OFF_SALE3_AMT, SALE_SIZ_SALEPRICE))
         {
            memcpy(pRec->acSalePrice, acTmp, SALE_SIZ_SALEPRICE);
            memcpy(pRec->acSaleCode, pInbuf+OFF_SALE3_CODE, SALE_SIZ_SALECODE);
         }

         fputs(acOutbuf, fdOut);
         memset(acOutbuf, ' ', sizeof(SALE_REC)-2);
         iRet++;
      }
      
      lDate = atoin(&acBuf[OFF_SALE2_DT], 8);
      if ((acBuf[OFF_SALE2_DOC] > ' ' || acBuf[OFF_SALE2_DOC+7] > ' ') && (lDate > 1900) && (lDate < lToday))
      {
         memcpy(pRec->acApn, pInbuf+OFF_APN_S, SALE_SIZ_APN);
         memcpy(pRec->acDocNum, pInbuf+OFF_SALE2_DOC, SALE_SIZ_DOCNUM);
         memcpy(pRec->acDocDate, pInbuf+OFF_SALE2_DT, SALE_SIZ_DOCDATE);
         memcpy(acTmp, pInbuf+OFF_SALE2_DOCTYPE, SIZ_SALE1_DOCTYPE);
         if (acTmp[0] >= 'A')
         {
            iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
            if (iTmp > 0)
            {
               iTmp = sprintf(acTmp, "%d", iTmp);
               memcpy(pRec->acDocType, acTmp, iTmp);
            }
         } else if (isdigit(acTmp[0]))
            memcpy(pRec->acDocType, pInbuf+OFF_SALE2_DOCTYPE, SIZ_SALE1_DOCTYPE);

         if (fixNum(acTmp, pInbuf+OFF_SALE2_AMT, SALE_SIZ_SALEPRICE))
         {
            memcpy(pRec->acSalePrice, acTmp, SALE_SIZ_SALEPRICE);
            memcpy(pRec->acSaleCode, pInbuf+OFF_SALE2_CODE, SALE_SIZ_SALECODE);
         }

         fputs(acOutbuf, fdOut);
         memset(acOutbuf, ' ', sizeof(SALE_REC)-2);
         iRet++;
      }

      lDate = atoin(&acBuf[OFF_SALE1_DT], 8);
      if ((acBuf[OFF_SALE1_DOC] > ' ' || acBuf[OFF_SALE1_DOC+7] > ' ') && (lDate > 1900) && (lDate < lToday))
      {
         memcpy(pRec->acApn, pInbuf+OFF_APN_S, SALE_SIZ_APN);
         memcpy(pRec->acSeller, pInbuf+OFF_SELLER, SIZ_SELLER);
         memcpy(pRec->acDocNum, pInbuf+OFF_SALE1_DOC, SALE_SIZ_DOCNUM);
         memcpy(pRec->acDocDate, pInbuf+OFF_SALE1_DT, SALE_SIZ_DOCDATE);
         memcpy(acTmp, pInbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
         if (acTmp[0] >= 'A')
         {
            iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
            if (iTmp > 0)
            {
               iTmp = sprintf(acTmp, "%d", iTmp);
               memcpy(pRec->acDocType, acTmp, iTmp);
            }
         } else if (isdigit(acTmp[0]))
            memcpy(pRec->acDocType, pInbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);

         if (fixNum(acTmp, pInbuf+OFF_SALE1_AMT, SALE_SIZ_SALEPRICE))
         {
            memcpy(pRec->acSalePrice, acTmp, SALE_SIZ_SALEPRICE);
            memcpy(pRec->acSaleCode, pInbuf+OFF_SALE1_CODE, SALE_SIZ_SALECODE);
         }

         memcpy(pRec->acName1, pInbuf+OFF_NAME1, SIZ_NAME1);
         memcpy(pRec->acName2, pInbuf+OFF_NAME2, SIZ_NAME2);

         fputs(acOutbuf, fdOut);
         iRet++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("      records extracted: %u", iRet);

   return iRet;
}

/*****************************************************************************
 *
 * Extract all sales from current record.
 *
 * Return 0  = input file missing
 *        <0 = open file error
 *        >0 = successful and return number of output records
 *
 *****************************************************************************/

long ExtrXfer(char *pInfile, char *pOutfile, int iRecSize)
{
   int      iRet=0, iTmp;
   long     lCnt=0;
   unsigned long nBytesRead, iLen;
   char     acBuf[2048], acOutbuf[1024], acTmp[64], *pInbuf, cFileCnt;

   HANDLE   fhIn;
   FILE     *fdOut;
   XFER_REC *pRec = (XFER_REC *)&acOutbuf;

   iLen = iRecSize;      // Default for R01 format
   // Check file is exist
   if (_access(pInfile, 0))
      return 0;

   LogMsg("Extract sale info from %s to %s", pInfile, pOutfile);
   LogMsg("Open input file %s", pInfile);
   if (INVALID_HANDLE_VALUE == (fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, 
       NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
      return -3;

   // Skip test record
   ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL);

   // Open Output file
   if (!(fdOut = fopen(pOutfile, "w")))
      return -4;

   pRec->CRLF[0] = '\n';
   pRec->CRLF[1] = 0;
   pInbuf = acBuf;
   cFileCnt = 1;
   while (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         pInfile[strlen(pInfile)-1] = cFileCnt | 0x30;
         if (!_access(pInfile, 0))
         {
            CloseHandle(fhIn);
            fhIn = 0;

            // Open next Input file
            LogMsg("Open input file %s", pInfile);
            fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }
            if (!ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
               break;
         } else
            break;
      }

#ifdef _DEBUG
      //if (!memcmp(pInbuf, "009600434", 9))
      //   iTmp = 0;
#endif
      memset(acOutbuf, ' ', sizeof(XFER_REC)-2);      // Do not overwrite CRLF
      memcpy(pRec->acApn, pInbuf+OFF_APN_S, SALE_SIZ_APN);
      memcpy(pRec->acXferDocNum, pInbuf+OFF_TRANSFER_DOC, SALE_SIZ_DOCNUM);
      memcpy(pRec->acXferDocDate, pInbuf+OFF_TRANSFER_DT, SALE_SIZ_DOCDATE);

      if ((acBuf[OFF_SALE3_DOC] > ' ' || acBuf[OFF_SALE3_DOC+7] > ' ') && acBuf[OFF_SALE3_DT] > ' ')
      {
         memcpy(pRec->acDoc3Num, pInbuf+OFF_SALE3_DOC, SALE_SIZ_DOCNUM);
         memcpy(pRec->acDoc3Date, pInbuf+OFF_SALE3_DT, SALE_SIZ_DOCDATE);
         memcpy(acTmp, pInbuf+OFF_SALE3_DOCTYPE, SIZ_SALE1_DOCTYPE);
         if (acTmp[0] >= 'A')
         {
            iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
            if (iTmp > 0)
            {
               iTmp = sprintf(acTmp, "%d", iTmp);
               memcpy(pRec->acDoc3Type, acTmp, iTmp);
            }
         } else if (isdigit(acTmp[0]))
            memcpy(pRec->acDoc3Type, pInbuf+OFF_SALE3_DOCTYPE, SIZ_SALE1_DOCTYPE);

         memcpy(pRec->acSale3Price, pInbuf+OFF_SALE3_AMT, SALE_SIZ_SALEPRICE);
         memcpy(pRec->acSale3Code, pInbuf+OFF_SALE3_CODE, SALE_SIZ_SALECODE);
      }
      
      if ((acBuf[OFF_SALE2_DOC] > ' ' || acBuf[OFF_SALE2_DOC+7] > ' ') && acBuf[OFF_SALE2_DT] > ' ')
      {
         memcpy(pRec->acDoc2Num, pInbuf+OFF_SALE2_DOC, SALE_SIZ_DOCNUM);
         memcpy(pRec->acDoc2Date, pInbuf+OFF_SALE2_DT, SALE_SIZ_DOCDATE);
         memcpy(acTmp, pInbuf+OFF_SALE2_DOCTYPE, SIZ_SALE1_DOCTYPE);
         if (acTmp[0] >= 'A')
         {
            iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
            if (iTmp > 0)
            {
               iTmp = sprintf(acTmp, "%d", iTmp);
               memcpy(pRec->acDoc2Type, acTmp, iTmp);
            }
         } else if (isdigit(acTmp[0]))
            memcpy(pRec->acDoc2Type, pInbuf+OFF_SALE2_DOCTYPE, SIZ_SALE1_DOCTYPE);

         memcpy(pRec->acSale2Price, pInbuf+OFF_SALE2_AMT, SALE_SIZ_SALEPRICE);
         memcpy(pRec->acSale2Code, pInbuf+OFF_SALE2_CODE, SALE_SIZ_SALECODE);
      }

      if ((acBuf[OFF_SALE1_DOC] > ' ' || acBuf[OFF_SALE1_DOC+7] > ' ') && acBuf[OFF_SALE1_DT] > ' ')
      {
         memcpy(pRec->acSeller, pInbuf+OFF_SELLER, SIZ_SELLER);
         memcpy(pRec->acDoc1Num, pInbuf+OFF_SALE1_DOC, SALE_SIZ_DOCNUM);
         memcpy(pRec->acDoc1Date, pInbuf+OFF_SALE1_DT, SALE_SIZ_DOCDATE);
         memcpy(acTmp, pInbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
         if (acTmp[0] >= 'A')
         {
            iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
            if (iTmp > 0)
            {
               iTmp = sprintf(acTmp, "%d", iTmp);
               memcpy(pRec->acDoc1Type, acTmp, iTmp);
            }
         } else if (isdigit(acTmp[0]))
            memcpy(pRec->acDoc1Type, pInbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
         memcpy(pRec->acSale1Price, pInbuf+OFF_SALE1_AMT, SALE_SIZ_SALEPRICE);
         memcpy(pRec->acSale1Code, pInbuf+OFF_SALE1_CODE, SALE_SIZ_SALECODE);

      }

      if (pRec->acXferDocDate[0] > ' ' || pRec->acDoc1Date[0] > ' ')
      {
         fputs(acOutbuf, fdOut);
         iRet++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("      records extracted: %u", iRet);

   return iRet;
}

/********************************** MergeSaleX *******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update seller if that is the sale from assessor.
 * This is the modified version of MergeSale() which will not update sale in 
 * the same date without SaleAmt.
 *
 *****************************************************************************/

int MergeSaleX(CSAL_REC *pSaleRec, char *pOutbuf, bool bSaleFlag)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   char  *pTmp, acTmp[32];

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   if (lCurSaleDt < lLstSaleDt)
      return -1;

   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      lPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT); 
      if (!lPrice && lLastAmt > 0)
         return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE3_DT);
   if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, 3);
   else if (pTmp=findDocType(pSaleRec->DocType, acTmp))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pTmp, strlen(pTmp));

   // Remove sale code - 
   // Notes: SALE_SIZ_SALECODE is smaller than  SIZ_SALE1_CODE.  Don't swap them.
   if (pSaleRec->SaleCode[0] >= '0')
      memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, SALE_SIZ_SALECODE);
   else
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);

   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

   // Seller
   if (bSaleFlag && pSaleRec->Seller[0] > ' ')
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller, SIZ_SELLER);

   // Update transfers
   //lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   //if (lCurSaleDt >= lLstSaleDt)
   //{
   //   memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
   //   memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   //}
   return 1;
}

/******************************** MergeSaleXRec ******************************
 *
 *
 *****************************************************************************/

int MergeSaleXRec(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iLoop;
   CSAL_REC *pSale;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdSale);

   pSale = (CSAL_REC *)&acRec[0];
   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->Apn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", iApnLen, pSale->Apn);
         pRec = fgets(acRec, 1024, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "004020015", 9))
      //   iRet = 0;
#endif

   // Merge sale
   while (!iLoop)
   {
      // Merge sale rec
      MergeSaleX(pSale, pOutbuf);

      // Get next sale record
      pRec = fgets(acRec, 1024, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

      iLoop = memcmp(pOutbuf, pSale->Apn, iApnLen);
   }

   lSaleMatch++;
  
   return 0;
}

/******************************* MergeSaleXFile ******************************
 *
 * Merge extracted sale data into current sale.  
 *
 *****************************************************************************/

int MergeSaleXFile(char *pInfile, char *pOutfile, char *pXSalefile)
{
   int      iRet=0, iUpdCnt=0;
   long     lCnt=0;
   unsigned long nBytesRead, nBytesWritten;
   char     acBuf[2048], acSaleRec[512], *pInbuf, cFileCnt;

   HANDLE   fhIn, fhOut;
   CSAL_REC *pRec = (CSAL_REC *)&acSaleRec;

   // Check file is exist
   if (_access(pInfile, 0))
      return 0;

   // Open input file
   LogMsg("Open input file %s", pInfile);
   if (INVALID_HANDLE_VALUE == (fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, 
       NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
      return -3;

   // Open output file
   LogMsg("Open output file %s", pOutfile);
   if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0, 
       NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
      return -4;

   // Open sale file
   if (!(fdSale = fopen(pXSalefile, "r")))
      return -4;


   // Read record, no sale update for this one
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   pInbuf = acBuf;
   cFileCnt = 1;
   lSaleSkip=lSaleMatch=0;
   while (ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         pInfile[strlen(pInfile)-1] = cFileCnt | 0x30;
         pOutfile[strlen(pOutfile)-1] = cFileCnt | 0x30;
         if (!_access(pInfile, 0))
         {
            CloseHandle(fhIn);
            CloseHandle(fhOut);
            fhIn = fhOut = 0;

            // Open next Input file
            LogMsg("Open input file %s", pInfile);
            fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }

            if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0, 
                NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
            {
               iRet = -4;
               fhOut = 0;
               break;
            }

            if (!ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
               break;
         } else
            break;
      }

      // Update sale
      if (fdSale)
         if (!MergeSaleXRec(acBuf))
            iUpdCnt++;

      // Write output record
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fhOut)
      CloseHandle(fhOut);
   if (fdSale)
      fclose(fdSale);

   if (!iRet)
      iRet = iUpdCnt;
   return iRet;
}

/******************************** MergeXferRec *******************************
 *
 *
 *****************************************************************************/

int MergeXferRec(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iLoop;
   XFER_REC *pSale;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdSale);

   pSale = (XFER_REC *)&acRec[0];
   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->acApn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", iApnLen, pSale->acApn);
         pRec = fgets(acRec, 1024, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "8764014005", 10))
   //   iLoop = 0;
#endif

   // Merge sale rec
   memcpy(pOutbuf+OFF_SALE3_DOC,     pSale->acDoc3Num,    SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT,      pSale->acDoc3Date,   SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pSale->acDoc3Type,   SIZ_SALE3_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT,     pSale->acSale3Price, SIZ_SALE3_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE,    pSale->acSale3Code,  SALE_SIZ_SALECODE);

   memcpy(pOutbuf+OFF_SALE2_DOC,     pSale->acDoc2Num,    SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT,      pSale->acDoc2Date,   SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pSale->acDoc2Type,   SIZ_SALE3_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT,     pSale->acSale2Price, SIZ_SALE3_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE,    pSale->acSale2Code,  SALE_SIZ_SALECODE);

   memcpy(pOutbuf+OFF_SALE1_DOC,     pSale->acDoc1Num,    SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT,      pSale->acDoc1Date,   SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSale->acDoc1Type,   SIZ_SALE3_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE1_AMT,     pSale->acSale1Price, SIZ_SALE3_AMT);
   memcpy(pOutbuf+OFF_SALE1_CODE,    pSale->acSale1Code,  SALE_SIZ_SALECODE);

   // Seller
   if (pSale->acSeller[0] > ' ')
      memcpy(pOutbuf+OFF_SELLER, pSale->acSeller, SIZ_SELLER);

   // Update transfers
   memcpy(pOutbuf+OFF_TRANSFER_DOC, pSale->acXferDocNum, SIZ_TRANSFER_DOC);
   memcpy(pOutbuf+OFF_TRANSFER_DT,  pSale->acXferDocDate, SIZ_TRANSFER_DT);

   // Get next sale record
   pRec = fgets(acRec, 1024, fdSale);
   if (!pRec)
   {
      fclose(fdSale);
      fdSale = NULL;
   }

   lSaleMatch++;
  
   return 0;
}

/******************************* MergeXferFile *******************************
 *
 * Merge extracted sale data into current sale.  
 * Return < 0 if error. 
 *        = 0 if sale file missing
 *        > 0 if success (number of updated records
 *
 *****************************************************************************/

int MergeXferFile(char *pInfile, char *pOutfile, char *pXferfile)
{
   int      iRet=0, iUpdCnt=0;
   long     lCnt=0;
   unsigned long nBytesRead, nBytesWritten;
   char     acBuf[2048], *pInbuf, cFileCnt;

   HANDLE   fhIn, fhOut;

   // Check file is exist
   if (_access(pInfile, 0))
      return 0;

   LogMsg("Merge sale info from %s to %s", pXferfile, pInfile);

   // Open input file
   LogMsg("Open input file %s", pInfile);
   if (INVALID_HANDLE_VALUE == (fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, 
       NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
   {
      LogMsg("***** Error opening input file: %s", pInfile);
      return -3;
   }

   // Open output file
   LogMsg("Open output file %s", pOutfile);
   if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0, 
       NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
   {
      LogMsg("***** Error creating output file: %s", pOutfile);
      return -4;
   }

   // Open sale file
   if (!(fdSale = fopen(pXferfile, "r")))
   {
      LogMsg("***** Error opening transfer file: %s", pXferfile);
      return -5;
   }

   // Read record, no sale update for this one
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   pInbuf = acBuf;
   cFileCnt = 1;
   lSaleSkip=lSaleMatch=0;
   while (ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhIn = fhOut = 0;
         if (!iRet && iUpdCnt > 0)
         {
            // Remove R0? file
            remove(pInfile);
            // Rename N0? to R01 file
            rename(pOutfile, pInfile);
         } else
         {
            LogMsg("***** Something wrong in merging transfer data.  Please check !!!");
            break;
         }

         // EOF
         cFileCnt++;
         pInfile[strlen(pInfile)-1] = cFileCnt | 0x30;
         pOutfile[strlen(pOutfile)-1] = cFileCnt | 0x30;
         if (!_access(pInfile, 0))
         {

            // Open next Input file
            LogMsg("Open input file %s", pInfile);
            fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }

            LogMsg("Open output file %s", pOutfile);
            if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0, 
                NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
            {
               iRet = -4;
               fhOut = 0;
               break;
            }

            if (!ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
               break;
         } else
            break;
      }

      // Update sale
      if (fdSale)
         if (!MergeXferRec(acBuf))
            iUpdCnt++;

      // Write output record
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fhOut)
      CloseHandle(fhOut);
   if (fdSale)
      fclose(fdSale);

   if (!iRet)
      iRet = iUpdCnt;
   
   LogMsg("Total records processed: %u", lCnt);
   LogMsg("      records updated:   %u", iRet);

   return iRet;
}

/******************************** MergeHistSale ******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * 07/24/2007 Merge seller to support BUT
 *
 *****************************************************************************/

static void MergeHistSale(SALE_REC *pSaleRec, char *pOutbuf)
{
   int   iTmp;
   long  lCurSaleDt, lLstSaleDt;
   char  acDocNum[32];

 
   lCurSaleDt = atoin(pSaleRec->acDocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   // If current sale is older than last sale, clear all sales
   if (lCurSaleDt < lLstSaleDt)
      ClearOldSale(pOutbuf);
   else if (lCurSaleDt == lLstSaleDt)
   {
      iTmp = memcmp(pSaleRec->acDocNum, pOutbuf+OFF_SALE1_DOC, sizeof(pSaleRec->acDocNum));
      if (iTmp < 0)
         ClearOldSale(pOutbuf);
      else 
      {  
         if (iTmp > 0)
         {
            // Update Sale1 - Multi transfer in same day
            memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->acDocNum, sizeof(pSaleRec->acDocNum));
            memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->acDocType, sizeof(pSaleRec->acDocType));
            if (pSaleRec->acSeller[0] > ' ')
               memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);
            iTmp = atoin(pSaleRec->acSalePrice, SIZ_SALE1_AMT);
            if (iTmp > 0)
               memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->acSalePrice, SIZ_SALE1_AMT);
         }
         return;
      }
   }

   // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE2_CODE);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE1_CODE);

   // Format DocNum
   //if (!isdigit(pSaleRec->acDocNum[0]) || !isdigit(pSaleRec->acDocNum[4]))
      memcpy(acDocNum, pSaleRec->acDocNum, sizeof(pSaleRec->acDocNum));
   //else
   //{
   //   // This block should be removed - only use when neccessary
   //   iTmp = atoin(pSaleRec->acDocNum, 8);
   //   sprintf(acDocNum, "%.8d", iTmp);
   //   blankPad(acDocNum, sizeof(pSaleRec->acDocNum));
   //}

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, sizeof(pSaleRec->acDocNum));
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->acDocDate, sizeof(pSaleRec->acDocDate));
   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->acDocType, sizeof(pSaleRec->acDocType));

   // Merge sale code
   if (pSaleRec->acSaleCode[0] > '0')
      memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->acSaleCode, sizeof(pSaleRec->acSaleCode));
   else
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);

   // Merge seller
   if (pSaleRec->acSeller[0] > ' ')
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);
   else if (*(pOutbuf+OFF_SELLER) > ' ')
      memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

   // Update SalePrice
   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->acSalePrice, SIZ_SALE1_AMT);

   // Update transfers
   //lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
   //if (lCurSaleDt > lLstSaleDt)
   //{
   //   memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC);
   //   memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->acDocDate, SIZ_TRANSFER_DT);
   //}
}

/******************************** MergeHistSale1 *****************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * This is generic version and can be used by any county.  It won't reset old
 * sales like MergeHistSale() or reformat DocNum.
 *
 *****************************************************************************/

static void MergeHistSale1(char *pSale, char *pOutbuf, bool bNoPrice=false, bool bUpdtXfer=false)
{
   int         iTmp;
   long        lCurSaleDt, lLstSaleDt;
   SALE_REC1   *pSaleRec = (SALE_REC1 *)pSale;

 
   lCurSaleDt = atoin(pSaleRec->acDocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   // If current sale is older than last sale or newer than today, return
   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return;
   else if (lCurSaleDt == lLstSaleDt)
   {
      iTmp = memcmp(pSaleRec->acDocNum, pOutbuf+OFF_SALE1_DOC, sizeof(pSaleRec->acDocNum));
      if (iTmp < 0)
         return;
      else 
      {  
         if (iTmp > 0)
         {
            // Update Sale1 - Multi transfer in same day
            memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->acDocNum, sizeof(pSaleRec->acDocNum));
            memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->acDocType, sizeof(pSaleRec->acDocType));
            if (pSaleRec->acSeller[0] > ' ')
               memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);

            // If county request not to put sale price out, don't update sale price
            if (!bNoPrice)
            {
               iTmp = atoin(pSaleRec->acSalePrice, SIZ_SALE1_AMT);
               if (iTmp > 0)
                  memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->acSalePrice, SIZ_SALE1_AMT);
            }

            // Update transfers - to synchronize with Sale1
            if (bUpdtXfer)
            {
               lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
               if (lCurSaleDt >= lLstSaleDt)
               {
                  memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->acDocNum, SIZ_TRANSFER_DOC);
                  memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->acDocDate, SIZ_TRANSFER_DT);
               }
            }
         } else if (!bNoPrice)
         {
            iTmp = atoin(pSaleRec->acSalePrice, SIZ_SALE1_AMT);
            if (iTmp > 1000)
               memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->acSalePrice, SIZ_SALE1_AMT);
         }
         return;
      }
   }

   // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   if (!bNoPrice)
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE2_CODE);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   if (!bNoPrice)
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE1_CODE);

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->acDocNum, sizeof(pSaleRec->acDocNum));
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->acDocDate, sizeof(pSaleRec->acDocDate));
   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->acDocType, sizeof(pSaleRec->acDocType));

   // Merge sale code
   if (pSaleRec->acSaleCode[0] > '0')
      memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->acSaleCode, sizeof(pSaleRec->acSaleCode));
   else
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);

   // Merge seller
   if (pSaleRec->acSeller[0] > ' ')
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);
   else if (*(pOutbuf+OFF_SELLER) > ' ')
      memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

   // Update SalePrice
   if (!bNoPrice)
      memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->acSalePrice, SIZ_SALE1_AMT);

   // Update transfers
   if (bUpdtXfer)
   {
      lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
      if (lCurSaleDt >= lLstSaleDt)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->acDocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->acDocDate, SIZ_TRANSFER_DT);
      }
   }
}

/******************************** ApplySaleRec1 ******************************
 *
 * Copy new sale data into current sale.  Move other sales accordingly.
 * This version is used to copy cum sale into roll file.
 *
 *****************************************************************************/

int ApplySaleRec1(char *pOutbuf, char *pSale, bool bUpdtXfer=false)
{
   SALE_REC1   *pSaleRec = (SALE_REC1 *)pSale;

    // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE2_CODE);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE1_CODE);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   *(pOutbuf+OFF_AR_CODE1) = 'A';

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->acDocNum, sizeof(pSaleRec->acDocNum));
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->acDocDate, sizeof(pSaleRec->acDocDate));
   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->acDocType, sizeof(pSaleRec->acDocType));

   // Merge sale code
   if (pSaleRec->acSaleCode[0] > '0')
      memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->acSaleCode, sizeof(pSaleRec->acSaleCode));
   else
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);

   // Merge seller
   if (pSaleRec->acSeller[0] > ' ')
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);
   else if (*(pOutbuf+OFF_SELLER) > ' ')
      memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

   // Update SalePrice
   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->acSalePrice, SIZ_SALE1_AMT);

   // Update transfers
   if (bUpdtXfer)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->acDocNum, SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->acDocDate, SIZ_TRANSFER_DT);
   }

   return 0;
}

/******************************** ApplySCSalRec ******************************
 *
 * Apply history sales to R01 record.  Ignore known non-sale transactions. 
 * If bNoPrice is true, do not update sale price.
 * If bUpdtXfer is true, it's ok to update transfer.
 * If same date but different docnum, update with new doc data
 *    - 07/10/2013: Only when new docnum > current docnum.  This will show the latest
 *                  docnum on that date.
 *    - 07/24/2013: Fix problem when CurSaleDt > LstSaleDt and CurDocNum < LstDocNum
 *    - 05/05/2017: If SalePrice is 0, check ConfSalePrice
 *    - 09/03/2018: Cleanup old sale amt & salecode if not avail on new sale
 *    - 03/07/2019: Keep latest DocNum in Transfer Doc
 *    - 01/05/2021: When sale rec has the same date as last record, only update SALE1
 *                  if it has sale price or DocType=1 or 13
 *
 * cDataSrc = A : Assessor (SALE file)
 *            R : Recorder (GRGR file)
 *
 *****************************************************************************/

int ApplySCSalRec(char *pOutbuf, char *pSale, bool bNoPrice, bool bUpdtXfer, char cDataSrc)
{
   ULONG       lCurPrice, lLastPrice, lSalePrice;
   int         iRet=1, iTmp, lCurSaleDt, lLstSaleDt;
   char        sTmp[64];
   SCSAL_REC   *pSaleRec = (SCSAL_REC *)pSale;

   if (pSaleRec->DocNum[0] < '0' || pSaleRec->DocDate[0] < '0')
      return iRet;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "3082 018", 8))
   //   iTmp = 0;
#endif

   // Update transfers
   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   if (bUpdtXfer)
   {
      lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
      if (lCurSaleDt == lLstSaleDt)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
      } else if (lCurSaleDt > lLstSaleDt)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
      }
   }

   // Ignore known non-sale transactions
   if (pSaleRec->NoneSale_Flg == 'Y' || pSaleRec->XferType == 'T')
      return iRet;

   lCurPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
   lLastPrice = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);

   if (lCurPrice > 2147483647)
      LogMsg("*** Big sale price in [%.14s]: %u", pOutbuf, lCurPrice);

   if (pSaleRec->DocType[0] == 'G')
      memcpy(pSaleRec->DocType, "1  ", SIZ_SALE1_DOCTYPE);
   else if (pSaleRec->DocType[0] == 'Q')
      memcpy(pSaleRec->DocType, "4  ", SIZ_SALE1_DOCTYPE);

   // If current sale is older than last sale or newer than today, return
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return iRet;
   else if (lCurSaleDt == lLstSaleDt)
   {
      // 01/05/2021 spn
      if (cDataSrc != 'R' && (lCurPrice > 1000 || !memcmp(pSaleRec->DocType, "1 ", 2) || !memcmp(pSaleRec->DocType, "13", 2) ))
      {
         iTmp = memcmp(pSaleRec->DocNum, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
         if (iTmp > 0)
         {
            // Make sure Sale1 matches Transfer Doc#
            memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
            memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);
            if (pSaleRec->Seller1[0] > ' ')
               memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

            if (!bNoPrice)
            {
               iTmp = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
               if (iTmp > 1000)
               {
                  memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
                  *(pOutbuf+OFF_SALE1_CODE) = pSaleRec->SaleCode[0];
               } else if (bUseConfSalePrice)
               {
                  iTmp = atoin(pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);
                  if (iTmp > 1000)
                  {
                     memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);
                     *(pOutbuf+OFF_SALE1_CODE) = pSaleRec->SaleCode[0];
                  }
               }
            }
         }
      }
      return iRet;
   }

    // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   if (!bNoPrice)   
   {
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
      *(pOutbuf+OFF_SALE3_CODE) = *(pOutbuf+OFF_SALE2_CODE);
   }
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   if (!bNoPrice)   
   {
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      *(pOutbuf+OFF_SALE2_CODE) = *(pOutbuf+OFF_SALE1_CODE);
   }
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   *(pOutbuf+OFF_AR_CODE1) = cDataSrc;

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);

   if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);

   // Update SalePrice
   lSalePrice = lCurPrice;
   if (lSalePrice < 1000 && bUseConfSalePrice)
      lSalePrice = atoin(pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);

   if (lSalePrice > 0 && !bNoPrice)
   {
      *(pOutbuf+OFF_SALE1_CODE) = pSaleRec->SaleCode[0];
      sprintf(sTmp, "%*d", SIZ_SALE1_AMT, lSalePrice);
      memcpy(pOutbuf+OFF_SALE1_AMT, sTmp, SIZ_SALE1_AMT);
   } else
   {
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);
      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
   }

   // Merge seller
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

   if (pSaleRec->MultiSale_Flg == 'Y' || pSaleRec->NumOfPrclXfer[0] == 'M')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else if (!bKeepMAFlag)
      *(pOutbuf+OFF_MULTI_APN) = ' ';

   lSaleUpdate++;
   return 0;
}

/******************************** ApplySCSalRec ******************************
 *
 * Same as ApplySCSalRec() except it updates XFER's DocNum & DocDate only if newer date
 *
 *****************************************************************************/

int ApplySCSalRec_ChkXfer(char *pOutbuf, char *pSale, bool bNoPrice, bool bUpdtXfer, char cDataSrc)
{
   ULONG       lCurPrice, lLastPrice, lSalePrice;
   int         iRet=1, iTmp, lCurSaleDt, lLstSaleDt;
   char        sTmp[64];
   SCSAL_REC   *pSaleRec = (SCSAL_REC *)pSale;

   if (pSaleRec->DocNum[0] < '0' || pSaleRec->DocDate[0] < '0')
      return iRet;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0071C006", 8))
   //   iTmp = 0;
#endif

   // Update transfers
   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   if (bUpdtXfer)
   {
      lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
      if (lCurSaleDt > lLstSaleDt)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
      }
   }

   // Ignore known non-sale transactions
   if (pSaleRec->NoneSale_Flg == 'Y' || pSaleRec->XferType == 'T')
      return iRet;

   lCurPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
   lLastPrice = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);

   if (lCurPrice > 2147483647)
      LogMsg("*** Big sale price in [%.14s]: %u", pOutbuf, lCurPrice);

   if (pSaleRec->DocType[0] == 'G')
      memcpy(pSaleRec->DocType, "1  ", SIZ_SALE1_DOCTYPE);
   else if (pSaleRec->DocType[0] == 'Q')
      memcpy(pSaleRec->DocType, "4  ", SIZ_SALE1_DOCTYPE);

   // If current sale is older than last sale or newer than today, return
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return iRet;
   else if (lCurSaleDt == lLstSaleDt)
   {
      // 01/05/2021 spn
      if (cDataSrc != 'R' && (lCurPrice > 1000 || !memcmp(pSaleRec->DocType, "1 ", 2) || !memcmp(pSaleRec->DocType, "13", 2) ))
      {
         iTmp = memcmp(pSaleRec->DocNum, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
         if (iTmp > 0)
         {
            // Make sure Sale1 matches Transfer Doc#
            memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
            memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);
            if (pSaleRec->Seller1[0] > ' ')
               memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

            if (!bNoPrice)
            {
               iTmp = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
               if (iTmp > 1000)
               {
                  memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
                  *(pOutbuf+OFF_SALE1_CODE) = pSaleRec->SaleCode[0];
               } else if (bUseConfSalePrice)
               {
                  iTmp = atoin(pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);
                  if (iTmp > 1000)
                  {
                     memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);
                     *(pOutbuf+OFF_SALE1_CODE) = pSaleRec->SaleCode[0];
                  }
               }
            }
         } else if (cDataSrc == 'A')
         {
            *(pOutbuf+OFF_AR_CODE1) = cDataSrc;
         }
      } 

      return iRet;
   }

    // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   if (!bNoPrice)   
   {
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
      *(pOutbuf+OFF_SALE3_CODE) = *(pOutbuf+OFF_SALE2_CODE);
   }
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   if (!bNoPrice)   
   {
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      *(pOutbuf+OFF_SALE2_CODE) = *(pOutbuf+OFF_SALE1_CODE);
   }
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   *(pOutbuf+OFF_AR_CODE1) = cDataSrc;

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);

   if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);

   // Update SalePrice
   lSalePrice = lCurPrice;
   if (lSalePrice < 1000 && bUseConfSalePrice)
      lSalePrice = atoin(pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);

   if (lSalePrice > 0 && !bNoPrice)
   {
      *(pOutbuf+OFF_SALE1_CODE) = pSaleRec->SaleCode[0];
      sprintf(sTmp, "%*d", SIZ_SALE1_AMT, lSalePrice);
      memcpy(pOutbuf+OFF_SALE1_AMT, sTmp, SIZ_SALE1_AMT);
   } else
   {
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);
      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
   }

   // Merge seller
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

   if (pSaleRec->MultiSale_Flg == 'Y' || pSaleRec->NumOfPrclXfer[0] == 'M')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else if (!bKeepMAFlag)
      *(pOutbuf+OFF_MULTI_APN) = ' ';

   lSaleUpdate++;
   return 0;
}

/******************************** ApplySCSalRec ******************************
 *
 * Same as ApplySCSalRec() except it check year in DocNum & DocDate before update
 *
 *****************************************************************************/

int ApplySCSalRec_ChkDocYear(char *pOutbuf, char *pSale, bool bNoPrice, bool bUpdtXfer, char cDataSrc)
{
   long        lCurSaleDt, lLstSaleDt, iTmp;
   SCSAL_REC   *pSaleRec = (SCSAL_REC *)pSale;

   if (pSaleRec->DocNum[0] < '0' || pSaleRec->DocDate[0] < '0')
      return 1;

   // Update transfers
   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   if (bUpdtXfer)
   {
      lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
      if (lCurSaleDt == lLstSaleDt && !memcmp(pSaleRec->DocDate, pSaleRec->DocNum, 4))
      {
         if (memcmp(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC))
            memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
      } else 
      {
         if (lLstSaleDt > 0 && lCurSaleDt > lLstSaleDt && 
            memcmp(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC))
         {
            memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
            memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
         } else if (lLstSaleDt == 0 && lCurSaleDt > 19000101)
         {
            memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
            memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
         }
      }
   }

   // Ignore known non-sale transactions
   if (pSaleRec->NoneSale_Flg == 'Y' || pSaleRec->XferType == 'T')
      return 1;

   // If current sale is older than last sale or newer than today, return
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return 1;
   else if (lCurSaleDt == lLstSaleDt)
   {
      if (cDataSrc != 'R')
      {
         iTmp = memcmp(pSaleRec->DocNum, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
         if (iTmp > 0)
         {
            // Make sure Sale1 matches Transfer Doc#
            memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
            memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);
            memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, sizeof(pSaleRec->SaleCode));
            if (pSaleRec->Seller1[0] > ' ')
               memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

            if (!bNoPrice)
            {
               iTmp = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
               if (iTmp > 1000)
                  memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
            }
         }
      }
      return 1;
   }

   // Check Year - If same doc but different year, ignore it.
   if (memcmp(pSaleRec->DocDate, pSaleRec->DocNum, 4) &&
      !memcmp(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC))
      return 1;

   // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   if (!bNoPrice)   
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE2_CODE);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   if (!bNoPrice)   
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE1_CODE);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   *(pOutbuf+OFF_AR_CODE1) = cDataSrc;

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);

   if (pSaleRec->DocType[0] == 'G')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '1';
   else if (pSaleRec->DocType[0] == 'Q')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '4';
   else if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);

   // Merge sale code
   memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, sizeof(pSaleRec->SaleCode));

   // Merge seller
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

   // Update SalePrice
   if (!bNoPrice)
      memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

   if (pSaleRec->MultiSale_Flg == 'Y' || pSaleRec->NumOfPrclXfer[0] == 'M')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else if (!bKeepMAFlag)
      *(pOutbuf+OFF_MULTI_APN) = ' ';

   lSaleUpdate++;
   return 0;
}

/******************************** ApplySCSalRec ******************************
 *
 * Same as above function.  Except it won't check for DOCNUM
 * Used by SBD, SFX
 *
 *****************************************************************************/

int ApplySCSalRec_NoDocChk(char *pOutbuf, char *pSale, bool bNoPrice, bool bUpdtXfer, char cDataSrc)
{
   long        lCurSaleDt, lLstSaleDt, iTmp;
   SCSAL_REC   *pSaleRec = (SCSAL_REC *)pSale;
   int         iRet=1;

   // Ignore record without RecDate
   if (pSaleRec->DocDate[0] < '0')
      return iRet;
   
#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0211222640000", 9))
   //   iTmp = 0;
#endif

   // Ignore record without sale price and DocNum
   iTmp = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
   if (!iTmp && pSaleRec->DocNum[0] < '0')
      return iRet;

   // Update transfers
   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   if (bUpdtXfer)
   {
      lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
      if (lCurSaleDt > lLstSaleDt)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
      }
   }

   // Ignore known non-sale transactions
   if (pSaleRec->NoneSale_Flg == 'Y' || pSaleRec->XferType == 'T')
      return iRet;

   // If current sale is older than last sale or newer than today, return
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return iRet;
   else if (lCurSaleDt == lLstSaleDt)
   {
      if (cDataSrc != 'R')
      {
         iTmp = memcmp(pSaleRec->DocNum, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
         if (iTmp != 0)
         {
            // Make sure Sale1 matches Transfer Doc#
            memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
            memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);
         }

         if (!bNoPrice && *(pOutbuf+OFF_SALE1_AMT+8) > ' ')
         {
            memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
         }
      }
      return iRet;
   }

    // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   if (!bNoPrice)   
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE2_CODE);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   if (!bNoPrice)   
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE1_CODE);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   *(pOutbuf+OFF_AR_CODE1) = cDataSrc;

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);

   if (pSaleRec->DocType[0] == 'G')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '1';
   else if (pSaleRec->DocType[0] == 'Q')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '4';
   else if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);

   // Merge sale code
   memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, sizeof(pSaleRec->SaleCode));

   // Merge seller
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

   // Update SalePrice
   if (!bNoPrice)
      memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

   if (pSaleRec->MultiSale_Flg == 'Y' || pSaleRec->NumOfPrclXfer[0] == 'M')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else if (!bKeepMAFlag)
      *(pOutbuf+OFF_MULTI_APN) = ' ';

   lSaleUpdate++;
   return 0;
}

int ApplySCSalRec_ReformatDocNumX4(char *pOutbuf, char *pSale)
{
   long        lCurSaleDt, lLstSaleDt, iTmp, lCurPrice, lLastPrice;
   char        sTmp[64], sDocNum[32];
   SCSAL_REC   *pSaleRec = (SCSAL_REC *)pSale;

   if (pSaleRec->DocNum[0] < '0' || pSaleRec->DocDate[0] < '0')
      return 1;
#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0211222640000", 98))
   //   iTmp = 0;
#endif

   // Reformat DocNum
   iTmp = atoin(pSaleRec->DocNum, 4);
   if (!iTmp)
      return 1;

   if (pSaleRec->DocNum[4] == 'R')
      memcpy(sDocNum, pSaleRec->DocNum, SALE_SIZ_DOCNUM);
   else
      sprintf(sDocNum, "%.4sR%.4d    ", pSaleRec->DocDate, iTmp);

   // Update transfers
   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
   if (lCurSaleDt == lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, sDocNum, SALE_SIZ_DOCNUM);
   } else if (lCurSaleDt > lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, sDocNum, SALE_SIZ_DOCNUM);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }

   // Ignore known non-sale transactions
   if (pSaleRec->NoneSale_Flg == 'Y' || pSaleRec->XferType == 'T')
      return 1;

   lCurPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
   lLastPrice = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);

   // If current sale is older than last sale or newer than today, return
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return 1;
   else if (lCurSaleDt == lLstSaleDt)
   {
      iTmp = memcmp(sDocNum, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
      if (iTmp > 0)
      {
         // Make sure Sale1 matches Transfer Doc#
         memcpy(pOutbuf+OFF_SALE1_DOC, sDocNum, SIZ_SALE1_DOC);
         memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);
         memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, sizeof(pSaleRec->SaleCode));
         if (pSaleRec->Seller1[0] > ' ')
            memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

         iTmp = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
         if (iTmp > 1000)
            memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
         else if (bUseConfSalePrice)
         {
            iTmp = atoin(pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);
            if (iTmp > 1000)
               memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);
         }
      }
      return 1;
   }

    // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE2_CODE);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE1_CODE);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   *(pOutbuf+OFF_AR_CODE1) = pSaleRec->ARCode;

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, sDocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);

   if (pSaleRec->DocType[0] == 'G')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '1';
   else if (pSaleRec->DocType[0] == 'Q')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '4';
   else if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);

   // Update SalePrice
   long lSalePrice;
   lSalePrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
   if (lSalePrice < 1000 && bUseConfSalePrice)
      lSalePrice = atoin(pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);

   if (lSalePrice > 0)
   {
      memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, sizeof(pSaleRec->SaleCode));
      sprintf(sTmp, "%*d", SIZ_SALE1_AMT, lSalePrice);
      memcpy(pOutbuf+OFF_SALE1_AMT, sTmp, SIZ_SALE1_AMT);
   } else
   {
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);
      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
   }

   // Merge seller
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

   if (pSaleRec->MultiSale_Flg == 'Y' || pSaleRec->NumOfPrclXfer[0] == 'M')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else if (!bKeepMAFlag)
      *(pOutbuf+OFF_MULTI_APN) = ' ';

   lSaleUpdate++;
   return 0;
}

/******************************** ApplySCSalRec ******************************
 *
 * Same as above function.  Except it won't check for DOCDATE
 * Used by TRI
 *
 *****************************************************************************/

int ApplySCSalRec_NoDateChk(char *pOutbuf, char *pSale, bool bNoPrice, bool bUpdtXfer, char cDataSrc)
{
   long        lCurSaleDt, lLstSaleDt, iTmp;
   SCSAL_REC   *pSaleRec = (SCSAL_REC *)pSale;

   // Update transfers
   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   if (bUpdtXfer)
   {
      lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
      if (lCurSaleDt >= lLstSaleDt)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
      }
   }

   // Ignore known non-sale transactions
   if (pSaleRec->NoneSale_Flg == 'Y' || pSaleRec->XferType == 'T')
      return 1;

   // If current sale is older than last sale or newer than today, return
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if ((lCurSaleDt > 19000101) && (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday))
      return 1;
   else if (lCurSaleDt > 19000101 && lCurSaleDt == lLstSaleDt)
   {
      if (cDataSrc != 'R')
      {
         iTmp = memcmp(pSaleRec->DocNum, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
         if (iTmp != 0)
         {
            // Make sure Sale1 matches Transfer Doc#
            memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
            memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);
         }

         if (!bNoPrice && *(pOutbuf+OFF_SALE1_AMT+8) == ' ')
         {
            memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
         }
      }
      return 1;
   }

    // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   if (!bNoPrice)   
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE2_CODE);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   if (!bNoPrice)   
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE1_CODE);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   *(pOutbuf+OFF_AR_CODE1) = cDataSrc;

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);

   if (pSaleRec->DocType[0] == 'G')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '1';
   else if (pSaleRec->DocType[0] == 'Q')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '4';
   else if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);

   // Merge sale code
   memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, sizeof(pSaleRec->SaleCode));

   // Merge seller
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

   // Update SalePrice
   if (!bNoPrice)
      memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

   if (pSaleRec->MultiSale_Flg == 'Y' || pSaleRec->NumOfPrclXfer[0] == 'M')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else if (!bKeepMAFlag)
      *(pOutbuf+OFF_MULTI_APN) = ' ';

   lSaleUpdate++;
   return 0;
}

/******************************** ApplySCSalRec ******************************
 *
 * Apply history sales to R01 record.  Ignore known non-sale transactions. 
 * Only update if there is sale price.
 * If bUpdtXfer is true, it's ok to update transfer.
 *
 * cDataSrc = A : Assessor (SALE file)
 *            R : Recorder (GRGR file)
 *
 *****************************************************************************/

int ApplySCSalRecWP(char *pOutbuf, char *pSale, bool bUpdtXfer, char cDataSrc)
{
   long        lCurSaleDt, lLstSaleDt, lPrice, iTmp;
   SCSAL_REC   *pSaleRec = (SCSAL_REC *)pSale;

   // Update transfers
   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   if (bUpdtXfer)
   {
      lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
      if (lCurSaleDt >= lLstSaleDt)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
      }
   }

   // Check sale price
   lPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
   if (!lPrice)
      return 1;

   // If current sale is older than last sale or newer than today, return
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return 1;
   else if (lCurSaleDt == lLstSaleDt)
   {
      iTmp = memcmp(pSaleRec->DocNum, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
      if (iTmp < 0)
         return 1;
      else 
      {  
         if (iTmp > 0)
         {
            // Update Sale1 - Multi transfer in same day
            memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
            if (isdigit(pSaleRec->DocType[0]))
               memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);
            else if (pSaleRec->DocType[0] == 'G')
               *(pOutbuf+OFF_SALE1_DOCTYPE) = '1';
            else if (pSaleRec->DocType[0] == 'Q')
               *(pOutbuf+OFF_SALE1_DOCTYPE) = '4';

            if (pSaleRec->Seller1[0] > ' ')
               memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);
         }
         
         memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
         return 1;
      }
   }

    // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE2_CODE);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE1_CODE);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   *(pOutbuf+OFF_AR_CODE1) = cDataSrc;

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);

   if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);
   else if (pSaleRec->DocType[0] == 'G')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '1';
   else if (pSaleRec->DocType[0] == 'Q')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '4';

   // Merge sale code
   memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, sizeof(pSaleRec->SaleCode));

   // Merge seller
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

   // Update SalePrice
   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

   if (pSaleRec->MultiSale_Flg == 'Y' || pSaleRec->NumOfPrclXfer[0] == 'M')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else if (!bKeepMAFlag)
      *(pOutbuf+OFF_MULTI_APN) = ' ';

   lSaleUpdate++;
   return 0;
}

/******************************** ApplySCSalRec ******************************
 *
 * Apply GD transaction to R01 record, No price.
 *
 * cDataSrc = A : Assessor (SALE file)
 *            R : Recorder (GRGR file)
 *
 *****************************************************************************/

int ApplySCSalRecGD(char *pOutbuf, char *pSale, char cDataSrc)
{
   long        lCurSaleDt, lLstSaleDt, iTmp;
   SCSAL_EXT   *pSaleRec = (SCSAL_EXT *)pSale;

   // Update transfers
   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
   if (lCurSaleDt >= lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }

   // Check for GD
   iTmp = atoin(pSaleRec->DocType, SIZ_SALE1_DOCTYPE);
   if (iTmp != 1)
      return 1;

   // If current sale is older than last sale or newer than today, return
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return 1;
   else if (lCurSaleDt == lLstSaleDt)
   {
      iTmp = memcmp(pSaleRec->DocNum, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
      if (iTmp < 0)
         return 1;
      else 
      {  
         if (iTmp > 0)
         {
            // Update Sale1 - Multi transfer in same day
            memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
            if (isdigit(pSaleRec->DocType[0]))
               memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);
            else if (pSaleRec->DocType[0] == 'G')
               *(pOutbuf+OFF_SALE1_DOCTYPE) = '1';
            else if (pSaleRec->DocType[0] == 'Q')
               *(pOutbuf+OFF_SALE1_DOCTYPE) = '4';

            if (pSaleRec->Seller1[0] > ' ')
               memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);
         }
         
         return 1;
      }
   }

    // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE2_CODE);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE1_CODE);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   *(pOutbuf+OFF_AR_CODE1) = cDataSrc;

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);

   if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);
   else if (pSaleRec->DocType[0] == 'G')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '1';
   else if (pSaleRec->DocType[0] == 'Q')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '4';

   // Merge sale code
   memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, sizeof(pSaleRec->SaleCode));

   // Merge seller
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

   // Update SalePrice
   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

   if (pSaleRec->MultiSale_Flg == 'Y' || pSaleRec->NumOfPrclXfer[0] == 'M')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else if (!bKeepMAFlag)
      *(pOutbuf+OFF_MULTI_APN) = ' ';

   lSaleUpdate++;
   return 0;
}

/******************************* ApplySCSalRecDN *****************************
 *
 * Copy TransferDoc to Sale1Doc if they have same date
 *
 *****************************************************************************/

int ApplySCSalRecDN(char *pOutbuf, char *pSale, bool bNoPrice, bool bUpdtXfer)
{
   long        lCurSaleDt, lLstSaleDt;
   SCSAL_REC   *pSaleRec = (SCSAL_REC *)pSale;

   // Update transfers
   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
   if (bUpdtXfer && lCurSaleDt > lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   } else if (lCurSaleDt == lLstSaleDt && *(pOutbuf+OFF_TRANSFER_DOC) > ' ')
      memcpy(pSaleRec->DocNum, pOutbuf+OFF_TRANSFER_DOC, SIZ_TRANSFER_DOC);


   // Ignore known non-sale transactions
   if (pSaleRec->NoneSale_Flg == 'Y')
      return 1;

   // If current sale is older than last sale or newer than today, return
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return 1;
   else if (lCurSaleDt == lLstSaleDt)
   {
      // Update DocNum if currently blank
      if (pSaleRec->DocNum[0] > ' ' && *(pOutbuf+OFF_SALE1_DOC) == ' ')
         memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
      return 1;
   }

    // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   if (!bNoPrice)   
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE2_CODE);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   if (!bNoPrice)   
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE1_CODE);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   *(pOutbuf+OFF_AR_CODE1) = 'A';

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);

   if (pSaleRec->DocType[0] == 'G')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '1';
   else if (pSaleRec->DocType[0] == 'Q')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '4';
   else if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);

   // Merge sale code
   memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, sizeof(pSaleRec->SaleCode));

   // Merge seller
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

   // Update SalePrice
   if (!bNoPrice)
      memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

   if (pSaleRec->MultiSale_Flg == 'Y' || pSaleRec->NumOfPrclXfer[0] == 'M')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else if (!bKeepMAFlag)
      *(pOutbuf+OFF_MULTI_APN) = ' ';

   lSaleUpdate++;
   return 0;
}

/**************************** ApplySCSalRec_NoChk ****************************
 *
 * Apply history sales to R01 record. Use all transactions. 
 * 
 * cDataSrc = A : Assessor (SALE file)
 *            R : Recorder (GRGR file)
 *
 *****************************************************************************/

int ApplySCSalRec_NoChk(char *pOutbuf, char *pSale, bool bUpdtXfer, char cDataSrc)
{
   long        lCurSaleDt, lLstSaleDt;
   SCSAL_REC   *pSaleRec = (SCSAL_REC *)pSale;

   if (pSaleRec->DocNum[0] < '0' && pSaleRec->DocDate[0] < '0')
      return 1;

   // Update transfers
   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   if (bUpdtXfer)
   {
      lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
      if (lCurSaleDt >= lLstSaleDt)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
      }
   }

   // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE2_CODE);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE1_CODE);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   *(pOutbuf+OFF_AR_CODE1) = cDataSrc;

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, sizeof(pSaleRec->SaleCode));

   // Merge seller
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

   if (pSaleRec->MultiSale_Flg == 'Y' || pSaleRec->NumOfPrclXfer[0] == 'M')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else if (!bKeepMAFlag)
      *(pOutbuf+OFF_MULTI_APN) = ' ';

   lSaleUpdate++;
   return 0;
}

/******************************** ApplySCSalExt ******************************
 *
 * Apply NDC sales to R01 record.  Ignore known non-sale transactions. 
 * This function is the same as ApplySCSalRec except that it updates owner information
 *
 * cDataSrc = A : Assessor (SALE file)
 *            R : Recorder (GRGR file)
 *            N : NDC
 *
 *****************************************************************************/

int ApplySCSalExt(char *pOutbuf, char *pSale, bool bNoPrice, bool bUpdtXfer, char cDataSrc)
{
   long        lCurSaleDt, lLstSaleDt, iTmp, iStrNum;
   char        sTmp[256], sName1[256], sName2[256];
   SCSAL_EXT   *pSaleRec = (SCSAL_EXT *)pSale;

   if (pSaleRec->DocNum[0] < '0' || pSaleRec->DocDate[0] < '0')
      return 1;

   // Update transfers
   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   if (bUpdtXfer)
   {
      lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
      if (lCurSaleDt >= lLstSaleDt)
      {
#ifdef _DEBUG
         //if (!memcmp(pOutbuf, "00118317 ", myCounty.iApnLen))
         //   iTmp = 0;
#endif
         iTmp = memcmp(pSaleRec->DocNum, pOutbuf+OFF_TRANSFER_DOC, SIZ_TRANSFER_DOC);
         if (lCurSaleDt == lLstSaleDt)
         {
            if (iTmp > 0)
               memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
         } else
         {
            memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
            memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
            iTmp = 1;
         }

         // Update owner info - Update for sale only
         if (iTmp > 0 && pSaleRec->Name1[0] > ' ' && lCurSaleDt > lLienDate && pSaleRec->NoneSale_Flg != 'Y')
         {
            // Remove comma
            memcpy(sName1, pSaleRec->Name1, SALE_SIZ_BUYER);
            replChar(sName1, ',', ' ', SALE_SIZ_BUYER);
            iTmp = blankRem(sName1, SALE_SIZ_BUYER);

            // If new owner, update mail addr
            if (memcmp(sName1, pOutbuf+OFF_NAME1, iTmp))
            {
               // Clear output buffer
               removeNames(pOutbuf, true);

               // Update owner1
               memcpy(pOutbuf+OFF_NAME1, sName1, iTmp);
               memcpy(pOutbuf+OFF_NAME_SWAP, sName1, iTmp);

               if (pSaleRec->Name2[0] > ' ')
               {
                  memcpy(sName2, pSaleRec->Name2, SALE_SIZ_BUYER);
                  replChar(sName2, ',', ' ', SALE_SIZ_BUYER);
                  iTmp = blankRem(sName2, SIZ_NAME2);
                  memcpy(pOutbuf+OFF_NAME2, sName2, iTmp);
               }

               // Update mailing
               if (pSaleRec->MailAdr1[0] > ' ')
               {
                  // Clear old Mailing
                  removeMailing(pOutbuf, false);

                  // Replace mail addr with situs addr
                  iStrNum = atoin(pSaleRec->M_StrNum, SIZ_S_STRNUM);
                  if (iStrNum > 0)
                  {
                     iTmp = sprintf(sTmp, "%d ", iStrNum);
                     memcpy(pOutbuf+OFF_M_STRNUM, sTmp, iTmp);
                  }

                  memcpy(pOutbuf+OFF_M_STR_SUB, pSaleRec->M_StrSub, SIZ_M_STR_SUB);
                  memcpy(pOutbuf+OFF_M_DIR, pSaleRec->M_PreDir, SIZ_M_DIR);
                  memcpy(pOutbuf+OFF_M_STREET, pSaleRec->M_StrName, SIZ_M_STREET);
                  memcpy(pOutbuf+OFF_M_SUFF, pSaleRec->M_StrSfx, SIZ_M_SUFF);
                  memcpy(pOutbuf+OFF_M_UNITNO, pSaleRec->M_UnitNo, SIZ_S_UNITNO);
                  memcpy(pOutbuf+OFF_M_CITY, pSaleRec->M_City, SIZ_M_CITY);
                  memcpy(pOutbuf+OFF_M_ST, pSaleRec->M_St, SIZ_M_ST);
                  memcpy(pOutbuf+OFF_M_ZIP, pSaleRec->MailZip, SIZ_M_ZIP);
                  memcpy(pOutbuf+OFF_M_ZIP4, pOutbuf+OFF_S_ZIP4, SIZ_M_ZIP4);

                  memcpy(pOutbuf+OFF_M_ADDR_D, pSaleRec->MailAdr1, SIZ_M_ADDR_D);
                  memcpy(pOutbuf+OFF_M_CTY_ST_D, pSaleRec->MailAdr2, SIZ_M_CTY_ST_D);
               }
            }
         }
      }
   }

   // Ignore known non-sale transactions
   if (pSaleRec->NoneSale_Flg == 'Y' || pSaleRec->XferType == 'T')
      return 1;

   // If current sale is older than last sale or newer than today, return
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return 1;
   else if (lCurSaleDt == lLstSaleDt)
   {
      if (cDataSrc != 'R')
      {
         iTmp = memcmp(pSaleRec->DocNum, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
         if (iTmp > 0)
         {
            // Make sure Sale1 matches Transfer Doc#
            memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
            memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);
            memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, sizeof(pSaleRec->SaleCode));
            if (pSaleRec->Seller1[0] > ' ')
               memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

            if (!bNoPrice)
            {
               iTmp = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
               if (iTmp > 1000)
                  memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
               else  if (bUseConfSalePrice)
               {
                  iTmp = atoin(pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);
                  if (iTmp > 1000)
                     memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);
               }
            }
         }
      }
      return 1;
   }

    // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   if (!bNoPrice)   
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE2_CODE);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   if (!bNoPrice)   
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE1_CODE);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   *(pOutbuf+OFF_AR_CODE1) = cDataSrc;

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);

   if (pSaleRec->DocType[0] == 'G')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '1';
   else if (pSaleRec->DocType[0] == 'Q')
      *(pOutbuf+OFF_SALE1_DOCTYPE) = '4';
   else if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);

   // Merge sale code
   memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, sizeof(pSaleRec->SaleCode));

   // Merge seller
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

   // Update SalePrice
   if (!bNoPrice)
   {
      iTmp = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
      if (iTmp > 1000)
         memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
      else if (bUseConfSalePrice)
         memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);
   }

   if (pSaleRec->MultiSale_Flg == 'Y' || pSaleRec->NumOfPrclXfer[0] == 'M')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else if (!bKeepMAFlag)
      *(pOutbuf+OFF_MULTI_APN) = ' ';

   lSaleUpdate++;
   return 0;
}

// If same doc and different date, copy sale date over transfer date
int Verify_XferDate(char *pRec)
{
   if (!memcmp(pRec+OFF_SALE1_DOC, pRec+OFF_TRANSFER_DOC, SIZ_SALE1_DOC))
   {
      if (memcmp(pRec+OFF_SALE1_DT, pRec+OFF_TRANSFER_DT, SIZ_SALE1_DT) )
      {
         memcpy(pRec+OFF_TRANSFER_DT, pRec+OFF_SALE1_DT, SIZ_SALE1_DT);
         return 1;
      }
   }

   return 0;
}

/********************************* ApplyHistSale() *********************************
 *
 * Merge sale data to roll record depending on sale record type
 * Type = 1: SALE_REC1
 *        2: SCSAL_REC 
 *        4: SCSAL_REC and update XFER
 *        5: GRGR_DOC
 *
 * ClearSaleFlg = 1 (clear sales whether there is sale update or not)
 *              = 2 (clear sales only if sale update available)
 *                4 (Clear old Grgr only if there is update)
 *                8
 *               16 (Do not check DocNum)
 *               32 (Do not check DocDate)
 *               64 (Do not check SalePrice)
 *            0x080 (DOn't check anything, apply all sale records)
 *            0x200 Verify Xfer_Date
 *              = 0 (do not clear sales before update)
 *
 * Return 0 if success, 1=No match, -1=EOF
 *
 ***********************************************************************************/

int ApplyHistSale(char *pOutbuf, int iType, int iClearSaleFlg, bool bReset=false)
{
   static   char  acRec[2048], *pRec=NULL;
   int      iLoop, iRet=1;
   SCSAL_EXT *pSale = (SCSAL_EXT *)acRec;

   if (bReset)
   {
      pRec = NULL;
      return iRet;
   }

   if (!pRec)
      pRec = fgets(acRec, 2048, fdSale);

   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return -1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", myCounty.iApnLen, acRec);
         pRec = fgets(acRec, 2048, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return iRet;

   // Clear old sales
   if ((iClearSaleFlg & CLEAR_UPD_SALE) || (iClearSaleFlg & CLEAR_OLD_SALE))
   {
      if (iClearSaleFlg & CLEAR_OLD_XFER)
         ClearOldSale(pOutbuf, true);
      else
         ClearOldSale(pOutbuf);
   } else if (iClearSaleFlg & CLEAR_UPD_GRGR)
      ClearOldGrGr(pOutbuf);

   do
   {
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "0071C006", 8))
      //   iRet = 0;
#endif
      switch(iType)
      {
         //case SALE_USE_SALEREC1:
         //   iRet = ApplySaleRec1(pOutbuf, acRec);
         //   break;
         case SALE_USE_SCSALREC:
            if (iClearSaleFlg & DONT_CHK_DOCNUM)
               iRet = ApplySCSalRec_NoDocChk(pOutbuf, acRec, false, false);
            else if (iClearSaleFlg & DONT_CHK_DOCDATE)
               iRet = ApplySCSalRec_NoDateChk(pOutbuf, acRec, false, false);
            else if (iClearSaleFlg & DONT_CHK_ANY)
               iRet = ApplySCSalRec_NoChk(pOutbuf, acRec, false);
            else
            {
               if (pSale->ARCode > ' ')
                  iRet = ApplySCSalRec(pOutbuf, acRec, false, false, pSale->ARCode);
               else
                  iRet = ApplySCSalRec(pOutbuf, acRec, false, false);
            }
            break;
         case SALE_USE_SCUPDXFR:
            if (iClearSaleFlg & DONT_CHK_DOCNUM)
               iRet = ApplySCSalRec_NoDocChk(pOutbuf, acRec, false, true);
            else if (iClearSaleFlg & DONT_CHK_DOCDATE)
               iRet = ApplySCSalRec_NoDateChk(pOutbuf, acRec, false, true);
            else if (iClearSaleFlg & DONT_CHK_ANY)
               iRet = ApplySCSalRec_NoChk(pOutbuf, acRec, true);
            else if (iClearSaleFlg & FMT_DOCNUM_X4)
               iRet = ApplySCSalRec_ReformatDocNumX4(pOutbuf, acRec);
            else if (iClearSaleFlg & VERIFY_DATE)
               iRet = ApplySCSalRec_ChkXfer(pOutbuf, acRec, false, true, pSale->ARCode);
            else if (bChkYear)
               iRet = ApplySCSalRec_ChkDocYear(pOutbuf, acRec, false, true, 'A');
            else
            {
               if (pSale->ARCode > ' ')
                  iRet = ApplySCSalRec(pOutbuf, acRec, false, true, pSale->ARCode);
               else
                  iRet = ApplySCSalRec(pOutbuf, acRec, false, true);
            }
            break;
         case SALE_USE_SCNOPRICE:
            iRet = ApplySCSalRec(pOutbuf, acRec, true,true);
            break;
         case GRGR_UPD_XFR:
            // Update sales and transfer
            iRet = ApplySCSalRecWP(pOutbuf, acRec, true, 'R');
            break;
         case GRGR_UPD_GD:
            // Update sales and transfer
            iRet = ApplySCSalRecGD(pOutbuf, acRec, 'R');
            break;
         case GRGR_UPD_OWNER:
            // Update sale, transfer, owner, mailing
            iRet = ApplySCSalExt(pOutbuf, acRec, false, true, pSale->ARCode);
            break;
      }

      // Get next sale record
      if (!(pRec = fgets(acRec, 2048, fdSale)))
         break;
   } while (!memcmp(pOutbuf, acRec, myCounty.iApnLen));

   lSaleMatch++;
   return iRet;
}

// Same as ApplyHistSale() but ignore record w/o sale price
int ApplyHistSaleWP(char *pOutbuf, int iType, int iClearSaleFlg)
{
   static   char  acRec[2048], *pRec=NULL;
   int      iLoop, iRet=1;

   if (!pRec)
      pRec = fgets(acRec, 1024, fdSale);

   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return -1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", myCounty.iApnLen, acRec);
         pRec = fgets(acRec, 1024, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return iRet;

   // Clear old sales
   if (iClearSaleFlg == CLEAR_UPD_SALE)
      ClearOldSale(pOutbuf);

   do
   {
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "032022026000", 9))
      //   iLoop = 0;
#endif
      switch(iType)
      {
         case SALE_USE_SCSALREC:
            iRet = ApplySCSalRecWP(pOutbuf, acRec);
            break;
         case SALE_USE_SCUPDXFR:
            iRet = ApplySCSalRecWP(pOutbuf, acRec, true);
            break;
         case GRGR_UPD_XFR:
            // Update sales and transfer
            iRet = ApplySCSalRecWP(pOutbuf, acRec, true, 'R');
            break;
      }

      // Get next sale record
      if (!(pRec = fgets(acRec, 1024, fdSale)))
         break;
   } while (!memcmp(pOutbuf, acRec, myCounty.iApnLen));

   lSaleMatch++;
   return iRet;
}

// Sale as ApplyHistSale() but copy TransferDoc to Sale1Doc if they have same date
int ApplyHistSaleDN(char *pOutbuf, int iClearSaleFlg)
{
   static   char  acRec[2048], *pRec=NULL;
   int      iLoop, iRet=1;

   if (!pRec)
      pRec = fgets(acRec, 1024, fdSale);

   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return -1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", myCounty.iApnLen, acRec);
         pRec = fgets(acRec, 1024, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return iRet;

   // Clear old sales
   if (iClearSaleFlg == 2)
      ClearOldSale(pOutbuf);

   do
   {
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "032022026000", 9))
      //   iLoop = 0;
#endif
      iRet = ApplySCSalRecDN(pOutbuf, acRec, false, true);

      // Get next sale record
      if (!(pRec = fgets(acRec, 1024, fdSale)))
         break;
   } while (!memcmp(pOutbuf, acRec, myCounty.iApnLen));

   lSaleMatch++;
   return iRet;
}

/******************************** MergeHistGrGr *****************************
 *
 * Merge new GrGr data into current sale.  Move other sales accordingly.
 * If sale occurs on the same day, use the one with sale price to update Sale1,
 * use latest doc to update transfer.
 * This function is designed for SCL, but can be used for other counties like ORG
 * on Lien Date.
 *
 *****************************************************************************/

static void MergeHistGrGr(char *pSale, char *pOutbuf)
{
   int         iTmp;
   long        lCurSaleDt, lLstSaleDt;
   SALE_REC1   *pSaleRec = (SALE_REC1 *)pSale;

   lCurSaleDt = atoin(pSaleRec->acDocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   // If current sale is older than last sale, return
   if (lCurSaleDt < lLstSaleDt)
      return;
   else if (lCurSaleDt == lLstSaleDt)
   {
      iTmp = memcmp(pSaleRec->acDocNum, pOutbuf+OFF_SALE1_DOC, sizeof(pSaleRec->acDocNum));
      if (iTmp < 0)
         return;
      else 
      {  
         if (iTmp > 0)
         {
            // Update sale1 only if sale price avail
            iTmp = atoin(pSaleRec->acSalePrice, SIZ_SALE1_AMT);
            if (iTmp > 0)
            {
               // Update Sale1 - Multi transfer in same day
               memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->acDocNum, sizeof(pSaleRec->acDocNum));
               memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->acDocType, sizeof(pSaleRec->acDocType));
               if (pSaleRec->acSeller[0] > ' ')
                  memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);
               memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->acSalePrice, SIZ_SALE1_AMT);
            }

            // Update transfers 
            //lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
            //if (lCurSaleDt >= lLstSaleDt)
            //{
            //   memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->acDocNum, SIZ_TRANSFER_DOC);
            //   memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->acDocDate, SIZ_TRANSFER_DT);
            //}
         } else
         {
            iTmp = atoin(pSaleRec->acSalePrice, SIZ_SALE1_AMT);
            if (iTmp > 0)
            {
               if (pSaleRec->acSeller[0] > ' ')
                  memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);
               memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->acSalePrice, SIZ_SALE1_AMT);
            }
         }
         return;
      }
   }

   // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE2_CODE);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE1_CODE);

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->acDocNum, sizeof(pSaleRec->acDocNum));
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->acDocDate, sizeof(pSaleRec->acDocDate));
   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->acDocType, sizeof(pSaleRec->acDocType));

   // Merge sale code
   if (pSaleRec->acSaleCode[0] > '0')
      memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->acSaleCode, sizeof(pSaleRec->acSaleCode));
   else
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);

   // Merge seller
   if (pSaleRec->acSeller[0] > ' ')
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);
   else if (*(pOutbuf+OFF_SELLER) > ' ')
      memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

   // Update SalePrice
   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->acSalePrice, SIZ_SALE1_AMT);

   // Update transfers - 06/25/2010 sn
   //lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
   //if (lCurSaleDt >= lLstSaleDt)
   //{
   //   memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->acDocNum, SIZ_TRANSFER_DOC);
   //   memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->acDocDate, SIZ_TRANSFER_DT);
   //}
}

/********************************** MergeCumGrGr ******************************
 *
 * Merge cumulative sales to R01 file.
 * First sort cumsale file in SALE1_REC format.
 * Then merge into T01 file.  If success, delete R01 and rename T01 to R01.
 * Designed for SCL and ORG, but can be used for any county.
 *
 ******************************************************************************/

int MergeCumGrGr(int iSkip, char *pCSaleFile)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acCumSaleFile[_MAX_PATH];
   char		*pRec;

   HANDLE   fhIn, fhOut;

   int      iRet,iTmp, iRollUpd;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bCont;
   long     lCnt=0;

   if (pCSaleFile && !_access(pCSaleFile, 0))
      strcpy(acCumSaleFile, pCSaleFile);
   else
      strcpy(acCumSaleFile, acCSalFile);

   LogMsg("\nMerge sale history %s.", acCumSaleFile);

   // Sort cumulative sale file
   /*
   strcpy(acOutFile, acCumSaleFile);
   pRec = strrchr(acOutFile, '.');
   if (pRec)
      strcpy(pRec, ".SRT");
   else
      strcat(acOutFile, ".SRT");
   */
   sprintf(acOutFile, "%s\\%s\\%s_Sale.Srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sorting %s to %s.", acCumSaleFile, acOutFile);

   // Sort on APN asc, DocDate asc, DocNum asc
   sprintf(acRec, "S(1,%d,C,A,27,8,C,A,15,%d,C,A) F(TXT) DUPO(B8000,1,34) ", SALE_SIZ_APN, SALE_SIZ_DOCNUM);
   iTmp = sortFile(acCumSaleFile, acOutFile, acRec);
   if (!iTmp)
      return -1;

   // Rename SRT file to SLS file
   //iRet = remove(acCumSaleFile);
   //if (iRet)
   //   return -1;
   //iRet = rename(acOutFile, acCumSaleFile); 
   //if (iRet)
   //   return -1;

   strcpy(acCumSaleFile, acOutFile);
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "T01");

   // Check input file for processing
   if (_access(acRawFile, 0))
   {
      LogMsg("Missing input file %s.  Please recheck!", acRawFile);
      return -1;
   }

   // Open cumulative file
   LogMsg("Open cumulative sale file %s", acCumSaleFile);
   fdSale = fopen(acCumSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acCumSaleFile);
      return -2;
   }

   // Get first record
   pRec = fgets(acRec, MAX_RECSIZE, fdSale);

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iRet=iNoMatch=iRollUpd=0;

   // Merge loop
   bCont = true;
   while (bCont)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for read error
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -1;
         break;
      }

      // Check for EOF
      if (!nBytesRead)
         break;         // EOF

CumSale_Reload:
      // Check roll record - if roll record is smaller than sale record
      // output that record without updating.
      iTmp = memcmp(acBuf, acRec, iApnLen);
      if (!iTmp)
      {
         // Merge Sale
         MergeHistGrGr(acRec, acBuf);
         iRollUpd++;

         // Read next sale record
         pRec = fgets(acRec, MAX_RECSIZE, fdSale);
         if (!pRec)
            bCont = false;
         else
            goto CumSale_Reload;
      } else if (iTmp > 0)             // Get next sale
      {
         if (bDebug)
            LogMsg0("*** Sale not match : %.*s (%d) ***", iApnLen, acRec, lCnt);
         iNoMatch++;

         // Read next sale record
         pRec = fgets(acRec, MAX_RECSIZE, fdSale);
         if (!pRec)
            bCont = false;
         else
            goto CumSale_Reload;
      }

      iTmp = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
      if (iTmp > lLastRecDate && iTmp < lToday)
         lLastRecDate = iTmp;

      // Write to output file
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         iRet = -2;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }

   // Close files
   fclose(fdSale);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // If everything is OK, rename output file
   if (!iRet)
   {
      // Rename files
      iRet = remove(acRawFile);
      if (iRet)
         LogMsg("***** Unable to delete %s (%d)", acRawFile, errno);
      else
      {
         iRet = rename(acOutFile, acRawFile);
         if (iRet)
            LogMsg("***** Error renaming %s to %s (%d)", acOutFile, acRawFile, errno);
      }
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total sale records applied: %u", iRollUpd);
   LogMsg("Total sale not matched:     %u", iNoMatch);
   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return iRet;
}

/********************************** MergeCumSale ******************************
 *
 * Merge cumulative sales to R01 file.
 * First sort cumsale file in SALE_REC format.
 * Then merge into T01 file.  If success, delete R01 and rename T01 to R01.
 * Designed for SJX and BUT, but can be used for any county.
 *
 ******************************************************************************/

int MergeCumSale(int iSkip, char *pCSaleFile)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acCumSaleFile[_MAX_PATH];
   char		*pRec;

   HANDLE   fhIn, fhOut;

   int      iRet,iTmp, iRollUpd;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bCont;
   long     lCnt=0;

   if (pCSaleFile && !_access(pCSaleFile, 0))
      strcpy(acCumSaleFile, pCSaleFile);
   else
      strcpy(acCumSaleFile, acCSalFile);

   LogMsg("\nMerge sale history %s.", acCumSaleFile);

   // Sort cumulative sale file
   //sprintf(acOutFile, acGrGrTmpl, myCounty.acCntyCode, "srt");      // accummulated sale file
   strcpy(acOutFile, acCumSaleFile);
   pRec = strrchr(acOutFile, '.');
   if (pRec)
      strcpy(pRec, ".SRT");
   else
      strcat(acOutFile, ".SRT");
   LogMsg("Sorting %s to %s.", acCumSaleFile, acOutFile);

   // Sort on APN asc, DocDate asc, DocNum asc
   sprintf(acRec, "S(1,%d,C,A,27,8,C,A,15,%d,C,A) F(TXT) DUPO(B8000,1,34) ", SALE_SIZ_APN, SALE_SIZ_DOCNUM);
   iTmp = sortFile(acCumSaleFile, acOutFile, acRec);
   if (!iTmp)
      return -1;

   // Rename SRT file to SLS file
   iRet = remove(acCumSaleFile);
   if (iRet)
      return -1;
   iRet = rename(acOutFile, acCumSaleFile); 
   if (iRet)
      return -1;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "T01");

   // Check input file for processing
   if (_access(acRawFile, 0))
   {
      LogMsg("Missing input file %s.  Please recheck!", acRawFile);
      return 1;
   }

   // Open cumulative file
   LogMsg("Open cumulative sale file %s", acCumSaleFile);
   fdSale = fopen(acCumSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acCumSaleFile);
      return 2;
   }

   // Get first record
   pRec = fgets(acRec, MAX_RECSIZE, fdSale);

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iRet=iNoMatch=iRollUpd=0;

   // Merge loop
   bCont = true;
   while (bCont)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for read error
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -1;
         break;
      }

      // Check for EOF
      if (!nBytesRead)
         break;         // EOF

CumSale_Reload:
      // Check roll record - if roll record is smaller than sale record
      // output that record without updating.
      iTmp = memcmp(acBuf, acRec, iApnLen);
      if (!iTmp)
      {
         // Merge Sale
         MergeHistSale((SALE_REC *)&acRec[0], acBuf);
         iRollUpd++;

         // Read next sale record
         pRec = fgets(acRec, MAX_RECSIZE, fdSale);
         if (!pRec)
            bCont = false;
         else
            goto CumSale_Reload;
      } else if (iTmp > 0)             // Get next sale
      {
         if (bDebug)
            LogMsg0("*** Sale not match : %.*s (%d) ***", iApnLen, acRec, lCnt);
         iNoMatch++;

         // Read next sale record
         pRec = fgets(acRec, MAX_RECSIZE, fdSale);
         if (!pRec)
            bCont = false;
         else
            goto CumSale_Reload;
      }

      iTmp = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
      if (iTmp > lLastRecDate && iTmp < lToday)
         lLastRecDate = iTmp;

      // Write to output file
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         iRet = -2;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }

   // Close files
   fclose(fdSale);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // If everything is OK, rename output file
   if (!iRet)
   {
      // Rename files
      iRet = remove(acRawFile);
      if (iRet)
         LogMsg("***** Unable to delete %s (%d)", acRawFile, errno);
      else
      {
         iRet = rename(acOutFile, acRawFile);
         if (iRet)
            LogMsg("***** Error renaming %s to %s (%d)", acOutFile, acRawFile, errno);
      }
   }

   LogMsgD("Total output records:       %u", lCnt);
   LogMsg ("Total sale records applied: %u", iRollUpd);
   LogMsg ("Total sale not matched:     %u", iNoMatch);
   LogMsg ("Last recording date:        %u", lLastRecDate);

   lRecCnt = lCnt;
   return iRet;
}

/********************************* MergeCumSale1 ******************************
 *
 * Merge cumulative sales to R01 file.  This is generic for all counties.
 * First sort cumsale file in SALE_REC1 format.
 * Then merge into T01 file.  If success, delete R01 and rename T01 to R01.
 * Same as MergeCumSale() but calls MergeHistSale1() instead.
 *
 ******************************************************************************/

int MergeCumSale1(int iSkip, char *pCSaleFile, bool bResort, bool bNoPrice, bool bUpdtXfer)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acCumSaleFile[_MAX_PATH];
   char		*pRec;

   HANDLE   fhIn, fhOut;

   int      iRet,iTmp, iRollUpd;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bCont;
   long     lCnt=0;

   if (pCSaleFile && !_access(pCSaleFile, 0))
      strcpy(acCumSaleFile, pCSaleFile);
   else
      strcpy(acCumSaleFile, acCSalFile);

   LogMsg("\nMerge sale history %s.", acCumSaleFile);

   // Sort cumulative sale file
   if (bResort)
   {
      //sprintf(acOutFile, acGrGrTmpl, myCounty.acCntyCode, "srt");      // accummulated sale file
      strcpy(acOutFile, acCumSaleFile);
      pRec = strrchr(acOutFile, '.');
      if (pRec)
         strcpy(pRec, ".SRT");
      else
         strcat(acOutFile, ".SRT");
      LogMsg("Sorting %s to %s.", acCumSaleFile, acOutFile);

      // Sort on APN asc, DocDate asc, DocNum asc
      sprintf(acRec, "S(1,%d,C,A,27,8,C,A,15,%d,C,A) F(TXT) DUPO(B8000,1,34) ", SALE_SIZ_APN, SALE_SIZ_DOCNUM);
      iTmp = sortFile(acCumSaleFile, acOutFile, acRec);
      if (!iTmp)
         return -1;

      // Rename SRT file to SLS file
      iRet = remove(acCumSaleFile);
      if (iRet)
         return -1;
      iRet = rename(acOutFile, acCumSaleFile); 
      if (iRet)
         return -1;
   } 

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "T01");

   // Check input file for processing
   if (_access(acRawFile, 0))
   {
      LogMsg("Missing input file %s.  Please recheck!", acRawFile);
      return 1;
   }

   // Open cumulative file
   LogMsg("Open cumulative sale file %s", acCumSaleFile);
   fdSale = fopen(acCumSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acCumSaleFile);
      return 2;
   }

   // Get first record
   pRec = fgets(acRec, MAX_RECSIZE, fdSale);

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return 3;

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return 4;

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iRet=iNoMatch=iRollUpd=0;

   // Merge loop
   bCont = true;
   while (bCont)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for read error
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -1;
         break;
      }

      // Check for EOF
      if (!nBytesRead)
         break;         // EOF

CumSale1_Reload:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "10214029", 8))
      //   iTmp = 0;
#endif
      // Check roll record - if roll record is smaller than sale record
      // output that record without updating.
      iTmp = memcmp(acBuf, acRec, iApnLen);
      if (!iTmp)
      {
         // Merge Sale
         MergeHistSale1(acRec, acBuf, bNoPrice, bUpdtXfer);
         iRollUpd++;

         // Read next sale record
         pRec = fgets(acRec, MAX_RECSIZE, fdSale);
         if (!pRec)
            bCont = false;
         else
            goto CumSale1_Reload;
      } else if (iTmp > 0)             // Get next sale
      {
         if (bDebug)
            LogMsg0("*** Sale not match : %.*s (%d) ***", iApnLen, acRec, lCnt);
         iNoMatch++;

         // Read next sale record
         pRec = fgets(acRec, MAX_RECSIZE, fdSale);
         if (!pRec)
            bCont = false;
         else
            goto CumSale1_Reload;
      }

      iTmp = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
      if (iTmp > lLastRecDate && iTmp < lToday)
         lLastRecDate = iTmp;

      // Write to output file
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         iRet = -2;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }

   // Close files
   fclose(fdSale);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // If everything is OK, rename output file
   if (!iRet)
   {
      // Rename files
      iRet = remove(acRawFile);
      if (iRet)
         LogMsg("***** Unable to delete %s (%d)", acRawFile, errno);
      else
      {
         iRet = rename(acOutFile, acRawFile);
         if (iRet)
            LogMsg("***** Error renaming %s to %s (%d)", acOutFile, acRawFile, errno);
      }
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total sale records applied: %u", iRollUpd);
   LogMsg("Total sale not matched:     %u", iNoMatch);
   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return iRet;
}

/********************************* ApplyCumSale *******************************
 *
 * Apply cumulative sales to R01 file.  
 *    iType         = SALE_USE_SALEREC1 : use SALE1_REC format
 *                    SALE_USE_SCSALREC : use SCSAL_REC format
 *                    SALE_USE_SCUPDXFR : use SCSAL_REC format and update XFER if newer
 *                    GRGR_UPD_OWNER    : use SCSAL_REC format and update OWNER
 *                    GRGR_UPD_XFR      : use SCSAL_REC format and update XFER
 *
 *    iClearSaleFlg = 1 : Clear old sales regardless of update available (default)
 *                    2 : Clear old sales only if there is update.
 *                    4 : Clear old Grgr only if there is update.
 *
 * Return 0 if successful
 *
 ******************************************************************************/

int ApplyCumSale(int iSkip, char *pCSaleFile, bool bResort, int iType, int iClearSaleFlg)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acCumSaleFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   char		*pRec;

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bRename, bEof;
   long     lCnt=0;

   if (pCSaleFile && !_access(pCSaleFile, 0))
      strcpy(acCumSaleFile, pCSaleFile);
   else
      strcpy(acCumSaleFile, acCSalFile);

   LogMsg0("ApplyCumSale(): Apply history sale using %s", acCumSaleFile);

   // Prepare input/output files
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   bRename = true;

   // Check input file for processing
   if (_access(acRawFile, 0))
   {
      strcpy(acOutFile, acRawFile);
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (_access(acRawFile, 0))
      {
         LogMsg("Missing input file %s.  Please recheck!", acOutFile);
         return -1;
      }
      bRename = false;
   } else
      sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "N01");

   // Sort cumulative sale file
   if (bResort)
   {
      //sprintf(acOutFile, acGrGrTmpl, myCounty.acCntyCode, "srt");      // accummulated sale file
      strcpy(acTmpFile, acCumSaleFile);
      pRec = strrchr(acTmpFile, '.');
      if (pRec)
         strcpy(pRec, ".SRT");
      else
         strcat(acTmpFile, ".SRT");

      // Sort on APN asc, DocDate asc, DocNum asc
      sprintf(acRec, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) F(TXT) DUPO(B8000,1,34) ", SALE_SIZ_APN, SALE_SIZ_DOCNUM);
      iTmp = sortFile(acCumSaleFile, acTmpFile, acRec);
      if (!iTmp)
         return -2;

      // Rename SRT file to SLS file
      iRet = remove(acCumSaleFile);
      if (iRet)
         return -3;

      LogMsg("Rename %s to %s", acTmpFile, acCumSaleFile); 
      iRet = rename(acTmpFile, acCumSaleFile); 
      if (iRet)
         return -4;
   } 

   // Open cumulative file
   LogMsg("Open cumulative sale file %s", acCumSaleFile);
   fdSale = fopen(acCumSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening cumulative sale file: %s\n", acCumSaleFile);
      return -5;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -6;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -7;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   for (iRet = 0; iRet < iSkip; iRet++)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Reset buffer
   iRet = ApplyHistSale(NULL, 0, 0, true);
   lSaleUpdate=lSaleMatch=iRet=iNoMatch=iRollUpd=0;

   // Merge loop
   bEof = false;
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for read error
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -8;
         break;
      }

      // Check for EOF
      if (!nBytesRead)
      {
         iRet = 0;
         break;         // EOF
      }

#ifdef _DEBUG
      //if (!memcmp(acBuf, "014760037000", 9))
      //   iRet = 0;
#endif
      // Remove GRGR regardless of sale update or not
      if (iClearSaleFlg & CLEAR_UPD_GRGR)
         ClearOldGrGr(acBuf);

      // Apply Sale
      iRet = ApplyHistSale(acBuf, iType, iClearSaleFlg);
      if (!iRet)
         iRollUpd++;
      else if (iRet == -1)
      {  // EOF
         bEof = true;
      } else
         iNoMatch++;

      iRet = 0;
      iTmp = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
      if (iTmp > lLastRecDate && iTmp < lToday)
         lLastRecDate = iTmp;

      // Verify date
      if (iClearSaleFlg & VERIFY_DATE)
      {
         iTmp = Verify_XferDate(acBuf);
         if (iTmp == 1 && bDebug)
            LogMsg("*** Correct transfer date for APN=%.14s", acBuf);
      }

      // Write to output file
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error output record: %d\n", GetLastError());
         iRet = -9;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // If everything is OK, rename output file
   if (!iRet && bRename)
   {
      // Rename files
      LogMsg("Remove old file %s", acRawFile);
      iRet = remove(acRawFile);
      if (iRet)
         LogMsg("***** Unable to delete %s (%d)", acRawFile, errno);
      else
      {
         LogMsg("Rename %s to %s", acOutFile, acRawFile);
         iRet = rename(acOutFile, acRawFile);
         if (iRet)
            LogMsg("***** Error renaming %s to %s (%d)", acOutFile, acRawFile, errno);
      }
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total sale matched:         %u", lSaleMatch);
   LogMsg("Total sale not matched:     %u", iNoMatch);
   LogMsg("Total sale records used:    %u", lSaleUpdate);
   LogMsg("Total roll records updated: %u", iRollUpd);
   LogMsg("Last sale recording date:   %u\n", lLastRecDate);
   
   lRecCnt = lCnt;
   return iRet;
}

/***************************** ApplySCSalRecNR *******************************
 *
 * Apply history sales to R01 record.  Ignore known non-sale transactions. 
 * This function use SCSAL_EXT layout as it is output from NR_CreateSCSale()
 *
 * Return 0 if sale is updated, 1 if not
 *
 *****************************************************************************/

int ApplySCSalRecNR(char *pOutbuf, char *pSale, int iUpdateFlg)
{
   long        lCurSaleDt, lLstSaleDt, lCurPrice, lLastPrice;
   int         iTmp, iStrNum;
   char        sTmp[64], sName1[64], sName2[64];
   SCSAL_EXT   *pSaleRec = (SCSAL_EXT *)pSale;

   if (pSaleRec->DocNum[0] < '0' || pSaleRec->DocDate[0] < '0')
      return 1;

   // Update transfers
   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
   if (lCurSaleDt >= lLstSaleDt)
   {
      iTmp = memcmp(pSaleRec->DocNum, pOutbuf+OFF_TRANSFER_DOC, SIZ_TRANSFER_DOC);
      if (lCurSaleDt == lLstSaleDt)
      {
         if (iTmp > 0)
         {
            memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
            iTmp = 10;
         }
      } else
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
         iTmp = 10;
      }

      if (iUpdateFlg & UPDATE_OWNER)
      {
         // Update owner info - Update for sale only
         if (iTmp == 10 && pSaleRec->Name1[0] > ' ' && lCurSaleDt > lLienDate && pSaleRec->NoneSale_Flg != 'Y')
         {
            // Remove comma
            memcpy(sName1, pSaleRec->Name1, SALE_SIZ_BUYER);
            replChar(sName1, ',', ' ', SALE_SIZ_BUYER);
            iTmp = blankRem(sName1, SALE_SIZ_BUYER);

            // If new owner, update mail addr
            if (memcmp(sName1, pOutbuf+OFF_NAME1, iTmp))
            {
               // Clear output buffer
               removeNames(pOutbuf, true);

               // Update owner1
               memcpy(pOutbuf+OFF_NAME1, sName1, iTmp);
               memcpy(pOutbuf+OFF_NAME_SWAP, sName1, iTmp);

               if (pSaleRec->Name2[0] > ' ')
               {
                  memcpy(sName2, pSaleRec->Name2, SALE_SIZ_BUYER);
                  replChar(sName2, ',', ' ', SALE_SIZ_BUYER);
                  iTmp = blankRem(sName2, SIZ_NAME2);
                  memcpy(pOutbuf+OFF_NAME2, sName2, iTmp);
               }

               // Update mailing
               if (pSaleRec->MailAdr1[0] > ' ')
               {
                  // Clear old Mailing
                  removeMailing(pOutbuf, false);

                  // Replace mail addr with situs addr
                  iStrNum = atoin(pSaleRec->M_StrNum, SIZ_S_STRNUM);
                  if (iStrNum > 0)
                  {
                     iTmp = sprintf(sTmp, "%d ", iStrNum);
                     memcpy(pOutbuf+OFF_M_STRNUM, sTmp, iTmp);
                  }

                  memcpy(pOutbuf+OFF_M_STR_SUB, pSaleRec->M_StrSub, SIZ_M_STR_SUB);
                  memcpy(pOutbuf+OFF_M_DIR, pSaleRec->M_PreDir, SIZ_M_DIR);
                  memcpy(pOutbuf+OFF_M_STREET, pSaleRec->M_StrName, SIZ_M_STREET);
                  memcpy(pOutbuf+OFF_M_SUFF, pSaleRec->M_StrSfx, SIZ_M_SUFF);
                  memcpy(pOutbuf+OFF_M_UNITNO, pSaleRec->M_UnitNo, SIZ_S_UNITNO);
                  memcpy(pOutbuf+OFF_M_CITY, pSaleRec->M_City, SIZ_M_CITY);
                  memcpy(pOutbuf+OFF_M_ST, pSaleRec->M_St, SIZ_M_ST);
                  memcpy(pOutbuf+OFF_M_ZIP, pSaleRec->MailZip, SIZ_M_ZIP);
                  memcpy(pOutbuf+OFF_M_ZIP4, pOutbuf+OFF_S_ZIP4, SIZ_M_ZIP4);

                  memcpy(pOutbuf+OFF_M_ADDR_D, pSaleRec->MailAdr1, SIZ_M_ADDR_D);
                  memcpy(pOutbuf+OFF_M_CTY_ST_D, pSaleRec->MailAdr2, SIZ_M_CTY_ST_D);
               }
            }
         }
      }
   }

   // Ignore known non-sale transactions
   if (pSaleRec->NoneSale_Flg == 'Y' || pSaleRec->XferType == 'T')
      return 1;

   lCurPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
   lLastPrice = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);

   // If current sale is older than last sale or newer than today, return
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return 1;
   else if (lCurSaleDt == lLstSaleDt)
   {
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "00103111", 8))
      //   iTmp = 0;
#endif
      iTmp = memcmp(pSaleRec->DocNum, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
      if (iTmp > 0)
      {
         // Make sure Sale1 matches Transfer Doc#
         memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
         memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);
         if (pSaleRec->Seller1[0] > ' ')
            memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

         iTmp = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
         if (iTmp > 1000)
         {
            memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
            *(pOutbuf+OFF_SALE1_CODE) = pSaleRec->SaleCode[0];
         } else if (bUseConfSalePrice)
         {
            iTmp = atoin(pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);
            if (iTmp > 1000)
            {
               memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);
               *(pOutbuf+OFF_SALE1_CODE) = pSaleRec->SaleCode[0];
            }
         }
      }
      return 1;
   }

    // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
   *(pOutbuf+OFF_SALE3_CODE) = *(pOutbuf+OFF_SALE2_CODE);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   *(pOutbuf+OFF_SALE2_CODE) = *(pOutbuf+OFF_SALE1_CODE);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   *(pOutbuf+OFF_AR_CODE1) = pSaleRec->ARCode;

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, SIZ_SALE1_DOCTYPE);

   // Update SalePrice
   long lSalePrice;
   lSalePrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
   if (lSalePrice < 1000 && bUseConfSalePrice)
      lSalePrice = atoin(pSaleRec->ConfirmedSalePrice, SIZ_SALE1_AMT);

   if (lSalePrice > 1000)
   {
      *(pOutbuf+OFF_SALE1_CODE) = pSaleRec->SaleCode[0];
      sprintf(sTmp, "%*d", SIZ_SALE1_AMT, lSalePrice);
      memcpy(pOutbuf+OFF_SALE1_AMT, sTmp, SIZ_SALE1_AMT);
   } else
   {
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);
      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
   }

   // Merge seller
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);
   *(pOutbuf+OFF_MULTI_APN) = pSaleRec->MultiSale_Flg;

   lSaleUpdate++;
   return 0;
}

/******************************** ApplyHistSaleNR() ********************************
 * 
 * ClearSaleFlg = 1 (clear sales whether there is sale update or not)
 *              = 2 (clear sales only if sale update available)
 *                4 (Clear old Grgr only if there is update)
 *                8
 *               16 (Do not check DocNum)
 *               32 (Do not check DocDate)
 *               64 (Do not check SalePrice)
 *              128 (DOn't check anything, apply all sale records)
 *              = 0 (do not clear sales before update)
 *
 * Return 0 if success, 1=No match, -1=EOF
 *
 ***********************************************************************************/

int ApplyHistSaleNR(char *pOutbuf, int iUpdateFlg, bool bReset=false)
{
   static   char  acRec[2048], *pRec=NULL;
   int      iLoop, iRet=1;
   SCSAL_EXT *pSale = (SCSAL_EXT *)acRec;

   if (bReset)
   {
      pRec = NULL;
      return iRet;
   }

   if (!pRec)
      pRec = fgets(acRec, 2048, fdSale);

   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return -1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", myCounty.iApnLen, acRec);
         pRec = fgets(acRec, 2048, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return iRet;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0071C006", 8))
   //   iRet = 0;
#endif

   // Clear old sales
   if ((iUpdateFlg & CLEAR_UPD_SALE) || (iUpdateFlg & CLEAR_OLD_SALE))
   {
      if (iUpdateFlg & CLEAR_OLD_XFER)
         ClearOldSale(pOutbuf, true);
      else
         ClearOldSale(pOutbuf);
   } 

   do
   {
      iRet = ApplySCSalRecNR(pOutbuf, acRec, iUpdateFlg);

      // Get next sale record
      if (!(pRec = fgets(acRec, 2048, fdSale)))
         break;
   } while (!memcmp(pOutbuf, acRec, myCounty.iApnLen));

   lSaleMatch++;
   return iRet;
}

/****************************** ApplyCumSaleNR *******************************
 *
 * Special version to use ASH file to merge into R01.
 *    - If no DocTax, it's not a sale.  Only consider it a transfer even if it has a transfer amount
 *    - 
 *
 * Apply cumulative sales to R01 file.  
 *    iType = SALE_USE_SCUPDXFR : use SCSAL_REC format and update XFER if newer
 *
 *    iClearSaleFlg = 1 : Clear old sales regardless of update available (default)
 *                    2 : Clear old sales only if there is update.
 *
 * Return 0 if successful
 *
 ******************************************************************************/

int ApplyCumSaleNR(int iSkip, char *pCSaleFile, int iType, int iUpdateFlg, int iFileCnt)
{
   char     acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acCumSaleFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet,iTmp, iRollUpd, iFileIdx=1;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bRename, bEof;
   long     lCnt=0;

   if (pCSaleFile && !_access(pCSaleFile, 0))
      strcpy(acCumSaleFile, pCSaleFile);
   else
      strcpy(acCumSaleFile, acCSalFile);

   LogMsg0("ApplyCumSaleNR(): Apply history sale using %s", acCumSaleFile);

   // Open cumulative file
   LogMsg("Open cumulative sale file %s", acCumSaleFile);
   fdSale = fopen(acCumSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening cumulative sale file: %s\n", acCumSaleFile);
      return -5;
   }

   // Reset buffer
   iRet = ApplyHistSaleNR(NULL, 0, true);

   // Loop through all input files
   lSaleUpdate=lSaleMatch=iNoMatch=iRollUpd=0;
   for (iFileIdx = 1; iFileIdx <= iFileCnt; iFileIdx++)
   {
      // Prepare input/output files
      sprintf(acBuf, "R0%d", iFileIdx);
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, acBuf);
      bRename = true;

      // Check input file for processing
      if (_access(acRawFile, 0))
      {
         strcpy(acOutFile, acRawFile);
         sprintf(acBuf, "S0%d", iFileIdx);
         sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, acBuf);
         if (_access(acRawFile, 0))
         {
            LogMsg("Missing input file %s.  Please recheck!", acOutFile);
            return -1;
         }
         bRename = false;
      } else
      {
         sprintf(acBuf, "N0%d", iFileIdx);
         sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, acBuf);
      }

      // Open Input file
      LogMsg("Open input file %s", acRawFile);
      fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
      if (fhIn == INVALID_HANDLE_VALUE)
      {
         LogMsg("***** Error opening input file: %s\n", acRawFile);
         return -6;
      }

      // Open Output file
      LogMsg("Open output file %s", acOutFile);
      fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
      if (fhOut == INVALID_HANDLE_VALUE)
      {
         LogMsg("***** Error opening output file: %s\n", acOutFile);
         return -7;
      }

      // Copy skip record
      memset(acBuf, ' ', iRecLen);
      for (iRet = 0; iRet < iSkip; iRet++)
      {
         ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      }

      // Merge loop
      bEof = false;
      while (!bEof)
      {
         bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

         // Check for read error
         if (!bRet)
         {
            LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
            iRet = -8;
            break;
         }

         // Check for EOF
         if (!nBytesRead)
         {
            iRet = 0;
            break;         // EOF
         }

#ifdef _DEBUG
         //if (!memcmp(acBuf, "0071C006", 8))
         //   iRet = 0;
#endif
         // Apply Sale
         iRet = ApplyHistSaleNR(acBuf, iUpdateFlg);
         if (!iRet)
         {
            iRollUpd++;
         } else if (iRet == -1)
         {  // EOF
            bEof = true;
         } else
            iNoMatch++;

         iRet = 0;
         iTmp = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
         if (iTmp > lLastRecDate && iTmp < lToday)
            lLastRecDate = iTmp;

         // Write to output file
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error output record: %d\n", GetLastError());
            iRet = -9;
            break;
         }
      }

      // Do the rest of the file
      while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
      {
         if (!nBytesRead)
            break;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      }

      if (fhOut)
         CloseHandle(fhOut);
      if (fhIn)
         CloseHandle(fhIn);

      // If everything is OK, rename output file
      if (!iRet && bRename)
      {
         // Rename files
         LogMsg("Remove old file %s", acRawFile);
         iRet = remove(acRawFile);
         if (iRet)
            LogMsg("***** Unable to delete %s (%d)", acRawFile, errno);
         else
         {
            LogMsg("Rename %s to %s", acOutFile, acRawFile);
            iRet = rename(acOutFile, acRawFile);
            if (iRet)
               LogMsg("***** Error renaming %s to %s (%d)", acOutFile, acRawFile, errno);
         }
      }
   }

   // Close files
   if (fdSale)
      fclose(fdSale);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total sale matched:         %u", lSaleMatch);
   LogMsg("Total sale not matched:     %u", iNoMatch);
   LogMsg("Total sale records used:    %u", lSaleUpdate);
   LogMsg("Total roll records updated: %u", iRollUpd);
   LogMsg("Last sale recording date:   %u\n", lLastRecDate);
   lRecCnt = lCnt;

   return iRet;
}

/**************************** ApplyCumSaleWP **********************************
 *
 * Apply cumulative sales to R01 file. Use only record with sale price.
 * Return 0 if successful
 *
 ******************************************************************************/

int ApplyCumSaleWP(int iSkip, char *pCSaleFile, bool bResort, int iType, int iClearSaleFlg)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acCumSaleFile[_MAX_PATH];
   char		*pRec;

   HANDLE   fhIn, fhOut;

   int      iRet,iTmp, iRollUpd;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bRename, bEof;
   long     lCnt=0;

   if (pCSaleFile && !_access(pCSaleFile, 0))
      strcpy(acCumSaleFile, pCSaleFile);
   else
      strcpy(acCumSaleFile, acCSalFile);

   LogMsg0("ApplyCumSaleWP(): Apply history sale using %s", acCumSaleFile);

   // Prepare input/output files
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "T01");
   bRename = true;

   // Check input file for processing
   if (_access(acRawFile, 0))
   {
      strcpy(acOutFile, acRawFile);
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (_access(acRawFile, 0))
      {
         LogMsg("Missing input file %s.  Please recheck!", acOutFile);
         return -1;
      }
      bRename = false;
   }

   // Sort cumulative sale file
   if (bResort)
   {
      //sprintf(acOutFile, acGrGrTmpl, myCounty.acCntyCode, "srt");      // accummulated sale file
      strcpy(acOutFile, acCumSaleFile);
      pRec = strrchr(acOutFile, '.');
      if (pRec)
         strcpy(pRec, ".SRT");
      else
         strcat(acOutFile, ".SRT");
      LogMsg("Sorting %s to %s.", acCumSaleFile, acOutFile);

      // Sort on APN asc, DocDate asc, DocNum asc
      sprintf(acRec, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) F(TXT) DUPO(B8000,1,34) ", SALE_SIZ_APN, SALE_SIZ_DOCNUM);
      iTmp = sortFile(acCumSaleFile, acOutFile, acRec);
      if (!iTmp)
         return -2;

      // Rename SRT file to SLS file
      iRet = remove(acCumSaleFile);
      if (iRet)
         return -3;
      iRet = rename(acOutFile, acCumSaleFile); 
      if (iRet)
         return -4;
   } 

   // Open cumulative file
   LogMsg("Open cumulative sale file %s", acCumSaleFile);
   fdSale = fopen(acCumSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening cumulative sale file: %s\n", acCumSaleFile);
      return -5;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -6;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -7;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   for (iRet = 0; iRet < iSkip; iRet++)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iRet=iNoMatch=iRollUpd=0;

   // Merge loop
   iRollUpd = 0;
   bEof = false;
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for read error
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -8;
         break;
      }

      // Check for EOF
      if (!nBytesRead)
      {
         iRet = 0;
         break;         // EOF
      }

      // Clear old sales
      if (iClearSaleFlg & CLEAR_OLD_SALE)
         ClearOldSale(acBuf);

      // Apply Sale
      iRet = ApplyHistSaleWP(acBuf, iType, iClearSaleFlg);
      if (!iRet)
         iRollUpd++;
      else if (iRet == -1)
      {  // EOF
         bEof = true;
      } else
         iNoMatch++;

      iRet = 0;
      iTmp = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
      if (iTmp > lLastRecDate && iTmp < lToday)
         lLastRecDate = iTmp;

      // Write to output file
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error output record: %d\n", GetLastError());
         iRet = -9;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }

   // Close files
   if (fdSale)
      fclose(fdSale);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // If everything is OK, rename output file
   if (!iRet && bRename)
   {
      // Rename files
      LogMsg("Remove old file %s", acRawFile);
      iRet = remove(acRawFile);
      if (iRet)
         LogMsg("***** Unable to delete %s (%d)", acRawFile, errno);
      else
      {
         LogMsg("Rename %s to %s", acOutFile, acRawFile);
         iRet = rename(acOutFile, acRawFile);
         if (iRet)
            LogMsg("***** Error renaming %s to %s (%d)", acOutFile, acRawFile, errno);
      }
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total sale records applied: %u", iRollUpd);
   LogMsg("Total sale not matched:     %u", iNoMatch);
   LogMsg("Last sale recording date:   %u", lLastRecDate);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return iRet;
}

/******************************* ConvertSaleData() *********************************
 *
 * Convert sale file from one sale format to SCSAL_REC (???_Sale.sls)
 * Type = 1: SALE_REC
 *        2: SALE_REC1 
 *        3: SALE_REC - zero fill for DocNum
 *        4: SALE_REC - remove yyyy from DocNum
 *        5: CSAL_REC
 *        6: GRGR_DEF
 *        7: ALAGRGR
 *        8: GRGR_DOC
 *        9: SLO_SALE
 *       10: ORG_GRGR
 *
 * Return 0 if success, otherwise error
 *
 ***********************************************************************************/

static int convertSaleType1(char *pInbuf, char *pOutbuf)
{
   SALE_REC  *pSale  = (SALE_REC *)pInbuf;
   SCSAL_REC *pCSale = (SCSAL_REC *)pOutbuf;
   int       iLen = strlen(pInbuf);
   
   memcpy(pCSale->Apn, pSale->acApn, SALE_SIZ_APN);
   memcpy(pCSale->DocNum, pSale->acDocNum, SALE_SIZ_DOCNUM);
   memcpy(pCSale->DocDate, pSale->acDocDate, SALE_SIZ_DOCDATE);
   memcpy(pCSale->DocType, pSale->acDocType, SIZ_SALE1_DOCTYPE);
   memcpy(pCSale->SalePrice, pSale->acSalePrice, SALE_SIZ_SALEPRICE);

   if (iLen > 100)
   {
      if (isalpha(pSale->acSaleCode[0]) )
         pCSale->SaleCode[0] = pSale->acSaleCode[0];
      memcpy(pCSale->Seller1, pSale->acSeller, SALE_SIZ_SELLER);
      //memcpy(pCSale->Name1, pSale->acName1, SALE_SIZ_NAME);
      //memcpy(pCSale->Name2, pSale->acName2, SALE_SIZ_NAME);

      memcpy(pCSale->StampAmt, pSale->acStampAmt, SALE_SIZ_STAMPAMT);
      memcpy(pCSale->NumOfPrclXfer, pSale->acNumOfPrclXfer, SALE_SIZ_NOPRCLXFR);
   }
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

static int convertSaleType2(char *pInbuf, char *pOutbuf)
{
   SALE_REC1 *pSale  = (SALE_REC1 *)pInbuf;
   SCSAL_REC *pCSale = (SCSAL_REC *)pOutbuf;
   int       iLen = strlen(pInbuf);
   
   memcpy(pCSale->Apn, pSale->acApn, SALE_SIZ_APN);
   memcpy(pCSale->DocNum, pSale->acDocNum, SALE_SIZ_DOCNUM);
   memcpy(pCSale->DocDate, pSale->acDocDate, SALE_SIZ_DOCDATE);
   memcpy(pCSale->DocType, pSale->acDocType, SIZ_SALE1_DOCTYPE);
   memcpy(pCSale->SalePrice, pSale->acSalePrice, SALE_SIZ_SALEPRICE);

   if (iLen > 100)
   {
      if (isalpha(pSale->acSaleCode[0]))
         pCSale->SaleCode[0] = pSale->acSaleCode[0];
      memcpy(pCSale->Seller1, pSale->acSeller, SALE_SIZ_SELLER);
      memcpy(pCSale->StampAmt, pSale->acStampAmt, SALE_SIZ_STAMPAMT);
      memcpy(pCSale->NumOfPrclXfer, pSale->acNumOfPrclXfer, SALE_SIZ_NOPRCLXFR);

      if (iLen > 132 && pSale->acName1[0] > ' ')
      {
         memcpy(pCSale->Name1, pSale->acName1, SALE_SIZ_NAME);
         memcpy(pCSale->Name2, pSale->acName2, SALE_SIZ_NAME);
      }
   }

   pCSale->ARCode = 'A';
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

static int convertSaleType99(char *pInbuf, char *pOutbuf)
{
   SALE_REC1 *pSale  = (SALE_REC1 *)pInbuf;
   SCSAL_REC *pCSale = (SCSAL_REC *)pOutbuf;
   int       iLen = strlen(pInbuf);
   
   memcpy(pCSale->Apn, pSale->acApn, SALE_SIZ_APN);
   memcpy(pCSale->DocNum, pSale->acDocNum, SALE_SIZ_DOCNUM);
   memcpy(pCSale->DocDate, pSale->acDocDate, SALE_SIZ_DOCDATE);
   memcpy(pCSale->DocType, pSale->acDocType, SIZ_SALE1_DOCTYPE);
   memcpy(pCSale->SalePrice, pSale->acSalePrice, SALE_SIZ_SALEPRICE);

   if (isalpha(pSale->acSaleCode[0]))
      pCSale->SaleCode[0] = pSale->acSaleCode[0];
   memcpy(pCSale->Seller1, pSale->acSeller, SALE_SIZ_SELLER);
   memcpy(pCSale->StampAmt, pSale->acStampAmt, SALE_SIZ_STAMPAMT);
   memcpy(pCSale->NumOfPrclXfer, pSale->acNumOfPrclXfer, SALE_SIZ_NOPRCLXFR);

   if (iLen > 132 && pSale->acName1[0] > ' ')
   {
      memcpy(pCSale->Name1, pSale->acName1, SALE_SIZ_NAME);
      memcpy(pCSale->Name2, pSale->acName2, SALE_SIZ_NAME);
   }

   /*
   if (pCSale->DocDate[0] == ' ')
   {
      char  sTmp[32];
      int   iTmp;

      if (pCSale->DocNum[0] == '0')
         iLen = 3;
      else
         iLen = 2;
      iTmp = atoin(pCSale->DocNum, iLen);
      iLen = sprintf(sTmp, "19%.2d0101", iTmp);
      memcpy(pCSale->DocDate, sTmp, iLen);
   }
   */
   pCSale->ARCode = 'A';
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

// This is the same as Type 1, but zero fill for DocNum
static int convertSaleType3(char *pInbuf, char *pOutbuf)
{
   SALE_REC  *pSale  = (SALE_REC *)pInbuf;
   SCSAL_REC *pCSale = (SCSAL_REC *)pOutbuf;
   int       iLen;
   
   iLen = 0;
   while (pSale->acDocNum[iLen] == ' ' && iLen < 7)
      pSale->acDocNum[iLen++] = '0';
   memcpy(pCSale->Apn, pSale->acApn, SALE_SIZ_APN);
   memcpy(pCSale->DocNum, pSale->acDocNum, SALE_SIZ_DOCNUM);
   memcpy(pCSale->DocDate, pSale->acDocDate, SALE_SIZ_DOCDATE);
   memcpy(pCSale->DocType, pSale->acDocType, SALE_SIZ_DOCTYPE);
   memcpy(pCSale->SalePrice, pSale->acSalePrice, SALE_SIZ_SALEPRICE);
   
   iLen = strlen(pInbuf);
   if (iLen > 100)
   {
      if (isalpha(pSale->acSaleCode[0]) )
         pCSale->SaleCode[0] = pSale->acSaleCode[0];
      memcpy(pCSale->Seller1, pSale->acSeller, SALE_SIZ_SELLER);
      //memcpy(pCSale->Name1, pSale->acName1, SALE_SIZ_NAME);
      //memcpy(pCSale->Name2, pSale->acName2, SALE_SIZ_NAME);

      memcpy(pCSale->StampAmt, pSale->acStampAmt, SALE_SIZ_STAMPAMT);
      if (isalpha(pSale->acNumOfPrclXfer[0]))
         pCSale->NumOfPrclXfer[0] = pSale->acNumOfPrclXfer[0];
      else
      {
         iLen = atoin(pSale->acNumOfPrclXfer, SALE_SIZ_NOPRCLXFR);
         if (iLen > 1)
            pCSale->NumOfPrclXfer[0] = 'M';
      }
   }
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

// This is the same as Type 1, but remove yyyy from DocNum
static int convertSaleType4(char *pInbuf, char *pOutbuf)
{
   SALE_REC  *pSale  = (SALE_REC *)pInbuf;
   SCSAL_REC *pCSale = (SCSAL_REC *)pOutbuf;
   int       iLen;
   
   if (pSale->acDocNum[4] == '-')
   {
      iLen = atoin(&pSale->acDocNum[5], SALE_SIZ_DOCNUM-5);
      if (iLen > 0)
         memcpy(pCSale->DocNum, &pSale->acDocNum[5], SALE_SIZ_DOCNUM-5);
   } else
      memcpy(pCSale->DocNum, pSale->acDocNum, SALE_SIZ_DOCNUM);
   memcpy(pCSale->Apn, pSale->acApn, SALE_SIZ_APN);
   memcpy(pCSale->DocDate, pSale->acDocDate, SALE_SIZ_DOCDATE);
   memcpy(pCSale->DocType, pSale->acDocType, SALE_SIZ_DOCTYPE);
   memcpy(pCSale->SalePrice, pSale->acSalePrice, SALE_SIZ_SALEPRICE);
   
   iLen = strlen(pInbuf);
   if (iLen > 100)
   {
      if (isalpha(pSale->acSaleCode[0]) )
         pCSale->SaleCode[0] = pSale->acSaleCode[0];
      memcpy(pCSale->Seller1, pSale->acSeller, SALE_SIZ_SELLER);
      //memcpy(pCSale->Name1, pSale->acName1, SALE_SIZ_NAME);
      //memcpy(pCSale->Name2, pSale->acName2, SALE_SIZ_NAME);

      memcpy(pCSale->StampAmt, pSale->acStampAmt, SALE_SIZ_STAMPAMT);
      if (isalpha(pSale->acNumOfPrclXfer[0]))
         pCSale->NumOfPrclXfer[0] = pSale->acNumOfPrclXfer[0];
      else
      {
         iLen = atoin(pSale->acNumOfPrclXfer, SALE_SIZ_NOPRCLXFR);
         if (iLen > 1)
            pCSale->NumOfPrclXfer[0] = 'M';
      }
   }
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

// Convert CSAL_REC to SCSAL_REC
static int convertSaleType5(char *pInbuf, char *pOutbuf)
{
   CSAL_REC  *pSale  = (CSAL_REC *) pInbuf;
   SCSAL_REC *pCSale = (SCSAL_REC *)pOutbuf;
   
   memcpy(pCSale->Apn, pSale->Apn, SALE_SIZ_APN);
   memcpy(pCSale->DocDate, pSale->DocDate, SALE_SIZ_DOCDATE);
   memcpy(pCSale->DocNum, pSale->DocNum, SALE_SIZ_DOCNUM);
   memcpy(pCSale->DocType, pSale->DocType, SALE_SIZ_DOCTYPE);
   memcpy(pCSale->SalePrice, pSale->SalePrice, SALE_SIZ_SALEPRICE);
   memcpy(pCSale->Seller1, pSale->Seller, SALE_SIZ_SELLER);
   memcpy(pCSale->StampAmt, pSale->StampAmt, SALE_SIZ_STAMPAMT);

   // SDX has no sale code, but it stores multi-sale flag instead
   if (!memcmp(myCounty.acCntyCode, "SDX", 3))
      pCSale->NumOfPrclXfer[0] = pSale->SaleCode[0];
   else
   {
      memcpy(pCSale->SaleCode, pSale->SaleCode, SALE_SIZ_SALECODE);
      if (isalpha(pSale->NumOfPrclXfer[0]))
         pCSale->NumOfPrclXfer[0] = pSale->NumOfPrclXfer[0];
      else
      {
         int iTmp = atoin(pSale->NumOfPrclXfer, SALE_SIZ_NOPRCLXFR);
         if (iTmp > 1)
            pCSale->NumOfPrclXfer[0] = 'M';
      }
   }

   memcpy(pCSale->Name1, pSale->Name1, SALE_SIZ_BUYER);
   memcpy(pCSale->Name2, pSale->Name2, SALE_SIZ_BUYER);
   memcpy(pCSale->CareOf, pSale->CareOf, SALE_SIZ_CAREOF);
   memcpy(pCSale->MailAdr1, pSale->MailAdr1, SALE_SIZ_M_ADR1);
   memcpy(pCSale->MailAdr2, pSale->MailAdr2, SALE_SIZ_M_ADR2);
   memcpy(pCSale->MailZip, pSale->MailZip, SALE_SIZ_M_ZIP);
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

// Convert GRGR_DEF to SCSAL_EXT
static int convertSaleType6(char *pInbuf, char *pOutbuf)
{
   GRGR_DEF  *pSale  = (GRGR_DEF *) pInbuf;
   SCSAL_EXT *pCSale = (SCSAL_EXT *)pOutbuf;
   int       iTmp, iDocType;
   char      acTmp[1024];

   if (!isdigit(pSale->APN[0]))
      return -1;

   memset(pOutbuf, ' ', sizeof(SCSAL_EXT));
   memcpy(pCSale->Apn, pSale->APN, SALE_SIZ_APN);
   memcpy(pCSale->DocDate, pSale->DocDate, SALE_SIZ_DOCDATE);
   memcpy(pCSale->DocNum, pSale->DocNum, SALE_SIZ_DOCNUM);
   memcpy(pCSale->DocTitle, pSale->DocTitle, SALE_SIZ_DOCTITLE);
   if (isdigit(pSale->DocType[0]))
   {
      memcpy(pCSale->DocType, pSale->DocType, SIZ_GR_DOCTYPE);
      pCSale->NoneSale_Flg = pSale->NoneSale;
      pCSale->NoneXfer_Flg = pSale->NoneXfer;
   } else
   {
      // SCL
      if (!memcmp(pSale->DocTitle, "DEED   ", 7))
         iDocType = 13;
      else if (!memcmp(pSale->DocTitle, "NOTICE OF DEF", 13))
         iDocType = 52; 
      else if (!memcmp(pSale->DocTitle, "TRUSTEES D", 10) || !memcmp(pSale->DocTitle, "NOTICE TRUSTEE SALE", 18))
         iDocType = 27;
      else if (!memcmp(pSale->DocTitle, "CERTIFICATE SALE", 16))
         iDocType = 67;       // Tax Deed
      else 
      {
         pCSale->NoneSale_Flg = 'Y';
         if (!memcmp(pSale->DocTitle, "ASSIGNMENT  ", 12))
            iDocType = 7;
         else if (!memcmp(pSale->DocTitle, "CORRECTION DEED", 15))
            iDocType = 9; 
         else if (!memcmp(pSale->DocTitle, "DEED OF TRUST", 13))
            iDocType = 65; 
         else if (!memcmp(pSale->DocTitle, "SHERIFFS DEED EXECUTION", 23))
            iDocType = 25; 
         else if (!memcmp(pSale->DocTitle, "QCDD", 4) || !memcmp(pSale->DocTitle, "DEED/QUITCLAIM", 9))
            iDocType = 4; 
         else if (!memcmp(pSale->DocTitle, "EASEMENT", 4))
            iDocType = 40; 
         else if (!memcmp(pSale->DocTitle, "ADEA", 4))
            iDocType = 6; 
         else
         {
            pCSale->NoneXfer_Flg = 'Y';
            iDocType = 0;
         }
      }

      if (iDocType > 0)
      {
         iTmp = sprintf(acTmp, "%d", iDocType);
         memcpy(pCSale->DocType, acTmp, iTmp);
      }
   }

   memcpy(pCSale->SalePrice, pSale->SalePrice, SALE_SIZ_SALEPRICE);
   memcpy(pCSale->StampAmt, pSale->Tax, SALE_SIZ_STAMPAMT);

   memcpy(pCSale->Seller1, pSale->Grantors[0].Name, SALE_SIZ_SELLER);
   memcpy(pCSale->Seller2, pSale->Grantors[1].Name, SALE_SIZ_SELLER);
   memcpy(pCSale->Name1, pSale->Grantees[0].Name, SIZ_GR_NAME);
   memcpy(pCSale->Name2, pSale->Grantees[1].Name, SIZ_GR_NAME);
   memcpy(pCSale->CareOf, pSale->CurCareOf, SIZ_GR_NAME);

   //if (pSale->CurMailing.strName[0] > ' ')
   //{
   //   sprintf(acTmp, "%s %s %s %s %s %s", pSale->CurMailing.strNum,
   //      pSale->CurMailing.strSub, pSale->CurMailing.strDir,
   //      pSale->CurMailing.strName,pSale->CurMailing.strSfx,pSale->CurMailing.Unit);

   //   iTmp = blankRem(acTmp);
   //   vmemcpy(pCSale->MailAdr1, acTmp, SALE_SIZ_M_ADR1, iTmp);

   //   sprintf(acTmp, "%.17s %.2s %.5s", pSale->CurMailing.City, pSale->CurMailing.State, pSale->CurMailing.Zip);
   //   iTmp = blankRem(acTmp);
   //   memcpy(pCSale->MailAdr2, acTmp, SALE_SIZ_M_ADR2, iTmp);

   //   memcpy(pCSale->MailZip, pSale->CurMailing.Zip, SIZ_M_ZIP+SIZ_M_ZIP4);
   //}

   // Multi parcel
   iTmp = atoin(pSale->ParcelCount, SIZ_GR_PRCLCNT);
   if (iTmp > 1)
      pCSale->MultiSale_Flg = 'Y';

   pCSale->Etal = pSale->MoreName;
   pCSale->OwnerMatched = pSale->Owner_Matched;
   pCSale->ApnMatched = pSale->APN_Matched;
   pCSale->ARCode = 'R';
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}
         
// Convert ALAGRGR to SCSAL_REC
static int convertSaleType7(char *pInbuf, char *pOutbuf)
{
   ALAGRGR   *pSale  = (ALAGRGR *)  pInbuf;
   SCSAL_REC *pCSale = (SCSAL_REC *)pOutbuf;

   if (!isdigit(pSale->APN[0]))
      return -1;

   memcpy(pCSale->Apn, pSale->APN, SALE_SIZ_APN);
   memcpy(pCSale->DocDate, pSale->DocDate, SALE_SIZ_DOCDATE);
   memcpy(pCSale->DocNum, pSale->DocNum, SALE_SIZ_DOCNUM);
   if (isdigit(pSale->DocTitle[0]))
      memcpy(pCSale->DocType, pSale->DocTitle, SIZ_SALE1_DOCTYPE);
   else
   {
      if (!memcmp(pSale->DocTitle, "ADEA", 4))
      {
         pCSale->DocType[0] = '6';
         pCSale->NoneSale_Flg = 'Y';
      } else if (!memcmp(pSale->DocTitle, "NOTI", 4))
      {  // Don't know what kind of notice
         //memcpy(pCSale->DocType, "   ", 3);
         pCSale->NoneSale_Flg = 'Y';
      } else
         LogMsg("Unknown DocTitle: %.8s [APN=%.*s] [DOCNUM=%.12s]", pSale->DocTitle, iApnLen, pSale->APN, pSale->DocNum);
   }
   memcpy(pCSale->SalePrice, pSale->SalePrice, SALE_SIZ_SALEPRICE);
   memcpy(pCSale->StampAmt, pSale->Tax, SALE_SIZ_STAMPAMT);

   memcpy(pCSale->Seller1, pSale->Grantor[0], SALE_SIZ_SELLER);
   memcpy(pCSale->Seller2, pSale->Grantor[1], SALE_SIZ_SELLER);
   memcpy(pCSale->Name1,   pSale->Grantee[0], SIZ_GR_NAME);
   memcpy(pCSale->Name2,   pSale->Grantee[1], SIZ_GR_NAME);

   pCSale->Etal = pSale->MoreName;
   pCSale->OwnerMatched = pSale->Owner_Matched;
   pCSale->ApnMatched = pSale->APN_Matched;
   pCSale->ARCode = 'R';
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

// Convert GRGR_DOC to SCSAL_REC
static int convertSaleType8(char *pInbuf, char *pOutbuf)
{
   GRGR_DOC  *pSale  = (GRGR_DOC *) pInbuf;
   SCSAL_REC *pCSale = (SCSAL_REC *)pOutbuf;
   int       iTmp;

   if (!isdigit(pSale->APN[0]))
      return -1;

   memcpy(pCSale->Apn, pSale->APN, SALE_SIZ_APN);
   memcpy(pCSale->DocDate, pSale->DocDate, SALE_SIZ_DOCDATE);
   memcpy(pCSale->DocNum, pSale->DocNum, SALE_SIZ_DOCNUM);
   if (isdigit(pSale->DocTitle[0]))
      memcpy(pCSale->DocType, pSale->DocTitle, SIZ_SALE1_DOCTYPE);
   else
   {
      if (!memcmp(pSale->DocTitle, "ADEA", 4))
      {
         pCSale->DocType[0] = '6';
         pCSale->NoneSale_Flg = 'Y';
      } else if (!memcmp(pSale->DocTitle, "NOTI", 4))
      {  // Don't know what kind of notice
         //memcpy(pCSale->DocType, "   ", 3);
         pCSale->NoneSale_Flg = 'Y';
      } else
         LogMsg("Unknown DocTitle: %.8s [APN=%.*s] [DOCNUM=%.12s]", pSale->DocTitle, iApnLen, pSale->APN, pSale->DocNum);
   }
   memcpy(pCSale->SalePrice, pSale->SalePrice, SALE_SIZ_SALEPRICE);
   memcpy(pCSale->StampAmt, pSale->DocTax, SALE_SIZ_STAMPAMT);

   memcpy(pCSale->Seller1, pSale->Grantor[0], SALE_SIZ_SELLER);
   memcpy(pCSale->Seller2, pSale->Grantor[1], SALE_SIZ_SELLER);
   memcpy(pCSale->Name1, pSale->Grantee[0], SIZ_GR_NAME);
   memcpy(pCSale->Name2, pSale->Grantee[1], SIZ_GR_NAME);

   // Multi parcel
   if (pSale->MultiApn > ' ')
      pCSale->MultiSale_Flg = 'Y';

   iTmp = atoin(pSale->NameCnt, SIZ_GD_NAMECNT);
   if (iTmp > 2)
      pCSale->Etal = 'Y';
   pCSale->ARCode = 'R';
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

// Convert SLO_SALE to SCSAL_REC
static int convertSaleType9(char *pInbuf, char *pOutbuf)
{
   SLO_SALE  *pSale  = (SLO_SALE *) pInbuf;
   SCSAL_REC *pCSale = (SCSAL_REC *)pOutbuf;
   int       iTmp, lTmp;
   char      acTmp[256];

   if (!isdigit(pSale->Apn[0]))
      return -1;

   memcpy(pCSale->Apn, pSale->Apn, SALE_SIZ_APN);
   memcpy(pCSale->DocDate, pSale->DocDate, SALE_SIZ_DOCDATE);
   memcpy(pCSale->DocNum, pSale->DocNum, SALE_SIZ_DOCNUM);
   if (isdigit(pSale->DocType[0]))
      memcpy(pCSale->DocType, pSale->DocType, SIZ_SALE1_DOCTYPE);
   else
   {
      // Translate DocType
      iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], pSale->DocType, iNumDeeds);
      if (iTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d  ", iTmp);
         memcpy(pCSale->DocType, acTmp, iTmp);
      } else if (!memcmp(pSale->DocType, "AF", 2))
      {
         pCSale->DocType[0] = '6';
      } else if (!memcmp(pSale->DocType, "GD", 2))
      {
         pCSale->DocType[0] = '1';
      } else if (!memcmp(pSale->DocType, "QC", 2))
      {  
         pCSale->DocType[0] = '4';
      } else
         LogMsg("Unknown DocTitle: %.8s [APN=%.*s] [DOCNUM=%.12s]", pSale->DocType, iApnLen, pSale->Apn, pSale->DocNum);
   }

   lTmp = atoin(pSale->DocTax, SALE_SIZ_STAMPAMT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.2f", (double)(lTmp/100.0));
      memcpy(pCSale->StampAmt, acTmp, iTmp);
      memcpy(pCSale->SalePrice, pSale->SalePrice, SALE_SIZ_SALEPRICE);
   }

   //memcpy(pCSale->SaleCode, pSale->SaleCode, SALE_SIZ_SALECODE);
   switch(pSale->SaleCode[0])
   {
      case 'F':   // Full value
         pCSale->SaleCode[0] = pSale->SaleCode[0];
         break;
      case 'L':   // Less liens
         pCSale->SaleCode[0] = 'N';
         break;
      case 'N':   // NO CONSIDERATION
         if (pCSale->DocType[0] == '4')
            pCSale->NoneSale_Flg = 'Y';
         if (pSale->SaleCode[1] != 'C')
            pCSale->SaleCode[0] = pSale->SaleCode[1];
         break;
      case 'M':   // Multi Parcel
         pCSale->MultiSale_Flg = 'Y';
         if (pSale->SaleCode[1] == 'F' || pSale->SaleCode[1] == 'L')
            pCSale->SaleCode[0] = pSale->SaleCode[1];
         break;
   }

   if (*pSale->Grantor[0] > ' ')
   {
      memcpy(acTmp, pSale->Grantor[0], SALE_SIZ_SELLER);
      iTmp = blankRem(acTmp, SALE_SIZ_SELLER);
      memcpy(pCSale->Seller1, acTmp, iTmp);

      if (*pSale->Grantor[1] > ' ')
      {
         memcpy(acTmp, pSale->Grantor[1], SALE_SIZ_SELLER);
         iTmp = blankRem(acTmp, SALE_SIZ_SELLER);
         memcpy(pCSale->Seller2, acTmp, iTmp);
      }
   }
   if (*pSale->Grantee[0] > ' ')
   {
      memcpy(acTmp, pSale->Grantee[0], SALE_SIZ_NAME);
      iTmp = blankRem(acTmp, SALE_SIZ_NAME);
      memcpy(pCSale->Name1, acTmp, iTmp);

      if (*pSale->Grantor[1] > ' ')
      {
         memcpy(acTmp, pSale->Grantee[1], SALE_SIZ_NAME);
         iTmp = blankRem(acTmp, SALE_SIZ_NAME);
         memcpy(pCSale->Name2, acTmp, iTmp);
      }
   }

   iTmp = atoin(pSale->NameCnt, 2);
   if (iTmp > 2)
      pCSale->Etal = 'Y';
   pCSale->ARCode = 'A';
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

// Convert ORG_GRGR to SCSAL_EXT
extern IDX_SALE ORG_DocCode[];
static int convertSaleType10(char *pInbuf, char *pOutbuf)
{
   ORG_GRGR  *pSale  = (ORG_GRGR *) pInbuf;
   SCSAL_EXT *pCSale = (SCSAL_EXT *)pOutbuf;
   int       iTmp;
   double    dTmp;
   char      acTmp[256];

   if (!isdigit(pSale->ParcelNo[0]))
      return -1;

   memset(pOutbuf, ' ', sizeof(SCSAL_EXT));
   memcpy(pCSale->Apn, pSale->ParcelNo, SALE_SIZ_APN);
   memcpy(pCSale->DocDate, pSale->DocDate, SALE_SIZ_DOCDATE);
   memcpy(pCSale->DocNum, pSale->DocNum, SALE_SIZ_DOCNUM);

   if (pSale->DocTitle[0] > ' ')
   {
      memcpy(pCSale->DocTitle, pSale->DocTitle, SALE_SIZ_DOCTITLE);
      iTmp = findDocType(pSale->DocTitle, (IDX_SALE *)&ORG_DocCode);
      if (iTmp >= 0)
      {
         memcpy(pCSale->DocType, ORG_DocCode[iTmp].DocType, ORG_DocCode[iTmp].DocTypeLen);
         pCSale->NoneSale_Flg = (ORG_DocCode[iTmp].isSale == 'Y' ? 'N':'Y');
         pCSale->NoneXfer_Flg = (ORG_DocCode[iTmp].isTransfer == 'Y' ? 'N':'Y');
      } else
         LogMsg("*** Unknown DocTitle %.22s [%.12s]", pSale->DocTitle, pSale->ParcelNo);
   }

   dTmp = atofn(pSale->DocTax, SALE_SIZ_STAMPAMT);
   if (dTmp > 0.0)
   {
      iTmp = sprintf(acTmp, "%.2f", dTmp);
      memcpy(pCSale->StampAmt, acTmp, iTmp);
      memcpy(pCSale->SalePrice, pSale->SalePrice, SALE_SIZ_SALEPRICE);
   }

   // Grantor
   if (*pSale->Grantor[0] > ' ')
   {
      memcpy(acTmp, pSale->Grantor[0], SALE_SIZ_SELLER);
      iTmp = blankRem(acTmp, SALE_SIZ_SELLER);
      memcpy(pCSale->Seller1, acTmp, iTmp);

      if (*pSale->Grantor[1] > ' ')
      {
         memcpy(acTmp, pSale->Grantor[1], SALE_SIZ_SELLER);
         iTmp = blankRem(acTmp, SALE_SIZ_SELLER);
         memcpy(pCSale->Seller2, acTmp, iTmp);
      }
   }

   // Grantee
   if (*pSale->Grantee[0] > ' ')
   {
      memcpy(acTmp, pSale->Grantee[0], SALE_SIZ_NAME);
      iTmp = blankRem(acTmp, SALE_SIZ_NAME);
      memcpy(pCSale->Name1, acTmp, iTmp);

      if (*pSale->Grantor[1] > ' ')
      {
         memcpy(acTmp, pSale->Grantee[1], SALE_SIZ_NAME);
         iTmp = blankRem(acTmp, SALE_SIZ_NAME);
         memcpy(pCSale->Name2, acTmp, iTmp);
      }
   }

   // Mail addr
   memcpy(pCSale->MailAdr1, pSale->Addr1, SALE_SIZ_M_ADR1);
   if (pSale->City[0] > ' ')
   {
      memcpy(pCSale->M_City, pSale->City, SALE_SIZ_M_CITY);
      memcpy(pCSale->M_St, pSale->State, SALE_SIZ_M_ST);
      memcpy(pCSale->MailZip, pSale->Zip, SALE_SIZ_M_ZIP);
      sprintf(acTmp, "%.*s %.2s %.5s", SALE_SIZ_M_CITY, pSale->City, pSale->State, pSale->Zip);
      iTmp = blankRem(acTmp);
      vmemcpy(pCSale->MailAdr2, acTmp, SALE_SIZ_M_ADR2, iTmp);
   }

   // Tract-Block-Lot
   memcpy(pCSale->Tract, pSale->Tract, SALE_SIZ_TRACT);
   memcpy(pCSale->Block, pSale->Block, SALE_SIZ_BLOCK);
   memcpy(pCSale->Lot, pSale->Lot, SALE_SIZ_LOT);

   // Etal
   pCSale->Etal = pSale->MoreName;
   pCSale->ARCode = 'R';
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

int convertSaleData(char *pCnty, char *pInfile, int iType, char *pOutfile)
{
   char     acInbuf[2048], acOutbuf[2048], *pRec;
   char     acOutFile[_MAX_PATH], acSrtFile[_MAX_PATH];
   long     lCnt=0, lOut=0, iRet;
   FILE     *fdOut;

   SCSAL_REC *pOutRec = (SCSAL_REC *)&acOutbuf[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** ConvertSaleData(): Missing input file: %s", pInfile);
      return -1;
   }

   sprintf(acOutFile, "%s\\%s\\%s_Sale.tmp", acTmpPath, pCnty, pCnty);
   LogMsg("Convert %s to %s.", pInfile, acOutFile);

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 2048, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0010100230", 10))
      //   iRet = 0;
#endif
      // Drop bad record
      //iRet = atoin(acInbuf, 6);
      //if (acInbuf[0] == ' ' || iRet < 1000 || iRet > 999998)
      //{
      //   lCnt++;
      //   continue;
      //}

      // Reformat sale
      memset(acOutbuf, ' ', sizeof(SCSAL_REC));
      switch (iType)
      {
         case CONV_SALE_REC1:
            iRet = convertSaleType1(acInbuf, acOutbuf);
            break;
         case CONV_SALE_REC2:
            iRet = convertSaleType2(acInbuf, acOutbuf);
            break;
         case CONV_SALE_REC3:
            iRet = convertSaleType3(acInbuf, acOutbuf);
            break;
         case CONV_SALE_REC4:
            iRet = convertSaleType4(acInbuf, acOutbuf);
            break;
         case CONV_CSAL_REC:
            iRet = convertSaleType5(acInbuf, acOutbuf);
            break;
         case CONV_GRGR_DEF:
            iRet = convertSaleType6(acInbuf, acOutbuf);
            break;
         case CONV_GRGR_ALA:
            iRet = convertSaleType7(acInbuf, acOutbuf);
            break;
         case CONV_GRGR_DOC:
            iRet = convertSaleType8(acInbuf, acOutbuf);
            break;
         case CONV_SALE_SLO:
            iRet = convertSaleType9(acInbuf, acOutbuf);
            break;
         case CONV_GRGR_ORG:
            iRet = convertSaleType10(acInbuf, acOutbuf);
            break;
         case 99:
            iRet = convertSaleType99(acInbuf, acOutbuf);
            break;
         default:
            iRet = 1;
      }

      // Write to output file
      if (!iRet)
      {
         fputs(acOutbuf, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   // Sort sale file - APN (asc), Saledate (asc), DocNum (asc), Sale price (desc)
   sprintf(acInbuf, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) DUPO(B8000,1,34)");
   if (pOutfile)
      strcpy(acSrtFile, pOutfile);
   else
      sprintf(acSrtFile, acESalTmpl, pCnty, pCnty, "sls");
   iRet = sortFile(acOutFile, acSrtFile, acInbuf);
   if (!iRet)
      iRet = 1;
   else
      iRet = 0;

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   printf("\nTotal output records: %u\n", lCnt);
   return iRet;
}

/***************************** convertXferData *******************************
 *
 * Convert Xfer (XFER_REC) file to standard cum sale (SCSAL_REC) file.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int convertXferData(char *pInfile, char *pOutfile)
{
   char     acInbuf[1024], acOutbuf[1024], *pRec;
   long     lCnt=0, lOut=0;
   FILE     *fdOut;

   SCSAL_REC *pOutRec = (SCSAL_REC *)&acOutbuf[0];
   XFER_REC  *pInRec  = (XFER_REC  *)&acInbuf[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** convertXferData(): Missing input file: %s", pInfile);
      return -1;
   }

   LogMsg("Convert %s to %s.", pInfile, pOutfile);

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   LogMsg("Create output sale file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", pOutfile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

      // Reformat sale
      memset(acOutbuf, ' ', sizeof(SCSAL_REC));
      memcpy(pOutRec->Apn, pInRec->acApn, SALE_SIZ_APN);

      // Sale1
      if (pInRec->acDoc1Date[0] > ' ')
      {
         memcpy(pOutRec->DocNum,   pInRec->acDoc1Num,   SALE_SIZ_DOCNUM);
         memcpy(pOutRec->DocDate,  pInRec->acDoc1Date,  SALE_SIZ_DOCDATE);
         memcpy(pOutRec->DocType,  pInRec->acDoc1Type,  SALE_SIZ_DOCTYPE);
         memcpy(pOutRec->SalePrice,pInRec->acSale1Price,SALE_SIZ_SALEPRICE);
         memcpy(pOutRec->SaleCode, pInRec->acSale1Code, SALE_SIZ_SALECODE);
         memcpy(pOutRec->Seller1,  pInRec->acSeller,    SALE_SIZ_SELLER);
         pOutRec->CRLF[0] = '\n';
         pOutRec->CRLF[1] = 0;

         // Write to output file
         fputs(acOutbuf, fdOut);
         lOut++;

         // Sale2
         if (pInRec->acDoc2Date[0] > ' ')
         {
            memcpy(pOutRec->DocNum,   pInRec->acDoc2Num,   SALE_SIZ_DOCNUM);
            memcpy(pOutRec->DocDate,  pInRec->acDoc2Date,  SALE_SIZ_DOCDATE);
            memcpy(pOutRec->DocType,  pInRec->acDoc2Type,  SALE_SIZ_DOCTYPE);
            memcpy(pOutRec->SalePrice,pInRec->acSale2Price,SALE_SIZ_SALEPRICE);
            memcpy(pOutRec->SaleCode, pInRec->acSale2Code, SALE_SIZ_SALECODE);

            // Write to output file
            fputs(acOutbuf, fdOut);
            lOut++;

            // Sale3
            if (pInRec->acDoc3Date[0] > ' ')
            {
               memcpy(pOutRec->DocNum,   pInRec->acDoc3Num,   SALE_SIZ_DOCNUM);
               memcpy(pOutRec->DocDate,  pInRec->acDoc3Date,  SALE_SIZ_DOCDATE);
               memcpy(pOutRec->DocType,  pInRec->acDoc3Type,  SALE_SIZ_DOCTYPE);
               memcpy(pOutRec->SalePrice,pInRec->acSale3Price,SALE_SIZ_SALEPRICE);
               memcpy(pOutRec->SaleCode, pInRec->acSale3Code, SALE_SIZ_SALECODE);

               // Write to output file
               fputs(acOutbuf, fdOut);
               lOut++;
            }
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   printf("\n");
   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lOut);
   LogMsg("Last recording date:        %u", lLastRecDate);

   return 0;
}

/***************************** CleanupSales **********************************
 *
 * Clean up (SCSAL_REC) file by removing sales that are duplicate with same APN
 * and date but blank DocNum.  SFX
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int CleanupSales(char *pInfile)
{
   char     acInbuf[2048], acOutbuf[2048], acSortFile[_MAX_PATH], *pRec;
   long     lCnt=0, lOut=0, lDrop=0, iTmp;
   FILE     *fdOut;

   SCSAL_REC *pOutRec = (SCSAL_REC *)&acOutbuf[0];
   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];

   LogMsg0("Cleanup %s", pInfile);

   if (_access(pInfile, 0))
   {
      LogMsg("***** CleanupSales(): Missing input file: %s", pInfile);
      return -1;
   }

   // Resort sale file - APN ASC, DATE ASC, DOCNUM DESC
   strcpy(acSortFile, pInfile);
   pRec = strrchr(acSortFile, '.');
   strcpy(pRec, ".SRT");

   sprintf(acInbuf, "S(1,%d,C,A,27,8,C,A,15,%d,C,D) F(TXT) DUPO(B8000,1,34) ", iApnLen, SALE_SIZ_DOCNUM);
   iTmp = sortFile(pInfile, acSortFile, acInbuf);
   if (!iTmp)
      return -10;

   // Open input file
   LogMsg("Open input sale file %s", acSortFile);
   fdSale = fopen(acSortFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", acSortFile);
      return -2;
   }

   // Open output file
   LogMsg("Create output sale file %s", pInfile);
   fdOut = fopen(pInfile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", pInfile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0025 024", 8))
      //   iTmp = 0;
#endif
      if (!memcmp(acOutbuf, acInbuf, iApnLen) &&
          !memcmp(pInRec->DocDate, pOutRec->DocDate, 8) &&
          pInRec->DocNum[0] == ' ')
      {
         if (pInRec->SalePrice[6] > ' ' && pOutRec->SalePrice[6] == ' ')
         {
            // Copy DocNum to InRec and output.  Later dedup and drop prev one.
            memcpy(pInRec->DocNum, pOutRec->DocNum, SALE_SIZ_DOCNUM);
            strcpy(acOutbuf, acInbuf);
            fputs(acOutbuf, fdOut);
            lOut++;
         } else
            lDrop++;
      } else
      {
         strcpy(acOutbuf, acInbuf);
         fputs(acOutbuf, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   // Resort sale file - APN ASC, DATE ASC, DOCNUM ASC, PRICE DESC
   sprintf(acInbuf, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) F(TXT) DUPO(B8000,1,34) ", iApnLen, SALE_SIZ_DOCNUM);
   lOut = sortFile(pInfile, acSortFile, acInbuf);
   if (lOut > 0)
   {
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".TMP");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save temp sale file
      iTmp = rename(pInfile, acInbuf);

      iTmp = rename(acSortFile, pInfile);
   } else 
      iTmp = -1;

   printf("\n");
   LogMsgD("Total input records:     %u", lCnt);
   LogMsgD("      output records:    %u", lOut);
   LogMsgD("      drop records:      %u\n", lDrop);

   return iTmp;
}

/******************************************************************************
 *
 * Return 0 if success.  Otherwise error;
 *
 ******************************************************************************/

int createSaleOutrec(SALES_REC *pSales, LPSTR pSql, LPSTR pDocLink)
{
   char     acTmp[MAX_RECSIZE];

   quoteRem(pSales->Seller1);
   quoteRem(pSales->Buyer1);
   quoteRem(pSales->Seller2);
   quoteRem(pSales->Buyer2);
   quoteRem(pSales->M_Addr1);
   quoteRem(pSales->M_Addr2);
   sprintf(acTmp, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s",
               pSales->Apn,
               pSales->DocNum,
               pSales->DocDate,
               pSales->DocType,
               pSales->DocCode,
               pSales->SaleCode,
               pSales->Seller1,
               pSales->Buyer1,
               pSales->Seller2,
               pSales->Buyer2,
               pSales->Price,
               pSales->TaxAmt,
               pSales->GrpSale,
               pSales->GrpAsmt,
               pSales->XferType,
               pSales->NumXfer,
               pSales->M_Addr1,
               pSales->M_Addr2,
               pSales->M_Zip,
               pSales->ARCode, pSales->NoneSale_Flg, pDocLink);

   blankRem(acTmp);
   strcpy(pSql, acTmp);
   strcat(pSql, "\n");

   return 0;
}

LPSTR createSaleHeader(LPSTR sHdr, char cDelimiter)
{
   sprintf(sHdr, "Apn|DocNum|DocDate|DocType|DocCode|SaleCode|Seller1|Buyer1|Seller2|Buyer2|"
      "Price|TaxAmt|GrpSale|GrpAsmt|XferType|NumXfer|M_Addr1|M_Addr2|M_Zip|ARCode|DocLink\n");
   return sHdr;
}

LPSTR createGrgrHeader(LPSTR sHdr, char cDelimiter)
{
   sprintf(sHdr, "Apn|DocNum|DocDate|DocType|DocCode|SaleCode|Seller1|Buyer1|Seller2|Buyer2|"
      "Price|TaxAmt|GrpSale|GrpAsmt|XferType|NumXfer|M_Addr1|M_Addr2|M_Zip|ARCode|DocLink\n");
   return sHdr;
}

/******************************************************************************
 *
 * Type contains bitmap value.  Following value can OR together for multiple types
 *
 * iType 1: generate Sale import (default)
 *       2: generate GRGR_DOC import
 *       4: generate GRGR_DEF import
 *
 * Return: Number of output records
 *
 ******************************************************************************/

int createSaleImport(LPCSTR CountyCode, LPCSTR pInfile, LPCSTR pOutfile, int iType, bool bInclHdr)
{
   char     *pTmp, acInfile[_MAX_PATH], acOutfile[_MAX_PATH], acRec[MAX_RECSIZE], 
            acTmp[MAX_RECSIZE];
   BOOL     bEof;
   int      iRet, lCnt=0;
   FILE     *fdIn, *fdOut;

   // Input record
   SCSAL_REC *pSale;
   // Output record
   SALES_REC  mySales;

   if (iType & TYPE_SCSAL_REC)
   {
      LogMsg0("Create Import sale file ...");
      if (pInfile && *pInfile > ' ')
         strcpy(acInfile, pInfile);
      else
      {
         GetIniString("Data", "SaleOut", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acInfile, acTmp, CountyCode, CountyCode, "sls");
      }
   } else if (iType & TYPE_GRGR_DOC)
   {
      LogMsg0("Create Import GrGr file ...");
      if (pInfile && *pInfile > ' ')
         strcpy(acInfile, pInfile);
      else
      {
         GetIniString("Data", "GrGrOut", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acInfile, acTmp, CountyCode, CountyCode, "sls");
      }
   } else
   {
      LogMsg("??? Incorrect file type: %d", iType);
      return -999;
   }

   strcpy(acOutfile, pOutfile);

   // Open Sales file
   LogMsg("Open Sales/GrGr file %s", acInfile);
   fdIn = fopen(acInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Sales/GrGr file: %s\n", acInfile);
      return -1;
   }

   LogMsg("Create import Sales/GrGr file %s", acInfile);
   if (!(fdOut = fopen(acOutfile, "w")))
   {
      LogMsg("***** Error creating output file %s", acOutfile);
      return -2;
   }

   // Write header
   if (bInclHdr)
      fputs(createSaleHeader(acTmp, '|'), fdOut);

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   bEof = (pTmp ? false:true);
   pSale = (SCSAL_REC *)&acRec[0];

   // Merge loop
   while (pTmp && !feof(fdIn))
   {
      memset(&mySales, 0, sizeof(SALES_REC));
      strncpy(mySales.Apn, pSale->Apn, iApnLen);

#ifdef _DEBUG
      //if (!memcmp(mySales.Apn, "077273017", 9))
      //   iRet = 0;
#endif

      strncpy(mySales.DocNum, pSale->DocNum, SALE_SIZ_DOCNUM);
      myTrim(mySales.DocNum);
      replCharEx(mySales.DocNum, "+*`.{}&%$", ' ', 0, false);
      remChar(mySales.DocNum, ' ');
      strncpy(mySales.DocDate, pSale->DocDate, SALE_SIZ_DOCDATE);
      strncpy(mySales.DocType, pSale->DocType, SALE_SIZ_DOCTYPE);
      myTrim(mySales.DocType);
      strncpy(mySales.DocCode, pSale->DocCode, SALE_SIZ_DOCCODE);
      myTrim(mySales.DocCode);
      strncpy(mySales.SaleCode, pSale->SaleCode, SALE_SIZ_SALECODE);
      myTrim(mySales.SaleCode);

      if (pSale->Seller1[0] > ' ')
      {
         strncpy(mySales.Seller1, pSale->Seller1, SALE_SIZ_SELLER);
         myTrim(mySales.Seller1);
      }
      if (pSale->Seller2[0] > ' ')
      {
         strncpy(mySales.Seller2, pSale->Seller2, SALE_SIZ_SELLER);
         myTrim(mySales.Seller2);
      }

      if (pSale->Name1[0] > ' ')
      {
         strncpy(mySales.Buyer1, pSale->Name1, SALE_SIZ_BUYER);
         myTrim(mySales.Buyer1);
      }
      if (pSale->Name2[0] > ' ')
      {
         strncpy(mySales.Buyer2, pSale->Name2, SALE_SIZ_BUYER);
         myTrim(mySales.Buyer2);
      }

      if (pSale->MailAdr1[0] > ' ')
      {
         strncpy(mySales.M_Addr1, pSale->MailAdr1, SALE_SIZ_M_ADR1);
         myTrim(mySales.M_Addr1);
         strncpy(mySales.M_Addr2, pSale->MailAdr2, SALE_SIZ_M_ADR2);
         myTrim(mySales.M_Addr2);
      }

      //if (iType & 4)
      //{
      //   strncpy(mySales.GrpAsmt, pSale->OtherApn, iApnLen);
      //   mySales.GrpSale[0] = 'O';
      //}

      long lTmp = atoin(pSale->NumOfPrclXfer, SALE_SIZ_NOPRCLXFR);
      if (lTmp > 0)
      {
         sprintf(mySales.NumXfer, "%d", lTmp);
         mySales.GrpSale[0] = 'Y';
      } else
      {
         if (pSale->NumOfPrclXfer[0] > ' ')
            mySales.GrpSale[0] = 'Y';
         else if(pSale->MultiSale_Flg > ' ')
            mySales.GrpSale[0] = 'Y';
      }

      // This only occurs in partial or group sale
      if (pSale->PrimaryApn[0] > ' ')
         strncpy(mySales.GrpAsmt, pSale->PrimaryApn, iApnLen);

      long lPrice = atoin(pSale->SalePrice, SALE_SIZ_SALEPRICE);
      if (lPrice > 0)
         sprintf(mySales.Price, "%d", lPrice);

      double dAmt = atofn(pSale->StampAmt, SALE_SIZ_STAMPAMT);
      if (dAmt > 0.0)
         sprintf(mySales.TaxAmt, "%.2f", dAmt);

      strncpy(mySales.SaleCode, pSale->SaleCode, SALE_SIZ_SALECODE);

      if (pSale->ARCode > ' ')
         mySales.ARCode[0] = pSale->ARCode;
      else if (iType & (TYPE_GRGR_DEF|TYPE_GRGR_DOC))
         mySales.ARCode[0] = 'R';
      else
         mySales.ARCode[0] = 'A';

      // NonSale flag
      mySales.NoneSale_Flg[0] = pSale->NoneSale_Flg;

      // Assume nonsale is transfer
      mySales.XferType[0] = pSale->XferType;

      acTmp[0] = 0;
      iRet = createSaleOutrec(&mySales, acTmp, "");

      fputs(acTmp, fdOut);

      if (!(++lCnt % 1000))
         printf("\rSales records: %u", lCnt);

      // Get next record
      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
   }

   printf("\rSales records: %u\n", lCnt);
   LogMsg("Total Sales records : %u", lCnt);

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return lCnt;
}

/******************************************************************************
 *
 * Type contains bitmap value.  Following value can OR together for multiple types
 *
 * iType 1: generate Sale import (default)
 *       2: generate GrGr import using GRGR_DOC input
 *       4: generate GrGr import using GRGR_DEF input
 *
 * Return: Number of output records
 *
 ******************************************************************************/

int createStdSaleImportRec(void (*fn)(LPSTR, LPSTR, LPSTR), LPSTR pInrec, LPSTR pOutrec)
{
   // Input record
   SCSAL_REC *pSale	  = (SCSAL_REC *)pInrec;
   // Output record
   SALES_REC mySales;

   char  acDocLink[128];
   int	iRet;

   // Skip records without APN
   if (pSale->Apn[0] < '0')
      return -1;

   replChar((char *)pInrec, '|', '1');
   memset(&mySales, 0, sizeof(SALES_REC));
   strncpy(mySales.Apn, pSale->Apn, iApnLen);

   strncpy(mySales.DocNum, pSale->DocNum, SALE_SIZ_DOCNUM);
   myTrim(mySales.DocNum);
   replCharEx(mySales.DocNum, "+*`.{}&%$", ' ', 0, false);
   remChar(mySales.DocNum, ' ');
   strncpy(mySales.DocDate, pSale->DocDate, SALE_SIZ_DOCDATE);
   strncpy(mySales.DocType, pSale->DocType, SALE_SIZ_DOCTYPE);
   myTrim(mySales.DocType);
   strncpy(mySales.DocCode, pSale->DocCode, SALE_SIZ_DOCCODE);
   myTrim(mySales.DocCode);
   strncpy(mySales.SaleCode, pSale->SaleCode, SALE_SIZ_SALECODE);
   myTrim(mySales.SaleCode);

   if (pSale->Seller1[0] > ' ')
   {
      strncpy(mySales.Seller1, pSale->Seller1, SALE_SIZ_SELLER);
      myTrim(mySales.Seller1);
   }
   if (pSale->Seller2[0] > ' ')
   {
      strncpy(mySales.Seller2, pSale->Seller2, SALE_SIZ_SELLER);
      myTrim(mySales.Seller2);
   }

   if (pSale->Name1[0] > ' ')
   {
      strncpy(mySales.Buyer1, pSale->Name1, SALE_SIZ_BUYER);
      myTrim(mySales.Buyer1);
   }
   if (pSale->Name2[0] > ' ')
   {
      strncpy(mySales.Buyer2, pSale->Name2, SALE_SIZ_BUYER);
      myTrim(mySales.Buyer2);
   }

   if (pSale->MailAdr1[0] > ' ')
   {
      strncpy(mySales.M_Addr1, pSale->MailAdr1, SALE_SIZ_M_ADR1);
      myTrim(mySales.M_Addr1);
      strncpy(mySales.M_Addr2, pSale->MailAdr2, SALE_SIZ_M_ADR2);
      myTrim(mySales.M_Addr2);
   }

   long lTmp = atoin(pSale->NumOfPrclXfer, SALE_SIZ_NOPRCLXFR);
   if (lTmp > 0)
   {
      sprintf(mySales.NumXfer, "%d", lTmp);
      mySales.GrpSale[0] = 'Y';
   } else
   {
      if (pSale->MultiSale_Flg == 'Y' || pSale->NumOfPrclXfer[0] == 'M')
         mySales.GrpSale[0] = 'Y';
   }

   long lPrice = atoin(pSale->SalePrice, SALE_SIZ_SALEPRICE);
   if (lPrice > 0)
      sprintf(mySales.Price, "%d", lPrice);

   double dAmt = atofn(pSale->StampAmt, SALE_SIZ_STAMPAMT);
   if (dAmt > 0.0)
      sprintf(mySales.TaxAmt, "%.2f", dAmt);

   strncpy(mySales.SaleCode, pSale->SaleCode, SALE_SIZ_SALECODE);
   mySales.ARCode[0] = pSale->ARCode;

   // Assume nonsale is transfer
   mySales.XferType[0] = pSale->XferType;

   *pOutrec = 0;
   (*fn)((LPSTR)&acDocLink[0], (LPSTR)&mySales.DocNum[0], (LPSTR)&mySales.DocDate[0]); 
   iRet = createSaleOutrec(&mySales, pOutrec, acDocLink);

   return iRet;
}

int createGrgrDocImportRec(void (*fn)(LPSTR, LPSTR, LPSTR), LPSTR pInrec, LPSTR pOutrec)
{
   // Input record
   GRGR_DOC  *pGrgr = (GRGR_DOC *)pInrec;
   // Output record
   SALES_REC mySales;

   char  acDocLink[128];
   int	iRet;

   // Skip records without APN
   if (pGrgr->APN[0] < '0')
      return -1;

   memset(&mySales, 0, sizeof(SALES_REC));
   strncpy(mySales.Apn, pGrgr->APN, iApnLen);

   strncpy(mySales.DocNum, pGrgr->DocNum, SIZ_GD_DOCNUM);
   myTrim(mySales.DocNum);
   strncpy(mySales.DocDate, pGrgr->DocDate, SIZ_GD_DOCDATE);
   strncpy(mySales.DocType, pGrgr->DocType, SIZ_GD_DOCTYPE);
   myTrim(mySales.DocType);
   strncpy(mySales.DocCode, pGrgr->DocTitle, IRSSIZ_DOCCODE);
   myTrim(mySales.DocCode);
   strncpy(mySales.SaleCode, pGrgr->SaleCode, SALE_SIZ_SALECODE);
   myTrim(mySales.SaleCode);

   if (*pGrgr->Grantor[0] > ' ')
   {
      strncpy(mySales.Seller1, pGrgr->Grantor[0], SIZ_GD_NAME);
      myTrim(mySales.Seller1);
      if (strchr(mySales.Seller1, '|'))
         replChar(mySales.Seller1, '|', 'I');
      if (*pGrgr->Grantor[1] > ' ')
      {
         strncpy(mySales.Seller2, pGrgr->Grantor[1], SIZ_GD_NAME);
         myTrim(mySales.Seller2);
         if (strchr(mySales.Seller2, '|'))
            replChar(mySales.Seller2, '|', 'I');
      }
   }

   if (*pGrgr->Grantee[0] > ' ')
   {
      strncpy(mySales.Buyer1, pGrgr->Grantee[0], SIZ_GD_NAME);
      myTrim(mySales.Buyer1);
      if (strchr(mySales.Buyer1, '|'))
         replChar(mySales.Buyer1, '|', 'I');
      if (*pGrgr->Grantee[1] > ' ')
      {
         strncpy(mySales.Buyer2, pGrgr->Grantee[1], SIZ_GD_NAME);
         myTrim(mySales.Buyer2);
         if (strchr(mySales.Buyer2, '|'))
            replChar(mySales.Buyer2, '|', ' ');
      }
   }

   long lPrice = atoin(pGrgr->SalePrice, SIZ_GD_SALE);
   if (lPrice > 0)
      sprintf(mySales.Price, "%d", lPrice);

   double dAmt = atofn(pGrgr->DocTax, SIZ_GD_TAX);
   if (dAmt > 0.0)
      sprintf(mySales.TaxAmt, "%.2f", dAmt);

   strncpy(mySales.SaleCode, pGrgr->SaleCode, SALE_SIZ_SALECODE);
   mySales.ARCode[0] = 'R';

   *pOutrec = 0;
   acDocLink[0] = 0;
   if (fn)
      (*fn)((LPSTR)&acDocLink[0], (LPSTR)&mySales.DocNum[0], (LPSTR)&mySales.DocDate[0]); 
   iRet = createSaleOutrec(&mySales, pOutrec, acDocLink);

   return iRet;
}

int createGrgrDefImportRec(void (*fn)(LPSTR, LPSTR, LPSTR), LPSTR pInrec, LPSTR pOutrec)
{
   // Input record
   GRGR_DEF	 *pGrgr = (GRGR_DEF *)pInrec;
   // Output record
   SALES_REC mySales;

   char  acDocLink[128];
   int	iRet;

   // Skip records without APN
   if (pGrgr->APN[0] < '0')
      return -1;

   memset(&mySales, 0, sizeof(SALES_REC));
   strncpy(mySales.Apn, pGrgr->APN, myCounty.iApnLen);

   strncpy(mySales.DocNum, pGrgr->DocNum, SIZ_GR_DOCNUM);
   myTrim(mySales.DocNum);
   strncpy(mySales.DocDate, pGrgr->DocDate, SIZ_GR_DOCDATE);
   //strncpy(mySales.DocType, pGrgr->DocType, SIZ_GD_DOCTYPE);
   //myTrim(mySales.DocType);
   strncpy(mySales.DocCode, pGrgr->DocTitle, IRSSIZ_DOCCODE);
   myTrim(mySales.DocCode);
   //strncpy(mySales.SaleCode, pGrgr->SaleCode, SALE_SIZ_SALECODE);
   //myTrim(mySales.SaleCode);

   if (*pGrgr->Grantors[0].Name > ' ')
   {
      strncpy(mySales.Seller1, pGrgr->Grantors[0].Name, SIZ_GR_NAME);
      myTrim(mySales.Seller1);
      if (*pGrgr->Grantors[1].Name > ' ')
      {
         strncpy(mySales.Seller2, pGrgr->Grantors[1].Name, SIZ_GR_NAME);
         myTrim(mySales.Seller2);
      }
   }

   if (*pGrgr->Grantees[0].Name > ' ')
   {
      strncpy(mySales.Buyer1, pGrgr->Grantees[0].Name, SIZ_GR_NAME);
      myTrim(mySales.Buyer1);
      if (*pGrgr->Grantees[1].Name > ' ')
      {
         strncpy(mySales.Buyer2, pGrgr->Grantees[1].Name, SIZ_GR_NAME);
         myTrim(mySales.Buyer2);
      }
   }

   long lPrice = atoin(pGrgr->SalePrice, SIZ_GD_SALE);
   if (lPrice > 0)
      sprintf(mySales.Price, "%d", lPrice);

   double dAmt = atofn(pGrgr->Tax, SIZ_GR_TAX);
   if (dAmt > 0.0)
      sprintf(mySales.TaxAmt, "%.2f", dAmt);

   mySales.ARCode[0] = 'R';

   *pOutrec = 0;
   acDocLink[0] = 0;
   if (fn)
      (*fn)((LPSTR)&acDocLink[0], (LPSTR)&mySales.DocNum[0], (LPSTR)&mySales.DocDate[0]); 
   iRet = createSaleOutrec(&mySales, pOutrec, acDocLink);

   return iRet;
}

int createOrgGrgrImportRec(void (*fn)(LPSTR, LPSTR, LPSTR), LPSTR pInrec, LPSTR pOutrec)
{
   // Input record
   ORG_GRGR  *pGrgr = (ORG_GRGR *)pInrec;
   // Output record
   SALES_REC mySales;

   char  acDocLink[128];
   int	iRet;

   // Skip records without APN
   if (pGrgr->ParcelNo[0] < '0')
      return -1;

   memset(&mySales, 0, sizeof(SALES_REC));
   strncpy(mySales.Apn, pGrgr->ParcelNo, iApnLen);

   strncpy(mySales.DocNum, pGrgr->DocNum, SIZ_OGR_DOCNUM);
   myTrim(mySales.DocNum);
   strncpy(mySales.DocDate, pGrgr->DocDate, SIZ_OGR_DOCDATE);
   strncpy(mySales.DocType, pGrgr->DocTitle, SIZ_OGR_DOCTITLE);
   myTrim(mySales.DocType);
   strncpy(mySales.DocCode, pGrgr->DocCode, SIZ_OGR_DOCCODE);
   myTrim(mySales.DocCode);

   if (*pGrgr->Grantor[0] > ' ')
   {
      strncpy(mySales.Seller1, pGrgr->Grantor[0], SIZ_GD_NAME);
      myTrim(mySales.Seller1);
      if (*pGrgr->Grantor[1] > ' ')
      {
         strncpy(mySales.Seller2, pGrgr->Grantor[1], SIZ_GD_NAME);
         myTrim(mySales.Seller2);
      }
   }

   if (*pGrgr->Grantee[0] > ' ')
   {
      strncpy(mySales.Buyer1, pGrgr->Grantee[0], SIZ_GD_NAME);
      myTrim(mySales.Buyer1);
      if (*pGrgr->Grantee[1] > ' ')
      {
         strncpy(mySales.Buyer2, pGrgr->Grantee[1], SIZ_GD_NAME);
         myTrim(mySales.Buyer2);
      }
   }

   long lPrice = atoin(pGrgr->SalePrice, SIZ_GD_SALE);
   if (lPrice > 0)
      sprintf(mySales.Price, "%d", lPrice);

   double dAmt = atofn(pGrgr->DocTax, SIZ_GD_TAX);
   if (dAmt > 0.0)
      sprintf(mySales.TaxAmt, "%.2f", dAmt);

   mySales.ARCode[0] = 'R';

   *pOutrec = 0;
   acDocLink[0] = 0;
   if (fn)
      (*fn)((LPSTR)&acDocLink[0], (LPSTR)&mySales.DocNum[0], (LPSTR)&mySales.DocDate[0]); 
   iRet = createSaleOutrec(&mySales, pOutrec, acDocLink);

   return iRet;
}

int createSaleImport(void (*fn)(LPSTR, LPSTR, LPSTR), LPCSTR CountyCode, LPCSTR pInfile, LPCSTR pOutfile, int iType, bool bInclHdr)
{
   char     *pTmp, acInfile[_MAX_PATH], acOutfile[_MAX_PATH], acRec[MAX_RECSIZE], acTmp[MAX_RECSIZE];
   BOOL     bEof;
   int      iRet, lCnt=0;
   FILE     *fdIn, *fdOut;

   if (iType & TYPE_SCSAL_REC)
   {
      LogMsg("Create Import sale file ...");
      if (pInfile && *pInfile > ' ')
         strcpy(acInfile, pInfile);
      else
      {
         GetIniString("Data", "SaleOut", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acInfile, acTmp, CountyCode, CountyCode, "sls");
      }
   } else if ((iType & TYPE_GRGR_DOC) || (iType & TYPE_GRGR_DEF) || (iType & TYPE_GRGR_ORG))
   {
      LogMsg("Create Import GrGr file ...");
      if (pInfile && *pInfile > ' ')
         strcpy(acInfile, pInfile);
      else
      {
         GetIniString("Data", "GrGrOut", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acInfile, acTmp, CountyCode, CountyCode, "sls");
      }
   } else
   {
      LogMsg("??? Incorrect file type: %d", iType);
      return -999;
   }

   strcpy(acOutfile, pOutfile);

   // Open Sales file
   LogMsg("Open Sales/GrGr file %s", acInfile);
   fdIn = fopen(acInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Sales/GrGr file: %s\n", acInfile);
      return -1;
   }

   if (!(fdOut = fopen(acOutfile, "w")))
   {
      LogMsg("***** Error creating output file %s", acOutfile);
      return -2;
   }

   // Write header
   if (bInclHdr)
   {
      if (iType & TYPE_SCSAL_REC)
         fputs(createSaleHeader(acTmp, '|'), fdOut);
      else
         fputs(createGrgrHeader(acTmp, '|'), fdOut);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   bEof = (pTmp ? false:true);

   // Merge loop
   while (pTmp && !feof(fdIn))
   {
#ifdef _DEBUG
      //if (!memcmp(&acRec[0], "2020-0260064", 12))
      //   iRet = 0;
#endif

      if (iType & TYPE_SCSAL_REC)
         iRet = createStdSaleImportRec(fn, acRec, acTmp);
      else if (iType & TYPE_GRGR_DOC)
         iRet = createGrgrDocImportRec(fn, acRec, acTmp);
      else if (iType & TYPE_GRGR_DEF)
         iRet = createGrgrDefImportRec(fn, acRec, acTmp);
      else if (iType & TYPE_GRGR_ORG)
         iRet = createOrgGrgrImportRec(fn, acRec, acTmp);      

      if (!iRet)
         fputs(acTmp, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u ...", lCnt);

      // Get next record
      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
   }

   printf("\rSales records: %u\n", lCnt);
   LogMsg("Total Sales records : %u", lCnt);

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return lCnt;
}

/******************************************************************************
 *
 * Create sale import for web scraping data (SBD)
 *
 * iType 1: Assessor data
 *       2: Recorder data
 *
 * Return: Number of output records
 *
 ******************************************************************************/

int createWebSaleImport(LPCSTR CountyCode, LPCSTR pInfile, LPCSTR pOutfile, int iType, bool bInclHdr, IDX_TBL5 *pDocTbl)
{
   char     *pTmp, acRec[MAX_RECSIZE];
   BOOL     bEof;
   int      iRet, lCnt=0;
   FILE     *fdIn, *fdOut;

   // Input record
   GRGR_DOC   *pSale;
   // Output record
   SALES_REC  mySales;

   if (iType & TYPE_GRGR_DOC)
   {
      LogMsg("Create Import GrGr file ...");
      //if (pInfile && *pInfile > ' ')
      //   strcpy(acInfile, pInfile);
      //else
      //   GetIniString(CountyCode, "XSalFile", "", acInfile, _MAX_PATH, acIniFile);    
   } else
   {
      LogMsg("??? Incorrect file type: %d", iType);
      return -999;
   }

   // Open Sales file
   LogMsg("Open Sales/GrGr file %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Sales/GrGr file: %s\n", pInfile);
      return -1;
   }

   if (!(fdOut = fopen(pOutfile, "w")))
   {
      LogMsg("***** Error creating output file %s", pOutfile);
      return -2;
   }

   // Write header
   if (bInclHdr)
      fputs(createSaleHeader(acRec, '|'), fdOut);

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   bEof = (pTmp ? false:true);
   pSale = (GRGR_DOC *)&acRec[0];

   // Merge loop
   while (pTmp && !feof(fdIn))
   {
      memset(&mySales, 0, sizeof(SALES_REC));

#ifdef _DEBUG
      //if (!memcmp(mySales.Apn, "077273017", 9))
      //   iRet = 0;
#endif

      // Translate to DocType if needed
      if (pDocTbl)
      {
         strncpy(mySales.DocCode, pSale->DocTitle, IRSSIZ_DOCCODE);
         myTrim(mySales.DocCode);
         iRet = findDocType(mySales.DocCode, pDocTbl);
         if (iRet >= 0 && pDocTbl[iRet].flag == 'N')
         {
            memcpy(mySales.DocType, pDocTbl[iRet].pCode, pDocTbl[iRet].iCodeLen);
         } else
         {
            // Skip non-sale records
            pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
            lCnt++;
            continue;
         }
      } else
      {
         strncpy(mySales.DocType, pSale->DocTitle, IRSSIZ_DOCCODE);
         myTrim(mySales.DocType);
      }

      strncpy(mySales.Apn, pSale->APN, iApnLen);
      strncpy(mySales.DocNum, pSale->DocNum, SALE_SIZ_DOCNUM);
      myTrim(mySales.DocNum);
      strncpy(mySales.DocDate, pSale->DocDate, SALE_SIZ_DOCDATE);
         
      if (pSale->Grantor[0][0] > ' ')
      {
         strncpy(mySales.Seller1, pSale->Grantor[0], SIZ_GD_NAME);
         myTrim(mySales.Seller1);
      }
      if (pSale->Grantor[1][0] > ' ')
      {
         strncpy(mySales.Seller2, pSale->Grantor[1], SIZ_GD_NAME);
         myTrim(mySales.Seller2);
      }

      if (pSale->Grantee[0][0] > ' ')
      {
         strncpy(mySales.Buyer1, pSale->Grantee[0], SIZ_GD_NAME);
         myTrim(mySales.Buyer1);
      }
      if (pSale->Grantee[1][0] > ' ')
      {
         strncpy(mySales.Buyer2, pSale->Grantee[1], SIZ_GD_NAME);
         myTrim(mySales.Buyer2);
      }

      long lPrice = atoin(pSale->SalePrice, SIZ_GD_SALE);
      if (lPrice > 0)
         sprintf(mySales.Price, "%d", lPrice);

      double dAmt = atofn(pSale->DocTax, SIZ_GD_TAX);
      if (dAmt > 0.0)
         sprintf(mySales.TaxAmt, "%.2f", dAmt);

      if (iType & 2)
         mySales.ARCode[0] = 'R';

      acRec[0] = 0;
      iRet = createSaleOutrec(&mySales, acRec, "");

      fputs(acRec, fdOut);

      if (!(++lCnt % 1000))
         printf("\rSales records: %u", lCnt);

      // Get next record
      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
   }

   printf("\rSales records: %u\n", lCnt);
   LogMsg("Total Sales records : %u", lCnt);

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return lCnt;
}

/***************************** FixSalePrice **********************************
 *
 * CRES counties used to carry stamp amt from one transaction to the next.  Here
 * we try to check for same APN & sale price with different RECDATE, remove sale 
 * price from newer transaction.  Used in CRES counties.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int FixSalePrice(char *pInfile, bool bResort)
{
   char     acInbuf[2048], acSortFile[_MAX_PATH], acOutFile[_MAX_PATH], *pRec;
   long     lCnt=0, lClean=0, iTmp;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];
   SCSAL_REC  sLastSale;

   if (_access(pInfile, 0))
   {
      LogMsg("***** FixSalePrice(): Missing input file: %s", pInfile);
      return -1;
   }

   LogMsg("Fix Sale Price for %s", pInfile);

   // Resort sale file - APN ASC, DATE ASC, DOCNUM DESC
   strcpy(acSortFile, pInfile);
   if (bResort)
   {
      pRec = strrchr(acSortFile, '.');
      strcpy(pRec, ".SRT");

      sprintf(acInbuf, "S(1,%d,C,A,27,8,C,A,57,10,C,D,15,12,C,A) F(TXT) DUPO(B8000,1,34) ", iApnLen);
      iTmp = sortFile(pInfile, acSortFile, acInbuf);
      if (!iTmp)
         return -10;
   }

   // Open input file
   LogMsg("Open input sale file %s", acSortFile);
   fdSale = fopen(acSortFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", acSortFile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0025 024", 8))
      //   iTmp = 0;
#endif
      if (!memcmp(sLastSale.Apn, acInbuf, iApnLen) )
      {
         iTmp = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
         if (iTmp > 0)
         {
            if (!memcmp(sLastSale.SalePrice, pInRec->SalePrice, SALE_SIZ_SALEPRICE))
            {
               memset(pInRec->SalePrice, ' ', SALE_SIZ_SALEPRICE);
               memset(pInRec->StampAmt, ' ', SALE_SIZ_STAMPAMT);
               lClean++;
            } else
               strcpy((char *)&sLastSale, acInbuf);
         }
      } else
      {
         strcpy((char *)&sLastSale, acInbuf);
      }

      fputs(acInbuf, fdOut);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   // Save input file
   strcpy(acInbuf, pInfile);
   pRec = strrchr(acInbuf, '.');
   strcpy(pRec, ".fix");
   if (!_access(acInbuf, 0))
      DeleteFile(acInbuf);

   // Save input file
   iTmp = rename(pInfile, acInbuf);

   // Rename output file
   iTmp = rename(acOutFile, pInfile);

   printf("\nTotal input records:   %u\n", lCnt);
   LogMsg("Total input records:     %u", lCnt);
   LogMsg("      cleaned records:   %u", lClean);

   return iTmp;
}

/********************************* ApplyCumSaleDN *****************************
 *
 * Apply cumulative sales to R01 file.  If transfer date is the same as DocDate,
 * copy TransferDoc to Sale1_DocNum.
 * Return 0 if successful
 *
 ******************************************************************************/

int ApplyCumSaleDN(int iSkip, char *pCSaleFile, bool bResort, int iType, int iClearSaleFlg)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acCumSaleFile[_MAX_PATH];
   char		*pRec;

   HANDLE   fhIn, fhOut;

   int      iRet,iTmp, iRollUpd;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bRename, bEof;
   long     lCnt=0;

   if (pCSaleFile && !_access(pCSaleFile, 0))
      strcpy(acCumSaleFile, pCSaleFile);
   else
      strcpy(acCumSaleFile, acCSalFile);

   LogMsg0("ApplyCumSaleDN(): Apply history sale using %s", acCumSaleFile);

   // Prepare input/output files
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "T01");
   bRename = true;

   // Check input file for processing
   if (_access(acRawFile, 0))
   {
      strcpy(acOutFile, acRawFile);
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (_access(acRawFile, 0))
      {
         LogMsg("Missing input file %s.  Please recheck!", acOutFile);
         return -1;
      }
      bRename = false;
   }

   LogMsg("\nApply sale history %s to %s.", acCumSaleFile, acRawFile);

   // Sort cumulative sale file
   if (bResort)
   {
      //sprintf(acOutFile, acGrGrTmpl, myCounty.acCntyCode, "srt");      // accummulated sale file
      strcpy(acOutFile, acCumSaleFile);
      pRec = strrchr(acOutFile, '.');
      if (pRec)
         strcpy(pRec, ".SRT");
      else
         strcat(acOutFile, ".SRT");

      // Sort on APN asc, DocDate asc, DocNum asc
      sprintf(acRec, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) F(TXT) DUPO(B8000,1,34) ", SALE_SIZ_APN, SALE_SIZ_DOCNUM);
      iTmp = sortFile(acCumSaleFile, acOutFile, acRec);
      if (!iTmp)
         return -2;

      // Rename SRT file to SLS file
      iRet = remove(acCumSaleFile);
      if (iRet)
         return -3;
      iRet = rename(acOutFile, acCumSaleFile); 
      if (iRet)
         return -4;
   } 

   // Open cumulative file
   LogMsg("Open cumulative sale file %s", acCumSaleFile);
   fdSale = fopen(acCumSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening cumulative sale file: %s\n", acCumSaleFile);
      return -5;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -6;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -7;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   for (iRet = 0; iRet < iSkip; iRet++)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iRet=iNoMatch=iRollUpd=0;

   // Merge loop
   iRollUpd = 0;
   bEof = false;
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for read error
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -8;
         break;
      }

      // Check for EOF
      if (!nBytesRead)
      {
         iRet = 0;
         break;         // EOF
      }

      // Clear old sales
      if (iClearSaleFlg == 1)
         ClearOldSale(acBuf);

      // Apply Sale
      iRet = ApplyHistSaleDN(acBuf, iClearSaleFlg);
      if (!iRet)
         iRollUpd++;
      else if (iRet == -1)
      {  // EOF
         iRet = 0;
         bEof = true;
      } else
         iNoMatch++;

      iTmp = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
      if (iTmp > lLastRecDate && iTmp < lToday)
         lLastRecDate = iTmp;

      // Write to output file
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error output record: %d\n", GetLastError());
         iRet = -9;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }

   // Close files
   if (fdSale)
      fclose(fdSale);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // If everything is OK, rename output file
   if (!iRet && bRename)
   {
      // Rename files
      LogMsg("Remove old file %s", acRawFile);
      iRet = remove(acRawFile);
      if (iRet)
         LogMsg("***** Unable to delete %s (%d)", acRawFile, errno);
      else
      {
         LogMsg("Rename %s to %s", acOutFile, acRawFile);
         iRet = rename(acOutFile, acRawFile);
         if (iRet)
            LogMsg("***** Error renaming %s to %s (%d)", acOutFile, acRawFile, errno);
      }
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total sale records applied: %u", iRollUpd);
   LogMsg("Total sale not matched:     %u", iNoMatch);
   LogMsg("Last sale recording date:   %u", lLastRecDate);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return iRet;
}

/******************************* FixCumSale **********************************
 *
 * Fix specific case in sale file.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int FixCumSale(char *pInfile, int iType, bool bRemove)
{
   char     acInbuf[2048], acOutFile[_MAX_PATH], *pRec, sTmp1[32], sTmp2[32];
   long     lCnt=0, lClean=0, iTmp, lVal;
   double   dTmp;
   bool     bGoodRec;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** FixCumSale(): Missing input file: %s", pInfile);
      return -1;
   }

   LogMsg("Fix Sale Price for %s", pInfile);

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0282630300", 9))
      //   iTmp = 0;
#endif
      //replCharEx(acInbuf, "`{}~[]", ' ', 0, false);

      bGoodRec = true;
      switch (iType)
      {
         // Remove sale record with internal DocNum
         case SALE_FLD_CLEANUP:
            if (pInRec->DocNum[4] == 'I')
               bGoodRec = false;
            else if (memcmp(pInRec->DocNum, pInRec->DocDate, 4))
               bGoodRec = false;
            break;

         case SALE_FLD_SALECODE:
            if (pInRec->SaleCode[0] == 'M')
            {
               pInRec->SaleCode[0] = ' ';
               pInRec->MultiSale_Flg = 'Y';
               lClean++;
            }
            break;

         case SALE_FLD_NOPRCLXFR:
            if (pInRec->NumOfPrclXfer[0] == 'M')
            {
               pInRec->NumOfPrclXfer[0] = ' ';
               pInRec->MultiSale_Flg = 'Y';
               lClean++;
            } else if (pInRec->NumOfPrclXfer[0] > '1' && pInRec->NumOfPrclXfer[0] <= '9')
               pInRec->MultiSale_Flg = 'Y';
            break;

         case SALE_FLD_SALEPRICE:
            iTmp = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
            if (iTmp >= 0)
            {
               if (bRemove)
               {
                  memset(pInRec->SalePrice, ' ', SALE_SIZ_SALEPRICE);
                  memset(pInRec->StampAmt, ' ', SALE_SIZ_STAMPAMT);
                  lClean++;
               } else 
               {
                  // to be done ...
               }
            }
            break;

         case SALE_FLD_DOCNUM:   // Make sure DocNum are all digits
            if (pInRec->DocNum[11] == ' ')
               iTmp = replNonNum(&pInRec->DocNum[5], ' ', 6);
            else
               iTmp = replNonNum(&pInRec->DocNum[5], ' ', 7);
            if (iTmp > 0)
               lClean++;
            break;

         case SALE_FLD_DOCNUM+CNTY_STA:      // Reformat DocNum for STA
            if (pInRec->DocNum[4] == 'R')
            {
               memcpy(sTmp1, &pInRec->DocNum[5], 7);
               sTmp1[7] = 0;
               replCharEx(sTmp1, "-+*`./{}&%$", ' ', 0, false);
               remChar(sTmp1, ' ');
               if (isNumber(sTmp1))
               {
                  lVal = atol(sTmp1);
                  sprintf(sTmp2, "%.6d ", lVal);
                  memcpy(&pInRec->DocNum[5], sTmp2, 7);
                  lClean++;
               }
            }
            break;

         case SALE_FLD_DOCNUM+CNTY_RIV:      // Reformat DocNum for RIV
            if (pInRec->DocNum[6] == ' ')
            {
               lVal = atoin(&pInRec->DocNum[0], 6);
               sprintf(sTmp1, "%.7d ", lVal);
               memcpy(&pInRec->DocNum[0], sTmp1, 7);
               lClean++;
            }
            break;

         case SALE_FLD_DOCNUM+CNTY_FRE:      // Reformat DocNum for FRE
            // Format DOCNUM to 1234567
            lVal = atoin(&pInRec->DocNum[0], 7);
            iTmp = atoin(pInRec->DocDate, 4);
            if (lVal > 0 && iTmp > 1980)
            {
               iTmp = sprintf(sTmp1, "%.7d", lVal);
               memcpy(&pInRec->DocNum[0], sTmp1, iTmp);
               lClean++;
            }
            break;

         case SALE_FLD_DOCNUM+CNTY_MEN:      // Remove bad DocNum
            if (!isNumber(pInRec->DocNum, 5))
               bGoodRec = false;
            break;

         case SALE_FLD_DOCNUM+CNTY_MNO:      // Reformat DocNum for MNO
            if (isdigit(pInRec->DocNum[4]))
            {
               try
               {
                  lVal = atoin(&pInRec->DocNum[4], 7);
                  iTmp = sprintf(sTmp1, "R%.7d", lVal);
                  memcpy(&pInRec->DocNum[4], sTmp1, iTmp);
                  lClean++;
               } catch (...)
               {
                  LogMsg("*** Bad DocNum: %.12s", pInRec->DocNum);
               }
            }
            break;

         case SALE_FLD_DOCNUM+CNTY_MOD:      // Reformat DocNum for MOD
            memcpy(sTmp1, pInRec->DocNum, SALE_SIZ_DOCNUM);
            sTmp1[SALE_SIZ_DOCNUM] = 0;
            if (pInRec->DocNum[4] != 'R' && !strchr(sTmp1, '-'))
            {
               if  (sTmp1[7] == ' ')
               {
                  iTmp = sprintf(sTmp2, "%.4sR%.7s", pInRec->DocDate, sTmp1);
                  memcpy(pInRec->DocNum, sTmp2, iTmp);
                  lClean++;
               } else
               {
                  LogMsg("Unknown DocNum format: %s [%.12s]", sTmp1, pInRec->Apn);
               }
            }

            // Fix DocType
            lVal = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
            if (pInRec->DocType[0] == ' ' && lVal > 5000)
               pInRec->DocType[0] = '1';

            // Fix DocTax
            dTmp = atofn(pInRec->StampAmt, SALE_SIZ_STAMPAMT);
            if (dTmp > 0.0)
            {
               iTmp = sprintf(sTmp1, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
               memcpy(pInRec->StampAmt, sTmp1, iTmp);
            }
            break;

         case SALE_FLD_DOCNUM+CNTY_SFX:      // Reformat DocNum for SFX
            // Format DOCNUM to A999999
            if (pInRec->DocNum[0] >= 'A')
            {
               if (isdigit(pInRec->DocNum[1]) )
               {
                  iTmp = sprintf(sTmp1, "%.3s  ", &pInRec->DocNum[6]);
                  memcpy(&pInRec->DocNum[4], sTmp1, iTmp);
               } else
               {
                  iTmp = sprintf(sTmp1, "%.4s%.4s  ", &pInRec->DocNum[0], &pInRec->DocNum[5]);
                  memcpy(&pInRec->DocNum[0], sTmp1, iTmp);
               }
               lClean++;
            }
            break;

         case SALE_FLD_DOCNUM+CNTY_SIE:      // Reformat DocNum for SIE
            if (pInRec->DocNum[4] == '-' && pInRec->DocNum[7] == ' ')
            {
               sprintf(sTmp1, "%.4s%.2s  ", &pInRec->DocNum[0], &pInRec->DocNum[5]);
               memcpy(&pInRec->DocNum[0], sTmp1, 7);
               lClean++;
            }
            break;

         case SALE_FLD_DOCNUM+CNTY_SON:      // Remove bad DocNum for SON
            // 02/10/2023
            if (pInRec->DocNum[4] == 'R')
            {
               if (isdigit(pInRec->DocNum[5]))
               {
                  lVal = atoin(&pInRec->DocNum[5], 6);
                  iTmp = sprintf(sTmp1, "%.5s%.6d ", pInRec->DocNum, lVal);

                  memcpy(&pInRec->DocNum[0], sTmp1, iTmp);
                  lClean++;
               }
            } else
               bGoodRec = false;
            break;

         case SALE_FLD_DOCNUM+CNTY_TRI:      // Reformat DocNum for TRI
            // 02/19/2021
            if (pInRec->DocNum[4] == 'R')
            {
               if (pInRec->DocNum[10] > ' ')
               {
                  lVal = atoin(&pInRec->DocNum[5], 6);
                  iTmp = sprintf(sTmp1, "%.5s%.5d ", pInRec->DocNum, lVal);

                  memcpy(&pInRec->DocNum[0], sTmp1, iTmp);
                  lClean++;
               }
            } else
               bGoodRec = false;

            // Format DOCNUM to yyyy12345
            //if (pInRec->DocNum[0] >= '0')
            //{
            //   iTmp = atoin(pInRec->DocNum, 4);
            //   if (pInRec->DocNum[0] == '9')
            //   {
            //      lVal = atoin(&pInRec->DocNum[2], 6);
            //      iTmp = sprintf(sTmp1, "19%.2s%.5d", pInRec->DocNum, lVal);
            //   } else if (iTmp > 1996 && iTmp <= lToyear)
            //   {
            //      lVal = atoin(&pInRec->DocNum[4], 6);
            //      iTmp = sprintf(sTmp1, "%.4s%.5d", pInRec->DocNum, lVal);
            //   } else if (!memcmp(pInRec->DocNum, "09", 2))
            //   {
            //      lVal = atoin(&pInRec->DocNum[3], 6);
            //      iTmp = sprintf(sTmp1, "19%.2s%.5d", &pInRec->DocNum[1], lVal);
            //   } else if (iTmp > 1990 && iTmp <= 1996)
            //   {
            //      lVal = atoin(&pInRec->DocNum[4], 6);
            //      iTmp = sprintf(sTmp1, "%.4s%.5d", pInRec->DocNum, lVal);
            //   } else
            //      iTmp = 0;

            //   memcpy(&pInRec->DocNum[0], sTmp1, iTmp);
            //   lClean++;
            //}
            break;

         case SALE_FLD_DOCTYPE: // for SJX
            iTmp = atoin(pInRec->DocType, SIZ_SALE1_DOCTYPE);
            lClean++;
            if (iTmp == 70)
            {  // AM -> Administrator's Deed
               memcpy(pInRec->DocType, "5  ", 3);
            } if (iTmp == 73)
            {  // GU -> Deed Of Guardian
               memcpy(pInRec->DocType, "14 ", 3);
            } if (iTmp == 74)
            {  // MS -> Tax Deed or Misc (Contract, Deed, ...)
               iTmp = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
               if (iTmp < 1000)
                  pInRec->NoneSale_Flg = 'Y';
               else
                  memcpy(pInRec->DocType, "67 ", 3);
            } if (iTmp == 14 || iTmp == 17)
            {  // Deed of Guardian, Individual GD -> Deed
               memcpy(pInRec->DocType, "13 ", 3);
            } if (iTmp == 84 || iTmp == 83 )
            {  // Correction -> Agreement of Sale
               memcpy(pInRec->DocType, "8  ", 3);
            } if (iTmp == 9 || iTmp == 93 || iTmp == 94)
            {  // CD-Correction Deed -> Deed (Conservator)
               memcpy(pInRec->DocType, "13 ", 3);
            } if (iTmp == 36)
            {  // CT-Contract For Deed -> Decree Of Distribution
               memcpy(pInRec->DocType, "80 ", 3);
            } if (iTmp == 37)
            {  // CV-Conveyance Deed -> Agreement of Sale (Cal Vet)
               memcpy(pInRec->DocType, "8  ", 3);
            } if (iTmp == 0)
            {  // Some type of Deed                
               iTmp = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
               if (iTmp > 1000)
               {
                  memcpy(pInRec->DocType, "13 ", 3);
                  LogMsg("APN=%.8s, Price=%.10s", pInRec->Apn, pInRec->SalePrice);
               }
            } if (iTmp == 43 || iTmp == 44)
            {
               memcpy(pInRec->DocType, "   ", 3);
               pInRec->NoneSale_Flg = 'Y';
            } else
               lClean--;

            break;

         case SALE_FLD_NONSALE: // for SFX
            if (bRemove)
               pInRec->NoneSale_Flg = ' ';
            break;
      }

      if (bGoodRec)
         fputs(acInbuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if (lClean > 0)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".org");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Rename output file
      iTmp = rename(acOutFile, pInfile);
   } else
      LogMsg("Good sale file, no record drop!");

   LogMsgD("\nTotal input records:     %u\n", lCnt);
   LogMsg("    records cleaned:     %u", lClean);

   return iTmp;
}

/************************** CombineSaleAndTransfer ****************************
 *
 * Combine transfer with cum sale then remove duplicate.
 * Return number of output records. <0 if error.
 *
 ******************************************************************************/

int CombineSaleAndTransfer(char *pCnty)
{
   char  sXferFile[_MAX_PATH], sTmpFile[_MAX_PATH], sInbuf[2048], sOutbuf[2048], *pRec;
   char  *apFlds[10];

   int   iRet, lCnt=0;
   FILE  *fdXfer;
   SCSAL_REC *pXfer  = (SCSAL_REC *)&sOutbuf[0];

   LogMsg("Combine transfer data with history sale file");

   // Prepare input files
   iRet = GetIniString("Data", "XferFile", "", sInbuf, _MAX_PATH, acIniFile);
   if (!iRet)
   {
      LogMsg("***** Transfer file wasn't defined.  Please check LOADMB.INI for XferFile= in [Data] section.");
      return -1;
   }

   sprintf(sXferFile, sInbuf, pCnty, pCnty, "Txt");
   if (_access(sXferFile, 0))
   {
      LogMsg("***** Missing input file: %s", sXferFile);
      return -2;
   }

   sprintf(acCSalFile, acESalTmpl, pCnty, pCnty, "Sls");
   if (_access(acCSalFile, 0))
   {
      LogMsg("***** Missing history sale file: %s", acCSalFile);
      return -3;
   }

   // Open xfer file
   LogMsg("Open xfer file %s", sXferFile);
   fdXfer = fopen(sXferFile, "r");
   if (fdXfer == NULL)
   {
      LogMsg("***** Error opening xfer file: %s\n", sXferFile);
      return -2;
   }

   sprintf(sTmpFile, acESalTmpl, pCnty, pCnty, "Tmp");
   LogMsg("Create new sale file %s", sTmpFile);
   fdSale = fopen(sTmpFile, "w");
   if (fdSale == NULL)
   {
      LogMsg("***** Error creating new sale file: %s\n", sTmpFile);
      return -3;
   }
      
   while (!feof(fdXfer))
   {
      if (!(pRec = fgets(sInbuf, 1024, fdXfer)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0282630300", 9))
      //   iTmp = 0;
#endif
      iRet = ParseString(pRec, '|', 10, apFlds);
      if (iRet >= 3 && *apFlds[1] > '0' && *apFlds[2] > ' ')
      {
         memset(sOutbuf, ' ', sizeof(SCSAL_REC));
         memcpy(pXfer->Apn, apFlds[0], strlen(apFlds[0]));
         memcpy(pXfer->DocDate, apFlds[1], strlen(apFlds[1]));
         memcpy(pXfer->DocNum, apFlds[2], strlen(apFlds[2]));
         pXfer->NoneSale_Flg = 'Y';
         pXfer->XferType = 'T';
         pXfer->CRLF[0] = 10;
         pXfer->CRLF[1] = 0;
         fputs(sOutbuf, fdSale);
      }
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdXfer)
      fclose(fdXfer);

   // Sort output
   sprintf(sXferFile, acESalTmpl, pCnty, pCnty, "Srt");
   sprintf(sInbuf, "%s+%s", acCSalFile, sTmpFile); 
   lCnt = sortFile(sInbuf, sXferFile, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D,129,1,C,A) F(TXT) DUPO(B8000,1,34) ");
   if (lCnt > 0)
   {                
      // Backup current file then rename output file to current
      sprintf(sTmpFile, acESalTmpl, pCnty, pCnty, "bak");
      if (!_access(sTmpFile, 0))
         DeleteFile(sTmpFile);
      LogMsg("Save original sale file to %s", sTmpFile);
      iRet = rename(acCSalFile, sTmpFile);
      if (!iRet)
      {
         // Rename srt to SLS file
         iRet = rename(sXferFile, acCSalFile);
         if (iRet)
            LogMsg("***** Error renaming %s to %s", sXferFile, acCSalFile);
      } else
         LogMsg("***** Error renaming %s to %s", acCSalFile, sTmpFile);
   }

   return lCnt;
}

/******************************* FixDocType **********************************
 *
 * Fix DocType in cumsale file.
 * If bRemove=true, drop record without DocNum
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int FixDocType(char *pInfile, IDX_TBL5 *pDocTbl, 
               bool bRemove, /* remove rec w/o DocNum */
               bool bFormatStamp)
{
   char   acTmp[32], acInbuf[2048], acOutFile[_MAX_PATH], *pRec;
   long   lCnt=0, lDrop=0, iTmp;
   double dTmp;
   FILE   *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** FixDocType(): Missing input file: %s", pInfile);
      return -1;
   }

   LogMsg("Fix DocType for %s", pInfile);

   // Open input file
   LogMsg("Open cum sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0282630300", 9))
      //   iTmp = 0;
#endif
      iTmp = findDocType(pInRec->DocCode, pDocTbl);
      if (iTmp >= 0)
      {
         memcpy(pInRec->DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
         pInRec->NoneSale_Flg = pDocTbl[iTmp].flag;
      }

      // If no DocNum and bRemove=true, drop this record
      if (pInRec->DocNum[0] > ' ' || !bRemove)
      {
         // Reformat DocTax
         if (bFormatStamp)
         {
            dTmp = atofn(pInRec->StampAmt, SALE_SIZ_STAMPAMT);
            if (dTmp > 0.0)
            {
               iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
               memcpy(pInRec->StampAmt, acTmp, iTmp);
            } else
               memset(pInRec->StampAmt, ' ', SALE_SIZ_STAMPAMT);
         }

         fputs(acInbuf, fdOut);
      } else
         lDrop++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   // Save input file
   strcpy(acInbuf, pInfile);
   pRec = strrchr(acInbuf, '.');
   strcpy(pRec, ".org");
   if (!_access(acInbuf, 0))
      DeleteFile(acInbuf);

   // Save input file
   LogMsg("Rename input file %s --> %s", pInfile, acInbuf);
   iTmp = rename(pInfile, acInbuf);

   // Rename output file
   LogMsg("Rename output file %s --> %s", acOutFile, pInfile);
   iTmp = rename(acOutFile, pInfile);

   LogMsg("Total input records:     %u", lCnt);
   LogMsg("    records dropped:     %u", lDrop);

   return iTmp;
}

/******************************** MergePrice *********************************
 *
 * Merge Sale price.  Assume fd2 contains updated sale record.
 *
 *****************************************************************************/

static int MergePrice(char *pOutbuf, FILE *fdOut, FILE *fd2)
{
   static   char acRec[2048], *pRec=NULL;
   int      iTmp, iPrice1, iPrice2, iRet=0;

   SCSAL_REC *pSale1  = (SCSAL_REC *)pOutbuf;
   SCSAL_REC *pSale2  = (SCSAL_REC *)&acRec[0];

   // Get rec
   if (!pRec)
      pRec = fgets(acRec, 1024, fd2);

   do 
   {
      if (!pRec)
         return 99;      // EOF 

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "0010421400", 10))
      //   iTmp = 0;
#endif
      iTmp = memcmp(pOutbuf, pRec, 34);
      if (iTmp)
      {
         iTmp = memcmp(pOutbuf, pRec, 33);
         if (iTmp > 0)
         {
            if (bDebug)
               LogMsg("Skipping: %.34s", pRec);

            // Output unmatched record
            if (pSale2->DocNum[0] > ' ' && pSale2->DocDate[0] > ' ')
               fputs(acRec, fdOut);

            pRec = fgets(acRec, 1024, fd2);
         }
         if (!iTmp)
         {
            LogMsg("+++Wrong date: %.34s <==> %.34s", pRec, pOutbuf);
            if (!isValidYMD(pSale1->DocDate, true) && isValidYMD(pSale2->DocDate, true))
               memcpy(pSale1->DocDate, pSale2->DocDate, SALE_SIZ_DOCDATE);
         }
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iTmp;

   // Now both records have the same DocNum & DocDate, we need to check sale price
   iPrice1 = atoin(pSale1->SalePrice, SALE_SIZ_SALEPRICE);
   iPrice2 = atoin(pSale2->SalePrice, SALE_SIZ_SALEPRICE);
   if (iPrice1 != iPrice2 && iPrice2 > 0)
   {
      // Use new sale price
      memcpy(pSale1->SalePrice, pSale2->SalePrice, SALE_SIZ_SALEPRICE); 
      if (pSale2->Name1[0] > '0')
         memcpy(pSale1->Name1, pSale2->Name1, SALE_SIZ_BUYER); 
      iRet = 2;
   } 

   if (pSale2->SaleCode[0] > ' ')
   {
      iRet |= 4;
      pSale1->SaleCode[0] = pSale2->SaleCode[0];
   }
   if (pSale2->MultiSale_Flg > ' ')
   {
      iRet |= 8;
      pSale1->MultiSale_Flg = pSale2->MultiSale_Flg;
   }

   // Get next record
   pRec = fgets(acRec, 1024, fd2);

   return iRet;
}

/***************************** MergeSaleFiles **********************************
 *
 * Merge sale price from file2 into file one if sale price on file1 is 0 (used in TUO & INY)
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int MergeSaleFiles(char *pFile1, char *pFile2, char *pOutfile, bool bResort)
{
   char     acInbuf[2048], acSortFile[_MAX_PATH], acOutFile[_MAX_PATH], *pRec;
   char     acFile1[_MAX_PATH], acFile2[_MAX_PATH];
   long     lCnt=0, lMatch=0, lUpdPrice=0, lUpdMFlag=0, lUpdCode=0, iTmp;
   FILE     *fdOut, *fd1, *fd2;

   LogMsg("Merge Sale Price to cumsale %s", pFile1);

   if (_access(pFile1, 0))
   {
      LogMsg("***** MergeSaleFiles(): Missing input file: %s", pFile1);
      return -1;
   }
   if (_access(pFile2, 0))
   {
      LogMsg("***** MergeSaleFiles(): Missing input file: %s", pFile2);
      return -1;
   }

   // Resort sale file - APN ASC, DATE ASC, DOCNUM DESC
   strcpy(acFile1, pFile1);
   strcpy(acFile2, pFile2);
   if (bResort)
   {
      pRec = strrchr(acFile1, '.');
      strcpy(pRec, ".SR1");

      sprintf(acInbuf, "S(1,34,C,A,57,10,C,D) F(TXT) DUPO(B8000,1,34) ");
      iTmp = sortFile(pFile1, acFile1, acInbuf);
      if (!iTmp)
         return -10;

      pRec = strrchr(acFile2, '.');
      strcpy(pRec, ".SR2");

      sprintf(acInbuf, "S(1,34,C,A,57,10,C,D) F(TXT) DUPO(B8000,1,34) ");
      iTmp = sortFile(pFile2, acFile2, acInbuf);
      if (!iTmp)
         return -10;
   }

   // Open input file
   LogMsg("Open input sale file1 %s", acFile1);
   fd1 = fopen(acFile1, "r");
   if (fd1 == NULL)
   {
      LogMsg("***** Error opening sale file1: %s\n", acFile1);
      return -2;
   }
   LogMsg("Open input sale file2 %s", acFile2);
   fd2 = fopen(acFile2, "r");
   if (fd2 == NULL)
   {
      LogMsg("***** Error opening sale file2: %s\n", acFile2);
      return -2;
   }

   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");

   // Open output file
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fd1))
   {
      if (!(pRec = fgets(acInbuf, 1024, fd1)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0025 024", 8))
      //   iTmp = 0;
#endif
      iTmp = MergePrice(acInbuf, fdOut, fd2);
      fputs(acInbuf, fdOut);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (iTmp == 99)
         break;

      if (iTmp >= 0)
      {
         if (iTmp & 2)
            lUpdPrice++;
         if (iTmp & 4)
            lUpdCode++;
         if (iTmp & 8)
            lUpdMFlag++;
         lMatch++;
      } else if (bDebug)
         LogMsg("---> unmatched: %.34s (%d)", acInbuf, iTmp);
   }

   // Copy the rest of the file over
   while (!feof(fd1))
   {
      if (!(pRec = fgets(acInbuf, 1024, fd1)))
         break;

      fputs(acInbuf, fdOut);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fd1)
      fclose(fd1);
   if (fd2)
      fclose(fd2);
   if (fdOut)
      fclose(fdOut);

   if (bResort)
   {
      DeleteFile(acFile1);
      DeleteFile(acFile2);
   }

   sprintf(acSortFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acInbuf, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) F(TXT) DUPO(B8000,1,34)");
   iTmp = sortFile(acOutFile, acSortFile, acInbuf);
   if (iTmp <= 0)
   {
      LogMsg("***** Error sorting %s to %s", acOutFile, acSortFile);
      iTmp = -1;
   } else
   {
      // Remove tmp output file
      DeleteFile(acOutFile);

      // If no output file specified, output to cum sale file
      if (!pOutfile)
         strcpy(acOutFile, acCSalFile);
      else
         strcpy(acOutFile, pOutfile);

      // Rename files
      if (!_access(acOutFile, 0))
      {
         sprintf(acInbuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acInbuf, 0))
            remove(acInbuf);
         iTmp = rename(acOutFile, acInbuf);
      }
      iTmp = rename(acSortFile, acOutFile);
      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acSortFile, acOutFile);
   }


   LogMsgD("\nTotal input records:      %u", lCnt);
   LogMsg("      matched records:    %u", lMatch);
   LogMsg("      price updated:      %u", lUpdPrice);
   LogMsg("      salecode updated:   %u", lUpdCode);
   LogMsg("      multi-sale updated: %u", lUpdMFlag);

   return iTmp;
}

/******************************* DedupCumSale ********************************
 *
 * Merge records with same DocDate & DocNum then remove duplicate.
 * Remove records that has mismatched year, test sale price.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int DedupCumSale(char *pInfile)
{
   char     acInbuf[2048], acPrevRec[2048], acOutFile[_MAX_PATH], *pRec;
   long     lCnt=0, lDrop=0, iTmp;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];
   SCSAL_REC *pPrevRec= (SCSAL_REC *)&acPrevRec[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** DedupCumSale(): Missing input file: %s", pInfile);
      return -1;
   }

   LogMsg("DedupCumSale for %s", pInfile);

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Get first rec
   fgets(acPrevRec, 1024, fdSale);

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

      // Check DocTax
      if (pInRec->StampAmt[0] > '0' && !ChkDocTax(pInRec->StampAmt))
      {
         // Bad StampAmt
         memset(pInRec->StampAmt, ' ', SALE_SIZ_STAMPAMT);
         memset(pInRec->SalePrice, ' ', SALE_SIZ_SALEPRICE);
      }

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0282630300", 9))
      //   iTmp = 0;
#endif
      // Check year
      if (pInRec->DocNum[0] > ' ' && memcmp(pInRec->DocDate, pInRec->DocNum, 4))
      {
         LogMsg("Drop sale: %.67s", acInbuf);
         lDrop++;
         continue;
      }

      // Check APN
      if (!memcmp(acInbuf, acPrevRec, 12))
      {
         // Check DocDate
         if (!memcmp(pInRec->DocDate, pPrevRec->DocDate, 8))
         {
            // Check DocNum
            if (pPrevRec->DocNum[0] == ' ' && pInRec->DocNum[0] > ' ')
               memcpy(pPrevRec->DocNum, pInRec->DocNum, SALE_SIZ_DOCNUM);
            else if (pInRec->DocNum[0] > ' ' && pPrevRec->DocNum[0] > ' ' &&
                     memcmp(pInRec->DocNum, pPrevRec->DocNum, SALE_SIZ_DOCNUM))
            {
               fputs(acPrevRec, fdOut);
               strcpy(acPrevRec, acInbuf);
               acInbuf[0] = 0;
            }

            if (acInbuf[0])
            {
               // Check Sale price
               if (pPrevRec->SalePrice[0] == ' ' && pInRec->SalePrice[0] > ' ')
               {
                  memcpy(pPrevRec->SalePrice, pInRec->SalePrice, SALE_SIZ_SALEPRICE);
                  pPrevRec->ARCode = 'M';
               }

               // Check Owner
               if (pPrevRec->Name1[0] == ' ' && pInRec->Name1[0] > ' ')
               {
                  memcpy(pPrevRec->Name1, pInRec->Name1, SALE_SIZ_BUYER);
                  pPrevRec->ARCode = 'M';
               }

               // Sale code
               if (pPrevRec->SaleCode[0] == ' ' && pInRec->SaleCode[0] > ' ')
               {
                  pPrevRec->SaleCode[0] = pInRec->SaleCode[0];
                  pPrevRec->ARCode = 'M';
               }

               // % transfer
               if (pPrevRec->PctXfer[0] == ' ' && pInRec->PctXfer[0] > ' ')
               {
                  memcpy(pPrevRec->PctXfer, pInRec->PctXfer, SALE_SIZ_PCTXFER);
                  pPrevRec->ARCode = 'M';
               }
            }  
         } else
         {
            fputs(acPrevRec, fdOut);
            strcpy(acPrevRec, acInbuf);
         }
      } else
      {
         fputs(acPrevRec, fdOut);
         strcpy(acPrevRec, acInbuf);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Output last rec
   fputs(acPrevRec, fdOut);

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if (lDrop > 0)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".org");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Rename output file
      iTmp = rename(acOutFile, pInfile);
   } else
      LogMsg("Good sale file, no record drop!");

   LogMsg("Total input records:     %u", lCnt);
   LogMsg("    records dropped:     %u", lDrop);

   return iTmp;
}

/******************************** FixDocTax **********************************
 *
 * Remove DocTax & SalePrice if it is carried over from prior sale - VEN
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int FixDocTax(char *pInfile)
{
   char     acInbuf[2048], acLastRec[2048], acLastSP[32], acOutFile[_MAX_PATH], *pRec;
   long     lCnt=0, lClean=0, iTmp;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];
   SCSAL_REC *pLastRec= (SCSAL_REC *)&acLastRec[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** FixCumSale(): Missing input file: %s", pInfile);
      return -1;
   }

   LogMsg0("Fix DocTax and Sale Price for %s", pInfile);

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   acLastSP[0] = 0;
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

      // Remove bad seller name
      if (pInRec->Seller2[0] > ' ' && pInRec->Seller2[0] <= 'A')
         memset(pInRec->Seller2, ' ', SALE_SIZ_SELLER);

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0030851700", 9) || !memcmp(acInbuf, "0631011050", 9))
      //   iTmp = 0;
#endif
      if (!memcmp(pInRec->Apn, pLastRec->Apn, iApnLen))
      {
         if (pInRec->SalePrice[SALE_SIZ_SALEPRICE-2] > ' ' && pInRec->Spc_Flg != '1' &&
            !memcmp(pInRec->SalePrice, acLastSP, SALE_SIZ_SALEPRICE) &&
             memcmp(pInRec->DocDate, pLastRec->DocDate, 8) >= 0)
         {
            if (bDebug)
               LogMsg("P&T %.10s: DocDate->%.8s DocNum->%.12s", pInRec->Apn, pInRec->DocDate, pInRec->DocNum);
            memset(pInRec->SalePrice, ' ', SALE_SIZ_SALEPRICE);
            memset(pInRec->StampAmt,  ' ', SALE_SIZ_STAMPAMT);
            lClean++;
         } else if (pInRec->StampAmt[0] == ' ' && pInRec->SalePrice[SALE_SIZ_SALEPRICE-2] > ' ')
         {
            if (bDebug)
               LogMsg("P %.10s: DocDate->%.8s DocNum->%.12s", pInRec->Apn, pInRec->DocDate, pInRec->DocNum);
            memset(pInRec->SalePrice, ' ', SALE_SIZ_SALEPRICE);
            lClean++;
         } else
         {
            strcpy(acLastRec, acInbuf);
            iTmp = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
            if (iTmp > 100)
               memcpy(acLastSP, pInRec->SalePrice, SALE_SIZ_SALEPRICE);
         }
      } else
      {
         strcpy(acLastRec, acInbuf);
         memcpy(acLastSP, pInRec->SalePrice, SALE_SIZ_SALEPRICE);
      }
      fputs(acInbuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if (lClean > 0)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".org");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Rename output file
      iTmp = rename(acOutFile, pInfile);
   } else
   {
      LogMsg("Good sale file, no record drop!");
      iTmp = 0;
   }
   LogMsgD("\nTotal input records:     %u\n", lCnt);
   LogMsg("    records cleaned:     %u", lClean);

   return iTmp;
}

/******************************** SaleDedup **********************************
 *
 * Dedup sale records that has same APN and DocDate without DocNum.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int SaleDedup(char *pInfile)
{
   char     acInbuf[2048], acLastRec[2048], acOutFile[_MAX_PATH], *pRec;
   long     lCnt=0, lClean=0, lDrop=0, iTmp;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];
   SCSAL_REC *pLastRec= (SCSAL_REC *)&acLastRec[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** FixCumSale(): Missing input file: %s", pInfile);
      return -1;
   }

   LogMsg("Dedup sale records that has same APN and DocDate without DocNum for %s", pInfile);

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Get first rec
   pRec = fgets(acLastRec, 1024, fdSale);

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "2362630350", 9))
      //   iTmp = 0;
#endif

      // Same APN & DocDate
      if (!memcmp(pInRec->Apn, pLastRec->Apn, iApnLen) &&
          !memcmp(pInRec->DocDate, pLastRec->DocDate, 8)) 
      {
         // If same sale price, remove it
         if (!memcmp(pInRec->SalePrice, pLastRec->SalePrice, SALE_SIZ_SALEPRICE) &&
            pInRec->SalePrice[SALE_SIZ_SALEPRICE-2] > ' ' )
         {
            // Different DocNum, copy to last rec
            if (pInRec->DocNum[0] > ' ' && pLastRec->DocNum[0] == ' ')
            {
               if (bDebug)
                  LogMsg("Update %.10s: DocDate->%.8s DocNum->%.12s", pInRec->Apn, pInRec->DocDate, pInRec->DocNum);

               // Copy DocNum to LastRec
               memcpy(pLastRec->DocNum, pInRec->DocNum, SALE_SIZ_DOCNUM);

               // Copy seller & new owner
               if (pInRec->Seller1[0] > ' ' && pLastRec->Seller1[0] == ' ')
                  memcpy(pLastRec->Seller1, pInRec->Seller1, SALE_SIZ_SELLER);
               if (pInRec->Name1[0] > ' ' && pLastRec->Name1[0] == ' ')
                  memcpy(pLastRec->Name1, pInRec->Name1, SALE_SIZ_BUYER);

               lClean++;
            } else if (memcmp(pLastRec->DocNum, pInRec->DocNum, SALE_SIZ_DOCNUM))
            {
               fputs(acLastRec, fdOut);
               strcpy(acLastRec, acInbuf);
            } else
            {
               lDrop++;
               if (bDebug)
                  LogMsg("Drop record %.10s: DocDate->%.8s DocNum->%.12s", pInRec->Apn, pInRec->DocDate, pInRec->DocNum);
            }
         } else if (pLastRec->DocNum[0] == ' ' && isdigit(pInRec->DocNum[0]) &&
            pLastRec->SalePrice[SALE_SIZ_SALEPRICE-2] > ' ')
         {
            if (bDebug)
               LogMsg("Update %.10s: DocDate->%.8s DocNum->%.12s", pInRec->Apn, pInRec->DocDate, pInRec->DocNum);

            // Copy current DocNum to last rec
            memcpy(pLastRec->DocNum, pInRec->DocNum, SALE_SIZ_DOCNUM);

            // Copy seller & new owner
            if (pInRec->Seller1[0] > ' ' && pLastRec->Seller1[0] == ' ')
               memcpy(pLastRec->Seller1, pInRec->Seller1, SALE_SIZ_SELLER);
            if (pInRec->Name1[0] > ' ' && pLastRec->Name1[0] == ' ')
               memcpy(pLastRec->Name1, pInRec->Name1, SALE_SIZ_BUYER);

            lClean++;
         } else if (!memcmp(pLastRec->DocNum, pInRec->DocNum, SALE_SIZ_DOCNUM))
         {
            lDrop++;
            if (bDebug)
               LogMsg("Drop same doc %.10s: DocDate->%.8s DocNum->%.12s", pInRec->Apn, pInRec->DocDate, pInRec->DocNum);
         } else
         {
            fputs(acLastRec, fdOut);
            strcpy(acLastRec, acInbuf);
         }
/*
      } else if (!memcmp(pInRec->SalePrice, pLastRec->SalePrice, SALE_SIZ_SALEPRICE) &&
         !memcmp(pInRec->Apn, pLastRec->Apn, iApnLen) &&
         pInRec->SalePrice[SALE_SIZ_SALEPRICE-2] > ' ' )
      {
         if (bDebug)
            LogMsg("Clean amt %.10s: DocDate->%.8s DocNum->%.12s DocTax->%.12s", pInRec->Apn, pInRec->DocDate, pInRec->DocNum, pInRec->StampAmt);

         // Remove sale price of cureent rec
         memset(pInRec->SalePrice, ' ', SALE_SIZ_SALEPRICE);
         memset(pInRec->StampAmt,  ' ', SALE_SIZ_STAMPAMT);
         lClean++;

         fputs(acLastRec, fdOut);
         strcpy(acLastRec, acInbuf);
*/
      } else
      {
         fputs(acLastRec, fdOut);
         strcpy(acLastRec, acInbuf);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if (lClean > 0)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".dup");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Rename output file
      iTmp = rename(acOutFile, pInfile);
   } else
      LogMsg("Good sale file, no record drop!");

   LogMsgD("\nTotal input records:     %u\n", lCnt);
   LogMsg("    records cleaned:     %u", lClean);

   return iTmp;
}

/********************************* FixSCSal **********************************
 *
 * Fix specific cases in sale file.  This will replace FixCumSale()
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int FixSCSal(char *pInfile, int iType, int iOption, bool bRemove)
{
   char     acInbuf[2048], acOutFile[_MAX_PATH], *pRec, sTmp1[32], sTmp2[32];
   long     lCnt=0, lClean=0, iTmp, lVal;
   bool     bKeepIt;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];

   LogMsg0("Fix sale history file %s", pInfile);
   if (_access(pInfile, 0))
   {
      LogMsg("***** FixSCSal(): Missing input file: %s", pInfile);
      return -1;
   }

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0282630300", 9))
      //   iTmp = 0;
#endif
      //replCharEx(acInbuf, "`{}~[]", ' ', 0, false);
      if (iType & SALE_FIX_REMSCODE)            // Remove SaleCode
      {
         iTmp = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
         if (pInRec->SaleCode[0] > ' ' && !iTmp)
         {
            pInRec->SaleCode[0] = ' ';
            lClean++;
         }
      }
      if (iType & SALE_FIX_SALECODE)
      {
         if (pInRec->SaleCode[0] == 'M')
         {
            pInRec->SaleCode[0] = ' ';
            pInRec->MultiSale_Flg = 'Y';
            lClean++;
         }
      }
      if (iType & SALE_FIX_NOPRCLXFR)
      {
         if (pInRec->NumOfPrclXfer[0] == 'M')
         {
            pInRec->NumOfPrclXfer[0] = ' ';
            pInRec->MultiSale_Flg = 'Y';
            lClean++;
         } else if (pInRec->NumOfPrclXfer[0] > '1' && pInRec->NumOfPrclXfer[0] <= '9')
         {
            pInRec->MultiSale_Flg = 'Y';
            lClean++;
         }
      }
      if (iType & SALE_FIX_SALEPRICE)
      {
         iTmp = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
         if (iTmp >= 0)
         {
            if (bRemove)
            {
               memset(pInRec->SalePrice, ' ', SALE_SIZ_SALEPRICE);
               memset(pInRec->StampAmt, ' ', SALE_SIZ_STAMPAMT);
               lClean++;
            } else 
            {
               // to be done ...
            }
         }
      }

      bKeepIt = true;
      if (iType & SALE_FIX_DOCNUM)
      {
         if (iOption == CNTY_STA)
         {
            if (pInRec->DocNum[4] == 'R')
            {
               memcpy(sTmp1, &pInRec->DocNum[5], 7);
               sTmp1[7] = 0;
               replCharEx(sTmp1, "-+*`./{}&%$", ' ', 0, false);
               remChar(sTmp1, ' ');
               if (isNumber(sTmp1))
               {
                  lVal = atol(sTmp1);
                  sprintf(sTmp2, "%.7d ", lVal);
                  memcpy(&pInRec->DocNum[5], sTmp2, 7);
                  lClean++;
               }
            }
         } else if (iOption == CNTY_RIV)
         {
            if (pInRec->DocNum[6] == ' ')
            {
               lVal = atoin(&pInRec->DocNum[0], 6);
               sprintf(sTmp1, "%.7d ", lVal);
               memcpy(&pInRec->DocNum[0], sTmp1, 7);
               lClean++;
            }
         } else if (iOption == CNTY_FRE)
         {
            // Format DOCNUM to 1234567
            lVal = atoin(&pInRec->DocNum[0], 7);
            iTmp = atoin(pInRec->DocDate, 4);
            if (lVal > 0 && iTmp > 1980)
            {
               iTmp = sprintf(sTmp1, "%.7d", lVal);
               memcpy(&pInRec->DocNum[0], sTmp1, iTmp);
               lClean++;
            }
         } else if (iOption == CNTY_MNO)
         {
            if (isdigit(pInRec->DocNum[4]))
            {
               try
               {
                  lVal = atoin(&pInRec->DocNum[4], 7);
                  iTmp = sprintf(sTmp1, "R%.7d", lVal);
                  memcpy(&pInRec->DocNum[4], sTmp1, iTmp);
                  lClean++;
               } catch (...)
               {
                  LogMsg("*** Bad DocNum: %.12s", pInRec->DocNum);
               }
            }
         } else if (iOption == CNTY_SFX)
         {
            // Format DOCNUM to A999999
            if (pInRec->DocNum[0] >= 'A')
            {
               if (isdigit(pInRec->DocNum[1]) )
               {
                  iTmp = sprintf(sTmp1, "%.3s  ", &pInRec->DocNum[6]);
                  memcpy(&pInRec->DocNum[4], sTmp1, iTmp);
               } else
               {
                  iTmp = sprintf(sTmp1, "%.4s%.4s  ", &pInRec->DocNum[0], &pInRec->DocNum[5]);
                  memcpy(&pInRec->DocNum[0], sTmp1, iTmp);
               }
               lClean++;
            }
         } else if (iOption == CNTY_SJX)
         {
            // Format DOCNUM to A999999
            if (isdigit(pInRec->DocNum[0]))
            {
               if (!strchr(pInRec->DocNum, '-') && 
                  !memcmp(pInRec->DocNum, &pInRec->DocDate[2], 2) &&
                  isNumber(pInRec->DocNum, 8) )
               {
                  iTmp = sprintf(sTmp1, "%.4sR%.6s", pInRec->DocDate, &pInRec->DocNum[2]);
                  memcpy(pInRec->DocNum, sTmp1, iTmp);
                  lClean++;
               }
            } else
               bKeepIt = false;
         } else if (iOption == CNTY_TRI)
         {
            // Format DOCNUM to yyyy12345
            if (pInRec->DocNum[0] >= '0')
            {
               iTmp = atoin(pInRec->DocNum, 4);
               if (pInRec->DocNum[0] == '9')
               {
                  lVal = atoin(&pInRec->DocNum[2], 6);
                  iTmp = sprintf(sTmp1, "19%.2s%.5d", pInRec->DocNum, lVal);
               } else if (iTmp > 1996 && iTmp <= lToyear)
               {
                  lVal = atoin(&pInRec->DocNum[4], 6);
                  iTmp = sprintf(sTmp1, "%.4s%.5d", pInRec->DocNum, lVal);
               } else if (!memcmp(pInRec->DocNum, "09", 2))
               {
                  lVal = atoin(&pInRec->DocNum[3], 6);
                  iTmp = sprintf(sTmp1, "19%.2s%.5d", &pInRec->DocNum[1], lVal);
               } else if (iTmp > 1990 && iTmp <= 1996)
               {
                  lVal = atoin(&pInRec->DocNum[4], 6);
                  iTmp = sprintf(sTmp1, "%.4s%.5d", pInRec->DocNum, lVal);
               } else
                  iTmp = 0;

               memcpy(&pInRec->DocNum[0], sTmp1, iTmp);
               lClean++;
            }
         } else
         {
            if (pInRec->DocNum[11] == ' ')
               iTmp = replNonNum(&pInRec->DocNum[5], ' ', 6);
            else
               iTmp = replNonNum(&pInRec->DocNum[5], ' ', 7);
            if (iTmp > 0)
               lClean++;
         }
      }
      if (iType & SALE_FIX_SPCFLG)
      {  // VEN
         if (pInRec->Spc_Flg > 0 && pInRec->Spc_Flg < 10)
         {
            pInRec->Spc_Flg |= 48;
            lClean++;
         }

         if (pInRec->Spc_Flg == '2' && pInRec->DocNum[0] >= '0')
            pInRec->Spc_Flg = '3';
      }
      if (iType & SALE_FIX_NONESALE)
      {  // SFX
         if (bRemove)
         {
            pInRec->NoneSale_Flg = ' ';
            lClean++;
         }
      }
      if (iType & SALE_FIX_DOCTYPE)
      {  // SJX
         iTmp = atoin(pInRec->DocType, SIZ_SALE1_DOCTYPE);
         lClean++;
         if (iTmp == 70)
         {  // AM -> Administrator's Deed
            memcpy(pInRec->DocType, "5  ", 3);
         } if (iTmp == 73)
         {  // GU -> Deed Of Guardian
            memcpy(pInRec->DocType, "14 ", 3);
         } if (iTmp == 74)
         {  // MS -> Tax Deed or Misc (Contract, Deed, ...)
            iTmp = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
            if (iTmp < 1000)
               pInRec->NoneSale_Flg = 'Y';
            else
               memcpy(pInRec->DocType, "67 ", 3);
         } if (iTmp == 14 || iTmp == 17)
         {  // Deed of Guardian, Individual GD -> Deed
            memcpy(pInRec->DocType, "13 ", 3);
         } if (iTmp == 84 || iTmp == 83 )
         {  // Correction -> Agreement of Sale
            memcpy(pInRec->DocType, "8  ", 3);
         } if (iTmp == 9 || iTmp == 93 || iTmp == 94)
         {  // CD-Correction Deed -> Deed (Conservator)
            memcpy(pInRec->DocType, "13 ", 3);
         } if (iTmp == 36)
         {  // CT-Contract For Deed -> Decree Of Distribution
            memcpy(pInRec->DocType, "80 ", 3);
         } if (iTmp == 37)
         {  // CV-Conveyance Deed -> Agreement of Sale (Cal Vet)
            memcpy(pInRec->DocType, "8  ", 3);
         } if (iTmp == 0)
         {  // Some type of Deed                
            iTmp = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
            if (iTmp > 1000)
            {
               memcpy(pInRec->DocType, "13 ", 3);
               LogMsg("APN=%.8s, Price=%.10s", pInRec->Apn, pInRec->SalePrice);
            }
         } if (iTmp == 43 || iTmp == 44)
         {
            memcpy(pInRec->DocType, "   ", 3);
            pInRec->NoneSale_Flg = 'Y';
         } else
            lClean--;
      }

      if (bKeepIt)
         fputs(acInbuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if (lClean > 0)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".org");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Rename output file
      iTmp = rename(acOutFile, pInfile);
   } else
      LogMsg("Good sale file, no record drop!");

   LogMsgD("\nTotal input records:     %u\n", lCnt);
   LogMsg("    records cleaned:     %u", lClean);

   return iTmp;
}

// Same as FixSCSal() but for SCSAL_EXT record
int FixSCSalEx(char *pInfile, int iType, int iOption, bool bRemove)
{
   char     acInbuf[2048], acOutFile[_MAX_PATH], *pRec, sTmp1[32], sTmp2[32];
   long     lCnt=0, lClean=0, iTmp, lVal;
   bool     bKeepIt;
   FILE     *fdOut;

   SCSAL_EXT *pInRec  = (SCSAL_EXT *)&acInbuf[0];

   LogMsg0("Fix sale history file %s", pInfile);
   if (_access(pInfile, 0))
   {
      LogMsg("***** FixSCSal(): Missing input file: %s", pInfile);
      return -1;
   }

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      memset(acInbuf, ' ', 2048);
      if (!(pRec = fgets(acInbuf, 2048, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0282630300", 9))
      //   iTmp = 0;
#endif
      if (pInRec->filler1 < ' ')
      {
         memset(&pInRec->filler1-1, ' ', 2);
         pInRec->CRLF[0] = '\n';
         pInRec->CRLF[1] = 0;
      }

      //replCharEx(acInbuf, "`{}~[]", ' ', 0, false);
      if (iType & SALE_FIX_REMSCODE)            // Remove SaleCode
      {
         iTmp = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
         if (pInRec->SaleCode[0] > ' ' && !iTmp)
         {
            pInRec->SaleCode[0] = ' ';
            lClean++;
         }
      }
      if (iType & SALE_FIX_SALECODE)
      {
         if (pInRec->SaleCode[0] == 'M')
         {
            pInRec->SaleCode[0] = ' ';
            pInRec->MultiSale_Flg = 'Y';
            lClean++;
         }
      }
      if (iType & SALE_FIX_NOPRCLXFR)
      {
         if (pInRec->NumOfPrclXfer[0] == 'M')
         {
            pInRec->NumOfPrclXfer[0] = ' ';
            pInRec->MultiSale_Flg = 'Y';
            lClean++;
         } else if (pInRec->NumOfPrclXfer[0] > '1' && pInRec->NumOfPrclXfer[0] <= '9')
         {
            pInRec->MultiSale_Flg = 'Y';
            lClean++;
         }
      }
      if (iType & SALE_FIX_SALEPRICE)
      {
         iTmp = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
         if (iTmp >= 0)
         {
            if (bRemove)
            {
               memset(pInRec->SalePrice, ' ', SALE_SIZ_SALEPRICE);
               memset(pInRec->StampAmt, ' ', SALE_SIZ_STAMPAMT);
               lClean++;
            } else 
            {
               // to be done ...
            }
         }
      }

      bKeepIt = true;
      if (iType & SALE_FIX_DOCNUM)
      {
         if (iOption == CNTY_STA)
         {
            if (pInRec->DocNum[4] == 'R')
            {
               memcpy(sTmp1, &pInRec->DocNum[5], 7);
               sTmp1[7] = 0;
               replCharEx(sTmp1, "-+*`./{}&%$", ' ', 0, false);
               remChar(sTmp1, ' ');
               if (isNumber(sTmp1))
               {
                  lVal = atol(sTmp1);
                  sprintf(sTmp2, "%.7d ", lVal);
                  memcpy(&pInRec->DocNum[5], sTmp2, 7);
                  lClean++;
               }
            }
         } else if (iOption == CNTY_RIV)
         {
            if (pInRec->DocNum[6] == ' ')
            {
               lVal = atoin(&pInRec->DocNum[0], 6);
               sprintf(sTmp1, "%.7d ", lVal);
               memcpy(&pInRec->DocNum[0], sTmp1, 7);
               lClean++;
            }
         } else if (iOption == CNTY_FRE)
         {
            // Format DOCNUM to 1234567
            lVal = atoin(&pInRec->DocNum[0], 7);
            iTmp = atoin(pInRec->DocDate, 4);
            if (lVal > 0 && iTmp > 1980)
            {
               iTmp = sprintf(sTmp1, "%.7d", lVal);
               memcpy(&pInRec->DocNum[0], sTmp1, iTmp);
               lClean++;
            }
         } else if (iOption == CNTY_MNO)
         {
            if (isdigit(pInRec->DocNum[4]))
            {
               try
               {
                  lVal = atoin(&pInRec->DocNum[4], 7);
                  iTmp = sprintf(sTmp1, "R%.7d", lVal);
                  memcpy(&pInRec->DocNum[4], sTmp1, iTmp);
                  lClean++;
               } catch (...)
               {
                  LogMsg("*** Bad DocNum: %.12s", pInRec->DocNum);
               }
            }
         } else if (iOption == CNTY_SFX)
         {
            // Format DOCNUM to A999999
            if (pInRec->DocNum[0] >= 'A')
            {
               if (isdigit(pInRec->DocNum[1]) )
               {
                  iTmp = sprintf(sTmp1, "%.3s  ", &pInRec->DocNum[6]);
                  memcpy(&pInRec->DocNum[4], sTmp1, iTmp);
               } else
               {
                  iTmp = sprintf(sTmp1, "%.4s%.4s  ", &pInRec->DocNum[0], &pInRec->DocNum[5]);
                  memcpy(&pInRec->DocNum[0], sTmp1, iTmp);
               }
               lClean++;
            }
         } else if (iOption == CNTY_SJX)
         {
            // Format DOCNUM to A999999
            if (isdigit(pInRec->DocNum[0]))
            {
               if (!strchr(pInRec->DocNum, '-') && 
                  !memcmp(pInRec->DocNum, &pInRec->DocDate[2], 2) &&
                  isNumber(pInRec->DocNum, 8) )
               {
                  iTmp = sprintf(sTmp1, "%.4sR%.6s", pInRec->DocDate, &pInRec->DocNum[2]);
                  memcpy(pInRec->DocNum, sTmp1, iTmp);
                  lClean++;
               }
            } else
               bKeepIt = false;
         } else if (iOption == CNTY_TRI)
         {
            // Format DOCNUM to yyyy12345
            if (pInRec->DocNum[0] >= '0')
            {
               iTmp = atoin(pInRec->DocNum, 4);
               if (pInRec->DocNum[0] == '9')
               {
                  lVal = atoin(&pInRec->DocNum[2], 6);
                  iTmp = sprintf(sTmp1, "19%.2s%.5d", pInRec->DocNum, lVal);
               } else if (iTmp > 1996 && iTmp <= lToyear)
               {
                  lVal = atoin(&pInRec->DocNum[4], 6);
                  iTmp = sprintf(sTmp1, "%.4s%.5d", pInRec->DocNum, lVal);
               } else if (!memcmp(pInRec->DocNum, "09", 2))
               {
                  lVal = atoin(&pInRec->DocNum[3], 6);
                  iTmp = sprintf(sTmp1, "19%.2s%.5d", &pInRec->DocNum[1], lVal);
               } else if (iTmp > 1990 && iTmp <= 1996)
               {
                  lVal = atoin(&pInRec->DocNum[4], 6);
                  iTmp = sprintf(sTmp1, "%.4s%.5d", pInRec->DocNum, lVal);
               } else
                  iTmp = 0;

               memcpy(&pInRec->DocNum[0], sTmp1, iTmp);
               lClean++;
            }
         } else if (iOption == CNTY_VEN)
         {
            if (pInRec->DocNum[0] >= '0')
            {
               memcpy(pInRec->DocNum, "00", 2);
               pInRec->ARCode = 'R';
               lClean++;
            }
         } else
         {
            if (pInRec->DocNum[11] == ' ')
               iTmp = replNonNum(&pInRec->DocNum[5], ' ', 6);
            else
               iTmp = replNonNum(&pInRec->DocNum[5], ' ', 7);
            if (iTmp > 0)
               lClean++;
         }
      }
      if (iType & SALE_FIX_SPCFLG)
      {  // VEN
         if (pInRec->Spc_Flg > 0 && pInRec->Spc_Flg < 10)
         {
            pInRec->Spc_Flg |= 48;
            lClean++;
         }

         if (pInRec->Spc_Flg == '2' && pInRec->DocNum[0] >= '0')
            pInRec->Spc_Flg = '3';
      }
      if (iType & SALE_FIX_NONESALE)
      {  // SFX
         if (bRemove)
         {
            pInRec->NoneSale_Flg = ' ';
            lClean++;
         }
      }
      if (iType & SALE_FIX_DOCTYPE)
      {  // SJX
         iTmp = atoin(pInRec->DocType, SIZ_SALE1_DOCTYPE);
         lClean++;
         if (iTmp == 70)
         {  // AM -> Administrator's Deed
            memcpy(pInRec->DocType, "5  ", 3);
         } if (iTmp == 73)
         {  // GU -> Deed Of Guardian
            memcpy(pInRec->DocType, "14 ", 3);
         } if (iTmp == 74)
         {  // MS -> Tax Deed or Misc (Contract, Deed, ...)
            iTmp = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
            if (iTmp < 1000)
               pInRec->NoneSale_Flg = 'Y';
            else
               memcpy(pInRec->DocType, "67 ", 3);
         } if (iTmp == 14 || iTmp == 17)
         {  // Deed of Guardian, Individual GD -> Deed
            memcpy(pInRec->DocType, "13 ", 3);
         } if (iTmp == 84 || iTmp == 83 )
         {  // Correction -> Agreement of Sale
            memcpy(pInRec->DocType, "8  ", 3);
         } if (iTmp == 9 || iTmp == 93 || iTmp == 94)
         {  // CD-Correction Deed -> Deed (Conservator)
            memcpy(pInRec->DocType, "13 ", 3);
         } if (iTmp == 36)
         {  // CT-Contract For Deed -> Decree Of Distribution
            memcpy(pInRec->DocType, "80 ", 3);
         } if (iTmp == 37)
         {  // CV-Conveyance Deed -> Agreement of Sale (Cal Vet)
            memcpy(pInRec->DocType, "8  ", 3);
         } if (iTmp == 0)
         {  // Some type of Deed                
            iTmp = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
            if (iTmp > 1000)
            {
               memcpy(pInRec->DocType, "13 ", 3);
               LogMsg("APN=%.8s, Price=%.10s", pInRec->Apn, pInRec->SalePrice);
            }
         } if (iTmp == 43 || iTmp == 44)
         {
            memcpy(pInRec->DocType, "   ", 3);
            pInRec->NoneSale_Flg = 'Y';
         } else
            lClean--;
      }

      if (bKeepIt)
         fputs(acInbuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if (lClean > 0)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".org");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Rename output file
      iTmp = rename(acOutFile, pInfile);
   } else
      LogMsg("Good sale file, no record drop!");

   LogMsgD("\nTotal input records:     %u\n", lCnt);
   LogMsg("    records cleaned:     %u", lClean);

   return iTmp;
}

/***************************** UpdateSaleUsingGrGr **************************
 *
 * If sale has no DocNum and DocDate is the same as GrGr DocDate, 
 * copy GrGr DocNum to sale record for that parcel.
 *
 * Return 0 if success, otherwise error.
 *
 ****************************************************************************/

int UpdateSaleInfo(char *pSaleRec, int iUpdFlds, bool bSaleOnly)
{
   static   char  acRec[2048], *pRec=NULL;
   int      iRet=1, iLoop;

   SCSAL_REC *pSale = (SCSAL_REC *)pSaleRec;
   GRGR_DOC  *pGrgr = (GRGR_DOC *)&acRec[0];
   bool       bGrantDeed=false;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdGrGr);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001060075", 9))
   //   iRet = 0;
#endif

   // First match APN
   do
   {
      if (!pRec)
      {
         fclose(fdGrGr);
         fdGrGr = NULL;
         return iRet;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pSale->Apn, pGrgr->APN, iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 1024, fdGrGr);
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return iRet;

   // Second match - DocDate
   do 
   {
      iLoop = memcmp(pSale->DocDate, pGrgr->DocDate, 8);
      if (iLoop > 0)
         pRec = fgets(acRec, 1024, fdGrGr);
   } while (pRec && iLoop > 0 && !memcmp(pSale->Apn, pGrgr->APN, iApnLen));

   // If not match, return
   if (iLoop)
      return iRet;

   // Third match - DocType GD
   if (bSaleOnly)
   {
      while (!bGrantDeed && pRec && !memcmp(pSale->Apn, pGrgr->APN, iApnLen))
      {
         if (strstr(pGrgr->DocTitle, "GRANT"))
            bGrantDeed = true;
         else
            pRec = fgets(acRec, 1024, fdGrGr);
      } 
   }

   if (bGrantDeed && !memcmp(pSale->Apn, pGrgr->APN, iApnLen) && isdigit(pGrgr->DocNum[0]))
   {
      if (pSale->DocNum[0] < '0')
      {
         char *pTmp;

         memcpy(pSale->DocNum,  pGrgr->DocNum,  SALE_SIZ_DOCNUM);

         // Update seller
         if (iUpdFlds & GRGR_UPD_SELLER)
         {        
            pGrgr->Grantor[0][SALE_SIZ_SELLER] = 0;
            if (pTmp = strchr(pGrgr->Grantor[0], '(')) *pTmp = '\0';
            vmemcpy(pSale->Seller1, pGrgr->Grantor[0], SALE_SIZ_SELLER);
            if (*pGrgr->Grantor[1] > ' ')
            {
               pGrgr->Grantor[1][SALE_SIZ_SELLER] = 0;
               if (pTmp = strchr(pGrgr->Grantor[1], '(')) *pTmp = '\0';
               vmemcpy(pSale->Seller2, pGrgr->Grantor[1], SALE_SIZ_SELLER);
            }
         }

         // Update owner
         if (iUpdFlds & GRGR_UPD_OWNER)
         {
            pGrgr->Grantee[0][SALE_SIZ_BUYER-1] = 0;
            if (pTmp = strchr(pGrgr->Grantee[0], '(')) *pTmp = '\0';
            vmemcpy(pSale->Name1, pGrgr->Grantee[0], SALE_SIZ_BUYER);
            if (*pGrgr->Grantee[1] > ' ')
            {
               pGrgr->Grantee[1][SALE_SIZ_BUYER-1] = 0;
               if (pTmp = strchr(pGrgr->Grantee[1], '(')) *pTmp = '\0';
               vmemcpy(pSale->Name2, pGrgr->Grantee[1], SALE_SIZ_BUYER);
            }
         }
         pSale->Spc_Flg = '3';      // Flag to specify DocNum is from GRGR
         iRet = 0;
      }
   }

   return iRet;
}

int UpdateSaleUsingGrGr(char *pGrgrFile, int iUpdFlds, bool bSaleOnly)
{
   char     acSaleBuf[2048], acTmp[_MAX_PATH], acTmpFile[_MAX_PATH];
   char     *pTmp;
   FILE     *fdOut;
   int      iRet, iUpdate=0, lCnt=0;

   SCSAL_REC *pSale = (SCSAL_REC *)&acSaleBuf[0];

   LogMsg0("Update sale Doc# using GrGr");

   // Open input file
   LogMsg("Open cum sale file: ", acCSalFile);
   if (!(fdCSale = fopen(acCSalFile, "r")))
   {
      LogMsg("***** Error opening %s", acCSalFile);
      return -1;
   }

   if (pGrgrFile && !_access(pGrgrFile, 0))
      strcpy(acTmp, pGrgrFile);
   else
      sprintf(acTmp, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   LogMsg("Open GrGr file: ", acTmp);
   if (!(fdGrGr = fopen(acTmp, "r")))
   {
      LogMsg("***** Error opening %s", acTmp);
      return -2;
   }

   // Open output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Open temp output file: ", acTmpFile);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error opening %s", acTmpFile);
      return -3;
   }

   while (!feof(fdCSale))
   {
      if (!(pTmp = fgets(acSaleBuf, 1024, fdCSale)))
         break;

      if (fdGrGr)
      {
         iRet = UpdateSaleInfo(acSaleBuf, iUpdFlds, bSaleOnly);
         if (!iRet)
            iUpdate++;
      }
      fputs(acSaleBuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u - %d", lCnt, iUpdate);      
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdGrGr)
      fclose(fdGrGr);
   if (fdOut)
      fclose(fdOut);

   // Rename output if updating occurs
   if (iUpdate > 0)
   {
      sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Ugr");
      if (!_access(acTmp, 0))
         DeleteFile(acTmp);
      LogMsg("Rename %s to %s", acCSalFile, acTmp);
      iRet = rename(acCSalFile, acTmp);
      LogMsg("Rename %s to %s", acTmpFile, acCSalFile);
      iRet = rename(acTmpFile, acCSalFile);
   }

   LogMsg("Total output records:    %u", lCnt);
   LogMsg("      updated records:   %u", iUpdate);

   return iRet;
}

/******************************** FixOtherApn *********************************
 *
 * Right align OtherApn.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int FixOtherApn(char *pInfile, int iLen)
{
   char     acInbuf[2048],  acOutFile[_MAX_PATH], acTmp[32], *pRec;
   long     lCnt=0, iTmp;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** FixOtherApn(): Missing input file: %s", pInfile);
      return -1;
   }

   LogMsg("Fix OtherApn for %s", pInfile);

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Loop through all records
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

      iTmp = atoin(pInRec->OtherApn, iLen);
      sprintf(acTmp, "%*d", iLen, iTmp);
      memcpy(pInRec->OtherApn, acTmp, iLen);
      fputs(acInbuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   // Save input file
   strcpy(acInbuf, pInfile);
   pRec = strrchr(acInbuf, '.');
   strcpy(pRec, ".org");
   if (!_access(acInbuf, 0))
      DeleteFile(acInbuf);

   // Save input file
   iTmp = rename(pInfile, acInbuf);

   // Rename output file 
   iTmp = rename(acOutFile, pInfile);

   LogMsgD("\nTotal records processed:     %u\n", lCnt);

   return iTmp;
}

/******************************************************************************
 *
 * Sort on Docdate & DocNum. Set MultiSale_Flg='Y' on records that have the same
 * DocNum & DocDate.
 *
 * Return 0 if success
 *
 ******************************************************************************/

int SetMultiSale(char *pInfile, char *pSrtCmd, char *pCnty)
{
   char     acTmp[_MAX_PATH], acInbuf[2048], acSavbuf[2048], acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH], acSrtCmd[256], *pRec;
   long     lCnt=0, lPrice;
   int      iCnt, iTmp, iRet = -1;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];
   SCSAL_REC *pSavRec = (SCSAL_REC *)&acSavbuf[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** SetMultiSale(): Missing input file: %s", pInfile);
      return iRet;
   }

   LogMsg("Set MultiSale Flag for %s", pInfile);

   // Sort input file on DocDate & DocNum
   sprintf(acSrtCmd, "S(27,8,C,A,15,12,C,A,35,3,C,A,1,14,C,A) DUPO(B8000,1,34)");
   sprintf(acTmpFile, "%s\\%s\\SaleDNA.tmp", acTmpPath, pCnty);
   iTmp = sortFile(pInfile, acTmpFile, acSrtCmd);
   if (iTmp <= 0)
      return iRet;

   // Open input file
   LogMsg("Open input file %s", acTmpFile);
   fdSale = fopen(acTmpFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening file: %s\n", acTmpFile);
      return iRet;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return iRet;
   }

   // Save first record
   fgets(acSavbuf, 1024, fdSale);
   iCnt=0;

   // Loop through all records
   while (!feof(fdSale))
   {
      //if (!memcmp(pInRec->Apn, "00027005", 8))
      //   iTmp =0;
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;
      if (pInRec->DocNum[0] > ' ' && 
         !memcmp(pInRec->DocNum, pSavRec->DocNum, SALE_SIZ_DOCNUM) &&
         !memcmp(pInRec->DocDate, pSavRec->DocDate, SALE_SIZ_DOCDATE) &&
         !memcmp(pInRec->DocType, pSavRec->DocType, 3) )
      {
         pInRec->MultiSale_Flg = 'Y';
         pSavRec->MultiSale_Flg = 'Y';
         memcpy(acTmp, pInRec->SalePrice, SALE_SIZ_SALEPRICE);
         acTmp[SALE_SIZ_SALEPRICE] = 0;
         lPrice = atol(pSavRec->SalePrice);
         if (lPrice > 0)
         {
            pSavRec->SaleCode[0] = 'P';
            pInRec->SaleCode[0] = 'P';
         }

         iCnt++;
      }

      fputs(acSavbuf, fdOut);
      memcpy(acSavbuf, acInbuf, sizeof(SCSAL_REC));

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Output last record
   fputs(acSavbuf, fdOut);

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   // Save input file
   if (iCnt > 0)
   {
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".sav");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Resort output file on APN, DocDate, DocNum
      if (pSrtCmd && *pSrtCmd > 'A')
      {
         iTmp = sortFile(acOutFile, pInfile, pSrtCmd);
         if (iTmp > 0)
            iRet = DeleteFile(acOutFile);
      } else
         iRet = MoveFile(acOutFile, pInfile);
   } else
   {
      LogMsg("*** Nothing is changed...");
      iRet = DeleteFile(acOutFile);
   }

   LogMsg("Total records processed:   %u", lCnt);
   LogMsg("Total records updated:     %u\n", iCnt);

   if (!iRet) 
      iRet = -1;
   else
      iRet = 0;

   return iRet;
}

/******************************************************************************
 *
 * Set SaleCode='P' on records that have 0 < PctXfer < 100 and 'F' if PctXfer=100
 * Return 0 if successful
 *
 ******************************************************************************/

int SetFullPartialSale(char *pInfile)
{
   char     acInbuf[2048], acSavbuf[2048], acOutFile[_MAX_PATH], *pRec;
   long     lCnt=0;
   int      iCnt, iTmp, iRet=-1;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** SetFullPartialSale(): Missing input file: %s", pInfile);
      return iRet;
   }

   LogMsg("Set MultiSale Flag for %s", pInfile);

   // Open input file
   LogMsg("Open input file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening file: %s\n", pInfile);
      return iRet;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return iRet;
   }

   iCnt = 0;

   // Loop through all records
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

      iTmp = atoin(pInRec->PctXfer, SALE_SIZ_PCTXFER);
      if (iTmp > 0)
      {
         if (iTmp == 100)
            pInRec->SaleCode[0] = 'F';
         else
            pInRec->SaleCode[0] = 'P';
         pInRec->SaleCode[1] = ' ';
         iCnt++;
      }

      fputs(acInbuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Output last record
   fputs(acSavbuf, fdOut);

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   // Save input file
   if (iCnt > 0)
   {
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".ful");
      if (!_access(acInbuf, 0))
         iRet = DeleteFile(acInbuf);

      // Save input file
      iRet = MoveFile(pInfile, acInbuf);

      // Rename output file 
      iRet = MoveFile(acOutFile, pInfile);
   } else
   {
      LogMsg("*** Nothing is changed...");
      iRet = DeleteFile(acOutFile);
   }

   LogMsg("Total records processed:   %u", lCnt);
   LogMsg("Total records updated:     %u\n", iCnt);

   if (!iRet) 
      iRet = -1;
   else
      iRet = 0;

   return iRet;
}

/******************************************************************************
 *
 * Reset SaleCode & MultiSaleFlg.  Remove sales without DocDate
 *
 * Return number of records changed if success
 *
 ******************************************************************************/

int ResetSaleCode(char *pInfile, char *pOutfile)
{
   char      acInbuf[2048], *pRec;
   long     lCnt=0, lPrice;
   int      iCnt, iDrop=0, iRet = -1;
   FILE     *fdOut;
   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** ResetSaleCode(): Missing input file: %s", pInfile);
      return iRet;
   }

   LogMsg("Reset SaleCode Flag for %s", pInfile);

   // Open input file
   LogMsg("Open input file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening file: %s\n", pInfile);
      return iRet;
   }

   // Open output file
   LogMsg("Create output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", pOutfile);
      return iRet;
   }

   iCnt=0;
   // Loop through all records
   while (!feof(fdSale))
   {
      //if (!memcmp(pInRec->Apn, "00027005", 8))
      //   iTmp =0;
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

      if (pInRec->DocDate[0] == ' ')
      {
         iDrop++;
         continue;
      }

      lPrice = atol(pInRec->SalePrice);
      if (lPrice > 10)
      {
         pInRec->MultiSale_Flg = ' ';
         pInRec->SaleCode[0] = 'F';
         iCnt++;
      } else
      {
         pInRec->MultiSale_Flg = ' ';
         pInRec->SaleCode[0] = ' ';
      }

      fputs(acInbuf, fdOut);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:   %u", lCnt);
   LogMsg("              updated:     %u\n", iCnt);
   LogMsg("              dropped:     %u\n", iDrop);

   return iCnt;
}

/********************************* SC2SCExt **********************************
 *
 * Fix specific cases in sale file.  This will replace FixCumSale()
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int SC2SCExt(char *pInfile)
{
   char     acInbuf[2048], acOutFile[_MAX_PATH], *pRec;
   long     lCnt=0;
   FILE     *fdOut;

   SCSAL_EXT *pInRec  = (SCSAL_EXT *)&acInbuf[0];

   LogMsg0("Fix sale history file %s", pInfile);
   if (_access(pInfile, 0))
   {
      LogMsg("***** SC2SCExt(): Missing input file: %s", pInfile);
      return -1;
   }

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 2048, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0282630300", 9))
      //   iTmp = 0;
#endif

      if (strlen(pRec) == 511)
      {
         memset(&pInRec->NoneXfer_Flg, ' ', 512);
         pInRec->CRLF[0] = '\n';
         pInRec->CRLF[1] = 0;
      }
      fputs(acInbuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   // Save input file
   strcpy(acInbuf, pInfile);
   pRec = strrchr(acInbuf, '.');
   strcpy(pRec, ".org");
   if (!_access(acInbuf, 0))
      DeleteFile(acInbuf);

   // Save input file
   int iTmp = rename(pInfile, acInbuf);

   // Rename output file
   iTmp = rename(acOutFile, pInfile);

   LogMsgD("\nTotal processed records:     %u\n", lCnt);

   return iTmp;
}
