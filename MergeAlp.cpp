/***************************************************************************
 *
 * Notes:
 *    - Copy code from HUM 
 *
 * Revision
 * 09/18/2019 19.3.0   First release to production
 * 09/27/2019 19.3.1   Add -Xv option
 * 10/25/2019 19.4.2   Modify Alp_Load_Roll() to sort tax file on APN & Tax year
 *                     Call MB_Load_TaxBase() using group 3 (AGENCYCDCURRSEC_TR.TAB)
 * 12/11/2019 19.6.1   Modify LoadRoll() to sort roll file before processing.
 * 03/26/2020 19.8.0.1 Add MergeZoning.h and call MergeZoming() instead of MB_LoadZoning()
 * 04/28/2020 19.9.1   Allow using tax file Alp_Tax.csv if defined in INI file.
 * 07/17/2020 20.1.5   Modify Alp_MergeSitus() to work around bad data.
 * 07/28/2020 20.2.4   Modify Alp_ConvStdChar() to remove null from input record.
 *                     Modify Alp_MergeSitus() to handle special case of "VIEW" in suffix.
 *                     Clean up code in Alp_MergeLien3() & Alp_Load_LDR().  Modify Alp_CreateSCSale()
 *                     to update DocCode.  Add -Up option to merge previous APM.
 * 09/06/2020 20.3.1   Fix Alp_ConvertApnRoll() and re-populate PREV_APN
 * 10/31/2020 20.4.2   Modify Alp_MergeRoll() to populate default PQZoning.
 * 05/04/2021 20.8.6   Add Alp_ResetPVReason() for testing only.
 * 07/14/2021 21.0.8   Modify Alp_MergeStdChar() to populate QualityClass.
 * 08/26/2021 21.2.0   Default bUseConfSalePrice=false so we don't use confidential sale price.
 * 07/02/2024 24.0.0   Modify Alp_MergeLien3() to add ExeType.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "CharRec.h"
#include "PQ.h"
#include "Tax.h"
#include "doZip.h"
#include "MergeAlp.h"
#include "MB_Value.h"
#include "LoadValue.h"
#include "MergeZoning.h"

extern long lLotSqftCount;

/**************************** Alp_ConvStdChar ********************************
 *
 * Other counties has similar format: ALP, EDX, HUM, MAD, MNO, NAP, SBT, YOL
 * 
 *****************************************************************************/

int Alp_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0, iNoAsmt=0;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);

   // Sort input file on ASMT
   sprintf(acTmpFile, "%s\\%s\\%s_char.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort char file %s to %s", pInfile, acTmpFile);
   iRet = sortFile(pInfile, acTmpFile, "S(#23,C,A)");
   if (iRet < 500)
   {
      LogMsg("***** Input file is too small.");
      return 1;
   }

   if (!(fdIn = fopen(acTmpFile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Start loop
   while (!feof(fdIn))
   {
      // Get next record
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec || acBuf[0] > '9')
         break;

      replNull(acBuf);
      iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < ALP_CHAR_FLDS)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[ALP_CHAR_ASMT], strlen(apTokens[ALP_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[ALP_CHAR_FEEPARCEL], strlen(apTokens[ALP_CHAR_FEEPARCEL]));

      // Format APN
      if (*apTokens[ALP_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[ALP_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         iNoAsmt++;
         LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[ALP_CHAR_FEEPARCEL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[ALP_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[ALP_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - not avail
      //iTmp = blankRem(apTokens[ALP_CHAR_POOLSPA]);
      //if (iTmp > 1)
      //{
      //   pRec = findXlatCode(apTokens[ALP_CHAR_POOLSPA], &asPool[0]);
      //   if (pRec)
      //      myCharRec.Pool[0] = *pRec;
      //}

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "006122003000", 9) || !memcmp(myCharRec.Apn, "002430002000", 9))
      //   iRet = 0;
#endif

      // QualityClass 
      iTmp = remChar(apTokens[ALP_CHAR_QUALITYCLASS], ' ');    // Remove all blanks before process
      pRec = _strupr(apTokens[ALP_CHAR_QUALITYCLASS]);

      if (!memcmp(apTokens[ALP_CHAR_QUALITYCLASS], "000", 3))
         *apTokens[ALP_CHAR_QUALITYCLASS] = 0;
      else
         vmemcpy(myCharRec.QualityClass, apTokens[ALP_CHAR_QUALITYCLASS], SIZ_CHAR_QCLS, iTmp);

      if (*apTokens[ALP_CHAR_QUALITYCLASS] >= '0' && *apTokens[ALP_CHAR_QUALITYCLASS] <= 'Z')
      {
         acCode[0] = ' ';
         strcpy(acTmp, apTokens[ALP_CHAR_QUALITYCLASS]);
         if (isalpha(acTmp[0])) 
         {
            myCharRec.BldgClass = acTmp[0];

            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
         } else if (isdigit(acTmp[0]))
            iRet = Quality2Code(acTmp, acCode, NULL);
         else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[ALP_CHAR_QUALITYCLASS], apTokens[ALP_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (*apTokens[ALP_CHAR_QUALITYCLASS] > ' ' && *apTokens[ALP_CHAR_QUALITYCLASS] != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[ALP_CHAR_QUALITYCLASS], apTokens[ALP_CHAR_ASMT]);

      // YrBlt
      int iYrBlt = atoi(apTokens[ALP_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[ALP_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[ALP_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[ALP_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // Stories/NumFloors
      iTmp = atoi(apTokens[ALP_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 99)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[ALP_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[ALP_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport Sqft
      int iCarport = atoi(apTokens[ALP_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Parking spaces - no data
      iTmp = atoi(apTokens[ALP_CHAR_PARKINGSPACES]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.ParkSpace, acTmp, iRet);
      }

      // Patio SF
      iTmp = atoi(apTokens[ALP_CHAR_PATIOSF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[ALP_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[ALP_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[ALP_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[ALP_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[ALP_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[ALP_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[ALP_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[ALP_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[ALP_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace 
      if (*apTokens[ALP_CHAR_FIREPLACE] >= '0' && *apTokens[ALP_CHAR_FIREPLACE] <= '9')
      {
         pRec = findXlatCode(apTokens[ALP_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      } 

      // Haswell - not avail
      blankRem(apTokens[ALP_CHAR_HASWELL]);
      if (*(apTokens[ALP_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Lot Sqft
      if (iFldCnt >= ALP_CHAR_LOTSQFT)
      {
         ULONG    lSqft;
         lSqft = (ULONG)atol(apTokens[ALP_CHAR_LOTSQFT]);
         if (lSqft > 1)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(lSqft*SQFT_MF_1000);
            iTmp = sprintf(acTmp, "%u", (long)(dTmp+0.1));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);
   LogMsg("                  No ASMT  : %d\n", iNoAsmt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum, UnitSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/******************************** Alp_CleanName ******************************
 *
 * Return 99 if found a vesting
 *
 *****************************************************************************/

int Alp_CleanName(char *pSrcName, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], *pTmp;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);

   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

/******************************** Alp_MergeOwner *****************************
 *
 * Merge Name1 & Name2 only for swap name. Keep Name1 & Name2 separate as provided
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Alp_MergeOwnerx(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acTmp[64], acName1[64], acName2[64], acVesting1[8], acVesting2[8];
   char  *pTmp, *pName1, *pName2, acOwners[64];

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

   // Initialize
   //memcpy(acName1, pNames, CRESIZ_NAME_1);
   //myTrim((char *)&acName1[0], CRESIZ_NAME_1);
   strcpy(acOwners, acName1);
   pName1 = acName1;
   acVesting1[0] = 0;

   // Point to name2
   //memcpy(acName2, pNames+CRESIZ_NAME_1, CRESIZ_NAME_2);
   //myTrim((char *)&acName2[0], CRESIZ_NAME_2);
   pName2 = acName2;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0411000230", 10) || !memcmp(pOutbuf, "0470100269", 10) )
   //   iTmp = 0;
#endif

   // Check owner2 for # and %
   if (*pName2 == '%')
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (pTmp = strstr(acName2, "C/O"))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (!memcmp(acName2, "ATTN", 4))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (*pName2 == '&')
   {
      // Process as two names
      pName2 += 2;
   } else if (*pName2 > ' ')
   {
      // Ignore name2 if it is duplicate of name1 last and first names
      if (!strcmp(pName1, pName2))
      {
         *pName2 = 0;
      } else if (!strchr(pName2, ' '))
      {
         // Continuation of name1
         iTmp = sprintf(acOwners, "%s %s", pName1, pName2);
         blankRem(acOwners, iTmp);
         strcpy(acName1, acOwners);
         *pName2 = 0;
      }
   }

   // Check name2
   if (*pName2 > ' ')
   {
      remChar(pName2, '.');
      memcpy(pOutbuf+OFF_NAME2, pName2, strlen(pName2));
      // Find vesting in Name2 
      pTmp = findVesting(pName2, acVesting2);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_VEST, acVesting2, strlen(acVesting2));

         // Check EtAl
         if (!memcmp(acVesting2, "EA", 2))
            *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      } 
   }

   // Update Name1
   remChar(acName1, '.');
   iTmp = strlen(acName1);
   if (iTmp > SIZ_NAME1) iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);

   // Cleanup Name1 
   iTmp = Alp_CleanName(acName1, acTmp, acVesting1);
   if (iTmp == 99)
   {
      if (strchr(acTmp, ' '))
         strcpy(acOwners, acTmp);
      memcpy(pOutbuf+OFF_VEST, acVesting1, strlen(acVesting1));

      // Check Etal
      if (!memcmp(acVesting1, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   } 

   // Now parse owners
   splitOwner(acOwners, &myOwner, 3);
   if (acVesting1[0] > ' ' && isVestChk(acVesting1))
   {
      memcpy(myOwner.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
      myOwner.acSwapName[SIZ_NAME1] = 0;
   }
   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
}

void Alp_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64], acVesting[8], acOwner[SIZ_NAME1+2];
   OWNER myOwner;
   bool  bDecease = false;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);
   strcpy(acTmp1, pNames);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009172022", 9))
   //if (!memcmp(pOutbuf, "880000069", 9))
   //   iTmp = 0;
#endif

   // Replace comma with space
   if (pTmp = strrchr(acTmp1, ','))
      *pTmp = ' ';

   // Remove multiple spaces
   pTmp = acTmp1;
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = toupper(*pTmp);

      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '*' || *pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acName2[0] = 0;
   acSave1[0] = 0;
   acVesting[0] = 0;
   
   // If name ending with CR, it is a company
   // PT = Partnership
   // PL = Public parcel
   // SB = SBE parcel
 
   iTmp1 = iTmp -3;
   pTmp = (char *)&acTmp[iTmp1];
   if (!memcmp(pTmp, " CR", 3) || 
       !memcmp(pTmp, " PT", 3) ||
       !memcmp(pTmp, " PL", 3) ||
       !memcmp(pTmp, " SB", 3) )
   {
      *pTmp++ = 0;
      memcpy(pOutbuf+OFF_VEST, pTmp, 2);
      if (!memcmp(pTmp, "PL", 2) )
         *(pOutbuf+OFF_PUBL_FLG) = 'Y';

      iTmp1 = strlen(acTmp);
      if (iTmp1 > SIZ_NAME1) iTmp1 = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp1);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp1);

      return;
   } else if (!memcmp(pTmp, " ID", 3) )
   {  // Indicated decease
      memcpy(pOutbuf+OFF_VEST, pTmp+1, 2);
      *pTmp = 0;
      iTmp -= 3;
      bDecease = true;
   }

   // Check for vesting on last 6 letters
   pTmp = (char *)&acTmp[iTmp-6];
   iTmp1 = 0;
   while (asVestCode5[iTmp1][0])
   {
      if (!memcmp(pTmp, asVestCode5[iTmp1], 6))
      {
         memcpy(pOutbuf+OFF_VEST, asVestCode4[iTmp1], 4);
         *pTmp = 0;
         break;
      }
      iTmp1++;
   }
   if (*pTmp)
   {
      // Check for vesting on last 4 letters
      pTmp = (char *)&acTmp[iTmp-4];
      iTmp1 = 0;
      while (asVestCode4[iTmp1][0])
      {
         if (!memcmp(pTmp, asVestCode4[iTmp1], 4))
         {
            memcpy(pOutbuf+OFF_VEST, pTmp, 4);
            *pTmp = 0;
            break;
         }
         iTmp1++;
      }
   }
   if (*pTmp)
   {
      // Check for vesting on last 3 letters
      pTmp = (char *)&acTmp[iTmp-3];
      iTmp1 = 0;
      while (asVestCode3[iTmp1][0])
      {
         if (!memcmp(pTmp, asVestCode3[iTmp1], 3))
         {
            if (bDecease)
            {
               strcat(pTmp, "ID");
               memcpy(pOutbuf+OFF_VEST, pTmp+1, 4);
            } else
               memcpy(pOutbuf+OFF_VEST, pTmp+1, 2);
            *pTmp = 0;
            break;
         }
         iTmp1++;
      }
   }
   if (*pTmp)
   {
      // Find last word
      pTmp = strrchr(acTmp, ' ');
      if (pTmp)
      {
         // Prevent HWJTCP ...
         if (!memcmp(pTmp, " HWJT", 5))
         {
            memcpy(pOutbuf+OFF_VEST, pTmp+1, 4);
            *pTmp = 0;
         } else if (!memcmp(pTmp, " MMSE", 5))
         {
            memcpy(pOutbuf+OFF_VEST, pTmp+1, 4);
            *pTmp = 0;
         }
      }
   }
   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp1 = strlen(acTmp);
      if (iTmp1 > SIZ_NAME1) iTmp1 = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp1);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp1);
      return;
   }

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Save owner name
   strncpy(acOwner, acTmp, SIZ_NAME1);
   acOwner[SIZ_NAME1] = 0;

   // If TRUST appears after number, save from number forward
   if (acTmp[iTmp])
   {
      if (pTmp=strstr((char *)&acTmp[0], " PENSION & 401(K)") )
      {
         strcpy(acSave1, pTmp);
         *pTmp = 0;         
      } else if (strstr((char *)&acTmp[iTmp], " TRUST") )
      {
         iTmp--;
         strcpy(acSave1, (char *)&acTmp[iTmp]);
         acTmp[iTmp] = 0;
      }
   }

   // Filter out words
   if ((pTmp=strstr(acTmp, " ETAL")) || 
       (pTmp=strstr(acTmp, " ET AL")) || 
       (pTmp=strstr(acTmp, " & FBO")) ||
       (pTmp=strstr(acTmp, " TRUSTEE")) ||
       (pTmp=strstr(acTmp, " SUC CO TR")) ||
       (pTmp=strstr(acTmp, " SUC TR")) )
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " & TRUST "))
   {
      // Break into two name
      *pTmp = 0;
      strcpy(acName1, acTmp);

      //iTmp1 = strlen(pTmp+3);
      //if (iTmp1 > SIZ_NAME2) iTmp1 = SIZ_NAME2;
      //memcpy(pOutbuf+OFF_NAME2, pTmp+3, iTmp1);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // MARTIN PATSY L TRUST & WILDER EVERETT E & HEWITT B
      // Break names in two parts
      *pTmp = 0;        
      strcpy(acSave1, " TRUST");
      strcpy(acName1, acTmp);

      //iTmp1 = strlen(pTmp+9);
      //if (iTmp1 > SIZ_NAME2) iTmp1 = SIZ_NAME2;
      //memcpy(pOutbuf+OFF_NAME2, pTmp+9, iTmp1);
   }

   // Check for multiple '&' in name
   // If more than one "&", put second part into name2
   if (pTmp = strchr(acTmp, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         char *pTmp2;

         pTmp2 = strchr(pTmp1+2, ' ');
         if (pTmp2)
         {  // SCHWEIGERT STANLEY C & LAWLOR ANN S & GOODRICH J
            iTmp1 = strlen(pTmp2+1);
            if (iTmp1 == 1)
            {
               // Put 2nd and 3rd names into name2
               strcpy(acName2, pTmp+2);
               *pTmp = 0;
            } else
            {
               // Put 3rd names into name2
               strcpy(acName2, pTmp1+2);
               *pTmp1 = 0;
            }
         } else
         {
            // Put 2nd and 3rd names into name2
            strcpy(acName2, pTmp+2);
            *pTmp = 0;
         }
      }
   }
   
   if (pTmp=strstr(acTmp, " REVOCABLE FAMILY TRUST"))
   {
      strcpy(acSave1, " REVOCABLE FAMILY TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY REVOCABLE TRUST"))
   {
      strcpy(acSave1, " REVOCABLE LIVING TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE LIVING TRUST"))
   {
      strcpy(acSave1, " REVOCABLE LIVING TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY LIVING TRUST"))
   {
      strcpy(acSave1, " FAMILY LIVING TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY TRUST"))
   {  
      strcpy(acSave1, " FAMILY TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FRAMILY TRUST"))
   {  
      strcpy(acSave1, " FAMILY TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST"))
   {  
      strcpy(acSave1, " REV TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST"))
   {
      strcpy(acSave1, " LIVING TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REV TRUST"))
   {  
      strcpy(acSave1, " REV TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOC TRUST"))
   {  
      strcpy(acSave1, " REV TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " EXEMPTION TRUST"))
   {  
      strcpy(acSave1, " EXEMPTION TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LAND TRUST"))
   {  
      strcpy(acSave1, " LAND TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " PROPERTY TRUST"))
   {  
      strcpy(acSave1, " PROPERTY TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " SURVIVOR'S TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST UNDER"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST NO"))
   {
      strcpy(acSave1, " TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST DATE"))
   {
      strcpy(acSave1, " TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST TR"))
   {
      strcpy(acSave1, " TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST THE"))
   {  // Reverse the word order
      strcpy(acName1, "THE ");
      *(pTmp+6) = 0;
      strcat(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[strlen(acTmp)-6], " TRUST", 6))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      acTmp[strlen(acTmp)-6] = 0;
      strcpy(acName1, acTmp);
      strcpy(acSave1, " TRUST");
   } else if (pTmp=strstr(acTmp, " LLC LLC"))
   {  // Keep one only
      *pTmp = 0;
      strcpy(acName1, acTmp);
      strcat(acName1, pTmp+4);
   } else
      strcpy(acName1, acTmp);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(acTmp1, myOwner.acName1);
      //if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
      //{
      //   iTmp = strlen(myOwner.acName2);
      //   if (iTmp > SIZ_NAME2)
      //      iTmp = SIZ_NAME2;
      //   memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, iTmp);
      //}
      if (myOwner.acVest[0] > ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // Concat what in saved buffer if it is not " TRUST"
      if (acSave1[0])
      {
         if (acTmp1[strlen(acTmp1)-1] == ' ')
            strcat(acTmp1, (char *)&acSave1[1]);      // avoid double blank
         else
            strcat(acTmp1, acSave1);
      }

      // Save Name1
      //iTmp = strlen(acTmp1);
      //if (iTmp > SIZ_NAME1)
      //   iTmp = SIZ_NAME1;
      //memcpy(pOutbuf+OFF_NAME1, acTmp1, iTmp);

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
      {
         iTmp = strlen(acTmp1);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, iTmp);
      } else
      {
         iTmp = strlen(myOwner.acSwapName);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
      }
   } else
   {
      if (acSave1[0])
      {
         strcat(acName1, acSave1);
         acSave1[0] = 0;
         iTmp = strlen(acName1);
         if (iTmp > SIZ_NAME1)
            iTmp = SIZ_NAME1;
         //memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);
         memcpy(pOutbuf+OFF_NAME_SWAP, acName1, iTmp);
      } else
      {
         // Couldn't split names
         iTmp = strlen(pNames);
         if (iTmp > SIZ_NAME1)
            iTmp = SIZ_NAME1;
         //memcpy(pOutbuf+OFF_NAME1, pNames, iTmp);
         memcpy(pOutbuf+OFF_NAME_SWAP, pNames, iTmp);
         //acName2[0] = 0;
      }
   }

   /* Keep owner name as is - spn 06/02/2008
   // Process Name2 when there is more than one word and has not been populated
   if (acName2[0] && strchr((char *)&acName2[1], ' ') && myOwner.acName2[0] <= ' ')
   {
      if ((pTmp=strstr(acName2, " REVOC")) || (pTmp=strstr(acName2, " FAMILY "))
         || (pTmp=strstr(acName2, " LIVING TR")) )
      {
         iTmp = strlen(acName2);
         if (iTmp > SIZ_NAME2)
            iTmp = SIZ_NAME2;
         memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);
      } else
      {
         acSave1[0] = 0;
         if (pTmp=strstr(acName2, " TRUST"))
         {
            strcpy(acSave1, pTmp);
            *pTmp = 0;
         }

         iRet = splitOwner(acName2, &myOwner, 3);
         if (iRet >= 0)
            strcpy(acName2, myOwner.acName1);

         if (acSave1[0])
            strcat(acName2, acSave1);

         iTmp = strlen(acName2);
         if (iTmp > SIZ_NAME2)
            iTmp = SIZ_NAME2;
         memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);
      }
   }
   */
   iTmp = strlen(acOwner);
   if (acOwner[iTmp-1] == '&')
      iTmp -= 1;
   memcpy(pOutbuf+OFF_NAME1, acOwner, iTmp);
}

/******************************************************************************
 *
 * This function is needed to replace current MergeOwner() and properly assign CareOf
 * since CareOf is sometimes part of Owner Name.
 *
 ******************************************************************************/

void Alp_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[128], acName2[128], acVesting[8], acOwner[SIZ_NAME1+2];
   OWNER myOwner;
   bool  bDecease = false;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);
   acName2[0] = 0;
   acSave1[0] = 0;
   acVesting[0] = 0;

   strcpy(acName1, pNames);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "011101017000", 9))
   //   iTmp = 0;
#endif

   // Check CareOf
   strcpy(acTmp, _strupr(pCareOf));
   blankRem(acTmp);

   // If CareOf start with '&', it is part of Owner name, save it as Name2
   if (acTmp[0] == '&')
   {
      strcpy(acName2, &acTmp[2]);
      *pCareOf = 0;
   } else if (!memcmp(acTmp, "ET AL", 5))
   {
      strcat(acName1, " ");
      strcat(acName1, acTmp);
      *pCareOf = 0;
   }

   // Remove extra suffix
   if (pTmp = strstr(acName1, "LLC LLC"))
      *(pTmp+3) = 0;
   else if (pTmp = strstr(acName1, " LP LP"))
      *(pTmp+3) = 0;
   else if (pTmp = strstr(acName1, " TR TR"))
      *(pTmp+3) = 0;

   // Remove special chars
   remCharEx(acName1, ".,*");

   // Remove multiple spaces
   iTmp = blankRem(acName1);

  
   // If name ending with CR, it is a company
   // PT = Partnership
   // PL = Public parcel
   // SB = SBE parcel
 
   iTmp1 = iTmp -3;
   pTmp = (char *)&acName1[iTmp1];
   if (!memcmp(pTmp, " CR", 3) || 
       !memcmp(pTmp, " PT", 3) ||
       !memcmp(pTmp, " PL", 3) ||
       !memcmp(pTmp, " SB", 3) )
   {
      *pTmp++ = 0;
      memcpy(pOutbuf+OFF_VEST, pTmp, 2);
      if (!memcmp(pTmp, "PL", 2) )
         *(pOutbuf+OFF_PUBL_FLG) = 'Y';

      iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
      memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);
      return;
   } else if (!memcmp(pTmp, " ID", 3) )
   {  // Indicated decease
      memcpy(pOutbuf+OFF_VEST, pTmp+1, 2);
      *pTmp = 0;
      iTmp -= 3;
      bDecease = true;
   } else if (pTmp = strstr(acName1, " ID REC") )
   {  // Indicated decease
      memcpy(pOutbuf+OFF_VEST, pTmp+1, 2);
      *pTmp = 0;
      iTmp = strlen(acName1);
      bDecease = true;
   }
   
   // Check for vesting on last 6 letters
   pTmp = &acName1[iTmp-6];
   iTmp1 = 0;
   while (asVestCode5[iTmp1][0])
   {
      if (!memcmp(pTmp, asVestCode5[iTmp1], 6))
      {
         memcpy(pOutbuf+OFF_VEST, asVestCode4[iTmp1], 4);
         *pTmp = 0;
         break;
      }
      iTmp1++;
   }
   if (*pTmp)
   {
      // Check for vesting on last 4 letters
      pTmp = (char *)&acName1[iTmp-4];
      iTmp1 = 0;
      while (asVestCode4[iTmp1][0])
      {
         if (!memcmp(pTmp, asVestCode4[iTmp1], 4))
         {
            memcpy(pOutbuf+OFF_VEST, pTmp, 4);
            *pTmp = 0;
            break;
         }
         iTmp1++;
      }
   }
   if (*pTmp)
   {
      // Check for vesting on last 3 letters
      pTmp = (char *)&acName1[iTmp-3];
      iTmp1 = 0;
      while (asVestCode3[iTmp1][0])
      {
         if (!memcmp(pTmp, asVestCode3[iTmp1], 3))
         {
            if (bDecease)
            {
               strcat(pTmp, "ID");
               memcpy(pOutbuf+OFF_VEST, pTmp+1, 4);
            } else
               memcpy(pOutbuf+OFF_VEST, pTmp+1, 2);
            *pTmp = 0;
            break;
         }
         iTmp1++;
      }
   }
   if (*pTmp)
   {
      // Find last word
      pTmp = strrchr(acName1, ' ');
      if (pTmp)
      {
         // Prevent HWJTCP ...
         if (!memcmp(pTmp, " HWJT", 5))
         {
            memcpy(pOutbuf+OFF_VEST, pTmp+1, 4);
            *pTmp = 0;
         } else if (!memcmp(pTmp, " MMSE", 5))
         {
            memcpy(pOutbuf+OFF_VEST, pTmp+1, 4);
            *pTmp = 0;
         }
      }
   }
   // Check for year that goes before TRUST
   iTmp =0;
   while (acName1[iTmp])
   {
      if (isdigit(acName1[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
      memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);
      return;
   }

   // Update vesting
   updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Save owner name
   strncpy(acOwner, acName1, SIZ_NAME1);
   acOwner[SIZ_NAME1] = 0;
   strcpy(acTmp, acName1);

   // If TRUST appears after number, save from number forward
   if (acTmp[iTmp])
   {
      if (pTmp=strstr((char *)&acTmp[0], " PENSION & 401(K)") )
         *pTmp = 0;         
      else if (strstr((char *)&acTmp[iTmp], " TRUST") )
      {
         iTmp--;
         acTmp[iTmp] = 0;
      }
   }

   // Filter out words
   if ((pTmp=strstr(acTmp, " ETAL")) || 
       (pTmp=strstr(acTmp, " ET AL")) || 
       (pTmp=strstr(acTmp, " & FBO")) ||
       (pTmp=strstr(acTmp, " TRUSTEE")) ||
       (pTmp=strstr(acTmp, " SUC CO TR")) ||
       (pTmp=strstr(acTmp, " SUC TR")) )
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " & TRUST "))
   {
      // Break into two name
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // MARTIN PATSY L TRUST & WILDER EVERETT E & HEWITT B
      // Break names in two parts
      *pTmp = 0;        
      strcpy(acName1, acTmp);
   }

   // Check for multiple '&' in name
   // If more than one "&", put second part into name2
   if ((pTmp = strchr(acTmp, '&')) && strlen(acTmp) > SIZ_NAME1)
   {
      if (pTmp1 = strchr(pTmp+1, '&')) 
      {
         char *pTmp2;

         pTmp2 = strchr(pTmp1+2, ' ');
         if (pTmp2)
         {  // SCHWEIGERT STANLEY C & LAWLOR ANN S & GOODRICH J
            iTmp1 = strlen(pTmp2+1);
            if (iTmp1 == 1)
            {
               // Put 2nd and 3rd names into name2
               strcpy(acName2, pTmp+2);
               *pTmp = 0;
            } else
            {
               // Put 3rd names into name2
               strcpy(acName2, pTmp1+2);
               *pTmp1 = 0;
            }
         } else
         {
            // Put 2nd and 3rd names into name2
            strcpy(acName2, pTmp+2);
            *pTmp = 0;
         }
      }
   }
   
   if (pTmp=strstr(acTmp, " REVOCABLE FAMILY TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY REVOCABLE TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE LIVING TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY LIVING TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FRAMILY TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REV TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOC TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " EXEMPTION TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LAND TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " PROPERTY TRUST"))
   {  
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " SURVIVOR'S TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST UNDER"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST NO"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST DATE"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST TR"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST THE"))
   {  // Reverse the word order
      strcpy(acName1, "THE ");
      *(pTmp+6) = 0;
      strcat(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[strlen(acTmp)-6], " TRUST", 6))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      acTmp[strlen(acTmp)-6] = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LLC LLC"))
   {  // Keep one only
      *(pTmp+4) = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(acTmp1, myOwner.acName1);
      if (myOwner.acVest[0] > ' ' && *(pOutbuf+OFF_VEST) == ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
         iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
      else
         iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
      iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);

   iTmp = strlen(acOwner);
   if (acOwner[iTmp-1] == '&')
      iTmp -= 1;
   memcpy(pOutbuf+OFF_NAME1, acOwner, iTmp);

   if (acName2[0] > ' ')
      iTmp = vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);
}

/******************************** Alp_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Alp_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4, bool bAdrOnly=false)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *pCareOf, *pDba, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64], acDba[128];
   int   iTmp;

#ifdef _DEBUG
   // (!memcmp(pOutbuf, "001011009000", 9))
   //   iTmp = 0;
#endif

   // Initialize
   if (!bAdrOnly)
      removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   pCareOf = pDba = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         pCareOf = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         pCareOf = pLine2;
         p1 = pLine3;
         if (!_memicmp(pLine1, "DBA ", 4) )
            pDba = pLine1+4;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         p1 = pLine3;
         sprintf(acDba, "%s %s", pLine1+4, pLine2);
         pDba = &acDba[0];
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         pCareOf = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         pDba = pLine1+4;
         p1 = pLine2;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
   }

   // Update DBA
   if (pDba && !bAdrOnly)
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

   // Check for C/O
   if (pCareOf && !bAdrOnly)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   strcpy(acAddr2, p2);
   quoteRem(acAddr2);
   remChar(acAddr2, ',');
   iTmp = blankRem(acAddr2);
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D, iTmp);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}

void Alp_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004034005000", 9))
   //   iTmp = 0;
#endif

   // CareOf - can be part of owner name - 222153003000
   _strupr(apTokens[MB_ROLL_M_ADDR1]);
   _strupr(apTokens[MB_ROLL_M_ADDR2]);
   iTmp = blankRem(_strupr(apTokens[MB_ROLL_CAREOF]));
   pTmp = NULL;
   if (iTmp > 6)
   {
      if (!memcmp(apTokens[MB_ROLL_CAREOF], "C/O", 3) || !memcmp(apTokens[MB_ROLL_CAREOF], "C/0", 3) )
      {
         *(apTokens[MB_ROLL_CAREOF]+2) = 'O';
         pTmp = apTokens[MB_ROLL_CAREOF];
      } else if (!memcmp(apTokens[MB_ROLL_CAREOF], "ATTN", 4) ||
         !memcmp(apTokens[MB_ROLL_CAREOF], "ATT:", 4) ||
         !memcmp(apTokens[MB_ROLL_CAREOF], "ATT ", 4) )
         pTmp = apTokens[MB_ROLL_CAREOF];
   }
   if (!pTmp && !memcmp(apTokens[MB_ROLL_M_ADDR1], "C/O", 3))
      pTmp = apTokens[MB_ROLL_M_ADDR1];
   else if (!pTmp && !memcmp(apTokens[MB_ROLL_M_ADDR2], "C/O", 3))
      pTmp = apTokens[MB_ROLL_M_ADDR2];
   if (pTmp)
      updateCareOf(pOutbuf, pTmp, strlen(pTmp));

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   pTmp = NULL;
   if (!memcmp(apTokens[MB_ROLL_DBA], "DBA", 3) )
      pTmp = apTokens[MB_ROLL_DBA] + 4;
   else if (!memcmp(apTokens[MB_ROLL_M_ADDR1], "DBA", 3))
      pTmp = apTokens[MB_ROLL_M_ADDR1] + 4;
   else if (!memcmp(apTokens[MB_ROLL_M_ADDR2], "DBA", 3))
      pTmp = apTokens[MB_ROLL_M_ADDR2] + 4;

   if (pTmp)
   {
      if (*pTmp == ' ') pTmp++;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   }

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   quoteRem(acAddr1);
   blankRem(acAddr1);
   if (acAddr1[0] <= ' ')
   {
      // Mailaddr is not populated here, try unformatted version
      Alp_MergeMAdr(pOutbuf, apTokens[MB_ROLL_M_ADDR1], apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3], apTokens[MB_ROLL_M_ADDR4], true);
      return;
   }

   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      remChar(apTokens[MB_ROLL_M_CITY], ',');
      quoteRem(apTokens[MB_ROLL_M_CITY]);
      blankRem(apTokens[MB_ROLL_M_CITY]);

      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], SIZ_M_CITY);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      int iZip, iZip4=0;
      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {

         iZip = atoin(apTokens[MB_ROLL_M_ZIP], 5);
         iTmp = strlen(apTokens[MB_ROLL_M_ZIP]);
         if (iTmp == 9)
            iZip4 = atoin(apTokens[MB_ROLL_M_ZIP]+5, 4);

         sprintf(acTmp, "%.5d", iZip);
         memcpy(pOutbuf+OFF_M_ZIP, acTmp, 5);
         if (iZip4 > 0)
         {
            sprintf(acTmp, "%.4d", iZip4);
            memcpy(pOutbuf+OFF_M_ZIP4, acTmp, 4);
         }
      }

      if (iZip > 0)
      {
         if (iZip4 > 0)
            iTmp = sprintf(acTmp, "%s %s %.5d-%.4d", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], iZip, iZip4);
         else
            iTmp = sprintf(acTmp, "%s %s %.5d", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], iZip);
      } else
         iTmp = sprintf(acTmp, "%s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST]);

      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
   }

}

/******************************** Alp_MergeSitus *****************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Alp_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acStrName[32], acStrSfx[32], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do
      {
         pRec = fgets(acRec, 512, fdSitus);
      } while (!isdigit(acRec[1]));
   }

   pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Remove null char
   replNull(acRec);

   // Parse situs input
   if (cDelim == ',')
      iTmp = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iTmp = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

   if (iTmp < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old Situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      sprintf(acTmp, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
      pTmp = strchr(acTmp, ' ');
      *(pTmp+1) = 0;
      strcpy(acAddr1, acTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, SIZ_S_STR_SUB);

      if (*apTokens[MB_SITUS_STRDIR] > ' ' && isDir(apTokens[MB_SITUS_STRDIR]))
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         vmemcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], SIZ_S_DIR);
      }
   }

   if (!strcmp(apTokens[MB_SITUS_STRTYPE], "VIEW"))
   {
      sprintf(acStrName, "%s VIEW", apTokens[MB_SITUS_STRNAME]);
      acStrSfx[0] = 0;
   } else
   {
      strcpy(acStrName, apTokens[MB_SITUS_STRNAME]);
      strcpy(acStrSfx, apTokens[MB_SITUS_STRTYPE]);
   }

   if (acStrSfx[0] > ' ')
   {
      // Fix specific spelling - 07/21/2016
      if (!strcmp(apTokens[MB_SITUS_STRTYPE], "PNT"))
         strcpy(acStrSfx, "PT");
      else if (!strcmp(apTokens[MB_SITUS_STRTYPE], "PLA"))
         strcpy(acStrSfx, "PL");
      else if (!strcmp(apTokens[MB_SITUS_STRTYPE], "PR"))
         strcpy(acStrSfx, "PKWY");

      strcat(acAddr1, acStrName);
      strcat(acAddr1, " ");
      strcat(acAddr1, acStrSfx);
      vmemcpy(pOutbuf+OFF_S_STREET, acStrName, SIZ_S_STREET);

      // Encode suffix
      iTmp = GetSfxCodeX(acStrSfx, acTmp);
      if (iTmp > 0)
      {
         sprintf(acCode, "%d", iTmp);
      } else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         iBadSuffix++;
         acCode[0] = 0;
      }
      vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, acStrName);
      if (sAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      if (sAdr.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, SIZ_S_SUFF);

      strcat(acAddr1, acStrName);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      memcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], strlen(apTokens[MB_SITUS_UNIT]));
   }

   iTmp = blankRem(acAddr1, SIZ_S_ADDR_D);
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D, iTmp);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1, pOutbuf);   
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
         sprintf(acTmp, "%s CA      ", myTrim(acAddr1));
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/******************************* Alp_MergeChar *******************************
 *
 * Merge Alp_Char.dat in STDCHAR format
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the one that has value.
 *
 *****************************************************************************/

int Alp_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lSqft;
   int      iLoop, iBeds, iFBath, iHBath, iBldgNum, iRooms, iUnits;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   while (!iLoop)
   {
      // Quality Class
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
      *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
      memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

      // YrBlt
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

      // YrEff
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

      // BldgSqft
      lSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      } else
         memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

      // Garage Sqft
      lSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
      } else
      {
         memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = ' ';
      }

      // LotSqft
      lSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lSqft);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
         
         lSqft = atoin(pChar->LotAcre, SIZ_CHAR_SQFT);
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lSqft);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      } else
      {
         // Don't overwrite values from roll file
         //memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
         //memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      }

      // PatioSqft
      lSqft = atoin(pChar->PatioSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_PATIO_SF, lSqft);
         memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
      } else
         memset(pOutbuf+OFF_PATIO_SF, ' ', SIZ_BLDG_SF);

      // Heating
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   
      // Cooling 
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

      // Total Rooms
      iRooms = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
      if (iRooms > 0)
      {
         sprintf(acTmp, "%*d", SIZ_ROOMS, iRooms);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      } else
         memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

      // Beds
      iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      } else
         memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

      // Bath
      iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      } else
         memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

      // Half bath
      iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
      if (iHBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      } else
         memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

      // Fireplace
      *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

      // HasSeptic or HasSewer
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

      // HasWell
      *(pOutbuf+OFF_WATER) = pChar->HasWater;

      // Pools
      *(pOutbuf+OFF_POOL) = pChar->Pool[0];

      // Units count
      iUnits = atoin(pChar->Units, SIZ_CHAR_UNITS);
      if (iUnits > 0)
      {
         sprintf(acTmp, "%*d", SIZ_UNITS, iUnits);
         memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
      } else
         memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

      // Stories
      if (pChar->Stories[0] > ' ')
         memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
      else
         memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

      // BldgSeqNum
      iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);

      lCharMatch++;

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (!pRec)
         break;
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (!iLoop && (iBeds > 0 && iBldgNum > 0))
         break;
   }

   return 0;
}

/******************************** Alp_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************

int Alp_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      iTmp = memcmp(pOutbuf, pTmp+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   iRet = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Alp_MergeTax ******************************
 *
 * Note:
 *
 *****************************************************************************

int Alp_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double   dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iRet;

   // Merge tax data
   while (!iTmp)
   {
      iTmp = ParseStringNQ(pRec, ',', MB_TAX_PAIDDATE2+1, apTokens);
      if (iTmp < MB_TAX_PAIDDATE2)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
         return -1;
      }

      // ALP has no tax year.  keep updating
      dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
      dTax1 = atof(acTmp);
      dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
      dTax2 = atof(acTmp);
      dTmp = dTax1+dTax2;
      if (dTax1 == 0.0 || dTax2 == 0.0)
         dTmp *= 2;

      if (dTmp > 0.0)
      {
         sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
         memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/********************************* Alp_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Alp_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse input
   //iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (cDelim == ',')
      iTokens = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iTokens = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);

   if (iTokens < MB_ROLL_M_ADDR4)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[iApnFld], iTokens);
      return -1;
   }

   // Ignore APN starts with 800-999 except 910, 911 or 914 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910 && iTmp != 911 && iTmp != 914))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "02ALP", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   } else
   {
      // Reformat APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
   }

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "001010014", 9))
   //   iTmp = 0;
#endif

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   _strupr(apTokens[MB_ROLL_LEGAL]);
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   if (acTmp[0] > ' ')
   {
      acTmp[SIZ_USE_CO] = 0;
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Megabyte:
   // Standard UseCode
   //if (acTmp[0] > ' ')
   //{
   //   _strupr(acTmp);
   //   iTmp = strlen(acTmp);
   //   memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
   //   if (iNumUseCodes > 0)
   //   {
   //      pTmp = Cnty2StdUse(acTmp);
   //      if (pTmp)
   //         memcpy(pOutbuf+OFF_USE_STD, pTmp, 3);
   //      else
   //      {
   //         memcpy(pOutbuf+OFF_USE_STD, USE_MISC, 3);
   //         logUsecode(acTmp, pOutbuf);
   //      }
   //   }
   //}

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      lLotSqftCount++;

      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
   memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
   pTmp = apTokens[MB_ROLL_DOCNUM];
   //if ((memcmp(pTmp+8, apTokens[MB_ROLL_DOCDATE], 2) || memcmp(pTmp+10, apTokens[MB_ROLL_DOCDATE]+3, 2))
   //   && *(pTmp+4) == 'R')
   if (*(pTmp+4) == 'R')
   {
      remChar(apTokens[MB_ROLL_DOCNUM], ' ');
      sprintf(acTmp, "%.5s%.7d", apTokens[MB_ROLL_DOCNUM], atol(apTokens[MB_ROLL_DOCNUM]+5));

      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp1, MM_DD_YYYY_1);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp1, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, SIZ_TRANSFER_DOC);
      }
   }

   // Owner
   try {
      Alp_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER], apTokens[MB_ROLL_CAREOF]);
   } catch(...) {
      LogMsg("***** Exeception occured in Alp_MergeOwner()");
   }

   // Mailing
   try {
      Alp_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Alp_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   // Clear all sales - Temoporary use
   //ClearOldSale(pOutbuf);

   return 0;
}

/********************************* Alp_Load_Roll ******************************
 *
 *
 ******************************************************************************/

int Alp_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Sort input file
   sprintf(acRollRec, "%s\\ALP\\ALP_Roll.srt", acTmpPath);
   iTmp = sortFile(acRollFile, acRollRec, "S(#1,C,A) F(TXT)");

   // Open roll file
   LogMsg("Open Roll file %s", acRollRec);
   fdRoll = fopen(acRollRec, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollRec);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }
 
   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Open Tax file
   sprintf(acRec, "%s\\ALP\\ALP_Tax.srt", acTmpPath);
   iRet = sortFile(acTaxFile, acRec, "S(#1,C,A,#2,C,D) DEL(124) F(TXT)", &iTmp);
   LogMsg("Open Tax file %s", acRec);
   fdTax = fopen(acRec, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acRec);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Skip header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
   //if (!memcmp(acRollRec, "001010003", 9))
   //   iTmp = 0;
#endif

      iTmp = replNull(acRollRec);
      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Alp_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         if (!iRet)
         {
            // Remove old sale data
            //if (bClearSales)
            //   ClearOldSale(acBuf);

            // Merge Situs
            if (fdSitus)
               lRet = Alp_MergeSitus(acBuf);

             // Merge Char
            if (fdChar)
               lRet = Alp_MergeStdChar(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acBuf, 0);        // for new Alp_Tax.txt

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Alp_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Alp_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Alp_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acRec, 0);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      replNull(acRollRec);

      // Create new R01 record
      iRet = Alp_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Alp_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Alp_MergeStdChar(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, 0);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u\n", lRecCnt);
   return 0;
}

/********************************* Alp_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Alp_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_ISAGPRESERVE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // APN
   iRet = strlen(apTokens[L3_ASMT]);
   if (iRet != iApnLen)
   {
      LogMsg("***** Invalid APN=%s.  Ignore this parcel.", apTokens[L3_ASMT]);
      return -1;
   }
   memcpy(pOutbuf, apTokens[L3_ASMT], iRet);

   // Format APN
   memcpy(pOutbuf+OFF_APN_D, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "02ALP", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   vmemcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], SIZ_TRA);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&ALP_Exemption);
   
   // Legal
   remChar(apTokens[L3_PARCELDESCRIPTION], '"');
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   remChar(apTokens[L3_OWNER], '"');
   Alp_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs - Use Alp_Situs.csv

   // Mailing
   remChar(apTokens[L3_MAILADDRESS1], '"');
   remChar(apTokens[L3_MAILADDRESS2], '"');
   remChar(apTokens[L3_MAILADDRESS3], '"');
   Alp_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*apTokens[L3_CURRENTDOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp && isNumber(apTokens[L3_CURRENTDOCNUM]+5))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // Parking space
   if (*apTokens[L3_GARAGE] > ' ' && isdigit(*apTokens[L3_GARAGE]))
      *(pOutbuf+OFF_PARK_SPACE) = *apTokens[L3_GARAGE];

   return 0;
}

/******************************** Alp_Load_LDR ******************************
 *
 * 2020 LDR
 *
 ****************************************************************************/

int Alp_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) OMIT(#1,C,GT,\"3\") DEL(9) F(TXT)");  // 2020
   if (!iRet) return -1;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) DEL(124) F(TXT)");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      // Create new R01 record
      iRet = Alp_MergeLien3(acBuf, acRec);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Alp_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Alp_MergeStdChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/*************************** Alo_ConvertApnRoll *****************************
 *
 * Convert APN format from Cres to MB.  Keep old APN in PREV_APN.
 *
 ****************************************************************************/

int Alp_ConvertApnRoll(char *pInfile, char *pOutfile, int iRecordLen)
{
   FILE *fdIn, *fdOut;
   int   iTmp, iCnt=0;
   char  acBuf[2048], acTmp[32], acApn[32];

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "rb");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "wb");

   fread(acBuf, 1, iRecordLen, fdIn);
   fwrite(acBuf, 1, iRecordLen, fdOut);

   while (!feof(fdIn))
   {
      iTmp = fread(acBuf, 1, iRecordLen, fdIn);
      if (iTmp < iRecordLen)
         break;

      if (acBuf[0] == '0')
      {
         // Format new APN 
         if (acBuf[9] == '0')
            iTmp = sprintf(acApn, "%.9s000", acBuf);
         else 
            iTmp = sprintf(acApn, "%.9s5%c0", acBuf, acBuf[9]);
      } else if (acBuf[0] == '3')
         iTmp = sprintf(acApn, "91000000%c000", acBuf[8]);
      else
      {
         LogMsg("Drop old parcel: %.12s", acBuf);
         continue;
      }

      // Save old APN to previous APN
      memcpy(&acBuf[OFF_PREV_APN], &acBuf[0], 10);

      // Update new APN
      memcpy(acBuf, acApn, iTmp);

      // Format APN_D
      iTmp = formatApn(acApn, acTmp, &myCounty);
      memcpy(&acBuf[OFF_APN_D], acTmp, iTmp);

      fwrite(acBuf, 1, iRecordLen, fdOut);
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert Roll APN completed with %d records", iCnt);

   return 0;
}

int Alp_ConvertApn(char *pInfile, char *pOutfile)
{
   FILE *fdIn, *fdOut;
   int   iTmp, iCnt=0, iDrop=0;
   char  acBuf[2048], acApn[32], *pBuf;

   LogMsg("Convert APN file");
   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");

   while (!feof(fdIn))
   {
      pBuf = fgets(acBuf, 2048, fdIn);
      if (!pBuf)
         break;

//#ifdef _DEBUG
//      if (!memcmp(pSale->Apn, "027431011", 9))
//         iTmp1 = 0;
//#endif

      // Format new APN
      if (acBuf[0] == '0')
      {
         // Format new APN 
         if (acBuf[9] == '0')
            iTmp = sprintf(acApn, "%.9s000", acBuf);
         else 
            iTmp = sprintf(acApn, "%.9s5%c0", acBuf, acBuf[9]);
      } else if (acBuf[0] == '3')
         iTmp = sprintf(acApn, "91000000%c000", acBuf[8]);
      else
      {
         LogMsg("Drop old parcel: %.12s", acBuf);
         continue;
      }

      memcpy(acBuf, acApn, iTmp);

      fputs(acBuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Number of records converted %d", iCnt);
   LogMsg("                  dropped   %d", iDrop);

   return 0;
}

int Alp_ConvertSaleApn(char *pInfile, char *pOutfile)
{
   FILE *fdIn, *fdOut;
   int   iTmp, iCnt=0, iDrop=0;
   char  acBuf[2048], acApn[32], *pBuf;
   SCSAL_REC *pSaleRec = (SCSAL_REC *)&acBuf;

   LogMsg("Convert APN file");
   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");

   while (!feof(fdIn))
   {
      pBuf = fgets(acBuf, 2048, fdIn);
      if (!pBuf)
         break;

//#ifdef _DEBUG
//      if (!memcmp(pSale->Apn, "027431011", 9))
//         iTmp1 = 0;
//#endif

      // Format new APN
      if (acBuf[0] == '0')
      {
         // Format new APN 
         if (acBuf[9] == '0')
            iTmp = sprintf(acApn, "%.9s000", acBuf);
         else 
            iTmp = sprintf(acApn, "%.9s5%c0", acBuf, acBuf[9]);
      } else if (acBuf[0] == '3')
         iTmp = sprintf(acApn, "91000000%c000", acBuf[8]);
      else
      {
         LogMsg("Drop old parcel: %.12s", acBuf);
         continue;
      }
      memcpy(acBuf, acApn, iTmp);

      // Filter out bad sale
      iTmp = atol(pSaleRec->SalePrice);
      if (iTmp > 100)
      {
         pSaleRec->DocType[0] = '1';
         fputs(acBuf, fdOut);
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Number of records converted %d", iCnt);
   LogMsg("                  dropped   %d", iDrop);

   return 0;
}

/******************************* Alp_CreateSCSale ***************************
 *
 * Copy from MB_CreateSCSale() and customize for ALP.
 *
 ****************************************************************************/

int Alp_CreateSCSale(int iDateFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acDocNum[32], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp;

   LogMsg0("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Replace null char with space
      replNull(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[MB_SALES_ASMT], "006080004000", 9))
      //   iTmp = 0;
#endif

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Ignore DocNum with wrong DocYear
      if (memcmp(apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_DOCDATE]+6, 4))
         continue;

      // Replace blank with '0'
      if ((pTmp = strchr(apTokens[MB_SALES_DOCNUM], ' ')) && *(pTmp+1) > '0')
         *pTmp = '0';

      // Remove blank in DocNum
      remChar(apTokens[MB_SALES_DOCNUM], ' ');
      sprintf(acDocNum, "%.5s%.7d", apTokens[MB_SALES_DOCNUM], atol(apTokens[MB_SALES_DOCNUM]+5));

      // OR last four digit of DocNum=DocDate
      if (!memcmp(&acDocNum[8], apTokens[MB_SALES_DOCDATE], 2) && !memcmp(&acDocNum[10], apTokens[MB_SALES_DOCDATE]+3, 2))
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

      // Docnum
      memcpy(SaleRec.DocNum, acDocNum, 12);

      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
      {
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1' || *apTokens[MB_SALES_GROUPSALE] == 'T')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001130011000", 9))
      //   iTmp = 0;
#endif
      // Confirmed sale price
      dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
      if (acTmp[0] > '0')
      {
         lPrice = atol(acTmp);
         if (lPrice < 1000)
         {
            // This doesn't look normal, but keep it for reference only
            iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            lPrice = 0;
         } else
            iTmp = sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      } 

      // Do not use confirmed sale price
      if (!bUseConfSalePrice)
         lPrice = 0;

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001160016000", 9))
      //   iTmp = 0;
#endif
      // Tax
      dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
      if (acTmp[0] > '0')
      {
         dTmp = atof(acTmp);

         // Save DocTax
         iTmp = sprintf(acTmp, "%.2f", dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);

         // Calculate sale price
         lTmp = (long)(dTmp * SALE_FACTOR);
         iTmp = ((int)dTmp/100)*100;

         // Check for bad DocTax
         if (dTmp > 100000)
         {
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else if (lPrice > 100000 && lPrice == (lPrice/100)*100)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTmp);
            } else
            {
               LogMsg("??? (1) Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f, CP=%d.  Need investigation.  Ignore price.", SaleRec.Apn, SaleRec.DocNum, dTmp, lPrice);
               lPrice = 0;
            }
         } else if (lPrice > 0 && (long)dTmp >= lPrice+5000)
         {
            // Even though sale tax is questionable, we still rely on it if it is reasonable
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** (2) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               if (lPrice == (lPrice/100)*100)
                  LogMsg("*** (2) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTmp);
               else
               {
                  LogMsg("??? (2) Questionable Sale Tax Amt for %.12s: DocNum=%.12s, Price=%d, \ttax=%.2f).  Need investigation.", SaleRec.Apn, SaleRec.DocNum, lPrice, dTmp);
                  lPrice = lTmp;
               }
            }
         } else if (iTmp == (int)dTmp && iTmp > 10000)
         {
            if (lTmp != (lTmp/100)*100)
            {
               LogMsg("*** (3) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
               lPrice = lTmp;
         } else if (lTmp == (lTmp/100)*100)
            lPrice = lTmp;
         else if (lTmp > 1000 && lPrice == 0)
            lPrice = lTmp;

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Ignore sale price.", 
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE]);
               lPrice = 0;
            }
         }  
      } 

      // Ignore sale price if less than 1000
      if (lPrice >= 10000)
         sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
      else if (lPrice >= 1000)
         sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
      else
         memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
      memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);

      // Doc code - accept following code only
      SaleRec.NoneSale_Flg = 'Y';
      if (pDocTbl)
      {
         iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
         if (iTmp >= 0)
         {
            memcpy(SaleRec.DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
            if (lPrice < 1000)
               SaleRec.NoneSale_Flg = pDocTbl[iTmp].flag;
         } else if (bDebug)
            LogMsg("*** Unknown DocCode: %s", apTokens[MB_SALES_DOCCODE]);
      } else if (lPrice > 1000)
      {
         if (!memcmp(apTokens[MB_SALES_DOCCODE], "TT", 2) || 
             !memcmp(apTokens[MB_SALES_DOCCODE], "TF", 2) ||
             !memcmp(apTokens[MB_SALES_DOCCODE], "TN", 2) ||
             !memcmp(apTokens[MB_SALES_DOCCODE], "TJ", 2) ||
             !memcmp(apTokens[MB_SALES_DOCCODE], "FV", 2) ||
             !memcmp(apTokens[MB_SALES_DOCCODE], "TM", 2))
         {
            SaleRec.DocType[0] = '1';
            SaleRec.NoneSale_Flg = 'N';
            SaleRec.SaleCode[0] = 'F';
         } else if (!memcmp(apTokens[MB_SALES_DOCCODE], "TA", 2) )
         {
            memcpy(SaleRec.DocType, "61", 2);
         } else if (!memcmp(apTokens[MB_SALES_DOCCODE], "LL", 2) )
         {
            SaleRec.DocType[0] = '1';
            SaleRec.NoneSale_Flg = 'N';
            SaleRec.SaleCode[0] = 'N';       // Less lien
         } else
            LogMsg("*** Unknown DocCode %s [%s]", apTokens[MB_SALES_DOCCODE], apTokens[MB_SALES_ASMT]);
      }

      // Save original DocCode
      vmemcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);
      
      // Transfer Type
      if (*apTokens[MB_SALES_XFERTYPE] > ' ' && lPrice > 1000)
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         strcpy(acTmp, apTokens[MB_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Seller1, acTmp, SALE_SIZ_SELLER, iTmp);

         // Buyer
         strcpy(acTmp, apTokens[MB_SALES_BUYER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Name1, acTmp, SALE_SIZ_BUYER, iTmp);

         SaleRec.ARCode  = 'A';
         SaleRec.CRLF[0] = 10;
         SaleRec.CRLF[1] = 0;
         fputs((char *)&SaleRec,fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(20,1,C,EQ,\" \",OR,31,1,C,EQ,\" \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -2;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            // Save old cumsale file
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);
            iTmp = rename(acCSalFile, acTmpFile);
   
            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -2;
      } else
         iTmp = rename(acOutFile, acCSalFile);

      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acCSalFile, acTmpFile);
      }
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp)
   {
      iErrorCnt++;
      if (iTmp == -2)
         LogMsg("***** Error sorting output file");
      else
         LogMsg("***** Error renaming to %s", acCSalFile);
   }

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                          output: %d.", lTmp);
   LogMsg("         Latetest recording date: %d.", lLastRecDate);
   return iTmp;
}

/**************************** Alp_CreateLienRec *****************************
 *
 * Format lien extract record.  This function may not work with all counties.
 * Check lien file layout first before use.
 *
 ****************************************************************************/

int Alp_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   replStrAll(pRollRec, "|NULL", "|");
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_USERID)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   iRet = strlen(apTokens[L_ASMT]);
   if (iRet < iApnLen)
   {
      memcpy(pLienExtr->acApn, "0000", iApnLen - iRet);
      memcpy(&pLienExtr->acApn[iApnLen - iRet], apTokens[L_ASMT], iRet);
   } else
      memcpy(pLienExtr->acApn, apTokens[L_ASMT], iRet);

   // TRA
   lTmp = atol(apTokens[L_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LIEN_FIXT);
   }

   // Improve
   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_LIEN_FIXT);
   }

   // Other values
   long lGrow = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);

   // Fixtures
   long lFixt   = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);

   // Business Property
   long lBP   = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);

   // Personal Property
   long lPP   = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lBP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lBP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lBP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  
   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Prop 8 - correcting by spn 01/16/2010
   lTmp = atoin(apTokens[L_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Taxability
   if (lTmp > 0)
   {
      iRet = strlen(apTokens[L_TAXABILITY]);
      if (iRet > SIZ_LIEN_TAXCODE)
         iRet = SIZ_LIEN_TAXCODE;
      memcpy(pLienExtr->acTaxCode, apTokens[L_TAXABILITY], iRet);
   }

   // Save Exe code
   memcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
   memcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
   memcpy(pLienExtr->extra.MB.ExeCode3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************* Alp_ExtrLien *******************************
 *
 * Extract lien data from Alp_yyyy.txt
 *
 ****************************************************************************/

int Alp_ExtrLien(LPCSTR pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg("\nExtract lien roll for %s", pCnty);

   // Open lien file
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acRollFile);
      return 2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Skip header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (!isdigit(acRec[3]))
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new record
      iRet = Alp_CreateLienRec(acBuf, acRec);
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (strlen(acRec) < 20)
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Alp_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Alp_MergeLien3(char *pOutbuf, char *pRollRec, bool bRemHyphen)
{
   char     acApn[32], acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Remove hyphen from APN
   if (bRemHyphen)
   {
      remChar(apTokens[L3_ASMT], '-');
      remChar(apTokens[L3_FEEPARCEL], '-');
   }

   // Copy APN
   iTmp = iApnLen-strlen(apTokens[L3_ASMT]);
   sprintf(acApn, "%.*s%s", iTmp, "000", apTokens[L3_ASMT]);
   memcpy(pOutbuf, acApn, iApnLen);

   // Copy ALT_APN
   iTmp = iApnLen-strlen(apTokens[L3_FEEPARCEL]);
   sprintf(acTmp, "%.*s%s", iTmp, "000", apTokens[L3_FEEPARCEL]);
   memcpy(pOutbuf+OFF_ALT_APN, acTmp, iApnLen);

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "03AMA", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   remChar(apTokens[L3_TRA], '-');
   lTmp = atol(apTokens[L3_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%0.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   //if (*apTokens[L3_ISAGPRESERVE] == '1')
   //   *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Alp_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Mailing
   Alp_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*(apTokens[L3_CURRENTDOCNUM]+3) > '0')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 
   return 0;
}

/********************************************************************************
 *
 * The format should be similar to AMA where code 3 in the raw file is a Prop 13 and has code 2 in the Pcl_Values table, 
 * code 8 is a Prop 8 and codes 5, 55 and 96 are Williamson Act, which is code 9 in the Pcl_Values table.
 *
 * 1,3,96      001010011000: 1,2,9
 * 1,2,3,96    002170008000: 1,2,9
 * 1,3,5       005281008000: 1,2,9
 * 1,3,5,55    001150032000: 1,2,9,9
 * 1,3,8       001010023000: 1,2,8
 *
 *********************************************************************************/

int Alp_ResetPVReason(char *pInFile, char *pOutFile)
{
   char     *pTmp, acRec[1024], acApn[32], acReason[4], acPrevVst[4];
   int      lCnt=0;
   FILE     *fdIn, *fdOut;

   // Input record
   VALUE_REC *pVal;

   LogMsg0("Reset PV Reason in %s", pInFile);

   // Open Value file
   LogMsg("Open value file %s", pInFile);
   fdIn = fopen(pInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening value file: %s\n", pInFile);
      return -1;
   }

   LogMsg("Open output file %s", pOutFile);
   if (!(fdOut = fopen(pOutFile, "w")))
   {
      LogMsg("***** Error creating output file %s", pOutFile);
      return -2;
   }

   pVal = (VALUE_REC *)&acRec[0];
   memset(acApn, ' ', 20);
   memset(acReason, ' ', 4);
   memset(acPrevVst, ' ', 4);

   // Merge loop
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], 1024, fdIn);
      if (!pTmp)
         break;

#ifdef _DEBUG
      //if (!memcmp(pTmp, "001010011000", 9))  
      //   pTmp = NULL;
#endif
      // Set reason
      if (memcmp(acApn, pVal->Apn, iApnLen))
      {
         memcpy(acApn, pVal->Apn, iApnLen);
         memcpy(acReason, pVal->Reason, 2);
         memcpy(acPrevVst, pVal->ValueSetType, 2);

         if (pVal->Reason[0] > '1')
            memset(pVal->Reason, ' ', sizeof(pVal->Reason));
         else
            memcpy(pVal->Reason, "3 ", 2);
      } else if (pVal->Reason[0] > '1')
      {
         memset(pVal->Reason, ' ', sizeof(pVal->Reason));
         if (memcmp(pVal->ValueSetType, acPrevVst, 2) > 0)
            memset(pVal->ValueSetType, ' ', 2);
         else
            memcpy(acPrevVst, pVal->ValueSetType, 2);
      }

      if (pVal->Reason[0] == '1' && memcmp(acReason, "1 ", 2) > 0)
         memcpy(pVal->Reason, acReason, 2);

      fputs(acRec, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total Value records for import: %u", lCnt);

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   // Resort output on Asmt, ValueSetType, Reason
   LogMsg0("\nResorting value file %s --> %s", pOutFile, pInFile);
   lCnt = sortFile(pOutFile, pInFile, "S(1,14,C,A,21,1,C,A,259,1,C,D) DUPO(B4096,1,77)");

   return lCnt;
}

/*********************************** loadAlp ********************************
 *
 * Options:
 *    -CALP -L -Xs -Xl (load lien)
 *    -CALP -U -Xs[i] [-Z] [-T] (load update)
 *
 ****************************************************************************/

int loadAlp(int iSkip)
{
   int   iRet;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;
   iApnLen = myCounty.iApnLen;
   lLotSqftCount = 0;

   //char sInfile[_MAX_PATH], sOutfile[_MAX_PATH], sTmpl[_MAX_PATH];
   //sprintf(sInfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   //sprintf(sOutfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "P01");
   //iRet = Alp_ConvertApnRoll(sInfile, sOutfile, 1900);
   //iRet = GetIniString("Data", "GisFile", "", sTmpl, _MAX_PATH, acIniFile);
   //sprintf(sInfile, sTmpl, myCounty.acCntyCode, "Gis");
   //sprintf(sOutfile, sTmpl, myCounty.acCntyCode, "Out");
   //iRet = Alp_ConvertApn(sInfile, sOutfile);
   //sprintf(sInfile, acSaleTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "cres");
   //sprintf(sOutfile, acSaleTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   //iRet = Alp_ConvertSaleApn(sInfile, sOutfile);

   // Daily files - Temporary not use this until county said OK - 10/24/2019
   // Use quarterly file AGENCYCDCURRSEC_TR.TAB to tax base
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {      
      if (strstr(sTaxTmpl, "AGENCYCD"))
         iRet = MB_Load_TaxBase(bTaxImport, true, 3, iHdrRows, MM_DD_YYYY_2);    // AGENCYCDCURRSEC_TR.TAB
      else
         iRet = MB_Load_TaxBase(bTaxImport, true, 2, iHdrRows, YYYY_MM_DD);      // Load Alp_Tax.csv

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet = MB_Load_TaxCodeMstr(bTaxImport, 0);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport, 0);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport, iHdrRows);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
      if (!iLoadFlag)
         return iRet;
   }

   // Extract lien file - this option has to go first.
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = MB_ExtrTR601(myCounty.acCntyCode);
   }

   // Load Value file - from TUO
   if (iLoadFlag & EXTR_VALUE)                     // -Xv
   {
      char sDbName[64], sBYVFile[_MAX_PATH], sCVFile[_MAX_PATH], acTmp[256];

      sprintf(sBYVFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Value");
      sprintf(acTmp, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = MB_ExtrValues(myCounty.acCntyCode, acValueFile, acExeFile, acTmp, sBYVFile, iHdrRows);

      // Reset reason and VST
      if (iRet > 0)
      {
         sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Vmp");
         iRet = MB_ResetPVReason(sBYVFile, acTmp);
      }

      if (iRet > 0)
      {
         sprintf(sDbName, "LDR%d", lLienYear);
         GetIniString("Data", "SqlValueFile", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(sCVFile, acTmp, sDbName, myCounty.acCntyCode);
         lRecCnt = createValueImport(sBYVFile, sCVFile, false);
         if (lRecCnt > 0)
         {
            // Save output file for import
            strcpy(acValueFile, sCVFile);
            iRet = 0;
         } else
            iLoadFlag ^= EXTR_IVAL;                // Turn off import
      }
   }

   // Create/Update cum sale file from Alp_Sales.csv
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Do not use confirmed sale price
      //bUseConfSalePrice = false;

      // ALP sale file can have different date format, use all default settings
      //iRet = Alp_CreateSCSale(MM_DD_YYYY_2,false,(IDX_TBL5 *)&ALP_DocCode[0]);
      iRet = Alp_CreateSCSale(MM_DD_YYYY_2, true, (IDX_TBL5 *)NULL);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Load Char file
      if (!_access(acCharFile, 0))
      {
         iRet = Alp_ConvStdChar(acCharFile);

         if (iRet <= 0)
            LogMsg("*** WARNING: Error converting Char file %s.  Use existing data.", acCharFile);
      } else
      {
         LogMsg("*** WARNING: CHAR file does not exist: %s: ", acCharFile);
         LogMsg("    -Xa option is ignore.  Please verify input file");
      }
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Create Lien file
      LogMsg0("Load %s LDR file", myCounty.acCntyCode);
      iRet = Alp_Load_LDR(iSkip);                  // 2016
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Alp_Load_Roll(iSkip);
      LogMsg("Total LotSqft populated: %d", lLotSqftCount);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Alp_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   if (bUpdPrevApn)                                // -Up
   {
      // If not defined, use current apn length
      iRet = GetPrivateProfileInt(myCounty.acCntyCode, "PrevApnLen", iApnLen, acIniFile);
      iRet = updatePrevApn(myCounty.acCntyCode, iRet, iSkip);
   }

   return iRet;
}
