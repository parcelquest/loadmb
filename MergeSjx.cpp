/**************************************************************************
 *
 * Options:
 *    -CSJX -L [-Xs|-Ms] -Xa -Xl          (load lien)
 *    -CSJX -U [-Xsi|-Ms] [-Xa] [-Mr]     (load update)
 *    -CSJX -T|-Ut [-Dr]                  (load tax)
 *
 * Notes:
 *
 * Revision
 * 02/23/2018 17.6.0    First version - Copy from MergeGle.cpp
 * 03/05/2018           Final version for production
 * 03/27/2018 17.9.0    Add -Ut option to update tax.
 * 06/28/2018 18.0.0    Modify Sjx_MergeStdChar() to add BATH_4Q & BATH_2Q.  
 *                      Modify Sjx_ConvStdChar() to sort output file instead of input file.
 *                      Fix DocNum bug in Sjx_CreateSCSale() that chops off DocNum at 8 bytes.
 *                      Fix USE_STD format and filter out bad DocNum in Sjx_MergeLien3().
 *                      Remove unused code and modify situs sort command in Sjx_Load_LDR3().
 * 01/22/2019 18.9.5    Adding code to load tax using MB files.
 * 03/13/2019 18.10.6   Modify Sjx_CreateSCSale() to populate sale code using owned asTransferTypes[] instead os asSaleCode[].
 * 04/20/2019 18.11.3   Modify Sjx_MergeRoll() to populate ALT_APN with PREV_APN.  
 *                      This is needed to link to DataTree until they changed to new APN format.
 * 07/16/2019 19.0.4    County is now sending us tax data daily in zip file. Unzip before processing.
 * 10/31/2019 19.5.0.1  Remove unused code and add comments
 * 07/16/2020 20.1.5    Modify Sjx_MergeSitus() to handle special case of S_DIR.
 * 11/01/2020 20.4.2    Modify Sjx_MergeRoll() to populate default PQZoning.
 * 04/02/2021 20.8.4    Modify Sjx_MergeOwner() to handle special char.
 * 05/05/2021 20.8.6    Modify Sjx_ConvStdChar() to fix BldgClass and FirePlace.  Fix SaleCode in Sjx_CreateSCSale().
 * 06/28/2021 21.0.3    Modify Sjx_MergeOwner() to handle char 0xC9 (showing as 0x90 in debug)
 *                      and remove extra space in name.
 * 08/26/2021 21.2.0    Default bUseConfSalePrice=false so we don't use confidential sale price.
 * 10/18/2023 23.4.0    Update skip header value for MB_Load_TaxBase() in loadSjx().
 * 02/05/2024 23.6.0    Modify Sjx_MergeSitus() & Sjx_MergeMAdr() to populate UnitNox.
 * 03/22/2024 23.7.3.1  Modify Sjx_MergeStdChar() to populate LOT_ACRES/LOtSQFT
 * 08/01/2024 24.0.4    Modify Sjx_MergeLien3() to remove bad char in legal and to add ExeType.
 * 01/23/2025 24.5.1    Add support for -Xn & -Mn
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "dozip.h"

#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "CharRec.h"
#include "PQ.h"
#include "MergeSjx.h"
#include "Situs.h"
#include "Tax.h"
#include "MB_TaxInfo.h"
#include "NdcExtr.h"

/******************************** Sjx_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sjx_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf)
{
   int   iTmp, iRet, iVet;
   char  acOwner[64], acName1[64], acName2[64], acTmp[128], *pTmp, *pTmp1;
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);

#ifdef _DEBUG
   // MC DONALD JOHN E &   % MC DONALD ELIZABETH ETAL
   //if (!memcmp(pOutbuf, "013240110", 9))
   //   iTmp = 0;
#endif

   // Check embeded CareOf
   if (pTmp = strstr(pNames, "C/O"))
   {
      updateCareOf(pOutbuf, pTmp, strlen(pTmp));
      *pTmp = 0;
   }

   // Remove multiple spaces
   strcpy(acName1, pNames);
   replUChar((unsigned char *)acName1, 0x90, 'E', 0);
   iTmp = blankRem(acName1);
   if (pCareOf && *pCareOf == '%')
   {
      if (*(pCareOf+1) == ' ')
         strcpy(acName2, pCareOf+2);
      else
         strcpy(acName2, pCareOf+1);
      *pCareOf = 0;
   } else
      acName2[0] = 0;

   // Update vesting
   iVet = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST);

   // Check for overflow character '&'
   if (acName1[iTmp-1] == '&')
   {
      acName1[iTmp-2] = 0;

      // Check Name2
      if (acName2[0] > ' ')
      {
         // Check vesting
         if (!iVet)
            iVet = updateVesting(myCounty.acCntyCode, acName2, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

            // Check embeded CareOf
            if (pTmp = strstr(acName2, "C/O"))
            {
               updateCareOf(pOutbuf, pTmp, strlen(pTmp));
               *pTmp = 0;
            }

         iTmp = MergeName2(acName1, acName2, acOwner, ' ');
         // Remove Name2 if mergable
         if (iTmp == 1)
            acName2[0] = 0;
         else
         {
            strcpy(acOwner, acName1);
            pTmp = strrchr(acName2, ' ');
            if (pTmp && (!memcmp(pTmp, " JT", 3) || !memcmp(pTmp, " J/T", 4) ||
                         !strcmp(pTmp, " CP")    ||!strcmp(pTmp, " C/P") ||
                         !strcmp(pTmp, " SP")    || !strcmp(pTmp, " TC")))
               *pTmp = 0;
         }
      } else
         strcpy(acOwner, acName1);
   } else
      strcpy(acOwner, acName1);

   strcpy(acTmp, acOwner);
   // Remove ETAL or DVA
   if (pTmp = strrchr(acTmp, ' '))
   {
      if (!strcmp(pTmp, " ETAL") || !strcmp(pTmp, " DVA") )
         *pTmp = 0;
      else if (pTmp = strstr(acTmp, " ET AL"))
         *pTmp = 0;
   }

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (acTmp[iTmp] > '0' && acTmp[iTmp] < 'A')
      {
         iTmp = 0;
         break;
      }
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp || strstr(acOwner, "L/P"))
   {
      vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
      return;
   }

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
      if ( (pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " TR")))
         *pTmp = 0;

      if (strstr((char *)&acTmp[iTmp], " TRUST") ||
         strstr((char *)&acTmp[iTmp], " LIVING") ||
         strstr((char *)&acTmp[iTmp], " REVOCABLE") )
         acTmp[--iTmp] = 0;
   }

   // Filter out words, things in parenthesis
   if ( (pTmp = strchr(acTmp, '('))      || (pTmp=strstr(acTmp, " CO TR"))   ||
        (pTmp=strstr(acTmp, " CO-TR"))   || (pTmp=strstr(acTmp, " COTR"))    ||
        (pTmp=strstr(acTmp, " TRUSTEE")) || (pTmp=strstr(acTmp, " TR ETAL")) ||
        (pTmp=strstr(acTmp, " TTEE"))    || (pTmp=strstr(acTmp, " TRES"))    ||
        (pTmp=strstr(acTmp, " ETAL"))    || (pTmp=strstr(acTmp, " ET AL"))  )
      *pTmp = 0;

   // Filter some more
   pTmp = (char *)&acTmp[strlen(acTmp)-4];
   //if (!memcmp(pTmp, " DVA", 4) || !memcmp(pTmp, " LIV", 4) )
   if (!memcmp(pTmp, " DVA", 4) )
      *pTmp = 0;

   // If there is number goes before REV, keep it.
   if (!memcmp(pTmp, " REV", 4))
   {
      pTmp1 = pTmp;
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
   }

   // Trim trailing number
   iTmp = strlen(acTmp)-1;
   while (iTmp > 0 && isdigit(acTmp[iTmp]))
      acTmp[iTmp--] = 0;

   if ((pTmp=strstr(acTmp, " SUCCS-TR")) || (pTmp=strstr(acTmp, " SUCCESSOR")) ||
      (pTmp=strstr(acTmp, " SURVIVOR")) )
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " FAMILY TRUST &")) || (pTmp=strstr(acTmp, " LIVING TRUST &")) )
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      // RUMAN RICKY L & RUMAN FAMILY TRUST &
      // RUMAN RICKY L & NANCY FAMILY TRUST & MARIE
      // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      //strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      //strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      *(pTmp+6) = 0;
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
         *pTmp1 = 0;
      else
         *pTmp = 0;

      //strcpy(acName2, pTmp+9);
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " FAM TR")) || (pTmp=strstr(acTmp, " FAMILY ")) ||
      (pTmp=strstr(acTmp, " REVOC"))          || (pTmp=strstr(acTmp, " REV TR")) ||
      (pTmp=strstr(acTmp, " REV LIV TR"))     || (pTmp=strstr(acTmp, " REV LIVING")) ||
      (pTmp=strstr(acTmp, " LIV TRUST"))      || (pTmp=strstr(acTmp, " LIVING ")) ||
      (pTmp=strstr(acTmp, " INCOME TR"))      || (pTmp=strstr(acTmp, " 1992 REV"))
      )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ( (pTmp=strstr(acTmp, " ESTATE OF")) || (pTmp=strstr(acTmp, " EST OF")) ||
               (pTmp=strstr(acTmp, " ESTS OF")) )
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   pTmp = strrchr(acName1, ' ');
   if (pTmp && (!memcmp(pTmp, " JT", 3) || !strcmp(pTmp, " CP")    ||!strcmp(pTmp, " C/P") ||
                !strcmp(pTmp, " SP")    || !strcmp(pTmp, " TC")))
      *pTmp = 0;

   if ((pTmp=strstr(acName1, " S/S")))
      *pTmp = 0;
   if ((pTmp=strstr(acName1, " J/T")) || (pTmp=strstr(acName1, " J/ T")))
      *pTmp = 0;
   if ((pTmp=strstr(acName1, " T/C")) || (pTmp=strstr(acName1, " C/B")))
      *pTmp = 0;

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&TEHRON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
         *pTmp1++ = 0;
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = -1;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      if (myOwner.acVest[0] > ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // If name is swapable, use it
      if (iRet >= 0)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   }

   if (iRet == -1)
   {
      // Couldn't split names
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME1);
   }

   // Save Name2 if exist
   if (acName2[0] > ' ')
      vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);

   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);
}

/******************************** Sjx_MergeOwner *****************************
 *
 * The CareOf field may contain Owner2.  Check for & at the end of Assessee
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sjx_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf, char *pDba)
{
   int   iTmp, iRet, iACnt;
   char  acOwner[64], acName1[64], acName2[64], *pTmp;
   OWNER myOwner;
   boolean bUseCareOf=false, bHasName2=false;

   // Clear output buffer if needed
   removeNames(pOutbuf, true, true);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0050200020", 10))
   //   iTmp = 0;
#endif

   // Init names
   iACnt=0;
   strcpy(acName1, pNames);
   replUChar((unsigned char *)acName1, 0xC9, 'A', 0);
   //myTrim(acName1);
   acName2[0] = 0;

   // Check DBA
   if (*pDba > ' ')
   {
      pTmp = pDba;
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   }

   // Check CareOf
   if (pTmp = strstr(acName1, "C/O"))
   {
      updateCareOf(pOutbuf, pTmp, strlen(pTmp));
      *--pTmp = 0;
   }

   // Check for Name2
   iTmp = strlen(acName1);
   if (acName1[iTmp-1] == '&')
   {
      bHasName2 = true;
      acName1[iTmp-1] = 0;
      if (*pCareOf == '%')
         strcpy(acName2, pCareOf+2);
      else
         strcpy(acName2, pCareOf);
   }


   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST);
   if (!iTmp && acName2[0] > ' ')
      iTmp = updateVesting(myCounty.acCntyCode, acName2, pOutbuf+OFF_VEST);

   if (bHasName2 && !iTmp)
   {
      iTmp = MergeName1(acName1, acName2, acOwner);
      if (!iTmp)
         strcpy(acOwner, acName1);
   } else
      strcpy(acOwner, acName1);

   // Remove extra space in name
   blankRem(acName1);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = -1;
   if (strchr(acOwner, ' '))
   {
      iRet = splitOwner(acOwner, &myOwner, 5);

      // If name is swapable, use it
      if (iRet >= 0)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   }

   if (iRet == -1)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);

   // Save Name2 if exist
   vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);

   // Save Name1
   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);
}

/******************************** Sjx_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Sjx_MergeMAdr(char *pOutbuf)
{
   char    acTmp[256], acAddr1[64], *pTmp;
   int     iTmp;
   ADR_REC sMailAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0050200020", 10))
   //   iTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, false, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }

   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
      sMailAdr.strName[SIZ_M_STREET] = 0;
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
      vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
   }

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], SIZ_M_CITY);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         // Take out '-' in zipcode if present
         strcpy(acTmp, apTokens[MB_ROLL_M_ZIP]);
         iTmp = strlen(acTmp);
         if (acTmp[5] == '-' && iTmp == 10)
         {
            strcpy(&acTmp[5], &acTmp[6]);
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         } else if (iTmp > 9)
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         else
            vmemcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
      } else
         iTmp = 0;

      if (iTmp == 9)
         sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP], apTokens[MB_ROLL_M_ZIP]+5);
      else
         sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

void Sjx_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)

{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2, *pDba;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "005070005000", 9) || !memcmp(pOutbuf, "005230043000", 9) )
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   pDba = p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }

      if (!_memicmp(pLine1, "DBA ", 4) )
         pDba = pLine1;
      else if (!_memicmp(pLine2, "DBA ", 4))
         pDba = pLine2;
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }

      if (!_memicmp(pLine1, "DBA ", 4) )
         pDba = pLine1;
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
   }

   // Update DBA
   if (pDba)
   {
      memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));

         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ')
         {
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
            vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
         }
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   strcpy(acAddr2, p2);
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);

      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}


/******************************** Sjx_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Sjx_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acAddr2[64], acSfx[32], 
            acStrName[32], *pTmp, *pUnit, *pSfx;
   long     lTmp;
   int      iRet=0, iTmp, iSfxCode;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do {
         pRec = fgets(acRec, 512, fdSitus);
      } while (*pRec < ' ' || *pRec > '9');
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      pTmp = pRec;
      if (*pTmp == '"')
         pTmp++;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   else
      iRet = ParseStringIQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   if (*apTokens[MB_SITUS_STRNAME] == ' ' && *apTokens[MB_SITUS_STRNUM] == ' ')
      return 0;

   // Initialize 
   acAddr1[0] = 0;
   strcpy(acStrName, apTokens[MB_SITUS_STRNAME]);
   removeSitus(pOutbuf);
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);

   if (lTmp > 0)
   {
      // Save original StrNum
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apTokens[MB_SITUS_STRDIR] > ' ' && isDir(apTokens[MB_SITUS_STRDIR]))
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         vmemcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], SIZ_S_DIR);
      } else if (*apTokens[MB_SITUS_STRDIR] == 'O')
      {
         // Special case
         if (pTmp = strstr(apTokens[MB_SITUS_STRNAME], "HARA"))
            sprintf(acStrName, "O %s", pTmp);
      }
   }

   acSfx[0] = 0;
   iSfxCode = -1;
   if (*apTokens[MB_SITUS_STRTYPE] == '#')
   {
      pUnit = apTokens[MB_SITUS_STRTYPE];
      if ((pTmp = strrchr(acStrName, ' ')) && (iSfxCode = GetSfxCodeX(pTmp+1, acSfx)) > 0)
      {
         *pTmp = 0;
         pSfx = pTmp+1;
      }
   } else
   {
      pSfx = apTokens[MB_SITUS_STRTYPE];
      iSfxCode = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acSfx);
      pUnit = apTokens[MB_SITUS_UNIT];
   }

   if (iSfxCode > 0)
   {
      strcat(acAddr1, acStrName);
      strcat(acAddr1, " ");
      strcat(acAddr1, acSfx);
      vmemcpy(pOutbuf+OFF_S_STREET, acStrName, SIZ_S_STREET);

      iTmp = sprintf(acCode, "%d", iSfxCode);
      memcpy(pOutbuf+OFF_S_SUFF, acCode, iTmp);
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, acStrName);
      vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      vmemcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, SIZ_S_SUFF);

      strcat(acAddr1, acStrName);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004090016520", 12))
   //   iTmp = 0;
#endif
   if (*pUnit > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, pUnit);
      vmemcpy(pOutbuf+OFF_S_UNITNO, pUnit, SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, pUnit, SIZ_S_UNITNOX);
   }

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);
   memset(acAddr2, ' ', SIZ_S_CTY_ST_D);

   // Situs city
   pTmp = myBTrim(apTokens[MB_SITUS_COMMUNITY]);
   if (*pTmp > ' ')
   {
      // Bypass "UN" prefix
      if (strlen(pTmp) == 4)
         pTmp+= 2;
      Abbr2Code(pTmp, acTmp, acAddr2, pOutbuf);
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr2[0] > ' ')
      {
         // Zip
         lTmp = atol(apTokens[MB_SITUS_ZIP]);
         if (lTmp > 10000 && lTmp < 99999)
            memcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], 5);

         iTmp = sprintf(acTmp, "%s, CA %.5s", acAddr2, apTokens[MB_SITUS_ZIP]);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
      }
   }

   // Get city/zip from GIS extract
   //if (fdCity && *(pOutbuf+OFF_S_CITY) == ' ')
   //{
   //   char acZip[16], acCity[32];

   //   iTmp = getCityZip(pOutbuf, acCity, acZip, 9);
   //   if (!iTmp)
   //   {
   //      City2Code(acCity, acCode, pOutbuf);
   //      memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
   //      memcpy(pOutbuf+OFF_S_ZIP, acZip, strlen(acZip));
   //      sprintf(acAddr2, "%s CA %s", acCity, acZip);
   //      lUseGis++;
   //   }
   //}

   iTmp = blankRem(acAddr2, SIZ_S_CTY_ST_D);
   memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int Sjx_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "007280027000", 9))
   //   acTmp[0] = 0;
#endif
   //
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   if (sSitusAdr.lStrNum > 0)
   {
      char *pTmp = strchr(acAddr1, ' ');
      *pTmp = 0;

      // Save original StrNum
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
      memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   }

   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, sSitusAdr.UnitNox, SIZ_S_UNITNOX);
   }

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/******************************** Sjx_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Sjx_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      pTmp = pRec;
      if (*pTmp == '"')
         pTmp++;

      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002017014", 9))
   //   iRet = 0;
#endif

   if (iTmp)
      return 1;

   while (!iTmp)
   {
      // Parse input
      if (cDelim == ',')
         iRet = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iRet = ParseStringIQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);

      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, YYYY_MM_DD);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         /* Do not use Confidential Sale Price
         } else
         {
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            }
         */
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         MB_MergeSale(&sCurSale, pOutbuf, true, false);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
      {
         pTmp = pRec;
         if (*pTmp == '"')
            pTmp++;
         iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      } else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/***************************** Sjx_MergeStdChar ******************************
 *
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the one that has value.
 *
 *****************************************************************************/

int Sjx_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft;
   int      iLoop, iBeds, iFBath, iHBath, iBldgNum, iTmp;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001051007", 9))
   //   iTmp = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;

   // Yrblt
   memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // YrEff
   iTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (iTmp > 1600 && iTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // LotSqft
   long lLotSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lLotSqft > 100)
   {
      memcpy(pOutbuf+OFF_LOT_SQFT, pChar->LotSqft, SIZ_LOT_SQFT);
      memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_ACRES);
   } 

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   } else
      memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);

   // Parking type
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // ParkSpace
   iTmp = atoin(pChar->ParkSpace, SIZ_CHAR_SIZE4);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d      ", iTmp);
      memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
   } else
      memset(pOutbuf+OFF_PARK_SPACE, ' ', SIZ_PARK_SPACE);

   // Heating
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];

   // Cooling
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      memcpy(pOutbuf+OFF_BATH_4Q, acTmp, SIZ_BATH_F);
   } else
   {
      memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);
      memset(pOutbuf+OFF_BATH_4Q, ' ', SIZ_BATH_F);
   }

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      memcpy(pOutbuf+OFF_BATH_2Q, acTmp, SIZ_BATH_H);
   } else
   {
      memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);
      memset(pOutbuf+OFF_BATH_2Q, ' ', SIZ_BATH_H);
   }

   // Rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   } else
      memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

   // Units
   iTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (pChar->Units[0] > ' ')
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   } else
      memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

   // Stories
   if (pChar->Stories[0] > ' ')
      memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
   else
      memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

   // Fireplace
   if (pChar->Fireplace[0] > ' ')
      memcpy(pOutbuf+OFF_FIRE_PL, pChar->Fireplace, SIZ_FIRE_PL);
   else
      memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);

   // HasSeptic or HasSewer
   *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Pools
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // BldgNum
   iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************** Sjx_MergeChar ******************************
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

//int Sjx_MergeChar(char *pOutbuf)
//{
//   static   char acRec[1024], *pRec=NULL;
//   char     acTmp[256];
//   long     lBldgSqft, lGarSqft;
//   int      iLoop, iBeds, iFBath, iHBath, iFp;
//   STDCHAR *pChar;
//
//   // Get first Char rec for first call
//   if (!pRec)
//      pRec = fgets(acRec, 1024, fdChar);
//
//   do
//   {
//      if (!pRec)
//      {
//         fclose(fdChar);
//         fdChar = NULL;
//         return 1;      // EOF
//      }
//
//      // Compare Asmt
//      iLoop = memcmp(pOutbuf, pRec, iApnLen);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
//         pRec = fgets(acRec, 1024, fdChar);
//         lCharSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 1;
//
//   pChar = (STDCHAR *)pRec;
//
//   // Quality Class
//   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
//   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
//
//   // Yrblt
//   memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
//
//   // BldgSqft
//   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
//   if (lBldgSqft > 10)
//   {
//      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
//      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
//   } else
//      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);
//
//   // Garage Sqft
//   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
//   if (lGarSqft > 10)
//   {
//      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
//      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
//   } else if (*(pOutbuf+OFF_PARK_TYPE) > ' ')
//   {
//      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
//   }
//
//   // Parking type
//   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
//
//   // Heating
//   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
//
//   // Cooling
//   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];
//
//   // Beds
//   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
//   if (iBeds > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
//      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
//   } else
//      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);
//
//   // Bath
//   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
//   if (iFBath > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
//      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
//   } else
//      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);
//
//   // Half bath
//   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
//   if (iHBath > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
//      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
//   } else
//      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);
//
//   // Fireplace
//   iFp = atoin(pChar->Fireplace, SIZ_CHAR_SIZE2);
//   if (iFp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
//      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
//   } else
//      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);
//
//   // HasSeptic or HasSewer
//   *(pOutbuf+OFF_SEWER) = pChar->HasSewer;
//
//   // HasWell
//   *(pOutbuf+OFF_WATER) = pChar->HasWater;
//
//   // Pools
//   *(pOutbuf+OFF_POOL) = pChar->Pool[0];
//
//   lCharMatch++;
//
//   iLoop = 0;
//   do
//   {
//      // Get next Char rec
//      pRec = fgets(acRec, 1024, fdChar);
//      if (!pRec)
//      {
//         fclose(fdChar);
//         fdChar = NULL;
//         break;      // EOF
//      }
//      iLoop++;
//   } while (!memcmp(pOutbuf, pRec, iApnLen));
//
//   if (iLoop > 1)
//   {
//      int iTmp;
//
//      iTmp = sprintf(acTmp, "%d", iLoop);
//      memcpy(pOutbuf+OFF_UNITS, acTmp, iTmp);
//   }
//
//   return 0;
//}

/********************************* Sjx_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sjx_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Remove NULL
   iRet = replStr(pRollRec, "NULL", "");

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Sjx_MergeLien(): bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L_ASMT], strlen(apTokens[L_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L_FEEPARCEL], strlen(apTokens[L_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "39SJX", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values
   lTmp = atoin(apTokens[L_ASMT], 3);
   if (lTmp > 799 && lTmp < 900)
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   // Land
   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixtr = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
   long lGrow  = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);
   long lPers  = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
   long lPP_MH = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L_TAXAMT1]);
   double dTax2 = atof(apTokens[L_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }
   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'


   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L_TRA], strlen(apTokens[L_TRA]));

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030330020", 9))
   //   iTmp = 0;
#endif
   // Legal
   updateLegal(pOutbuf, apTokens[L_PARCELDESCRIPTION]);

   // UseCode
   iTmp = strlen(apTokens[L_USECODE]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L_USECODE], SIZ_USE_CO, iTmp);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L_USECODE], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Sjx_MergeOwner(pOutbuf, apTokens[L_OWNER], "");

   // Situs
   Sjx_MergeSitus(pOutbuf, apTokens[L_SITUS1], apTokens[L_SITUS2]);

   // Mailing
   Sjx_MergeMAdr(pOutbuf, apTokens[L_MAILADDRESS1], apTokens[L_MAILADDRESS2], apTokens[L_MAILADDRESS3], apTokens[L_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L_TAXABILITY], true, true);

   return 0;
}

/********************************* Sjx_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sjx_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp, iFeeBook;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Sjx_MergeRoll(): bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN starts with 8
   iFeeBook = atoin(apTokens[MB_ROLL_FEEPARCEL], 3);
   if (*apTokens[iApnFld] == '8' || iFeeBook == 555)
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // PREV_APN - Only apply to physical parcels
      if (memcmp(pOutbuf, "500", 3) < 0)
         vmemcpy(pOutbuf+OFF_PREV_APN, apTokens[iApnFld], 10);

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "39SJX", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   } else
   {
      memcpy(pOutbuf+OFF_CO_NUM, "39SJX", 5);

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Copy ALT_APN
      if (*(pOutbuf+OFF_PREV_APN) > ' ')
         memcpy(pOutbuf+OFF_ALT_APN, pOutbuf+OFF_PREV_APN, 12);
      else
         vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], SIZ_ALT_APN);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   acTmp[SIZ_USE_CO] = 0;
   if (acTmp[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO, 0);

      // Reformat UseCode for matching to StdUse table
      iTmp = iTrim(acTmp);
      if (iTmp == 2)
      {
         acTmp[3] = 0;
         acTmp[2] = acTmp[1];
         acTmp[1] = acTmp[0];
         acTmp[0] = '0';
      }
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 3, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);

      if (*(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
         lTmp = atol(apTokens[MB_ROLL_DOCNUM]+5);
      else
         lTmp = 0;
      if (pTmp && !memcmp(acTmp, apTokens[MB_ROLL_DOCNUM], 4) && lTmp < 9999)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], strlen(apTokens[MB_ROLL_DOCNUM]));
      }

   }

   // Owner
   try {
      Sjx_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER], apTokens[MB_ROLL_CAREOF], apTokens[MB_ROLL_DBA]);
   } catch(...) {
      LogMsg("***** Exeception occured in Sjx_MergeOwner()");
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "990018287", 9) )
   //   iTmp = 0;
#endif
   // Mailing
   try {
      Sjx_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Sjx_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************** Sjx_Load_Roll *****************************
 *
 *
 *
 ******************************************************************************/

int Sjx_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,GT,\"9\",OR,1,1,C,LT,\"0\")");
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"Z\") DEL(124) ");
   else
      lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) ");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) ");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   //LogMsg("Open Tax file %s", acTaxFile);
   //sprintf(acTmpFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (cDelim == '|')
   //   lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   //else
   //   lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) ");
   //fdTax = fopen(acTmpFile, "r");
   //if (fdTax == NULL)
   //{
   //   LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
   //   return -2;
   //}

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (acRollRec[0] > '9' || acRollRec[0] < '0');

   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "007280027000", 9))
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Sjx_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Sjx_MergeSitus(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Sjx_MergeStdChar(acBuf);

            // Merge Taxes
            //if (fdTax)
            //   lRet = MB_MergeTaxG2(acBuf, 0);

            iRollUpd++;
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Sjx_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Sjx_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe2(acRec, 0);

            // Merge Char
            if (fdChar)
               lRet = Sjx_MergeStdChar(acRec);

            // Merge Taxes
            //if (fdTax)
            //   lRet = MB_MergeTaxG2(acRec, iHdrRows);

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Sjx_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Sjx_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe2(acRec, 0);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Sjx_MergeStdChar(acRec);

         // Merge Taxes
         //if (fdTax)
         //   lRet = MB_MergeTaxG2(acRec, iHdrRows);

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   //if (fdTax)
   //   fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   return 0;
}

/********************************** Sjx_Load_Roll *****************************
 *
 *
 *
 ******************************************************************************/

int Sjx_Load_OldLdr(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet, iTmp, iNewRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SMP");
   lRet = sortFile(acSalesFile, acTmpFile, "S(#1,C,A,#3,DAT,A) F(TXT) ");
   fdSale = fopen(acTmpFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acTmpFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "EMP");
   lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) ");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Write header record
   memset(acBuf, '9', iRecLen);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
#ifdef _DEBUG
      //if (!memcmp(acBuf, "030330020", 9) || !memcmp((char *)&acRollRec[1], "030330020", 9))
      //   iTmp = 0;
#endif

      // Create new R01 record
      iRet = Sjx_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Sjx_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);

         // Merge Char
         if (fdChar)
            lRet = Sjx_MergeStdChar(acRec);

         // Merge Sales
         if (fdSale)
            lRet = Sjx_MergeSale(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, iHdrRows);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }

      // Get next roll record
      do
      {
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (pTmp && cDelim == '"')
            pTmp++;
      } while (pTmp && (memcmp(pTmp, "799", 3) > 0) && (memcmp(pTmp, "900", 3) < 0 ));

      if (!pTmp)
         bEof = true;    // Signal to stop

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdSale)
      fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);

   lRecCnt = iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   return 0;
}

/********************************* Sjx_Load_LDR *****************************
 *
 * Load Sjx_Roll.csv into 1900-byte record.
 *
 ****************************************************************************/

int Sjx_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A)");
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"Z\") DEL(124) ");
   else
      lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) ");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) ");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) ");
   fdTax = fopen(acTmpFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (acRollRec[0] > '9' || acRollRec[0] < '0');

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   lCnt = 1;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new R01 record
      iRet = Sjx_MergeRoll(acBuf, acRollRec, iRecLen, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Sjx_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Sjx_MergeStdChar(acBuf);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acBuf, 0);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe2(acBuf, 0);

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lLDRRecCount++;

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else
         LogMsg("*** Skip record# %d [%.12s]", lLDRRecCount, acRollRec);

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRollRec[1]))
         break;      // EOF

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);

   lRecCnt = lLDRRecCount;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total records output:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   return 0;
}

/**************************** Sjx_ConvStdChar ********************************
 *
 * Copy from MergeYol.cpp to convert new char file. 08/21/2015
 * MER is similar without LandSqft at the end.
 * 
 *****************************************************************************/

int Sjx_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], *pRec, *pTmp;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg0("\nConverting char file %s", pInfile);

   // Open input file
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening input file %s", pInfile);
      return -1;
   }

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip header
   do
   {
      pRec = fgets(acBuf, 4096, fdIn);
   } while (!feof(fdIn) && !isdigit(*pRec));

   // Start loop
   while (!feof(fdIn))
   {
      // Get next record
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec || acBuf[0] > '9' || acBuf[0] < '0')
         break;

      replStrAll(acBuf, "|NULL", "|");
      iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < SJX_CHAR_HASWELL)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[SJX_CHAR_ASMT], strlen(apTokens[SJX_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[SJX_CHAR_FEEPARCEL], strlen(apTokens[SJX_CHAR_FEEPARCEL]));

      // Format APN
      if (*apTokens[SJX_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[SJX_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         if (bDebug)
            LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[SJX_CHAR_FEEPARCEL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[SJX_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[SJX_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "001280140000", 9))
      //   iRet = 0;
#endif
      // Pool 
      iTmp = blankRem(apTokens[SJX_CHAR_POOLSPA]);
      if (iTmp > 0)
      {
         // Use findXlatCodeA since asPool is not sorted
         pRec = findXlatCodeA(apTokens[SJX_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass 
      vmemcpy(myCharRec.QualityClass, _strupr(apTokens[SJX_CHAR_QUALITYCLASS]), SIZ_CHAR_QCLS);

      if (memcmp(apTokens[SJX_CHAR_QUALITYCLASS], "0000", 4) > 0 &&
          memcmp(apTokens[SJX_CHAR_QUALITYCLASS], "1644", 4) &&
          memcmp(apTokens[SJX_CHAR_QUALITYCLASS], "2944", 4))
      {
         acCode[0] = ' ';
         pTmp = strcpy(acTmp, apTokens[SJX_CHAR_QUALITYCLASS]);

         if (isalpha(acTmp[1]) || isalpha(acTmp[0])) 
         {
            if (isalpha(acTmp[1]))
            {
               myCharRec.BldgClass = acTmp[1];
               pTmp = &acTmp[2];
            } else
            {
               myCharRec.BldgClass = acTmp[0];
               pTmp = &acTmp[1];
            }

            if (isdigit(*pTmp))
               iRet = Quality2Code(pTmp, acCode, NULL);
            else if (!_memicmp(acTmp, "ST", 2))
               memcpy(myCharRec.QualityClass, "ST", 2);     // Steel
            else if (*pTmp > '0')
               LogMsg("*** Invalid QUALITY: '%s' in [%s]", apTokens[SJX_CHAR_QUALITYCLASS], apTokens[SJX_CHAR_ASMT]);
         } else if (isdigit(acTmp[0]))
            iRet = Quality2Code(acTmp, acCode, NULL);
         else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[SJX_CHAR_QUALITYCLASS], apTokens[SJX_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } 

      // YrBlt
      int iYrBlt = atoi(apTokens[SJX_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[SJX_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[SJX_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[SJX_CHAR_UNITSCNT]);
      //if (iTmp > 0 && iBldgSize > iTmp*100)
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // Stories/NumFloors
      iTmp = atoi(apTokens[SJX_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 99)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[SJX_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[SJX_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport Sqft
      int iCarport = atoi(apTokens[SJX_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Patio SF
      iTmp = atoi(apTokens[SJX_CHAR_PATIOSF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }


#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "001280140000", 9))
      //   iRet = 0;
#endif

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[SJX_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[SJX_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[SJX_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[SJX_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[SJX_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[SJX_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[SJX_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[SJX_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
         memcpy(myCharRec.Bath_4Q, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[SJX_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
         memcpy(myCharRec.Bath_2Q, acTmp, iRet);
      }

      // FirePlace - 
      if (*apTokens[SJX_CHAR_FIREPLACE] >= '0' && *apTokens[SJX_CHAR_FIREPLACE] <= '9')
      {
         iTmp = atoi(apTokens[SJX_CHAR_FIREPLACE]);
         if (iTmp > 9)
            myCharRec.Fireplace[0] = 'M';
         else if (iTmp == 0)
            myCharRec.Fireplace[0] = 'N';
         else
            myCharRec.Fireplace[0] = *apTokens[SJX_CHAR_FIREPLACE];
      } else if (*apTokens[SJX_CHAR_FIREPLACE] > '9')
      {
         pRec = findXlatCode(apTokens[SJX_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      }

      // Haswell 
      blankRem(apTokens[SJX_CHAR_HASWELL]);
      if (*(apTokens[SJX_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Lot Sqft
      if (iFldCnt >= SJX_CHAR_LOTSQFT)
      {
         ULONG    lSqft;
         lSqft = (ULONG)atol(apTokens[SJX_CHAR_LOTSQFT]);
         if (lSqft > 1)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(lSqft*SQFT_MF_1000);
            iTmp = sprintf(acTmp, "%u", (long)(dTmp+0.1));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum, UnitSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/*************************** Sjx_ConvertApnRoll *****************************
 *
 * Convert APN format from MDB to MB.  Keep old APN in PREV_APN.
 *
 ****************************************************************************/

int Sjx_ConvertApnRoll(char *pInfile, char *pOutfile, int iRecordLen)
{
   FILE *fdIn, *fdOut;
   int   iTmp, iCnt=0;
   char  acBuf[2048], acTmp[32], acApn[32];

   LogMsg("Convert APN in roll file to new format for SJX");
   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "rb");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "wb");

   fread(acBuf, 1, iRecordLen, fdIn);
   fwrite(acBuf, 1, iRecordLen, fdOut);
   while (!feof(fdIn))
   {
      iTmp = fread(acBuf, 1, iRecordLen, fdIn);
      if (iTmp < iRecordLen)
         break;

      // Format new APN 
      if (!memcmp(acBuf, "0002", 4) || !memcmp(acBuf, "0003", 4))
         iTmp = sprintf(acApn, "940%.5s0000", &acBuf[3]);
      else if (!memcmp(acBuf, "0005", 4))
         iTmp = sprintf(acApn, "930%.5s0000", &acBuf[3]);
      else if (!memcmp(acBuf, "70", 2))
         iTmp = sprintf(acApn, "910%.6s000", &acBuf[2]);
      else if (!memcmp(acBuf, "80", 2))
         iTmp = sprintf(acApn, "920%.6s000", &acBuf[2]);
      else
         iTmp = sprintf(acApn, "%.8s0000", acBuf);

      // Save old APN to previous APN
      memcpy(&acBuf[OFF_PREV_APN], &acBuf[0], 8);

      // Update new APN
      memcpy(acBuf, acApn, iTmp);
      iTmp = sprintf(acTmp, "%.3s-%.3s-%.3s-000", acApn, &acApn[3], &acApn[6]);
      memcpy(&acBuf[OFF_APN_D], acTmp, iTmp);

      fwrite(acBuf, 1, iRecordLen, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert Roll APN completed with %d records", iCnt);

   return 0;
}

int Sjx_ConvDocNum(char *pSaleRec)
{
   int   iTmp, iRet = -1;
   char  sTmp[32];
   SCSAL_REC *pSale = (SCSAL_REC *)pSaleRec;

   memcpy(sTmp, pSale->DocNum, SALE_SIZ_DOCNUM);
   if (pSale->DocNum[0] > '9' || strchr(sTmp, '-'))
      return iRet;

   if (!memcmp(pSale->DocDate, "1900", 4))
      return iRet;

   // Only convert DocNum if year in DocNum matches DocDate
   if (!memcmp(&pSale->DocDate[2], pSale->DocNum, 2))
   {
      iTmp = sprintf(sTmp, "%.4sR%.6s", pSale->DocDate, &pSale->DocNum[2]);
      memcpy(pSale->DocNum, sTmp, iTmp);
      iRet = 0;
   }

   return iRet;
}


int Sjx_ConvertApnSale(char *pInfile, char *pOutfile)
{
   FILE *fdIn, *fdOut;
   int   iTmp1, iCnt=0, iOut=0;
   char  acBuf[1024], acApn[32], *pBuf;
   SCSAL_REC *pSale = (SCSAL_REC *)&acBuf[0];

   LogMsg0("Convert APN in sale file to new format for SJX");
   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");

   while (!feof(fdIn))
   {
      pBuf = fgets(acBuf, 1024, fdIn);
      if (!pBuf)
         break;

      // Format new APN
      if (!memcmp(acBuf, "0002", 4) || !memcmp(acBuf, "0003", 4))
         iTmp1 = sprintf(acApn, "940%.5s0000", &acBuf[3]);
      else if (!memcmp(acBuf, "0005", 4))
         iTmp1 = sprintf(acApn, "930%.5s0000", &acBuf[3]);
      else if (!memcmp(acBuf, "70", 2))
         iTmp1 = sprintf(acApn, "910%.6s000", &acBuf[2]);
      else if (!memcmp(acBuf, "80", 2))
         iTmp1 = sprintf(acApn, "920%.6s000", &acBuf[2]);
      else
         iTmp1 = sprintf(acApn, "%.8s0000", acBuf);
      memcpy(acBuf, acApn, iTmp1);

      // Reformat DocNum
      iTmp1 = Sjx_ConvDocNum(acBuf);
      if (!iTmp1)
      {
         fputs(acBuf, fdOut);
         iOut++;
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Total sale records processed %d", iCnt);
   LogMsg("                   converted %d", iOut);

   return 0;
}

/***************************** SJX_CreateSCSale *******************************
 *
 * Extract sale data from ???_Sales.csv and output to ???_SALE.DAT
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * DateFmt:
 *    0 : Default. Auto format by checking input date
 *    MM_DD_YYYY_1 (MON)
 *    YYYY_MM_DD   (HUM)
 *
 * DocTypeFmt:
 *    0 : Default. Set DocType=1 (GD) if DocCode=1 or sale price > 0
 *    1 : AMA, BUT, MAD, SON, SBT
 *    2 : PLA
 *    3 : SHA, STA
 *
 * DocNumFmt:
 *    0 : Default. Copy DocNum as is
 *    1 : Format DocNum second part of DocNum to 7 digits (i.e. 2010R1234 = 2010R0001234)
 *    2 : Format DocNum second part of DocNum to n digits (i.e. 2010R0034 = 2010R34) (COL)
 *    3 : Remove all nonnumeric after 5th character and format to 6 digits (SON)
 *    4 : Format to yyyyR9999999 if R is in pos 4 or 5 of original DocNum (SBT)
 *    5 : TEH
 *    6 : SJX - format as yyyy9999 (i.e. 2012-0001 = 20120001)
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Sjx_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH], acTmp[256], acRec[1024], *pTmp, *pDocNum;
   int      iTmp, iYear;
   long     lCnt=0, lPrice, lTmp, lDocYear;
   double   dTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   LogMsg0("Load sale file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Ignore docnum starts with alpha
      pDocNum = apTokens[MB_SALES_DOCNUM];
      if (!isdigit(*pDocNum) || *(pDocNum+4) != 'R' || strchr(pDocNum, '-') || !isNumber(pDocNum+5, 6))
      {
         if (bDebug)
            LogMsg0("--->Bad DocNum: APN=%s, DocNum=%s <-- Ignore!", apTokens[MB_SALES_ASMT], pDocNum);
         continue;
      }

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "052202011000", 9))
      //   iTmp = 0;
#endif
      // Doc date
      if (iDateFmt > 0)
      {
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      } else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
      {
         lDocYear = atoin(acTmp, 4);
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      } else
      {
         LogMsg0("--->Bad DocDate: APN=%s, DocDate=%s <-- Ignore!", apTokens[MB_SALES_ASMT], apTokens[MB_SALES_DOCDATE]);
         continue;
      }

      // Docnum
      if (*(pDocNum+4) == 'R' && isNumber(pDocNum+5,6))
      {
         lTmp = atol(pDocNum+5);
         iYear = atoin(apTokens[MB_SALES_DOCNUM], 4);
         if (iYear == lDocYear)
         {
            iTmp = sprintf(acTmp, "%dR%.6d", iYear, lTmp);
            memcpy(SaleRec.DocNum, acTmp, iTmp);
         } else
         {
            if (bDebug)
               LogMsg("*** Bad DocNum: %s %s [%s]", apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_DOCDATE], apTokens[MB_SALES_ASMT]);
            continue;
         }
      } else
      {
         if (bDebug)
            LogMsg("*** Bad DocNum: %s %s [%s]", apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_DOCDATE], apTokens[MB_SALES_ASMT]);
         continue;
      }

      //if (lTmp > 0 && lTmp < 999999)
      //{
      //   if (!iYear && *apTokens[MB_SALES_DOCNUM] == '0')
      //      iYear = atoin(apTokens[MB_SALES_DOCNUM], 5);
      //   if (iYear <= lToyear && iYear > 1900)
      //   {
      //      if (iYear == lDocYear)
      //      {
      //         iTmp = sprintf(acTmp, "%dR%.6d", iYear, lTmp);
      //         memcpy(SaleRec.DocNum, acTmp, iTmp);
      //      } else
      //         LogMsg("*** Bad DocNum: %s %s [%s]", apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_DOCDATE], apTokens[MB_SALES_ASMT]);
      //   } else if (lDocYear < 1988)
      //   {
      //      // Keep DocNum as is
      //      memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], iLen);
      //   } else
      //      LogMsg("*** Bad DocNum: %s [%s]", apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_ASMT]);
      //}

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

      // Confirmed sale price
      dTmp = atof(apTokens[MB_SALES_PRICE]);
      if (dTmp > 0.0)
      {
         lPrice = (long)((dTmp*100)/100.0);
         iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      } 

      // Do not use confirmed sale price
      if (!bUseConfSalePrice)
         lPrice = 0;

      // Tax
      dTmp = atof(apTokens[MB_SALES_TAXAMT]);
      if (dTmp > 0.0)
      {
         // Save DocTax
         iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);
         lTmp = (long)(dTmp * SALE_FACTOR);

         // Check for bad sale price
         if (lPrice != lTmp)
         {
            if (bDebug && bUseConfSalePrice)
               LogMsg("--> Questionable conf price APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Use stamp amt to calculate sale price.",
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE]);
            lPrice = lTmp;
         }

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Ignore sale price.",
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE]);
               lPrice = 0;
            }
         }
      } 

      // Ignore sale price if less than 1000
      if (lPrice > 1000)
         sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
      else
         memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
      memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);

      // Doc code - accept following code only
      int iDocCode = 0;
      if (isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         if (pDocTbl)
         {
            iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
            if (iTmp >= 0)
            {
#ifdef _DEBUG
               if (lPrice > 1000 && SaleRec.NoneSale_Flg == 'Y')
                  LogMsg("+++ Verify DocType for: %s [%s]", apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_ASMT]);
#endif
               memcpy(SaleRec.DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
               if (lPrice < 1000)
                  SaleRec.NoneSale_Flg = pDocTbl[iTmp].flag;
            } else if (bDebug || lPrice > 1000)
               LogMsg("*** Unknown DocCode: %s APN=%s sale price = %d", apTokens[MB_SALES_DOCCODE], apTokens[MB_SALES_ASMT], lPrice);
         }
      } else if (lPrice > 1000)
      {
         SaleRec.DocType[0] = '1';
      }

      // Save original DocCode
      vmemcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);

      // Transfer Type
      if (*apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_XFER_TYPE && asTranferTypes[iTmp].iLen > 0)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asTranferTypes[iTmp].acSrc, asTranferTypes[iTmp].iLen))
            {
               SaleRec.SaleCode[0] = asTranferTypes[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }

      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         if (pTmp = strstr(apTokens[MB_SALES_SELLER], "C/O"))
            *pTmp = 0;
         if (pTmp = strstr(apTokens[MB_SALES_SELLER], "J/T"))
            *pTmp = 0;
         if (pTmp = strstr(apTokens[MB_SALES_SELLER], "C/P"))
            *pTmp = 0;
         if (pTmp = strstr(apTokens[MB_SALES_SELLER], "S/S"))
            *pTmp = 0;

         iTmp = replStr(apTokens[MB_SALES_SELLER], " % ", " ");
         if (iTmp > SIZ_SELLER)
         {
            // Chop it down to fit seller
            if (pTmp = strrchr(apTokens[MB_SALES_SELLER], '&'))
               *pTmp = 0;
         }
         vmemcpy(SaleRec.Seller1, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);

         // Buyer
         iTmp = replStr(apTokens[MB_SALES_BUYER], " % ", " ");
         vmemcpy(SaleRec.Name1, apTokens[MB_SALES_BUYER], SALE_SIZ_BUYER, iTmp);

         // Skip if DocNum not available
         if (SaleRec.DocNum[0] > ' ')
         {
            SaleRec.CRLF[0] = 10;
            SaleRec.CRLF[1] = 0;
            fputs((char *)&SaleRec,fdOut);
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);

   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(27,2,C,EQ,\"  \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -2;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            // Save old cumsale file
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);
            iTmp = rename(acCSalFile, acTmpFile);

            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -2;
      } else
         iTmp = rename(acOutFile, acCSalFile);

      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acCSalFile, acTmpFile);
      }
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp == -2)
      LogMsg("***** Error sorting output file");
   else if (iTmp)
      LogMsg("***** Error renaming to %s", acCSalFile);

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                          output: %d.", lTmp);
   LogMsg("         Latetest recording date: %d.", lLastRecDate);
   return iTmp;
}

///********************************* Sjx_MergeLien *****************************
// *
// * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
// * Return 0 if successful, < 0 if error
// *        1 retired record, not use
// *
// *****************************************************************************/
//
//int Sjx_MergeLien3x(char *pOutbuf, char *pRollRec)
//{
//   char     acTmp[256], acTmp1[64], *pTmp;
//   long     lTmp;
//   double   dTmp;
//   int      iRet=0, iTmp;
//
//   // Replace null char with space
//   iRet = replNull(pRollRec, ' ', 0);
//
//   // Parse input rec
//   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
//   if (iRet < L3_CURRENTDOCDATE)
//   {
//      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
//      return -1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//
//   // Remove hyphen from APN
//   remChar(apTokens[L3_ASMT], '-');
//
//   // Start copying data
//   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));
//
//   // Copy ALT_APN
//   iTmp = strlen(apTokens[L3_FEEPARCEL]);
//   if (iTmp < iApnLen)
//   {
//      iRet = iApnLen-iTmp;
//      memcpy(pOutbuf+OFF_ALT_APN, "0000", iRet);
//      memcpy(pOutbuf+OFF_ALT_APN+iRet, apTokens[L3_FEEPARCEL], iTmp);
//   } else
//      memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], iTmp);
//
//   // Format APN
//   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//   // Create MapLink and output new record
//   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // Create index map link
//   if (getIndexPage(acTmp, acTmp1, &myCounty))
//      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//   // County code
//   memcpy(pOutbuf+OFF_CO_NUM, "11SJX", 5);
//
//   // status
//   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];
//
//   // TRA
//   lTmp = atol(apTokens[L3_TRA]);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%0.6d", lTmp);
//      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
//   }
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // Land
//   long lLand = atoi(apTokens[L3_LANDVALUE]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: Growing Impr, Fixture, PersProp, PPMH
//   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
//   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
//   long lGrow  = atoi(apTokens[L3_GROWING]);
//   long lPers  = atoi(apTokens[L3_PPVALUE]);
//   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
//   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//
//      if (lFixtr > 0)
//      {
//         sprintf(acTmp, "%d         ", lFixtr);
//         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
//      }
//      if (lFixtRP > 0)
//      {
//         sprintf(acTmp, "%d         ", lFixtRP);
//         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
//      }
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%d         ", lPers);
//         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
//      }
//      if (lGrow > 0)
//      {
//         sprintf(acTmp, "%d         ", lGrow);
//         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
//      }
//      if (lPP_MH > 0)
//      {
//         sprintf(acTmp, "%d         ", lPP_MH);
//         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
//      }
//   }
//
//   // Gross total
//   lTmp += (lLand+lImpr);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//   // Exemption
//   long lExe1 = atol(apTokens[L3_HOX]);
//   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
//   lTmp = lExe1+lExe2;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
//      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
//   }
//
//   iTmp = OFF_EXE_CD1;
//   if (lExe1 > 0)
//   {
//      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
//      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
//      iTmp = OFF_EXE_CD2;
//   } else
//      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
//
//   // Save exemption code
//   if (lExe2 > 0 && *apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
//      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));
//
//   // Legal
//   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);
//
//   // UseCode
//   if (*apTokens[L3_LANDUSE1] > ' ')
//   {
//      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
//
//      // Std Usecode
//      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
//   } else
//      memcpy(pOutbuf+OFF_USE_STD, "999", 3);
//
//   // Acres
//   dTmp = atof(apTokens[L3_ACRES]);
//   lTmp = atol(apTokens[L3_LANDSIZE]);
//   if (dTmp > 0.0)
//   {
//      // Lot Sqft
//      lTmp = (long)(dTmp * SQFT_PER_ACRE);
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      // Format Acres
//      lTmp = (long)(dTmp * ACRES_FACTOR);
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   } else if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      lTmp = (long)(lTmp*SQFT_MF_1000);
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   // AgPreserved
//   //if (*apTokens[L3_ISAGPRESERVE] == '1')
//   //   *(pOutbuf+OFF_AG_PRE) = 'Y';
//
//   // Owner
//   if (*apTokens[L3_MAILADDRESS1] == '%')
//      Sjx_MergeOwner(pOutbuf, apTokens[L3_OWNER], apTokens[L3_MAILADDRESS1]);
//   else
//      Sjx_MergeOwner(pOutbuf, apTokens[L3_OWNER], NULL);
//
//   // Situs
//   //Sjx_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);
//
//   // Mailing
//   Sjx_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);
//
//   // SetTaxcode, Prop8 flag, FullExe flag
//   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);
//
//   // Recorded Doc - 2016
//   if (*apTokens[L3_CURRENTDOCNUM] > '0')
//   {
//      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
//      if (pTmp)
//      {
//         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
//         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
//      }
//   }
//
//   //// Garage size
//   //dTmp = atof(apTokens[L3_GARAGESIZE]);
//   //if (dTmp > 0.0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_GAR_SQFT, dTmp);
//   //   memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
//   //   *(pOutbuf+OFF_PARK_TYPE) = '2';                 // GARAGE/CARPORT
//   //}
//
//   //// Number of parking spaces
//   //if (*apTokens[L3_GARAGE] == '0' || *apTokens[L3_GARAGE] == 'N')
//   //   *(pOutbuf+OFF_PARK_TYPE) = 'H';                 // None
//   //else if (*apTokens[L3_GARAGE] > '0' && *apTokens[L3_GARAGE] <= '9')
//   //{
//   //   iTmp = atol(apTokens[L3_GARAGE]);
//   //   sprintf(acTmp, "%d", iTmp);
//   //   vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
//   //   if (dTmp > 100)
//   //      *(pOutbuf+OFF_PARK_TYPE) = 'Z';              // Garage
//   //   else
//   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
//   //} else
//   //{
//   //   if (*(apTokens[L3_GARAGE]) == 'C')
//   //      *(pOutbuf+OFF_PARK_TYPE) = 'C';              // Carport
//   //   else if (*(apTokens[L3_GARAGE]) == 'A')
//   //   {
//   //      if (dTmp > 100)
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
//   //      else
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
//   //   } else if (!_memicmp(apTokens[L3_GARAGE], "DOU", 3))
//   //   {
//   //      *(pOutbuf+OFF_PARK_SPACE) = '2';
//   //      if (dTmp > 100)
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
//   //      else
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
//   //   } else if (!_memicmp(apTokens[L3_GARAGE], "TRI", 3))
//   //   {
//   //      *(pOutbuf+OFF_PARK_SPACE) = '3';
//   //      if (dTmp > 100)
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
//   //      else
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
//   //   } else if (!_memicmp(apTokens[L3_GARAGE], "SIN", 3))
//   //   {
//   //      *(pOutbuf+OFF_PARK_SPACE) = '1';
//   //      if (dTmp > 100)
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
//   //      else
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
//   //   } else if (!_memicmp(apTokens[L3_GARAGE], "GC", 2))
//   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT
//   //   else if (!_memicmp(apTokens[L3_GARAGE], "GS", 2))
//   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT ?
//   //   else if (!_memicmp(apTokens[L3_GARAGE], "DE", 2))
//   //      *(pOutbuf+OFF_PARK_TYPE) = 'D';              // Detached
//   //   else if (*(apTokens[L3_GARAGE]) == 'S')
//   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Space ?
//   //}
//
//   //// YearBlt
//   //lTmp = atol(apTokens[L3_YEARBUILT]);
//   //if (lTmp > 1800 && lTmp < lToyear)
//   //{
//   //   iTmp = sprintf(acTmp, "%d", lTmp);
//   //   memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
//   //}
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "006380014000", 9))
//   //   iTmp = 0;
//#endif
//
//   // Acres
//   dTmp = atof(apTokens[L3_ACRES]);
//   if (dTmp > 0.0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   // Lot size
//   lTmp = atol(apTokens[L3_LANDSIZE]);
//   if (lTmp > 10)
//   {
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//      if (!dTmp)
//      {
//         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(lTmp*SQFT_MF_1000));
//         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//      }
//   } else if (dTmp > 0.0)
//   {
//      //lTmp = (dTmp+0.0005)*SQFT_PER_ACRE;
//      lTmp = (long)(dTmp*SQFT_PER_ACRE);
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//   }
//
//   //// Total rooms
//   //iTmp = atol(apTokens[L3_TOTALROOMS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
//   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
//   //}
//
//   //// Stories
//   //iTmp = atol(apTokens[L3_STORIES]);
//   //if (iTmp > 0 && iTmp < 100)
//   //{
//   //   sprintf(acTmp, "%d.0", iTmp);
//   //   vmemcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
//   //}
//
//   //// Units
//   //iTmp = atol(apTokens[L3_UNITS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%d", iTmp);
//   //   vmemcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
//   //}
//
//   //// Beds
//   //iTmp = atol(apTokens[L3_BEDROOMS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_BEDS, iTmp);
//   //   memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
//   //}
//
//   //// Baths
//   //iTmp = atol(apTokens[L3_BATHS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_BATH_F, iTmp);
//   //   memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
//   //}
//
//   //// HBaths
//   //iTmp = atol(apTokens[L3_HALFBATHS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_BATH_H, iTmp);
//   //   memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
//   //}
//
//   //// Heating
//   //int iCmp;
//   //if (*apTokens[L3_HEATING] > ' ')
//   //{
//   //   iTmp = 0;
//   //   iCmp = -1;
//   //   while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_HEATING], asHeating_LDR[iTmp].acSrc, asHeating_LDR[iTmp].iLen)) > 0)
//   //      iTmp++;
//
//   //   if (!iCmp)
//   //      *(pOutbuf+OFF_HEAT) = asHeating_LDR[iTmp].acCode[0];
//   //}
//
//   //// Cooling
//   //if (*apTokens[L3_AC] == 'C')
//   //   *(pOutbuf+OFF_AIR_COND) = 'C';
//   //else if (*apTokens[L3_AC] > ' ')
//   //   LogMsg("*** Unknown A/C: %s", apTokens[L3_AC]);
//
//   //// Pool/Spa
//   //if (*apTokens[L3_POOLSPA] > ' ')
//   //{
//   //   iTmp = 0;
//   //   iCmp = -1;
//   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_POOLSPA], asPool_LDR[iTmp].acSrc, asPool_LDR[iTmp].iLen)) > 0)
//   //      iTmp++;
//
//   //   if (!iCmp)
//   //      *(pOutbuf+OFF_POOL) = asPool_LDR[iTmp].acCode[0];
//   //}
//
//   //// Fire place
//   //if (*apTokens[L3_FIREPLACE] > ' ')
//   //{
//   //   iTmp = 0;
//   //   iCmp = -1;
//   //   pTmp = _strupr(apTokens[L3_FIREPLACE]);
//   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_FIREPLACE], asFP_LDR[iTmp].acSrc, asFP_LDR[iTmp].iLen)) > 0)
//   //      iTmp++;
//
//   //   if (!iCmp)
//   //      *(pOutbuf+OFF_FIRE_PL) = asPool[iTmp].acCode[0];
//   //}
//
//   //// Quality Class
//   //acTmp1[0] = 0;
//   //if (*apTokens[L3_QUALITYCLASS] > '0' || strlen(apTokens[L3_QUALITYCLASS]) > 1)
//   //{
//   //   strcpy(acTmp, apTokens[L3_QUALITYCLASS]);
//   //   remCharEx(acTmp, " ,'?");
//   //   pTmp = _strupr(acTmp);
//
//   //   //memcpy(myCharRec.QualityClass, pTmp, strlen(pTmp));
//   //   if (acTmp[0] == 'A' && !isdigit(acTmp[1]))
//   //      acTmp1[0] = 'A';
//   //   else if (acTmp[0] == 'F' && !isdigit(acTmp[1]))
//   //      acTmp1[0] = 'F';
//   //   else if (!memcmp(pTmp, "POOR", 4) || !memcmp(pTmp, "LOW", 3))
//   //      acTmp1[0] = 'P';
//   //   else if (*pTmp == 'G')
//   //      acTmp1[0] = 'G';
//   //   else if (isalpha(*pTmp))
//   //   {
//   //      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
//   //      if (isdigit(acTmp[1]))
//   //         iRet = Quality2Code(&acTmp[1], acTmp1, NULL);
//   //      else if (isdigit(acTmp[2]))
//   //         iRet = Quality2Code(&acTmp[2], acTmp1, NULL);
//   //      else if (isalpha(acTmp[1]))
//   //      {
//   //         switch (acTmp[1])
//   //         {
//   //            case 'L':
//   //            case 'P':
//   //               acTmp1[0] = 'P';
//   //               break;
//   //            case 'A':
//   //               acTmp1[0] = 'A';
//   //               break;
//   //            case 'F':
//   //               acTmp1[0] = 'F';
//   //               break;
//   //            case 'G':
//   //               acTmp1[0] = 'G';
//   //               break;
//   //         }
//   //      }
//   //   } else if (isdigit(*pTmp))
//   //   {
//   //      iTmp = atol(pTmp);
//   //      if (iTmp < 100)
//   //         iRet = Quality2Code(pTmp, acTmp1, NULL);
//   //   }
//
//   //   if (acTmp1[0] > '0')
//   //      *(pOutbuf+OFF_BLDG_QUAL) = acTmp1[0];
//   //}
//
//   return 0;
//}
//
///******************************** Sjx_Load_LDR3 *****************************
// *
// * Load LDR 2016
// *
// ****************************************************************************/
//
//int Sjx_Load_LDR3x(int iFirstRec /* 1=create header rec */)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
//   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
//
//   HANDLE   fhOut;
//   FILE     *fdRoll;
//
//   int      iRet;
//   DWORD    nBytesWritten;
//   BOOL     bRet, bEof;
//   long     lRet=0, lCnt=0, lTmp;
//
//   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
//   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//
//   // Sort roll file on ASMT
//   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   lTmp = getFileDate(acRollFile);
//   if (lTmp < lToday)
//   {
//      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
//      if (!iRet)
//         return -1;
//   }
//
//   // Open roll file
//   LogMsg("Open Roll file %s", acRollFile);
//   fdRoll = fopen(acRollFile, "r");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acRollFile);
//      return -1;
//   }
//
//   // Open Char file
//   LogMsg("Open Char file %s", acCChrFile);
//   fdChar = fopen(acCChrFile, "r");
//   if (fdChar == NULL)
//   {
//      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
//      return -2;
//   }
//
//   // Open Situs file
//   LogMsg("Open Situs file %s", acSitusFile);
//   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   lTmp = getFileDate(acTmpFile);
//   if (lTmp < lToday)
//   {
//      strcpy(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
//      if (cDelim == '|')
//         strcat(acRec, "DEL(124) ");
//      lRet = sortFile(acSitusFile, acTmpFile, acRec);
//   }
//   fdSitus = fopen(acTmpFile, "r");
//   if (fdSitus == NULL)
//   {
//      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
//      return -2;
//   }
//
//   // Open Output file
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error opening output file: %s\n", acOutFile);
//      return -4;
//   }
//
//   // Output first header record
//   if (iFirstRec > 0)
//   {
//      memset(acBuf, '9', iRecLen);
//      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//   }
//
//   // Get 1st rec
//   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
//   if (*pTmp > '9')
//      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
//
//   bEof = (pTmp ? false:true);
//
//   // Init variables
//   iNoMatch=iBadCity=iBadSuffix=0;
//
//   // Merge loop
//   while (!feof(fdRoll))
//   {
//      lLDRRecCount++;
//      // Create new R01 record
//      iRet = Sjx_MergeLien3(acBuf, acRec);
//      if (!iRet)
//      {
//         // Merge Situs
//         if (fdSitus)
//            lRet = Sjx_MergeSitus(acBuf);
//
//         // Merge Char
//         if (fdChar)
//            lRet = Sjx_MergeStdChar(acBuf);
//
//         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//         if (!(++lCnt % 1000))
//            printf("\r%u", lCnt);
//
//         if (!bRet)
//         {
//            LogMsg("***** Error writing to output file at record %d\n", lCnt);
//            lRet = WRITE_ERR;
//            break;
//         }
//      }
//
//      // Get next roll record
//      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
//      if (!pTmp)
//         break;
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fdChar)
//      fclose(fdChar);
//   if (fdSitus)
//      fclose(fdSitus);
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   LogMsg("Total input records:        %u", lLDRRecCount);
//   LogMsg("Total output records:       %u", lCnt);
//   LogMsg("Total bad-city records:     %u", iBadCity);
//   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
//   LogMsg("Number of Char matched:     %u", lCharMatch);
//   LogMsg("Number of Char skiped:      %u", lCharSkip);
//   LogMsg("Number of Situs matched:    %u", lSitusMatch);
//   LogMsg("Number of Situs skiped:     %u\n", lSitusSkip);
//   printf("\nTotal output records: %u\n", lCnt);
//
//   lRecCnt = lCnt;
//   return 0;
//}

/********************************* Sjx_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sjx_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "39SJX", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SJX_Exemption);

   // Legal
   if (pTmp = strchr(apTokens[L3_PARCELDESCRIPTION], '|'))
   {
      LogMsg("*** Bad char found in LEGAL: %s [%s]", apTokens[L3_PARCELDESCRIPTION], apTokens[L3_ASMT]);
      remChar(apTokens[L3_PARCELDESCRIPTION], '|');
   }
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   strcpy(acTmp, apTokens[L3_LANDUSE1]);
   acTmp[SIZ_USE_CO] = 0;
   if (acTmp[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO);

      // Reformat UseCode for matching to StdUse table
      iTmp = iTrim(acTmp);
      if (iTmp == 2)
      {
         acTmp[3] = 0;
         acTmp[2] = acTmp[1];
         acTmp[1] = acTmp[0];
         acTmp[0] = '0';
      }
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 3, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   //if (*apTokens[L3_ISAGPRESERVE] == '1')
   //   *(pOutbuf+OFF_AG_PRE) = 'Y';

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "005100050", 9))
   //   iTmp = 0;
#endif

   //strcpy(acTmp, apTokens[L3_OWNER]);
   //if (pTmp = strchr(acTmp, 0x90))
   //   *pTmp = 'E';

   // Owner
   if (*apTokens[L3_MAILADDRESS1] == '%')
      Sjx_MergeOwner(pOutbuf, apTokens[L3_OWNER], apTokens[L3_MAILADDRESS1]);
   else
      Sjx_MergeOwner(pOutbuf, apTokens[L3_OWNER], NULL);

   // Situs
   //Sjx_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Sjx_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*apTokens[L3_CURRENTDOCNUM] > '0' && *(apTokens[L3_CURRENTDOCNUM]+4) == 'R' && isdigit(*(apTokens[L3_CURRENTDOCNUM]+6)))
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   }

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Lot size
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      if (!dTmp)
      {
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(lTmp*SQFT_MF_1000));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   } else if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   return 0;
}

/******************************** Sjx_Load_LDR3 *****************************
 *
 * Load LDR 2016
 *
 ****************************************************************************/

int Sjx_Load_LDR3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Value file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Lien file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   } else
      fdLienExt = NULL;

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lRet = sortFile(acSitusFile, acTmpFile, "S(1,13,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!feof(fdRoll))
   {
      // Create new R01 record
      iRet = Sjx_MergeLien3(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         if (fdLienExt)
            lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Situs
         if (fdSitus)
            lRet = Sjx_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Sjx_MergeStdChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/*********************************** loadSjx ********************************
 *
 * Options:
 *
 *    -CSJX -U -Xsi -T [-Xa] -O   (Normal update)
 *    -CSJX -L -Xl -Ms|Xsi [-Xa] -O (LDR)
 *
 ****************************************************************************/

int loadSjx(int iSkip)
{
   char  acTmpFile[_MAX_PATH], acTmp[_MAX_PATH];
   int   iRet=0;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;
   iApnLen = myCounty.iApnLen;

   if (bUpdPrevApn)
   {
      // If not defined, use current apn length
      iRet = GetPrivateProfileInt(myCounty.acCntyCode, "PrevApnLen", iApnLen, acIniFile);
      iRet = updatePrevApn(myCounty.acCntyCode, iRet, iSkip);
   }

   // One time task
   //char sInfile[_MAX_PATH], sOutfile[_MAX_PATH];
   //sprintf(sInfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Org");
   //sprintf(sOutfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   //iRet = Sjx_ConvertApnRoll(sInfile, sOutfile, 1900);
   //sprintf(sInfile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Org");
   //sprintf(sOutfile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   //iRet = Sjx_ConvertApnSale(sInfile, sOutfile);

   // Load tax
   //TC_SetDateFmt(MM_DD_YYYY_1);
   //if (iLoadTax == TAX_LOADING)              // -T or -Lt
   //{
   //   iRet = Load_TC(myCounty.acCntyCode, bTaxImport);
   //} else if (iLoadTax == TAX_UPDATING)   // -Ut
   //{
   //   iRet = Update_TC(myCounty.acCntyCode, bTaxImport);
   //}
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      char *pTmp, acOrgFile[_MAX_PATH], acInFile[_MAX_PATH], acOutFile[_MAX_PATH];
      char acTmp[_MAX_PATH], acZipFile[_MAX_PATH];

      // Check input
      iRet = GetIniString(myCounty.acCntyCode, "TaxZip", "", acZipFile, 255, acIniFile);
      if (iRet > 0 && !_access(acZipFile, 0))
      {
         // Unzip input
         strcpy(acTmp, acZipFile);
         pTmp = strrchr(acTmp, '\\');
         *pTmp = 0;

         // Unzip input files
         doZipInit();  
         setUnzipToFolder(acTmp);
         setReplaceIfExist(true);
         iRet = startUnzip(acZipFile);
         doZipShutdown();

         if (iRet)
         {
            LogMsg("***** Error unzipping %s.  Program terminated", acZipFile);
            return -1;
         }
      }
      sprintf(acOutFile, "%s\\%s\\Tax_Base.txt", acTmpPath, myCounty.acCntyCode);
      sprintf(acInFile, sTaxTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      lLastTaxFileDate = getFileDate(acInFile);
      iRet = isNewTaxFile(acInFile, myCounty.acCntyCode);
      if (iRet <= 0)
      {
         lLastTaxFileDate = 0;
         return iRet;
      }

      iRet = RebuildCsv(acInFile, acOutFile, cDelim, MB2_TX_RATEUSED);
      if (iRet > 0)
      {
         // Rename original file
         sprintf(acOrgFile, sTaxTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
         if (pTmp = strrchr(acOrgFile, '.'))
            strcpy(pTmp, ".bak");
         rename(acInFile, acOrgFile);
         CopyFile(acOutFile, acInFile, false);
      }

      // Load Tax_Base
      iRet = MB_Load_TaxBase(bTaxImport, true, iTaxGrp, 3);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load Tax_Agency (taxcodemstr)
         iRet |= MB_Load_TaxCodeMstr(bTaxImport, 5);

         // Load Tax_Items (taxcodes)
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport, 2);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport, 5);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Extract lien file
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acValueFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acValueFile, 0);      // 2016
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode);
   }

   // Extract Sale file from Sjx_Sales.txt to Sjx_Sale.sls
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Do not use confirmed sale price
      //bUseConfSalePrice = false;

      // Doc date - input format MM/DD/YYYY
      iRet = Sjx_CreateSCSale(MM_DD_YYYY_1, 0, 6, true, &SJX_DocCode[0]);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract Attom sale 
   if (iLoadFlag & EXTR_NRSAL)                      // -Xn
   {
      GetIniString(myCounty.acCntyCode, "NdcSale", "", acSalesFile, _MAX_PATH, acIniFile);
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      if (!_access(acSalesFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSalesFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSalesFile);
         return -1;
      }
   } else
   {
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   }

   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      iRet = Sjx_ConvStdChar(acCharFile);
      if (iRet <= 0)
      {
         LogMsg("***** Error extracting attributes data from %s", acCharFile);
         return -1;
      }
   }

   if (iLoadFlag & LOAD_LIEN)
   {
      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      // Test this one before 2018 LDR
      iRet = Sjx_Load_LDR3(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Sjx_Load_Roll(iSkip);
   }

   // Apply cum sale file to R01
   GetIniString(myCounty.acCntyCode, "NdcSalePlace", "1", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == '1')
   {
      // Apply NDC sale file to R01
      if (!iRet && (iLoadFlag & UPDT_XSAL))           // -Mn
      {
         // Apply Org_Ash.sls to R01 file
         GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
         iRet = ApplyCumSaleNR(iSkip, acTmpFile, SALE_USE_SCUPDXFR,CLEAR_OLD_SALE|CLEAR_OLD_XFER);
         if (!iRet)
            iLoadFlag |= LOAD_UPDT;
      }

      if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
      {
         // Apply Sjx_Sale.sls to R01 file
         iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
      }
   } else
   {
      if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
      {
         LogMsg("Merge county sale first..");
         // Apply Sjx_Sale.sls to R01 file
         iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
      }

      if (!iRet && (iLoadFlag & UPDT_XSAL))           // -Mn
      {
         LogMsg("Then merge Attom sale...");

         // Apply Org_Ash.sls to R01 file
         GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
         if (iLoadFlag & LOAD_LIEN)
            iRet = ApplyCumSaleNR(iSkip, acTmpFile, SALE_USE_SCUPDXFR);
         else
         {
            iRet = ApplyCumSaleNR(iSkip, acTmpFile, SALE_USE_SCUPDXFR, UPDATE_OWNER);
            if (!iRet)
               iLoadFlag |= LOAD_UPDT;
         }
      }
   }

   return iRet;
}
