#ifndef  _MERGEMAD_H_
#define  _MERGEMAD_H_

// Version 1 - 2014, 2017
#define  PV_APN               0
#define  PV_TRA               1
#define  PV_VST               2
#define  PV_ACRES             3
#define  PV_ENROLLED          4
#define  PV_LAND              5
#define  PV_IMPR              6
#define  PV_GROWIMPR          7
#define  PV_FIXTR             8
#define  PV_PPBUSINESS        9
#define  PV_PP_MH             10
#define  PV_PP_PI             11    // PP Penalty included
#define  PV_FIXTR_RP          12
#define  PV_EXE_AMT           13
#define  PV_NETVALUE          14
#define  PV_ALTDESC           15
#define  PV_ASSESSEENAME      16
#define  PV_ADDRESS1          17
#define  PV_ADDRESS2          18
#define  PV_ADDRESS3          19
#define  PV_ADDRESS4          20
#define  PV_BARCODEZIP        21
#define  PV_ADDRESSZIP        22
#define  PV_LIENOWNER         23
#define  PV_SITUS1            24
#define  PV_SITUS2            25

// Version 2 - 2015-2016
#define  PV2_APN               0
#define  PV2_VST					 1
#define  PV2_BASEVALUEEXISTS	 2
#define  PV2_RESVALUEEXISTS	 3
#define  PV2_PROP8VALUEEXISTS	 4
#define  PV2_TRA					 5
#define  PV2_ACRES				 6
#define  PV2_OWNERNAME			 7
#define  PV2_ADDRESS1			 8
#define  PV2_ADDRESS2			 9
#define  PV2_ADDRESS3			 10
#define  PV2_ADDRESS4			 11
#define  PV2_ADDRESSZIP			 12
#define  PV2_ASSESSEENAME		 13
#define  PV2_SITUS1				 14
#define  PV2_SITUS2				 15
#define  PV2_LAND					 16
#define  PV2_STRUCTURE			 17
#define  PV2_GROWING				 18
#define  PV2_FIXTURES			 19
#define  PV2_PP					 20
#define  PV2_PPMH					 21
#define  PV2_FIXRP				 22
#define  PV2_EXEMP				 23
#define  PV2_NETVALUE			 24
#define  PV2_BASELAND			 25
#define  PV2_BASESTRUCTURE		 26
#define  PV2_BASEGROWING		 27
#define  PV2_BASEFIXTURES		 28
#define  PV2_BASEPP				 29
#define  PV2_BASEPPMH			 30
#define  PV2_BASEFIXRP			 31
#define  PV2_BASENETVALUE		 32
#define  PV2_RESLAND				 33
#define  PV2_RESSTRUCTURE		 34
#define  PV2_RESGROWING			 35
#define  PV2_RESFIXTURES	    36
#define  PV2_RESPP				 37
#define  PV2_RESPPMH				 38
#define  PV2_RESFIXRP			 39
#define  PV2_RESNETVALUE		 40
#define  PV2_PROP8LAND			 41
#define  PV2_PROP8STRUCTURE	 42
#define  PV2_PROP8GROWING		 43
#define  PV2_PROP8FIXTURES		 44
#define  PV2_PROP8PP				 45
#define  PV2_PROP8PPMH			 46
#define  PV2_PROP8FIXRP			 47
#define  PV2_PROP8NETVALUE		 48
#define  PV2_APRCODE				 49

#define  MAD_CHAR_FEEPARCEL                0
#define  MAD_CHAR_POOLSPA                  1
#define  MAD_CHAR_CATTYPE                  2
#define  MAD_CHAR_QUALITYCLASS             3
#define  MAD_CHAR_YRBLT                    4
#define  MAD_CHAR_BUILDINGSIZE             5
#define  MAD_CHAR_ATTACHGARAGESF           6
#define  MAD_CHAR_DETACHGARAGESF           7
#define  MAD_CHAR_CARPORTSF                8
#define  MAD_CHAR_HEATING                  9
#define  MAD_CHAR_COOLINGCENTRALAC         10
#define  MAD_CHAR_COOLINGEVAPORATIVE       11
#define  MAD_CHAR_COOLINGROOMWALL          12
#define  MAD_CHAR_COOLINGWINDOW            13
#define  MAD_CHAR_STORIESCNT               14
#define  MAD_CHAR_UNITSCNT                 15
#define  MAD_CHAR_TOTALROOMS               16
#define  MAD_CHAR_EFFYR                    17
#define  MAD_CHAR_PATIOSF                  18
#define  MAD_CHAR_BEDROOMS                 19
#define  MAD_CHAR_BATHROOMS                20
#define  MAD_CHAR_HALFBATHS                21
#define  MAD_CHAR_FIREPLACE                22
#define  MAD_CHAR_ASMT                     23
#define  MAD_CHAR_BLDGSEQNUM               24
#define  MAD_CHAR_HASWELL                  25
#define  MAD_CHAR_LOTSQFT                  26
#define  MAD_CHAR_PARKSPACES               27

static XLAT_CODE  asHeating[] = 
{  // 11/16/2015
   // Value, lookup code, value length
   "01", "Z", 2,               // Central
   "02", "W", 2,               // Dual Pack
   "03", "F", 2,               // Electric
   "04", "C", 2,               // Floor Htg
   "05", "B", 2,               // Forced Air
   "06", "A", 2,               // Gravity Htg
   "07", "G", 2,               // Heat Pump
   "08", "X", 2,               // Perimeter Htg
   "09", "D", 2,               // Wall Htg
   "10", "R", 2,               // Firepl w/Wdstv
   "11", "R", 2,               // Fireplace
   "12", "I", 2,               // Radiator
   "13", "X", 2,               // Other
   "14", "L", 2,               // None
   "15", "P", 2,               // Panel Ray
   "99", "Q", 2,               // HeatingType - Structure Heating Type (2)
   "",   "",  0
};

static XLAT_CODE  asCooling[] =
{
   // Value, lookup code, value length
   "01", "C", 2,               // Central
   "02", "D", 2,               // Dual Pack
   "03", "Q", 2,               // Electric
   "04", "E", 2,               // Evap (Swamp)
   "05", "H", 2,               // Heat Pump
   "06", "A", 2,               // Refrig
   "07", "L", 2,               // Wall
   "08", "X", 2,               // Other
   "09", "N", 2,               // None
   "10", "T", 2,               // Slab AC
   "11", "W", 2,               // Evap (Window)
   "99", "Y", 2,               // CoolingType - Structure Cooling Type (2)
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "01", "P", 2,               // Swimming Pool
   "0",  "N", 1,               // None
   "1",  "C", 1,               // Pool/Spa
   "99", "P", 2,               // Pool
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{  // 11/16/2015
   // Value, lookup code, value length
   "01", "Y", 2,                // Fireplace
   "02", "W", 2,                // Wood Stove
   "03", "Z", 2,                // Zero Clearance
   "04", "G", 2,                // Gas Appliance
   "99", "O", 2,                // Other
   "",   "", 0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// I=Hospital
// S=School
// X=Misc exempt

IDX_TBL4 MAD_Exemption[] = 
{
   "E01", "H", 3,1,
   "E02", "V", 3,1,
   "E03", "R", 3,1,
   "E04", "C", 3,1,
   "E05", "W", 3,1,
   "E06", "E", 3,1,
   "E07", "V", 3,1,
   "E08", "D", 3,1,
   "E09", "V", 3,1,
   "E10", "V", 3,1,
   "E12", "V", 3,1,
   "E13", "X", 3,1,     // Aircraft
   "E14", "C", 3,1,     // CHURCH AND WELFARE
   "E15", "M", 3,1,
   "E16", "X", 3,1,
   "E17", "P", 3,1,
   "E18", "D", 3,1,
   "E19", "S", 3,1,
   "E20", "I", 3,1,
   "E21", "W", 3,1,     // WELFARE-LOW INC HOUSING
   "E22", "T", 3,1,
   "E28", "D", 3,1,
   "E31", "W", 3,1,
   "E40", "I", 3,1,
   "E41", "I", 3,1,
   "E60", "W", 3,1,
   "E61", "T", 3,1,
   "E70", "W", 3,1,
   "E71", "W", 3,1,
   "E72", "W", 3,1,
   "E73", "W", 3,1,
   "E80", "C", 3,1,
   "E81", "C", 3,1,
   "E82", "C", 3,1,
   "E86", "X", 3,1,
   "E90", "R", 3,1,
   "E91", "R", 3,1,
   "E92", "R", 3,1,
   "E97", "X", 3,1,
   "E98", "X", 3,1,
   "E99", "X", 3,1,
   "","",0,0
};

#endif