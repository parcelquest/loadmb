#ifndef  _MERGE_MEN_H
#define  _MERGE_MEN_H

// LDR "2024 Secured.txt"
#define  MEN_SR_PIN                         0
#define  MEN_SR_TAXYEAR                     1
#define  MEN_SR_NEXTYEAR                    2
#define  MEN_SR_ASMTTYPE                    3
#define  MEN_SR_ROLLTYPE                    4
#define  MEN_SR_TAG                         5
#define  MEN_SR_TRA                         5
#define  MEN_SR_CLASSCD                     6
#define  MEN_SR_CLASSCODE                   6
#define  MEN_SR_COUNTYNAME                  7
#define  MEN_SR_PRIMARYOWNER                8
#define  MEN_SR_RECIPIENT                   9
#define  MEN_SR_DELIVERYADDR                10
#define  MEN_SR_LASTLINE                    11
#define  MEN_SR_SITUSADDR                   12
#define  MEN_SR_SITUSCITY                   13
#define  MEN_SR_SITUSSTATE                  14
#define  MEN_SR_SITUSPOSTALCD               15
#define  MEN_SR_SITUSCOMPLETE               16
#define  MEN_SR_HOX                         17
#define  MEN_SR_DVX                         18
#define  MEN_SR_LDVX                        19
#define  MEN_SR_OTHEREXMPT                  20
#define  MEN_SR_ASSESSEDLAND                21
#define  MEN_SR_LAND                        21
#define  MEN_SR_ASSESSEDIMP                 22
#define  MEN_SR_IMPR                        22
#define  MEN_SR_ASSESSEDPERSONAL            23
#define  MEN_SR_PERSPROP                    23
#define  MEN_SR_ASSESSEDFIXTURES            24
#define  MEN_SR_FIXTURES                    24
#define  MEN_SR_ASSESSEDLIVIMP              25
#define  MEN_SR_LIVIMPR                     25
#define  MEN_SR_ASSESSEDFULL                26
#define  MEN_SR_GROSS                       26
#define  MEN_SR_TOTALEXEMPTIONS             27
#define  MEN_SR_PENALTY                     28
#define  MEN_SR_NETTAXABLE                  29
#define  MEN_SR_ASMTTRANID                  30
#define  MEN_SR_ASMTID                      31
#define  MEN_SR_REVOBJID                    32
#define  MEN_SR_MAILCOMPLETE                33
#define  MEN_SR_MAILCOMPLETEWITHATTENTION   34 
#define  MEN_SR_PERSONALSECUREDTOPARCEL     35
#define  MEN_SR_ISPUBLICLYOWNEDPROPERTY     36
#define  MEN_SR_ASSESSORMAILWITHCOUNTYOF    37
#define  MEN_SR_ASSESSORMAILWITHCOUNTY      38
#define  MEN_SR_ASSESSORMAILCOMPLETE        39
#define  MEN_SR_EQUALIZATIONBOARDCOMPLETE   40
#define  MEN_SR_ATTENTIONLINE               41
#define  MEN_SR_PRINTDATE                   42
#define  MEN_SR_FLDS                        43

// Aumentum layout
// CA-Mendocino-AsmtRoll.csv 11/12/2024
#define  R_REVOBJID                                0
#define  R_PIN                                     1
#define  R_APN                                     1
#define  R_CONVENYANCENUMBER                       2
#define  R_CONVEYANCEMONTH                         3
#define  R_CONVEYANCEYEAR                          4
#define  R_CLASSDESCRIPTION                        5
#define  R_ROLLCAT                                 6
#define  R_CLASSTYPE                               6
#define  R_BASEYEAR                                7
#define  R_BUSINESSUSECODEDESCRIPTION              8
#define  R_DOINGBUSINESSAS                         9
#define  R_MAILNAME                                10
#define  R_MAILADDRESS                             11
#define  R_MAILCITY                                12
#define  R_MAILSTATE                               13
#define  R_MAILZIPCODE                             14
#define  R_SITUSSTREETNUMBER                       15
#define  R_SITUSSTREETNUMBERSUFFIX                 16
#define  R_SITUSSTREETPREDIRECTIONAL               17
#define  R_SITUSSTREETNAME                         18
#define  R_SITUSSTREETTYPE                         19
#define  R_SITUSUNITNUMBER                         20
#define  R_SITUSZIPCODE                            21
#define  R_SITUSCITYNAME                           22
#define  R_SITUSSTATE                              23
#define  R_ASSESSMENTDESCRIPTION                   24
#define  R_TAXABILITYCODE                          25
#define  R_TRA                                     26
#define  R_GEO                                     27
#define  R_RECORDERSTYPE                           28
#define  R_RECORDERSBOOK                           29
#define  R_RECORDERSPAGE                           30
#define  R_LOTTYPE                                 31
#define  R_LOT                                     32
#define  R_BLOCK                                   33
#define  R_PORTIONDIRECTION                        34
#define  R_SUBDIVISIONNAMEORTRACT                  35
#define  R_ACREAGEAMOUNT                           36
#define  R_FIRSTSECTION                            37
#define  R_FIRSTTOWNSHIP                           38
#define  R_FIRSTRANGE                              39
#define  R_FIRSTRANGEDIRECTION                     40
#define  R_LEGALPARTY1                             41
#define  R_LEGALPARTY2                             42
#define  R_LEGALPARTY3                             43
#define  R_LEGALPARTY4                             44
#define  R_LAND                                    45
#define  R_IMPR                                    46
#define  R_LIVINGIMPROVEMENTS                      47
#define  R_PERSONALVALUE                           48
#define  R_TRADEFIXTURESAMOUNT                     49
#define  R_PERSONALPROPERTYAPPRAISED               50
#define  R_TENPERCENTASSESSEDPENALTY               51
#define  R_HOX                                     52
#define  R_VETERANSEXEMPTION                       53
#define  R_DISABLEDVETERANSEXEMPTION               54
#define  R_CHURCHEXEMPTION                         55
#define  R_RELIGIOUSEXEMPTION                      56
#define  R_CEMETERYEXEMPTION                       57
#define  R_PUBLICSCHOOLEXEMPTION                   58
#define  R_PUBLICLIBRARYEXEMPTION                  59
#define  R_PUBLICMUSEUMEXEMPTION                   60
#define  R_WELFARECOLLEGEEXEMPTION                 61
#define  R_WELFAREHOSPITALEXEMPTION                62
#define  R_WELFAREPRIVATEPAROCHIALSCHOOLEXEMPTION  63
#define  R_WELFARECHARITYRELIGIOUSEXEMPTION        64
#define  R_OTHEREXEMPTION                          65
#define  R_CAMEFROM                                66
#define  R_CLASSCODE                               67
#define  R_ISPUBLICLYOWNEDPROPERTY                 68
#define  R_ISTAXABLE                               68
#define  R_COLS                                    69

// CA-Mendocino-AsmtRoll.csv 02/02/2023 and LDR 2024 CA-Mendocino-AsmtRoll_ppa.txt
#define  R2_PIN                                     0
#define  R2_APN                                     0
#define  R2_CONVENYANCENUMBER                       1
#define  R2_CONVEYANCEMONTH                         2
#define  R2_CONVEYANCEYEAR                          3
#define  R2_CLASSDESCRIPTION                        4
#define  R2_ROLLCAT                                 5
#define  R2_BASEYEAR                                6
#define  R2_BUSINESSUSECODEDESCRIPTION              7
#define  R2_DOINGBUSINESSAS                         8
#define  R2_MAILNAME                                9
#define  R2_MAILADDRESS                             10
#define  R2_MAILCITY                                11
#define  R2_MAILSTATE                               12
#define  R2_MAILZIPCODE                             13
#define  R2_SITUSSTREETNUMBER                       14
#define  R2_SITUSSTREETNUMBERSUFFIX                 15
#define  R2_SITUSSTREETPREDIRECTIONAL               16
#define  R2_SITUSSTREETNAME                         17
#define  R2_SITUSSTREETTYPE                         18
#define  R2_SITUSUNITNUMBER                         19
#define  R2_SITUSZIPCODE                            20
#define  R2_SITUSCITYNAME                           21
#define  R2_SITUSSTATE                              22
#define  R2_ASSESSMENTDESCRIPTION                   23
#define  R2_ISTAXABLE                               24
#define  R2_TRA                                     25
#define  R2_GEO                                     26
#define  R2_RECORDERSTYPE                           27
#define  R2_RECORDERSBOOK                           28
#define  R2_RECORDERSPAGE                           29
#define  R2_LOTTYPE                                 30
#define  R2_LOT                                     31
#define  R2_BLOCK                                   32
#define  R2_PORTIONDIRECTION                        33
#define  R2_SUBDIVISIONNAMEORTRACT                  34
#define  R2_ACREAGEAMOUNT                           35
#define  R2_FIRSTSECTION                            36
#define  R2_FIRSTTOWNSHIP                           37
#define  R2_FIRSTRANGE                              38
#define  R2_FIRSTRANGEDIRECTION                     39
#define  R2_LEGALPARTY1                             40
#define  R2_LEGALPARTY2                             41
#define  R2_LEGALPARTY3                             42
#define  R2_LEGALPARTY4                             43
#define  R2_LAND                                    44
#define  R2_IMPR                                    45
#define  R2_LIVINGIMPROVEMENTS                      46
#define  R2_PERSONALVALUE                           47
#define  R2_TRADEFIXTURESAMOUNT                     48
#define  R2_PERSONALPROPERTYAPPRAISED               49
#define  R2_TENPERCENTASSESSEDPENALTY               50
#define  R2_HOX                                     51
#define  R2_VETERANSEXEMPTION                       52
#define  R2_DISABLEDVETERANSEXEMPTION               53
#define  R2_CHURCHEXEMPTION                         54
#define  R2_RELIGIOUSEXEMPTION                      55
#define  R2_CEMETERYEXEMPTION                       56
#define  R2_PUBLICSCHOOLEXEMPTION                   57
#define  R2_PUBLICLIBRARYEXEMPTION                  58
#define  R2_PUBLICMUSEUMEXEMPTION                   59
#define  R2_WELFARECOLLEGEEXEMPTION                 60
#define  R2_WELFAREHOSPITALEXEMPTION                61
#define  R2_WELFAREPRIVATEPAROCHIALSCHOOLEXEMPTION  62
#define  R2_WELFARECHARITYRELIGIOUSEXEMPTION        63
#define  R2_OTHEREXEMPTION                          64
#define  R2_CAMEFROM                                65
#define  R2_CLASSCODE                               66
#define  R2_CLASSSHORTDESC                          67
#define  R2_REVOBJID                                68
#define  R2_COLS                                    69
// 03/07/2023
//#define  R_PROP8ENROLLED                           69
//#define  R_TAXYEAR                                 70
//#define  R_COLS                                    71

// CA-Mendo-Sales.csv
#define  S_PIN                                     0
#define  S_GEO                                     1
#define  S_CONVEYANCEMONTH                         2
#define  S_CONVEYANCEYEAR                          3
#define  S_CONVEYANCENUMBER                        4
#define  S_CONVEYANCEDATE                          5
#define  S_FORMSENTDATE                            6
#define  S_SALELETTERDATE                          7
#define  S_TRANSFERTYPECODE                        8
#define  S_INDICATEDSALEPRICE                      9
#define  S_ADJUSTEDSALEPRICE                       10
#define  S_CLASSDESCRIPTION                        11
#define  S_BASEYEAR                                12
#define  S_BASEYEARFLAG                            13
#define  S_SITUS_LINE1                             14
#define  S_SITUS_LINE2                             15
#define  S_TOTALACRES                              16
#define  S_DESIGNTYPE101                           17
#define  S_CONSTRUCTIONTYPE                        18
#define  S_AREA_1_1                                19
#define  S_COSTAREA                                20
#define  S_TOTALAREA                               21
#define  S_EFFECTIVEAGE                            22
#define  S_CARPORT                                 23
#define  S_ATTGARAGE                               24
#define  S_DETGARAGE                               25
#define  S_POOLIND                                 26
#define  S_BEDROOMS                                27
#define  S_HALFBATHROOM                            28
#define  S_THREEQTRBATHROOM                        29
#define  S_FULLBATHROOM                            30
#define  S_TOTALBATHROOMS                          31
#define  S_SPECADJUSTMENT                          32
#define  S_LANDVALUE                               33
#define  S_STRUCTUREVALUE                          34
#define  S_TREEVINEVALUE                           35
#define  S_TRANSFERFEE                             36
#define  S_OFFICIALDOCID                           37
#define  S_REVOBJID                                38
#define  S_COLS                                    39

// CA-Mendocino-PropChar.csv 11/09/2022
#define  C_OBJID                                   0
#define  C_APN                                     0
#define  C_CLASSCODEDESCRIPTION                    1
#define  C_SITUSSTREETNUMBER                       2
#define  C_SITUSSTREETNUMBERSUFFIX                 3
#define  C_SITUSSTREETPREDIRECTIONAL               4
#define  C_SITUSSTREETNAME                         5
#define  C_SITUSSTREETTYPE                         6
#define  C_SITUSUNITNUMBER                         7
#define  C_SITUSZIPCODE                            8
#define  C_SITUSCITYNAME                           9
#define  C_STREETSURFACED                          10
#define  C_SEWER                                   11
#define  C_DOMESTICWATER                           12
#define  C_IRRIGATIONWATER                         13
#define  C_WELLWATER                               14
#define  C_UTILITYELECTRIC                         15
#define  C_UTILITYGAS                              16
#define  C_EFFYEARBUILT                            17
#define  C_YEARBUILT                               18
#define  C_NUMBEROFSTORIES                         19
#define  C_ROOFTYPE                                20
#define  C_GARAGETYPE                              21
#define  C_GARAGEAREA                              22
#define  C_CARPORTSIZE                             23
#define  C_GARAGE2TYPE                             24
#define  C_GARAGE2AREA                             25
#define  C_CARPORT2SIZE                            26
#define  C_HASFIREPLACE                            27
#define  C_BATHSFULL                               28
#define  C_BATHS3_4                                29
#define  C_BATHSHALF                               30
#define  C_TOTALBATHS                              31
#define  C_HASCENTRALHEATING                       32
#define  C_HASCENTRALCOOLING                       33
#define  C_DESIGNTYPE                              34
#define  C_CONSTRUCTIONTYPE                        35
#define  C_QUALITYCODE                             36
#define  C_SHAPECODE                               37
#define  C_LIVINGAREA                              38
#define  C_ACTUALAREA                              39
#define  C_HASPOOL                                 40
#define  C_BEDROOMCOUNT                            41
#define  C_FAIRWAY                                 42
#define  C_WATERFRONT                              43
#define  C_ACREAGE                                 44    // Added 04/10/2020
#define  C_LANDUNITTYPE                            45
#define  C_COLS                                    46

static XLAT_CODE  asRoofType[] =
{
   // Value, lookup code, value length
   "GABLE",                "G", 3,      
   "HIPVALLEY",            "O", 4,      
   "HIP",                  "H", 2,      
   "SHED",                 "K", 2,      
   "OTHER",                "J", 2,      
   "FLAT",                 "F", 2,      
   "GAMBREL",              "N", 3,      
   "AFRAME",               "A", 2,      
   "DORMER",               "K", 2,      
   "MANSARD",              "M", 2,      
   "PEAKED",               "J", 2,      // Other
   "",   "",  0
};

static XLAT_CODE  asConstType[] =
{
   // Value, lookup code, value length
   "MASONRY BEARING WALLS", "C", 2,
   "POLE FRAME",            "M", 2,    // Special
   "PRE-ENGINEERED STEEL",  "A", 2,
   "WOOD OR LIGHT STEEL",   "Z", 2,
   "",   "",  0
};

// CA-Mendo-CurrentAmounts.csv
//#define  CA_PIN                                    0
//#define  CA_BILLNUMBER                             1
//#define  CA_TAXBILLID                              2
//#define  CA_INST1DUEDATE                           3
//#define  CA_INST2DUEDATE                           4
//#define  CA_INST1TAXCHARGE                         5
//#define  CA_INST1PENALTYCHARGE                     6
//#define  CA_INST1FEECHARGE                         7
//#define  CA_INST1INTERESTCHARGE                    8
//#define  CA_INST1TAXPAYMENT                        9         // Paid value as negative
//#define  CA_INST1PENALTYPAYMENT                    10
//#define  CA_INST1FEEPAYMENT                        11
//#define  CA_INST1INTERESTPAYMENT                   12
//#define  CA_INST1AMOUNTDUE                         13
//#define  CA_INST2TAXCHARGE                         14
//#define  CA_INST2PENALTYCHARGE                     15
//#define  CA_INST2FEECHARGE                         16
//#define  CA_INST2INTERESTCHARGE                    17
//#define  CA_INST2TAXPAYMENT                        18
//#define  CA_INST2PENALTYPAYMENT                    19
//#define  CA_INST2FEEPAYMENT                        20
//#define  CA_INST2INTERESTPAYMENT                   21
//#define  CA_INST2AMOUNTDUE                         22
//#define  CA_FLDS                                   23
//#define  CA_COLS                                   23

// CA-Mendocino-CurrentAmounts.csv
// 02/10/2023
#define  CA_PIN                                    0
#define  CA_TAXYEAR                                1
#define  CA_TAXBILLID                              2
#define  CA_BILLNUMBER                             3
#define  CA_INST1DUEDATE                           4
#define  CA_INST2DUEDATE                           5
#define  CA_INST1TAXCHARGE                         6
#define  CA_INST1PENALTYCHARGE                     7
#define  CA_INST1FEECHARGE                         8
#define  CA_INST1INTERESTCHARGE                    9
#define  CA_INST1TAXPAYMENT                        10         // Paid value as negative
#define  CA_INST1PENALTYPAYMENT                    11
#define  CA_INST1FEEPAYMENT                        12
#define  CA_INST1INTERESTPAYMENT                   13
#define  CA_INST1AMOUNTDUE                         14
#define  CA_INST2TAXCHARGE                         15
#define  CA_INST2PENALTYCHARGE                     16
#define  CA_INST2FEECHARGE                         17
#define  CA_INST2INTERESTCHARGE                    18
#define  CA_INST2TAXPAYMENT                        19
#define  CA_INST2PENALTYPAYMENT                    20
#define  CA_INST2FEEPAYMENT                        21
#define  CA_INST2INTERESTPAYMENT                   22
#define  CA_INST2AMOUNTDUE                         23
#define  CA_FLDS                                   24
#define  CA_COLS                                   24

// CA-Mendo-CurrentDistricts.csv
#define  CD_PIN                                    0
#define  CD_TAXRATEAREA                            1
#define  CD_TAXYEAR                                2
#define  CD_BILLNUMBER                             3
#define  CD_BILLTYPE                               4
#define  CD_TAXBILLID                              5
#define  CD_DETAILS                                6
#define  CD_TAXCODE                                7
#define  CD_RATE                                   8
#define  CD_TOTAL                                  9
#define  CD_FLDS                                   10
#define  CD_COLS                                   10

// CA-Mendocino-CurrentDistricts.csv
//#define  CD_REVOBJID                               0
//#define  CD_PIN                                    1
//#define  CD_TAXRATEAREA                            2
//#define  CD_TAXYEAR                                3
//#define  CD_BILLNUMBER                             4
//#define  CD_BILLTYPE                               5
//#define  CD_TAXBILLID                              6
//#define  CD_DETAILS                                7
//#define  CD_TAXCODE                                8
//#define  CD_RATE                                   9
//#define  CD_TOTAL                                  10
//#define  CD_FLDS                                   11
//#define  CD_COLS                                   11

// CA-Mendo-PriorYear.csv
#define  PY_PIN                                    0
#define  PY_DEFAULTNUMBER                          1
#define  PY_DEFAULTYEAR                            2
#define  PY_BILLTYPE                               3
#define  PY_AMOUNTDUE                              4
#define  PY_FLDS                                   5
#define  PY_COLS                                   5

// 2021 Roll corretion
#define  MEN_COR_AIN                   0
#define  MEN_COR_PIN                   1
#define  MEN_COR_TAXYEAR               2
#define  MEN_COR_NEXTYEAR              3
#define  MEN_COR_ASMTTYPE              4
#define  MEN_COR_ROLLTYPE              5
#define  MEN_COR_TAG                   6
#define  MEN_COR_CLASSCD               7
#define  MEN_COR_COUNTYNAME            8
#define  MEN_COR_PRIMARYOWNER          9
#define  MEN_COR_RECIPIENT             10
#define  MEN_COR_DELIVERYADDR          11
#define  MEN_COR_LASTLINE              12
#define  MEN_COR_SITUSADDR             13
#define  MEN_COR_SITUSCITY             14
#define  MEN_COR_SITUSPOSTALCD         15
#define  MEN_COR_SITUSCOMPLETE         16
#define  MEN_COR_TOTALMARKET           17
#define  MEN_COR_TOTALFBYV             18
#define  MEN_COR_HOX                   19
#define  MEN_COR_DVX                   20
#define  MEN_COR_LDVX                  21
#define  MEN_COR_OTHEREXMPT            22
#define  MEN_COR_ASSDLAND              23
#define  MEN_COR_ASSDIMP               24
#define  MEN_COR_ASSDPERSONAL          25
#define  MEN_COR_ASSDFIXTURES          26
#define  MEN_COR_ASSDLIVIMP            27
#define  MEN_COR_ASSESSEDFULL          28
#define  MEN_COR_TOTALEXMPT            29
#define  MEN_COR_NETTAXABLE            30
#define  MEN_COR_ASMTTRANID            31
#define  MEN_COR_ID                    32
#define  MEN_COR_REVOBJID              33
#define  MEN_COR_PRINTDATE             34
#define  MEN_COR_AMAILWITHCOUNTYOF     35
#define  MEN_COR_AMAILWITHCOUNTY       36
#define  MEN_COR_AMAILCOMPLETE         37
#define  MEN_COR_ATTENTIONLINE         38
#define  MEN_COR_MAILCOMPLETE          39
#define  MEN_COR_MAILCOMPWITHATTN      40
#define  MEN_COR_FLDS                  41

// Current secured tax file
#define  MEN_SEC_APN                0
#define  MEN_SEC_TRA                1
#define  MEN_SEC_NAME1              2
#define  MEN_SEC_BILL_NUM           3
#define  MEN_SEC_NAME2              4
#define  MEN_SEC_CAREOF             5
#define  MEN_SEC_EXCEPTION          6
#define  MEN_SEC_VESTING            7
#define  MEN_SEC_DBA                8
#define  MEN_SEC_MAR_ONE            9
#define  MEN_SEC_M_ADDR             10
#define  MEN_SEC_M_CITYST           11
#define  MEN_SEC_M_ZIP              12
#define  MEN_SEC_LANDUSE            13
#define  MEN_SEC_S_ADDR             14
#define  MEN_SEC_CORTAC             15
#define  MEN_SEC_LAND               16
#define  MEN_SEC_IMPR               17
#define  MEN_SEC_PERSPROP           18
#define  MEN_SEC_FIXTR              19
#define  MEN_SEC_OTHER              20
#define  MEN_SEC_HOME_EXE           21
#define  MEN_SEC_MISC_EXE           22
#define  MEN_SEC_ECODE              23    // Exe_Code
#define  MEN_SEC_EVET               24
#define  MEN_SEC_EMISC              25
#define  MEN_SEC_ACRES              26
#define  MEN_SEC_STS_DATE           27    // Default date offset 474
#define  MEN_SEC_SEG_FLAG           28
#define  MEN_SEC_GROSS_TAX          29
#define  MEN_SEC_HO_REF             30
#define  MEN_SEC_MISC_REF           31
#define  MEN_SEC_NET_TAX            32    // Decimal value
#define  MEN_SEC_TOT_BOND           33
#define  MEN_SEC_TOT_DIR            34
#define  MEN_SEC_TOT_PEN            35
#define  MEN_SEC_TOT_RATE           36
#define  MEN_SEC_AS_CODE_1          37
#define  MEN_SEC_AMT1_1             38
#define  MEN_SEC_AMT2_1             39
#define  MEN_SEC_AS_CODE_2          40
#define  MEN_SEC_AMT1_2             41
#define  MEN_SEC_AMT2_2             42
#define  MEN_SEC_AS_CODE_3          43
#define  MEN_SEC_AMT1_3             44
#define  MEN_SEC_AMT2_3             45
#define  MEN_SEC_AS_CODE_4          46
#define  MEN_SEC_AMT1_4             47
#define  MEN_SEC_AMT2_4             48
#define  MEN_SEC_AS_CODE_5          49
#define  MEN_SEC_AMT1_5             50
#define  MEN_SEC_AMT2_5             51
#define  MEN_SEC_AS_CODE_6          52
#define  MEN_SEC_AMT1_6             53
#define  MEN_SEC_AMT2_6             54
#define  MEN_SEC_AS_CODE_7          55
#define  MEN_SEC_AMT1_7             56
#define  MEN_SEC_AMT2_7             57
#define  MEN_SEC_AS_CODE_8          58
#define  MEN_SEC_AMT1_8             59
#define  MEN_SEC_AMT2_8             60
#define  MEN_SEC_AS_CODE_9          61
#define  MEN_SEC_AMT1_9             62
#define  MEN_SEC_AMT2_9             63
#define  MEN_SEC_AS_CODE_10         64
#define  MEN_SEC_AMT1_10            65
#define  MEN_SEC_AMT2_10            66
#define  MEN_SEC_AS_CODE_11         67
#define  MEN_SEC_AMT1_11            68
#define  MEN_SEC_AMT2_11            69
#define  MEN_SEC_AS_CODE_12         70
#define  MEN_SEC_AMT1_12            71
#define  MEN_SEC_AMT2_12            72
#define  MEN_SEC_AS_CODE_13         73
#define  MEN_SEC_AMT1_13            74
#define  MEN_SEC_AMT2_13            75
#define  MEN_SEC_AS_CODE_14         76
#define  MEN_SEC_AMT1_14            77
#define  MEN_SEC_AMT2_14            78
#define  MEN_SEC_AS_CODE_15         79
#define  MEN_SEC_AMT1_15            80
#define  MEN_SEC_AMT2_15            81

#define  MEN_SEC_I1_STAT            82    // P(aid), blank=unpaid, N(one) tax, C(orrection/cancel) offset 964
#define  MEN_SEC_I1_TAX             83    // V99
#define  MEN_SEC_I1_PEN             84    // V99
#define  MEN_SEC_I1_DUEDATE         85
#define  MEN_SEC_I1_PAIDDATE        86
#define  MEN_SEC_I1_BATCH           87

#define  MEN_SEC_I2_STAT            88
#define  MEN_SEC_I2_TAX             89
#define  MEN_SEC_I2_PEN             90
#define  MEN_SEC_I2_COST            91
#define  MEN_SEC_I2_DUEDATE         92
#define  MEN_SEC_I2_PAIDDATE        93
#define  MEN_SEC_I2_BATCH           94

#define  MEN_SEC_PAYOR              95
#define  MEN_SEC_CORR_TYPE          96
#define  MEN_SEC_BILL_ADJ           97
#define  MEN_SEC_ORIG_PRCL          98
#define  MEN_SEC_DMND_STMNT         99
#define  MEN_SEC_FD_02              100
#define  MEN_SEC_DRCT_ASSMT_RMVD    101
#define  MEN_SEC_LANDSCAPE          102
#define  MEN_SEC_ST_LTG_SUBDIV      103
#define  MEN_SEC_LAST_ACT_DATE      104
#define  MEN_SEC_PEN_CODE           105
#define  MEN_SEC_PEND_APPR          106
#define  MEN_SEC_BRUP               107

// Delinquent file
#define  MEN_DELQ_NAME              0
#define  MEN_DELQ_PRCL              1
#define  MEN_DELQ_NAME2             2
#define  MEN_DELQ_CAREOF            3
#define  MEN_DELQ_ADDR              4
#define  MEN_DELQ_CTY_ST            5
#define  MEN_DELQ_ZIP               6
#define  MEN_DELQ_ORIG_OWNR         7
#define  MEN_DELQ_ORIG_PRCL         8
#define  MEN_DELQ_A0_ASSMT_NBR      9
#define  MEN_DELQ_A0_YR             10
#define  MEN_DELQ_A0_PRCL           11    // Orig. parcel
#define  MEN_DELQ_A0_TYPE           12    // Secu(R)ed, (S)uplemental
#define  MEN_DELQ_A0_PAY_FLAG       13    // N ???
#define  MEN_DELQ_A0_AREA           14    // TRA
#define  MEN_DELQ_A0_TAX            15
#define  MEN_DELQ_A0_PEN            16
#define  MEN_DELQ_A0_COST           17
#define  MEN_DELQ_A0_AUDIT_AMT      18
#define  MEN_DELQ_A1_ASSMT_NBR      19
#define  MEN_DELQ_A1_YR             20
#define  MEN_DELQ_A1_PRCL           21
#define  MEN_DELQ_A1_TYPE           22
#define  MEN_DELQ_A1_PAY_FLAG       23
#define  MEN_DELQ_A1_AREA           24
#define  MEN_DELQ_A1_TAX            25
#define  MEN_DELQ_A1_PEN            26
#define  MEN_DELQ_A1_COST           27
#define  MEN_DELQ_A1_AUDIT_AMT      28
#define  MEN_DELQ_A2_ASSMT_NBR      29
#define  MEN_DELQ_A2_YR             30
#define  MEN_DELQ_A2_PRCL           31
#define  MEN_DELQ_A2_TYPE           32
#define  MEN_DELQ_A2_PAY_FLAG       33
#define  MEN_DELQ_A2_AREA           34
#define  MEN_DELQ_A2_TAX            35
#define  MEN_DELQ_A2_PEN            36
#define  MEN_DELQ_A2_COST           37
#define  MEN_DELQ_A2_AUDIT_AMT      38
#define  MEN_DELQ_A3_ASSMT_NBR      39
#define  MEN_DELQ_A3_YR             40
#define  MEN_DELQ_A3_PRCL           41
#define  MEN_DELQ_A3_TYPE           42
#define  MEN_DELQ_A3_PAY_FLAG       43
#define  MEN_DELQ_A3_AREA           44
#define  MEN_DELQ_A3_TAX            45
#define  MEN_DELQ_A3_PEN            46
#define  MEN_DELQ_A3_COST           47
#define  MEN_DELQ_A3_AUDIT_AMT      48
#define  MEN_DELQ_A4_ASSMT_NBR      49
#define  MEN_DELQ_A4_YR             50
#define  MEN_DELQ_A4_PRCL           51
#define  MEN_DELQ_A4_TYPE           52
#define  MEN_DELQ_A4_PAY_FLAG       53
#define  MEN_DELQ_A4_AREA           54
#define  MEN_DELQ_A4_TAX            55
#define  MEN_DELQ_A4_PEN            56
#define  MEN_DELQ_A4_COST           57
#define  MEN_DELQ_A4_AUDIT_AMT      58
#define  MEN_DELQ_A5_ASSMT_NBR      59
#define  MEN_DELQ_A5_YR             60
#define  MEN_DELQ_A5_PRCL           61
#define  MEN_DELQ_A5_TYPE           62
#define  MEN_DELQ_A5_PAY_FLAG       63
#define  MEN_DELQ_A5_AREA           64
#define  MEN_DELQ_A5_TAX            65
#define  MEN_DELQ_A5_PEN            66
#define  MEN_DELQ_A5_COST           67
#define  MEN_DELQ_A5_AUDIT_AMT      68
#define  MEN_DELQ_A6_ASSMT_NBR      69
#define  MEN_DELQ_A6_YR             70
#define  MEN_DELQ_A6_PRCL           71
#define  MEN_DELQ_A6_TYPE           72
#define  MEN_DELQ_A6_PAY_FLAG       73
#define  MEN_DELQ_A6_AREA           74
#define  MEN_DELQ_A6_TAX            75
#define  MEN_DELQ_A6_PEN            76
#define  MEN_DELQ_A6_COST           77
#define  MEN_DELQ_A6_AUDIT_AMT      78
#define  MEN_DELQ_A7_ASSMT_NBR      79
#define  MEN_DELQ_A7_YR             80
#define  MEN_DELQ_A7_PRCL           81
#define  MEN_DELQ_A7_TYPE           82
#define  MEN_DELQ_A7_PAY_FLAG       83
#define  MEN_DELQ_A7_AREA           84
#define  MEN_DELQ_A7_TAX            85
#define  MEN_DELQ_A7_PEN            86
#define  MEN_DELQ_A7_COST           87
#define  MEN_DELQ_A7_AUDIT_AMT      88
#define  MEN_DELQ_A8_ASSMT_NBR      89
#define  MEN_DELQ_A8_YR             90
#define  MEN_DELQ_A8_PRCL           91
#define  MEN_DELQ_A8_TYPE           92
#define  MEN_DELQ_A8_PAY_FLAG       93
#define  MEN_DELQ_A8_AREA           94
#define  MEN_DELQ_A8_TAX            95
#define  MEN_DELQ_A8_PEN            96
#define  MEN_DELQ_A8_COST           97
#define  MEN_DELQ_A8_AUDIT_AMT      98
#define  MEN_DELQ_A9_ASSMT_NBR      99
#define  MEN_DELQ_A9_YR             100
#define  MEN_DELQ_A9_PRCL           101
#define  MEN_DELQ_A9_TYPE           102
#define  MEN_DELQ_A9_PAY_FLAG       103
#define  MEN_DELQ_A9_AREA           104
#define  MEN_DELQ_A9_TAX            105
#define  MEN_DELQ_A9_PEN            106
#define  MEN_DELQ_A9_COST           107
#define  MEN_DELQ_A9_AUDIT_AMT      108
#define  MEN_DELQ_STATE_FEE         109
#define  MEN_DELQ_TAX_DEF_AMT       110
#define  MEN_DELQ_TAX_DEF_DATE      111
#define  MEN_DELQ_TAX_DEF_NR        112
#define  MEN_DELQ_PLAN_DATE         113
#define  MEN_DELQ_ACT_DATE          114
#define  MEN_DELQ_RDM_DATE          115
#define  MEN_DELQ_RDM_AMT           116
#define  MEN_DELQ_TOT_TAX           117
#define  MEN_DELQ_TOT_PEN           118
#define  MEN_DELQ_TOT_COST          119
#define  MEN_DELQ_TDL_FEE           120
#define  MEN_DELQ_REDEEM_FEE        121
#define  MEN_DELQ_LAST_ACT          122      // P=Paid, R=Redeem (bad check)
#define  MEN_DELQ_ACT_BATCH         123
#define  MEN_DELQ_ORIG_PLAN_DATE    124
#define  MEN_DELQ_BRUP              125
#define  MEN_DELQ_FD_02             126
#define  MEN_DELQ_DRCT_ASSMT_RMVD   127      // No data
#define  MEN_DELQ_ADJ_CODE          128      // No data
#define  MEN_DELQ_ADJ_AMT           129
#define  MEN_DELQ_MISC_FLAG         130      // A,V,S=Skip all penalties
#define  MEN_DELQ_RDM_SW            131
#define  MEN_DELQ_RDM_STAT          132      // P=paid
#define  MEN_DELQ_INST_INT_X        133
#define  MEN_DELQ_REM               134

#define  OFF_DELQ_NAME              1
#define  OFF_DELQ_PRCL              32-1
#define  OFF_DELQ_NAME2             47
#define  OFF_DELQ_CAREOF            78
#define  OFF_DELQ_ADDR              104
#define  OFF_DELQ_CTY_ST            135
#define  OFF_DELQ_ZIP               161
#define  OFF_DELQ_ORIG_OWNR         172
#define  OFF_DELQ_ORIG_PRCL         233
#define  OFF_DELQ_A0_ASSMT_NBR      244
#define  OFF_DELQ_A0_YR             251
#define  OFF_DELQ_A0_PRCL           256    
#define  OFF_DELQ_A0_TYPE           267
#define  OFF_DELQ_A0_PAY_FLAG       269
#define  OFF_DELQ_A0_AREA           271
#define  OFF_DELQ_A0_TAX            278
#define  OFF_DELQ_A0_PEN            288
#define  OFF_DELQ_A0_COST           297
#define  OFF_DELQ_A0_AUDIT_AMT      303
#define  OFF_DELQ_A1_ASSMT_NBR      313
#define  OFF_DELQ_A2_ASSMT_NBR      382
#define  OFF_DELQ_A3_ASSMT_NBR      451
#define  OFF_DELQ_A4_ASSMT_NBR      520
#define  OFF_DELQ_A5_ASSMT_NBR      589
#define  OFF_DELQ_A6_ASSMT_NBR      658
#define  OFF_DELQ_A7_ASSMT_NBR      727
#define  OFF_DELQ_A8_ASSMT_NBR      796
#define  OFF_DELQ_A9_ASSMT_NBR      865
#define  OFF_DELQ_STATE_FEE         934
#define  OFF_DELQ_TAX_DEF_AMT       940
#define  OFF_DELQ_TAX_DEF_DATE      950
#define  OFF_DELQ_TAX_DEF_NR        959
#define  OFF_DELQ_PLAN_DATE         964
#define  OFF_DELQ_ACT_DATE          973
#define  OFF_DELQ_RDM_DATE          982
#define  OFF_DELQ_RDM_AMT           991
#define  OFF_DELQ_TOT_TAX           1001
#define  OFF_DELQ_TOT_PEN           1012
#define  OFF_DELQ_TOT_COST          1021
#define  OFF_DELQ_TDL_FEE           1030
#define  OFF_DELQ_REDEEM_FEE        1033
#define  OFF_DELQ_LAST_ACT          1036
#define  OFF_DELQ_ACT_BATCH         1038
#define  OFF_DELQ_ORIG_PLAN_DATE    1045
#define  OFF_DELQ_BRUP              1054
#define  OFF_DELQ_FD_02             1070
#define  OFF_DELQ_DRCT_ASSMT_RMVD   1091
#define  OFF_DELQ_ADJ_CODE          1093
#define  OFF_DELQ_ADJ_AMT           1095
#define  OFF_DELQ_MISC_FLAG         1106
#define  OFF_DELQ_RDM_SW            1108
#define  OFF_DELQ_RDM_STAT          1110
#define  OFF_DELQ_INST_INT_X        1112
#define  OFF_DELQ_REM               1122

// LDR 2022 - ar_8_25_secured.csv
#define  MEN_L_PIN                            0
#define  MEN_L_CONVENYANCENUMBER              1     // Store 7-digit DocNum only (1975-0000181-D)
#define  MEN_L_CONVEYANCEMONTH                2
#define  MEN_L_CONVEYANCEYEAR                 3
#define  MEN_L_REALPROPERTYUSECODE            4
#define  MEN_L_BASEYEAR                       5
#define  MEN_L_BUSINESSUSECODEDESCRIPTION     6
#define  MEN_L_DOINGBUSINESSAS                7
#define  MEN_L_MAILNAME                       8
#define  MEN_L_M_ADDR1                        9
#define  MEN_L_M_CITY                         10
#define  MEN_L_M_STATE                        11
#define  MEN_L_M_ZIP                          12
#define  MEN_L_S_STRNUM                       13
#define  MEN_L_S_STRFRA                       14
#define  MEN_L_S_DIR                          15
#define  MEN_L_S_STRNAME                      16
#define  MEN_L_S_STRSFX                       17
#define  MEN_L_S_UNIT                         18
#define  MEN_L_S_ZIP                          19
#define  MEN_L_S_CITY                         20
#define  MEN_L_ASSESSMENTDESCRIPTION          21
#define  MEN_L_ISTAXABLE                      22    // Y/N
#define  MEN_L_TRA                            23
#define  MEN_L_GEO                            24
#define  MEN_L_RECORDERSTYPE                  25
#define  MEN_L_RECORDERSBOOK                  26
#define  MEN_L_RECORDERSPAGE                  27
#define  MEN_L_LOTTYPE                        28
#define  MEN_L_LOT                            29
#define  MEN_L_BLOCK                          30
#define  MEN_L_PORTIONDIRECTION               31
#define  MEN_L_SUBDIVISIONNAMEORTRACT         32
#define  MEN_L_ACREAGEAMOUNT                  33
#define  MEN_L_FIRSTSECTION                   34
#define  MEN_L_FIRSTTOWNSHIP                  35
#define  MEN_L_FIRSTRANGE                     36
#define  MEN_L_FIRSTRANGEDIRECTION            37
#define  MEN_L_LEGALPARTY1                    38
#define  MEN_L_LEGALPARTY2                    39
#define  MEN_L_LEGALPARTY3                    40
#define  MEN_L_LEGALPARTY4                    41
#define  MEN_L_LAND                           42
#define  MEN_L_IMPROVEMENTS                   43
#define  MEN_L_LIVINGIMPROVEMENTS             44
#define  MEN_L_PERSONALVALUE                  45
#define  MEN_L_TRADEFIXTURESAMOUNT            46
#define  MEN_L_PERSONALPROPERTYAPPRAISED      47 
#define  MEN_L_TENPERCENTASSESSEDPENALTY      48 
#define  MEN_L_HOX                            49
#define  MEN_L_VETERANSEXEMPTION              50
#define  MEN_L_DISABLEDVETERANSEXEMPTION      51
#define  MEN_L_CHURCHEXEMPTION                52
#define  MEN_L_RELIGIOUSEXEMPTION             53
#define  MEN_L_CEMETERYEXEMPTION              54
#define  MEN_L_PUBLICSCHOOLEXEMPTION          55
#define  MEN_L_PUBLICLIBRARYEXEMPTION         56
#define  MEN_L_PUBLICMUSEUMEXEMPTION          57
#define  MEN_L_WELFAREHOSPITALEXEMPTION       58
#define  MEN_L_WPP_SCHOOLEXEMPTION            59
#define  MEN_L_WC_RELIGIOUSEXEMPTION          60
#define  MEN_L_OTHEREXEMPTION                 61
#define  MEN_L_CAMEFROM                       62
#define  MEN_L_CLASSCODE                      63
#define  MEN_L_CLASSDESC                      64
#define  MEN_L_REVOBJID                       65
#define  MEN_L_COLS                           66

// 2021 ANNUAL ROLLS - SEPARATED.TXT
//#define  L_APN                      0
//#define  L_TAXYEAR                  1
//#define  L_NEXTYEAR                 2
//#define  L_ASMTTYPE                 3
//#define  L_MEN_ROLLTYPE                 4
//#define  L_MEN_TRA                      5
//#define  L_USECODEDESC              6
//#define  L_OWNERX                   7        // Owner name - last name first, mostly blank
//#define  L_MEN_OWNER                    8
//#define  L_MADDR1                   9
//#define  L_MADDR2                   10
//#define  L_SADDR1                   11
//#define  L_SCITY                    12
//#define  L_SZIP                     13
//#define  L_SITUS                    14
//#define  L_TOTALMARKET              15
//#define  L_TOTALFBYV                16
//#define  L_HOEXE                    17
//#define  L_EXE1                     18
//#define  L_EXE2                     19
//#define  L_OTHEREXE                 20
//#define  L_LAND                     21
//#define  L_IMPR                     22
//#define  L_PPVAL                    23
//#define  L_FIXTURES                 24
//#define  L_LIVINGIMPR               25
//#define  L_ASSDVAL                  26
//#define  L_TOTALEXE                 27
//#define  L_NETVAL                   28
//#define  L_REVOBJID                 29
//#define  L_ASMTTRANID               30
//#define  L_ASMTID                   31
//#define  L_PRINTDATE                32
//#define  L_FLDS                     33

USEXREF  Men_UseTbl[] =
{
   "00","120",      //VACANT - RESIDENTIAL
   "01","101",      //SINGLE FAMILY RESIDENCE
   "02","103",      //DUPLEX OR DOUBLE RESIDENTIAL
   "03","100",      //MULTIPLE RESIDENTIAL
   "04","106",      //RESIDENTIAL - COURTS
   "05","115",      //MOBILE HOME (0 TO 5 ACRES)
   "06","600",      //RECREATIONAL RESID (5-40 AC)
   "10","833",      //VACANT - COMMERCIAL PROPERTY
   "11","200",      //RETAIL STORE/USED CAR LOT
   "12","200",      //RETAIL STORE W/OFFICE OVER
   "13","200",      //OFFICES, NON-PROFESSIONAL
   "14","200",      //RESTAURANT OR BAR
   "15","200",      //COMMERCIAL - SERVICE SHOP
   "16","200",      //HOTEL / MOTEL
   "17","234",      //SERVICE STATION
   "18","200",      //COMMERCIAL RECREATIONAL PROP
   "19","200",      //COMMERCIAL WHOLESALE OUTLET
   "21","200",      //COMMERCIAL - TRAILER PARK
   "22","200",      //PROF. BLDGS (MD,DDS,LAW ETC)
   "23","246",      //NURSERY
   "24","312",      //BANK
   "25","240",      //COMMERCIAL - PARKING LOT
   "26","226",      //SHOPPING CENTER
   "27","419",      //COMMERCIAL - AIRPORT
   "28","200",      //NEWSPAPER OR RADIO STATION
   "29","200",      //SHIPYARD, WHARF OR DOCKS
   "30","829",      //VACANT - INDUSTRIAL PROPERTY
   "31","402",      //LIGHT MANUFACTURING
   "32","403",      //HEAVY MANUFACTURING
   "33","415",      //INDUSTRIAL - PACKING PLANT
   "34","420",      //INDUSTRIAL MINERAL EXTRACTN
   "35","404",      //INDUSTRIAL - WAREHOUSING
   "36","400",      //INDUSTRIAL - JUNK YARD
   "40","175",      //IRRIGATED RURAL PROPERTY
   "41","175",      //RURAL - IRRIGATED ORCHARD
   "42","175",      //RURAL - IRRIGATED VINEYARD
   "43","175",      //RURAL - IRRIGATED PASTURE
   "44","175",      //RURAL - IRRIGATED ROW CROP
   "45","175",      //RECREATIONAL RESID.40-160 AC
   "50","175",      //DRY RURAL PROPERTY
   "51","175",      //RURAL - DRY ORCHARD
   "52","175",      //RURAL - DRY VINEYARD
   "53","175",      //RURAL - DRY CLASSIFIED LAND
   "54","175",      //RURAL - DRY RANGE LAND
   "55","175",      //RECREATIONAL RESID.40-160 AC
   "60","536",      //TIMBER
   "61","536",      //TIMBER:ASSESSABLE OLD-GROWTH
   "62","536",      //TIMBER:ASSESSABLE YNG-GROWTH
   "63","536",      //TIMBER: EXEMPT OLD-GROWTH
   "64","536",      //TIMBER: EXEMPT YOUNG-GROWTH
   "65","536",      //TIMBER:EXEMPT YNG/OLD GROWTH
   "66","536",      //RECREATNL RES TAXABLE TIMBER
   "67","536",      //RECREATNL RESD EXEMPT TIMBER
   "70","826",      //VACANT - INSTITUTIONAL PROP.
   "71","726",      //CHURCH
   "72","807",      //SCHOOL
   "73","753",      //HOSPITAL
   "74","752",      //CONVALESCENT HOSPITAL
   "75","752",      //REST HOME
   "76","751",      //ORPHANAGE,BRDING SCH,ACADEMY
   "77","758",      //MORTUARY
   "78","758",      //CEMETERY,MAUSOLEUM,CREMATORY
   "79","600",      //RECREATIONAL NON-PROFIT ORG.
   "80","776",      //MISCELLANEOUS
   "81","780",      //WASTE LAND
   "82","776",      //RIGHT-OF-WAY
   "83","783",      //MINERAL RIGHTS
   "84","536",      //TIMBER RIGHTS
   "85","789",      //UTILITY
   "", ""
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 MEN_Exemption[] = 
{
   "HO", "H", 2,1,     // HOMEOWNERS EXEMPTION
   "VE", "V", 2,1,     // VETERAN
   "DV", "D", 2,1,     // DISABLED VETERAN
   "CH", "C", 2,1,     // CHURCH
   "RE", "R", 2,1,     // RELIGIOUS
   "CE", "E", 2,1,     // CEMETERY
   "PS", "P", 2,1,     // PUBLIC SCHOOL
   "PL", "L", 2,1,     // PUBLIC LIBRARY
   "PM", "M", 2,1,     // PUBLIC MUSEUM
   "WH", "I", 2,1,     // WELFARE - HOSPITAL
   "WS", "S", 2,1,     // WELFARE - PRIVATE SCHOOL
   "WR", "R", 2,1,     // WELFARE - RELIGIOUS
   "OE", "X", 2,1,     // OTHER
   "","",0,0
};

#endif