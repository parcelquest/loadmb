/**************************************************************************
 *
 * Notes:
 *    - Col_Tax.txt doesn't have the same layout as other MB counties.
 *
 * Revision
 * 10/31/2005 1.0.0    First version by Tung Bui
 * 01/25/2008 1.10.3.1 Add code to support standard usecode
 * 02/29/2008 1.10.5   Add code to output update records.
 * 08/04/2008 8.2.5    Add Col_MergeMAdr(), Col_MergeSitus(), Col_MergeLien()
 *                     and Col_Load_LDR() to support new TR601 format.  Old
 *                     function CreateColRoll() is used for Col_Roll.csv format only.
 * 01/16/2009 8.5.4    Modify Col_MergeOwner() to make sure Name1 contains all significant
 *                     info from original name.  Reformat if needed.
 * 03/30/2009 8.7.1    Use MB_MergeSale() to do sale update instead of MergeSale3() to fix 
 *                     problem that non-sale transaction overwrites sale transaction that  
 *                     occurs in the same day  since we only keep one transaction per day.
 * 05/21/2009 8.7.6    Remove reference to OldApn & NewApn files.  We no longer need them.
 * 08/16/2010 10.3.1   County has changed LDR file names and format. Change to MB group 2.
 *                     Save EXE_CD and TAXCODE to R01 record.
 * 08/24/2010 10.3.3   Overwrite EXE_TOTAL with GROSS if it is greater than GROSS.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 03/18/2011 10.5.7   COL is changing to new layout format.  Program is changed to
 *                     handle it.  Also adding -Xs option to extract cum sale.
 *                     Do not accept book >= 800 except book 910 (mobile home)
 * 06/05/2011 10.9.0   Exclude HomeSite from OtherVal. Replace Col_MergeExe() with MB_MergeExe(),
 *                     Col_MergeTax() with MB_MergeTax(), and Col_ExtrSale() with MB_CreateSCSale().
 * 05/21/2012 11.14.4  Add option to load tax file into tax database.
 * 07/21/2012 12.2.2   Fix Col_MergeMAdr() to merge CAREOF correctly. Stop using Col_Sales.txt
 *                     when loading LDR.  Use ApplyCumSale() instead.
 * 11/06/2012 12.5.2   Do not update roll values for parcels not in LDR.
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 07/25/2013 13.6.8   Add DocCode table for use in MB_CreateSCSale().
 * 08/14/2013 13.9.12  Do not update EXE in Load_Roll(). No need to merge lien either.
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 01/11/2014 13.19.0  Comment out Col_MergeSale().
 * 01/20/2014 13.20.1  Modify DocCode[] to reset some non-sale event.
 * 04/28/2014 13.23.1  Add Col_ConvStdChar() and Col_MergeStdChar() to support new CHAR file
 * 07/09/2014 14.0.2   Left justified LotAcres and LotSqft
 * 07/31/2015 15.2.0   Add DBA to Col_MergeMAdr()
 * 10/13/2015 15.4.0   Use MB_Load_TaxBase() instead of MB_Load_TaxRollInfo() to import 
 *                     data into Tax_Base & Tax_Owner tables.
 * 04/04/2016 15.14.2  Resort asParkType[] for use in findXlatCode().
 * 07/10/2016 16.0.3   Don't use CHAR from LDR file since the county said so.
 *                     Replace But_Load_LDR() with But_Load_LDR3() to support new LDR format.
 * 07/17/2016 16.0.4   Fix LOT_ACRES format in MergeRoll().
 * 08/10/2016 16.3.0   Add Col_MergeZone() to copy Zoning, TaxCode and Transfer data from roll file
 *                     to update R01 in LDR processing.  Add Col_Load_LDR4() to support latest LDR format.
 * 08/14/2016          Modify Col_MergeMAdr() to include zip4 in M_CTY_ST_D 
 *                     Replace MB_MergeTax() with MB_MergeTaxG2().
 * 07/11/2017 17.1.3   Modify loadCol() to update LDR layout change.
 * 07/11/2018 18.1.2   Fix bug in Col_MergeMAdr() that may leave null char in M_CTY_ST_D
 * 08/01/2018 18.2.2   Modify Col_MergeSitus() to use local apItems[] instead of global apTokens[]
 *                     to avoid conflict.  Modify Col_ConvStdChar() to filter out record without APN.
 *                     Add Col_Load_LDR6(), Col_ExtrLien() & Col_MergeCurRoll() to handle 2018 LDR file.
 * 09/05/2018 18.5.0   Fix bug in Col_ConvStdChar() & Col_Load_Roll() when input has no APN.
 * 07/20/2019 19.1.0   Remove duplicate code in MergeLien3().
 * 08/09/2019 19.1.3   Modify Col_MergeOwner() to remove Json sensitive char.  Modify Col_MergeLien3()
 *                     to add AgPreserve and remove unused code.  Modify Col_Load_LDR3() to support LDR 2019.
 *                     Add Col_ExtrTR601() to support LDR 2019.
 * 06/14/2020 19.10.2  Add -Xn & -Mn options
 * 08/06/2020 20.2.6   Modify Col_MergeRoll() to update TRANSFER_DOC only if DocNum in 9999R9999 format.
 * 10/31/2020 20.4.2   Modify Col_MergeRoll(), Col_MergeZone(), & Col_MergeStdChar() to populate default PQZoning.
 * 07/24/2021 21.1.3   Add QualityClass to R01 in Col_MergeStdChar().
 * 09/10/2021 21.2.2   Add -Xv option to extract value.
 * 10/13/2021 21.2.8   Remove unused code and add more error check. 
 * 07/25/2023 23.0.7   Change sort command in Col_Load_LDR3(). 
 *                     Fix LotSqft in Col_MergeLien3(), Col_MergeRoll() and Col_MergeStdChar().
 * 10/10/2023 23.3.0   Modify Col_ConvStdChar(), turn ON Heating & Cooling.
 * 10/31/2023 23.4.0   Clean up Col_ConvStdChar().
 * 11/10/2023 23.4.2   Change sort command in Col_ConvStdChar() to get the most complete record.
 * 08/05/2024 24.0.5   Modify Col_MergeLien3() to add ExeType.  Modify Col_ConvStdChar() to update ParkType.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "CharRec.h"

#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "PQ.h"
#include "Tax.h"
#include "MB_Value.h"
#include "LoadValue.h"
#include "MergeCol.h"
#include "NdcExtr.h"

/******************************** Col_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Col_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64], acOwner[64], *apItems[16];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001071002", 9) || !memcmp(pOutbuf, "012180046", 9))
   //   iTmp = 0;
#endif

   // Remove bad chars
   pTmp = strcpy(acTmp, pNames);
   iTmp = remJSChar(acTmp);
   if (iTmp > 0)
      LogMsg("*** Bad char found in Owner name APN=%.12s: %s", pOutbuf, pNames);

   // Remove double '&'
   if (pTmp=strstr(acTmp, "& &"))
      strcpy(pTmp, pTmp+2);

   if (pTmp=strstr(acTmp, " 1/2 INT"))
      *pTmp = 0;

   // Special cases
   if (pTmp = strchr(acTmp, '/'))
   {  // CARRASCO HERBERT/MOORE E
      strcpy(acName2, pTmp+1);
      iTmp = ParseString(acName2, ' ', 8, apItems);
      if ((iTmp == 2 && strlen(apItems[1]) == 1) || (iTmp == 1 && strlen(apItems[0]) > 1) )
         replStr(acTmp, "/", " & ");
   }

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Save orig. name
   strcpy(acOwner, acTmp);

   // Check for year that goes before TRUST
   iTmp =0;   
   acName2[0] = 0;
   acSave1[0] = 0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp1 = strlen(acTmp);
      if (iTmp1 > SIZ_NAME1) iTmp1 = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp1);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp1);
      return;
   }

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
      // If TRUST appears after number, save from number forward
      if (strstr((char *)&acTmp[iTmp], " TRUST") )
      {
         iTmp--;
         strcpy(acSave1, (char *)&acTmp[iTmp]);
         acTmp[iTmp] = 0;
      }
   }

   // Filter out words, things in parenthesis 
   // MONDANI NELLIE M ESTATE OF
   if ( (pTmp = strchr(acTmp, '(')) || (pTmp=strstr(acTmp, " TRUSTE"))  ||
      (pTmp=strstr(acTmp, " ESTATE OF")) || (pTmp=strstr(acTmp, " LIFE ESTATE")) ||
      (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTS OF")) || 
      (pTmp=strstr(acTmp, " ETAL"))  || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " JT")) || (pTmp=strstr(acTmp, " CP")) || (pTmp=strstr(acTmp, " CO-TR"))  ||
       (pTmp=strstr(acTmp, " L/T")) || (pTmp=strstr(acTmp, " C/P")) || (pTmp=strstr(acTmp, " T/C")))
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " SUCCS-TR")) || (pTmp=strstr(acTmp, " SUCCESSOR")) || 
      (pTmp=strstr(acTmp, " SURVIVOR")) )
   {
      *pTmp = 0; 
      strcpy(acName1, acTmp);
 
   } else if (pTmp=strstr(acTmp, " FAMILY TRUST &"))
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      // RUMAN RICKY L & RUMAN FAMILY TRUST &
      // RUMAN RICKY L & NANCY FAMILY TRUST & MARIE
      // VERMETTE JAMES & THERESA A & ZENDER ROBERT A &
      strcpy(acSave1, " FAMILY TRUST");
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST &"))
   {  // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      strcpy(acSave1, " LIVING TRUST");
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      strcpy(acSave1, " REV TRUST");
      strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE  
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      *(pTmp+6) = 0;
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
      {
         strcpy(acSave1, pTmp1);
         *pTmp1 = 0;
      } else
      {
         strcpy(acSave1, pTmp);
         *pTmp = 0;
      }

      strcpy(acName1, acTmp);
      strcpy(acName2, pTmp+9);
   } else if (pTmp=strstr(acTmp, " LAND TRUST"))
   {
      strcpy(acSave1, " LAND TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " FAMILY "))
             || (pTmp=strstr(acTmp, " INCOME TR")) || (pTmp=strstr(acTmp, " LIVING TR")) )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " ESTATE OF"))
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   if (pTmp = strstr(acSave1, " THE"))
      *pTmp = 0;

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
         strcpy(acName2, pTmp);
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      
      strcpy(acName1, myOwner.acName1);
      // Concat what in saved buffer
      if (acSave1[0])
      {
         if (acName1[strlen(acName1)-1] == ' ' && acSave1[0] == ' ')
            strcat(acName1, (char *)&acSave1[1]);      // avoid double blank
         else
            strcat(acName1, acSave1);
      }

      // If name is not swapable, use orig. instead
      if (iRet == -1)
      {
         iTmp = strlen(acOwner);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, acOwner, iTmp);
         memcpy(pOutbuf+OFF_NAME1, acOwner, iTmp);
      } else
      {
         if (myOwner.acName2[0] >= 'A' && strcmp(myOwner.acName1, myOwner.acName2))
         {
            // Save Name1
            iTmp = strlen(acName1);
            if (iTmp > SIZ_NAME1)
               iTmp = SIZ_NAME1;
            memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);

            iTmp = strlen(myOwner.acName2);
            if (iTmp > SIZ_NAME2)
               iTmp = SIZ_NAME2;
            memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, iTmp);

            if (myOwner.acVest[0] > ' ')
               memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));
         } else if (acName2[0] > ' ' && strchr(acName2, ' ') )
         {
            // Save Name1
            iTmp = strlen(acName1);
            if (iTmp > SIZ_NAME1)
               iTmp = SIZ_NAME1;
            memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);
            if (myOwner.acVest[0] > ' ')
               memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

            iTmp = strlen(acName2);
            if (iTmp > SIZ_NAME2)
               iTmp = SIZ_NAME2;
            memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);
         } else
         {
            // Use orig. name
            iTmp = strlen(acOwner);
            if (iTmp > SIZ_NAME1)
               iTmp = SIZ_NAME1;
            memcpy(pOutbuf+OFF_NAME1, acOwner, iTmp);
         }

         if (strlen(myOwner.acOF) < 2)
         {
            iTmp = strlen(acOwner);
            if (iTmp > SIZ_NAME_SWAP)
               iTmp = SIZ_NAME_SWAP;
            memcpy(pOutbuf+OFF_NAME_SWAP, acOwner, iTmp);
         } else
         {
            iTmp = strlen(myOwner.acSwapName);
            if (iTmp > SIZ_NAME_SWAP)
               iTmp = SIZ_NAME_SWAP;
            memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
         }
      }
   } else
   {
      // Couldn't split names
      iTmp = strlen(acOwner);
      if (iTmp > SIZ_NAME1)
         iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acOwner, iTmp);
      memcpy(pOutbuf+OFF_NAME_SWAP, acOwner, iTmp);
      acName2[0] = 0;
   }
}

/******************************** Col_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Col_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64];
   int      iTmp;
   char *    pTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   } 

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
      sMailAdr.strName[SIZ_M_STREET] = 0;
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      memcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], strlen(apTokens[MB_ROLL_M_CITY]));
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], 9);

      if (*(pOutbuf+OFF_M_ZIP4) > ' ')
         sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], pOutbuf+OFF_M_ZIP, pOutbuf+OFF_M_ZIP4);
      else
         sprintf(acTmp, "%s %s %.5s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], pOutbuf+OFF_M_ZIP);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

/********************************* Col_MergeMAdr *****************************
 *
 * Notes:
 *    - Mail addr is provided in various form.  We try the best we can since
 *      there is no way to have 100% covered.
 *
 *    - Sample Input:
 *      AMERICAS SERVICING CO           3476 STATEVIEW BLVD             FORECLOSURE MAC#7801-013        FT MILL SC 29715
 *      ALFRED F & IRENE G DEMELO       DBA WALGREENS #6418-S-P         300 WILMOT RD MS#1435           DEERFIELD IL 60015-5121
 *      AD VALOREM TAX DEPT (UNIT 3505) DBA BEACON OIL COMPANY          ONE VALERO WAY                  SAN ANTONIO TX 78249-1616
 *      ATTN  DAVE ARNAIZ               PMB 330                         PO BOX 2818                     ALPHARETTA GA 30023
 *      C/O BANK OF AMERICA             ATTN: BURR WOLFF LP             505 CITY PARKWAY WEST STE 100   ALPHARETTA GA 30023
 *      C/O CITIMORTGAGE, INC.          MAILSTOP 02-25                  27555 FARMINGTON RD             FARMINGTON HILLS MI 48334-3357
 *      RE: LOAN# 0089214605/BALTAZAR   2300 BROOKSTONE CENTRE PKWY     PO BOX 84013                    PO BOX 84013
 *
 *****************************************************************************/

void Col_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64];

#ifdef _DEBUG
   //int   iTmp;
   //if (!memcmp(pOutbuf, "015310041000", 9))
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
         {
            p1 = pLine1;
         } else
         {
            // If last word of line 3 is number and first word of line 1 is alpha, 
            // line 1 more likely be CareOf
            p0 = pLine1;
            p1 = pLine2;
         }
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   strcpy(acAddr2, p2);
   blankRem(acAddr2);

   // Addr2
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);
   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      // City
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      // State
      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
   }
}

/******************************** Col_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Col_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acAddr2[64], *pTmp, *apItems[32];
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSitus);

      pRec = fgets(acRec, 512, fdSitus);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %s", pRec);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apItems);
   else
      iRet = ParseStringIQ(pRec, cDelim, MB_SITUS_SEQ+1, apItems);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old data
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apItems[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      // Save original house number
      memcpy(pOutbuf+OFF_S_HSENO, apItems[MB_SITUS_STRNUM], strlen(apItems[MB_SITUS_STRNUM]));

      sprintf(acTmp, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
      pTmp = strchr(acTmp, ' ');
      *(pTmp+1) = 0;
      strcpy(acAddr1, acTmp);

      if (pTmp = strchr(apItems[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apItems[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apItems[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apItems[MB_SITUS_STRDIR], strlen(apItems[MB_SITUS_STRDIR]));
      }
   }

   if (*apItems[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, apItems[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, apItems[MB_SITUS_STRTYPE]);

      vmemcpy(pOutbuf+OFF_S_STREET, apItems[MB_SITUS_STRNAME], SIZ_S_STREET);
      iTmp = GetSfxCodeX(apItems[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
         Sfx2Code(acTmp, acCode);
      else
      {
         LogMsg0("*** Invalid suffix: %s", apItems[MB_SITUS_STRTYPE]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apItems[MB_SITUS_STRNAME]);
      if (sAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);

      if (sAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, strlen(sAdr.strSfx));

      strcat(acAddr1, apItems[MB_SITUS_STRNAME]);
   }

   if (*apItems[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apItems[MB_SITUS_UNIT]);
      memcpy(pOutbuf+OFF_S_UNITNO, apItems[MB_SITUS_UNIT], strlen(apItems[MB_SITUS_UNIT]));
   }

   blankRem(acAddr1, SIZ_S_ADDR_D);
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   acAddr2[0] = 0;
   if (*apItems[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apItems[MB_SITUS_COMMUNITY], acTmp, acAddr2);   
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr2[0] > ' ')
      {
         strcat(acAddr2, " CA ");

         if (*(pOutbuf+OFF_M_ZIP) == '9' && !memcmp(pOutbuf+OFF_M_CITY, acAddr2, strlen(acAddr2)))
         {
            memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
            memcpy(acTmp, pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
            acTmp[SIZ_M_ZIP] = 0;
            strcat(acAddr2, acTmp);
         }
         blankRem(acAddr2);
      } 
   }

   if (acAddr2[0] > ' ')
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int Col_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004130013", 9))
   //   acTmp[0] = 0;
#endif
   // 
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   if (sSitusAdr.lStrNum > 0)
   {
      char *pTmp = strchr(acAddr1, ' ');
      *pTmp = 0;

      // Save original StrNum
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
      memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   }

   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/******************************** Col_ExtrSale ******************************
 *
 * Extract sale file to Col_Sale.sls
 *
 *****************************************************************************/

int Col_ExtrSale()
{
   SCSAL_REC SaleRec;
   char      acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   char      acTmp[256], acRec[512], *pRec;
   double    dTmp;
   long      lCnt=0, lPrice, iTmp;
   FILE      *fdOut;

   LogMsg("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return 2;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acRec, 512, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      pRec = fgets(acRec, 512, fdSale);
      if (!pRec)
         break;

      // Parse input rec
      if (cDelim == ',')
         iTmp = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTmp = ParseStringIQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);

      if (iTmp < MB_SALES_CONFCODE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTmp);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

      // Docnum
      if (*apTokens[MB_SALES_DOCNUM] > '0')
      {
         memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));

         // Doc date - input format YYYY-MM-DD
         if (dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, YYYY_MM_DD))
            memcpy(SaleRec.DocDate, acTmp, 8);
         else
            continue;
      }

      // Tax
      lPrice = 0;
      if (*apTokens[MB_SALES_TAXAMT] > '0')
      {
         dTmp = atof(apTokens[MB_SALES_TAXAMT]);
         iTmp = sprintf(acTmp, "%.2f", dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);

         lPrice = (long)(dTmp * SALE_FACTOR);
         if (lPrice < 100)
            sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         else
            sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         memcpy(SaleRec.SalePrice, acTmp, SIZ_SALE1_AMT);
      }

      // Doc code - accept following code only.  See MB DocCodes.xls for more desc
      // 1 (Sale-Reappraise), 2 (Partial xfer), 3 (Non-Reappraisal event)
      // 5 (Intermarrital), 6 (Vesting/Name change), 60 (Split/Combo) 
      // 10 (Tax sale), 11 (Add/Delete JT), 19 (Foreclosure)
      /*
      memcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], strlen(apTokens[MB_SALES_DOCCODE]));
      if (isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         iTmp = atoi(apTokens[MB_SALES_DOCCODE]);
         if (iTmp == 1 || (iTmp == 60 && lPrice > 0))
            SaleRec.DocType[0] = '1';
         else if (iTmp == 2)
            memcpy(SaleRec.DocType, "57", 2);
         else if (iTmp == 10)
            memcpy(SaleRec.DocType, "67", 2);
         else if (iTmp == 3 || iTmp == 6 || iTmp == 60)
            SaleRec.DocType[0] = ' ';
         else 
         {
            SaleRec.NoneSale_Flg = 'Y';
            if (iTmp == 5)                        
               memcpy(SaleRec.DocType, "18", 2);
            else if (iTmp == 11)
               SaleRec.DocType[0] = '3';
            else if (iTmp == 19)
               memcpy(SaleRec.DocType, "77", 2);
         }
      } else if (lPrice == 0)
         SaleRec.NoneSale_Flg = 'Y';
      */

      // Transfer Type
      if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      // Group sale?
      if (*apTokens[MB_SALES_GROUPSALE] == '1')
      {
         SaleRec.NumOfPrclXfer[0] = 'M';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

      // Seller
      strcpy(acTmp, apTokens[MB_SALES_SELLER]);
      iTmp = blankRem(acTmp);
      if (iTmp > SALE_SIZ_SELLER)
         iTmp = SALE_SIZ_SELLER;
      memcpy(SaleRec.Seller1, acTmp, iTmp);

      // Buyer
      strcpy(acTmp, apTokens[MB_SALES_BUYER]);
      iTmp = blankRem(acTmp);
      if (iTmp > SALE_SIZ_BUYER)
         iTmp = SALE_SIZ_BUYER;
      memcpy(SaleRec.Name1, acTmp, iTmp);

      // Confirmed sale price
      lPrice = atol(apTokens[MB_SALES_PRICE]);
      if (lPrice > 100)
      {
         iTmp = sprintf(acTmp, "%d", lPrice);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      }

      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   // Sort output file and dedup
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "DAT");
   LogMsg("Sorting %s to %s.", acTmpFile, acOutFile);

   // Sort on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A) F(TXT) DUPO(1,34) ");
   lCnt = sortFile(acTmpFile, acOutFile, acTmp);

   // Update cumulative sale file
   if (lCnt > 0)
   {
      //sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");      // accummulated sale file
      LogMsg("Append %s to %s.", acCSalFile, acTmpFile);
      if (!_access(acCSalFile, 0))
         iTmp = appendTxtFile(acCSalFile, acTmpFile);
      else
         iTmp = 0;

      if (!iTmp)
      {
         // Resort SLS file
         sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         lCnt = sortFile(acTmpFile, acOutFile, acTmp);
         if (lCnt > 0)
         {
            if (!_access(acCSalFile, 0))
               iTmp = remove(acCSalFile);
            if (!iTmp)
               iTmp = rename(acOutFile, acCSalFile);
            else
               LogMsg("***** Error removing %s.  Please rerun with -Xs option", acCSalFile);
         }
      }

   } else
      iTmp = -1;

   LogMsgD("\nTotal Sale records exported: %d.", lCnt);
   return iTmp;
}

/******************************** Col_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************

int Col_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      pTmp = pRec;
      if (*pTmp == '"')
         pTmp++;

      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   if (iTmp)
      return 1;

   while (!iTmp)
   {
      // Parse input
      if (cDelim == ',')
         iRet = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iRet = ParseStringIQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);

      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "030330020000", 9))
      //   iRet = 0;
#endif

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         //pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, YYYY_MM_DD);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // DocType - need translation before production
         // 01, 02, 03, 06, 09, 10, 11, 12, 15, 38, 50, 51, 52, 58, 5M, 86, 92
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         //*(pOutbuf+OFF_MULTI_APN) = *apTokens[MB_SALES_GROUPSALE];
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         MB_MergeSale(&sCurSale, pOutbuf, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
      {
         pTmp = pRec;
         if (*pTmp == '"')
            pTmp++;
         iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      } else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Col_MergeChar ******************************
 *
 * Note: need code table for Heating and Colling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

int Col_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   ULONG    lTmp;
   int      iRet, iLoop, iBeds, iFBath, iHBath, iTmp;
   STDCHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 1024, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "044510005", 9))
   //   iRet = 0;
#endif

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   // Quality Class
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Impr condition
   *(pOutbuf+OFF_IMPR_COND) = pChar->ImprCond[0];

   // Yrblt
   long lYrBlt = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lYrBlt > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
   //else
   //   memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // YrEff
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 1900 && lTmp >= lYrBlt)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_EFF);
   //else
   //   memcpy(pOutbuf+OFF_YR_EFF, BLANK32, SIZ_YR_EFF);

   // LotSqft
   ULONG lLotSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lLotSqft > 100)
   {
      if (lLotSqft < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      } else
         memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
      memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_ACRES);
   } 

   // BldgSqft
   long lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 100)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } 
   //else
   //   memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   long lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 100)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   }
   //else
   //   memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);

   // Parking type
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // Heating
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   
   // Cooling 
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }
   //else
   //   memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } 
   //else
   //   memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } 
   //else
   //   memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Total rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   } 
   //else
   //   memcpy(pOutbuf+OFF_ROOMS, BLANK32, SIZ_ROOMS);
   
   // Fireplace
   memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);
   *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

   // HasSeptic or HasSewer
   if (pChar->HasSeptic > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
   else if (pChar->HasSewer > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;
   //else
   //   *(pOutbuf+OFF_SEWER) = ' ';

   // Pool
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Stories
   iTmp = atoin(pChar->Stories, SIZ_CHAR_SIZE4);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d.0 ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   } else
      memcpy(pOutbuf+OFF_STORIES, BLANK32, SIZ_STORIES);

   // Units
   if (pChar->Units[0] > '0')
      memcpy(pOutbuf+OFF_UNITS, pChar->Units, SIZ_CHAR_UNITS);

   // Zoning
   if (pChar->Zoning[0] > ' ')
      memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);
   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************** Col_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************

int Col_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      if (*pTmp == '"')
         pTmp++;

      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   if (cDelim == ',')
      iTmp = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
   else
      iTmp = ParseStringIQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
   if (iTmp < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      long lGross = atoin(pOutbuf+OFF_GROSS, SIZ_GROSS);

      if (lTmp > lGross)
      {
         LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Col_MergeTax ******************************
 *
 * Note: 
 *
 *****************************************************************************

int Col_MergeTax(char *pOutbuf)
{
   static   char acRec[MAX_RECSIZE], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iTmp;
   double	dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);

      // Get first rec
      pRec = fgets(acRec, MAX_RECSIZE, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      pTmp = pRec;
      if (*pTmp == '"')
         pTmp++;

      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Merge tax data
   while (!iTmp)
   {
      // Parse input
      if (cDelim == ',')
         iTmp = ParseStringNQ(pRec, cDelim, lTaxFlds+1, apTokens);
      else
         iTmp = ParseStringIQ(pRec, cDelim, lTaxFlds+1, apTokens);
      if (iTmp < lTaxFlds)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);
         lTaxSkip++;
         return -1;
      }

      // If same tax year, apply values
      if (!memcmp(myCounty.acYearAssd, apTokens[lTaxYearIdx], 4))
      {
         dollar2Num(apTokens[lTaxAmt1Idx], acTmp);
         dTax1 = atof(acTmp);
         dollar2Num(apTokens[lTaxAmt2Idx], acTmp);
         dTax2 = atof(acTmp);
         dTmp = dTax1+dTax2;
         if (dTax1 == 0.0 || dTax2 == 0.0)
            dTmp *= 2;

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
      }

      // Get next tax record
      pRec = fgets(acRec, MAX_RECSIZE, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      pTmp = pRec;
      if (*pTmp == '"')
         pTmp++;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
   }

   lTaxMatch++;
   return 0;
}

/********************************* Col_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Col_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse input
   if (cDelim == ',')
      iTmp = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iTmp = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Skip excluding APN - unsecured parcels
   lTmp = atoin(apTokens[iApnFld], 3);
   if (lTmp >= 800 && lTmp != 910)
       return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iTmp = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iTmp);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "06COL", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      /* Temporary remove due to county complain that these parcels are on RBP
      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lPP    = atoi(apTokens[MB_ROLL_PERSPROP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lBusInv= atoi(apTokens[MB_ROLL_BUSPROP]);
      lTmp = lFixt+lPP+lMH+lGrow+lBusInv;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lBusInv > 0)
         {
            sprintf(acTmp, "%d         ", lBusInv);
            memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
      */
      // TRA
      memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));
   }

   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) != cInZoneDelim)
      {
         iTmp = sprintf(acTmp, "%c%s%c", cInZoneDelim, apTokens[MB_ROLL_ZONING], cInZoneDelim);
         memcpy(pOutbuf+OFF_ZONE_X1, acTmp, iTmp);
      }
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   if (acTmp[0] == ' ' && acTmp[1] > ' ')
      pTmp = &acTmp[1];
   else
      pTmp = &acTmp[0];

   // Standard UseCode
   if (*pTmp > ' ')
   {
      _strupr(pTmp);
      iTmp = strlen(pTmp);
      memcpy(pOutbuf+OFF_USE_CO, pTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, pTmp, iTmp, apTokens[iApnFld]);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
      if (lTmp < 999999999)
      {
         sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      } else
         memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (ULONG)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0' && *(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], strlen(apTokens[MB_ROLL_DOCNUM]));
      //pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   }

   // Owner
   try {
      Col_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   } catch(...) {
      LogMsg("***** Exeception occured in Col_MergeOwner()");
      iRet = -1;
   }

   // Mailing 
   try {
      Col_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Col_MergeMAdr()");
      iRet = -1;
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return iRet;
}

/********************************* Col_Load_Roll ******************************
 *
 * Update roll file but not values & exemption.
 * County doesn't want us to display values of RBP parcels.
 *
 ******************************************************************************/

int Col_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return 2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return 2;
   }
   
   /*
   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return 2;
   }
   
   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return 2;
   }
   */

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return 2;
   }

   // Open lien file
   /*
   fdLienExt = NULL;
   sprintf(acRec, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acRec, 0))
   {
      LogMsg("Open Lien file %s", acRec);
      fdLienExt = fopen(acRec, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acRec);
         return -7;
      }
   }
   */

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Drop header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   
   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (pTmp && *pTmp == ' ');

   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "010120011", 9))
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Col_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Col_MergeSitus(acBuf);

            // Merge Lien
            /* No need 8/14/2013
            lRet = 1;
            if (fdLienExt)
               lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

            // Merge Exe if not found in LienExt
            if (fdExe && lRet)
               lRet = MB_MergeExe(acBuf);
            */

            // Merge Char
            if (fdChar)
               lRet = Col_MergeStdChar(acBuf);

            // Remove old sale data
            //if (bClearSales)
            //   ClearOldSale(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acBuf, iHdrRows);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         // Create new R01 record
         if (!(iRet = Col_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01)))
         {
            if (bDebug)
               LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

            // Merge Situs
            if (fdSitus)
               lRet = Col_MergeSitus(acRec);

            // Merge Exe
            //if (fdExe)
            //   lRet = MB_MergeExe(acRec);
            //else
            //   acRec[OFF_HO_FL] = '2';

            // Merge Char
            if (fdChar)
               lRet = Col_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acRec, iHdrRows);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired 
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[0], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!iRet && !bEof)
   {
      // Create new R01 record
      if (!(iRet = Col_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01)))
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Merge Situs
         if (fdSitus)
            lRet = Col_MergeSitus(acRec);

         // Merge Exe
         //if (fdExe)
         //   lRet = MB_MergeExe(acRec);
         //else
         //   acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Col_MergeStdChar(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, iHdrRows);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   /*
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdSale)
      fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   */
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   //LogMsg("Number of Exe matched:      %u", lExeMatch);
   //LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   //LogMsg("Number of Exe skiped:       %u", lExeSkip);
   //LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lRecCnt);

   return 0;
}

int Col_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L2_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L2_ASMT], strlen(apTokens[L2_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L2_FEEPARCEL], strlen(apTokens[L2_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L2_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L2_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "06COL", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values
   // Land
   long lLand = atoi(apTokens[L2_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L2_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow = atoi(apTokens[L2_CURRENTGROWINGIMPRVALUE]);
   long lFixt = atoi(apTokens[L2_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L2_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[L2_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   long lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L2_TAXAMT1]);
   double dTax2 = atof(apTokens[L2_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L2_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L2_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L2_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      if (lTmp > lGross)
      {
         LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L2_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L2_EXEMPTIONCODE1], strlen(apTokens[L2_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L2_EXEMPTIONCODE2], strlen(apTokens[L2_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L2_EXEMPTIONCODE3], strlen(apTokens[L2_EXEMPTIONCODE3]));

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L2_TRA], strlen(apTokens[L2_TRA]));

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L2_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030330020000", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L2_PARCELDESCRIPTION]);

   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   iTmp = strlen(apTokens[L2_USECODE]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L2_USECODE], SIZ_USE_CO, iTmp);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L2_USECODE], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L2_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, iTmp);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, iTmp);
   }

   // AgPreserved
   if (*apTokens[L2_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Col_MergeOwner(pOutbuf, apTokens[L2_OWNER]);

   // Situs
   Col_MergeSitus(pOutbuf, apTokens[L2_SITUS1], apTokens[L2_SITUS2]);

   // Mailing
   Col_MergeMAdr(pOutbuf, apTokens[L2_MAILADDRESS1], apTokens[L2_MAILADDRESS2], apTokens[L2_MAILADDRESS3], apTokens[L2_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L2_TAXABILITY], true, true);

   return 0;
}

/********************************* Col_Load_LDR *****************************
 *
 * Load TR601 LDR into 1900-byte record.
 *
 ****************************************************************************/

int Col_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     iRet, lRet=0, lCnt=0;

   lLastFileDate = getFileDate(acRollFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A)");
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return 1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return 2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return 2;
   }

   // Open Sales file
   /*
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return 2;
   }
   */

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating R01 file: %s\n", acTmpFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      lLDRRecCount++;

      // Create new R01 record
      iRet = Col_MergeLien(acBuf, acRec);
      if (!iRet)
      {
         // Merge Char
         if (fdChar)
            lRet = Col_MergeStdChar(acBuf);

         // Merge Sales
         //if (fdSale)
         //   lRet = Col_MergeSale(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   //if (fdSale)
   //   fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   //LogMsg("Number of Sale matched:     %u\n", lSaleMatch);

   LogMsg("Number of Char skiped:      %u", lCharSkip);
   //LogMsg("Number of Sale skiped:      %u\n", lSaleSkip);

   //LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/**************************** Col_ConvStdChar ********************************
 *
 * Copy from MergeAma.cpp to convert new char file. 10/31/2023
 *
 *****************************************************************************/

int Col_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[4], *pRec, *pTmp;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 4096, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);

      if (!pRec)
         break;

      if (acBuf[0] == ' ')
         continue;

      // Replace NULL with empty string
      replStrAll(acBuf, "NULL", "");
      iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < COL_CHAR_FLDS)     // 20231010
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      // Event Date
      if (*apTokens[COL_CHAR_DOCNUM] > ' ')
         continue;

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[COL_CHAR_ASMT], strlen(apTokens[COL_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[COL_CHAR_FEEPRCL], strlen(apTokens[COL_CHAR_FEEPRCL]));
      // Format APN
      if (*apTokens[COL_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[COL_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         if (bDebug)
            LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[COL_CHAR_FEEPRCL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[COL_CHAR_BLDGSEQNO]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d [%s]", iTmp, apTokens[COL_CHAR_ASMT]);

      // Unit# 
      iTmp = atoi(apTokens[COL_CHAR_UNITSEQNO]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.UnitSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** UnitSeqNo too big: %d [%s]", iTmp, apTokens[COL_CHAR_ASMT]);

      // Rooms
      iTmp = atoi(apTokens[COL_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - 01, 02, 03, 99
      iTmp = blankRem(apTokens[COL_CHAR_POOLSPA]);
      if (iTmp > 1)
      {
         pRec = findXlatCode(apTokens[COL_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass
      _strupr(apTokens[COL_CHAR_QUALITYCLASS]);
      if (*apTokens[COL_CHAR_QUALITYCLASS] > ' ' && *apTokens[COL_CHAR_QUALITYCLASS] < 'Z')
      {
         iTmp = strlen(apTokens[COL_CHAR_QUALITYCLASS]);
         memcpy(myCharRec.QualityClass, apTokens[COL_CHAR_QUALITYCLASS], iTmp);

         acCode[0] = ' ';
         blankRem(apTokens[COL_CHAR_QUALITYCLASS]);
         strcpy(acTmp, apTokens[COL_CHAR_QUALITYCLASS]);
         if ((acTmp[1] == ' ' && isalpha(acTmp[2])) ||
             (acTmp[1] == 'K' && isalpha(acTmp[2])) ||
             (acTmp[1] == 'X' && isalnum(acTmp[2])) ||
             (acTmp[1] == '-' && isalnum(acTmp[2])) )
         {
            pTmp = &acTmp[2];
         } else if (!memcmp(acTmp, "CC", 2) || !memcmp(acTmp, "CD", 2) || !memcmp(acTmp, "CM", 2) ||
            !memcmp(acTmp, "MD", 2) || !memcmp(acTmp, "RC", 2) || !memcmp(acTmp, "RD", 2) || !memcmp(acTmp, "RM", 2))
         {
            pTmp = &acTmp[1];
         } else
         {
            pTmp = &acTmp[0];
         }

         remChar(pTmp, ' ');
         if (isalpha(*pTmp))
         {
            myCharRec.BldgClass = *pTmp++;

            if (isdigit(*pTmp))
               iRet = Quality2Code(pTmp, acCode, NULL);
            else if (strlen(pTmp) > 1)
            {
               if (isdigit(*(pTmp+1)))
                  iRet = Quality2Code(pTmp+1, acCode, NULL);
               else if (!memcmp(acTmp, "RX", 2) || !memcmp(acTmp, "AX", 2) || !memcmp(acTmp, "CX", 2))
                  myCharRec.BldgClass = *pTmp;
               else if (*pTmp > '0')
               {
                  pRec = strchr(pTmp, ' ');
                  if (pRec)
                  {
                     pRec++;
                     if (isdigit(*pRec))
                        iRet = Quality2Code(pRec, acCode, NULL);
                     else
                        LogMsg("*** Unknown QUALITYCLASS: '%s' in [%s]", apTokens[COL_CHAR_QUALITYCLASS], apTokens[COL_CHAR_ASMT]);
                  }
               }
            }
         } else
         { // number only
            iRet = Quality2Code(pTmp, acCode, NULL);
         }

         if (acCode[0] > '0')
            myCharRec.BldgQual = acCode[0];
      } else if (*apTokens[COL_CHAR_QUALITYCLASS] > ' ' && *apTokens[COL_CHAR_QUALITYCLASS] != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[COL_CHAR_QUALITYCLASS], apTokens[COL_CHAR_ASMT]);

      // Improved Condition
      if (*apTokens[COL_CHAR_CONDITION] >= '0')
      {
         pRec = findXlatCode(apTokens[COL_CHAR_CONDITION], &asCond[0]);
         if (pRec)
            myCharRec.ImprCond[0] = *pRec;
      }

      // YrBlt
      int iYrBlt = atoi(apTokens[COL_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      } else if (iYrBlt > 1)
         LogMsg("*** Questionable YrBlt: %d, APN=%s", iYrBlt, apTokens[COL_CHAR_ASMT]);

      // YrEff
      iTmp = atoi(apTokens[COL_CHAR_EFFYR]);
      if (iTmp > 1600 && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      } else if (iTmp > 1)
      {
         if (myCharRec.YrBlt[0] > '0')
            memcpy(myCharRec.YrEff, myCharRec.YrBlt, 4);
         LogMsg("*** Questionable YrEff: %d, YrBlt=%d, APN=%.12s.  Use YrBlt.", iTmp, iYrBlt, apTokens[COL_CHAR_ASMT]);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[COL_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[COL_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // LandSqft is not populated, use Acres
      double dAcres = atof(apTokens[COL_CHAR_ACRES]);
      if (dAcres > 0.0)
      {
         iRet = sprintf(acTmp, "%d", (unsigned long)(dAcres*1000.0));
         memcpy(myCharRec.LotAcre, acTmp, iRet);

         iRet = sprintf(acTmp, "%d", (long)(dAcres * SQFT_PER_ACRE));
         memcpy(myCharRec.LotSqft, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "004010008000", 9))
      //   iRet = 0;
#endif
      // Attached SF
      int iAttGar = atoi(apTokens[COL_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[COL_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            memcpy(myCharRec.GarSqft, acTmp, iRet);
            myCharRec.ParkType[0] = 'L';
         }
      }

      // Carport SF
      int iCarport = atoi(apTokens[COL_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
            myCharRec.ParkType[0] = 'C';
      }

      if ((iAttGar > 0 || iDetGar > 0) && iCarport > 0)
         myCharRec.ParkType[0] = '2';     // GARAGE/CARPORT

      // Garage Type - Bad data
      //if (*apTokens[COL_CHAR_GARAGE] > ' ')
      //{
      //   if (!memcmp(apTokens[COL_CHAR_GARAGE], "CP", 2))
      //      myCharRec.ParkType[0] = 'C';
      //   else if (isdigit(*apTokens[COL_CHAR_GARAGE]))
      //   {
      //      myCharRec.ParkType[0] = 'Z';
      //      iTmp = atol(apTokens[COL_CHAR_GARAGE]);
      //      iRet = sprintf(acTmp, "%d", iTmp);
      //      memcpy(myCharRec.ParkSpace, acTmp, iRet);
      //   } else
      //      LogMsg("*** Unknown Garage type: %s [%s]", apTokens[COL_CHAR_GARAGE],  apTokens[COL_CHAR_ASMT]);
      //}

      // Park space - no data avail
      //iTmp = atoi(apTokens[COL_CHAR_PARKINGSPACES]);
      //if (iTmp > 0)
      //{
      //   iRet = sprintf(acTmp, "%d", iTmp);
      //   memcpy(myCharRec.ParkSpace, acTmp, iRet);
      //}

      // Heating
      iTmp = blankRem(apTokens[COL_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[COL_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[COL_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[COL_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[COL_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[COL_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[COL_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[COL_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[COL_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace
      if (*apTokens[COL_CHAR_FIREPLACE] > ' ')
      {
         pRec = findXlatCode(apTokens[COL_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      }

      // Sewer - data not avail
      //blankRem(apTokens[COL_CHAR_SEWERCODE]);
      //if (*apTokens[COL_CHAR_SEWERCODE] > ' ')
      //   myCharRec.HasSewer = 'Y';

      // Water - data not avail
      //blankRem(apTokens[COL_CHAR_HASWELL]);
      //if (*(apTokens[COL_CHAR_HASWELL]) > '0')
      //{
      //   myCharRec.HasWell = 'Y';
      //   myCharRec.HasWater = 'W';
      //} else if (*(apTokens[COL_CHAR_WATERSOURCE]) > '0')
      //{
      //   blankRem(apTokens[COL_CHAR_WATERSOURCE]);
      //   pRec = findXlatCode(apTokens[COL_CHAR_WATERSOURCE], &asWaterSrc[0]);
      //   if (pRec)
      //   {
      //      myCharRec.HasWater = *pRec;
      //      if (*pRec == 'W')
      //         myCharRec.HasWell = 'Y';
      //   }
      //}

      // OfficeSpaceSF
      iTmp = atoi(apTokens[COL_CHAR_OFFICESPACESF]);
      if (iTmp > 10)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Misc.sExtra.OfficeSqft, acTmp, iRet);
      }

      // NonConditionSF

      // ViewCode
      if (*apTokens[COL_CHAR_VIEWCODE] >= 'A')
      {
         pRec = findXlatCode(apTokens[COL_CHAR_VIEWCODE], &asView[0]);
         if (pRec)
            myCharRec.View[0] = *pRec;
      }

      // Stories/NumFloors
      iTmp = atoi(apTokens[COL_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Zoning - I, M
      if (*apTokens[COL_CHAR_ZONING] > '0')
         vmemcpy(myCharRec.Zoning, apTokens[COL_CHAR_ZONING], SIZ_CHAR_ZONING);

      // Solar
      if (*apTokens[COL_CHAR_SOLAR] > '0')
      {
         vmemcpy(myCharRec.Solar, apTokens[COL_CHAR_ZONING], SIZ_CHAR_SIZE4);
         myCharRec.HasSolar = 'Y';
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Sort APN, BldgSeqNum+UnitSeqNum, YrEff, Stories
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D,71,3,C,D) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

int Col_ConvStdCharOld(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[4], *pRec, *pTmp;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 4096, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);

      if (!pRec)
         break;

      if (acBuf[0] == ' ')
         continue;

      // Replace NULL with empty string
      replStrAll(acBuf, "NULL", "");
      iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < COL_CHAR_FLDS)     // 20231010
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      // Event Date
      if (*apTokens[COL_CHAR_DOCNUM] > ' ')
         continue;

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[COL_CHAR_ASMT], strlen(apTokens[COL_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[COL_CHAR_FEEPRCL], strlen(apTokens[COL_CHAR_FEEPRCL]));
      // Format APN
      if (*apTokens[COL_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[COL_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[COL_CHAR_FEEPRCL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[COL_CHAR_BLDGSEQNO]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Unit# 
      iTmp = atoi(apTokens[COL_CHAR_UNITSEQNO]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.UnitSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** UnitSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[COL_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - 01, 02, 03, 99
      iTmp = blankRem(apTokens[COL_CHAR_POOLSPA]);
      if (iTmp > 1)
      {
         pRec = findXlatCode(apTokens[COL_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass
      _strupr(apTokens[COL_CHAR_QUALITYCLASS]);
      if (*apTokens[COL_CHAR_QUALITYCLASS] > ' ' && *apTokens[COL_CHAR_QUALITYCLASS] < 'Z')
      {
         iTmp = strlen(apTokens[COL_CHAR_QUALITYCLASS]);
         memcpy(myCharRec.QualityClass, apTokens[COL_CHAR_QUALITYCLASS], iTmp);

         acCode[0] = ' ';
         blankRem(apTokens[COL_CHAR_QUALITYCLASS]);
         strcpy(acTmp, apTokens[COL_CHAR_QUALITYCLASS]);
         if ((acTmp[1] == ' ' && isalpha(acTmp[2])) ||
             (acTmp[1] == 'K' && isalpha(acTmp[2])) ||
             (acTmp[1] == 'X' && isalnum(acTmp[2])) ||
             (acTmp[1] == '-' && isalnum(acTmp[2])) )
         {
            pTmp = &acTmp[2];
         } else if (!memcmp(acTmp, "CC", 2) || !memcmp(acTmp, "CD", 2) || !memcmp(acTmp, "CM", 2) ||
            !memcmp(acTmp, "MD", 2) || !memcmp(acTmp, "RC", 2) || !memcmp(acTmp, "RD", 2) || !memcmp(acTmp, "RM", 2))
         {
            pTmp = &acTmp[1];
         } else
         {
            pTmp = &acTmp[0];
         }

         remChar(pTmp, ' ');
         if (isalpha(*pTmp))
         {
            myCharRec.BldgClass = *pTmp++;

            if (isdigit(*pTmp))
               iRet = Quality2Code(pTmp, acCode, NULL);
            else if (strlen(pTmp) > 1)
            {
               if (isdigit(*(pTmp+1)))
                  iRet = Quality2Code(pTmp+1, acCode, NULL);
               else if (!memcmp(acTmp, "RX", 2) || !memcmp(acTmp, "AX", 2) || !memcmp(acTmp, "CX", 2))
                  myCharRec.BldgClass = *pTmp;
               else if (*pTmp > '0')
               {
                  pRec = strchr(pTmp, ' ');
                  if (pRec)
                  {
                     pRec++;
                     if (isdigit(*pRec))
                        iRet = Quality2Code(pRec, acCode, NULL);
                     else
                        LogMsg("*** Unknown QUALITYCLASS: '%s' in [%s]", apTokens[COL_CHAR_QUALITYCLASS], apTokens[COL_CHAR_ASMT]);
                  }
               }
            }
         } else
         { // number only
            iRet = Quality2Code(pTmp, acCode, NULL);
         }

         if (acCode[0] > '0')
            myCharRec.BldgQual = acCode[0];
      } else if (*apTokens[COL_CHAR_QUALITYCLASS] > ' ' && *apTokens[COL_CHAR_QUALITYCLASS] != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[COL_CHAR_QUALITYCLASS], apTokens[COL_CHAR_ASMT]);

      // Improved Condition - Not avail yet
      if (*apTokens[COL_CHAR_CONDITION] >= '0')
      {
         pRec = findXlatCode(apTokens[COL_CHAR_CONDITION], &asCond[0]);
         if (pRec)
            myCharRec.ImprCond[0] = *pRec;
      }

      // YrBlt
      int iYrBlt = atoi(apTokens[COL_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      } else if (iYrBlt > 1)
         LogMsg("*** Questionable YrBlt: %d, APN=%s", iYrBlt, apTokens[COL_CHAR_ASMT]);

      // YrEff
      iTmp = atoi(apTokens[COL_CHAR_EFFYR]);
      if (iTmp > 1600 && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      } else if (iTmp > 1)
      {
         if (myCharRec.YrBlt[0] > '0')
            memcpy(myCharRec.YrEff, myCharRec.YrBlt, 4);
         LogMsg("*** Questionable YrEff: %d, YrBlt=%d, APN=%.12s", iTmp, iYrBlt, apTokens[COL_CHAR_ASMT]);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[COL_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[COL_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // Attached SF
      int iAttGar = atoi(apTokens[COL_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[COL_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'L';
      }

      // Carport SF
      int iCarport = atoi(apTokens[COL_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'C';
      }

      // LandSqft is not populated, use Acres
      double dAcres = atof(apTokens[COL_CHAR_ACRES]);
      if (dAcres > 0.0)
      {
         iRet = sprintf(acTmp, "%d", (unsigned long)(dAcres*1000.0));
         memcpy(myCharRec.LotAcre, acTmp, iRet);

         iRet = sprintf(acTmp, "%d", (long)(dAcres * SQFT_PER_ACRE));
         memcpy(myCharRec.LotSqft, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "004010008000", 9))
      //   iRet = 0;
#endif
      // Garage Type - bad data
      /*
      if (*apTokens[COL_CHAR_GARAGE] > ' ')
      {
         _toupper(*apTokens[COL_CHAR_GARAGE]);
         pRec = findXlatCode(apTokens[COL_CHAR_GARAGE], &asParkType[0]);
         if (pRec)
            myCharRec.ParkType[0] = *pRec;
      }
      */

      // Park space - no data avail
      //iTmp = atoi(apTokens[COL_CHAR_PARKINGSPACES]);
      //if (iTmp > 0)
      //{
      //   iRet = sprintf(acTmp, "%d", iTmp);
      //   memcpy(myCharRec.ParkSpace, acTmp, iRet);
      //}

      // Heating 
      iTmp = blankRem(apTokens[COL_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[COL_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[COL_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[COL_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[COL_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[COL_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[COL_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[COL_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[COL_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace
      if (*apTokens[COL_CHAR_FIREPLACE] > ' ')
      {
         pRec = findXlatCode(apTokens[COL_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      }

      // Sewer - data not avail
      //blankRem(apTokens[COL_CHAR_SEWERCODE]);
      //if (*apTokens[COL_CHAR_SEWERCODE] > ' ')
      //   myCharRec.HasSewer = 'Y';

      // Water - data not avail
      //blankRem(apTokens[COL_CHAR_HASWELL]);
      //if (*(apTokens[COL_CHAR_HASWELL]) > '0')
      //{
      //   myCharRec.HasWell = 'Y';
      //   myCharRec.HasWater = 'W';
      //} else if (*(apTokens[COL_CHAR_WATERSOURCE]) > '0')
      //{
      //   blankRem(apTokens[COL_CHAR_WATERSOURCE]);
      //   pRec = findXlatCode(apTokens[COL_CHAR_WATERSOURCE], &asWaterSrc[0]);
      //   if (pRec)
      //   {
      //      myCharRec.HasWater = *pRec;
      //      if (*pRec == 'W')
      //         myCharRec.HasWell = 'Y';
      //   }
      //}

      // OfficeSpaceSF
      iTmp = atoi(apTokens[COL_CHAR_OFFICESPACESF]);
      if (iTmp > 10)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Misc.sExtra.OfficeSqft, acTmp, iRet);
      }

      // NonConditionSF

      // ViewCode
      if (*apTokens[COL_CHAR_VIEWCODE] >= 'A')
      {
         pRec = findXlatCode(apTokens[COL_CHAR_VIEWCODE], &asView[0]);
         if (pRec)
            myCharRec.View[0] = *pRec;
      }

      // Stories/NumFloors
      iTmp = atoi(apTokens[COL_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Zoning - I, M
      if (*apTokens[COL_CHAR_ZONING] > '0')
         vmemcpy(myCharRec.Zoning, apTokens[COL_CHAR_ZONING], SIZ_CHAR_ZONING);

      // Solar
      if (*apTokens[COL_CHAR_SOLAR] > '0')
      {
         vmemcpy(myCharRec.Solar, apTokens[COL_CHAR_ZONING], SIZ_CHAR_SIZE4);
         myCharRec.HasSolar = 'Y';
      }

      // Event Date
      /*
      if (*apTokens[COL_CHAR_EVENTDATE] >= '0')
      {  
         // Convert date from yyyy-mm-dd to yyyymmdd
         pRec = dateConversion(apTokens[COL_CHAR_EVENTDATE], acTmp, YYYY_MM_DD);
         if (pRec)
            memcpy(myCharRec.Misc.sExtra.RecDate, acTmp, 8);
      }

      // DocNum
      if (*apTokens[COL_CHAR_DOCNUM] > '0')
         memcpy(myCharRec.Misc.sExtra.DocNum, apTokens[COL_CHAR_DOCNUM], strlen(apTokens[COL_CHAR_DOCNUM]));

      // Sale price
      iTmp = atoi(apTokens[COL_CHAR_SALESPRICE]);
      if (iTmp > 1000)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Misc.sExtra.SalePrice, acTmp, iRet);
      }
      */

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Sort APN, BldgSeqNum+UnitSeqNum, YrEff
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/********************************* Col_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Col_MergeLien3(char *pOutbuf, char *pRollRec, bool bRemHyphen)
{
   char     acTmp[256], acTmp1[64], acApn[16], *pTmp;
   ULONG    lTmp;
   double   dTmp;
   int      iRet, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L3_ISAGPRESERVE)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[iApnFld], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Remove hyphen from APN
   if (bRemHyphen)
   {
      remChar(apTokens[L3_TRA], '-');
      remChar(apTokens[iApnFld], '-');
   }

   iRet = strlen(apTokens[iApnFld]);
   if (iRet < iApnLen)
      sprintf(acApn, "%.*s%s", iApnLen-iRet, "000", apTokens[iApnFld]);
   else
      strcpy(acApn, apTokens[iApnFld]);

   // Start copying data
   memcpy(pOutbuf, acApn, iApnLen);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Copy ALT_APN
   iRet = strlen(apTokens[L3_FEEPARCEL]);
   if (iRet < iApnLen)
      sprintf(acApn, "%.*s%s", iApnLen-iRet, "000", apTokens[L3_FEEPARCEL]);
   else
      strcpy(acApn, apTokens[L3_FEEPARCEL]);
   memcpy(pOutbuf+OFF_ALT_APN, acApn, iApnLen);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "06COL", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   ULONG lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   ULONG lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   ULONG lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   ULONG lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   ULONG lGrow  = atoi(apTokens[L3_GROWING]);
   ULONG lPers  = atoi(apTokens[L3_PPVALUE]);
   ULONG lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   ULONG lExe1 = atol(apTokens[L3_HOX]);
   ULONG lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&COL_Exemption);

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
      if (lTmp < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }
      // Format Acres
      lTmp = (ULONG)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (ULONG)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Col_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   //Col_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Col_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - added 07/25/2023
   if (*apTokens[L3_CURRENTDOCNUM] > '0' && *(apTokens[L3_CURRENTDOCNUM]+4) == 'R' && *apTokens[L3_CURRENTDOCDATE] > '0')
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], strlen(apTokens[L3_CURRENTDOCNUM]));
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   }

   return 0;
}

/******************************** Col_Load_LDR3 *****************************
 *
 * For LDR 2016-2017, 2019, 2020
 *
 ****************************************************************************/

int Col_Load_LDR3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;
   bool     bRemHyphen=false;

   // Check for special case
   GetIniString(myCounty.acCntyCode, "RemHyphen", "", acBuf, _MAX_PATH, acIniFile);
   if (acBuf[0] == 'Y')
      bRemHyphen = true;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT or APN
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (_access(acRollFile, 0))
   {
      sprintf(acRec, "S(#%d,C,A) DEL(%d) F(TXT) OMIT(#1,C,GT,\"3\") ", iApnFld+1, cLdrSep); 
      iRet = sortFile(acTmpFile, acRollFile, acRec);  
      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open lien file %s", acRollFile);
   fdLDR = fopen(acRollFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
      if (cDelim == '|')
         strcat(acRec, "DEL(124) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      // Create new R01 record
      iRet = Col_MergeLien3(acBuf, acRec, bRemHyphen);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Col_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Col_MergeStdChar(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Situs skiped:     %u\n", lSitusSkip);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/********************************* Col_MergeLien6 ****************************
 *
 * For 2018 LDR Secured_2018.txt
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Col_MergeLien6(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L6_PRIORNETVALUE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L6_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf+OFF_APN_D, apTokens[L6_FMT_APN], strlen(apTokens[L6_FMT_APN]));
   iRet = remChar(apTokens[L6_FMT_APN], '-');
   memcpy(pOutbuf, apTokens[L6_FMT_APN], iRet);

   // Copy ALT_APN - FeeParcel is not available
   //memcpy(pOutbuf+OFF_ALT_APN, apTokens[L6_FEEPARCEL], strlen(apTokens[L6_FEEPARCEL]));

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L6_FMT_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "06COL", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L6_STATUS];

   // TRA
   iRet = remChar(apTokens[L6_FMT_TRA], '-');
   memcpy(pOutbuf+OFF_TRA, apTokens[L6_FMT_TRA], strlen(apTokens[L6_FMT_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L6_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L6_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L6_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L6_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L6_GROWING]);
   long lPers  = atoi(apTokens[L6_PPVALUE]);
   long lPP_MH = atoi(apTokens[L6_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L6_HOX]);
   long lExe2 = atol(apTokens[L6_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L6_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L6_OTHEREXEMPTIONCODE], strlen(apTokens[L6_OTHEREXEMPTIONCODE]));

   // Legal - not avail
   //updateLegal(pOutbuf, apTokens[L6_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L6_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L6_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L6_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L6_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Lot size
   lTmp = atol(apTokens[L6_LANDSIZE]);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      if (!dTmp)
      {
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(lTmp*SQFT_MF_1000));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   } else if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Owner
   Col_MergeOwner(pOutbuf, apTokens[L6_OWNER]);

   // Mailing
   Col_MergeMAdr(pOutbuf, apTokens[L6_MAILADDRESS1], apTokens[L6_MAILADDRESS2], apTokens[L6_MAILADDRESS3], apTokens[L6_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L6_TAXABILITY], true, true);

   // YearBlt
   lTmp = atol(apTokens[L6_YEARBUILT]);
   if (lTmp > 1800 && lTmp < lToyear)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // Total rooms
   iTmp = atol(apTokens[L6_TOTALROOMS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Stories
   iTmp = atol(apTokens[L6_STORIES]);
   if (iTmp > 0 && iTmp < 100)
   {
      sprintf(acTmp, "%d.0", iTmp);
      vmemcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   return 0;
}

/******************************* Col_MergeCurRoll ****************************
 *
 * Merge legal, feeparcel, zoning, current doc from monthly update roll
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Col_MergeCurRoll(char *pOutbuf)
{
   static   char acRec[MAX_RECSIZE], *pRec=NULL;
   char     acTmp[256], *pTmp, *apItems[MAX_FLD_TOKEN];
   int      iRet=0, iTmp;

   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
      pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "015310041000", 9))
   //   iTmp = 0;
#endif

   do 
   {
      if (!pRec)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         return 1;      // EOF 
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Roll rec: %s", pRec);
         pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);
   else
      iRet = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad roll record for APN=%s", apItems[iApnFld]);
      return -1;
   }

   // FeeParcel
   memcpy(pOutbuf+OFF_ALT_APN, apItems[MB_ROLL_FEEPARCEL], strlen(apItems[MB_ROLL_FEEPARCEL]));

   // Legal
   iTmp = updateLegal(pOutbuf, apItems[MB_ROLL_LEGAL]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // Zoning
   if (*apItems[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apItems[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) != cInZoneDelim)
      {
         iTmp = sprintf(acTmp, "%c%s%c", cInZoneDelim, apTokens[MB_ROLL_ZONING], cInZoneDelim);
         memcpy(pOutbuf+OFF_ZONE_X1, acTmp, iTmp);
      }
}

   // Recorded Doc - Not consistent with sale file - use only when matched
   if (*apItems[MB_ROLL_DOCNUM] > '0')
   {
      pTmp = dateConversion(apItems[MB_ROLL_DOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apItems[MB_ROLL_DOCNUM], strlen(apItems[MB_ROLL_DOCNUM]));
      }
   } 

   lRollMatch++;

   // Get next record
   pRec = fgets(acRec, MAX_RECSIZE, fdRoll);

   return 0;
}

/******************************** Col_Load_LDR6 *****************************
 *
 * First version of LDR 2018
 *
 ****************************************************************************/

int Col_Load_LDR6(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048], acTmp[256];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acLienFile[_MAX_PATH];

   HANDLE   fhOut;

   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     iRet, lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);

   // Sort roll file on ASMT
   sprintf(acLienFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   // Sort on Fmt_APN
   sprintf(acTmp, "S(#2,C,A) DEL(%d)", cLdrSep);
   iRet = sortFile(acTmpFile, acLienFile, acTmp);  // 2018
   if (!iRet)
      return -1;

   // Open LDR file
   LogMsg("Open LDR file %s", acLienFile);
   fdLDR = fopen(acLienFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acLienFile);
      return -1;
   }  

   // Need FeeParcel & Legal from roll file
   GetIniString(myCounty.acCntyCode, "RollFile", "", acTmpFile, _MAX_PATH, acIniFile);

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      // Sort on Fmt_APN
      sprintf(acTmp, "S(#1,C,A) DEL(%d)", cDelim);
      iRet = sortFile(acTmpFile, acRollFile, acTmp);  // 2018
      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening Roll file: %s\n", acRollFile);
      return -1;
   }  


   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
      if (cDelim == '|')
         strcat(acRec, "DEL(124) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   // Clear header row since files are sorted
   iHdrRows = 0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      // Create new R01 record
      iRet = Col_MergeLien6(acBuf, acRec);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Col_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Col_MergeStdChar(acBuf);

         // Merge Legal & FeeParcel from monthly update roll 
         if (fdRoll)
            lRet = Col_MergeCurRoll(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp || *pTmp > '9')
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("      output records:       %u", lLDRRecCount);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of Roll matched:     %u\n", lRollMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Situs skiped:     %u\n", lSitusSkip);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************** Col_MergeZone ******************************
 *
 * Merge Zoning, tax code, and transfer from roll file
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Col_MergeZone(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 1024, fdZone);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdZone);
         fdZone = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Roll rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 1024, fdZone);
         lZoneSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdZone);
         fdZone = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input roll record for APN=%s", apTokens[0]);
         iRet = -1;
      }

      return iRet;
   }

   // Merge data
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      memcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], iTmp);
      if (*(pOutbuf+OFF_ZONE_X1) != cInZoneDelim)
      {
         char acTmp[256];

         iTmp = sprintf(acTmp, "%c%s%c", cInZoneDelim, apTokens[MB_ROLL_ZONING], cInZoneDelim);
         memcpy(pOutbuf+OFF_ZONE_X1, acTmp, iTmp);
      }
   }
   lZoneMatch++;

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      char  acTmp[32], *pTmp;

      memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], strlen(apTokens[MB_ROLL_DOCNUM]));
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   }

   // Get next record
   pRec = fgets(acRec, 1024, fdZone);

   return 0;
}

/********************************* Col_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Col_MergeLien4(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L4_UNITS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L4_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L4_ASMT], strlen(apTokens[L4_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L4_FEEPARCEL], strlen(apTokens[L4_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L4_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L4_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "06COL", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L4_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L4_TRA], strlen(apTokens[L4_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L4_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L4_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L4_FIXTURESVALUE]);
   long lGrow  = atoi(apTokens[L4_GROWING]);
   long lPers  = atoi(apTokens[L4_PPVALUE]);
   long lPP_MH = atoi(apTokens[L4_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L4_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L4_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L4_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   if (!memcmp(apTokens[L4_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L4_EXEMPTIONCODE1], strlen(apTokens[L4_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L4_EXEMPTIONCODE2], strlen(apTokens[L4_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L4_EXEMPTIONCODE3], strlen(apTokens[L4_EXEMPTIONCODE3]));

   // Legal
#ifdef _DEBUG
   //if (!memcmp(apTokens[iApnFld], "001221006000", 9) )
   //   iTmp = 0;
#endif
   // Legal - replace known bad char
   iTmp = replChar(apTokens[L4_PARCELDESCRIPTION], 221, 47);
   if (iTmp)
      LogMsg("***Fixing Legal [%.12s]: %s", pOutbuf, apTokens[L4_PARCELDESCRIPTION]);

   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L4_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   if (*apTokens[L4_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L4_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L4_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L4_ACRES]);
   lTmp = atol(apTokens[L4_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   //if (*apTokens[L4_ISAGPRESERVE] == '1')
   //   *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Col_MergeOwner(pOutbuf, apTokens[L4_OWNER]);

   // Situs
   //Col_MergeSitus(pOutbuf, apTokens[L4_SITUS1], apTokens[L4_SITUS2]);

   // Mailing
   Col_MergeMAdr(pOutbuf, apTokens[L4_MAILADDRESS1], apTokens[L4_MAILADDRESS2], apTokens[L4_MAILADDRESS3], apTokens[L4_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   //iTmp = updateTaxCode(pOutbuf, apTokens[L4_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016

   //// Garage size
   //dTmp = atof(apTokens[L4_GARAGESIZE]);
   //if (dTmp > 0.0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_GAR_SQFT, dTmp);
   //   memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   //   *(pOutbuf+OFF_PARK_TYPE) = '2';                 // GARAGE/CARPORT
   //}

   //// Number of parking spaces
   //if (*apTokens[L4_GARAGE] == '0' || *apTokens[L4_GARAGE] == 'N')
   //   *(pOutbuf+OFF_PARK_TYPE) = 'H';                 // None
   //else if (*apTokens[L4_GARAGE] > '0' && *apTokens[L4_GARAGE] <= '9')
   //{
   //   iTmp = atol(apTokens[L4_GARAGE]);
   //   sprintf(acTmp, "%d", iTmp);
   //   vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
   //   if (dTmp > 100)
   //      *(pOutbuf+OFF_PARK_TYPE) = 'Z';              // Garage
   //   else
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
   //} else
   //{
   //   if (*(apTokens[L4_GARAGE]) == 'C')
   //      *(pOutbuf+OFF_PARK_TYPE) = 'C';              // Carport
   //   else if (*(apTokens[L4_GARAGE]) == 'A')
   //   {
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L4_GARAGE], "DOU", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '2';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L4_GARAGE], "TRI", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '3';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L4_GARAGE], "SIN", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '1';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L4_GARAGE], "GC", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT
   //   else if (!_memicmp(apTokens[L4_GARAGE], "GS", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT ?
   //   else if (!_memicmp(apTokens[L4_GARAGE], "DE", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = 'D';              // Detached
   //   else if (*(apTokens[L4_GARAGE]) == 'S')
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Space ?
   //}

   //// YearBlt
   //lTmp = atol(apTokens[L4_YEARBUILT]);
   //if (lTmp > 1800 && lTmp < lToyear)
   //{
   //   iTmp = sprintf(acTmp, "%d", lTmp);
   //   memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   //}

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // YearBlt
   lTmp = atol(apTokens[L4_YEARBUILT]);
   if (lTmp > 1800 && lTmp < lToyear)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   }

   // Acres
   dTmp = atof(apTokens[L4_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Lot size
   lTmp = atol(apTokens[L4_LANDSIZE]);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      if (!dTmp)
      {
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(lTmp*SQFT_MF_1000));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   } else if (dTmp > 0.0)
   {
      //lTmp = (dTmp+0.0005)*SQFT_PER_ACRE;
      lTmp = (long)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   //// Total rooms
   //iTmp = atol(apTokens[L4_TOTALROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   //}

   //// Stories
   //iTmp = atol(apTokens[L4_STORIES]);
   //if (iTmp > 0 && iTmp < 100)
   //{
   //   sprintf(acTmp, "%d.0", iTmp);
   //   vmemcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   //}

   //// Units
   //iTmp = atol(apTokens[L4_UNITS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%d", iTmp);
   //   vmemcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   //}

   //// Beds
   //iTmp = atol(apTokens[L4_BEDROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BEDS, iTmp);
   //   memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   //}

   //// Baths
   //iTmp = atol(apTokens[L4_BATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_F, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   //}

   //// HBaths
   //iTmp = atol(apTokens[L4_HALFBATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_H, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   //}

   //// Heating
   //int iCmp;
   //if (*apTokens[L4_HEATING] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L4_HEATING], asHeating_LDR[iTmp].acSrc, asHeating_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_HEAT) = asHeating_LDR[iTmp].acCode[0];
   //}

   //// Cooling
   //if (*apTokens[L4_AC] == 'C')
   //   *(pOutbuf+OFF_AIR_COND) = 'C';
   //else if (*apTokens[L4_AC] > ' ')
   //   LogMsg("*** Unknown A/C: %s", apTokens[L4_AC]);

   //// Pool/Spa
   //if (*apTokens[L4_POOLSPA] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L4_POOLSPA], asPool_LDR[iTmp].acSrc, asPool_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_POOL) = asPool_LDR[iTmp].acCode[0];
   //}

   //// Fire place
   //if (*apTokens[L4_FIREPLACE] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   pTmp = _strupr(apTokens[L4_FIREPLACE]);
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L4_FIREPLACE], asFP_LDR[iTmp].acSrc, asFP_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_FIRE_PL) = asPool[iTmp].acCode[0];
   //}

   //// Quality Class
   //acTmp1[0] = 0;
   //if (*apTokens[L4_QUALITYCLASS] > '0' || strlen(apTokens[L4_QUALITYCLASS]) > 1)
   //{
   //   strcpy(acTmp, apTokens[L4_QUALITYCLASS]);
   //   remCharEx(acTmp, " ,'?");
   //   pTmp = _strupr(acTmp);

   //   //memcpy(myCharRec.QualityClass, pTmp, strlen(pTmp));
   //   if (acTmp[0] == 'A' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'A';
   //   else if (acTmp[0] == 'F' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'F';
   //   else if (!memcmp(pTmp, "POOR", 4) || !memcmp(pTmp, "LOW", 3))
   //      acTmp1[0] = 'P';
   //   else if (*pTmp == 'G')
   //      acTmp1[0] = 'G';
   //   else if (isalpha(*pTmp))
   //   {
   //      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
   //      if (isdigit(acTmp[1]))
   //         iRet = Quality2Code(&acTmp[1], acTmp1, NULL);
   //      else if (isdigit(acTmp[2]))
   //         iRet = Quality2Code(&acTmp[2], acTmp1, NULL);
   //      else if (isalpha(acTmp[1]))
   //      {
   //         switch (acTmp[1])
   //         {
   //            case 'L':
   //            case 'P':
   //               acTmp1[0] = 'P';
   //               break;
   //            case 'A':
   //               acTmp1[0] = 'A';
   //               break;
   //            case 'F':
   //               acTmp1[0] = 'F';
   //               break;
   //            case 'G':
   //               acTmp1[0] = 'G';
   //               break;
   //         }
   //      }
   //   } else if (isdigit(*pTmp))
   //   {
   //      iTmp = atol(pTmp);
   //      if (iTmp < 100)
   //         iRet = Quality2Code(pTmp, acTmp1, NULL);
   //   }

   //   if (acTmp1[0] > '0')
   //      *(pOutbuf+OFF_BLDG_QUAL) = acTmp1[0];
   //}

   return 0;
}

/******************************** Col_Load_LDR4 *****************************
 *
 * Load LDR 2016
 *
 ****************************************************************************/

int Col_Load_LDR4(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Zoning file
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acBuf, acRec, myCounty.acCntyCode, myCounty.acCntyCode);
   if (acBuf[0] > ' ')
   {
      // Sort on ASMT
      sprintf(acTmpFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = sortFile(acBuf, acTmpFile, "S(#1,C,A)");

      LogMsg("Open Zoning file %s", acTmpFile);
      fdZone = fopen(acTmpFile, "r");
      if (fdZone == NULL)
      {
         LogMsg("***** Error opening Zoning file: %s\n", acTmpFile);
         return -2;
      }
   }

   // Open Value file
   //sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (!_access(acTmpFile, 0))
   //{
   //   LogMsg("Open Lien file %s", acTmpFile);
   //   fdLienExt = fopen(acTmpFile, "r");
   //   if (fdLienExt == NULL)
   //   {
   //      LogMsg("***** Error opening lien file: %s\n", acTmpFile);
   //      return -7;
   //   }
   //} else
   //   fdLienExt = NULL;

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
      if (cDelim == '|')
         strcat(acRec, "DEL(124) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      // Create new R01 record
      iRet = Col_MergeLien4(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         //if (fdLienExt)
         //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Situs
         if (fdSitus)
            lRet = Col_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = MB_MergeStdChar(acBuf, NULL, NULL, NULL);

         // Merge Zoning
         if (fdZone)
            lRet = Col_MergeZone(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;
         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdZone)
      fclose(fdZone);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Zoning matched:   %u", lZoneMatch);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Zoning skiped:    %u", lZoneSkip);
   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/***************************** Col_ExtrLienRec ********************************
 *
 ******************************************************************************/

int Col_ExtrLienRec(char *pOutbuf, char *pRollRec, bool bRemHyphen)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L6_PRIORNETVALUE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L6_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L6_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Remove hyphen from APN
   if (bRemHyphen)
   {
      remChar(apTokens[L6_FMT_TRA], '-');
      iRet = remChar(apTokens[L6_FMT_APN], '-');
      if (iRet != iApnLen)
      {
         LogMsg("***** Error: bad APN=%s", apTokens[L6_FMT_APN]);
         return -2;
      }
   }

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[L6_FMT_APN], iApnLen);

   // TRA
   lTmp = atol(apTokens[L6_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L6_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L6_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixt  = atoi(apTokens[L6_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L6_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L6_GROWING]);
   long lPers  = atoi(apTokens[L6_PPVALUE]);
   long lPP_MH = atoi(apTokens[L6_MHPPVALUE]);
   lTmp = lFixt+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L6_HOX]);
   long lExe2 = atol(apTokens[L6_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  

   iRet = 1;
   if (lExe1 > 0)
   {
      pLienExtr->acHO[0] = '1';      // 'Y'
      memcpy(pLienExtr->extra.MB.ExeCode1, "E01", 3);
      iRet = 2;
   } else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L6_OTHEREXEMPTIONCODE] > ' ')
   {
      if (iRet == 1)
         vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L6_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
      else
         vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L6_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
   }

   // Taxability
   lTmp = atoin(apTokens[L6_TAXABILITY], 3);
   if (lTmp > 0)
   {
      // Prop 8 
      if (lTmp > 799 && lTmp < 900)
         pLienExtr->SpclFlag = LX_PROP8_FLG;
      vmemcpy(pLienExtr->acTaxCode, apTokens[L6_TAXABILITY], SIZ_LIEN_TAXCODE);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************* Col_ExtrLien *******************************
 *
 * Extract lien data from Secured_2018.txt
 *
 ****************************************************************************/

int Col_ExtrLien(LPCSTR pLDRFile)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;
   bool     bRemHyphen=false;

   LogMsg0("Extract lien roll for COL");

   // Check for special case
   GetIniString(myCounty.acCntyCode, "RemHyphen", "", acBuf, _MAX_PATH, acIniFile);
   if (acBuf[0] == 'Y')
      bRemHyphen = true;

   // Open lien file
   if (!pLDRFile)
   {
      GetIniString(myCounty.acCntyCode, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
      if (!acBuf[0])
         return -1;
   } else
      strcpy(acBuf, pLDRFile);

   LogMsg("Open Lien Date Roll file %s", acBuf);
   fdRoll = fopen(acBuf, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acBuf);
      return -2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   
   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Replace null char with space
      //iRet = replNull(acRec, ' ', 0);

      // Create new record
      iRet = Col_ExtrLienRec(acBuf, acRec, bRemHyphen);
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%d", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);

      // EOF
      if (acRec[1] > '9')
         break;
   }
   iRet = 0;

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return iRet;
}

/******************************* Col_ExtrTR601 ******************************
 *
 * Extract lien data from AGENCYCDCURRSEC_TR601.TXT 2019
 *
 ****************************************************************************/

int Col_ExtrTR601Rec(char *pOutbuf, char *pRollRec, bool bRemHyphen)
{
   int      iRet, lTmp;
   char     acTmp[64], acApn[16];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L3_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Remove hyphen from APN
   if (bRemHyphen)
   {
      remChar(apTokens[L3_TRA], '-');
      remChar(apTokens[iApnFld], '-');
   }

   iRet = strlen(apTokens[iApnFld]);
   if (iRet < iApnLen)
      sprintf(acApn, "%.*s%s", iApnLen-iRet, "000", apTokens[iApnFld]);
   else
      strcpy(acApn, apTokens[iApnFld]);

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[iApnFld], strlen(apTokens[iApnFld]));

   // TRA
   lTmp = atol(apTokens[L3_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixt  = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixt+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  

   iRet = 1;
   if (lExe1 > 0)
   {
      pLienExtr->acHO[0] = '1';      // 'Y'
      memcpy(pLienExtr->extra.MB.ExeCode1, "E01", 3);
      iRet = 2;
   } else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
   {
      if (iRet == 1)
         vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L3_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
      else
         vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L3_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
   }

   // Taxability
   lTmp = atoin(apTokens[L3_TAXABILITYFULL], 3);
   if (lTmp > 0)
   {
      // Prop 8 
      if (lTmp > 799 && lTmp < 900)
         pLienExtr->SpclFlag = LX_PROP8_FLG;
      vmemcpy(pLienExtr->acTaxCode, apTokens[L3_TAXABILITYFULL], SIZ_LIEN_TAXCODE);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int Col_ExtrTR601(LPCSTR pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;
   bool     bRemHyphen=false;

   LogMsg0("Extract lien roll for %s", pCnty);

   // Check for special case
   GetIniString(pCnty, "RemHyphen", "", acBuf, _MAX_PATH, acIniFile);
   if (acBuf[0] == 'Y')
      bRemHyphen = true;

   // Open lien file
   LogMsg("Open Lien Date Roll file %s", acRollFile);
   sprintf(acTmpFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Sort on ASMT
   sprintf(acRec, "S(#%d,C,A) DEL(9) OMIT(2,1,C,LT,\"0\",OR,2,1,C,GT,\"9\")", iApnFld+1);

   iRet = sortFile(acRollFile, acTmpFile, acRec);
   fdLDR = fopen(acTmpFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acTmpFile);
      return -2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Get 1st rec
   do
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   } while (pTmp && *pTmp < '0');

   // Merge loop
   while (pTmp && !feof(fdLDR))
   {
      // Replace null char with space
      iRet = replNull(acRec, ' ', 0);

      // Create new record
      iRet = Col_ExtrTR601Rec(acBuf, acRec, bRemHyphen);    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%d", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
   }
   iRet = 0;

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return iRet;
}

/*********************************** loadCol ********************************
 *
 * Notes
 *    - Use LdrGrp to determine LDR layout.  2016=4, 2017=3.
 *
 * Options:
 *    -CCol -L -Xl (load lien)
 *    -CCol -U (load update)
 *
 ****************************************************************************/

int loadCol(int iSkip)
{
   int   iRet=0;

   // Set default APN field
   if (iLoadFlag & LOAD_UPDT)
   {
      if (iApnFld == -1)
         iApnFld = MB_ROLL_ASMT;
   } else
   {
      iApnFld = L3_ASMT;
   }
   iApnLen = myCounty.iApnLen;

   if (iLoadTax == TAX_LOADING)
   {
      // Load taxrollinfo.txt into SQL
      //iRet = MB_Load_TaxRollInfo(bTaxImport);

      // Load Col_Tax.txt
      iRet = MB_Load_TaxBase(bTaxImport, true, 2, iHdrRows);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet = MB_Load_TaxCodeMstr(bTaxImport);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
   }

   if (!iLoadFlag)
      return iRet;

   // Load Value file
   if (iLoadFlag & EXTR_VALUE)                     // -Xv
   {
      char sDbName[64], sBYVFile[_MAX_PATH], sTmpFile[_MAX_PATH];
      char acTmp[256], acTmp1[256];

      sprintf(sBYVFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Value");
      sprintf(sTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = MB_ExtrValues(myCounty.acCntyCode, acValueFile, acExeFile, sTmpFile, sBYVFile, 1);
      
      // Reset reason and VST
      if (iRet > 0)
      {
         sprintf(sTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Vmp");
         iRet = MB_ResetPVReason(sBYVFile, sTmpFile);
      }
      if (iRet > 0)
      {
         sprintf(sDbName, "LDR%d", lLienYear);
         GetIniString("Data", "SqlValueFile", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acTmp1, acTmp, sDbName, myCounty.acCntyCode);
         lRecCnt = createValueImport(sBYVFile, acTmp1, false);
         if (lRecCnt > 0)
         {
            // Save output file for import
            strcpy(acValueFile, acTmp1);
            iRet = 0;
         } else
            iLoadFlag ^= EXTR_IVAL;                // Turn off import
      }
	}

   // Extract lien value
   if (iLoadFlag & EXTR_LIEN)
   {
      //GetIniString(myCounty.acCntyCode, "LienFile", "", acRollFile, _MAX_PATH, acIniFile);
      // 2017 and prior
      //iRet = MB_ExtrTR601(myCounty.acCntyCode, NULL);
      // 2018
      //iRet = Col_ExtrLien(acRollFile);
      // 2019-2021
      iRet = Col_ExtrTR601(myCounty.acCntyCode);
      // 2020
      //iRet = Col_ExtrLdrValue(myCounty.acCntyCode);
   }

   // Create/Update cum sale file
   // Extract Sale file from Col_Sales.csv to Col_Sale.dat and append to Col_Sale.sls
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      //iRet = MB_CreateSCSale(YYYY_MM_DD, 0, 2 /* 20120831 */); 
      // 7/24/2013
      iRet = MB_CreateSCSale(YYYY_MM_DD,0,2,false,(IDX_TBL5 *)&COL_DocCode[0]);

      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract NDC recorder sale 
   if (iLoadFlag & EXTR_NRSAL)                     // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSalesFile, _MAX_PATH, acIniFile);
      if (!_access(acSalesFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSalesFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSalesFile);
         return -1;
      }
   }

   if (!_access(acCharFile, 0) && (iLoadFlag & EXTR_ATTR))
   {
      if (strstr(acCharFile, ".csv"))
         iRet = MB_ConvChar(acCharFile);
      else
         iRet = Col_ConvStdChar(acCharFile);
      if (iRet <= 0)
      {
         LogMsg("***** Error converting Char file %s", acCharFile);
         return -1;
      }
   }

   if (iLoadFlag & LOAD_LIEN)
   {
      // Create Lien file
      LogMsg0("Load %s Lien file using file group %d", myCounty.acCntyCode, iLdrGrp);
      if (iLdrGrp == 6)                            // 2018
         iRet = Col_Load_LDR6(iSkip);
      else if (iLdrGrp == 3)                       // 2017, >=2019
         iRet = Col_Load_LDR3(iSkip);
      else if (iLdrGrp == 4)                       // 2016
         iRet = Col_Load_LDR4(iSkip);
      else
      {
         LogMsg("***** Please check the LDRGRP in INI file then retry.");
         return -1;
      }

      iLoadFlag |= MERG_CSAL;
   } else if (iLoadFlag & LOAD_UPDT)
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Col_Load_Roll(iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Col_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   // Apply NDC sale file to R01
   if (!iRet && (iLoadFlag & UPDT_XSAL))           // -Mn
   {
      // Apply Col_Ash.sls to R01 file
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSaleNR(iSkip, acCSalFile, SALE_USE_SCUPDXFR);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   return iRet;
}