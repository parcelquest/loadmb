
#if !defined(AFX_LOADMB_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_)
#define AFX_LOADMB_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"
#include "hlAdo.h"

#define  OPEN_ERR       0xF0000001
#define  READ_ERR       0xF0000002
#define  WRITE_ERR      0xF0000004

#define  UPDATE_R01     0
#define  CREATE_R01     1
#define  CREATE_LIEN    2
#define  CLEAR_R01      4

#define  LOAD_LIEN      0x10000000
#define  LOAD_UPDT      0x01000000
#define  LOAD_GRGR      0x00100000
#define  LOAD_SALE      0x00010000
#define  LOAD_ATTR      0x00001000
#define  LOAD_ASSR      0x00000100
#define  LOAD_DAILY     0x00000010
#define  UPDT_TAX       0x00000001

#define  EXTR_LIEN      0x20000000
#define  EXTR_CSAL      0x02000000
#define  EXTR_VALUE     0x00200000
#define  EXTR_SALE      0x00020000
#define  EXTR_ATTR      0x00002000
#define  EXTR_REGN      0x00000200
#define  EXTR_PRP8      0x00000020

#define  MERG_LIEN      0x40000000
#define  MERG_CSAL      0x04000000
#define  MERG_GRGR      0x00400000
#define  MERG_GISA      0x00040000
#define  MERG_ATTR      0x00004000
#define  MERG_SADR      0x00000400

#define  UPDT_XSAL      0x00080000
#define  UPDT_ASSR      0x00000800
#define  FIX_CSDOC      0x00000080
#define  FIX_CSTYP      0x00000008

// _ROLL 2016
//#define  MB_ROLL_ASMT                 0
//#define  MB_ROLL_FEEPARCEL            1
//#define  MB_ROLL_TRA                  2
//#define  MB_ROLL_LEGAL                3
//#define  MB_ROLL_ZONING               4
//#define  MB_ROLL_USECODE              5
//#define  MB_ROLL_NBHCODE              6
//#define  MB_ROLL_ACRES                7
//#define  MB_ROLL_DOCNUM               8
//#define  MB_ROLL_DOCDATE              9
//#define  MB_ROLL_TAXABILITY           10
//#define  MB_ROLL_OWNER                11
//#define  MB_ROLL_CAREOF               12
//#define  MB_ROLL_DBA                  13
//#define  MB_ROLL_M_ADDR               14
//#define  MB_ROLL_M_CITY               15
//#define  MB_ROLL_M_ST                 16
//#define  MB_ROLL_M_ZIP                17
//#define  MB_ROLL_STRNAME              18
//#define  MB_ROLL_STRNUM               19
//#define  MB_ROLL_STRTYPE              20
//#define  MB_ROLL_STRDIR               21
//#define  MB_ROLL_UNIT                 22
//#define  MB_ROLL_COMMUNITY            23
//#define  MB_ROLL_ZIP                  24
//#define  MB_ROLL_ASMTSTATUS           25
//#define  MB_ROLL_KILLEDDATE           26
//#define  MB_ROLL_INACTIVE             27

// 2017-07-04
//#define  MB_ROLL_ASMT                 0
//#define  MB_ROLL_FEEPARCEL            1
//#define  MB_ROLL_TRA                  2
//#define  MB_ROLL_LEGAL                3
//#define  MB_ROLL_USECODE              4
//#define  MB_ROLL_NBHCODE              5
//#define  MB_ROLL_ACRES                6
//#define  MB_ROLL_DOCNUM               7
//#define  MB_ROLL_DOCDATE              8
//#define  MB_ROLL_TAXABILITY           9
//#define  MB_ROLL_OWNER                10
//#define  MB_ROLL_CAREOF               11
//#define  MB_ROLL_DBA                  12
//#define  MB_ROLL_M_ADDR               13
//#define  MB_ROLL_M_CITY               14
//#define  MB_ROLL_M_ST                 15
//#define  MB_ROLL_M_ZIP                16
//#define  MB_ROLL_STRNAME              17
//#define  MB_ROLL_STRNUM               18
//#define  MB_ROLL_STRTYPE              19
//#define  MB_ROLL_STRDIR               20
//#define  MB_ROLL_UNIT                 21
//#define  MB_ROLL_COMMUNITY            22
//#define  MB_ROLL_ZIP                  23
//#define  MB_ROLL_ASMTSTATUS           24
//#define  MB_ROLL_KILLEDDATE           25
//#define  MB_ROLL_INACTIVE             26

// 2024-01-09
#define  MB_ROLL_ASMT                 0
#define  MB_ROLL_FEEPARCEL            1
#define  MB_ROLL_TRA                  2
#define  MB_ROLL_ASMTSTATUS           3
#define  MB_ROLL_LEGAL                4
#define  MB_ROLL_USECODE              5
//#define  MB_ROLL_NBHCODE              6
#define  MB_ROLL_ACRES                6
#define  MB_ROLL_DOCNUM               7
#define  MB_ROLL_DOCDATE              8
#define  MB_ROLL_TAXABILITY           9
#define  MB_ROLL_OWNER                10
#define  MB_ROLL_CAREOF               11
#define  MB_ROLL_DBA                  12
#define  MB_ROLL_M_ADDR               13
#define  MB_ROLL_M_CITY               14
#define  MB_ROLL_M_ST                 15
#define  MB_ROLL_M_ZIP                16
#define  MB_ROLL_STRNAME              17
#define  MB_ROLL_STRNUM               18
#define  MB_ROLL_STRTYPE              19
#define  MB_ROLL_STRDIR               20
#define  MB_ROLL_UNIT                 21
#define  MB_ROLL_COMMUNITY            22
#define  MB_ROLL_ZIP                  23
#define  MB_ROLL_KILLEDDATE           24
#define  MB_ROLL_INACTIVE             25
#define  MB_ROLL_FLDS                 26

// LIEN Values - 2016
//#define  MB_LIEN_ASMT                 0
//#define  MB_LIEN_LAND                 1
//#define  MB_LIEN_IMPR                 2
//#define  MB_LIEN_GROWING              3
//#define  MB_LIEN_FIXTRS               4
//#define  MB_LIEN_PP                   5
//#define  MB_LIEN_NET                  6
//#define  MB_LIEN_HOEXE                7
//#define  MB_LIEN_OTHEXE               8

// LIEN Values - 2017
#define  MB_LIEN_ASMT                 0
#define  MB_LIEN_TAXYR                1
#define  MB_LIEN_LAND                 2
#define  MB_LIEN_IMPR                 3
#define  MB_LIEN_GROWING              4
#define  MB_LIEN_FIXTRS               5
#define  MB_LIEN_PP                   6
#define  MB_LIEN_NET                  7
#define  MB_LIEN_HOEXE                8
#define  MB_LIEN_OTHEXE               9

// Current Values
#define  MB_CURR_ASMT                 0
#define  MB_CURR_DATE                 1
#define  MB_CURR_DOCNUM               2
#define  MB_CURR_LAND                 3
#define  MB_CURR_IMPR                 4
#define  MB_CURR_GROWING              5
#define  MB_CURR_FIXTRS               6
#define  MB_CURR_FIXTR_RP             7
#define  MB_CURR_PP_BUS               8
#define  MB_CURR_PPMOBILHOME          9
#define  MB_CURR_HOMESITE             10
#define  MB_CURR_HOEXE                11
#define  MB_CURR_OTHEXE               12
#define  MB_CURR_OTHEXE_CD            13

// _EXE
//#define  MB_EXE_STATUS                0
#define  MB_EXE_ASMT                  0
#define  MB_EXE_CODE                  1
#define  MB_EXE_HOEXE                 2
#define  MB_EXE_EXEAMT                3
#define  MB_EXE_EXEPCT                4

// _CHAR 
#define  MB_CHAR_FEE_PRCL             0
#define  MB_CHAR_POOLS                1
#define  MB_CHAR_USECAT               2
#define  MB_CHAR_QUALITY              3
#define  MB_CHAR_YRBLT                4
#define  MB_CHAR_BLDGSQFT             5
#define  MB_CHAR_GARSQFT              6
#define  MB_CHAR_HEATING              7
#define  MB_CHAR_COOLING              8
#define  MB_CHAR_HEATING_SRC          9
#define  MB_CHAR_COOLING_SRC          10
#define  MB_CHAR_BEDS                 11
#define  MB_CHAR_FBATHS               12
#define  MB_CHAR_HBATHS               13
#define  MB_CHAR_FP                   14
#define  MB_CHAR_UNITSEQNO            16
               
// From 07/01/2014
//#define  PLA_CHR_FEE_PRCL             0
//#define  PLA_CHR_POOLS                1
//#define  PLA_CHR_USECAT               2
//#define  PLA_CHR_QUALITY              3
//#define  PLA_CHR_YRBLT                4
//#define  PLA_CHR_BLDGSQFT             5
//#define  PLA_CHR_GARSQFT              6
//#define  PLA_CHR_HEATING              7
//#define  PLA_CHR_COOLING              8
//#define  PLA_CHR_BEDS                 9
//#define  PLA_CHR_FBATHS               10
//#define  PLA_CHR_HBATHS               11
//#define  PLA_CHR_FP                   12
//#define  PLA_CHR_VIEW                 13
//#define  PLA_CHR_BLDGTYPE             14
//#define  PLA_CHR_UNITS                15
//#define  PLA_CHR_FLDS                 16

// From 06/03/2020
#define  PLA_CHR_FEE_PRCL             0
#define  PLA_CHR_ASMT                 1
#define  PLA_CHR_ORIGASMT             2
#define  PLA_CHR_DOCNUM               3
#define  PLA_CHR_POOLS                4
#define  PLA_CHR_USECAT               5
#define  PLA_CHR_CLASS                6
#define  PLA_CHR_YRBLT                7
#define  PLA_CHR_BLDGSQFT             8
#define  PLA_CHR_GARSQFT              9
#define  PLA_CHR_HEATING              10
#define  PLA_CHR_COOLING              11
#define  PLA_CHR_BEDS                 12
#define  PLA_CHR_FBATHS               13
#define  PLA_CHR_HBATHS               14
#define  PLA_CHR_FP                   15
#define  PLA_CHR_VIEW                 16
#define  PLA_CHR_BLDGTYPE             17
#define  PLA_CHR_UNITS                18
#define  PLA_CHR_FLDS                 19

#define  MBSIZ_CHAR_ASMT              12
#define  MBSIZ_CHAR_POOLS             2
#define  MBSIZ_CHAR_USECAT            2
#define  MBSIZ_CHAR_QUALITY           6
#define  MBSIZ_CHAR_YRBLT             4
#define  MBSIZ_CHAR_BLDGSQFT          9
#define  MBSIZ_CHAR_GARSQFT           9
#define  MBSIZ_CHAR_HEATING           2
#define  MBSIZ_CHAR_COOLING           2
#define  MBSIZ_CHAR_HEATSRC           2
#define  MBSIZ_CHAR_COOLSRC           2
#define  MBSIZ_CHAR_BEDS              3
#define  MBSIZ_CHAR_FBATHS            3
#define  MBSIZ_CHAR_HBATHS            3
#define  MBSIZ_CHAR_FP                2
#define  MBSIZ_CHAR_UNITSEQNO         8

// _SALES
#define  MB_SALES_ASMT                0
#define  MB_SALES_DOCNUM              1
#define  MB_SALES_DOCDATE             2
#define  MB_SALES_DOCCODE             3
#define  MB_SALES_SELLER              4
#define  MB_SALES_BUYER               5
#define  MB_SALES_PRICE               6
#define  MB_SALES_TAXAMT              7
#define  MB_SALES_GROUPSALE           8
#define  MB_SALES_GROUPASMT           9
#define  MB_SALES_XFERTYPE            10
#define  MB_SALES_FLDS                11

//typedef struct _tCharRec
//{
//   char  Asmt[MBSIZ_CHAR_ASMT];
//   char  NumPools[MBSIZ_CHAR_POOLS];
//   char  LandUseCat[MBSIZ_CHAR_USECAT];
//   char  QualityClass[MBSIZ_CHAR_QUALITY];
//   char  YearBuilt[MBSIZ_CHAR_YRBLT];
//   char  BuildingSize[MBSIZ_CHAR_BLDGSQFT];
//   char  SqFTGarage[MBSIZ_CHAR_GARSQFT];
//   char  Heating[MBSIZ_CHAR_HEATING];
//   char  Cooling[MBSIZ_CHAR_COOLING];
//   char  HeatingSource[MBSIZ_CHAR_HEATSRC];
//   char  CoolingSource[MBSIZ_CHAR_COOLSRC];
//   char  NumBedrooms[MBSIZ_CHAR_BEDS];
//   char  NumFullBaths[MBSIZ_CHAR_FBATHS];
//   char  NumHalfBaths[MBSIZ_CHAR_HBATHS];
//   char  NumFireplaces[MBSIZ_CHAR_FP];
//   char  FeeParcel[MBSIZ_CHAR_ASMT];
//   char  HasSeptic;
//   char  HasSewer;
//   char  HasWell;
//   char  CRLF[2];
//} MB_CHAR;

static XLAT_CODE  asFirePlace[] =
{  // 11/03/2015
   // Value, lookup code, value length
   "0", "N", 1,                // None
   "1", "1", 1,                // Fireplace
   "2", "2", 1,                // Fireplaces
   "3", "3", 1,                // 
   "4", "4", 1,                // 
   "5", "5", 1,                // 
   "6", "6", 1,                // 
   "7", "7", 1,                // 
   "8", "8", 1,                // 
   "9", "9", 1,                // 
   "A", "1", 1,                // Fireplace 
   "B", "1", 1,                // Fireplace & Wood stove
   "F", "1", 1,                // Fireplace
   "G", "1", 1,                // Fireplace
   "I", "1", 1,                // Fireplace
   "M", "Y", 1,                // Fireplace
   "N", "N", 1,                // None
   "P", "S", 1,                // Pellet Stove
   "W", "W", 1,                // Wood stove
   "Y", "Y", 1,                // Yes
   "",   "", 0
};

static XLAT_CODE  asHeating[] =
{  // 11/03/2015
   // Value, lookup code, value length
   "A", "K", 1,               // Active Solar
   "B", "F", 1,               // Baseboard
   "C", "Z", 1,               // Central
   "E", "F", 1,               // Electric
   "FC","C", 2,               // Floor
   "F", "C", 1,               // Floor
   "G", "N", 1,               // ? Gas
   "H", "E", 1,               // Hydronic 
   "N", "L", 1,               // None
   "O", "X", 1,               // Other
   "P", "K", 1,               // Passive Solar 
   "S", "K", 1,               // Solar
   "W", "D", 1,               // Wall
   "1", "Y", 1,               // Heated
   "99","X", 2,               // HeatingType - Structure Heating Type (2)
   "", "",  0
};

static XLAT_CODE  asCooling[] =
{  // 11/03/2015
   // Value, lookup code, value length
   "B", "K", 1,               // Baseboard
   "C", "C", 1,               // Central
   "E", "E", 1,               // Evaporative
   "F", "M", 1,               // Floor
   "N", "N", 1,               // None
   "O", "O", 1,               // Other
   "R", "O", 1,               // Room/Wall
   "W", "L", 1,               // Wall
   "1", "O", 1,               // ?
   "99","Y", 2,               // CoolingType - Structure Cooling Type (2)
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{  // 11/02/2015
   // Value, lookup code, value length
   "GS","C", 2,               // Gunite w/Spa
   "G", "G", 1,               // Gunite in ground
   "S", "S", 1,               // Spa/Hot tub 
   "V", "V", 1,               // Vinyl in ground
   "H", "S", 1,               // Spa/Hot Tub only
   "A", "X", 1,               // Above Ground
   "N", "N", 1,               // No Pool or Spa
   "Y", "P", 1,               // Yes
   "F", "F", 1,               // Fiberglass in ground
   "99","P", 2,               // Pool (99)
   "",  "",  0
};

static XLAT_CODE  asView[] =
{
   " F", "Y", 2,              // Fair
   " G", "Y", 2,              // Good
   "GE", "M", 2,              // Excellent Golf Course View
   "GF", "M", 2,              // Fair Golf Course View
   "GG", "M", 2,              // Good Golf Course View
   "LE", "F", 2,              // Excellent Lake View
   "LF", "F", 2,              // Fair Lake View
   "LG", "F", 2,              // Good Lake View
   "ME", "T", 2,              // Excellent Mountain View
   "MF", "T", 2,              // Fair Mountain View
   "MG", "T", 2,              // Good Mountain View
   "N",  "N", 1,              // NO VIEW
   "OE", "O", 2,              // Excellent Open Space
   "OF", "O", 2,              // Fair Open Space
   "OG", "O", 2,              // Good Open Space
   "SE", "5", 2,              // Excellent Stream View
   "SF", "2", 2,              // Fair Stream View
   "SG", "4", 2,              // Good Stream View
   "VE", "R", 2,              // Excellent Valley View
   "VF", "R", 2,              // Fair Valley View
   "VG", "R", 2,              // Good Valley View
   "",  "",  0
};

IDX_TBL5 PLA_DocCode[] =
{  // DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN
   "01", "1 ", 'N', 2, 2,     // TRANSFER
   "02", "13", 'N', 2, 2,     // PARTIAL TRANSFER-REAPPRAISAL/Deed
   "03", "27", 'Y', 2, 2,     // FORECLOSURE-REAPPRAISAL/Trustees Deed
   "04", "25", 'N', 2, 2,     // SHERIFF'S DEED REAPPRAISAL
   "05", "74", 'Y', 2, 2,     // NEW MOBILE HOME
   "06", "13", 'N', 2, 2,     // PROP 58 100% REAPPRAISAL
   "07", "13", 'N', 2, 2,     // PROP 58 PARTIAL REAPPRAISAL
   "08", "67", 'N', 2, 2,     // TAX DEED REAPPRAISAL
   "09", "13", 'Y', 2, 2,     // TRUST-REAPPRAISAL/Lease assignment
   "10", "13", 'N', 2, 2,     // TIMESHARE-REAPPRAISAL
   "11", "13", 'Y', 2, 2,     // 100% TRANSFER NO REAPPRAISAL
   "12", "13", 'Y', 2, 2,     // PARTIAL TRANSFER NO REAPPRAISA
   "13", "18", 'Y', 2, 2,     // INTERSPOUSAL
   "14", "61", 'Y', 2, 2,     // TRUST
   "15", "13", 'N', 2, 2,     // STRAWMAN TRANSFER
   "16", "13", 'Y', 2, 2,     // CHANGE FORM OF TITLE/Deed/Order
   "17", "13", 'Y', 2, 2,     // CORRECT/PERFECT TITLE
   "18", "74", 'Y', 2, 2,     // CAL VET-NO APPRAISAL
   "19", "74", 'Y', 2, 2,     // NEW MOBILE HOME-VOLUNTARY CONV
   "20", "74", 'Y', 2, 2,     // RESCISSIONS
   "21", "3 ", 'Y', 2, 2,     // END J/T OR LIFE EST 100% REAPP
   "22", "3 ", 'Y', 2, 2,     // END J/T OR LIFE EST PART REAPP
   "23", "3 ", 'Y', 2, 2,     // ADD J/T (ORIG TRANS REMAINS)
   "24", "74", 'Y', 2, 2,     // RETURN TO ORIG TRANSFEROR-NO R
   "25", "13", 'Y', 2, 2,     // RETAIN LIFE EST-NO REAPP
   "26", "74", 'Y', 2, 2,     // CREATE LIFE ESTATE-REAPPRAISAL
   "27", "74", 'Y', 2, 2,     // TERMINATE LIFE ESTATE-NO REAPP
   "28", "74", 'Y', 2, 2,     // NO ACTION BUILDING PERMIT
   "29", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-PORC
   "30", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-NEW SFR
   "31", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-NEW MULTI
   "32", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-NEW COMML/IND
   "33", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-NEW MH
   "34", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-NEW MISC
   "35", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-ADDS/ALT,SFR
   "36", "74", 'Y', 2, 2,     // NEW CONST-ADDS/ALT,MULTI
   "37", "74", 'Y', 2, 2,     // NEW CONST-ADDS/ALT,COMML/IND
   "38", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-ADDS/ALT,MH
   "39", "74", 'Y', 2, 2,     // AGRICULTURE BUILDINGS
   "40", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-REMOVAL/IMP
   "41", "75", 'Y', 2, 2,     // TRANSFER TO PUBLIC ENTITY
   "42", "74", 'Y', 2, 2,     // FEDERAL SEIZURES
   "43", "74", 'Y', 2, 2,     // LIEN DATE UPDATE
   "44", "74", 'Y', 2, 2,     // CIP  RESIDENTIAL
   "45", "74", 'Y', 2, 2,     // CIP MULTI
   "46", "74", 'Y', 2, 2,     // CIP COMML/IND
   "47", "74", 'Y', 2, 2,     // PROP 8
   "48", "74", 'Y', 2, 2,     // BOARD ORDER CHANGE
   "49", "74", 'Y', 2, 2,     // DOMESTIC PARTNER NO REAPPRAISA
   "50", "74", 'Y', 2, 2,     // CONSERVATION EASEMENTS
   "51", "74", 'Y', 2, 2,     // LAND CONSERVATION AGRMT
   "52", "74", 'Y', 2, 2,     // AMENDED LAND CONSERVATION AGRM
   "53", "74", 'Y', 2, 2,     // NON-RENEW LAND CONSERVATION AG
   "54", "74", 'Y', 2, 2,     // ANNUAL REVIEW TPZ
   "55", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-POOLS
   "56", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-PATIO COVERS
   "57", "74", 'Y', 2, 2,     // APPEAL
   "58", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-DECKS
   "59", "74", 'Y', 2, 2,     // NEW CONSTRUCTION-GARAGE CONV
   "60", "74", 'Y', 2, 2,     // PROPOSITION 60
   "61", "44", 'Y', 2, 2,     // LEASE-REAPPRAISAL/Lease/Deed...
   "62", "44", 'Y', 2, 2,     // CANCEL LEASE-REAPPRAISAL
   "63", "44", 'Y', 2, 2,     // LEASE-NO REAPPRAISAL
   "64", "44", 'Y', 2, 2,     // CANCEL LEASE-NO REAPPRAISAL
   "67", "74", 'Y', 2, 2,     // CALAMITY FLOOD
   "68", "74", 'Y', 2, 2,     // CALAMITY
   "69", "74", 'Y', 2, 2,     // REMOVAL OF IMPROVEMENTS
   "71", "74", 'Y', 2, 2,     // BOUNDRY LINE ADJUSTMENT
   "72", "19", 'Y', 2, 2,     // SPLIT
   "73", "19", 'Y', 2, 2,     // COMBINATION
   "74", "74", 'Y', 2, 2,     // ROADWAY ACQUISITION
   "75", "74", 'Y', 2, 2,     // ROAD ABANDONMENT
   "76", "74", 'Y', 2, 2,     // ORDER OF POSSESSION
   "77", "74", 'Y', 2, 2,     // DIVISION OF INTERESTS
   "78", "74", 'Y', 2, 2,     // PAGE TRANSFER/BLOCKED PAGE
   "79", "40", 'Y', 2, 2,     // EASEMENTS
   "80", "74", 'Y', 2, 2,     // PROP 8 REVIEW
   "81", "74", 'Y', 2, 2,     // MOBILE HOME ON FOUNDATION
   "82", "74", 'Y', 2, 2,     // GOVERNMENT ADD/ALT/NEW PERMITS
   "83", "3 ", 'Y', 2, 2,     // JOINT TENANCY WITH A TRUST
   "84", "74", 'Y', 2, 2,     // MINING CLAIMS
   "85", "74", 'Y', 2, 2,     // MINES AND QUARRIES
   "87", "74", 'Y', 2, 2,     // AFFORDABLE HOUSING
   "88", "74", 'Y', 2, 2,     // POSSESSORY INTEREST
   "89", "74", 'Y', 2, 2,     // IREO
   "90", "74", 'Y', 2, 2,     // DEATH-NO REAPPRAISAL
   "91", "74", 'Y', 2, 2,     // DEATH DATE-100% REAPPRAISAL
   "92", "74", 'Y', 2, 2,     // DEATH DATE-PARTIAL REAPPRAISAL
   "93", "74", 'Y', 2, 2,     // LEGAL ENTITY CHANGE
   "94", "74", 'Y', 2, 2,     // HOLDING COMPANY
   "95", "74", 'Y', 2, 2,     // UNRECORDED CONTRACT
   "96", "74", 'Y', 2, 2,     // MISCELLANEOUS
   "97", "74", 'Y', 2, 2,     // HOX-CLAIM REQUEST
   "98", "74", 'Y', 2, 2,     // PROBLEM DEED NO REAPP
   "99", "74", 'Y', 2, 2,     // PROBLEM DEED-REAPPRAISAL
   "", "", '\0', 0, 0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 PLA_Exemption[] = 
{
   "E01", "H", 3,1,     // H HOMEOWNERS EXEMPTION
   "E02", "D", 3,1,     // D DISABLED VETERAN 100000
   "E03", "D", 3,1,     // D DISABLED VET 150000
   "E08", "V", 3,1,     // V VETERAN
   "E09", "C", 3,1,     // C CHURCH
   "E10", "R", 3,1,     // R RELIGIOUS
   "E11", "W", 3,1,     // W WELFARE
   "E12", "W", 3,1,     // W WELFARE - PRIV PAR SCHOOL
   "E13", "I", 3,1,     // W HOSPITAL
   "E15", "L", 3,1,     // E LIBRARY
   "E16", "M", 3,1,     // M MUSEUM
   "E17", "P", 3,1,     // P PUBLIC SCHOOL
   "E18", "E", 3,1,     // B CEMETARY
   "E19", "U", 3,1,     // S COLLEGE
   "E20", "X", 3,1,     // L LESSOR EXEMPT
   "E21", "X", 3,1,     // L QUALIFIED LESSOR
   "E22", "Y", 3,1,     // F SOLDIERS&SAILORS
   "E23", "X", 3,1,     // Y BOAT 4%
   "E24", "X", 3,1,     // U OTHER AIRCRAFT
   "E25", "X", 3,1,     // T HISTORICAL AIRCRAFT
   "E26", "X", 3,1,     // A WORK OF ART
   "E27", "X", 3,1,     // J EXHIBITION
   "E28", "V", 3,1,     // K VETS ORGANIZATION
   "E29", "G", 3,1,     // G GOVT ENTITY
   "E30", "X", 3,1,     // Z LOW VALUE PROP
   "E31", "X", 3,1,     // I INVENTORY
   "E32", "X", 3,1,     // O OTHER
   "E34", "P", 3,1,     // P PUBLIC SCHOOL PARTIAL LAND
   "E35", "P", 3,1,     // P PUBLIC SCHOOL PARTIAL IMP
   "E36", "P", 3,1,     // P PUBLIC SCHOOL PARTIAL PP
   "E37", "U", 3,1,     // S COLLEGE PARTIAL LAND
   "E38", "U", 3,1,     // S COLLEGE PARTIAL IMP
   "E39", "U", 3,1,     // S COLLEGE PARTIAL PP
   "E40", "W", 3,1,     // W PARTIAL WEL-LAND
   "E41", "W", 3,1,     // W PARTIAL WEL-IMPRV
   "E42", "W", 3,1,     // W PARTIAL WELFARE PP
   "E43", "S", 3,1,     // W PARTIAL WEL-SCHOOL LAND
   "E44", "S", 3,1,     // W PARTIAL WEL SCHOOL IMP
   "E45", "S", 3,1,     // W PARTIAL WEL SCHOOL PP
   "E46", "D", 3,1,     // D PARTIAL DISABLED VET 100000
   "E47", "D", 3,1,     // D PARTIAL DISABLED VET 150000
   "E50", "C", 3,1,     // C PARTIAL CHURCH-LAND
   "E51", "C", 3,1,     // C PARTIAL CHURCH-IMPROVEMENT
   "E52", "C", 3,1,     // C PARTIAL CHURCH PP
   "E60", "R", 3,1,     // R PARTIAL RELIGIOUS-LAND
   "E61", "R", 3,1,     // R PARTIAL RELIGIOUS-IMPR
   "E62", "R", 3,1,     // R PARTIAL RELIGIOUS PP
   "E63", "I", 3,1,     // W PARTIAL HOSPITAL - LAND
   "E64", "I", 3,1,     // W PARTIAL HOSPITAL - IMPRV
   "E65", "I", 3,1,     // W PARTIAL HOSPITAL -  PP
   "E70", "D", 3,1,     // DISABLED VETERAN 100000 CLAIM FORM
   "E71", "D", 3,1,     // DISABLED VETERAN 150000 CLAIM FORM
   "E72", "C", 3,1,     // CHURCH CLAIM FORM
   "E73", "R", 3,1,     // RELIGIOUS CLAIM FORM
   "E74", "R", 3,1,     // RELIGIOUS & WELFARE CLAIM FORMS
   "E75", "W", 3,1,     // WELFARE CLAIM FORM - ANNUAL
   "E76", "W", 3,1,     // WELFARE CLAIM FORM - ELDERLY/HANDICAP
   "E77", "W", 3,1,     // WELFARE CLAIM FORM - LOW-INCOME HOUSING
   "E78", "W", 3,1,     // WELFARE CLAIM FORM - LIMITED PARTNERSHIP
   "E79", "W", 3,1,     // WELFARE CLAIM FORM - REHABILITATION
   "E80", "M", 3,1,     // MUSEUM CLAIM FORM
   "E81", "P", 3,1,     // PUBLIC SCHOOL CLAIM FORM
   "E82", "U", 3,1,     // COLLEGE CLAIM FORM
   "E83", "X", 3,1,     // LESSOR CLAIM FORM
   "E84", "Y", 3,1,     // SOLDIERS & SAILORS CLAIM FORM
   "E85", "X", 3,1,     // CLAIM FORM SENT ON OTHER ASSESSMENT
   "E86", "X", 3,1,     // REVIEW
   "E87", "X", 3,1,     // LESSOR CLAIM FORM - LEASED PP
   "E88", "W", 3,1,     // WELFARE CLAIM FORM - LMTD PARTNERSHIP-LEASED LAND
   "E97", "H", 3,1,     // HOX PENALTY
   "E98", "X", 3,1,     // OTHER EXEMPTION
   "E99", "X", 3,1,     // LATE FILE FLAG
   "P19", "X", 3,1,     // PROP 19 - FOR BYVT 19 REPORTING ONLY
   "","",0,0
};
   
#endif // !defined(AFX_LOADMB_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_)
