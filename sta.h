#include "resource.h"
#include "hlAdo.h"

#ifndef _STANISLAUS_
#define _STANISLAUS_

#define  MAX_SUFFIX     256
#define  MAX_RECSIZE    4096
#define  MAX_FLD_TOKEN  256

#define  OPEN_ERR       0xF0000001
#define  READ_ERR       0xF0000002
#define  WRITE_ERR      0xF0000004

#define  UPDATE_R01     0
#define  CREATE_R01     1
#define  CREATE_LIEN    2
#define  CLEAR_R01      4

#define  LOAD_LIEN      0x10000000
#define  LOAD_UPDT      0x01000000
#define  LOAD_GRGR      0x00100000
#define  LOAD_DAILY     0x00100000
#define  LOAD_SALE      0x00010000
#define  LOAD_ATTR      0x00001000
#define  LOAD_ASSR      0x00000100

#define  EXTR_LIEN      0x20000000
#define  EXTR_CSAL      0x02000000
#define  EXTR_GRGR      0x00200000
#define  EXTR_SALE      0x00020000
#define  EXTR_ATTR      0x00002000
#define  EXTR_REGN      0x00000200
#define  EXTR_PRP8      0x00000020

#define  MERG_LIEN      0x40000000
#define  MERG_CSAL      0x04000000
#define  MERG_GRGR      0x00400000
#define  MERG_SALE      0x00040000
#define  MERG_ATTR      0x00004000
#define  MERG_SADR      0x00000400
#define  MERG_PUBL      0x00000040

#define  UPDT_SALE      0x00080000
#define  UPDT_ASSR      0x00000800

// MON_LIEN.CSV and CAL_LIEN.CSV
#define  L_ASMT                        0
#define  L_TAXYEAR                     1
#define  L_ROLLCHGNUM                  2
#define  L_MAPCATEGORY                 3
#define  L_ROLLCATEGORY                4
#define  L_ROLLTYPE                    5
#define  L_DESTINATIONROLL             6
#define  L_INSTALLMENTS                7
#define  L_BILLTYPE                    8
#define  L_FEEPARCEL                   9
#define  L_ORIGINATINGASMT             10
#define  L_XREFASMT                    11
#define  L_STATUS                      12
#define  L_TRA                         13
#define  L_TAXABILITY                  14
#define  L_ACRES                       15
#define  L_SIZEACRESFTTYPE             16
#define  L_INTDATEFROM                 17
#define  L_INTDATEFROM2                18
#define  L_INTDATETHRU                 19
#define  L_INTERESTTYPE                20
#define  L_USECODE                     21
#define  L_CURRENTMARKETLANDVALUE      22
#define  L_CURRENTFIXEDIMPRVALUE       23
#define  L_CURRENTGROWINGIMPRVALUE     24
#define  L_CURRENTSTRUCTURALIMPRVALUE  25
#define  L_CURRENTPERSONALPROPVALUE    26
#define  L_CURRENTPERSONALPROPMHVALUE  27
#define  L_CURRENTNETVALUE             28
#define  L_BILLEDMARKETLANDVALUE       29
#define  L_BILLEDFIXEDIMPRVALUE        30
#define  L_BILLEDGROWINGIMPRVALUE      31
#define  L_BILLEDSTRUCTURALIMPRVALUE   32
#define  L_BILLEDPERSONALPROPVALUE     33
#define  L_BILLEDPERSONALPROPMHVALUE   34
#define  L_BILLEDNETVALUE              35
#define  L_OWNER                       36
#define  L_ASSESSEE                    37
#define  L_MAILADDRESS1                38
#define  L_MAILADDRESS2                39
#define  L_MAILADDRESS3                40
#define  L_MAILADDRESS4                41
#define  L_ZIPMATCH                    42
#define  L_BARCODEZIP                  43
#define  L_EXEMPTIONCODE1              44
#define  L_EXEMPTIONAMT1               45
#define  L_EXEMPTIONCODE2              46
#define  L_EXEMPTIONAMT2               47
#define  L_EXEMPTIONCODE3              48
#define  L_EXEMPTIONAMT3               49
#define  L_BILLDATE                    50
#define  L_DUEDATE1                    51
#define  L_DUEDATE2                    52
#define  L_TAXAMT1                     53
#define  L_TAXAMT2                     54
#define  L_PENAMT1                     55
#define  L_PENAMT2                     56
#define  L_COST1                       57
#define  L_COST2                       58
#define  L_PENCHRGDATE1                59
#define  L_PENCHRGDATE2                60
#define  L_PAIDAMT1                    61
#define  L_PAIDAMT2                    62
#define  L_PAYMENTDATE1                63
#define  L_PAYMENTDATE2                64
#define  L_TRANSDATE1                  65
#define  L_TRANSDATE2                  66
#define  L_COLLECTIONNUM1              67
#define  L_COLLECTIONNUM2              68
#define  L_UNSDELINQPENAMTPAID1        69
#define  L_SECDELINQPENAMTPAID2        70
#define  L_TOTALFEESPAID1              71
#define  L_TOTALFEESPAID2              72
#define  L_TOTALFEES                   73
#define  L_PMTCNLDATE1                 74
#define  L_PMTCNLDATE2                 75
#define  L_TRANSFERDATE                76
#define  L_PRORATIONFACTOR             77
#define  L_PRORATIONPCT                78
#define  L_DAYSTOFISCALYEAREND         79
#define  L_DAYSOWNED                   80
#define  L_OWNFROM                     81
#define  L_OWNTHRU                     82
#define  L_SITUS1                      83
#define  L_SITUS2                      84
#define  L_ASMTROLLYEAR                85
#define  L_PARCELDESCRIPTION           86
#define  L_BILLCOMMENTSLINE1           87
#define  L_BILLCOMMENTSLINE2           88
#define  L_DEFAULTNUM                  89
#define  L_DEFAULTDATE                 90
#define  L_REDEEMEDDATE                91
#define  L_SECDELINQFISCALYEAR         92
#define  L_PRIORTAXPAID1               93
#define  L_PENINTERESTCODE             94
#define  L_FOURPAYPLANNUM              95
#define  L_EXISTSLIEN                  96
#define  L_EXISTSCORTAC                97
#define  L_EXISTSCOUNTYCORTAC          98
#define  L_EXISTSBANKRUPTCY            99
#define  L_EXISTSREFUND                100
#define  L_EXISTSDELINQUENTVESSEL      101
#define  L_EXISTSNOTES                 102
#define  L_EXISTSCRITICALNOTE          103
#define  L_EXISTSROLLCHG               104
#define  L_ISPENCHRGCANCELED1          105
#define  L_ISPENCHRGCANCELED2          106
#define  L_ISPERSONALPROPERTYPENALTY   107
#define  L_ISAGPRESERVE                108
#define  L_ISCARRYOVER1STPAID          109
#define  L_ISCARRYOVERRECORD           110
#define  L_ISFAILURETOFILE             111
#define  L_ISOWNERSHIPPENALTY          112
#define  L_ISELIGIBLEFOR4PAY           113
#define  L_ISNOTICE4SENT               114
#define  L_ISPARTPAY                   115
#define  L_ISFORMATTEDADDRESS          116
#define  L_ISADDRESSCONFIDENTIAL       117
#define  L_SUPLCNT                     118
#define  L_ORIGINALDUEDATE1            119
#define  L_ORIGINALDUEDATE2            120
#define  L_ISPEN1REFUND                121
#define  L_ISPEN2REFUND                122
#define  L_ISDELINQPENREFUND           123
#define  L_ISREFUNDAUTHORIZED          124
#define  L_ISNODISCHARGE               125
#define  L_ISAPPEALPENDING             126
#define  L_OTHERFEESPAID               127
#define  L_FOURPAYREASON               128
#define  L_DATEDISCHARGED              129
#define  L_ISONLYFIRSTPAID             130
#define  L_ISALLPAID                   131
#define  L_DTS                         132
#define  L_USERID                      133

// Group MB1
#define  L1_ASMT                        0
#define  L1_TAXYEAR                     1
#define  L1_MAPCATEGORY                 2
#define  L1_ROLLCATEGORY                3
#define  L1_ROLLTYPE                    4
#define  L1_DESTINATIONROLL             5
#define  L1_INSTALLMENTS                6
#define  L1_BILLTYPE                    7
#define  L1_FEEPARCEL                   8
#define  L1_ORIGINATINGASMT             9
#define  L1_XREFASMT                    10
#define  L1_STATUS                      11
#define  L1_TRA                         12
#define  L1_TAXABILITY                  13
#define  L1_ACRES                       14
#define  L1_SIZEACRESFTTYPE             15
#define  L1_INTDATEFROM                 16
#define  L1_INTDATEFROM2                17
#define  L1_INTDATETHRU                 18
#define  L1_INTERESTTYPE                19
#define  L1_USECODE                     20
#define  L1_CURRENTMARKETLANDVALUE      21
#define  L1_CURRENTFIXEDIMPRVALUE       22
#define  L1_CURRENTGROWINGIMPRVALUE     23
#define  L1_CURRENTSTRUCTURALIMPRVALUE  24
#define  L1_CURRENTPERSONALPROPVALUE    25
#define  L1_CURRENTPERSONALPROPMHVALUE  26
#define  L1_CURRENTNETVALUE             27
#define  L1_BILLEDMARKETLANDVALUE       28
#define  L1_BILLEDFIXEDIMPRVALUE        29
#define  L1_BILLEDGROWINGIMPRVALUE      30
#define  L1_BILLEDSTRUCTURALIMPRVALUE   31
#define  L1_BILLEDPERSONALPROPVALUE     32
#define  L1_BILLEDPERSONALPROPMHVALUE   33
#define  L1_BILLEDNETVALUE              34
#define  L1_OWNER                       35
#define  L1_ASSESSEE                    36
#define  L1_MAILADDRESS1                37
#define  L1_MAILADDRESS2                38
#define  L1_MAILADDRESS3                39
#define  L1_MAILADDRESS4                40
#define  L1_ZIPMATCH                    41
#define  L1_BARCODEZIP                  42
#define  L1_EXEMPTIONCODE1              43
#define  L1_EXEMPTIONAMT1               44
#define  L1_EXEMPTIONCODE2              45
#define  L1_EXEMPTIONAMT2               46
#define  L1_EXEMPTIONCODE3              47
#define  L1_EXEMPTIONAMT3               48
#define  L1_BILLDATE                    49
#define  L1_DUEDATE1                    50
#define  L1_DUEDATE2                    51
#define  L1_TAXAMT1                     52
#define  L1_TAXAMT2                     53
#define  L1_PENAMT1                     54
#define  L1_PENAMT2                     55
#define  L1_COST1                       56
#define  L1_COST2                       57
#define  L1_PENCHRGDATE1                58
#define  L1_PENCHRGDATE2                59
#define  L1_PAIDAMT1                    60
#define  L1_PAIDAMT2                    61
#define  L1_PAYMENTDATE1                62
#define  L1_PAYMENTDATE2                63
#define  L1_TRANSDATE1                  64
#define  L1_TRANSDATE2                  65
#define  L1_COLLECTIONNUM1              66
#define  L1_COLLECTIONNUM2              67
#define  L1_UNSDELINQPENAMTPAID1        68
#define  L1_SECDELINQPENAMTPAID2        69
#define  L1_TOTALFEESPAID1              70
#define  L1_TOTALFEESPAID2              71
#define  L1_TOTALFEES                   72
#define  L1_PMTCNLDATE1                 73
#define  L1_PMTCNLDATE2                 74
#define  L1_TRANSFERDATE                75
#define  L1_PRORATIONFACTOR             76
#define  L1_PRORATIONPCT                77
#define  L1_DAYSTOFISCALYEAREND         78
#define  L1_DAYSOWNED                   79
#define  L1_OWNFROM                     80
#define  L1_OWNTHRU                     81
#define  L1_SITUS1                      82
#define  L1_SITUS2                      83
#define  L1_ASMTROLLYEAR                84
#define  L1_PARCELDESCRIPTION           85
#define  L1_BILLCOMMENTSLINE1           86
#define  L1_BILLCOMMENTSLINE2           87
#define  L1_DEFAULTNUM                  88
#define  L1_DEFAULTDATE                 89
#define  L1_REDEEMEDDATE                90
#define  L1_SECDELINQFISCALYEAR         91
#define  L1_PRIORTAXPAID1               92
#define  L1_PENINTERESTCODE             93
#define  L1_FOURPAYPLANNUM              94
#define  L1_EXISTSLIEN                  95
#define  L1_EXISTSCORTAC                96
#define  L1_EXISTSCOUNTYCORTAC          97
#define  L1_EXISTSBANKRUPTCY            98
#define  L1_EXISTSREFUND                99
#define  L1_EXISTSDELINQUENTVESSEL      100
#define  L1_EXISTSNOTES                 101
#define  L1_EXISTSCRITICALNOTE          102
#define  L1_EXISTSROLLCHG               103
#define  L1_ISPENCHRGCANCELED1          104
#define  L1_ISPENCHRGCANCELED2          105
#define  L1_ISPERSONALPROPERTYPENALTY   106
#define  L1_ISAGPRESERVE                107
#define  L1_ISCARRYOVER1STPAID          108
#define  L1_ISCARRYOVERRECORD           109
#define  L1_ISFAILURETOFILE             110
#define  L1_ISOWNERSHIPPENALTY          111
#define  L1_ISELIGIBLEFOR4PAY           112
#define  L1_ISNOTICE4SENT               113
#define  L1_ISPARTPAY                   114
#define  L1_ISFORMATTEDADDRESS          115
#define  L1_ISADDRESSCONFIDENTIAL       116
#define  L1_SUPLCNT                     117
#define  L1_ORIGINALDUEDATE1            118
#define  L1_ORIGINALDUEDATE2            119
#define  L1_ISPEN1REFUND                120
#define  L1_ISPEN2REFUND                121
#define  L1_ISDELINQPENREFUND           122
#define  L1_ISREFUNDAUTHORIZED          123
#define  L1_ISNODISCHARGE               124
#define  L1_ISAPPEALPENDING             125
#define  L1_OTHERFEESPAID               126
#define  L1_FOURPAYREASON               127
#define  L1_DATEDISCHARGED              128
#define  L1_ISONLYFIRSTPAID             129
#define  L1_ISALLPAID                   131
#define  L1_DTS                         132
#define  L1_USERID                      133
#define  L1_CORTAC                      134

#define  L2_ASMT                        0
#define  L2_TAXYEAR                     1
#define  L2_ROLLCHGNUM                  2
#define  L2_MAPCATEGORY                 3
#define  L2_ROLLCATEGORY                4
#define  L2_ROLLTYPE                    5
#define  L2_DESTINATIONROLL             6
#define  L2_INSTALLMENTS                7
#define  L2_BILLTYPE                    8
#define  L2_FEEPARCEL                   9
#define  L2_ORIGINATINGASMT             10
#define  L2_XREFASMT                    11
#define  L2_STATUS                      12
#define  L2_TRA                         13
#define  L2_TAXABILITY                  14
#define  L2_ACRES                       15
#define  L2_SIZEACRESFTTYPE             16
#define  L2_INTDATEFROM                 17
#define  L2_INTDATEFROM2                18
#define  L2_INTDATETHRU                 19
#define  L2_INTERESTTYPE                20
#define  L2_USECODE                     21
#define  L2_CURRENTMARKETLANDVALUE      22
#define  L2_CURRENTFIXEDIMPRVALUE       23
#define  L2_CURRENTGROWINGIMPRVALUE     24
#define  L2_CURRENTSTRUCTURALIMPRVALUE  25
#define  L2_CURRENTPERSONALPROPVALUE    26
#define  L2_CURRENTPERSONALPROPMHVALUE  27
#define  L2_CURRENTNETVALUE             28
#define  L2_BILLEDMARKETLANDVALUE       29
#define  L2_BILLEDFIXEDIMPRVALUE        30
#define  L2_BILLEDGROWINGIMPRVALUE      31
#define  L2_BILLEDSTRUCTURALIMPRVALUE   32
#define  L2_BILLEDPERSONALPROPVALUE     33
#define  L2_BILLEDPERSONALPROPMHVALUE   34
#define  L2_BILLEDNETVALUE              35
#define  L2_OWNER                       36
#define  L2_ASSESSEE                    37
#define  L2_MAILADDRESS1                38
#define  L2_MAILADDRESS2                39
#define  L2_MAILADDRESS3                40
#define  L2_MAILADDRESS4                41
#define  L2_ZIPMATCH                    42
#define  L2_BARCODEZIP                  43
#define  L2_EXEMPTIONCODE1              44
#define  L2_EXEMPTIONAMT1               45
#define  L2_EXEMPTIONCODE2              46
#define  L2_EXEMPTIONAMT2               47
#define  L2_EXEMPTIONCODE3              48
#define  L2_EXEMPTIONAMT3               49
#define  L2_BILLDATE                    50
#define  L2_DUEDATE1                    51
#define  L2_DUEDATE2                    52
#define  L2_TAXAMT1                     53
#define  L2_TAXAMT2                     54
#define  L2_PENAMT1                     55
#define  L2_PENAMT2                     56
#define  L2_COST1                       57
#define  L2_COST2                       58
#define  L2_PENCHRGDATE1                59
#define  L2_PENCHRGDATE2                60
#define  L2_PAIDAMT1                    61
#define  L2_PAIDAMT2                    62
#define  L2_PAYMENTDATE1                63
#define  L2_PAYMENTDATE2                64
#define  L2_TRANSDATE1                  65
#define  L2_TRANSDATE2                  66
#define  L2_COLLECTIONNUM1              67
#define  L2_COLLECTIONNUM2              68
#define  L2_UNSDELINQPENAMTPAID1        69
#define  L2_SECDELINQPENAMTPAID2        70
#define  L2_TOTALFEESPAID1              71
#define  L2_TOTALFEESPAID2              72
#define  L2_TOTALFEES                   73
#define  L2_PMTCNLDATE1                 74
#define  L2_PMTCNLDATE2                 75
#define  L2_TRANSFERDATE                76
#define  L2_PRORATIONFACTOR             77
#define  L2_PRORATIONPCT                78
#define  L2_DAYSTOFISCALYEAREND         79
#define  L2_DAYSOWNED                   80
#define  L2_OWNFROM                     81
#define  L2_OWNTHRU                     82
#define  L2_SITUS1                      83
#define  L2_SITUS2                      84
#define  L2_ASMTROLLYEAR                85
#define  L2_PARCELDESCRIPTION           86
#define  L2_BILLCOMMENTSLINE1           87
#define  L2_BILLCOMMENTSLINE2           88
#define  L2_DEFAULTNUM                  89
#define  L2_DEFAULTDATE                 90
#define  L2_REDEEMEDDATE                91
#define  L2_SECDELINQFISCALYEAR         92
#define  L2_PRIORTAXPAID1               93
#define  L2_PENINTERESTCODE             94
#define  L2_FOURPAYPLANNUM              95
#define  L2_EXISTSLIEN                  96
#define  L2_EXISTSCORTAC                97
#define  L2_EXISTSCOUNTYCORTAC          98
#define  L2_EXISTSBANKRUPTCY            99
#define  L2_EXISTSREFUND                100
#define  L2_EXISTSDELINQUENTVESSEL      101
#define  L2_EXISTSNOTES                 102
#define  L2_EXISTSCRITICALNOTE          103
#define  L2_EXISTSROLLCHG               104
#define  L2_ISPENCHRGCANCELED1          105
#define  L2_ISPENCHRGCANCELED2          106
#define  L2_ISPERSONALPROPERTYPENALTY   107
#define  L2_ISAGPRESERVE                108
#define  L2_ISCARRYOVER1STPAID          109
#define  L2_ISCARRYOVERRECORD           110
#define  L2_ISFAILURETOFILE             111
#define  L2_ISOWNERSHIPPENALTY          112
#define  L2_ISELIGIBLEFOR4PAY           113
#define  L2_ISNOTICE4SENT               114
#define  L2_ISPARTPAY                   115
#define  L2_ISFORMATTEDADDRESS          116
#define  L2_ISADDRESSCONFIDENTIAL       117
#define  L2_SUPLCNT                     118
#define  L2_ORIGINALDUEDATE1            119
#define  L2_ORIGINALDUEDATE2            120
#define  L2_ISPEN1REFUND                121
#define  L2_ISPEN2REFUND                122
#define  L2_ISDELINQPENREFUND           123
#define  L2_ISREFUNDAUTHORIZED          124
#define  L2_ISNODISCHARGE               125
#define  L2_ISAPPEALPENDING             126
#define  L2_OTHERFEESPAID               127
#define  L2_FOURPAYREASON               128
#define  L2_DATEDISCHARGED              129
#define  L2_ISONLYFIRSTPAID             130
#define  L2_ISALLPAID                   131
#define  L2_DTS                         132
#define  L2_USERID                      133
#define  L2_TAX_RATEUSED                134
#define  L2_TAX_ROLLCHGNUM              135

// _ROLL
#define  MB_ROLL_ASMT                 0
#define  MB_ROLL_FEEPARCEL            1
#define  MB_ROLL_TRA                  2
#define  MB_ROLL_LEGAL                3
#define  MB_ROLL_ZONING               4
#define  MB_ROLL_USECODE              5
#define  MB_ROLL_NBHCODE              6
#define  MB_ROLL_ACRES                7
#define  MB_ROLL_DOCNUM               8
#define  MB_ROLL_DOCDATE              9
#define  MB_ROLL_TAXABILITY           10
#define  MB_ROLL_OWNER                11
#define  MB_ROLL_CAREOF               12
#define  MB_ROLL_DBA                  13
#define  MB_ROLL_M_ADDR               14
#define  MB_ROLL_M_CITY               15
#define  MB_ROLL_M_ST                 16
#define  MB_ROLL_M_ZIP                17
#define  MB_ROLL_LAND                 18
#define  MB_ROLL_HOMESITE             19
#define  MB_ROLL_IMPR                 20
#define  MB_ROLL_GROWING              21
#define  MB_ROLL_FIXTRS               22

#define  MB_ROLL_FIXTR_RP             23  // Old script
#define  MB_ROLL_FIXTR_BUS            24
#define  MB_ROLL_PP_BUS               24

#define  MB_ROLL_PERSPROP             23  // New script
#define  MB_ROLL_BUSPROP              24

#define  MB_ROLL_PPMOBILHOME          25
#define  MB_ROLL_M_ADDR1              26
#define  MB_ROLL_M_ADDR2              27
#define  MB_ROLL_M_ADDR3              28
#define  MB_ROLL_M_ADDR4              29

// _EXE
#define  MB_EXE_STATUS                0
#define  MB_EXE_ASMT                  1
#define  MB_EXE_CODE                  2
#define  MB_EXE_HOEXE                 3
#define  MB_EXE_EXEAMT                4
#define  MB_EXE_EXEPCT                5

// _CHAR
#define  MB_CHAR_FEE_PRCL             0
#define  MB_CHAR_POOLS                1
#define  MB_CHAR_USECAT               2
#define  MB_CHAR_LANDUSE              3
#define  MB_CHAR_QUALITY              4
#define  MB_CHAR_YRBLT                5
#define  MB_CHAR_BLDGSQFT             6
#define  MB_CHAR_GARSQFT              7
#define  MB_CHAR_HEATING              8
#define  MB_CHAR_COOLING              9
#define  MB_CHAR_HEATING_SRC          10
#define  MB_CHAR_COOLING_SRC          11
#define  MB_CHAR_BEDS                 12
#define  MB_CHAR_FBATHS               13
#define  MB_CHAR_HBATHS               14
#define  MB_CHAR_FP                   15
#define  MB_CHAR_ASMT                 16
#define  MB_CHAR_HASSEPTIC            17
#define  MB_CHAR_HASSEWER             18
#define  MB_CHAR_HASWELL              19
#define  MB_CHAR_BLDGSEQNO            20
#define  MB_CHAR_LOTACRES             21
#define  MB_CHAR_LOTSQFT              22
#define  MB_CHAR_DOCNO                23
#define  MB_CHAR_ASMT_STATUS          24
#define  MB_CHAR_UNITSEQNO            25

#define  MBSIZ_CHAR_ASMT              12
#define  MBSIZ_CHAR_POOLS             2
#define  MBSIZ_CHAR_USECAT            2
#define  MBSIZ_CHAR_QUALITY           6
#define  MBSIZ_CHAR_YRBLT             4
#define  MBSIZ_CHAR_BLDGSQFT          9
#define  MBSIZ_CHAR_GARSQFT           9
#define  MBSIZ_CHAR_HEATING           2
#define  MBSIZ_CHAR_COOLING           2
#define  MBSIZ_CHAR_HEATSRC           2
#define  MBSIZ_CHAR_COOLSRC           2
#define  MBSIZ_CHAR_BEDS              3
#define  MBSIZ_CHAR_FBATHS            3
#define  MBSIZ_CHAR_HBATHS            3
#define  MBSIZ_CHAR_FP                2
#define  MBSIZ_CHAR_HASSEPTIC         1
#define  MBSIZ_CHAR_HASSEWER          1
#define  MBSIZ_CHAR_HASWELL           1
// Sonoma
#define  MBSIZ_CHAR_DOCNO             12
#define  MBSIZ_CHAR_BLDGSEQNO         2
#define  MBSIZ_CHAR_UNITSEQNO         2
// Yolo
#define  MBSIZ_CHAR_NUMFLOORS         3
#define  MBSIZ_CHAR_LANDSQFT          9
#define  MBSIZ_CHAR_TOTALROOMS        4
#define  MBSIZ_CHAR_PARKTYPE          4
#define  MBSIZ_CHAR_PARKSPACES        4
/*
#define  MBOFF_CHAR_ASMT              1
#define  MBOFF_CHAR_POOLS             13
#define  MBOFF_CHAR_USECAT            15
#define  MBOFF_CHAR_QUALITY           17
#define  MBOFF_CHAR_YRBLT             23
#define  MBOFF_CHAR_BLDGSQFT          27
#define  MBOFF_CHAR_GARSQFT           36
#define  MBOFF_CHAR_HEATING           45
#define  MBOFF_CHAR_COOLING           47
#define  MBOFF_CHAR_HEATSRC           49
#define  MBOFF_CHAR_COOLSRC           51
#define  MBOFF_CHAR_BEDS              53
#define  MBOFF_CHAR_FBATHS            56
#define  MBOFF_CHAR_HBATHS            59
#define  MBOFF_CHAR_FP                62
#define  MBOFF_CHAR_HASSEPTIC         64
#define  MBOFF_CHAR_HASSEWER          65
#define  MBOFF_CHAR_HASWELL           66
// Sonoma
#define  MBOFF_CHAR_DOCNO             67
#define  MBOFF_CHAR_BLDGSEQNO         79
#define  MBOFF_CHAR_UNITSEQNO         81
*/
// _SITUS
#define  MB_SITUS_ASMT                0
#define  MB_SITUS_STRNAME             1
#define  MB_SITUS_STRNUM              2
#define  MB_SITUS_STRTYPE             3
#define  MB_SITUS_STRDIR              4
#define  MB_SITUS_UNIT                5
#define  MB_SITUS_COMMUNITY           6
#define  MB_SITUS_ZIP                 7
#define  MB_SITUS_SEQ                 8

// _SALES
#define  MB_SALES_ASMT                0
#define  MB_SALES_DOCNUM              1
#define  MB_SALES_DOCDATE             2
#define  MB_SALES_DOCCODE             3
#define  MB_SALES_SELLER              4
#define  MB_SALES_BUYER               5
#define  MB_SALES_TAXAMT              6
#define  MB_SALES_GROUPSALE           7
#define  MB_SALES_GROUPASMT           8
#define  MB_SALES_XFERTYPE            9
#define  MB_SALES_ADJREASON           10
#define  MB_SALES_CONFCODE            11
#define  MB_SALES_DTS                 12

// _TAX
#define  MB_TAX_ASMT                  0
#define  MB_TAX_FEEPARCEL             1
#define  MB_TAX_TAXAMT1               2
#define  MB_TAX_TAXAMT2               3
#define  MB_TAX_PENAMT1               4
#define  MB_TAX_PENAMT2               5
#define  MB_TAX_PENDATE1              6
#define  MB_TAX_PENDATE2              7
#define  MB_TAX_PAIDAMT1              8
#define  MB_TAX_PAIDAMT2              9
#define  MB_TAX_TOTALPAID1            10
#define  MB_TAX_TOTALPAID2            11
#define  MB_TAX_TOTALFEES             12
#define  MB_TAX_PAIDDATE1             13
#define  MB_TAX_PAIDDATE2             14
#define  MB_TAX_YEAR                  15
#define  MB_TAX_ROLLCAT               16

typedef struct _tApnRange
{
   char  acApn[12];
   int   iLen;
} APN_RNG;

int   MergeGrGrFile1(char *pCnty);
int   MB_ConvChar(char *pInfile);
bool  sqlConnect(LPCSTR strDb, hlAdo *phDb);
int   execSqlCmd(LPCSTR strCmd);
int   execSqlCmd(LPCSTR strCmd, hlAdo *phDb);
int   MB_CreateSale();
int   MB_ExtrTR601(LPCSTR pCnty, LPCSTR pLDRFile=NULL);
int   MB_MergeSale(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag, bool bUpdtXfer=true);
int   MB_ExtrProp8Ldr(char *pCnty, char *pLDRFile=NULL);

#endif