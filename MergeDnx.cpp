/**************************************************************************
 *
 * Options:
 *    -CDNX -U [-O] [-Xn] [-Mn] [-Ps] [-Ut|-Lt] (Normal update)
 *    -CDNX -L [-Xl] [-Mn] [-O]                  (LDR)
 *
 * Notes:
 *		- Currently county sends us CDDATA.CSV only, no sale, tax or char data
 *    - County starts sending MB format files (sale, tax, char, roll, situs, values... similar to GLE) on 20220912 
 *		- InCareOf & DBA are repeated in Address1 & Address2
 *
 * Revision
 * 03/01/2017 16.9.4    First version
 * 03/02/2017 16.9.4.1  Modify Dnx_MergeRoll() to format DocNum matching with 
 *                      old format from CRES and populate PREV_APN for new parcels 
 *                      book 900 and above. Modify Dnx_MergeOwner() to remove all "$ ETAL"
 *                      from CareOf and reformat Owner name.
 * 03/07/2017 16.10.1   Fix StrName in Dnx_MergeSAdr()
 * 03/08/2017 16.10.2   Set date format before calling Load_TC()
 * 12/06/2017 17.5.1    Add Dnx_ExtrLien & Dnx_Load_LDR() to process new 2017 format
 * 02/07/2018 17.5.3    Add Dnx_MergeFixt() to merge Fixt & PP to roll record when loading LDR.
 *                      Modify Dnx_Load_Roll() to sort roll file before processing to remove duplicate.
 *                      Add -Xs option to load NDC sale extract.
 * 02/09/2018 17.5.4    Add Dnx_MergeLien() to ignore other values from cddata.csv in LDR processing.
 *                      Use only MH value from cddata.csv.
 * 02/27/2018 17.6.0    Fix S_HSENO in Dnx_MergeSAdr() by storing the numerical portion only in S_HSENO
 *                      to avoid duplicate in bulk file.  Modify Dnx_MergeLien() to ignore repeated APN.
 * 03/23/2018 17.7.1    Modify Dnx_MergeRoll() to remove old sales.
 * 03/26/2018 17.8.0    Add -Ut option to update tax table.
 * 08/28/2018 18.4.1    Add Dnx_MergeOwner_SR(), Dnx_MergeLien_SR(), Dnx_CreateLienRec_SR()
 *                      Modify Dnx_MergeMAdr() to remove '%' from CareOf and '/' from Dba.
 *                      Modify Dnx_Load_LDR() & Dnx_ExtrLien() to support both LDR file "cddata.csv" and "Secured Roll CD.txt"
 * 11/04/2018 18.7.3    Activate -Mn option to replace -Ms option for DNX.
 * 10/16/2019 19.4.1    Modify Dnx_Load_LDR() to support new AGENCYCDCURRSEC_TR601.TAB lien roll format.  
 *                      Add Dnx_MergeLien3(), Dnx_MergeOwner3(), Dnx_CreateLienRec3().
 *                      Modify Dnx_MergeMAdr() to use parseMAdr1_1() to parse mailing addr insyead of parseAdr1()
 * 04/03/2020 19.8.1    Remove -Xs and replace it with -Xn option to format NDC recorder data.
 * 04/27/2020 19.9.0    Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 04/29/2020 19.9.2    Modify Dnx_MergeRoll() to select proper Dnx_MergeMAdr() based on which mailadr available.
 * 05/06/2020 19.9.3    Add -Mz option to merge Zoning.
 * 07/23/2020 20.2.3    Modify Dnx_MergeRoll() to reset county zoning.
 * 08/10/2020 20.2.6    Adding Dnx_MergeLien14() & Dnx_CreateLienRec14() to support 2020 LDR.
 * 08/30/2021 21.2.1    Fix TRANSFER_DOC in Dnx_MergeLien3() for LDR 2021.
 * 03/05/2022 21.5.1    Allow both -Ms and -Mn.
 * 09/12/2022 22.2.3    Add Dnx_ConvStdChar() & Dnx_CreateSCSale() to load new CHAR & SALE files.
 * 09/14/2022 22.2.4    Create Dnx_Load_LDR3() and other functions to support new file format.
 * 07/03/2023 23.0.3    Modify Dnx_CreateSCSale() to fix sorting issue. Modify Dnx_Load_LDR3() to skip last record.
 * 07/02/2024 24.0.0    Modify Dnx_MergeLien3() to add ExeType.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "doSort.h"
#include "formatApn.h"
#include "UseCode.h"
#include "SaleRec.h"
#include "LoadMB.h"
#include "MBExtrn.h"
#include "PQ.h"
#include "MergeDnx.h"
#include "Situs.h"
#include "Tax.h"
#include "CharRec.h"
#include "NdcExtr.h"

static long lUseGis;

/***************************** Dnx_MergeStdChar ******************************
 *
 *   - If parcel has more than one CHAR record, use the one that has value.
 *
 *****************************************************************************/

int Dnx_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft;
   int      iLoop, iBeds, iFBath, iHBath, iBldgNum, iTmp;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001051007", 9))
   //   iTmp = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   //while (!iLoop)
   {
      // Quality Class
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
      *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;

      // Yrblt
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

      // YrEff
      iTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
      if (iTmp > 1600 && iTmp <= lToyear)
         memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

      // BldgSqft
      lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
      if (lBldgSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      } else
         memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

      // Garage Sqft
      lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
      if (lGarSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      } else
         memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);

      // Parking type
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

      // ParkSpace
      iTmp = atoin(pChar->ParkSpace, SIZ_CHAR_SIZE4);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d      ", iTmp);
         memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      } else
         memset(pOutbuf+OFF_PARK_SPACE, ' ', SIZ_PARK_SPACE);

      // Heating
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];

      // Cooling
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

      // Beds
      iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      } else
         memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

      // Bath
      iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      } else
         memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

      // Half bath
      iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
      if (iHBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      } else
         memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

      // Rooms
      iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      } else
         memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

      // Units
      iTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
      if (pChar->Units[0] > ' ')
      {
         sprintf(acTmp, "%*d", SIZ_UNITS, iTmp);
         memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
      } else
         memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

      // Stories
      if (pChar->Stories[0] > ' ')
         memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
      else
         memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

      // Fireplace
      if (pChar->Fireplace[0] > ' ')
         memcpy(pOutbuf+OFF_FIRE_PL, pChar->Fireplace, SIZ_FIRE_PL);
      else
         memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);

      // HasSeptic or HasSewer
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

      // HasWell
      *(pOutbuf+OFF_WATER) = pChar->HasWater;

      // Pools
      *(pOutbuf+OFF_POOL) = pChar->Pool[0];

      // BldgNum
      iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);

      lCharMatch++;

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      //if (!pRec)
      //   break;
      //iLoop = memcmp(pOutbuf, pRec, iApnLen);
      //if (!iLoop && (iBeds > 0 && iBldgNum > 0))
      //   break;
   }

   return 0;
}

/******************************** Dnx_MergeOwner *****************************
 *
 * The CareOf field may contain Owner2.  Check for $ at start of CareOf.  If it 
 * contains "$ETAL", append ETAL to owner name.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Dnx_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf=NULL, char *pDba=NULL)
{
   int   iTmp, iRet, iNameCnt;
   char  acOwner[128], acName1[128], acName2[128], acNames[128], *pTmp, *pTmp1;
   OWNER myOwner;
   bool  bHW=false;

   // Clear output buffer if needed
   removeNames(pOutbuf, true, true);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001040007000", 10))
   //   iTmp = 0;
#endif

   // Init names
   iNameCnt=0;
   strcpy(acName1, pNames);
   // Remove dot, comma, backslash, double and single quote
   remCharEx(acName1, ",.'\"\\");
   acName2[0] = 0;

   // Check DBA
   if (pDba && *pDba > ' ')
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

   // Check CareOf 
   if (pCareOf && strlen(pCareOf) > 7)
   {
      if (*pCareOf == '$' || *pCareOf == '%')
      {
         // Ifnore trailing $ETAL
         if (pTmp = strchr(pCareOf+5, '$'))
            *pTmp = 0;
         pTmp = pCareOf+2;
      } else
      {
         pTmp = pCareOf;
      } 

      updateCareOf(pOutbuf, pTmp, strlen(pTmp));
   }

   // Replace '/' with '&' in Name1 and save it in Owner
   if (pTmp = strchr(acName1, '/'))
   {
      *pTmp++ = 0;
      sprintf(acOwner, "%s & %s", acName1, pTmp);
      strcpy(acName1, acOwner);
   }  else
      strcpy(acOwner, acName1);

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = -1;
   if (strchr(acName1, ' '))
   {
      // Remove notes
      if (pTmp = strchr(acName1, '('))
      {
         pTmp1 = pTmp;
         while (*pTmp1 && *pTmp1 != ')')
            *pTmp1++ = ' ';
         if (*pTmp1 == ')')
            *pTmp1 = ' ';
      }

      // Remove extra blank
      blankRem(acName1);

      // Make a copy of owner and remove unwanted trailing words before swap
      strcpy(acNames, acName1);
      strcat(acNames, " ");
      if (pTmp = strstr(acNames, " JT "))
         *pTmp = 0;
      if (pTmp = strstr(acNames, " REV TR"))
         *pTmp = 0;
      if (pTmp = strstr(acNames, " LIV TR"))
         *pTmp = 0;
      if ((pTmp = strstr(acNames, " TR ")) || (pTmp = strstr(acNames, " TRST ")))
         *pTmp = 0;
      if ((pTmp = strstr(acNames, " TRUS")) || (pTmp = strstr(acNames, " TRSTE")))
         *pTmp = 0;

      iRet = splitOwner(acNames, &myOwner, 3);

      // If name is swapable, use it
      if (iRet >= 0)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   }

   if (iRet < 0)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);

   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);
}

void Dnx_MergeOwner2(char *pOutbuf, char *pNames, char *pCareOf=NULL, char *pDba=NULL)
{
   int   iTmp, iRet, iNameCnt;
   char  acOwner[128], acName1[128], acName2[128], acNames[128], *pTmp, *pTmp1;
   OWNER myOwner;
   bool  bHW=false;

   // Clear output buffer if needed
   //removeNames(pOutbuf, true, true);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001040007000", 10))
   //   iTmp = 0;
#endif

   // Init names
   iNameCnt=0;
   strcpy(acName1, pNames);
   // Remove dot and single quote
   remCharEx(acName1, ",.'");
   myTrim(acName1);
   acName2[0] = 0;

   // Check DBA
   if (pDba && *pDba > ' ')
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

   // Check CareOf 
   if (pCareOf && strlen(pCareOf) > 7)
   {
      if (*pCareOf == '$' || *pCareOf == '%')
      {
         // Ifnore trailing $ETAL
         if (pTmp = strchr(pCareOf+5, '$'))
            *pTmp = 0;
         pTmp = pCareOf+2;
      } else
      {
         pTmp = pCareOf;
      } 

      updateCareOf(pOutbuf, pTmp, strlen(pTmp));
   }

   // Replace '/' with '&' in Name1 and save it in Owner
   if (pTmp = strchr(acName1, '/'))
   {
      *pTmp++ = 0;
      sprintf(acOwner, "%s & %s", acName1, pTmp);
      strcpy(acName1, acOwner);
   }  else
      strcpy(acOwner, acName1);

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = -1;
   if (strchr(acName1, ' '))
   {
      // Remove notes
      if (pTmp = strchr(acName1, '('))
      {
         pTmp1 = pTmp;
         while (*pTmp1 && *pTmp1 != ')')
            *pTmp1++ = ' ';
         if (*pTmp1 == ')')
            *pTmp1 = ' ';
      }

      // Remove extra blank
      blankRem(acName1);

      // Make a copy of owner and remove unwanted trailing words before swap
      strcpy(acNames, acName1);
      strcat(acNames, " ");
      if (pTmp = strstr(acNames, " JT "))
         *pTmp = 0;
      if (pTmp = strstr(acNames, " REV TR"))
         *pTmp = 0;
      if (pTmp = strstr(acNames, " LIV TR"))
         *pTmp = 0;
      if ((pTmp = strstr(acNames, " TR ")) || (pTmp = strstr(acNames, " TRST ")))
         *pTmp = 0;
      if ((pTmp = strstr(acNames, " TRUS")) || (pTmp = strstr(acNames, " TRSTE")))
         *pTmp = 0;

      iRet = splitOwner(acNames, &myOwner, 3);

      // If name is swapable, use it
      if (iRet >= 0)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   }

   if (iRet < 0)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);

   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);
}

void Dnx_MergeOwner3(char *pOutbuf, char *pNames)
{
   int   iTmp, iRet, iNameCnt;
   char  acOwner[128], acName1[128], acName2[128], acNames[128], *pTmp, *pTmp1;
   OWNER myOwner;
   bool  bHW=false;

   // Clear output buffer if needed
   //removeNames(pOutbuf, false, false);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001040007000", 10))
   //   iTmp = 0;
#endif
   // Init names
   iNameCnt=0;
   strcpy(acName1, pNames);
   // Remove dot, comma, backslash, double and single quote
   remCharEx(acName1, ",.'\"\\");
   acName2[0] = 0;

   // Replace '/' with '&' in Name1 and save it in Owner
   if (pTmp = strchr(acName1, '/'))
   {
      *pTmp++ = 0;
      sprintf(acOwner, "%s & %s", acName1, pTmp);
      strcpy(acName1, acOwner);
   }  else
      strcpy(acOwner, acName1);

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = -1;
   if (strchr(acName1, ' '))
   {
      // Remove notes
      if (pTmp = strchr(acName1, '('))
      {
         pTmp1 = pTmp;
         while (*pTmp1 && *pTmp1 != ')')
            *pTmp1++ = ' ';
         if (*pTmp1 == ')')
            *pTmp1 = ' ';
      }

      // Remove extra blank
      blankRem(acName1);

      // Make a copy of owner and remove unwanted trailing words before swap
      strcpy(acNames, acName1);
      strcat(acNames, " ");
      if (pTmp = strstr(acNames, " JT "))
         *pTmp = 0;
      if (pTmp = strstr(acNames, " REV TR"))
         *pTmp = 0;
      if (pTmp = strstr(acNames, " LIV TR"))
         *pTmp = 0;
      if ((pTmp = strstr(acNames, " TR ")) || (pTmp = strstr(acNames, " TRST ")))
         *pTmp = 0;
      if ((pTmp = strstr(acNames, " TRUS")) || (pTmp = strstr(acNames, " TRSTE")))
         *pTmp = 0;

      iRet = splitOwner(acNames, &myOwner, 3);

      // If name is swapable, use it
      if (iRet >= 0)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   }

   if (iRet < 0)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);

   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);
}

/******************************** Dnx_MergeMAdr ******************************
 *
 * Merge Mail address using M_ADDR, M_CITY, M_ST, & M_ZIP grom cddata.csv
 *
 *****************************************************************************/

void Dnx_MergeMAdr(char *pOutbuf)
{
   char    acTmp[256], acAddr1[64], *pTmp;
   int     iTmp;
   ADR_REC sMailAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "131250011000", 10))
   //   iTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, false, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Mail address
   strcpy(acAddr1, apTokens[DNX_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   //parseAdr1(&sMailAdr, acAddr1);
   parseMAdr1_1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }

   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
      sMailAdr.strName[SIZ_M_STREET] = 0;
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[DNX_M_CITY] > ' ')
   {
      // Remove comma
      iTmp = remChar(apTokens[DNX_M_CITY], ',');
      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[DNX_M_CITY], SIZ_M_CITY, iTmp);
      if (2 == strlen(apTokens[DNX_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[DNX_M_ST], 2);

      if (*apTokens[DNX_M_ZIP] >= '0')
      {
         // Take out '-' in zipcode if present
         strcpy(acTmp, apTokens[DNX_M_ZIP]);
         iTmp = strlen(acTmp);
         if (acTmp[5] == '-' && iTmp == 10)
         {
            strcpy(&acTmp[5], &acTmp[6]);
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         } else if (iTmp > 9)
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         else
            vmemcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
      } else
         iTmp = 0;

      if (iTmp == 9)
         sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[DNX_M_CITY], apTokens[DNX_M_ST], apTokens[DNX_M_ZIP], apTokens[DNX_M_ZIP]+5);
      else
         sprintf(acTmp, "%s %s %s", apTokens[DNX_M_CITY], apTokens[DNX_M_ST], apTokens[DNX_M_ZIP]);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

/******************************** Dnx_MergeMAdr ******************************
 *
 * Merge Mail address using ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4
 *
 *****************************************************************************/

void Dnx_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2, *pDba;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "101010024000", 9)  )
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   pDba = p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }

      if (!_memicmp(pLine1, "DBA ", 4) )
         pDba = pLine1;
      else if (!_memicmp(pLine2, "DBA ", 4))
         pDba = pLine2;
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }

      if (!_memicmp(pLine1, "DBA ", 4) )
         pDba = pLine1;
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      if (pTmp = strchr(p0, '$'))
         *pTmp = 0;
      if (*p0 == '%')
         if (*(p0+1) == ' ')
            p0 += 2;
         else
            p0++;
      updateCareOf(pOutbuf, p0, strlen(p0));
   }

   // Update DBA
   if (pDba)
   {
      if (pTmp = strchr(pDba, '/'))
         *pTmp = 0;

      memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Special case
      int iRoadChg = replStr(acAddr1, "CO RD ", "COUNTY ROAD ");
      if (iRoadChg > 0)
         LogMsg("--> Rename CO RD to %s [%.12s]", acAddr1, pOutbuf);

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));

         if (!iRoadChg)
         {
            if (sMailAdr.strSfx[0] > '0')
               memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
            if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
               memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
            if (pTmp = strstr(sMailAdr.strName, " PMB"))
               *pTmp = 0;
            vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
         } else
         {
            pTmp = strstr(acAddr1, "COUNTY");
            vmemcpy(pOutbuf+OFF_M_STREET, pTmp, SIZ_M_STREET);
         }
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   strcpy(acAddr2, p2);
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);

      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}

/******************************* Dnx_MergeMAdr1 ******************************
 *
 * Merge Mail address from Dnx_Roll.csv
 *
 *****************************************************************************/

void Dnx_MergeMAdr1(char *pOutbuf)
{
   char    acTmp[256], acAddr1[64], *pTmp;
   int     iTmp;
   ADR_REC sMailAdr;

#ifdef _DEBUG
   // SANDHILL ROAD
   //if (!memcmp(pOutbuf, "110140031000", 10))
   //   iTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, false, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   // Special case
   iTmp = replStr(acAddr1, "CO RD ", "COUNTY ROAD ");
   if (iTmp > 0 && bDebug)
      LogMsg("--> Rename CO RD to %s [%.12s]", acAddr1, pOutbuf);

   remChar(acAddr1, ',');
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1_3(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }

   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
      sMailAdr.strName[SIZ_M_STREET] = 0;
      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      iTmp = blankRem(apTokens[MB_ROLL_M_CITY]);
      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], SIZ_M_CITY, iTmp);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         // Take out '-' in zipcode if present
         strcpy(acTmp, apTokens[MB_ROLL_M_ZIP]);
         iTmp = strlen(acTmp);
         if (acTmp[5] == '-' && iTmp == 10)
         {
            strcpy(&acTmp[5], &acTmp[6]);
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         } else if (iTmp > 9)
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         else
            vmemcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
      } else
         iTmp = 0;

      if (iTmp == 9)
         sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP], apTokens[MB_ROLL_M_ZIP]+5);
      else
         sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

/******************************** Dnx_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Dnx_MergeSAdr(char *pOutbuf)
{
   char     acTmp[256], acCode[32], acAddr1[256], acAddr2[64], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   if (*apTokens[DNX_S_STRNAME] == ' ' && *apTokens[DNX_S_STRNUM] == ' ')
      return 0;

   // Merge data
   acAddr1[0] = 0;
   removeSitus(pOutbuf);
   lTmp = atol(apTokens[DNX_S_STRNUM]);

   if (lTmp > 0)
   {
      remChar(apTokens[DNX_S_STRNUM], ' ');

      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      // Save original StrNum - avoid duplicate S_FRA in bulk file
      //memcpy(pOutbuf+OFF_S_HSENO, apTokens[DNX_S_STRNUM], strlen(apTokens[DNX_S_STRNUM]));
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);

      //Keep original street number in addr1
      iTmp = sprintf(acAddr1, "%s ", apTokens[DNX_S_STRNUM]);

      if (pTmp = strchr(apTokens[DNX_S_STRNUM], '/'))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp, strlen(pTmp));

      if (*apTokens[DNX_S_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[DNX_S_STRDIR]);
         strcat(acAddr1, " ");
         vmemcpy(pOutbuf+OFF_S_DIR, apTokens[DNX_S_STRDIR], SIZ_S_DIR);
      } else
         memset(pOutbuf+OFF_S_DIR, ' ', SIZ_S_DIR);
   }

   if (*apTokens[DNX_S_STRNAME] > ' ')
   {
      strcat(acAddr1, apTokens[DNX_S_STRNAME]);
      vmemcpy(pOutbuf+OFF_S_STREET, apTokens[DNX_S_STRNAME], SIZ_S_STREET, strlen(apTokens[DNX_S_STRNAME]));

      if (*apTokens[DNX_S_STRTYPE] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, apTokens[DNX_S_STRTYPE]);

         iTmp = GetSfxCodeX(apTokens[DNX_S_STRTYPE], acTmp);
         if (iTmp > 0)
         {
            Sfx2Code(acTmp, acCode);
            vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
         } else
         {
            LogMsg0("*** Invalid suffix: %s", apTokens[DNX_S_STRTYPE]);
            iBadSuffix++;
         }
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004090016520", 12))
   //   iTmp = 0;
#endif
   if (*apTokens[DNX_S_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[DNX_S_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[DNX_S_UNIT], SIZ_S_UNITNO);
   }

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);
   memset(acAddr2, ' ', SIZ_S_CTY_ST_D);

   // Situs city
   if (*apTokens[DNX_S_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[DNX_S_COMMUNITY], acTmp, acAddr2, pOutbuf);
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr2[0] > ' ')
      {
         // Zip
         if (*apTokens[DNX_S_ZIP] == '9')
         {
            lTmp = atol(apTokens[DNX_S_ZIP]);
            if (lTmp > 90000 && lTmp < 99999)
               memcpy(pOutbuf+OFF_S_ZIP, apTokens[DNX_S_ZIP], 5);

            iTmp = sprintf(acTmp, "%s, CA %.5s", acAddr2, apTokens[DNX_S_ZIP]);
         } else if (!memcmp(acAddr2, pOutbuf+OFF_M_CITY, strlen(acAddr2)) && *(pOutbuf+OFF_M_ZIP) == '9')
         {
            memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, 5);
            iTmp = sprintf(acTmp, "%s, CA %.5s", acAddr2, pOutbuf+OFF_M_ZIP);
         } else
         {
            iTmp = sprintf(acTmp, "%s, CA", acAddr2);
         }
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
      }
   }

   return 0;
}

int Dnx_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "007280027000", 9))
   //   acTmp[0] = 0;
#endif
   //
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   if (sSitusAdr.lStrNum > 0)
   {
      char *pTmp = strchr(acAddr1, ' ');
      *pTmp = 0;

      // Save original StrNum
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
      memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   }

   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/******************************* Dnx_MergeSitus1 *****************************
 *
 * Merge Situs address from Dnx_Situs.csv
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Dnx_MergeSitus1(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acAddr2[64], *pTmp, *pTmp1,
            acStrName[32], acStrSfx[32];
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do {
         pRec = fgets(acRec, 512, fdSitus);
      } while (pRec && (*pRec < ' ' || *pRec > '9'));
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      pTmp = pRec;
      if (*pTmp == '"')
         pTmp++;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   replNull(pRec);
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   else
      iRet = ParseStringIQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   if (*apTokens[MB_SITUS_STRNAME] == ' ' && *apTokens[MB_SITUS_STRNUM] == ' ')
      return 0;

   // Merge data
   acAddr1[0] = 0;
   strcpy(acStrName, apTokens[MB_SITUS_STRNAME]);
   strcpy(acStrSfx, apTokens[MB_SITUS_STRTYPE]);
   removeSitus(pOutbuf);
   lTmp = atol(myTrim(apTokens[MB_SITUS_STRNUM]));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "040282002000", 12))    // 6TH ST
   //   iTmp = 0;
#endif

   if (lTmp > 0)
   {
      // Save original StrNum
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
      {
         if (pTmp1 = strstr(pTmp, "6TH"))
         {
            strcpy(acStrName, pTmp1);
            strcpy(acStrSfx, apTokens[MB_SITUS_STRNAME]);
         } else if (*(pTmp+1) > ' ')
            vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, SIZ_S_STR_SUB);
      }

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         vmemcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], SIZ_S_DIR);
      }
   }

   if (acStrSfx[0] > ' ')
   {
      strcat(acAddr1, acStrName);
      strcat(acAddr1, " ");
      strcat(acAddr1, acStrSfx);
      vmemcpy(pOutbuf+OFF_S_STREET, acStrName, SIZ_S_STREET);

      iTmp = GetSfxCodeX(acStrSfx, acTmp);
      if (iTmp > 0)
      {
         Sfx2Code(acTmp, acCode);
         vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      } else
      {
         LogMsg0("*** Invalid suffix: %s", acStrSfx);
         iBadSuffix++;
      }
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, acStrName);
      vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      vmemcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, SIZ_S_SUFF);

      strcat(acAddr1, acStrName);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNO);
   }

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);
   memset(acAddr2, ' ', SIZ_S_CTY_ST_D);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr2, pOutbuf);   
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr2[0] > ' ')
      {
         // Zip
         lTmp = atol(apTokens[MB_SITUS_ZIP]);
         if (lTmp > 10000 && lTmp < 99999)
            memcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], 5);

         iTmp = sprintf(acTmp, "%s, CA %.5s", acAddr2, apTokens[MB_SITUS_ZIP]);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
      }
   }

   // Get city/zip from GIS extract
   //if (fdCity && *(pOutbuf+OFF_S_CITY) == ' ')
   //{
   //   char acZip[16], acCity[32];

   //   iTmp = getCityZip(pOutbuf, acCity, acZip, 9);
   //   if (!iTmp)
   //   {
   //      City2Code(acCity, acCode, pOutbuf);
   //      memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
   //      memcpy(pOutbuf+OFF_S_ZIP, acZip, strlen(acZip));
   //      sprintf(acAddr2, "%s CA %s", acCity, acZip);
   //      lUseGis++;
   //   }
   //}

   iTmp = blankRem(acAddr2, SIZ_S_CTY_ST_D);
   memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/********************************* Dnx_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Dnx_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Remove NULL
   replStrAll(pRollRec, "NULL", "");

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < DNX_FLDCOUNT)
   {
      LogMsg("***** Dnx_MergeRoll(): bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN on certain book
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || iTmp == 555 || (iTmp > 799 && iTmp < 900))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[DNX_FEEPARCEL], strlen(apTokens[DNX_FEEPARCEL]));

      // PREV_APN - Only apply to book 900 and above
      //if (memcmp(pOutbuf, "900", 3) >= 0)
      {
         iTmp = sprintf(acTmp, "%.6s%.2s", apTokens[DNX_FEEPARCEL], apTokens[DNX_FEEPARCEL]+7);
         memcpy(pOutbuf+OFF_PREV_APN, acTmp, iTmp);
      }

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "08DNX", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[DNX_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[DNX_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[DNX_FIXTRS]);
      long lPP    = atoi(apTokens[DNX_PERSPROP]);     // FixtureRealProperty
      long lMH    = atoi(apTokens[DNX_PPMOBILHOME]);
      long lHSite = atoi(apTokens[DNX_HOMESITE]);
      long lGrow  = atoi(apTokens[DNX_GROWING]);
      long lBusInv= atoi(apTokens[DNX_BUSPROP]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lBusInv;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%u         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%u         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%u         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%u         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%u         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lBusInv > 0)
         {
            sprintf(acTmp, "%u         ", lBusInv);
            memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // Exemption
      long lExe  = atoi(apTokens[DNX_EXEAMT]);
      if (lExe > 0)
      {
         if (lExe == 999999999)
            lExe = lTmp;
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lExe);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

      }
      if (*apTokens[DNX_EXECODE] == 'E')
      {
         if (!memcmp(apTokens[DNX_EXECODE], "E01", 3))
            *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
         else
            *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         vmemcpy(pOutbuf+OFF_EXE_CD1, apTokens[DNX_EXECODE], 3);
      }
   } else
      // Reset Zoning
      memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[DNX_TRA], strlen(apTokens[DNX_TRA]));

   // Legal
   updateLegal(pOutbuf, apTokens[DNX_LEGAL]);

   // UseCode
   strcpy(acTmp, apTokens[DNX_USECODE]);
   acTmp[SIZ_USE_CO] = 0;
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[DNX_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "131250011000", 10))
   //   iTmp = 0;
#endif

   // Clear old sales
   //if (bClearSales)
   //   ClearOldSale(pOutbuf, true);

   // Recorded Doc
   if (*apTokens[DNX_DOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[DNX_DOCDATE], acTmp, YYYY_MM_DD);

      if (*(apTokens[DNX_DOCNUM]+4) == 'R')
         lTmp = atol(apTokens[DNX_DOCNUM]+5);
      else
      {
         lTmp = 0;
      }

      // Only populate if DocNum and DocDate match the year.
      if (pTmp && !memcmp(acTmp, apTokens[DNX_DOCNUM], 4) && lTmp > 0)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         lTmp = atol(acTmp);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;

         // Format DocNum using new format
         sprintf(acTmp, "%s       ", apTokens[DNX_DOCNUM]);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, SIZ_TRANSFER_DOC);
      }
   }

   // Owner
   try {
      Dnx_MergeOwner(pOutbuf, apTokens[DNX_OWNER], apTokens[DNX_CAREOF], apTokens[DNX_DBA]);
   } catch(...) {
      LogMsg("***** Exeception occured in Dnx_MergeOwner()");
   }

   // Situs addr
   try {
      Dnx_MergeSAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Dnx_MergeSAdr()");
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "101070014000", 9) )
   //   iTmp = 0;
#endif
   // Mailing
   if (*apTokens[DNX_M_ADDR] > ' ')
      Dnx_MergeMAdr(pOutbuf);
   else
      Dnx_MergeMAdr(pOutbuf, apTokens[DNX_ADDRESS1], apTokens[DNX_ADDRESS2], apTokens[DNX_ADDRESS3], apTokens[DNX_ADDRESS4]);

   return 0;
}

/********************************* Dnx_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Dnx_MergeLien1(char *pOutbuf, char *pRollRec, int iLen)
{
   static   char acApn[16];
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Remove NULL
   replStrAll(pRollRec, "NULL", "");

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < DNX_FLDCOUNT)
   {
      LogMsg("***** Dnx_MergeRoll(): bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN on certain book
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || iTmp == 555 || (iTmp > 799 && iTmp < 900))
      return 1;

   if (!memcmp(apTokens[iApnFld], acApn, iApnLen))
   {
      LogMsg("*** Duplicate APN: %s", apTokens[iApnFld]);
      return 1;
   } else
      strcpy(acApn, apTokens[iApnFld]);

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
   *(pOutbuf+OFF_STATUS) = 'A';

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[DNX_FEEPARCEL], strlen(apTokens[DNX_FEEPARCEL]));

   // PREV_APN - Only apply to book 900 and above
   //if (memcmp(pOutbuf, "900", 3) >= 0)
   {
      iTmp = sprintf(acTmp, "%.6s%.2s", apTokens[DNX_FEEPARCEL], apTokens[DNX_FEEPARCEL]+7);
      memcpy(pOutbuf+OFF_PREV_APN, acTmp, iTmp);
   }

   // Format APN
   iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "08DNX", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[DNX_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[DNX_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   long lMH    = atoi(apTokens[DNX_PPMOBILHOME]);
   lTmp = lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lMH > 0)
      {
         sprintf(acTmp, "%u         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe  = atoi(apTokens[DNX_EXEAMT]);
   if (lExe > 0)
   {
      if (lExe == 999999999)
         lExe = lTmp;
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

   }
   if (*apTokens[DNX_EXECODE] == 'E')
   {
      if (!memcmp(apTokens[DNX_EXECODE], "E01", 3))
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      vmemcpy(pOutbuf+OFF_EXE_CD1, apTokens[DNX_EXECODE], 3);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[DNX_TRA], strlen(apTokens[DNX_TRA]));

   // Legal
   updateLegal(pOutbuf, apTokens[DNX_LEGAL]);

   // UseCode
   strcpy(acTmp, apTokens[DNX_USECODE]);
   acTmp[SIZ_USE_CO] = 0;
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[DNX_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "131250011000", 10))
   //   iTmp = 0;
#endif

   // Recorded Doc
   if (*apTokens[DNX_DOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[DNX_DOCDATE], acTmp, YYYY_MM_DD);

      if (*(apTokens[DNX_DOCNUM]+4) == 'R')
         lTmp = atol(apTokens[DNX_DOCNUM]+5);
      else
         lTmp = 0;

      // Only populate if DocNum and DocDate match the year.
      if (pTmp && !memcmp(acTmp, apTokens[DNX_DOCNUM], 4) && lTmp > 0)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         // Format DocNum to make it match with old data from CRES
         sprintf(acTmp, "%d            ", lTmp);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, SIZ_TRANSFER_DOC);
      }
   }

   // Owner
   try {
      Dnx_MergeOwner(pOutbuf, apTokens[DNX_OWNER], apTokens[DNX_CAREOF], apTokens[DNX_DBA]);
   } catch(...) {
      LogMsg("***** Exeception occured in Dnx_MergeOwner()");
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "990018287", 9) )
   //   iTmp = 0;
#endif
   // Mailing
   try {
      Dnx_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Dnx_MergeMAdr()");
   }

   // Situs addr
   try {
      Dnx_MergeSAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Dnx_MergeSAdr()");
   }

   return 0;
}

int Dnx_MergeLien9(char *pOutbuf, char *pRollRec, int iLen)
{
   static   char acApn[16];
   char     acTmp[256], acTmp1[256];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Remove NULL
   replStrAll(pRollRec, "NULL", "");

   // Parse input
   iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L9_FLDCOUNT)
   {
      LogMsg("***** Dnx_MergeLien_SR(): bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN on certain book
   iTmp = atoin(apTokens[L9_ASMT], 3);
   if (!iTmp || iTmp == 555 || (iTmp > 799 && iTmp < 900))
      return 1;

   if (!memcmp(apTokens[L9_ASMT], acApn, iApnLen))
   {
      LogMsg("*** Duplicate APN: %s", apTokens[L9_ASMT]);
      return 1;
   } else
      strcpy(acApn, apTokens[L9_ASMT]);

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L9_ASMT], strlen(apTokens[L9_ASMT]));
   *(pOutbuf+OFF_STATUS) = 'A';

   // Copy ALT_APN
   //memcpy(pOutbuf+OFF_ALT_APN, apTokens[DNX_FEEPARCEL], strlen(apTokens[DNX_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L9_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L9_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "08DNX", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L9_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L9_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   long lFixt  = atoi(apTokens[L9_FIXTURESVALUE]);
   long lPP    = atoi(apTokens[L9_PPVALUE]);     
   long lMH    = atoi(apTokens[L9_MHPPVALUE]);
   lTmp = lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_FIXTR, lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_PERSPROP, lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_PP_MH, lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // Exemption
   long lExe    = atoi(apTokens[L9_HOX]);
   long lOthExe = atoi(apTokens[L9_OTHEREXEMPTION]);
   if (lExe > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'

      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lExe+lOthExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   } else if (lOthExe > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lOthExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[DNX_TRA], strlen(apTokens[DNX_TRA]));

   // TRA
   lTmp = atol(apTokens[L9_TRA]);
   iTmp = sprintf(acTmp, "%.6d", lTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, iTmp);

   // Legal
   //updateLegal(pOutbuf, apTokens[DNX_LEGAL]);

   // UseCode
   strcpy(acTmp, apTokens[L9_LANDUSE1]);
   acTmp[SIZ_USE_CO] = 0;
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L9_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "131250011000", 10))
   //   iTmp = 0;
#endif

   // Owner
   try {
      Dnx_MergeOwner2(pOutbuf, apTokens[L9_OWNER]);
   } catch(...) {
      LogMsg("***** Exeception occured in Dnx_MergeOwner()");
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "990018287", 9) )
   //   iTmp = 0;
#endif
   // Mailing
   try {
      Dnx_MergeMAdr(pOutbuf, apTokens[L9_MAILADDRESS1], apTokens[L9_MAILADDRESS2], apTokens[L9_MAILADDRESS3], apTokens[L9_MAILADDRESS4]);
   } catch(...) {
      LogMsg("***** Exeception occured in Dnx_MergeMAdr()");
   }

   // Situs addr
   try {
      Dnx_MergeSitus(pOutbuf, apTokens[L9_SITUS1], apTokens[L9_SITUS2]);
   } catch(...) {
      LogMsg("***** Exeception occured in Dnx_MergeSAdr()");
   }

   return 0;
}

int Dnx_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], acApn[16];
   ULONG    lTmp;
   double   dTmp;
   int      iRet, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L3_ISAGPRESERVE)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L3_ASMT], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   iRet = strlen(apTokens[L3_ASMT]);
   if (iRet < iApnLen)
      sprintf(acApn, "%.*s%s", iApnLen-iRet, "000", apTokens[L3_ASMT]);
   else
   {
      strcpy(acApn, apTokens[L3_ASMT]);
      remChar(acApn, '-');
   }

   // Start copying data
   memcpy(pOutbuf, acApn, iApnLen);

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Copy ALT_APN
   iRet = strlen(apTokens[L3_FEEPARCEL]);
   if (iRet < iApnLen)
      sprintf(acApn, "%.*s%s", iApnLen-iRet, "000", apTokens[L3_FEEPARCEL]);
   else
   {
      strcpy(acApn, apTokens[L3_FEEPARCEL]);
      remChar(acApn, '-');
   }
   memcpy(pOutbuf+OFF_ALT_APN, acApn, iApnLen);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "08DNX", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         iTmp = sprintf(acTmp, "%u", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, iTmp);
      }
      if (lFixtRP > 0)
      {
         iTmp = sprintf(acTmp, "%u", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, iTmp);
      }
      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%u", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, iTmp);
      }
      if (lGrow > 0)
      {
         iTmp = sprintf(acTmp, "%u", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, iTmp);
      }
      if (lPP_MH > 0)
      {
         iTmp = sprintf(acTmp, "%u", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&DNX_Exemption);

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres - Do not use Landsize
   dTmp = atof(apTokens[L3_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (ULONG)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Recorded Doc
   if (*apTokens[L3_CURRENTDOCNUM] > '0')
   {
      char *pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);

      if (*(apTokens[L3_CURRENTDOCNUM]+4) == 'R')
         lTmp = atol(apTokens[L3_CURRENTDOCNUM]+5);
      else
         lTmp = 0;

      // Only populate if DocNum and DocDate match the year.
      if (pTmp && !memcmp(acTmp, apTokens[L3_CURRENTDOCNUM], 4) && lTmp > 0)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         lTmp = atol(acTmp);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;

         // Format DocNum to make it match with old data from CRES
         iTmp = sprintf(acTmp, "%.5s%.4u", apTokens[L3_CURRENTDOCNUM], atol(apTokens[L3_CURRENTDOCNUM]+5));
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, iTmp);
      }
   }

   // AgPreserved
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "101010011000", 9))
   //   iTmp = 0;
#endif

   // Owner
   Dnx_MergeOwner3(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   Dnx_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Dnx_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   return 0;
}

int Dnx_MergeLien14(char *pOutbuf, char *pRollRec, int iLen)
{
   char     acTmp[256], acTmp1[64], acApn[16];
   ULONG    lTmp;
   double   dTmp;
   int      iRet, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L14_FLDCOUNT)
   {
      LogMsg("***** Error: bad input record for APN=%s (%u)", apTokens[L14_ASMT], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   iRet = strlen(apTokens[L14_ASMT]);
   if (iRet < iApnLen)
      sprintf(acApn, "%.*s%s", iApnLen-iRet, "000", apTokens[L14_ASMT]);
   else
      strcpy(acApn, apTokens[L14_ASMT]);

   // Start copying data
   memcpy(pOutbuf, acApn, iApnLen);

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Copy ALT_APN
   iRet = strlen(apTokens[L14_FEEPARCEL]);
   if (iRet < iApnLen)
      sprintf(acApn, "%.*s%s", iApnLen-iRet, "000", apTokens[L14_FEEPARCEL]);
   else
      strcpy(acApn, apTokens[L14_FEEPARCEL]);
   memcpy(pOutbuf+OFF_ALT_APN, acApn, iApnLen);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "08DNX", 5);

   // status
   *(pOutbuf+OFF_STATUS) = 'A';

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L14_TRA], strlen(apTokens[L14_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L14_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L14_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L14_FIXTURESVALUE]);
   long lPers  = atoi(apTokens[L14_PPVALUE]);
   long lPP_MH = atoi(apTokens[L14_MHPPVALUE]);
   lTmp = lFixtr+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%u         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L14_HOX]);
   long lExe2 = atol(apTokens[L14_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Legal
   //updateLegal(pOutbuf, apTokens[L14_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L14_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L14_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L14_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres - Do not use Landsize
   dTmp = atof(apTokens[L14_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Recorded Doc

   // AgPreserved

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "101010011000", 9))
   //   iTmp = 0;
#endif

   // Owner
   Dnx_MergeOwner3(pOutbuf, apTokens[L14_OWNER]);

   // Situs
   Dnx_MergeSitus(pOutbuf, apTokens[L14_SITUS1], apTokens[L14_SITUS2]);

   // Mailing
   Dnx_MergeMAdr(pOutbuf, apTokens[L14_MAILADDRESS1], apTokens[L14_MAILADDRESS2], apTokens[L14_MAILADDRESS3], apTokens[L14_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag

   return 0;
}

/********************************** Dnx_Load_Roll *****************************
 *
 * Loading cddata.csv
 *
 ******************************************************************************/

int Dnx_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acLastApn[32];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Prepare input file, sort and remove duplicate entries
   sprintf(acRec, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acRollFile, acRec, "S(#1,C,A) DEL(124) OMIT(1,1,C,EQ,\"-\",OR,1,1,C,GT,\"9\") DUPO(1,25)");
   if (iRet > 0)
      strcpy(acRollFile, acRec);

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (acRollRec[0] > '9' || acRollRec[0] < '0');

   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   acLastApn[iApnLen] = 0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "007280027000", 9))
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Dnx_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            iRollUpd++;
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         // Check with last apn to avoid duplicate
         if (pTmp && !memcmp(acLastApn, pTmp , iApnLen))
         {
            LogMsg("*** Duplicate APN found: %.50s", acRollRec);
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         }
         memcpy(acLastApn, acRollRec , iApnLen);

         if (!pTmp || acRollRec[0] > '9')
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Dnx_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

         if (!iRet)
         {
            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp || acRollRec[0] > '9')
         {
            bEof = true;    // Signal to stop
            break;
         } else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.12s < Roll->%.12s (%d) ***", acBuf, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Dnx_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   return 0;
}

/********************************* Dnx_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Dnx_MergeRoll1(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Dnx_MergeRoll1(): bad input record for APN=%s (%d)", apTokens[iApnFld], iRet);
      return -1;
   }

   // Ignore APN starts with 100-999 except 910 (MH), 920 (common area)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp > 799 && iTmp < 905))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // PREV_APN - Only apply to physical parcels
      if (memcmp(pOutbuf, "100", 3) < 0)
         vmemcpy(pOutbuf+OFF_PREV_APN, apTokens[iApnFld], 10);

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "08DNX", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   acTmp[SIZ_USE_CO] = 0;
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "015040009000", 9) )
   //   iTmp = 0;
#endif

   // Recorded Doc
   memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
   memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
   if (*apTokens[MB_ROLL_DOCNUM] > '0' && *(apTokens[MB_ROLL_DOCNUM]+4) != 'I')
   {
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp && !memcmp(acTmp, apTokens[MB_ROLL_DOCNUM], 4))
      {
         if (*(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
         {
            memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
            vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], SIZ_TRANSFER_DOC);
         } else
            LogMsg("*** Invalid DocNum: %s APN=%s", apTokens[MB_ROLL_DOCNUM], apTokens[MB_ROLL_ASMT]);
      }
   }

   // Owner
   try {
      Dnx_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER], apTokens[MB_ROLL_CAREOF], apTokens[MB_ROLL_DBA]);
   } catch(...) {
      LogMsg("***** Exeception occured in Gle_MergeOwner()");
   }

   // Mailing
   if (*apTokens[MB_ROLL_M_ADDR] > ' ')
      Dnx_MergeMAdr1(pOutbuf);
   else if (*apTokens[MB_ROLL_M_ADDR2] > ' ')
      Dnx_MergeMAdr(pOutbuf, apTokens[MB_ROLL_M_ADDR1], apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3], apTokens[MB_ROLL_M_ADDR4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************* Dnx_Load_Roll1 *****************************
 *
 * Loading Dnx_Roll.csv
 *
 ******************************************************************************/

int Dnx_Load_Roll1(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acSortCmd[256];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lDrop=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   sprintf(acSortCmd, "S(#1,C,A) DEL(%d) F(TXT)", cDelim);
   iRet = sortFile(acRollFile, acTmpFile, acSortCmd);
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"Z\") DEL(124) ");
   else
      lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) ");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) ");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) ");
   fdTax = fopen(acTmpFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   //do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   //} while (acRollRec[0] > '9' || acRollRec[0] < '0');

   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "920000256000", 9))
      //   iTmp = 0;
#endif
      replNull(acRollRec);
      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Dnx_MergeRoll1(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Dnx_MergeSitus1(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Dnx_MergeStdChar(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acBuf, 0);

            iRollUpd++;
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         } else
         {
            lDrop++;
            LogMsg("*** Bad record: %s", acRollRec);
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Dnx_MergeRoll1(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Dnx_MergeSitus1(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe2(acRec, 0);

            // Merge Char
            if (fdChar)
               lRet = Dnx_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acRec, iHdrRows);

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      replNull(acRollRec);

      // Create new R01 record
      iRet = Dnx_MergeRoll1(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Dnx_MergeSitus1(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe2(acRec, 0);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Dnx_MergeStdChar(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, iHdrRows);

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      } else
      {
         lDrop++;
         LogMsg("*** Bad record: %s", acRollRec);
      }

      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   //LogMsg("Use GIS city:               %u", lUseGis);
   //LogMsg("Use Mail city:              %u\n", lUseMailCity);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   return 0;
}

/****************************** Dnx_MergeFixt *******************************
 *
 * Merge fixture & PP values to roll record
 *
 ****************************************************************************/

int Dnx_MergeFixt(FILE *fd, char *pOutbuf)
{
   static   char  acRec[4096], sLastApn[16], *apItems[128], *pRec=NULL;
   int      iLoop, iRet;
   char     acTmp[256];

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 4096, fd);   // header
      pRec = fgets(acRec, 4096, fd);
   }

   do
   {
      if (!pRec)
      {
         fclose(fd);
         fd = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Fixtr rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 4096, fd);
      }
   } while (pRec && iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "8764014005", 10))
   //   iLoop = 0;
#endif

   // Parse input string
   iRet = ParseStringIQ(acRec, '|', 128, apItems);
   if (iRet < TC_UPD_DATE)
   {
      LogMsg("***** Error: bad fixtr record for APN=%.100s (#tokens=%d)", acRec, iRet);
      return -1;
   }

   long lFixt = atol(apItems[TC_FIX_IMPR]);
   if (lFixt > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIXTR, lFixt);
      memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
   }
   long lPP = atol(apItems[TC_PERS_PROP]);
   if (lPP > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIXTR, lPP);
      memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_FIXTR);
   }

   long lOthVal, lGross;
   if (lFixt > 0 || lPP > 0)
   {
      lOthVal = lFixt+lPP;
      sprintf(acTmp, "%*d", SIZ_FIXTR, lOthVal);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_FIXTR);

      lGross = atoin(pOutbuf+OFF_GROSS, SIZ_GROSS);
      sprintf(acTmp, "%*d", SIZ_GROSS, lGross+lOthVal);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      iRet = 0;
   } else
      iRet = 1;

   // Get next sale record
   pRec = fgets(acRec, 4096, fd);
   if (!pRec)
   {
      fclose(fd);
      fd = NULL;
   }

   return iRet;
}

/********************************* Dnx_Load_LDR *****************************
 *
 * Load CDDATA.csv - version=1
 * Load Secured Roll CD.txt - version=2
 * Load AGENCYCDCURRSEC_TR601.TAB - version=3
 *
 ****************************************************************************/

int Dnx_Load_LDR(int iFirstRec, int iVersion=1)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH], acTmp[_MAX_PATH];
   HANDLE   fhOut;
   int      iRet, iFixtCnt=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;
   FILE     *fdFixt=NULL;


   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Check for fixture & PP file
   iRet = GetIniString(myCounty.acCntyCode, "FixtFile", "", acTmp, _MAX_PATH, acIniFile);
   if (iRet > 0)
   {
      LogMsg("Open Fixture file %s", acTmp);
      fdFixt = fopen(acTmp, "r");
      if (fdFixt == NULL)
      {
         LogMsg("***** Error opening fixture file: %s\n", acTmp);
         return -2;
      }
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (acRollRec[0] > '9' || acRollRec[0] < '0');

   bEof = (pTmp ? false:true);

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   // Merge loop
   while (!bEof)
   {
      // Create new R01 record
      if (iVersion == 1)
         iRet = Dnx_MergeLien1(acBuf, acRollRec, iRecLen);
      else if (iVersion == 9)
         iRet = Dnx_MergeLien9(acBuf, acRollRec, iRecLen);
      else if (iVersion == 3)
         iRet = Dnx_MergeLien3(acBuf, acRollRec);
      else
         iRet = Dnx_MergeLien14(acBuf, acRollRec, iRecLen);    // 2020
      if (!iRet)
      {
         // Merge Fixt & PP
         if (fdFixt)
         {
            iRet = Dnx_MergeFixt(fdFixt, acBuf);
            if (!iRet)
               iFixtCnt++;
         }

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lLDRRecCount++;

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else if (bDebug)
         LogMsg("*** Skip record# %d [%.12s]", lLDRRecCount, acRollRec);

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp || acRollRec[0] > '9')
         break;      // EOF

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdFixt)
      fclose(fdFixt);
   if (fhOut)
      CloseHandle(fhOut);

   lRecCnt = lLDRRecCount;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total records output:       %u", lLDRRecCount);
   LogMsg("Total fixt records updated: %u", iFixtCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   return 0;
}

/******************************** Dnx_Load_LDR3 *****************************
 *
 * Load LDR 2022 "AGENCYCDCURRSEC_TR601.TAB"
 *
 ****************************************************************************/

int Dnx_Load_LDR3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
      if (!pTmp ||*pTmp > '9')
         break;

      // Create new R01 record
      iRet = Dnx_MergeLien3(acBuf, acRec);
      if (!iRet)
      {
         // Merge Char
         if (fdChar)
            lRet = Dnx_MergeStdChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/*************************** Dnx_ConvertApnRoll *****************************
 *
 * Convert APN format from Cres to MB.  Keep old APN in PREV_APN.
 *
 ****************************************************************************/

int Dnx_ConvertApnRoll(char *pInfile, char *pOutfile, int iRecordLen)
{
   FILE *fdIn, *fdOut;
   int   iTmp, iCnt=0;
   char  acBuf[2048], acTmp[32], acApn[32], acLastApn[32];

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "rb");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "wb");

   fread(acBuf, 1, iRecordLen, fdIn);
   fwrite(acBuf, 1, iRecordLen, fdOut);
   acLastApn[8] = 0;

   while (!feof(fdIn))
   {
      iTmp = fread(acBuf, 1, iRecordLen, fdIn);
      if (iTmp < iRecordLen)
         break;

      if (acBuf[8] != ' ' && !memcmp(acLastApn, acBuf, 8))
         continue;

      memcpy(acLastApn, acBuf, 8);

      // Format new APN - ignore last 2 digits of old APN
      sprintf(acApn, "%.6s0%.2s000", acBuf, &acBuf[6]);

      // Save old APN to previous APN
      memcpy(&acBuf[OFF_PREV_APN], &acBuf[0], 8);

      sprintf(acTmp, "%.3s-%.3s-%.3s-000", acApn, &acApn[3], &acApn[6]);
      memcpy(&acBuf[OFF_APN_D], acTmp, 15);

      memcpy(acBuf, acApn, 12);
      fwrite(acBuf, 1, iRecordLen, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert Roll APN completed with %d records", iCnt);

   return 0;
}

int Dnx_ConvertApnSale(char *pInfile, char *pOutfile)
{
   FILE     *fdIn, *fdOut;
   int      iCnt=0, iTmp;
   char     acBuf[1024], acApn[32], *pBuf;

   SCSAL_REC *pSale = (SCSAL_REC *)&acBuf[0];

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");

   while (!feof(fdIn))
   {
      pBuf = fgets(acBuf, 1024, fdIn);
      if (!pBuf)
         break;

      // Format new APN - ignore last 2 digits of old APN
      sprintf(acApn, "%.6s0%.2s000", acBuf, &acBuf[6]);
      memcpy(acBuf, acApn, 12);

      // Set none sale flag
      iTmp = atoin(pSale->SalePrice, SALE_SIZ_SALEPRICE);
      if (!iTmp && pSale->DocType[0] != '1')
         pSale->NoneSale_Flg = 'Y';

      fputs(acBuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert Sale APN completed with %d records", iCnt);

   return 0;
}

int Dnx_CreateLienRec1(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp;
   int      iRet=0, iTmp;
   LIENEXTR *pLien = (LIENEXTR *)pOutbuf;

   // Remove NULL
   replStrAll(pRollRec, "NULL", "");

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < DNX_FLDCOUNT)
   {
      LogMsg("***** Dnx_CreateLienRec(): bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN on certain book
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || iTmp == 555 || (iTmp > 799 && iTmp < 900))
      return 1;

   // Clear output buffer
   memset((void *)pLien, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLien->acApn, apTokens[iApnFld], strlen(apTokens[iApnFld]));

#ifdef _DEBUG
   //if (!memcmp(pLien->acApn, "131250011000", 10))
   //   iTmp = 0;
#endif


   // Copy ALT_APN
   memcpy(pLien->acPrevApn, apTokens[DNX_FEEPARCEL], strlen(apTokens[DNX_FEEPARCEL]));
   // Assessment year
   memcpy(pLien->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[DNX_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLien->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[DNX_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLien->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixt  = atoi(apTokens[DNX_FIXTRS]);
   long lPP    = atoi(apTokens[DNX_PERSPROP]);     // FixtureRealProperty
   long lMH    = atoi(apTokens[DNX_PPMOBILHOME]);
   long lHSite = atoi(apTokens[DNX_HOMESITE]);
   long lGrow  = atoi(apTokens[DNX_GROWING]);
   long lBusInv= atoi(apTokens[DNX_BUSPROP]);
   lTmp = lFixt+lPP+lMH+lHSite+lGrow+lBusInv;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLien->acOther, acTmp, SIZ_LIEN_OTHERS);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLien->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLien->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLien->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLien->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lBusInv > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lBusInv);
         memcpy(pLien->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLien->acGross, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLien->acRatio, acTmp, SIZ_RATIO);
      }
   }

   // Exemption
   long lExe  = atoi(apTokens[DNX_EXEAMT]);
   if (lExe > 0)
   {
      if (lExe == 999999999)
         lExe = lTmp;
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lExe);
      memcpy(pLien->acExAmt, acTmp, SIZ_EXE_TOTAL);
   }

   if (*apTokens[DNX_EXECODE] == 'E')
   {
      if (!memcmp(apTokens[DNX_EXECODE], "E01", 3))
         pLien->acHO[0] = '1';      // 'Y'
      else
         pLien->acHO[0] = '2';      // 'N'
      vmemcpy(pLien->extra.MB.ExeCode1, apTokens[DNX_EXECODE], SIZ_LIEN_EXECODEX);
   }

   // TRA
   vmemcpy(pLien->acTRA, apTokens[DNX_TRA], SIZ_TRA);

   pLien->LF[0] = 10;
   pLien->LF[1] = 0;

   return 0;
}

int Dnx_CreateLienRec9(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp;
   int      iRet=0, iTmp;
   LIENEXTR *pLien = (LIENEXTR *)pOutbuf;

   // Parse input
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L9_FLDCOUNT)
   {
      LogMsg("***** Dnx_CreateLienRec(): bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN on certain book
   iTmp = atoin(apTokens[L9_ASMT], 3);
   if (!iTmp || iTmp == 555 || (iTmp > 799 && iTmp < 900))
      return 1;

   // Clear output buffer
   memset((void *)pLien, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLien->acApn, apTokens[L9_ASMT], strlen(apTokens[L9_ASMT]));

#ifdef _DEBUG
   //if (!memcmp(pLien->acApn, "131250011000", 10))
   //   iTmp = 0;
#endif

   // Assessment year
   memcpy(pLien->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L9_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLien->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L9_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLien->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value
   long lFixt  = atoi(apTokens[L9_FIXTURESVALUE]);
   long lPP    = atoi(apTokens[L9_PPVALUE]);     
   long lMH    = atoi(apTokens[L9_MHPPVALUE]);
   lTmp = lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLien->acOther, acTmp, SIZ_LIEN_OTHERS);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLien->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLien->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLien->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLien->acGross, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLien->acRatio, acTmp, SIZ_RATIO);
      }
   }

   // Exemption
   long lExe    = atoi(apTokens[L9_HOX]);
   long lOthExe = atoi(apTokens[L9_OTHEREXEMPTION]);
   if (lExe > 0)
   {
      pLien->acHO[0] = '1';      // 'Y'

      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lExe+lOthExe);
      memcpy(pLien->acExAmt, acTmp, SIZ_EXE_TOTAL);
   } else if (lOthExe > 0)
   {
      pLien->acHO[0] = '2';      // 'N'

      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lOthExe);
      memcpy(pLien->acExAmt, acTmp, SIZ_EXE_TOTAL);
   } else
      pLien->acHO[0] = '2';      // 'N'

   // TRA
   lTmp = atol(apTokens[L9_TRA]);
   sprintf(acTmp, "%.6d", lTmp);
   memcpy(pLien->acTRA, acTmp, 6);

   pLien->LF[0] = 10;
   pLien->LF[1] = 0;

   return 0;
}

/***************************** Dnx_CreateLienRec3 ***************************
 *
 * Extract lien data from AGENCYCDCURRSEC_TR601.TAB 2019
 *
 ****************************************************************************/

int Dnx_CreateLienRec3(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64], acApn[16];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Replace null with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L3_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   iRet = strlen(apTokens[L3_ASMT]);
   if (iRet < iApnLen)
      sprintf(acApn, "%.*s%s", iApnLen-iRet, "000", apTokens[L3_ASMT]);
   else
      strcpy(acApn, apTokens[L3_ASMT]);

   // Start copying data
   memcpy(pLienExtr->acApn, acApn, strlen(acApn));

   // TRA
   lTmp = atol(apTokens[L3_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixt  = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixt+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  

   iRet = 1;
   if (lExe1 > 0)
   {
      pLienExtr->acHO[0] = '1';      // 'Y'
      memcpy(pLienExtr->extra.MB.ExeCode1, "E01", 3);
      iRet = 2;
   } else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
   {
      if (iRet == 1)
         vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L3_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
      else
         vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L3_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
   }

   // Taxability
   lTmp = atoin(apTokens[L3_TAXABILITYFULL], 3);
   if (lTmp > 0)
   {
      // Prop 8 
      if (lTmp > 799 && lTmp < 900)
         pLienExtr->SpclFlag = LX_PROP8_FLG;
      vmemcpy(pLienExtr->acTaxCode, apTokens[L3_TAXABILITYFULL], SIZ_LIEN_TAXCODE);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/**************************** Dnx_CreateLienRec14 ***************************
 *
 * Extract lien data from "2020 Secured Tax Roll.txt"
 *
 ****************************************************************************/

int Dnx_CreateLienRec14(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64], acApn[16];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Replace null with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L14_FLDCOUNT)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L14_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L14_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   iRet = strlen(apTokens[L14_ASMT]);
   if (iRet < iApnLen)
      sprintf(acApn, "%.*s%s", iApnLen-iRet, "000", apTokens[L14_ASMT]);
   else
      strcpy(acApn, apTokens[L14_ASMT]);

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[L14_ASMT], strlen(apTokens[L14_ASMT]));

   // TRA
   lTmp = atol(apTokens[L14_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L14_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L14_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixt  = atoi(apTokens[L14_FIXTURESVALUE]);
   long lPers  = atoi(apTokens[L14_PPVALUE]);
   long lPP_MH = atoi(apTokens[L14_MHPPVALUE]);
   lTmp = lFixt+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L14_HOX]);
   long lExe2 = atol(apTokens[L14_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  

   // HO Exempt
   if (lExe1 > 0)
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Taxability
   //lTmp = atoin(apTokens[L14_TAXABILITYFULL], 3);
   //if (lTmp > 0)
   //{
   //   // Prop 8 
   //   if (lTmp > 799 && lTmp < 900)
   //      pLienExtr->SpclFlag = LX_PROP8_FLG;
   //   vmemcpy(pLienExtr->acTaxCode, apTokens[L14_TAXABILITYFULL], SIZ_LIEN_TAXCODE);
   //}

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/****************************** Dnx_ExtrLien ********************************
 *
 * Create lien extract file.
 *
 ****************************************************************************/

int Dnx_ExtrLien(char *pCnty, int iVersion=1)
{
   char     *pTmp, acBuf[256], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   long     iRet, lCnt=0;
   FILE     *fdLien;

   LogMsg0("Extract lien values");

   // Open roll file
   GetIniString(pCnty, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
   LogMsg("Extract lien data from Roll file %s", acBuf);
   fdRoll = fopen(acBuf, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acBuf);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (acRollRec[0] > '9' || acRollRec[0] < '0');

   bool bEof = (pTmp ? false:true);

   // Merge loop
   while (!feof(fdRoll))
   {
      // Create new record
      switch (iVersion)
      {
         case 1:
            iRet = Dnx_CreateLienRec1(acBuf, acRollRec);
            break;
         case 9:
            iRet = Dnx_CreateLienRec9(acBuf, acRollRec);
            break;
         case 3:
            iRet = Dnx_CreateLienRec3(acBuf, acRollRec);
            break;
         default:
            iRet = Dnx_CreateLienRec14(acBuf, acRollRec);
      }
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLien);

         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      } else
         LogMsg("Skip rec: %.50s", acRollRec);

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp || acRollRec[0] > '9')
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("\nTotal lien records extracted:    %u", lCnt);

   return 0;
}

/**************************** Dnx_ConvStdChar ********************************
 *
 * Copy from GLE
 *
 *****************************************************************************/

int Dnx_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acApn[32], acCode[32], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0, iDrop=0;
   double   dTmp;
   STDCHAR  myCharRec;

   LogMsg0("DNX_ConvStdChar - Converting char file %s", pInfile);

   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 4096, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);
      iCnt++;
      if (!pRec)
         break;

      if (acBuf[0] == ' ')
      {
         LogMsg("*** No APN at iCnt=%d: %.40s", iCnt, acBuf);
         iDrop++;
         continue;
      }

      // Parse string
      iTmp = replNull(acBuf);
      if (cDelim == '|')
         iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

      if (iFldCnt < DNX_CHAR_FLDS)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }
      //_strupr(acBuf);

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      if (*apTokens[DNX_CHAR_ASMT] >= '0')
         strcpy(acApn, apTokens[DNX_CHAR_ASMT]);
      else
      {
         strcpy(acApn, apTokens[DNX_CHAR_FEEPARCEL]);
         if (bDebug)
            LogMsg("*** ASMT not available, use FEEPARCEL %s", acApn);
      }

      iTmp = atoin(acApn, 3);
      if (!iTmp || acApn[0] > '9')
      {
         if (bDebug)
            LogMsg("*** Skip APN %s", acApn);
         iDrop++;
         continue;
      }

      memcpy(myCharRec.Apn, acApn, strlen(acApn));
      memcpy(myCharRec.FeeParcel, apTokens[DNX_CHAR_FEEPARCEL], strlen(apTokens[DNX_CHAR_FEEPARCEL]));

      // Format APN
      iRet = formatApn(acApn, acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      // Bldg#
      iTmp = atoi(apTokens[DNX_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[DNX_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - prepare for future - currently not avail 02/04/2021
      iTmp = blankRem(apTokens[DNX_CHAR_POOLSPA]);
      if (iTmp > 1)
      {
         pRec = findXlatCode(apTokens[DNX_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "120217003000", 9))
      //   iRet = 0;
#endif
      // QualityClass
      if (*apTokens[DNX_CHAR_QUALITYCLASS] > ' ')
      {
         strcpy(acTmp, apTokens[DNX_CHAR_QUALITYCLASS]);
         iTmp = iTrim(acTmp);
         if (iTmp > SIZ_CHAR_QCLS)
            replStr(acTmp, "00", "0");
         vmemcpy(myCharRec.QualityClass, acTmp, SIZ_CHAR_QCLS);

         acCode[0] = ' ';
         if (isalpha(acTmp[0])) 
         {
            if (isdigit(acTmp[1]))
            {
               myCharRec.BldgClass = acTmp[0];
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            } else 
            {
               myCharRec.BldgClass = acTmp[1];
               if (isdigit(acTmp[2]))
                  iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
               else
                  LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[DNX_CHAR_QUALITYCLASS], apTokens[DNX_CHAR_ASMT]);
            }
         } else if (isdigit(acTmp[0]))
         {
            if (isalpha(acTmp[1])) 
            {
               myCharRec.BldgClass = acTmp[1];
               if (isdigit(acTmp[2]))
                  iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
               else
                  LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[DNX_CHAR_QUALITYCLASS], apTokens[DNX_CHAR_ASMT]);
            } else
               iRet = Quality2Code((char *)&acTmp[0], acCode, NULL);
         } else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[DNX_CHAR_QUALITYCLASS], apTokens[DNX_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];

      }

      // YrBlt
      int iYrBlt = atoi(apTokens[DNX_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[DNX_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[DNX_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count - no data 5/15/2022
      iTmp = atoi(apTokens[DNX_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      }

      // Stories/NumFloors
      dTmp = atof(apTokens[DNX_CHAR_STORIESCNT]);
      if (dTmp > 0.0 && dTmp < 99.9)
      {
         iRet = sprintf(acTmp, "%.1f", dTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[DNX_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[DNX_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport Sqft
      int iCarport = atoi(apTokens[DNX_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Patio SF
      iTmp = atoi(apTokens[DNX_CHAR_PATIOSF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // Heating 
      iTmp = iTrim(apTokens[DNX_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[DNX_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      }

      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[DNX_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[DNX_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[DNX_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[DNX_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[DNX_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[DNX_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[DNX_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace 
      if (*apTokens[DNX_CHAR_FIREPLACE] > ' ')
      {
         pRec = findXlatCode(apTokens[DNX_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      }

      // Haswell 
      blankRem(apTokens[DNX_CHAR_HASWELL]);
      if (*(apTokens[DNX_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Lot Sqft 
      if (iFldCnt >= DNX_CHAR_LOTSQFT)
      {
         ULONG    lSqft;
         lSqft = (ULONG)atol(apTokens[DNX_CHAR_LOTSQFT]);
         if (lSqft > 1)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(lSqft*SQFT_MF_1000);
            iTmp = sprintf(acTmp, "%u", (long)(dTmp+0.1));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      // Parking spaces
      iTmp = atoi(apTokens[DNX_CHAR_PARKINGSPACES]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.ParkSpace, acTmp, iRet);
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);
   LogMsg("                    dropped: %d", iDrop);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsg("Sorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/***************************** MB_CreateSCSale *******************************
 *
 * Extract sale data from ???_Sales.csv and output to ???_SALE.DAT
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * DateFmt:
 *    0 : Default. Auto format by checking input date
 *    MM_DD_YYYY_1 (MON, DNX)
 *    YYYY_MM_DD   (HUM)
 *
 * DocTypeFmt:
 *    0 : Default. Set DocType=1 (GD) if DocCode=1 or sale price > 0
 *    1 : AMA, BUT, MAD, SON, SBT
 *    2 : PLA
 *    3 : SHA, STA
 *
 * DocNumFmt:
 *    0 : Default. Copy DocNum as is
 *    1 : Format DocNum second part of DocNum to 7 digits (i.e. 2010R1234 = 2010R0001234) (DNX)
 *    2 : Format DocNum second part of DocNum to n digits (i.e. 2010R0034 = 2010R34) (COL)
 *    3 : Remove all nonnumeric after 5th character and format to 6 digits (SON)
 *    4 : Format to yyyyR9999999 if R is in pos 4 or 5 of original DocNum (SBT)
 *    5 : TEH
 *    6 : GLE - format as yyyy9999 (i.e. 2012-0001 = 20120001)
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Dnx_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp, lDocYear;

   LogMsg0("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;
      lCnt++;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Remove null
      replNull(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ' || *(apTokens[MB_SALES_DOCNUM]+4) == 'I')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "052202011000", 9))
      //   iTmp = 0;
#endif
      // Doc date
      if (iDateFmt > 0)
      {
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      } else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
      {
         lDocYear = atoin(acTmp, 4);
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      } else
         lDocYear = 0;

      // Docnum
      if (!iDocNumFmt)
      {
         // DNX,MON, NAP, PLA, SIS, SHA, LAK, MNO, YUB
         memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));
      } else if (iDocNumFmt == 6)
      {  // GLE
         int iYear, iLen;

         myTrim(apTokens[MB_SALES_DOCNUM]);
         iYear = 0;
         iLen = strlen(apTokens[MB_SALES_DOCNUM]);
         if (iLen == 7 && !memcmp(apTokens[MB_SALES_DOCNUM], "00-", 3))
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+3, 4);
            iYear = 2000;
         } else if (!memcmp(apTokens[MB_SALES_DOCNUM], "00", 2) && !memcmp(&SaleRec.DocDate[2], apTokens[MB_SALES_DOCNUM]+2, 2))
         {
            if (iLen == 8)
               lTmp = atoin(apTokens[MB_SALES_DOCNUM]+4, 4);
            else
               lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 4);
            iYear = lDocYear;
         } else if ((pTmp = strchr(apTokens[MB_SALES_DOCNUM], '-')) || (pTmp = strchr(apTokens[MB_SALES_DOCNUM], 'R')) )
         {
            lTmp = atol(pTmp+1);
            iYear = atol(apTokens[MB_SALES_DOCNUM]);
            if (iYear < 100 && lDocYear == (iYear+1900))
               iYear = lDocYear;
         } else if (iLen == 8 && isNumber(apTokens[MB_SALES_DOCNUM]))
         {
            iYear = atoin(apTokens[MB_SALES_DOCNUM], 4);
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+4, 4);
         } else
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
         }
         if (lTmp > 0 && lTmp < 9999)
         {
            if (!iYear && *apTokens[MB_SALES_DOCNUM] == '0')
               iYear = atoin(apTokens[MB_SALES_DOCNUM], 5);
            if (iYear <= lToyear && iYear > 1900)
            {
               if (iYear == lDocYear)
               {
                  sprintf(acTmp, "%d%0.4d   ", iYear, lTmp);
                  memcpy(SaleRec.DocNum, acTmp, 8);
               } else
                  LogMsg("*** Bad DocNum: %s %s (%s) [%s]", apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_DOCDATE], apTokens[MB_SALES_DOCCODE], apTokens[MB_SALES_ASMT]);
            } else if (lDocYear < 1988)
            {
               // Keep DocNum as is
               memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], iLen);
            } else
               LogMsg("*** Bad DocNum: %s [%s]", apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_ASMT]);
         }
      }

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

      // Confirmed sale price
      dTmp = atof(apTokens[MB_SALES_PRICE]);
      if (dTmp > 0.0)
      {
         lPrice = (long)((dTmp*100)/100.0);
         iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      }

      // Do not use confirmed sale price
      if (!bUseConfSalePrice)
         lPrice = 0;

      // Tax
      dTmp = atof(apTokens[MB_SALES_TAXAMT]);
      if (dTmp > 0.0)
      {
         // Save DocTax
         iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);

         // Check for bad DocTax
         if (lPrice == 0)
         {
            // Calculate sale price
            lPrice = (long)(dTmp * SALE_FACTOR);
         }

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Ignore sale price.",
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE]);
               lPrice = 0;
            }
         }
      } 

      // Ignore sale price if less than 1000
      if (lPrice > 1000)
         sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
      else
         memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
      memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);

      // Doc code - accept following code only
      iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
      if (iTmp >= 0)
      {
         memcpy(SaleRec.DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
         if (lPrice < 1000)
            SaleRec.NoneSale_Flg = pDocTbl[iTmp].flag;
      } else if (bDebug)
         LogMsg("*** Unknown DocCode: %s", apTokens[MB_SALES_DOCCODE]);

      // Save original DocCode
      vmemcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);

      // Transfer Type
      if (*apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      if (SaleRec.DocDate[0] > ' ')
      {
         vmemcpy(SaleRec.Seller1, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);

         // Buyer
         iTmp = replStr(apTokens[MB_SALES_BUYER], " % ", " ");
         vmemcpy(SaleRec.Name1, apTokens[MB_SALES_BUYER], SALE_SIZ_BUYER, iTmp);

         // Skip if DocNum not available
         if (SaleRec.DocNum[0] > ' ')
         {
            SaleRec.CRLF[0] = 10;
            SaleRec.CRLF[1] = 0;
            fputs((char *)&SaleRec,fdOut);
         }
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);

   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(27,2,C,EQ,\"  \") F(TXT) DUPO(B2048,1,34)");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -2;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            // Save old cumsale file
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);
            iTmp = rename(acCSalFile, acTmpFile);

            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -2;
      } else
         iTmp = rename(acOutFile, acCSalFile);

      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acCSalFile, acTmpFile);
      }
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp == -2)
      LogMsg("***** Error sorting output file");
   else if (iTmp)
      LogMsg("***** Error renaming to %s", acCSalFile);

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                          output: %d.", lTmp);
   LogMsg("         Latetest recording date: %d.", lLastRecDate);
   return iTmp;
}

/*********************************** loadDnx ********************************
 *
 * Options:
 *
 *    -CDNX -U [-O] [-Xs] [-Mn] [-Ps] [-Ut|-Lt] (Normal update)
 *    -CDNX -L [-Xl] [-Mn] [-O]                 (LDR)
 *
 ****************************************************************************/

int loadDnx(int iSkip)
{
   int   iRet=0;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = DNX_ASMT;
   iApnLen = myCounty.iApnLen;

   //char sInfile[_MAX_PATH], sOutfile[_MAX_PATH];
   //sprintf(sInfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Org");
   //sprintf(sOutfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   //iRet = Dnx_ConvertApnRoll(sInfile, sOutfile, 1900);
   //sprintf(sInfile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Org");
   //sprintf(sOutfile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   //iRet = Dnx_ConvertApnSale(sInfile, sOutfile);

   // Load tax file
   //if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   //{
   //   TC_SetDateFmt(MM_DD_YYYY_1);
   //   iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   //}
   if (iLoadTax == TAX_LOADING)
   {
      // Load tax base
      iRet = MB_Load_TaxBase(bTaxImport, false, iTaxGrp, iHdrRows);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet = MB_Load_TaxCodeMstr(bTaxImport, iHdrRows);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport, iHdrRows);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport, iHdrRows);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Extract NDC recorder sale 
   if (iLoadFlag & EXTR_NRSAL)                     // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSalesFile, _MAX_PATH, acIniFile);
      if (!_access(acSalesFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSalesFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSalesFile);
         return -1;
      }
   }

   // Extract Sale file from Dnx_Sales.csv to Dnx_Sale.sls
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      if (!_access(acSalesFile, 0))
      {
         // Doc date - input format MM/DD/YYYY
         iRet = Dnx_CreateSCSale(MM_DD_YYYY_1, 0, 0, true, &DNX_DocCode[0]);
         if (!iRet)
            iLoadFlag |= MERG_CSAL;
      } else
         LogMsg("*** Sale file not available.  Please verify: %s", acSalesFile);
   }


   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      iRet = Dnx_ConvStdChar(acCharFile);
      if (iRet <= 0)
      {
         LogMsg("***** Error extracting attributes data from %s", acCharFile);
         return -1;
      }
   }

   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      // 2017
      //iRet = Dnx_ExtrLien(myCounty.acCntyCode, 1);
      // 2018
      //iRet = Dnx_ExtrLien(myCounty.acCntyCode, 9);
      // 2019,2021,2022
      iRet = Dnx_ExtrLien(myCounty.acCntyCode, 3);
      // 2020
      //iRet = Dnx_ExtrLien(myCounty.acCntyCode, 14);
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      //iRet = Dnx_Load_LDR(iSkip, 1);             // 2017
      //iRet = Dnx_Load_LDR(iSkip, 9);             // 2018
      //iRet = Dnx_Load_LDR(iSkip, 3);             // 2019, 2021
      //iRet = Dnx_Load_LDR(iSkip, 14);            // 2020
      iRet = Dnx_Load_LDR3(iSkip);                 // 2022
      bUpdPrevApn = true;
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      //iRet = Dnx_Load_Roll(iSkip);
      iRet = Dnx_Load_Roll1(iSkip);
   }

   if (bUpdPrevApn)                                
   {
      // If not defined, use current apn length
      iRet = GetPrivateProfileInt(myCounty.acCntyCode, "PrevApnLen", iApnLen, acIniFile);
      iRet = updatePrevApn(myCounty.acCntyCode, iRet, iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Dnx_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE|FMT_DOCNUM_X4);
   }
   if (!iRet && (iLoadFlag & UPDT_XSAL) )          // -Mn
   {
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

      // Apply Dnx_ash.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, FMT_DOCNUM_X4);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   return iRet;
}
