/*********************************************************************************
 *
 * Notes: 1) Since 2008 LDR, SIS is packed CHAR, SITUS, & EXE into ROLL file.
 *           They now only send us ROLL & SALES files daily
 *        2) Sale file must be resorted before processing
 *        3) Set UseRollChar=Y to use chars from roll file
 *
 * Revision
 * 11/17/2005 1.0.0    First version by Tung Bui
 * 09/27/2007 1.9.17   Fix Sis_ConvertChar() and Sis_MergeChar() to fix Heating,
 *                     Cooling, and Pool problem.  Also modify PQLkup.txt table.
 * 01/26/2008 1.10.3.1 Add code to support standard usecode
 * 03/05/2008 1.10.5.1 Add code to output update records.  
 * 03/13/2008 1.10.5.2 Clear out old usecode before assigning new one.  
 * 04/17/2008 1.10.7   Change Sis_MergeRoll() and related functions to support
 *                     new county layout.
 * 06/04/2008 1.11.2   Fix bug in MergeSisRoll() to skip inactive parcels.
 * 06/17/2008 1.12.3   Update assessment values when LastOfDocNum includes IBC.
 * 07/21/2008 8.2.2    Add exemption data to Sis_MergeRoll() and allow Sis_ConvertChar()
 *                     to process if data file available
 * 03/30/2009 8.7.1    Use MB_MergeSale() to do sale update instead of MergeSale3() to fix 
 *                     problem that non-sale transaction overwrites sale transaction that  
 *                     occurs in the same day  since we only keep one transaction per day.
 * 05/21/2009 8.7.6    Remove reference to OldApn & NewApn files.  We no longer need them.
 * 07/14/2009 9.1.2    Add option -Xa and Sis_ExtrChar() to pull CHAR from ROLL file. Add new
 *                     Sis_MergeMAdr() & Sis_MergeSitus() to format addr from LDR file.
 *                     Modify Sis_MergeSale() not to skip hdr record since it was resorted.
 *                     Add Sis_MergeCumChar() to merge CHAR to R01 in LDR processing.
 *                     Modify Sis_MergeRoll() to add other values. Rename MergeSisRoll()
 *                     to Sis_Load_Roll(). Sort sale file on ASMT & DOCNUM.
 * 07/21/2009 9.1.2.1  Fix bug in Sis_MergeChar() that displays Stories as 4.0
 * 08/03/2009 9.1.4.1  Fix bug formatting Stories & TRS.
 * 11/01/2009 9.2.7.1  Fix duplicate roll record.  Ignore second rec with same APN.
 * 01/04/2009 9.2.10.1 Replace myGetStrQC() with myGetStrBD() to fix problem loading
 *                     roll file.
 * 07/06/2010 10.0.1   Fix bug in Sis_MergeMAdr() to blank out mail addr completely.
 *                     Rewrite Sis_MergeCumChar() to use new STDCHAR format.  Modify Sis_ExtrChar()
 *                     to output STDCHAR Sis_Char.dat and long legal file Sis_LegalDesc.txt.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 05/14/2011 10.8.1   Add -Xs option to create cum sale.
 * 07/07/2011 11.1.2   Add Sis_MergeLegal() to add legal to R01 when loading LDR.
 * 09/16/2011 11.5.4   Fix bug that creates duplicate record in Sis_Load_Roll().
 * 06/06/2012 11.15.2  Modify program to load new data file from SIS.  The new format is
 *                     similar to SHA and most functions are copied from SHA with some modification.
 *                     Add option to load tax files into SQL.
 * 07/09/2012 12.1.2   Modify Sis_MergeSitus() to add zip to R01. Add Sis_ExtrRollChar() to 
 *                     extract CHAR from old roll file Sis_Roll.csv.  To activate, add [OldRollFile]
 *                     to [SIS] section in INI file.
 * 07/21/2012 12.2.2   Add Sis_MergeRChar() to merge missing CHAR fields from old roll file. DO not
 *                     use Sis_ExtrRollChar(). Remove sale merge from Sis_Load_LDR().
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 01/01/2014 13.19.0  Add DocCode[] table and use FixDocType() to cleanup cumsale file.
 * 01/20/2014 13.20.1  Modify DocCode[] to set RESIDENTIAL-MISC IMP non-sale event.
 * 11/06/2014 14.9.1   Fix memory issue in Sis_ConvertChar()
 * 02/09/2015 14.12.2  Use CHAR from Sis_Roll.csv since Sis_Char.txt is not getting regular update.
 *                     County also want us to use Sis_Roll.csv for updated values after LDR.
 *                     Add Sis_LoadRoll2() and modify Sis_MergeRoll().
 * 02/10/2015 14.12.3  Fix bug in Sis_ExtrRollChar() that sorted to wrong filename.
 *                     Fix FirePlace in Sis_MergeChar1() and make it left justify.
 * 02/12/2015 14.13.0  Update Sis_DocCode[] table using latest file from county.
 * 02/13/2015 14.14.0  Update asCooling[] table to convert Heat Pump to Central.
 * 02/27/2015 14.14.2  Fix buffer overrun in Sis_ExtrRollChar() by long legal.
 * 07/05/2015 15.0.3   Fix AIR_COND bug in Sis_MergeChar1().  Remove Sis_MergeChar()
 *                     Modify Sis_Load_LDR() and add Sis_MergeMHAtt() to add MHAtt to R01.
 * 07/31/2015 15.2.0   Add DBA to Sis_MergeMAdr() and fix some specific international addr.
 * 10/13/2015 15.4.0   Use MB_Load_TaxBase() instead of MB_Load_TaxRollInfo() to import 
 *                     data into Tax_Base & Tax_Owner tables.
 * 01/13/2016 15.10.1  Remove old situs in Cal_MergeSitus() before update to avoid remnant from old addr.
 * 07/04/2016 16.0.1   Add -Xa
 * 08/14/2016 16.3.0   Add log msg
 * 07/08/2017 17.1.4   Modify Sis_MergeMAdr() to add DBA. Modify Sis_MergeSitus() to add zipcode.
 *                     Add Sis_Load_LDR3() & Sis_MergeLien3() to handle new LDR layout.
 * 11/16/2018 18.7.5   Add Sis_LoadZoning() and -Mz option.
 * 02/09/2019 18.10.1  Replace Sis_LoadZoning() with MB_LoadZoning()
 * 07/18/2019 19.0.5   Remove extra code in fix lot acres in Sis_MergeLien3().  Only use LANDSIZE
 *                     when ACRES is not available since LANDSIZE value might not be accurate.
 *                     Add Sis_MergeAcres() to merge acres using data from roll update.  Modify Sis_Load_LDR3()
 *                     to update acres using roll data.  Modify Sis_MergeRoll() to populate TRS. Remove 
 *                     unneeded code in Sis_MergeChar1().  Fix TRS and add Ag & Timber Preserve in Sis_MergeRoll().
 * 01/24/2020 19.6.6   Modify Sis_MergeChar1() to reset use data as is, not to preserve prior data.
 * 03/26/2020 19.8.0.1 Add MergeZoning.h and call MergeZoming() instead of MB_LoadZoning()
 * 07/17/2020 20.1.5   Modify Sis_MergeSitus() to work around bad data.
 * 07/19/2020 20.2.0   Modify Sis_MergeMAdr() to fix M_DIR bug.
 * 11/01/2020 20.4.2   Modify Sis_MergeRoll() to populate default PQZoning.
 * 11/30/2021 21.3.1   Add Sis_LoadGrGr() & Sis_LoadGrGrXml() to load GRGR data.  Add Sis_MakeDocLink().
 * 12/07/2021 21.4.1   Modify Sis_LoadGrGrXml() to fix buffer overflow in sApn[].
 * 04/25/2022 21.9.0   Use acGrGrBakTmpl as backup folder for GrGr files. 
 * 09/17/2024 24.1.4   Modify Sis_MergeLien3() to add ExeType.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Markup.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "LoadMB.h"
#include "UseCode.h"
#include "CharRec.h"
#include "MBExtrn.h"
#include "PQ.h"
#include "Tax.h"
#include "doGrGr.h"
#include "MergeSis.h"

static XLAT_CODE  asHeating[] = 
{
   // Value, lookup code, value length
   "01",  "F", 2,               // Baseboard
   "02",  "Z", 2,               // Central Electric
   "03",  "Z", 2,               // Central Gas
   "04",  "C", 2,               // Floor
   "05",  "G", 2,               // Heat Pump
   "06",  "V", 2,               // Monitor
   "07",  "D", 2,               // Wall Unit
   "08",  "E", 2,               // H2O/Oil
   "09",  "K", 2,               // Solar Assist
   "",   "",  0
};

static XLAT_CODE  asCooling[] =
{
   // Value, lookup code, value length
   "01", "C", 2,               // Central
   "02", "E", 2,               // Evaporative
   "04", "L", 2,               // Wall Unit
   "05", "C", 2,               // Heat Pump - this has been converted to "01" 2/13/2015
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "01", "X", 2,               // Above Ground
   "02", "A", 2,               // Concrete In-Ground
   "03", "V", 2,               // Vinyl In-Ground
   "04", "S", 2,               // Spa/ Hot Tub
   "",   "",  0
};

static XLAT_CODE  asParkType[] =
{
   "01", "A1", 2,               // Attached, 1 space
   "02", "A2", 2,               // Attached, 2+ space
   "10", "D1", 2,               // Detached
   "11", "D2", 2,               // Detached
   "20", "E1", 2,               // Basement
   "21", "E2", 2,               // Basement
   "30", "C1", 2,               // Car port
   "31", "C2", 2,               // Car port
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "01", "1", 2,               // Fireplace
   "02", "2", 2,               // 2+ Fireplaces
   "05", "1", 2,               // Hearth & Stove
   "06", "2", 2,               // 2+ Hearth & Stove
   "",   "",  0
};

IDX_TBL5 SIS_DocCode[] =
{// DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN
   "01","1  ",'N',2,3,  // SALE (DOC TAX)  [FORMERLY "OA" CODE]
   "02","13 ",'Y',2,3,  // TRANSFER (NO DOC TAX)   [FORMERLY "OA" CODE]
   "03","19 ",'Y',2,3,  // NON-REAPPRAISABLE EVENT [FORMERLY "A" CODE]
   "04","57 ",'Y',2,3,  // PARTIAL INTEREST (BOTH SALES & TRANSFERS)  [FORMERLY "OB"]
   "05","74 ",'Y',2,3,  // AG PRESERVE SALE-UNIMPROVED (NO SB813)
   "06","1  ",'N',2,3,  // TPZ SALE-UNIMPROVED (NO SB813)
   "07","19 ",'Y',2,3,  // CALAMITY-REMOVAL OR RESTORATION
   "08","19 ",'Y',2,3,  // POSSESSORY INTEREST-TRANS./RENEWAL/NEW CONST.
   "09","19 ",'Y',2,3,  // TRANSFER-DETAILS UNKNOWN [FORMERLY "X," " XF"]
   "10","13 ",'N',2,3,  // NON-REAPPR EVENT W/ SALES PRICE (USE 03 IF NO SALES PRICE)
   "11","77 ",'Y',2,3,  // *FORECLOSURE/ DEED IN LIEU
   "12","74 ",'Y',2,3,  // LAND IMPROVEMENTS
   "13","74 ",'Y',2,3,  // NEW SINGLE FAMILY DWELLING (SFD)
   "14","74 ",'Y',2,3,  // SFD ADDITION/ CONVERSION/ REMODEL
   "15","74 ",'Y',2,3,  // GARAGE/ SHOP/ CARPORT
   "16","74 ",'Y',2,3,  // MISCELLANEOUS IMPROVEMENTS
   "17","74 ",'Y',2,3,  // MH INSTALLATION-NEW, USED, OR VOL. CONVERSION
   "18","74 ",'Y',2,3,  // MH TRANSFER-DUP 9 OR PARK
   "19","19 ",'Y',2,3,  // EMINENT DOMAIN - New 2013
   "22","74 ",'Y',2,3,  // LAND IMPROVEMENTS ON MULTI-RESIDENTIAL
   "23","74 ",'Y',2,3,  // NEW MULTI-RESIDENTIAL BLDG
   "24","74 ",'Y',2,3,  // ADDITION/ CONVERSION TO MULTI-RESIDENTIAL
   "25","74 ",'Y',2,3,  // MISC. IMPROVEMENTS ON MULTI-RESIDENTIAL
   //"26","74 ",'Y',2,3,  // MULTI-RES LAND IMPROVEMENTS
   //"30","74 ",'Y',2,3,  // NEW COMMERCIAL BUILDING
   //"31","74 ",'Y',2,3,  // COMMERCIAL - ADDITION/CONVERSION
   "32","74 ",'Y',2,3,  // LAND IMPROVEMENTS ON COMMERCIAL
   "33","74 ",'Y',2,3,  // NEW COMMERCIAL BUILDING
   "34","74 ",'Y',2,3,  // ADDITION/ CONVERSION TO COMMERCIAL
   "35","74 ",'Y',2,3,  // MISC. IMPROVEMENTS ON COMMERCIAL   
   //"37","74 ",'Y',2,3,  // INDUSTRIAL-ADDITION/CONVERSION
   //"38","74 ",'Y',2,3,  // INDUSTRIAL-MISC IMPROVEMENTS
   //"39","74 ",'Y',2,3,  // INDUSTRIAL-LAND IMPROVEMENTS
   "42","74 ",'Y',2,3,  // LAND IMPROVEMENTS ON INDUSTRIAL
   "43","74 ",'Y',2,3,  // NEW INDUSTRIAL BUILDING
   "44","74 ",'Y',2,3,  // ADDITION/ CONVERSION TO INDUSTRIAL
   "45","74 ",'Y',2,3,  // MISC. IMPROVEMENTS ON INDUSTRIAL
   //"48","74 ",'Y',2,3,  // NEW BUILDING - AG PRESERVE
   //"49","74 ",'Y',2,3,  // AG PRESERVE-ADDITION/CONVERSION
   //"50","74 ",'Y',2,3,  // AG PRESERVE-MISC IMPROVEMENTS
   //"51","74 ",'Y',2,3,  // AG PRESERVE-LAND IMPROVEMENTS
   //"54","74 ",'Y',2,3,  // NON PRESERVE AG-NEW BUILDING
   //"55","74 ",'Y',2,3,  // NON PRESERVE AG-ADDITION/CONVERSION
   //"56","74 ",'Y',2,3,  // NON PRESERVE AG-MISC IMPS
   //"57","74 ",'Y',2,3,  // NON PRESERVE AG-LAND IMPS
   //"60","74 ",'Y',2,3,  // NEW BUILDING TPZ
   "61","74 ",'Y',2,3,  // *TPZ-ADDITION/CONVERSION
   "62","74 ",'Y',2,3,  // *TPZ-MISC IMPROVEMENTS
   //"63","74 ",'Y',2,3,  // TPZ-LAND IMPROVEMENTS
   //"70","74 ",'Y',2,3,  // LIEN DATE UPDATE (CONST IN PROGRESS)
   //"71","74 ",'Y',2,3,  // PROP 8
   //"72","74 ",'Y',2,3,  // CALAMITY APPLICATION
   //"73","74 ",'Y',2,3,  // CALAMITY RESTORATION
   //"74","74 ",'Y',2,3,  // BOARD ORDER VALUE
   //"75","74 ",'Y',2,3,  // NSOLAR IMPROVEMENT
   //"78","74 ",'Y',2,3,  // MH PRTL CIO SA
   //"80","74 ",'Y',2,3,  // FED-STATE-COUNTY PROPERTY (NON TAX)
   "81","74 ",'Y',2,3,  // *GOVT LAND OUTSIDE (SEC 11)
   //"82","19 ",'Y',2,3,  // GOVT LAND-TAXABLE (NO S/A-FHA,ETC)
   //"86","74 ",'Y',2,3,  // NO CHANGE IN VALUE
   //"89","74 ",'Y',2,3,  // MH PLACED ON PERM FOUND
   "90","74 ",'Y',2,3,  // SEGREGATION/ COMBINATION
   "91","74 ",'Y',2,3,  // PROP 58
   "92","74 ",'Y',2,3,  // PROP 60
   "93","74 ",'Y',2,3,  // NON-TAX GOV'T (FEDERAL/ STATE/ COUNTY PROP.)
   "94","74 ",'Y',2,3,  // SECTION 11 PROPERTIES (TAXABLE GOV'T PROP.)
   "95","74 ",'Y',2,3,  // GOV'T ENTITY/ NO SB813  (VA, FMHA, TAX SALE)
   "96","74 ",'Y',2,3,  // CLERICAL FUNCTIONS (CATCH ALL)
   "97","74 ",'Y',2,3,  // AAB ACTIVITY
   "98","74 ",'Y',2,3,  // DEMOLITION/REMOVAL
   "","",0,0,0
};

static int  iValChg, lLegalMatch, lLegalSkip, lRCharMatch, lRCharSkip, lMHCharMatch, lMHCharSkip;
static char acLegalFile[_MAX_PATH], acRollChar[_MAX_PATH];
static FILE *fdLegal, *fdRollChar, *fdMHAtt;

/******************************* Sis_MergeMHAtt ******************************
 *
 * Merge MH attributes
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sis_MergeMHAtt(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet, iLoop, iBeds, iFBath, iRooms;

   // Get first Char rec for first call
   if (!pRec)    
   {
      pRec = fgets(acRec, 512, fdMHAtt);              
      pRec = fgets(acRec, 512, fdMHAtt);       
      lMHCharSkip = 0;
      lMHCharMatch = 0;
   }

   do
   {
      if (!pRec)
      {
         fclose(fdMHAtt);
         fdMHAtt = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip MH Char rec  %.*s", iApnLen, pRec+1);
         pRec = fgets((char *)&acRec[0], 512, fdMHAtt);
         lMHCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   iRet = ParseStringNQ(acRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < SIS_MH_ASMTSTATUS)
   {
      LogMsg("***** Error: bad MH char input record for APN=%s", apTokens[0]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "036320250", 9))
   //   iBeds = 0;
#endif

   // Yrblt
   if (*apTokens[SIS_MH_YEARBUILT] > ' ')
   {
      iRet = atol(apTokens[SIS_MH_YEARBUILT]);
      if (iRet > 1900 && iRet < lLienYear)
         memcpy(pOutbuf+OFF_YR_BLT, apTokens[SIS_MH_YEARBUILT], SIZ_YR_BLT);
   }

   // BldgSqft
   iRet = atol(apTokens[SIS_MH_SIZESQFT]);
   if (iRet > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, iRet);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Beds
   iBeds = atol(apTokens[SIS_MH_BEDROOMS]);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } 

   // Bath
   iFBath = atol(apTokens[SIS_MH_BATHROOMS]);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   }

   // Rooms
   iRooms = atol(apTokens[SIS_MH_TOTALROOMS]);
   if (iRooms > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iRooms);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   } 
   lMHCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 512, fdMHAtt);
   if (!pRec)
   {
      fclose(fdMHAtt);
      fdMHAtt = NULL;
   }

   return 0;
}

/******************************** Sis_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sis_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1, *pTmp2;
   char  acName1[64], acName2[64];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false, false);
   memset(acTmp, 0, 128);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001011002", 9))
   //   iTmp = 0;
#endif

   CString sTmp;
   sTmp = pNames;
   sTmp.Replace(" ETAL", " ");

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, sTmp);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acName2[0] = 0;
   acSave1[0] = 0;
   
   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
   
   // Check for year that goes before TRUST
   iTmp =0;   
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp1 = strlen(acTmp);
      if (iTmp1 > SIZ_NAME1) iTmp1 = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp1);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp1);
      return;
   }

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
      // If TRUST appears after number, save from number forward
      if ( (strstr((char *)&acTmp[iTmp], " TRUST")))
      {
         iTmp--;
         strcpy(acSave1, (char *)&acTmp[iTmp]);
         acTmp[iTmp] = 0;
      }
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
      if ((pTmp=strstr(acName2, " ETAL")) || (pTmp=strstr(acName2, " ET AL")) )
         *pTmp = 0;
   } else if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Filter out words, things in parenthesis 
   // MONDANI NELLIE M ESTATE OF

   if (pTmp=strstr(acTmp, " IRREVOCABLE TRUST"))
   {
      strcpy(acSave1, " IRREVOCABLE TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " EXEMPTION TRUST"))
   {  // DENNY JAMES M EXEMPTION TRUST ETAL & ROEHRICH MARY
      strcpy(acSave1, " EXEMPTION TRUST");
      if (pTmp2 = strchr(pTmp, '&'))
         strcpy(acName2, pTmp2+2);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST ETAL &"))
   {  // GLEASON TRUST ETAL & GERBER TRUST ETAL CP
      strcpy(acSave1, " TRUST");
      strcpy(acName2, pTmp+14);
     if (pTmp2=strstr(acName2, " ETAL"))
        *pTmp2 = 0;
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ( (pTmp = strchr(acTmp, '(')) || 
      (pTmp=strstr(acTmp, " ESTATE OF")) || (pTmp=strstr(acTmp, " LIFE ESTATE")) ||
      (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTS OF")) || 
      (pTmp=strstr(acTmp, " ETAL"))  || (pTmp=strstr(acTmp, " ET AL"))  )
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " JT")) || (pTmp=strstr(acTmp, " CP")) || (pTmp=strstr(acTmp, " CO-TR")))
      *pTmp = 0;


   if ((pTmp=strstr(acTmp, " SUCCS-TR")) || (pTmp=strstr(acTmp, " SUCCESSOR")) || 
      (pTmp=strstr(acTmp, " SURVIVOR")) )
   {
      *pTmp = 0; 
      strcpy(acName1, acTmp);
 
   } else if (pTmp=strstr(acTmp, " FAMILY TRUST &"))
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      // RUMAN RICKY L & RUMAN FAMILY TRUST &
      // RUMAN RICKY L & NANCY FAMILY TRUST & MARIE
      // VERMETTE JAMES & THERESA A & ZENDER ROBERT A &
      strcpy(acSave1, " FAMILY TRUST");
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST &"))
   {  // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      strcpy(acSave1, " LIVING TRUST");
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      strcpy(acSave1, " REV TRUST");
      strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE  
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      *(pTmp+6) = 0;
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
      {
         strcpy(acSave1, pTmp1);
         *pTmp1 = 0;
      } else
      {
         strcpy(acSave1, pTmp);
         *pTmp = 0;
      }

      strcpy(acName1, acTmp);

      // Preparing Name2 - matching up last name
      /* 
      pTmp1 = pTmp+9;
      iTmp1 = strlen(pTmp1);
      for (iTmp = 0; iTmp < iTmp1; iTmp++)
      {
         if (acName1[iTmp] != *pTmp1)
            break;
         pTmp1++;
      }

      // Skip first word after & if it is the same as last name
      if (iTmp > 2 && *(pTmp1-1) == ' ')
      {
         strcat(acName1, " & ");
         // Search and remove TRUST in second part of name since we already saved
         if (pTmp=strstr(pTmp1, " TRUST"))
            *pTmp = 0;
         strcat(acName1, pTmp1);
      } else
      {
         strcpy(acName2, pTmp+9);
      }
      */
      strcpy(acName2, pTmp+9);

   } else if (pTmp=strstr(acTmp, " LAND TRUST"))
   {
      strcpy(acSave1, " LAND TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " FAMILY "))
             || (pTmp=strstr(acTmp, " INCOME TR")) || (pTmp=strstr(acTmp, " LIVING TR")) )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " IRREVOCABLE"))
   {
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp); 
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " ESTATE OF"))
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);
   
   if (pTmp = strstr(acSave1, " THE"))
      *pTmp = 0;

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
         strcpy(acName2, pTmp);
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;
   if ((pTmp=strstr(acName2, " AS ")) || (pTmp=strstr(acName2, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(acTmp1, myOwner.acName1);
      if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
      {
         vmemcpy(pOutbuf+OFF_NAME2, myOwner.acName2, SIZ_NAME2);
      }
      if (myOwner.acVest[0] > ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // Concat what in saved buffer
      //if (acSave[0] && strcmp(acSave, " TRUST"))
      if (acSave1[0])
      {
         if (acTmp1[strlen(acTmp1)-1] == ' ' && acSave1[0] == ' ')
            strcat(acTmp1, (char *)&acSave1[1]);      // avoid double blank
         else
            strcat(acTmp1, acSave1);
      }

      // Save Name1
      vmemcpy(pOutbuf+OFF_NAME1, acTmp1, SIZ_NAME1);

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, SIZ_NAME_SWAP);
      else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
   {
      // Couldn't split names
      if (acSave1[0])
      {
         strcat(acName1, acSave1);
         acSave1[0] = 0;
         vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME1);
      } else
      {
         vmemcpy(pOutbuf+OFF_NAME1, pNames, SIZ_NAME1);
         vmemcpy(pOutbuf+OFF_NAME_SWAP, pNames, SIZ_NAME1);
         acName2[0] = 0;
      }
   }

   // Process Name2 when there is more than one word
   if (acName2[0] && strchr((char *)&acName2[1], ' ') && myOwner.acName2[0] <= ' ')
   {
      if ((pTmp=strstr(acName2, " REVOC")) || (pTmp=strstr(acName2, " FAMILY "))
         || (pTmp=strstr(acName2, " INCOME TR")) || (pTmp=strstr(acName2, " LIVING TR")) )
      {
         vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);
      } else
      {
         acSave1[0] = 0;
         if (pTmp=strstr(acName2, " TRUST"))
         {
            strcpy(acSave1, pTmp);
            *pTmp = 0;
         }

         iRet = splitOwner(acName2, &myOwner, 3);
         if (iRet >= 0)
            strcpy(acName2, myOwner.acName1);

         if (acSave1[0])
            strcat(acName2, acSave1);

         vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);
      }
   }
}

/******************************** Sis_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Sis_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *pCareOf, *p1, *p2, *pDba;
   char  acAddr1[128], acAddr2[128], acTmp[64];

   // Initialize
   removeMailing(pOutbuf, true, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   pCareOf = NULL;
   pDba = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         pCareOf = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else if (!_memicmp(pLine2, "DBA ", 4) )
         {
            pDba = pLine2;
            p1 = pLine3;
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         pCareOf = pLine2;
         p1 = pLine3;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         pDba = pLine1;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         pCareOf = pLine1;
         p1 = pLine2;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         pDba = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      pCareOf = NULL;
   }

   // Check DBA
   if (pDba)
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

   // Check for C/O
   if (pCareOf)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001050080000", 9))
   //   pTmp = "";
#endif

      // 18980 - 57TH AVENUE
      // 901 MORRIS ST - SP 14
      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      } else
      {
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
      }
   }

   strcpy(acAddr2, p2);
   blankRem(acAddr2);
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);

      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}

void Sis_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], *pTmp;
   int      iTmp;
   int      iAdr1, iAdr2;

   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true);

   // Do not process blank line
   if (*apTokens[SIS_ROLL_M_ADDR2] < '0')
      return;

   // CareOf
   pTmp = apTokens[SIS_ROLL_M_ADDR1];
   if (!memcmp(pTmp, "C/O", 3) ||
       !memcmp(pTmp, "C&O", 3) )
   {
      if (*(pTmp+1) == '&')
         *(pTmp+1) = '/';
      updateCareOf(pOutbuf, pTmp, strlen(pTmp));
   } else if (!memcmp(pTmp, "ATTN", 4))
   {
      updateCareOf(pOutbuf, pTmp, strlen(pTmp));
   }

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[SIS_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[SIS_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   } 
   
   if (*apTokens[SIS_ROLL_M_ADDR4] > ' ')
      iAdr2 = SIS_ROLL_M_ADDR4;
   else  if (*apTokens[SIS_ROLL_M_ADDR3] > ' ')
      iAdr2 = SIS_ROLL_M_ADDR3;
   else  if (*apTokens[SIS_ROLL_M_ADDR2] > ' ')
      iAdr2 = SIS_ROLL_M_ADDR2;
   iAdr1 = iAdr2 - 1;

   if (iAdr1 == SIS_ROLL_M_ADDR3 && isdigit(*apTokens[SIS_ROLL_M_ADDR2]))
   {
      iAdr1 = SIS_ROLL_M_ADDR2;    // Backup line 2

      // Combine line3 & line4 for known foreign addr
      if (strstr(apTokens[SIS_ROLL_M_ADDR4], "CANADA") || 
          strstr(apTokens[SIS_ROLL_M_ADDR3], "CANADA") ||
          strstr(apTokens[SIS_ROLL_M_ADDR4], "CHINA") )
      {
         strcat(apTokens[SIS_ROLL_M_ADDR3], " ");
         strcat(apTokens[SIS_ROLL_M_ADDR3], apTokens[SIS_ROLL_M_ADDR4]);
         iAdr2 = SIS_ROLL_M_ADDR3;
      }
   }

   memcpy(pOutbuf+OFF_M_ADDR_D, apTokens[iAdr1], strlen(apTokens[iAdr1]));
   pTmp = strrchr(apTokens[iAdr2], ' ');
   if (pTmp && isdigit(*(pTmp-1)) && isdigit(*(pTmp+1)))
      *pTmp = '-';                  // 92029 1234

   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, apTokens[iAdr2], SIZ_M_CTY_ST_D);  

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001050080000", 9))
   //   acTmp[0] = 0;
#endif

   // Parse address
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   parseMAdr1_3(&sMailAdr, apTokens[iAdr1]);
   parseMAdr2(&sMailAdr, apTokens[iAdr2]);

   if (sMailAdr.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
   }

   if (sMailAdr.strSub[0] > ' ')
      vmemcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, SIZ_M_STR_SUB);

   if (sMailAdr.strDir[0] > ' ')
      vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
   if (sMailAdr.strName[0] > ' ')
      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
   if (sMailAdr.strSfx[0] > ' ')
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Copy unit#
   if (sMailAdr.Unit[0] > ' ')
      vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);


   // city state
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
   }

   // Zipcode
   iTmp = strlen(sMailAdr.Zip);
   if (iTmp >= SIZ_M_ZIP)
   {
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}

void Sis_MergeMAdr1(char *pOutbuf)
{
   char     acTmp[256], acAddr1[256], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   memset(pOutbuf+OFF_M_STRNUM, ' ', SIZ_M_ADDR);
   memset(pOutbuf+OFF_M_ADDR_D, ' ', SIZ_M_ADDR_D);
   memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001510010000", 9))
   //   acTmp[0] = 0;
#endif

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
     sMailAdr.strName[SIZ_M_STREET] = 0;
     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      myTrim(apTokens[MB_ROLL_M_CITY]);
      memcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], strlen(apTokens[MB_ROLL_M_CITY]));
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         iTmp = strlen(apTokens[MB_ROLL_M_ZIP]);
         if (iTmp > 9)
            iTmp = 9;
         memcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], iTmp);
      }      

      memset(pOutbuf+OFF_M_CTY_ST_D, ' ', SIZ_M_CTY_ST_D);
      if (*apTokens[MB_ROLL_M_ADDR4] > ' ')
      {
         myTrim(apTokens[MB_ROLL_M_ADDR4]);
         memcpy(pOutbuf+OFF_M_CTY_ST_D, apTokens[MB_ROLL_M_ADDR4], strlen(apTokens[MB_ROLL_M_ADDR4]));
      } else if (*apTokens[MB_ROLL_M_ADDR3] > ' ')
      {
         myTrim(apTokens[MB_ROLL_M_ADDR3]);
         memcpy(pOutbuf+OFF_M_CTY_ST_D, apTokens[MB_ROLL_M_ADDR3], strlen(apTokens[MB_ROLL_M_ADDR3]));
      } else if (*apTokens[MB_ROLL_M_ADDR2] > ' ')
      {
         myTrim(apTokens[MB_ROLL_M_ADDR2]);
         memcpy(pOutbuf+OFF_M_CTY_ST_D, apTokens[MB_ROLL_M_ADDR2], strlen(apTokens[MB_ROLL_M_ADDR2]));
      }
   }
}

/******************************** Sis_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Sis_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004130013", 9))
   //   acTmp[0] = 0;
#endif
    
   if (*pLine1 <= ' ')
      return -1;

   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

#ifdef _DEBUG
   if (sSitusAdr.lStrNum > 0)
      lSitusMatch++;
   else
      LogMsg("?%.12s - %s %s", pOutbuf, pLine1, pLine2);
#endif

   memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
   memcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   if (sSitusAdr.Zip[0] == '9')
      vmemcpy(pOutbuf+OFF_S_ZIP, sSitusAdr.Zip, SIZ_S_ZIP);

   return 0;
}

int Sis_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Clear old Situs
   removeSitus(pOutbuf);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "037420040000", 9))
   //   lTmp = 0;
#endif

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[SIS_ROLL_S_STRNUM]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);

      // Special case - remove garbage
      strcpy(acAddr1, apTokens[SIS_ROLL_S_STRNUM]);
      iTmp = blankRem(acAddr1);

      // Save original StrNum
      if ((pTmp = strchr(acAddr1, '-')) && (*(pTmp+1) > ' '))
      {
         vmemcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_HSENO);
      } else
      {
         memcpy(pOutbuf+OFF_S_HSENO, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
         if ((pTmp = strchr(acAddr1, ',')) || (pTmp = strchr(acAddr1, ' ')))
         {
            LogMsg("??? StrNum = %s [%s]", acAddr1, apTokens[SIS_ROLL_ASMT]);
            if (isdigit(*(++pTmp)))
            {
               if (!memcmp(pTmp, "1/2", 3))
                  vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp, SIZ_S_STR_SUB);
               else
                  LogMsg("*** Drop trailing str number(2): %s [%s]", pTmp, apTokens[SIS_ROLL_ASMT]);
            } 
            else if (isalpha(*pTmp) && *(pTmp+1) == 0)
               *(pOutbuf+OFF_S_STR_SUB) = *pTmp;
            else
               vmemcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_HSENO);
         }
      }   

      if (*apTokens[SIS_ROLL_S_STRDIR] > ' ' && isDir(apTokens[SIS_ROLL_S_STRDIR]))
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, apTokens[SIS_ROLL_S_STRDIR]);
         memcpy(pOutbuf+OFF_S_DIR, apTokens[SIS_ROLL_S_STRDIR], strlen(apTokens[SIS_ROLL_S_STRDIR]));
      }
   }

   strcat(acAddr1, " ");
   strcat(acAddr1, apTokens[SIS_ROLL_S_STRNAME]);

   strcpy(acTmp, apTokens[SIS_ROLL_S_STRNAME]);
   vmemcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET);

   if (*apTokens[SIS_ROLL_S_STRTYPE] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[SIS_ROLL_S_STRTYPE]);

      iTmp = GetSfxCodeX(apTokens[SIS_ROLL_S_STRTYPE], acTmp);
      if (iTmp > 0)
      {
         iTmp = sprintf(acCode, "%d", iTmp);
         vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      } else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[SIS_ROLL_S_STRTYPE]);
         iBadSuffix++;
      }
   }

   if (*apTokens[SIS_ROLL_S_UNITNO] > ' ')
   {
      if (*apTokens[SIS_ROLL_S_UNITNO] == '#')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, apTokens[SIS_ROLL_S_UNITNO]);
      } else
      {
         strcat(acAddr1, " #");
         strcat(acAddr1, apTokens[SIS_ROLL_S_UNITNO]);
      }
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[SIS_ROLL_S_UNITNO], SIZ_S_UNITNO);
   }

   iTmp = blankRem(acAddr1);
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D, iTmp);

   // Situs city
   if (*apTokens[SIS_ROLL_S_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[SIS_ROLL_S_COMMUNITY], acCode, acAddr1);   
      memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
      {
         iTmp = sprintf(acTmp, "%s CA %.5s", myTrim(acAddr1), apTokens[SIS_ROLL_S_ZIP]);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
      }
   }

   // Situs Zip
   lTmp = atol(apTokens[SIS_ROLL_S_ZIP]);
   if (lTmp > 90000)
      vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[SIS_ROLL_S_ZIP], SIZ_S_ZIP);

   return 0;
}

int Sis_MergeSitus1(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSitus);
      // Get first rec
      pRec = fgets(acRec, 512, fdSitus);
   }

   do 
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF 
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   else
      iRet = ParseStringIQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      // Save original StrNum
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      sprintf(acTmp, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
      pTmp = strchr(acTmp, ' ');
      *(pTmp+1) = 0;
      strcpy(acAddr1, acTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
      }
   }

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);

      strcpy(acTmp, apTokens[MB_SITUS_STRNAME]);
      blankPad(acTmp, SIZ_S_STREET);
      memcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET);

      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
         Sfx2Code(acTmp, acCode);
      else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apTokens[MB_SITUS_STRNAME]);
      if (sAdr.strName[0] > ' ')
      {
         blankPad(sAdr.strName, SIZ_S_STREET);
         memcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      }
      if (sAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, strlen(sAdr.strSfx));

      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      memcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], strlen(apTokens[MB_SITUS_UNIT]));
   }

   iTmp = strlen(acAddr1);
   if (iTmp > SIZ_S_ADDR_D)
      iTmp = SIZ_S_ADDR_D;
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1);   
      blankPadz(acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      // Situs Zip
      lTmp = atol(apTokens[MB_SITUS_ZIP]);
      if (lTmp > 10000 && lTmp < 99999)
         memcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], 5);

      if (acAddr1[0] > ' ')
      {
         sprintf(acTmp, "%s CA %.5s", myTrim(acAddr1), pOutbuf+OFF_S_ZIP);

         blankPad(acTmp, SIZ_S_CTY_ST_D);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
      }
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/******************************** Sis_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Sis_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      //pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "060561300000", 9))
   //   iRet = 0;
#endif
   do 
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg("Skip Sale rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   while (!iTmp)
   {
      // Replace tab char with 0
      if (pRec)
         iRet = ParseStringNQ(pRec, ',', MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         /* Do not use Confidential Sale Price
         } else
         {
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            }
         */
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         MB_MergeSale(&sCurSale, pOutbuf, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Sis_MergeChar ******************************
 *
 * Use CHAR from Sis_Roll.csv
 *
 *****************************************************************************

int Sis_MergeChar(char *pOutbuf)
{
   char     acTmp[256], acCode[32], *pTmp;
   long     lTmp, lBldgSqft, lGarSqft;
   int      iRet, iTmp, iBeds, iFBath, iHBath, iFp;

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Quality Class
   strcpy(acTmp, apTokens[SIS_ROLL_QUALITY]);
   acCode[0] = 0;
   if (isalpha(acTmp[0])) 
   {
      *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0];
      
      iTmp = 0;
      while (acTmp[iTmp] && !isdigit(acTmp[iTmp]))
         iTmp++;

      if (acTmp[iTmp] > '0')
         iRet = Quality2Code((char *)&acTmp[iTmp], acCode, NULL);
   } else if (acTmp[0] > '0' && acTmp[0] <= '9')
      iRet = Quality2Code(acTmp, acCode, NULL);

   blankPad(acCode, SIZ_BLDG_QUAL);
   memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "036320250", 9))
   //   iTmp = 0;
#endif

   // Yrblt
   lTmp = atol(apTokens[SIS_ROLL_YRBLT]);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, apTokens[SIS_ROLL_YRBLT], SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // YrEff
   lTmp = atol(apTokens[SIS_ROLL_YREFF]);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_EFF, apTokens[SIS_ROLL_YREFF], SIZ_YR_EFF);
   else
      memcpy(pOutbuf+OFF_YR_EFF, BLANK32, SIZ_YR_EFF);


   // BldgSqft
   lBldgSqft = atol(apTokens[SIS_ROLL_BLDGSQFT]);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atol(apTokens[SIS_ROLL_GARSQFT]);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = '2';
   } else
   {
      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = ' ';
   }

   // Park type
   pTmp = apTokens[SIS_ROLL_GARTYPE];
   if (*pTmp > ' ')
   {
      iTmp = 0;
      while (asParkType[iTmp].iLen > 0)
      {
         if (!memcmp(pTmp, asParkType[iTmp].acSrc, asParkType[iTmp].iLen))
         {
            *(pOutbuf+OFF_PARK_TYPE) = asParkType[iTmp].acCode[0];
            *(pOutbuf+OFF_PARK_SPACE) = asParkType[iTmp].acCode[1];
            break;
         }
         iTmp++;
      }
   }

   // Heating
   pTmp = apTokens[SIS_ROLL_HEATING];
   if (*pTmp > ' ')
   {
      iTmp = 0;
      while (asHeating[iTmp].iLen > 0)
      {
         if (!memcmp(pTmp, asHeating[iTmp].acSrc, asHeating[iTmp].iLen))
         {
            *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }

   // Cooling 
   pTmp = apTokens[SIS_ROLL_COOLING];
   if (*pTmp > ' ')
   {
      iTmp = 0;
      while (asCooling[iTmp].iLen > 0)
      {
         if (!memcmp(pTmp, asCooling[iTmp].acSrc, asCooling[iTmp].iLen) )
         {
            *(pOutbuf+OFF_AIR_COND) = asCooling[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }
   
   // Pool 
   pTmp = apTokens[SIS_ROLL_POOLS];
   if (*pTmp > ' ')
   {
      iTmp = 0;
      while (asPool[iTmp].iLen > 0)
      {
         if (!memcmp(pTmp, asPool[iTmp].acSrc, asPool[iTmp].iLen) )
         {
            *(pOutbuf+OFF_POOL) = asPool[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }
   
   // Beds
   iBeds = atol(apTokens[SIS_ROLL_BEDS]);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atol(apTokens[SIS_ROLL_FBATHS]);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atol(apTokens[SIS_ROLL_HBATHS]);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Fireplace - 01, 02 (2+ fp), 05 (hearth & Stove), 06 (2+ hearth & Stove)
   pTmp = apTokens[SIS_ROLL_FP];
   if (*pTmp > ' ')
   {
      iTmp = 0;
      while (asFirePlace[iTmp].iLen > 0)
      {
         if (!memcmp(pTmp, asFirePlace[iTmp].acSrc, asFirePlace[iTmp].iLen) )
         {
            *(pOutbuf+OFF_FIRE_PL) = asFirePlace[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }
   
   // Stories
   iTmp = atol(apTokens[SIS_ROLL_FLOORS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d.0 ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   } 

   // Rooms
   iTmp = atol(apTokens[SIS_ROLL_ROOMS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Ag Preserve
   if (*apTokens[SIS_ROLL_APZ] == 'Y')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Timber Preserve
   if (*apTokens[SIS_ROLL_TPZ] == 'Y')
      *(pOutbuf+OFF_TIMBER) = 'Y';

   // Township-Range-Section
   if (*apTokens[SIS_ROLL_SECTION] > '0' && *apTokens[SIS_ROLL_RANGE] > '0' && *apTokens[SIS_ROLL_SECTION] > '0')
   {
      sprintf(acTmp, "%s-%s-%s         ", apTokens[SIS_ROLL_TOWNSHIP],apTokens[SIS_ROLL_RANGE],apTokens[SIS_ROLL_SECTION]);
      memcpy(pOutbuf+OFF_TRS, acTmp, SIZ_TRS);
   } else
      memset(pOutbuf+OFF_TRS, ' ', SIZ_TRS);

   return 0;
}
*/

/******************************* Sis_MergeChar1 ******************************
 *
 * Merge CHAR data only.  Do not use info that is already merged in roll file
 * such as TRS, LotSize, Acres, AgPreserve, TimberPreserve.
 *
 *****************************************************************************/

int Sis_MergeChar1(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lTmp, lBldgSqft, lGarSqft;
   int      iLoop, iBeds, iFBath, iHBath, iRooms;
   STDCHAR  *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);              

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "036320250", 9))
   //   iBeds = 0;
#endif

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   // Quality Class
   //if (pChar->BldgQual > ' ')
   //{
      *(pOutbuf+OFF_BLDG_CLASS) =pChar->BldgClass;
      *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   //}

   // Yrblt
   //if (pChar->YrBlt[0] > ' ')
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
   //if (pChar->YrEff[0] > ' ')
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, MBSIZ_CHAR_BLDGSQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, MBSIZ_CHAR_GARSQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   } else
      memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);

   //if (pChar->ParkType[0] > '0')
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   //if (pChar->ParkSpace[0] > '0')
      *(pOutbuf+OFF_PARK_SPACE) = pChar->ParkSpace[0];

   // Heating
   //*(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   memcpy(pOutbuf+OFF_HEAT, pChar->Heating, SIZ_CHAR_SIZE2);

   // Cooling 
   //*(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];
   memcpy(pOutbuf+OFF_AIR_COND, pChar->Cooling, SIZ_CHAR_SIZE2);

   // Pool
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Beds
   iBeds = atoin(pChar->Beds, MBSIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, MBSIZ_CHAR_FBATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, MBSIZ_CHAR_HBATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

   // Rooms
   iRooms = atoin(pChar->Rooms, MBSIZ_CHAR_TOTALROOMS);
   if (iRooms > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iRooms);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   } else
      memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

   // Fireplace
   //if (isdigit(pChar->Fireplace[0]))
      memcpy(pOutbuf+OFF_FIRE_PL, pChar->Fireplace, SIZ_FIRE_PL);

   // Stories
   lTmp = atoin(pChar->Stories, MBSIZ_CHAR_NUMFLOORS);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%d.0 ", lTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   } else
      memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

   // HasSeptic or HasSewer
   //if (pChar->HasSeptic > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;

   // HasWell
   //if (pChar->HasWell > '0')
      *(pOutbuf+OFF_WATER) = pChar->HasWell;

   // TRS 
   //if (pChar->TRS[0] > '0')
   //   memcpy(pOutbuf+OFF_TRS, pChar->TRS, SIZ_CHAR_TRS);

   //if (pChar->Misc.Comment[0] > ' ')
   //{
   //   pChar->Misc.Comment[SIZ_CHAR_COMMENTS] = 0;
   //   updateLegal(pOutbuf, pChar->Misc.Comment);
   //}
   
   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

// Merge some CHARS from roll file
int Sis_MergeRChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet, iLoop, iTmp;

   // Get first Char rec for first call
   if (!pRec)    
   {
      pRec = fgets(acRec, 1024, fdRollChar);              
      pRec = fgets(acRec, 1024, fdRollChar);              
   }

   do
   {
      if (!pRec)
      {
         fclose(fdRollChar);
         fdRollChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec+1);
         iRet = myGetStrEQ((char *)&acRec[0], MAX_RECSIZE, fdRollChar);
         if (iRet <= 0)
            pRec = NULL;
         lRCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   iRet = ParseStringNQ(acRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < SIS_ROLL_NOTES)
   {
      LogMsg("***** Error: bad roll char input record for APN=%s", apTokens[0]);
      return -1;
   }
   
   // Yrblt
   iTmp = atol(apTokens[SIS_ROLL_YRBLT]);
   if (iTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, apTokens[SIS_ROLL_YRBLT], SIZ_YR_BLT);

   // YrEff
   iTmp = atol(apTokens[SIS_ROLL_YREFF]);
   if (iTmp > 1700)
      memcpy(pOutbuf+OFF_YR_EFF, apTokens[SIS_ROLL_YREFF], SIZ_YR_EFF);

   // BldgSqft
   iTmp = atol(apTokens[SIS_ROLL_BLDGSQFT]);
   if (iTmp > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, iTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Garage Sqft
   iTmp = atol(apTokens[SIS_ROLL_GARSQFT]);
   if (iTmp > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, iTmp);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = '2';
   }

   // Park type
   pTmp = apTokens[SIS_ROLL_GARTYPE];
   if (*pTmp > ' ')
   {
      iTmp = 0;
      while (asParkType[iTmp].iLen > 0)
      {
         if (!memcmp(pTmp, asParkType[iTmp].acSrc, asParkType[iTmp].iLen))
         {
            *(pOutbuf+OFF_PARK_TYPE) = asParkType[iTmp].acCode[0];
            *(pOutbuf+OFF_PARK_SPACE) = asParkType[iTmp].acCode[1];
            break;
         }
         iTmp++;
      }
   }

   // Fireplace - 01, 02 (2+ fp), 05 (hearth & Stove), 06 (2+ hearth & Stove)
   iTmp = atol(apTokens[SIS_ROLL_FP]);
   if (iTmp > 0)
   {
      iTmp &= 3;     // Very tricky, do not change
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iTmp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   }

   // Stories
   iTmp = atol(apTokens[SIS_ROLL_FLOORS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d.0 ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   } 

   // Rooms
   iTmp = atol(apTokens[SIS_ROLL_ROOMS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Ag Preserve
   if (*apTokens[SIS_ROLL_APZ] == 'Y')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Timber Preserve
   if (*apTokens[SIS_ROLL_TPZ] == 'Y')
      *(pOutbuf+OFF_TIMBER) = 'Y';

   // Township-Range-Section
   if (*apTokens[SIS_ROLL_SECTION] > '0' && *apTokens[SIS_ROLL_RANGE] > '0' && *apTokens[SIS_ROLL_SECTION] > '0')
   {
      sprintf(acTmp, "%s-%s-%s         ", apTokens[SIS_ROLL_TOWNSHIP],apTokens[SIS_ROLL_RANGE],apTokens[SIS_ROLL_SECTION]);
      memcpy(pOutbuf+OFF_TRS, acTmp, SIZ_TRS);
   } 

   // Recorded Doc
   if (*apTokens[SIS_ROLL_DOCNUM] > '0')
   {
      iTmp = strlen(apTokens[SIS_ROLL_DOCNUM]);
      if (iTmp > SIZ_TRANSFER_DOC)
         iTmp = SIZ_TRANSFER_DOC;
      memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[SIS_ROLL_DOCNUM], iTmp);
      pTmp = dateConversion(apTokens[SIS_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   }

   lRCharMatch++;

   // Get next Char rec
   iRet = myGetStrEQ((char *)&acRec[0], MAX_RECSIZE, fdRollChar);
   if (iRet <= 0)
      pRec = NULL;

   return 0;
}

/******************************** Sis_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************/

int Sis_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do 
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';
         return 1;      // EOF 
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, ',');
      pTmp += 2;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 0;
   }

   iRet = ParseStringNQ(pRec, ',', MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Sis_MergeTax ******************************
 *
 * Note: 
 *
 *****************************************************************************/

int Sis_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double	dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iRet;

   // Merge tax data
   while (!iTmp)
   {
      iTmp = ParseStringNQ(pRec, ',', MB_TAX_ROLLCAT+1, apTokens);
      if (iTmp < MB_TAX_ROLLCAT)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
         return -1;
      }

      // If same tax year, apply values
      //if (!memcmp(myCounty.acYearAssd, apTokens[MB_TAX_YEAR], 4))
      {
         dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
         dTax1 = atof(acTmp);
         dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
         dTax2 = atof(acTmp);
         dTmp = dTax1+dTax2;
         if (dTax1 == 0.0 || dTax2 == 0.0)
            dTmp *= 2;

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/********************************* Sis_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sis_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp, lLand, lImpr;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse input string
   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < SIS_ROLL_NOTES)
   {
      LogMsg("***** Sis_MergeRoll: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Skip inactive parcel
   if (*apTokens[SIS_ROLL_STATUS] == 'I')
      return 1;

   // Ignore APN starts with 800-999 except 910 & 912 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910 && iTmp != 912))
      return 1;

   // Check Last DocNum for value changed when it contains "IBC"
   pTmp = apTokens[SIS_ROLL_LDN];

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[SIS_ROLL_FEEPARCEL], strlen(apTokens[SIS_ROLL_FEEPARCEL]));

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "47SIS", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      lLand = atoi(apTokens[SIS_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      lImpr = atoi(apTokens[SIS_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[SIS_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[SIS_ROLL_FIXTR_RP]);
      long lPP    = atoi(apTokens[SIS_ROLL_BUSPROP]);
      long lMH    = atoi(apTokens[SIS_ROLL_PPMOBILHOME]);
      lTmp = lFixt+lFixtRP+lPP+lMH;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      long lHOE    = atoi(apTokens[SIS_ROLL_HO_EXE]);
      long lOthExe = atoi(apTokens[SIS_ROLL_OTH_EXE]);
      if (lHOE > 0 || lOthExe > 0)
      {
         if (lHOE > 0)
            *(pOutbuf+OFF_HO_FL) = '1';     // Y
         else
            *(pOutbuf+OFF_HO_FL) = '2';     // N

         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, (lHOE + lOthExe));
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      } else
         *(pOutbuf+OFF_HO_FL) = '2';     
   } else if (!memcmp(pTmp+4, "IBC", 3))
   {
      // Land
      lLand = atoi(apTokens[SIS_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      lImpr = atoi(apTokens[SIS_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[SIS_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[SIS_ROLL_FIXTR_RP]);
      long lPP    = atoi(apTokens[SIS_ROLL_BUSPROP]);
      long lMH    = atoi(apTokens[SIS_ROLL_PPMOBILHOME]);
      lTmp = lFixt+lFixtRP+lPP+lMH;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      } else
         memset(pOutbuf+OFF_RATIO, ' ', SIZ_RATIO);

      long lHOE    = atoi(apTokens[SIS_ROLL_HO_EXE]);
      long lOthExe = atoi(apTokens[SIS_ROLL_OTH_EXE]);
      if (lHOE > 0 || lOthExe > 0)
      {
         if (lHOE > 0)
            *(pOutbuf+OFF_HO_FL) = '1';     // Y
         else
            *(pOutbuf+OFF_HO_FL) = '2';     // N

         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, (lHOE + lOthExe));
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      } else
         *(pOutbuf+OFF_HO_FL) = '2';     

      if (bDebug)
         LogMsg0("+++ Update value for %s", apTokens[iApnFld]);
      iValChg++;
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[SIS_ROLL_TRA], strlen(apTokens[SIS_ROLL_TRA]));

   // Format APN - do this to reset existing format
   iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   if (*apTokens[SIS_ROLL_STATUS] == 'D')    // Deferred Inactive
      *(pOutbuf+OFF_STATUS) = 'P';           // Pending
   else
      *(pOutbuf+OFF_STATUS) = *apTokens[SIS_ROLL_STATUS];

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Legal
   remChar(apTokens[SIS_ROLL_LEGAL], 9);
   updateLegal(pOutbuf, apTokens[SIS_ROLL_LEGAL]);

   // Zoning
   memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
   if (*apTokens[SIS_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[SIS_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   pTmp = strcpy(acTmp, apTokens[SIS_ROLL_USECODE]);
   if (*pTmp == ' ')
      pTmp++;
   if (*pTmp > ' ')
   {
      _strupr(pTmp);
      iTmp = strlen(pTmp);
      memcpy(pOutbuf+OFF_USE_CO, pTmp, iTmp);
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, pTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Recorded Doc
   if (*apTokens[SIS_ROLL_DOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[SIS_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
      {
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[SIS_ROLL_DOCNUM], SIZ_TRANSFER_DOC);
         vmemcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
      }
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[iApnFld], "049122090000", 9))
   //   iTmp = 0;
#endif

   // Acres
   dTmp = atof(apTokens[SIS_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Township-Range-Section
   if (*apTokens[SIS_ROLL_TOWNSHIP] > '0' && *apTokens[SIS_ROLL_RANGE] > '0' && *apTokens[SIS_ROLL_SECTION] > '0')
   {
      sprintf(acTmp, "%s-%s-%s          ", apTokens[SIS_ROLL_TOWNSHIP],apTokens[SIS_ROLL_RANGE],apTokens[SIS_ROLL_SECTION]);
      memcpy(pOutbuf+OFF_TRS, acTmp, SIZ_TRS);
   }

   // Ag Preserve
   if (*apTokens[SIS_ROLL_APZ] == 'Y')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Timber Preserve
   if (*apTokens[SIS_ROLL_TPZ] == 'Y')
      *(pOutbuf+OFF_TIMBER) = 'Y';

   iRet = 0;
   // Owner
   try {
      Sis_MergeOwner(pOutbuf, apTokens[SIS_ROLL_OWNER]);
   } catch(...) {
      LogMsg("***** Exeception occured in Sis_MergeOwner()");
      iRet = -2;
   }

   // Mailing 
   try {
      Sis_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Sis_MergeMAdr()");
      iRet = -3;
   }

   // Situs
   try {
      Sis_MergeSitus(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Sis_MergeSitus()");
      iRet = -4;
   }

   return iRet;
}

int Sis_MergeRoll1(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_M_ADDR4)
   {
      LogMsg("***** Sis_MergeRoll1: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN starts with 800-999 except 910 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910  && iTmp != 920))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "47SIS", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Format APN - do this to reset existing format
   iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030330020", 9))
   //   iTmp = 0;
#endif
   // Legal
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   pTmp = strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   if (*pTmp == ' ')
      pTmp++;
   if (*pTmp > ' ')
   {
      _strupr(pTmp);
      iTmp = strlen(pTmp);
      memcpy(pOutbuf+OFF_USE_CO, pTmp, iTmp);
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, pTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], SIZ_TRANSFER_DOC);
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   }

   // Owner
   try {
      Sis_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   } catch(...) {
      LogMsg("***** Exeception occured in Sis_MergeOwner()");
   }

   // Mailing 
   try {
      Sis_MergeMAdr1(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Sis_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************* Sis_Load_Roll ******************************
 *
 * This function is copied from Sha_Load_Roll() and modified for SIS to be used
 * with .TXT input files as of 6/1/2012.
 *
 ******************************************************************************/

int Sis_Load_Roll1(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg("Roll update using Sis_Load_Roll1()");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "EMP");
   if (cDelim == '|')
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) DEL(124) ");
   else
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) ");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TAX");
   if (cDelim == '|')
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) DEL(124) ");
   else
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) ");
   fdTax = fopen(acTmpFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
      return -2;
   }

   // Open lien file
   fdLienExt = NULL;
   /*
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Lien file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   }
   */

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "030330020", 9) || !memcmp((char *)&acRollRec[1], "030330020", 9))
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Sis_MergeRoll1(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Sis_MergeSitus1(acBuf);

            // Merge Lien
            //lRet = 1;
            //if (fdLienExt)
            //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

            // Merge Exe if not found in LienExt
            //if (fdExe && lRet)
            //   lRet = MB_MergeExe2(acBuf, 0);

            // Merge Char
            if (fdChar)
               lRet = Sis_MergeChar1(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acBuf, 0);
               //lRet = MB_MergeTax(acBuf);

            iRollUpd++;

            // Save last recording date
            //lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
            //if (lRet > lLastRecDate && lRet < lToday)
            //   lLastRecDate = lRet;

            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         }

         // Read next roll record
         do
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
            if (pTmp && cDelim == '"')
               pTmp++;
         } while (pTmp && (memcmp(pTmp, "799", 3) > 0) && (memcmp(pTmp, "910", 3) && memcmp(pTmp, "920", 3)));

         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Sis_MergeRoll1(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Sis_MergeSitus1(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Sis_MergeChar1(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acRec, iHdrRows);

            // Save last recording date
            //lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            //if (lRet > lLastRecDate && lRet < lToday)
            //   lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         do
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
            if (pTmp && cDelim == '"')
               pTmp++;
         } while (pTmp && (memcmp(pTmp, "799", 3) > 0) && (memcmp(pTmp, "900", 3) < 0 ));

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Sis_MergeRoll1(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Sis_MergeSitus1(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Sis_MergeChar1(acRec);

         // Merge Sales
         //if (fdSale)
         //   lRet = Sis_MergeSale(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, iHdrRows);

         // Save last recording date
         //lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         //if (lRet > lLastRecDate && lRet < lToday)
         //   lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      do
      {
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (pTmp && cDelim == '"')
            pTmp++;
      } while (pTmp && (memcmp(pTmp, "799", 3) > 0) && (memcmp(pTmp, "900", 3) < 0 ));

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   return 0;
}

/********************************* Sis_Load_Roll ******************************
 *
 * This function processes current roll using Sis_Roll.csv
 *
 ******************************************************************************/

int Sis_Load_Roll2(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acLastApn[SIZ_APN_S+2];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Roll update using Sis_Load_Roll2()");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open roll file - Input records may be broken, watch out
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open tax file
   LogMsg("Open Tax file %s", acTaxFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TAX");
   if (cDelim == '|')
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) DEL(124) ");
   else
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) ");
   fdTax = fopen(acTmpFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
      return -2;
   }
   
   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   iRet = myGetStrBD((char *)&acRollRec[0], '"', MAX_RECSIZE, fdRoll);
   bEof = (iRet > 0 ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "001010080000", 9) )
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, (char *)&acRollRec[1], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Sis_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            iRollUpd++;
            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acBuf, 0);

            // Merge Char
            if (fdChar)
               lRet = Sis_MergeChar1(acBuf);
         }

         // Save last APN
         memcpy(acLastApn, acBuf, SIZ_APN_S);

         // Read next roll record
         iTmp = myGetStrBD((char *)&acRollRec[0], '"', MAX_RECSIZE, fdRoll);
         if (iTmp <= 0)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Sis_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         // Avoid duplicate record
         if (!iRet && memcmp(acLastApn, acRec, SIZ_APN_S))
         {
            iNewRec++;
            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acRec, 0);

            // Merge Char
            if (fdChar)
               lRet = Sis_MergeChar1(acRec);

            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         } else if (!iRet)
            LogMsg("*** Drop duplicate record: %.14s", acLastApn);

         lCnt++;
         iTmp = myGetStrBD((char *)&acRollRec[0], '"', MAX_RECSIZE, fdRoll);
         if (iTmp <= 0)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired 
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Sis_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         iNewRec++;
         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, 0);

         // Merge Char
         if (fdChar)
            lRet = Sis_MergeChar1(acRec);

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      iRet = myGetStrBD((char *)&acRollRec[0], '"', MAX_RECSIZE, fdRoll);
      if (iRet <= 0)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdTax)
      fclose(fdTax);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   LogMsg("Recs with changed values:   %u\n", iValChg);
   return 0;
}

// This function is used for old CSV format
int Sis_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acLastApn[SIZ_APN_S+2];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Sort sale file on ASMT & DocNum
   sprintf(acRec, "%s\\%s\\%s_sale.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acSalesFile, acRec, "S(#1,C,A,#2,C,A)");

   // Open Sales file
   LogMsg("Open Sales file %s", acRec);
   fdSale = fopen(acRec, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acRec);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   //pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   //bEof = (pTmp ? false:true);
   iRet = myGetStrBD((char *)&acRollRec[0], '"', MAX_RECSIZE, fdRoll);
   bEof = (iRet > 0 ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "910001864", 9) )
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, (char *)&acRollRec[1], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Sis_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Remove old sale data
            if (bClearSales)
               ClearOldSale(acBuf);

            // Merge Sales
            if (fdSale)
               lRet = Sis_MergeSale(acBuf);

            iRollUpd++;
         }

         // Save last APN
         memcpy(acLastApn, acBuf, SIZ_APN_S);

         // Read next roll record
         iTmp = myGetStrBD((char *)&acRollRec[0], '"', MAX_RECSIZE, fdRoll);
         if (iTmp <= 0)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Sis_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         // Avoid duplicate record
         if (!iRet && memcmp(acLastApn, acRec, SIZ_APN_S))
         {
            // Merge Sales
            if (fdSale)
               lRet = Sis_MergeSale(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         } else if (!iRet)
            LogMsg("*** Drop duplicate record: %.14s", acLastApn);

         lCnt++;
         iTmp = myGetStrBD((char *)&acRollRec[0], '"', MAX_RECSIZE, fdRoll);
         if (iTmp <= 0)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired 
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Sis_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Sales
         if (fdSale)
            lRet = Sis_MergeSale(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      iRet = myGetStrBD((char *)&acRollRec[0], '"', MAX_RECSIZE, fdRoll);
      if (iRet <= 0)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Sale skiped:      %u\n", lSaleSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   LogMsg("Recs with changed values:   %u", iValChg);
   return 0;
}

/********************************* Sis_MergeLien *****************************
 *
 *
 *
 *****************************************************************************

int Sis_MergeLien(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   int      iLoop;
   SIS_LIEN *pLien;

   // Get first lien rec for first call
   if (!pRec && !lValueMatch)
      pRec = fgets(acRec, 1024, fdValue);
   pLien = (SIS_LIEN *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pLien->Apn, LSIZ_APN);
      if (iLoop > 0)
         pRec = fgets(acRec, 1024, fdValue);
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003120007", 9))
   //   lTmp = 0;
#endif

   // Format APN
   //memcpy(acTmp1, pLien->Apn, LSIZ_APN);
   //acTmp1[LSIZ_APN] = 0;
   //iRet = formatApn(acTmp1, acTmp, &myCounty);
   //memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   //iRet = formatMapLink(acTmp1, acTmp, &myCounty);
   //memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // County code
   //memcpy(pOutbuf+OFF_CO_NUM, "47SIS", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // TRA
   if (pLien->Tra[0] > ' ')
      memcpy(pOutbuf+OFF_TRA, pLien->Tra, LSIZ_TRA);

   // Land
   long lLand = atoin(pLien->Land, LSIZ_VALUE);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pLien->Structure, LSIZ_VALUE);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lTmp = atoin(pLien->Growing, LSIZ_VALUE);
   lTmp += atoin(pLien->Fixtures, LSIZ_VALUE);
   lTmp += atoin(pLien->FixturesRealProp, LSIZ_VALUE);
   lTmp += atoin(pLien->PPBusiness, LSIZ_VALUE);
   lTmp += atoin(pLien->PPMH, LSIZ_VALUE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }                                          

   // LandUse
   //if (pLien->UseCode[0] > '0')
   //   memcpy(pOutbuf+OFF_USE_CO, pLien->UseCode, LSIZ_USECODE);

   // TaxCode
   if (pLien->Taxability[0] > ' ' && memcmp(pLien->Taxability, "000", 3))
      memcpy(pOutbuf+OFF_TAX_CODE, pLien->Taxability, LSIZ_TAXABILITY);

   // HO Exemption
   lTmp = atoin(pLien->HO_Exe, LSIZ_VALUE);
   if (lTmp > 0)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Other exemption
   lTmp += atoin(pLien->OtherExe, LSIZ_VALUE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   lValueMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdValue);

   return 0;
}

/******************************** CreateSisRoll ****************************
 *
 * Tested on 9/1/2005 by SPN
 *
 ****************************************************************************/

int CreateSisRoll(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return 4;

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Drop header record
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   iRet = myGetStrQC((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (iRet > 0)
   {
      // Create new R01 record
      iRet = Sis_MergeRoll(acBuf, acRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Sales
         if (fdSale)
            lRet = Sis_MergeSale(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      iRet = myGetStrBD((char *)&acRec[0], '"', MAX_RECSIZE, fdRoll);
      //iRet = myGetStrQC((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSale)
      fclose(fdSale);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Last recording date:        %u\n", lLastRecDate);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Sis_ConvertChar() **************************
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 * Notes: 
 *    - SIS doesn't have the HAS* fields. MB_CHAR_ASMT is the last field.
 *
 ****************************************************************************/

//int Sis_ConvertChar(char *pInfile)
//{
//   FILE     *fdIn, *fdOut;
//   char     acOutBuf[1024], acInBuf[1024], acTmpFile[256], acTmp[256], *pRec;
//   int      iRet, iTmp, iCnt=0;
//   MB_CHAR  *pCharRec = (MB_CHAR *)&acOutBuf;
//
//   LogMsg("\nConverting char file %s\n", pInfile);
//   if (!(fdIn = fopen(pInfile, "r")))
//      return -1;
//
//   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   if (!(fdOut = fopen(acTmpFile, "w")))
//   {
//      fclose(fdIn);
//      LogMsg("***** Error creating output file %s", acTmpFile);
//      return -2;
//   }
//
//   // Loop start
//   while (!feof(fdIn))
//   {
//      pRec = fgets(acInBuf, 1024, fdIn);
//      if (acInBuf[1] > '9')
//         pRec = fgets(acInBuf, 1024, fdIn);    // Skip header
//
//      if (!pRec)
//         break;
//
//      iRet = ParseStringNQ(pRec, ',', MB_CHAR_ASMT+1, apTokens);
//      if (iRet < MB_CHAR_ASMT)            // SPN 06/28/2006
//      {
//         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
//         continue;
//      }
//
//      memset(acOutBuf, ' ', sizeof(MB_CHAR));
//      memcpy(pCharRec->Asmt, apTokens[MB_CHAR_ASMT], strlen(apTokens[MB_CHAR_ASMT]));
//
//      iTmp = atoi(apTokens[MB_CHAR_POOLS]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%.2d", iTmp);
//         memcpy(pCharRec->NumPools, acTmp, iRet);
//      }
//
//      memcpy(pCharRec->LandUseCat, apTokens[MB_CHAR_USECAT], strlen(apTokens[MB_CHAR_USECAT]));
//      memcpy(pCharRec->QualityClass, apTokens[MB_CHAR_QUALITY], strlen(apTokens[MB_CHAR_QUALITY]));
//      memcpy(pCharRec->YearBuilt, apTokens[MB_CHAR_YRBLT], strlen(apTokens[MB_CHAR_YRBLT]));
//      memcpy(pCharRec->BuildingSize, apTokens[MB_CHAR_BLDGSQFT], strlen(apTokens[MB_CHAR_BLDGSQFT]));
//      memcpy(pCharRec->SqFTGarage, apTokens[MB_CHAR_GARSQFT], strlen(apTokens[MB_CHAR_GARSQFT]));
//      memcpy(pCharRec->Heating, apTokens[MB_CHAR_HEATING], strlen(apTokens[MB_CHAR_HEATING]));
//      memcpy(pCharRec->Cooling, apTokens[MB_CHAR_COOLING], strlen(apTokens[MB_CHAR_COOLING]));
//      memcpy(pCharRec->HeatingSource, apTokens[MB_CHAR_HEATING_SRC], strlen(apTokens[MB_CHAR_HEATING_SRC]));
//      memcpy(pCharRec->CoolingSource, apTokens[MB_CHAR_COOLING_SRC], strlen(apTokens[MB_CHAR_COOLING_SRC]));
//
//      iTmp = atoi(apTokens[MB_CHAR_BEDS]);
//      if (iTmp > 0)
//         memcpy(pCharRec->NumBedrooms, apTokens[MB_CHAR_BEDS], strlen(apTokens[MB_CHAR_BEDS]));
//
//      iTmp = atoi(apTokens[MB_CHAR_FBATHS]);
//      if (iTmp > 0)
//         memcpy(pCharRec->NumFullBaths, apTokens[MB_CHAR_FBATHS], strlen(apTokens[MB_CHAR_FBATHS]));
//
//      iTmp = atoi(apTokens[MB_CHAR_HBATHS]);
//      if (iTmp > 0)
//         memcpy(pCharRec->NumHalfBaths, apTokens[MB_CHAR_HBATHS], strlen(apTokens[MB_CHAR_HBATHS]));
//
//      iTmp = atoi(apTokens[MB_CHAR_FP]);
//      if (iTmp > 0)
//         memcpy(pCharRec->NumFireplaces, apTokens[MB_CHAR_FP], strlen(apTokens[MB_CHAR_FP]));
//
//      memcpy(pCharRec->FeeParcel, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));
//
//      pCharRec->CRLF[0] = '\n';
//      pCharRec->CRLF[1] = '\0';
//      fputs(acOutBuf, fdOut);
//
//      if (!(++iCnt % 1000))
//         printf("\r%u", iCnt);
//   }
//
//   if (fdIn) fclose(fdIn);
//   if (fdOut) fclose(fdOut);
//
//   // Sort output on ASMT
//   if (iCnt > 100)
//   {
//      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
//      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
//      strcpy(pInfile, acCChrFile);
//   } else
//   {
//      printf("\n");
//      iRet = 0;
//   }
//
//   return iRet;
//}

/***************************** Sis_ExtrChar *********************************
 *
 * Extract characteristic and legal from roll file.
 * Return number of records extracted
 *
 ****************************************************************************/

int Sis_ExtrRollChar(char *pInfile)
{
   FILE     *fdIn, *fdOut, *fdLgl; // ,*fdTest;
   STDCHAR  myCharRec;
   char     acRollRec[MAX_RECSIZE], acTmpFile[256], acTmp[2048], acCode[16], *pTmp;
   char     cDelim = ',';
   int      iRet, iTmp, iBeds, iFBath, iHBath, iMaxTokens, iCnt=0;
   double   dTmp;

   LogMsgD("Extracting CHAR from %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening file %s", pInfile);
      return -1;
   }

   if (!(fdLgl = fopen(acLegalFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating legal file %s", acLegalFile);
      return -3;
   }

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

#ifdef _DEBUG
   //strcpy(acTmp, pInfile);
   //pTmp = strstr(acTmp, ".csv");
   //if (pTmp) 
   //   strcpy(pTmp, ".txt");
   //if (!(fdTest = fopen(acTmp, "w")))
   //{
   //   LogMsg("***** Error opening file %s", acTmp);
   //   return -1;
   //}
#endif

   // Drop header record
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdIn);
   iMaxTokens = countTokens(acRollRec, cDelim);
#ifdef _DEBUG
   //fputs(acRollRec, fdTest);
#endif

   // Loop start
   while (pTmp)
   {
      //iRet = myGetStrEQ((char *)&acRollRec[0], MAX_RECSIZE, fdIn);
      iRet = myGetStrTC((char *)&acRollRec[0], cDelim, iMaxTokens, MAX_RECSIZE, fdIn);
      if (iRet <= 0)
         break;         // eof
      else if (iRet != iMaxTokens)
         LogMsg("*** WARNING: bad record [%.12s]", acRollRec);

#ifdef _DEBUG
      //fputs(acRollRec, fdTest);
#endif

      iRet = ParseStringNQ(acRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iRet < SIS_ROLL_NOTES)
      {
         LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
         return -1;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[SIS_ROLL_ASMT], strlen(apTokens[SIS_ROLL_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[SIS_ROLL_FEEPARCEL], strlen(apTokens[SIS_ROLL_FEEPARCEL]));
      memcpy(myCharRec.Zoning, apTokens[SIS_ROLL_ZONING], strlen(apTokens[SIS_ROLL_ZONING]));
      memcpy(myCharRec.LandUse, apTokens[SIS_ROLL_USECODE], strlen(apTokens[SIS_ROLL_USECODE]));
      memcpy(myCharRec.Nbh_Code, apTokens[SIS_ROLL_NBHCODE], strlen(apTokens[SIS_ROLL_NBHCODE]));

      // Lot Size
      dTmp = atof(apTokens[SIS_ROLL_ACRES]);
      if (dTmp > 0.0)
      {
         iTmp = sprintf(acTmp, "%u", (int)(dTmp * 1000.0));
         memcpy(myCharRec.LotAcre, acTmp, iTmp);

         dTmp *= SQFT_PER_ACRE;
         iTmp = sprintf(acTmp, "%u", (ULONG)(dTmp+0.5));
         memcpy(myCharRec.LotSqft, acTmp, iTmp);
      }

      // Quality class
      if (*apTokens[SIS_ROLL_QUALITY] > ' ')
      {
         strcpy(acTmp, apTokens[SIS_ROLL_QUALITY]);
         pTmp = _strupr(acTmp);
         memcpy(myCharRec.QualityClass, pTmp, strlen(pTmp));
         acCode[0] = 0;
         if (isalpha(*pTmp))
         {
            myCharRec.BldgClass = *pTmp;
            if (isdigit(acTmp[1]))
               iRet = Quality2Code(&acTmp[1], acCode, NULL);
            else if (isdigit(acTmp[2]))
               iRet = Quality2Code(&acTmp[2], acCode, NULL);
         } else if (isdigit(*pTmp))
         {
            iTmp = atol(pTmp);
            if (iTmp < 100)
               iRet = Quality2Code(pTmp, acCode, NULL);
         }

         if (acCode[0] > '0')
            myCharRec.BldgQual = acCode[0];
      }

      memcpy(myCharRec.YrBlt, apTokens[SIS_ROLL_YRBLT], strlen(apTokens[SIS_ROLL_YRBLT]));
      memcpy(myCharRec.YrEff, apTokens[SIS_ROLL_YREFF], strlen(apTokens[SIS_ROLL_YREFF]));
      memcpy(myCharRec.BldgSqft, apTokens[SIS_ROLL_BLDGSQFT], strlen(apTokens[SIS_ROLL_BLDGSQFT]));
      memcpy(myCharRec.GarSqft, apTokens[SIS_ROLL_GARSQFT], strlen(apTokens[SIS_ROLL_GARSQFT]));

      // Park type
      pTmp = apTokens[SIS_ROLL_GARTYPE];
      if (*pTmp > ' ')
      {
         iTmp = 0;
         while (asParkType[iTmp].iLen > 0)
         {
            if (!memcmp(pTmp, asParkType[iTmp].acSrc, asParkType[iTmp].iLen))
            {
               myCharRec.ParkType[0] = asParkType[iTmp].acCode[0];
               myCharRec.ParkSpace[0] = asParkType[iTmp].acCode[1];
               break;
            }
            iTmp++;
         }
      }

      // Pool 
      pTmp = apTokens[SIS_ROLL_POOLS];
      if (*pTmp > ' ')
      {
         iTmp = 0;
         while (asPool[iTmp].iLen > 0)
         {
            if (!memcmp(pTmp, asPool[iTmp].acSrc, asPool[iTmp].iLen) )
            {
               myCharRec.Pool[0] = asPool[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }
   
      // Stories
      iTmp = atol(apTokens[SIS_ROLL_FLOORS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "060561300000", 9))
      //   iTmp = 0;
#endif

      // Heating
      pTmp = apTokens[SIS_ROLL_HEATING];
      if (*pTmp > ' ')
      {
         iTmp = 0;
         while (asHeating[iTmp].iLen > 0)
         {
            if (!memcmp(pTmp, asHeating[iTmp].acSrc, asHeating[iTmp].iLen))
            {
               myCharRec.Heating[0] = asHeating[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }

      // Cooling 
      pTmp = apTokens[SIS_ROLL_COOLING];
      if (*pTmp > ' ')
      {
         iTmp = 0;
         while (asCooling[iTmp].iLen > 0)
         {
            if (!memcmp(pTmp, asCooling[iTmp].acSrc, asCooling[iTmp].iLen) )
            {
               myCharRec.Cooling[0] = asCooling[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }
   
      // Beds
      iBeds = atol(apTokens[SIS_ROLL_BEDS]);
      if (iBeds > 0)
      {
         iRet = sprintf(acTmp, "%d", iBeds);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }
      // Bath
      iFBath = atol(apTokens[SIS_ROLL_FBATHS]);
      if (iFBath > 0)
      {
         iRet = sprintf(acTmp, "%d", iFBath);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }
      // Half bath
      iHBath = atol(apTokens[SIS_ROLL_HBATHS]);
      if (iHBath > 0)
      {
         iRet = sprintf(acTmp, "%d", iHBath);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }
      // Rooms
      iTmp = atol(apTokens[SIS_ROLL_ROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Fireplace - 01, 02 (2+ fp), 05 (hearth & Stove), 06 (2+ hearth & Stove)
      pTmp = apTokens[SIS_ROLL_FP];
      if (*pTmp > ' ')
      {
         iTmp = 0;
         while (asFirePlace[iTmp].iLen > 0)
         {
            if (!memcmp(pTmp, asFirePlace[iTmp].acSrc, asFirePlace[iTmp].iLen) )
            {
               myCharRec.Fireplace[0] = asFirePlace[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }
     
      // Township-Range-Section
      if (*apTokens[SIS_ROLL_SECTION] > '0' && *apTokens[SIS_ROLL_RANGE] > '0' && *apTokens[SIS_ROLL_SECTION] > '0')
      {
         iTmp = sprintf(acTmp, "%s-%s-%s", apTokens[SIS_ROLL_TOWNSHIP],apTokens[SIS_ROLL_RANGE],apTokens[SIS_ROLL_SECTION]);
         vmemcpy(myCharRec.TRS, acTmp, SIZ_CHAR_TRS, iTmp);
      } 

      // Ag Preserve
      if (*apTokens[SIS_ROLL_APZ] == 'Y')
         myCharRec.AGP = 'Y';

      // Timber Preserve
      if (*apTokens[SIS_ROLL_TPZ] == 'Y')
         myCharRec.TPZ = 'Y';

      // Legal
      iTmp = blankRem(apTokens[SIS_ROLL_LEGAL], 0);
      if (iTmp > 0)
      {
         iTmp = sprintf(acTmp, "%s|%s\n", apTokens[SIS_ROLL_ASMT], apTokens[SIS_ROLL_LEGAL]);
         if (iTmp > 400)
            LogMsg("+++ Long legal (%d): %s", iTmp, acTmp);
         fputs(acTmp, fdLgl);
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);
   if (fdLgl) fclose(fdLgl);
   //if (fdTest) fclose(fdTest);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
      //sprintf(acRollChar, "%s\\%s\\%s_roll_char.dat", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      //LogMsgD("\nSorting char file %s --> %s", acTmpFile, acRollChar);
      //iRet = sortFile(acTmpFile, acRollChar, "S(1,12,C,A) F(TXT)");
   } else
   {
      acRollChar[0] = 0;
      printf("\n");
      iRet = 0;
   }

   return iRet;
}

/****************************** Sis_ExtrChar() ******************************
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

int Sis_ExtrChar()
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmpFile[256], acTmp[256], acCode[64], *pRec, *pTmp;
   int      iRet, iTmp, iCmp, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nExtracting char file %s\n", acCharFile);

   if (!(fdIn = fopen(acCharFile, "r")))
   {
      LogMsg("***** Error open input file %s.  Please check for file exist.", acCharFile);
      return -1;
   }

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);

      if (!pRec)
         break;

      if (cDelim == ',')
         iRet = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iRet = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iRet < MB2_CHAR_BLDGTYPE)
      {
         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));

      // Apn
      memcpy(myCharRec.Apn, apTokens[MB2_CHAR_ASMT], strlen(apTokens[MB2_CHAR_ASMT]));
      
      // Fee parcel
      memcpy(myCharRec.FeeParcel, apTokens[MB2_CHAR_FEE_PRCL], strlen(apTokens[MB2_CHAR_FEE_PRCL]));
      
      // Pool/Spa
      if (*apTokens[MB2_CHAR_POOLS] >= 'A')
      {
         iTmp = 0;
         iCmp = -1;
         while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[MB2_CHAR_POOLS], asPool[iTmp].acSrc, asPool[iTmp].iLen)) )
            iTmp++;

         if (!iCmp)
            myCharRec.Pool[0] = asPool[iTmp].acCode[0];
      } else if (*apTokens[MB2_CHAR_POOLS] > '0' && *apTokens[MB2_CHAR_POOLS] <= '9')
         myCharRec.Pool[0] = 'P';

      // Use cat
      memcpy(myCharRec.LandUseCat, apTokens[MB2_CHAR_USECAT], strlen(apTokens[MB2_CHAR_USECAT]));

      // Quality Class
      if (*apTokens[MB2_CHAR_QUALITY] > ' ')
      {
         strcpy(acTmp, apTokens[MB2_CHAR_QUALITY]);
         pTmp = _strupr(acTmp);
         memcpy(myCharRec.QualityClass, pTmp, strlen(pTmp));
         acCode[0] = 0;
         if (!memcmp(pTmp, "AV", 2) || !_memicmp(pTmp, "AG", 2) || !strcmp(acTmp, "A"))
            acCode[0] = 'A';
         else if (!memcmp(pTmp, "POOR", 4))
            acCode[0] = 'P';
         else if (*pTmp == 'G')
            acCode[0] = 'G';
         else if (!memcmp(pTmp, "FAIR", 4) || !strcmp(pTmp, "F"))
            acCode[0] = 'F';
         else if (!memcmp(pTmp, "LOW", 3))   // Unknown condition
            acCode[0] = 0;
         else if (isalpha(*pTmp))
         {
            myCharRec.BldgClass = *pTmp;
            if (isdigit(acTmp[1]))
               iRet = Quality2Code(&acTmp[1], acCode, NULL);
            else if (isdigit(acTmp[2]))
               iRet = Quality2Code(&acTmp[2], acCode, NULL);
         } else if (isdigit(*pTmp))
         {
            iTmp = atol(pTmp);
            if (iTmp < 100)
               iRet = Quality2Code(pTmp, acCode, NULL);
         }

         if (acCode[0] > '0')
            myCharRec.BldgQual = acCode[0];
      }

      // YrBlt
      iTmp = atoi(apTokens[MB2_CHAR_YRBLT]);
      if (iTmp > 1600 && iTmp <= lLienYear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // BldgSqft
      iTmp = atoi(apTokens[MB2_CHAR_BLDGSQFT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // GarSqft
      iTmp = atoi(apTokens[MB2_CHAR_GARSQFT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[iApnFld], "001040360", 9))
      //   iTmp = 0;
#endif

      // Heating
      if (*apTokens[MB2_CHAR_HEATING] > ' ')
      {
         iTmp = 0;
         while (asHeating[iTmp].iLen > 0)
         {
            if (!memcmp(apTokens[MB2_CHAR_HEATING], asHeating[iTmp].acSrc, asHeating[iTmp].iLen))
            {
               myCharRec.Heating[0] = asHeating[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }

      // Cooling
      if (*apTokens[MB2_CHAR_COOLING] > ' ')
      {
         iTmp = 0;
         while (asCooling[iTmp].iLen > 0)
         {
            if (!memcmp(apTokens[MB2_CHAR_COOLING], asCooling[iTmp].acSrc, asCooling[iTmp].iLen))
            {
               myCharRec.Cooling[0] = asCooling[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }

      // Heating src
      blankRem(apTokens[MB2_CHAR_HEATING_SRC]);
      memcpy(myCharRec.HeatingSrc, apTokens[MB2_CHAR_HEATING_SRC], strlen(apTokens[MB2_CHAR_HEATING_SRC]));

      // Cooling src
      blankRem(apTokens[MB2_CHAR_COOLING_SRC]);
      memcpy(myCharRec.CoolingSrc, apTokens[MB2_CHAR_COOLING_SRC], strlen(apTokens[MB2_CHAR_COOLING_SRC]));

      // Beds
      iTmp = atoi(apTokens[MB2_CHAR_BEDS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Baths
      iTmp = atoi(apTokens[MB2_CHAR_FBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half baths
      iTmp = atoi(apTokens[MB2_CHAR_HBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // Fireplace
      iTmp = atoi(apTokens[MB2_CHAR_FP]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Fireplace, acTmp, iRet);
      }

      blankRem(apTokens[MB2_CHAR_HASSEPTIC]);
      if (*(apTokens[MB2_CHAR_HASSEPTIC]) > '0')
      {
         myCharRec.HasSeptic = 'Y';
         myCharRec.HasSewer = 'S';
      } else
      {
         blankRem(apTokens[MB2_CHAR_HASSEWER]);
         if (*(apTokens[MB2_CHAR_HASSEWER]) > '0')
            myCharRec.HasSewer = 'Y';
      }

      blankRem(apTokens[MB2_CHAR_HASWELL]);
      if (*(apTokens[MB2_CHAR_HASWELL]) > '0')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // BldgSeqNum
      iTmp = atoi(apTokens[MB2_CHAR_BLDGSEQNO]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      }

      // UnitSeqNum
      iTmp = atoi(apTokens[MB2_CHAR_UNITSEQNO]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.UnitSeqNo, acTmp, iRet);
      }

      // Building Type

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
   } else
      iRet = 0;

   printf("\n");
   return iRet;
}

/***************************** Sis_CreateLienRec ****************************
 *
 * Format lien extract record
 *
 ****************************************************************************/

int Sis_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < SIS_ROLL_NOTES)
   {
      LogMsg("***** Sis_CreateLienRec: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[iApnFld], "004180019", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[iApnFld], strlen(apTokens[iApnFld]));

   long lHOE    = atoi(apTokens[SIS_ROLL_HO_EXE]);
   long lOthExe = atoi(apTokens[SIS_ROLL_OTH_EXE]);
   if (lHOE > 0 || lOthExe > 0)
   {
      if (lOthExe > 0)
         memcpy(pLienExtr->acExCode, apTokens[SIS_ROLL_OTH_EXECODE], strlen(apTokens[SIS_ROLL_OTH_EXECODE]));

      if (lHOE > 0)
         pLienExtr->acHO[0] = '1';     // Y
      else
         pLienExtr->acHO[0] = '2';     // N

      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, (lHOE + lOthExe));
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_EXE_TOTAL);
   } else
      pLienExtr->acHO[0] = '2';     // N

   // TRA
   memcpy(pLienExtr->acTRA, apTokens[SIS_ROLL_TRA], strlen(apTokens[SIS_ROLL_TRA]));

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[SIS_ROLL_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[SIS_ROLL_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lPP   = atoi(apTokens[SIS_ROLL_FIXTR_RP]);
   long lFixt   = atoi(apTokens[SIS_ROLL_FIXTRS]);
   long lBusPP= atoi(apTokens[SIS_ROLL_BUSPROP]);
   long lMH   = atoi(apTokens[SIS_ROLL_PPMOBILHOME]);

   lTmp = lFixt+lPP+lBusPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_IMPR);
      }
      if (lBusPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lBusPP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_IMPR);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************** Sis_ExtrLien ******************************
 *
 * Extract lien data from ???_lien.csv
 *
 ****************************************************************************/

int Sis_ExtrLien(LPCTSTR pCnty)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg("\nExtract lien roll for %s", pCnty);

   // Open roll file
   LogMsg("Open Lien Date Roll file %s", acRollFile);

   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acTmpFile);
      return 2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Skip 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Get 1st rec
   iRet = myGetStrQC((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (iRet > 0)
   {
      // Create new record
      iRet = Sis_CreateLienRec(acBuf, acRec);
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      }

      // Get next roll record
      iRet = myGetStrBD((char *)&acRec[0], '"', MAX_RECSIZE, fdRoll);
      //iRet = myGetStrQC((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Sis_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sis_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

#ifdef _DEBUG
//   if (!memcmp(pRollRec, "011061007000", 12))
//      iTmp = 0;
#endif

   // Replace null char with blank
   iRet = replNull(pRollRec, ' ', 0);
   //iRet = replChar(pRollRec, 34, 39, 0);

   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Sis_MergeLien: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L_ASMT], strlen(apTokens[L_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L_FEEPARCEL], strlen(apTokens[L_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink 
   iRet = formatMapLink(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "47SIS", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
   long lGrow  = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);
   long lPers  = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
   long lPP_MH = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L_TAXAMT1]);
   double dTax2 = atof(apTokens[L_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L_TRA], strlen(apTokens[L_TRA]));

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030330020", 9))
   //   iTmp = 0;
#endif
   // Legal - This is comments, not legal.  It will be overwritten in Sis_MergeRoll()
   if (*apTokens[L_PARCELDESCRIPTION] > ' ')
      updateLegal(pOutbuf, apTokens[L_PARCELDESCRIPTION]);

   // UseCode
   strcpy(acTmp, apTokens[L_USECODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Ag preserved
   if (*apTokens[L_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';      
      
   // Owner
   Sis_MergeOwner(pOutbuf, apTokens[L_OWNER]);

   // Situs
   Sis_MergeSitus(pOutbuf, apTokens[L_SITUS1], apTokens[L_SITUS2]);

   // Mailing
   Sis_MergeMAdr(pOutbuf, apTokens[L_MAILADDRESS1], apTokens[L_MAILADDRESS2], apTokens[L_MAILADDRESS3], apTokens[L_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L_TAXABILITY], true, true);

   return 0;
}

/********************************* Sis_MergeLien *****************************
 *
 * For 2017 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sis_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s [Tokens=%d]", apTokens[L3_ASMT], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "47SIS", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SIS_Exemption);

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Sis_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "036320250000", 9))
   //   iTmp = 0;
#endif
   
   // Situs
   Sis_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Sis_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*apTokens[L3_CURRENTDOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   return 0;
}

/******************************** Sis_MergeLegal *****************************
 *
 *
 *****************************************************************************/

int Sis_MergeLegal(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   int      iLoop;

   // Get first Legal rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdLegal);              

   do
   {
      if (!pRec)
      {
         fclose(fdLegal);
         fdLegal = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Legal rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdLegal);
         lLegalSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Update legal
   updateLegal(pOutbuf, &acRec[iApnLen+1]);
   
   lLegalMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdLegal);

   return 0;
}

/****************************** Sis_MergeRollData ****************************
 *
 * Merge Acres, Agpreserve, Timber preserve from roll file 
 * This function can be used when loading LDR where these fields are not available
 * or not reliable.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Sis_MergeRollData(char *pOutbuf)
{
   static   char acRec[MAX_RECSIZE], *pRec=NULL;
   char     acTmp[64];
   int      iRet=0, iTmp, lTmp;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (acRec[1] > '9' || acRec[1] < '0')
         pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Roll rec %.*s", iApnLen, pRec);
         iRet = myGetStrTC(acRec, ',', SIS_ROLL_FLDS, MAX_RECSIZE, fdRoll);
         if (!iRet)
            pRec = NULL;
         else if (iRet != SIS_ROLL_FLDS)
            LogMsg("***** File may be corrupted. %d tokens in  %.12s", iRet, acRec);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringNQ(acRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < SIS_ROLL_FLDS)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input roll record for APN=%s", apTokens[0]);
         iRet = -1;
      }

      return iRet;
   }

   // Merge data
   double dTmp = atof(apTokens[SIS_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      lRollMatch++;
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Ag Preserve
   if (*apTokens[SIS_ROLL_APZ] == 'Y')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Timber Preserve
   if (*apTokens[SIS_ROLL_TPZ] == 'Y')
      *(pOutbuf+OFF_TIMBER) = 'Y';

   // Township-Range-Section
   if (*apTokens[SIS_ROLL_TOWNSHIP] > '0' && *apTokens[SIS_ROLL_RANGE] > '0' && *apTokens[SIS_ROLL_SECTION] > '0')
   {
      sprintf(acTmp, "%s-%s-%s          ", apTokens[SIS_ROLL_TOWNSHIP],apTokens[SIS_ROLL_RANGE],apTokens[SIS_ROLL_SECTION]);
      memcpy(pOutbuf+OFF_TRS, acTmp, SIZ_TRS);
   }

   return 0;
}

/***************************** Sis_Load_LDR() *******************************
 *
 * Load TR601 file into 1900-byte record.
 *
 ****************************************************************************/

int Sis_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acMHFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acTmpFile, acRollFile, "S(#1,C,A)");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open MH Char file
   GetIniString(myCounty.acCntyCode, "MHChar", "", acMHFile, _MAX_PATH, acIniFile);
   LogMsg("Open MH Char file %s", acMHFile);
   fdMHAtt = fopen(acMHFile, "r");
   if (fdMHAtt == NULL)
   {
      LogMsg("***** Error opening MH Char file: %s\n", acMHFile);
      return -2;
   }

   // Open legal file
   if (!_access(acLegalFile, 0))
   {
      LogMsg("Open Legal file %s", acLegalFile);
      fdLegal = fopen(acLegalFile, "r");
      if (fdLegal == NULL)
      {
         LogMsg("***** Error opening Legal file: %s\n", acLegalFile);
         return -2;
      }
   } else
      fdLegal = NULL;

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      lLDRRecCount++;

      // Create new R01 record
      iRet = Sis_MergeLien(acBuf, acRec);
      if (!iRet)
      {
         lRet = 1;

         // Merge Char
         if (fdChar)
            lRet = Sis_MergeChar1(acBuf);

         // Merge MH Char
         if (fdMHAtt)
            lRet = Sis_MergeMHAtt(acBuf);

         // Merge Legal
         if (fdLegal)
            lRet = Sis_MergeLegal(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp || acRec[1] > '9')
         break;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdMHAtt)
      fclose(fdMHAtt);
   if (fdLegal)
      fclose(fdLegal);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of MH Char matched:  %u", lMHCharMatch);
   LogMsg("Number of Legal matched:    %u\n", lLegalMatch);

   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of MHChar skiped:    %u", lMHCharSkip);
   LogMsg("Number of Legal skiped:     %u\n", lLegalSkip);
   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/***************************** Sis_Load_LDR3() ******************************
 *
 * New version to handle new 2017 LDR layout.
 * Update in 2019 to pull Acres from roll update file.
 *
 ****************************************************************************/

int Sis_Load_LDR3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acMHFile[_MAX_PATH], acLdrFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   // Sort roll file on ASMT
   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acLdrFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acTmpFile, acLdrFile, "S(#1,C,A)");

   // Open LDR file
   LogMsg("Open LDR file %s", acLdrFile);
   fdLDR = fopen(acLdrFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acLdrFile);
      return -1;
   }  

   // Open roll update file to get acres info
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRollFile, _MAX_PATH, acIniFile);
   LogMsg("Open Roll update file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }
   lRollMatch = 0;

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open MH Char file
   GetIniString(myCounty.acCntyCode, "MHChar", "", acMHFile, _MAX_PATH, acIniFile);
   LogMsg("Open MH Char file %s", acMHFile);
   fdMHAtt = fopen(acMHFile, "r");
   if (fdMHAtt == NULL)
   {
      LogMsg("***** Error opening MH Char file: %s\n", acMHFile);
      return -2;
   }

   // Open legal file
   if (!_access(acLegalFile, 0))
   {
      LogMsg("Open Legal file %s", acLegalFile);
      fdLegal = fopen(acLegalFile, "r");
      if (fdLegal == NULL)
      {
         LogMsg("***** Error opening Legal file: %s\n", acLegalFile);
         return -2;
      }
   } else
      fdLegal = NULL;

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      // Create new R01 record
      iRet = Sis_MergeLien3(acBuf, acRec);
      if (!iRet)
      {
         lRet = 1;

         // Merge Char
         if (fdChar)
            lRet = Sis_MergeChar1(acBuf);

         // Merge MH Char
         if (fdMHAtt)
            lRet = Sis_MergeMHAtt(acBuf);

         // Merge Legal
         if (fdLegal)
            lRet = Sis_MergeLegal(acBuf);

         // Merge Acres, AgPreserve & Timber preserve
         if (fdRoll)
            lRet = Sis_MergeRollData(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp || acRec[1] > '9')
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdMHAtt)
      fclose(fdMHAtt);
   if (fdLegal)
      fclose(fdLegal);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of MH Char matched:  %u", lMHCharMatch);
   LogMsg("Number of Acres updated:    %u", lRollMatch);
   LogMsg("Number of Legal matched:    %u\n", lLegalMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of MHChar skiped:    %u", lMHCharSkip);
   LogMsg("Number of Legal skiped:     %u\n", lLegalSkip);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************* Sis_LoadGrGrXml ******************************
 *
 * Process GrGr in XML format
 * Logic:
 *    - Load all docs
 *    - If there is tax amount, calculate sale price
 *    - Merge all docs with same doc# & APN.
 * Notes:
 *    - Same Doc# with multiple APN, create as many records as APNs.  If two records
 *      has same Doc# & APN, keep one that has Transfer Tax
 *    - See FRE
 *
 ******************************************************************************/

int Sis_LoadGrGrXml(LPCSTR pXmlFile, FILE *fdOut) 
{
   // Get pathname
   CString  csText, csNotes, sApn[1024], sPageCnt, sGrtor[MAX_NAMES], sGrtee[MAX_NAMES];
   CString  sDocTitle, sDocNum, sDocDate, sDocType, sDocTax, sFeeTotal;
   CFile    file;
   int      iTmp, iApnIdx, iGrtor, iGrtee, iRecCnt=0, iNoApn=0;
   char     acTmp[1024], acDocDate[16], acOutbuf[2048];
   GRGR_DEF *pGrGr = (GRGR_DEF *)&acOutbuf[0];

   LogMsg0("Load GrGr file: %s", pXmlFile);

#ifdef _DEBUG
   //if (!stricmp(pXmlFile, "F:\\CO_OUT\\LAX\\01272010-FullData.xml"))
   //   iRet = 0;
#endif

   if (!file.Open( pXmlFile, CFile::modeRead ) )
   {
      LogMsg("***** Error unable to open file: %s", pXmlFile);
      return -1;
   }
   int nFileLen = (int)file.GetLength();

   // Allocate buffer for binary file data
   unsigned char* pBuffer = new unsigned char[nFileLen + 2];
   nFileLen = file.Read( pBuffer, nFileLen );
   file.Close();
   pBuffer[nFileLen] = '\0';
   pBuffer[nFileLen+1] = '\0'; // in case 2-byte encoded

   // Windows Unicode file is detected if starts with FEFF
   if (pBuffer[0] == 0xff && pBuffer[1] == 0xfe )
   {
      // Contains byte order mark, so assume wide char content
      // non _UNICODE builds should perform UCS-2 (wide char) to UTF-8 conversion here
      csText = (LPCWSTR)(&pBuffer[2]);
      csNotes += _T("File starts with hex FFFE, assumed to be wide char format. ");
   } else
   {
      // _UNICODE builds should perform UTF-8 to UCS-2 (wide char) conversion here
      csText = (LPCSTR)pBuffer;
   }
   delete [] pBuffer;

   // If it is too short, assume it got truncated due to non-text content
   if (csText.GetLength() < nFileLen / 2 - 20 )
   {
      LogMsg("***** Error converting file to string (may contain binary data)");
      return -1;
   }

   // Parse
   CMarkup xml;
   BOOL bResult = xml.SetDoc( csText );

   acDocDate[0] = 0;
   while (xml.FindChildElem( _T("Transaction") ) )
   {
      Check(xml.IntoElem(), "Look into Transaction");
         
      sPageCnt = sDocNum = "";
      memset((void *)&acOutbuf, ' ', sizeof(GRGR_DEF));

      // Look into DocumentInformation
      if (xml.FindChildElem( _T("DocumentInformation") ))
      {
         Check(xml.IntoElem(), "Look into DocumentInformation");

         // Get DocDate
         if (xml.FindChildElem( _T("SourceDate") ))
            sDocDate = xml.GetChildData();
         // Get DocumentNumber
         if (xml.FindChildElem( _T("UniqueTransactionID") ))
            sDocNum = xml.GetChildData();

         // Look into DocumentType
         if (xml.FindChildElem(_T("DocumentTypes")) )
         {
            Check(xml.IntoElem(), "Look into DocumentTypes");

            if (xml.FindChildElem( _T("DocumentType") ) )
               sDocType = xml.GetChildData();
            if (xml.FindChildElem( _T("DocumentTypeDescription") ) )
               sDocTitle = xml.GetChildData();

            Check(xml.OutOfElem(), "Out of DocumentTypes");
         }

         // Page count
         if (xml.FindChildElem( _T("ImagePageCount")))
            sPageCnt = xml.GetChildData();

         Check(xml.OutOfElem(), "Out of DocumentInformation");
      }  // End DocumentInformation

      iApnIdx = 0;

      // Get index info
      while (xml.FindChildElem(_T("IndexInformation")) )
      {
         Check(xml.IntoElem(), "Look into IndexInformation");

         // Get APN
         if (iApnIdx < 1024)
         {
            if (xml.FindChildElem( _T("AssessorParcelNumber") ))
               sApn[iApnIdx] = xml.GetChildData();
            else
               sApn[iApnIdx] = "";

            if (*sApn[iApnIdx] < '0')
               iNoApn++;
            iApnIdx++;
         } else
            LogMsg("+++++ Too many APN, increase buffer size to avoid problem. DocNum=%s", sDocNum);

         // Get names
         iGrtor = iGrtee = 0;
         sGrtor[0] = sGrtor[1] = sGrtee[0] = sGrtee[1] = "";
         if (xml.FindChildElem( _T("IndexNames") ))
         {
            Check(xml.IntoElem(), "Look into IndexNames");

            while (xml.FindChildElem( _T("Name") ))
            {
               Check(xml.IntoElem(), "Look into Name");
               csNotes = "";
               if (xml.FindChildElem( _T("GrantorGranteeType") ))
                  csNotes = xml.GetChildData();
               if (csNotes == "E")           // Grantee
               {
                  if (xml.FindChildElem( _T("IndexNameLast") ))
                     csNotes = xml.GetChildData();

                  if (iGrtee < MAX_NAMES)
                     sGrtee[iGrtee] = csNotes;
                  iGrtee++;
               } else if (csNotes == "R")    // Grantor
               {
                  if (xml.FindChildElem( _T("IndexNameLast") ))
                     csNotes = xml.GetChildData();

                  if (iGrtor < MAX_NAMES)
                     sGrtor[iGrtor] = csNotes;
                  iGrtor++;
               }
               Check(xml.OutOfElem(), "Out of Name");
            }
            Check(xml.OutOfElem(), "Out of IndexNames");
         }

         // Get tax info
         if (xml.FindChildElem( _T("DocumentFees") ))
         {
            Check(xml.IntoElem(), "Look into DocumentFees");

            while (xml.FindChildElem( _T("Fee") ))
            {
               Check(xml.IntoElem(), "Look into Fee");

               if (xml.FindChildElem( _T("FeeType")))
               {
                  csNotes = xml.GetChildData();

                  if (csNotes == "Taxes")
                  {
                     // Tax Amount
                     if (xml.FindChildElem( _T("FeeAmount")))
                        sDocTax = xml.GetChildData();
                  }
               }         
               Check(xml.OutOfElem(), "Out of Fee");
            }
            Check(xml.OutOfElem(), "Out of DocumentFees");
         }

         if (xml.FindChildElem( _T("RecordingInformation") ))
         {
            Check(xml.IntoElem(), "Look into RecordingInformation");

            // DocNum
            if (xml.FindChildElem( _T("RecordingDocumentNumber")))
            {
               sDocNum = xml.GetChildData();
               sDocNum.Remove('-');
            }
            // DocDate
            if (xml.FindChildElem( _T("RecordingDate")))
               sDocDate = xml.GetChildData();
            // Fee Total
            if (xml.FindChildElem( _T("RecordingFeeTotal")))
               sFeeTotal = xml.GetChildData();

            Check(xml.OutOfElem(), "Out of RecordingInformation");
         }

         Check(xml.OutOfElem(), "Out of IndexInformation");
      }
      Check(xml.OutOfElem(), "Out of Transaction");

      // Create output record
      memcpy(pGrGr->DocNum, sDocNum.GetBuffer(0), sDocNum.GetLength());
      memcpy(pGrGr->DocDate, sDocDate.GetBuffer(0), sDocDate.GetLength());
      memcpy(pGrGr->DocTitle, sDocTitle.GetBuffer(0), sDocTitle.GetLength());
      memcpy(pGrGr->DocType, sDocType.GetBuffer(0), sDocType.GetLength());
      memcpy(pGrGr->Tax, sDocTax.GetBuffer(0), sDocTax.GetLength());
      double dTmp = atof(sDocTax);
      if (dTmp > 0.0)
      {
         iTmp = sprintf(acTmp, "%d", (ULONG)(dTmp * SALE_FACTOR));
         memcpy(pGrGr->SalePrice, acTmp, iTmp);
      }

      iTmp = sprintf(acTmp, "%d", iGrtee);
      memcpy(pGrGr->NameCnt, acTmp, iTmp);

      // Grantee
      for (int i = 0; i < iGrtee && i < MAX_NAMES; i++)
      {
         pGrGr->Grantees[i].NameType[0] = 'E';
         memcpy(pGrGr->Grantees[i].Name, sGrtee[i].GetBuffer(0), sGrtee[i].GetLength());
      }

      // Current owner - if it's a sale
      if (iGrtee > 0 && pGrGr->Tax[0] > '0')
         memcpy(pGrGr->CurOwner, sGrtee[0].GetBuffer(0), sGrtee[0].GetLength());

      // Grantor
      for (int i = 0; i < iGrtor && i < MAX_NAMES; i++)
      {
         pGrGr->Grantors[i].NameType[0] = 'E';
         memcpy(pGrGr->Grantors[i].Name, sGrtor[i].GetBuffer(0), sGrtor[i].GetLength());
      }

      if (iGrtee > MAX_NAMES)
         pGrGr->MoreName = 'Y';
      if (iApnIdx > 1)
      {
         iTmp = sApn[iApnIdx-1].GetLength();
         pGrGr->MultiApn = 'Y';
         if (iTmp != iGrGrApnLen || strchr(sApn[iApnIdx-1].GetBuffer(), ' '))
         {
            if (iApnIdx == 2)
            {
               pGrGr->MultiApn = 'N';
            }
         }

         iTmp = sprintf(acTmp, "%d", iApnIdx);
         memcpy(pGrGr->ParcelCount, acTmp, iTmp);
      }

      // Testing
      //memcpy(pGrGr->Legal, "Legal", 5);
      //memcpy(pGrGr->CurCareOf, "CareOf", 6);
      memcpy(pGrGr->CurDocNum, sDocNum.GetBuffer(0), sDocNum.GetLength());
      memcpy(pGrGr->CurDocDate, sDocDate.GetBuffer(0), sDocDate.GetLength());

      pGrGr->CRLF[0] = '\n';
      pGrGr->CRLF[1] = 0;

#ifdef _DEBUG
      //if (!memcmp(sDocNum.GetBuffer(0), "20210011587", 12))
      //   iTmp = 0;
#endif

      // Write to file - create record for every APN
      for (int i = 0; i < iApnIdx; i++)
      {
         iTmp = sApn[i].GetLength();
         memcpy(pGrGr->APN, sApn[i], iTmp);
         if (iTmp != iGrGrApnLen || strchr(sApn[i], ' '))
         {
            LogMsg("*** Bad APN: %s, DocNum: %s, DocDate: %s", sApn[i], sDocNum, sDocDate);
         } else
         {
            fputs(acOutbuf, fdOut);
            iRecCnt++;
         }
      }
   }

   LogMsg("Number of records output : %d", iRecCnt);
   LogMsg("                 w/o APN : %d\n", iNoApn);

   return iRecCnt;
}

int Sis_LoadGrGr(LPCSTR pCnty) 
{
   char     *pTmp, acTmp[256], acInFile[_MAX_PATH];
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   int      iCnt, iRet;
   long     lCnt, lHandle;
   struct   _finddata_t  sFileInfo;
   FILE     *fdOut;

   // Form GrGr input path
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
   } else
   {
      LogMsg("*** No new file avail for processing: %s", acGrGrIn);
      return 0;
   }

   // Create Output file - Sis_GrGr.dat
   sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "Dat");
   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      _findclose(lHandle);
      return -2;
   }

   // Prepare backup folder
   sprintf(acGrGrBak, acGrGrBakTmpl, pCnty, acToday);
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   iRet = iCnt = lCnt = 0;
   while (!iRet)
   {
      sprintf(acInFile, "%s\\%s", acGrGrIn, sFileInfo.name);

      // Parse input file
      iRet = Sis_LoadGrGrXml(acInFile, fdOut);
      if (iRet < 0)
         LogMsg("*** Skip %s", acInFile);
      else
         lCnt += iRet;

      // Move input file to backup folder
      sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
      rename(acInFile, acTmp);

      printf("\r%d - %d", iCnt, lCnt);

      iCnt++;
      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }


   // Close handle
   _findclose(lHandle);
   iRet = 0;

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsgD("Total processed records  : %u", lCnt);

   // Sort output
   if (lCnt > 0)
   {
      // Sort output file and dedup
      sprintf(acGrGrIn, acGrGrTmpl, pCnty, pCnty, "sls"); // Fre_GrGr.sls
      if (!_access(acGrGrIn, 0))
         sprintf(acInFile, "%s+%s", acGrGrOut, acGrGrIn);
      else
         strcpy(acInFile, acGrGrOut);

      // Sort by APN, RecDate, DocNum, Sale price
      // Dupout the first 44 bytes (changed on 7/14/2017)
      // S(17,12,C,A,37,8,C,A,1,12,C,A,125,1,C,D) F(TXT) OMIT(17,1,C,LT,"0",OR,17,1,C,GT,"9") DUPO(B2048,1,44)
      sprintf(acTmp, "S(%d,12,C,A,%d,8,C,A,%d,12,C,A,125,10,C,D) F(TXT) OMIT(17,1,C,LT,\"0\",OR,17,1,C,GT,\"9\") DUPO(B8192,1,44) ", 
         OFF_GD_APN, OFF_GD_DOCDATE, OFF_GD_DOCNUM);
      sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "srt"); // Sis_GrGr.srt
      lCnt = sortFile(acInFile, acGrGrOut, acTmp);
      if (lCnt > 0)
      {
         if (!_access(acGrGrIn, 0))
            DeleteFile(acGrGrIn);

         // Rename srt to SLS file
         iRet = rename(acGrGrOut, acGrGrIn);
         LogMsg("GRGR process completed: %d", lCnt);
      } else
      {
         iRet = -1;
         LogMsg("***** Error sorting output file in Fre_LoadGrGrCsv()");
      }
   } else
      iRet = 1;

   LogMsg("Total output records after dedup: %u", lCnt);
   printf("\nTotal output records after dedup: %u\n", lCnt);

   return iRet;
}

/******************************* Sis_MakeDocLink *******************************
 *
 * DocNum = yyyy9999999
 * Format DocLink
 *
 ******************************************************************************/

void Sis_MakeDocLink(char *pDocLink, char *pDoc, char *pDate)
{
   int   iDocNum;
   char  acTmp[256], acDocName[256], acDocNum[32];

   *pDocLink = 0;
   if (*pDoc > ' ' && *pDate > ' ')
   {
      iDocNum = atoin((char *)pDoc+4, 7);       
      sprintf(acDocNum, "%.7d", iDocNum);       // Reformat DocNum
      sprintf(acDocName, "%.4s\\%.4s\\%.4s%s", pDate, acDocNum, pDate, acDocNum);
      sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocName);

      try
      {
         if (!_access(acTmp, 0))
            strcpy(pDocLink, &acDocName[0]);
         else
            *pDocLink = 0;
      } catch (...)
      {
         LogMsg("*** Bad file name: %s", acTmp);
      }
   }
}

int Sis_ExtrMiscBldgs()
{
   return 0;
}

int Sis_ExtrMHAtt()
{
   return 0;
}

/*********************************** loadSis ********************************
 *
 * Options:
 *    -CSIS -L -Xl -Xa -Xs (load lien)
 *    -CSIS -U (load update)
 *
 ****************************************************************************/

int loadSis(int iSkip)
{
   int   iRet=0;
   char  acTmp[_MAX_PATH];

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;

   iApnLen = myCounty.iApnLen;
   iValChg = 0;

   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      // Load taxrollinfo.txt into SQL
      //iRet = MB_Load_TaxRollInfo(bTaxImport, iHdrRows);

      // Load Sis_Tax.txt
      iRet = MB_Load_TaxBase(bTaxImport, true, 2, iHdrRows);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet = MB_Load_TaxCodeMstr(bTaxImport, iHdrRows);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport, iHdrRows);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport, iHdrRows);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
      if (!iLoadFlag)
         return iRet;
   }

    // Load GrGr
   if (iLoadFlag & LOAD_GRGR)                      // -G
   {
      // Create Mon_GrGr.dat
      LogMsg0("Load %s GrGr file", myCounty.acCntyCode);
      iRet = Sis_LoadGrGr(myCounty.acCntyCode);
      if (!iRet)
         iLoadFlag |= MERG_GRGR;                   // Trigger merge grgr
   }

   // Setup legal file
   GetIniString("Data", "LglFile", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acLegalFile, acTmp, myCounty.acCntyCode, myCounty.acCntyCode);

   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      // Extract lien value
      //iRet = Sis_ExtrLien(myCounty.acCntyCode);
      iRet = MB_ExtrTR601(myCounty.acCntyCode);     
   }

   // Fix DocType if needed
   if (iLoadFlag & FIX_CSTYP)
   {
      iRet = FixDocType(acCSalFile, (IDX_TBL5 *)&SIS_DocCode[0], true /* remove rec w/o DocNum */);
      if (iRet)
         return iRet;
   }

   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      iRet = GetIniString(myCounty.acCntyCode, "UseRollChar", "Y", acTmp, _MAX_PATH, acIniFile);
      if (acTmp[0] == 'Y')
      {
         GetIniString(myCounty.acCntyCode, "RollChar", "", acRollChar, _MAX_PATH, acIniFile);
         // Extract characteristics
         if (!_access(acRollChar, 0))
         {
            strcpy(acCharFile, acRollChar);
            iRet = Sis_ExtrRollChar(acRollChar);
         } else
            LogMsg("***** File missing: %s.  Skip CHAR extract", acRollChar);
      } else if (!_access(acCharFile, 0))
      {
         iRet = Sis_ExtrChar();
      } else
      {
         // ... do this before 2024 LDR ...

         // Load Sis_MiscBldgs.csv
         iRet = Sis_ExtrMiscBldgs();

         // Load Sis_MHAtt.csv
         iRet = Sis_ExtrMHAtt();

      }

      if (iRet <= 0)
      {
         LogMsg("***** Error extracting CHAR data from %s", acCharFile);
         return -1;
      }
   }

   // Create/Update cum sale file
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      if (strstr(acSalesFile, ".csv"))
      {
         char delim = cDelim;
         int  skipquote = iSkipQuote;

         cDelim = ',';
         iSkipQuote = 1;
         LogMsg("Loading CSV sale file '%c'", cDelim);
         iRet = MB_CreateSCSale(MM_DD_YYYY_1, 0, 0, false, (IDX_TBL5 *)&SIS_DocCode[0]);
         cDelim = delim;
         iSkipQuote = skipquote;
      } else
      {
         LogMsg("Loading TXT sale file delimited by %c", cDelim);
         iRet = MB_CreateSCSale(YYYY_MM_DD, 0, 0, false, (IDX_TBL5 *)&SIS_DocCode[0]);
      }

      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {     
      if (iLoadFlag & LOAD_LIEN)                   // -L
      {
         // Create Lien file
         LogMsg0("Load %s Lien file", myCounty.acCntyCode);
         //iRet = CreateSisRoll(iSkip);
         //iRet = Sis_Load_LDR(iSkip);             // 2016
         iSkipQuote = 1;
         iRet = Sis_Load_LDR3(iSkip);              // 2017
      } else if (iLoadFlag & LOAD_UPDT)            // -U
      {
         LogMsg0("Load %s roll update file", myCounty.acCntyCode);
         if (strstr(acRollFile, ".csv"))
            iRet = Sis_Load_Roll2(iSkip);
         else
            iRet = Sis_Load_Roll1(iSkip);
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Sis_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   if (!iRet && (iLoadFlag & MERG_GRGR))           // -Mg
   {
      // Load special table for GrGr
      GetIniString(myCounty.acCntyCode, "GrGr_Xref", "", acTmp, _MAX_PATH, acIniFile);
      iNumDeeds = LoadXrefTable(acTmp, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

      iRet = MergeGrGrDef(myCounty.acCntyCode, true, 2);

      if (!iRet && !(iLoadFlag & LOAD_LIEN))
         iLoadFlag |= LOAD_UPDT;                   // Trigger build index
   }

   // Format DocLinks - Doclinks are concatenate fields separated by comma 
   if (!iRet && (iLoadFlag & (LOAD_UPDT|LOAD_LIEN|MERG_GRGR)) )
      iRet = updateDocLinks(Sis_MakeDocLink, myCounty.acCntyCode, iSkip, 0);

   return iRet;
}