LDR 2008
1) County is now using TR601 comma delimited format

Heating  Expr1
   111161
0  114960
01 628
02 96
03 30
04 9
05 13
06 9
07 8
08 6
09 7
1  25461
2  3707
7  149
8  369
99 1

Cooling  Expr1
   110781
0  144849
01 550
02 11
03 70
04 1
8  348
99 4

NumPools Expr1
   12
         1042131
0        142166
01       11866
02       181
03       1795
04       80910
1        2
99       539


LDR 2016
#define  L4_TAXYEAR                    0
#define  L4_ROLLCATEGORY               1
#define  L4_ASMT                       2
#define  L4_FEEPARCEL                  3
#define  L4_ASMTSTATUS                 4
#define  L4_TRA                        5
#define  L4_MAPCATEGORY                6
#define  L4_OWNER                      7
#define  L4_ASSESSEE                   8
#define  L4_MAILADDRESS1               9
#define  L4_MAILADDRESS2               10
#define  L4_MAILADDRESS3               11
#define  L4_MAILADDRESS4               12
#define  L4_ZIPMATCH                   13
#define  L4_BARCODEZIP                 14
#define  L4_SITUS1                     15
#define  L4_SITUS2                     16
#define  L4_PARCELDESCRIPTION          17
#define  L4_LANDVALUE                  18
#define  L4_FIXTURESVALUE              19
#define  L4_GROWING                    20
#define  L4_STRUCTUREVALUE             21
#define  L4_PPVALUE                    22
#define  L4_MHPPVALUE                  23
#define  L4_NETVALUE                   24
#define  L4_BILLEDMARKETLANDVALUE      25
#define  L4_BILLEDFIXEDIMPRVALUE       26
#define  L4_BILLEDGROWINGIMPRVALUE     27
#define  L4_BILLEDSTRUCTURALIMPRVALUE  28
#define  L4_BILLEDPERSONALPROPVALUE    29
#define  L4_BILLEDPERSONALPROPMHVALUE  30
#define  L4_BILLEDNETVALUE             31
#define  L4_EXEMPTIONCODE1             32
#define  L4_EXEMPTIONAMT1              33
#define  L4_EXEMPTIONCODE2             34
#define  L4_EXEMPTIONAMT2              35
#define  L4_EXEMPTIONCODE3             36
#define  L4_EXEMPTIONAMT3              37
#define  L4_LANDUSE1                   38
#define  L4_LANDSIZE                   39
#define  L4_ACRES                      40
#define  L4_BUILDINGTYPE               41
#define  L4_QUALITYCLASS               42
#define  L4_BUILDINGSIZE               43
#define  L4_YEARBUILT                  44
#define  L4_TOTALAREA                  45
#define  L4_BEDROOMS                   46
#define  L4_BATHS                      47
#define  L4_HALFBATHS                  48
#define  L4_TOTALROOMS                 49
#define  L4_GARAGE                     50
#define  L4_GARAGESIZE                 51
#define  L4_HEATING                    52
#define  L4_AC                         53
#define  L4_FIREPLACE                  54
#define  L4_POOLSPA                    55
#define  L4_STORIES                    56
#define  L4_UNITS                      57
