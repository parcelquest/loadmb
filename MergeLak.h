#ifndef  _MERGELAK_H_
#define  _MERGELAK_H_

// 05/09/2017
#define  LAK_CHAR_FEEPARCEL            0
#define  LAK_CHAR_POOLSPA              1
#define  LAK_CHAR_CATTYPE              2
#define  LAK_CHAR_QUALITYCLASS         3
#define  LAK_CHAR_YRBLT                4
#define  LAK_CHAR_BUILDINGSIZE         5
#define  LAK_CHAR_ATTACHGARAGESF       6
#define  LAK_CHAR_DETACHGARAGESF       7
#define  LAK_CHAR_CARPORTSF            8
#define  LAK_CHAR_HEATING              9
#define  LAK_CHAR_COOLINGCENTRALAC     10
#define  LAK_CHAR_COOLINGEVAPORATIVE   11
#define  LAK_CHAR_COOLINGROOMWALL      12
#define  LAK_CHAR_COOLINGWINDOW        13
#define  LAK_CHAR_STORIESCNT           14
#define  LAK_CHAR_UNITSCNT             15
#define  LAK_CHAR_TOTALROOMS           16
#define  LAK_CHAR_EFFYR                17
#define  LAK_CHAR_PATIOSF              18
#define  LAK_CHAR_BEDROOMS             19
#define  LAK_CHAR_BATHROOMS            20
#define  LAK_CHAR_HALFBATHS            21
#define  LAK_CHAR_FIREPLACE            22
#define  LAK_CHAR_ASMT                 23
#define  LAK_CHAR_BLDGSEQNUM           24
#define  LAK_CHAR_HASWELL              25
#define  LAK_CHAR_LOTSQFT              26

// 07/16/2021
static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "99", "Q", 2,     // Structure Heating Type
   "B",  "8", 1,     // Base Board
   "C",  "Z", 1,     // Central Forced
   "D",  "D", 1,     // Double wall
   "F",  "C", 1,     // Floor
   "M",  "X", 1,     // MS
   "O",  "X", 1,     // Other
   "P",  "X", 1,     // Personal
   "R",  "I", 1,     // Radiant Floor
   "WA", "D", 2,     // Wall
   "WO", "S", 2,     // Wood Stove
   "",   "",  0
};

static XLAT_CODE  asCooling[] =
{
   // Value, lookup code, value length
   "CA", "C", 2,               // Central air conditioning
   "EC", "E", 2,               // Evaporative cooler
   "WA", "W", 2,               // Wall or window mounted ac unit
   "",   "",  0
};

// This table is not being used.  It's defined for future use.
static XLAT_CODE  asParkType[] =
{
   // Value, lookup code, value length
   "C", " C", 1,               // CARPORT
   "G",  "Z", 1,               // Garage
   "N",  "H", 1,               // None
   "O",  "O", 1,               // Others
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "1",     "P", 1,              // Pool
   "2",     "C", 1,              // Multiple/Combo
   "F",     "F", 1,              // Fiberglass
   "G",     "G", 1,              // Gunite Pool
   "H",     "H", 1,              // Heated
   "M",     "C", 1,              // Multiple
   "Non-H", "P", 5,              // Non-Heated
   "None",  "N", 4,              // None
   "Pool/S","C", 6,              // Pool/Spa
   "W",     "C", 1,              // With Spa
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "1 M",         "L", 3,     // 1 Masonry
   "1 Stove & 1", "2", 3,     // 1 Stove & 1 Mas
   "1",           "1", 1,     // Fireplace
   "2 M",         "2", 3,     // 2 Masonry
   "2 W",         "2", 3,     // 2 Wood Stoves
   "2",           "2", 1,     // 2 fireplace
   "3 M",         "3", 3,     // 3 Masonry
   "G",           "G", 1,     // Gas stove
   "O",           "O", 1,     // Other
   "P",           "S", 1,     // Pellet Stove
   "W",           "W", 1,     // Wood Stove
   "Z",           "Z", 1,     // Zero Clear
   "",   "", 0
};

IDX_TBL5 LAK_DocCode[] =
{// DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN
   "01", "1 ", 'N', 2, 2,          // SALE (DTT)
   "02", "13", 'Y', 2, 2,          // TRANSFER (NO DTT)
   "03", "74", 'Y', 2, 2,          // NON-REAPPRAISABLE EVENT
   "04", "74", 'Y', 2, 2,          // VOLUNTARY CONVERSION
   "05", "74", 'Y', 2, 2,          // NEW MOBILE HOME
   "06", "74", 'Y', 2, 2,          // POSSESSORY INT NEW/RENEWAL
   "07", "74", 'Y', 2, 2,          // POSSESSORY INT LIEN UPDATE
   "08", "74", 'Y', 2, 2,          // CLERICAL FUNCTIONS
   "09", "19", 'Y', 2, 2,          // SPLIT/COMBO  (Same Owner)
   "10", "13", 'N', 2, 2,          // PARTIAL INTEREST SALE (DTT)
   "11", "13", 'Y', 2, 2,          // PARTIAL INT TRANSFER (NO DTT)
   "12", "74", 'Y', 2, 2,          // GOVERNMENT ENTITY/NO SUPL
   "13", "74", 'Y', 2, 2,          // NEW SINGLE FAMILY DWELLING
   "14", "74", 'Y', 2, 2,          // NEW GARAGE/CARPORT
   "15", "74", 'Y', 2, 2,          // NEW SWIMMING POOL/SPA
   "16", "74", 'Y', 2, 2,          // DWELLING ADD'N/CONVERSION
   "17", "74", 'Y', 2, 2,          // GAR ADD'N/CONV/PATIO/DECK
   "18", "74", 'Y', 2, 2,          // MISC ADD'N
   "19", "74", 'Y', 2, 2,          // NEW SINGLE FAMILY DWELLING
   "20", "74", 'Y', 2, 2,          // SPLIT/COMB (diff owners)
   "22", "74", 'Y', 2, 2,          // DEED
   "23", "74", 'Y', 2, 2,          // NEW MULTIPLE BLDG
   "24", "74", 'Y', 2, 2,          // ADD'N/CONV TO MULTIPLES
   "25", "74", 'Y', 2, 2,          // MISC MULTIPLES
   "33", "74", 'Y', 2, 2,          // NEW RURAL BLDG
   "34", "74", 'Y', 2, 2,          // RURAL BLDG ADD'N
   "35", "74", 'Y', 2, 2,          // MISC RURAL ADD'N, N.L.I.
   "36", "74", 'Y', 2, 2,          // VINEYARD ADD'N/DELETION
   "37", "74", 'Y', 2, 2,          // LAND IMPROVEMENTS
   "44", "74", 'Y', 2, 2,          // NEW INDUSTRIAL BLDG
   "45", "74", 'Y', 2, 2,          // ADD'N/CONV INDUSTRIAL
   "46", "74", 'Y', 2, 2,          // MISC INDUSTRIAL
   "53", "74", 'Y', 2, 2,          // NEW COMM'L BLDG
   "54", "74", 'Y', 2, 2,          // ADD'N/CONV COMM'L
   "55", "74", 'Y', 2, 2,          // MISC COMM'L
   "58", "74", 'Y', 2, 2,          // PROP 58-NON REAPPRAISAL
   "60", "74", 'Y', 2, 2,          // PROP 60
   "63", "74", 'Y', 2, 2,          // AG CONTRACT-NEW BLDG
   "64", "74", 'Y', 2, 2,          // AG CONTRACT-ADD'N/CONV
   "65", "74", 'Y', 2, 2,          // AG CONTRACT-MISC
   "66", "74", 'Y', 2, 2,          // AG CONTRACT- VINES INC/DEC
   "67", "74", 'Y', 2, 2,          // AG CONTRACT LAND IMPS
   "70", "74", 'Y', 2, 2,          // LIEN DATE UPDATE
   "71", "74", 'Y', 2, 2,          // PROP 8
   "72", "74", 'Y', 2, 2,          // CALAMITY-APPLICATION
   "73", "74", 'Y', 2, 2,          // CALAMITY-RESTORATION
   "75", "74", 'Y', 2, 2,          // SOLAR IMPROVEMENT
   "80", "74", 'Y', 2, 2,          // NON-TAXABLE GOVERNMENT PROP
   "81", "74", 'Y', 2, 2,          // TAXABLE GOVERNMENT LAND
   "86", "74", 'Y', 2, 2,          // NO CHANGE IN VALUE
   "91", "74", 'Y', 2, 2,          // PROP 58- DENIED
   "92", "74", 'Y', 2, 2,          // PROP 58 APPROVED
   "93", "74", 'Y', 2, 2,          // PROP 60 FORM REC'D
   "94", "74", 'Y', 2, 2,          // FIXTURES (SUPPL ONLY)
   "95", "74", 'Y', 2, 2,          // BUS PROP STATE REVIEW
   "96", "74", 'Y', 2, 2,          // DEMO/REMOVAL
   "DD", "74", 'Y', 2, 2,          // DATE OF DEATH
   "GA", "74", 'Y', 2, 2,          // GOVT ACQUISITION - TAXABLE TO NONTAXABLE
   "GP", "74", 'Y', 2, 2,          // SEC 63.1: GP TO GC APPROVED
   "NC", "74", 'Y', 2, 2,          // NEW CONSTRUCTION
   "TS", "67", 'Y', 2, 2,          // TAX SALE - NO SUPP
   "", "", '\0', 0, 0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 LAK_Exemption[] = 
{
   "E00", "R", 3, 1,    // RELIGIOUS EXEMPTION
   "E01", "H", 3, 1,    // HOMEOWNERS EXEMPTION
   "E02", "D", 3, 1,    // TOTALLY DISABLED VET
   "E03", "W", 3, 1,    // WELFARE EXEMPTION
   "E04", "V", 3, 1,    // VETERANS EXEMPTION
   "E05", "X", 3, 1,    // LESSOR, UNSECURED
   "E06", "I", 3, 1,    // PARTIAL HOSPITAL LAND
   "E07", "I", 3, 1,    // PARTIAL HOSPITAL STRUCTURE
   "E08", "I", 3, 1,    // PARTIAL HOSPITAL PP-FIX
   "E09", "I", 3, 1,    // HOSPITAL
   "E10", "D", 3, 1,    // DISABLED VET
   "E11", "D", 3, 1,    // DISABLED VET SPOUSE
   "E12", "V", 3, 1,    // VETERANS ORGANIZATION
   "E13", "X", 3, 1,    // LESSOR, SECURED
   "E14", "D", 3, 1,    // DELTA LOW INCOME TD-VET
   "E15", "D", 3, 1,    // TOTALLY DISABLED VET
   "E16", "D", 3, 1,    // TOTALLY DISABLED VET SPOUSE
   "E17", "D", 3, 1,    // TOTALLY DISABLED VET BASIC
   "E18", "D", 3, 1,    // TOTALLY DISABLED VET BASIC SPOUSE
   "E19", "R", 3, 1,    // RELIGIOUS EXEMPTION
   "E20", "R", 3, 1,    // RELIGIOUS EXEMPTION
   "E21", "R", 3, 1,    // PARTIAL RELIGIOUS LAND
   "E22", "R", 3, 1,    // PARTIAL RELIGIOUS STRUCTURE
   "E23", "R", 3, 1,    // PARTIAL RELIGIOUS PP-FIX
   "E30", "W", 3, 1,    // WELFARE EXEMPTION
   "E31", "W", 3, 1,    // PARTIAL WELFARE LAND
   "E32", "W", 3, 1,    // PARTIAL WELFARE STRUCTURE
   "E33", "W", 3, 1,    // PARTIAL WELFARE PP-FIX
   "E34", "S", 3, 1,    // WELFARE SCHOOL
   "E40", "X", 3, 1,    // LESSOR SECURED
   "E41", "X", 3, 1,    // PARTIAL LESSOR SECURED LAND
   "E42", "X", 3, 1,    // PARTIAL LESSOR SECURED STRUCTURE
   "E43", "X", 3, 1,    // PARTIAL LESSOR SECURED PP-FIX
   "E50", "X", 3, 1,    // LOW INCOME TRIBAL HOUSING
   "E60", "X", 3, 1,    // LOW VALUE BOAT EXEMPTION
   "E70", "C", 3, 1,    // CHURCH EXEMPTION
   "E80", "X", 3, 1,    // HISTORICAL AIRCRAFT
   "E98", "X", 3, 1,    // Other
   "","",0,0
};

#endif