/******************************************************************************
 *
 * Operation commands:
 *    - Normal update:  LoadOne -CMEN -U -Xsi -T
 *    - Load Lien:      LoadOne -CMEN -L
 *
 * Revision:
 * 04/27/2022 21.9.1    Move from LoadOne to process new CSV format
 * 05/06/2022           Both roll & tax data functions are added.
 * 08/26/2022 22.2.1    Add Men_MergeLOwner(), Men_MergeMailing(), Men_MergeSitus(), Men_CreateLienRecCsv().
 *                      Rewrite Men_MergeLienCsv() & Men_Load_LDR_Csv(), Men_ExtrLienCsv() to support new LDR file.
 * 11/03/2022 22.2.7    Modify all tax related functions to support new tax layouts and field formats.
 * 11/10/2022 22.2.9    Modify Men_ConvStdCharCsv() to support new CHAR layout.  Modify Men_MergeRollCsv() to
 *                      stop populate LOT_SQFT if LOT_ACRES > MAX_LOTACRES since SIZ_LOT_SQFT is only 9-digit long.
 * 11/27/2022 22.4.2.1  Fix bug due to county situs issue in Men_MergeSAdr().
 * 12/07/2022 22.5.0    Modify Men_CreateLienRecCsv(), Men_CreateRollCorrectionRec(), Men_MergeLienCsv(), Men_MergeRollCorrection(),
 *                      Men_MergeRollCsv() to update LivImpr.  Now we put LivImpr in OTH_IMPR in R01 record, not GR_IMPR or TREEVINES.
 * 01/16/2023 22.5.6    Modify Men_ExtrSaleCsv() to handle special cases of DOCNUM.  Rework on Men_MergeSAdr().
 * 02/03/2023 22.6.2    Fix Men_MergeSAdr() not to use Unit# from MailAdr.  Modify Men_Load_RollCsv() to rebuild
 *                      the AsmtRoll file before processing.
 * 02/05/2023 22.6.3    Modify Men_ExtrSaleCsv() to filter out some bad DocNum.
 * 02/11/2023 22.6.6    Modify Men_Load_TaxCurrent() to check for bad APN and DueDate.  Also remove check for UNICODE.
 * 05/25/2023 22.9.1    Modify Men_Load_TaxCurrent() to change date format from mm/dd/yyyy to dd/mm/yyyy.
 * 08/08/2023 23.1.6    Modify Men_Load_RollCsv() to set lRecCnt=iRollUpdt+iNewRec instead of lCnt (records processed).
 * 08/10/2023 23.1.7    Add Men_ExtrRollCsv(), Men_MergeLienCsv1(), Men_MergeMailing1(), Men_MergeSitus1() & Men_Load_LDR_Csv1()
 *                      to process new LDR file format.
 * 10/16/2023 23.3.1    Modify Men_CreateTaxItems() to copy TaxCode up tp 10 bytes.
 * 12/03/2023 23.4.4    Fix DueDate in Men_Load_TaxCurrent() to allow any valid date.
 * 01/10/2024 23.5.2    Suspense processing of tax delinquence until county makes DelqAmt available.
 * 06/09/2024 23.9.1    Modify Men_Load_TaxCurrent() to ignore DueDate2 if Tax2 is 0.
 * 10/02/2024 24.1.7    Add Men_Load_LDR_Csv2() & Men_ExtrRollCsv2() to support new LDR files.
 * 11/12/2024 24.3.3    Modify Men_MergeRollCsv() & Men_Load_RollCsv() to support new asmt roll layout.
 * 02/03/2025 24.5.2    Add Men_MergeMailing2(), Men_MergeSitus2(), Men_MergeLienCsv2().  
 *                      Modify Men_Load_LDR_Csv2() to remove lien extract for LDR values.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "hlAdo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "LoadMB.h"
#include "MergeMen.h"
#include "UseCode.h"
#include "MBExtrn.h"
#include "Tax.h"
#include "NdcExtr.h"
#include "CharRec.h"
#include "PQ.h"

static   FILE  *fdLien, *fdDelq, *fdChar;
static   long  lCharSkip, lCharMatch, lSaleSkip, lSaleMatch, lLienSkip, lLienMatch, lUnkMailAdr;
static   char  acUnsRoll[_MAX_PATH];

//static long     lCharSkip, lSaleSkip, lRollSkip;
//static long     lCharMatch, lSaleMatch, lMatchCChr, lRollMatch;

int Men_MergeStdChar(char *pBuf);
int Men_MergeRollCsv(char *pOutbuf, char *pRollRec, int iLen, int iFlag);

/******************************************************************************
 *
 * Remove all known bad chars in string
 *
 ******************************************************************************/

int Men_ReplBadChar(unsigned char *pBuf)
{
   int  iTmp, iRet=0;
   unsigned char *pTmp = pBuf;

   iTmp = 0;
   while (*pBuf && *pBuf != 10)  // scan till CR
   {
      if (*pBuf == 0xC2 || *pBuf == 0xE2)
      {
         iRet = iTmp;
      } else if (*pBuf == 0x80)
      {
         *pTmp++ = 0x27;
         iRet = iTmp;
      } else if (*pBuf == 0xB0)
      {
         *pTmp++ = '*';
         iRet = iTmp;
      } else
         *pTmp++ = *pBuf;

      pBuf++;
      iTmp++;
   }
   *pTmp = 0;

   return iRet;
}

/******************************* Men_ExtrSaleCsv *****************************
 *
 * Extract sale file CA-Mendocino-Sales.csv 07/15/2022
 *
 *****************************************************************************/

int Men_ExtrSaleCsv()
{
   char      acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acApn[32], acTmp[256], acRec[2048], *pRec;
   long      lCnt=0, lPrice, iTmp;
   FILE      *fdOut;
   SCSAL_REC SaleRec;

   LogMsg0("Loading Sale file");

   // Check for Unicode
   sprintf(acTmpFile, "%s\\MEN\\Sales.txt", acTmpPath);
   if (!UnicodeToAnsi(acSalesFile, acTmpFile))
      strcpy(acSalesFile, acTmpFile);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return 2;
   }

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      pRec = fgets(acRec, 2048, fdSale);
      if (!pRec)
         break;

      // Parse input rec
      iTokens = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

      if (iTokens < S_COLS)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[S_CONVEYANCENUMBER] == ' ' || *apTokens[S_CONVEYANCEDATE] == ' ')
         continue;
      if (*apTokens[S_CONVEYANCENUMBER] > '9' || *apTokens[S_CONVEYANCENUMBER] < '2')
         continue;
      if (*apTokens[S_PIN] > '9')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      iTmp = iTrim(apTokens[S_PIN]);
      sprintf(acApn, "%.*s%s", iApnLen-iTmp, "000000", apTokens[S_PIN]);
      if (iTmp < iApnLen)
      {
         LogMsg("*** Verify APN: %s", apTokens[S_PIN]);
         memcpy(SaleRec.Apn, acApn, iApnLen);
      } else
         memcpy(SaleRec.Apn, apTokens[S_PIN], iApnLen);

      // Doc date
      pRec = dateConversion(apTokens[S_CONVEYANCEDATE], acTmp, YYYY_MM_DD);
      if (pRec)
      {
         memcpy(SaleRec.DocDate, pRec, 8);
         iTmp = atol(acTmp);
         if (iTmp > lLastRecDate)
            lLastRecDate = iTmp;
      } else
      {
         LogMsg("*** Bad sale date: %s [%s]", apTokens[S_CONVEYANCEDATE], apTokens[S_PIN]);
         continue;
      }

      // Verify DocNum
      if (memcmp(apTokens[S_CONVEYANCENUMBER], acTmp, 4))
      {
         LogMsg("*** Bad DocNum: %s, DocDate: %s APN:%s", apTokens[S_CONVEYANCENUMBER], acTmp, apTokens[S_PIN]);
         continue;
      }

      // Docnum
      if (*(apTokens[S_CONVEYANCENUMBER]+4) == '-' && *(apTokens[S_CONVEYANCENUMBER]+9) == '-')
         vmemcpy(SaleRec.DocNum, apTokens[S_CONVEYANCENUMBER]+10, 5);
      else if (*(apTokens[S_CONVEYANCENUMBER]+4) == '-' && isdigit(*(apTokens[S_CONVEYANCENUMBER]+5)))
         vmemcpy(SaleRec.DocNum, apTokens[S_CONVEYANCENUMBER]+5, 5);
      else if (*(apTokens[S_CONVEYANCENUMBER]+5) == '-' && isdigit(*(apTokens[S_CONVEYANCENUMBER]+6)))
         vmemcpy(SaleRec.DocNum, apTokens[S_CONVEYANCENUMBER]+6, 5);
      else if (*(apTokens[S_CONVEYANCENUMBER]+3) == '-')
         vmemcpy(SaleRec.DocNum, apTokens[S_CONVEYANCENUMBER]+4, 5);
      else
      {
         LogMsg("*** Bad docnum: %s [%s]", apTokens[S_CONVEYANCENUMBER], apTokens[S_PIN]);
         continue;
      }

      // Sale price
      lPrice = atol(apTokens[S_INDICATEDSALEPRICE]);
      if (!lPrice)
         lPrice = atol(apTokens[S_ADJUSTEDSALEPRICE]);

      if (lPrice > 0)
      {
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(SaleRec.SalePrice, acTmp, SIZ_SALE1_AMT);
      }

      // Transfer Type
      int iIdx = XrefCodeIndex((XREFTBL *)&asDeed[0], apTokens[S_TRANSFERTYPECODE], iNumDeeds);
      if (iIdx >= 0)
      {
         if (lPrice > 1000)
         {
            // Set fractional interest
            if (iIdx == 57)
               SaleRec.SaleCode[0] = 'P';
         } else if (asDeed[iIdx].acFlags[0] == 'Y')
            SaleRec.NoneSale_Flg = 'Y';
         iTmp = sprintf(acTmp, "%d", asDeed[iIdx].iIdxNum);
         memcpy(SaleRec.DocType, acTmp, iTmp);
         memcpy(SaleRec.DocCode, apTokens[S_TRANSFERTYPECODE], 3);
      } else
         LogMsg("*** Unknown TransferTypeCode: '%s' APN=%s", apTokens[S_TRANSFERTYPECODE], apTokens[S_PIN]);

      // Group sale?

      // Seller

      // Buyer

      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   LogMsg("Total processed records: %u", lCnt);
   LogMsg("    Last recording date: %u\n", lLastRecDate);

   // Update cumulative sale file
   if (lCnt > 0)
   {
      // Append and resort SLS file
      // Sort on APN asc, DocDate asc, DocNum asc
      sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A) F(TXT) DUPO(1,34) ");
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");     
      if (!_access(acCSalFile, 0))
         sprintf(acRec, "%s+%s", acTmpFile, acCSalFile);
      else
         strcpy(acRec, acTmpFile);
      lCnt = sortFile(acRec, acOutFile, acTmp);
      if (lCnt > 0)
      {
         // Rename old cum sale
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         iTmp = 0;
         if (!_access(acTmpFile, 0))
            iTmp = remove(acTmpFile);
         if (!_access(acCSalFile, 0))
            iTmp = rename(acCSalFile, acTmpFile);
         if (!iTmp)
            iTmp = rename(acOutFile, acCSalFile);
         else
            LogMsg("***** Error removing %s.  Please rerun with -Xs option", acCSalFile);
      }
   } else
      iTmp = -1;

   LogMsg("Total Sale records output: %d.\n", lCnt);
   return iTmp;
}

/******************************* Men_AddUnits ********************************
 *
 * Use Men_Units.txt to populate number of units.  This file is generated by 
 * importing the PropertyCharacteristics.csv into SQL then select those APN
 * with multiple count.
 * select pin, count(*) as c from men_propchar group by pin
 * having count(*) > 1 order by pin
 *
 *****************************************************************************/

int Men_AddUnits(char *pOutbuf, FILE **fdUnit)
{
   static   char acRec[512], *apItems[8], *pRec=NULL;
   static   int iItems=0;

   int      iLoop;
   STDCHAR  *pStdChar = (STDCHAR *)pOutbuf;

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 512, *fdUnit);
      iItems = ParseString(acRec, '|', 3, apItems);
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip rec  %s", pRec);
         pRec = fgets(acRec, 512, *fdUnit);
         if (!pRec)
         {
            fclose(*fdUnit);
            *fdUnit = NULL;
            return -1;      // EOF
         } else
            iItems = ParseString(acRec, '|', 3, apItems);
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutBase->Assmnt_No, "44257021", 8))
   //   iTmp = 0;
#endif
   if (iItems > 1)
      memcpy(pStdChar->Units, apItems[1], strlen(apItems[1]));
   
   return 0;
}

/***************************** Men_ConvStdCharCsv ****************************
 *
 * For data version dated 11/09/2022 and beyond, county adds ObjID to the beginning
 * of the record and some fields are "NULL" instead of blank or empty.
 *
 * Convert CA-Mendo-PropChar.csv to STD_CHAR format
 *
 *****************************************************************************/

int Men_ConvStdCharCsv(char *pCharfile)
{
   char     acInbuf[1024], acOutbuf[2048], *pRec, acTmpFile[256], acTmp[256], cTmp1;
   double   dTmp;
   int      iRet, iTmp, iBeds, iFBath, iHBath, iBath3Q, iBath4Q, iCnt, iExt,
            iYrBlt, iYrEff;
   ULONG    ulSqft;

   STDCHAR  *pStdChar = (STDCHAR *)acOutbuf;
   FILE     *fdChar, *fdOut, *fdUnit;

   LogMsg0("Converting standard char file.");

   GetIniString(myCounty.acCntyCode, "UnitFile", "", acTmpFile, _MAX_PATH, acIniFile);
   fdUnit = (FILE *)NULL;
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Unit file %s", acTmpFile);
      if (!(fdUnit = fopen(acTmpFile, "r")))
         LogMsg("*** Error opening Unit file %s.  Ignore updating number of Units", acTmpFile);
   }

   // Check for Unicode
   sprintf(acTmpFile, "%s\\MEN\\PropChar.txt", acTmpPath);
   if (!UnicodeToAnsi(pCharfile, acTmpFile))
      strcpy(pCharfile, acTmpFile);
   LogMsg("Open char file %s", pCharfile);
   if (!(fdChar = fopen(pCharfile, "r")))
   {
      LogMsg("***** Error opening input file %s", pCharfile);
      return -1;
   }

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdChar);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Add extension to field index sice county added ObjID to position 0
   iExt = 1;
   iCnt = 0;
   while (!feof(fdChar))
   {
      pRec = fgets(acInbuf, 1024, fdChar);
      if (!pRec) break;

      if (*pRec > '9')
         continue;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "009619939", 9))
      //   iTokens = 0;
#endif

      // Remove all "N/A"
      replStrAll(acInbuf, "N/A", "");
      replStrAll(acInbuf, "NULL", "");

      iTokens = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < C_COLS)
      {
         if (iTokens > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iTokens);
         continue;
      } 
      memset(acOutbuf, ' ', sizeof(STDCHAR));

      // Format APN
      vmemcpy(pStdChar->Apn, apTokens[C_APN+iExt], SIZ_CHAR_APN);
      iRet = formatApn(apTokens[C_APN+iExt], acTmp, &myCounty);
      memcpy(pStdChar->Apn_D, acTmp, iRet);

      // Yrblt
      iYrBlt = atol(apTokens[C_YEARBUILT+iExt]);
      if (iYrBlt > 1700)
         memcpy(pStdChar->YrBlt, apTokens[C_YEARBUILT+iExt], SIZ_YR_BLT);
      iYrEff = atol(apTokens[C_EFFYEARBUILT+iExt]);
      if (iYrEff > 1900)
      {
         memcpy(pStdChar->YrEff, apTokens[C_EFFYEARBUILT+iExt], SIZ_YR_BLT);
         if (!iYrBlt)
            memcpy(pStdChar->YrBlt, pStdChar->YrEff, SIZ_YR_BLT);
      }

      // Stories
      iTmp = atol(apTokens[C_NUMBEROFSTORIES+iExt]);
      if (iTmp > 0 && iTmp < 100)
      {
         if (bDebug && iTmp > 9)
            LogMsg("*** Please verify number of stories for %s (%d)", apTokens[C_APN+iExt], iTmp);

         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(pStdChar->Stories, acTmp, iRet);
      }

      // Garage Sqft
      // Property may have more than one garage, but we only have space for one
      long lGarSqft = atol(apTokens[C_GARAGEAREA+iExt]) + atol(apTokens[C_GARAGE2AREA+iExt]);
      long lCarpSqft= atol(apTokens[C_CARPORTSIZE+iExt]) + atol(apTokens[C_CARPORT2SIZE+iExt]);

      // Garage type
      if (*apTokens[C_GARAGETYPE+iExt] == 'A' || *apTokens[C_GARAGE2TYPE+iExt] == 'A')
         pStdChar->ParkType[0] = 'I';              // Attached garage
      else if (*apTokens[C_GARAGETYPE+iExt] == 'D')
         pStdChar->ParkType[0] = 'L';              // Detached garage
      else if (lCarpSqft > 100)
         pStdChar->ParkType[0] = 'C';              // Carport

      if (lGarSqft > 100 || lCarpSqft > 100)
      {
         if (lGarSqft >= 100 && lCarpSqft > 100)
         {
            lGarSqft += lCarpSqft;
            pStdChar->ParkType[0] = '2';           // Garage/Carport
         }
         iTmp = sprintf(acTmp, "%d", lGarSqft);
         memcpy(pStdChar->GarSqft, acTmp, iTmp);
      }

      // Central Heating-Cooling
      if (*apTokens[C_HASCENTRALHEATING+iExt] == 'Y')
         pStdChar->Heating[0] = 'Z';
      if (*apTokens[C_HASCENTRALCOOLING+iExt] == 'Y')
         pStdChar->Cooling[0] = 'C';

      // Beds
      iBeds = atol(apTokens[C_BEDROOMCOUNT+iExt]);
      if (iBeds > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBeds);
         memcpy(pStdChar->Beds, acTmp, iTmp);
      }

#ifdef _DEBUG
      //if (!memcmp(pStdChar->Apn, "009600002", 9) )
      //   lTmp = 0;
#endif

      // Bath
      iBath4Q = atol(apTokens[C_BATHSFULL+iExt]);
      iHBath  = atol(apTokens[C_BATHSHALF+iExt]);
      iBath3Q = atol(apTokens[C_BATHS3_4+iExt]);
      iFBath = iBath3Q+iBath4Q;
      if (iBath3Q > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBath3Q);
         memcpy(pStdChar->Bath_3QX, acTmp, iTmp);
         if (iTmp < 3)
            memcpy(pStdChar->Bath_3Q, acTmp, iTmp);
      }

      if (iBath4Q > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBath4Q);
         memcpy(pStdChar->Bath_4QX, acTmp, iTmp);
         if (iTmp < 3)
            memcpy(pStdChar->Bath_4Q, acTmp, iTmp);
      }

      if (iFBath > 0 && iFBath < 100)
      {
         iTmp = sprintf(acTmp, "%d", iFBath);
         memcpy(pStdChar->FBaths, acTmp, iTmp);
      }

      if (iHBath > 0)
      {
         iTmp = sprintf(acTmp, "%d", iHBath);
         memcpy(pStdChar->Bath_2QX, acTmp, iTmp);
         if (iTmp < 3)
         {
            memcpy(pStdChar->HBaths, acTmp, iTmp);
            memcpy(pStdChar->Bath_2Q, acTmp, iTmp);
         }
      }

      // Fireplace Y/N
      if (*apTokens[C_HASFIREPLACE+iExt] > ' ')
         pStdChar->Fireplace[0] = *apTokens[C_HASFIREPLACE+iExt];

      // Roof
      if (*apTokens[C_ROOFTYPE+iExt] >= 'A')
      {
         _strupr(apTokens[C_ROOFTYPE+iExt]);
         pRec = findXlatCodeA(apTokens[C_ROOFTYPE+iExt], &asRoofType[0]);
         if (pRec)
            pStdChar->RoofType[0] = *pRec;
      }

      // Pool/Spa Y/N
      if (*apTokens[C_HASPOOL+iExt] == 'Y')
         pStdChar->Pool[0] = 'P';       // Pool

      // Electric
      if (*apTokens[C_UTILITYELECTRIC+iExt] >= 'A')
      {
         switch (*apTokens[C_UTILITYELECTRIC+iExt])
         {
            case 'A':
            case 'D':
               cTmp1 = *apTokens[C_UTILITYELECTRIC+iExt];  // AVAIL
               break;
            case 'N':
               cTmp1 = 'N';                           // NON AVAIL
               break;
            default:
               if (!_memicmp(apTokens[C_UTILITYELECTRIC+iExt], "Under", 5))
                  cTmp1 = 'D';
               else
                  cTmp1 = ' ';
         }
         pStdChar->HasElectric = cTmp1;
      }

      // Gas
      if (*apTokens[C_UTILITYGAS+iExt] >= 'A')
      {
         switch (*apTokens[C_UTILITYGAS+iExt])
         {
            case 'A':
            case 'D':
               cTmp1 = *apTokens[C_UTILITYGAS+iExt];    // AVAIL
               break;
            case 'N':
               cTmp1 = 'N';                        // NON AVAIL
               break;
            default:
               cTmp1 = ' ';
         }
         pStdChar->HasGas = cTmp1;
      }

      // Water - currently not avail. 5/27/2022
      if (*apTokens[C_DOMESTICWATER+iExt] == 'A' || *apTokens[C_DOMESTICWATER+iExt] == 'D')
      {
         pStdChar->Water = *apTokens[C_DOMESTICWATER+iExt];
         pStdChar->HasWater = 'Y';
      } else if (*apTokens[C_WELLWATER+iExt] == 'A' || *apTokens[C_WELLWATER+iExt] == 'D')
      {
         pStdChar->Water = *apTokens[C_WELLWATER+iExt];
         pStdChar->HasWater = 'W';
         pStdChar->HasWell = 'Y';
      } else if (*apTokens[C_IRRIGATIONWATER+iExt] == 'A' || *apTokens[C_IRRIGATIONWATER+iExt] == 'D')
      {
         pStdChar->HasWater = 'L';
         pStdChar->Water = *apTokens[C_IRRIGATIONWATER+iExt];
      } else if (*apTokens[C_DOMESTICWATER+iExt] == 'N' || *apTokens[C_WELLWATER+iExt] == 'N' || *apTokens[C_IRRIGATIONWATER+iExt] == 'N')
      {
         pStdChar->Water = 'N';
         pStdChar->HasWater = 'N';
      }

      // Sewer
      if (*apTokens[C_SEWER+iExt] > ' ')
      {
         pStdChar->Sewer = *apTokens[C_SEWER+iExt];
         if (*apTokens[C_SEWER+iExt] == 'A' || *apTokens[C_SEWER+iExt] == 'D')
            pStdChar->HasSewer = 'Y';
      }

      // View
      if (*apTokens[C_FAIRWAY+iExt] == 'Y')
         pStdChar->View[0] = 'H';   
      else if (*apTokens[C_WATERFRONT+iExt] == 'Y')
         pStdChar->View[0] = 'W';   

      // BldgSqft
      ulSqft = atol(apTokens[C_LIVINGAREA+iExt]);
      if (!ulSqft)
         ulSqft = atol(apTokens[C_ACTUALAREA+iExt]);
      if (ulSqft > 0)
      {
         iTmp = sprintf(acTmp, "%d", ulSqft);
         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
      }

      // Construction type
      if (*apTokens[C_CONSTRUCTIONTYPE+iExt] >= 'A')
      {
         _strupr(apTokens[C_CONSTRUCTIONTYPE+iExt]);
         pRec = findXlatCodeA(apTokens[C_CONSTRUCTIONTYPE+iExt], &asConstType[0]);
         if (pRec)
            pStdChar->ConstType[0] = *pRec;
      }

      // BldgClass 
      // A (Fireproof Steel Frame)
      // B (Reinf. Conc. Frame)
      // C (Masonry Bearing Walls)
      // D (Stud Frame/Walls)
      // S (Metal Frame & Walls)
      if (*apTokens[C_CONSTRUCTIONTYPE+iExt] == 'W')
         pStdChar->BldgClass = 'D';
      else if (*apTokens[C_CONSTRUCTIONTYPE+iExt] == 'M')
         pStdChar->BldgClass = 'C';
      else if (!memcmp(apTokens[C_CONSTRUCTIONTYPE+iExt], "POLE", 2))
         pStdChar->BldgClass = 'P';
      else if (!memcmp(apTokens[C_CONSTRUCTIONTYPE+iExt], "PR", 2))
         pStdChar->BldgClass = 'S';

      // Quality
      if (!_memicmp(apTokens[C_QUALITYCODE+iExt], "Rank", 4))
         dTmp = atof(apTokens[C_QUALITYCODE+iExt]+5);
      else
         dTmp = atof(apTokens[C_QUALITYCODE+iExt]);
      if (dTmp > 0.1)
      {
         char acCode[16];

         sprintf(acTmp, "%.1f", dTmp);
         iTmp = Value2Code(acTmp, acCode, NULL);
         if (acCode[0] > ' ')
            pStdChar->BldgQual = acCode[0];
      }

      // QualityClass
      if (pStdChar->BldgClass > ' ' && dTmp > 0.1)
      {
         sprintf(acTmp, "%c%.1f%c", pStdChar->BldgClass, dTmp, *apTokens[C_SHAPECODE+iExt]);
         vmemcpy(pStdChar->QualityClass, acTmp, SIZ_CHAR_QCLS, strlen(acTmp));
      }

      // Lot sqft - Lot Acres
      dTmp = atof(apTokens[C_ACREAGE+iExt]);
      if (dTmp > 0.001)
      {
         iTmp = sprintf(acTmp, "%d", (long)(dTmp*1000));
         memcpy(pStdChar->LotAcre, acTmp, iTmp);

         // 
         if (dTmp < MAX_LOTACRES)
         {
            ulSqft = ULONG(dTmp*SQFT_PER_ACRE);

            iTmp = sprintf(acTmp, "%d", ulSqft);
            memcpy(pStdChar->LotSqft, acTmp, iTmp);
         }
      }

      // Add #Units
      if (fdUnit)
      {
         Men_AddUnits(acOutbuf, &fdUnit);
      }

      pStdChar->CRLF[0] = '\n';
      pStdChar->CRLF[1] = '\0';
      fputs(acOutbuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdChar) fclose(fdChar);
   if (fdOut)  fclose(fdOut);
   if (fdUnit) fclose(fdUnit);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      // Rename old CHAR file if needed
      if (!_access(acCChrFile, 0))
      {
         strcpy(acInbuf, acCChrFile);
         replStr(acInbuf, ".dat", ".sav");
         if (!_access(acInbuf, 0))
         {
            LogMsg("Delete old %s", acInbuf);
            DeleteFile(acInbuf);
         }
         LogMsg("Rename %s to %s", acCChrFile, acInbuf);
         RenameToExt(acCChrFile, "sav");
      }

      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Sort on APN, YrBlt to keep the latest record on top
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,14,C,A,53,4,C,D) F(TXT) ");
   } else
   {
      printf("\n");
      iRet = 0;
   }

   return iRet;
}

/******************************** Men_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Men_MergeOwner(char *pOutbuf, char *pOwner, char *pOwnerX)
{
   int   iTmp;
   char  acTmp[256], acName1[256], *pTmp;
   bool  bAppend=false;

   OWNER    myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1670301800", 10))
   //   iTmp = 0;
#endif

   // Save name1
   _strupr(pOwner);
   _strupr(pOwnerX);
   if (*pOwnerX > ' ')
   {
      if (strstr(pOwnerX, " ETUX ") || strstr(pOwnerX, " JR ") || 
         strstr(pOwnerX, " III ") || strchr(pOwnerX, '/') )
      {
         vmemcpy(pOutbuf+OFF_NAME_SWAP, pOwner, SIZ_NAME_SWAP);
         vmemcpy(pOutbuf+OFF_NAME1, pOwner, SIZ_NAME1);
         return;
      } else
         strcpy(acName1, pOwnerX);
   } else
      strcpy(acName1, pOwner);
   
   iTmp = iTrim(acName1);
   if (acName1[iTmp-1] == '/')
   {
      if (acName1[iTmp-2] < 'A')
         acName1[iTmp-2] = 0;
      else
         acName1[iTmp-1] = 0;
   }

   // Update Owner1
   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);

   // Remove number in name1
   // FREY LUKE 14.29% 
   pTmp = acName1;
   while (*pTmp)
   {
      // Break where name has numeric value
      if (isdigit(*pTmp))
         break;

      if (*pTmp == '.' || *pTmp == '(' || *pTmp == ')')
         *pTmp = ' ';
      pTmp++;
   }
   *pTmp = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Check for MD, DDS, and DVM
   if (pTmp=strstr(acName1, " MD "))
      *pTmp = 0;
   else if ((pTmp=strstr(acName1, " DDS ")) || (pTmp=strstr(acName1, " DVA ")) || (pTmp=strstr(acName1, " DVM ")) )
      *pTmp = 0;

   // Save Name1
   strcpy(acTmp, acName1);

   // Terminate name
   iTmp = strlen(acTmp);
   if (acTmp[iTmp-1] == '-')
      acTmp[iTmp-1] = 0;

   if ((pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, "CO-TTEE")) || (pTmp=strstr(acTmp, " TTEE"))     ||
       (pTmp=strstr(acTmp, "TTEES"))   || (pTmp=strstr(acTmp, " TRUSTEE")) )
      *pTmp = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00108234", 8) )
   //   iTmp = 0;
#endif

   if ((pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " LIFE EST")) ||
       (pTmp=strstr(acTmp, " ESTATE OF")) )
      *pTmp = 0;

   // Now parse owners
   iTmp = splitOwner(acTmp, &myOwner, 3);

   if (iTmp >= 0)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP);
}

/********************************* Men_MergeSAdr *****************************
 *
 * 08/13/2021 New layout
 *
 *****************************************************************************/

void Men_MergeSAdr(char *pOutbuf, char *pSAddr1, char *pCity, char *pZip)
{
   char     *pTmp, *pAddr1, acTmp[256], acUnit[8], 
            acCode[16], acAddr1[64], acAddr2[64];
   int      iTmp;
   ADR_REC  sSitusAdr;

   // Clear old Mailing
   removeSitus(pOutbuf);

   // Check for blank address
   if (!memcmp(pSAddr1, "     ", 5))
      return;

   strcpy(acAddr1, pSAddr1);
   blankRem(acAddr1);
   pAddr1 = acAddr1;

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   // Situs
   if (sSitusAdr.lStrNum > 0 && sSitusAdr.strName[0] >= ' ')
   {
      int   iIdx=0;

      removeSitus(pOutbuf);

      vmemcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, SIZ_S_STRNUM);
      vmemcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.HseNo, SIZ_S_HSENO);
      vmemcpy(pOutbuf+OFF_S_STR_SUB, sSitusAdr.strSub, SIZ_S_STR_SUB);
      strcpy(acTmp, sSitusAdr.strName);
      if (pTmp = strchr(acTmp, '*'))
         *pTmp = ' ';
      if (pTmp = strchr(acTmp, '('))
      {
         *pTmp++ = 0;
         if (!memcmp(pTmp, "REAR", 4))
            strcpy(acUnit, "REAR");
      }

      // Prepare display field
      vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

      if (sSitusAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, SIZ_S_STREET);
      if (sSitusAdr.strDir[0] > ' ' && isDir(sSitusAdr.strDir))
         vmemcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, SIZ_S_DIR);
      if (sSitusAdr.SfxCode[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, SIZ_S_SUFF);
      if (sSitusAdr.Unit[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
      
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "048523120", 9))
      //   iTmp = 0;
#endif

      // Situs city
      acCode[0] = acAddr2[0] = 0;
      if (*pCity >= 'A')
      {
         strcpy(acAddr2, pCity);
         City2CodeEx(acAddr2, acCode, sSitusAdr.City, pOutbuf);
         if (acCode[0] > ' ')
         {
            strcpy(acAddr2, sSitusAdr.City);
            memcpy(pOutbuf+OFF_S_CITY, acCode, 3);
            // Turn ON this 2 lines when zipcode is in Iny_City.N2CX
            //if (City2Zip(acAddr2, acTmp))
            //   memcpy(pOutbuf+OFF_S_ZIP, acTmp, 5);
         }
      }

      if (*pZip > ' ')
         vmemcpy(pOutbuf+OFF_S_ZIP, pZip, SIZ_S_ZIP);

      memcpy(pOutbuf+OFF_S_ST, "CA", 2);     
      if (sSitusAdr.City[0] > ' ')
      {
         sprintf(acAddr2, "%s CA %s", sSitusAdr.City, pZip);
         iTmp = blankRem(acAddr2);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
      }
   }
}

/********************************* Men_MergeMAdr *****************************
 *
 * 08/13/2021 New layout
 *
 *****************************************************************************/

void Men_MergeMAdr(char *pOutbuf, char *pMAddr1, char *pMAddr2)
{
   char     *pTmp, *pAddr1, acTmp[256], acAddr1[256], acAddr2[256];
   int      iTmp;

   // Clear old Mailing
   removeMailing(pOutbuf);

   // Check for blank address
   if (!memcmp(pMAddr1, "     ", 5))
      return;

   strcpy(acAddr1, pMAddr1);
   strcpy(acAddr2, pMAddr2);
   blankRem(acAddr1);
   blankRem(acAddr2);
   pAddr1 = acAddr1;

   iTmp = 0;
   if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
   {
      if (*pAddr1 == '%')
         pAddr1 += 2;
      else
         pAddr1 += 4;

      // Check for C/O name
      pTmp = strchr(pAddr1, ',');
      if (pTmp && !isdigit(*(pTmp-1)) && *(pOutbuf+OFF_CARE_OF) == ' ')
      {
         *pTmp = 0;
         pAddr1 = pTmp + 1;
         if (*pAddr1 == ' ')
            pAddr1++;
         
         updateCareOf(pOutbuf, pAddr1, strlen(pAddr1));
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0263400200", 10))
   //   iTmp = 0;
#endif
   // Start processing
   vmemcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, SIZ_M_ADDR_D);

   // Parsing mail address
   ADR_REC sMailAdr;
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sMailAdr, pAddr1);

   if (sMailAdr.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);

      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }

      if (sMailAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);
   parseAdr2(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] >= 'A')
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
   if (sMailAdr.State[0] >= 'A' && sMailAdr.State[1] >= 'A')
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
   if (sMailAdr.Zip[0] >= '0')
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
}


/********************************* Men_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Men_ExtrLien()
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   long     lCnt, lRead;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Merge loop
   lCnt=lRead=0;
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new lien record
      if (acRollRec[0] != 'R')
      {      
         //Men_CreateLienRec(acBuf, acRollRec);
         fputs(acBuf, fdLien);
         lCnt++;
      }
      if (!(++lRead % 1000))
         printf("\r%u", lRead);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total records processed:    %d", lRead);
   LogMsg("         records output:    %d\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/*************************** Men_ExtrRollCorrection *************************
 *
 * Extract values from roll correction file and replace the Lien_Exp.MEN
 *
 ****************************************************************************/

int Men_CreateRollCorrectionRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp;
   int      iRet, iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Parse input string
   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < MEN_COR_FLDS)
   {
      LogMsg("***** Men_CreateRollCorrectionRec: bad input record for APN=%s", apTokens[0]);
      return -1;
   }

   // Start copying data
   vmemcpy(pLienRec->acApn, apTokens[MEN_COR_AIN], iApnLen);

   // TRA
   lTmp = atol(apTokens[MEN_COR_TAG]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   vmemcpy(pLienRec->acYear, apTokens[MEN_COR_TAXYEAR], 4);

   // Land
   long lLand = dollar2Num(apTokens[MEN_COR_ASSDLAND]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[MEN_COR_ASSDIMP]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   long lPers   = dollar2Num(apTokens[MEN_COR_ASSDPERSONAL]);
   long lFixt   = dollar2Num(apTokens[MEN_COR_ASSDFIXTURES]);
   long lLivImpr= dollar2Num(apTokens[MEN_COR_ASSDLIVIMP]);
   long lOther= lPers + lFixt + lLivImpr;
   if (lOther > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lOther);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lLivImpr > 0)
      {
         iTmp = sprintf(acTmp, "%u", lLivImpr);
         memcpy(pLienRec->extra.Men.LivingImpr, acTmp, iTmp);
      }
   }

   // Gross total
   long lGross = dollar2Num(apTokens[MEN_COR_ASSESSEDFULL]);
   if (lGross > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lGross);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exempt
   long lExe = dollar2Num(apTokens[MEN_COR_HOX]);
   if (lExe > 0)
      pLienRec->acHO[0] = '1';         // Y
   else
      pLienRec->acHO[0] = '2';         // N

   lTmp = dollar2Num(apTokens[MEN_COR_OTHEREXMPT]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Men.acOtherExe), lTmp);
      memcpy(pLienRec->extra.Men.acOtherExe, acTmp, iTmp);
   }

   lExe = dollar2Num(apTokens[MEN_COR_TOTALEXMPT]);
   if (lExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   // Full exemption
   if (lExe > 0 && lExe >= lGross)
      pLienRec->SpclFlag = LX_FULLEXE_FLG;

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Men_ExtrRollCorrection()
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   long     lCnt, lRead;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Drop header
   pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Merge loop
   lCnt=lRead=0;
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new lien record
      if (memcmp(acRollRec, "0000", 4) > 0)
      {
         Men_CreateRollCorrectionRec(acBuf, acRollRec);
         fputs(acBuf, fdLien);
         lCnt++;
      }
      if (!(lCnt % 1000))
         printf("\r%u", lRead);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total records output:    %d\n", lCnt);

   return 0;
}

/***************************** Men_ParseTaxDetail ****************************
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Men_ParseTaxDetail(FILE *fdDetail, FILE *fdAgency)
{
   char     acTmp[512], acOutbuf[512];
   int      iIdx;
   long     lTmp;
   TAXDETAIL *pDetail = (TAXDETAIL *)acOutbuf;
   TAXAGENCY *pResult, sAgency, *pAgency;

   // Clear output buffer
   memset(acOutbuf, 0, sizeof(TAXDETAIL));
   memset(&sAgency, 0, sizeof(TAXAGENCY));
   pAgency = &sAgency;

   // APN
   strcpy(pDetail->Apn, apTokens[MEN_SEC_APN]);

   // BillNumber
   strcpy(pDetail->BillNum, apTokens[MEN_SEC_BILL_NUM]);

   // Tax Year
   sprintf(pDetail->TaxYear, "%d", lTaxYear);

   iIdx = MEN_SEC_AS_CODE_1;
   while (*apTokens[iIdx] > ' ')
   {
      memset(&sAgency, 0, sizeof(TAXAGENCY));

      // Tax code
      strcpy(pDetail->TaxCode, apTokens[iIdx]);
      strcpy(pAgency->Code, apTokens[iIdx]);

      memset(pDetail->TaxRate, 0, sizeof(pDetail->TaxRate));
      pResult = findTaxAgency(pAgency->Code, 0);
      if (pResult)
      {
         strcpy(pAgency->Agency, pResult->Agency);
         strcpy(pAgency->Phone, pResult->Phone);
         pAgency->TC_Flag[0] = pResult->TC_Flag[0];
         pDetail->TC_Flag[0] = pResult->TC_Flag[0];

         // Tax Rate - Make sure we have latest tax rate table for the tax year
         if (pResult->TaxRate[0] > ' ')
         {
            strcpy(pAgency->TaxRate, pResult->TaxRate);
            strcpy(pDetail->TaxRate, pResult->TaxRate);
         }
      } else
      {
         pAgency->Agency[0] = 0;
         LogMsg("+++ Unknown TaxCode: %s APN=%s", pDetail->TaxCode, pDetail->Apn);
      }

      // Tax amt
      lTmp = atol(apTokens[iIdx+1]);
      lTmp += atol(apTokens[iIdx+2]);

      if (lTmp > 0)
         sprintf(pDetail->TaxAmt, "%.2f", (double)lTmp/100.0);
      else
         strcpy(pDetail->TaxAmt, "0");

      // Generate csv line and write to file
      Tax_CreateDetailCsv(acTmp, pDetail);
      fputs(acTmp, fdDetail);

      // Forming Agency record
      Tax_CreateAgencyCsv(acTmp, pAgency);
      fputs(acTmp, fdAgency);

      iIdx += 3;
   }

   return 0;
}

/***************************** Men_ParseTaxBase ******************************
 *
 *
 *****************************************************************************/

int Men_ParseTaxBase(char *pOutbuf, char *pInbuf)
{
   char     acTmp[256], *pTmp;
   int      iTmp;
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, '|', MAX_FLD_TOKEN, apTokens);
   if (iTmp < MEN_SEC_BRUP)
   {
      LogMsg("***** Error: bad TaxBase record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // If canceled, ignore it
   if (*apTokens[MEN_SEC_I1_STAT] == 'C' || *apTokens[MEN_SEC_I2_STAT] == 'C')
   {
      if (!memcmp(apTokens[MEN_SEC_PAYOR], "CANCELLED", 6))
         LogMsg("+++ Keep %s reason %s", apTokens[MEN_SEC_APN], apTokens[MEN_SEC_PAYOR]);
      else if (*(apTokens[MEN_SEC_APN]+9) != 'N')
         return 1;
   }

   if (*(apTokens[MEN_SEC_APN]+9) == 'N')
   {
      LogMsg("<--- Rename record %s = %.9s0", apTokens[MEN_SEC_APN], apTokens[MEN_SEC_APN]); 
      *(apTokens[MEN_SEC_APN]+9) = '0';
   }

   // APN
   strcpy(pOutRec->Apn, apTokens[MEN_SEC_APN]);
   strcpy(pOutRec->BillNum, myBTrim(apTokens[MEN_SEC_BILL_NUM]));
   strcpy(pOutRec->OwnerInfo.Apn, apTokens[MEN_SEC_APN]);
   strcpy(pOutRec->OwnerInfo.BillNum, apTokens[MEN_SEC_BILL_NUM]);

   // Bill Type
   pOutRec->BillType[0] = BILLTYPE_SECURED;

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "0010408900", 10))
   //   iTmp = 0;
#endif

   // Installment Status
   if (*apTokens[MEN_SEC_I1_STAT] == 'P')
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
   else if (*apTokens[MEN_SEC_I1_STAT] == 'C')
      pOutRec->Inst1Status[0] = TAX_BSTAT_CANCEL;
   else if (*apTokens[MEN_SEC_I1_STAT] == 'N')
      pOutRec->Inst1Status[0] = TAX_BSTAT_NOTAX;
   else
      pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;

   if (*apTokens[MEN_SEC_I2_STAT] == 'P')
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
   else if (*apTokens[MEN_SEC_I1_STAT] == 'C')
      pOutRec->Inst2Status[0] = TAX_BSTAT_CANCEL;
   else if (*apTokens[MEN_SEC_I1_STAT] == 'N')
      pOutRec->Inst2Status[0] = TAX_BSTAT_NOTAX;
   else
      pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   // TRA
   strcpy(pOutRec->TRA, apTokens[MEN_SEC_TRA]);

   // Tax Year
   iTmp = sprintf(pOutRec->TaxYear, "%d", lTaxYear);
   strcpy(pOutRec->OwnerInfo.TaxYear, pOutRec->TaxYear);

   // Default date - Use data from Delq file instead
   //pOutRec->isDelq[0] = '0';
   //if (*apTokens[MEN_SEC_STS_DATE] > ' ' )
   //{
   //   apTokens[MEN_SEC_STS_DATE][8] = 0;
   //   pTmp = dateConversion(apTokens[MEN_SEC_STS_DATE], pOutRec->Def_Date, MMDDYYYY);
   //   if (pTmp)
   //   {
   //      memcpy(pOutRec->DelqYear, pOutRec->Def_Date, 4);
   //      if (!memcmp(pOutRec->DelqYear, pOutRec->TaxYear, 4))
   //         pOutRec->isDelq[0] = '1';
   //   }
   //}

   // Penalty
   double dPen1 = atof(apTokens[MEN_SEC_I1_PEN]);
   double dPen2 = atof(apTokens[MEN_SEC_I2_PEN]);
   double dTotalDue = 0;

   // Check for Tax amount
   double dTax1 = atof(apTokens[MEN_SEC_I1_TAX]);
   if (dTax1 > 0.0)
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1/100.0);

   double dTax2 = atof(apTokens[MEN_SEC_I2_TAX]);
   if (dTax2 > 0.0)
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2/100.0);

   double dTaxTotal = atof(apTokens[MEN_SEC_NET_TAX]);
   if (dTaxTotal > 0.0)
   {
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal);

      // Paid Date
      if (*apTokens[MEN_SEC_I1_PAIDDATE] >= '0' && dateConversion(apTokens[MEN_SEC_I1_PAIDDATE], acTmp, MMDDYYYY))
      {
         strcpy(pOutRec->PaidDate1, acTmp);
         dPen1 = 0;
      }
      if (*apTokens[MEN_SEC_I2_PAIDDATE] >= '0' && dateConversion(apTokens[MEN_SEC_I2_PAIDDATE], acTmp, MMDDYYYY))
      {
         strcpy(pOutRec->PaidDate2, acTmp);
         dPen2 = 0;
      }

      // Due date
      if (*apTokens[MEN_SEC_I1_DUEDATE] >= '0' && dateConversion(apTokens[MEN_SEC_I1_DUEDATE], acTmp, MMDDYYYY))
         strcpy(pOutRec->DueDate1, acTmp);
      if (*apTokens[MEN_SEC_I2_DUEDATE] >= '0' && dateConversion(apTokens[MEN_SEC_I2_DUEDATE], acTmp, MMDDYYYY, lToyear+1))
         strcpy(pOutRec->DueDate2, acTmp);

      // If past due date, add penalty to due amt
      if (dPen1 > 0.0)
      {
         if (ChkDueDate(1, pOutRec->DueDate1))
         {
            dTotalDue = dTax1+dPen1+dTax2;
            sprintf(pOutRec->PenAmt1, "%.2f", dPen1/100.0);
         } else
            dTotalDue = dTax1;
      }
      if (dPen2 > 0.0  && ChkDueDate(2, pOutRec->DueDate2))
      {
         dTotalDue += dPen2;
         sprintf(pOutRec->PenAmt2, "%.2f", dPen2/100.0);
      }
   }

   if (dTotalDue > 0.0)
      sprintf(pOutRec->TotalDue, "%.2f", dTotalDue/100.0);

   // Tax rate
   strcpy(pOutRec->TotalRate, apTokens[MEN_SEC_TOT_RATE]);

   // Owner Info
   myTrim(apTokens[MEN_SEC_NAME2]);
   if ((pTmp = strstr(apTokens[MEN_SEC_NAME1], " /")) || (pTmp = strstr(apTokens[MEN_SEC_NAME1], "/ ")))
   {
      *pTmp = 0;
      strcpy(pOutRec->OwnerInfo.Name1, apTokens[MEN_SEC_NAME1]);
      strcpy(pOutRec->OwnerInfo.Name2, apTokens[MEN_SEC_NAME2]);
   } else if (pTmp = strstr(apTokens[MEN_SEC_NAME1], "1/2/"))
   {
      *(pTmp+3) = 0;
      strcpy(pOutRec->OwnerInfo.Name1, apTokens[MEN_SEC_NAME1]);
      strcpy(pOutRec->OwnerInfo.Name2, apTokens[MEN_SEC_NAME2]);
   } else if (*apTokens[MEN_SEC_NAME2] > ' ' && (pTmp = strstr(apTokens[MEN_SEC_NAME1], " TTEE")))
   {
      iTmp = strlen(apTokens[MEN_SEC_NAME2]);
      if (iTmp > 15)
      {
         strcpy(pOutRec->OwnerInfo.Name1, myTrim(apTokens[MEN_SEC_NAME1]));
         strcpy(pOutRec->OwnerInfo.Name2, apTokens[MEN_SEC_NAME2]);
      } else
      {
         strcpy(pOutRec->OwnerInfo.Name1, apTokens[MEN_SEC_NAME1]);
         strcat(pOutRec->OwnerInfo.Name1, apTokens[MEN_SEC_NAME2]);
      }
   } else if (*apTokens[MEN_SEC_NAME2] > ' ')
   {
      sprintf(acTmp, "%s%s", apTokens[MEN_SEC_NAME1], apTokens[MEN_SEC_NAME2]);
      iTmp = blankRem(acTmp);
      if (iTmp >= TAX_NAME)
      {
         strcpy(pOutRec->OwnerInfo.Name1, apTokens[MEN_SEC_NAME1]);
         strcpy(pOutRec->OwnerInfo.Name2, apTokens[MEN_SEC_NAME2]);
      } else
         strcpy(pOutRec->OwnerInfo.Name1, acTmp);
   } else
      strcpy(pOutRec->OwnerInfo.Name1, myTrim(apTokens[MEN_SEC_NAME1]));

   // CareOf
   if (*apTokens[MEN_SEC_CAREOF] > ' ')
      strcpy(pOutRec->OwnerInfo.CareOf, apTokens[MEN_SEC_CAREOF]);

   // DBA
   if (*apTokens[MEN_SEC_DBA] > ' ')
      strcpy(pOutRec->OwnerInfo.Dba, apTokens[MEN_SEC_DBA]);

   // Mailing address
   if (*apTokens[MEN_SEC_M_ADDR] > ' ' && memcmp(apTokens[MEN_SEC_M_ADDR], "UNK", 3))
   {
      strcpy(pOutRec->OwnerInfo.MailAdr[0], myTrim(apTokens[MEN_SEC_M_ADDR]));
      sprintf(acTmp, "%s %s", apTokens[MEN_SEC_M_CITYST], apTokens[MEN_SEC_M_ZIP]);
      iTmp = blankRem(acTmp);
      strcpy(pOutRec->OwnerInfo.MailAdr[1], acTmp);
   }

   pOutRec->isSecd[0] = '1';
   pOutRec->isSupp[0] = '0';

   return 0;
}

/****************************** Men_MergeTaxDelq *****************************
 *
 * Copy DelqYear from delq record to populate Tax_Base
 *
 *****************************************************************************/

int Men_MergeTaxDelq(char *pOutbuf)
{
   static   char  acRec[2048], *pRec=NULL;
   char     *apItems[MAX_FLD_TOKEN], acTmp[32];
   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   int      iLoop, iTmp;

   // Get first Sale rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 2048, fdDelq);
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pTaxBase->Apn, (pRec+OFF_DELQ_PRCL), iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip delq rec  %.*s", iApnLen, pRec+OFF_DELQ_PRCL);
         pRec = fgets(acRec, 2048, fdDelq);
         if (!pRec)
         {
            fclose(fdDelq);
            fdDelq = NULL;
            return -1;      // EOF
         }

         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Parse input tax data
   iTmp = ParseStringIQ(acRec, '|', MAX_FLD_TOKEN, apItems);
   if (iTmp < MEN_DELQ_REM)
   {
      LogMsg("***** Error: bad TaxDelq record for: %.50s (#tokens=%d)", acRec, iTmp);
      return -1;
   }

   // Default date
   if (dateConversion(apItems[MEN_DELQ_TAX_DEF_DATE], acTmp, MMDDYYYY))
   {
      strcpy(pTaxBase->Def_Date, acTmp);
      memcpy(pTaxBase->DelqYear, acTmp, 4);

      if (*apItems[MEN_DELQ_RDM_STAT] != 'P')
         pTaxBase->isDelq[0] = '1';
   }

   lSaleMatch++;

   return 0;
}

/**************************** Men_Load_TaxBase *******************************
 *
 * Create import file for Tax_Base, Tax_Items, Tax_Agency & Tax_Owner tables
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Men_Load_TaxBase(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   char     acAgencyFile[_MAX_PATH], acOwnerFile[_MAX_PATH], acBaseFile[_MAX_PATH], 
            acItemsFile[_MAX_PATH], acDelqFile[_MAX_PATH], acSecRollFile[_MAX_PATH];

   int      iRet, iLen;
   long     lOut=0, lCnt=0, lTmp;
   FILE     *fdAgency, *fdOwner, *fdBase, *fdDetail, *fdSecRoll;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];

   LogMsg("Loading Current tax file");

   GetIniString(myCounty.acCntyCode, "CurrTax", "", acSecRollFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acSecRollFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   iLen = GetPrivateProfileInt(myCounty.acCntyCode, "CurrTaxLen", 0, acIniFile);
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   NameTaxCsvFile(acOwnerFile, myCounty.acCntyCode, "Owner");

   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Convert EBCDIC to ASCII before use
   if (iLen > 0)
   {                    
      strcpy(acBuf, acSecRollFile);
      strcat(acSecRollFile, ".ASC");
      iRet = getFileDate(acSecRollFile);
      if (iRet <= lLastTaxFileDate)
      {
         LogMsg("Convert EBC2ASC: %s --> %s", acBuf, acSecRollFile);
         iRet = doEBC2ASCAddCR(acBuf, acSecRollFile, iLen);
         if (iRet)
         {
            LogMsg("***** Error converting EBC2ASC from %s to %s", acBuf, acSecRollFile);
            return -1;
         }
      }
   }

   GetIniString(myCounty.acCntyCode, "DelqTax", "", acDelqFile, _MAX_PATH, acIniFile);
   iLen = GetPrivateProfileInt(myCounty.acCntyCode, "DelqTaxLen", 0, acIniFile);
   lTmp = getFileDate(acDelqFile);

   // Convert EBCDIC to ASCII before use
   if (iLen > 0)
   {                    
      strcpy(acBuf, acDelqFile);
      strcat(acDelqFile, ".ASC");
      iRet = getFileDate(acDelqFile);
      if (iRet <= lTmp)
      {
         LogMsg("Convert EBC2ASC: %s --> %s", acBuf, acDelqFile);
         iRet = doEBC2ASCAddCR(acBuf, acDelqFile, iLen);
         if (iRet)
         {
            LogMsg("***** Error converting EBC2ASC from %s to %s", acBuf, acDelqFile);
            return -1;
         }
      }
   }

   // Open input file
   LogMsg("Open delinquent tax file %s", acDelqFile);
   fdDelq = fopen(acDelqFile, "r");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error opening delinquent tax file: %s\n", acDelqFile);
      return -2;
   }  

   // Open input file
   LogMsg("Open Current tax file %s", acSecRollFile);
   fdSecRoll = fopen(acSecRollFile, "r");
   if (fdSecRoll == NULL)
   {
      LogMsg("***** Error opening Current tax file: %s\n", acSecRollFile);
      return -2;
   }  

   // Open Detail file
   LogMsg("Open Detail file %s", acItemsFile);
   fdDetail = fopen(acItemsFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail file: %s\n", acItemsFile);
      return -4;
   }

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open owner file
   LogMsg("Open Owner file %s", acOwnerFile);
   fdOwner = fopen(acOwnerFile, "w");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error creating Owner file: %s\n", acOwnerFile);
      return -4;
   }

   // Open agency file
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.lst", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Owner file: %s\n", acTmpFile);
      return -4;
   }

   // Init variables

   // Merge loop 
   while (!feof(fdSecRoll))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdSecRoll);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Men_ParseTaxBase(acBuf, acRec);
      if (!iRet)
      {
         // Create Detail & Agency records
         Men_ParseTaxDetail(fdDetail, fdAgency);

         // Update delq data
         iRet = Men_MergeTaxDelq(acBuf);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         lOut++;
         fputs(acRec, fdBase);

         // Create Owner record
         Tax_CreateTaxOwnerCsv(acRec, &pTaxBase->OwnerInfo);
         fputs(acRec, fdOwner);
      } else
      {
         if (iRet == 1)
            LogMsg("---> Drop cancel record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSecRoll)
      fclose(fdSecRoll);
   if (fdDelq)
      fclose(fdDelq);
   if (fdBase)
      fclose(fdBase);
   if (fdDetail)
      fclose(fdDetail);
   if (fdOwner)
      fclose(fdOwner);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
      {
         iRet = doTaxImport(myCounty.acCntyCode, TAX_OWNER);
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      }

      if (!iRet)
      {
         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }

   } else
      iRet = 0;

   return iRet;
}

/****************************** Men_ParseTaxDelq *****************************
 *
 * Return number of records output
 *
 *****************************************************************************/

int Men_ParseTaxDelq(char *pOutbuf, char *pInbuf, FILE *fdOut)
{
   char     acTmp[256];
   int      iTmp, iCnt;
   long     lTmp;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, '|', MAX_FLD_TOKEN, apTokens);
   if (iTmp < MEN_DELQ_REM)
   {
      LogMsg("***** Error: bad TaxDelq record for: %.50s (#tokens=%d)", pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));

   // APN
   strcpy(pOutRec->Apn, myTrim(apTokens[MEN_DELQ_PRCL]));

   // Updated date
   if (*apTokens[MEN_DELQ_ACT_DATE] > ' ' && dateConversion(apTokens[MEN_DELQ_ACT_DATE], acTmp, MMDDYYYY))
      strcpy(pOutRec->Upd_Date, acTmp);
   else
      sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);
/*
#define  MEN_DELQ_A0_ASSMT_NBR      9
#define  MEN_DELQ_A0_YR             10
#define  MEN_DELQ_A0_PRCL           11    // Orig. parcel
#define  MEN_DELQ_A0_TYPE           12    // Secu(R)ed, (S)uplemental
#define  MEN_DELQ_A0_PAY_FLAG       13    // N ???
#define  MEN_DELQ_A0_AREA           14    // TRA
#define  MEN_DELQ_A0_TAX            15
#define  MEN_DELQ_A0_PEN            16
#define  MEN_DELQ_A0_COST           17
#define  MEN_DELQ_A0_AUDIT_AMT      18
*/
   iCnt = 0;

/* Need more investigation
   iTmp = MEN_DELQ_A0_ASSMT_NBR;
   while (iTmp < MEN_DELQ_A9_AUDIT_AMT && *apTokens[iTmp+1] > ' ')
   {
      // For now, just process secured record
      if (*apTokens[iTmp+3] == 'R')
      {
         strcpy(pOutRec->Assmnt_No, apTokens[iTmp]);

         // Tax Year
         strcpy(pOutRec->TaxYear, apTokens[iTmp+1]);

         // Default amt
         lTmp = atol(apTokens[iTmp+6]);
         sprintf(pOutRec->Def_Amt, "%.2f", (double)lTmp/100.0);

         // Pen
         lTmp = atol(apTokens[iTmp+7]);
         sprintf(pOutRec->Pen_Amt, "%.2f", (double)lTmp/100.0);

         // Fee
         lTmp = atol(apTokens[iTmp+8]);
         sprintf(pOutRec->Fee_Amt, "%.2f", (double)lTmp/100.0);

         // Create delimited record
         Tax_CreateDelqCsv(acTmp, (TAXDELQ *)pOutRec);

         // Output record			
         fputs(acTmp, fdOut);
         iCnt++;
      }

      iTmp += 10;
   }
*/

   // Default number
   strcpy(pOutRec->Default_No, myTrim(apTokens[MEN_DELQ_TAX_DEF_NR]));

   // Default date
   if (dateConversion(apTokens[MEN_DELQ_TAX_DEF_DATE], acTmp, MMDDYYYY))
   {
      strcpy(pOutRec->Def_Date, acTmp);
      // Tax Year
      iTmp = atoin(acTmp, 4);
      sprintf(pOutRec->TaxYear, "%d", iTmp-1);
   }

   // Default amt
   lTmp = atol(apTokens[MEN_DELQ_TAX_DEF_AMT]);
   sprintf(pOutRec->Def_Amt, "%.2f", (double)lTmp/100.0);

   // Redemption date
   if (dateConversion(apTokens[MEN_DELQ_RDM_DATE], acTmp, MMDDYYYY))
      strcpy(pOutRec->Red_Date, acTmp);

   // Redemption amt
   lTmp = atol(apTokens[MEN_DELQ_RDM_AMT]);
   sprintf(pOutRec->Red_Amt, "%.2f", (double)lTmp/100.0);

   // Tax amt
   lTmp = atol(apTokens[MEN_DELQ_TOT_TAX]);
   sprintf(pOutRec->Tax_Amt, "%.2f", (double)lTmp/100.0);

   // Pen
   lTmp = atol(apTokens[MEN_DELQ_TOT_PEN]);
   sprintf(pOutRec->Pen_Amt, "%.2f", (double)lTmp/100.0);

   // Fee
   long lCost = atol(apTokens[MEN_DELQ_TOT_COST]);
   long lStateFee = atol(apTokens[MEN_DELQ_STATE_FEE]);
   lTmp = lStateFee+lCost;
   sprintf(pOutRec->Fee_Amt, "%.2f", (double)lTmp/100.0);

   if (*apTokens[MEN_DELQ_RDM_STAT] == 'P')
      pOutRec->isDelq[0] = '0';
   else
      pOutRec->isDelq[0] = '1';

   // Create delimited record
   Tax_CreateDelqCsv(acTmp, (TAXDELQ *)pOutRec);

   // Output record			
   fputs(acTmp, fdOut);
   iCnt++;

   return iCnt;
}

/**************************** Men_Load_Delq **********************************
 *
 * Create import file for sabsdata.www and import into SQL if specified
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Men_Load_TaxDelq(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet, iLen;
   long     lOut=0, lCnt=0;
   FILE     *fdOut, *fdIn;

   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");
   pTmp = strrchr(acOutFile, '\\');
   *pTmp = 0;
   if (_access(acOutFile, 0))
      _mkdir(acOutFile);
   *pTmp = '\\';

   GetIniString(myCounty.acCntyCode, "DelqTax", "", acInFile, _MAX_PATH, acIniFile);
   iLen = GetPrivateProfileInt(myCounty.acCntyCode, "DelqTaxLen", 0, acIniFile);
   lLastFileDate = getFileDate(acInFile);

   // Convert EBCDIC to ASCII before use
   if (iLen > 0)
   {                    
      strcpy(acBuf, acInFile);
      strcat(acInFile, ".ASC");
      iRet = getFileDate(acInFile);
      if (iRet <= lLastFileDate)
      {
         LogMsg("Convert EBC2ASC: %s --> %s", acBuf, acInFile);
         iRet = doEBC2ASCAddCR(acBuf, acInFile, iLen);
         if (iRet)
         {
            LogMsg("***** Error converting EBC2ASC from %s to %s", acBuf, acInFile);
            return -1;
         }
      }
   }

   // Open input file
   LogMsg("Open delinquent tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening delinquent tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < ' ')
         break;

      // Create new TaxDelq record
      iRet = Men_ParseTaxDelq(acBuf, acRec, fdOut);
      if (iRet > 0)
      {
         lOut += iRet;
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:          %u", lCnt);
   LogMsg("Total delinquent records output:  %u", lOut);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport && lOut > 0)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/******************************** Men_MergeOwner *****************************
 *
 * This version parses owners from CA-Mendo-AsmtRoll.csv
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Men_MergeOwner(char *pOutbuf)
{
   int   iTmp;
   char  *pName1, *pName2, *pTmp;
   OWNER myOwner;

   // Clear old names, vesting, and DBA
   removeNames(pOutbuf, false, true);

   if (*apTokens[R_DOINGBUSINESSAS] > ' ')
   {
      iTmp = cleanOwner(apTokens[R_DOINGBUSINESSAS]);
      if (iTmp > 0)
         blankRem(apTokens[R_DOINGBUSINESSAS]);
      vmemcpy(pOutbuf+OFF_DBA, _strupr(apTokens[R_DOINGBUSINESSAS]), SIZ_DBA);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009619813", 9) )
   //   iTmp = 0;
#endif
   cleanOwner(apTokens[R_LEGALPARTY1]);
   cleanOwner(apTokens[R_LEGALPARTY2]);

   if (*apTokens[R_LEGALPARTY1] > ' ')
   {
      pName1 = _strupr(apTokens[R_LEGALPARTY1]);
      pName2 = _strupr(apTokens[R_LEGALPARTY2]);
   } else
   {
      pName1 = _strupr(apTokens[R_LEGALPARTY2]);
      pName2 = _strupr(apTokens[R_LEGALPARTY1]);
   }

   if (*pName1 > ' ')
   {
      pTmp = strchr(pName1, '/');
      if (pTmp && *(pTmp -1) == ' ')
         *pTmp = 0;
      vmemcpy(pOutbuf+OFF_NAME1, pName1, SIZ_NAME1);

      // Update vesting
      iTmp = updateVesting(myCounty.acCntyCode, pName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
      if (!iTmp)
      {
         if (pTmp = strstr(pName1, "1/2"))
            *pTmp = 0;
         iTmp = splitOwner(pName1, &myOwner, 3);
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
      } else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, pName1, SIZ_NAME_SWAP);

   }

   if (*pName2 > ' ')
   {
      vmemcpy(pOutbuf+OFF_NAME2, pName2, SIZ_NAME2);

      // Update vesting
      if (*(pOutbuf+OFF_VEST) == ' ')
         updateVesting(myCounty.acCntyCode, pName2, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
   }

   // Check to see if we can combine names
   //pTmp = strchr(pName1, ' ');
   //if (!memcmp(pName1, pName2, pTmp-pName1))
   //{
   //   iTmp = sprintf(acTmp, "%.*s & %.*s %.*s",
   //      RSIZ_OWN1_FIRST+RSIZ_OWN1_MIDDLE, pRec->Own1_First,
   //      RSIZ_OWN2_FIRST+RSIZ_OWN2_MIDDLE, pRec->Own2_First,
   //      RSIZ_OWN1_LAST, pRec->Own1_Last);
   //   iTmp = blankRem(acTmp, iTmp);
   //   vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP, iTmp);
   //} else
   //{
   //   // Swapped name
   //   if (pRec->Own1_Flag == 'F')
   //      sprintf(acTmp, "%.24s %.19s", pRec->Own1_First, pRec->Own1_Last);
   //   else
   //      memcpy(acTmp, pRec->Own1_Last, RSIZ_OWNER);
   //   iTmp = blankRem(acTmp, RSIZ_OWNER);
   //   memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp);
   //}

   if (*apTokens[R_LEGALPARTY3] > ' ')
      *(pOutbuf+OFF_ETAL_FLG) = 'Y';
}

/******************************* Men_MergeLOwner *****************************
 *
 * This version parses owners from LDR file
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Men_MergeLOwner(char *pOutbuf, char *pNames, int iNameSeq)
{
   char  acTmp[256];
   int   iTmp, iNameIdx;
   OWNER myOwner;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1880600600", 9) )
   //   iTmp = 0;
#endif

   iNameIdx = iNameSeq;
   if (!memcmp(pNames, "NULL", 4))
      return;

   strcpy(acTmp, pNames);
   iTmp = iTrim(acTmp);
   if (acTmp[iTmp-1] == '/')
      acTmp[iTmp-1] = 0;
   blankRem(acTmp);

LOAD_NAME1:
   if (iNameIdx == 1)
   {
      // 19 LAST, 12 FIRST, 12 MIDL
      // Name1
      vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);

      // Update vesting
      iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

      if (*apTokens[MEN_L_MAILNAME] > ' ')
      {
         strcpy(acTmp, apTokens[MEN_L_MAILNAME]);
         iTmp = blankRem(acTmp);
         if (acTmp[iTmp-1] == '/' && acTmp[iTmp-2] == ' ')
            acTmp[iTmp-1] = 0;
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP);
      } else
      {
#ifdef _DEBUG
         //if (!memcmp(pOutbuf, "008042180", 9) )
         //   iTmp = 0;
#endif
         // Swapped name - if no vesting found
         if (!iTmp)
         {
            iTmp = splitOwner(acTmp, &myOwner, 3);
            vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
         } else
         {
            vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP);
         }
      }
   } else if (iNameIdx == 2)
   {
      if (*(pOutbuf+OFF_NAME1) == ' ' && acTmp[0] > ' ')
      {
         iNameIdx = 1;
         goto LOAD_NAME1;
      }

      // Name2
      iTmp = blankRem(acTmp);
      vmemcpy(pOutbuf+OFF_NAME2, acTmp, SIZ_NAME2, iTmp);
   }
}

/***************************** Men_MergeLienCsv ******************************
 *
 * For LDR "2021 ANNUAL ROLLS - SEPARATED.txt".  
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

//int Men_MergeLienCsv(char *pOutbuf, char *pRollRec)
//{
//   static   char acApn[32];
//   char     acTmp[256], acTmp1[64];
//   long     lTmp;
//   int      iRet;
//
//   // Replace null char with space
//   iRet = replNull(pRollRec, ' ', 0);
//
//   // Parse input rec
//   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
//   if (iRet < L_FLDS)
//   {
//      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L_APN], iRet);
//      return -1;
//   }
//
//   // Skip Personal Property
//   if (*apTokens[L_ROLLTYPE] == 'P')
//   {
//      LogMsg("*** Ignore Personal Property APN: %s", apTokens[L_APN]);
//      return 1;
//   }
//
//   if (!strcmp(apTokens[L_APN], acApn))
//   {
//      LogMsg("*** Ignore duplicate APN: %s", apTokens[L_APN]);
//      return 1;
//   }
//
//   // Copy APN
//   strcpy(acApn, apTokens[L_APN]);
//   iRet = iTrim(acApn);
//   if (iRet != iApnLen)
//   {
//      LogMsg("*** Ignore bad APN: %s", acApn);
//      return 1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//   memcpy(pOutbuf, acApn, iApnLen);
//
//   // Format APN
//   iRet = formatApn(acApn, acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//   // Create MapLink and output new record
//   iRet = formatMapLink(acApn, acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // Create index map link
//   if (getIndexPage(acTmp, acTmp1, &myCounty))
//      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//   // County code
//   memcpy(pOutbuf+OFF_CO_NUM, "23MEN", 5);
//
//   // status
//   *(pOutbuf+OFF_STATUS) = 'A';
//
//   // TRA
//   sprintf(acTmp, "%.6d", atol(apTokens[L_MEN_TRA]));
//   vmemcpy(pOutbuf+OFF_TRA, acTmp, 6);
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // Land
//   long lLand = dollar2Num(apTokens[L_LAND]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = dollar2Num(apTokens[L_IMPR]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: Fixture, PersProp, PPMH
//   long lFixtr = dollar2Num(apTokens[L_FIXTURES]);
//   long lPers  = dollar2Num(apTokens[L_PPVAL]);
//   long lGrowVal = dollar2Num(apTokens[L_LIVINGIMPR]);
//   lTmp = lFixtr+lPers+lGrowVal;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//
//      if (lFixtr > 0)
//      {
//         sprintf(acTmp, "%d         ", lFixtr);
//         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
//      }
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%d         ", lPers);
//         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
//      }
//   }
//
//   // Gross total
//   lTmp += (lLand+lImpr);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//   // Exemption
//   long lExe1 = dollar2Num(apTokens[L_HOEXE]);
//   long lTotalExe = dollar2Num(apTokens[L_TOTALEXE]);
//   if (lTotalExe > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTotalExe);
//      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
//   }  
//
//   if (lExe1 > 0)
//      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
//   else
//      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "001100061000", 9) )
//   //   iTmp = 0;
//#endif
//
//   // Legal - obtain legal from roll update
//   //if (*apTokens[L_PROPDESC] > ' ')
//   //   updateLegal(pOutbuf, apTokens[L_PROPDESC]);
//
//   // UseCode - pull from old roll file REDIFILE
//
//   // Owner
//   Men_MergeOwner(pOutbuf, apTokens[L_MEN_OWNER], apTokens[L_OWNERX]);
//
//   // Situs
//   Men_MergeSAdr(pOutbuf, apTokens[L_SADDR1], apTokens[L_SCITY], apTokens[L_SZIP]);
//
//   // Mailing
//   Men_MergeMAdr(pOutbuf, apTokens[L_MADDR1], apTokens[L_MADDR2]);
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "006380014000", 9))
//   //   iTmp = 0;
//#endif
//
//   return 0;
//}

/****************************** Men_MergeMailing *****************************
 *
 * Merge mail address for LDR 2019
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Men_MergeMailing(char *pOutbuf)
{
   char        acTmp[256], acAddr1[64], *pAddr1;
   int         iTmp;
   bool        bUnitAvail = false;

   ADR_REC     sMailAdr;

   // Check for blank address
   replUnPrtChar(apTokens[MEN_L_M_ADDR1]);
   pAddr1 = apTokens[MEN_L_M_ADDR1];
   if (memcmp(pAddr1, "     ", 5) && memcmp(pAddr1, "UNKNOWN", 7))
   {
      strcpy(acAddr1, pAddr1);
      pAddr1 = myTrim(acAddr1);
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

      // Parsing mail address
      parseMAdr1(&sMailAdr, pAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }

      if (sMailAdr.strDir[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);

      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);

      if (sMailAdr.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);

      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            vmemcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, SIZ_M_UNITNO);
         }
      }

      if (bUnitAvail)
      {
         iTmp = sprintf(acTmp, "%s #%s", acAddr1, sMailAdr.Unit);
         vmemcpy(pOutbuf+OFF_M_ADDR_D, acTmp, SIZ_M_ADDR_D, iTmp);
      } else
         vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "607224002", 9) )
      //   iTmp = 0;
#endif

      // City/State
      if (*apTokens[MEN_L_M_CITY] > ' ')
      {
         // Remove quote & comma at the end of city name
         strcpy(acAddr1, apTokens[MEN_L_M_CITY]);
         iTmp = iTrim(acAddr1);
         if (acAddr1[iTmp-1] == ',')
            acAddr1[iTmp-1] = 0;
         remUnPrtChar(acAddr1);

         if (*apTokens[MEN_L_M_STATE] > ' ')
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY);
            vmemcpy(pOutbuf+OFF_M_ST, apTokens[MEN_L_M_STATE], 2);

            iTmp = sprintf(acTmp, "%s, %s %s", acAddr1, myTrim(apTokens[MEN_L_M_STATE]), apTokens[MEN_L_M_ZIP]);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
         } else
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY+SIZ_M_ST+SIZ_M_ZIP);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, SIZ_M_CTY_ST_D);
         }
      }

      iTmp = atol(apTokens[MEN_L_M_ZIP]);
      if (iTmp > 500)
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[MEN_L_M_ZIP], SIZ_M_ZIP);
   }
}

void Men_MergeMailing1(char *pOutbuf)
{
   char        acTmp[256], acAddr1[64], *pAddr1;
   int         iTmp;
   bool        bUnitAvail = false;

   ADR_REC     sMailAdr;

   // Check for blank address
   replUnPrtChar(apTokens[R_MAILADDRESS]);
   pAddr1 = apTokens[R_MAILADDRESS];
   if (memcmp(pAddr1, "     ", 5) && memcmp(pAddr1, "UNKNOWN", 7))
   {
      strcpy(acAddr1, pAddr1);
      pAddr1 = myTrim(acAddr1);
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

      // Parsing mail address
      parseMAdr1(&sMailAdr, pAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }

      if (sMailAdr.strDir[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);

      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);

      if (sMailAdr.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);

      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            vmemcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, SIZ_M_UNITNO);
         }
      }

      if (bUnitAvail)
      {
         iTmp = sprintf(acTmp, "%s #%s", acAddr1, sMailAdr.Unit);
         vmemcpy(pOutbuf+OFF_M_ADDR_D, acTmp, SIZ_M_ADDR_D, iTmp);
      } else
         vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "607224002", 9) )
      //   iTmp = 0;
#endif

      // City/State
      if (*apTokens[R_MAILCITY] > ' ')
      {
         // Remove quote & comma at the end of city name
         strcpy(acAddr1, apTokens[R_MAILCITY]);
         iTmp = iTrim(acAddr1);
         if (acAddr1[iTmp-1] == ',')
            acAddr1[iTmp-1] = 0;
         remUnPrtChar(acAddr1);

         if (*apTokens[R_MAILSTATE] > ' ')
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY);
            vmemcpy(pOutbuf+OFF_M_ST, apTokens[R_MAILSTATE], 2);

            iTmp = sprintf(acTmp, "%s, %s %s", acAddr1, myTrim(apTokens[R_MAILSTATE]), apTokens[R_MAILZIPCODE]);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
         } else
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY+SIZ_M_ST+SIZ_M_ZIP);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, SIZ_M_CTY_ST_D);
         }
      }

      iTmp = atol(apTokens[R_MAILZIPCODE]);
      if (iTmp > 500)
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[R_MAILZIPCODE], SIZ_M_ZIP);
   }
}

void Men_MergeMailing2(char *pOutbuf)
{
   char        acTmp[256], acAddr1[64], *pAddr1;
   int         iTmp;
   bool        bUnitAvail = false;

   ADR_REC     sMailAdr;

   // Check for blank address
   replUnPrtChar(apTokens[R2_MAILADDRESS]);
   pAddr1 = apTokens[R2_MAILADDRESS];
   if (memcmp(pAddr1, "     ", 5) && memcmp(pAddr1, "UNKNOWN", 7))
   {
      strcpy(acAddr1, pAddr1);
      pAddr1 = myTrim(acAddr1);
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

      // Parsing mail address
      parseMAdr1(&sMailAdr, pAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }

      if (sMailAdr.strDir[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);

      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);

      if (sMailAdr.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);

      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            vmemcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, SIZ_M_UNITNO);
         }
      }

      if (bUnitAvail)
      {
         iTmp = sprintf(acTmp, "%s #%s", acAddr1, sMailAdr.Unit);
         vmemcpy(pOutbuf+OFF_M_ADDR_D, acTmp, SIZ_M_ADDR_D, iTmp);
      } else
         vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "607224002", 9) )
      //   iTmp = 0;
#endif

      // City/State
      if (*apTokens[R2_MAILCITY] > ' ')
      {
         // Remove quote & comma at the end of city name
         strcpy(acAddr1, apTokens[R2_MAILCITY]);
         iTmp = iTrim(acAddr1);
         if (acAddr1[iTmp-1] == ',')
            acAddr1[iTmp-1] = 0;
         remUnPrtChar(acAddr1);

         if (*apTokens[R2_MAILSTATE] > ' ')
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY);
            vmemcpy(pOutbuf+OFF_M_ST, apTokens[R2_MAILSTATE], 2);

            iTmp = sprintf(acTmp, "%s, %s %s", acAddr1, myTrim(apTokens[R2_MAILSTATE]), apTokens[R2_MAILZIPCODE]);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
         } else
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY+SIZ_M_ST+SIZ_M_ZIP);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, SIZ_M_CTY_ST_D);
         }
      }

      iTmp = atol(apTokens[R2_MAILZIPCODE]);
      if (iTmp > 500)
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[R2_MAILZIPCODE], SIZ_M_ZIP);
   }
}

/******************************** Men_MergeSitus ****************************
 *
 *
 ****************************************************************************/

int Men_MergeSitus(char *pOutbuf)
{
   static   char acRec[256], *pRec=NULL;
   char     *pTmp, acTmp[256], acTmp1[64], acAddr1[64], acSfx[8];
   int      lTmp, iRet, iTmp;

   // Initialize
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MEN_L_S_STRNUM]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MEN_L_S_STRNUM], strlen(apTokens[MEN_L_S_STRNUM]));
      if (*apTokens[MEN_L_S_STRFRA] > ' ')
      {
         vmemcpy(pOutbuf+OFF_S_STR_SUB, apTokens[MEN_L_S_STRFRA], SIZ_S_STR_SUB);
         strcat(acAddr1, apTokens[MEN_L_S_STRFRA]);
         strcat(acAddr1, " ");
      }
   }

   // DIR - N, S, E, W, NO, SO
   if (*apTokens[MEN_L_S_DIR] > ' ')
   {
      *(pOutbuf+OFF_S_DIR) = *apTokens[MEN_L_S_DIR];
      acTmp[0] = *apTokens[MEN_L_S_DIR];
      acTmp[1] = ' ';
      acTmp[2] = 0;
      strcat(acAddr1, acTmp);
   }
   vmemcpy(pOutbuf+OFF_S_STREET, _strupr(apTokens[MEN_L_S_STRNAME]), SIZ_S_STREET);
   acSfx[0] = 0;
   if (*apTokens[MEN_L_S_STRSFX] > ' ')
   {
      // Translate to code
      strcpy(acTmp, apTokens[MEN_L_S_STRSFX]);
      iTmp = GetSfxCodeX(acTmp, acSfx);
      if (iTmp)
      {
         iRet = sprintf(acTmp1, "%d", iTmp);
         memcpy(pOutbuf+OFF_S_SUFF, acTmp1, iRet);
      } else
      {
         LogMsg0("*** Unknown suffix %s [%s], append to street name", acTmp, apTokens[MEN_L_PIN]);
         strcpy(acSfx, acTmp);
         iTmp = sprintf(acTmp, "%s %s", apTokens[MEN_L_S_STRNAME], acSfx);
         vmemcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET, iTmp);
      }
   }

   sprintf(acTmp, "%s %s ", apTokens[MEN_L_S_STRNAME], acSfx);
   strcat(acAddr1, acTmp);

   if (*apTokens[MEN_L_S_UNIT] > '0')
   {
      // Remove garbage
      if (pTmp = strchr(apTokens[MEN_L_S_UNIT], ')'))
         *pTmp = 0;

      if (*apTokens[MEN_L_S_UNIT] == '#')
         iTmp = 0;
      else 
      {
         iTmp = 1;
         *(pOutbuf+OFF_S_UNITNO) = '#';
         strcat(acAddr1, "#");
      }
      memcpy(pOutbuf+OFF_S_UNITNO+iTmp, apTokens[MEN_L_S_UNIT], strlen(apTokens[MEN_L_S_UNIT]));
      strcat(acAddr1, apTokens[MEN_L_S_UNIT]);
   } else if (*(pOutbuf+OFF_M_UNITNO) > ' ' && *(pOutbuf+OFF_HO_FL) == '1' && 
      !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
      !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) )
   {
      memcpy(pOutbuf+OFF_S_UNITNO, pOutbuf+OFF_M_UNITNO, SIZ_M_UNITNO);
      sprintf(acTmp, " %.*s", SIZ_M_UNITNO, pOutbuf+OFF_M_UNITNO);
      strcat(acAddr1, acTmp);
   }

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

   // Zipcode
   int lZip = atoin(apTokens[MEN_L_S_ZIP], 5);
   if (lZip > 90000)
      memcpy(pOutbuf+OFF_S_ZIP, apTokens[MEN_L_S_ZIP], SIZ_S_ZIP);
   else if (*apTokens[MEN_L_S_CITY] >= 'A' && !memcmp(apTokens[MEN_L_S_CITY], pOutbuf+OFF_M_CITY, strlen(apTokens[MEN_L_S_CITY])))
   {
      lZip = atoin(pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
      if (lZip > 90000)
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
   }

   // City
   if (*apTokens[MEN_L_S_CITY] > ' ')
   {
      char sCity[32], sCode[32];

      iTmp = City2CodeEx(_strupr(apTokens[MEN_L_S_CITY]), sCode, sCity, apTokens[MEN_L_PIN]);
      if (iTmp > 0)
      {
         vmemcpy(pOutbuf+OFF_S_CITY, sCode, SIZ_S_CITY);
      } else
         strcpy(sCity, apTokens[MEN_L_S_CITY]);

      if (*apTokens[MEN_L_S_ZIP] == '9')
         vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[MEN_L_S_ZIP], SIZ_S_ZIP);

      iTmp = sprintf(acTmp, "%s, CA %s", sCity, apTokens[MEN_L_S_ZIP]);
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
   }

   // State
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   return 0;
}

int Men_MergeSitus1(char *pOutbuf)
{
   static   char acRec[256], *pRec=NULL;
   char     *pTmp, acTmp[256], acTmp1[64], acAddr1[64], acSfx[8];
   int      lTmp, iRet, iTmp;

   // Initialize
   acAddr1[0] = 0;
   lTmp = atol(apTokens[R_SITUSSTREETNUMBER]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[R_SITUSSTREETNUMBER], strlen(apTokens[R_SITUSSTREETNUMBER]));
      if (*apTokens[R_SITUSSTREETNUMBERSUFFIX] > ' ')
      {
         vmemcpy(pOutbuf+OFF_S_STR_SUB, apTokens[R_SITUSSTREETNUMBERSUFFIX], SIZ_S_STR_SUB);
         strcat(acAddr1, apTokens[R_SITUSSTREETNUMBERSUFFIX]);
         strcat(acAddr1, " ");
      }
   }

   // DIR - N, S, E, W, NO, SO
   if (*apTokens[R_SITUSSTREETPREDIRECTIONAL] > ' ')
   {
      *(pOutbuf+OFF_S_DIR) = *apTokens[R_SITUSSTREETPREDIRECTIONAL];
      acTmp[0] = *apTokens[R_SITUSSTREETPREDIRECTIONAL];
      acTmp[1] = ' ';
      acTmp[2] = 0;
      strcat(acAddr1, acTmp);
   }
   vmemcpy(pOutbuf+OFF_S_STREET, _strupr(apTokens[R_SITUSSTREETNAME]), SIZ_S_STREET);
   acSfx[0] = 0;
   if (*apTokens[R_SITUSSTREETTYPE] > ' ')
   {
      // Translate to code
      strcpy(acTmp, apTokens[R_SITUSSTREETTYPE]);
      iTmp = GetSfxCodeX(acTmp, acSfx);
      if (iTmp)
      {
         iRet = sprintf(acTmp1, "%d", iTmp);
         memcpy(pOutbuf+OFF_S_SUFF, acTmp1, iRet);
      } else
      {
         LogMsg0("*** Unknown suffix %s [%s], append to street name", acTmp, apTokens[R_PIN]);
         strcpy(acSfx, acTmp);
         iTmp = sprintf(acTmp, "%s %s", apTokens[R_SITUSSTREETNAME], acSfx);
         vmemcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET, iTmp);
      }
   }

   sprintf(acTmp, "%s %s ", apTokens[R_SITUSSTREETNAME], acSfx);
   strcat(acAddr1, acTmp);

   if (*apTokens[R_SITUSUNITNUMBER] > '0')
   {
      // Remove garbage
      if (pTmp = strchr(apTokens[R_SITUSUNITNUMBER], ')'))
         *pTmp = 0;

      if (*apTokens[R_SITUSUNITNUMBER] == '#')
         iTmp = 0;
      else 
      {
         iTmp = 1;
         *(pOutbuf+OFF_S_UNITNO) = '#';
         strcat(acAddr1, "#");
      }
      memcpy(pOutbuf+OFF_S_UNITNO+iTmp, apTokens[R_SITUSUNITNUMBER], strlen(apTokens[R_SITUSUNITNUMBER]));
      strcat(acAddr1, apTokens[R_SITUSUNITNUMBER]);
   } else if (*(pOutbuf+OFF_M_UNITNO) > ' ' && *(pOutbuf+OFF_HO_FL) == '1' && 
      !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
      !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) )
   {
      memcpy(pOutbuf+OFF_S_UNITNO, pOutbuf+OFF_M_UNITNO, SIZ_M_UNITNO);
      sprintf(acTmp, " %.*s", SIZ_M_UNITNO, pOutbuf+OFF_M_UNITNO);
      strcat(acAddr1, acTmp);
   }

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

   // Zipcode
   int lZip = atoin(apTokens[R_SITUSZIPCODE], 5);
   if (lZip > 90000)
      memcpy(pOutbuf+OFF_S_ZIP, apTokens[R_SITUSZIPCODE], SIZ_S_ZIP);
   else if (*apTokens[R_SITUSCITYNAME] >= 'A' && !memcmp(apTokens[R_SITUSCITYNAME], pOutbuf+OFF_M_CITY, strlen(apTokens[R_SITUSCITYNAME])))
   {
      lZip = atoin(pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
      if (lZip > 90000)
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
   }

   // City
   if (*apTokens[R_SITUSCITYNAME] > ' ')
   {
      char sCity[32], sCode[32];

      iTmp = City2CodeEx(_strupr(apTokens[R_SITUSCITYNAME]), sCode, sCity, apTokens[R_PIN]);
      if (iTmp > 0)
      {
         vmemcpy(pOutbuf+OFF_S_CITY, sCode, SIZ_S_CITY);
      } else
         strcpy(sCity, apTokens[R_SITUSCITYNAME]);

      if (*apTokens[R_SITUSZIPCODE] == '9')
         vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[R_SITUSZIPCODE], SIZ_S_ZIP);

      iTmp = sprintf(acTmp, "%s, CA %s", sCity, apTokens[R_SITUSZIPCODE]);
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
   }

   // State
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   return 0;
}

int Men_MergeSitus2(char *pOutbuf)
{
   static   char acRec[256], *pRec=NULL;
   char     *pTmp, acTmp[256], acTmp1[64], acAddr1[64], acSfx[8];
   int      lTmp, iRet, iTmp;

   // Initialize
   acAddr1[0] = 0;
   lTmp = atol(apTokens[R2_SITUSSTREETNUMBER]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[R2_SITUSSTREETNUMBER], strlen(apTokens[R2_SITUSSTREETNUMBER]));
      if (*apTokens[R2_SITUSSTREETNUMBERSUFFIX] > ' ')
      {
         vmemcpy(pOutbuf+OFF_S_STR_SUB, apTokens[R2_SITUSSTREETNUMBERSUFFIX], SIZ_S_STR_SUB);
         strcat(acAddr1, apTokens[R2_SITUSSTREETNUMBERSUFFIX]);
         strcat(acAddr1, " ");
      }
   }

   // DIR - N, S, E, W, NO, SO
   if (*apTokens[R2_SITUSSTREETPREDIRECTIONAL] > ' ')
   {
      *(pOutbuf+OFF_S_DIR) = *apTokens[R2_SITUSSTREETPREDIRECTIONAL];
      acTmp[0] = *apTokens[R2_SITUSSTREETPREDIRECTIONAL];
      acTmp[1] = ' ';
      acTmp[2] = 0;
      strcat(acAddr1, acTmp);
   }
   vmemcpy(pOutbuf+OFF_S_STREET, _strupr(apTokens[R2_SITUSSTREETNAME]), SIZ_S_STREET);
   acSfx[0] = 0;
   if (*apTokens[R2_SITUSSTREETTYPE] > ' ')
   {
      // Translate to code
      strcpy(acTmp, apTokens[R2_SITUSSTREETTYPE]);
      iTmp = GetSfxCodeX(acTmp, acSfx);
      if (iTmp)
      {
         iRet = sprintf(acTmp1, "%d", iTmp);
         memcpy(pOutbuf+OFF_S_SUFF, acTmp1, iRet);
      } else
      {
         LogMsg0("*** Unknown suffix %s [%s], append to street name", acTmp, apTokens[R2_PIN]);
         strcpy(acSfx, acTmp);
         iTmp = sprintf(acTmp, "%s %s", apTokens[R2_SITUSSTREETNAME], acSfx);
         vmemcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET, iTmp);
      }
   }

   sprintf(acTmp, "%s %s ", apTokens[R2_SITUSSTREETNAME], acSfx);
   strcat(acAddr1, acTmp);

   if (*apTokens[R2_SITUSUNITNUMBER] > '0')
   {
      // Remove garbage
      if (pTmp = strchr(apTokens[R2_SITUSUNITNUMBER], ')'))
         *pTmp = 0;

      if (*apTokens[R2_SITUSUNITNUMBER] == '#')
         iTmp = 0;
      else 
      {
         iTmp = 1;
         *(pOutbuf+OFF_S_UNITNO) = '#';
         strcat(acAddr1, "#");
      }
      memcpy(pOutbuf+OFF_S_UNITNO+iTmp, apTokens[R2_SITUSUNITNUMBER], strlen(apTokens[R2_SITUSUNITNUMBER]));
      strcat(acAddr1, apTokens[R2_SITUSUNITNUMBER]);
   } else if (*(pOutbuf+OFF_M_UNITNO) > ' ' && *(pOutbuf+OFF_HO_FL) == '1' && 
      !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
      !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) )
   {
      memcpy(pOutbuf+OFF_S_UNITNO, pOutbuf+OFF_M_UNITNO, SIZ_M_UNITNO);
      sprintf(acTmp, " %.*s", SIZ_M_UNITNO, pOutbuf+OFF_M_UNITNO);
      strcat(acAddr1, acTmp);
   }

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

   // Zipcode
   int lZip = atoin(apTokens[R2_SITUSZIPCODE], 5);
   if (lZip > 90000)
      memcpy(pOutbuf+OFF_S_ZIP, apTokens[R2_SITUSZIPCODE], SIZ_S_ZIP);
   else if (*apTokens[R2_SITUSCITYNAME] >= 'A' && !memcmp(apTokens[R2_SITUSCITYNAME], pOutbuf+OFF_M_CITY, strlen(apTokens[R2_SITUSCITYNAME])))
   {
      lZip = atoin(pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
      if (lZip > 90000)
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
   }

   // City
   if (*apTokens[R2_SITUSCITYNAME] > ' ')
   {
      char sCity[32], sCode[32];

      iTmp = City2CodeEx(_strupr(apTokens[R2_SITUSCITYNAME]), sCode, sCity, apTokens[R2_PIN]);
      if (iTmp > 0)
      {
         vmemcpy(pOutbuf+OFF_S_CITY, sCode, SIZ_S_CITY);
      } else
         strcpy(sCity, apTokens[R2_SITUSCITYNAME]);

      if (*apTokens[R2_SITUSZIPCODE] == '9')
         vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[R2_SITUSZIPCODE], SIZ_S_ZIP);

      iTmp = sprintf(acTmp, "%s, CA %s", sCity, apTokens[R2_SITUSZIPCODE]);
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
   }

   // State
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   return 0;
}

/****************************** Men_Load_LDR_Csv ****************************
 *
 * For LDR "ar_8_25_secured.csv".  
 * 2022 - ISTAXABLE is not populated
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Men_MergeLienCsv(char *pOutbuf, char *pRollRec, char cNonTax)
{
   char     acTmp[256], acTmp1[64], acApn[16];
   LONGLONG lTmp;
   int      iRet, iTmp, iIdx=0;

   // Parse input rec
   iTokens = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < MEN_L_COLS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[MEN_L_PIN], iTokens);
      return -1;
   }

   // Ignore non-taxable parcels - 
   if (cNonTax == 'N' && *apTokens[MEN_L_ISTAXABLE] == 'N')
      return -1;

   iRet = iTrim(apTokens[MEN_L_PIN]);
   if (iRet != iApnLen)
   {
      if (bDebug)
         LogMsg("*** Drop bad parcel: %s", apTokens[MEN_L_PIN]);
      return -1;
   }

   strcpy(acApn, apTokens[MEN_L_PIN]);

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, acApn, iApnLen);

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[MEN_L_GEO], strlen(apTokens[MEN_L_GEO]));

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "23MENA", 6);

   // status

   // TRA
   vmemcpy(pOutbuf+OFF_TRA, apTokens[MEN_L_TRA], 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[MEN_L_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[MEN_L_IMPROVEMENTS]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: 
   long lFixtr  = atoi(apTokens[MEN_L_TRADEFIXTURESAMOUNT]);
   long lPers   = atoi(apTokens[MEN_L_PERSONALVALUE]);
   long lLivImpr= atoi(apTokens[MEN_L_LIVINGIMPROVEMENTS]);
   lTmp = lFixtr+lPers+lLivImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lLivImpr > 0)
      {
         sprintf(acTmp, "%d         ", lLivImpr);
         memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
      }
   }

   // Gross total
   LONGLONG lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lHO_Exe = atol(apTokens[MEN_L_HOX]);
   ULONG lOE_Exe = atol(apTokens[MEN_L_OTHEREXEMPTION]);
   ULONG lVE_Exe = atol(apTokens[MEN_L_VETERANSEXEMPTION]);
   ULONG lDV_Exe = atol(apTokens[MEN_L_DISABLEDVETERANSEXEMPTION]);
   ULONG lCH_Exe = atol(apTokens[MEN_L_CHURCHEXEMPTION]);
   ULONG lRE_Exe = atol(apTokens[MEN_L_RELIGIOUSEXEMPTION]);
   ULONG lCE_Exe = atol(apTokens[MEN_L_CEMETERYEXEMPTION]);
   ULONG lPS_Exe = atol(apTokens[MEN_L_PUBLICSCHOOLEXEMPTION]);
   ULONG lPL_Exe = atol(apTokens[MEN_L_PUBLICLIBRARYEXEMPTION]);
   ULONG lPM_Exe = atol(apTokens[MEN_L_PUBLICMUSEUMEXEMPTION]);
   ULONG lWH_Exe = atol(apTokens[MEN_L_WELFAREHOSPITALEXEMPTION]);
   ULONG lWS_Exe = atol(apTokens[MEN_L_WPP_SCHOOLEXEMPTION]);
   ULONG lWR_Exe = atol(apTokens[MEN_L_WC_RELIGIOUSEXEMPTION]);
   LONGLONG lTotalExe = lHO_Exe+lOE_Exe+lVE_Exe+lDV_Exe+lCH_Exe+lRE_Exe+lCE_Exe+lPS_Exe+lPL_Exe+lPM_Exe+
      lWH_Exe+lWS_Exe+lWR_Exe;
   if (lTotalExe > 0)
   {
      if (lTotalExe > lGross)
         lTotalExe = lGross;
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "138470010", 9))
   //   iTmp = 0;
   //if (lOE_Exe > 0 && lOE_Exe != lTotalExe)
   //   iTmp = 0;
#endif

   if (lHO_Exe > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "HO", 2);
   } else
   {
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      iIdx = OFF_EXE_CD1;
      iTmp = 0;

      if (lVE_Exe > 1)
      {
         memcpy(pOutbuf+iIdx, "VE", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lDV_Exe > 1)
      {
         memcpy(pOutbuf+iIdx, "DV", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lCE_Exe > 1)
      {
         memcpy(pOutbuf+iIdx, "CH", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lRE_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "RE", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lCE_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "CE", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lPS_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "PS", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lPL_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "PL", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lPM_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "PM", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lWH_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "WH", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lWS_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "WS", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lWR_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "WR", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lOE_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "OE", 2);
         iTmp++;
      }
   }

#ifdef _DEBUG
   //if (iTmp > 3)
   //   iTmp = 0;
#endif

   // Populate per bulk customer request 07/20/2022
   lTmp = atol(apTokens[MEN_L_TENPERCENTASSESSEDPENALTY]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LATE_PENALTY, lTmp);
      memcpy(pOutbuf+OFF_LATE_PENALTY, acTmp, SIZ_LATE_PENALTY);
   }

   // Set full exemption flag 
   if (lGross == lTotalExe)
   {
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   //} else if (isdigit(*apTokens[MEN_L_TAXABILITYCODE]))
   //{
   //   // For 2020, there is no tax code.  The new field ISTAXABLE contains Y/N
   //   vmemcpy(pOutbuf+OFF_TAX_CODE, apTokens[MEN_L_TAXABILITYCODE], SIZ_TAX_CODE);
   //   if (*apTokens[MEN_L_TAXABILITYCODE] < '4')
   //      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009000009", 9) )
   //   iTmp = 0;
#endif

   // UseCode - LDR 2022
   if (*apTokens[MEN_L_CLASSCODE] > ' ')
   {
      strcpy(acTmp, myTrim(apTokens[MEN_L_CLASSCODE]));
      vmemcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, pOutbuf+OFF_USE_CO, SIZ_USE_CO, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Lot acreage
   double dAcreage = atof(apTokens[MEN_L_ACREAGEAMOUNT]);
   if (dAcreage > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dAcreage*1000));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      // If number is too big, we don't want to display in sqft
      if (dAcreage < MAX_LOTACRES)
      {
         lTmp = (ULONG)(dAcreage * SQFT_PER_ACRE);
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }
   }

   // Block/Lot/Track
   if (!_memicmp(apTokens[MEN_L_LOTTYPE], "LOT", 3))
      vmemcpy(pOutbuf+OFF_LOT, apTokens[MEN_L_LOT], SIZ_LOT);

   // Legal
   _strupr(apTokens[MEN_L_ASSESSMENTDESCRIPTION]);
   remJSChar(apTokens[MEN_L_ASSESSMENTDESCRIPTION]);
   iRet = updateLegal(pOutbuf, apTokens[MEN_L_ASSESSMENTDESCRIPTION]);
   if (iRet > iMaxLegal)
      iMaxLegal = iRet;

   // Owner
   iRet = replUnPrtChar(apTokens[MEN_L_LEGALPARTY1]);
   if (iRet > 0)
      blankRem(apTokens[MEN_L_LEGALPARTY1]);
   Men_MergeLOwner(pOutbuf, apTokens[MEN_L_LEGALPARTY1], 1);
   if (*apTokens[MEN_L_LEGALPARTY2] > ' ')
      Men_MergeLOwner(pOutbuf, apTokens[MEN_L_LEGALPARTY2], 2);

   // Situs - always populate situs after mailing
   Men_MergeSitus(pOutbuf);

   // Mailing
   Men_MergeMailing(pOutbuf);

   // SetTaxcode, Prop8 flag, FullExe flag
   //iTmp = updateTaxCode(pOutbuf, apTokens[MEN_L_TAXABILITYCODE], true, true);

   if (*apTokens[MEN_L_ISTAXABLE] == 'N')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   return 0;
}

int Men_Load_LDR_Csv(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   long     lRet=0, lCnt;
   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   HANDLE   fhOut;

   // If LDR file has been sorted by -Xl, no need to resort
   if (!strstr(acRollFile, "Lien.srt"))
   {
      // Check for Unicode
      sprintf(acTmpFile, "%s\\MEN\\Men_Lien.txt", acTmpPath);
      if (!UnicodeToAnsi(acRollFile, acTmpFile))
      {
         // Sort on PIN
         sprintf(acOutFile, "%s\\MEN\\Men_Lien.srt", acTmpPath);
         if (sortFile(acTmpFile, acOutFile, "S(#1,C,A) DEL(34) F(TXT)", &iRet) > 0)
            strcpy(acRollFile, acOutFile);
         else
            return iRet;
      }
   } 

   // Open Lien file
   LogMsg("Open lien file %s", acRollFile);
   fdLien = fopen(acRollFile, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", acCharFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);

   lCnt = 1;

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLien))
   {
      // Create new R01 record
      iRet = Men_MergeLienCsv(acBuf, acRec, 'N');     // 2022
      if (!iRet)
      {
         if (fdChar)
            iRet = Men_MergeStdChar(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else
         LogMsg("*** Bad input record %s", acRec);

      // Get next roll record
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);

      if (!pTmp || *pTmp > '9')
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg(" Total input records:  %u", lCnt);
   LogMsg("      output records:  %u", lLDRRecCount);
   LogMsg("        Char matched:  %u", lCharMatch);
   LogMsg("         Char skiped:  %u", lCharSkip);
   LogMsg("    bad-city records:  %u", iBadCity);
   LogMsg("  bad-suffix records:  %u\n", iBadSuffix);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return lRet;
}

int Men_MergeLienCsv1(char *pOutbuf, char *pRollRec, char cNonTax)
{
   char     acTmp[256], acTmp1[64], acApn[16];
   LONGLONG lTmp;
   int      iRet, iTmp, iIdx=0;

   // Parse input rec
   iTokens = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < R_COLS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[R_PIN], iTokens);
      return -1;
   }

   // Ignore non-taxable parcels - 
   if (cNonTax == 'N' && *apTokens[R_ISTAXABLE] == 'N')
      return -1;

   iRet = iTrim(apTokens[R_PIN]);
   if (iRet != iApnLen)
   {
      if (bDebug)
         LogMsg("*** Drop bad parcel: %s", apTokens[R_PIN]);
      return -1;
   }

   strcpy(acApn, apTokens[R_PIN]);

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, acApn, iApnLen);

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[R_GEO], strlen(apTokens[R_GEO]));

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "23MENA", 6);

   // status

   // TRA
   vmemcpy(pOutbuf+OFF_TRA, apTokens[R_TRA], 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[R_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[R_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: 
   long lFixtr  = atoi(apTokens[R_TRADEFIXTURESAMOUNT]);
   long lPers   = atoi(apTokens[R_PERSONALVALUE]);
   long lLivImpr= atoi(apTokens[R_LIVINGIMPROVEMENTS]);
   lTmp = lFixtr+lPers+lLivImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lLivImpr > 0)
      {
         sprintf(acTmp, "%d         ", lLivImpr);
         memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
      }
   }

   // Gross total
   LONGLONG lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lHO_Exe = atol(apTokens[R_HOX]);
   ULONG lOE_Exe = atol(apTokens[R_OTHEREXEMPTION]);
   ULONG lVE_Exe = atol(apTokens[R_VETERANSEXEMPTION]);
   ULONG lDV_Exe = atol(apTokens[R_DISABLEDVETERANSEXEMPTION]);
   ULONG lCH_Exe = atol(apTokens[R_CHURCHEXEMPTION]);
   ULONG lRE_Exe = atol(apTokens[R_RELIGIOUSEXEMPTION]);
   ULONG lCE_Exe = atol(apTokens[R_CEMETERYEXEMPTION]);
   ULONG lPS_Exe = atol(apTokens[R_PUBLICSCHOOLEXEMPTION]);
   ULONG lPL_Exe = atol(apTokens[R_PUBLICLIBRARYEXEMPTION]);
   ULONG lPM_Exe = atol(apTokens[R_PUBLICMUSEUMEXEMPTION]);
   ULONG lWH_Exe = atol(apTokens[R_WELFAREHOSPITALEXEMPTION]);
   ULONG lWS_Exe = atol(apTokens[R_WELFAREPRIVATEPAROCHIALSCHOOLEXEMPTION]);
   ULONG lWR_Exe = atol(apTokens[R_WELFARECHARITYRELIGIOUSEXEMPTION]);
   LONGLONG lTotalExe = lHO_Exe+lOE_Exe+lVE_Exe+lDV_Exe+lCH_Exe+lRE_Exe+lCE_Exe+lPS_Exe+lPL_Exe+lPM_Exe+
      lWH_Exe+lWS_Exe+lWR_Exe;
   if (lTotalExe > 0)
   {
      if (lTotalExe > lGross)
         lTotalExe = lGross;
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "138470010", 9))
   //   iTmp = 0;
   //if (lOE_Exe > 0 && lOE_Exe != lTotalExe)
   //   iTmp = 0;
#endif

   if (lHO_Exe > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "HO", 2);
   } else
   {
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      iIdx = OFF_EXE_CD1;
      iTmp = 0;

      if (lVE_Exe > 1)
      {
         memcpy(pOutbuf+iIdx, "VE", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lDV_Exe > 1)
      {
         memcpy(pOutbuf+iIdx, "DV", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lCE_Exe > 1)
      {
         memcpy(pOutbuf+iIdx, "CH", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lRE_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "RE", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lCE_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "CE", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lPS_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "PS", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lPL_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "PL", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lPM_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "PM", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lWH_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "WH", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lWS_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "WS", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lWR_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "WR", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lOE_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "OE", 2);
         iTmp++;
      }
   }

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&MEN_Exemption);

#ifdef _DEBUG
   //if (iTmp > 3)
   //   iTmp = 0;
#endif

   // Populate per bulk customer request 07/20/2022
   lTmp = atol(apTokens[R_TENPERCENTASSESSEDPENALTY]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LATE_PENALTY, lTmp);
      memcpy(pOutbuf+OFF_LATE_PENALTY, acTmp, SIZ_LATE_PENALTY);
   }

   // Set full exemption flag 
   if (lGross == lTotalExe)
   {
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   }


#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009000009", 9) )
   //   iTmp = 0;
#endif

   // UseCode - LDR 2022
   if (*apTokens[R_CLASSCODE] > ' ')
   {
      strcpy(acTmp, myTrim(apTokens[R_CLASSCODE]));
      vmemcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, pOutbuf+OFF_USE_CO, SIZ_USE_CO, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Lot acreage
   double dAcreage = atof(apTokens[R_ACREAGEAMOUNT]);
   if (dAcreage > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dAcreage*1000));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      // If number is too big, we don't want to display in sqft
      if (dAcreage < MAX_LOTACRES)
      {
         lTmp = (ULONG)(dAcreage * SQFT_PER_ACRE);
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }
   }

   // Block/Lot/Track
   //if (!_memicmp(apTokens[R_LOTTYPE], "LOT", 3))
   //   vmemcpy(pOutbuf+OFF_LOT, apTokens[R_LOT], SIZ_LOT);

   // Legal
   _strupr(apTokens[R_ASSESSMENTDESCRIPTION]);
   remJSChar(apTokens[R_ASSESSMENTDESCRIPTION]);
   iRet = updateLegal(pOutbuf, apTokens[R_ASSESSMENTDESCRIPTION]);
   if (iRet > iMaxLegal)
      iMaxLegal = iRet;

   // Owner
   iRet = replUnPrtChar(apTokens[R_LEGALPARTY1]);
   if (iRet > 0)
      blankRem(apTokens[R_LEGALPARTY1]);
   Men_MergeLOwner(pOutbuf, apTokens[R_LEGALPARTY1], 1);
   if (*apTokens[R_LEGALPARTY2] > ' ')
      Men_MergeLOwner(pOutbuf, apTokens[R_LEGALPARTY2], 2);

   // Situs - always populate situs after mailing
   Men_MergeSitus1(pOutbuf);

   // Mailing
   Men_MergeMailing1(pOutbuf);

   // SetTaxcode, Prop8 flag, FullExe flag
   //iTmp = updateTaxCode(pOutbuf, apTokens[R_TAXABILITYCODE], true, true);

   //if (*apTokens[R_ISTAXABLE] == 'N')
   //   *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   return 0;
}

int Men_MergeLienCsv2(char *pOutbuf, char *pRollRec, char cNonTax)
{
   char     acTmp[256], acTmp1[64], acApn[16];
   LONGLONG lTmp;
   int      iRet, iTmp, iIdx=0;

   // Parse input rec
   iTokens = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < R_COLS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[R2_PIN], iTokens);
      return -1;
   }

   // Ignore non-taxable parcels - 
   if (cNonTax == 'N' && *apTokens[R2_ISTAXABLE] == 'N')
      return -1;

   iRet = iTrim(apTokens[R2_PIN]);
   if (iRet != iApnLen)
   {
      if (bDebug)
         LogMsg("*** Drop bad parcel: %s", apTokens[R2_PIN]);
      return -1;
   }

   strcpy(acApn, apTokens[R2_PIN]);

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, acApn, iApnLen);

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[R2_GEO], strlen(apTokens[R2_GEO]));

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "23MENA", 6);

   // status

   // TRA
   vmemcpy(pOutbuf+OFF_TRA, apTokens[R2_TRA], 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[R2_LAND]);
   sprintf(acTmp, "%*u", SIZ_LAND, lLand);
   memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);

   // Improve
   long lImpr = atoi(apTokens[R2_IMPR]);
   sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
   memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1600142200", 10))
   //   iTmp = 0;
#endif

   // Other value: 
   long lFixtr  = atoi(apTokens[R2_TRADEFIXTURESAMOUNT]);
   long lPers   = atoi(apTokens[R2_PERSONALVALUE]);
   long lLivImpr= atoi(apTokens[R2_LIVINGIMPROVEMENTS]);
   lTmp = lFixtr+lPers+lLivImpr;
   sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
   memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
   sprintf(acTmp, "%d         ", lFixtr);
   memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
   sprintf(acTmp, "%d         ", lPers);
   memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
   sprintf(acTmp, "%d         ", lLivImpr);
   memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);

   // Gross total
   LONGLONG lGross = lTmp+lLand+lImpr;
   sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
   memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

   // Ratio
   if ((lLand+lImpr) > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lHO_Exe = atol(apTokens[R2_HOX]);
   ULONG lOE_Exe = atol(apTokens[R2_OTHEREXEMPTION]);
   ULONG lVE_Exe = atol(apTokens[R2_VETERANSEXEMPTION]);
   ULONG lDV_Exe = atol(apTokens[R2_DISABLEDVETERANSEXEMPTION]);
   ULONG lCH_Exe = atol(apTokens[R2_CHURCHEXEMPTION]);
   ULONG lRE_Exe = atol(apTokens[R2_RELIGIOUSEXEMPTION]);
   ULONG lCE_Exe = atol(apTokens[R2_CEMETERYEXEMPTION]);
   ULONG lPS_Exe = atol(apTokens[R2_PUBLICSCHOOLEXEMPTION]);
   ULONG lPL_Exe = atol(apTokens[R2_PUBLICLIBRARYEXEMPTION]);
   ULONG lPM_Exe = atol(apTokens[R2_PUBLICMUSEUMEXEMPTION]);
   ULONG lWH_Exe = atol(apTokens[R2_WELFAREHOSPITALEXEMPTION]);
   ULONG lWS_Exe = atol(apTokens[R2_WELFAREPRIVATEPAROCHIALSCHOOLEXEMPTION]);
   ULONG lWR_Exe = atol(apTokens[R2_WELFARECHARITYRELIGIOUSEXEMPTION]);
   LONGLONG lTotalExe = lHO_Exe+lOE_Exe+lVE_Exe+lDV_Exe+lCH_Exe+lRE_Exe+lCE_Exe+lPS_Exe+lPL_Exe+lPM_Exe+
      lWH_Exe+lWS_Exe+lWR_Exe;
   if (lTotalExe > lGross)
      lTotalExe = lGross;
   sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
   memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

   if (lHO_Exe > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "HO", 2);
   } else
   {
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      iIdx = OFF_EXE_CD1;
      iTmp = 0;

      if (lVE_Exe > 1)
      {
         memcpy(pOutbuf+iIdx, "VE", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lDV_Exe > 1)
      {
         memcpy(pOutbuf+iIdx, "DV", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lCE_Exe > 1)
      {
         memcpy(pOutbuf+iIdx, "CH", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lRE_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "RE", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lCE_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "CE", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lPS_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "PS", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lPL_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "PL", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lPM_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "PM", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lWH_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "WH", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lWS_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "WS", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lWR_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "WR", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lOE_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "OE", 2);
         iTmp++;
      }
   }

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&MEN_Exemption);

#ifdef _DEBUG
   //if (iTmp > 3)
   //   iTmp = 0;
#endif

   // Populate per bulk customer request 07/20/2022
   lTmp = atol(apTokens[R2_TENPERCENTASSESSEDPENALTY]);
   sprintf(acTmp, "%*u", SIZ_LATE_PENALTY, lTmp);
   memcpy(pOutbuf+OFF_LATE_PENALTY, acTmp, SIZ_LATE_PENALTY);

   // Set full exemption flag 
   if (lGross == lTotalExe)
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   else
      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009000009", 9) )
   //   iTmp = 0;
#endif

   // UseCode - LDR 2022
   if (*apTokens[R2_CLASSCODE] > ' ')
   {
      strcpy(acTmp, myTrim(apTokens[R2_CLASSCODE]));
      vmemcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, pOutbuf+OFF_USE_CO, SIZ_USE_CO, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Lot acreage
   double dAcreage = atof(apTokens[R2_ACREAGEAMOUNT]);
   if (dAcreage > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dAcreage*1000));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      // If number is too big, we don't want to display in sqft
      if (dAcreage < MAX_LOTACRES)
      {
         lTmp = (ULONG)(dAcreage * SQFT_PER_ACRE);
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }
   }

   // Legal
   _strupr(apTokens[R2_ASSESSMENTDESCRIPTION]);
   remJSChar(apTokens[R2_ASSESSMENTDESCRIPTION]);
   iRet = updateLegal(pOutbuf, apTokens[R2_ASSESSMENTDESCRIPTION]);
   if (iRet > iMaxLegal)
      iMaxLegal = iRet;

   // Owner
   iRet = replUnPrtChar(apTokens[R2_LEGALPARTY1]);
   if (iRet > 0)
      blankRem(apTokens[R2_LEGALPARTY1]);
   Men_MergeLOwner(pOutbuf, apTokens[R2_LEGALPARTY1], 1);
   if (*apTokens[R2_LEGALPARTY2] > ' ')
      Men_MergeLOwner(pOutbuf, apTokens[R2_LEGALPARTY2], 2);

   // Situs - always populate situs after mailing
   Men_MergeSitus2(pOutbuf);

   // Mailing
   Men_MergeMailing2(pOutbuf);

   return 0;
}

int Men_Load_LDR_Csv1(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet;
   long     lRet=0, lCnt, lDrop=0;
   DWORD    nBytesWritten;
   BOOL     bRet;
   HANDLE   fhOut;

   // If LDR file has been sorted by -Xl, no need to resort
   if (!strstr(acRollFile, "Lien.srt"))
   {
      // Sort on PIN
      sprintf(acOutFile, "%s\\MEN\\Men_Lien.srt", acTmpPath);
      sprintf(acRec, "S(#1,C,A) OMIT(#1,C,GT,\"A\") DEL(%d) F(TXT)", cLdrSep);
      if (sortFile(acRollFile, acOutFile, acRec, &iRet) > 0)
         strcpy(acRollFile, acOutFile);
      else
         return iRet;
   } 

   // Open Lien file
   LogMsg("Open lien file %s", acRollFile);
   fdLien = fopen(acRollFile, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", acCharFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);

   lCnt = 1;

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLien))
   {
      // Create new R01 record
      iRet = Men_MergeLienCsv1(acBuf, acRec, 'N');     // 2023
      if (!iRet)
      {
         if (fdChar)
            iRet = Men_MergeStdChar(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else
      {
         lDrop++;
      }

      // Get next roll record
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);

      if (!pTmp || *pTmp > '9')
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg(" Total input records:  %u", lCnt);
   LogMsg("      output records:  %u", lLDRRecCount);
   LogMsg("     records dropped:  %u", lDrop);
   LogMsg("        Char matched:  %u", lCharMatch);
   LogMsg("         Char skiped:  %u", lCharSkip);
   LogMsg("    bad-city records:  %u", iBadCity);
   LogMsg("  bad-suffix records:  %u\n", iBadSuffix);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return lRet;
}

/**************************** Men_Load_LDR_Csv2 ******************************
 *
 * Load LDR from "CA-Mendocino-AsmtRoll_ppa.txt".  This file includes values for
 * both secured, PP, Fixtured and Living improve.
 *
 *****************************************************************************/

int Men_Load_LDR_Csv2(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH], sApn[32];
   int      iRet;
   long     lRet=0, lCnt, lDrop=0;
   DWORD    nBytesWritten;
   BOOL     bRet;
   HANDLE   fhOut;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acRollFile, _MAX_PATH, acIniFile);

   // Sort on PIN
   sprintf(acOutFile, "%s\\MEN\\Men_Lien.srt", acTmpPath);
   sprintf(acRec, "S(#1,C,A) OMIT(#1,C,GT,\"A\") DEL(%d) F(TXT)", cLdrSep);
   if (sortFile(acRollFile, acOutFile, acRec, &iRet) > 0)
      strcpy(acRollFile, acOutFile);
   else
      return iRet;

   // Open Lien file
   LogMsg("Open lien file %s", acRollFile);
   fdLien = fopen(acRollFile, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acRollFile);
      return -1;
   }  

   // Open Value file
   //sprintf(acRec, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (!_access(acRec, 0))
   //{
   //   LogMsg("Open Lien extract file %s", acRec);
   //   fdLienExt = fopen(acRec, "r");
   //   if (fdLienExt == NULL)
   //   {
   //      LogMsg("***** Error opening lien file: %s\n", acRec);
   //      return -7;
   //   }
   //} else
   //   fdLienExt = NULL;

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);

   lCnt = 1;

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   sApn[0] = 0;

   // Merge loop 
   while (!feof(fdLien))
   {
      // Create new R01 record
      iRet = Men_MergeLienCsv2(acBuf, acRec, 'N');     // 2023
      if (!iRet)
      {
         // Merge value from LDR extract
         //if (fdLienExt)
         //{
         //   iRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MEN);
         //   if (!iRet)
         //      lLienMatch++;
         //}

         if (fdChar)
            iRet = Men_MergeStdChar(acBuf);

         memcpy(sApn, acBuf, iApnLen);
         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else
      {
         lDrop++;
      }

      // Get next roll record
      do
      {
         pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);
      } while (pTmp && !memcmp(pTmp, sApn, iApnLen));

      if (!pTmp || *pTmp > '9')
         break;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg(" Total input records:  %u", lCnt);
   LogMsg("      output records:  %u", lLDRRecCount);
   LogMsg("     records dropped:  %u", lDrop);
   LogMsg("        Char matched:  %u", lCharMatch);
   LogMsg("         Char skiped:  %u", lCharSkip);
   LogMsg("    bad-city records:  %u", iBadCity);
   LogMsg("  bad-suffix records:  %u\n", iBadSuffix);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return lRet;
}

/******************************* Men_ExtrLienCsv *****************************
 *
 * Extract values from "2021 ANNUAL ROLLS - SECURED.txt"
 *
 *****************************************************************************/

//int Men_ExtrLienRec(char *pOutbuf, char *pRollRec)
//{
//   int      iRet, lTmp, iTmp;
//   char     acTmp[64], acApn[32];
//   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;
//
//   // Replace null char with space
//   iRet = replNull(pRollRec, ' ', 0);
//
//   // Parse input rec
//   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
//   if (iRet < L_FLDS)
//   {
//      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L_APN], iRet);
//      return -1;
//   }
//
//   // Skip Personal Property
//   if (*apTokens[L_ROLLTYPE] == 'P')
//   {
//      LogMsg("*** Ignore Personal Property APN: %s", apTokens[L_APN]);
//      return 1;
//   }
//
//   // Copy APN
//   strcpy(acApn, apTokens[L_APN]);
//   iRet = iTrim(acApn);
//   if (iRet != iApnLen)
//   {
//      LogMsg("*** Ignore bad APN: %s", acApn);
//      return 1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', sizeof(LIENEXTR));
//
//   // Copy APN
//   memcpy(pLienExtr->acApn, acApn, iApnLen);
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "001100061000", 9) )
//   //   iTmp = 0;
//#endif
//
//   // Year assessed
//   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);
//
//   // Land
//   long lLand = dollar2Num(apTokens[L_LAND]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
//      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = dollar2Num(apTokens[L_IMPR]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
//      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: Fixture, PersProp, PPMH
//   long lFixtr = dollar2Num(apTokens[L_FIXTURES]);
//   long lPers  = dollar2Num(apTokens[L_PPVAL]);
//   long lGrowVal = dollar2Num(apTokens[L_LIVINGIMPR]);
//   lTmp = lFixtr+lPers+lGrowVal;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
//      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);
//
//      if (lFixtr > 0)
//      {
//         sprintf(acTmp, "%u         ", lFixtr);
//         memcpy(pLienExtr->acME_Val, acTmp, SIZ_FIXTR);
//      }
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%u         ", lPers);
//         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_PERSPROP);
//      }
//      if (lGrowVal > 0)
//      {
//         iTmp = sprintf(acTmp, "%d", lGrowVal);
//         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, iTmp);
//      }
//   }
//
//   // Gross total
//   lTmp += (lLand+lImpr);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
//      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
//   }
//
//   // Exemption
//   long lExe1 = dollar2Num(apTokens[L_HOEXE]);
//   long lTotalExe = dollar2Num(apTokens[L_TOTALEXE]);
//   if (lTotalExe > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
//      memcpy(pLienExtr->acExAmt, acTmp, SIZ_EXE_TOTAL);
//   }  
//
//   if (lExe1 > 0)
//      pLienExtr->acHO[0] = '1';      // 'Y'
//   else
//      pLienExtr->acHO[0] = '2';      // 'N'
//
//   pLienExtr->LF[0] = 10;
//   pLienExtr->LF[1] = 0;
//
//   return 0;
//}

//int Men_ExtrLienCsv(LPCSTR pCnty)
//{
//   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
//   int      iRet, iNewRec=0, lCnt=0;
//   FILE     *fdLienExt;
//
//   LogMsg0("Extract LDR values for %s", pCnty);
//
//   GetIniString(myCounty.acCntyCode, "LienFile", "", acRec, _MAX_PATH, acIniFile);
//
//   // Sort roll file on ASMT
//   sprintf(acOutFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   iRet = sortFile(acRec, acOutFile, "S(#1,N,A) F(TXT) DEL(9) ");  
//   if (!iRet)
//      return -1;
//
//   LogMsg("Open sorted LDR file %s", acOutFile);
//   fdLien = fopen(acOutFile, "r");
//   if (fdLien == NULL)
//   {
//      LogMsg("***** Error opening LDR file: %s\n", acOutFile);
//      return -2;
//   }
//
//   // Create lien extract
//   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
//   LogMsg("Create lien extract file %s", acOutFile);
//   fdLienExt = fopen(acOutFile, "w");
//   if (fdLienExt == NULL)
//   {
//      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
//      return -3;
//   }
//
//   // Get 1st rec
//   do {
//      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);
//   } while (pTmp && !isdigit(*pTmp));
//
//   // Merge loop
//   while (pTmp && !feof(fdLien))
//   {
//      // Create new record
//      iRet = Men_ExtrLienRec(acBuf, acRec);
//      if (!iRet)
//      {
//         // Write to output
//         fputs(acBuf, fdLienExt);
//         iNewRec++;
//      }
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//
//      // Get next roll record
//      pTmp = fgets(acRec, MAX_RECSIZE, fdLien);
//      if (!isdigit(acRec[1]))
//         break;      // EOF
//   }
//
//   // Close files
//   if (fdLien)
//      fclose(fdLien);
//   if (fdLienExt)
//      fclose(fdLienExt);
//
//   LogMsg("Total output records:       %u", iNewRec);
//   LogMsg("Total records processed:    %u", lCnt);
//   printf("\nTotal output records: %u", lCnt);
//
//   lRecCnt = lCnt;
//   return 0;
//}

/******************************* Men_ExtrLienCsv *****************************
 *
 * Extract values from 2022 "ar_8_25_secured.csv"
 *
 *****************************************************************************/

int Men_CreateLienRecCsv(char *pOutbuf)
{
   char     acTmp[256];
   long     iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   ULONG    lTmp;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   vmemcpy(pLienRec->acApn, apTokens[MEN_L_PIN], SIZ_LIEN_APN);

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // TRA
   vmemcpy(pLienRec->acTRA, apTokens[MEN_L_TRA], 6);

   // Land
   long lLand = atol(apTokens[MEN_L_LAND]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atol(apTokens[MEN_L_IMPROVEMENTS]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // Other values
   long lFixtr  = atoi(apTokens[MEN_L_TRADEFIXTURESAMOUNT]);
   long lPers   = atoi(apTokens[MEN_L_PERSONALVALUE]);
   long lLivImpr= atoi(apTokens[MEN_L_LIVINGIMPROVEMENTS]);
   lTmp = lFixtr+lPers+lLivImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_PERS, lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lFixtr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixtr);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }

      if (lLivImpr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_AMT, lLivImpr);
         memcpy(pLienRec->extra.Men.LivingImpr, acTmp, iTmp);
      }
   }

   // Gross total
   ULONG lGross = lTmp + lLand + lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lGross);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lHO_Exe = atol(apTokens[MEN_L_HOX]);
   ULONG lOE_Exe = atol(apTokens[MEN_L_OTHEREXEMPTION]);
   ULONG lVE_Exe = atol(apTokens[MEN_L_VETERANSEXEMPTION]);
   ULONG lDV_Exe = atol(apTokens[MEN_L_DISABLEDVETERANSEXEMPTION]);
   ULONG lCH_Exe = atol(apTokens[MEN_L_CHURCHEXEMPTION]);
   ULONG lRE_Exe = atol(apTokens[MEN_L_RELIGIOUSEXEMPTION]);
   ULONG lCE_Exe = atol(apTokens[MEN_L_CEMETERYEXEMPTION]);
   ULONG lPS_Exe = atol(apTokens[MEN_L_PUBLICSCHOOLEXEMPTION]);
   ULONG lPL_Exe = atol(apTokens[MEN_L_PUBLICLIBRARYEXEMPTION]);
   ULONG lPM_Exe = atol(apTokens[MEN_L_PUBLICMUSEUMEXEMPTION]);
   ULONG lWH_Exe = atol(apTokens[MEN_L_WELFAREHOSPITALEXEMPTION]);
   ULONG lWS_Exe = atol(apTokens[MEN_L_WPP_SCHOOLEXEMPTION]);
   ULONG lWR_Exe = atol(apTokens[MEN_L_WC_RELIGIOUSEXEMPTION]);
   ULONG lTotalExe = lHO_Exe+lOE_Exe+lVE_Exe+lDV_Exe+lCH_Exe+lRE_Exe+lCE_Exe+lPS_Exe+lPL_Exe+lPM_Exe+
      lWH_Exe+lWS_Exe+lWR_Exe;

   pLienRec->acHO[0] = '2';            // 'N'
   if (lTotalExe > 0)
   {
      if (lTotalExe > lGross)
         lTotalExe = lGross;
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, lTotalExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);

      if (lHO_Exe > 0)
         pLienRec->acHO[0] = '1';      // 'Y'

      // Set full exemption flag 
      if (lGross == lTotalExe || *apTokens[MEN_L_ISTAXABLE] == 'N')
      {
         pLienRec->SpclFlag = LX_FULLEXE_FLG;
      //} else if (isdigit(*apTokens[MEN_L_TAXABILITYCODE]))
      //{
      //   // For 2022, there is no tax code.  The new field ISTAXABLE contains Y/N
      //   vmemcpy(pLienRec->acTaxCode, apTokens[MEN_L_TAXABILITYCODE], SIZ_TAX_CODE);
      }
   }  

   // Populate per bulk customer request 07/20/2022
   lTmp = atol(apTokens[MEN_L_TENPERCENTASSESSEDPENALTY]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LATEPEN, lTmp);
      memcpy(pLienRec->acLatePen, acTmp, SIZ_LIEN_LATEPEN);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Men_ExtrLienCsv()
{
   char      *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH], cNonTax;
   int       iRet;
   long      lOut=0, lCnt=0;
   FILE      *fdOut, *fdIn;

   LogMsg0("Extracting LDR value.");

   // Open input file
   LogMsg("Open %s for input", acRollFile);
   fdIn = fopen(acRollFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening input file: %s\n", acRollFile);
      return -2;
   }  

   // Open out file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open file %s for output", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   iRet = GetPrivateProfileString(myCounty.acCntyCode, "NonTax", "Y", acTmpFile, 4, acIniFile);
   cNonTax = acTmpFile[0];

   // Start loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      if (acRec[0] > '9')
         continue;

      // Parse input rec
      iTokens = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < MEN_L_COLS)
      {
         LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[MEN_L_PIN], iTokens);
         continue;
      }

      // Ignore non-taxable parcels
      if (cNonTax == 'N' && *apTokens[MEN_L_ISTAXABLE] == 'N')
         continue;

      iRet = iTrim(apTokens[MEN_L_PIN]);
      if (iRet != iApnLen)
      {
         if (bDebug)
            LogMsg("*** Drop bad APN: %s", apTokens[MEN_L_PIN]);
         continue;
      }

      // Create new base record
      iRet = Men_CreateLienRecCsv(acBuf);
      if (!iRet)
      {
         fputs(acBuf, fdOut);
         lOut++;
      } else
      {
         LogMsg("---> Drop record %d [APN=%s, TaxYear=%s]", lCnt, apTokens[MEN_L_PIN]); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records output:    %u", lOut);

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return iRet;
}

/******************************* Men_ExtrRollCsv *****************************
 *
 * Extract values from 2023 "CA-Mendocino-AsmtRoll_ppa.txt"
 *
 *****************************************************************************/

int Men_CreateRollRecCsv(char *pOutbuf)
{
   char     acTmp[256];
   long     iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   ULONG    lTmp;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   vmemcpy(pLienRec->acApn, apTokens[R_PIN], SIZ_LIEN_APN);

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // TRA
   vmemcpy(pLienRec->acTRA, apTokens[R_TRA], 6);

   // Land
   long lLand = atol(apTokens[R_LAND]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atol(apTokens[R_IMPR]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // Other values
   long lFixtr  = atoi(apTokens[R_TRADEFIXTURESAMOUNT]);
   long lPers   = atoi(apTokens[R_PERSONALVALUE]);
   long lLivImpr= atoi(apTokens[R_LIVINGIMPROVEMENTS]);
   lTmp = lFixtr+lPers+lLivImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_PERS, lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lFixtr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixtr);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }

      if (lLivImpr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_AMT, lLivImpr);
         memcpy(pLienRec->extra.Men.LivingImpr, acTmp, iTmp);
      }
   }

   // Gross total
   ULONG lGross = lTmp + lLand + lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lGross);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lHO_Exe = atol(apTokens[R_HOX]);
   ULONG lOE_Exe = atol(apTokens[R_OTHEREXEMPTION]);
   ULONG lVE_Exe = atol(apTokens[R_VETERANSEXEMPTION]);
   ULONG lDV_Exe = atol(apTokens[R_DISABLEDVETERANSEXEMPTION]);
   ULONG lCH_Exe = atol(apTokens[R_CHURCHEXEMPTION]);
   ULONG lRE_Exe = atol(apTokens[R_RELIGIOUSEXEMPTION]);
   ULONG lCE_Exe = atol(apTokens[R_CEMETERYEXEMPTION]);
   ULONG lPS_Exe = atol(apTokens[R_PUBLICSCHOOLEXEMPTION]);
   ULONG lPL_Exe = atol(apTokens[R_PUBLICLIBRARYEXEMPTION]);
   ULONG lPM_Exe = atol(apTokens[R_PUBLICMUSEUMEXEMPTION]);
   ULONG lWH_Exe = atol(apTokens[R_WELFAREHOSPITALEXEMPTION]);
   ULONG lWS_Exe = atol(apTokens[R_WELFAREPRIVATEPAROCHIALSCHOOLEXEMPTION]);
   ULONG lWR_Exe = atol(apTokens[R_WELFARECHARITYRELIGIOUSEXEMPTION]);
   ULONG lTotalExe = lHO_Exe+lOE_Exe+lVE_Exe+lDV_Exe+lCH_Exe+lRE_Exe+lCE_Exe+lPS_Exe+lPL_Exe+lPM_Exe+
      lWH_Exe+lWS_Exe+lWR_Exe;

   pLienRec->acHO[0] = '2';            // 'N'
   if (lTotalExe > 0)
   {
      if (lTotalExe > lGross)
         lTotalExe = lGross;
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, lTotalExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);

      if (lHO_Exe > 0)
         pLienRec->acHO[0] = '1';      // 'Y'

      // Set full exemption flag 
      if (lGross == lTotalExe || *apTokens[R_ISTAXABLE] == 'N')
      {
         pLienRec->SpclFlag = LX_FULLEXE_FLG;
      //} else if (isdigit(*apTokens[R_TAXABILITYCODE]))
      //{
      //   // For 2022, there is no tax code.  The new field ISTAXABLE contains Y/N
      //   vmemcpy(pLienRec->acTaxCode, apTokens[R_TAXABILITYCODE], SIZ_TAX_CODE);
      }
   }  

   // Populate per bulk customer request 07/20/2022
   lTmp = atol(apTokens[R_TENPERCENTASSESSEDPENALTY]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LATEPEN, lTmp);
      memcpy(pLienRec->acLatePen, acTmp, SIZ_LIEN_LATEPEN);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

// Extract LDR values from CA-Mendocino-AsmtRoll_ppa.txt
int Men_ExtrRollCsv()
{
   char      *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH], cNonTax;
   int       iRet;
   long      lOut=0, lCnt=0;
   FILE      *fdOut, *fdIn;

   LogMsg0("Extracting LDR value.");

   // Open input file
   LogMsg("Open %s for input", acRollFile);
   fdIn = fopen(acRollFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening input file: %s\n", acRollFile);
      return -2;
   }  

   // Open out file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open file %s for output", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   iRet = GetPrivateProfileString(myCounty.acCntyCode, "NonTax", "Y", acTmpFile, 4, acIniFile);
   cNonTax = acTmpFile[0];

   // Start loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      if (acRec[0] > '9')
         continue;

      // Parse input rec
      iTokens = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < R_COLS)
      {
         LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[R_PIN], iTokens);
         continue;
      }

      // Ignore non-taxable parcels
      if (cNonTax == 'N' && *apTokens[R_ISTAXABLE] == 'N')
         continue;

      iRet = iTrim(apTokens[R_PIN]);
      if (iRet != iApnLen)
      {
         if (bDebug)
            LogMsg("*** Drop bad APN: %s", apTokens[R_PIN]);
         continue;
      }

      // Create new base record
      iRet = Men_CreateRollRecCsv(acBuf);
      if (!iRet)
      {
         fputs(acBuf, fdOut);
         lOut++;
      } else
      {
         LogMsg("---> Drop record %d [APN=%s, TaxYear=%s]", lCnt, apTokens[R_PIN]); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records output:    %u", lOut);

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return iRet;
}

/****************************** Men_ExtrRollCsv2 *****************************
 *
 * Extract values from "2024 Secured.txt"
 *
 *****************************************************************************/

int Men_CreateRollRecCsv2(char *pOutbuf)
{
   char     acTmp[256];
   long     iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   ULONG    lTmp;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   vmemcpy(pLienRec->acApn, apTokens[MEN_SR_PIN], SIZ_LIEN_APN);

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // TRA
   vmemcpy(pLienRec->acTRA, apTokens[MEN_SR_TRA], 6);

   // Land
   long lLand = atol(apTokens[MEN_SR_LAND]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atol(apTokens[MEN_SR_IMPR]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // Other values
   long lFixtr  = atoi(apTokens[MEN_SR_FIXTURES]);
   long lPers   = atoi(apTokens[MEN_SR_PERSPROP]);
   long lLivImpr= atoi(apTokens[MEN_SR_LIVIMPR]);
   lTmp = lFixtr+lPers+lLivImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_PERS, lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lFixtr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixtr);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }

      if (lLivImpr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_AMT, lLivImpr);
         memcpy(pLienRec->extra.Men.LivingImpr, acTmp, iTmp);
      }
   }

   // Gross total
   ULONG lGross = lTmp + lLand + lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lGross);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   ULONG lHO_Exe = atol(apTokens[MEN_SR_HOX]);
   ULONG lOE_Exe = atol(apTokens[MEN_SR_OTHEREXMPT]);
   ULONG lVE_Exe = atol(apTokens[MEN_SR_DVX]);
   ULONG lDV_Exe = atol(apTokens[MEN_SR_LDVX]);
   ULONG lTotalExe = lHO_Exe+lOE_Exe+lVE_Exe+lDV_Exe;

   pLienRec->acHO[0] = '2';            // 'N'
   if (lTotalExe > 0)
   {
      if (lTotalExe > lGross)
         lTotalExe = lGross;
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, lTotalExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);

      if (lHO_Exe > 0)
         pLienRec->acHO[0] = '1';      // 'Y'

      // Set full exemption flag 
      if (lGross == lTotalExe)
      {
         pLienRec->SpclFlag = LX_FULLEXE_FLG;
      //} else if (isdigit(*apTokens[MEN_SR_TAXABILITYCODE]))
      //{
      //   // For 2022, there is no tax code.  The new field ISTAXABLE contains Y/N
      //   vmemcpy(pLienRec->acTaxCode, apTokens[MEN_SR_TAXABILITYCODE], SIZ_TAX_CODE);
      }
   }  

   // Populate per bulk customer request 07/20/2022
   lTmp = atol(apTokens[MEN_SR_PENALTY]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LATEPEN, lTmp);
      memcpy(pLienRec->acLatePen, acTmp, SIZ_LIEN_LATEPEN);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

// Extract LDR values from "2024 Secured.txt"
int Men_ExtrRollCsv2()
{
   char      *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH], cNonTax;
   int       iRet;
   long      lOut=0, lCnt=0;
   FILE      *fdOut, *fdIn;

   LogMsg0("Extracting LDR value.");

   // Open input file
   LogMsg("Open %s for input", acRollFile);
   fdIn = fopen(acRollFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening input file: %s\n", acRollFile);
      return -2;
   }  

   // Open out file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open file %s for output", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   iRet = GetPrivateProfileString(myCounty.acCntyCode, "NonTax", "Y", acTmpFile, 4, acIniFile);
   cNonTax = acTmpFile[0];

   // Start loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      if (acRec[0] > '9')
         continue;

      // Parse input rec
      iTokens = ParseStringIQ(acRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
      if (iTokens < MEN_SR_FLDS)
      {
         LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[MEN_SR_PIN], iTokens);
         continue;
      }

      // Ignore non-taxable parcels
      //if (cNonTax == 'N' && *apTokens[R_ISTAXABLE] == 'N')
      //   continue;

      iRet = iTrim(apTokens[MEN_SR_PIN]);
      if (iRet != iApnLen)
      {
         //if (bDebug)
            LogMsg("*** Drop bad APN: %s (%s)", apTokens[MEN_SR_PIN], apTokens[MEN_SR_CLASSCD]);
         continue;
      }

      // Create new base record
      iRet = Men_CreateRollRecCsv2(acBuf);
      if (!iRet)
      {
         fputs(acBuf, fdOut);
         lOut++;
      } else
      {
         LogMsg("---> Drop record %d [APN=%s, TaxYear=%s]", lCnt, apTokens[R_PIN]); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records output:    %u", lOut);

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return iRet;
}

/*************************** Men_Load_RollCorrection *************************
 *
 * Extract values from "2021 ANNUAL ROLLS COMBINED FOR PARCEL QUEST 12-9-2021.csv"
 * and replace LDR data.
 *
 * Run FixCsv and resort it by AIN before processing
 *
 *****************************************************************************/

int Men_MergeRollCorrection(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp, iRet;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "0010824200", 10))
   //   lTmp = 0;
#endif

   // Parse input string
   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < MEN_COR_FLDS)
   {
      LogMsg("***** Men_MergeRollCorrection: bad input record for APN=%s", apTokens[0]);
      return -1;
   }

   // Land
   long lLand = dollar2Num(apTokens[MEN_COR_ASSDLAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[MEN_COR_ASSDIMP]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   long lPers = dollar2Num(apTokens[MEN_COR_ASSDPERSONAL]);
   long lLiv = dollar2Num(apTokens[MEN_COR_ASSDLIVIMP]);
   long lFixt = dollar2Num(apTokens[MEN_COR_ASSDFIXTURES]);
   lTmp = lPers + lFixt + lLiv;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%u         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lLiv > 0)
      {
         sprintf(acTmp, "%u         ", lLiv);
         memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // HO Exempt
   long lExe = dollar2Num(apTokens[MEN_COR_HOX]);
   if (lExe > 0)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   lExe = dollar2Num(apTokens[MEN_COR_TOTALEXMPT]);
   if (lExe > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Full exemption
   if (lExe > 0 && lExe >= lTmp)
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // TRA
   lTmp = atol(apTokens[MEN_COR_TAG]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   return 0;
}

int Men_Load_RollCorrection(int iSkip)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acRawFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd, iRollDrop;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open roll file
   LogMsg("Open roll correction file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll correction file: %s\n", acRollFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iSkip > 0)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iRollUpd=iRollDrop=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "004020015", 9))
      //   iRet = 1;
#endif
      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Men_MergeRollCorrection(acBuf, acRollRec);
         if (!iRet)
            iRollUpd++;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         lCnt++;

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)
      {
         iRollDrop++;         // Ignore roll record that doesn't match

         // Get next record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         lCnt++;

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Write out any record left from S01 
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      if (nBytesRead == iRecLen)
      {
         WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else
         break;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhIn)
      CloseHandle(fhIn);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("      updated records:      %u", iRollUpd);
   LogMsg("      dropped records:      %u\n", iRollDrop);
   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;

   return lRet;
}

/********************************* Men_MergeSAdr *****************************
 *
 * This version parse mail address from "CA-Mendo-AsmtRoll.csv"
 * The situs on this file is not parsed fully.  The StreetName field contains full address line 1.
 * Use parseAdr1C() if needed to parse StreetName when it contains ADDR1.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Men_MergeSAdr(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acStreet[64];
   long     lTmp;
   int      iTmp;

   // Remove old situs
   removeSitus(pOutbuf);

   if (!_memicmp(apTokens[R_SITUSSTREETNAME], "NONE", 4))
      return 0;

#ifdef _DEBUG
   // 0061313300, 0061334800, 0081821700
   //if (!memcmp(pOutbuf, "1770900470", 10) ) 
   //   acTmp[0] = 0;
#endif

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[R_SITUSSTREETNUMBER]);
   if (lTmp > 0)
   {
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[R_SITUSSTREETNUMBER], strlen(apTokens[R_SITUSSTREETNUMBER]));
      iTmp = sprintf(acTmp, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      strcpy(acAddr1, acTmp);

      if (*apTokens[R_SITUSSTREETNUMBERSUFFIX] > ' ')
         vmemcpy(pOutbuf+OFF_S_STR_SUB, apTokens[R_SITUSSTREETNUMBERSUFFIX], SIZ_S_STR_SUB);

      if (*apTokens[R_SITUSSTREETPREDIRECTIONAL] > ' ')
      {
         strcat(acAddr1, apTokens[R_SITUSSTREETPREDIRECTIONAL]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[R_SITUSSTREETPREDIRECTIONAL], strlen(apTokens[R_SITUSSTREETPREDIRECTIONAL]));

      }
   }

   _strupr(apTokens[R_SITUSSTREETNAME]);
   iTmp = blankRem(apTokens[R_SITUSSTREETNAME]);
   iTmp -= 3;

   // Special case
   if (iTmp > 0 && !memcmp(apTokens[R_SITUSSTREETNAME]+iTmp, " WE", 3))
   {
      LogMsg("+++ Hardcode direction for %s", apTokens[R_APN]);
      *(apTokens[R_SITUSSTREETNAME]+iTmp) = 0;
      if (*(pOutbuf+OFF_S_DIR) == ' ')
      {
         strcat(acAddr1, "W ");
         *(pOutbuf+OFF_S_DIR) = 'W';
      }
   }

   strcpy(acStreet, apTokens[R_SITUSSTREETNAME]);
   strcat(acAddr1, acStreet);

   if (*apTokens[R_SITUSSTREETTYPE] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[R_SITUSSTREETTYPE]);

      // Make Plaza part of street name
      if (!memcmp(apTokens[R_SITUSSTREETTYPE], "PLAZA", 5))
      {
         strcat(acStreet, " ");
         strcat(acStreet, apTokens[R_SITUSSTREETTYPE]);
      } else
      {
         iTmp = GetSfxCodeX(apTokens[R_SITUSSTREETTYPE], acTmp);
         if (iTmp > 0)
         {
            Sfx2Code(acTmp, acCode);
            memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
         } else
         {
            LogMsg0("*** Invalid suffix: %s [%s]", apTokens[R_SITUSSTREETTYPE], apTokens[R_APN]);
            iBadSuffix++;
         }
      }
   } 
   
   vmemcpy(pOutbuf+OFF_S_STREET, acStreet, SIZ_S_STREET);
   
   if (isalnum(*apTokens[R_SITUSUNITNUMBER]) || *apTokens[R_SITUSUNITNUMBER] == '#')
   {
      strcat(acAddr1, " ");
      if (isdigit(*apTokens[R_SITUSUNITNUMBER]))
         strcat(acAddr1, "#");
      else if ((*apTokens[R_SITUSUNITNUMBER]) != '#')
         strcat(acAddr1, "STE ");
      strcat(acAddr1, apTokens[R_SITUSUNITNUMBER]);
      memcpy(pOutbuf+OFF_S_UNITNO, apTokens[R_SITUSUNITNUMBER], strlen(apTokens[R_SITUSUNITNUMBER]));
   //} else if (*(pOutbuf+OFF_M_UNITNO) > ' ' &&
   //   !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
   //   !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) )
   //{
   //   memcpy(pOutbuf+OFF_S_UNITNO, pOutbuf+OFF_M_UNITNO, SIZ_M_UNITNO);
   //   sprintf(acTmp, " %.*s", SIZ_M_UNITNO, pOutbuf+OFF_M_UNITNO);
   //   strcat(acAddr1, acTmp);
   } else if (*apTokens[R_SITUSUNITNUMBER] > ' ')
   {
      if (isalnum(*(1+apTokens[R_SITUSUNITNUMBER])))
      {
         strcat(acAddr1, " #");
         strcat(acAddr1, apTokens[R_SITUSUNITNUMBER]+1);
         memcpy(pOutbuf+OFF_S_UNITNO, apTokens[R_SITUSUNITNUMBER]+1, strlen(apTokens[R_SITUSUNITNUMBER])-1);
      } else
         LogMsg("*** Invalid situs Unit#: %s [%s]", apTokens[R_SITUSUNITNUMBER], apTokens[R_APN]);
   }

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   
   // Situs city
   if (*apTokens[R_SITUSCITYNAME] > ' ')
   {
      char sCity[32];

      iTmp = City2CodeEx(_strupr(apTokens[R_SITUSCITYNAME]), acCode, sCity, apTokens[R_APN]);
      if (iTmp > 0)
      {
         vmemcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
      } else
         strcpy(sCity, apTokens[R_SITUSCITYNAME]);

      if (*apTokens[R_SITUSZIPCODE] == '9')
         vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[R_SITUSZIPCODE], SIZ_S_ZIP);

      iTmp = sprintf(acTmp, "%s, CA %s", sCity, apTokens[R_SITUSZIPCODE]);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
   }

   return 0;
}

/******************************** Men_MergeMAdr ******************************
 *
 * This version parse mail address from "CA-Mendo-AsmtRoll.csv"
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Men_MergeMAdr(char *pOutbuf)
{
   char        acTmp[256], acAddr1[256], *pAddr1;
   int         iTmp;
   bool        bUnitAvail = false;

   ADR_REC     sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf);

   //if (*apTokens[R_MAILNAME] > ' ')
   //{
   //   iTmp = replUnPrtChar(apTokens[R_MAILNAME]);
   //   updateCareOf(pOutbuf, apTokens[R_MAILNAME], 0);
   //}

   // Check for blank address
   replUnPrtChar(apTokens[R_MAILADDRESS]);
   pAddr1 = apTokens[R_MAILADDRESS];
   if (memcmp(pAddr1, "     ", 5) && memcmp(pAddr1, "UNKNOWN", 7))
   {
      strcpy(acAddr1, pAddr1);
      iTmp = 0;
      if (!memcmp(acAddr1, "C/O", 3))
      {
         iTmp = 4;
         while (!isdigit(*(pAddr1+iTmp)))
         {
            iTmp++;
         }
         pAddr1 += iTmp;
         if (iTmp > 8)
            memcpy(pOutbuf+OFF_CARE_OF, acAddr1, iTmp);
      } else
         pAddr1 = acAddr1;
      memcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, strlen(pAddr1));

      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

      // Parsing mail address
      parseAdr1(&sMailAdr, pAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }

      if (sMailAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));

      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));

      if (sMailAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            memcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, strlen(sMailAdr.Unit));
         }
      }


#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "607224002", 9) )
      //   iTmp = 0;
#endif

      // City/State
      if (*apTokens[R_MAILCITY] > ' ')
      {
         // Remove quote & comma at the end of city name
         strcpy(acAddr1, apTokens[R_MAILCITY]);
         iTmp = iTrim(acAddr1);
         if (acAddr1[iTmp-1] == ',')
            acAddr1[iTmp-1] = 0;

         if (*apTokens[R_MAILSTATE] > ' ')
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY);
            vmemcpy(pOutbuf+OFF_M_ST, apTokens[R_MAILSTATE], 2);

            sprintf(acTmp, "%s, %s %s", acAddr1, apTokens[R_MAILSTATE], apTokens[R_MAILZIPCODE]);
            iTmp = blankRem(acTmp);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
         } else
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY+SIZ_M_ST+SIZ_M_ZIP);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, SIZ_M_CTY_ST_D);
         }
      }

      iTmp = atol(apTokens[R_MAILZIPCODE]);
      if (iTmp > 500)
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[R_MAILZIPCODE], SIZ_M_ZIP);
   }
}

/******************************* Men_MergeChar *******************************
 *
 * Merge Men_Char.dat in STDCHAR format
 *
 *****************************************************************************/

int Men_MergeStdChar(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   int      iLoop, iBeds, iFBath, iHBath;
   long     lBldgSqft, lGarSqft;
   ULONG    lTmp;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 2048, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare APN
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 2048, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Yrblt
   lTmp = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // Eff Yrblt
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Garage Sqft (may be carport or detached garage)
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];  
   }

   // Pools
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

#ifdef _DEBUG
   //if (!memcmp(pChar->Apn, "009600002", 9) )
   //   lTmp = 0;
#endif

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      
      *(pOutbuf+OFF_BATH_4Q) = pChar->Bath_4Q[0];
   }

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);

      *(pOutbuf+OFF_BATH_2Q) = pChar->Bath_2Q[0];
   }

   // 3/4 bath
   *(pOutbuf+OFF_BATH_3Q) = pChar->Bath_3Q[0];

   // Stories
   memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);

   // Central Heating-Cooling
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Fireplace
   if (pChar->Fireplace[0] == 'Y')
      memcpy(pOutbuf+OFF_FIRE_PL, " 1", 2);

   // Roof
   *(pOutbuf+OFF_ROOF_MAT) = pChar->RoofMat[0];

   // Water
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Sewer
   *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // Zoning
   //if (pChar->Zoning[0] > ' ')
   //   memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL) = pChar->BldgQual;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Acres
   lTmp = atoln(pChar->LotAcre, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } 

   // Lot Sqft
   lTmp = atoln(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   } 

   // Number of units
   lTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (lTmp > 1)
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 2048, fdChar);

   return 0;
}

/****************************** Men_MergeRollCsv *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Men_MergeRollCsv(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   LONGLONG lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Check for bad char
   Men_ReplBadChar((unsigned char *)pRollRec);

   // Remove extra spaces
   blankRem(pRollRec);

   // Parse roll record
   iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < R_COLS)
   {
      if (!_memicmp(apTokens[R_ROLLCAT], "Sec", 3))
         LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[R_APN], iRet);
      return -1;
   }

   // Ignore non-taxable parcels - Keep non-taxable parcel during update
   //if (*apTokens[R_ISTAXABLE] == 'N')
   //   return 1;

#ifdef _DEBUG
   //if (!memcmp(apTokens[R_APN], "02300669PF000", 9) )
   //   iTmp = 0;
#endif

   // Ignore unsecured parcel 
   if (_memicmp(apTokens[R_ROLLCAT], "Sec", 3))
      return 1;

   // Ignore BPP parcels
   if (strlen(apTokens[R_APN]) < iApnLen)
   {
      LogMsg("*** Skip: %s", apTokens[R_APN]);
      return 1;
   }

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[R_APN], strlen(apTokens[R_APN]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Format APN
      iRet = formatApn(apTokens[R_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[R_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "23MEN", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[R_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[R_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other values
      long lFixt   = atoi(apTokens[R_TRADEFIXTURESAMOUNT]);
      long lPP     = atoi(apTokens[R_PERSONALVALUE]);
      long lLivImpr= atoi(apTokens[R_LIVINGIMPROVEMENTS]);
      lTmp = lFixt+lPP+lLivImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lLivImpr > 0)
         {
            sprintf(acTmp, "%d         ", lLivImpr);
            memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   iTmp = atol(apTokens[R_TRA]);
   sprintf(acTmp, "%.6u", iTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, 6);

   // Is Taxable
   if (*apTokens[R_ISPUBLICLYOWNEDPROPERTY] == 'Y')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Legal
   iTmp = updateLegal(pOutbuf, _strupr(apTokens[R_ASSESSMENTDESCRIPTION]));
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode 
   removeUseCode(pOutbuf);
   if (*apTokens[R_CLASSCODE] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[R_CLASSCODE], SIZ_USE_CO);
      if (acTmp[0] >= '0')
      {
         memcpy(acTmp, apTokens[R_CLASSCODE], 2);
         acTmp[2] = 0;
         iRet = updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 2, pOutbuf);
         if (!iRet)
         {
            iRet = updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 2, pOutbuf);
         }
      } else
         memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
   }

   // Acres
   dTmp = atof(apTokens[R_ACREAGEAMOUNT]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      if (dTmp < MAX_LOTACRES)
      {
         lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      } else
         memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (ULONG)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[R_APN], "009000009", 9) )
   //   iTmp = 0;
#endif

   // Owner
   try {
      Men_MergeOwner(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Men_MergeOwner()");
   }

   // Situs
   Men_MergeSAdr(pOutbuf);

   // Mailing
   Men_MergeMAdr(pOutbuf);

   return 0;
}

/****************************** Men_Load_RollCsv ******************************
 *
 * Load updated roll file AssessmentRoll.csv (10/06/2019)
 *
 ******************************************************************************/

int Men_Load_RollCsv(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acLastApn[32];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acSrtFile[_MAX_PATH], *pTmp;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0, lCnt=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   HANDLE   fhIn, fhOut;

   LogMsg0("Loading roll update");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Check for Unicode
   sprintf(acBuf, "%s\\%s\\%s_Roll.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = RebuildCsv(acRollFile, acBuf, cDelim, R_COLS);
   if (iRet < 1)
      return iRet;
   else
      strcpy(acRollFile, acBuf);

   //if (!UnicodeToAnsi(acRollFile, acBuf))
   //   strcpy(acRollFile, acBuf);

   // Sort input
   sprintf(acSrtFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acRec, "S(#%d,C,A,#%d,C,D) F(TXT) DUPOUT(B8192,#1) DEL(%d) F(TXT)", R_APN+1, R_ACREAGEAMOUNT+1, cDelim);
   iTmp = sortFile(acRollFile, acSrtFile, acRec);

   // Open roll file
   LogMsg("Open Roll file %s", acSrtFile);
   fdRoll = fopen(acSrtFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acSrtFile);
      return 2;
   }

   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);    
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   strcpy(acLastApn, "000000000");

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "164760072", 9) )
      //   iTmp = 0;
#endif

      // Find APN offset
      pTmp = strchr(acRollRec, ',');
      // Check for bad record
      if (!pTmp)
         break;

      iTmp = memcmp(acBuf, pTmp+1, iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Men_MergeRollCsv(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Char
            if (fdChar)
               iTmp = Men_MergeStdChar(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Men_MergeRollCsv(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Char
            if (fdChar)
               iTmp = Men_MergeStdChar(acRec);

            if (memcmp(acLastApn, acRec, 14))
            {
               memcpy(acLastApn, acRec, 14);
               bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
               iNewRec++;
            } else
            {
               LogMsg("---> Duplicate APN: %.14s", acLastApn);
            }
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("Retired record? : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, (char *)&acRollRec[iSkipQuote], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         if (memcmp(acLastApn, acBuf, 14))
         {
            memcpy(acLastApn, acBuf, 14);
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            if (!bRet)
            {
               LogMsg("Error occurs: %d\n", GetLastError());
               break;
            }
         }
      }
   }

   // Do the rest of the file
   while (!bEof && isdigit(acRollRec[0]))
   {
      // Create new R01 record
      iRet = Men_MergeRollCsv(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Merge Char
         if (fdChar)
            iRet = Men_MergeStdChar(acRec);

         if (memcmp(acLastApn, acRec, 14))
         {
            memcpy(acLastApn, acRec, 14);
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            iNewRec++;
         } else
         {
            LogMsg("---> Duplicate APN: %.14s", acLastApn);
         }
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("      updated records:      %u", iRollUpd);
   LogMsg("      new records:          %u", iNewRec);
   LogMsg("      retired records:      %u", iRetiredRec);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = iRollUpd+iNewRec;
   return 0;
}

/*************************** Men_CreateTaxItems() *****************************
 *
 * Parse current detail tax bill to update TaxBase and create TaxItems and TaxAgency
 *
 ******************************************************************************/

//int Men_CreateTaxItems1(char *pTaxBase, FILE *fdDetail, FILE *fdItems, FILE *fdAgency)
//{
//   static   char acRec[1024], *apItems[16];
//   static   long lPos=0;
//
//   int      iRet, iTmp, iItems, iAdjIdx;
//   double   dTaxRate;
//   char     acTmp[1024], acDetail[1024], acAgency[1024], *pTmp;
//
//   TAXBASE   *pOutBase   = (TAXBASE *)pTaxBase;
//   TAXDETAIL *pOutDetail = (TAXDETAIL *)acDetail;
//   TAXAGENCY *pOutAgency = (TAXAGENCY *)acAgency, *pResult;
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutBase->Assmnt_No, "44257021", 8))
//   //   iTmp = 0;
//#endif
//   
//   dTaxRate = 0;
//   iRet = 1;
//   while (!feof(fdDetail))
//   {
//      // Save current position
//      lPos = ftell(fdDetail);
//      pTmp = fgets(acRec, 1024, fdDetail);
//      if (!pTmp || *pTmp > '9')
//         break;
//     
//#ifdef _DEBUG
//      //if (!memcmp(acRec, "0000010201", 10) )
//      //   iRet = 0;
//#endif
//      // Parse detail rec
//      iAdjIdx = 0;
//      iItems = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apItems);
//      if (iItems < CD_FLDS)
//      {
//         LogMsg("***** Error: bad Tax record for APN=%s (#tokens=%d)", acRec, iItems);
//         return -1;
//      } else if (iItems > CD_FLDS)
//      {
//         iAdjIdx++;
//         LogMsg("*** WARNING: Too many tokens APN=%s (#tokens=%d).", acRec, iItems);
//      }
//
//      iTmp = strcmp(pOutBase->Assmnt_No, apItems[CD_TAXBILLID]);
//      if (!iTmp)
//      {
//         iRet = 0;
//         memset(acAgency, 0, sizeof(TAXAGENCY));
//         memset(acDetail, 0, sizeof(TAXDETAIL));
//         strcpy(pOutDetail->Apn, myTrim(apItems[CD_PIN]));
//         strcpy(pOutDetail->Assmnt_No, apItems[CD_TAXBILLID]);
//         strcpy(pOutDetail->TaxYear, apItems[CD_TAXYEAR]);
//         strcpy(pOutDetail->BillNum, myTrim(apItems[CD_BILLNUMBER]));
//         strcpy(pOutDetail->TaxRate, apItems[CD_RATE+iAdjIdx]);
//         strcpy(pOutDetail->TaxAmt, apItems[CD_TOTAL+iAdjIdx]);
//         strcpy(pOutDetail->TaxDesc, myTrim(apItems[CD_DETAILS]));
//         strcpy(pOutDetail->TaxCode, myTrim(apItems[CD_TAXCODE+iAdjIdx]));
//         dTaxRate += atof(pOutDetail->TaxRate);
//
//         // Update TaxBase
//         //if (!pOutBase->Apn[0])
//         if (!pOutBase->TaxYear[0])
//         {
//            // pOutBase contains new APN.  We need old APN for current tax roll.
//            if (strcmp(pOutBase->Apn, pOutDetail->Apn))
//            {
//               LogMsg("*** APN mismatched BillID=%s Base->PIN=%s ... Detail->PIN=%s", pOutDetail->BillNum, pOutBase->Apn, pOutDetail->Apn);
//               strcpy(pOutBase->Apn, pOutDetail->Apn);
//               strcpy(pOutBase->BillNum, pOutDetail->BillNum);
//            }
//            strcpy(pOutBase->TaxYear, pOutDetail->TaxYear);
//
//            remChar(apItems[CD_TAXRATEAREA], '-');
//            iTmp = atol(apItems[CD_TAXRATEAREA]);
//            sprintf(pOutBase->TRA, "%.6d", iTmp);
//
//            // BillType (Annual Secured|Annual Unsecured) 
//            if (*(apItems[CD_BILLTYPE]+7) == 'S')
//            {
//               pOutBase->isSecd[0] = '1';
//               pOutBase->isSupp[0] = '0';
//               pOutBase->BillType[0] = BILLTYPE_SECURED;
//            } else if (*(apItems[CD_BILLTYPE]+7) == 'U')
//            {
//               pOutBase->isSecd[0] = '0';
//               pOutBase->isSupp[0] = '0';
//               pOutBase->BillType[0] = BILLTYPE_UNSECURED;
//            } else
//            {
//               LogMsg("** Unknown BillType: %s", apItems[CD_BILLTYPE]);
//               continue;
//            }
//         }
//
//         // Tax code
//         if (iNumTaxDist > 1)
//         {
//            pResult = findTaxAgency(pOutDetail->TaxCode, 0);
//            if (pResult)
//            {
//               strcpy(pOutAgency->Agency, pResult->Agency);
//               strcpy(pOutAgency->Code, pResult->Code);
//               strcpy(pOutAgency->Phone, pResult->Phone);
//               pOutAgency->TC_Flag[0] = pResult->TC_Flag[0];
//               pOutDetail->TC_Flag[0] = pResult->TC_Flag[0];
//            } else
//            {
//               strcpy(pOutAgency->Agency, pOutDetail->TaxDesc);
//               strcpy(pOutAgency->Code, pOutDetail->TaxCode);
//               pOutAgency->Phone[0] = 0;
//               pOutAgency->TC_Flag[0] = '0';
//               pOutDetail->TC_Flag[0] = '0';
//               LogMsg("+++ New Agency: %s:%s", pOutDetail->TaxCode, pOutDetail->TaxDesc);
//            }
//         }
//
//         // Output detail record
//         Tax_CreateDetailCsv(acTmp, (TAXDETAIL *)&acDetail);
//         fputs(acTmp, fdItems);
//
//         Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgency);
//         fputs(acTmp, fdAgency);
//      } else if (iTmp > 0)
//      {
//         continue;
//      } else
//      {
//         fseek(fdDetail, lPos, SEEK_SET);
//         break;
//      }
//   }
//
//   // Update TaxBase
//   sprintf(pOutBase->TotalRate, "%.2f", dTaxRate);
//
//   return iRet;
//}

/*************************** Men_Load_TaxCurrent ******************************
 *
 * Load CA-Mendo-CurrentAmounts.csv & CA-Mendo-CurrentDistricts.csv
 * Load current tax files and create Base, Items, & Agency import.
 *
 ******************************************************************************/

//int Men_Load_TaxCurrent1(char *pCurrentTaxFile, char *pDetailFile, bool bImport)
//{
//   char     *pTmp, acOutbuf[1024], acRec[1024], acCurrent[_MAX_PATH], acDetail[_MAX_PATH], acTmp[256],
//            sBase[1024], sAgencyFile[_MAX_PATH], sBaseFile[_MAX_PATH], sItemsFile[_MAX_PATH], sTmpFile[_MAX_PATH];
//   long     iRet, lOut=0, lCnt=0, lDrop=0, lBad=0, lNoTax=0;
//   double	dTax1, dTax2, dPen1, dPen2, dFee1, dFee2, dInt1, dInt2, dTaxDue1, dTaxDue2,
//   	      dPaid1, dPaid2, dPaidPen1, dPaidPen2, dPaidFee1, dPaidFee2, dPaidInt1, dPaidInt2, 
//            dTotalTax, dTotalDue, dTotalFees;
//
//   TAXBASE  *pOutBase = (TAXBASE *)sBase;
//   FILE     *fdCurrent, *fdDetail, *fdBase, *fdItems, *fdAgency;
//
//   LogMsg0("Loading current tax file");
//
//   // Check for Unicode
//   sprintf(sTmpFile, "%s\\%s\\CurrentAmounts.txt", acTmpPath, myCounty.acCntyCode);
//   if (!UnicodeToAnsi(pCurrentTaxFile, sTmpFile))
//      strcpy(pCurrentTaxFile, sTmpFile);
//
//   // Sort current tax file on TaxBillId
//   sprintf(acCurrent, "%s\\%s\\Men_Current.txt", acTmpPath, myCounty.acCntyCode);
//   iRet = sortFile(pCurrentTaxFile, acCurrent, "S(#3,C,A) DEL(44) OMIT(#4,C,GT,\"9999\")");
//   if (!iRet)
//      return -1;
//
//   // Check for Unicode
//   sprintf(sTmpFile, "%s\\%s\\CurrentDistricts.txt", acTmpPath, myCounty.acCntyCode);
//   if (!UnicodeToAnsi(pDetailFile, sTmpFile))
//      strcpy(pDetailFile, sTmpFile);
//
//   // Sort detail file on TaxBillId & PIN
//   sprintf(acDetail, "%s\\%s\\Men_Detail.txt", acTmpPath, myCounty.acCntyCode);
//   iRet = sortFile(pDetailFile, acDetail, "S(#6,C,A,#1,C,A) DEL(44) ");
//   if (!iRet)
//      return -1;
//
//   // Open current tax file
//   LogMsg("Open current tax file %s", acCurrent);
//   fdCurrent = fopen(acCurrent, "r");
//   if (fdCurrent == NULL)
//   {
//      LogMsg("***** Error opening current tax file: %s\n", acCurrent);
//      return -2;
//   }  
//
//   // Open detail file
//   LogMsg("Open tax detail file %s", acDetail);
//   fdDetail = fopen(acDetail, "r");
//   if (fdDetail == NULL)
//   {
//      LogMsg("***** Error opening tax Detail file: %s\n", acDetail);
//      return -2;
//   }  
//
//   // Create output file names
//   sprintf(sBaseFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Base");
//   sprintf(sItemsFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Items");
//   sprintf(sAgencyFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Agency");
//   sprintf(sTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//
//   LogMsg("Create TaxBase file %s", sBaseFile);
//   fdBase = fopen(sBaseFile, "w");
//   if (fdBase == NULL)
//   {
//      LogMsg("***** Error creating TaxBase file: %s\n", sBaseFile);
//      return -4;
//   }
//
//   LogMsg("Create TaxItems file %s", sItemsFile);
//   fdItems = fopen(sItemsFile, "w");
//   if (fdItems == NULL)
//   {
//      LogMsg("***** Error creating TaxItems file: %s\n", sItemsFile);
//      return -4;
//   }
//
//   LogMsg("Create TaxAgency file %s", sTmpFile);
//   fdAgency = fopen(sTmpFile, "w");
//   if (fdAgency == NULL)
//   {
//      LogMsg("***** Error creating TaxAgency file: %s\n", sTmpFile);
//      return -4;
//   }
//
//   // Init variables
//   iNewAgency = 0;
//   while (!feof(fdCurrent))
//   {
//      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdCurrent);
//      if (!pTmp || *pTmp > '9')
//         break;
//
//#ifdef _DEBUG
//      //if (!memcmp(acRec, "44207859", 8) )
//      //   iRet = 0;
//      //if (lOut == 138169)
//      //   iRet = 0;
//#endif
//
//      // Parse input tax data
//      iTokens = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
//      if (iTokens < CA_TAXBILLID)
//      {
//         LogMsg("***** Error: bad Tax bill record for taxbillid=%s (#tokens=%d)", acRec, iTokens);
//         return -1;
//      }
//
//      // Reset buffer
//      memset(sBase, 0, sizeof(TAXBASE));
//
//      // Populate TaxBase
//      strcpy(pOutBase->Assmnt_No, apTokens[CA_TAXBILLID]);
//      strcpy(pOutBase->Apn, myTrim(apTokens[CA_PIN]));
//      strcpy(pOutBase->BillNum, myTrim(apTokens[CA_BILLNUMBER]));
//
//      // Due Date - there is no paid date
//      pTmp = dateConversion(apTokens[CA_INST1DUEDATE], acTmp, YYYY_MM_DD);
//      if (pTmp)
//         strcpy(pOutBase->DueDate1, acTmp);
//      pTmp = dateConversion(apTokens[CA_INST2DUEDATE], acTmp, YYYY_MM_DD);
//      if (pTmp)
//         strcpy(pOutBase->DueDate2, acTmp);
//
//      // 1st inst charge/paid/due
//      strcpy(pOutBase->TaxAmt1, apTokens[CA_INST1TAXCHARGE]);
//      if (*apTokens[CA_INST1TAXPAYMENT] == '-')
//         strcpy(pOutBase->PaidAmt1, apTokens[CA_INST1TAXPAYMENT]+1);
//      else
//         strcpy(pOutBase->PaidAmt1, apTokens[CA_INST1TAXPAYMENT]);
//      dTax1 = atof(apTokens[CA_INST1TAXCHARGE]);
//      dPaid1 = atof(pOutBase->PaidAmt1);
//      dTaxDue1 = atof(apTokens[CA_INST1AMOUNTDUE]);
//
//      // 1st penalty/fee/int
//      dPen1 = atof(apTokens[CA_INST1PENALTYCHARGE]);
//      strcpy(pOutBase->PenAmt1, apTokens[CA_INST1PENALTYCHARGE]);
//      dFee1 = atof(apTokens[CA_INST1FEECHARGE]);
//      dInt1 = atof(apTokens[CA_INST1INTERESTCHARGE]);
//
//      dPaidPen1=dPaidFee1=dPaidInt1 = 0;
//      if (*apTokens[CA_INST1PENALTYPAYMENT] == '-')
//         dPaidPen1 = atof(apTokens[CA_INST1PENALTYPAYMENT]+1);
//      if (*apTokens[CA_INST1FEEPAYMENT] == '-')
//         dPaidFee1 = atof(apTokens[CA_INST1FEEPAYMENT]+1);
//      if (*apTokens[CA_INST1INTERESTPAYMENT] == '-')
//         dPaidInt1 = atof(apTokens[CA_INST1INTERESTPAYMENT]+1);
//
//      // 2nd inst charge/paid/due
//      strcpy(pOutBase->TaxAmt2, apTokens[CA_INST2TAXCHARGE]);
//      if (*apTokens[CA_INST2TAXPAYMENT] == '-')
//         strcpy(pOutBase->PaidAmt2, apTokens[CA_INST2TAXPAYMENT]+1);
//      else
//         strcpy(pOutBase->PaidAmt2, apTokens[CA_INST2TAXPAYMENT]);
//      dTax2 = atof(apTokens[CA_INST2TAXCHARGE]);
//      dPaid2 = atof(pOutBase->PaidAmt2);
//      dTaxDue2 = atof(apTokens[CA_INST2AMOUNTDUE]);
//
//      // 2nd penalty/fee/int
//      dPen2 = atof(apTokens[CA_INST2PENALTYCHARGE]);
//      strcpy(pOutBase->PenAmt2, apTokens[CA_INST2PENALTYCHARGE]);
//      dFee2 = atof(apTokens[CA_INST2FEECHARGE]);
//      dInt2 = atof(apTokens[CA_INST2INTERESTCHARGE]);
//      dPaidPen2=dPaidFee2=dPaidInt2 = 0;
//      if (*apTokens[CA_INST2PENALTYPAYMENT] == '-')
//         dPaidPen2 = atof(apTokens[CA_INST2PENALTYPAYMENT]+1);
//      if (*apTokens[CA_INST2FEEPAYMENT] == '-')
//         dPaidFee2 = atof(apTokens[CA_INST2FEEPAYMENT]+1);
//      if (*apTokens[CA_INST2INTERESTPAYMENT] == '-')
//         dPaidInt2 = atof(apTokens[CA_INST2INTERESTPAYMENT]+1);
//
//      // TaxAmt
//      dTotalTax = dTax1+dTax2;
//      pOutBase->dTotalTax = dTotalTax;
//      sprintf(pOutBase->TotalTaxAmt, "%.2f", dTotalTax);
//
//      dTotalDue = dTaxDue1+dTaxDue2;
//      sprintf(pOutBase->TotalDue, "%.2f", dTotalDue);
//
//      dTotalFees = dFee1+dFee2;
//      sprintf(pOutBase->TotalFees, "%.2f", dTotalFees);
//
//      // Paid status
//      if (dPaid1 >= dTax1)
//         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
//      else if (!dTax1)
//         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
//      else if (dPaid1 < dTax1)
//      {
//         if (dPen1 > 0)
//            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
//         else
//            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
//      } 
//
//      if (dPaid2 >= dTax2)
//         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
//      else if (!dTax2)
//         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
//      else if (dPaid2 < dTax2)
//      {
//         if (dPen2 > 0)
//            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
//         else
//            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
//      } 
//
//      // Create detail
//      iRet = Men_CreateTaxItems(sBase, fdDetail, fdItems, fdAgency);
//      if (iRet == 1)
//      {
//         lNoTax++;
//         lBad++;
//         LogMsg("--- No Detail, taxbillid=%s, PIN=%s", pOutBase->Assmnt_No, pOutBase->Apn);
//      }
//
//      if (iRet >= 0)
//      {
//         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&sBase);
//         fputs(acOutbuf, fdBase);
//
//         lOut++;
//      } else
//         lBad++;
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   fclose(fdBase);
//   fclose(fdItems);
//   fclose(fdAgency);
//   fclose(fdCurrent);
//   fclose(fdDetail);
//
//   LogMsg("Total records processed:    %u", lCnt);
//   LogMsg("         output records:    %u", lOut);
//   LogMsg("           skip records:    %u", lBad);
//   LogMsg("      no detail records:    %u", lNoTax);
//
//   // Import into SQL
//   if (bImport)
//   {
//      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
//      if (!iRet)
//         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
//      if (!iRet)
//      {
//         // Dedup Agency file before import
//         LogMsg("Dedup Agency file %s to %s", sTmpFile, sAgencyFile);
//         iRet = sortFile(sTmpFile, sAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
//         if (iRet > 0)
//            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
//      }
//   } else
//      iRet = 0;
//
//   return iRet;
//}

/**************************** Men_Load_TaxPriorYear *************************
 *
 * Load CA-Mendo-PriorYear.csv
 *
 ****************************************************************************/

//int Men_Load_TaxPriorYear1(char *pDelqFile, bool bImport)
//{
//   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutBuf[2048];
//   char     acTmpFile[_MAX_PATH];
//
//   int      iRet, lCnt=0;
//   FILE     *fdIn, *fdOut;
//   TAXDELQ  *pTaxDelq = (TAXDELQ *)&acBuf[0];
//   double   dTotalTax;
//
//   LogMsg0("Loading tax prior year file");
//
//   // Check for Unicode
//   sprintf(acTmpFile, "%s\\%s\\PriorYear.txt", acTmpPath, myCounty.acCntyCode);
//   if (!UnicodeToAnsi(pDelqFile, acTmpFile))
//      strcpy(pDelqFile, acTmpFile);
//
//   sprintf(acTmpFile, "%s\\%s\\Men_Delq.txt", acTmpPath, myCounty.acCntyCode);
//   iRet = sortFile(pDelqFile, acTmpFile, "S(#1,C,A,#3,C,A) DEL(44) OMIT(#4,C,GE,\"U\") ");
//   if (!iRet)
//      return -1;
//
//   // Open input file
//   LogMsg("Open tax redemption file %s", acTmpFile);
//   fdIn = fopen(acTmpFile, "r");
//   if (fdIn == NULL)
//   {
//      LogMsg("***** Error opening tax redemption file: %s\n", acTmpFile);
//      return -2;
//   }  
//
//   // Create output file
//   sprintf(acTmpFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Delq");
//   LogMsg("Open Delq file %s", acTmpFile);
//   fdOut = fopen(acTmpFile, "w");
//   if (fdOut == NULL)
//   {
//      LogMsg("***** Error creating Delq file: %s\n", acTmpFile);
//      return -4;
//   }
//
//   // Clear output buffer
//   memset(acBuf, 0, sizeof(TAXDELQ));
//   // Updated date
//   sprintf(pTaxDelq->Upd_Date, "%d", lLastTaxFileDate);
//
//   while (!feof(fdIn))
//   {
//      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
//      if (!pTmp || *pTmp > '9')
//         break;
//
//      // Bypass BPP records
//      if (acRec[10] == 'B' || acRec[1] < '0')
//         continue;
//
//      // Parse input record
//      iTokens = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
//      if (iTokens < PY_FLDS)
//      {
//         LogMsg("***** Error: bad delq record for APN=%s (#tokens=%d)", acRec, iTokens);
//         break;
//      }
//
//#ifdef _DEBUG
//      //if (!memcmp(apTokens[MEN_D_PIN], "010008497", 9) )
//      //   iRet = 0;
//#endif
//
//      dTotalTax = atof(apTokens[PY_AMOUNTDUE]);
//      if (dTotalTax > 0.0)
//      {
//         // APN
//         strcpy(pTaxDelq->Apn, myTrim(apTokens[PY_PIN]));
//
//         // Default Number
//         strcpy(pTaxDelq->Default_No, myTrim(apTokens[PY_DEFAULTNUMBER]));
//         strcpy(pTaxDelq->TaxYear, apTokens[PY_DEFAULTYEAR]);
//
//         // Default Amt - When Fee & Pen are added to Delq table, we can break DefAmt to separate fields
//         sprintf(pTaxDelq->Def_Amt, "%.2f", dTotalTax);
//         pTaxDelq->isDelq[0] = '1';
//
//         if (!_memicmp(apTokens[PY_BILLTYPE], "Secured", 7) || !_memicmp(apTokens[PY_BILLTYPE], "Annual", 6))
//            pTaxDelq->isSecd[0] = '1';
//         else
//            pTaxDelq->isSecd[0] = '0';
//
//         // Output delq record for every year
//         Tax_CreateDelqCsv(acOutBuf, (TAXDELQ *)acBuf);
//         fputs(acOutBuf, fdOut);
//      }
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   if (fdOut)
//      fclose(fdOut);
//   if (fdIn)
//      fclose(fdIn);
//
//   LogMsg("Total Delq records processed:    %u", lCnt);
//
//   // Import into SQL
//   if (bImport)
//      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
//   else
//      iRet = 0;
//
//   return iRet;
//}

/*************************** Men_CreateTaxItems() *****************************
 *
 * Parse current detail tax bill to update TaxBase and create TaxItems and TaxAgency
 *
 ******************************************************************************/

int Men_CreateTaxItems(char *pTaxBase, FILE *fdDetail, FILE *fdItems, FILE *fdAgency)
{
   static   char acRec[1024], *apItems[16];
   static   long lPos=0;

   int      iRet, iTmp, iItems;
   double   dTaxRate;
   char     acTmp[1024], acDetail[1024], acAgency[1024], *pTmp;

   TAXBASE   *pOutBase   = (TAXBASE *)pTaxBase;
   TAXDETAIL *pOutDetail = (TAXDETAIL *)acDetail;
   TAXAGENCY *pOutAgency = (TAXAGENCY *)acAgency, *pResult;

   dTaxRate = 0;
   iRet = 1;
   while (!feof(fdDetail))
   {
      // Save current position
      lPos = ftell(fdDetail);
      pTmp = fgets(acRec, 1024, fdDetail);
      if (!pTmp || *pTmp > '9')
         break;
     
      // Parse detail rec
      iItems = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apItems);
      if (iItems < CD_COLS)
      {
         LogMsg("***** Error: bad Tax record for APN=%s (#tokens=%d)", acRec, iItems);
         return -1;
      } else if (iItems > CD_COLS)
      {
         LogMsg("*** WARNING: Too many tokens APN=%s (#tokens=%d).", acRec, iItems);
      }

#ifdef _DEBUG
      // 006183011000
      //if (!memcmp(apItems[CD_TAXBILLID], "391382", 6) )
      //   iRet = 0;
#endif

      iTmp = strcmp(pOutBase->Assmnt_No, myTrim(apItems[CD_TAXBILLID]));
      if (!iTmp)
      {
         // Ignore unsecured records
         if (strstr(apItems[CD_BILLTYPE], "Unsec"))
         {
            pOutBase->BillType[0] = BILLTYPE_UNSECURED;
            continue;
         }

         // Ignore records with no tax amount
         if (*apItems[CD_TOTAL] == '0')
            continue;

         memset(acAgency, 0, sizeof(TAXAGENCY));
         memset(acDetail, 0, sizeof(TAXDETAIL));

         iTmp = iTrim(apItems[CA_PIN]);
         if (iTmp == iApnLen)
            strcpy(pOutDetail->Apn, apItems[CA_PIN]);
         else if (iTmp < iApnLen)
            sprintf(pOutDetail->Apn, "%s%.*s", apItems[CA_PIN], iApnLen-iTmp, "0000");
         else 
            continue;

         strcpy(pOutDetail->Assmnt_No, apItems[CD_TAXBILLID]);
         strcpy(pOutDetail->TaxYear, apItems[CD_TAXYEAR]);
         strcpy(pOutDetail->BillNum, myTrim(apItems[CD_BILLNUMBER]));
         strcpy(pOutDetail->TaxRate, apItems[CD_RATE]);
         strcpy(pOutDetail->TaxAmt, apItems[CD_TOTAL]);
         strcpy(pOutDetail->TaxDesc, myTrim(apItems[CD_DETAILS]));
         //strcpy(pOutDetail->TaxCode, myTrim(apItems[CD_TAXCODE]));
         vmemcpy(pOutDetail->TaxCode, myTrim(apItems[CD_TAXCODE]), 10);
         dTaxRate += atof(pOutDetail->TaxRate);

         // Update TaxBase
         if (!pOutBase->TaxYear[0])
         {
            // pOutBase contains new APN.  We need old APN for current tax roll.
            if (strcmp(pOutBase->Apn, pOutDetail->Apn))
            {
               LogMsg("*** APN mismatched BillID=%s Base->PIN=%s ... Detail->PIN=%s", pOutDetail->BillNum, pOutBase->Apn, pOutDetail->Apn);
               strcpy(pOutBase->Apn, pOutDetail->Apn);
               strcpy(pOutBase->BillNum, pOutDetail->BillNum);
            }

            // Update Tax Year, TRA
            strcpy(pOutBase->TaxYear, pOutDetail->TaxYear);
            iTmp = atol(apItems[CD_TAXRATEAREA]);
            sprintf(pOutBase->TRA, "%.6d", iTmp);

            // Update bill type
            if (!_memicmp(apItems[CD_BILLTYPE], "Sup", 3))
            {
               pOutBase->isSupp[0] = '1';
               pOutBase->isSecd[0] = '0';
               pOutBase->BillType[0] = BILLTYPE_SECURED_SUPPL;
            }
         }

         // Tax code
         if (iNumTaxDist > 1)
         {
            pResult = findTaxAgency(pOutDetail->TaxCode, 0);
            if (pResult)
            {
               strcpy(pOutAgency->Agency, pResult->Agency);
               strcpy(pOutAgency->Code, pResult->Code);
               strcpy(pOutAgency->Phone, pResult->Phone);
               pOutAgency->TC_Flag[0] = pResult->TC_Flag[0];
               pOutDetail->TC_Flag[0] = pResult->TC_Flag[0];
            } else
            {
               strcpy(pOutAgency->Agency, pOutDetail->TaxDesc);
               strcpy(pOutAgency->Code, pOutDetail->TaxCode);
               pOutAgency->Phone[0] = 0;
               pOutAgency->TC_Flag[0] = '0';
               pOutDetail->TC_Flag[0] = '0';
               LogMsg("+++ New Agency: %s:%s [%s]", pOutDetail->TaxCode, pOutDetail->TaxDesc, pOutDetail->Apn);
            }
         }

         // Output detail record
         Tax_CreateDetailCsv(acTmp, (TAXDETAIL *)&acDetail);
         fputs(acTmp, fdItems);

         Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgency);
         fputs(acTmp, fdAgency);
      } else if (iTmp > 0)
      {
         continue;
      } else
      {
         fseek(fdDetail, lPos, SEEK_SET);
         break;
      }
   }

   // Update TaxBase
   sprintf(pOutBase->TotalRate, "%.2f", dTaxRate);

   return iRet;
}

/*************************** Men_Load_TaxCurrent ******************************
 *
 * Load CA-Mendocino-CurrentAmounts.csv & CA-Mendocino-CurrentDistricts.csv
 * Load current tax files and create Base, Items, & Agency import.
 *
 ******************************************************************************/

int Men_Load_TaxCurrent(char *pCurrentTaxFile, char *pDetailFile, bool bImport)
{
   char     *pTmp, acOutbuf[1024], acRec[1024], acCurrent[_MAX_PATH], acDetail[_MAX_PATH], acTmp[256],
            sBase[1024], sAgencyFile[_MAX_PATH], sBaseFile[_MAX_PATH], sItemsFile[_MAX_PATH], sTmpFile[_MAX_PATH];
   long     iTmp, iRet, lOut=0, lCnt=0, lDropUnsec=0, lDropNonTax=0, lBad=0, lNoTax=0;
   int      iDueDateFmt;
   double	dTax1, dTax2, dPen1, dPen2, dFee1, dFee2, dInt1, dInt2, dTaxDue1, dTaxDue2,
   	      dPaid1, dPaid2, dPaidPen1, dPaidPen2, dPaidFee1, dPaidFee2, dPaidInt1, dPaidInt2, 
            dTotalTax, dTotalDue, dTotalFees;

   TAXBASE  *pOutBase = (TAXBASE *)sBase;
   FILE     *fdCurrent, *fdDetail, *fdBase, *fdItems, *fdAgency;

   LogMsg0("Loading current tax file");

   // Check for Unicode
   //sprintf(sTmpFile, "%s\\%s\\CurrentAmounts.txt", acTmpPath, myCounty.acCntyCode);
   //if (!UnicodeToAnsi(pCurrentTaxFile, sTmpFile))
   //   strcpy(pCurrentTaxFile, sTmpFile);

   // Sort current tax file on TaxBillId, PIN
   sprintf(acCurrent, "%s\\%s\\%s_Current.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acTmp, "S(#%d,C,A,#%d,C,A) DEL(44) OMIT(#%d,C,GT,\"9999\")", CA_TAXBILLID+1, CA_PIN+1, CA_TAXBILLID+1);
   iRet = sortFile(pCurrentTaxFile, acCurrent, acTmp);
   if (!iRet)
      return -1;

   // Check for Unicode
   //sprintf(sTmpFile, "%s\\%s\\CurrentDistricts.txt", acTmpPath, myCounty.acCntyCode);
   //if (!UnicodeToAnsi(pDetailFile, sTmpFile))
   //   strcpy(pDetailFile, sTmpFile);

   // Sort detail file on TaxBillId & PIN
   sprintf(acDetail, "%s\\%s\\%s_Detail.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acTmp, "S(#%d,C,A,#%d,C,A) DEL(44) OMIT(#%d,C,GT,\"9999\")", CD_TAXBILLID+1, CD_PIN+1, CD_PIN+1);
   iRet = sortFile(pDetailFile, acDetail, acTmp);
   if (!iRet)
      return -1;

   // Open current tax file
   LogMsg("Open current tax file %s", acCurrent);
   fdCurrent = fopen(acCurrent, "r");
   if (fdCurrent == NULL)
   {
      LogMsg("***** Error opening current tax file: %s\n", acCurrent);
      return -2;
   }  

   // Open detail file
   LogMsg("Open tax detail file %s", acDetail);
   fdDetail = fopen(acDetail, "r");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error opening tax Detail file: %s\n", acDetail);
      return -2;
   }  

   // Create output file names
   NameTaxCsvFile(sBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(sItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(sAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(sTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   LogMsg("Create TaxBase file %s", sBaseFile);
   fdBase = fopen(sBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating TaxBase file: %s\n", sBaseFile);
      return -4;
   }

   LogMsg("Create TaxItems file %s", sItemsFile);
   fdItems = fopen(sItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating TaxItems file: %s\n", sItemsFile);
      return -4;
   }

   LogMsg("Create TaxAgency file %s", sTmpFile);
   fdAgency = fopen(sTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating TaxAgency file: %s\n", sTmpFile);
      return -4;
   }

   // Init variables
   iNewAgency = 0;
   iDueDateFmt = GetPrivateProfileInt(myCounty.acCntyCode, "DueDateFmt", DD_MM_YYYY, acIniFile);

   while (!feof(fdCurrent))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdCurrent);
      if (!pTmp)
         break;
      lCnt++;

      if (*pTmp > '9')
      {
         lBad++;
         if (bDebug)
            LogMsg("*** Invalid APN: %.50s", pTmp);
         continue;
      }

      // Parse input tax data
      iTokens = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < CA_COLS)
      {
         LogMsg("***** Error: bad Tax bill record for taxbillid=%s (#tokens=%d)", acRec, iTokens);
         return -1;
      }

      // Reset buffer
      memset(sBase, 0, sizeof(TAXBASE));

      // Populate TaxBase
      strcpy(pOutBase->Assmnt_No, apTokens[CA_TAXBILLID]);
#ifdef _DEBUG
      //if (!memcmp(pOutBase->Assmnt_No, "406809", 6))
      //   iTmp = 0;
#endif

      iTmp = iTrim(apTokens[CA_PIN]);
      if (iTmp == iApnLen)
         strcpy(acTmp, apTokens[CA_PIN]);
      else if (iTmp < iApnLen)
         sprintf(acTmp, "%s%.*s", apTokens[CA_PIN], iApnLen-iTmp, "0000");
      else 
      {
         if (bDebug)
            LogMsg("*** Ignore BPP bill: APN=%s, BillID=%s", apTokens[CA_PIN], apTokens[CA_TAXBILLID]);
         lBad++;
         continue;
      }

      strcpy(pOutBase->Apn, acTmp);
      strcpy(pOutBase->BillNum, myTrim(apTokens[CA_BILLNUMBER]));

      // 1st inst charge/paid/due
      strcpy(pOutBase->TaxAmt1, apTokens[CA_INST1TAXCHARGE]);
      if (*apTokens[CA_INST1TAXPAYMENT] == '-')
         strcpy(pOutBase->PaidAmt1, apTokens[CA_INST1TAXPAYMENT]+1);
      else
         strcpy(pOutBase->PaidAmt1, apTokens[CA_INST1TAXPAYMENT]);
      dTax1 = atof(apTokens[CA_INST1TAXCHARGE]);
      dPaid1 = atof(pOutBase->PaidAmt1);
      dTaxDue1 = atof(apTokens[CA_INST1AMOUNTDUE]);

      // 1st penalty/fee/int
      dPen1 = atof(apTokens[CA_INST1PENALTYCHARGE]);
      strcpy(pOutBase->PenAmt1, apTokens[CA_INST1PENALTYCHARGE]);
      dFee1 = atof(apTokens[CA_INST1FEECHARGE]);
      dInt1 = atof(apTokens[CA_INST1INTERESTCHARGE]);

      dPaidPen1=dPaidFee1=dPaidInt1 = 0;
      if (*apTokens[CA_INST1PENALTYPAYMENT] == '-')
         dPaidPen1 = atof(apTokens[CA_INST1PENALTYPAYMENT]+1);
      if (*apTokens[CA_INST1FEEPAYMENT] == '-')
         dPaidFee1 = atof(apTokens[CA_INST1FEEPAYMENT]+1);
      if (*apTokens[CA_INST1INTERESTPAYMENT] == '-')
         dPaidInt1 = atof(apTokens[CA_INST1INTERESTPAYMENT]+1);

      // 2nd inst charge/paid/due
      strcpy(pOutBase->TaxAmt2, apTokens[CA_INST2TAXCHARGE]);
      if (*apTokens[CA_INST2TAXPAYMENT] == '-')
         strcpy(pOutBase->PaidAmt2, apTokens[CA_INST2TAXPAYMENT]+1);
      else
         strcpy(pOutBase->PaidAmt2, apTokens[CA_INST2TAXPAYMENT]);
      dTax2 = atof(apTokens[CA_INST2TAXCHARGE]);
      dPaid2 = atof(pOutBase->PaidAmt2);
      dTaxDue2 = atof(apTokens[CA_INST2AMOUNTDUE]);

      // 2nd penalty/fee/int
      dPen2 = atof(apTokens[CA_INST2PENALTYCHARGE]);
      strcpy(pOutBase->PenAmt2, apTokens[CA_INST2PENALTYCHARGE]);
      dFee2 = atof(apTokens[CA_INST2FEECHARGE]);
      dInt2 = atof(apTokens[CA_INST2INTERESTCHARGE]);
      dPaidPen2=dPaidFee2=dPaidInt2 = 0;
      if (*apTokens[CA_INST2PENALTYPAYMENT] == '-')
         dPaidPen2 = atof(apTokens[CA_INST2PENALTYPAYMENT]+1);
      if (*apTokens[CA_INST2FEEPAYMENT] == '-')
         dPaidFee2 = atof(apTokens[CA_INST2FEEPAYMENT]+1);
      if (*apTokens[CA_INST2INTERESTPAYMENT] == '-')
         dPaidInt2 = atof(apTokens[CA_INST2INTERESTPAYMENT]+1);

      // TaxAmt
      dTotalTax = dTax1+dTax2;
      pOutBase->dTotalTax = dTotalTax;
      sprintf(pOutBase->TotalTaxAmt, "%.2f", dTotalTax);

      dTotalDue = dTaxDue1+dTaxDue2;
      sprintf(pOutBase->TotalDue, "%.2f", dTotalDue);

      dTotalFees = dFee1+dFee2;
      sprintf(pOutBase->TotalFees, "%.2f", dTotalFees);

      // Due Date - there is no paid date
      pTmp = dateConversion(apTokens[CA_INST1DUEDATE], acTmp, iDueDateFmt, lLienYear);
      if (pTmp)
         strcpy(pOutBase->DueDate1, acTmp);
      else
      {
         iTmp = InstDueDate(pOutBase->DueDate1, 1, lTaxYear);
         LogMsg("***** Bad DueDate1: %s [%s]: Use default: %s.", apTokens[CA_INST1DUEDATE], apTokens[CA_PIN], pOutBase->DueDate1);
      }
      pTmp = dateConversion(apTokens[CA_INST2DUEDATE], acTmp, iDueDateFmt, lLienYear+1);
      if (pTmp)
         strcpy(pOutBase->DueDate2, acTmp);
      else if (dTax2 > 0)
      {
         iTmp = InstDueDate(pOutBase->DueDate2, 2, lTaxYear);
         LogMsg("***** Bad DueDate2: %s [%s]: Use default: %s.", apTokens[CA_INST2DUEDATE], apTokens[CA_PIN], pOutBase->DueDate2);
      }

      // Paid status
      if (dPaid1 >= dTax1)
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      else if (!dTax1)
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else if (dPaid1 < dTax1)
      {
         if (dPen1 > 0)
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      if (dPaid2 >= dTax2)
         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
      else if (!dTax2)
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else if (dPaid2 < dTax2)
      {
         if (dPen2 > 0)
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Set default - these values can be updated in Sut_CreateTaxItems
      pOutBase->isSecd[0] = '1';
      pOutBase->isSupp[0] = '0';
      pOutBase->BillType[0] = BILLTYPE_SECURED;

      // Create tax detail record
      iRet = Men_CreateTaxItems(sBase, fdDetail, fdItems, fdAgency);

      if (pOutBase->BillType[0] >= BILLTYPE_UNSECURED && pOutBase->BillType[0] <= BILLTYPE_UNSECURED_INT_OWNER)
      {
         if (bDebug)
            LogMsg("*** Drop unsecured record: BillNum=%s", pOutBase->BillNum);
         lDropUnsec++;
      } else if (pOutBase->dTotalTax == 0)
      {
         if (bDebug)
            LogMsg("*** Drop non-tax record: BillNum=%s", pOutBase->BillNum);
         lDropNonTax++;
      } else
      if (iRet >= 0)
      {
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&sBase);
         fputs(acOutbuf, fdBase);

         lOut++;
      } else
         lBad++;

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdBase);
   fclose(fdItems);
   fclose(fdAgency);
   fclose(fdCurrent);
   fclose(fdDetail);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("         output records:    %u", lOut);
   LogMsg(" drop unsecured records:    %u", lDropUnsec);
   LogMsg("   drop Non-Tax records:    %u", lDropNonTax);
   LogMsg("            bad records:    %u", lBad);
   LogMsg("      no detail records:    %u", lNoTax);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", sTmpFile, sAgencyFile);
         iRet = sortFile(sTmpFile, sAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/**************************** Men_Load_TaxPriorYear *************************
 *
 * Load CA-Mendocino-PriorYear.csv
 *
 ****************************************************************************/

int Men_Load_TaxPriorYear(char *pDelqFile, bool bImport)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutBuf[2048];
   char     acTmpFile[_MAX_PATH];

   int      iRet, lCnt=0, lOut=0;
   FILE     *fdIn, *fdOut;
   TAXDELQ  *pTaxDelq = (TAXDELQ *)&acBuf[0];
   double   dTotalTax;

   LogMsg0("Loading tax prior year file");

   // Check for Unicode
   //sprintf(acTmpFile, "%s\\%s\\PriorYear.txt", acTmpPath, myCounty.acCntyCode);
   //if (!UnicodeToAnsi(pDelqFile, acTmpFile))
   //   strcpy(pDelqFile, acTmpFile);

   sprintf(acTmpFile, "%s\\%s\\MEN_Delq.txt", acTmpPath, myCounty.acCntyCode);
   iRet = sortFile(pDelqFile, acTmpFile, "S(#1,C,A,#3,C,A) DEL(44) OMIT(#4,C,GE,\"U\",OR,#1,C,GT,\"9999\") ");
   if (!iRet)
      return -1;

   // Open input file
   LogMsg("Open tax redemption file %s", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax redemption file: %s\n", acTmpFile);
      return -2;
   }  

   // Create output file
   NameTaxCsvFile(acTmpFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open Delq file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Delq file: %s\n", acTmpFile);
      return -4;
   }

   // Clear output buffer
   memset(acBuf, 0, sizeof(TAXDELQ));
   // Updated date
   sprintf(pTaxDelq->Upd_Date, "%d", lLastTaxFileDate);

   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp > '9')
         break;

      // Bypass BPP records
      if (acRec[10] == 'B' || acRec[1] < '0')
         continue;

      // Parse input record
      iTokens = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < PY_COLS)
      {
         LogMsg("***** Error: bad delq record for APN=%s (#tokens=%d)", acRec, iTokens);
         break;
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[Iny_D_PIN], "010008497", 9) )
      //   iRet = 0;
#endif

      dTotalTax = atof(apTokens[PY_AMOUNTDUE]);
      if (dTotalTax > 0.0)
      {
         // APN
         //iRet = Sut_FormatPrevApn(myTrim(apTokens[PY_PIN]), pTaxDelq->Apn);
         iRet = iTrim(apTokens[PY_PIN]);
         if (iRet == iApnLen)
            strcpy(pTaxDelq->Apn, apTokens[PY_PIN]);
         else if (iRet < iApnLen)
            sprintf(pTaxDelq->Apn, "%.*s%s", iApnLen-iRet, "0000", apTokens[PY_PIN]);
         else 
            vmemcpy(pTaxDelq->Apn, apTokens[PY_PIN], iApnLen);

         // Default Number
         strcpy(pTaxDelq->Default_No, myTrim(apTokens[PY_DEFAULTNUMBER]));
         strcpy(pTaxDelq->TaxYear, apTokens[PY_DEFAULTYEAR]);

         // Default Amt - When Fee & Pen are added to Delq table, we can break DefAmt to separate fields
         sprintf(pTaxDelq->Def_Amt, "%.2f", dTotalTax);
         pTaxDelq->isDelq[0] = '1';

         if (!_memicmp(apTokens[PY_BILLTYPE], "Secured Annual", 10))
            pTaxDelq->isSecd[0] = '1';
         else if (!_memicmp(apTokens[PY_BILLTYPE], "Secured Supp", 10))
         {
            pTaxDelq->isSecd[0] = '0';
            pTaxDelq->isSupp[0] = '1';
         } else
            pTaxDelq->isSecd[0] = '0';

         // Output delq record for every year
         Tax_CreateDelqCsv(acOutBuf, (TAXDELQ *)acBuf);
         fputs(acOutBuf, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fdOut)
      fclose(fdOut);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total Delq records processed:  %u", lCnt);
   LogMsg("      Delq records output:     %u", lOut);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

/******************************************************************************
 *
 * -L = load lien
 * -U = load update
 * -Xl= extract lien
 *
 * Regular update: -CMEN -U -Xsi -T
 *
 ******************************************************************************/

int loadMen(int iSkip)
{
   int   iRet;
   char  acTmpFile[_MAX_PATH], acDetailFile[_MAX_PATH], acPaidFile[_MAX_PATH], 
         acDelqFile[_MAX_PATH], acSortCmd[256];

   iApnLen = myCounty.iApnLen;

   // 02/05/2023
   //iRet = FixCumSale(acCSalFile, SALE_FLD_DOCNUM+CNTY_MEN);

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      GetIniString(myCounty.acCntyCode, "TaxDetail", "", acDetailFile, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "TaxPaid", "", acPaidFile, _MAX_PATH, acIniFile);
      lLastTaxFileDate = getFileDate(acPaidFile);
      // Only process if new tax file
      iRet = isNewTaxFile(acPaidFile, myCounty.acCntyCode);
      if (iRet <= 0)
      {
         lLastTaxFileDate = 0;
         return iRet;
      }

      // 03/09/2021 - Load Current Amount & District data
      iRet = Men_Load_TaxCurrent(acPaidFile, acDetailFile, bTaxImport);
      // As of 01/10/2024 - Redemption file doesn't have delq amount.
      //if (!iRet)
      //{
      //   GetIniString(myCounty.acCntyCode, "Redemption", "", acDelqFile, _MAX_PATH, acIniFile);
      //   iRet = Men_Load_TaxPriorYear(acDelqFile, bTaxImport);
      //   if (!iRet)
      //      iRet = updateDelqFlag(myCounty.acCntyCode);
      //}
   }

   if (!iLoadFlag)
      return 0;

   // Load char file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      if (!_access(acCharFile, 0))
      {
         iRet = Men_ConvStdCharCsv(acCharFile);
         if (iRet > 0)
            iRet = 0;
      } else
         LogMsg0("*** CHAR file not available: %s", acCharFile);
   }

   // Load new sale file LDR 2022
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      if (!_access(acSalesFile, 0))
      {
         // Load special table for Sale
         GetIniString(myCounty.acCntyCode, "Sale_Xref", "", acTmpFile, _MAX_PATH, acIniFile);
         iNumDeeds = LoadXrefTable(acTmpFile, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

         iRet = Men_ExtrSaleCsv();
         if (!iRet)
            iLoadFlag |= MERG_CSAL;
      } else
         LogMsg0("*** Sale file not available: %s", acSalesFile);
   }

   // Extract NDC recorder sale 
   if (iLoadFlag & EXTR_NRSAL)                     // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSalesFile, _MAX_PATH, acIniFile);
      if (!_access(acSalesFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSalesFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSalesFile);
         return -1;
      }
   }

   iRet = 0;
   if (iLoadFlag & EXTR_LIEN)                         // -Xl
   {
      if (lLienYear >= 2023)
      {
         GetIniString(myCounty.acCntyCode, "LienValue", "", acRollFile, _MAX_PATH, acIniFile);
         sprintf(acTmpFile, "%s\\MEN\\Men_Value.srt", acTmpPath);
         sprintf(acSortCmd, "S(#1,C,A) OMIT(#1,C,GT,\"A\",OR,#1,C,LT,\"0\") DEL(%d) F(TXT)", cDelim);
         if (sortFile(acRollFile, acTmpFile, acSortCmd, &iRet) > 0)
            strcpy(acRollFile, acTmpFile);
         else
            return iRet;
         if (lLienYear >= 2024)
            iRet = Men_ExtrRollCsv2();
         else
            iRet = Men_ExtrRollCsv();
      } else
      {
         // Check for Unicode
         sprintf(acTmpFile, "%s\\MEN\\Men_Lien.txt", acTmpPath);
         if (!UnicodeToAnsi(acRollFile, acTmpFile))
            strcpy(acRollFile, acTmpFile);

         sprintf(acTmpFile, "%s\\MEN\\Men_Lien.srt", acTmpPath);
         sprintf(acSortCmd, "S(#1,C,A) DEL(%d) F(TXT)", cDelim);
         if (sortFile(acRollFile, acTmpFile, acSortCmd, &iRet) > 0)
            strcpy(acRollFile, acTmpFile);
         else
            return iRet;
         iRet = Men_ExtrLienCsv();
      }
   }

   if (iLoadFlag & LOAD_LIEN)                         // -L
   {
      // Create PQ4 file
      LogMsg0("Load %s LDR roll", myCounty.acCntyCode);
      if (lLienYear >= 2024)
         iRet = Men_Load_LDR_Csv2(iSkip);
      else if (lLienYear == 2023)
         iRet = Men_Load_LDR_Csv1(iSkip);
      else
         iRet = Men_Load_LDR_Csv(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)                  // -U
   {
      // Create PQ4 file
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      //iRet = Men_Load_Roll(iSkip);
      iRet = Men_Load_RollCsv(iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )             // -Ms
   {
      // Apply Men_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_UPD_SALE);
   }

   // Apply NDC sale file to R01
   if (!iRet && (iLoadFlag & UPDT_XSAL))              // -Mn
   {
      // Apply Men_Ash.sls to R01 file
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSaleNR(iSkip, acTmpFile, SALE_USE_SCUPDXFR);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   return iRet;
}
