/*****************************************************************************
 *
 * This file is prepared for CCX and counties that use zoning data from Denise.
 * All data must be preprocessed by LoadZoning program before calling MergeZoning().
 *
 * 03/25/2020  Modify MergeZoning() to clear zoning before applying new one to avoid any left over.
 * 04/07/2020  Modify MergeZoning() log msg if missing zoning file
 * 06/24/2020  Add support for LoadCres
 * 07/20/2020  Modify MergeZoneRec() to populate PQZoning only.  Leave old Zoning field as county supplied.
 * 08/13/2020  Modify MergeZoning() to use zoning in [Data] section of INI file if not defined in county section.
 * 10/15/2020  Modify MergeZoning() to add ZONE_X4.
 * 10/26/2020  Modify MergeZoneRec() to remove old zoning only if there is new one.
 * 12/12/2020  Rename MergeZoneRec() to MergeZoneRec1() and add MergeZoneRec2() to support variable length APN.
 * 01/06/2021  Modify MergeZoneRec1&2() to use ApnLen from parent function, not global iApnLen.
 *             Modify MergeZoning() to get ApnLen from INI file (default iApnLen).
 * 01/14/2021  Modify MergeZoneRec1 && MergeZoneRec2 to fix bug when read pointer is null.
 * 07/21/2021  Modify MergeZoning() to initialize lZoneMatch & lZoneSkip.
 * 09/13/2021  Modify MergeZoneRec1() to merge PQZoningType
 * 09/16/2021  Fix bug in MergeZoneRec1() & MergeZoneRec2().
 * 09/26/2021  Clean up code in MergeZoneRec1() & MergeZoneRec2().
 *             Modify MergeZoning() to keep output file as .R01
 * 11/07/2021  Modify MergeZoning() to remove all left over zoning.
 * 11/18/2021  Fix bug in MergeZoneRec1() & MergeZoneRec2() when ZoneLen > SIZ_ZONE_X1
 * 03/11/2022  Modify MergeZoning() & MergeZoneRec() to check for ZonyType in INI file.
 * 03/15/2022  Add MergeZoneRec3() for SFX to support embedded '|' in zone code.
 * 08/04/2022  Add MergeZoneRec4().  This function is similar to ZoneRec1, but use PREV_APN.
 * 03/02/2023  Add MergeZoneRecX() to support multiple APN choice such as PREV_APN, ALT_APN, ... (VEN)
 * 10/02/2023  Modify MergeZoning() to support LAX.
 * 05/24/2024  Log longest zonecode.
 * 08/13/2024  Add ZONE_JURIS & ZONE_DESC.
 * 10/17/2024  Modify MergeZoneRec?() to clean out ZONE_DESC before apply new one.
 * 10/27/2024  Modify MergeZoneRec?() to make sure ZONE_CODE ended with '~'.
 * 10/30/2024  Modify MergeZoning() to reset ZONE_DESC before updating to avoid leftover.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "Prodlib.h"
#include "R01.h"
#include "Tables.h"
#include "Logs.h"
#include "Utils.h"
#include "doSort.h"
#if defined(AFX_STDAFX_H__LOADONE_EFA4_45F7_9334_A6196A45181B__INCLUDED_)
   #include "LOExtrn.h"
#endif
#if defined(AFX_STDAFX_H__LOADMB_EFA4_45F7_9334_A6196A45181B__INCLUDED_)
   #include "MBExtrn.h"
#endif
#if defined(AFX_STDAFX_H__LOADCRES_500C_46D1_99BD_BD199B02CBC1__INCLUDED_)
   #include "LCExtrn.h"
#endif
#include "MergeZoning.h"


FILE  *fdZone;
long  lZoneSkip, lZoneMatch;
int   iMaxZoning, iMaxZoneType, iMaxZoneLen=SIZ_ZONE_X1+SIZ_ZONE_X2+SIZ_ZONE_X3+SIZ_ZONE_X4;
bool  bAddZoneType;
char  g_sZoning[256];

/********************************* MergeZoneRec ******************************
 *
 * Merge Zoning from ???_Zone.txt
 * This function will overwrite zoning from county roll 
 *    - MergeZoneRec1() is for fixed length APN.
 *    - MergeZoneRec2() is for variable length APN.
 *    - MergeZoneRec3() is for special case SFX.
 *    - MergeZoneRecX() is for using other APN field (i.e. PREV_APN or ALT_APN)
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int MergeZoneRec1(char *pOutbuf, int iZoneApnLen)
{
   static   char acRec[1024], *pRec=NULL;
   static   int  iCnt=0;
   char     *apItems[16], *pNextZone, acTmp[64], *pTmp;
   int      iRet=0, iTmp, iZoneLen;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 1024, fdZone);
      if (pRec && *pRec > '9')
         pRec = fgets(acRec, 1024, fdZone);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006351003000 ", 10))
   //   iTmp = 0;
#endif
   do
   {
      if (!pRec)
      {
         fclose(fdZone);
         fdZone = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iZoneApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Zoning rec %.*s", iZoneApnLen, pRec);
         pRec = fgets(acRec, 1024, fdZone);
         lZoneSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iCnt = ParseStringIQ(acRec, '|', 16, apItems);
   if (iCnt < ZONE_TYPE)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdZone);
         fdZone = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input roll record for APN=%s", apItems[0]);
         iRet = -1;
      }

      return iRet;
   }

   pNextZone = NULL;

   // Zoning
   if (*apItems[ZONE_CODE] > ' ')
   {
      lZoneMatch++;

      iZoneLen = iTrim(apItems[ZONE_CODE]);
      if (iZoneLen > iMaxZoning)
      {
         iMaxZoning = iZoneLen;
         strcpy(g_sZoning, apItems[ZONE_CODE]);
      }

      // PQZoning Type
      if (bAddZoneType) 
      {
         if (iCnt > ZONE_TYPE && *apItems[ZONE_TYPE] > ' ')
         {
            iTmp = iTrim(apItems[ZONE_TYPE]);
            vmemcpy(pOutbuf+OFF_ZONECAT, apItems[ZONE_TYPE], SIZ_ZONECAT, iTmp);
            if (iTmp > iMaxZoneType) iMaxZoneType = iTmp;
         }
      } else
         memset(pOutbuf+OFF_ZONECAT, ' ', SIZ_ZONECAT);

      // Zoning Jurisdiction
      if (*apItems[ZONE_JURI] > ' ')
      {
         iTmp = Juris2Code(apItems[ZONE_JURI], acTmp, pOutbuf);
         if (iTmp > 0)
            vmemcpy(pOutbuf+OFF_ZONE_JURIS, acTmp, SIZ_ZONE_JURIS);
      }

      // Zoning Desc
      if (iCnt > ZONE_DESC && *apItems[ZONE_DESC] > ' ')
      {
         iTmp = strlen(apItems[ZONE_DESC]);
         vmemcpy(pOutbuf+OFF_ZONE_DESC, apItems[ZONE_DESC], SIZ_ZONE_DESC, iTmp-1);
      }

      if (iZoneLen > iMaxZoneLen)
      {
         *(apItems[ZONE_CODE]+iMaxZoneLen) = 0;
         pTmp = strrchr(apItems[ZONE_CODE], '~');
         // Alway terminate ZONE_CODE with '~'
         if (pTmp)
            *(pTmp+1) = 0;
      }
      vmemcpy(pOutbuf+OFF_ZONE_X1, apItems[ZONE_CODE], SIZ_ZONE_X1);
      if (iZoneLen > SIZ_ZONE_X1)
      {
         pNextZone = apItems[ZONE_CODE]+SIZ_ZONE_X1;
         iTmp = strlen(pNextZone);
         vmemcpy(pOutbuf+OFF_ZONE_X2, pNextZone, SIZ_ZONE_X2, iTmp);
         if (iTmp > SIZ_ZONE_X2)
         {
            pNextZone += SIZ_ZONE_X2;
            iTmp = strlen(pNextZone);
            vmemcpy(pOutbuf+OFF_ZONE_X3, pNextZone, SIZ_ZONE_X3, iTmp);
            if (iTmp > SIZ_ZONE_X3)
            {
               vmemcpy(pOutbuf+OFF_ZONE_X4, pNextZone+SIZ_ZONE_X3, SIZ_ZONE_X4);
#ifdef _DEBUG
               LogMsg0("APN=%s PQZoning=\"%s\"", apItems[ZONE_ASMT], apItems[ZONE_CODE]);
#endif
            }
         }
      }
   }

   // Get next record
   pRec = fgets(acRec, 1024, fdZone);

   return 0;
}

int MergeZoneRec2(char *pOutbuf, int iZoneApnLen)
{
   static   char acRec[1024], *pRec=NULL;
   static   int  iCnt=0;
   char     *apItems[16], *pNextZone, *pTmp, acTmp[64];
   int      iRet=0, iTmp, iLen, iZoneLen;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 1024, fdZone);
      if (pRec && *pRec > '9')
         pRec = fgets(acRec, 1024, fdZone);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdZone);
         fdZone = NULL;
         return 1;      // EOF
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "0012 007", 8))
      //   iLen = 0;
#endif

      pTmp = strchr(pRec, '|');
      iLen = pTmp - pRec;
      if (iLen < iZoneApnLen)
         iTmp = memcmp(pOutbuf, pRec, iLen);
      else
         iTmp = memcmp(pOutbuf, pRec, iZoneApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Zoning rec %.*s", iZoneApnLen, pRec);
         pRec = fgets(acRec, 1024, fdZone);
         lZoneSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   //iCnt = ParseStringIQ(acRec, '|', 16, apItems);
   iCnt = ParseStringEx(acRec, '|', '~', 16, apItems);
   if (iCnt < ZONE_TYPE)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdZone);
         fdZone = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input roll record for APN=%s", apItems[0]);
         iRet = -1;
      }

      return iRet;
   }

   pNextZone = NULL;

   // Zoning
   if (*apItems[ZONE_CODE] > ' ')
   {
      lZoneMatch++;

      iZoneLen = iTrim(apItems[ZONE_CODE]);
      if (iZoneLen > iMaxZoning)
      {
         iMaxZoning = iZoneLen;
         strcpy(g_sZoning, apItems[ZONE_CODE]);
      }

      if (bAddZoneType) 
      {
         if (iCnt > ZONE_TYPE && *apItems[ZONE_TYPE] > ' ')
         {
            iTmp = iTrim(apItems[ZONE_TYPE]);
            vmemcpy(pOutbuf+OFF_ZONECAT, apItems[ZONE_TYPE], SIZ_ZONECAT, iTmp);
            if (iTmp > iMaxZoneType) iMaxZoneType = iTmp;
         }
      } else
         memset(pOutbuf+OFF_ZONECAT, ' ', SIZ_ZONECAT);

      // Zoning Jurisdiction
      if (*apItems[ZONE_JURI] > ' ')
      {
         iTmp = Juris2Code(apItems[ZONE_JURI], acTmp, pOutbuf);
         if (iTmp > 0)
            vmemcpy(pOutbuf+OFF_ZONE_JURIS, acTmp, SIZ_ZONE_JURIS);
      }

      // Zoning Desc
      if (iCnt > ZONE_DESC && *apItems[ZONE_DESC] > ' ')
      {
         iTmp = strlen(apItems[ZONE_DESC]);
         vmemcpy(pOutbuf+OFF_ZONE_DESC, apItems[ZONE_DESC], SIZ_ZONE_DESC, iTmp-1);
      }

      if (iZoneLen > iMaxZoneLen)
      {
         *(apItems[ZONE_CODE]+iMaxZoneLen) = 0;
         pTmp = strrchr(apItems[ZONE_CODE], '~');
         // Alway terminate ZONE_CODE with '~'
         if (pTmp)
            *(pTmp+1) = 0;
      }

      vmemcpy(pOutbuf+OFF_ZONE_X1, apItems[ZONE_CODE], SIZ_ZONE_X1);
      if (iLen > SIZ_ZONE_X1)
      {
         pNextZone = apItems[ZONE_CODE]+SIZ_ZONE_X1;
         iTmp = strlen(pNextZone);
         vmemcpy(pOutbuf+OFF_ZONE_X2, pNextZone, SIZ_ZONE_X2, iTmp);
         if (iTmp > SIZ_ZONE_X2)
         {
            pNextZone += SIZ_ZONE_X2;
            iTmp = strlen(pNextZone);
            vmemcpy(pOutbuf+OFF_ZONE_X3, pNextZone, SIZ_ZONE_X3, iTmp);
            if (iTmp > SIZ_ZONE_X3)
            {
               vmemcpy(pOutbuf+OFF_ZONE_X4, pNextZone+SIZ_ZONE_X3, SIZ_ZONE_X4);
#ifdef _DEBUG
               LogMsg0("APN=%s PQZoning=\"%s\"", apItems[ZONE_ASMT], apItems[ZONE_CODE]);
#endif
            }
         }
      }
   }

   // Get next record
   pRec = fgets(acRec, 1024, fdZone);

   return 0;
}

int MergeZoneRec3(char *pOutbuf, int iZoneApnLen)
{
   static   char acRec[1024], *pRec=NULL;
   static   int  iCnt=0;
   char     *apItems[16], *pNextZone, *pTmp, acZoneCode[64], acZoneType[64], acTmp[64];
   int      iRet=0, iTmp, iZoneLen, iTypeLen;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 1024, fdZone);
      if (pRec && *pRec > '9')
         pRec = fgets(acRec, 1024, fdZone);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdZone);
         fdZone = NULL;
         return 1;      // EOF
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "0012 007", 8))
      //   iLen = 0;
#endif

      pTmp = strchr(pRec, '|');
      iRet = pTmp - pRec;
      if (iRet < iZoneApnLen)
         iTmp = memcmp(pOutbuf, pRec, iRet);
      else
         iTmp = memcmp(pOutbuf, pRec, iZoneApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Zoning rec %.*s", iZoneApnLen, pRec);
         pRec = fgets(acRec, 1024, fdZone);
         lZoneSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iCnt = ParseStringEx(acRec, '|', '~', 16, apItems);
   if (iCnt < ZONE_TYPE)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdZone);
         fdZone = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input roll record for APN=%s", apItems[0]);
         iRet = -1;
      }

      return iRet;
   }

   pNextZone = NULL;

   // Zoning
   if (*apItems[ZONE_CODE] > ' ')
   {
      lZoneMatch++;

      iZoneLen = sprintf(acZoneCode, "~%s~", apItems[ZONE_CODE]);
      iTypeLen = sprintf(acZoneType, "~%s~", apItems[ZONE_TYPE]);
      if (iZoneLen > iMaxZoning)
      {
         iMaxZoning = iZoneLen;
         strcpy(g_sZoning, acZoneCode);
      }

      if (bAddZoneType) 
      {
         if (iCnt > ZONE_TYPE && *apItems[ZONE_TYPE] > ' ')
         {
            vmemcpy(pOutbuf+OFF_ZONECAT, acZoneType, SIZ_ZONECAT, iTypeLen);
            if (iTypeLen > iMaxZoneType) iMaxZoneType = iTypeLen;
         }
      } else
         memset(pOutbuf+OFF_ZONECAT, ' ', SIZ_ZONECAT);

      // Zoning Jurisdiction
      if (*apItems[ZONE_JURI] > ' ')
      {
         iTmp = Juris2Code(apItems[ZONE_JURI], acTmp, pOutbuf);
         if (iTmp > 0)
            vmemcpy(pOutbuf+OFF_ZONE_JURIS, acTmp, SIZ_ZONE_JURIS);
      }

      // Zoning Desc
      if (iCnt > ZONE_DESC && *apItems[ZONE_DESC] > ' ')
      {
         iTmp = strlen(apItems[ZONE_DESC]);
         vmemcpy(pOutbuf+OFF_ZONE_DESC, apItems[ZONE_DESC], SIZ_ZONE_DESC, iTmp-1);
      }

      if (iZoneLen > iMaxZoneLen)
      {
         *(apItems[ZONE_CODE]+iMaxZoneLen) = 0;
         pTmp = strrchr(apItems[ZONE_CODE], '~');
         // Alway terminate ZONE_CODE with '~'
         if (pTmp)
            *(pTmp+1) = 0;
      }

      vmemcpy(pOutbuf+OFF_ZONE_X1, acZoneCode, SIZ_ZONE_X1);
      if (iZoneLen > SIZ_ZONE_X1)
      {
         pNextZone = &acZoneCode[SIZ_ZONE_X1];
         iTmp = strlen(pNextZone);
         vmemcpy(pOutbuf+OFF_ZONE_X2, pNextZone, SIZ_ZONE_X2, iTmp);
         if (iTmp > SIZ_ZONE_X2)
         {
            pNextZone += SIZ_ZONE_X2;
            iTmp = strlen(pNextZone);
            vmemcpy(pOutbuf+OFF_ZONE_X3, pNextZone, SIZ_ZONE_X3, iTmp);
            if (iTmp > SIZ_ZONE_X3)
            {
               vmemcpy(pOutbuf+OFF_ZONE_X4, pNextZone+SIZ_ZONE_X3, SIZ_ZONE_X4);
#ifdef _DEBUG
               LogMsg0("APN=%s PQZoning=\"%s\"", apItems[ZONE_ASMT], apItems[ZONE_CODE]);
#endif
            }
         }
      }
   }

   // Get next record
   pRec = fgets(acRec, 1024, fdZone);

   return 0;
}

// This function need more testing 3/2/2023 - spn
//int MergeZoneRecXX(char *pOutbuf, int iZoneApnLen, int iApnRoot)
//{
//   static   char acRec[1024], *pRec=NULL;
//   static   int  iCnt=0;
//   char     *apItems[16], *pNextZone, *pTmp, acZoneCode[64], acZoneType[64];
//   int      iRet=0, iTmp, iZoneLen, iTypeLen;
//
//   // Get rec
//   if (!pRec)
//   {
//      // Get first rec
//      pRec = fgets(acRec, 1024, fdZone);
//      if (pRec && *pRec > '9')
//         pRec = fgets(acRec, 1024, fdZone);
//   }
//
//   do
//   {
//      if (!pRec)
//      {
//         fclose(fdZone);
//         fdZone = NULL;
//         return 1;      // EOF
//      }
//#ifdef _DEBUG
//      //if (!memcmp(pOutbuf, "0191104200", 8))
//      //   iRet = 0;
//#endif
//
//      pTmp = strchr(pRec, '|');
//      iRet = pTmp - pRec;
//      if (iRet < iZoneApnLen)
//         iTmp = memcmp(pOutbuf+iApnRoot, pRec, iRet);
//      else
//         iTmp = memcmp(pOutbuf+iApnRoot, pRec, iZoneApnLen);
//      if (iTmp > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Zoning rec %.*s", iZoneApnLen, pRec);
//         pRec = fgets(acRec, 1024, fdZone);
//         lZoneSkip++;
//      }
//   } while (iTmp > 0);
//
//   // If not match, return
//   if (iTmp)
//      return 1;
//
//   //iCnt = ParseStringIQ(acRec, '|', 16, apItems);
//   iCnt = ParseStringEx(acRec, '|', '~', 16, apItems);
//   //if (iCnt < ZONE_TYPE)
//   if (iCnt < 6)
//   {
//      if (*pRec == 13 || *pRec == 10)
//      {
//         fclose(fdZone);
//         fdZone = NULL;
//         iRet = 1;      // EOF
//      } else
//      {
//         LogMsg("***** Error: bad input Zoning record for APN=%s (%d <> 6", apItems[0], iCnt);
//         iRet = -1;
//      }
//
//      return iRet;
//   }
//
//   pNextZone = NULL;
//
//   // Zoning
//   if (*apItems[ZONE_CODE] > ' ')
//   {
//      lZoneMatch++;
//
//      iZoneLen = sprintf(acZoneCode, "~%s~", apItems[ZONE_CODE]);
//      iTypeLen = sprintf(acZoneType, "~%s~", apItems[ZONE_TYPE]);
//      if (iZoneLen > iMaxZoning)
//         iMaxZoning = iZoneLen;
//
//      if (bAddZoneType) 
//      {
//         if (iCnt > ZONE_TYPE && *apItems[ZONE_TYPE] > ' ')
//         {
//            vmemcpy(pOutbuf+OFF_ZONECAT, acZoneType, SIZ_ZONECAT, iTypeLen);
//            if (iTypeLen > iMaxZoneType) iMaxZoneType = iTypeLen;
//         }
//      } else
//         memset(pOutbuf+OFF_ZONECAT, ' ', SIZ_ZONECAT);
//
//      vmemcpy(pOutbuf+OFF_ZONE_X1, acZoneCode, SIZ_ZONE_X1);
//      if (iZoneLen > SIZ_ZONE_X1)
//      {
//         pNextZone = &acZoneCode[SIZ_ZONE_X1];
//         iTmp = strlen(pNextZone);
//         vmemcpy(pOutbuf+OFF_ZONE_X2, pNextZone, SIZ_ZONE_X2, iTmp);
//         if (iTmp > SIZ_ZONE_X2)
//         {
//            pNextZone += SIZ_ZONE_X2;
//            iTmp = strlen(pNextZone);
//            vmemcpy(pOutbuf+OFF_ZONE_X3, pNextZone, SIZ_ZONE_X3, iTmp);
//            if (iTmp > SIZ_ZONE_X3)
//            {
//               vmemcpy(pOutbuf+OFF_ZONE_X4, pNextZone+SIZ_ZONE_X3, SIZ_ZONE_X4);
//#ifdef _DEBUG
//               LogMsg0("APN=%s PQZoning=\"%s\"", apItems[ZONE_ASMT], apItems[ZONE_CODE]);
//#endif
//            }
//         }
//      }
//   }
//
//   // Get next record
//   pRec = fgets(acRec, 1024, fdZone);
//
//   return 0;
//}

// iApnRoot is the offset of APN in R01 we use to match with zoning file
int MergeZoneRecX(char *pOutbuf, int iZoneApnLen, int iApnRoot)
{
   static   char acRec[1024], *pRec=NULL;
   static   int  iCnt=0;
   char     *apItems[16], *pNextZone, acTmp[64], *pTmp;
   int      iRet=0, iTmp, iZoneLen;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 1024, fdZone);
      if (pRec && *pRec > '9')
         pRec = fgets(acRec, 1024, fdZone);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006351003000 ", 10))
   //   iTmp = 0;
#endif
   do
   {
      if (!pRec)
      {
         fclose(fdZone);
         fdZone = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf+iApnRoot, pRec, iZoneApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Zoning rec %.*s", iZoneApnLen, pRec);
         pRec = fgets(acRec, 1024, fdZone);
         lZoneSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iCnt = ParseStringIQ(acRec, '|', 16, apItems);
   if (iCnt < ZONE_TYPE)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdZone);
         fdZone = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input roll record for APN=%s", apItems[0]);
         iRet = -1;
      }

      return iRet;
   }

   pNextZone = NULL;

   // Zoning
   if (*apItems[ZONE_CODE] > ' ')
   {
      lZoneMatch++;

      iZoneLen = iTrim(apItems[ZONE_CODE]);
      if (iZoneLen > iMaxZoning)
      {
         iMaxZoning = iZoneLen;
         strcpy(g_sZoning, apItems[ZONE_CODE]);
      }

      if (bAddZoneType) 
      {
         if (iCnt > ZONE_TYPE && *apItems[ZONE_TYPE] > ' ')
         {
            iTmp = iTrim(apItems[ZONE_TYPE]);
            vmemcpy(pOutbuf+OFF_ZONECAT, apItems[ZONE_TYPE], SIZ_ZONECAT, iTmp);
            if (iTmp > iMaxZoneType) iMaxZoneType = iTmp;
         }
      } else
         memset(pOutbuf+OFF_ZONECAT, ' ', SIZ_ZONECAT);

      // Zoning Jurisdiction
      if (*apItems[ZONE_JURI] > ' ')
      {
         iTmp = Juris2Code(apItems[ZONE_JURI], acTmp, pOutbuf);
         if (iTmp > 0)
            vmemcpy(pOutbuf+OFF_ZONE_JURIS, acTmp, SIZ_ZONE_JURIS);
      }

      // Zoning Desc
      if (iCnt > ZONE_DESC && *apItems[ZONE_DESC] > ' ')
      {
         iTmp = strlen(apItems[ZONE_DESC]);
         vmemcpy(pOutbuf+OFF_ZONE_DESC, apItems[ZONE_DESC], SIZ_ZONE_DESC, iTmp-1);
      }

      if (iZoneLen > iMaxZoneLen)
      {
         *(apItems[ZONE_CODE]+iMaxZoneLen) = 0;
         pTmp = strrchr(apItems[ZONE_CODE], '~');
         // Alway terminate ZONE_CODE with '~'
         if (pTmp)
            *(pTmp+1) = 0;
      }

      vmemcpy(pOutbuf+OFF_ZONE_X1, apItems[ZONE_CODE], SIZ_ZONE_X1);
      if (iZoneLen > SIZ_ZONE_X1)
      {
         pNextZone = apItems[ZONE_CODE]+SIZ_ZONE_X1;
         iTmp = strlen(pNextZone);
         vmemcpy(pOutbuf+OFF_ZONE_X2, pNextZone, SIZ_ZONE_X2, iTmp);
         if (iTmp > SIZ_ZONE_X2)
         {
            pNextZone += SIZ_ZONE_X2;
            iTmp = strlen(pNextZone);
            vmemcpy(pOutbuf+OFF_ZONE_X3, pNextZone, SIZ_ZONE_X3, iTmp);
            if (iTmp > SIZ_ZONE_X3)
            {
               vmemcpy(pOutbuf+OFF_ZONE_X4, pNextZone+SIZ_ZONE_X3, SIZ_ZONE_X4);
#ifdef _DEBUG
               LogMsg0("APN=%s PQZoning=\"%s\"", apItems[ZONE_ASMT], apItems[ZONE_CODE]);
#endif
            }
         }
      }
   }

   // Get next record
   pRec = fgets(acRec, 1024, fdZone);

   return 0;
}

/******************************** MergeZoning *******************************
 *
 * Assuming zoning file is sorted on APN
 *
 * Merge zoning data
 *
 ****************************************************************************/

int RenameZoneFile(char *pCnty, char *pOutFile, char *pRawFile, BOOL bRename, char cFileNo) 
{ 
   char acTmp[_MAX_PATH], acTmpFile[_MAX_PATH], acNewFile[_MAX_PATH];
   int   iRet;

   sprintf(acTmp, "Z0%d", cFileNo);
   sprintf(acTmpFile, acRawTmpl, pCnty, pCnty, acTmp); 
   if (!_access(acTmpFile, 0)) 
      remove(acTmpFile); 
   if (bRename) 
   { 
      LogMsg("Rename %s to %s", pRawFile, acTmpFile); 
      iRet = rename(pRawFile, acTmpFile); 
      strcpy(acNewFile, pRawFile);
   } else 
   {
      sprintf(acTmp, "R0%d", cFileNo);
      sprintf(acNewFile, acRawTmpl, pCnty, pCnty, acTmp); 
   }
   LogMsg("Rename %s to %s", pOutFile, acNewFile); 
   iRet = rename(pOutFile, acNewFile); 

   return iRet;
}

int MergeZoning(char *pCnty, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acTmp[_MAX_PATH];
   char     acOutFile[_MAX_PATH], acRawFile[_MAX_PATH];
   char     cFileCnt=1;

   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bR01;
   int      iRet, iApnType, iZoneApnLen, iTmp;
   long     lCnt=0;

   LogMsg0("Merge Zoning");

   // Setup file names
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "TMP");

   if (_access(acRawFile, 0))
   {
      bR01 = false;
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");
   } else
      bR01 = true;

   // Check APN type (1=fixed length, 2=variable, 3=special case (i.e. SFX)), 4=use PREV_APN, 5=use ALT_APN
   iZoneApnLen = GetPrivateProfileInt(pCnty, "ApnLen", iApnLen, acIniFile);
   iApnType = GetPrivateProfileInt(pCnty, "ApnType", 1, acIniFile);

   // ZoneType
   GetIniString(pCnty, "ZoneType", "Y", acBuf, _MAX_PATH, acIniFile);
   if (acBuf[0] == 'Y')
      bAddZoneType = true;
   else
      bAddZoneType = false;

   // Open Zoning file
   iRet = GetIniString(pCnty, "Zoning", "", acBuf, _MAX_PATH, acIniFile);
   if (!iRet)
   {
      iRet = GetIniString("Data", "Zoning", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acBuf, acTmp, pCnty, pCnty);
   }
   if (acBuf[0] > ' ')
   {
      LogMsg("Open Zoning file %s", acBuf);
      fdZone = fopen(acBuf, "r");
      if (fdZone == NULL)
      {
         LogMsg("***** Error opening Zoning file: %s\n", acBuf);
         return -2;
      }
   } else
   {
      LogMsg("*** Missing zoning file.  Merge zoning ignored.  Please check INI file");
      return 0;
   }

   // Open R01 file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   bRet = true;
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Initialize
   lZoneMatch=lZoneSkip=iMaxZoning = 0;
   iRet = 0;

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Error reading
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (!nBytesRead)
      {
         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhOut = 0;
         fhIn = 0;

         // Rename output file
         RenameZoneFile(pCnty, acOutFile, acRawFile, bR01, cFileCnt);

         // Check new file
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (_access(acRawFile, 0))
         {
            if (!_access(acOutFile, 0))
               rename(acOutFile, acRawFile);
            else
               break;
         }

         // Open next Input file
         LogMsg("Open input file %s", acRawFile);
         fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
               FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

         if (fhIn == INVALID_HANDLE_VALUE)
            break;
         bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

         // Open Output file
         LogMsg("Open output file %s", acOutFile);
         fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
               FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
         if (fhOut == INVALID_HANDLE_VALUE)
            break;
      }

      // Remove old data
      removeZoning(acBuf);

      // Merge Zoning
      if (fdZone)
      {
         // Remove old desc
         memset(acBuf+OFF_ZONE_DESC, ' ', SIZ_ZONE_DESC);
         if (!_memicmp(acBuf+OFF_ZONE_X1, "NULL", 4))
            memset(acBuf+OFF_ZONE_X1, ' ', 4);

         if (iApnType == 1)
            iTmp = MergeZoneRec1(acBuf, iZoneApnLen);     // fixed length APN
         else if (iApnType == 2)
            iTmp = MergeZoneRec2(acBuf, iZoneApnLen);     // variable length APN (ALA)
         else if (iApnType == 3)
            iTmp = MergeZoneRec3(acBuf, iZoneApnLen);     // special case (SFX)
         else if (iApnType == 4)
            iTmp = MergeZoneRecX(acBuf, iZoneApnLen, OFF_PREV_APN);  // Use PREV_APN
         else if (iApnType == 5)
            iTmp = MergeZoneRecX(acBuf, iZoneApnLen, OFF_ALT_APN);   // Use ALT_APN
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
#ifdef _DEBUG
      //if (cFileCnt > 2 && lCnt > 1000000)
      //   break;
#endif
   }
   printf("\n");

   // Close files
   if (fdZone)
      fclose(fdZone);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Number of Zone matched:     %u", lZoneMatch);
   LogMsg("Number of Zone skiped:      %u", lZoneSkip);
   LogMsg("      Max zoning len:       %u [%s]", iMaxZoning, g_sZoning);
   LogMsg("      Max zonetype len:     %u\n", iMaxZoneType);

   return iRet;
}

