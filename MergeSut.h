#ifndef  _MERGE_SUT_H
#define  _MERGE_SUT_H

// Aumentum layout
// CA-Sutter-AsmtRoll.csv 04/26/2022
#define  R_PIN                                     0
#define  R_APN                                     0
#define  R_CONVENYANCENUMBER                       1
#define  R_CONVEYANCEMONTH                         2
#define  R_CONVEYANCEYEAR                          3
#define  R_CLASSDESCRIPTION                        4
#define  R_ROLLCAT                                 5
#define  R_BASEYEAR                                6
#define  R_BUSINESSUSECODEDESCRIPTION              7
#define  R_DOINGBUSINESSAS                         8
#define  R_MAILNAME                                9
#define  R_MAILADDRESS                             10
#define  R_MAILCITY                                11
#define  R_MAILSTATE                               12
#define  R_MAILZIPCODE                             13
#define  R_SITUSSTREETNUMBER                       14
#define  R_SITUSSTREETNUMBERSUFFIX                 15
#define  R_SITUSSTREETPREDIRECTIONAL               16
#define  R_SITUSSTREETNAME                         17
#define  R_SITUSSTREETTYPE                         18
#define  R_SITUSUNITNUMBER                         19
#define  R_SITUSZIPCODE                            20
#define  R_SITUSCITYNAME                           21
#define  R_ASSESSMENTDESCRIPTION                   22
#define  R_ISTAXABLE                               23
#define  R_TRA                                     24
#define  R_GEO                                     25
#define  R_RECORDERSTYPE                           26
#define  R_RECORDERSBOOK                           27
#define  R_RECORDERSPAGE                           28
#define  R_LOTTYPE                                 29
#define  R_LOT                                     30
#define  R_BLOCK                                   31
#define  R_PORTIONDIRECTION                        32
#define  R_SUBDIVISIONNAMEORTRACT                  33
#define  R_ACREAGEAMOUNT                           34
#define  R_FIRSTSECTION                            35
#define  R_FIRSTTOWNSHIP                           36
#define  R_FIRSTRANGE                              37
#define  R_FIRSTRANGEDIRECTION                     38
#define  R_LEGALPARTY1                             39
#define  R_LEGALPARTY2                             40
#define  R_LEGALPARTY3                             41
#define  R_LEGALPARTY4                             42
#define  R_LAND                                    43
#define  R_IMPR                                    44
#define  R_LIVINGIMPROVEMENTS                      45
#define  R_PERSONALVALUE                           46
#define  R_TRADEFIXTURESAMOUNT                     47
#define  R_PERSONALPROPERTYAPPRAISED               48
#define  R_TENPERCENTASSESSEDPENALTY               49
#define  R_HOX                                     50
#define  R_VETERANSEXEMPTION                       51
#define  R_DISABLEDVETERANSEXEMPTION               52
#define  R_CHURCHEXEMPTION                         53
#define  R_RELIGIOUSEXEMPTION                      54
#define  R_CEMETERYEXEMPTION                       55
#define  R_PUBLICSCHOOLEXEMPTION                   56
#define  R_PUBLICLIBRARYEXEMPTION                  57
#define  R_PUBLICMUSEUMEXEMPTION                   58
#define  R_WELFARECOLLEGEEXEMPTION                 59
#define  R_WELFAREHOSPITALEXEMPTION                60
#define  R_WELFAREPRIVATEPAROCHIALSCHOOLEXEMPTION  61
#define  R_WELFARECHARITYRELIGIOUSEXEMPTION        62
#define  R_OTHEREXEMPTION                          63
#define  R_CAMEFROM                                64
#define  R_CLASSCODE                               65
#define  R_COLS                                    66

// CA-Sutter-Sales.csv
#define  S_PIN                                     0
#define  S_GEO                                     1
#define  S_CONVEYANCEMONTH                         2
#define  S_CONVEYANCEYEAR                          3
#define  S_CONVEYANCENUMBER                        4
#define  S_CONVEYANCEDATE                          5
#define  S_FORMSENTDATE                            6
#define  S_SALELETTERDATE                          7
#define  S_TRANSFERTYPECODE                        8
#define  S_INDICATEDSALEPRICE                      9
#define  S_ADJUSTEDSALEPRICE                       10
#define  S_CLASSDESCRIPTION                        11
#define  S_BASEYEAR                                12
#define  S_BASEYEARFLAG                            13
#define  S_SITUS_LINE1                             14
#define  S_SITUS_LINE2                             15
#define  S_TOTALACRES                              16
#define  S_DESIGNTYPE101                           17
#define  S_CONSTRUCTIONTYPE                        18
#define  S_AREA_1_1                                19
#define  S_COSTAREA                                20
#define  S_TOTALAREA                               21
#define  S_EFFECTIVEAGE                            22
#define  S_CARPORT                                 23
#define  S_ATTGARAGE                               24
#define  S_DETGARAGE                               25
#define  S_POOLIND                                 26
#define  S_BEDROOMS                                27
#define  S_HALFBATHROOM                            28
#define  S_THREEQTRBATHROOM                        29
#define  S_FULLBATHROOM                            30
#define  S_TOTALBATHROOMS                          31
#define  S_SPECADJUSTMENT                          32
#define  S_LANDVALUE                               33
#define  S_STRUCTUREVALUE                          34
#define  S_TREEVINEVALUE                           35
#define  S_TRANSFERFEE                             36
#define  S_COLS                                    37

// CA-Sutter-PropChar.csv
#define  C_APN                                     0
#define  C_CLASSCODEDESCRIPTION                    1
#define  C_SITUSSTREETNUMBER                       2
#define  C_SITUSSTREETNUMBERSUFFIX                 3
#define  C_SITUSSTREETPREDIRECTIONAL               4
#define  C_SITUSSTREETNAME                         5
#define  C_SITUSSTREETTYPE                         6
#define  C_SITUSUNITNUMBER                         7
#define  C_SITUSZIPCODE                            8
#define  C_SITUSCITYNAME                           9
#define  C_STREETSURFACED                          10
#define  C_SEWER                                   11
#define  C_DOMESTICWATER                           12
#define  C_IRRIGATIONWATER                         13
#define  C_WELLWATER                               14
#define  C_UTILITYELECTRIC                         15
#define  C_UTILITYGAS                              16
#define  C_EFFYEARBUILT                            17
#define  C_YEARBUILT                               18
#define  C_NUMBEROFSTORIES                         19
#define  C_ROOFTYPE                                20
#define  C_GARAGETYPE                              21
#define  C_GARAGEAREA                              22
#define  C_CARPORTSIZE                             23
#define  C_GARAGE2TYPE                             24
#define  C_GARAGE2AREA                             25
#define  C_CARPORT2SIZE                            26
#define  C_HASFIREPLACE                            27
#define  C_BATHSFULL                               28
#define  C_BATHS3_4                                29
#define  C_BATHSHALF                               30
#define  C_TOTALBATHS                              31
#define  C_HASCENTRALHEATING                       32
#define  C_HASCENTRALCOOLING                       33
#define  C_DESIGNTYPE                              34
#define  C_CONSTRUCTIONTYPE                        35
#define  C_QUALITYCODE                             36
#define  C_SHAPECODE                               37
#define  C_LIVINGAREA                              38
#define  C_ACTUALAREA                              39
#define  C_HASPOOL                                 40
#define  C_BEDROOMCOUNT                            41
#define  C_FAIRWAY                                 42
#define  C_WATERFRONT                              43
#define  C_ACREAGE                                 44 
#define  C_LANDUNITTYPE                            45
#define  C_COLS                                    46

static XLAT_CODE  asRoofType[] =
{
   // Value, lookup code, value length
   "GABLE",                "G", 3,      
   "HIPVALLEY",            "O", 4,      
   "HIP",                  "H", 2,      
   "SHED",                 "K", 2,      
   "OTHER",                "J", 2,      
   "FLAT",                 "F", 2,      
   "GAMBREL",              "N", 3,      
   "AFRAME",               "A", 2,      
   "DORMER",               "K", 2,      
   "MANSARD",              "M", 2,      
   "PEAKED",               "J", 2,      // Other
   "",   "",  0
};

static XLAT_CODE  asConstType[] =
{
   // Value, lookup code, value length
   "MASONRY BEARING WALLS", "C", 2,
   "POLE FRAME",            "M", 2,    // Special
   "PRE-ENGINEERED STEEL",  "A", 2,    // Steel frame
   "WOOD OR LIGHT STEEL",   "Z", 2,
   "",   "",  0
};

// CA-Mendo-CurrentAmounts.csv
#define  CA_PIN                                    0
#define  CA_BILLNUMBER                             1
#define  CA_TAXBILLID                              2
#define  CA_INST1DUEDATE                           3
#define  CA_INST2DUEDATE                           4
#define  CA_INST1TAXCHARGE                         5
#define  CA_INST1PENALTYCHARGE                     6
#define  CA_INST1FEECHARGE                         7
#define  CA_INST1INTERESTCHARGE                    8
#define  CA_INST1TAXPAYMENT                        9         // Paid value as negative
#define  CA_INST1PENALTYPAYMENT                    10
#define  CA_INST1FEEPAYMENT                        11
#define  CA_INST1INTERESTPAYMENT                   12
#define  CA_INST1AMOUNTDUE                         13
#define  CA_INST2TAXCHARGE                         14
#define  CA_INST2PENALTYCHARGE                     15
#define  CA_INST2FEECHARGE                         16
#define  CA_INST2INTERESTCHARGE                    17
#define  CA_INST2TAXPAYMENT                        18
#define  CA_INST2PENALTYPAYMENT                    19
#define  CA_INST2FEEPAYMENT                        20
#define  CA_INST2INTERESTPAYMENT                   21
#define  CA_INST2AMOUNTDUE                         22
#define  CA_COLS                                   23

// CA-Mendo-CurrentDistricts.csv
#define  CD_PIN                                    0
#define  CD_TAXRATEAREA                            1
#define  CD_TAXYEAR                                2
#define  CD_BILLNUMBER                             3
#define  CD_BILLTYPE                               4
#define  CD_TAXBILLID                              5
#define  CD_DETAILS                                6
#define  CD_TAXCODE                                7
#define  CD_RATE                                   8
#define  CD_TOTAL                                  9
#define  CD_COLS                                   10

// CA-Mendo-PriorYear.csv
#define  PY_PIN                                    0
#define  PY_DEFAULTNUMBER                          1
#define  PY_DEFAULTYEAR                            2
#define  PY_BILLTYPE                               3
#define  PY_AMOUNTDUE                              4
#define  PY_COLS                                   5

// 2021 Roll corretion
#define  SUT_COR_AIN                   0
#define  SUT_COR_PIN                   1
#define  SUT_COR_TAXYEAR               2
#define  SUT_COR_NEXTYEAR              3
#define  SUT_COR_ASMTTYPE              4
#define  SUT_COR_ROLLTYPE              5
#define  SUT_COR_TAG                   6
#define  SUT_COR_CLASSCD               7
#define  SUT_COR_COUNTYNAME            8
#define  SUT_COR_PRIMARYOWNER          9
#define  SUT_COR_RECIPIENT             10
#define  SUT_COR_DELIVERYADDR          11
#define  SUT_COR_LASTLINE              12
#define  SUT_COR_SITUSADDR             13
#define  SUT_COR_SITUSCITY             14
#define  SUT_COR_SITUSPOSTALCD         15
#define  SUT_COR_SITUSCOMPLETE         16
#define  SUT_COR_TOTALMARKET           17
#define  SUT_COR_TOTALFBYV             18    // Factored Base Year Value
#define  SUT_COR_HOX                   19
#define  SUT_COR_DVX                   20
#define  SUT_COR_LDVX                  21
#define  SUT_COR_OTHEREXMPT            22
#define  SUT_COR_ASSDLAND              23
#define  SUT_COR_ASSDIMP               24
#define  SUT_COR_ASSDPERSONAL          25
#define  SUT_COR_ASSDFIXTURES          26
#define  SUT_COR_ASSDLIVIMP            27
#define  SUT_COR_ASSESSEDFULL          28
#define  SUT_COR_TOTALEXMPT            29
#define  SUT_COR_NETTAXABLE            30
#define  SUT_COR_ASMTTRANID            31
#define  SUT_COR_ID                    32
#define  SUT_COR_REVOBJID              33
#define  SUT_COR_PRINTDATE             34
#define  SUT_COR_AMAILWITHCOUNTYOF     35
#define  SUT_COR_AMAILWITHCOUNTY       36
#define  SUT_COR_AMAILCOMPLETE         37
#define  SUT_COR_ATTENTIONLINE         38
#define  SUT_COR_MAILCOMPLETE          39
#define  SUT_COR_MAILCOMPWITHATTN      40
#define  SUT_COR_FLDS                  41

// Current secured tax file
#define  SUT_SEC_APN                0
#define  SUT_SEC_TRA                1
#define  SUT_SEC_NAME1              2
#define  SUT_SEC_BILL_NUM           3
#define  SUT_SEC_NAME2              4
#define  SUT_SEC_CAREOF             5
#define  SUT_SEC_EXCEPTION          6
#define  SUT_SEC_VESTING            7
#define  SUT_SEC_DBA                8
#define  SUT_SEC_MAR_ONE            9
#define  SUT_SEC_M_ADDR             10
#define  SUT_SEC_M_CITYST           11
#define  SUT_SEC_M_ZIP              12
#define  SUT_SEC_LANDUSE            13
#define  SUT_SEC_S_ADDR             14
#define  SUT_SEC_CORTAC             15
#define  SUT_SEC_LAND               16
#define  SUT_SEC_IMPR               17
#define  SUT_SEC_PERSPROP           18
#define  SUT_SEC_FIXTR              19
#define  SUT_SEC_OTHER              20
#define  SUT_SEC_HOME_EXE           21
#define  SUT_SEC_MISC_EXE           22
#define  SUT_SEC_ECODE              23    // Exe_Code
#define  SUT_SEC_EVET               24
#define  SUT_SEC_EMISC              25
#define  SUT_SEC_ACRES              26
#define  SUT_SEC_STS_DATE           27    // Default date offset 474
#define  SUT_SEC_SEG_FLAG           28
#define  SUT_SEC_GROSS_TAX          29
#define  SUT_SEC_HO_REF             30
#define  SUT_SEC_MISC_REF           31
#define  SUT_SEC_NET_TAX            32    // Decimal value
#define  SUT_SEC_TOT_BOND           33
#define  SUT_SEC_TOT_DIR            34
#define  SUT_SEC_TOT_PEN            35
#define  SUT_SEC_TOT_RATE           36
#define  SUT_SEC_AS_CODE_1          37
#define  SUT_SEC_AMT1_1             38
#define  SUT_SEC_AMT2_1             39
#define  SUT_SEC_AS_CODE_2          40
#define  SUT_SEC_AMT1_2             41
#define  SUT_SEC_AMT2_2             42
#define  SUT_SEC_AS_CODE_3          43
#define  SUT_SEC_AMT1_3             44
#define  SUT_SEC_AMT2_3             45
#define  SUT_SEC_AS_CODE_4          46
#define  SUT_SEC_AMT1_4             47
#define  SUT_SEC_AMT2_4             48
#define  SUT_SEC_AS_CODE_5          49
#define  SUT_SEC_AMT1_5             50
#define  SUT_SEC_AMT2_5             51
#define  SUT_SEC_AS_CODE_6          52
#define  SUT_SEC_AMT1_6             53
#define  SUT_SEC_AMT2_6             54
#define  SUT_SEC_AS_CODE_7          55
#define  SUT_SEC_AMT1_7             56
#define  SUT_SEC_AMT2_7             57
#define  SUT_SEC_AS_CODE_8          58
#define  SUT_SEC_AMT1_8             59
#define  SUT_SEC_AMT2_8             60
#define  SUT_SEC_AS_CODE_9          61
#define  SUT_SEC_AMT1_9             62
#define  SUT_SEC_AMT2_9             63
#define  SUT_SEC_AS_CODE_10         64
#define  SUT_SEC_AMT1_10            65
#define  SUT_SEC_AMT2_10            66
#define  SUT_SEC_AS_CODE_11         67
#define  SUT_SEC_AMT1_11            68
#define  SUT_SEC_AMT2_11            69
#define  SUT_SEC_AS_CODE_12         70
#define  SUT_SEC_AMT1_12            71
#define  SUT_SEC_AMT2_12            72
#define  SUT_SEC_AS_CODE_13         73
#define  SUT_SEC_AMT1_13            74
#define  SUT_SEC_AMT2_13            75
#define  SUT_SEC_AS_CODE_14         76
#define  SUT_SEC_AMT1_14            77
#define  SUT_SEC_AMT2_14            78
#define  SUT_SEC_AS_CODE_15         79
#define  SUT_SEC_AMT1_15            80
#define  SUT_SEC_AMT2_15            81

#define  SUT_SEC_I1_STAT            82    // P(aid), blank=unpaid, N(one) tax, C(orrection/cancel) offset 964
#define  SUT_SEC_I1_TAX             83    // V99
#define  SUT_SEC_I1_PEN             84    // V99
#define  SUT_SEC_I1_DUEDATE         85
#define  SUT_SEC_I1_PAIDDATE        86
#define  SUT_SEC_I1_BATCH           87

#define  SUT_SEC_I2_STAT            88
#define  SUT_SEC_I2_TAX             89
#define  SUT_SEC_I2_PEN             90
#define  SUT_SEC_I2_COST            91
#define  SUT_SEC_I2_DUEDATE         92
#define  SUT_SEC_I2_PAIDDATE        93
#define  SUT_SEC_I2_BATCH           94

#define  SUT_SEC_PAYOR              95
#define  SUT_SEC_CORR_TYPE          96
#define  SUT_SEC_BILL_ADJ           97
#define  SUT_SEC_ORIG_PRCL          98
#define  SUT_SEC_DMND_STMNT         99
#define  SUT_SEC_FD_02              100
#define  SUT_SEC_DRCT_ASSMT_RMVD    101
#define  SUT_SEC_LANDSCAPE          102
#define  SUT_SEC_ST_LTG_SUBDIV      103
#define  SUT_SEC_LAST_ACT_DATE      104
#define  SUT_SEC_PEN_CODE           105
#define  SUT_SEC_PEND_APPR          106
#define  SUT_SEC_BRUP               107

// Delinquent file
#define  SUT_DELQ_NAME              0
#define  SUT_DELQ_PRCL              1
#define  SUT_DELQ_NAME2             2
#define  SUT_DELQ_CAREOF            3
#define  SUT_DELQ_ADDR              4
#define  SUT_DELQ_CTY_ST            5
#define  SUT_DELQ_ZIP               6
#define  SUT_DELQ_ORIG_OWNR         7
#define  SUT_DELQ_ORIG_PRCL         8
#define  SUT_DELQ_A0_ASSMT_NBR      9
#define  SUT_DELQ_A0_YR             10
#define  SUT_DELQ_A0_PRCL           11    // Orig. parcel
#define  SUT_DELQ_A0_TYPE           12    // Secu(R)ed, (S)uplemental
#define  SUT_DELQ_A0_PAY_FLAG       13    // N ???
#define  SUT_DELQ_A0_AREA           14    // TRA
#define  SUT_DELQ_A0_TAX            15
#define  SUT_DELQ_A0_PEN            16
#define  SUT_DELQ_A0_COST           17
#define  SUT_DELQ_A0_AUDIT_AMT      18
#define  SUT_DELQ_A1_ASSMT_NBR      19
#define  SUT_DELQ_A1_YR             20
#define  SUT_DELQ_A1_PRCL           21
#define  SUT_DELQ_A1_TYPE           22
#define  SUT_DELQ_A1_PAY_FLAG       23
#define  SUT_DELQ_A1_AREA           24
#define  SUT_DELQ_A1_TAX            25
#define  SUT_DELQ_A1_PEN            26
#define  SUT_DELQ_A1_COST           27
#define  SUT_DELQ_A1_AUDIT_AMT      28
#define  SUT_DELQ_A2_ASSMT_NBR      29
#define  SUT_DELQ_A2_YR             30
#define  SUT_DELQ_A2_PRCL           31
#define  SUT_DELQ_A2_TYPE           32
#define  SUT_DELQ_A2_PAY_FLAG       33
#define  SUT_DELQ_A2_AREA           34
#define  SUT_DELQ_A2_TAX            35
#define  SUT_DELQ_A2_PEN            36
#define  SUT_DELQ_A2_COST           37
#define  SUT_DELQ_A2_AUDIT_AMT      38
#define  SUT_DELQ_A3_ASSMT_NBR      39
#define  SUT_DELQ_A3_YR             40
#define  SUT_DELQ_A3_PRCL           41
#define  SUT_DELQ_A3_TYPE           42
#define  SUT_DELQ_A3_PAY_FLAG       43
#define  SUT_DELQ_A3_AREA           44
#define  SUT_DELQ_A3_TAX            45
#define  SUT_DELQ_A3_PEN            46
#define  SUT_DELQ_A3_COST           47
#define  SUT_DELQ_A3_AUDIT_AMT      48
#define  SUT_DELQ_A4_ASSMT_NBR      49
#define  SUT_DELQ_A4_YR             50
#define  SUT_DELQ_A4_PRCL           51
#define  SUT_DELQ_A4_TYPE           52
#define  SUT_DELQ_A4_PAY_FLAG       53
#define  SUT_DELQ_A4_AREA           54
#define  SUT_DELQ_A4_TAX            55
#define  SUT_DELQ_A4_PEN            56
#define  SUT_DELQ_A4_COST           57
#define  SUT_DELQ_A4_AUDIT_AMT      58
#define  SUT_DELQ_A5_ASSMT_NBR      59
#define  SUT_DELQ_A5_YR             60
#define  SUT_DELQ_A5_PRCL           61
#define  SUT_DELQ_A5_TYPE           62
#define  SUT_DELQ_A5_PAY_FLAG       63
#define  SUT_DELQ_A5_AREA           64
#define  SUT_DELQ_A5_TAX            65
#define  SUT_DELQ_A5_PEN            66
#define  SUT_DELQ_A5_COST           67
#define  SUT_DELQ_A5_AUDIT_AMT      68
#define  SUT_DELQ_A6_ASSMT_NBR      69
#define  SUT_DELQ_A6_YR             70
#define  SUT_DELQ_A6_PRCL           71
#define  SUT_DELQ_A6_TYPE           72
#define  SUT_DELQ_A6_PAY_FLAG       73
#define  SUT_DELQ_A6_AREA           74
#define  SUT_DELQ_A6_TAX            75
#define  SUT_DELQ_A6_PEN            76
#define  SUT_DELQ_A6_COST           77
#define  SUT_DELQ_A6_AUDIT_AMT      78
#define  SUT_DELQ_A7_ASSMT_NBR      79
#define  SUT_DELQ_A7_YR             80
#define  SUT_DELQ_A7_PRCL           81
#define  SUT_DELQ_A7_TYPE           82
#define  SUT_DELQ_A7_PAY_FLAG       83
#define  SUT_DELQ_A7_AREA           84
#define  SUT_DELQ_A7_TAX            85
#define  SUT_DELQ_A7_PEN            86
#define  SUT_DELQ_A7_COST           87
#define  SUT_DELQ_A7_AUDIT_AMT      88
#define  SUT_DELQ_A8_ASSMT_NBR      89
#define  SUT_DELQ_A8_YR             90
#define  SUT_DELQ_A8_PRCL           91
#define  SUT_DELQ_A8_TYPE           92
#define  SUT_DELQ_A8_PAY_FLAG       93
#define  SUT_DELQ_A8_AREA           94
#define  SUT_DELQ_A8_TAX            95
#define  SUT_DELQ_A8_PEN            96
#define  SUT_DELQ_A8_COST           97
#define  SUT_DELQ_A8_AUDIT_AMT      98
#define  SUT_DELQ_A9_ASSMT_NBR      99
#define  SUT_DELQ_A9_YR             100
#define  SUT_DELQ_A9_PRCL           101
#define  SUT_DELQ_A9_TYPE           102
#define  SUT_DELQ_A9_PAY_FLAG       103
#define  SUT_DELQ_A9_AREA           104
#define  SUT_DELQ_A9_TAX            105
#define  SUT_DELQ_A9_PEN            106
#define  SUT_DELQ_A9_COST           107
#define  SUT_DELQ_A9_AUDIT_AMT      108
#define  SUT_DELQ_STATE_FEE         109
#define  SUT_DELQ_TAX_DEF_AMT       110
#define  SUT_DELQ_TAX_DEF_DATE      111
#define  SUT_DELQ_TAX_DEF_NR        112
#define  SUT_DELQ_PLAN_DATE         113
#define  SUT_DELQ_ACT_DATE          114
#define  SUT_DELQ_RDM_DATE          115
#define  SUT_DELQ_RDM_AMT           116
#define  SUT_DELQ_TOT_TAX           117
#define  SUT_DELQ_TOT_PEN           118
#define  SUT_DELQ_TOT_COST          119
#define  SUT_DELQ_TDL_FEE           120
#define  SUT_DELQ_REDEEM_FEE        121
#define  SUT_DELQ_LAST_ACT          122      // P=Paid, R=Redeem (bad check)
#define  SUT_DELQ_ACT_BATCH         123
#define  SUT_DELQ_ORIG_PLAN_DATE    124
#define  SUT_DELQ_BRUP              125
#define  SUT_DELQ_FD_02             126
#define  SUT_DELQ_DRCT_ASSMT_RMVD   127      // No data
#define  SUT_DELQ_ADJ_CODE          128      // No data
#define  SUT_DELQ_ADJ_AMT           129
#define  SUT_DELQ_MISC_FLAG         130      // A,V,S=Skip all penalties
#define  SUT_DELQ_RDM_SW            131
#define  SUT_DELQ_RDM_STAT          132      // P=paid
#define  SUT_DELQ_INST_INT_X        133
#define  SUT_DELQ_REM               134

#define  OFF_DELQ_NAME              1
#define  OFF_DELQ_PRCL              32-1
#define  OFF_DELQ_NAME2             47
#define  OFF_DELQ_CAREOF            78
#define  OFF_DELQ_ADDR              104
#define  OFF_DELQ_CTY_ST            135
#define  OFF_DELQ_ZIP               161
#define  OFF_DELQ_ORIG_OWNR         172
#define  OFF_DELQ_ORIG_PRCL         233
#define  OFF_DELQ_A0_ASSMT_NBR      244
#define  OFF_DELQ_A0_YR             251
#define  OFF_DELQ_A0_PRCL           256    
#define  OFF_DELQ_A0_TYPE           267
#define  OFF_DELQ_A0_PAY_FLAG       269
#define  OFF_DELQ_A0_AREA           271
#define  OFF_DELQ_A0_TAX            278
#define  OFF_DELQ_A0_PEN            288
#define  OFF_DELQ_A0_COST           297
#define  OFF_DELQ_A0_AUDIT_AMT      303
#define  OFF_DELQ_A1_ASSMT_NBR      313
#define  OFF_DELQ_A2_ASSMT_NBR      382
#define  OFF_DELQ_A3_ASSMT_NBR      451
#define  OFF_DELQ_A4_ASSMT_NBR      520
#define  OFF_DELQ_A5_ASSMT_NBR      589
#define  OFF_DELQ_A6_ASSMT_NBR      658
#define  OFF_DELQ_A7_ASSMT_NBR      727
#define  OFF_DELQ_A8_ASSMT_NBR      796
#define  OFF_DELQ_A9_ASSMT_NBR      865
#define  OFF_DELQ_STATE_FEE         934
#define  OFF_DELQ_TAX_DEF_AMT       940
#define  OFF_DELQ_TAX_DEF_DATE      950
#define  OFF_DELQ_TAX_DEF_NR        959
#define  OFF_DELQ_PLAN_DATE         964
#define  OFF_DELQ_ACT_DATE          973
#define  OFF_DELQ_RDM_DATE          982
#define  OFF_DELQ_RDM_AMT           991
#define  OFF_DELQ_TOT_TAX           1001
#define  OFF_DELQ_TOT_PEN           1012
#define  OFF_DELQ_TOT_COST          1021
#define  OFF_DELQ_TDL_FEE           1030
#define  OFF_DELQ_REDEEM_FEE        1033
#define  OFF_DELQ_LAST_ACT          1036
#define  OFF_DELQ_ACT_BATCH         1038
#define  OFF_DELQ_ORIG_PLAN_DATE    1045
#define  OFF_DELQ_BRUP              1054
#define  OFF_DELQ_FD_02             1070
#define  OFF_DELQ_DRCT_ASSMT_RMVD   1091
#define  OFF_DELQ_ADJ_CODE          1093
#define  OFF_DELQ_ADJ_AMT           1095
#define  OFF_DELQ_MISC_FLAG         1106
#define  OFF_DELQ_RDM_SW            1108
#define  OFF_DELQ_RDM_STAT          1110
#define  OFF_DELQ_INST_INT_X        1112
#define  OFF_DELQ_REM               1122

// Secured Tax Master layout - "SUTTER COUNTY FY 2023-24 SECURED TAX MASTER (TAX100P).txt"
#define OFF_TM_TAXYEAR              1
#define OFF_TM_APN                  5
#define OFF_TM_BILLNUM              22
#define OFF_TM_COLLTYPE             38
#define OFF_TM_ASMTTYPE             55    // ANNUAL,SUPPLEMENTAL,ESCAPE
#define OFF_TM_INST1_AMT            70    // [-]11.2
#define OFF_TM_INST1_DUEDATE        82
#define OFF_TM_INST2_AMT            90    // [-]11.2
#define OFF_TM_INST2_DUEDATE        102
#define OFF_TM_TOTALDUE             110   // [-]11.2
#define OFF_TM_TOTALPAID            122   // [-]11.2
#define OFF_TM_LAST_PAIDDATE        134
#define OFF_TM_LENDER_CODE          142
#define OFF_TM_LOANNUM              146
#define OFF_TM_OWNER                178
#define OFF_TM_M_ATTN               238
#define OFF_TM_M_ADDR1              278
#define OFF_TM_M_ADDR2              318
#define OFF_TM_M_CITY               358
#define OFF_TM_M_STATE              388
#define OFF_TM_M_ZIP                390
#define OFF_TM_COUNTRY              400
#define OFF_TM_S_ADDR               430
#define OFF_TM_S_STRNUM             465
#define OFF_TM_S_PREDIR             470
#define OFF_TM_S_STRNAME            472
#define OFF_TM_S_STRSFX             507
#define OFF_TM_S_UNITNO             513
#define OFF_TM_S_POSTDIR            518
#define OFF_TM_S_CITY               520
#define OFF_TM_S_STATE              550
#define OFF_TM_S_ZIP                552
#define OFF_TM_ACRES                562   // 9.4
#define OFF_TM_TRA                  572
#define OFF_TM_USECODE              577
#define OFF_TM_USEDESC              585
#define OFF_TM_NET_TAXABLE          640   // [-]9.0
#define OFF_TM_LAND                 650   // [-]9.0
#define OFF_TM_IMPR                 660   // [-]9.0
#define OFF_TM_TREE_VINES           670   // [-]9.0
#define OFF_TM_FIXTURES             680   // [-]9.0
#define OFF_TM_PERSPROP             690   // [-]9.0
#define OFF_TM_BPS_PENALTY          700   // [-]9.0
#define OFF_TM_HO_EXE               710   // [-]9.0
#define OFF_TM_VET_EXE              720   // [-]9.0
#define OFF_TM_MISC_EXE             730   // [-]9.0
#define OFF_TM_EXE_TYPE             740   // REL,VET,WCR,WELLHAND,WLILP,OTHER,LOWVALUE
#define OFF_TM_BASEYR_VAL           750   // [-]9.0 Factored base year value
#define OFF_TM_TAX_RATE             760   // [-]8.6
#define OFF_TM_TOTALTAX             769   // [-]11.2
#define OFF_TM_PENALTY              781   // [-]11.2
#define OFF_TM_INTEREST             793   // [-]11.2
#define OFF_TM_FEES                 805   // [-]11.2
#define OFF_TM_ISDELQ               817   // Y/N
#define OFF_TM_RED_DUE              818   // [-]11.2
#define OFF_TM_BILL_NAME            830
#define OFF_TM_BILL_ATTN            890
#define OFF_TM_BILL_ADDR1           930
#define OFF_TM_BILL_ADDR2           970
#define OFF_TM_BILL_LASTLINE        1010

#define SIZ_TM_TAXYEAR               4
#define SIZ_TM_APN                  17
#define SIZ_TM_BILLNUM              16
#define SIZ_TM_COLLTYPE             17
#define SIZ_TM_ASMTTYPE             15
#define SIZ_TM_INST1_AMT            12
#define SIZ_TM_INST1_DUEDATE         8
#define SIZ_TM_INST2_AMT            12
#define SIZ_TM_INST2_DUEDATE         8
#define SIZ_TM_TOTALDUE             12
#define SIZ_TM_TOTALPAID            12
#define SIZ_TM_LAST_PAIDDATE         8
#define SIZ_TM_LENDER_CODE           4
#define SIZ_TM_LOANNUM              32
#define SIZ_TM_OWNER                60
#define SIZ_TM_M_ATTN               40
#define SIZ_TM_M_ADDR1              40
#define SIZ_TM_M_ADDR2              40
#define SIZ_TM_M_CITY               30
#define SIZ_TM_M_STATE               2
#define SIZ_TM_M_ZIP                10
#define SIZ_TM_COUNTRY              30
#define SIZ_TM_S_ADDR               35
#define SIZ_TM_S_STRNUM              5
#define SIZ_TM_S_PREDIR              2
#define SIZ_TM_S_STRNAME            35
#define SIZ_TM_S_STRSFX              6
#define SIZ_TM_S_UNITNO              5
#define SIZ_TM_S_POSTDIR             2
#define SIZ_TM_S_CITY               30
#define SIZ_TM_S_STATE               2
#define SIZ_TM_S_ZIP                10
#define SIZ_TM_ACRES                10
#define SIZ_TM_TRA                   5
#define SIZ_TM_USECODE               8
#define SIZ_TM_USEDESC              55
#define SIZ_TM_NET_TAXABLE          10
#define SIZ_TM_LAND                 10
#define SIZ_TM_IMPR                 10
#define SIZ_TM_TREE_VINES           10
#define SIZ_TM_FIXTURES             10
#define SIZ_TM_PERSPROP             10
#define SIZ_TM_BPS_PENALTY          10
#define SIZ_TM_HO_EXE               10
#define SIZ_TM_VET_EXE              10
#define SIZ_TM_MISC_EXE             10
#define SIZ_TM_EXE_TYPE             10
#define SIZ_TM_BASEYR_VAL           10
#define SIZ_TM_TAX_RATE              9
#define SIZ_TM_TOTALTAX             12
#define SIZ_TM_PENALTY              12
#define SIZ_TM_INTEREST             12
#define SIZ_TM_FEES                 12
#define SIZ_TM_ISDELQ                1
#define SIZ_TM_RED_DUE              12
#define SIZ_TM_BILL_NAME            60
#define SIZ_TM_BILL_ATTN            40
#define SIZ_TM_BILL_ADDR1           40
#define SIZ_TM_BILL_ADDR2           40
#define SIZ_TM_BILL_LASTLINE        40

typedef struct _SutTaxMaster
{
   char  TaxYear       [SIZ_TM_TAXYEAR      ];   // 1
   char  Apn           [SIZ_TM_APN          ];   // 5
   char  BillNum       [SIZ_TM_BILLNUM      ];   // 22
   char  CollType      [SIZ_TM_COLLTYPE     ];   // 38
   char  AsmtType      [SIZ_TM_ASMTTYPE     ];   // 55
   char  Inst1_Amt     [SIZ_TM_INST1_AMT    ];   // 70
   char  DueDate1      [SIZ_TM_INST1_DUEDATE];   // 82
   char  Inst2_Amt     [SIZ_TM_INST2_AMT    ];   // 90
   char  DueDate2      [SIZ_TM_INST2_DUEDATE];   // 102
   char  TotalDue      [SIZ_TM_TOTALDUE     ];   // 110
   char  TotalPaid     [SIZ_TM_TOTALPAID    ];   // 122
   char  LastPaidDate  [SIZ_TM_LAST_PAIDDATE];   // 134
   char  LenderCode    [SIZ_TM_LENDER_CODE  ];   // 142
   char  LoanNum       [SIZ_TM_LOANNUM      ];   // 146
   char  Owner         [SIZ_TM_OWNER        ];   // 178
   char  M_Attn        [SIZ_TM_M_ATTN       ];   // 238
   char  M_Addr1       [SIZ_TM_M_ADDR1      ];   // 278
   char  M_Addr2       [SIZ_TM_M_ADDR2      ];   // 318
   char  M_City        [SIZ_TM_M_CITY       ];   // 358
   char  M_State       [SIZ_TM_M_STATE      ];   // 388
   char  M_Zip         [SIZ_TM_M_ZIP        ];   // 390
   char  M_Country     [SIZ_TM_COUNTRY      ];   // 400
   char  S_Addr        [SIZ_TM_S_ADDR       ];   // 430
   char  S_StrNum      [SIZ_TM_S_STRNUM     ];   // 465
   char  S_PreDir      [SIZ_TM_S_PREDIR     ];   // 470
   char  S_StrName     [SIZ_TM_S_STRNAME    ];   // 472
   char  S_StrSfx      [SIZ_TM_S_STRSFX     ];   // 507
   char  S_UnitNo      [SIZ_TM_S_UNITNO     ];   // 513
   char  S_PostDir     [SIZ_TM_S_POSTDIR    ];   // 518
   char  S_City        [SIZ_TM_S_CITY       ];   // 520
   char  S_State       [SIZ_TM_S_STATE      ];   // 550
   char  S_Zip         [SIZ_TM_S_ZIP        ];   // 552
   char  Acres         [SIZ_TM_ACRES        ];   // 562
   char  TRA           [SIZ_TM_TRA          ];   // 572
   char  UseCode       [SIZ_TM_USECODE      ];   // 577
   char  UseDesc       [SIZ_TM_USEDESC      ];   // 585
   char  Net_Taxable   [SIZ_TM_NET_TAXABLE  ];   // 640
   char  Land          [SIZ_TM_LAND         ];   // 650
   char  Impr          [SIZ_TM_IMPR         ];   // 660
   char  Tree_Vines    [SIZ_TM_TREE_VINES   ];   // 670
   char  Fixtures      [SIZ_TM_FIXTURES     ];   // 680
   char  PersProp      [SIZ_TM_PERSPROP     ];   // 690
   char  BPS_Penalty   [SIZ_TM_BPS_PENALTY  ];   // 700
   char  HO_Exe        [SIZ_TM_HO_EXE       ];   // 710
   char  Vet_Exe       [SIZ_TM_VET_EXE      ];   // 720
   char  Misc_Exe      [SIZ_TM_MISC_EXE     ];   // 730
   char  Exe_Type      [SIZ_TM_EXE_TYPE     ];   // 740
   char  FBY_Value     [SIZ_TM_BASEYR_VAL   ];   // 750
   char  TaxRate       [SIZ_TM_TAX_RATE     ];   // 760
   char  TotalTax      [SIZ_TM_TOTALTAX     ];   // 769
   char  Penalty       [SIZ_TM_PENALTY      ];   // 781
   char  Interest      [SIZ_TM_INTEREST     ];   // 793
   char  TotalFees     [SIZ_TM_FEES         ];   // 805
   char  IsDelq        [SIZ_TM_ISDELQ       ];   // 817
   char  Red_Amt       [SIZ_TM_RED_DUE      ];   // 818
   char  Bill_Name     [SIZ_TM_BILL_NAME    ];   // 830
   char  Bill_Attn     [SIZ_TM_BILL_ATTN    ];   // 890
   char  Bill_Addr1    [SIZ_TM_BILL_ADDR1   ];   // 930
   char  Bill_Addr2    [SIZ_TM_BILL_ADDR2   ];   // 970
   char  Bill_Lastline [SIZ_TM_BILL_LASTLINE];   // 1010
   char  CrLf[2];
} SUT_MASTER;

// Tax Assessment Detail - "SUTTER COUNTY FY 2023-24 SECURED TAX ASSESSMENT DETAIL (TAX110P).txt"
#define OFF_TAD_TAXYEAR             1-1
#define OFF_TAD_APN                 5-1
#define OFF_TAD_BILLNUM             22-1
#define OFF_TAD_COLLTYPE            38-1
#define OFF_TAD_ASMTTYPE            55-1      // ANNUAL,SUPPLEMENTAL,ESCAPE
#define OFF_TAD_TRA                 70-1
#define OFF_TAD_RECTYPE             75-1      //  1=CW,2=BONDS,3=DIRECTS
#define OFF_TAD_ASMTCODE            76-1
#define OFF_TAD_ASMTDESC            82-1
#define OFF_TAD_ASMTRATE            127-1     // [-]8.6
#define OFF_TAD_TAXAMT              136-1     // [-]11.2

#define SIZ_TAD_TAXYEAR             4
#define SIZ_TAD_APN                 17
#define SIZ_TAD_BILLNUM             16
#define SIZ_TAD_COLLTYPE            17
#define SIZ_TAD_ASMTTYPE            15
#define SIZ_TAD_TRA                 5
#define SIZ_TAD_RECTYPE             1
#define SIZ_TAD_ASMTCODE            6
#define SIZ_TAD_ASMTDESC            45
#define SIZ_TAD_TAXRATE             9
#define SIZ_TAD_TAXAMT              12

typedef struct _SutTaxDetail
{
   char  TaxYear       [SIZ_TAD_TAXYEAR];
   char  Apn           [SIZ_TAD_APN];
   char  BillNum       [SIZ_TAD_BILLNUM];
   char  CollType      [SIZ_TAD_COLLTYPE];
   char  AsmtType      [SIZ_TAD_ASMTTYPE];
   char  TRA           [SIZ_TAD_TRA];
   char  RecType       [SIZ_TAD_RECTYPE];    // 1=tax, 2=bond, 3=direct assmnt
   char  AsmtCode      [SIZ_TAD_ASMTCODE];
   char  AsmtDesc      [SIZ_TAD_ASMTDESC];
   char  TaxRate       [SIZ_TAD_TAXRATE];
   char  TaxAmt        [SIZ_TAD_TAXAMT];
   char  CrLf[2];
} SUT_DETAIL;

// "2024 Roll Data - Secured - with All Prop Characteristics.csv"
#define  SUT_LC_PIN                             0
#define  SUT_LC_APN                             0
#define  SUT_LC_AIN                             1
#define  SUT_LC_EFFECTIVEYEAR                   2
#define  SUT_LC_EXTRACTDATETIME                 3
#define  SUT_LC_CONVEYANCENUMBER                4
#define  SUT_LC_CONVEYANCEMONTH                 5
#define  SUT_LC_CONVEYANCEYEAR                  6
#define  SUT_LC_CLASSDESCRIPTION                7
#define  SUT_LC_CLASSTYPE                       8
#define  SUT_LC_BASEYEAR                        9
#define  SUT_LC_BUSINESSUSECODEDESCRIPTION      10
#define  SUT_LC_DOINGBUSINESSAS                 11
#define  SUT_LC_ATTENTIONLINE                   12
#define  SUT_LC_MAILNAME                        13
#define  SUT_LC_MAILADDRESS                     14
#define  SUT_LC_MAILCITY                        15
#define  SUT_LC_MAILSTATE                       16
#define  SUT_LC_MAILZIPCODE                     17
#define  SUT_LC_SITUSNUMBER                     18
#define  SUT_LC_SITUSNUMBERSUFFIX               19
#define  SUT_LC_SITUSSTREETPREDIRECTIONAL       20
#define  SUT_LC_SITUSSTREETNAME                 21
#define  SUT_LC_SITUSSTREETTYPE                 22
#define  SUT_LC_SITUSUNITNUMBER                 23
#define  SUT_LC_SITUSZIPCODE                    24
#define  SUT_LC_SITUSCITYNAME                   25
#define  SUT_LC_ASSESSMENTDESCRIPTION           26
#define  SUT_LC_TRA                             27
#define  SUT_LC_GEO                             28
#define  SUT_LC_RECORDERSTYPE                   29
#define  SUT_LC_RECORDERSBOOK                   30
#define  SUT_LC_RECORDERSPAGE                   31
#define  SUT_LC_LOTTYPE                         32
#define  SUT_LC_LOT                             33
#define  SUT_LC_BLOCKNUMBER                     34
#define  SUT_LC_PORTIONDIRECTION                35
#define  SUT_LC_SUBDIVISIONNAMEORTRACT          36
#define  SUT_LC_ASSESSEDACREAGE                 37
#define  SUT_LC_FIRSTSECTION                    38
#define  SUT_LC_FIRSTTOWNSHIP                   39
#define  SUT_LC_FIRSTRANGE                      40
#define  SUT_LC_FIRSTRANGEDIRECTION             41
#define  SUT_LC_LEGALPARTY1                     42
#define  SUT_LC_LEGALPARTY2                     43
#define  SUT_LC_LEGALPARTY3                     44
#define  SUT_LC_LEGALPARTY4                     45
#define  SUT_LC_LEGALPARTY5                     46
#define  SUT_LC_LEGALPARTY6                     47
#define  SUT_LC_ASSDLAND                        48
#define  SUT_LC_ASSDIMP                         49
#define  SUT_LC_ASSDLIVIMP                      50
#define  SUT_LC_ASSDPERSONAL                    51
#define  SUT_LC_ASSDFIXTURES                    52
#define  SUT_LC_PERSONALPROPERTYAPPRAISED       53
#define  SUT_LC_TENPERCENTASSESSEDPENALTY       54
#define  SUT_LC_HOX                             55
#define  SUT_LC_VET_EXE                         56
#define  SUT_LC_DISABLEVET_EXE                  57
#define  SUT_LC_CHURCH_EXE                      58
#define  SUT_LC_RELIGIOUS_EXE                   59
#define  SUT_LC_CEMETERY_EXE                    60
#define  SUT_LC_PUBLICSCHOOL_EXE                61
#define  SUT_LC_PUBLICLIBRAY_EXE                62   
#define  SUT_LC_PUBLICMUSEUM_EXE                63
#define  SUT_LC_COLLEGE_EXE                     64
#define  SUT_LC_HOSPITAL_EXE                    65
#define  SUT_LC_PRIVATESCHOOL_EXE               66
#define  SUT_LC_CHARITYRELIGIOUS_EXE            67
#define  SUT_LC_OTHER_EXE                       68
#define  SUT_LC_CAMEFROM                        69
#define  SUT_LC_CLASSCODE                       70
#define  SUT_LC_CLASSCODEDESCRIPTION            71
#define  SUT_LC_STREETSURFACED                  72
#define  SUT_LC_SEWER                           73
#define  SUT_LC_DOMESTICWATER                   74
#define  SUT_LC_IRRIGATIONWATER                 75
#define  SUT_LC_WELLWATER                       76
#define  SUT_LC_UTILITYELECTRIC                 77
#define  SUT_LC_UTILITYGAS                      78
#define  SUT_LC_EFFYEARBUILT                    79
#define  SUT_LC_YEARBUILT                       80
#define  SUT_LC_NUMBEROFSTORIES                 81
#define  SUT_LC_ROOFTYPE                        82
#define  SUT_LC_GARAGETYPE                      83
#define  SUT_LC_GARAGEAREA                      84
#define  SUT_LC_CARPORTSIZE                     85
#define  SUT_LC_GARAGE2TYPE                     86
#define  SUT_LC_GARAGE2AREA                     87
#define  SUT_LC_CARPORT2SIZE                    88
#define  SUT_LC_HASFIREPLACE                    89
#define  SUT_LC_BATHSFULL                       90
#define  SUT_LC_BATHS3_4                        91
#define  SUT_LC_BATHSHALF                       92
#define  SUT_LC_TOTALBATHS                      93
#define  SUT_LC_HASCENTRALHEATING               94
#define  SUT_LC_HASCENTRALCOOLING               95
#define  SUT_LC_DESIGNTYPE                      96
#define  SUT_LC_CONSTRUCTIONTYPE                97
#define  SUT_LC_QUALITYCODE                     98
#define  SUT_LC_SHAPECODE                       99
#define  SUT_LC_LIVINGAREA                      100
#define  SUT_LC_ACTUALAREA                      101
#define  SUT_LC_HASPOOL                         102
#define  SUT_LC_BEDROOMCOUNT                    103
#define  SUT_LC_LANDUNITTYPE                    104
#define  SUT_LC_FLDS                            105

// 2024 - "2024 Roll Data - Secured.csv"
#define  SUT_L_PIN                  0
#define  SUT_L_TAXYEAR              1
#define  SUT_L_NEXTYEAR             2
#define  SUT_L_ASMTTYPE             3
#define  SUT_L_ROLLTYPE             4
#define  SUT_L_TAG                  5
#define  SUT_L_CLASSCD              6
#define  SUT_L_COUNTYNAME           7
#define  SUT_L_PRIMARYOWNER         8
#define  SUT_L_RECIPIENT            9
#define  SUT_L_DELIVERYADDR         10
#define  SUT_L_LASTLINE             11
#define  SUT_L_SITUSADDR            12
#define  SUT_L_SITUSCITY            13
#define  SUT_L_SITUSSTATE           14
#define  SUT_L_SITUSPOSTALCD        15
#define  SUT_L_SITUSCOMPLETE        16
#define  SUT_L_TOTALMARKET          17
#define  SUT_L_TOTALFBYV            18
#define  SUT_L_HOX                  19
#define  SUT_L_DVX                  20
#define  SUT_L_LDVX                 21
#define  SUT_L_OTHEREXMPT           22
#define  SUT_L_ASSDLAND             23
#define  SUT_L_ASSDIMP              24
#define  SUT_L_ASSDPERSONAL         25
#define  SUT_L_ASSDFIXTURES         26
#define  SUT_L_ASSDLIVIMP           27
#define  SUT_L_ASSESSEDFULL         28
#define  SUT_L_TOTALEXMPT           29
#define  SUT_L_PENALTY              30
#define  SUT_L_NETTAXABLE           31
#define  SUT_L_ATTNLINE             32
#define  SUT_L_MAILCOM              33
#define  SUT_L_MAILCOMWA            34
#define  SUT_L_VAL_DIFF             35
#define  SUT_L_FLDS                 36

// 2023 - "2023 Secured Roll.csv"
//#define  SUT_L_PIN                  0
//#define  SUT_L_TAXYEAR              1
//#define  SUT_L_ASMTTYPE             2
//#define  SUT_L_ROLLTYPE             3
//#define  SUT_L_TAG                  4
//#define  SUT_L_CLASSCD              5
//#define  SUT_L_COUNTYNAME           6
//#define  SUT_L_PRIMARYOWNER         7
//#define  SUT_L_RECIPIENT            8
//#define  SUT_L_DELIVERYADDR         9
//#define  SUT_L_LASTLINE             10
//#define  SUT_L_SITUSADDR            11
//#define  SUT_L_SITUSCITY            12
//#define  SUT_L_SITUSPOSTALCD        13
//#define  SUT_L_SITUSCOMPLETE        14
//#define  SUT_L_TOTALMARKET          15
//#define  SUT_L_TOTALFBYV            16
//#define  SUT_L_HOX                  17
//#define  SUT_L_DVX                  18
//#define  SUT_L_LDVX                 29
//#define  SUT_L_OTHEREXMPT           20
//#define  SUT_L_ASSDLAND             21
//#define  SUT_L_ASSDIMP              22
//#define  SUT_L_ASSDPERSONAL         23
//#define  SUT_L_ASSDFIXTURES         24
//#define  SUT_L_ASSDLIVIMP           25
//#define  SUT_L_ASSESSEDFULL         26
//#define  SUT_L_TOTALEXMPT           27
//#define  SUT_L_PENALTY              28
//#define  SUT_L_NETTAXABLE           29
//#define  SUT_L_ATTNLINE             30
//#define  SUT_L_MAILCOM              31
//#define  SUT_L_MAILCOMWA            32
//#define  SUT_L_PERSEC2PARCEL        33
//#define  SUT_L_FLDS                 34

// 2021 ANNUAL ROLLS - SEPARATED.TXT
#define  L_APN                      0
#define  L_TAXYEAR                  1
#define  L_NEXTYEAR                 2
#define  L_ASMTTYPE                 3
#define  L_SUT_ROLLTYPE             4
#define  L_SUT_TRA                  5
#define  L_USECODEDESC              6
#define  L_OWNERX                   7        // Owner name - last name first, mostly blank
#define  L_SUT_OWNER                8
#define  L_MADDR1                   9
#define  L_MADDR2                   10
#define  L_SADDR1                   11
#define  L_SCITY                    12
#define  L_SZIP                     13
#define  L_SITUS                    14
#define  L_TOTALMARKET              15
#define  L_TOTALFBYV                16
#define  L_HOEXE                    17
#define  L_EXE1                     18
#define  L_EXE2                     19
#define  L_OTHEREXE                 20
#define  L_LAND                     21
#define  L_IMPR                     22
#define  L_PPVAL                    23
#define  L_FIXTURES                 24
#define  L_LIVINGIMPR               25
#define  L_ASSDVAL                  26
#define  L_TOTALEXE                 27
#define  L_NETVAL                   28
#define  L_REVOBJID                 29
#define  L_ASMTTRANID               30
#define  L_ASMTID                   31
#define  L_PRINTDATE                32
#define  L_FLDS                     33

USEXREF  SUT_UseTbl[] =
{
   "00","120",      //VACANT - RESIDENTIAL
   "01","101",      //SINGLE FAMILY RESIDENCE
   "02","103",      //DUPLEX OR DOUBLE RESIDENTIAL
   "03","100",      //MULTIPLE RESIDENTIAL
   "04","106",      //RESIDENTIAL - COURTS
   "05","115",      //MOBILE HOME (0 TO 5 ACRES)
   "06","600",      //RECREATIONAL RESID (5-40 AC)
   "10","833",      //VACANT - COMMERCIAL PROPERTY
   "11","200",      //RETAIL STORE/USED CAR LOT
   "12","200",      //RETAIL STORE W/OFFICE OVER
   "13","200",      //OFFICES, NON-PROFESSIONAL
   "14","200",      //RESTAURANT OR BAR
   "15","200",      //COMMERCIAL - SERVICE SHOP
   "16","200",      //HOTEL / MOTEL
   "17","234",      //SERVICE STATION
   "18","200",      //COMMERCIAL RECREATIONAL PROP
   "19","200",      //COMMERCIAL WHOLESALE OUTLET
   "21","200",      //COMMERCIAL - TRAILER PARK
   "22","200",      //PROF. BLDGS (MD,DDS,LAW ETC)
   "23","246",      //NURSERY
   "24","312",      //BANK
   "25","240",      //COMMERCIAL - PARKING LOT
   "26","226",      //SHOPPING CENTER
   "27","419",      //COMMERCIAL - AIRPORT
   "28","200",      //NEWSPAPER OR RADIO STATION
   "29","200",      //SHIPYARD, WHARF OR DOCKS
   "30","829",      //VACANT - INDUSTRIAL PROPERTY
   "31","402",      //LIGHT MANUFACTURING
   "32","403",      //HEAVY MANUFACTURING
   "33","415",      //INDUSTRIAL - PACKING PLANT
   "34","420",      //INDUSTRIAL MINERAL EXTRACTN
   "35","404",      //INDUSTRIAL - WAREHOUSING
   "36","400",      //INDUSTRIAL - JUNK YARD
   "40","175",      //IRRIGATED RURAL PROPERTY
   "41","175",      //RURAL - IRRIGATED ORCHARD
   "42","175",      //RURAL - IRRIGATED VINEYARD
   "43","175",      //RURAL - IRRIGATED PASTURE
   "44","175",      //RURAL - IRRIGATED ROW CROP
   "45","175",      //RECREATIONAL RESID.40-160 AC
   "50","175",      //DRY RURAL PROPERTY
   "51","175",      //RURAL - DRY ORCHARD
   "52","175",      //RURAL - DRY VINEYARD
   "53","175",      //RURAL - DRY CLASSIFIED LAND
   "54","175",      //RURAL - DRY RANGE LAND
   "55","175",      //RECREATIONAL RESID.40-160 AC
   "60","536",      //TIMBER
   "61","536",      //TIMBER:ASSESSABLE OLD-GROWTH
   "62","536",      //TIMBER:ASSESSABLE YNG-GROWTH
   "63","536",      //TIMBER: EXEMPT OLD-GROWTH
   "64","536",      //TIMBER: EXEMPT YOUNG-GROWTH
   "65","536",      //TIMBER:EXEMPT YNG/OLD GROWTH
   "66","536",      //RECREATNL RES TAXABLE TIMBER
   "67","536",      //RECREATNL RESD EXEMPT TIMBER
   "70","826",      //VACANT - INSTITUTIONAL PROP.
   "71","726",      //CHURCH
   "72","807",      //SCHOOL
   "73","753",      //HOSPITAL
   "74","752",      //CONVALESCENT HOSPITAL
   "75","752",      //REST HOME
   "76","751",      //ORPHANAGE,BRDING SCH,ACADEMY
   "77","758",      //MORTUARY
   "78","758",      //CEMETERY,MAUSOLEUM,CREMATORY
   "79","600",      //RECREATIONAL NON-PROFIT ORG.
   "80","776",      //MISCELLANEOUS
   "81","780",      //WASTE LAND
   "82","776",      //RIGHT-OF-WAY
   "83","783",      //MINERAL RIGHTS
   "84","536",      //TIMBER RIGHTS
   "85","789",      //UTILITY
   "", ""
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SUT_Exemption[] = 
{
   "09", "E", 2,1,      // CEMETERY 
   "10", "V", 2,1,      // VETERANS' EXEMPTION
   "12", "D", 2,1,      // DISABLED VETERANS' EXEMPTION (124,932 BASE)
   "13", "D", 2,1,      // DISABLED VETERANS' EXEMPTION (187,399 BASE)
   "20", "W", 2,1,      // WELFARE EXEMPTION- CHARITABLE
   "21", "S", 2,1,      // WELFARE - PRIVATE/PAROCHIAL SCHOOLS < COLLEGE LEVEL
   "22", "I", 2,1,      // WELFARE - HOSPITAL
   "23", "U", 2,1,      // WELFARE - PRIVATELY OWNED COLLEGE
   "24", "L", 2,1,      // WELFARE - LIBRARY
   "25", "W", 2,1,      // WELFARE -LOW-INCOME HOUSING
   "26", "P", 2,1,      // WELFARE -PUBLIC SCHOOLS
   "30", "R", 2,1,      // RELIGIOUS (CARD)
   "32", "C", 2,1,      // CHURCH & RELIGIOUS (LONG FORM)
   "LF", "X", 2,1,      // EXEMPTION - LATE FILE
   "44", "H", 2,1,      // HOMEOWNER EXEMPTION - TIMELY FILING
   "4H", "H", 2,1,      // HOMEOWNER- REMOVE AFTER ROLL CLOSURE JAN 1 TO JUNE 30
   "4P", "H", 2,1,      // HOMEOWNER PENDING RESEARCH
   "4R", "H", 2,1,      // HOMEOWNER - REMOVE AFTER ROLL CLOSE
   "4S", "H", 2,1,      // HOMEOWNER - HO AVAILABLE FOR S/R BUT NOT R-1-P
   "4X", "H", 2,1,      // HOMEOWNER - MAIL FOR NEW OWNER ON LIEN DATE
   "50", "G", 2,1,      // GOVERNMENT EXEMPT
   "55", "X", 2,1,      // MISCELLANEOUS (PERS.PROP/DEALER LNV- MOBILE HOMES)
   "60", "X", 2,1,      // LOW VALUE
   "70", "Y", 2,1,      // SOLDIERS & SAILORS EXEMPTIONS
   "71", "X", 2,1,      // HISTORICAL AIRCRAFT
   "86", "X", 2,1,      // LESSORS' EXEMPTION
   "99", "X", 2,1,      // MISCELLANEOUS - EXEMPTS EVERYTHING
   "","",0,0
};

#endif