/**************************************************************************
 *
 * Notes:
 *    - Copy from MNO
 *
 * Revision
 * 11/28/2022 22.5.0    TUL is converted to MB system.  Copy from MNO and customize for TUL
 * 12/24/2022 22.5.2    Fine tune for production
 * 12/29/2022 22.5.4    Modify Load_Roll() to rebuild roll file before processing.
 * 01/01/2023 22.5.4.1  Fix Tul_MergeRoll() to reset Zoning before update.
 * 01/23/2023 22.5.6    Add -Xf option and Tul_ExtrVal() & Tul_CreateValueRec().
 *                      Fix tax update in Tul_Load_Roll().
 * 06/29/2023 22.0.3    Add Tul_MergeLien3() to support lien record group 3.  Use chars
 *                      from CHAR file since it's more reliable than from LDR file.
 *                      Update Tul_MergeStdChar() to allow usage of some char fields from lien file.
 * 07/27/2023 23.0.7    Fix LotSqft in Tul_MergeLien3(), Tul_MergeRoll() and Tul_MergeStdChar().
 * 07/10/2024 24.0.1    Modify Tul_MergeLien3() to add ExeType.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doZip.h"
#include "doOwner.h"
#include "doGrGr.h"
#include "formatApn.h"

#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "PQ.h"
#include "Charrec.h"
#include "MergeTul.h"
#include "Tax.h"

/******************************* Tul_MergeChar *******************************
 *
 * Merge Tul_Char.dat in STDCHAR format
 *
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the one that has value.
 *
 *****************************************************************************/

int Tul_MergeStdChar(char *pOutbuf, bool bLdr=false)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   ULONG    lSqft;
   int      iLoop, iBeds, iFBath, iHBath, iBldgNum, iRooms, iUnits, iTmp;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   while (!iLoop)
   {
      // Quality Class
      if (pChar->QualityClass[0] > ' ')
      {
         *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
         *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
         memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);
      }

      // YrBlt
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

      // YrEff
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

      // BldgSqft
      lSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      } else if (!bLdr)
         memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

      // Garage Sqft
      lSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
      } else if (!bLdr)
      {
         memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = ' ';
      }

      // Park space
      iTmp = atoin(pChar->ParkSpace, SIZ_CHAR_SIZE4);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_PARK_SPACE, iTmp);
         memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      } else if (!bLdr)
         memset(pOutbuf+OFF_PARK_SPACE, ' ', SIZ_PARK_SPACE);

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "001040017000", 9))
      //   lSqft = 0;
#endif

      // LotSqft
      lSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10 && *(pOutbuf+OFF_LOT_SQFT+8) == ' ')
      {
         if (lSqft < 999999999)
         {
            sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lSqft);
            memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
         } else
            memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);

         lSqft = atoin(pChar->LotAcre, SIZ_CHAR_SQFT);
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lSqft);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }

      // PatioSqft
      lSqft = atoin(pChar->PatioSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_PATIO_SF, lSqft);
         memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
      } else if (!bLdr)
         memset(pOutbuf+OFF_PATIO_SF, ' ', SIZ_BLDG_SF);

      // Heating
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   
      // Cooling 
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

      // Total Rooms
      iRooms = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
      if (iRooms > 0)
      {
         sprintf(acTmp, "%*d", SIZ_ROOMS, iRooms);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      } else if (!bLdr)
         memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

      // Beds
      iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      } else if (!bLdr)
         memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

      // Bath
      iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      } else if (!bLdr)
         memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

      // Half bath
      iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
      if (iHBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      } else if (!bLdr)
         memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

      // Fireplace
      *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

      // HasSeptic or HasSewer
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

      // HasWell
      *(pOutbuf+OFF_WATER) = pChar->HasWater;

      // Pools
      *(pOutbuf+OFF_POOL) = pChar->Pool[0];

      // Units count
      iUnits = atoin(pChar->Units, SIZ_CHAR_UNITS);
      if (iUnits > 0)
      {
         sprintf(acTmp, "%*d", SIZ_UNITS, iUnits);
         memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
      } 
      // 12/22/2022 - New char file doesn't have UNITS data
      //else
      //   memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

      // Stories
      if (pChar->Stories[0] > ' ')
         memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
     else if (!bLdr)
         memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

      // BldgSeqNum
      iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);

      lCharMatch++;

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (!pRec)
         break;
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (!iLoop && (iBeds > 0 && iBldgNum > 0))
         break;
   }

   return 0;
}

/******************************* Tul_MakeDocLink *******************************
 *
 * Format DocLink
 *    S_DOCLINK = 'dt;'+SUBSTRING(S_DOCNUM,1,4)+'.'+cast(cast(SUBSTRING(S_DOCNUM,6,7) as int) as varchar(6))+';2470'
 *       WHERE S_DOCNUM IS NOT NULL     
 *       and SUBSTRING(S_DOCNUM, 5, 1) = 'R'
 *       and SUBSTRING(S_DOCNUM, 6, 1) <= '9'
 *       and S_DOCDATE > 20000701
 *
 ******************************************************************************/

void Tul_MakeDocLink(LPSTR pDocLink, LPSTR pDoc, LPSTR pDate)
{
   char  acDocName[256];
   int   lDocDate, lDocNum;

   *pDocLink = 0;
   lDocDate = atoin(pDate, 8);
   if (*pDoc > ' ' && *(pDoc+4) == 'R' && *(pDoc+5) <= '9' && lDocDate > 20000701)
   {
      lDocNum = atoin(pDoc+5, 7);
      sprintf(acDocName, "dt;%.4s.%.6d;2470", pDoc, lDocNum);     
      strcpy(pDocLink, acDocName);
   }
}

/*****************************************************************************
 *
 *****************************************************************************/

void Tul_FormatApn(LPSTR pApn, LPSTR pFmtApn, int *piApnCnt)
{
   char acApn[64], acTmp[64], acTmp2[64], *pTmp, *pTmp1;
   int  iRet, iTmp, iLen;

   *pFmtApn = 0;
   *piApnCnt = 0;
   if (!pApn || !*pApn || *pApn == '-')
      return;

   if (pTmp = isCharIncluded(pApn, 0))
      return;

   strcpy(acTmp, pApn);

   // Check for multiple APN
   *piApnCnt = 1;

   // Take first APN and ignore the rest
   if (pTmp = strchr(acTmp, ';'))
   {
      iRet = countChar(acTmp, ';');
      *piApnCnt = iRet;
      *pTmp = 0;
   }
   if (pTmp = strchr(acTmp, ','))
   {
      blankRem(acTmp);
      iRet = ParseString(acTmp, ',', MAX_FLD_TOKEN, apTokens);
      if (strlen(apTokens[iRet-1]) > 7)
         *piApnCnt = iRet;
      *pTmp = 0;
   }
   if (pTmp = strchr(acTmp, ' '))
   {
      blankRem(acTmp);
      iRet = ParseString(acTmp, ' ', MAX_FLD_TOKEN, apTokens);
      if (strlen(apTokens[iRet-1]) > 7)
         *piApnCnt = iRet;
      *pTmp = 0;
   }
   if (pTmp = strchr(acTmp, '.'))
      *pTmp = 0;

   // acTmp should now contains only one APN
   if ((pTmp = strchr(acTmp, '*')) || (pTmp = strchr(acTmp, '+')) || (pTmp = strchr(acTmp, '`')))
      strcpy(pTmp, pTmp+1);

   // If last char is '-', remove it
   iRet = strlen(acTmp);
   if (acTmp[iRet-1] == '-')
      acTmp[iRet-1] = 0;

   // Make first part with 2 digits
   if (acTmp[1] == '-')
   {
      sprintf(acTmp2, "0%s", acTmp);
      strcpy(acTmp, acTmp2);
   }

   // Drop if APN is less than 7 digits
   if (strlen(acTmp) < 7)
   {
      *piApnCnt = 0;
      return;
   }

   // Original                 Format              Translate
   // --------                 ------              ---------
   // 60-230-14            ==> 060230014000
   // 8-095-03             ==> 008095003000
   // 02-60-45             ==> 002060045000
   // 26-09034             ==> 002609034000000 ==> 026090034000
   // 19210-01             ==> 019210010000
   // 33-160-66-02         ==> 003316066020000 ?
   // 32-160-01-201        ==> 003216001201000 ?
   // 35-086-03-021-07     ==> 003508603021070 ==> ?
   // 35-242-07-000-000    ==> 003524207000000 ==> 035242007000
   // 31-220-06-0010       ==> 003122006001000 ==> 219010000000
   // 15-086-03-0004-45    ==> 001508603000445 ==> 233004045000
   // 00-32-100-01-0015-00 ==> 003210001001500 ==> 203015000000
   // 00-15-086093-0008-35 ==> 001508609300083 ==> ?
   // 3324009              ==> 003324009000000 ==> 033240009000
   // 3102010010           ==> 003102010010000 ?
   // 003205003008500      ==> 003205003008500 ==> 344085000000
   // 19/181/01            ==> 019181001000
   // 02-361-03/04         ==> 002361003000
   // 15-086-03-0022-10/15-086-03-0019-52
   //                      ==> 001508603002210 ==> 233022010000
   // 15-08/6-03-13987     ==> ?
   // 08-184-03 / 14-A
   // 22-320-23/24/25/28
   // 16/212-08
   // 22-470-2-
   // 15-010-12-0017
   // 26-2720=-08
   // 40=110=10=0004       ==> 004011010000400 ==> 330004000000
   // 024-100-008-000

   // Replace '=' with '-'
   replChar(acTmp, '=', '-');

   // Replace '--' with '-'
   replStr(acTmp, "--", "-");

   // If there is '/' in string and no '-' preceeded it, replace all with '-'
   // If there is '/' in string and no '-' in it, replace all with '-'
   // If there is '/' in string and more than one '-' preceeded it, terminate string right there
   pTmp = strchr(acTmp, '/');
   if (pTmp)
   {
      *pTmp = 0;        // Terminate string here
      if (!(pTmp1 = strchr(acTmp, '-')) || !(pTmp1 = strchr(pTmp+1, '-')))
      {
         *pTmp = '-';
         replChar(acTmp, '/', '-');
      } else
         *piApnCnt = 2;
   }

    // Save it
   strcpy(acApn, acTmp);
   iLen = strlen(acTmp);
   iRet = ParseString(acTmp, '-', MAX_FLD_TOKEN, apTokens);
   switch(iRet)
   {
      case 1:
         if (strlen(acApn) == 12)
            sprintf(acTmp2, "%s000", acApn);
         else if (acApn[0] > '0')
            sprintf(acTmp2, "00%s000000", acApn);
         else
            sprintf(acTmp2, "%s00000000", acApn);
         break;
      case 2:
         iTmp = strlen(apTokens[0]);
         if (iTmp < 3)
         {
            if (acApn[0] > '0')
               sprintf(acTmp2, "00%.3d%s000000000", atol(apTokens[0]), apTokens[1]);
            else
               sprintf(acTmp2, "%.3d%s00000000000", atol(apTokens[0]), apTokens[1]);
         } else if (iTmp > 3)
         {
            if (acApn[0] > '0')
               sprintf(acTmp2, "0%s%s000000000", apTokens[0], apTokens[1]);
            else
               sprintf(acTmp2, "%s%s000000000", apTokens[0], apTokens[1]);
         } else
         {
            if (acApn[0] > '0')
               sprintf(acTmp2, "00%s%s000000000", apTokens[0], apTokens[1]);
            else
               sprintf(acTmp2, "%s%s000000000", apTokens[0], apTokens[1]);
         }
         break;
      case 3:
         // Format each tokens as %.3d then add "000" to the end
         sprintf(acTmp2, "%.3d%.3d%.3d000000", atol(apTokens[0]), atol(apTokens[1]), atol(apTokens[2]));
         break;
      default: // 4, 5, 6, 7
         if (iRet == 4 && iLen == 15)
            sprintf(acTmp2, "%s%s%s%s", apTokens[0], apTokens[1], apTokens[2], apTokens[3]);
         else
         {
            // Should starts and ends with "00"
            if (iRet == 4 && acApn[3] == '-' && acApn[7] == '-' && acApn[11] == '-')
               sprintf(acTmp2, "%s000", acApn);
            else if (!memcmp(acApn, "00", 2))
               sprintf(acTmp2, "%s000000000", acApn);
            else if (acApn[0] == '0')
               sprintf(acTmp2, "0%s000000000", acApn);
            else
               sprintf(acTmp2, "00%s000000000", acApn);
            remChar(acTmp2, '-');
         }
         break;
   }

   acTmp2[15] = 0;
   strcpy(pFmtApn, acTmp2);
}

/******************************** Tul_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

//void Tul_MergeOwner(char *pOutbuf, char *pNames)
//{
//   int   iTmp, iTmp1;
//   char  acTmp[128], *pTmp, *pTmp1;
//   char  acOwners[64];
//   OWNER myOwner;
//
//   // Clear output buffer if needed
//   removeNames(pOutbuf, false);
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "002361013", 9) )
//   //   iTmp = 0;
//#endif
//
//   // Remove multiple spaces
//   if (*pNames == '"')
//   {
//      pTmp = strcpy(acOwners, pNames+1);
//      acOwners[strlen(pTmp)-1] = 0;
//   } else
//      pTmp = strcpy(acOwners, pNames);
//
//   replStrAll(acOwners, "\"\"", "\"");
//   replChar(acOwners, ',', ' ');
//   replChar(acOwners, '.', ' ');
//   iTmp = iTmp1 = 0;
//   while (*pTmp)
//   {
//      // Mark name with numeric value
//      if (isdigit(*pTmp))
//         iTmp1++;
//
//      // skip unwanted chars at beginning of name
//      if (*pTmp < '1' && iTmp == 0)
//      {
//         pTmp++;
//         continue;
//      }
//
//      // skip unwanted chars at beginning of name
//      if (*pTmp < '1' && iTmp == 0)
//      {
//         pTmp++;
//         continue;
//      }
//      acTmp[iTmp++] = *pTmp;
//      // Remove multi-space
//      if (*pTmp == ' ')
//         while (*pTmp == ' ') pTmp++;
//      else
//         pTmp++;
//   }
//   if (acTmp[iTmp-1] == ' ' || acTmp[iTmp-1] == '"')
//      iTmp -= 1;
//   acTmp[iTmp] = 0;
//
//   // Update vesting
//   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
//
//   // Drop ET AL
//   if ((pTmp=strstr(acTmp, " ETAL &")) || (pTmp=strstr(acTmp, " ET AL &")) )
//   {
//      memset(pTmp, ' ', 6);
//      blankRem(pTmp);
//   }
//
//   if (pTmp=strstr(acTmp, "(ET AL"))
//      *pTmp = 0;
//   else if (pTmp=strstr(acTmp, " ETAL"))
//      *pTmp = 0;
//   else if (pTmp=strstr(acTmp, " ET AL"))
//      *pTmp = 0;
//   strcpy(acOwners, acTmp);
//   
//   // Drop everything from these words
//   if (pTmp=strstr(acTmp, " CO-TR"))
//      *pTmp = 0;
//   else if (pTmp=strstr(acTmp, ",FAM"))
//      *pTmp = 0;
//
//   if ((pTmp=strstr(acTmp, " TRUST &")))
//   {
//      memset(pTmp, ' ', 6);
//      blankRem(pTmp);
//   }
//
//   if (pTmp = strchr(acTmp, '&'))
//   {
//      if (pTmp1 = strchr(pTmp+1, '&'))
//         *pTmp1++ = 0;
//   }
//
//   // Now parse owners
//   acTmp[63] = 0;
//   splitOwner(acTmp, &myOwner, 0);
//
//   iTmp = strlen(acOwners);
//   if (iTmp > SIZ_NAME1)
//      iTmp = SIZ_NAME1;
//   memcpy(pOutbuf+OFF_NAME1, acOwners, iTmp);
//
//   iTmp = strlen(myOwner.acSwapName);
//   if (iTmp > SIZ_NAME_SWAP)
//      iTmp = SIZ_NAME_SWAP;
//   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
//}

void Tul_MergeOwner(char *pOutbuf, char *pOwner)
{
   int   iTmp;
   char  acOwner1[128], acSave1[128], *pTmp;
   bool  bNoMerge=false, bKeep=false;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   strcpy(acOwner1, pOwner);
   if (pTmp = strchr(acOwner1, '&'))
   {
      if (*(pTmp-1) != ' ')
      {
         *pTmp++ = 0;
         sprintf(acSave1, "%s & %s", acOwner1, pTmp);
         strcpy(acOwner1, acSave1);
      }
   }

   blankRem(acOwner1);
   strcpy(acSave1, acOwner1);

   // Take out ()
   if (pTmp = strchr(acOwner1, '('))
   {
      replChar(acOwner1, '(', ' ');
      replChar(acOwner1, ')', ' ');
      blankRem(acOwner1);
   }

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acOwner1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   if (!memcmp(acOwner1, "THE ", 4) && (pTmp=strstr(acOwner1, " TRUST")) )
   {
      bKeep = true;
   } else if ((pTmp=strstr(acOwner1, " FAMILY T"))  ||
              (pTmp=strstr(acOwner1, " REVOCABLE")) ||
              (pTmp=strstr(acOwner1, " TRUST")))
   {
      *pTmp = 0;
      bNoMerge = true;
   }

   if ((pTmp=strstr(acOwner1, " CO-TRS"))  ||
       (pTmp=strstr(acOwner1, " TRS")) ||
       (pTmp=strstr(acOwner1, " TR ")))
      *pTmp = 0;

   // Now parse owners
   if (bNoMerge)
      memcpy(pOutbuf+OFF_NAME_SWAP, acOwner1, strlen(acOwner1));
   else if (bKeep)
      memcpy(pOutbuf+OFF_NAME_SWAP, acSave1, strlen(acSave1));
   else
   {
      iTmp = splitOwner(acOwner1, &myOwner, 5);
      if (iTmp == -1)
         memcpy(pOutbuf+OFF_NAME_SWAP, acSave1, strlen(acSave1));
      else
         memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, strlen(myOwner.acSwapName));
   }

   // Name1
   vmemcpy(pOutbuf+OFF_NAME1, acSave1, SIZ_NAME1);
}

/******************************** Tul_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Tul_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "061134014", 9))
   //   iTmp = 0;
#endif

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      iTmp = strlen(pTmp);
      if (iTmp > SIZ_DBA)
         iTmp = SIZ_DBA;
      memcpy(pOutbuf+OFF_DBA, pTmp, iTmp);
   } 

   // Mail address
   strcpy(acAddr1, _strupr(apTokens[MB_ROLL_M_ADDR]));
   vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

   // Parse mail address
   parseMAdr1_1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
     sMailAdr.strName[SIZ_M_STREET] = 0;
     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   char sZip[16];
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, _strupr(apTokens[MB_ROLL_M_CITY]), SIZ_M_CITY);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, _strupr(apTokens[MB_ROLL_M_ST]), 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         if (*(apTokens[MB_ROLL_M_ZIP]+5) == '-')
         {
            vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], 5);
            vmemcpy(pOutbuf+OFF_M_ZIP4, apTokens[MB_ROLL_M_ZIP]+6, 4);
         } else
            vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], 9);
      }

      memcpy(sZip, pOutbuf+OFF_M_ZIP, 9);
      iTmp = atol(sZip);
      if (iTmp > 100000)
         sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], sZip, &sZip[5]);
      else if (iTmp > 400)
         sprintf(acTmp, "%s %s %.5s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
      else
         sprintf(acTmp, "%s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST]);

      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

void Tul_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4=NULL)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2, sAdr1[128];
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "031130003000", 9) )
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   strcpy(sAdr1, pLine1);
   lTrim(sAdr1);

   if (sAdr1[0] == ' ' || sAdr1[0] == '0' || *pLine2 == '0')
      return;
   if (sAdr1[0] == '#')
      sAdr1[0] = ' ';

   acAddr1[0] = 0;
   p0 = NULL;
   if (pLine4 && *pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(sAdr1, "C/O", 3)  ||
          !_memicmp(sAdr1, "ATTN", 4) ||
          sAdr1[0] == '%')
      {
         p0 = sAdr1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(sAdr1[0]))
            p1 = sAdr1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(sAdr1, "C/O", 3)  ||
          !_memicmp(sAdr1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = sAdr1;
         p1 = pLine2;
      } else if (!memcmp(sAdr1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, sAdr1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(sAdr1, "STE"))
            p1 = sAdr1;
         else
         {
            sprintf(acAddr1, "%s %s", sAdr1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", sAdr1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = sAdr1;
            p2 = pLine2;
         } else if (isdigit(sAdr1[0]))
         {
            p1 = sAdr1;
         } else
         {
            p1 = pLine2;
            p0 = sAdr1;
         }
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = sAdr1;
      p2 = pLine2;
   } else
   {
      p2 = sAdr1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
      /*
      if (!_memicmp(p0, "C/O", 3))
         pTmp = p0+4;
      else if (!_memicmp(p0, "ATTN", 4))
         pTmp = p0+5;
      else if (*p0 == '%')
         pTmp = p0+1;
      else
         pTmp = p0;

      while (*pTmp == ' ')
         pTmp++;
      iTmp = strlen(pTmp);
      if (iTmp > SIZ_CARE_OF)
         iTmp = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, pTmp, iTmp);
      */
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      parseMAdr1_1(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      } else
      {
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
      }
   }

   strcpy(acAddr2, p2);
   remChar(acAddr2, ',');     // Remove comma
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      iTmp = strlen(sMailAdr.Zip);
      memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, iTmp);
      if (isdigit(sMailAdr.Zip4[0]))
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, strlen(sMailAdr.Zip4));
   }
}

/******************************** Tul_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Tul_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acAddr1[256], acSuffix[16], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do {
         pRec = fgets(acRec, 512, fdSitus);
      } while (pRec && !isdigit(*pRec));
   }

   pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input string
   _strupr(pRec);
   iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old data
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      // Save original house number
      //memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
      }
   }

   quoteRem(apTokens[MB_SITUS_STRNAME]);
   iTmp = strlen(apTokens[MB_SITUS_STRNAME]);
   if (iTmp > 0)
   {
      // Fix known bad char in StrName
      if (pTmp = strchr(apTokens[MB_SITUS_STRNAME], 241))
         *pTmp = 'N';

      acSuffix[0] = 0;
      if (*apTokens[MB_SITUS_STRTYPE] > ' ')
      {
         strcpy(acSuffix, apTokens[MB_SITUS_STRTYPE]);
         iTmp = GetSfxDev(acSuffix);
         if (iTmp > 0)
         {
            sprintf(acTmp, "%d     ", iTmp);
            memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
         } else
         {
            LogMsg0("*** Invalid suffix: %s [%s]", apTokens[MB_SITUS_STRTYPE], apTokens[MB_SITUS_ASMT]);
            iBadSuffix++;
         }        
      } else
      {
         // Check last word in StrName for suffix
         if (pTmp = strrchr(apTokens[MB_SITUS_STRNAME], ' '))
         {
            strcpy(acSuffix, pTmp+1);
            iTmp = GetSfxDev(acSuffix);
            if (iTmp > 0)
            {
               sprintf(acTmp, "%d     ", iTmp);
               memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
            } else
               acSuffix[0] = 0;
         }
      }

      vmemcpy(pOutbuf+OFF_S_STREET, apTokens[MB_SITUS_STRNAME], SIZ_S_STREET);
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      if (acSuffix[0] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, acSuffix);
      }

      if (*apTokens[MB_SITUS_UNIT] > ' ')
      {
         if (*apTokens[MB_SITUS_UNIT] != '#')
            iTmp = sprintf(acTmp, " #%s", apTokens[MB_SITUS_UNIT]);
         else
            iTmp = sprintf(acTmp, " %s", apTokens[MB_SITUS_UNIT]);
         strcat(acAddr1, acTmp);
         memcpy(pOutbuf+OFF_S_UNITNO, &acTmp[1], iTmp-1);
         memcpy(pOutbuf+OFF_S_UNITNOX, &acTmp[1], iTmp-1);
      }
   }

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1, pOutbuf);
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
      {
         iTmp = sprintf(acTmp, "%s, CA %s", myTrim(acAddr1), apTokens[MB_SITUS_ZIP]);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
      }
   }

   // Situs zip
   if (*apTokens[MB_SITUS_ZIP] > ' ')
   {
      lTmp = atol(apTokens[MB_SITUS_ZIP]);
      if (lTmp >= 90000 && lTmp < 99999)
         memcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], 5);
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/* Not use
int Tul_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001060015", 9))
   //   acTmp[0] = 0;
#endif

   if (*pLine1 > '0')
   {
      // Change case
      _strupr(pLine1);

      strcpy(acAddr1, pLine1);
      blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

      memset(&sSitusAdr, 0, sizeof(ADR_REC));
      parseMAdr1(&sSitusAdr, acAddr1);

      if (sSitusAdr.lStrNum > 0)
      {
         char *pTmp = strchr(acAddr1, ' ');
         *pTmp = 0;

         // Save original StrNum
         memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
         memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
         memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
      }

      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

      memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.strSfx[0] > ' ')
      {
         Sfx2Code(sSitusAdr.strSfx, acTmp);
         memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
      }

      if (sSitusAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));
   }

   // Situs city
   _strupr(pLine2);
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] >= 'A')
   {
      char *pTmp;
      int  iTmp, iCityIdx;

      sSitusAdr.State[0] = 0;    // Prevent long city name
      iCityIdx = City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (iCityIdx > 0)
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         iCityIdx = atol(acTmp);
         if (pTmp = GetCityName(iCityIdx))
         {
            iTmp = sprintf(acTmp, "%s CA", pTmp);
            memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
         }
      } else
         memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   } else
      memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));

   return 0;
}
*/

/******************************** Tul_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Tul_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdSale);
      pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg("Skip Sale rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   while (!iTmp)
   {
      // Replace tab char with 0
      if (pRec)
         iRet = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "381014000000", 9))
      //   iRet = 0;
#endif

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' &&
          *apTokens[MB_SALES_DOCDATE] > ' ' &&
          apTokens[MB_SALES_DOCNUM][4] == 'R')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         /* Do not use Confidential Sale Price
         } else
         {
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            }
         */
         }

         // DocType - need translation before production
         // 01, 02, 03, 09, 10, 11, 12, 58, 81
         // Right now we only know 01 is GD, the rest needs investigation
         iTmp = atoi(apTokens[MB_SALES_DOCCODE]);
         if (iTmp > 0 && iTmp < 100)
         {
            iTmp = sprintf(acTmp, "%d", iTmp);
            memcpy(sCurSale.acDocType, acTmp, iTmp);
         }

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strcpy(acTmp, apTokens[MB_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         if (iTmp > SALE_SIZ_SELLER)
            iTmp = SALE_SIZ_SELLER;
         memcpy(sCurSale.acSeller, acTmp, iTmp);

         MB_MergeSale(&sCurSale, pOutbuf, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/********************************* Tul_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Tul_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace tab char with 0
   if (cDelim == '|')
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN starts with 800-999 except 910 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910))
      return 1;

   // Ignore administrative parcels
   if (!_memicmp(apTokens[MB_ROLL_OWNER], "Assessor Administrative", 20))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "54TUL", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      ULONG lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      ULONG lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      ULONG lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      ULONG lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      ULONG lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      ULONG lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      ULONG lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      ULONG lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%u         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%u         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%u         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%u         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%u         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%u         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // Legal 
      if (*apTokens[MB_ROLL_LEGAL] > ' ')
      {
         updateLegal(pOutbuf, _strupr(apTokens[MB_ROLL_LEGAL]));
      }
   }

   // TRA
   lTmp = atol(apTokens[MB_ROLL_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "035245001000", 9))
   //   iTmp = 0;
#endif

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   if (*apTokens[MB_ROLL_USECODE] > ' ')
   {
      strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
      blankPadz(acTmp, SIZ_USE_CO);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO);

      // Standard UseCode
      iTmp = atol(acTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 0, pOutbuf);
   } else
   {
      memset(pOutbuf+OFF_USE_CO, ' ', SIZ_USE_CO);
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
   }
   
   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
      if (lTmp < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      } else
         memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (ULONG)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      if (*(apTokens[MB_ROLL_DOCNUM]+4) == 'R' && *(apTokens[MB_ROLL_DOCNUM]+5) <= '9')
         sprintf(acTmp, "%.4sR%.7d", apTokens[MB_ROLL_DOCNUM], atol(apTokens[MB_ROLL_DOCNUM]+5));
      else
         strcpy(acTmp, apTokens[MB_ROLL_DOCNUM]);
      vmemcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, SIZ_TRANSFER_DOC);

      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   }

   // Owner
   try {   
      lTrim(apTokens[MB_ROLL_OWNER]);
      if (*apTokens[MB_ROLL_OWNER] > ' ')
         Tul_MergeOwner(pOutbuf, _strupr(apTokens[MB_ROLL_OWNER]));
      //else if (*(apTokens[MB_ROLL_OWNER]+1) > ' ')
      //   Tul_MergeOwner(pOutbuf, _strupr(apTokens[MB_ROLL_OWNER]+1));
      else if (*apTokens[MB_ROLL_CAREOF] > ' ')
      {
         LogMsg("*** No owner name, use CareOf for owner: %.12s", pOutbuf);
         Tul_MergeOwner(pOutbuf, _strupr(apTokens[MB_ROLL_CAREOF]));
      } else
         LogMsg("*** No owner name, no CareOf: %.12s", pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Tul_MergeOwner()");
   }

   // Mailing
   try {
      if (*apTokens[MB_ROLL_M_ADDR] > ' ')
         Tul_MergeMAdr(pOutbuf);
      else if (*apTokens[MB_ROLL_M_ADDR1] > ' ')
         Tul_MergeMAdr(pOutbuf, apTokens[MB_ROLL_M_ADDR1], apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3], apTokens[MB_ROLL_M_ADDR4]);
   } catch(...) {
      LogMsg("***** Exeception occured in Tul_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************** Tul_Load_Roll *****************************
 *
 * Roll file may have <CR> in the middle of the record.  Rebuild CSV before processing.
 *
 ******************************************************************************/

int Tul_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }
   lLastFileDate = getFileDate(acRollFile);

   sprintf(acTmpFile, "%s\\TUL\\Tul_Roll.txt", acTmpPath);
   iRet = RebuildCsv_IQ(acRollFile, acTmpFile, cDelim, MB_ROLL_M_ADDR4+1);
   if (iRet > 100000)
   {
      sprintf(acRollFile, "%s\\TUL\\Tul_Roll.srt", acTmpPath);
      // Sort new file
      iTmp = sortFile(acTmpFile, acRollFile, "S(1,12,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") F(TXT)");
      if (iTmp <= 0)
         return -1;
   } else
      return -1;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -3;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -4;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -6;
   }

   // Fix Tul_Tax.csv
   sprintf(acTmpFile, "%s\\TUL\\Tul_Tax.txt", acTmpPath);
   iRet = RebuildCsv_IQ(acTaxFile, acTmpFile, cDelim, T2_FLDS);
   if (iRet < 100000)
   {
      LogMsg("***** Error rebuilding tax file: %s\n", acTaxFile);
      return -1;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTmpFile);
   fdTax = fopen(acTmpFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
      return -7;
   }

   // Open lien file
   fdLienExt = NULL;
   //sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (!_access(acTmpFile, 0))
   //{
   //   LogMsg("Open Lien file %s", acTmpFile);
   //   fdLienExt = fopen(acTmpFile, "r");
   //   if (fdLienExt == NULL)
   //   {
   //      LogMsg("***** Error opening lien file: %s\n", acTmpFile);
   //      return -7;
   //   }
   //}

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -9;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -10;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

#ifdef _DEBUG
      //if (!memcmp(acBuf, "001210007000", 10))
      //   iTmp = 0;
#endif

NextRollRec:
      iTmp = memcmp(acBuf, (char *)&acRollRec[0], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Tul_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Tul_MergeSitus(acBuf);

            // Merge Lien
            lRet = 1;
            if (fdLienExt)
               lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_TUL, true);

            // Merge Exe if not found in LienExt
            if (fdExe && lRet)
               lRet = MB_MergeExe(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Tul_MergeStdChar(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acBuf, iHdrRows);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Tul_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Tul_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Tul_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acRec, iHdrRows);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Tul_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Tul_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Tul_MergeStdChar(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, iHdrRows);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   printf("\n");

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   return 0;
}

/****************************** Tul_ExtrTR601Rec ****************************
 *
 * Format lien extract record from TR601 file.  This function may not work with
 * all counties.  Check lien file layout first before use.
 *
 ****************************************************************************/

int Tul_ExtrTR601Rec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp, iLen;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "001060037000", 12))
   //   iRet = 0;
#endif

   // Parse string ignoring quote
   //iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   iLen = strlen(apTokens[L_ASMT]);
   memcpy(pLienExtr->acApn,  apTokens[L_ASMT], iLen);

   // TRA
   lTmp = atol(apTokens[L_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lGrow = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);
   long lFixt   = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);
   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Prop 8
   lTmp = atoin(apTokens[L_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Save Exe code
   memcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
   memcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
   memcpy(pLienExtr->extra.MB.ExeCode3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************* Tul_ExtrTR601 ******************************
 *
 * Extract lien data from ???_lien.csv
 *
 ****************************************************************************/

int Tul_ExtrTR601(LPCSTR pCnty, LPCSTR pLDRFile=NULL)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg("Extract lien roll for %s", pCnty);

   // Open lien file
   if (!pLDRFile)
   {
      GetIniString(pCnty, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
      if (!acBuf[0])
         return -1;
   } else
      strcpy(acBuf, pLDRFile);

   LogMsg("Open Lien Date Roll file %s", acBuf);
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Sort on ASMT
   iRet = sortFile(acBuf, acTmpFile, "S(#1,C,A)");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acTmpFile);
      return -2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Replace null char with space
      //iRet = replChar(acRec, 0, ' ', 0);

      // Create new record
      iRet = Tul_ExtrTR601Rec(acBuf, acRec);

      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Tul_ExtrLien *******************************
 *
 * Extract lien data from Tul_yyyy.txt
 * Use this function to extract data from SQL export file as of LDR 2014.
 *
 ****************************************************************************/

int Tul_ExtrLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp, iLen;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "001060037000", 12))
   //   iRet = 0;
#endif

   // Parse string ignoring quote
   if (cLdrSep == 9)
      iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < TUL_L_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[TUL_L_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   iLen = remChar(apTokens[TUL_L_ASMT], '-');
   //iLen = strlen(apTokens[TUL_L_ASMT]);
   if (iLen < iApnLen)
   {
      memcpy(pLienExtr->acApn, "0000", iApnLen - iLen);
      memcpy(&pLienExtr->acApn[iApnLen - iLen], apTokens[TUL_L_ASMT], iLen);
   } else
      memcpy(pLienExtr->acApn, apTokens[TUL_L_ASMT], iLen);

   // TRA
   lTmp = atol(apTokens[TUL_L_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[TUL_L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[TUL_L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   //long lGrow = atoi(apTokens[TUL_L_CURRENTGROWINGIMPRVALUE]);
   long lGrow = 0;   // Not avail in 2014
   long lFixt = atoi(apTokens[TUL_L_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[TUL_L_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[TUL_L_CURRENTPERSONALPROPMHVALUE]);
   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Prop 8
   lTmp = atoin(apTokens[TUL_L_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Save Exe code
   memcpy(pLienExtr->extra.MB.ExeCode1, apTokens[TUL_L_EXEMPTIONCODE1], strlen(apTokens[TUL_L_EXEMPTIONCODE1]));
   memcpy(pLienExtr->extra.MB.ExeCode2, apTokens[TUL_L_EXEMPTIONCODE2], strlen(apTokens[TUL_L_EXEMPTIONCODE2]));
   memcpy(pLienExtr->extra.MB.ExeCode3, apTokens[TUL_L_EXEMPTIONCODE3], strlen(apTokens[TUL_L_EXEMPTIONCODE3]));

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int Tul_ExtrLien(LPCSTR pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg0("Extract LDR roll for %s", pCnty);

   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acRollFile);
      return -2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Replace null char with space
      //iRet = replChar(acRec, 0, ' ', 0);

      // Create new record
      iRet = Tul_ExtrLienRec(acBuf, acRec);

      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/*********************************** Tul_FixR01 ********************************
 *
 * Input/Output:  R01 file
 *
 ******************************************************************************/

int Tul_FixR01(char *pCnty, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acTmp[32], *pTmp;
   char     acOutFile[_MAX_PATH], acRawFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet;
   int      iRet;
   long     lCnt=0;

   // Setup file names
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "TMP");

   if (_access(acRawFile, 0))
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");

   LogMsg("Fix %s file for %s.  Correcting APN", acRawFile, pCnty);

   // Open R01 file
   LogMsg("Open R01 file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   bRet = true;
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      // Format APN
      pTmp = &acBuf[0];
      iRet = sprintf(acTmp, "0%.2s%.3s0%.2s%.3s ", pTmp, pTmp+2, pTmp+5, pTmp+7);
      memcpy(&acBuf[OFF_APN_S], acTmp, iRet);

      iRet = sprintf(acTmp, "%.3s-%.3s-%.3s-%.3s  ", pTmp, pTmp+3, pTmp+6, pTmp+9);
      memcpy(&acBuf[OFF_APN_D], acTmp, iRet);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }
   printf("\n");

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   sprintf(acTmp, acRawTmpl, pCnty, pCnty, "T01");
   if (!_access(acTmp, 0))
      remove(acTmp);
   LogMsg("Rename %s to %s", acRawFile, acTmp);
   iRet = rename(acRawFile, acTmp);
   LogMsg("Rename %s to %s", acOutFile, acRawFile);
   iRet = rename(acOutFile, acRawFile);

   LogMsgD("Total output records:       %u", lCnt);

   return iRet;
}

/**************************** Tul_ConvStdChar ********************************
 *
 * Copy from MergeHum.cpp to convert new char file. 11/02/2015
 * Other counties has similar format: MER, YOL, HUM, MAD, SBT
 *
 *****************************************************************************/

int Tul_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   long     lGarSqft;

   STDCHAR  myCharRec;

   LogMsg0("Converting char file %s", pInfile);

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_char.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort char file %s to %s", pInfile, acTmpFile);
   // FeeParcel and Asmt are the same.  We can sort on either one.
   iRet = sortFile(pInfile, acTmpFile, "S(#1,C,A) DEL(124) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
   if (iRet < 500)
   {
      LogMsg("***** Input file is too small.");
      return 1;
   }

   if (!(fdIn = fopen(acTmpFile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Start loop
   while (!feof(fdIn))
   {
      // Get next record
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec || acBuf[0] > '9')
         break;

      replStrAll(acBuf, "|NULL", "|");
      iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < TUL_CHAR_HASWELL)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[TUL_CHAR_ASMT], strlen(apTokens[TUL_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[TUL_CHAR_FEEPARCEL], strlen(apTokens[TUL_CHAR_FEEPARCEL]));

      // Format APN
      if (*apTokens[TUL_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[TUL_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[TUL_CHAR_FEEPARCEL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[TUL_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[TUL_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool 
      if (*apTokens[TUL_CHAR_POOLSPA] > ' ')
      {
         pRec = findXlatCodeA(apTokens[TUL_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass 
      iTmp = remChar(apTokens[TUL_CHAR_QUALITYCLASS], ' ');    // Remove all blanks before process
      pRec = _strupr(apTokens[TUL_CHAR_QUALITYCLASS]);
      if (*pRec == 'X') pRec++;

      vmemcpy(myCharRec.QualityClass, pRec, SIZ_CHAR_QCLS);

      if (*pRec > '0' && *pRec <= 'Z')
      {
         acCode[0] = ' ';
         strcpy(acTmp, pRec);
         if (isalpha(acTmp[0])) 
         {
            myCharRec.BldgClass = acTmp[0];

            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            else if (isdigit(acTmp[2]))
               iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
            else if (!_memicmp(acTmp, "ST", 2))
               memcpy(myCharRec.QualityClass, "ST", 2);     // Steel
         } else if (isdigit(acTmp[0]))
            iRet = Quality2Code(acTmp, acCode, NULL);
         else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[TUL_CHAR_QUALITYCLASS], apTokens[TUL_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (*pRec > ' ' && memcmp(pRec, "00", 2))
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[TUL_CHAR_QUALITYCLASS], apTokens[TUL_CHAR_ASMT]);

      // YrBlt
      int iYrBlt = atoi(apTokens[TUL_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[TUL_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[TUL_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "096321015000", 9))
      //   iRet = 0;
#endif

      // Units Count
      iTmp = atoi(apTokens[TUL_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // Stories/NumFloors
      iTmp = atoi(apTokens[TUL_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 99)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF 
      int iAttGar = atoi(apTokens[TUL_CHAR_ATTACHGARAGESF]);
      lGarSqft = iAttGar;
      if (iAttGar > 100)
      {
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[TUL_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         // Check for dup
         if (iDetGar == iAttGar)
         {
            myCharRec.ParkType[0] = 'Z';
         } else
         {
            if (iAttGar > 100)
               myCharRec.ParkType[0] = 'Z';
            else
               myCharRec.ParkType[0] = 'L';
            lGarSqft += iDetGar;
         }
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
      }

      // Carport Sqft
      int iCarport = atoi(apTokens[TUL_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         // Check for dup
         if (iCarport == iAttGar)
         {
            myCharRec.ParkType[0] = 'C';
         } else
         {
            if (iAttGar > 100)
               myCharRec.ParkType[0] = 'Z';
            else
               myCharRec.ParkType[0] = 'C';
            lGarSqft += iCarport;
         }
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
      }

      if (lGarSqft > 100)
      {
         iRet = sprintf(acTmp, "%d", lGarSqft);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
      }

      // Park spaces
      iTmp = atoi(apTokens[TUL_CHAR_PARKSPACE]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.ParkSpace, acTmp, iRet);
      }

      // Patio SF
      iTmp = atoi(apTokens[TUL_CHAR_PATIOSF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[TUL_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCodeA(apTokens[TUL_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[TUL_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[TUL_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[TUL_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[TUL_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Rooms
      iTmp = atoi(apTokens[TUL_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[TUL_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[TUL_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[TUL_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace
      iTmp = atol(apTokens[TUL_CHAR_FIREPLACE]);
      if (iTmp > 9)
         myCharRec.Fireplace[0] = 'M';
      else if (iTmp > 0)
         myCharRec.Fireplace[0] = *apTokens[TUL_CHAR_FIREPLACE];
      else if (*apTokens[TUL_CHAR_FIREPLACE] == 'Y' || *apTokens[TUL_CHAR_FIREPLACE] == 'N')
         myCharRec.Fireplace[0] = *apTokens[TUL_CHAR_FIREPLACE];
      else if (*apTokens[TUL_CHAR_FIREPLACE] > '0')
         LogMsg("*** Bad FirePlace %s [%s]", apTokens[TUL_CHAR_FIREPLACE], apTokens[TUL_CHAR_ASMT]);

      // Haswell
      if (*(apTokens[TUL_CHAR_HASWELL]) == 'T' || *(apTokens[TUL_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Lot Sqft
      if (iFldCnt >= TUL_CHAR_LOTSQFT)
      {
         ULONG    lSqft;
         lSqft = (ULONG)atol(apTokens[TUL_CHAR_LOTSQFT]);
         if (lSqft > 1)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(lSqft*SQFT_MF_1000);
            iTmp = sprintf(acTmp, "%*u", SIZ_CHAR_SQFT, (long)(dTmp+0.5));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum/UnitSeqNum, UnitSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/******************************** Tul_MergeLien3 *****************************
 *
 * For 2023 LDR AGENCYCDCURRSEC_TR601.TXT
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Tul_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_ISAGPRESERVE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // APN
   vmemcpy(pOutbuf, apTokens[L3_ASMT], iApnLen);

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Copy ALT_APN
   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], SIZ_ALT_APN);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "54TUL", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   ULONG lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   ULONG lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   ULONG lFixtr = atol(apTokens[L3_FIXTURESVALUE]);
   ULONG lFixtRP= atol(apTokens[L3_FIXTURESRP]);
   ULONG lGrow  = atol(apTokens[L3_GROWING]);
   ULONG lPers  = atol(apTokens[L3_PPVALUE]);
   ULONG lPP_MH = atol(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%u         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%u         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%u         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   ULONG lExe1 = atol(apTokens[L3_HOX]);
   ULONG lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      vmemcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], SIZ_EXE_CD1);

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&TUL_Exemption);

   // Legal
   remChar(apTokens[L3_PARCELDESCRIPTION], '"');
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode - Ignore usecode starts with "XX"
   if (*apTokens[L3_LANDUSE1] > ' ' && memcmp(apTokens[L3_LANDUSE1], "XX", 2))
   {
      iTmp = strlen(apTokens[L3_LANDUSE1]);
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO, iTmp);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
      if (lTmp < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }

      // Format Acres
      lTmp = (ULONG)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   remChar(apTokens[L3_OWNER], '"');
   Tul_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Mailing
   remChar(_strupr(apTokens[L3_MAILADDRESS1]), '"');
   remChar(_strupr(apTokens[L3_MAILADDRESS2]), '"');
   remChar(_strupr(apTokens[L3_MAILADDRESS3]), '"');
   remChar(_strupr(apTokens[L3_MAILADDRESS4]), '"');
   Tul_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc
   if (*apTokens[L3_CURRENTDOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp && isNumber(apTokens[L3_CURRENTDOCNUM]+5))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         iTmp = sprintf(acTmp, "%.5s%.6d", apTokens[L3_CURRENTDOCNUM], atol(apTokens[L3_CURRENTDOCNUM]+5));
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, iTmp);
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // Garage size
   //dTmp = atof(apTokens[L3_GARAGESIZE]);
   //if (dTmp > 0.0)
   //{
   //   sprintf(acTmp, "%*u", SIZ_GAR_SQFT, dTmp);
   //   memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   //   *(pOutbuf+OFF_PARK_TYPE) = '2';                 // GARAGE/CARPORT
   //}

   // Number of parking spaces
   lTmp = atol(apTokens[L3_GARAGE]);
   if (lTmp > 1)
   {
      iTmp = sprintf(acTmp, "%u", lTmp);
      memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, iTmp);
   }

   // YearBlt
   lTmp = atol(apTokens[L3_YEARBUILT]);
   if (lTmp > 1800 && lTmp < lToyear)
   {
      iTmp = sprintf(acTmp, "%u", lTmp);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // Total rooms
   iTmp = atol(apTokens[L3_TOTALROOMS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Units
   iTmp = atol(apTokens[L3_UNITS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%u", iTmp);
      vmemcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Beds
   //int iBeds = atol(apTokens[L3_BEDROOMS]);
   //if (iBeds > 0)
   //{
   //   sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
   //   memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   //}

   // Baths
   //int iFBaths = atol(apTokens[L3_BATHS]);
   //int iHBaths = atol(apTokens[L3_HALFBATHS]);
   //if (iFBaths == 0 && iHBaths > 0 && iBeds > 0)
   //{
   //   iFBaths = iHBaths;
   //   iHBaths = 0;
   //}
   //if (iFBaths > 0)
   //{
   //   sprintf(acTmp, "%*u", SIZ_BATH_F, iFBaths);
   //   memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   //}
   //if (iHBaths > 0)
   //{
   //   sprintf(acTmp, "%*u", SIZ_BATH_H, iHBaths);
   //   memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   //}

   // Heating
   //int iCmp;
   //if (*apTokens[L3_HEATING] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_HEATING], asHeating[iTmp].acSrc, asHeating[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
   //}

   // Cooling - no data
   //if (*apTokens[L3_AC] == 'C')
   //   *(pOutbuf+OFF_AIR_COND) = 'C';
   //else if (*apTokens[L3_AC] > ' ')
   //   LogMsg("*** Unknown A/C: %s", apTokens[L3_AC]);

   // Pool/Spa - no data
   //if (*apTokens[L3_POOLSPA] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_POOLSPA], asPool_LDR[iTmp].acSrc, asPool_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_POOL) = asPool_LDR[iTmp].acCode[0];
   //}

   return 0;
}

/******************************** Tul_Load_LDR ******************************
 *
 * Load LDR 2023
 *
 ****************************************************************************/

int Tul_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acLienFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);

   // Sort roll file on ASMT
   sprintf(acLienFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   //2021, 2023
   sprintf(acRec, "S(#3,C,A) DEL(%d) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")", cLdrSep);
   // 2022
   //sprintf(acRec, "S(#2,C,A) DEL(%d) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")", cLdrSep);
   iRet = sortFile(acTmpFile, acLienFile, acRec);  
   if (!iRet)
      return -1;

   // Open Lien file
   LogMsg("Open lien file %s", acLienFile);
   fdLDR = fopen(acLienFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acLienFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -4;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      // Create new R01 record
      iRet = Tul_MergeLien3(acBuf, acRec); 

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Tul_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Tul_MergeStdChar(acBuf, true);

         // Merge Exe 
         //if (fdExe)
         //   lRet = MB_MergeExe(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else if (iRet == -99)
         break;

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Situs skiped:     %u\n", lSitusSkip);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return iRet;
}

/***************************** TUL_CreateSCSale *****************************
 *
 * Copy from MB_CreateSCSale and customized for TUL
 *
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * DateFmt: 
 *    MM_DD_YYYY_1 
 *
 * DocTypeFmt: 
 *    0 : Default. Set DocType=1 (GD) if DocCode=1 or sale price > 0
 *    1 : AMA, BUT, MAD, SON, SBT
 *    2 : PLA
 *    3 : SHA, STA
 *
 * DocNumFmt: 
 *    0 : Default. Copy DocNum as is
 *    1 : Format DocNum second part of DocNum to 7 digits (i.e. 2010R1234 = 2010R0001234)
 *    2 : Format DocNum second part of DocNum to n digits (i.e. 2010R0034 = 2010R34) (COL)
 *    3 : Remove all nonnumeric after 5th character and format to 6 digits (SON)
 *    4 : Format to yyyyR9999999 if R is in pos 4 or 5 of original DocNum (SBT)
 *    5 : Format DocNum second part of DocNum to 5 digits (i.e. 2010R1234 = 2010R01234) (HUM)
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Tul_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], acDocNum[16], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp;

   LogMsg0("Loading Sale file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   do {
      lTmp = ftell(fdSale);
      pTmp = fgets(acRec, 1024, fdSale);
   } while (pTmp && !isdigit(*(pTmp+1)));    // Skip quote on 1st char
   
   if (fseek(fdSale, lTmp, SEEK_SET))
   {
      LogMsg("***** Error setting current position in sale file using fseek()");
      fclose(fdSale);
      return -2;
   }

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (strlen(acRec) < 14)
         break;

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Ignore DocNum start with 1900
      if (!memcmp(apTokens[MB_SALES_DOCNUM], "1900R", 5))
         continue;

      if (*(apTokens[MB_SALES_DOCNUM]+4) != 'R')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "052202011000", 9))
      //   iTmp = 0;
#endif
      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Default date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
      }

      if (pTmp)
      {
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }

      // Verify DocNum
      if (memcmp(apTokens[MB_SALES_DOCNUM], SaleRec.DocDate, 4))
      {
         LogMsg("*** Ignore transaction DocNum=%s, DocDate=%s, APN=%s", apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_DOCDATE], apTokens[MB_SALES_ASMT]);
         continue;
      }

      // Docnum
      lTmp = atol(apTokens[MB_SALES_DOCNUM]+5);
      iTmp = sprintf(acDocNum, "%.5s%.5d", apTokens[MB_SALES_DOCNUM], lTmp);
      memcpy(SaleRec.DocNum, acDocNum, iTmp);

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1' || *apTokens[MB_SALES_GROUPSALE] == 'T')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001130011000", 9))
      //   iTmp = 0;
#endif
      // Confirmed sale price
      lPrice = atol(apTokens[MB_SALES_PRICE]);
      if (lPrice > 1000)
      {
         iTmp = sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      } 

      // Do not use confirmed sale price
      //if (!bUseConfSalePrice)
      //   lPrice = 0;

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001160016000", 9))
      //   iTmp = 0;
#endif
      // Tax
      dTmp = atof(apTokens[MB_SALES_TAXAMT]);
      if (dTmp > 0)
      {
         // Save DocTax
         iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);

         // Calculate sale price
         lTmp = (long)(dTmp * SALE_FACTOR);
         iTmp = ((int)dTmp/100)*100;

         // Check for bad DocTax
         if (lPrice != lTmp && dTmp > 100000)
         {
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else if (lPrice > 100000 && lPrice == (lPrice/100)*100)
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).", SaleRec.Apn, lPrice, dTmp);
            else if (lPrice > 0)
            {
               LogMsg("??? (1) Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f, CP=%d.  Need investigation.  Ignore price.", SaleRec.Apn, SaleRec.DocNum, dTmp, lPrice);
               lPrice = 0;
            } else
               lPrice = lTmp;
         } else if (iTmp == (int)dTmp && iTmp > 10000)
         {
            if (lTmp != (lTmp/100)*100)
            {
               LogMsg("*** (3) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
               lPrice = lTmp;
         } else 
            lPrice = lTmp;

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d (%d) \tTax=%.2f \tDOCCODE=%s", 
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, lTmp, dTmp, apTokens[MB_SALES_DOCCODE]);
            }
         }  
      } 

      // Ignore sale price if less than 1000
      if (lPrice >= 10000)
         sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
      else if (lPrice >= 1000)
         sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
      else
         memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
      memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);

      // Doc code - accept following code only
      int iDocCode = 0;
      if (*apTokens[MB_SALES_DOCCODE] > ' ')
      {
         iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
         if (iTmp >= 0)
         {
            memcpy(SaleRec.DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
            if (lPrice < 1000)
               SaleRec.NoneSale_Flg = pDocTbl[iTmp].flag;
         } else if (bDebug)
            LogMsg("*** Unknown DocCode: %s", apTokens[MB_SALES_DOCCODE]);
      } 

      // Save original DocCode
      vmemcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);

      // Transfer Type
      if (*apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         strcpy(acTmp, apTokens[MB_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Seller1, acTmp, SALE_SIZ_SELLER, iTmp);

         // Buyer
         strcpy(acTmp, apTokens[MB_SALES_BUYER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Name1, acTmp, SALE_SIZ_BUYER, iTmp);

         SaleRec.CRLF[0] = 10;
         SaleRec.CRLF[1] = 0;
         fputs((char *)&SaleRec,fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(20,1,C,EQ,\" \",OR,31,1,C,EQ,\" \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -2;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            // Save old cumsale file
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);
            iTmp = rename(acCSalFile, acTmpFile);
   
            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -2;
      } else
         iTmp = rename(acOutFile, acCSalFile);

      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acCSalFile, acTmpFile);
      }
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp)
   {
      iErrorCnt++;
      if (iTmp == -2)
         LogMsg("***** Error sorting output file");
      else
         LogMsg("***** Error renaming to %s", acCSalFile);
   }

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                          output: %d.", lTmp);
   LogMsg("         Latetest recording date: %d.", lLastRecDate);
   return iTmp;
}

/********************************* Tul_ExtrVal ******************************
 *
 * Extract values from tax roll Tul_Tax.csv
 *
 ****************************************************************************/

int Tul_CreateValueRec(char *pOutbuf)
{
   char     acTmp[256];
   LONGLONG lTmp;
   int      iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, apTokens[T2_ASMT], strlen(apTokens[T2_ASMT]));

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[T2_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atol(apTokens[T2_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // PP value
   long lPers = atol(apTokens[T2_CURRENTPERSONALPROPVALUE]);
   long lPP_MH = atol(apTokens[T2_CURRENTPERSONALPROPMHVALUE]);

   // Fixture
   long lFixt = atol(apTokens[T2_CURRENTFIXEDIMPRVALUE]);

   // Growing
   long lGrow = atol(apTokens[T2_CURRENTGROWINGIMPRVALUE]);

   // Total other
   long lOthers = lPers + lFixt + lGrow + lPP_MH;
   if (lOthers > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lOthers);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%u", lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%u", lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
      if (lGrow > 0)
      {
         iTmp = sprintf(acTmp, "%u", lGrow);
         memcpy(pLienRec->extra.MB.GrowImpr, acTmp, iTmp);
      }
      if (lPP_MH > 0)
      {
         iTmp = sprintf(acTmp, "%u", lPP_MH);
         memcpy(pLienRec->extra.MB.PP_MobileHome, acTmp, iTmp);
      }
   }

   // Gross
   lTmp = lOthers + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exe 
   pLienRec->acHO[0] = '2';            // N
   lTmp = atol(apTokens[T2_EXEMPTIONAMT1])+atol(apTokens[T2_EXEMPTIONAMT2]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      if (lTmp == 7000)
         pLienRec->acHO[0] = '1';      // Y
   } 

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Tul_ExtrVal()
{
   char      *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acTaxFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int       iRet;
   long      lOut=0, lCnt=0;
   FILE      *fdOut, *fdIn;

   LogMsg0("Extracting final values from equalized file.");

   // Open input file
   GetIniString(myCounty.acCntyCode, "TaxFile", "", acTaxFile, _MAX_PATH, acIniFile);

   // Fix CSV
   sprintf(acTmpFile, "%s\\TUL\\Tul_Tax.txt", acTmpPath);
   iRet = RebuildCsv_IQ(acTaxFile, acTmpFile, cDelim, T2_FLDS);
   if (iRet < 100000)
   {
      LogMsg("***** Error rebuilding tax file: %s\n", acTaxFile);
      return -1;
   }

   LogMsg("Open tax roll file %s", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax roll file: %s\n", acTmpFile);
      return -2;
   }  

   // Open out file
   sprintf(acTmpFile, acValueTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open file %s for output", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Drop header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Start loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp > '9')
         break;

      // Parse input record
      iTokens = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < T2_DTS)
      {
         LogMsg("*** Bad record: %s", acRec);
         continue;
      }

      // Take CS only
      iRet = atoin(apTokens[T2_ASMT], 3);
      if (iRet >= 800 && iRet < 900)
         continue;

      iRet = atol(apTokens[T2_TAXYEAR]);
      if (iRet == lTaxYear)
      {
         // Create new base record
         iRet = Tul_CreateValueRec(acBuf);
         if (!iRet)
         {
            fputs(acBuf, fdOut);
            lOut++;
         } else
         {
            LogMsg("---> Drop record %d ", lCnt); 
         }
      } else
         LogMsg("---> Ignore prior year %d [%s] ", iRet, apTokens[T2_ASMT]); 

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records output:    %u", lOut);

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return iRet;
}

/*********************************** loadTul ********************************
 *
 * Options:
 *    -CTUL -L -Xl -Xs -Xa -Mz (load lien)
 *    -CTUL -U -Xs|Ms -Xa -Mz  (load update)
 *
 ****************************************************************************/

int loadTul(int iSkip)
{
   int   iRet;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;

   iApnLen = myCounty.iApnLen;
   iRet = 0;

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Extract lien file - LDR2023
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acValueFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acValueFile, 0);
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode);
   }

   // Extract final value from tax file
   if (iLoadFlag & EXTR_FVAL)                      // -Xf
   {
      iRet = Tul_ExtrVal();
      if (iRet < 0)
         return iRet;
      iRet = 0;
   }

   // Fix cumsale
   if (iLoadFlag & FIX_CSTYP)
   {
      iRet = FixDocType(acCSalFile, (IDX_TBL5 *)&TUL_DocCode[0], true /* remove rec w/o DocNum */);
      if (iRet)
         return iRet;
   }

   // Create/Update cum sale file from Tul_Sales.csv
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      iRet = Tul_CreateSCSale(MM_DD_YYYY_1, 0, 0, true, (IDX_TBL5 *)&TUL_DocCode[0]);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;                   // Trigger merge sale
   }
   
   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      iRet = Tul_ConvStdChar(acCharFile);
      if (iRet <= 0)
      {
         LogMsg("***** Error extracting attributes data from %s", acCharFile);
         return -1;
      }
   }

   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      if (iLoadFlag & LOAD_LIEN)                   // -L
      {
         // Create Lien file
         LogMsg0("Load %s Lien file", myCounty.acCntyCode);
         iRet = Tul_Load_LDR(iSkip); 
      } else if (iLoadFlag & LOAD_UPDT)            // -U
      {
         LogMsg0("Load %s roll update file", myCounty.acCntyCode);
         iRet = Tul_Load_Roll(iSkip);
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL))           // -Ms
   {
      // Apply Tul_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE|CLEAR_UPD_GRGR);
   }

   return iRet;
}