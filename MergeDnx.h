
#if !defined(AFX_MERGEDNX_H_INCLUDED_)
#define AFX_MERGEDNX_H_INCLUDED_

// CDDATA.CSV
#define  DNX_ASMT          0
#define  DNX_FEEPARCEL     1
#define  DNX_TRA           2
#define  DNX_LEGAL         3
#define  DNX_OWNER         4
#define  DNX_CAREOF        5
#define  DNX_DBA           6
#define  DNX_ADDRESS1      7
#define  DNX_ADDRESS2      8
#define  DNX_ADDRESS3      9
#define  DNX_ADDRESS4      10
#define  DNX_M_ADDR     	11
#define  DNX_M_CITY        12
#define  DNX_M_ST       	13
#define  DNX_M_ZIP         14
#define  DNX_S_STRNAME     15
#define  DNX_S_STRNUM      16
#define  DNX_S_STRTYPE     17
#define  DNX_S_STRDIR      18
#define  DNX_S_UNIT        19
#define  DNX_S_COMMUNITY   20
#define  DNX_ACRES         21
#define  DNX_USECODE       22
#define  DNX_DOCNUM        23
#define  DNX_DOCDATE       24
#define  DNX_S_ZIP         25
#define  DNX_LAND          26
#define  DNX_HOMESITE      27
#define  DNX_IMPR          28
#define  DNX_GROWING       29
#define  DNX_FIXTRS        30
#define  DNX_PERSPROP      31
#define  DNX_BUSPROP       32
#define  DNX_PPMOBILHOME   33
#define  DNX_EXECODE       34
#define  DNX_EXEAMT        35
#define  DNX_FLDCOUNT      35

// 2018 DNX L9_
#define  L9_TAXYEAR                    0
#define  L9_ASMT                       1
#define  L9_TRA                        2
#define  L9_OWNER                      3
#define  L9_ASSESSEENAME               4
#define  L9_MAILADDRESS1               5
#define  L9_MAILADDRESS2               6
#define  L9_MAILADDRESS3               7
#define  L9_MAILADDRESS4               8
#define  L9_SITUS1                     9
#define  L9_SITUS2                     10
#define  L9_LANDVALUE                  11
#define  L9_STRUCTUREVALUE             12
#define  L9_FIXTURESVALUE              13
#define  L9_MHPPVALUE                  14
#define  L9_PPVALUE                    15
#define  L9_HOX                        16
#define  L9_OTHEREXEMPTION             17
#define  L9_LANDUSE1                   18
#define  L9_ACRES                      19
#define  L9_FLDCOUNT                   20

// Copy from LAS
#define  DNX_CHAR_FEEPARCEL            0
#define  DNX_CHAR_POOLSPA              1
#define  DNX_CHAR_CATTYPE              2
#define  DNX_CHAR_QUALITYCLASS         3
#define  DNX_CHAR_YRBLT                4
#define  DNX_CHAR_BUILDINGSIZE         5
#define  DNX_CHAR_ATTACHGARAGESF       6
#define  DNX_CHAR_DETACHGARAGESF       7
#define  DNX_CHAR_CARPORTSF            8
#define  DNX_CHAR_HEATING              9
#define  DNX_CHAR_COOLINGCENTRALAC     10
#define  DNX_CHAR_COOLINGEVAPORATIVE   11
#define  DNX_CHAR_COOLINGROOMWALL      12
#define  DNX_CHAR_COOLINGWINDOW        13
#define  DNX_CHAR_STORIESCNT           14
#define  DNX_CHAR_UNITSCNT             15
#define  DNX_CHAR_TOTALROOMS           16
#define  DNX_CHAR_EFFYR                17
#define  DNX_CHAR_PATIOSF              18
#define  DNX_CHAR_BEDROOMS             19
#define  DNX_CHAR_BATHROOMS            20
#define  DNX_CHAR_HALFBATHS            21
#define  DNX_CHAR_FIREPLACE            22
#define  DNX_CHAR_ASMT                 23
#define  DNX_CHAR_BLDGSEQNUM           24
#define  DNX_CHAR_HASWELL              25
#define  DNX_CHAR_LOTSQFT              26
#define  DNX_CHAR_PARKINGSPACES        27
#define  DNX_CHAR_FLDS                 28

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "1",   "1", 1,             // One Fireplace
   "2",   "2", 1,             // 2 Fireplaces
   "3",   "3", 1,             // 3 Fireplaces
   "4",   "4", 1,             // 4 Fireplaces
   "Y",   "Y", 1,             // 
   "0",   "N", 1,             // None
   "",   "", 0
};

static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "B",  "8", 1,              // Baseboard
   "CA", "7", 2,              // Cadet - electric heating
   "C",  "Z", 1,              // Central
   "E",  "7", 1,              // Electric
   "FA", "B", 1,              // Force Air
   "F",  "C", 1,              // Floor
   "G",  "N", 1,              // Gas
   "H",  "G", 2,              // Heat Pump
   "M",  "X", 1,              // Other
   "O",  "V", 1,              // Oil Monitor
   "P",  "6", 1,              // Portable
   "R",  "X", 1,              // Other
   "W",  "D", 1,              // Wall
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "1",   "P", 1,             // 1 pool
   "2",   "P", 1,             // 2 pools
   "3",   "C", 1,             // pool/spa
   "99",  "P", 2,             // pool
   "DB",  "J", 2,     	      // Doughboy Pool
   "FG",  "F", 2,     	      // Fiberglass Pool
   "GU",  "G", 2,     	      // Gunite Pool
   "VI",  "V", 2,     	      // Vinyl Pool
   "",   "", 0
};

IDX_TBL5 DNX_DocCode[] =
{  // DocCode, Index, Non-sale, DocCodelen, Indexlen
   "00", "74", 'Y', 2, 2,     // No value change
   "01", "1 ", 'N', 2, 2,     // Reevaluation transfer
   "13", "19", 'Y', 2, 2,     // New mobile home, not a sale
   "A3", "19", 'Y', 2, 2,     // Interspousal: death of spouse
   "A4", "75", 'Y', 2, 2,     // Transfer, but not a sale
   "C1", "19", 'Y', 2, 2,     // Legal entity: proportional interest reeval, not a sale
   "D0", "3 ", 'Y', 2, 2,     // Joint tenant: original transferor remain
   "D1", "3 ", 'Y', 2, 2,     // Joint tenant: added tenant (A to A & B)
   "D2", "3 ", 'Y', 2, 2,     // Joint tenant: added tenants (all as J/T)
   "D3", "19", 'Y', 2, 2,     // Joint tenant: death of one tenant, one tenant remain
   "H1", "61", 'Y', 2, 2,     // Trust: revocable
   "H5", "61", 'Y', 2, 2,     // Trust: termination of revoc trust
   "H6", "61", 'Y', 2, 2,     // Trust: from one trust to another trust
   "K2", "75", 'Y', 2, 2,     // Transfer on death: A to B
   "MF", "75", 'Y', 2, 2,     // 100% transfer no reappraisal
   "P2", "1 ", 'N', 2, 2,     // New MH qualifies for reappraisal
   "R1", "1 ", 'N', 2, 2,     // Reappraisal 100%
   "R2", "1 ", 'N', 2, 2,     // Reappraisal 50%
   "R3", "1 ", 'N', 2, 2,     // Partial Reappraisal
   "S3", "19", 'Y', 2, 2,     // Perfection of title
   "S6", "19", 'Y', 2, 2,     // Exempt - public entity
   "S7", "51", 'Y', 2, 2,     // Name change
   "T1", "1 ", 'N', 2, 2,     // Change in title only
   "T7", "19", 'Y', 2, 2,     // Previous Reappraised on DOD
   "U2", "1 ", 'N', 2, 2,     // Parent/child exclusion
   "","",0,0,0
};

// Standard codes
// C=Church
// D=Disabled Veterans
// E=Cemetery
// H=Homeowners
// I=Hospital
// L=Library
// M=Museums
// P=Public School
// R=Religion
// S=School
// T=Tribal Housing
// U=College
// V=Veterans
// W=Welfare
// X=Misc exempt

IDX_TBL4 DNX_Exemption[] = 
{
   "E01", "H", 3,1,
   "E20", "X", 3,1,     // NATIVE AMERICAN EXEMPTION (TILLIE HARDWICK)
   "E21", "X", 3,1,     // YUROK INDIAN HOUSING EXEMPTION YIHA LOW INCOME HOU
   "E30", "D", 3,1,
   "E40", "W", 3,1,
   "E41", "W", 3,1,
   "E42", "P", 3,1,
   "E45", "X", 3,1,     // LESSORS' EXEMPTION
   "E50", "R", 3,1,
   "E51", "C", 3,1,
   "E60", "M", 3,1,
   "E62", "X", 3,1,     // BOE NOT OVER 40,000
   "E70", "E", 3,1,
   "E80", "D", 3,1,
   "E90", "X", 3,1,     // VESSEL EXEMPTION
   "E91", "X", 3,1,     // HISTORICAL AIRCRAFT EXEMPTION
   "E92", "V", 3,1,
   "P19", "X", 3,1,     // PROP 19 - FOR BYVT 19 REPORTING ONLY
   "","",0,0
};

#endif // !defined(AFX_MERGEDNX_H_INCLUDED_)
