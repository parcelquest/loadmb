
#if !defined(AFX_MERGETEH_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_)
#define AFX_MERGETEH_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_

// 02/23/2024
#define  TEH_CHAR_FEEPRCL              0
#define  TEH_CHAR_POOLSPA              1
#define  TEH_CHAR_CATTYPE              2
#define  TEH_CHAR_QUALITYCLASS         3
#define  TEH_CHAR_YRBLT                4
#define  TEH_CHAR_BUILDINGSIZE         5
#define  TEH_CHAR_ATTACHGARAGESF       6
#define  TEH_CHAR_DETACHGARAGESF       7
#define  TEH_CHAR_CARPORTSF            8
#define  TEH_CHAR_HEATING              9
#define  TEH_CHAR_COOLINGCENTRALAC     10
#define  TEH_CHAR_COOLINGEVAPORATIVE   11
#define  TEH_CHAR_COOLINGROOMWALL      12
#define  TEH_CHAR_COOLINGWINDOW        13
#define  TEH_CHAR_STORIESCNT           14
#define  TEH_CHAR_UNITSCNT             15
#define  TEH_CHAR_TOTALROOMS           16
#define  TEH_CHAR_EFFYR                17
#define  TEH_CHAR_PATIOSF              18
#define  TEH_CHAR_BEDROOMS             19
#define  TEH_CHAR_BATHROOMS            20
#define  TEH_CHAR_HALFBATHS            21
#define  TEH_CHAR_FIREPLACE            22
#define  TEH_CHAR_ASMT                 23
#define  TEH_CHAR_BLDGSEQNO            24
#define  TEH_CHAR_HASWELL              25
#define  TEH_CHAR_LANDSQFT             26
#define  TEH_CHAR_PARKSPACES           27
#define  TEH_CHAR_FLDS                 28

//#define  TEH_CHAR_FEEPRCL                 0
//#define  TEH_CHAR_DOCNUM                  1
//#define  TEH_CHAR_CATTYPE                 2
//#define  TEH_CHAR_BLDGSEQNO               3
//#define  TEH_CHAR_UNITSEQNO               4
//#define  TEH_CHAR_YRBLT                   5
//#define  TEH_CHAR_BLDGTYPE                6
//#define  TEH_CHAR_EFFYR                   7
//#define  TEH_CHAR_BUILDINGSIZE            8
//#define  TEH_CHAR_BUILDINGUSEDFOR         9
//#define  TEH_CHAR_STORIESCNT              10
//#define  TEH_CHAR_UNITSCNT                11
//#define  TEH_CHAR_CONDITION               12
//#define  TEH_CHAR_BEDROOMS                13
//#define  TEH_CHAR_BATHROOMS               14
//#define  TEH_CHAR_QUALITYCLASS            15
//#define  TEH_CHAR_HALFBATHS               16
//#define  TEH_CHAR_CONSTRUCTION            17
//#define  TEH_CHAR_FOUNDATION              18
//#define  TEH_CHAR_STRUCTURALFRAME         19
//#define  TEH_CHAR_STRUCTURALFLOOR         20
//#define  TEH_CHAR_EXTERIORTYPE            21
//#define  TEH_CHAR_ROOFCOVER               22
//#define  TEH_CHAR_ROOFTYPEFLAT            23
//#define  TEH_CHAR_ROOFTYPEHIP             24
//#define  TEH_CHAR_ROOFTYPEGABLE           25
//#define  TEH_CHAR_ROOFTYPESHED            26
//#define  TEH_CHAR_INSULATIONCEILINGS      27
//#define  TEH_CHAR_INSULATIONWALLS         28
//#define  TEH_CHAR_INSULATIONFLOORS        29
//#define  TEH_CHAR_WINDOWPANESINGLE        30
//#define  TEH_CHAR_WINDOWPANEDOUBLE        31
//#define  TEH_CHAR_WINDOWPANETRIPLE        32
//#define  TEH_CHAR_WINDOWTYPE              33
//#define  TEH_CHAR_LIGHTING                34
//#define  TEH_CHAR_COOLINGCENTRALAC        35
//#define  TEH_CHAR_COOLINGEVAPORATIVE      36
//#define  TEH_CHAR_COOLINGROOMWALL         37
//#define  TEH_CHAR_COOLINGWINDOW           38
//#define  TEH_CHAR_HEATING                 39
//#define  TEH_CHAR_FIREPLACE               40
//#define  TEH_CHAR_GARAGE                  41
//#define  TEH_CHAR_PLUMBING                42
//#define  TEH_CHAR_SOLAR                   43
//#define  TEH_CHAR_BUILDER                 44
//#define  TEH_CHAR_BLDGDESIGNEDFOR         45
//#define  TEH_CHAR_MODELDESC               46
//#define  TEH_CHAR_UNFINAREASSF            47
//#define  TEH_CHAR_ATTACHGARAGESF          48
//#define  TEH_CHAR_DETACHGARAGESF          49
//#define  TEH_CHAR_CARPORTSF               50
//#define  TEH_CHAR_DECKSSF                 51
//#define  TEH_CHAR_PATIOSF                 52
//#define  TEH_CHAR_CEILINGHEIGHT           53
//#define  TEH_CHAR_FIRESPINKLERS           54
//#define  TEH_CHAR_AVGWALLHEIGHT           55
//#define  TEH_CHAR_BAY                     56
//#define  TEH_CHAR_DOCK                    57
//#define  TEH_CHAR_ELEVATOR                58
//#define  TEH_CHAR_ESCALATOR               59
//#define  TEH_CHAR_ROLLUPDOOR              60
//#define  TEH_CHAR_FIELD1                  61
//#define  TEH_CHAR_FIELD2                  62
//#define  TEH_CHAR_FIELD3                  63
//#define  TEH_CHAR_FIELD4                  64
//#define  TEH_CHAR_FIELD5                  65
//#define  TEH_CHAR_NUMFIREPLACE            66
//#define  TEH_CHAR_FIELD7                  67
//#define  TEH_CHAR_FIELD8                  68
//#define  TEH_CHAR_FIELD9                  69
//#define  TEH_CHAR_FIELD10                 70
//#define  TEH_CHAR_FIELD11                 71
//#define  TEH_CHAR_FIELD12                 72
//#define  TEH_CHAR_FIELD13                 73
//#define  TEH_CHAR_FIELD14                 74
//#define  TEH_CHAR_FIELD15                 75
//#define  TEH_CHAR_FIELD16                 76
//#define  TEH_CHAR_FIELD17                 77
//#define  TEH_CHAR_FIELD18                 78
//#define  TEH_CHAR_FIELD19                 79
//#define  TEH_CHAR_FIELD20                 80
//#define  TEH_CHAR_LANDFIELD1              81
//#define  TEH_CHAR_LANDFIELD2              82
//#define  TEH_CHAR_LANDFIELD3              83
//#define  TEH_CHAR_LANDFIELD4              84
//#define  TEH_CHAR_LANDFIELD5              85
//#define  TEH_CHAR_LANDFIELD6              86
//#define  TEH_CHAR_LANDFIELD7              87
//#define  TEH_CHAR_LANDFIELD8              88
//#define  TEH_CHAR_ACRES                   89
//#define  TEH_CHAR_NEIGHBORHOODCODE        90
//#define  TEH_CHAR_ZONING                  91
//#define  TEH_CHAR_TOPOGRAPHY              92
//#define  TEH_CHAR_VIEWCODE                93
//#define  TEH_CHAR_POOLSPA                 94
//#define  TEH_CHAR_WATERSOURCE             95
//#define  TEH_CHAR_SUBDIVNAME              96
//#define  TEH_CHAR_SEWERCODE               97
//#define  TEH_CHAR_UTILITIESCODE           98
//#define  TEH_CHAR_ACCESSCODE              99
//#define  TEH_CHAR_LANDSCAPE               100
//#define  TEH_CHAR_PROBLEMCODE             101
//#define  TEH_CHAR_FRONTAGE                102
//#define  TEH_CHAR_LOCATION                103
//#define  TEH_CHAR_PLANTEDACRES            104
//#define  TEH_CHAR_ACRESUNUSEABLE          105
//#define  TEH_CHAR_WATERSOURCEDOMESTIC     106
//#define  TEH_CHAR_WATERSOURCEIRRIGATION   107
//#define  TEH_CHAR_HOMESITES               108
//#define  TEH_CHAR_PROPERTYCONDITIONCODE   109
//#define  TEH_CHAR_ROADTYPE                110
//#define  TEH_CHAR_HASWELL                 111
//#define  TEH_CHAR_HASCOUNTYROAD           112
//#define  TEH_CHAR_HASVINEYARD             113
//#define  TEH_CHAR_ISUNSECUREDBUILDING     114
//#define  TEH_CHAR_HASORCHARD              115
//#define  TEH_CHAR_HASGROWINGIMPRV         116
//#define  TEH_CHAR_SITECOVERAGE            117
//#define  TEH_CHAR_PARKINGSPACES           118
//#define  TEH_CHAR_EXCESSLANDSF            119
//#define  TEH_CHAR_FRONTFOOTAGESF          120
//#define  TEH_CHAR_MULTIPARCELECON         121
//#define  TEH_CHAR_LANDUSECODE1            122
//#define  TEH_CHAR_LANDUSECODE2            123
//#define  TEH_CHAR_LANDSQFT                124
//#define  TEH_CHAR_TOTALROOMS              125
//#define  TEH_CHAR_NETLEASABLESF           126
//#define  TEH_CHAR_BLDGFOOTPRINTSF         127
//#define  TEH_CHAR_OFFICESPACESF           128
//#define  TEH_CHAR_NONCONDITIONSF          129
//#define  TEH_CHAR_MEZZANINESF             130
//#define  TEH_CHAR_PERIMETERLF             131
//#define  TEH_CHAR_ASMT                    132
//#define  TEH_CHAR_ASMTCATEGORY            133
//#define  TEH_CHAR_EVENTDATE               134
//#define  TEH_CHAR_SALESPRICE              135      // Confirmed
//#define  TEH_CHAR_CONFIRMATIONCODE        136

#define  TEH_MH_FEEPARCEL		            0
#define  TEH_MH_QUALCLS		               1
#define  TEH_MH_YEARBUILT		            2
#define  TEH_MH_BLDGGSIZE		            3
#define  TEH_MH_NUMBEDROOMS		         4
#define  TEH_MH_NUMFULLBATHS	            5
#define  TEH_MH_ASMT				            6
#define  TEH_MH_BLDGTYPE			         7

static XLAT_CODE  asBldgType[] =
{
   // Value, lookup code, value length
   "BL"," ", 2,                // 
   "C", " ", 1,                // 
   "S", " ", 1,                // 
   "W", " ", 1,                // 
   "",   "",  0
};

static XLAT_CODE  asBldgUsedFor[] = 
{
   // Value, lookup code, value length
   "A",  " ", 1,               // 
   "CLB"," ", 3,               // 
   "D",  " ", 1,               // 
   "MHF"," ", 3,               // 
   "O",  " ", 1,               // 
   "R",  " ", 1,               // 
   "S",  " ", 1,               // 
   "T",  " ", 1,               // 
   "W",  " ", 1,               // 
   "",   "",  0
};

static XLAT_CODE  asCond[] =
{
   // Value, lookup code, value length
   "AV", "A", 2, 
   "E",  "E", 1, 
   "F",  "F", 1,
   "GD", "G", 2, 
   "P",  "P", 1,  
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "1", "L", 1,               // Masonary
   "2", "Z", 1,               // Zero Clearance
   "3", "W", 1,               // Wood Stove
   "4", "S", 1,               // Pellet Stove
   "5", "O", 1,               // Other
   "G", "O", 1,               // 
   "O", "O", 1,               // 
   "P", "O", 1,               // 
   "W", "O", 1,               // 
   "",   "", 0
};

static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "1",  "Z", 1,     // Central
   "2",  "X", 1,     // ?
   "3",  "X", 1,     // ?
   "8",  "X", 1,     // ?
   "9",  "X", 1,     // ?
   "B",  "Z", 1,     // ?
   "C",  "Z", 1,     // Central
   "D ", "D", 1,     // Double wall
   "E",  "Z", 1,     // ?
   "F ", "C", 1,     // Floor
   "G ", "L", 1,     // 
   "O ", "X", 1,     // Other
   "P",  "X", 1,     // ?
   "S ", "X", 1,     // ?
   "V",  "X", 1,     // ?
   "W ", "D", 1,     // Wall
   "X",  "X", 1,     // ?
   "Y ", "Y", 1,     // Yes, heated
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "1", "P", 1,     	// 
   "3", "P", 1,     	// 
   "DB","P", 2,		// DoughBoy
   "FG","F", 2,     	// Fiberglass
   "GU","G", 2,     	// Gunite
   "",   "", 0
};

static XLAT_CODE  asParkType[] =
{
   // Value, lookup code, value length
   "A", "I", 1,              // Attached
   "D", "L", 1,              // Detached
   "C", "C", 1,              // Carport
   "S", "W", 1,              // Space
   "Y", "4", 1,              // Others
   "N", "H", 1,              // None
   "",  "",  0
};

static XLAT_CODE  asSewer[] =
{
   // Value, lookup code, value length 
   "P", "P", 1,               // Public
   "S", "S", 1,               // Septic
   "",   "",  0
};

static XLAT_CODE  asWaterSrc[] = 
{
   // Value, lookup code, value length
   "C", "P", 1,               // Community
   "W", "W", 1,               // Well
   "M", "Y", 1,               // Other
   "",  "",  0
};

IDX_TBL5 TEH_DocCode[] =
{  // DocCode, Index, Non-sale, DocCodelen, Indexlen
   "00", "74", 'Y', 2, 2,     // NON-REAPPRAISABLE EVENT
   "01", "1 ", 'N', 2, 2,     // SALE - 100% TRANSFER
   "02", "75", 'Y', 2, 2,     // Parent-Child Transfer
   "04", "57", 'N', 2, 2,     // Partial Interest Transfer
   "05", "1 ", 'N', 2, 2,     // Final Value-Previously Partial
   "13", "74", 'N', 2, 2,     // New Mobile Home
   "","",0,0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 TEH_Exemption[] = 
{
   "E01", "H", 3,1,
   "E02", "X", 3,1,     // BOE DAV DUMMY EXEMPTION
   "E04", "X", 3,1,     // PENALTY SEC 504
   "E20", "S", 3,1,     // SCHOOL 
   "E21", "S", 3,1,     // SCHOOL UNSECURED
   "E40", "W", 3,1,     // WELFARE
   "E41", "W", 3,1,     // WELFARE UNSECURED
   "E42", "W", 3,1,     // WELFARE PARTIAL WITHOUT PP
   "E43", "W", 3,1,     // WELFARE PARTIAL PP ONLY
   "E50", "R", 3,1,     // RELIGIOUS
   "E51", "R", 3,1,     // RELIGIOUS UNSECURED
   "E52", "R", 3,1,     // RELIGIOUS PARTIAL LAND
   "E53", "R", 3,1,     // RELIGIOUS PARTIAL IMPS
   "E54", "R", 3,1,     // RELIGIOUS PP & FIX
   "E60", "M", 3,1,     // MUSEUM
   "E61", "C", 3,1,     // CHURCH UNSECURED
   "E70", "E", 3,1,     // CEMETERY
   "E80", "D", 3,1,     // DISABLED VETERAN BASIC
   "E81", "V", 3,1,     // VETERANS EXEMPTION EXPANDED
   "E82", "D", 3,1,     // DISABLED VETERAN PARTIAL OWNERSHIP
   "E90", "C", 3,1,     // CHURCH
   "E91", "X", 3,1,     // AIRCRAFT HISTORICAL
   "E92", "X", 3,1,     // AIRCRAFT HISTORICAL LATE
   "E98", "X", 3,1,     // OTHER
   "","",0,0
};

#endif // !defined(AFX_MERGETEH_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_)
