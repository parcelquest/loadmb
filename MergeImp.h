
#if !defined(AFX_LOADMB_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_)
#define AFX_LOADMB_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"
#include "hlAdo.h"

#define  OPEN_ERR       0xF0000001
#define  READ_ERR       0xF0000002
#define  WRITE_ERR      0xF0000004

#define  UPDATE_R01     0
#define  CREATE_R01     1
#define  CREATE_LIEN    2
#define  CLEAR_R01      4

#define  LOAD_LIEN      0x10000000
#define  LOAD_UPDT      0x01000000
#define  LOAD_GRGR      0x00100000
#define  LOAD_SALE      0x00010000
#define  LOAD_ATTR      0x00001000
#define  LOAD_ASSR      0x00000100
#define  LOAD_DAILY     0x00000010
#define  UPDT_TAX       0x00000001

#define  EXTR_LIEN      0x20000000
#define  EXTR_CSAL      0x02000000
#define  EXTR_VALUE     0x00200000
#define  EXTR_SALE      0x00020000
#define  EXTR_ATTR      0x00002000
#define  EXTR_REGN      0x00000200
#define  EXTR_PRP8      0x00000020

#define  MERG_LIEN      0x40000000
#define  MERG_CSAL      0x04000000
#define  MERG_GRGR      0x00400000
#define  MERG_GISA      0x00040000
#define  MERG_ATTR      0x00004000
#define  MERG_SADR      0x00000400

#define  UPDT_XSAL      0x00080000
#define  UPDT_ASSR      0x00000800
#define  FIX_CSDOC      0x00000080
#define  FIX_CSTYP      0x00000008

FLD_IDX tblCharVal[] = 
{
   "AttachGarageSF "       ,6, 10, 20, SCROFF_GARSQFT,
   "AvgWallHeight  "       ,0, 10, 20, SCROFF_NOTUSED,
   "BathRooms      "       ,1, 6,  20, SCROFF_FBATHS,
   "Bay            "       ,0, 3,  20, SCROFF_NOTUSED,
   "BldgDesignedFor"       ,0, 10, 20, SCROFF_NOTUSED,
   "BldgFootprintSF"       ,1, 10, 20, SCROFF_NOTUSED,
   "Builder        "       ,0, 7,  20, SCROFF_NOTUSED,
   "BuildingCode   "       ,5, 10, 20, SCROFF_BLDGCODE,
   "BuildingDesign "       ,0, 10, 20, SCROFF_NOTUSED,
   "BuildingSize   "       ,1, 10, 20, SCROFF_BLDGSQFT,
   "BuildingType   "       ,0, 10, 20, SCROFF_STYLE,
   "BuildingUsedFor"       ,0, 10, 20, SCROFF_NOTUSED,
   "CarportSF      "       ,6, 4,  20, SCROFF_CARPSQFT,
   "CeilingHeight  "       ,1, 4,  20, SCROFF_NOTUSED,
   "Condition      "       ,0, 5,  20, SCROFF_CONDITION,
   "Construction   "       ,5, 5,  20, SCROFF_CONSTTYPE,
   "Cooling        "       ,5, 5,  20, SCROFF_COOLING,
   "DeckSF         "       ,1, 5,  20, SCROFF_DECKSQFT,
   "DetachGarageSF "       ,6, 5,  20, SCROFF_DETGARSQFT,
   "Dock           "       ,0, 4,  20, SCROFF_NOTUSED,
   "EffectiveYear  "       ,1, 4,  20, SCROFF_YREFF,
   "Elevator       "       ,0, 4,  20, SCROFF_ELEVATOR,
   "Escalator      "       ,0, 4,  20, SCROFF_ESCALATOR,
   "ExteriorType   "       ,0, 4,  20, SCROFF_NOTUSED,
   "Field1         "       ,0, 6,  20, SCROFF_NOTUSED,
   "Field2         "       ,0, 6,  20, SCROFF_NOTUSED,
   "Field3         "       ,0, 6,  20, SCROFF_NOTUSED,
   "Field4         "       ,0, 6,  20, SCROFF_NOTUSED,
   "Field5         "       ,0, 6,  20, SCROFF_NOTUSED,
   "Field6         "       ,0, 6,  20, SCROFF_NOTUSED,
   "Field14        "       ,0, 6,  20, SCROFF_NOTUSED,
   "Field15        "       ,0, 6,  20, SCROFF_NOTUSED,
   "Field17        "       ,0, 6,  20, SCROFF_NOTUSED,
   "Field19        "       ,0, 6,  20, SCROFF_NOTUSED,
   "Field20        "       ,0, 6,  20, SCROFF_NOTUSED,
   "Fireplace      "       ,5, 6,  20, SCROFF_FIREPLACE,
   "FireSprinklers "       ,0, 6,  20, SCROFF_NOTUSED,
   "Foundation     "       ,1, 6,  20, SCROFF_FNDNTYPE,
   "Garage         "       ,1, 6,  20, SCROFF_PARKSPACE, // may contain "Enc1"
   "Heating        "       ,5, 6,  20, SCROFF_HEATING,
   "Insulated      "       ,5, 6,  20, SCROFF_NOTUSED,
   "Lighting       "       ,0, 6,  20, SCROFF_NOTUSED,
   "MezzanineSF    "       ,0, 6,  20, SCROFF_NOTUSED,
   "ModelDesc      "       ,0, 6,  20, SCROFF_NOTUSED,
   "NetLeasableSF  "       ,0, 6,  20, SCROFF_NOTUSED,
   "NonConditionSF "       ,0, 6,  20, SCROFF_NOTUSED,
   "OfficeSpaceSF  "       ,1, 6,  20, SCROFF_NOTUSED,
   "PatioSF        "       ,1, 6,  20, SCROFF_PATIOSQFT,
   "PerimeterLF    "       ,1, 6,  20, SCROFF_NOTUSED,
   "Plumbing       "       ,0, 6,  20, SCROFF_NOTUSED,
   "Primary        "       ,0, 6,  20, SCROFF_NOTUSED,
   "QualityClass   "       ,0, 8,  20, SCROFF_QUALCLASS,
   "Refrigeration  "       ,0, 6,  20, SCROFF_NOTUSED,
   "RollUpDoor     "       ,0, 6,  20, SCROFF_NOTUSED,
   "RoofCover      "       ,0, 6,  20, SCROFF_ROOFMAT,
   "RoofType       "       ,5, 6,  20, SCROFF_ROOFTYPE,
   "SitusSeq       "       ,0, 6,  20, SCROFF_NOTUSED,
   "Solar          "       ,0, 5,  20, SCROFF_NOTUSED,
   "StoriesCnt     "       ,2, 5,  20, SCROFF_STORIES,
   "StructuralFloor"       ,0, 14, 20, SCROFF_NOTUSED,
   "StructuralFrame"       ,0, 14, 20, SCROFF_NOTUSED,
   "UnfinAreasSF   "       ,0, 6,  20, SCROFF_NOTUSED,
   "UnitsCnt       "       ,1, 6,  20, SCROFF_UNITS,
   "WindowPane     "       ,5, 8,  20, SCROFF_NOTUSED,
   "WindowType     "       ,0, 8,  20, SCROFF_NOTUSED,
   "YearBuilt      "       ,1, 5,  20, SCROFF_YRBLT,
   "",0,0,0,0
};

// PC_BldgDetail.txt
#define  IMP_BLDG_FEEPARCEL               0
#define  IMP_BLDG_DOCNUM                  1
#define  IMP_BLDG_CATTYPE                 2
#define  IMP_BLDG_SEQNUM                  3
#define  IMP_BLDG_NAME                    4
#define  IMP_BLDG_VALUE                   5
#define  IMP_BLDG_DTS                     6

// LDR 2020-2021
// PQ_Asmt
#define  IMP_ASMT_FEEPARCEL               0
#define  IMP_ASMT_APN                     1
#define  IMP_ASMT_ASMTCATEGORY            2
#define  IMP_ASMT_CREATINGDOCNUM          3
#define  IMP_ASMT_CREATINGDOCDATE         4
#define  IMP_ASMT_STATUS                  5
#define  IMP_ASMT_STATUSDATE              6
#define  IMP_ASMT_KILLINGDOCNUM           7
#define  IMP_ASMT_KILLINGDOCDATE          8
#define  IMP_ASMT_INACTIVATEDROLLYEAR     9
#define  IMP_ASMT_TRA                     10
#define  IMP_ASMT_LEGAL                   11
#define  IMP_ASMT_TAXABILITY1             12
#define  IMP_ASMT_TAXABILITY2             13
#define  IMP_ASMT_TAXABILITY3             14
#define  IMP_ASMT_BASEDATE                15
#define  IMP_ASMT_ZONING1                 16
#define  IMP_ASMT_ZONING2                 17
#define  IMP_ASMT_LANDUSE1                18
#define  IMP_ASMT_LANDUSE2                19
#define  IMP_ASMT_DWELLINGUNITS           20
#define  IMP_ASMT_NEIGHBORHOODCODE        21
#define  IMP_ASMT_ACRES                   22
#define  IMP_ASMT_SIZETYPE                23
#define  IMP_ASMT_PPREPORTCODES           24
#define  IMP_ASMT_CURRENTDOCNUM           25
#define  IMP_ASMT_CURRENTDOCDATE          26
#define  IMP_ASMT_SUPLCNT                 27
#define  IMP_ASMT_BONDSOURCE              28
#define  IMP_ASMT_ISBONDS                 29
#define  IMP_ASMT_ISMULTSITUS             30
#define  IMP_ASMT_ISAPPLYINFLATION        31
#define  IMP_ASMT_ISCPISUPPRESSEDUSECODE  32
#define  IMP_ASMT_ISSPLITPENDING          33
#define  IMP_ASMT_FLAG1                   34
#define  IMP_ASMT_FLAG2                   35
#define  IMP_ASMT_ISNOTES                 36
#define  IMP_ASMT_ISAPPEALPENDING         37
#define  IMP_ASMT_ISETAL                  38
#define  IMP_ASMT_DTS                     39
#define  IMP_ASMT_USERID                  40
#define  IMP_ASMT_TAXABILITYFULL          41
#define  IMP_ASMT_TAXABILITY4             42
#define  IMP_ASMT_SECONDSTATUS            43
#define  IMP_ASMT_FLDS                    44

// PQ_Building.txt
#define  IMP_BG_FEEPARCEL                 0
#define  IMP_BG_DOCUMENTNUM               1
#define  IMP_BG_LANDUSECATEGORY           2
#define  IMP_BG_BUILDINGSEQNUM            3
#define  IMP_BG_BUILDINGCODE              4
#define  IMP_BG_BUILDINGTYPE              5
#define  IMP_BG_QUALITYCLASS              6
#define  IMP_BG_EFFECTIVEYEAR             7
#define  IMP_BG_YEARBUILT                 8
#define  IMP_BG_BUILDINGSIZE              9
#define  IMP_BG_SQFTUNFINISHED            10
#define  IMP_BG_SQFTGARAGE                11
#define  IMP_BG_HEATING                   12
#define  IMP_BG_COOLING                   13
#define  IMP_BG_IMPERVIOUSSURFACE         14
#define  IMP_BG_NUMUNITSCOMMERCIAL        15
#define  IMP_BG_NUMUNITSINDUSTRIAL        16
#define  IMP_BG_NUMUNITSRESIDENTIAL       17
#define  IMP_BG_ROOFCOVER                 18
#define  IMP_BG_ROOFSTYLE                 19
#define  IMP_BG_ROOFPITCH                 20
#define  IMP_BG_FOUNDATION                21
#define  IMP_BG_EXTERIOR                  22
#define  IMP_BG_FLOORING                  23
#define  IMP_BG_NUMFLOORS                 24
#define  IMP_BG_PLUMBINGCONDITION         25
#define  IMP_BG_BLDGWIRING                26
#define  IMP_BG_BLDGUSEDFOR               27
#define  IMP_BG_BLDGDESIGNEDFOR           28
#define  IMP_BG_CONDITION                 29
#define  IMP_BG_EXTERIORFRONT             30
#define  IMP_BG_EXTERIORLEFT              31
#define  IMP_BG_EXTERIORRIGHT             32
#define  IMP_BG_EXTERIORBACK              33
#define  IMP_BG_HASELEVATOR               34
#define  IMP_BG_HASFIRESPRINKLERS         35
#define  IMP_BG_ROOFHASRAFTERS            36
#define  IMP_BG_FOUNDATIONREINFORCED      37
#define  IMP_BG_FOUNDATIONONHILLSIDE      38
#define  IMP_BG_STRUCTUREFRAMED           39
#define  IMP_BG_STRUCTURESHEATHED         40
#define  IMP_BG_STRUCTUREHASFLOORJOISTS   41
#define  IMP_BG_STRUCTUREHASSUBFLOOR      42
#define  IMP_BG_STRUCTUREHASCONCRETEFLOOR 43
#define  IMP_BG_HASSINK                   44
#define  IMP_BG_PATIO                     45
#define  IMP_BG_DECKS                     46
#define  IMP_BG_HASLAUNDRY                47
#define  IMP_BG_FRAME                     48
#define  IMP_BG_ROOFSTRUCTURE             49
#define  IMP_BG_JOISTS                    50
#define  IMP_BG_FLOORSTRUCTURE            51
#define  IMP_BG_COMMENTEDFIELDSFLAGS      52
#define  IMP_BG_BUILDER                   53
#define  IMP_BG_MODELDESCRIPTION          54
#define  IMP_BG_HEATINGSOURCE             55
#define  IMP_BG_COOLINGSOURCE             56
#define  IMP_BG_NETLEASABLEAREA           57
#define  IMP_BG_GROSSLEASABLEAREA         58
#define  IMP_BG_SQFTOFFICES               59
#define  IMP_BG_WALLHEIGHT                60
#define  IMP_BG_RATIO1                    61
#define  IMP_BG_RATIO2                    62
#define  IMP_BG_GARAGETYPECODE            63
#define  IMP_BG_CONFORMINGUSE             64
#define  IMP_BG_DTS                       65
#define  IMP_BG_USERID                    66

// PQ_Exemption.txt
#define  IMP_EXE_APN                      0
#define  IMP_EXE_EXEMPTIONCODE            1
#define  IMP_EXE_MAXIMUMAMT               2
#define  IMP_EXE_EXEMPTIONPCT             3
#define  IMP_EXE_ISHOMEOWNERS             4
#define  IMP_EXE_DTS                      5
#define  IMP_EXE_USERID                   6

// PQ_Land.txt
#define  IMP_LAND_FEEPARCEL               0
#define  IMP_LAND_DOCUMENTNUM             1
#define  IMP_LAND_LANDUSECATEGORY         2
#define  IMP_LAND_ZONING1                 3
#define  IMP_LAND_LANDUSE1                4
#define  IMP_LAND_ACRES                   5
#define  IMP_LAND_NUMUNITS                6
#define  IMP_LAND_NUMBUILDINGS            7
#define  IMP_LAND_NUMPOOLS                8
#define  IMP_LAND_RESEVOIRFRONTAGE        9
#define  IMP_LAND_STREAMFRONTAGE          10
#define  IMP_LAND_STREAMTHRUPROPERTY      11
#define  IMP_LAND_STREETFRONTAGE          12
#define  IMP_LAND_STREETTHRUPROPERTY      13
#define  IMP_LAND_PLANTEDACRES            14
#define  IMP_LAND_SUBJECTTOPOGRAPHY       15
#define  IMP_LAND_HASSIDEWALK             16
#define  IMP_LAND_HASCURB                 17
#define  IMP_LAND_HASGUTTER               18
#define  IMP_LAND_HASPAVEMENT             19
#define  IMP_LAND_HASSEPTIC               20
#define  IMP_LAND_HASSEWER                21
#define  IMP_LAND_HASWELL                 22
#define  IMP_LAND_HASSTREETLIGHTS         23
#define  IMP_LAND_UTILITIESCODE           24
#define  IMP_LAND_HASVINEYARD             25
#define  IMP_LAND_HASORCHARD              26
#define  IMP_LAND_HASFIELDCROPS           27
#define  IMP_LAND_WATERSOURCEDOMESTIC     28
#define  IMP_LAND_WATERSOURCEIRRIGATION   29
#define  IMP_LAND_WATERSUPPLIER           30
#define  IMP_LAND_WATERQTYDOMESTIC        31
#define  IMP_LAND_WATERQTYIRRIGATION      32
#define  IMP_LAND_WATERQUALITYDOMESTIC    33
#define  IMP_LAND_WATERQUALITYIRRIGATION  34
#define  IMP_LAND_WELLDEPTH               35
#define  IMP_LAND_WATERRIGHTSTOLAND       36
#define  IMP_LAND_NEIGHBORTOPOGRAPHY      37
#define  IMP_LAND_PRODUCTS                38
#define  IMP_LAND_PRODUCTIVECAPACITY      39
#define  IMP_LAND_PERIODOFOPERATION       40
#define  IMP_LAND_INDUSTRYTYPE            41
#define  IMP_LAND_WELLLIFT                42
#define  IMP_LAND_HASGRAZING              43
#define  IMP_LAND_HASELECTRIC             44
#define  IMP_LAND_HASGAS                  45
#define  IMP_LAND_HASPHONE                46
#define  IMP_LAND_HASHIGHWAY              47
#define  IMP_LAND_HASCOUNTYROAD           48
#define  IMP_LAND_HASPRIVATEROAD          49
#define  IMP_LAND_ROADTYPE                50
#define  IMP_LAND_ROADCONDITION           51
#define  IMP_LAND_COMMUNITY               52
#define  IMP_LAND_SOILHASEROSION          53
#define  IMP_LAND_SOILTYPE                54
#define  IMP_LAND_VIEWCODE                55
#define  IMP_LAND_VEGETATION              56
#define  IMP_LAND_LANDSCAPEFRONT          57
#define  IMP_LAND_LANDSCAPEBACK           58
#define  IMP_LAND_COMMENTEDFIELDSFLAGS    59
#define  IMP_LAND_VINESAGE                60
#define  IMP_LAND_VINESVARIETY            61
#define  IMP_LAND_GEOCODE                 62
#define  IMP_LAND_SIZETYPE                63
#define  IMP_LAND_PROBLEMCODE             64
#define  IMP_LAND_ISUNSECUREDBUILDING     65
#define  IMP_LAND_COMMENTS                66
#define  IMP_LAND_SUBDIVISIONNAME         67
#define  IMP_LAND_ACRESUNUSEABLE          68
#define  IMP_LAND_NUMPONDS                69
#define  IMP_LAND_NUMPARKINGSPACES        70
#define  IMP_LAND_ACCESSCODE              71
#define  IMP_LAND_LANDSQFT                72
#define  IMP_LAND_FRONTAGECODE            73
#define  IMP_LAND_MAPREFERENCE            74
#define  IMP_LAND_PROPERTYCONDITIONCODE   75
#define  IMP_LAND_HOMESITES               76
#define  IMP_LAND_EXISTSFIXTURES          77
#define  IMP_LAND_UNUSABLESQFT            78
#define  IMP_LAND_SEWERSOURCECODE         79
#define  IMP_LAND_DTS                     80
#define  IMP_LAND_USERID                  81
#define  IMP_LAND_FLDS                    82

// PQ_MobileHomeAttribute.txt
#define  IMP_MA_APN                       0
#define  IMP_MA_DECALNUM                  1
#define  IMP_MA_ASMTYEAR                  2
#define  IMP_MA_DOCUMENTNUM               3
#define  IMP_MA_SERIAL1                   4
#define  IMP_MA_SERIAL2                   5
#define  IMP_MA_SERIAL3                   6
#define  IMP_MA_SALEDATE                  7
#define  IMP_MA_EVENTDATE                 8
#define  IMP_MA_EVENTYEAR                 9
#define  IMP_MA_SALEPRICE                 10
#define  IMP_MA_ISVALUEDIFFERENT          11
#define  IMP_MA_ADJUSTEDSALESVALUE        12
#define  IMP_MA_BASEYEARVALUE             13
#define  IMP_MA_BASEYEAR                  14
#define  IMP_MA_SALESVALADJEXPLAN         15
#define  IMP_MA_AH531VALUE                16
#define  IMP_MA_KELLYORNADAVALUE          17
#define  IMP_MA_TRUEPROP8VALUE            18
#define  IMP_MA_CALCULATEDVALUE           19
#define  IMP_MA_CALCVALUEADDFACTOR        20
#define  IMP_MA_ENROLLEDVALUE             21
#define  IMP_MA_ENROLLMENTTYPE            22
#define  IMP_MA_FACTORSCHEDULEID          23
#define  IMP_MA_BASEYEARFACTOREDVALUE     24
#define  IMP_MA_AMENITIESVALUE            25
#define  IMP_MA_AMENITIESEVENTDATE        26
#define  IMP_MA_EFFECTIVEYEARBUILT        27
#define  IMP_MA_EFFECTIVEAGE              28
#define  IMP_MA_LOCKENRVALUE              29
#define  IMP_MA_ROLLYEARVALUED            30
#define  IMP_MA_YEARBUILT                 31
#define  IMP_MA_MAKE                      32
#define  IMP_MA_MODEL                     33
#define  IMP_MA_QUALITYCLASS              34
#define  IMP_MA_OVERALLWIDTH              35
#define  IMP_MA_OVERALLLENGTH             36
#define  IMP_MA_SEC1WIDTH                 37
#define  IMP_MA_SEC1LENGTH                38
#define  IMP_MA_SEC2WIDTH                 39
#define  IMP_MA_SEC2LENGTH                40
#define  IMP_MA_SEC3WIDTH                 41
#define  IMP_MA_SEC3LENGTH                42
#define  IMP_MA_SEC4WIDTH                 43
#define  IMP_MA_SEC4LENGTH                44
#define  IMP_MA_SEC5WIDTH                 45
#define  IMP_MA_SEC5LENGTH                46
#define  IMP_MA_SEC6WIDTH                 47
#define  IMP_MA_SEC6LENGTH                48
#define  IMP_MA_CONDITIONADJUSTMENT       49
#define  IMP_MA_SIZESQFT                  50
#define  IMP_MA_TOTALROOMS                51
#define  IMP_MA_NUMBEDROOMS               52
#define  IMP_MA_NUMBATHROOMS              53
#define  IMP_MA_BATHFIXTURESCODE          54
#define  IMP_MA_CARPETINGCODE             55
#define  IMP_MA_ROOFCODE                  56
#define  IMP_MA_SIDINGCODE                57
#define  IMP_MA_AWNINGCODE                58
#define  IMP_MA_DECKCODE                  59
#define  IMP_MA_ELEVPORCHCODE             60
#define  IMP_MA_ENCLOSEDROOMCODE          61
#define  IMP_MA_CARPORTCODE               62
#define  IMP_MA_GARAGECODE                63
#define  IMP_MA_HEATINGCODE               64
#define  IMP_MA_COOLINGCODE               65
#define  IMP_MA_FIREPLACECODE             66
#define  IMP_MA_WETBARCODE                67
#define  IMP_MA_SKIRTINGCODE              68
#define  IMP_MA_STORAGESHEDCODE           69
#define  IMP_MA_PATIOCODE                 70
#define  IMP_MA_HASDISHWASHER             71
#define  IMP_MA_HASGARBAGEDISPOSAL        72
#define  IMP_MA_HASMICROWAVE              73
#define  IMP_MA_HASTRASHCOMPACTOR         74
#define  IMP_MA_IMPRSQFT                  75
#define  IMP_MA_CARPORTSQFT               76
#define  IMP_MA_GARAGESQFT                77
#define  IMP_MA_PORCHSQFT                 78
#define  IMP_MA_SHEDSSQFT                 79
#define  IMP_MA_PERIMETERLINEARFT         80
#define  IMP_MA_SKIRTINGLINEARFT          81
#define  IMP_MA_AMENITIES                 82
#define  IMP_MA_LASTTAXCLEARANCE          83
#define  IMP_MA_COMMENTS                  84
#define  IMP_MA_VALUEOFADDITIVES          85
#define  IMP_MA_COACHVALUEPERSQFT         86
#define  IMP_MA_LICENSE                   87
#define  IMP_MA_XREFMAPBOOK               88
#define  IMP_MA_UNITID                    89
#define  IMP_MA_DTS                       90
#define  IMP_MA_USERID                    91
#define  IMP_MA_ISLICENSED                92
#define  IMP_MA_VALUEOFACCESSORIES        93
#define  IMP_MA_FLDS                      94


// PQ_Ownership.txt
#define  IMP_OWNR_APN                     0
#define  IMP_OWNR_SETNUM                  1
#define  IMP_OWNR_SEQUENCE                2
#define  IMP_OWNR_SUBSEQUENCE             3
#define  IMP_OWNR_ISPRIMARY               4
#define  IMP_OWNR_TITLEATTACHED           5
#define  IMP_OWNR_OWNERFIRST              6
#define  IMP_OWNR_OWNERMIDDLE             7
#define  IMP_OWNR_OWNERLAST               8
#define  IMP_OWNR_OWNERSHIPPCT            9
#define  IMP_OWNR_GRANTINGDOCNUM          10
#define  IMP_OWNR_TITLETYPE               11
#define  IMP_OWNR_RTCODESECTION           12
#define  IMP_OWNR_NOTES                   13
#define  IMP_OWNR_ISPRIVATE               14
#define  IMP_OWNR_HWCODE                  15
#define  IMP_OWNR_DTS                     16
#define  IMP_OWNR_USERID                  17
#define  IMP_OWNR_OWNERSHIPID             18
#define  IMP_OWNR_ALIASID                 19
#define  IMP_OWNR_OWNERFULL               20

// PQ_Situs.txt
#define  IMP_SIT_APN                      0
#define  IMP_SIT_SEQUENCE                 1
#define  IMP_SIT_STATUS                   2
#define  IMP_SIT_STREET                   3
#define  IMP_SIT_STREETNUM                4
#define  IMP_SIT_STREETTYPE               5
#define  IMP_SIT_STREETDIRECTION          6
#define  IMP_SIT_SPACEAPT                 7
#define  IMP_SIT_COMMUNITY                8
#define  IMP_SIT_ZIP                      9
#define  IMP_SIT_FORMATTEDSITUS1          10
#define  IMP_SIT_FORMATTEDSITUS2          11
#define  IMP_SIT_ISADDRESSCONFIDENTIAL    12
#define  IMP_SIT_ISDIRECTIONAFTERSTREET   13
#define  IMP_SIT_DTS                      14
#define  IMP_SIT_USERID                   15

// PQ_TransferHistory.txt
#define  IMP_SALE_ASMT                    0
#define  IMP_SALE_DOCNUM                  1
#define  IMP_SALE_DOCDATE                 2
#define  IMP_SALE_DOCCODE                 3
#define  IMP_SALE_SELLER                  4
#define  IMP_SALE_BUYER                   5
#define  IMP_SALE_ACRES                   6
#define  IMP_SALE_SIZETYPE                7
#define  IMP_SALE_CONFIRMEDSALESPRICE     8
#define  IMP_SALE_TAXAMT                  9
#define  IMP_SALE_GROUPSALE               10
#define  IMP_SALE_GROUPASMT               11
#define  IMP_SALE_LANDUSE1                12
#define  IMP_SALE_LANDUSE2                13
#define  IMP_SALE_DEEDAMT1                14
#define  IMP_SALE_DEEDAMT2                15
#define  IMP_SALE_XFERTYPE                16
#define  IMP_SALE_SALESLTRRETURNEDDATE    17
#define  IMP_SALE_SALESPRICECODE          18
#define  IMP_SALE_SALESPRICESTATUS        19
#define  IMP_SALE_PCTDOWNPAYMENT          20
#define  IMP_SALE_FINANCINGCODE           21
#define  IMP_SALE_SECONDARYFINANCE        22
#define  IMP_SALE_FLAG1                   23
#define  IMP_SALE_FLAG2                   24
#define  IMP_SALE_DWELLINGUNITS           25
#define  IMP_SALE_ADJREASONCODE           26
#define  IMP_SALE_CONFIRMATIONCODE        27
#define  IMP_SALE_DTS                     28
#define  IMP_SALE_USERID                  29
#define  IMP_SALE_SALESPRICEDTT           30

// PQ_ValueSet.txt
#define  IMP_VAL_APN                      0
#define  IMP_VAL_VALUESETTYPE             1
#define  IMP_VAL_LAND                     2
#define  IMP_VAL_LANDAPRDATE              3
#define  IMP_VAL_HOMESITE                 4
#define  IMP_VAL_HOMESITEAPRDATE          5
#define  IMP_VAL_STRUCTURE                6
#define  IMP_VAL_STRUCTUREAPRDATE         7
#define  IMP_VAL_GROWING                  8
#define  IMP_VAL_GROWINGAPRDATE           9
#define  IMP_VAL_FIXTURES                 10    // No data
#define  IMP_VAL_FIXTURESREALPROPERTY     11    // No data
#define  IMP_VAL_PPBUSINESS               12    // PPBUSINESS & PPMH are mutually exclusive
#define  IMP_VAL_PPMH                     13
#define  IMP_VAL_PPPENALTYINCLUDED        14
#define  IMP_VAL_CURRENTVALUETYPE         15
#define  IMP_VAL_PPMHAPRDATE              16
#define  IMP_VAL_DTS                      17
#define  IMP_VAL_USERID                   18

// PQ_Unit.txt
#define  IMP_UNIT_FEEPARCEL               0
#define  IMP_UNIT_DOCUMENTNUM             1
#define  IMP_UNIT_LANDUSECATEGORY         2
#define  IMP_UNIT_BUILDINGSEQNUM          3
#define  IMP_UNIT_UNITSEQNUM              4
#define  IMP_UNIT_UNITCODE                5
#define  IMP_UNIT_UNITTYPE                6
#define  IMP_UNIT_NUMBEDROOMS             7
#define  IMP_UNIT_NUMFULLBATHS            8
#define  IMP_UNIT_NUMHALFBATHS            9
#define  IMP_UNIT_TOTALROOMS              10
#define  IMP_UNIT_NUMFIREPLACES           11
#define  IMP_UNIT_UNITSIZE                12
#define  IMP_UNIT_PLUMBING                13
#define  IMP_UNIT_ELECTRICITY             14
#define  IMP_UNIT_NATURALGAS              15
#define  IMP_UNIT_FURNISHED               16
#define  IMP_UNIT_HASRANGE                17
#define  IMP_UNIT_HASDISPOSAL             18
#define  IMP_UNIT_HASDISHWASHER           19
#define  IMP_UNIT_HASINTERCOM             20
#define  IMP_UNIT_HASTRASHCOMPACTOR       21
#define  IMP_UNIT_HASMICROWAVE            22
#define  IMP_UNIT_HASCARPORT              23
#define  IMP_UNIT_NUMSKYLIGHTS            24
#define  IMP_UNIT_NUMDOORS                25
#define  IMP_UNIT_NUMCUPBOARDS            26
#define  IMP_UNIT_NUMCLOSETS              27
#define  IMP_UNIT_NUMLIGHTFIXTURES        28
#define  IMP_UNIT_HASWATERHEATER          29
#define  IMP_UNIT_HASLAUNDRY              30
#define  IMP_UNIT_WINDOWS                 31
#define  IMP_UNIT_WINDOWSCASEMENT         32
#define  IMP_UNIT_FANORHOOD               33
#define  IMP_UNIT_OVENS                   34
#define  IMP_UNIT_NUMDISHWASHERS          35
#define  IMP_UNIT_NUMFLOORS               36
#define  IMP_UNIT_COMMENTEDFIELDSFLAGS    37
#define  IMP_UNIT_STUDIO                  38
#define  IMP_UNIT_OTHERUNITS              39
#define  IMP_UNIT_FIREPLACETYPECODE       40
#define  IMP_UNIT_UNITTYPECOUNT           41
#define  IMP_UNIT_DTS                     42
#define  IMP_UNIT_USERID                  43

// LDR - 2019-2020 Secured Roll.txt
#define  IMP_ROLL_FEEPARCEL      0
#define  IMP_ROLL_APN            1
#define  IMP_ROLL_TRA            2
#define  IMP_ROLL_USECODE1       3
#define  IMP_ROLL_USECODE2       4
#define  IMP_ROLL_ACRES          5
#define  IMP_ROLL_OWNER          6
#define  IMP_ROLL_CAREOF         7
#define  IMP_ROLL_DBA            8
#define  IMP_ROLL_M_ADDR         9
#define  IMP_ROLL_M_CITY         10
#define  IMP_ROLL_M_STATE        11
#define  IMP_ROLL_M_ZIP          12
#define  IMP_ROLL_M_ADDR1        13
#define  IMP_ROLL_M_ADDR2        14
#define  IMP_ROLL_M_ADDR3        15
#define  IMP_ROLL_M_ADDR4        16
#define  IMP_ROLL_S_ADDR1        17
#define  IMP_ROLL_S_ADDR2        18
#define  IMP_ROLL_S_STRNUM       19
#define  IMP_ROLL_S_STREET       20
#define  IMP_ROLL_S_ZIP          21
#define  IMP_ROLL_LEGAL          22
#define  IMP_ROLL_LAND           23
#define  IMP_ROLL_IMPR           24
#define  IMP_ROLL_PPVALUE        25
#define  IMP_ROLL_GROWING        26
#define  IMP_ROLL_S_CITY         27
#define  IMP_ROLL_S_DIR          28
#define  IMP_ROLL_S_UNIT         29
#define  IMP_ROLL_STATUS         30
#define  IMP_ROLL_DOCNUM         31
#define  IMP_ROLL_DOCDATE        32
#define  IMP_ROLL_TAXABILITY     33
#define  IMP_ROLL_SUBDIV         34

// 2019-2020 Mobile Homes.txt
#define  IMP_MH_FEEPARCEL        0
#define  IMP_MH_APN              1
#define  IMP_MH_OWNER            2
#define  IMP_MH_M_ADDR           3
#define  IMP_MH_M_CITY           4
#define  IMP_MH_M_STATE          5
#define  IMP_MH_M_ZIP            6
#define  IMP_MH_S_ADDR1          7
#define  IMP_MH_S_ADDR2          8
#define  IMP_MH_LEGAL            9
#define  IMP_MH_STATUS           10
#define  IMP_MH_LAND             11
#define  IMP_MH_IMPR             12
#define  IMP_MH_PPMH             13
#define  IMP_MH_FLDS             14
                                 
// OLD LIEN?
#define  L_ASMT                        0
#define  L_TAXYEAR                     1
#define  L_ROLLCHGNUM                  2
#define  L_MAPCATEGORY                 3
#define  L_ROLLCATEGORY                4
#define  L_ROLLTYPE                    5
#define  L_DESTINATIONROLL             6
#define  L_INSTALLMENTS                7
#define  L_BILLTYPE                    8
#define  L_FEEPARCEL                   9
#define  L_ORIGINATINGASMT             10
#define  L_XREFASMT                    11
#define  L_STATUS                      12
#define  L_TRA                         13
#define  L_TAXABILITY                  14
#define  L_ACRES                       15
#define  L_SIZEACRESFTTYPE             16
#define  L_INTDATEFROM                 17
#define  L_INTDATEFROM2                18
#define  L_INTDATETHRU                 19
#define  L_INTERESTTYPE                20
#define  L_USECODE                     21
#define  L_CURRENTMARKETLANDVALUE      22
#define  L_CURRENTFIXEDIMPRVALUE       23
#define  L_CURRENTGROWINGIMPRVALUE     24
#define  L_CURRENTSTRUCTURALIMPRVALUE  25
#define  L_CURRENTPERSONALPROPVALUE    26
#define  L_CURRENTPERSONALPROPMHVALUE  27
#define  L_CURRENTNETVALUE             28
#define  L_BILLEDMARKETLANDVALUE       29
#define  L_BILLEDFIXEDIMPRVALUE        30
#define  L_BILLEDGROWINGIMPRVALUE      31
#define  L_BILLEDSTRUCTURALIMPRVALUE   32
#define  L_BILLEDPERSONALPROPVALUE     33
#define  L_BILLEDPERSONALPROPMHVALUE   34
#define  L_BILLEDNETVALUE              35
#define  L_OWNER                       36
#define  L_ASSESSEE                    37 
#define  L_MAILADDRESS1                38
#define  L_MAILADDRESS2                39
#define  L_MAILADDRESS3                40
#define  L_MAILADDRESS4                41
#define  L_ZIPMATCH                    42
#define  L_BARCODEZIP                  43
#define  L_EXEMPTIONCODE1              44
#define  L_EXEMPTIONAMT1               45
#define  L_EXEMPTIONCODE2              46
#define  L_EXEMPTIONAMT2               47
#define  L_EXEMPTIONCODE3              48
#define  L_EXEMPTIONAMT3               49
#define  L_BILLDATE                    50
#define  L_DUEDATE1                    51
#define  L_DUEDATE2                    52
#define  L_TAXAMT1                     53
#define  L_TAXAMT2                     54
#define  L_PENAMT1                     55
#define  L_PENAMT2                     56
#define  L_COST1                       57
#define  L_COST2                       58
#define  L_PENCHRGDATE1                59
#define  L_PENCHRGDATE2                60
#define  L_PAIDAMT1                    61
#define  L_PAIDAMT2                    62
#define  L_PAYMENTDATE1                63
#define  L_PAYMENTDATE2                64
#define  L_TRANSDATE1                  65
#define  L_TRANSDATE2                  66
#define  L_COLLECTIONNUM1              67
#define  L_COLLECTIONNUM2              68
#define  L_UNSDELINQPENAMTPAID1        69
#define  L_SECDELINQPENAMTPAID2        70
#define  L_TOTALFEESPAID1              71
#define  L_TOTALFEESPAID2              72
#define  L_TOTALFEES                   73
#define  L_PMTCNLDATE1                 74
#define  L_PMTCNLDATE2                 75
#define  L_TRANSFERDATE                76
#define  L_PRORATIONFACTOR             77
#define  L_PRORATIONPCT                78
#define  L_DAYSTOFISCALYEAREND         79
#define  L_DAYSOWNED                   80
#define  L_OWNFROM                     81
#define  L_OWNTHRU                     82
#define  L_SITUS1                      83
#define  L_SITUS2                      84
#define  L_ASMTROLLYEAR                85
#define  L_PARCELDESCRIPTION           86
#define  L_BILLCOMMENTSLINE1           87
#define  L_BILLCOMMENTSLINE2           88
#define  L_DEFAULTNUM                  89
#define  L_DEFAULTDATE                 90
#define  L_REDEEMEDDATE                91
#define  L_SECDELINQFISCALYEAR         92
#define  L_PRIORTAXPAID1               93
#define  L_PENINTERESTCODE             94
#define  L_FOURPAYPLANNUM              95
#define  L_EXISTSLIEN                  96
#define  L_EXISTSCORTAC                97
#define  L_EXISTSCOUNTYCORTAC          98
#define  L_EXISTSBANKRUPTCY            99
#define  L_EXISTSREFUND                100
#define  L_EXISTSDELINQUENTVESSEL      101
#define  L_EXISTSNOTES                 102
#define  L_EXISTSCRITICALNOTE          103
#define  L_EXISTSROLLCHG               104
#define  L_ISPENCHRGCANCELED1          105
#define  L_ISPENCHRGCANCELED2          106
#define  L_ISPERSONALPROPERTYPENALTY   107
#define  L_ISAGPRESERVE                108
#define  L_ISCARRYOVER1STPAID          109
#define  L_ISCARRYOVERRECORD           110
#define  L_ISFAILURETOFILE             111
#define  L_ISOWNERSHIPPENALTY          112
#define  L_ISELIGIBLEFOR4PAY           113
#define  L_ISNOTICE4SENT               114
#define  L_ISPARTPAY                   115
#define  L_ISFORMATTEDADDRESS          116
#define  L_ISADDRESSCONFIDENTIAL       117
#define  L_SUPLCNT                     118
#define  L_ORIGINALDUEDATE1            119
#define  L_ORIGINALDUEDATE2            120
#define  L_ISPEN1REFUND                121
#define  L_ISPEN2REFUND                122
#define  L_ISDELINQPENREFUND           123
#define  L_ISREFUNDAUTHORIZED          124
#define  L_ISNODISCHARGE               125
#define  L_ISAPPEALPENDING             126
#define  L_OTHERFEESPAID               127
#define  L_FOURPAYREASON               128
#define  L_DATEDISCHARGED              129
#define  L_ISONLYFIRSTPAID             130
#define  L_ISALLPAID                   131
#define  L_DTS                         132
#define  L_USERID                      133

// LDR - 2017-2018 Secured Roll.txt
//#define  IMP_ROLL_FEEPARCEL      0
//#define  IMP_ROLL_APN            1
//#define  IMP_ROLL_TRA            2
//#define  IMP_ROLL_USECODE1       3
//#define  IMP_ROLL_USECODE2       4
//#define  IMP_ROLL_ACRES          5
//#define  IMP_ROLL_OWNER          6
//#define  IMP_ROLL_CAREOF         7
//#define  IMP_ROLL_DBA            8
//#define  IMP_ROLL_M_ADDR         9
//#define  IMP_ROLL_M_CITY         10
//#define  IMP_ROLL_M_STATE        11
//#define  IMP_ROLL_M_ZIP          12
//#define  IMP_ROLL_M_ADDR1        13
//#define  IMP_ROLL_M_ADDR2        14
//#define  IMP_ROLL_M_ADDR3        15
//#define  IMP_ROLL_M_ADDR4        16
//#define  IMP_ROLL_S_ADDR1        17
//#define  IMP_ROLL_S_ADDR2        18
//#define  IMP_ROLL_S_STRNUM       19
//#define  IMP_ROLL_S_STREET       20
//#define  IMP_ROLL_S_ZIP          21
//#define  IMP_ROLL_LEGAL          22
//#define  IMP_ROLL_LAND           23
//#define  IMP_ROLL_IMPR           24
//#define  IMP_ROLL_PPVALUE        25
//#define  IMP_ROLL_S_CITY         26
//#define  IMP_ROLL_S_DIR          27
//#define  IMP_ROLL_S_UNIT         28
//#define  IMP_ROLL_STATUS         29
//#define  IMP_ROLL_DOCNUM         30
//#define  IMP_ROLL_DOCDATE        31
//#define  IMP_ROLL_TAXABILITY     32

// LDR - 2018-2019 Secured Roll.txt
//#define  IMP_ROLL_FEEPARCEL      0
//#define  IMP_ROLL_APN            1
//#define  IMP_ROLL_TRA            2
//#define  IMP_ROLL_USECODE1       3
//#define  IMP_ROLL_USECODE2       4
//#define  IMP_ROLL_ACRES          5
//#define  IMP_ROLL_OWNER          6
//#define  IMP_ROLL_CAREOF         7
//#define  IMP_ROLL_DBA            8
//#define  IMP_ROLL_M_ADDR         9
//#define  IMP_ROLL_M_CITY         10
//#define  IMP_ROLL_M_STATE        11
//#define  IMP_ROLL_M_ZIP          12
//#define  IMP_ROLL_M_ADDR1        13
//#define  IMP_ROLL_M_ADDR2        14
//#define  IMP_ROLL_M_ADDR3        15
//#define  IMP_ROLL_M_ADDR4        16
//#define  IMP_ROLL_S_ADDR1        17
//#define  IMP_ROLL_S_ADDR2        18
//#define  IMP_ROLL_S_STRNUM       19
//#define  IMP_ROLL_S_STREET       20
//#define  IMP_ROLL_S_ZIP          21
//#define  IMP_ROLL_LEGAL          22
//#define  IMP_ROLL_LAND           23
//#define  IMP_ROLL_IMPR           24
//#define  IMP_ROLL_PPVALUE        25
//#define  IMP_ROLL_GROWING        26
//#define  IMP_ROLL_FIXTR          27
//#define  IMP_ROLL_S_CITY         28
//#define  IMP_ROLL_S_DIR          29
//#define  IMP_ROLL_S_UNIT         30
//#define  IMP_ROLL_STATUS         31
//#define  IMP_ROLL_DOCNUM         32
//#define  IMP_ROLL_DOCDATE        33
//#define  IMP_ROLL_TAXABILITY     34
//#define  IMP_ROLL_SUBDIV         35

// IMP_Roll.txt
#define  MB_ROLL_ASMT                 0
#define  MB_ROLL_FEEPARCEL            1
#define  MB_ROLL_TRA                  2
#define  MB_ROLL_LEGAL                3
#define  MB_ROLL_ZONING               4
#define  MB_ROLL_USECODE              5
#define  MB_ROLL_NBHCODE              6
#define  MB_ROLL_ACRES                7
#define  MB_ROLL_DOCNUM               8
#define  MB_ROLL_DOCDATE              9
#define  MB_ROLL_TAXABILITY           10
#define  MB_ROLL_STATUS               11
#define  MB_ROLL_OWNER                12
#define  MB_ROLL_CAREOF               13
#define  MB_ROLL_DBA                  14
#define  MB_ROLL_M_ADDR               15
#define  MB_ROLL_M_CITY               16
#define  MB_ROLL_M_ST                 17
#define  MB_ROLL_M_ZIP                18
#define  MB_ROLL_ADDR_TYPE            19
#define  MB_ROLL_LAND                 20
#define  MB_ROLL_HOMESITE             21
#define  MB_ROLL_IMPR                 22
#define  MB_ROLL_GROWING              23
#define  MB_ROLL_FIXTRS               24
#define  MB_ROLL_FIXTR_RP             25
#define  MB_ROLL_BUSPROP              26
#define  MB_ROLL_PPMOBILHOME          27
#define  MB_ROLL_VALUE_SET_TYPE       28
#define  MB_ROLL_M_ADDR1              29
#define  MB_ROLL_M_ADDR2              30
#define  MB_ROLL_M_ADDR3              31
#define  MB_ROLL_M_ADDR4              32

// IMP_Owner.txt 2017
#define  MB_NAME_ASMT                 0
#define  MB_NAME_OWNER                1
#define  MB_NAME_CAREOF               2
#define  MB_NAME_DBA                  3
#define  MB_NAME_M_ADDR               4
#define  MB_NAME_M_CITY               5
#define  MB_NAME_M_ST                 6
#define  MB_NAME_M_ZIP                7
#define  MB_NAME_M_ADDR1              8
#define  MB_NAME_M_ADDR2              9
#define  MB_NAME_M_ADDR3              10
#define  MB_NAME_M_ADDR4              11

// _VALUE
/*
#define  MB_VALU_ASMT                 0
#define  MB_VALU_SETTYPE              1
#define  MB_VALU_LAND                 2
#define  MB_VALU_LAND_DATE            3
#define  MB_VALU_HOMESITE             4
#define  MB_VALU_HOMESITE_DATE        5
#define  MB_VALU_IMPR                 6
#define  MB_VALU_IMPR_DATE            7
#define  MB_VALU_GROWING              8
#define  MB_VALU_GROWING_DATE         9
#define  MB_VALU_FIXTR                10

#define  MB_VALU_FIXTR_RP             11  // old script
#define  MB_VALU_PP_BUS               12

#define  MB_VALU_PERSPROP             11  // new script
#define  MB_VALU_BUSPROP              12

#define  MB_VALU_PP_MH                13
#define  MB_VALU_PP_PEN_INC           14
#define  MB_VALU_CURTYPE              15
#define  MB_VALU_PP_MH_DATE           16
#define  MB_VALU_DTS                  17
#define  MB_VALU_USERID               18
*/

// IMP_Exe.txt - 2017
#define  MB_EXE_STATUS               0
#define  MB_EXE_APN                  1
#define  MB_EXE_EXECODE              2
#define  MB_EXE_HOEXE                3
#define  MB_EXE_EXEAMT               4
#define  MB_EXE_PCT                  5

// 2017-2018 Exemption List.txt
#define  IMP_EXE_APN                  0
#define  IMP_EXE_EXECODE              1
#define  IMP_EXE_EXEAMT               2
#define  IMP_EXE_PCT                  3
#define  IMP_EXE_HOEXE                4
#define  IMP_EXE_DTS                  5
#define  IMP_EXE_USERID               6

// IMP_Asmt.txt - 2017
#define  MB_ASMT_ASMT                 0
#define  MB_ASMT_FEEPARCEL            1
#define  MB_ASMT_TRA                  2
#define  MB_ASMT_LEGAL                3
#define  MB_ASMT_ZONING1              4
#define  MB_ASMT_LANDUSE1             5
#define  MB_ASMT_NBHCODE              6
#define  MB_ASMT_ACRES                7
#define  MB_ASMT_CURRENTDOCNUM        8
#define  MB_ASMT_CURRENTDOCDATE       9
#define  MB_ASMT_TAXABILITY           10
#define  MB_ASMT_ASMTSTATUS           11

// IMP_Situs.txt - 2017
#define  MB_SITUS_ASMT                0
#define  MB_SITUS_STRNAME             1		// Might incluse StrType
#define  MB_SITUS_STRNUM              2
#define  MB_SITUS_STRTYPE             3
#define  MB_SITUS_STRDIR              4
#define  MB_SITUS_UNIT                5
#define  MB_SITUS_COMMUNITY           6
#define  MB_SITUS_ZIP                 7
#define  MB_SITUS_SEQ                 8

// IMP_Sales.txt - 2017
#define  MB_SALES_ASMT                0
#define  MB_SALES_DOCNUM              1
#define  MB_SALES_DOCDATE             2
#define  MB_SALES_DOCCODE             3
#define  MB_SALES_SELLER              4
#define  MB_SALES_BUYER               5
#define  MB_SALES_TAXAMT              6
#define  MB_SALES_GROUPSALE           7
#define  MB_SALES_GROUPASMT           8
#define  MB_SALES_XFERTYPE            9
#define  MB_SALES_ADJREASON           11
#define  MB_SALES_CONFCODE            12

bool  sqlConnect(LPCSTR strDb, hlAdo *phDb);
int   execSqlCmd(LPCSTR strCmd);
int   execSqlCmd(LPCSTR strCmd, hlAdo *phDb);
int   MB_CreateLienRec(char *pOutbuf, char *pRollRec);
int   MB_ExtrLien(LPCSTR pCnty, LPCSTR pLDRFile);
int   MB_ExtrTR601(LPCSTR pCnty, LPCSTR pLDRFile=NULL, int iChkLastChar=0);
int   MB_MergeSale(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag, bool bUpdtXfer=true);
int   Imp_MergeExeFile(int iSkip, int iMyRollLen);
int   Imp_MergeExe(char *pOutbuf);
int   MB_CreateSCSale(int iDateFmt=0, int iDocTypeFmt=0, int iDocNumFmt=0, bool bAppend=false, IDX_TBL5 *pDocTbl=NULL);
int   MB1_CreateSCSale(int iDateFmt=0, int iDocTypeFmt=0, int iDocNumFmt=0, bool bAppend=false, IDX_TBL5 *pDocTbl=NULL);

typedef struct _tTraCityTbl
{
   int   iLoTra;
   int   iHiTra;
   char  *pCity;
   char  *pZip;
} IMP_CITY;

IMP_CITY asImpCity[] =
{
   1000,  1999,  "BRAWLEY",      "92227",
   2000,  2999,  "CALEXICO",     "92231",
   3000,  3999,  "CALIPATRIA",   "92233",
   4000,  4999,  "EL CENTRO",    "92243",
   5000,  5999,  "HOLTVILLE",    "92250",
   6000,  6999,  "IMPERIAL",     "92251",
   7000,  7999,  "WESTMORLAND",  "92281",
   56000, 56003, "BRAWLEY",      "92227",
   57002, 57005, "CALEXICO",     "92231",
   58000, 58000, "CALIPATRIA",   "92233",
   58002, 58002, "NILAND",       "92257",
   58003, 58003, "CALIPATRIA",   "92233",
   58004, 58010, "NILAND",       "92257",
   62000, 62000, "EL CENTRO",    "92243",
   66001, 66004, "HEBER",        "92249",
   68001, 68001, "HOLTVILLE",    "92250",
   68002, 68002, "BRAWLEY",      "92227",
   68003, 68005, "HOLTVILLE",    "92250",
   68006, 68006, "BRAWLEY",      "92227",
   68007, 68015, "HOLTVILLE",    "92250",
   68018, 68018, "HOLTVILLE",    "92250",
   69000, 69000, "BRAWLEY",      "92227",
   69001, 69004, "IMPERIAL",     "92251",
   69007, 69007, "IMPERIAL",     "92251",
   69008, 69009, "OCOTILLO",     "92259",
   69011, 69011, "BRAWLEY",      "92227",
   73000, 73000, "BRAWLEY",      "92227",
   74000, 74004, "EL CENTRO",    "92243",
   74005, 74005, "CALEXICO",     "92231",
   75001, 75002, "EL CENTRO",    "92243",
   79000, 79000, "BRAWLEY",      "92227",
   82003, 82006, "THERMAL",      "92274",
   85000, 85005, "EL CENTRO",    "92243",
   90001, 90002, "BRAWLEY",      "92227",
   94002, 94002, "WINTERHAVEN",  "92283",
   94005, 94005, "PALO VERDE",   "92266",
   94006, 94010, "WINTERHAVEN",  "92283",
   95000, 99999, "", ""
};

// 2022
static XLAT_CODE  asFireplace[] =
{
   // Value, lookup code, value length
   "1", "L", 1,               // MASONRY 
   "2", "Z", 1,               // ZERO CLEARANCE
   "3", "W", 1,               // WOOD STOVE
   "4", "S", 1,               // PELLET STOVE
   "5", "O", 1,               // OTHER
   "EG","G", 2,               // Electric/Gas
   "MS","L", 2,               // MASONRY
   "MV","Y", 2,               // OTHER
   "",   "",  0
};

// 2020
static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "1", "Z", 1,               // Central Forced 
   "2", "D", 1,               // Wall 
   "3", "D", 1,               // Double wall
   "4", "C", 1,               // Floor
   "5", "S", 1,               // Wood Stove
   "6", "I", 1,               // Radiant floor
   "7", "I", 1,               // Radiant base board
   "8", "J", 1,               // Space heater
   "9", "X", 1,               // Other
   "BS","8", 2,               // Base board
   "PU","6", 2,               // Portable Unit
   "SH","J", 1,               // Space heater
   "",   "",  0
};

// 2020
static XLAT_CODE  asCooling[] =
{
   // Value, lookup code, value length
   "1", "C", 1,               // Central, refrigerated 
   "2", "H", 1,               // Heat pump
   "3", "E", 1,               // Evaporated
   "4", "L", 1,               // Wall
   "5", "X", 1,               // Other
   "",   "",  0
};

// 2020
static XLAT_CODE  asCondition[] =
{
   // Value, lookup code, value length
   "1", "N", 1,               // New 
   "2", "G", 1,               // Good
   "3", "A", 1,               // Average
   "4", "F", 1,               // Fair
   "5", "P", 1,               // Poor
   "",   "",  0
};

// 2020
static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "0", "N", 1,              // NONE
   "1", "P", 1,              // NON-HEATED
   "2", "H", 1,              // HEATED
   "3", "C", 1,              // WITH SPA
   "4", "P", 1,              // OTHER
   "",   "",  0
};

// 2020
static XLAT_CODE  asRoofMat[] =
{
   // Value, lookup code, value length
   "01", "C", 2,               // COMP SHINGLE 
   "02", "B", 2,               // WOOD SHAKE 
   "03", "A", 2,               // WOOD SHINGLE
   "04", "R", 2,               // COMP ROLL
   "05", "I", 2,               // TILE
   "06", "G", 2,               // SLATE
   "07", "L", 2,               // METAL, GALV OR ENAMELED
   "08", "F", 2,               // TAR & GRAVEL
   "09", "E", 2,               // BUILT UP
   "10", "Z", 2,               // Other
   "AS", "M", 2,               // Asphalt shingle
   "CA", "Z", 2,               // Corrugated aluminum
   "MT", "I", 2,               // Masonry tile
   "WS", "A", 2,               // Wooden shingle
   "WT", "A", 2,               // Wooden shingle
   "",   "",  0
};

// 2020
static XLAT_CODE  asRoofType[] =
{
   // Value, lookup code, value length
   "1", "G", 1,               // GABLE 
   "2", "H", 1,               // HIP 
   "3", "F", 1,               // FLAT
   "4", "N", 1,               // GAMBREL
   "5", "M", 1,               // MANSARD
   "6", "K", 1,               // SHED
   "7", "J", 1,               // OTHER
   "",   "",  0
};

// 2020
static XLAT_CODE  asViewCode[] =
{
   // Value, lookup code, value length
   "1", "R", 1,               // VALLEY 
   "2", "F", 1,               // LAKE
   "3", "E", 1,               // RIVER
   "4", "T", 1,               // MOUNTAIN
   "5", "N", 1,               // NO VIEW
   "",   "",  0
};

// 2020
static XLAT_CODE  asWaterSrc[] =
{
   // Value, lookup code, value length
   "1", "P", 1,               // PUBLIC DISTRICT 
   "2", "C", 1,               // PRIVATE/MUTUAL WATER COMPANY
   "3", "W", 1,               // WELL
   "4", "S", 1,               // SPRING
   "5", "N", 1,               // UNDEVELOPED
   "",   "",  0
};

IDX_TBL5 IMP_DocCode[] =
{// DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN  
   "00", "1  ", 'N', 2, 3,          // SALE (DECLARATION FILED)
   "01", "1  ", 'N', 2, 3,          // SALE (DTT)
   "02", "75 ", 'Y', 2, 3,          // TRANSFER (NO DTT)
   "03", "74 ", 'Y', 2, 3,          // NON-REAPPRAISABLE EVENT
   "04", "49 ", 'Y', 2, 3,          // MOBILEHOME LIEN UPDATE
   "05", "74 ", 'Y', 2, 3,          // NEW MOBILE HOME INSTALLATION (SOLD)
   "06", "74 ", 'Y', 2, 3,          // POSSESSORY INT NEW/RENEWAL
   "07", "74 ", 'Y', 2, 3,          // POSSESSORY INT LIEN UPDATE
   "08", "74 ", 'Y', 2, 3,          // FENCE
   "09", "74 ", 'Y', 2, 3,          // SPLIT/COMBO
   "10", "57 ", 'N', 2, 3,          // PARTIAL INTEREST SALE (DTT)
   "11", "57 ", 'Y', 2, 3,          // PARTIAL INT TRANSFER (NO DTT)
   "12", "74 ", 'Y', 2, 3,          // GOVERNMENT ENTITY/NO SUPL
   "13", "74 ", 'Y', 2, 3,          // NEW SINGLE FAMILY DWELLING
   "14", "74 ", 'Y', 2, 3,          // NEW GARAGE/CARPORT
   "15", "74 ", 'Y', 2, 3,          // NEW SWIMMING POOL/SPA
   "16", "74 ", 'Y', 2, 3,          // DWELLING ADD'N/CONVERSION
   "17", "74 ", 'Y', 2, 3,          // GAR ADD'N/CONV/PATIO/DECK
   "18", "74 ", 'Y', 2, 3,          // MISC ADD'N
   "19", "74 ", 'Y', 2, 3,          // NOTICE OF CONDEMNATION
   "20", "74 ", 'Y', 2, 3,          // Valuation Review
   "23", "74 ", 'Y', 2, 3,          // NEW MULTIPLE BLDG
   "24", "74 ", 'Y', 2, 3,          // ADD'N/CONV TO MULTIPLES
   "25", "74 ", 'Y', 2, 3,          // MISC MULTIPLES
   "30", "73 ", 'N', 2, 3,          // TRUSTEE'S DEED UPON SALE
   "33", "74 ", 'Y', 2, 3,          // NEW RURAL BLDG
   "34", "74 ", 'Y', 2, 3,          // RURAL BLDG ADD'N
   "35", "74 ", 'Y', 2, 3,          // GARAGE ENCLOSURE
   "36", "74 ", 'Y', 2, 3,          // PATIO
   "37", "74 ", 'Y', 2, 3,          // LAND IMPROVEMENTS
   "38", "74 ", 'Y', 2, 3,          // CITRUS ADD'S/DELETIONS
   "39", "74 ", 'Y', 2, 3,          // DATES ADD'S/DELETION
   "40", "73 ", 'N', 2, 3,          // TRUSTEE'S DEED UPON SALE
   "44", "74 ", 'Y', 2, 3,          // NEW INDUSTRIAL BLDG
   "45", "74 ", 'Y', 2, 3,          // ADD'N/CONV INDUSTRIAL
   "46", "74 ", 'Y', 2, 3,          // MISC INDUSTRIAL
   "52", "1  ", 'N', 2, 3,          // GRANT DEED (based on ATTOM)
   "53", "74 ", 'Y', 2, 3,          // NEW COMM'L BLDG
   "54", "74 ", 'Y', 2, 3,          // ADD'N/CONV COMM'L
   "55", "74 ", 'Y', 2, 3,          // MISC COMM'L
   "57", "74 ", 'Y', 2, 3,          // P58 & P193 - PARTIAL FILED & APPROVED
   "58", "74 ", 'Y', 2, 3,          // P58 & P193 FILED AND APPROVED
   "59", "74 ", 'Y', 2, 3,          // AG CONTRACT CITRUS INC/DEC
   "60", "74 ", 'Y', 2, 3,          // AG CONTRACT DATES INC/DEC
   "61", "74 ", 'Y', 2, 3,          // AG C
   "63", "74 ", 'Y', 2, 3,          // AG CONTRACT-NEW BLDG
   "64", "74 ", 'Y', 2, 3,          // AG CONTRACT-ADD'N/CONV
   "65", "74 ", 'Y', 2, 3,          // AG CONTRACT-MISC
   "66", "74 ", 'Y', 2, 3,          // AG CONTRACT- VINES INC/DEC
   "67", "74 ", 'Y', 2, 3,          // AG CONTRACT LAND IMPS
   "70", "74 ", 'Y', 2, 3,          // LIEN DATE UPDATE
   "71", "74 ", 'Y', 2, 3,          // PROP 8
   "72", "74 ", 'Y', 2, 3,          // CALAMITY-APPLICATION
   "73", "74 ", 'Y', 2, 3,          // CALAMITY-RESTORATION
   "74", "74 ", 'Y', 2, 3,          // ASSESSMENT APPEALS
   "75", "74 ", 'Y', 2, 3,          // SOLAR IMPROVEMENT
   "80", "74 ", 'Y', 2, 3,          // NON-TAXABLE GOVERNMENT PROP
   "81", "74 ", 'Y', 2, 3,          // FLATWORK
   "96", "74 ", 'Y', 2, 3,          // DEMO/REMOVAL
   "97", "74 ", 'Y', 2, 3,          // M/H INSTALL
   "98", "74 ", 'Y', 2, 3,          // WATER, SEWER, ELEC
   "99", "74 ", 'Y', 2, 3,          // SIGN
   "CD", "13 ", 'N', 2, 3,          // Sale/Gift - Divided % transfer
   "CH", "74 ", 'Y', 2, 3,          // CHURCH RELATED
   "CN", "16 ", 'Y', 2, 3,          // Gift/No $ involved transfer
   "CS", "1  ", 'N', 2, 3,          // Sale Transfer
   "CX", "74 ", 'Y', 2, 3,          // CONTRACTOR EXCLUSION 75.12
   "DN", "57 ", 'Y', 2, 3,          // Partial Interest - No reappraisal
   "DV", "58 ", 'Y', 2, 3,          // Change of Partial Value Interest
   "E1", "74 ", 'Y', 2, 3,          // Prop 58 & Prop 193 GIFT
   "E2", "74 ", 'Y', 2, 3,          // Prop 58 & Prop 193 SALE
   "E3", "74 ", 'Y', 2, 3,          // Prop 58 & 193 Partial - Gift
   "E4", "74 ", 'Y', 2, 3,          // Prop 58 & 193 Partial - Sale
   "EX", "74 ", 'Y', 2, 3,          // Prop 58 & 193 Exclusion
   "GR", "74 ", 'Y', 2, 3,          // GRADING, LAND WORK
   "HX", "74 ", 'Y', 2, 3,          // HOMEOWNER EXEMPTION
   "MC", "74 ", 'Y', 2, 3,          // M/H CONVERSION FROM LIC (NO CIO)
   "MO", "74 ", 'Y', 2, 3,          // M/H FROM OUT OF COUNTY (NO CIO)
   "NX", "74 ", 'Y', 2, 3,          // No Transfer
   "OI", "74 ", 'Y', 2, 3,          // 
   "PN", "74 ", 'Y', 2, 3,          // Value Pending unable to determine
   "RM", "74 ", 'Y', 2, 3,          // RE MODEL
   "RP", "74 ", 'Y', 2, 3,          // REPAIRS
   "RR", "74 ", 'Y', 2, 3,          // RE ROOF
   "SG", "74 ", 'Y', 2, 3,          // STORAGE
   "SH", "74 ", 'Y', 2, 3,          // SHADE
   "SI", "74 ", 'Y', 2, 3,          // ON SITE IMPROVEMENTS
   "SP", "74 ", 'Y', 2, 3,          // SPLIT TAX BILL
   "ST", "74 ", 'Y', 2, 3,          // STORAGE
   "TI", "74 ", 'Y', 2, 3,          // TENNANT IMPROVEMENTS
   "TK", "74 ", 'Y', 2, 3,          // TANK
   "TV", "74 ", 'Y', 2, 3,          // PROPOSITION 60
   "TW", "74 ", 'Y', 2, 3,          // CELL TOWER
   "VN", "74 ", 'Y', 2, 3,          // Gifts, Foreclosure, Trades
   "VP", "58 ", 'Y', 2, 3,          // Value Partial %
   "VR", "74 ", 'Y', 2, 3,          // VALUE REVIEW
   "VS", "75 ", 'N', 2, 3,          // Value Sale Transfer
   "VX", "74 ", 'Y', 2, 3,          // Correction
   "WL", "74 ", 'Y', 2, 3,          // WELL
   "XX", "75 ", 'Y', 2, 3,          // No Value Change Transfers
   "", "", '\0', 0, 0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 IMP_Exemption[] = 
{
   "E01", "H", 3,1,     // HOMEOWNERS EXEMPTION
   "E02", "V", 3,1,     // VETERAN EXEMPTION
   "E03", "D", 3,1,     // DISABLED VET (BASIC)
   "E04", "D", 3,1,     // DISABLED VET (LOW INCOME)
   "E05", "V", 3,1,     // VETERAN
   "E06", "X", 3,1,     // LESSORS EXEMPTION
   "E07", "X", 3,1,     // LESSEES EXEMPTION
   "E08", "L", 3,1,     // FREE PUBLIC LIBRARY OR MUSEUM
   "E09", "C", 3,1,     // CHURCH AND WELFARE EXEMPTION
   "E10", "C", 3,1,
   "E11", "W", 3,1,     // WELFARE EXEMPTION
   "E12", "W", 3,1, 
   "E13", "W", 3,1,
   "E14", "W", 3,1, 
   "E15", "P", 3,1,     // PUBLIC SCHOOL EXEMPTION
   "E16", "V", 3,1,     // VETERAN ORGANIZATION EXEMPTION
   "E17", "R", 3,1,
   "E18", "W", 3,1,     // WELFARE EX-HOUSING ELDERLY
   "E25", "S", 3,1,     // PRIVATE & PAROCHIAL SCHOOL
   "E30", "I", 3,1,     // HOSPITALS
   "E80", "U", 3,1,     // PRIVATE COLLEGE
   "E81", "X", 3,1,     // LOW VALUED PROPERTY
   "E82", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E90", "X", 3,1,     // VESSEL - LOW VALUE ORDINANCE
   "E91", "X", 3,1,     // BOE DUMMY 40K DISABLED VETERANS
   "E99", "X", 3,1,     // LATE FILE FLAG
   "","",0,0
};

#endif // !defined(AFX_LOADMB_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_)
