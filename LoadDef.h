#ifndef _LOADDEF_F
#define _LOADDEF_F
#define  MAX_RECCOLS    4
#define  MAX_RECFLDS    512

typedef struct _tRecInfo
{
   char  acFldName[30];
   char  acFldType[4];     // B=binary, C=Asc, E=Ebc, P=pack dec, Z=zone decimal, N=numeric
   int   iFldLen;
   int   iOffset;
} RECINFO;

#define B2L(dest, src)\
   *(dest) = *(src+3);\
   *((dest)+1) = *(src+2);\
   *((dest)+2) = *(src+1);\
   *((dest)+3) = *src

#define B2I(dest, src)\
   *(dest) = *(src+1);\
   *((dest)+1) = *src

void  *getOffsetTbl(void);
int   LoadDefFile(LPCSTR lpFilename);
int   R_Ebc2Asc(LPCSTR strInRec, LPSTR strOutRec);
int   F_Ebc2Asc(LPCSTR strInfile, LPCSTR strOutfile, LPCSTR strDeffile, int iERecLen);
int   F_Ebc2Asc(LPCSTR strInfile, LPCSTR strOutfile, int iERecLen);
int   F_Ebc2Asc(LPCSTR strInfile, LPCSTR strOutfile, LPCSTR strDeffile, int iOffset, unsigned char sCode);
int   F_Pd2Num(LPCSTR strValue, int iLen);
int   F_Bin2Asc(LPSTR strIn, LPSTR strOut, int iLen);

#endif