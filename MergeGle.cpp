/**************************************************************************
 *
 * Options:
 *    -CGLE -L -Xs|-Ms -Xa -Xl (load lien)
 *    -CGLE -U -Xsi -Xa (load update)
 *
 * Notes: InCareOf field in Gle_Roll.csv may contain extended Name1, Name2, or C/O
 *
 * Revision
 * 03/22/2016 15.14.2   First version - Copy from MergeTeh.cpp
 * 04/14/2016 15.15.0.1 Modify Load_Roll() to use MB_MergeExe2() in new MH.
 * 04/18/2016 15.15.1   Modify MergeRoll() to update ALT_APN in UPDATE_R01 mode.
 * 07/13/2016 16.0.4    Add Gle_MergeLien3() and Gle_Load_LDR3() to support new layout of LDR 2016
 *                      Modify MergeOwner() to merge Name1 & Name2 when possible
 * 07/18/2016 16.0.5    Modify Gle_Load_LDR3() to use lien value from TC601 file.
 * 08/14/2016 16.3.0    Delete unused function Gle_MergeTax()
 * 08/19/2016 16.4.1    Add option to update PREV_APN using last year file.
 * 02/15/2017 16.9.1    Use updateDelqFlag() to update tb_isDelq in base table.
 * 03/01/2017 16.9.4    Fix Other Value in Gle_MergeRoll() 
 * 07/07/2017 17.1.4    Minor change in Gle_MergeOwner() to fix special case.
 * 07/21/2017 17.1.6    Fix Parking space/type problem in Gle_ConvStdChar()
 * 08/02/2017 17.2.3    Remove TotalRooms from Gle_ConvStdChar() since this field is not keyed 
 * 02/20/2019 18.10.3   Change garage type from 'A' to 'I' and frpm 'D' to 'L' n Gle_ConvStdChar()
 * 04/16/2019 18.11.2   Add comment and update GLE_DocCode[]
 * 07/11/2019 19.0.3    Modify Gle_MergeMAdr() to support special case of "CO RD" to "COUNTY ROAD"
 * 07/20/2019 19.1.0    Remove duplicate code in MergeLien3().
 * 06/17/2020 19.10.2   Add -Mz
 * 07/16/2020 20.1.5    Modify loadGle() to extract sale in overwrite mode.  Modify Gle_CreateSCSale()
 *                      to fix Sale_Code bug.  Modify Gle_MergeSitus() to handle special case of S_DIR.
 * 11/01/2020 20.4.2    Modify Gle_MergeRoll() to populate default PQZoning.
 * 08/26/2021 21.2.0    Default bUseConfSalePrice=false so we don't use confidential sale price.
 * 04/19/2022 21.8.3    Add Gle_CleanSaleFile() to cleanup the remove bad docs from cum sale file.
 *                      Modify Merge_Roll() to check for error in DocNum before update.
 * 05/18/2022 21.9.2    Modify Gle_Load_Roll() & Gle_ConvStdChar() to handle new file layout.
 * 06/09/2023 22.9.4    Modify Gle_CreateSCSale() to remove null CHAR from sale record.
 * 09/11/2023 23.1.10   Modify Gle_ConvStdChar() to support new QualityClass format.
 * 11/20/2023 23.4.3    Modify Gle_ConvStdChar() to find the more complete record.
 * 12/12/2023 23.4.5    Modify Gle_ConvStdChar() to take out update to ImprCond.
 * 03/23/2024 23.7.3    Modify Gle_MergeStdChar() to update LotAcre/LotSqft if available.
 * 07/04/2024 24.0.1    Modify Gle_MergeLien3() to add ExeType.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"

#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "CharRec.h"
#include "PQ.h"
#include "MergeGle.h"
#include "Situs.h"
#include "Tax.h"
//#include "MergeZoning.h"

static long lUseGis;

/***************************** CleanSaleFile **********************************
 *
 * Fix DocNum in cumsale file.
 *
 * Remove internal record 'I' type (i.e. 2013I1234). Also remove 'R' at position 5
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Gle_CleanSaleFile(char *pInfile)
{
   char   acTmp[32], acInbuf[2048], acOutFile[_MAX_PATH], *pRec;
   long   lCnt=0, lDrop=0, iTmp;
   FILE   *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];

   LogMsg0("Fix DocNum for %s", pInfile);

   if (_access(pInfile, 0))
   {
      LogMsg("***** FixDocNum(): Missing input file: %s", pInfile);
      return -1;
   }

   // Open input file
   LogMsg("Open cum sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** FixDocNum(): Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "001032026000", 9))
      //   iTmp = 0;
#endif
      // If DocNum[4]=bType, drop this record
      if (pInRec->DocNum[4] != 'I' && !memcmp(pInRec->DocNum, pInRec->DocDate, 4))
      {
         if (pInRec->DocNum[5] == 'R')
         {
            iTmp = sprintf(acTmp, "%.4s%.4s  ", pInRec->DocNum, &pInRec->DocNum[5]);
            memcpy(pInRec->DocNum, acTmp, iTmp);
         }
         fputs(acInbuf, fdOut);
      } else
         lDrop++;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   // Save input file
   strcpy(acInbuf, pInfile);
   pRec = strrchr(acInbuf, '.');
   strcpy(pRec, ".org");
   if (!_access(acInbuf, 0))
      DeleteFile(acInbuf);

   // Save input file
   LogMsg("Rename input file %s --> %s", pInfile, acInbuf);
   iTmp = rename(pInfile, acInbuf);

   // Rename output file
   LogMsg("Rename output file %s --> %s", acOutFile, pInfile);
   iTmp = rename(acOutFile, pInfile);

   LogMsg("Total input records:     %u", lCnt);
   LogMsg("    records dropped:     %u", lDrop);

   return iTmp;
}

 /******************************** Gle_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 * BOLLINGER PETER P INV CO ETAL C/O INTER CAL REAL E (001032016000)
 *
 *****************************************************************************/

void Gle_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf)
{
   int   iTmp, iRet, iVet;
   char  acOwner[64], acName1[64], acName2[64], acTmp[128], *pTmp, *pTmp1;
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);

#ifdef _DEBUG
   // MC DONALD JOHN E &   % MC DONALD ELIZABETH ETAL
   //if (!memcmp(pOutbuf, "044060021000", 9))
   //   iTmp = 0;
#endif

   // Check embeded CareOf
   if (pTmp = strstr(pNames, "C/O"))
   {
      updateCareOf(pOutbuf, pTmp, strlen(pTmp));
      *pTmp = 0;
   }

   // Remove multiple spaces
   strcpy(acName1, pNames);
   iTmp = blankRem(acName1);
   if (pCareOf && *pCareOf == '%')
   {
      if (*(pCareOf+1) == ' ')
         strcpy(acName2, pCareOf+2);
      else
         strcpy(acName2, pCareOf+1);
      *pCareOf = 0;
   } else 
      acName2[0] = 0;

   // Update vesting
   iVet = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST);

   // Check for overflow character '&'
   if (acName1[iTmp-1] == '&')
   {
      acName1[iTmp-2] = 0;

      // Check Name2
      if (acName2[0] > ' ')
      {
         // Check vesting
         if (!iVet)
            iVet = updateVesting(myCounty.acCntyCode, acName2, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

            // Check embeded CareOf
            if (pTmp = strstr(acName2, "C/O"))
            {
               updateCareOf(pOutbuf, pTmp, strlen(pTmp));
               *pTmp = 0;
            }

         iTmp = MergeName2(acName1, acName2, acOwner, ' ');
         // Remove Name2 if mergable
         if (iTmp == 1)
            acName2[0] = 0;
         else
         {
            strcpy(acOwner, acName1);
            pTmp = strrchr(acName2, ' ');
            if (pTmp && (!memcmp(pTmp, " JT", 3) || !memcmp(pTmp, " J/T", 4) || 
                         !strcmp(pTmp, " CP")    ||!strcmp(pTmp, " C/P") ||
                         !strcmp(pTmp, " SP")    || !strcmp(pTmp, " TC")))
               *pTmp = 0;
         }
      } else
         strcpy(acOwner, acName1);
   } else
      strcpy(acOwner, acName1);

   strcpy(acTmp, acOwner);
   // Remove ETAL or DVA
   if (pTmp = strrchr(acTmp, ' '))
   {
      if (!strcmp(pTmp, " ETAL") || !strcmp(pTmp, " DVA") )
         *pTmp = 0;
      else if (pTmp = strstr(acTmp, " ET AL"))
         *pTmp = 0;
   }

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (acTmp[iTmp] > '0' && acTmp[iTmp] < 'A')
      {
         iTmp = 0;
         break;
      }
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp || strstr(acOwner, "L/P"))
   {
      vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
      return;
   }

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
      if ( (pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " TR")))
         *pTmp = 0;

      if (strstr((char *)&acTmp[iTmp], " TRUST") ||
         strstr((char *)&acTmp[iTmp], " LIVING") ||
         strstr((char *)&acTmp[iTmp], " REVOCABLE") )
         acTmp[--iTmp] = 0;
   }

   // Filter out words, things in parenthesis
   if ( (pTmp = strchr(acTmp, '('))      || (pTmp=strstr(acTmp, " CO TR"))   ||
        (pTmp=strstr(acTmp, " CO-TR"))   || (pTmp=strstr(acTmp, " COTR"))    ||
        (pTmp=strstr(acTmp, " TRUSTEE")) || (pTmp=strstr(acTmp, " TR ETAL")) ||
        (pTmp=strstr(acTmp, " TTEE"))    || (pTmp=strstr(acTmp, " TRES"))    ||
        (pTmp=strstr(acTmp, " ETAL"))    || (pTmp=strstr(acTmp, " ET AL"))  )
      *pTmp = 0;

   // Trim trailing number
   iTmp = strlen(acTmp)-1;
   while (iTmp > 0 && isdigit(acTmp[iTmp]))
      acTmp[iTmp--] = 0;

   if ((pTmp=strstr(acTmp, " SUCCS-TR")) || (pTmp=strstr(acTmp, " SUCCESSOR")) ||
      (pTmp=strstr(acTmp, " SURVIVOR")) )
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " FAMILY TRUST &")) || (pTmp=strstr(acTmp, " LIVING TRUST &")) )
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      // RUMAN RICKY L & RUMAN FAMILY TRUST &
      // RUMAN RICKY L & NANCY FAMILY TRUST & MARIE
      // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      //strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      //strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      *(pTmp+6) = 0;
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
         *pTmp1 = 0;
      else
         *pTmp = 0;

      //strcpy(acName2, pTmp+9);
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " FAM TR")) || (pTmp=strstr(acTmp, " FAMILY ")) ||
      (pTmp=strstr(acTmp, " REVOC"))          || (pTmp=strstr(acTmp, " REV TR")) ||
      (pTmp=strstr(acTmp, " REV LIV TR"))     || (pTmp=strstr(acTmp, " REV LIVING")) ||
      (pTmp=strstr(acTmp, " LIV TRUST"))      || (pTmp=strstr(acTmp, " LIVING ")) ||
      (pTmp=strstr(acTmp, " INCOME TR"))      || (pTmp=strstr(acTmp, " 1992 REV"))
      )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ( (pTmp=strstr(acTmp, " ESTATE OF")) || (pTmp=strstr(acTmp, " EST OF")) ||
               (pTmp=strstr(acTmp, " ESTS OF")) )
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   pTmp = strrchr(acName1, ' ');
   if (pTmp && (!memcmp(pTmp, " JT", 3) || !strcmp(pTmp, " CP")    ||!strcmp(pTmp, " C/P") ||
                !strcmp(pTmp, " SP")    || !strcmp(pTmp, " TC")))
      *pTmp = 0;

   if ((pTmp=strstr(acName1, " S/S")))
      *pTmp = 0;
   if ((pTmp=strstr(acName1, " J/T")) || (pTmp=strstr(acName1, " J/ T")))
      *pTmp = 0;
   if ((pTmp=strstr(acName1, " T/C")) || (pTmp=strstr(acName1, " C/B")))
      *pTmp = 0;

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&TEHRON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
         *pTmp1++ = 0;
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = -1;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      if (myOwner.acVest[0] > ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // If name is swapable, use it
      if (iRet >= 0)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   }

   if (iRet == -1)
   {
      // Couldn't split names
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME1);
   }

   // Save Name2 if exist
   if (acName2[0] > ' ')
      vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);

   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);
}


/******************************** Gle_MergeOwner *****************************
 *
 * The CareOf field may contain Owner2.  Check for & at the end of Assessee
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Gle_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf, char *pDba)
{
   int   iTmp, iRet, iACnt;
   char  acOwner[64], acName1[64], acName2[64], *pTmp;
   OWNER myOwner;
   boolean bUseCareOf=false, bHasName2=false;

   // Clear output buffer if needed
   removeNames(pOutbuf, true, true);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0050200020", 10))
   //   iTmp = 0;
#endif

   // Init names
   iACnt=0;
   strcpy(acName1, pNames);
   myTrim(acName1);
   acName2[0] = 0;

   // Check DBA
   if (*pDba > ' ')
   {
      pTmp = pDba;
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   }

   // Check CareOf
   if (pTmp = strstr(acName1, "C/O"))
   {
      updateCareOf(pOutbuf, pTmp, strlen(pTmp));
      *--pTmp = 0;
   }

   // Check for Name2
   iTmp = strlen(acName1);
   if (acName1[iTmp-1] == '&')
   {
      bHasName2 = true;
      acName1[iTmp-1] = 0;
      if (*pCareOf == '%')
         strcpy(acName2, pCareOf+2);
      else
         strcpy(acName2, pCareOf);
   }


   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST);
   if (!iTmp && acName2[0] > ' ')
      iTmp = updateVesting(myCounty.acCntyCode, acName2, pOutbuf+OFF_VEST);

   if (bHasName2 && !iTmp)
   {
      iTmp = MergeName1(acName1, acName2, acOwner);
      if (!iTmp)
         strcpy(acOwner, acName1);
   } else
      strcpy(acOwner, acName1);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = -1;
   if (strchr(acOwner, ' '))
   {
      iRet = splitOwner(acOwner, &myOwner, 5);

      // If name is swapable, use it
      if (iRet >= 0)
      {
         blankRem(myOwner.acSwapName);
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
      }
   }

   if (iRet == -1)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);

   // Save Name2 if exist
   vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);

   // Save Name1
   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);
}

/******************************** Gle_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Gle_MergeMAdr(char *pOutbuf)
{
   char    acTmp[256], acAddr1[64], *pTmp;
   int     iTmp;
   ADR_REC sMailAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0050200020", 10))
   //   iTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, false, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   // Special case
   iTmp = replStr(acAddr1, "CO RD ", "COUNTY ROAD ");
   if (iTmp > 0 && bDebug)
      LogMsg("--> Rename CO RD to %s [%.12s]", acAddr1, pOutbuf);

   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }

   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
      sMailAdr.strName[SIZ_M_STREET] = 0;
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], SIZ_M_CITY);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         // Take out '-' in zipcode if present
         strcpy(acTmp, apTokens[MB_ROLL_M_ZIP]);
         iTmp = strlen(acTmp);
         if (acTmp[5] == '-' && iTmp == 10)
         {
            strcpy(&acTmp[5], &acTmp[6]);
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         } else if (iTmp > 9)
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         else
            vmemcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
      } else
         iTmp = 0;

      if (iTmp == 9)
         sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP], apTokens[MB_ROLL_M_ZIP]+5);
      else
         sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

void Gle_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)

{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2, *pDba;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "005070005000", 9) || !memcmp(pOutbuf, "005230043000", 9) )
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   pDba = p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }

      if (!_memicmp(pLine1, "DBA ", 4) )
         pDba = pLine1;
      else if (!_memicmp(pLine2, "DBA ", 4))
         pDba = pLine2;
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }

      if (!_memicmp(pLine1, "DBA ", 4) )
         pDba = pLine1;
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
   }

   // Update DBA
   if (pDba)
   {
      memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Special case
      int iRoadChg = replStr(acAddr1, "CO RD ", "COUNTY ROAD ");
      if (iRoadChg > 0 && bDebug)
         LogMsg("--> Rename CO RD to %s [%.12s]", acAddr1, pOutbuf);

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));

         if (!iRoadChg)
         {
            if (sMailAdr.strSfx[0] > '0')
               memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
            if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
               memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
            if (pTmp = strstr(sMailAdr.strName, " PMB"))
               *pTmp = 0;
            vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
         } else
         {
            pTmp = strstr(acAddr1, "COUNTY");
            vmemcpy(pOutbuf+OFF_M_STREET, pTmp, SIZ_M_STREET);
         }
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   strcpy(acAddr2, p2);
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);

      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}


/******************************** Gle_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Gle_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acAddr2[64], *pTmp, *pTmp1,
            acStrName[32], acStrSfx[32];
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do {
         pRec = fgets(acRec, 512, fdSitus);
      } while (pRec && (*pRec < ' ' || *pRec > '9'));
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      pTmp = pRec;
      if (*pTmp == '"')
         pTmp++;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   replNull(pRec);
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   else
      iRet = ParseStringIQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   if (*apTokens[MB_SITUS_STRNAME] == ' ' && *apTokens[MB_SITUS_STRNUM] == ' ')
      return 0;

   // Merge data
   acAddr1[0] = 0;
   strcpy(acStrName, apTokens[MB_SITUS_STRNAME]);
   strcpy(acStrSfx, apTokens[MB_SITUS_STRTYPE]);
   removeSitus(pOutbuf);
   lTmp = atol(myTrim(apTokens[MB_SITUS_STRNUM]));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "040282002000", 12))    // 6TH ST
   //   iTmp = 0;
#endif

   if (lTmp > 0)
   {
      // Save original StrNum
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
      {
         if (pTmp1 = strstr(pTmp, "6TH"))
         {
            strcpy(acStrName, pTmp1);
            strcpy(acStrSfx, apTokens[MB_SITUS_STRNAME]);
         } else if (*(pTmp+1) > ' ')
            vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, SIZ_S_STR_SUB);
      }

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         vmemcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], SIZ_S_DIR);
      }
   }

   if (acStrSfx[0] > ' ')
   {
      strcat(acAddr1, acStrName);
      strcat(acAddr1, " ");
      strcat(acAddr1, acStrSfx);
      vmemcpy(pOutbuf+OFF_S_STREET, acStrName, SIZ_S_STREET);

      iTmp = GetSfxCodeX(acStrSfx, acTmp);
      if (iTmp > 0)
      {
         Sfx2Code(acTmp, acCode);
         vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      } else
      {
         LogMsg0("*** Invalid suffix: %s", acStrSfx);
         iBadSuffix++;
      }
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, acStrName);
      vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      vmemcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, SIZ_S_SUFF);

      strcat(acAddr1, acStrName);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNO);
   }

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);
   memset(acAddr2, ' ', SIZ_S_CTY_ST_D);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr2, pOutbuf);   
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr2[0] > ' ')
      {
         // Zip
         lTmp = atol(apTokens[MB_SITUS_ZIP]);
         if (lTmp > 10000 && lTmp < 99999)
            memcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], 5);

         iTmp = sprintf(acTmp, "%s, CA %.5s", acAddr2, apTokens[MB_SITUS_ZIP]);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
      }
   }

   // Get city/zip from GIS extract
   //if (fdCity && *(pOutbuf+OFF_S_CITY) == ' ')
   //{
   //   char acZip[16], acCity[32];

   //   iTmp = getCityZip(pOutbuf, acCity, acZip, 9);
   //   if (!iTmp)
   //   {
   //      City2Code(acCity, acCode, pOutbuf);
   //      memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
   //      memcpy(pOutbuf+OFF_S_ZIP, acZip, strlen(acZip));
   //      sprintf(acAddr2, "%s CA %s", acCity, acZip);
   //      lUseGis++;
   //   }
   //}

   iTmp = blankRem(acAddr2, SIZ_S_CTY_ST_D);
   memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int Gle_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "007280027000", 9))
   //   acTmp[0] = 0;
#endif
   //
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   if (sSitusAdr.lStrNum > 0)
   {
      char *pTmp = strchr(acAddr1, ' ');
      *pTmp = 0;

      // Save original StrNum
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
      memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   }

   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/******************************** Gle_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Gle_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      pTmp = pRec;
      if (*pTmp == '"')
         pTmp++;

      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002017014", 9))
   //   iRet = 0;
#endif

   if (iTmp)
      return 1;

   while (!iTmp)
   {
      // Parse input
      if (cDelim == ',')
         iRet = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iRet = ParseStringIQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);

      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, YYYY_MM_DD);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         /* Do not use Confidential Sale Price
         } else
         {
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            }
         */
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         MB_MergeSale(&sCurSale, pOutbuf, true, false);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
      {
         pTmp = pRec;
         if (*pTmp == '"')
            pTmp++;
         iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      } else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/***************************** Gle_MergeStdChar ******************************
 *
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the one that has value.
 *
 *****************************************************************************/

int Gle_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft;
   int      iLoop, iBeds, iFBath, iHBath, iBldgNum, iTmp;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001051007", 9))
   //   iTmp = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   //while (!iLoop)
   {
      // Quality Class
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
      *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;

      // Yrblt
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

      // YrEff
      iTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
      if (iTmp > 1600 && iTmp <= lToyear)
         memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

      // LotSqft
      long lLotSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
      if (lLotSqft > 100)
      {
         memcpy(pOutbuf+OFF_LOT_SQFT, pChar->LotSqft, SIZ_LOT_SQFT);
         memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_ACRES);
      } 

      // BldgSqft
      lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
      if (lBldgSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      } else
         memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

      // Garage Sqft
      lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
      if (lGarSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      } else
         memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);

      // Parking type
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

      // ParkSpace
      iTmp = atoin(pChar->ParkSpace, SIZ_CHAR_SIZE4);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d      ", iTmp);
         memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      } else
         memset(pOutbuf+OFF_PARK_SPACE, ' ', SIZ_PARK_SPACE);

      // Heating
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];

      // Cooling
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

      // Beds
      iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      } else
         memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

      // Bath
      iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      } else
         memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

      // Half bath
      iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
      if (iHBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      } else
         memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

      // Rooms
      iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      } else
         memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

      // Units
      iTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
      if (pChar->Units[0] > ' ')
      {
         sprintf(acTmp, "%*d", SIZ_UNITS, iTmp);
         memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
      } else
         memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

      // Stories
      if (pChar->Stories[0] > ' ')
         memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
      else
         memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

      // Fireplace
      if (pChar->Fireplace[0] > ' ')
         memcpy(pOutbuf+OFF_FIRE_PL, pChar->Fireplace, SIZ_FIRE_PL);
      else
         memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);

      // HasSeptic or HasSewer
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

      // HasWell
      *(pOutbuf+OFF_WATER) = pChar->HasWater;

      // Pools
      *(pOutbuf+OFF_POOL) = pChar->Pool[0];

      // BldgNum
      iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);

      lCharMatch++;

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      //if (!pRec)
      //   break;
      //iLoop = memcmp(pOutbuf, pRec, iApnLen);
      //if (!iLoop && (iBeds > 0 && iBldgNum > 0))
      //   break;
   }

   return 0;
}

/******************************** Gle_MergeChar ******************************
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

int Gle_MergeChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft;
   int      iLoop, iBeds, iFBath, iHBath, iFp;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;

   // Yrblt
   memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   } else if (*(pOutbuf+OFF_PARK_TYPE) > ' ')
   {
      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
   }

   // Parking type
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // Heating
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];

   // Cooling
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Fireplace
   iFp = atoin(pChar->Fireplace, SIZ_CHAR_SIZE2);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   } else
      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);

   // HasSeptic or HasSewer
   *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Pools
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   lCharMatch++;

   iLoop = 0;
   do
   {
      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         break;      // EOF
      }
      iLoop++;
   } while (!memcmp(pOutbuf, pRec, iApnLen));

   if (iLoop > 1)
   {
      int iTmp;

      iTmp = sprintf(acTmp, "%d", iLoop);
      memcpy(pOutbuf+OFF_UNITS, acTmp, iTmp);
   }

   return 0;
}

/********************************* Gle_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Gle_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Remove NULL
   iRet = replStr(pRollRec, "NULL", "");

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Gle_MergeLien(): bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L_ASMT], strlen(apTokens[L_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L_FEEPARCEL], strlen(apTokens[L_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "11GLE", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values
   lTmp = atoin(apTokens[L_ASMT], 3);
   if (lTmp > 799 && lTmp < 900)
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   // Land
   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixtr = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
   long lGrow  = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);
   long lPers  = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
   long lPP_MH = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L_TAXAMT1]);
   double dTax2 = atof(apTokens[L_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }
   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'


   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L_TRA], strlen(apTokens[L_TRA]));

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030330020", 9))
   //   iTmp = 0;
#endif
   // Legal
   updateLegal(pOutbuf, apTokens[L_PARCELDESCRIPTION]);

   // UseCode
   iTmp = strlen(apTokens[L_USECODE]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L_USECODE], SIZ_USE_CO, iTmp);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L_USECODE], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Gle_MergeOwner(pOutbuf, apTokens[L_OWNER], "");

   // Situs
   Gle_MergeSitus(pOutbuf, apTokens[L_SITUS1], apTokens[L_SITUS2]);

   // Mailing
   Gle_MergeMAdr(pOutbuf, apTokens[L_MAILADDRESS1], apTokens[L_MAILADDRESS2], apTokens[L_MAILADDRESS3], apTokens[L_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L_TAXABILITY], true, true);

   return 0;
}

/********************************* Gle_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Gle_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Gle_MergeRoll(): bad input record for APN=%s (%d)", apTokens[iApnFld], iRet);
      return -1;
   }

   // Ignore APN starts with 100-999 except 910 (MH), 920 (common area)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp > 99 && iTmp != 910 && iTmp != 920))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // PREV_APN - Only apply to physical parcels
      if (memcmp(pOutbuf, "100", 3) < 0)
         vmemcpy(pOutbuf+OFF_PREV_APN, apTokens[iApnFld], 10);

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "11GLE", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   acTmp[SIZ_USE_CO] = 0;
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "015040009000", 9) )
   //   iTmp = 0;
#endif

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0' && *(apTokens[MB_ROLL_DOCNUM]+4) != 'I')
   {
      memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);

      char sDocNum[16];
      if (*(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
      {
         sprintf(sDocNum, "%.4s%.4s", apTokens[MB_ROLL_DOCNUM], apTokens[MB_ROLL_DOCNUM]+5);
         lTmp = atol(apTokens[MB_ROLL_DOCNUM]+5);
      } else
      {
         strcpy(sDocNum, apTokens[MB_ROLL_DOCNUM]);
         lTmp = atol(apTokens[MB_ROLL_DOCNUM]+4);
      }
      if (pTmp && !memcmp(acTmp, apTokens[MB_ROLL_DOCNUM], 4) && lTmp > 0 && lTmp < 9999)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, sDocNum, SIZ_TRANSFER_DOC);
      }

   } else if (*(pOutbuf+OFF_TRANSFER_DOC+4) == 'I')
   {
      memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
   }

   // Owner
   try {
      Gle_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER], apTokens[MB_ROLL_CAREOF], apTokens[MB_ROLL_DBA]);
   } catch(...) {
      LogMsg("***** Exeception occured in Gle_MergeOwner()");
   }

   // Mailing
   try {
      Gle_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Gle_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************** Gle_Load_Roll *****************************
 *
 *
 *
 ******************************************************************************/

int Gle_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A)");
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"Z\") DEL(124) ");
   else
      lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) ");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) ");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) ");
   fdTax = fopen(acTmpFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   //do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   //} while (acRollRec[0] > '9' || acRollRec[0] < '0');

   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "920000256000", 9))
      //   iTmp = 0;
#endif
      replNull(acRollRec);
      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Gle_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Gle_MergeSitus(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Gle_MergeStdChar(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acBuf, 0);

            iRollUpd++;
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Gle_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Gle_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe2(acRec, 0);

            // Merge Char
            if (fdChar)
               lRet = Gle_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acRec, iHdrRows);

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      replNull(acRollRec);

      // Create new R01 record
      iRet = Gle_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Gle_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe2(acRec, 0);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Gle_MergeStdChar(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, iHdrRows);

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   //LogMsg("Use GIS city:               %u", lUseGis);
   //LogMsg("Use Mail city:              %u\n", lUseMailCity);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   return 0;
}

/********************************** Gle_Load_Roll *****************************
 *
 *
 *
 ******************************************************************************/

int Gle_Load_OldLdr(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet, iTmp, iNewRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SMP");
   lRet = sortFile(acSalesFile, acTmpFile, "S(#1,C,A,#3,DAT,A) F(TXT) ");
   fdSale = fopen(acTmpFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acTmpFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "EMP");
   lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) ");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Write header record
   memset(acBuf, '9', iRecLen);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
#ifdef _DEBUG
      //if (!memcmp(acBuf, "030330020", 9) || !memcmp((char *)&acRollRec[1], "030330020", 9))
      //   iTmp = 0;
#endif

      // Create new R01 record
      iRet = Gle_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Gle_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);

         // Merge Char
         if (fdChar)
            lRet = Gle_MergeStdChar(acRec);

         // Merge Sales
         if (fdSale)
            lRet = Gle_MergeSale(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, iHdrRows);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }

      // Get next roll record
      do
      {
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (pTmp && cDelim == '"')
            pTmp++;
      } while (pTmp && (memcmp(pTmp, "799", 3) > 0) && (memcmp(pTmp, "900", 3) < 0 ));

      if (!pTmp)
         bEof = true;    // Signal to stop

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdSale)
      fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);

   lRecCnt = iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   return 0;
}

/********************************* Gle_Load_LDR *****************************
 *
 * Load Gle_Roll.csv into 1900-byte record.
 *
 ****************************************************************************/

int Gle_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A)");
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"Z\") DEL(124) ");
   else
      lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) ");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) ");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) ");
   fdTax = fopen(acTmpFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (acRollRec[0] > '9' || acRollRec[0] < '0');

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   lCnt = 1;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new R01 record
      iRet = Gle_MergeRoll(acBuf, acRollRec, iRecLen, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Gle_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Gle_MergeStdChar(acBuf);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acBuf, 0);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe2(acBuf, 0);

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lLDRRecCount++;
         
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else
         LogMsg("*** Skip record# %d [%.12s]", lLDRRecCount, acRollRec);

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRollRec[1]))
         break;      // EOF

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);

   lRecCnt = lLDRRecCount;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total records output:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   return 0;
}

/**************************** Gle_ConvStdChar ********************************
 
 05/17/2022 New CHAR format
 09/11/2023 New QualityClass format
 CatType 1  = Rural Property (5226)
 CatType 2  = Commercial Property (4791)
 CatType 3  = Industrial Property (127)
 CatType 4  = Residential Property (8460)
 CatType 5  = Government Property (21)
 CatType 6  = Mineral Property
 CatType 7 = Recreational Property (21)
 CatType 8 = Professional Property (3)
 CatType 9 = Quarry Property
 CatType 10 = Timber Property (7)
 CatType 11 = Undefined Property (4)
 CatType 12 = Williamson Act Property (455)
 CatType 13 = Institutional Property (17)

 *****************************************************************************/

int Gle_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acApn[32], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0, iDrop=0;
   double   dTmp;
   STDCHAR  myCharRec;

   LogMsg0("Gle_ConvStdChar - Converting char file %s", pInfile);

   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 4096, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);
      iCnt++;
      if (!pRec)
         break;

      if (acBuf[0] == ' ')
      {
         LogMsg("*** No APN at iCnt=%d near APN=%s", iCnt, acApn);
         iDrop++;
         continue;
      }

      // Parse string
      iTmp = replNull(acBuf);
      if (cDelim == '|')
         iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

      if (iFldCnt < GLE_CHAR_FLDS)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }
      //_strupr(acBuf);

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      if (*apTokens[GLE_CHAR_ASMT] >= '0')
         strcpy(acApn, apTokens[GLE_CHAR_ASMT]);
      else
      {
         strcpy(acApn, apTokens[GLE_CHAR_FEEPARCEL]);
         if (bDebug)
            LogMsg("*** ASMT not available, use FEEPARCEL %s", acApn);
      }

      iTmp = atoin(acApn, 3);
      if (!iTmp || (iTmp > 99 && iTmp != 910 && iTmp != 920))
      {
         if (bDebug)
            LogMsg("*** Skip APN %s", acApn);
         iDrop++;
         continue;
      }

      memcpy(myCharRec.Apn, acApn, strlen(acApn));
      memcpy(myCharRec.FeeParcel, apTokens[GLE_CHAR_FEEPARCEL], strlen(apTokens[GLE_CHAR_FEEPARCEL]));

      // Format APN
      iRet = formatApn(acApn, acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      // Bldg#
      iTmp = atoi(apTokens[GLE_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // CatType

      // Rooms
      iTmp = atoi(apTokens[GLE_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - prepare for future - currently not avail 02/04/2021
      iTmp = blankRem(apTokens[GLE_CHAR_POOLSPA]);
      if (iTmp > 1)
      {
         pRec = findXlatCode(apTokens[GLE_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "129310010000", 9))
      //   iRet = 0;
#endif
      // QualityClass
      vmemcpy(myCharRec.QualityClass, myLTrim(apTokens[GLE_CHAR_QUALITYCLASS]), SIZ_CHAR_QCLS);
      if (myCharRec.QualityClass[1] >= '0' && myCharRec.QualityClass[1] <= '9')
      {
         myCharRec.BldgClass = myCharRec.QualityClass[0];
         iRet = Quality2Code((char *)&myCharRec.QualityClass[1], acTmp, NULL);
         if (iRet > 0)
            myCharRec.BldgQual = acTmp[0];
      } else if (!_memicmp(apTokens[GLE_CHAR_QUALITYCLASS], "EX", 2))
      {
         myCharRec.BldgQual = 'E';
         //myCharRec.ImprCond[0] = 'E';
      } else if (!_memicmp(apTokens[GLE_CHAR_QUALITYCLASS], "VE", 2))
      {
         myCharRec.BldgQual = 'V';
         //myCharRec.ImprCond[0] = 'V';
      } else if (*apTokens[GLE_CHAR_QUALITYCLASS] > ' ')
      {
         LogMsg("*** Bad Quality: %s [%s]", apTokens[GLE_CHAR_QUALITYCLASS], apTokens[GLE_CHAR_ASMT]);
      } 

      // YrBlt
      int iYrBlt = atoi(apTokens[GLE_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[GLE_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[GLE_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count - no data 12/05/2023
      iTmp = atoi(apTokens[GLE_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      }

      // Stories/NumFloors
      dTmp = atof(apTokens[GLE_CHAR_STORIESCNT]);
      if (dTmp > 0.0 && dTmp < 99.9)
      {
         iRet = sprintf(acTmp, "%.1f", dTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[GLE_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[GLE_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport Sqft
      int iCarport = atoi(apTokens[GLE_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Parking spaces - - no data 5/15/2022
      iTmp = atoi(apTokens[GLE_CHAR_PARKINGSPACES]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.ParkSpace, acTmp, iRet);
      }

      // Patio SF
      iTmp = atoi(apTokens[GLE_CHAR_PATIOSF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // Heating 
      iTmp = iTrim(apTokens[GLE_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[GLE_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      }

      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[GLE_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[GLE_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[GLE_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[GLE_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[GLE_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[GLE_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[GLE_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace 
      if (*apTokens[GLE_CHAR_FIREPLACE] > ' ')
      {
         pRec = findXlatCode(apTokens[GLE_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      }

      // Haswell 
      blankRem(apTokens[GLE_CHAR_HASWELL]);
      if (*(apTokens[GLE_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Lot Sqft 
      if (iFldCnt >= GLE_CHAR_LOTSQFT)
      {
         ULONG    lSqft;
         lSqft = (ULONG)atol(apTokens[GLE_CHAR_LOTSQFT]);
         if (lSqft > 1)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(lSqft*SQFT_MF_1000);
            iTmp = sprintf(acTmp, "%u", (long)(dTmp+0.1));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }
      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);
   LogMsg("                    dropped: %d", iDrop);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsg("Sorting char file %s --> %s", acTmpFile, acCChrFile);
      // 11/20/2023 - iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
      // Sort on APN, BldgSeqNo, YrEff, Stories
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D,71,3,C,D) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/*************************** Gle_ConvertApnRoll *****************************
 *
 * Convert APN format from Cres to MB.  Keep old APN in PREV_APN.
 *
 ****************************************************************************/

int Gle_ConvertApnRoll(char *pInfile, char *pOutfile, int iRecordLen)
{
   FILE *fdIn, *fdOut;
   int   iTmp, iCnt=0;
   char  acBuf[2048], acTmp[32], acApn[32];

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "rb");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "wb");

   fread(acBuf, 1, iRecordLen, fdIn);
   fwrite(acBuf, 1, iRecordLen, fdOut);

   while (!feof(fdIn))
   {
      iTmp = fread(acBuf, 1, iRecordLen, fdIn);
      if (iTmp < iRecordLen)
         break;

      if (acBuf[OFF_STATUS] != 'R' && memcmp(acBuf, "050", 3) < 0)
      {
         // Format new APN - ignore last digit of old APN
         sprintf(acApn, "%.9s000", acBuf);
      
         // Save old APN to previous APN
         memcpy(&acBuf[OFF_PREV_APN], &acBuf[0], 10);

         sprintf(acTmp, "%.3s-%.3s-%.3s-000", acApn, &acApn[3], &acApn[6]);
         memcpy(&acBuf[OFF_APN_D], acTmp, 15);

         memcpy(acBuf, acApn, 12);
         fwrite(acBuf, 1, iRecordLen, fdOut);
      } else if (acBuf[OFF_STATUS] == 'R')
         LogMsg("Drop retired parcel: %.12s", acBuf);
      else
         LogMsg("Drop old parcel: %.12s", acBuf);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert Roll APN completed with %d records", iCnt);

   return 0;
}

void Gle_ConvDocNum(char *pDocNum, char *pDocDate)
{
   int   iTmp;

   if (*pDocNum == ' ' || *(pDocNum+4) > '9')
      return;

   iTmp = atoin(pDocNum, 7);
   if (iTmp > 99999)
      return;

   //iTmp = sprintf(sTmp, "%.4s-%.4d", pDocDate, iTmp);
   //memcpy(pDocNum, sTmp, iTmp);
}

int Gle_ConvertApnSale(char *pInfile, char *pOutfile)
{
   FILE *fdIn, *fdOut;
   int   iTmp1, iTmp2, iCnt=0;
   char  acBuf[1024], acApn[32], *pBuf;
   SCSAL_REC *pSale = (SCSAL_REC *)&acBuf[0];

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");

   while (!feof(fdIn))
   {
      pBuf = fgets(acBuf, 1024, fdIn);
      if (!pBuf)
         break;

      // Format new APN
      if (acBuf[9] == 'I')
         iTmp2 = 10*(0x0F & acBuf[8]) + 500;
      else if (acBuf[9] > '1' && acBuf[9] != '9' && acBuf[9] != '3' && acBuf[9] != '8')
         iTmp2 = 0; //atoin(&acBuf[8], 2);
      else
         iTmp2 = 0;

      iTmp1 = sprintf(acApn, "%.9s%.3d", acBuf, iTmp2);
      memcpy(acBuf, acApn, iTmp1);

      fputs(acBuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert Sale APN completed with %d records", iCnt);

   return 0;
}

int Gle_ConvertApnCity(char *pInfile, char *pOutfile)
{
   FILE *fdIn, *fdOut;
   int   iTmp1, iTmp2, iCnt=0;
   char  acBuf[128], acOut[128], acApn[32], *pTmp;

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");

   while (!feof(fdIn))
   {
      pTmp = fgets(acBuf, 1024, fdIn);
      if (!pTmp)
         break;

      // Format new APN
      if (acBuf[10] == 'I')
         iTmp2 = 10*(0x0F & acBuf[9]) + 500;
      else if (acBuf[9] > '1')
         iTmp2 = atoin(&acBuf[9], 2);
      else
         iTmp2 = 0;

      iTmp1 = atoin(&acBuf[7], 2);
      sprintf(acApn, "%.6s%.3d%.3d", &acBuf[1], iTmp1, iTmp2);
      pTmp = strchr(acBuf, '|');
      sprintf(acOut, "\"%s\"%s", acApn, pTmp);
      fputs(acOut, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert APN from CityZip file completed with %d records", iCnt);

   return 0;
}


/***************************** MB_CreateSCSale *******************************
 *
 * Extract sale data from ???_Sales.csv and output to ???_SALE.DAT
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * DateFmt:
 *    0 : Default. Auto format by checking input date
 *    MM_DD_YYYY_1 (MON)
 *    YYYY_MM_DD   (HUM)
 *
 * DocTypeFmt:
 *    0 : Default. Set DocType=1 (GD) if DocCode=1 or sale price > 0
 *    1 : AMA, BUT, MAD, SON, SBT
 *    2 : PLA
 *    3 : SHA, STA
 *
 * DocNumFmt:
 *    0 : Default. Copy DocNum as is
 *    1 : Format DocNum second part of DocNum to 7 digits (i.e. 2010R1234 = 2010R0001234)
 *    2 : Format DocNum second part of DocNum to n digits (i.e. 2010R0034 = 2010R34) (COL)
 *    3 : Remove all nonnumeric after 5th character and format to 6 digits (SON)
 *    4 : Format to yyyyR9999999 if R is in pos 4 or 5 of original DocNum (SBT)
 *    5 : TEH
 *    6 : GLE - format as yyyy9999 (i.e. 2012-0001 = 20120001)
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Gle_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp, lDocYear;

   LogMsg0("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Remove null CHAR
      replNull(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ' || *(apTokens[MB_SALES_DOCNUM]+4) == 'I')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "052202011000", 9))
      //   iTmp = 0;
#endif
      // Doc date
      if (iDateFmt > 0)
      {
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      } else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
      {
         lDocYear = atoin(acTmp, 4);
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      } else
         lDocYear = 0;

      // Docnum
      if (!iDocNumFmt)
      {
         // MON, NAP, PLA, SIS, SHA, LAK, MNO, YUB
         memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));
      } else if (iDocNumFmt == 6)
      {  // GLE
         int iYear, iLen;

         myTrim(apTokens[MB_SALES_DOCNUM]);
         iYear = 0;
         iLen = strlen(apTokens[MB_SALES_DOCNUM]);
         if (iLen == 7 && !memcmp(apTokens[MB_SALES_DOCNUM], "00-", 3))
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+3, 4);
            iYear = 2000;
         } else if (!memcmp(apTokens[MB_SALES_DOCNUM], "00", 2) && !memcmp(&SaleRec.DocDate[2], apTokens[MB_SALES_DOCNUM]+2, 2))
         {
            if (iLen == 8)
               lTmp = atoin(apTokens[MB_SALES_DOCNUM]+4, 4);
            else
               lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 4);
            iYear = lDocYear;
         } else if ((pTmp = strchr(apTokens[MB_SALES_DOCNUM], '-')) || (pTmp = strchr(apTokens[MB_SALES_DOCNUM], 'R')) )
         {
            lTmp = atol(pTmp+1);
            iYear = atol(apTokens[MB_SALES_DOCNUM]);
            if (iYear < 100 && lDocYear == (iYear+1900))
               iYear = lDocYear;
         } else if (iLen == 8 && isNumber(apTokens[MB_SALES_DOCNUM]))
         {
            iYear = atoin(apTokens[MB_SALES_DOCNUM], 4);
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+4, 4);
         } else
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
         }
         if (lTmp > 0 && lTmp < 9999)
         {
            if (!iYear && *apTokens[MB_SALES_DOCNUM] == '0')
               iYear = atoin(apTokens[MB_SALES_DOCNUM], 5);
            if (iYear <= lToyear && iYear > 1900)
            {
               if (iYear == lDocYear)
               {
                  sprintf(acTmp, "%d%0.4d   ", iYear, lTmp);
                  memcpy(SaleRec.DocNum, acTmp, 8);
               } else
                  LogMsg("*** Bad DocNum: %s %s (%s) [%s]", apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_DOCDATE], apTokens[MB_SALES_DOCCODE], apTokens[MB_SALES_ASMT]);
            } else if (lDocYear < 1988)
            {
               // Keep DocNum as is
               memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], iLen);
            } else
               LogMsg("*** Bad DocNum: %s [%s]", apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_ASMT]);
         }
      }

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

      // Confirmed sale price
      dTmp = atof(apTokens[MB_SALES_PRICE]);
      if (dTmp > 0.0)
      {
         lPrice = (long)((dTmp*100)/100.0);
         iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      }

      // Do not use confirmed sale price
      if (!bUseConfSalePrice)
         lPrice = 0;

      // Tax
      dTmp = atof(apTokens[MB_SALES_TAXAMT]);
      if (dTmp > 0.0)
      {
         // Save DocTax
         iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);

         // Check for bad DocTax
         if (lPrice == 0)
         {
            // Calculate sale price
            lPrice = (long)(dTmp * SALE_FACTOR);
         }

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Ignore sale price.",
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE]);
               lPrice = 0;
            }
         }
      } 

      // Ignore sale price if less than 1000
      if (lPrice > 1000)
         sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
      else
         memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
      memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);

      // Doc code - accept following code only
      if (isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         if (pDocTbl)
         {
            iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
            if (iTmp >= 0)
            {
               memcpy(SaleRec.DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
               if (lPrice < 1000)
                  SaleRec.NoneSale_Flg = pDocTbl[iTmp].flag;
            } else if (bDebug)
               LogMsg("*** Unknown DocCode: %s", apTokens[MB_SALES_DOCCODE]);
         }
      } else if (lPrice > 1000)
      {
         SaleRec.DocType[0] = '1';
      }

      // Save original DocCode
      vmemcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);

      // Transfer Type
      if (*apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
         // 07/15/2020 - Need translation, cannot store as is
         //vmemcpy(SaleRec.SaleCode, apTokens[MB_SALES_XFERTYPE], SALE_SIZ_SALECODE);
      }

      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         if (pTmp = strstr(apTokens[MB_SALES_SELLER], "C/O"))
            *pTmp = 0;
         if (pTmp = strstr(apTokens[MB_SALES_SELLER], "J/T"))
            *pTmp = 0;
         if (pTmp = strstr(apTokens[MB_SALES_SELLER], "C/P"))
            *pTmp = 0;
         if (pTmp = strstr(apTokens[MB_SALES_SELLER], "S/S"))
            *pTmp = 0;

         iTmp = replStr(apTokens[MB_SALES_SELLER], " % ", " ");
         if (iTmp > SIZ_SELLER)
         {
            // Chop it down to fit seller
            if (pTmp = strrchr(apTokens[MB_SALES_SELLER], '&'))
               *pTmp = 0;
         }
         vmemcpy(SaleRec.Seller1, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);

         // Buyer
         iTmp = replStr(apTokens[MB_SALES_BUYER], " % ", " ");
         vmemcpy(SaleRec.Name1, apTokens[MB_SALES_BUYER], SALE_SIZ_BUYER, iTmp);

         // Skip if DocNum not available
         if (SaleRec.DocNum[0] > ' ')
         {
            SaleRec.CRLF[0] = 10;
            SaleRec.CRLF[1] = 0;
            fputs((char *)&SaleRec,fdOut);
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);

   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(27,2,C,EQ,\"  \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -2;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            // Save old cumsale file
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);
            iTmp = rename(acCSalFile, acTmpFile);

            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -2;
      } else
         iTmp = rename(acOutFile, acCSalFile);

      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acCSalFile, acTmpFile);
      }
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp == -2)
      LogMsg("***** Error sorting output file");
   else if (iTmp)
      LogMsg("***** Error renaming to %s", acCSalFile);

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                          output: %d.", lTmp);
   LogMsg("         Latetest recording date: %d.", lLastRecDate);
   return iTmp;
}

///********************************* Gle_MergeLien *****************************
// *
// * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
// * Return 0 if successful, < 0 if error
// *        1 retired record, not use
// *
// *****************************************************************************/
//
//int Gle_MergeLien3x(char *pOutbuf, char *pRollRec)
//{
//   char     acTmp[256], acTmp1[64], *pTmp;
//   long     lTmp;
//   double   dTmp;
//   int      iRet=0, iTmp;
//
//   // Replace null char with space
//   iRet = replNull(pRollRec, ' ', 0);
//
//   // Parse input rec
//   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
//   if (iRet < L3_CURRENTDOCDATE)
//   {
//      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
//      return -1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//
//   // Remove hyphen from APN
//   remChar(apTokens[L3_ASMT], '-');
//
//   // Start copying data
//   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));
//
//   // Copy ALT_APN
//   iTmp = strlen(apTokens[L3_FEEPARCEL]);
//   if (iTmp < iApnLen)
//   {
//      iRet = iApnLen-iTmp;
//      memcpy(pOutbuf+OFF_ALT_APN, "0000", iRet);
//      memcpy(pOutbuf+OFF_ALT_APN+iRet, apTokens[L3_FEEPARCEL], iTmp);
//   } else
//      memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], iTmp);
//
//   // Format APN
//   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//   // Create MapLink and output new record
//   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // Create index map link
//   if (getIndexPage(acTmp, acTmp1, &myCounty))
//      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//   // County code
//   memcpy(pOutbuf+OFF_CO_NUM, "11GLE", 5);
//
//   // status
//   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];
//
//   // TRA
//   lTmp = atol(apTokens[L3_TRA]);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%0.6d", lTmp);
//      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
//   }
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // Land
//   long lLand = atoi(apTokens[L3_LANDVALUE]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: Growing Impr, Fixture, PersProp, PPMH
//   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
//   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
//   long lGrow  = atoi(apTokens[L3_GROWING]);
//   long lPers  = atoi(apTokens[L3_PPVALUE]);
//   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
//   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//
//      if (lFixtr > 0)
//      {
//         sprintf(acTmp, "%d         ", lFixtr);
//         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
//      }
//      if (lFixtRP > 0)
//      {
//         sprintf(acTmp, "%d         ", lFixtRP);
//         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
//      }
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%d         ", lPers);
//         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
//      }
//      if (lGrow > 0)
//      {
//         sprintf(acTmp, "%d         ", lGrow);
//         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
//      }
//      if (lPP_MH > 0)
//      {
//         sprintf(acTmp, "%d         ", lPP_MH);
//         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
//      }
//   }
//
//   // Gross total
//   lTmp += (lLand+lImpr);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//   // Exemption
//   long lExe1 = atol(apTokens[L3_HOX]);
//   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
//   lTmp = lExe1+lExe2;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
//      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
//   }  
//
//   iTmp = OFF_EXE_CD1;
//   if (lExe1 > 0)
//   {
//      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
//      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
//      iTmp = OFF_EXE_CD2;
//   } else
//      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
//
//   // Save exemption code
//   if (lExe2 > 0 && *apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
//      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));
//
//   // Legal
//   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);
//
//   // UseCode
//   if (*apTokens[L3_LANDUSE1] > ' ')
//   {
//      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
//   
//      // Std Usecode
//      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
//   } else
//      memcpy(pOutbuf+OFF_USE_STD, "999", 3);
//
//   // Acres
//   dTmp = atof(apTokens[L3_ACRES]);
//   lTmp = atol(apTokens[L3_LANDSIZE]);
//   if (dTmp > 0.0)
//   {
//      // Lot Sqft
//      lTmp = (long)(dTmp * SQFT_PER_ACRE);
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      // Format Acres
//      lTmp = (long)(dTmp * ACRES_FACTOR);
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   } else if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      lTmp = (long)(lTmp*SQFT_MF_1000);
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   // AgPreserved
//   //if (*apTokens[L3_ISAGPRESERVE] == '1')
//   //   *(pOutbuf+OFF_AG_PRE) = 'Y';
//
//   // Owner
//   if (*apTokens[L3_MAILADDRESS1] == '%')
//      Gle_MergeOwner(pOutbuf, apTokens[L3_OWNER], apTokens[L3_MAILADDRESS1]);
//   else
//      Gle_MergeOwner(pOutbuf, apTokens[L3_OWNER], NULL);
//
//   // Situs
//   //Gle_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);
//
//   // Mailing
//   Gle_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);
//
//   // SetTaxcode, Prop8 flag, FullExe flag
//   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);
//
//   // Recorded Doc - 2016
//   if (*apTokens[L3_CURRENTDOCNUM] > '0')
//   {
//      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
//      if (pTmp)
//      {
//         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
//         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
//      }
//   } 
//
//   //// Garage size
//   //dTmp = atof(apTokens[L3_GARAGESIZE]);
//   //if (dTmp > 0.0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_GAR_SQFT, dTmp);
//   //   memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
//   //   *(pOutbuf+OFF_PARK_TYPE) = '2';                 // GARAGE/CARPORT
//   //}
//
//   //// Number of parking spaces
//   //if (*apTokens[L3_GARAGE] == '0' || *apTokens[L3_GARAGE] == 'N')
//   //   *(pOutbuf+OFF_PARK_TYPE) = 'H';                 // None
//   //else if (*apTokens[L3_GARAGE] > '0' && *apTokens[L3_GARAGE] <= '9')
//   //{
//   //   iTmp = atol(apTokens[L3_GARAGE]);
//   //   sprintf(acTmp, "%d", iTmp);
//   //   vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
//   //   if (dTmp > 100)
//   //      *(pOutbuf+OFF_PARK_TYPE) = 'Z';              // Garage
//   //   else
//   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
//   //} else
//   //{
//   //   if (*(apTokens[L3_GARAGE]) == 'C')
//   //      *(pOutbuf+OFF_PARK_TYPE) = 'C';              // Carport
//   //   else if (*(apTokens[L3_GARAGE]) == 'A')
//   //   {
//   //      if (dTmp > 100)
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
//   //      else
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
//   //   } else if (!_memicmp(apTokens[L3_GARAGE], "DOU", 3))
//   //   {
//   //      *(pOutbuf+OFF_PARK_SPACE) = '2';
//   //      if (dTmp > 100)
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
//   //      else
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
//   //   } else if (!_memicmp(apTokens[L3_GARAGE], "TRI", 3))
//   //   {
//   //      *(pOutbuf+OFF_PARK_SPACE) = '3';
//   //      if (dTmp > 100)
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
//   //      else
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
//   //   } else if (!_memicmp(apTokens[L3_GARAGE], "SIN", 3))
//   //   {
//   //      *(pOutbuf+OFF_PARK_SPACE) = '1';
//   //      if (dTmp > 100)
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
//   //      else
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
//   //   } else if (!_memicmp(apTokens[L3_GARAGE], "GC", 2))
//   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT
//   //   else if (!_memicmp(apTokens[L3_GARAGE], "GS", 2))
//   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT ?
//   //   else if (!_memicmp(apTokens[L3_GARAGE], "DE", 2))
//   //      *(pOutbuf+OFF_PARK_TYPE) = 'D';              // Detached
//   //   else if (*(apTokens[L3_GARAGE]) == 'S')
//   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Space ?
//   //}
//
//   //// YearBlt
//   //lTmp = atol(apTokens[L3_YEARBUILT]);
//   //if (lTmp > 1800 && lTmp < lToyear)
//   //{
//   //   iTmp = sprintf(acTmp, "%d", lTmp);
//   //   memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
//   //}
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "006380014000", 9))
//   //   iTmp = 0;
//#endif
//
//   // Acres
//   dTmp = atof(apTokens[L3_ACRES]);
//   if (dTmp > 0.0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   // Lot size
//   lTmp = atol(apTokens[L3_LANDSIZE]);
//   if (lTmp > 10)
//   {
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//      if (!dTmp)
//      {
//         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(lTmp*SQFT_MF_1000));
//         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//      }
//   } else if (dTmp > 0.0)
//   {
//      //lTmp = (dTmp+0.0005)*SQFT_PER_ACRE;
//      lTmp = (long)(dTmp*SQFT_PER_ACRE);
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//   }
//
//   //// Total rooms
//   //iTmp = atol(apTokens[L3_TOTALROOMS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
//   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
//   //}
//
//   //// Stories
//   //iTmp = atol(apTokens[L3_STORIES]);
//   //if (iTmp > 0 && iTmp < 100)
//   //{
//   //   sprintf(acTmp, "%d.0", iTmp);
//   //   vmemcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
//   //}
//
//   //// Units
//   //iTmp = atol(apTokens[L3_UNITS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%d", iTmp);
//   //   vmemcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
//   //}
//
//   //// Beds
//   //iTmp = atol(apTokens[L3_BEDROOMS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_BEDS, iTmp);
//   //   memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
//   //}
//
//   //// Baths
//   //iTmp = atol(apTokens[L3_BATHS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_BATH_F, iTmp);
//   //   memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
//   //}
//
//   //// HBaths
//   //iTmp = atol(apTokens[L3_HALFBATHS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_BATH_H, iTmp);
//   //   memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
//   //}
//
//   //// Heating
//   //int iCmp;
//   //if (*apTokens[L3_HEATING] > ' ')
//   //{
//   //   iTmp = 0;
//   //   iCmp = -1;
//   //   while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_HEATING], asHeating_LDR[iTmp].acSrc, asHeating_LDR[iTmp].iLen)) > 0)
//   //      iTmp++;
//
//   //   if (!iCmp)
//   //      *(pOutbuf+OFF_HEAT) = asHeating_LDR[iTmp].acCode[0];
//   //}
//
//   //// Cooling
//   //if (*apTokens[L3_AC] == 'C')
//   //   *(pOutbuf+OFF_AIR_COND) = 'C';
//   //else if (*apTokens[L3_AC] > ' ')
//   //   LogMsg("*** Unknown A/C: %s", apTokens[L3_AC]);
//
//   //// Pool/Spa
//   //if (*apTokens[L3_POOLSPA] > ' ')
//   //{
//   //   iTmp = 0;
//   //   iCmp = -1;
//   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_POOLSPA], asPool_LDR[iTmp].acSrc, asPool_LDR[iTmp].iLen)) > 0)
//   //      iTmp++;
//
//   //   if (!iCmp)
//   //      *(pOutbuf+OFF_POOL) = asPool_LDR[iTmp].acCode[0];
//   //}
//
//   //// Fire place
//   //if (*apTokens[L3_FIREPLACE] > ' ')
//   //{
//   //   iTmp = 0;
//   //   iCmp = -1;
//   //   pTmp = _strupr(apTokens[L3_FIREPLACE]);
//   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_FIREPLACE], asFP_LDR[iTmp].acSrc, asFP_LDR[iTmp].iLen)) > 0)
//   //      iTmp++;
//
//   //   if (!iCmp)
//   //      *(pOutbuf+OFF_FIRE_PL) = asPool[iTmp].acCode[0];
//   //}
//
//   //// Quality Class
//   //acTmp1[0] = 0;
//   //if (*apTokens[L3_QUALITYCLASS] > '0' || strlen(apTokens[L3_QUALITYCLASS]) > 1)
//   //{
//   //   strcpy(acTmp, apTokens[L3_QUALITYCLASS]);
//   //   remCharEx(acTmp, " ,'?");
//   //   pTmp = _strupr(acTmp);
//
//   //   //memcpy(myCharRec.QualityClass, pTmp, strlen(pTmp));
//   //   if (acTmp[0] == 'A' && !isdigit(acTmp[1]))
//   //      acTmp1[0] = 'A';
//   //   else if (acTmp[0] == 'F' && !isdigit(acTmp[1]))
//   //      acTmp1[0] = 'F';
//   //   else if (!memcmp(pTmp, "POOR", 4) || !memcmp(pTmp, "LOW", 3))
//   //      acTmp1[0] = 'P';
//   //   else if (*pTmp == 'G')
//   //      acTmp1[0] = 'G';
//   //   else if (isalpha(*pTmp))
//   //   {
//   //      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
//   //      if (isdigit(acTmp[1]))
//   //         iRet = Quality2Code(&acTmp[1], acTmp1, NULL);
//   //      else if (isdigit(acTmp[2]))
//   //         iRet = Quality2Code(&acTmp[2], acTmp1, NULL);
//   //      else if (isalpha(acTmp[1]))
//   //      {
//   //         switch (acTmp[1])
//   //         {
//   //            case 'L':
//   //            case 'P':
//   //               acTmp1[0] = 'P';
//   //               break;
//   //            case 'A':
//   //               acTmp1[0] = 'A';
//   //               break;
//   //            case 'F':
//   //               acTmp1[0] = 'F';
//   //               break;
//   //            case 'G':
//   //               acTmp1[0] = 'G';
//   //               break;
//   //         }
//   //      }
//   //   } else if (isdigit(*pTmp))
//   //   {
//   //      iTmp = atol(pTmp);
//   //      if (iTmp < 100)
//   //         iRet = Quality2Code(pTmp, acTmp1, NULL);
//   //   }
//
//   //   if (acTmp1[0] > '0')
//   //      *(pOutbuf+OFF_BLDG_QUAL) = acTmp1[0];
//   //}
//
//   return 0;
//}
//
///******************************** Gle_Load_LDR3 *****************************
// *
// * Load LDR 2016
// *
// ****************************************************************************/
//
//int Gle_Load_LDR3x(int iFirstRec /* 1=create header rec */)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
//   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
//
//   HANDLE   fhOut;
//   FILE     *fdRoll;
//
//   int      iRet;
//   DWORD    nBytesWritten;
//   BOOL     bRet, bEof;
//   long     lRet=0, lCnt=0, lTmp;
//
//   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
//   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//
//   // Sort roll file on ASMT
//   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   lTmp = getFileDate(acRollFile);
//   if (lTmp < lToday)
//   {
//      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
//      if (!iRet)
//         return -1;
//   }
//
//   // Open roll file
//   LogMsg("Open Roll file %s", acRollFile);
//   fdRoll = fopen(acRollFile, "r");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acRollFile);
//      return -1;
//   }  
//
//   // Open Char file
//   LogMsg("Open Char file %s", acCChrFile);
//   fdChar = fopen(acCChrFile, "r");
//   if (fdChar == NULL)
//   {
//      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
//      return -2;
//   }
//
//   // Open Situs file
//   LogMsg("Open Situs file %s", acSitusFile);
//   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   lTmp = getFileDate(acTmpFile);
//   if (lTmp < lToday)
//   {
//      strcpy(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
//      if (cDelim == '|')
//         strcat(acRec, "DEL(124) ");
//      lRet = sortFile(acSitusFile, acTmpFile, acRec);
//   }
//   fdSitus = fopen(acTmpFile, "r");
//   if (fdSitus == NULL)
//   {
//      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
//      return -2;
//   }
//
//   // Open Output file
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error opening output file: %s\n", acOutFile);
//      return -4;
//   }
//
//   // Output first header record
//   if (iFirstRec > 0)
//   {
//      memset(acBuf, '9', iRecLen);
//      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//   }
//
//   // Get 1st rec
//   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
//   if (*pTmp > '9')
//      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
//
//   bEof = (pTmp ? false:true);
//
//   // Init variables
//   iNoMatch=iBadCity=iBadSuffix=0;
//
//   // Merge loop 
//   while (!feof(fdRoll))
//   {
//      lLDRRecCount++;
//      // Create new R01 record
//      iRet = Gle_MergeLien3(acBuf, acRec);
//      if (!iRet)
//      {
//         // Merge Situs
//         if (fdSitus)
//            lRet = Gle_MergeSitus(acBuf);
//
//         // Merge Char
//         if (fdChar)
//            lRet = Gle_MergeStdChar(acBuf);
//
//         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//         if (!(++lCnt % 1000))
//            printf("\r%u", lCnt);
//
//         if (!bRet)
//         {
//            LogMsg("***** Error writing to output file at record %d\n", lCnt);
//            lRet = WRITE_ERR;
//            break;
//         }
//      }
//
//      // Get next roll record
//      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
//      if (!pTmp)
//         break;
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fdChar)
//      fclose(fdChar);
//   if (fdSitus)
//      fclose(fdSitus);
//   if (fhOut)
//      CloseHandle(fhOut); 
//
//   LogMsg("Total input records:        %u", lLDRRecCount);
//   LogMsg("Total output records:       %u", lCnt);
//   LogMsg("Total bad-city records:     %u", iBadCity);
//   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
//   LogMsg("Number of Char matched:     %u", lCharMatch);
//   LogMsg("Number of Char skiped:      %u", lCharSkip);
//   LogMsg("Number of Situs matched:    %u", lSitusMatch);
//   LogMsg("Number of Situs skiped:     %u\n", lSitusSkip);
//   printf("\nTotal output records: %u\n", lCnt);
//
//   lRecCnt = lCnt;
//   return 0;
//}

/********************************* Gle_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Gle_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Ignore APN starts with 100-999 except 910 (MH), 920 (common area)
   iTmp = atoin(apTokens[L3_ASMT], 3);
   if (!iTmp || (iTmp > 99 && iTmp != 910 && iTmp != 920))
      return 1;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "11GLE", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&GLE_Exemption);

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   //if (*apTokens[L3_ISAGPRESERVE] == '1')
   //   *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   if (*apTokens[L3_MAILADDRESS1] == '%')
      Gle_MergeOwner(pOutbuf, apTokens[L3_OWNER], apTokens[L3_MAILADDRESS1]);
   else
      Gle_MergeOwner(pOutbuf, apTokens[L3_OWNER], NULL);

   // Situs
   //Gle_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Gle_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*apTokens[L3_CURRENTDOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   return 0;
}

/******************************** Gle_Load_LDR3 *****************************
 *
 * Load LDR 2016
 *
 ****************************************************************************/

int Gle_Load_LDR3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Value file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Lien file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   } else
      fdLienExt = NULL;

   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SMP");
   lRet = sortFile(acSitusFile, acTmpFile, "S(1,13,C,A)");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(1,13,C,A) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      // Create new R01 record
      iRet = Gle_MergeLien3(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         if (fdLienExt)
            lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Situs
         if (fdSitus)
            lRet = Gle_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Gle_MergeStdChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/*********************************** loadGle ********************************
 *
 * Options:
 *
 *    -CGLE -U [-T|-Ut] [-Xa] [Xs|Ms] [-Mr] [-Mz] [-O]   (Normal update)
 *    -CGLE -L -Xl -Ms|Xs [-Xa] [-Mr] -O (LDR)
 *    -CGLE -T|-Ut [-O] (load tax)
 *
 ****************************************************************************/

int loadGle(int iSkip)
{
   int   iRet=0;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;
   iApnLen = myCounty.iApnLen;

   if (bUpdPrevApn)
   {
      // If not defined, use current apn length
      iRet = GetPrivateProfileInt(myCounty.acCntyCode, "PrevApnLen", iApnLen, acIniFile);
      iRet = updatePrevApn(myCounty.acCntyCode, iRet, iSkip);
   }

   //char sInfile[_MAX_PATH], sOutfile[_MAX_PATH];
   //sprintf(sInfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   //sprintf(sOutfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   //iRet = Gle_ConvertApnRoll(sInfile, sOutfile, 1900);
   //sprintf(sInfile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Org");
   //sprintf(sOutfile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   //iRet = Gle_ConvertApnSale(sInfile, sOutfile);
   //strcpy(sInfile, "H:\\CO_PROD\\SGLE_CD\\raw\\teh_cityzip.csv");
   //strcpy(sOutfile, "H:\\CO_PROD\\SGLE_CD\\raw\\teh_cityzip.srt");
   //iRet = Gle_ConvertApnCity(sInfile, sOutfile);

   if (iLoadTax == TAX_LOADING)
   {
      // Load tax base
      iRet = MB_Load_TaxBase(bTaxImport, false, iTaxGrp, iHdrRows);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet = MB_Load_TaxCodeMstr(bTaxImport, iHdrRows);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport, iHdrRows);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport, iHdrRows);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
   }

   if (!iLoadFlag)
      return iRet;

   // Clean up cum sale file
   //iRet = Gle_CleanSaleFile(acCSalFile);

   // Extract lien file
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acValueFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acValueFile, 0);      // 2016 
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode);
   }

   // Extract Sale file from Gle_Sales.txt to Gle_Sale.sls
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Doc date - input format MM/DD/YYYY
      iRet = Gle_CreateSCSale(MM_DD_YYYY_1, 0, 6, false, &GLE_DocCode[0]);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      iRet = Gle_ConvStdChar(acCharFile);
      if (iRet <= 0)
      {
         LogMsg("***** Error extracting attributes data from %s", acCharFile);
         return -1;
      }
   }

   // Open CityZip file
   //iRet = initSitus(acIniFile, myCounty.acCntyCode);
   //if (iRet < 0)
   //   return iRet;

   if (iLoadFlag & LOAD_LIEN)
   {
      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      iRet = Gle_Load_LDR3(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Gle_Load_Roll(iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Gle_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
   }

   return iRet;
}
