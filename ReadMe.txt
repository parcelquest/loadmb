Work in progress:
1) Keep original owner name: HUM, MNO
2) Remove all Create???Roll() and use ???_Load_LDR() instead.  Only SON still have for sample.
3) LDR files has three groups L, L1, & L2.  L & L2 are mostly the same.  Only that L2 has couple
   more fields at the end.
4) Roll update has six groups divided as follow:

Group 1: AMA, BUT, CAL, COL, HUM, MAD, MER, MON, PLU, SON, YUB
Ama_Roll.csv
Ama_Situs.csv
Ama_Exe.csv
Ama_Char.csv
Ama_Tax.csv
Ama_Sales.csv

Group 2: PLA
AsmtMstr.csv
AsrCodeList.csv
CurrentValues.csv
Exemptions.csv
PCCodeList.csv
PropChar.csv
TransferHistory.csv

Note: AsmtMstr.csv+CurrentValues.csv = AsmtAddressSitusValue.csv

Group 3: LAK, SHA
AsmtAddressSitusValue.csv
AsrCodeList.csv
Exemptions.csv
PCCodeList.csv
PropChar.csv
TaxRollInfo.csv (Tax info)
TransferHistory.csv

Group 4: NAP, MNO
Napa_Roll.csv
Napa_Situs.csv
Napa_Exe.csv
Napa_Char.csv
Napa_Tax.csv
Napa_Sales.csv
 
Group 5: STA
SecuredRoll.mdb
Sta_Situs.csv
Sta_Char.csv
Sta_Tax.csv

Group 6: SIS
Sis_Roll.csv
Sis_Sales.csv
Sis_MHAtt.csv
Sis_MiscBldgs.csv
Sis_Code.csv
Sis_Pccode.csv


