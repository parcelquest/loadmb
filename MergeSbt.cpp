/**************************************************************************
 *
 * Notes:
 *    1) Review Sbt_ExtrLien() before loading LDR.
 *    2) Use MergeSbtRoll() instead of Sbt_LoadRoll() until county corrects
 *       their data problem
 *
 * Options:
 *    -L  (load lien)
 *    -U  (load update)
 *    -Xl (extract lien value)
 *    -Xs (extract sale data)
 *    -O  (overwrite log file)
 *
 * Normal update: -CSBT -U -O -Xs
 *
 * Revision
 * 09/23/2008 8.3.5    Copy from MergeMer.cpp to create this file
 *                     Temporary not process CHAR, SITUS, and ACRES data
 *                     until county correct data on their site.
 * 11/26/2008 8.4.2    Update MergeChar() and MergeSitus() to work around data
 *                     problem from the county.
 * 12/03/2008 8.5.0    Use Acres values that < 1000 acres.  Values greater 
 *                     than this seems to be bogus
 * 03/30/2009 8.7.1    Use MB_MergeSale() to do sale update instead of MergeSale3() to fix 
 *                     problem that non-sale transaction overwrites sale transaction that  
 *                     occurs in the same day  since we only keep one transaction per day.
 * 06/08/2009 8.7.7    Add support for multi-row header.
 * 08/10/2009 9.1.6    Use M_CITY if S_CITY is blank and have same street name.
 *                     SBT is now uses MB grp 1 layout, same as NAP, for LDR.
 * 11/02/2009 9.2.8    The 20080806 is a place holder for XFER_DOC in the roll file.
 *                     The one is the sale file is OK.
 * 08/06/2010 10.3.0   Replace replChar() with replNull() to fix bug in Sbt_MergeLien().
 *                     Save TAXCODE & EXE_CD.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 06/09/2011 10.9.1   Exclude HomeSite from OtherVal.  Replace Sbt_MergeExe() with MB_MergeExe(),
 *                     Sbt_MergeTax() with MB_MergeTax(), and MB_CreateSale() with MB_CreateSCSale().
 * 07/25/2011 11.2.3   Add S_HSNO. Merge situs using monthly file when loading LDR.
 * 07/31/2012 12.2.3   Modify Sbt_MergeLien() to reformat ALT_APN and TRA.
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 07/29/2013 13.8.8.1 Modify Sbt_MergeLien() to use cLdrSep.
 * 10/01/2013 13.13.2  If DocNum from roll file looks funky, copy as is to TRANSFER_DOC.
 * 10/02/2013 13.14.0  Rewrite MergeOwner() to update Vesting.
 * 01/06/2014 13.19.0  Add DocCode[] table. Remove sale/lien update from Sbt_Load_Roll() and 
 *                     use ApplyCumSale() to update sale history. 
 * 02/23/2014 13.20.4  Modify Sbt_MergeRoll() to ignore transfer if bad DocNum.
 * 07/31/2014 14.1.1   Fix Sbt_MergeMAdr (LDR version) to capture C/O when there are more then
 *                     two lines. Modify Sbt_MergeSitus() to add Unit# & Zip if avail.
 *                     Add Sbt_MergeCurRoll() to update transfer when loading LDR.
 *                     Add Sbt_MergeLien2(), Sbt_Load_LDR2(), Sbt_ExtrLien() to deal with new LDR layout.
 * 02/25/2015 14.14.1  Fix DocNum in Sbt_MergeRoll().
 * 07/31/2015 15.2.0   Add DBA to Sbt_MergeMAdr()
 * 08/03/2015 15.2.1   Modify Sbt_MergeMAdr() to check for more cases of DBA. Modify Sbt_Load_Roll()
 *                     to skip header of roll file since it's already been sorted. 
 *                     Modify Sbt_Load_LDR2() to test header row by not using isdigit().
 * 01/04/2016 15.9.0   Add option to load TC file by calling Load_TC()
 * 08/02/2016 16.1.0   Add Sbt_Load_LDR4() to support 2016 LDR layout.  Add -Xa option.
 * 07/28/2017 17.2.1   Add Sbt_MergeLien3() & Sbt_Load_LDR3() to support 2017 LDR layout.
 * 08/03/2017 17.2.4   Fix bug in Sbt_MergeLien3() to change county code to "SBT"
 * 03/27/2018 17.9.0   Add -Ut option to update tax.
 * 04/11/2018 17.10.2  County has changed layout of tax & char layout.  Modify Sbt_MergeSitus(),
 *                     Sbt_MergeRoll(), Sbt_Load_Roll() to remove bad char before parsing.
 *                     Add Sbt_ConvStdChar() and use MB_MergeStdChar() to update char data.
 * 04/12/2018 17.10.2.2 Remove hardcoded delimiter and use cDelim.
 *                     Fix bug in Sbt_ConvStdChar() that ignores single char poll code.
 * 07/31/2018 18.2.2   Use MergeStdChar() in all Load_LDR() and modify Sbt_ConvStdChar() to sort on output only.
 * 08/01/2019 19.1.1   Add AgPreserve to Sbt_MergeLien3().
 * 01/15/2020 19.6.5   Change tax loading process to use daily county data (like TEH)
 * 11/01/2020 20.4.2   Modify Sbt_MergeRoll() to populate default PQZoning.
 * 08/10/2021 21.1.8   Modify Sbt_MergeLien3() to check for bad LotAcres.  Create SETACREAGE.
 * 08/01/2023 23.1.0   Fix bug in Las_ConvStdChar() when record is empty.
 *                     Fix HO_FLG in Sbt_MergeLien3() (set default to 2).
 * 01/24/2024 23.5.2   Set iRet=0 in loadSbt() before processing to make sure other functions works after exit.
 * 07/31/2024 24.0.4   Modify Sbt_MergeLien3() to add ExeType.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "PQ.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"

#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "Tax.h"
#include "CharRec.h"
#include "MergeSbt.h"

extern   bool  bEnCode;

#define SETACREAGE \
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp); \
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT); \
      lTmp = (long)(lTmp*SQFT_MF_1000); \
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp); \
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES); 

/******************************** Sbt_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sbt_MergeOwner1(char *pOutbuf, char *pNames)
{
   int   iTmp, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1, *pTmp2;
   char  acName1[64];
   bool  bKeep;
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);
   memset(acTmp, 0, 128);
   strcpy(acTmp1, pNames);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001011002", 9))
   //   iTmp = 0;
#endif

   strcpy(acTmp, pNames);
   if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Eliminate ETAL
   if ((pTmp=strstr(acTmp, " ETAL"))  || (pTmp=strstr(acTmp, " ET AL")) ||
       (pTmp=strstr(acTmp, " ETUX"))  || (pTmp=strstr(acTmp, " ET UX")) ||
       (pTmp=strstr(acTmp, " ETVIR")) || (pTmp=strstr(acTmp, " ET VIR")) )
      *++pTmp = 0;

   // Replace - with & and move it to name1.  If more than one found, drop
   // everything after the second one except when only one word goes before
   // the first '-'
   if (pTmp = strrchr(acTmp, '-'))
   {
      *pTmp++ = 0;

      // Remove TRUST in middle of name: 
      // BARBOSA RAYMOND A TRUST-VIRGINIA
      if ((pTmp1 = strstr(acTmp, "REV LIV TR")) || (pTmp1 = strstr(acTmp, "TRUST")) )
         *pTmp1 = 0;

      // Search for second '-'
      // FIGUEROA SILVINO-NATIVIDAD C-JOSIE C 
      // ANDERSON-PAPILLION NATHAN-GERLIE
      // HANETA ATAE-SUMIKO REV LIVE TR-SUMIKO
      if (pTmp2 = strchr(acTmp, '-'))
      {
         *pTmp2 = 0;       // Temporarily remove '-'
         if (pTmp1 = strchr(acTmp, ' '))
         {
            pTmp = pTmp2+1;
         } else
            *pTmp2 = '-';  // Put '-' back and keep it in name1
      }
      sprintf(acTmp1, "%s & %s ", acTmp, pTmp);
      strcpy(acTmp, acTmp1);
   }

   // Remove TRUSTEES
   if ((pTmp1=strstr(acTmp, " TRUSTEE")) || (pTmp1=strstr(acTmp, " TRS")) )
      *pTmp1 = 0;

   // Save Owner1
   strcpy(acSave1, acTmp);
   strcpy(acName1, acTmp);

   // Remove TRUST to format swapname
   if ((pTmp1=strstr(acName1, " REVOC "))    || 
       (pTmp1=strstr(acName1, " REVOCABLE")) ||
       (pTmp1=strstr(acName1, " REV "))      || 
       (pTmp1=strstr(acName1, " IRREV"))     ||
       (pTmp1=strstr(acName1, " FAMILY "))   ||
       (pTmp1=strstr(acName1, " FAM "))      ||
       (pTmp1=strstr(acName1, " LIVING "))   ||
       (pTmp1=strstr(acName1, " LIV "))      ||
       (pTmp1=strstr(acName1, " TESTAMENTARY")) || 
       (pTmp1=strstr(acName1, " INTER VIVOS"))  ||
       (pTmp1=strstr(acName1, " DECLARATION OF TRUST")) || 
       (pTmp1=strstr(acName1, " TRUST"))     ||
       (pTmp1=strstr(acName1, " TR ")) 
      )
   {
      pTmp = acName1;
      while (pTmp < pTmp1 && !isdigit(*(pTmp)))
         pTmp++;
      if (pTmp < pTmp1)
         *--pTmp = 0;
      else
         *pTmp1 = 0;
   } else if ((pTmp1=strstr(acName1, " LIFE EST")) ) 
       *pTmp1 = 0;

   // Do not parse if there is certain word in name
   if (strstr(acName1, " OF ")) 
      bKeep = true;
   else
      bKeep = false;

   // Parse owner
   blankRem(acName1);
   if (!bKeep && strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      // If name is not swapable, use Name1 for it
      if (!myOwner.acSwapName[0] )
         strcpy(myOwner.acSwapName, acName1);
   } else
      strcpy(myOwner.acSwapName, acSave1);

   // Keep owner name the same as county provided
   iTmp = strlen(pNames);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME1, pNames, iTmp);

   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
}

/******************************** Sbt_CleanName ******************************
 *
 * Return 99 if found a vesting
 *
 *****************************************************************************/

int Sbt_CleanName(char *pSrcName, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], *pTmp;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);

   if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

void Sbt_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iRet;
   char  acName1[64], acVesting[8], acTmp[128], *pTmp, *pTmp1;
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002070006000", 9))
   //   iTmp = 0;
#endif

   // Remove multiple spaces
   pTmp = strcpy(acTmp, pNames);
   iTmp = blankRem(acTmp);
   if (iTmp > SIZ_NAME1) iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp);
   strcpy(acName1, acTmp);

   iTmp = Sbt_CleanName(acTmp, acName1, acVesting);
   if (iTmp == 99)
   {
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

      // Check EtAl
      if (!memcmp(acVesting, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   }

   // If number appears at the beginning of name, do not parse
   if (acName1[0] < 'A')
   {
      memcpy(pOutbuf+OFF_NAME_SWAP, pOutbuf+OFF_NAME1, SIZ_NAME1);
      return;
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   if ((pTmp1=strchr(acName1, ' ')) && !(acVesting[0] > ' ' && isVestChk(acVesting)))
   {
      // Translate "-" into " & ", but do not translate combined name
      if ((pTmp = strchr(acName1, '-')) && pTmp > pTmp1)
      {
         if (pTmp = strchr(pTmp+1, '-'))
            *pTmp = 0;

         replStr(acName1, "-", " & ");
      }

      iRet = splitOwner(acName1, &myOwner, 3);
      if (iRet == -1)
      {
         memcpy(myOwner.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
         myOwner.acSwapName[SIZ_NAME1] = 0;
      }
   } else
   {
      memcpy(myOwner.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
      myOwner.acSwapName[SIZ_NAME1] = 0;
   }

   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
}

/******************************** Sbt_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Sbt_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64];
   int      iTmp;
   char *    pTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Skip test address
   if (!memcmp(apTokens[MB_ROLL_M_ADDR], "XXX", 3) || !memcmp(apTokens[MB_ROLL_M_ADDR], "000", 3))
      return;

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   } 

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
     sMailAdr.strName[SIZ_M_STREET] = 0;
     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      memcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], strlen(apTokens[MB_ROLL_M_CITY]));
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         iTmp = strlen(apTokens[MB_ROLL_M_ZIP]);
         if (iTmp > SIZ_M_ZIP)
         {
            pTmp = apTokens[MB_ROLL_M_ZIP]+SIZ_M_ZIP;
            if (*pTmp == '-')
            {
               memcpy(pOutbuf+OFF_M_ZIP4, pTmp+1, strlen(pTmp+1));
               iTmp = SIZ_M_ZIP;
            } else if (iTmp > 9)
               iTmp = 9;
         }
         memcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], iTmp);
      }

      iTmp = sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
      if (iTmp > SIZ_M_CTY_ST_D)
         iTmp = SIZ_M_CTY_ST_D;
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, iTmp);
   }

}

/********************************* Sbt_MergeMAdr *****************************
 *
 * Notes:
 *    - Mail addr is provided in various form.  We try the best we can since
 *      there is no way to have 100% covered.
 *
 *    - Sample Input:
 *      AMERICAS SERVICING CO           3476 STATEVIEW BLVD             FORECLOSURE MAC#7801-013        FT MILL SC 29715
 *      ALFRED F & IRENE G DEMELO       DBA WALGREENS #6418-S-P         300 WILMOT RD MS#1435           DEERFIELD IL 60015-5121
 *      AD VALOREM TAX DEPT (UNIT 3505) DBA BEACON OIL COMPANY          ONE VALERO WAY                  SAN ANTONIO TX 78249-1616
 *      ATTN  DAVE ARNAIZ               PMB 330                         PO BOX 2818                     ALPHARETTA GA 30023
 *      C/O BANK OF AMERICA             ATTN: BURR WOLFF LP             505 CITY PARKWAY WEST STE 100   ALPHARETTA GA 30023
 *      C/O CITIMORTGAGE, INC.          MAILSTOP 02-25                  27555 FARMINGTON RD             FARMINGTON HILLS MI 48334-3357
 *      RE: LOAN# 0089214605/BALTAZAR   2300 BROOKSTONE CENTRE PKWY     PO BOX 84013                    PO BOX 84013
 *
 *****************************************************************************/

void Sbt_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *pCareOf, *pDba, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64], acDba[128];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002030004000", 9) )
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   pCareOf = pDba = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         pCareOf = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         pCareOf = pLine2;
         p1 = pLine3;
         if (!_memicmp(pLine1, "DBA ", 4) )
            pDba = pLine1+4;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         p1 = pLine3;
         sprintf(acDba, "%s %s", pLine1+4, pLine2);
         pDba = &acDba[0];
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         pCareOf = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         pDba = pLine1+4;
         p1 = pLine2;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
         {
            p1 = pLine2;
            pCareOf = pLine1;
         }
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
   }

   // Update DBA
   if (pDba)
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

   // Check for C/O
   if (pCareOf)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      iTmp = blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, iTmp);

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      } else
      {
         if (iTmp > SIZ_M_STREET)
            iTmp = SIZ_M_STREET;
         memcpy(pOutbuf+OFF_M_STREET, acAddr1, iTmp);
      }
   }

   strcpy(acAddr2, p2);
   blankRem(acAddr2);

   iTmp = strlen(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D)
      iTmp = SIZ_M_CTY_ST_D;
   memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      iTmp = strlen(sMailAdr.City);
      if (iTmp > SIZ_M_CITY) iTmp = SIZ_M_CITY;
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, iTmp);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      iTmp = strlen(sMailAdr.Zip);
      if (iTmp > SIZ_M_ZIP)
         iTmp = SIZ_M_ZIP;
      memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, iTmp);
   }
}

/****************************** Sbt_ParseAdr() ******************************
 *
 * Parse address line 1 that has no street number, so parse
 * street name, suffix, and/or community.  Suffix will be coded
 *
 * StrName may contain strType and city abbr
 * i.e.
      SAN JOSE ST-SJB
      SECOND & JEFFERSON STS
      TAHUALAMI & 4TH ST SJB
      3RD & MONTEREY SJB
      MONTEREY ST SJB
      CHURCH & THIRD ST.  SJB
      THIRD ST.  SJB.
      NORTH ST.  SJB
      9A-409B SIXTH
      & 702 4TH
      3RD & 27 SAN JOSE STS SJ
      3RD ST-SJB-PINK HOUSE
      9 THOMAS LN (OFF 1ST ST-
      NORTH/THIRD LOT 4
      RIVER OAKS PARK/CHITTEND
      NORTH ST LOT 3
      1ST ST-SAN JUAN BAUTISTA
      3RD ST SAN JUAN BAUTISTA
      7TH ST PCL 8
      7TH STREET REAR
      4TH ST-CORNER WASHINGTON
      SOUTH SIDE OF HWY 156
      ARPENTERIA E SIDE
      THE ALAMEDA-SAN JUAN INN
      1/2 THE ALAMEDA (GROSCUP
      SAN JUAN-HOLLISTER
      CHITTENDEN RD NO OF HWY 
      FOREST & ANZAR RDS. AROM
      FOREST RD AROMAS
      SCHOOL ROAD-AROMAS
      ROSE AVE "SPRING LOT"
      "A" ROSE AVE-AROMAS
 *
 ****************************************************************************

void Sbt_ParseAdr(ADR_REC *pAdrRec, ADR_REC *pSitus)
{
   char  acAdr[128], acStr[64], acTmp[64], *apItems[16], *pTmp, *pTmp1;
   int   iCnt, iIdx, iSfxCode;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));
   strcpy(acAdr, pSitus->strName);

   // Check for unit #
   //   1 VIA PADRE UNIT #23
   //   SAN FELIPE RD, UNITS 1 T
   //   CANAL ALLEY UNITS A & B
   if ((pTmp = strstr(acAdr, " UNIT")) || (pTmp = strstr(acAdr, "-UNIT")))
   {
      // Only process Unit# id strNum is present
      if (*(pTmp+5) == 'S' || *(pTmp+5) == ' ')
      {
         pTmp1 = pTmp+6;
         if (*(pTmp+5) == 'S')
            pTmp1++;
         strncpy(pAdrRec->Unit, pTmp1, SIZ_M_UNITNO);
         *pTmp = 0;
      }
   }

   // Check for trailing community
   if (pTmp=strrchr(acAdr, '-'))
   {
      pTmp1 = pTmp + 1;
      if (*pTmp1 == ' ')
         pTmp1++;

      if (!memcmp(pTmp1, "SAN JUAN", 8) || !memcmp(pTmp1, "SJ", 2))
      {
         // SJB
      } else if (!memcmp(pTmp1, "AROMA", 5))
      {
      }else if (!memcmp(pTmp1, "N SIDE", 6))
      {
      }else if (!memcmp(pTmp1, "S SIDE", 6))
      {
      } else
      {
      }
   }

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
      return;

   if (iCnt > 2 && (!strcmp(apItems[iCnt-2], "STE")))
   {
      strncpy(&pAdrRec->Unit[1], apItems[iCnt-1], SIZ_M_UNITNO-1);
      iCnt -= 2;
   } else if (iCnt > 2 && (!strcmp(apItems[iCnt-2], "SP")))
   {
      sprintf(acTmp, "SP %s", apItems[iCnt-1]);
      strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
      iCnt -= 2;
   }


   acTmp[0] = 0;

   if (iSfxCode = GetSfxDev(apItems[iCnt-1]))
   {
      // Multi-word street name with suffix
      if (bEnCode)
         sprintf(pAdrRec->strSfx, "%d", GetSfxCode(GetSfxStr(iSfxCode)));
      else
         strcpy(pAdrRec->strSfx, GetSfxStr(iSfxCode));

      while (iIdx < iCnt-1)
      {
         strcat(acStr, apItems[iIdx++]);
         if (iIdx < iCnt)
            strcat(acStr, " ");
      }
   } else
   {
      // Multi-word street name without suffix
      while (iIdx < iCnt)
      {
         strcat(acStr, apItems[iIdx++]);
         if (iIdx < iCnt)
            strcat(acStr, " ");
      }
   }

   // Copy street name
   acStr[SIZ_M_STREET] = 0;
   strcpy(pAdrRec->strName, acStr);
}

/******************************** Sbt_MergeSAdr ******************************
 *
 * Notes:
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/
bool Sbt_ChkSfx(char *pSfx)
{
   bool bRet = false;

   if (!memcmp(pSfx, "DR",  2) ||
       !memcmp(pSfx, "RD",  2) ||
       !memcmp(pSfx, "ST",  2) ||
       !memcmp(pSfx, "CT",  2) ||
       !memcmp(pSfx, "AVE", 3) ||
       !memcmp(pSfx, "ST",  2) ||
       !memcmp(pSfx, "HWY", 3) ||
       !memcmp(pSfx, "LN",  2) ||
       !memcmp(pSfx, "WAY", 3) ||
       !memcmp(pSfx, "CIR", 3)       
      )
      bRet = true;

   return bRet;
}

int Sbt_ParseSAdr(ADR_REC *pAdrRec, ADR_REC *pSitus)
{
   char  acAdr[128], acStrName[128], acSfx[8], acTmp[128];
   char  *pTmp, *apItems[16], acCity[64];
   int   iStrNo, iTmp, iCnt, iSfxCode, iIdx, iRet;
   bool  bAnd;

   // Initialize
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));
   acCity[0]=acStrName[0] = 0;
   strcpy(acAdr, pSitus->strName);
   remChar(acAdr, 34);

   // Parse str name
   iIdx = 0;
   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
   {
      memcpy(pAdrRec, pSitus, sizeof(ADR_REC));
      return 0;
   }

   iStrNo = 0;
   if (pSitus->lStrNum > 0)
      iStrNo = pSitus->lStrNum;
   else
   {
      // If first token is not whole number, put everything to strName
      if (!isNumber(apItems[iIdx]) && 
          !strchr(apItems[iIdx], '-') && 
          !strchr(apItems[iIdx], ',') &&
          *(apItems[iIdx]+strlen(apItems[iIdx])-2) >= 'N'
         )
      {
         strncpy(pAdrRec->strName, pSitus->strName, SIZ_M_STREET);
         pAdrRec->lStrNum = 0;
         return 0;
      } else
      {
         iStrNo = atoi(apItems[iIdx]);
         if (iStrNo == 0)
         {
            strncpy(pAdrRec->strName, pSitus->strName, SIZ_M_STREET);
            pAdrRec->lStrNum = 0;
            return 0;
         }
      }
   }

   pAdrRec->lStrNum = iStrNo;

   // Validate StrNum
   // 416-A ROSE AVE
   // 106-108-110-112 PEARCE LANE SJB
   if (pTmp = strchr(apItems[iIdx], '-'))
   {
      *pTmp++ = 0;
      if (isalpha(*pTmp))
         strncpy(pAdrRec->strSub, pTmp, SIZ_M_STR_SUB);
   } else if (*apItems[iIdx+1] == '&')
   {
      //81 & 81-A 4TH ST SJB
      pTmp = apItems[iIdx+1];

      if (!*(pTmp+1))
         iIdx += 2;
      else
         iIdx++;
   } else if (iCnt > 4 && 
             (strlen(apItems[iIdx+1]) == 1 || strchr(apItems[iIdx+1], '-')) && 
              *apItems[iIdx+2] == '&' )
   {
      // 803 A & B MARENTES CT - Drop A & B
      // 540 A & 1026-1028 WEST ST
      if ((strlen(apItems[iIdx+3]) == 1) || isdigit(*(apItems[iIdx+3]))) 
         iIdx += 3;
   } else if ((pTmp = strchr(apItems[iIdx+1], '-')) && (*(pTmp-1) < 'A' || *(pTmp+1) < 'A'))
   {
      // 40 9A-409B SIXTH AVE
      iIdx++;
   } else if (*apItems[iIdx+1] != '&' && strchr(apItems[iIdx+1], '&'))
   {
      // Skip multi-unit
      // A,B,&C CAPUTO CT
      iIdx++;
   } else if (strchr(apItems[iIdx+1], ','))
   {
      // 1021 A, B, & C CAPUTO CT
      iTmp = 2;
      bAnd = false;
      while (iTmp+iIdx < iCnt)
      {
         if (*(apItems[iIdx+iTmp]) == '&') 
            bAnd = true;
         else if (!strchr(apItems[iIdx+iTmp], ',') )
         {
            if (bAnd)
               iTmp++;
            break;
         }

         iTmp++;
      }
      iIdx = iTmp-1;
   } else if (*(apItems[iIdx+1]+1) == '-')
   {
      iIdx++;
      remChar(apItems[iIdx], '-');
      strncpy(pAdrRec->Unit, apItems[iIdx], SIZ_M_UNITNO);
   }

   // Copy StrNum
   iTmp = sprintf(acTmp, "%d       ", iStrNo);
   strncpy(pAdrRec->strNum, acTmp, SIZ_M_STRNUM);

   if (iIdx > iCnt-2)
   {
      LogMsg("Bad situs: %s", pSitus->strName);
      return 1;
   } else
      iIdx++;

   // Check for StrSub
   if (*(apItems[iIdx]) == '\"')
   {
      pTmp = apItems[iIdx];
      pAdrRec->strSub[0] = *++pTmp;
      iIdx++;
   }

   iSfxCode = 0;
   // Check for fraction
   if (!memcmp(apItems[iIdx], "1/2", 3))
   {
      strcpy(pAdrRec->strSub, "1/2");
      iIdx++;
   }

   acTmp[0] = 0;

   // Check for unit #
   if (apItems[iCnt-1][0] == '#' && strcmp(apItems[iCnt-2], "HWY"))
   {
      // NORTH SCHOOL ST #1
      // 1551 MEMORIAL DR  #1 THRU #4
      if (iCnt > 3 && !strcmp(apItems[iCnt-2], "THRU"))
      {
         sprintf(pAdrRec->Unit, "%s-%s", apItems[iCnt-3], apItems[iCnt-1]+1);
         iCnt -= 3;
      } else
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (iCnt > 2 && 
            (!strcmp(apItems[iCnt-1], "STE") || 
             !strcmp(apItems[iCnt-1], "SUITE") ||
             !strcmp(apItems[iCnt-1], "UNIT") ))
            iCnt--;
      }
   } else if (iCnt > 3 && (!strcmp(apItems[iCnt-2], "STE") ||
                           !strcmp(apItems[iCnt-2], "SUITE") ||
                           !strcmp(apItems[iCnt-2], "APT")))
   {
      strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
      iCnt -= 2;
   }

   if (!iSfxCode)
   {
      pTmp = NULL;
      while (iIdx < iCnt)
      {
         if (pTmp = strchr(apItems[iIdx], '-'))
            *pTmp++ = 0;

         // Format sfxCode if street name is already identified
         if ((iSfxCode = GetSfxCodeX(apItems[iIdx], acSfx)) &&
             !(iCnt > iIdx+1 && (iTmp = GetSfxCodeX(apItems[iIdx+1], acTmp))) &&
             !(!memcmp(apItems[iIdx], "HWY", 3) && apItems[iIdx+1] && isdigit(*apItems[iIdx+1]) ) &&
             acStrName[0]
            )
         {
            if (!memcmp(apItems[iIdx-1], "WAY", 3) && !memcmp(apItems[iIdx], "CR", 2) )
               iSfxCode = GetSfxCodeX("CIR", acSfx);

            // Filter out valid sfx in SBT
            STRSFX *pSfx = GetSfxCode(acSfx, &Sbt_SfxTbl[0]);
            if (pSfx)
            {
               sprintf(pAdrRec->SfxCode, "%d", iSfxCode);
               strcpy(pAdrRec->strSfx, pSfx->pStrSfx);
               iIdx++;
               break;
            } else
            {
               strcat(acStrName, apItems[iIdx++]); 
               if (iIdx < iCnt)
                  strcat(acStrName, " ");
            }
         } else
         {
            if (!memcmp(apItems[iIdx], "LN", 2))
               strcat(acStrName, "LANE");
            else
            {
               if (iSfxCode > 0 && iSfxCode < 6)
               {
                  sprintf(pAdrRec->SfxCode, "%d", iSfxCode);
                  strcpy(pAdrRec->strSfx, acSfx);
                  iIdx++;
                  break;
               } else
                  strcat(acStrName, apItems[iIdx]);
            }
            iIdx++;
            if (iIdx < iCnt)
               strcat(acStrName, " ");

            if (pTmp)
               break;
         }
      }

      if (pTmp)
      {
         strcpy(acCity, pTmp);
         if (pTmp = strchr(acCity, '-'))
            *pTmp = 0;
         strcat(acCity, " ");
      }

      // Check for multiple address in strName
      if (pTmp = strchr(acStrName, '&'))
         *pTmp = 0;
   }

   // Left over could be city name, unit#, or notes
   while (iIdx < iCnt && *apItems[iIdx] != '&')
   {
      if (!strcmp(apItems[iIdx], "CA"))
         break;
      strcat(acCity, apItems[iIdx++]);
      strcat(acCity, " ");
   }

   // If no strName, put it in Legal
   if (acStrName[0])
   {
      // Remove comma in street name
      replCharEx(acStrName, ',', ' ');
      blankRem(acStrName);
      strncpy(pAdrRec->strName, acStrName, SIZ_M_STREET);

      // Check City
      if (acCity[0] && (pTmp = strchr(acCity, '-')))
         *pTmp++ = 0;
      if (!memcmp(acCity, "SP ", 3) && isdigit(acCity[3]))
      {
         strncpy(pAdrRec->Unit, acCity, SIZ_M_UNITNO);
         acCity[0] = 0;
         if (pTmp && *pTmp)
            strcpy(acCity, pTmp);
      }

      // City name
      if (acCity[0] > '0')
      {
         City2Code(acCity, acTmp);
         if (acTmp[0] > '0')
         {
            strncpy(pAdrRec->City, acTmp, SIZ_S_CITY);
            iTmp = atoi(acTmp);
            if (pTmp = GetCityName(iTmp))
               strncpy(pSitus->City, pTmp, SIZ_S_CITY);
         } else
            iTmp = 1;
      }

      if (pSitus->SfxCode[0] > ' ')
      {
         strcpy(pAdrRec->strSfx, pSitus->strSfx);
         strcpy(pAdrRec->SfxCode, pSitus->SfxCode);
      }

      iRet = 0;
   } else
      iRet = 1;

   return iRet;
}

/******************************** Sbt_MergeSitus *****************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 * If strType = "LA", translate it to "LN"
 * If no strType, it might be embeded in strName.  If so, translate STS to ST
 *
 *****************************************************************************/

int Sbt_CorrectCity(char *pOutbuf)
{
   int   iTmp, iRet;
   char  acTmp[128], acCity[128];

   iTmp = atoin(pOutbuf+OFF_S_CITY, 2);
   switch (iTmp)
   {
      case 1:
         iRet = 1;
         break;
      case 14:
         strcpy(acCity, "SAN JUAN BAUTISTA CA");
         iRet = 4;
         break;
      case 7:
         strcpy(acCity, "HOLLISTER CA");
         iRet = 2;
         break;
      case 9:
      case 18:
         strcpy(acCity, "PAICINES CA");
         iRet = 3;
         break;
      case 16:
         strcpy(acCity, "TRES PINOS CA");
         iRet = 5;
         break;
      default:
         iRet = 100;
         break;
   }

   if (iRet > 1)
   {
      if (iRet == 100)
      {
         memset(pOutbuf+OFF_S_CITY, ' ', SIZ_S_CITY);
         memset(pOutbuf+OFF_S_CTY_ST_D, ' ', SIZ_S_CTY_ST_D);
      } else
      {
         iTmp = sprintf(acTmp, "%d    ", iRet);
         memcpy(pOutbuf+OFF_S_CITY, acTmp, iTmp);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, strlen(acTmp));
      }
   }

   return iRet;
}

/* Special cases ********************************************************************
 "057070053000","VALLEY VIEW","1560 &1570","RD","  ","       ","HOL","95023",0
 "057070058000","1703,1705,1709 AIRLINE","1701,     ","HWY","  ","       ","","",0
 "057070060000","AIRLINE","1699      ","HWY","  ","       ","","",0
 "057070061000","1703, 1705 & 1709 AIRLIN","1701,     ","HWY","  ","       ","","",0
 "057070062000","1,2 & 3 & 1725 AIRLINE","1715      ","HWY","  ","       ","","",0
 "057070063000","1703,1705,1707 & 1709 AI","1701,     ","HWY","  ","       ","","",0
 "057070064000","SUNNYSLOPE RD/VERSAILLES","          ","DR","  ","       ","","",0
 ************************************************************************************/

int Sbt_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp, *apItems[32];
   long     lStrNum;
   int      iRet=0, iTmp;
   ADR_REC  myAdr;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSitus);
      // Get first rec
      pRec = fgets(acRec, 512, fdSitus);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "057070062000", 9) )
   //   iTmp = 0;
#endif

   if (pRec)
   {
      // Replace null char with space
      replNull(pRec);
      // Remove quotes
      quoteRem(pRec);

      iRet = ParseStringIQ(pRec, cDelim, MB_SITUS_SEQ+1, apItems);
   } else
      iRet = 0;
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clean up step 1 - Remove unwanted chars from strName
   memset((void *)&myAdr, 0, sizeof(ADR_REC));
   removeSitus(pOutbuf);

   pTmp =  apItems[MB_SITUS_STRNAME];
   iTmp = 0;
   acTmp[0] = 0;
   while (*pTmp)
   {
      // Remove unwanted characters
      if (*pTmp == '(')
         break;
      else if (*pTmp != '.' && *pTmp != 39 && *pTmp != 34)
         acTmp[iTmp++] = *pTmp;
      pTmp++;
   }

   blankRem(acTmp, iTmp);
   acAddr1[0] = 0;
   lStrNum = atol(apItems[MB_SITUS_STRNUM]);
   if (lStrNum > 0)
   {
      sprintf(myAdr.strNum, "%d", lStrNum);
      // Form addr1
      if (acTmp[0] == '&')
      {
         if (acTmp[1] == ' ')
            sprintf(myAdr.strName, "%d&%s", lStrNum, &acTmp[2]);
         else
            sprintf(myAdr.strName, "%d%s", lStrNum, acTmp);
      } else if (acTmp[0] == '-' || acTmp[1] == '-' || acTmp[2] == '-')
      {
         sprintf(myAdr.strName, "%d%s", lStrNum, acTmp);
         lStrNum = 0;
      } else
         sprintf(myAdr.strName, "%d %s", lStrNum, acTmp);
      myAdr.lStrNum = lStrNum;

      // Save original StrNum
      iTmp = blankRem(apItems[MB_SITUS_STRNUM]);
      if (strchr(apItems[MB_SITUS_STRNUM], '-'))
      {
         strcpy(acTmp, apItems[MB_SITUS_STRNUM]);
         myTrim(acTmp);
         memcpy(pOutbuf+OFF_S_HSENO, acTmp, strlen(acTmp));
      } else
      {
         if (pTmp = strchr(apItems[MB_SITUS_STRNUM], ' '))
            if (isdigit(*(++pTmp)))
               memcpy(pOutbuf+OFF_S_STR_SUB, pTmp, strlen(pTmp));
         memcpy(pOutbuf+OFF_S_HSENO, myAdr.strNum, strlen(myAdr.strNum));
      }   
   } 
   else if (acTmp[0] > ' ')
      strcpy(myAdr.strName, acTmp);
   else
      return 0;


   if (*apItems[MB_SITUS_STRDIR] > ' ')
      strcpy(myAdr.strDir, apItems[MB_SITUS_STRDIR]);

   if (*apItems[MB_SITUS_STRTYPE] > ' ')
   {
      if (!strcmp(apItems[MB_SITUS_STRTYPE], "LA"))
         iTmp = GetSfxCodeX("LN", acTmp);
      else
         iTmp = GetSfxCodeX(apItems[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
      {
         Sfx2Code(acTmp, acCode);
         strcpy(myAdr.strSfx, acTmp);
         strcpy(myAdr.SfxCode, acCode);
      } else
      {
         LogMsg0("*** Invalid suffix: %s", apItems[MB_SITUS_STRTYPE]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
   }

   // Unit #
   if (*apItems[MB_SITUS_UNIT] > ' ')
      strncpy(myAdr.Unit, apItems[MB_SITUS_UNIT], SIZ_M_UNITNO);

   // Parse street name
   ADR_REC sAdrRec;
   iRet = Sbt_ParseSAdr(&sAdrRec, &myAdr);
   if (!iRet)
   {
      // StrNum
      memcpy(pOutbuf+OFF_S_STRNUM, sAdrRec.strNum, strlen(sAdrRec.strNum));

      // StrSub
      if (sAdrRec.strSub[0])
         memcpy(pOutbuf+OFF_S_STR_SUB, sAdrRec.strSub, strlen(sAdrRec.strSub));

      // StrDir
      if (sAdrRec.strDir[0])
         memcpy(pOutbuf+OFF_S_DIR, sAdrRec.strDir, strlen(sAdrRec.strDir));
      else if (myAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, myAdr.strDir, strlen(myAdr.strDir));

      // StrName
      memcpy(pOutbuf+OFF_S_STREET, sAdrRec.strName, strlen(sAdrRec.strName));

      // StrSfx
      if (sAdrRec.SfxCode[0])
         memcpy(pOutbuf+OFF_S_SUFF, sAdrRec.SfxCode, strlen(sAdrRec.SfxCode));

      // StrUnit
      if (sAdrRec.Unit[0])
         memcpy(pOutbuf+OFF_S_UNITNO, sAdrRec.Unit, strlen(sAdrRec.Unit));  
   } else
   {
      memcpy(pOutbuf+OFF_S_STREET, sAdrRec.strName, strlen(sAdrRec.strName));
      LogMsg("??? %.12s --> %s", pOutbuf, apItems[MB_SITUS_STRNAME]);
   }

   sprintf(acAddr1, "%s%s %s %s %s %s", sAdrRec.strNum, sAdrRec.strSub, sAdrRec.strDir, sAdrRec.strName, sAdrRec.strSfx, sAdrRec.Unit);
   iRet = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iRet);

   // Situs city
   if (*apItems[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apItems[MB_SITUS_COMMUNITY], acTmp, acAddr1, pOutbuf);
      if (acAddr1[0] > ' ')
      {
         blankPad(acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         sprintf(acTmp, "%s CA", acAddr1);
         blankRem(acTmp);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
      } else if (*(pOutbuf+OFF_S_CITY) > ' ')
      {
         iRet = Sbt_CorrectCity(pOutbuf);
      }
   } else if (sAdrRec.City[0] > ' ')
   {
      blankPad(sAdrRec.City, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_CITY, sAdrRec.City, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      iTmp = sprintf(acTmp, "%s CA", myAdr.City);
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
   //} else if (*(pOutbuf+OFF_S_CITY) > ' ')
   //{
   //   iRet = Sbt_CorrectCity(pOutbuf);
   } else
   {
      //iRet = atoin(pOutbuf+OFF_S_CITY, 2);
      memset(pOutbuf+OFF_S_CITY, ' ', SIZ_S_CITY);
   }

   // State
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Zipcode
   if (*apItems[MB_SITUS_ZIP] > ' ')
   {
      iTmp = atol(apItems[MB_SITUS_ZIP]);
      if (iTmp >= 90000 && iTmp < 99999)
         memcpy(pOutbuf+OFF_S_ZIP, apItems[MB_SITUS_ZIP], SIZ_S_ZIP);
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int Sbt_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004130013", 9))
   //   acTmp[0] = 0;
#endif
   
   removeSitus(pOutbuf);

   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   if (sSitusAdr.lStrNum > 0)
   {
      memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
      memcpy(pOutbuf+OFF_S_HSENO, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
   }

   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   } else if (!memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_M_STREET) && 
         *(pOutbuf+OFF_M_CITY) > ' ' && *(pOutbuf+OFF_M_STREET) > ' ')
   {
      char  acCode[32];

      memcpy(acTmp, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      myTrim(acTmp, SIZ_M_CITY);
      City2Code(acTmp, acCode);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP+SIZ_S_ZIP4);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, pOutbuf+OFF_S_CTY_ST_D, OFF_M_CTY_ST_D);
      } else
         LogMsg("*** Bad city name %s in %.12s", acTmp, pOutbuf);
   }

   return 0;
}

/******************************** Sbt_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Sbt_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   while (!iTmp)
   {
      // Replace tab char with 0
      if (pRec)
         iRet = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "056400073", 9))
      //   iRet = 0;
#endif

      pTmp = apTokens[MB_SALES_DOCNUM];
      // Process recorded doc only
      if (*(pTmp+4) == 'R')
      {
         // Merge data - Only take sale rec that has both docnum and docdate
         if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
         {
            memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

            // Docnum
            iRet = atol(pTmp+5);
            sprintf(sCurSale.acDocNum, "%.5s%.6d", pTmp, iRet);
            sCurSale.acDocNum[11] = ' ';

            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
            if (pTmp)
            {
               // Doc date
               lCurSaleDt = atol(acTmp);
               memcpy(sCurSale.acDocDate, acTmp, 8);
            }

            // Confirmed sale price - SBT populates this field more often
            // than DOCTAX field.  So check this first
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*u00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            } else
            {
               // Tax
               dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
               lPrice = 0;
               if (acTmp[0] > '0')
               {
                  dTmp = atof(acTmp);
                  lPrice = (long)(dTmp * SALE_FACTOR);
                  if (lPrice > 0)
                  {
                     if (lPrice < 100)
                        sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
                     else
                        sprintf(acTmp, "%*u00", SIZ_SALE1_AMT-2, lPrice/100);
                     memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
                  }
               }
            }

            // DocType - need translation before production
            // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
            //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

            // Transfer Type
            if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
            {
               while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
               {
                  if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
                  {
                     sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                     break;
                  }
                  iTmp++;
               }
            } else
               sCurSale.acSaleCode[0] = ' ';

            // Group sale?
            if (*apTokens[MB_SALES_GROUPSALE] > '0')
               *(pOutbuf+OFF_MULTI_APN) = 'Y';
            else
               *(pOutbuf+OFF_MULTI_APN) = ' ';

            // Seller
            strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
            blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

            MB_MergeSale(&sCurSale, pOutbuf, true);
            iRet = 0;
         }
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Sbt_MergeChar ******************************
 *
 * Note: need code table for Heating and cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

int Sbt_MergeChar(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32];
   long     lTmp, lBldgSqft, lGarSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iFp;
   MB_CHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 512, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   pChar = (MB_CHAR *)pRec;

   // Quality Class
   memcpy(acTmp, pChar->QualityClass, MBSIZ_CHAR_QUALITY);
   acTmp[MBSIZ_CHAR_QUALITY] = 0;

   acCode[0] = 0;
   if (isalpha(acTmp[0]))
   {
      *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0] & 0x4F; // Force upper case

      iTmp = 0;
      while (acTmp[iTmp] && !isdigit(acTmp[iTmp]))
         iTmp++;

      if (acTmp[iTmp] > '0')
         iRet = Quality2Code((char *)&acTmp[iTmp], acCode, NULL);
   } else if (acTmp[0] > '0' && acTmp[0] <= '9')
      iRet = Quality2Code(acTmp, acCode, NULL);

   blankPad(acCode, SIZ_BLDG_QUAL);
   memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);

   // Yrblt
   lTmp = atoin(pChar->YearBuilt, MBSIZ_CHAR_YRBLT);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = '2';
   } else
   {
      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = ' ';
   }

   // Heating
   int iCmp;
   if (pChar->Heating[0] > ' ')
   {
      iTmp = 0;
      iCmp = -1;
      while (asHeating[iTmp].iLen > 0)
      {
         if (pChar->Heating[0] == asHeating[iTmp].acSrc[0])
         {
            *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }

   // Cooling
   if (pChar->Cooling[0] > ' ')
   {
      iTmp = 0;
      iCmp = -1;
      while (asCooling[iTmp].iLen > 0 && (iCmp=memcmp(pChar->Cooling, asCooling[iTmp].acSrc, asCooling[iTmp].iLen)) > 0)
         iTmp++;

      if (!iCmp)
         *(pOutbuf+OFF_AIR_COND) = asCooling[iTmp].acCode[0];
   }

   // Pool
   if (pChar->NumPools[0] > ' ')
   {
      iTmp = 0;
      iCmp = -1;
      while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(pChar->NumPools, asPool[iTmp].acSrc, asPool[iTmp].iLen)) > 0)
         iTmp++;

      if (!iCmp)
         *(pOutbuf+OFF_POOL) = asPool[iTmp].acCode[0];
   }

   // Beds
   iBeds = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Fireplace
   iFp = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   } else
      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);

   // HasSeptic or HasSewer
   if (pChar->HasSeptic > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
   else if (pChar->HasSewer > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWell;

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 512, fdChar);

   return 0;
}

/******************************** Sbt_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************

int Sbt_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      pTmp += iSkipQuote;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 0;
   }

   iRet = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/********************************* Sbt_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sbt_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   ULONG    lTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   replNull(pRollRec);

   // Remove quotes
   quoteRem(pRollRec);

   // Parse
   iTokens = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[iApnFld], iTokens);
      return -1;
   }

   // Ignore APN starts with 800-999 except 910 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910))
      return 1;

   if (iTokens > MB_ROLL_M_ADDR4+1)
   {
      LogMsg("*** Bad input record: %s (%d)", apTokens[iApnFld], iTokens);
   }

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], iApnLen);
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "35SBT", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres - current data in Sbt_Roll.csv is not reliable.  
   // Only use those values < 1000.0 acres
   double dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0 && dTmp < 1000.0)
   {
      // Lot Sqft
      lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
      if (lTmp < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "054340004000", 9))
   //   iTmp = 0;
#endif
   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      memset(pOutbuf+OFF_TRANSFER_DOC,' ', SIZ_TRANSFER_DOC);
      memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);

      pTmp = apTokens[MB_ROLL_DOCNUM];
      iRet = atol(pTmp+5);
      if (iRet > 0 && *(pTmp+4) == 'R')
      {
         // The 20080806 is a place holder for the county - do not accept it here
         pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp && memcmp(acTmp, "20080806", 8))
         {
            memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
            sprintf(acTmp, "%.5s%.7d", apTokens[MB_ROLL_DOCNUM], iRet);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, 12);
         }
      }
   }

   // Owner
   try {
      Sbt_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   } catch(...) {
      LogMsg("***** Exeception occured in Sbt_MergeOwner()");
   }

   // Mailing
   try {
      Sbt_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Sbt_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************* Sbt_Load_Roll ******************************
 *
 * This function has not been tested.
 *
 ******************************************************************************/

int Sbt_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "RMP");
   lRet = sortFile(acRollFile, acTmpFile, "S(1,13,C,A)");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record - 08/03/2015 -> roll file already sorted, no need to skip header
   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "011360001000", 9))
      //   iTmp = 0;
#endif

      // Replace all unprintable chars to space
      replCharEx(acRollRec, 31, 32, 0);

      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Sbt_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Sbt_MergeSitus(acBuf);

            // Merge Char
            if (fdChar)
               lRet = MB_MergeStdChar(acBuf, NULL, NULL, NULL);
               //lRet = Sbt_MergeChar(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, (char *)&acRollRec[iSkipQuote], lCnt);

         // Create new R01 record
         iRet = Sbt_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Sbt_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = MB_MergeStdChar(acRec, NULL, NULL, NULL);
               //lRet = Sbt_MergeChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof && isdigit(acRollRec[iSkipQuote]))
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Sbt_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Sbt_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = MB_MergeStdChar(acRec, NULL, NULL, NULL);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTax(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         bEof = true;    // Signal to stop
      else
         replCharEx(acRollRec, 31, 32, 0);
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   //LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   //LogMsg("Number of Exe skiped:       %u", lExeSkip);
   //LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal records processed: %u\n", lCnt);
   return 0;
}

/********************************* Sbt_MergeLien *****************************
 *
 * Load LDR roll 2013
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sbt_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_USERID)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   if (iLdrGrp == 1)
   {
      // Start copying data
      memcpy(pOutbuf, apTokens[L1_ASMT], strlen(apTokens[L1_ASMT]));

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[L1_FEEPARCEL], strlen(apTokens[L1_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[L1_ASMT], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[L1_ASMT], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "35SBT", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Lien values
      // Land
      long lLand = atoi(apTokens[L1_CURRENTMARKETLANDVALUE]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[L1_CURRENTSTRUCTURALIMPRVALUE]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: Growing, Fixture, PP, PPMH
      long lGrow = atoi(apTokens[L1_CURRENTGROWINGIMPRVALUE]);
      long lFixt   = atoi(apTokens[L1_CURRENTFIXEDIMPRVALUE]);
      long lPP   = atoi(apTokens[L1_CURRENTPERSONALPROPVALUE]);
      long lMH   = atoi(apTokens[L1_CURRENTPERSONALPROPMHVALUE]);

      lTmp = lGrow+lFixt+lPP+lMH;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lGrow > 0)
         {
            sprintf(acTmp, "%u         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixt > 0)
         {
            sprintf(acTmp, "%u         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%u         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%u         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // Tax
      double dTax1 = atof(apTokens[L1_TAXAMT1]);
      double dTax2 = atof(apTokens[L1_TAXAMT2]);
      dTmp = dTax1+dTax2;
      if (dTax1 == 0.0 || dTax2 == 0.0)
         dTmp *= 2;

      if (dTmp > 0.0)
      {
         sprintf(acTmp, "%*u", SIZ_TAX_AMT, (long)(dTmp*100));
         memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
      } else
         memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

      // Exemption
      long lExe1 = atol(apTokens[L1_EXEMPTIONAMT1]);
      long lExe2 = atol(apTokens[L1_EXEMPTIONAMT2]);
      long lExe3 = atol(apTokens[L1_EXEMPTIONAMT3]);
      lTmp = lExe1+lExe2+lExe3;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }
      if (!memcmp(apTokens[L1_EXEMPTIONCODE1], "E01", 3))
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Save exemption code
      memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L1_EXEMPTIONCODE1], strlen(apTokens[L1_EXEMPTIONCODE1]));
      memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L1_EXEMPTIONCODE2], strlen(apTokens[L1_EXEMPTIONCODE2]));
      memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L1_EXEMPTIONCODE3], strlen(apTokens[L1_EXEMPTIONCODE3]));

      // TRA
      memcpy(pOutbuf+OFF_TRA, apTokens[L1_TRA], strlen(apTokens[L1_TRA]));

      // status
      *(pOutbuf+OFF_STATUS) = *apTokens[L1_STATUS];

      // Legal
      iTmp = updateLegal(pOutbuf, apTokens[L1_PARCELDESCRIPTION]);
      if (iTmp > iMaxLegal)
         iMaxLegal = iTmp;

      // UseCode
      iTmp = strlen(apTokens[L1_USECODE]);
      if (iTmp > 0)
      {
         vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L1_USECODE], SIZ_USE_CO, iTmp);

         // Std Usecode
         updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L1_USECODE], iTmp, pOutbuf);
      } else
         memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

      // Acres
      dTmp = atof(apTokens[L1_ACRES]);
      if (dTmp > 0.0)
      {
         // Lot Sqft
         lTmp = (long)(dTmp * SQFT_PER_ACRE);
         if (lTmp < 999999999)
         {
            sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
            memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
         }

         // Format Acres
         lTmp = (long)(dTmp * ACRES_FACTOR);
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }

      // AgPreserved
      if (*apTokens[L1_ISAGPRESERVE] == '1')
         *(pOutbuf+OFF_AG_PRE) = 'Y';

      // Owner
      Sbt_MergeOwner(pOutbuf, apTokens[L1_OWNER]);

      // Mailing
      Sbt_MergeMAdr(pOutbuf, apTokens[L1_MAILADDRESS1], apTokens[L1_MAILADDRESS2], apTokens[L1_MAILADDRESS3], apTokens[L1_MAILADDRESS4]);

      // Situs
      //Sbt_MergeSitus(pOutbuf, apTokens[L1_SITUS1], apTokens[L1_SITUS2]);

      // SetTaxcode, Prop8 flag, FullExe flag
      iTmp = updateTaxCode(pOutbuf, apTokens[L1_TAXABILITY], true, true);

   } else
   {
      // Start copying data
      memcpy(pOutbuf, apTokens[L2_ASMT], strlen(apTokens[L2_ASMT]));

      // Copy ALT_APN
      iTmp = strlen(apTokens[L2_FEEPARCEL]);
      if (iTmp < 12)
         memcpy(pOutbuf+OFF_ALT_APN, "000000000000", 12-iTmp);
      memcpy(pOutbuf+OFF_ALT_APN+(12-iTmp), apTokens[L2_FEEPARCEL], iTmp);

      // Format APN
      iRet = formatApn(apTokens[L2_ASMT], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[L2_ASMT], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "35SBT", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Lien values
      // Land
      long lLand = atoi(apTokens[L2_CURRENTMARKETLANDVALUE]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[L2_CURRENTSTRUCTURALIMPRVALUE]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: Growing, Fixture, PP, PPMH
      long lGrow = atoi(apTokens[L2_CURRENTGROWINGIMPRVALUE]);
      long lFixt   = atoi(apTokens[L2_CURRENTFIXEDIMPRVALUE]);
      long lPP   = atoi(apTokens[L2_CURRENTPERSONALPROPVALUE]);
      long lMH   = atoi(apTokens[L2_CURRENTPERSONALPROPMHVALUE]);

      lTmp = lGrow+lFixt+lPP+lMH;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lGrow > 0)
         {
            sprintf(acTmp, "%u         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixt > 0)
         {
            sprintf(acTmp, "%u         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%u         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%u         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // Tax
      double dTax1 = atof(apTokens[L2_TAXAMT1]);
      double dTax2 = atof(apTokens[L2_TAXAMT2]);
      dTmp = dTax1+dTax2;
      if (dTax1 == 0.0 || dTax2 == 0.0)
         dTmp *= 2;

      if (dTmp > 0.0)
      {
         sprintf(acTmp, "%*u", SIZ_TAX_AMT, (long)(dTmp*100));
         memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
      } else
         memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

      // Exemption
      long lExe1 = atol(apTokens[L2_EXEMPTIONAMT1]);
      long lExe2 = atol(apTokens[L2_EXEMPTIONAMT2]);
      long lExe3 = atol(apTokens[L2_EXEMPTIONAMT3]);
      lTmp = lExe1+lExe2+lExe3;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }
      if (!memcmp(apTokens[L2_EXEMPTIONCODE1], "E01", 3))
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Save exemption code
      memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L2_EXEMPTIONCODE1], strlen(apTokens[L2_EXEMPTIONCODE1]));
      memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L2_EXEMPTIONCODE2], strlen(apTokens[L2_EXEMPTIONCODE2]));
      memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L2_EXEMPTIONCODE3], strlen(apTokens[L2_EXEMPTIONCODE3]));

      // TRA
      lTmp = atol(apTokens[L2_TRA]);
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iTmp);

      // status
      *(pOutbuf+OFF_STATUS) = *apTokens[L2_STATUS];

      // Legal
      iTmp = updateLegal(pOutbuf, apTokens[L2_PARCELDESCRIPTION]);
      if (iTmp > iMaxLegal)
         iMaxLegal = iTmp;

      // UseCode
      iTmp = strlen(apTokens[L2_USECODE]);
      if (iTmp > 0)
      {
         if (iTmp > SIZ_USE_CO)
            iTmp = SIZ_USE_CO;
         memcpy(pOutbuf+OFF_USE_CO, apTokens[L2_USECODE], iTmp);

         // Std Usecode
         updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L2_USECODE], iTmp, pOutbuf);
      } else
         memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

      // Acres
      dTmp = atof(apTokens[L2_ACRES]);
      if (dTmp > 0.0)
      {
         // Lot Sqft
         lTmp = (long)(dTmp * SQFT_PER_ACRE);
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

         // Format Acres
         lTmp = (long)(dTmp * ACRES_FACTOR);
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }

      // AgPreserved
      if (*apTokens[L2_ISAGPRESERVE] == '1')
         *(pOutbuf+OFF_AG_PRE) = 'Y';

      // Owner
      Sbt_MergeOwner(pOutbuf, apTokens[L2_OWNER]);

      // Mailing
      Sbt_MergeMAdr(pOutbuf, apTokens[L2_MAILADDRESS1], apTokens[L2_MAILADDRESS2], apTokens[L2_MAILADDRESS3], apTokens[L2_MAILADDRESS4]);

      // Situs
      //Sbt_MergeSitus(pOutbuf, apTokens[L2_SITUS1], apTokens[L2_SITUS2]);

      // SetTaxcode, Prop8 flag, FullExe flag
      iTmp = updateTaxCode(pOutbuf, apTokens[L2_TAXABILITY], true, true);
   }

   return 0;
}

/* for Excel format as in 2011
int Sbt_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L3_PARCELDESCRIPTION)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   switch (iLdrGrp)
   {
      case 1:
      {
         // Start copying data
         memcpy(pOutbuf, apTokens[L1_ASMT], strlen(apTokens[L1_ASMT]));

         // Copy ALT_APN
         memcpy(pOutbuf+OFF_ALT_APN, apTokens[L1_FEEPARCEL], strlen(apTokens[L1_FEEPARCEL]));

         // Format APN
         iRet = formatApn(apTokens[L1_ASMT], acTmp, &myCounty);
         memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

         // Create MapLink and output new record
         iRet = formatMapLink(apTokens[L1_ASMT], acTmp, &myCounty);
         memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

         // Create index map link
         if (getIndexPage(acTmp, acTmp1, &myCounty))
            memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

         // County code
         memcpy(pOutbuf+OFF_CO_NUM, "35SBT", 5);

         // Year assessed
         memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

         // Lien values
         // Land
         long lLand = atoi(apTokens[L1_CURRENTMARKETLANDVALUE]);
         if (lLand > 0)
         {
            sprintf(acTmp, "%*d", SIZ_LAND, lLand);
            memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
         }

         // Improve
         long lImpr = atoi(apTokens[L1_CURRENTSTRUCTURALIMPRVALUE]);
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
            memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
         }

         // Other value: Growing, Fixture, PP, PPMH
         long lGrow = atoi(apTokens[L1_CURRENTGROWINGIMPRVALUE]);
         long lFixt   = atoi(apTokens[L1_CURRENTFIXEDIMPRVALUE]);
         long lPP   = atoi(apTokens[L1_CURRENTPERSONALPROPVALUE]);
         long lMH   = atoi(apTokens[L1_CURRENTPERSONALPROPMHVALUE]);

         lTmp = lGrow+lFixt+lPP+lMH;
         if (lTmp > 0)
         {
            sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
            memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

            if (lGrow > 0)
            {
               sprintf(acTmp, "%d         ", lGrow);
               memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
            }
            if (lFixt > 0)
            {
               sprintf(acTmp, "%d         ", lFixt);
               memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
            }
            if (lPP > 0)
            {
               sprintf(acTmp, "%d         ", lPP);
               memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
            }
            if (lMH > 0)
            {
               sprintf(acTmp, "%d         ", lMH);
               memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
            }
         }

         // Gross total
         lTmp += (lLand+lImpr);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
            memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
         }

         // Ratio
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }

         // Tax
         double dTax1 = atof(apTokens[L1_TAXAMT1]);
         double dTax2 = atof(apTokens[L1_TAXAMT2]);
         dTmp = dTax1+dTax2;
         if (dTax1 == 0.0 || dTax2 == 0.0)
            dTmp *= 2;

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

         // Exemption
         long lExe1 = atol(apTokens[L1_EXEMPTIONAMT1]);
         long lExe2 = atol(apTokens[L1_EXEMPTIONAMT2]);
         long lExe3 = atol(apTokens[L1_EXEMPTIONAMT3]);
         lTmp = lExe1+lExe2+lExe3;
         if (lTmp > 0)
         {
            sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
            memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
         }
         if (!memcmp(apTokens[L1_EXEMPTIONCODE1], "E01", 3))
            *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
         else
            *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

         // Save exemption code
         memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L1_EXEMPTIONCODE1], strlen(apTokens[L1_EXEMPTIONCODE1]));
         memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L1_EXEMPTIONCODE2], strlen(apTokens[L1_EXEMPTIONCODE2]));
         memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L1_EXEMPTIONCODE3], strlen(apTokens[L1_EXEMPTIONCODE3]));

         // TRA
         memcpy(pOutbuf+OFF_TRA, apTokens[L1_TRA], strlen(apTokens[L1_TRA]));

         // status
         *(pOutbuf+OFF_STATUS) = *apTokens[L1_STATUS];

         // Legal
         iTmp = updateLegal(pOutbuf, apTokens[L1_PARCELDESCRIPTION]);
         if (iTmp > iMaxLegal)
            iMaxLegal = iTmp;

         // UseCode
         iTmp = strlen(apTokens[L1_USECODE]);
         if (iTmp > 0)
         {
            if (iTmp > SIZ_USE_CO)
               iTmp = SIZ_USE_CO;
            memcpy(pOutbuf+OFF_USE_CO, apTokens[L1_USECODE], iTmp);

            // Std Usecode
            updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L1_USECODE], iTmp);
         }

         // Acres
         dTmp = atof(apTokens[L1_ACRES]);
         if (dTmp > 0.0)
         {
            // Lot Sqft
            lTmp = dTmp * SQFT_PER_ACRE;
            sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
            memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

            // Format Acres
            lTmp = dTmp * ACRES_FACTOR;
            sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
            memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
         }

         // AgPreserved
         if (*apTokens[L1_ISAGPRESERVE] == '1')
            *(pOutbuf+OFF_AG_PRE) = 'Y';

         // Owner
         Sbt_MergeOwner(pOutbuf, apTokens[L1_OWNER]);

         // Mailing
         Sbt_MergeMAdr(pOutbuf, apTokens[L1_MAILADDRESS1], apTokens[L1_MAILADDRESS2], apTokens[L1_MAILADDRESS3], apTokens[L1_MAILADDRESS4]);

         // Situs
         Sbt_MergeSitus(pOutbuf, apTokens[L1_SITUS1], apTokens[L1_SITUS2]);

         // SetTaxcode, Prop8 flag, FullExe flag
         iTmp = updateTaxCode(pOutbuf, apTokens[L1_TAXABILITY], true, true);

      } 
      break;
      case 2:
      {
         // Start copying data
         memcpy(pOutbuf, apTokens[L2_ASMT], strlen(apTokens[L2_ASMT]));

         // Copy ALT_APN
         memcpy(pOutbuf+OFF_ALT_APN, apTokens[L2_FEEPARCEL], strlen(apTokens[L2_FEEPARCEL]));

         // Format APN
         iRet = formatApn(apTokens[L2_ASMT], acTmp, &myCounty);
         memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

         // Create MapLink and output new record
         iRet = formatMapLink(apTokens[L2_ASMT], acTmp, &myCounty);
         memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

         // Create index map link
         if (getIndexPage(acTmp, acTmp1, &myCounty))
            memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

         // County code
         memcpy(pOutbuf+OFF_CO_NUM, "35SBT", 5);

         // Year assessed
         memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

         // Lien values
         // Land
         long lLand = atoi(apTokens[L2_CURRENTMARKETLANDVALUE]);
         if (lLand > 0)
         {
            sprintf(acTmp, "%*d", SIZ_LAND, lLand);
            memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
         }

         // Improve
         long lImpr = atoi(apTokens[L2_CURRENTSTRUCTURALIMPRVALUE]);
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
            memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
         }

         // Other value: Growing, Fixture, PP, PPMH
         long lGrow = atoi(apTokens[L2_CURRENTGROWINGIMPRVALUE]);
         long lFixt   = atoi(apTokens[L2_CURRENTFIXEDIMPRVALUE]);
         long lPP   = atoi(apTokens[L2_CURRENTPERSONALPROPVALUE]);
         long lMH   = atoi(apTokens[L2_CURRENTPERSONALPROPMHVALUE]);

         lTmp = lGrow+lFixt+lPP+lMH;
         if (lTmp > 0)
         {
            sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
            memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

            if (lGrow > 0)
            {
               sprintf(acTmp, "%d         ", lGrow);
               memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
            }
            if (lFixt > 0)
            {
               sprintf(acTmp, "%d         ", lFixt);
               memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
            }
            if (lPP > 0)
            {
               sprintf(acTmp, "%d         ", lPP);
               memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
            }
            if (lMH > 0)
            {
               sprintf(acTmp, "%d         ", lMH);
               memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
            }
         }

         // Gross total
         lTmp += (lLand+lImpr);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
            memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
         }

         // Ratio
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }

         // Tax
         double dTax1 = atof(apTokens[L2_TAXAMT1]);
         double dTax2 = atof(apTokens[L2_TAXAMT2]);
         dTmp = dTax1+dTax2;
         if (dTax1 == 0.0 || dTax2 == 0.0)
            dTmp *= 2;

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

         // Exemption
         long lExe1 = atol(apTokens[L2_EXEMPTIONAMT1]);
         long lExe2 = atol(apTokens[L2_EXEMPTIONAMT2]);
         long lExe3 = atol(apTokens[L2_EXEMPTIONAMT3]);
         lTmp = lExe1+lExe2+lExe3;
         if (lTmp > 0)
         {
            sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
            memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
         }
         if (!memcmp(apTokens[L2_EXEMPTIONCODE1], "E01", 3))
            *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
         else
            *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

         // Save exemption code
         memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L2_EXEMPTIONCODE1], strlen(apTokens[L2_EXEMPTIONCODE1]));
         memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L2_EXEMPTIONCODE2], strlen(apTokens[L2_EXEMPTIONCODE2]));
         memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L2_EXEMPTIONCODE3], strlen(apTokens[L2_EXEMPTIONCODE3]));

         // TRA
         memcpy(pOutbuf+OFF_TRA, apTokens[L2_TRA], strlen(apTokens[L2_TRA]));

         // status
         *(pOutbuf+OFF_STATUS) = *apTokens[L2_STATUS];

         // Legal
         iTmp = updateLegal(pOutbuf, apTokens[L2_PARCELDESCRIPTION]);
         if (iTmp > iMaxLegal)
            iMaxLegal = iTmp;

         // UseCode
         iTmp = strlen(apTokens[L2_USECODE]);
         if (iTmp > 0)
         {
            if (iTmp > SIZ_USE_CO)
               iTmp = SIZ_USE_CO;
            memcpy(pOutbuf+OFF_USE_CO, apTokens[L2_USECODE], iTmp);

            // Std Usecode
            updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L2_USECODE], iTmp);
         }

         // Acres
         dTmp = atof(apTokens[L2_ACRES]);
         if (dTmp > 0.0)
         {
            // Lot Sqft
            lTmp = dTmp * SQFT_PER_ACRE;
            sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
            memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

            // Format Acres
            lTmp = dTmp * ACRES_FACTOR;
            sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
            memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
         }

         // AgPreserved
         if (*apTokens[L2_ISAGPRESERVE] == '1')
            *(pOutbuf+OFF_AG_PRE) = 'Y';

         // Owner
         Sbt_MergeOwner(pOutbuf, apTokens[L2_OWNER]);

         // Mailing
         Sbt_MergeMAdr(pOutbuf, apTokens[L2_MAILADDRESS1], apTokens[L2_MAILADDRESS2], apTokens[L2_MAILADDRESS3], apTokens[L2_MAILADDRESS4]);

         // Situs
         Sbt_MergeSitus(pOutbuf, apTokens[L2_SITUS1], apTokens[L2_SITUS2]);

         // SetTaxcode, Prop8 flag, FullExe flag
         iTmp = updateTaxCode(pOutbuf, apTokens[L2_TAXABILITY], true, true);
      }
      break;

      case 3:
      {
         // Start copying data
         memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

         // Copy ALT_APN
         memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

         // Format APN
         iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
         memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

         // Create MapLink and output new record
         iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
         memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

         // Create index map link
         if (getIndexPage(acTmp, acTmp1, &myCounty))
            memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

         // County code
         memcpy(pOutbuf+OFF_CO_NUM, "35SBT", 5);

         // Year assessed
         memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

         // Lien values
         // Land
         long lLand = atoi(apTokens[L3_CURRENTMARKETLANDVALUE]);
         if (lLand > 0)
         {
            sprintf(acTmp, "%*d", SIZ_LAND, lLand);
            memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
         }

         // Improve
         long lImpr = atoi(apTokens[L3_CURRENTSTRUCTURALIMPRVALUE]);
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
            memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
         }

         // Other value: Growing, Fixture, PP, PPMH
         long lGrow = atoi(apTokens[L3_CURRENTGROWINGIMPRVALUE]);
         long lFixt   = atoi(apTokens[L3_CURRENTFIXEDIMPRVALUE]);
         long lPP   = atoi(apTokens[L3_CURRENTPERSONALPROPVALUE]);
         long lMH   = atoi(apTokens[L3_CURRENTPERSONALPROPMHVALUE]);

         lTmp = lGrow+lFixt+lPP+lMH;
         if (lTmp > 0)
         {
            sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
            memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

            if (lGrow > 0)
            {
               sprintf(acTmp, "%d         ", lGrow);
               memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
            }
            if (lFixt > 0)
            {
               sprintf(acTmp, "%d         ", lFixt);
               memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
            }
            if (lPP > 0)
            {
               sprintf(acTmp, "%d         ", lPP);
               memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
            }
            if (lMH > 0)
            {
               sprintf(acTmp, "%d         ", lMH);
               memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
            }
         }

         // Gross total
         lTmp += (lLand+lImpr);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
            memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
         }

         // Ratio
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }


         // Exemption
         lTmp = atol(apTokens[L3_EXEMPTIONAMT1]);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
            memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
         }
         if (!memcmp(apTokens[L3_EXEMPTIONCODE1], "E01", 3))
            *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
         else
            *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

         // Save exemption code
         memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L3_EXEMPTIONCODE1], strlen(apTokens[L3_EXEMPTIONCODE1]));

         // TRA
         memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

         // status
         *(pOutbuf+OFF_STATUS) = *apTokens[L3_STATUS];

         // Legal
         if (iTokens > L3_PARCELDESCRIPTION)
         {
            iTmp = updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);
            if (iTmp > iMaxLegal)
               iMaxLegal = iTmp;
         } else
            lValueSkip++;

         // UseCode
         iTmp = strlen(apTokens[L3_USECODE]);
         if (iTmp > 0)
         {
            if (iTmp > SIZ_USE_CO)
               iTmp = SIZ_USE_CO;
            memcpy(pOutbuf+OFF_USE_CO, apTokens[L3_USECODE], iTmp);

            // Std Usecode
            updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_USECODE], iTmp);
         }

         // Acres
         dTmp = atof(apTokens[L3_ACRES]);
         if (dTmp > 0.0)
         {
            // Lot Sqft
            lTmp = dTmp * SQFT_PER_ACRE;
            sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
            memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

            // Format Acres
            lTmp = dTmp * ACRES_FACTOR;
            sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
            memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
         }

         // AgPreserved

         // Owner
         Sbt_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

         // Mailing
         Sbt_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

         // Situs
         Sbt_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

         // SetTaxcode, Prop8 flag, FullExe flag
         iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITY], true, true);
      }
      break;

      default:
         break;
   }

   return 0;
}

/********************************* Sbt_Load_LDR *****************************
 *
 * Load TR601 LDR into 1900-byte record.
 *
 ****************************************************************************/

int Sbt_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     iRet, lRet=0, lCnt=0;

   lLastFileDate = getFileDate(acRollFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A)");
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return -2;
   }

   // Open Sales file
   /*
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -2;
   }
   */

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating R01 file: %s\n", acTmpFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new R01 record
      iRet = Sbt_MergeLien(acBuf, acRec);
      if (!iRet)
      {
         // Merge Char
         if (fdChar)
            lRet = Sbt_MergeChar(acBuf);

         // Merge Situs
         if (fdSitus)
            lRet = Sbt_MergeSitus(acBuf);

         // Merge Sales
         //if (fdSale)
         //   lRet = Sbt_MergeSale(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSitus)
      fclose(fdSitus);
   if (fdChar)
      fclose(fdChar);
   //if (fdSale)
   //   fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   //LogMsg("Number of Sale matched:     %u\n", lSaleMatch);

   LogMsg("Number of Char skiped:      %u", lCharSkip);
   //LogMsg("Number of Sale skiped:      %u\n", lSaleSkip);

   LogMsg("Number of input w/o legal:  %u", lValueSkip);
   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/****************************** Sbt_MergeCurRoll *****************************
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Sbt_MergeCurRoll(char *pOutbuf)
{
   static   char acRec[MAX_RECSIZE], *pRec=NULL;
   char     acTmp[256], *pTmp, *apItems[MAX_FLD_TOKEN];
   int      iRet=0, iTmp;

   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
      pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002030004000", 9))
   //   iTmp = 0;
#endif

   do 
   {
      if (!pRec)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         return 1;      // EOF 
      }

      // Add 1 to roll rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip roll rec: %s", pRec);
         pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);
   else
      iRet = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad roll record for APN=%s", apItems[iApnFld]);
      return -1;
   }

   // Recorded Doc - Not consistent with sale file - use only when matched
   if (*apItems[MB_ROLL_DOCNUM] > '0')
   {
      pTmp = apItems[MB_ROLL_DOCNUM];
      iRet = atol(pTmp+5);
      if (iRet > 0)
      {
         sprintf(acTmp, "%.5s%.6d", pTmp, iRet);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, strlen(acTmp));

         // The 20080806 is a place holder for the county - do not accept it here
         pTmp = dateConversion(apItems[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp && memcmp(acTmp, "20080806", 8))
            memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
      }
   }

   lRollMatch++;

   // Get next record
   pRec = fgets(acRec, MAX_RECSIZE, fdRoll);

   return 0;
}

/********************************* Sbt_MergeLien2 ****************************
 *
 * Load 2014 LDR file - 
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sbt_MergeLien2(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], acApn[16];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with blank
   iRet = replNull(pRollRec, ' ', 0);
   replStrAll(pRollRec, "|NULL", "|");
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L2_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L2_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   iTmp = strlen(apTokens[L2_ASMT]);
   if (iTmp < iApnLen)
   {
      memcpy(acApn, "0000", iApnLen - iTmp);
      memcpy(&acApn[iApnLen - iTmp], apTokens[L2_ASMT], iTmp);
   } else
      memcpy(acApn, apTokens[L2_ASMT], iTmp);
   memcpy(pOutbuf, acApn, iApnLen);

   // Copy ALT_APN
   iTmp = strlen(apTokens[L2_FEEPARCEL]);
   if (iTmp < iApnLen)
   {
      memcpy(pOutbuf+OFF_ALT_APN, "0000", iApnLen - iTmp);
      memcpy(pOutbuf+OFF_ALT_APN+(iApnLen - iTmp), apTokens[L2_FEEPARCEL], iTmp);
   } else
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[L2_FEEPARCEL], iTmp);

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "35SBT", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values - Land
   long lLand = atoi(apTokens[L2_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L2_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow = atoi(apTokens[L2_CURRENTGROWINGIMPRVALUE]);
   long lFixt = atoi(apTokens[L2_CURRENTFIXEDIMPRVALUE]);
   long lBP   = atoi(apTokens[L2_CURRENTPERSONALPROPVALUE]);
   long lPP   = atoi(apTokens[L2_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lBP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%u         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%u         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%u         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lBP > 0)
      {
         sprintf(acTmp, "%u         ", lBP);
         memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L2_TAXAMT1]);
   double dTax2 = atof(apTokens[L2_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L2_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L2_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L2_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L2_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L2_EXEMPTIONCODE1], strlen(apTokens[L2_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L2_EXEMPTIONCODE2], strlen(apTokens[L2_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L2_EXEMPTIONCODE3], strlen(apTokens[L2_EXEMPTIONCODE3]));

   // TRA
   lTmp = atol(apTokens[L2_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L2_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "056182003000", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L2_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   strcpy(acTmp, apTokens[L2_USECODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Megabyte:
   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
      iTmp = atoin(pOutbuf+OFF_USE_STD, 3);
      if (iTmp == 401)
         *(pOutbuf+OFF_AG_PRE) = 'Y';
      else if (iTmp == 403)
         *(pOutbuf+OFF_TIMBER) = 'Y';
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L2_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      if (lTmp < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Sbt_MergeOwner(pOutbuf, apTokens[L2_OWNER]);

   // Situs  
   //Sbt_MergeSitus(pOutbuf, apTokens[L2_SITUS1], apTokens[L2_SITUS2]);

   // Mailing
   Sbt_MergeMAdr(pOutbuf, apTokens[L2_MAILADDRESS1], apTokens[L2_MAILADDRESS2], apTokens[L2_MAILADDRESS3], apTokens[L2_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L2_TAXABILITY], true, true);
   return 0;
}

/***************************** Sbt_Load_LDR2() ******************************
 *
 * Load 2014 LDR file - import xls file into SQL then export to Sbt_2014.txt
 * This file use group 2 layout.
 *
 ****************************************************************************/

int Sbt_Load_LDR2(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH], acCurRoll[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open LDR file
   LogMsg("Open LDR file %s", acRollFile);
   fdLDR = fopen(acRollFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acRollFile);
      return -2;
   }  

   // Open current roll file - to update recently transfer
   GetIniString(myCounty.acCntyCode, "RollFile", "", acBuf, _MAX_PATH, acIniFile);
   sprintf(acCurRoll, acBuf, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open current roll file %s", acCurRoll);
   fdRoll = fopen(acCurRoll, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening current roll file: %s.  Please check INI file in [%s] section\n", acCurRoll, myCounty.acCntyCode);
      return -2;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -1;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   if (acRec[0] > '9' || acRec[0] < '0')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      lLDRRecCount++;

      // Create new R01 record
      iRet = Sbt_MergeLien2(acBuf, acRec);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Sbt_MergeSitus(acBuf);
#ifdef _DEBUG
         //if (acBuf[OFF_S_CITY] > ' ')
         //   lSitusMatch++;
         //else
         //   lSitusSkip++;
#endif
         // Merge Char
         if (fdChar)
            lRet = MB_MergeStdChar(acBuf, NULL, NULL, NULL);

         // Merge transfer from current roll
         if (fdRoll)
            lRet = Sbt_MergeCurRoll(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp || acRec[1] > '9' || strlen(acRec) < 20)
         break;
   }

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   printf("\nTotal output records:       %u\n", lCnt);
   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of roll matched:     %u", lRollMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   lRecCnt = lCnt;
   return 0;
}

/**************************** Sbt_CreateLienRec *****************************
 *
 * Copy from MergeAma.
 *
 ****************************************************************************/

int Sbt_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   replStrAll(pRollRec, "|NULL", "|");
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_USERID)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   iRet = strlen(apTokens[L_ASMT]);
   if (iRet < iApnLen)
   {
      memcpy(pLienExtr->acApn, "0000", iApnLen - iRet);
      memcpy(&pLienExtr->acApn[iApnLen - iRet], apTokens[L_ASMT], iRet);
   } else
      memcpy(pLienExtr->acApn, apTokens[L_ASMT], iRet);

   // TRA
   lTmp = atol(apTokens[L_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LIEN_FIXT);
   }

   // Improve
   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_LIEN_FIXT);
   }

   // Other values
   long lGrow = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);

   // Fixtures
   long lFixt   = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);

   // Business Property
   long lBP   = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);

   // Personal Property
   long lPP   = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lBP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lBP > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lBP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  
   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Prop 8 - correcting by spn 01/16/2010
   lTmp = atoin(apTokens[L_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Taxability
   if (lTmp > 0)
   {
      iRet = strlen(apTokens[L_TAXABILITY]);
      if (iRet > SIZ_LIEN_TAXCODE)
         iRet = SIZ_LIEN_TAXCODE;
      memcpy(pLienExtr->acTaxCode, apTokens[L_TAXABILITY], iRet);
   }

   // Save Exe code
   memcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
   memcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
   memcpy(pLienExtr->extra.MB.ExeCode3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************* Sbt_ExtrLien *******************************
 *
 * Extract lien data from Ama_yyyy.txt
 *
 ****************************************************************************/

int Sbt_ExtrLien(LPCSTR pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg("\nExtract lien roll for %s", pCnty);

   // Open lien file
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acRollFile);
      return 2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Skip header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new record
      iRet = Sbt_CreateLienRec(acBuf, acRec);
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (strlen(acRec) < 20)
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Sbt_ExtrTR601 ******************************
 *
 * Extract lien data 
 *
 ****************************************************************************

int MB3_ExtrTR601Rec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_PARCELDESCRIPTION)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // TRA
   lTmp = atol(apTokens[L3_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lGrow = atoi(apTokens[L3_CURRENTGROWINGIMPRVALUE]);
   long lFixt = atoi(apTokens[L3_CURRENTFIXEDIMPRVALUE]);
   long lBP   = atoi(apTokens[L3_CURRENTPERSONALPROPVALUE]);
   long lPP   = atoi(apTokens[L3_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lBP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lBP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lBP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   lTmp = atol(apTokens[L3_EXEMPTIONAMT1]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  
   if (!memcmp(apTokens[L3_EXEMPTIONCODE1], "E01", 3))
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Prop 8 - correcting by spn 01/16/2010
   lTmp = atoin(apTokens[L3_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Taxability
   if (lTmp > 0)
   {
      iRet = strlen(apTokens[L3_TAXABILITY]);
      if (iRet > SIZ_LIEN_TAXCODE)
         iRet = SIZ_LIEN_TAXCODE;
      memcpy(pLienExtr->acTaxCode, apTokens[L3_TAXABILITY], iRet);
   }

   // Save Exe code
   memcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L3_EXEMPTIONCODE1], strlen(apTokens[L3_EXEMPTIONCODE1]));

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int Sbt_ExtrTR601(LPCSTR pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg("Extract lien roll for %s", pCnty);

   // Open lien file
   LogMsg("Open Lien Date Roll file %s", acRollFile);
   // Sort on ASMT
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acRollFile);
      return 2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Replace null char with space
      iRet = replNull(acRec, ' ', 0);

      // Create new record
      //if (iLdrGrp == 1)
      //   iRet = MB1_ExtrTR601Rec(acBuf, acRec);
      //else if (iLdrGrp == 2)
      //   iRet = MB2_ExtrTR601Rec(acBuf, acRec);
      //else
         iRet = MB3_ExtrTR601Rec(acBuf, acRec);
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}
*/

/********************************* Sbt_MergeLien4 *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TXT
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sbt_MergeLien4(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L4_UNITS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L4_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L4_ASMT], strlen(apTokens[L4_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L4_FEEPARCEL], strlen(apTokens[L4_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L4_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L4_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "35SBT", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L4_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L4_TRA], strlen(apTokens[L4_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L4_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L4_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L4_FIXTURESVALUE]);
   long lGrow  = atoi(apTokens[L4_GROWING]);
   long lPers  = atoi(apTokens[L4_PPVALUE]);
   long lPP_MH = atoi(apTokens[L4_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%u         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%u         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L4_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L4_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L4_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L4_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L4_EXEMPTIONCODE1], strlen(apTokens[L4_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L4_EXEMPTIONCODE2], strlen(apTokens[L4_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L4_EXEMPTIONCODE3], strlen(apTokens[L4_EXEMPTIONCODE3]));

   // Legal
#ifdef _DEBUG
   //if (!memcmp(apTokens[L4_ASMT], "056181006000", 9) )
   //   iTmp = 0;
#endif
   // Legal - replace known bad char
   iTmp = replChar(apTokens[L4_PARCELDESCRIPTION], 221, 47);
   if (iTmp)
      LogMsg("***Fixing Legal [%.12s]: %s", pOutbuf, apTokens[L4_PARCELDESCRIPTION]);

   if (iTmp = findUnPrtChar(apTokens[L4_PARCELDESCRIPTION]))
   {
      iTmp = remUnPrtChar(apTokens[L4_PARCELDESCRIPTION]);
      LogMsg("*** Remove bad char found in legal: %s",  apTokens[L4_ASMT]);
   }

   pTmp = apTokens[L4_PARCELDESCRIPTION];
   if (*pTmp == '"')
   {
      iTmp = remExtraQuote(++pTmp);
      if (iTmp)
         LogMsg("*** Remove extra quote (%d) in legal: %s", iTmp, apTokens[L4_ASMT]);
   }

   // Legal
   iTmp = updateLegal(pOutbuf, pTmp);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   if (*apTokens[L4_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L4_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L4_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L4_ACRES]);
   lTmp = atol(apTokens[L4_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   //if (*apTokens[L4_ISAGPRESERVE] == '1')
   //   *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   pTmp = apTokens[L4_OWNER];
   if (*pTmp == '"')
   {
      iTmp = remExtraQuote(++pTmp);
      if (iTmp)
         LogMsg("*** Remove extra quote (%d) in Owner name: %s", iTmp, apTokens[L4_ASMT]);
   }
   Sbt_MergeOwner(pOutbuf, pTmp);

   // Situs
   //Sbt_MergeSitus(pOutbuf, apTokens[L4_SITUS1], apTokens[L4_SITUS2]);

   // Mailing
   Sbt_MergeMAdr(pOutbuf, apTokens[L4_MAILADDRESS1], apTokens[L4_MAILADDRESS2], apTokens[L4_MAILADDRESS3], apTokens[L4_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   //iTmp = updateTaxCode(pOutbuf, apTokens[L4_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016


#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // Acres
   dTmp = atof(apTokens[L4_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Lot size
   lTmp = atol(apTokens[L4_LANDSIZE]);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      if (!dTmp)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(lTmp*SQFT_MF_1000));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   } else if (dTmp > 0.0)
   {
      //lTmp = (dTmp+0.0005)*SQFT_PER_ACRE;
      lTmp = (long)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   //// Garage size
   //dTmp = atof(apTokens[L4_GARAGESIZE]);
   //if (dTmp > 0.0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_GAR_SQFT, dTmp);
   //   memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   //   *(pOutbuf+OFF_PARK_TYPE) = '2';                 // GARAGE/CARPORT
   //}

   //// Number of parking spaces
   //if (*apTokens[L4_GARAGE] == '0' || *apTokens[L4_GARAGE] == 'N')
   //   *(pOutbuf+OFF_PARK_TYPE) = 'H';                 // None
   //else if (*apTokens[L4_GARAGE] > '0' && *apTokens[L4_GARAGE] <= '9')
   //{
   //   iTmp = atol(apTokens[L4_GARAGE]);
   //   sprintf(acTmp, "%d", iTmp);
   //   vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
   //   if (dTmp > 100)
   //      *(pOutbuf+OFF_PARK_TYPE) = 'Z';              // Garage
   //   else
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
   //} else
   //{
   //   if (*(apTokens[L4_GARAGE]) == 'C')
   //      *(pOutbuf+OFF_PARK_TYPE) = 'C';              // Carport
   //   else if (*(apTokens[L4_GARAGE]) == 'A')
   //   {
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L4_GARAGE], "DOU", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '2';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L4_GARAGE], "TRI", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '3';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L4_GARAGE], "SIN", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '1';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L4_GARAGE], "GC", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT
   //   else if (!_memicmp(apTokens[L4_GARAGE], "GS", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT ?
   //   else if (!_memicmp(apTokens[L4_GARAGE], "DE", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = 'D';              // Detached
   //   else if (*(apTokens[L4_GARAGE]) == 'S')
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Space ?
   //}

   //// YearBlt
   //lTmp = atol(apTokens[L4_YEARBUILT]);
   //if (lTmp > 1800 && lTmp < lToyear)
   //{
   //   iTmp = sprintf(acTmp, "%d", lTmp);
   //   memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   //}

   //// Total rooms
   //iTmp = atol(apTokens[L4_TOTALROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   //}

   //// Stories
   //iTmp = atol(apTokens[L4_STORIES]);
   //if (iTmp > 0 && iTmp < 100)
   //{
   //   sprintf(acTmp, "%d.0", iTmp);
   //   vmemcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   //}

   //// Units
   //iTmp = atol(apTokens[L4_UNITS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%d", iTmp);
   //   vmemcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   //}

   //// Beds
   //iTmp = atol(apTokens[L4_BEDROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BEDS, iTmp);
   //   memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   //}

   //// Baths
   //iTmp = atol(apTokens[L4_BATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_F, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   //}

   //// HBaths
   //iTmp = atol(apTokens[L4_HALFBATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_H, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   //}

   //// Heating
   //int iCmp;
   //if (*apTokens[L4_HEATING] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L4_HEATING], asHeating_LDR[iTmp].acSrc, asHeating_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_HEAT) = asHeating_LDR[iTmp].acCode[0];
   //}

   //// Cooling
   //if (*apTokens[L4_AC] == 'C')
   //   *(pOutbuf+OFF_AIR_COND) = 'C';
   //else if (*apTokens[L4_AC] > ' ')
   //   LogMsg("*** Unknown A/C: %s", apTokens[L4_AC]);

   //// Pool/Spa
   //if (*apTokens[L4_POOLSPA] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L4_POOLSPA], asPool_LDR[iTmp].acSrc, asPool_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_POOL) = asPool_LDR[iTmp].acCode[0];
   //}

   //// Fire place
   //if (*apTokens[L4_FIREPLACE] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   pTmp = _strupr(apTokens[L4_FIREPLACE]);
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L4_FIREPLACE], asFP_LDR[iTmp].acSrc, asFP_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_FIRE_PL) = asPool[iTmp].acCode[0];
   //}

   //// Quality Class
   //acTmp1[0] = 0;
   //if (*apTokens[L4_QUALITYCLASS] > '0' || strlen(apTokens[L4_QUALITYCLASS]) > 1)
   //{
   //   strcpy(acTmp, apTokens[L4_QUALITYCLASS]);
   //   remCharEx(acTmp, " ,'?");
   //   pTmp = _strupr(acTmp);

   //   //memcpy(myCharRec.QualityClass, pTmp, strlen(pTmp));
   //   if (acTmp[0] == 'A' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'A';
   //   else if (acTmp[0] == 'F' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'F';
   //   else if (!memcmp(pTmp, "POOR", 4) || !memcmp(pTmp, "LOW", 3))
   //      acTmp1[0] = 'P';
   //   else if (*pTmp == 'G')
   //      acTmp1[0] = 'G';
   //   else if (isalpha(*pTmp))
   //   {
   //      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
   //      if (isdigit(acTmp[1]))
   //         iRet = Quality2Code(&acTmp[1], acTmp1, NULL);
   //      else if (isdigit(acTmp[2]))
   //         iRet = Quality2Code(&acTmp[2], acTmp1, NULL);
   //      else if (isalpha(acTmp[1]))
   //      {
   //         switch (acTmp[1])
   //         {
   //            case 'L':
   //            case 'P':
   //               acTmp1[0] = 'P';
   //               break;
   //            case 'A':
   //               acTmp1[0] = 'A';
   //               break;
   //            case 'F':
   //               acTmp1[0] = 'F';
   //               break;
   //            case 'G':
   //               acTmp1[0] = 'G';
   //               break;
   //         }
   //      }
   //   } else if (isdigit(*pTmp))
   //   {
   //      iTmp = atol(pTmp);
   //      if (iTmp < 100)
   //         iRet = Quality2Code(pTmp, acTmp1, NULL);
   //   }

   //   if (acTmp1[0] > '0')
   //      *(pOutbuf+OFF_BLDG_QUAL) = acTmp1[0];
   //}

   return 0;
}

/******************************** Sbt_Load_LDR4 *****************************
 *
 * Load LDR 2016
 *
 ****************************************************************************/

int Sbt_Load_LDR4(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
      if (!iRet)
         return -1;
   }

   // Open LDR file
   LogMsg("Open LDR file %s", acRollFile);
   fdLDR = fopen(acRollFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Roll file
   //GetIniString(myCounty.acCntyCode, "RollFile", "", acTmpFile, _MAX_PATH, acIniFile);
   //LogMsg("Open current Roll file %s", acTmpFile);
   //fdRoll = fopen(acTmpFile, "r");
   //if (!fdRoll)
   //{
   //   LogMsg("***** Error opening current Roll file: %s\n", acTmpFile);
   //   return -2;
   //}
   fdRoll = NULL;

   // Open Value file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Lien file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   } else
      fdLienExt = NULL;

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      // Create new R01 record
      iRet = Sbt_MergeLien4(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         if (fdLienExt)
            lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Situs
         if (fdSitus)
            lRet = Sbt_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = MB_MergeStdChar(acBuf, NULL, NULL, NULL);

         // Merge transfer from current roll
         if (fdRoll)
            lRet = Sbt_MergeCurRoll(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdLDR)
      fclose(fdLDR);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   //LogMsg("Number of Roll matched:   %u", lRollMatch);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/********************************* Sbt_MergeLien3 *****************************
 *
 * For 2017 LDR AGENCYCDCURRSEC_TR601.TXT
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sbt_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L3_ISAGPRESERVE)
   {
      LogMsg("***** Error: bad input LDR record for APN=%s (%d)", apTokens[L3_ASMT], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "35SBT", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: 
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lFixtRP+lGrow+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      
      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%u         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%u         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%u         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SBT_Exemption);

   // Legal
#ifdef _DEBUG
   //if (!memcmp(apTokens[iApnFld], "001221006000", 9) )
   //   iTmp = 0;
#endif
   // Legal - replace known bad char
   iTmp = replChar(apTokens[L3_PARCELDESCRIPTION], 221, 47);
   if (iTmp)
      LogMsg("***Fixing Legal [%.12s]: %s", pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      if (dTmp < 50000.0)
      {
         // Lot Sqft
         lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
         if (lTmp < 999999999)
         {
            sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
            memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
         }

         // Format Acres
         lTmp = (long)(dTmp * ACRES_FACTOR);
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      } else if (lTmp > 0)
      {
         SETACREAGE;
      } else
      {
         LogMsg("*** Question acreage: %.2f, LotSize=%u [%.12s].  Set LotAcres=0", dTmp, lTmp, pOutbuf);
      }
   } else if (lTmp > 0)
   {
         SETACREAGE;
   }

   // Owner
   Sbt_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   //Sbt_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Sbt_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // DocNum
   if (*(apTokens[L3_CURRENTDOCNUM]+4) == 'R')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);     // 2002-03-18 00:00:00.000
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         sprintf(acTmp, "%.5s%.7d", apTokens[L3_CURRENTDOCNUM], atol(apTokens[L3_CURRENTDOCNUM]+5));
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, 12);
      }
   }

   // AgPreserved
   if (iTokens > L3_ISAGPRESERVE && *apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   return 0;
}

/******************************** Sbt_Load_LDR3 *****************************
 *
 * Load LDR 2017-2019
 *
 ****************************************************************************/

int Sbt_Load_LDR3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
      if (!iRet)
         return -1;
   }

   // Open LDR file
   LogMsg("Open LDR file %s", acRollFile);
   fdLDR = fopen(acRollFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      // Create new R01 record
      iRet = Sbt_MergeLien3(acBuf, acRec);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Sbt_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = MB_MergeStdChar(acBuf, NULL, NULL, NULL);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   //LogMsg("Number of Roll matched:   %u", lRollMatch);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lLDRRecCount;
   return 0;
}

/**************************** Sbt_ConvStdChar ********************************
 *
 * Copy from MergeMno.cpp to convert new char file. 04/10/2018
 * Other counties has similar format: MER, YOL, HUM, MAD, MNO, SBT
 *
 *****************************************************************************/

int Sbt_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   long     lGarSqft;

   STDCHAR  myCharRec;

   LogMsg0("\nConverting char file %s", pInfile);

   // Sort input file
   //sprintf(acTmpFile, "%s\\%s\\%s_char.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   //LogMsg("Sort char file %s to %s", pInfile, acTmpFile);
   // Sort on Asmt 
   //iRet = sortFile(pInfile, acTmpFile, "S(#1,C,A) DEL(124) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
   //sprintf(acTmp, "S(#%d,C,A) DEL(%d) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ", SBT_CHAR_ASMT+1, cDelim);
   //iRet = sortFile(pInfile, acTmpFile, acTmp);
   //if (iRet < 500)
   //{
   //   LogMsg("***** Input file is too small.");
   //   return 1;
   //}

   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Start loop
   while (!feof(fdIn))
   {
      // Get next record
      pRec = fgets(acBuf, 4096, fdIn);
      iCnt++;
      if (!pRec || acBuf[0] > '9')
         break;
      if (*pRec < '0')
      {
         LogMsg("*** No APN in CHAR record at %d.", iCnt);
         continue;
      }


#ifdef _DEBUG
      //if (!memcmp(acBuf, "054411010000", 9))
      //   iRet = 0;
#endif

      replNull(acBuf, ' ');
      iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < SBT_CHAR_HASWELL)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[SBT_CHAR_ASMT], strlen(apTokens[SBT_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[SBT_CHAR_FEEPARCEL], strlen(apTokens[SBT_CHAR_FEEPARCEL]));

      // Format APN
      if (*apTokens[SBT_CHAR_ASMT] > ' ')
         iRet = formatApn(apTokens[SBT_CHAR_ASMT], acTmp, &myCounty);
      else
         iRet = formatApn(apTokens[SBT_CHAR_FEEPARCEL], acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      // Bldg#
      iTmp = atoi(apTokens[SBT_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %u", iTmp);

      // Rooms
      iTmp = atoi(apTokens[SBT_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - not avail
      iTmp = blankRem(apTokens[SBT_CHAR_POOLSPA]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[SBT_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass 
      iTmp = remChar(apTokens[SBT_CHAR_QUALITYCLASS], ' ');    // Remove all blanks before process
      pRec = _strupr(apTokens[SBT_CHAR_QUALITYCLASS]);
      vmemcpy(myCharRec.QualityClass, apTokens[SBT_CHAR_QUALITYCLASS], SIZ_CHAR_QCLS);

      if (*apTokens[SBT_CHAR_QUALITYCLASS] > '0' && *apTokens[SBT_CHAR_QUALITYCLASS] <= 'Z')
      {
         acCode[0] = ' ';
         strcpy(acTmp, apTokens[SBT_CHAR_QUALITYCLASS]);
         if (isalpha(acTmp[0])) 
         {
            myCharRec.BldgClass = acTmp[0];

            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            else if (isdigit(acTmp[2]))
               iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
            else if (!_memicmp(acTmp, "ST", 2))
               memcpy(myCharRec.QualityClass, "ST", 2);     // Steel
         } else if (isdigit(acTmp[0]))
            iRet = Quality2Code(acTmp, acCode, NULL);
         else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[SBT_CHAR_QUALITYCLASS], apTokens[SBT_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (*apTokens[SBT_CHAR_QUALITYCLASS] > ' ' && *apTokens[SBT_CHAR_QUALITYCLASS] != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[SBT_CHAR_QUALITYCLASS], apTokens[SBT_CHAR_ASMT]);

      // YrBlt
      int iYrBlt = atoi(apTokens[SBT_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[SBT_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[SBT_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[SBT_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // Stories/NumFloors
      iTmp = atoi(apTokens[SBT_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 99)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[SBT_CHAR_ATTACHGARAGESF]);
      lGarSqft = iAttGar;
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[SBT_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         lGarSqft += iAttGar;
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport Sqft
      int iCarport = atoi(apTokens[SBT_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         lGarSqft += iCarport;
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      iTmp = atol(myCharRec.GarSqft);
      if (lGarSqft > iTmp)
      {
         iRet = sprintf(acTmp, "%d", lGarSqft);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
      }

      // Patio SF
      iTmp = atoi(apTokens[SBT_CHAR_PATIOSF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "001042001000", 9))
      //   iRet = 0;
#endif

      // Heating 
      iTmp = blankRem(apTokens[SBT_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCodeA(apTokens[SBT_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[SBT_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[SBT_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[SBT_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[SBT_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[SBT_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[SBT_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[SBT_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace 
      if (*apTokens[SBT_CHAR_FIREPLACE] >= '1' && *apTokens[SBT_CHAR_FIREPLACE] <= '9')
         myCharRec.Fireplace[0] = *apTokens[SBT_CHAR_FIREPLACE];
      else if (*apTokens[SBT_CHAR_FIREPLACE] > ' ')
      {
         pRec = findXlatCode(apTokens[SBT_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      } 

      // Haswell - not avail
      blankRem(apTokens[SBT_CHAR_HASWELL]);
      if (*(apTokens[SBT_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Lot Sqft
      if (iFldCnt >= SBT_CHAR_LOTSQFT)
      {
         ULONG    lSqft;
         lSqft = (ULONG)atol(apTokens[SBT_CHAR_LOTSQFT]);
         if (lSqft > 1)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(lSqft*SQFT_MF_1000);
            iTmp = sprintf(acTmp, "%u", (long)(dTmp+0.5));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nDedup char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum, UnitSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/*********************************** loadSbt ********************************
 *
 * Options:
 *    -L  (load lien)
 *    -U  (load update)
 *    -Xl (extract lien value)
 *    -Xs (extract sale data)
 *    -O  (overwrite log file)
 *
 * LDR load:      -CSBT -L -Xl -Ms -Xa -O
 * Normal update: -CSBT -U -Xs -Xa -O
 *
 ****************************************************************************/

int loadSbt(int iSkip)
{
   int   iRet=0;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;

   iApnLen = myCounty.iApnLen;

   // Load tax
   //if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   //{
   //   iRet = Load_TC(myCounty.acCntyCode, bTaxImport);
   //} else if (iLoadTax == TAX_UPDATING)            // -Ut
   //{
   //   iRet = Update_TC(myCounty.acCntyCode, bTaxImport);
   //}
   if (iLoadTax == TAX_LOADING)              // -T or -Lt
   {
      // Load tax base
      iRet = MB_Load_TaxBase(bTaxImport, true, 2, 0);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet |= MB_Load_TaxCodeMstr(bTaxImport, 0);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport, 0);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport, 0);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
      if (!iLoadFlag)
         return iRet;
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Fix Lien extract
   //iRet = PQ_FixLienExt(myCounty.acCntyCode, 1, 2);
   //iRet = PQ_FixR01(myCounty.acCntyCode, 1, 2, iSkip);

   // Extract lien file
   //if (iLoadFlag & EXTR_LIEN)    // -Xl
      // LDR 2014 - import xlsx file into SQL then export to txt file Sbt_2014.txt
      //iRet = Sbt_ExtrLien(myCounty.acCntyCode);
      //iRet = MB_ExtrTR601(myCounty.acCntyCode);     // 2010, 2012, 2013
      //iRet = Sbt_ExtrTR601(myCounty.acCntyCode);    // 2011

   // Extract lien file - LDR2016
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acValueFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acValueFile, 0);      // 2016 
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode, NULL, 0);

      if (iRet)
         return iRet;
   }

   // Create/Update cum sale file
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // 01/03/2014
      iRet = MB_CreateSCSale(MM_DD_YYYY_1, 1, 4, false, (IDX_TBL5 *)&SBT_DocCode[0]);
      if (iRet)
         return iRet;

      iLoadFlag |= MERG_CSAL;
   }

   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      if (!_access(acCharFile, 0))
      {
         iRet = Sbt_ConvStdChar(acCharFile);
         if (iRet <= 0)
         {
            LogMsg("***** Error converting Char file %s", acCharFile);
            return -1;
         }
      } else
         LogMsg("*** Char file not available: %s.  Use old extract Char file.", acCharFile);
   }

   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {      
      if (iLoadFlag & LOAD_LIEN)
      {
         // Create Lien file
         LogMsg0("Load %s Lien file", myCounty.acCntyCode);
         // 2013 and before
         //iRet = Sbt_Load_LDR(iSkip);
         // 2014
         //iRet = Sbt_Load_LDR2(iSkip);
         // 2016
         //iRet = Sbt_Load_LDR4(iSkip);
         // 2017
         iRet = Sbt_Load_LDR3(iSkip);
      } else if (iLoadFlag & LOAD_UPDT)
      {
         if (acRollFile[0])
         {
            LogMsg0("Load %s roll update file", myCounty.acCntyCode);
            iRet = Sbt_Load_Roll(iSkip);
         } else
         {
            LogMsg("***** Please specify Rollfile in [%s] section of %s", myCounty.acCntyCode, acIniFile);
            iRet = -1;
         }
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   return iRet;
}