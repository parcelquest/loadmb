/**************************************************************************
 *
 *    - Load lien: -L -CSON -O -Xs|-Ms -Xa -Xl
 *    - Load update: -CSON -U -Xa -Xs|-Ms -O [-T] [-Xf] [-Mf] [-Mg]

 * Notes:
 *    - Need update Heating, Cooling, and Pool table.
 *    - County provides cumulative sale file, no need to append new sale to existing file
 *
 * Revision
 * 04/02/2006 1.3.12.1 Update due to change in SON_CHAR.CSV format
 * 09/06/2006 1.7.2.1  Adding option to exclude unsecured parcels APN 800-999
 * 02/28/2007 1.9.2    Sort roll file.
 * 01/26/2008 1.10.3.1 Add code to support standard usecode
 * 03/05/2008 1.10.5.1 Add code to output update records.  
 * 07/03/2008 8.1.1    Modify Son_Load_LDR() to support new TR601 format.
 * 02/26/2009 8.6      Add -Mo option, modify Son_MergeLien() to populate other values.
 * 03/30/2009 8.7.1    Use MB_MergeSale() to do sale update instead of MergeSale3() to fix 
 *                     problem that non-sale transaction overwrites sale transaction that  
 *                     occurs in the same day  since we only keep one transaction per day.
 * 05/21/2009 8.7.6    Remove reference to OldApn & NewApn files.  We no longer need them.
 * 07/27/2009 9.1.3    Change processing method using iSkipQuote & cDelim to determine
 *                     input record format.  Apply APN exclusion to lien roll as well as 
 *                     updated roll.  Use MB_ExtrTR601() instead of MB_ExtrLien().
 * 08/03/2009 9.1.4.1  Fix potential bug in Son_MergeRoll() that may overide SALE1_DT.
 * 10/09/2009 9.2.3    Fix Son_MergeOwner() to keep NAME1 the same as county provided.
 * 11/24/2009 9.2.7    Use situs street as is, no parsing.  Pad blank to output.
 * 01/05/2010 9.2.11   Blank out YrBlt if it is greater than year assessed.
 * 08/02/2010 10.3.0   Modify Son_MergeLien() to save EXE_CD, reformat TRA, add L_TAXABILITY.
 *                     Add -Xs option to save sale history.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 04/15/2011 10.7.1   Modify loadSon() to support rerun of 2007 LDR and older.
 * 06/06/2011 10.9.1   Exclude HomeSite from OtherVal. Replace Son_MergeExe() with MB_MergeExe(),
 *                     Son_MergeTax() with MB_MergeTax(), and Son_ExtrSale() with MB_CreateSCSale().
 * 07/12/2011 11.1.3   Add S_HSENO.
 * 08/08/2012 12.2.5   Add Son_MergeZone() to merge Zoning from updated roll for LDR processing.
 *                     Modify Son_Load_LDR() to merge Zoning.
 * 10/19/2012 12.4.1   Fix OFF_S_ADDR_D and clean up DOCNUM.
 *                     Remove sale update from Load_Roll() and use ApplyCumSale().
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 07/17/2013 13.2.5   Fix LEGAL in Son_MergeRoll() and Son_MergeLien(). Remove sale update
 *                     from Son_Load_LDR() and use ApplyCumSale() instead.
 * 07/21/2013 13.3.5   Add SON_DocCode[] table.
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 10/17/2013 13.17.2  Modify Son_Load_Roll() to use MB_MergeStdChar() to update CHAR.
 *                     Add Son_ConvStdChar() to convert Son_Char.csv to fixed length STDCHAR format.
 *                     This function also extract LandUse from roll file for Lexis request.
 *                     Modify Son_ConvStdChar() to parse QualityClass for Son_Char_dat.
 * 11/01/2013 13.18.0  Update Heating, Cooling, and Pool tables. Clean up Son_Load_Roll().
 *                     Modify Son_ConvStdChar() to encode Heating, Cooling, and Pool.
 * 01/02/2014 13.19.0  Update DocCode[] table and use FixDocType() to cleanup cumsale file.
 * 05/21/2015 14.15.9  Add -G option and Son_LoadGrGr() to load GrGr data.
 * 07/31/2015 15.2.0   Add DBA to Son_MergeMAdr()
 * 02/23/2016 15.12.3  New data layout.  Create new Son_ConvStdChar() & Son_MergeStdChar().
 * 04/06/2016 15.14.3  Load TC data
 * 04/11/2016 15.14.5  Modify Son_LoadGrGr() to return correct error code when no data avail.
 *                     and to send out correct email msg.
 * 08/02/2016 16.1.0   Add Son_Load_LDR4() to support new LDR layout.
 * 09/15/2016 16.4.3   Modify Son_MergeStdChar() not to overwrite LotAcres & LotSqft.
 *            16.4.3.1 Calculate LotAcres when char file doesn't have it in Son_MergeStdChar()
 * 07/27/2017 17.0.2   Fix bug in Son_MergeZone() when zoning includes double quote.
 *                     Add Son_MergeLien5() for new 2017 layout.
 * 07/28/2017 17.0.3   Rename Son_MergeLien5() to Son_MergeLien3() since it used Grp 3 layout.
 *                     Modify Son_MergeStdChar() to update LotSqft overwrite existing value from roll file.
 * 03/27/2018 17.9.0   Add -Ut option to update tax.
 * 07/19/2019 19.0.5   Add AgPreserved and remove duplicate code in Son_MergeLien3().
 * 04/27/2020 19.9.0   Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 07/07/2020 20.1.2   Modify Son_MergeMAdr() to add support for mailadr from ITALY.
 * 11/01/2020 20.4.2   Modify Son_MergeRoll() & Son_MergeZone() to populate default PQZoning.
 * 02/10/2021 20.7.0   Modify loadSon() to use sale date format specify in INI file.
 * 02/14/2021 20.7.1   Add Son_MakeDocLink() to assign local doc links.
 * 02/23/2021 20.7.4   Modify Son_MergeRoll() to populate TRANSFER only if it's a 'R' doc number.
 * 03/31/2022 21.7.3   Modify Son_CreateGrGrRec() to update NoneSale & NoneXfer flags.
 * 04/25/2022 21.9.0   Use acGrGrBakTmpl as backup folder for GrGr files. 
 * 08/24/2022 22.2.0   Modify Son_Load_LDR4() to determine whether to use value from TC601.
 * 11/17/2022 22.4.0   Add Son_ExtrVal() to create value file Value_exp.SON via -Xf option.
 * 11/19/2022 22.4.1   Add feature to check for delimiter in CHAR file Son_Char.csv in Son_ConvStdChar().
 * 11/27/2022 22.4.2   Modify Son_CreateValueRec() to fix duplicate value issue.  The CURRENTFIXEDIMPRVALUE
 *                     in tax file is the same as FIXT_RP value in roll file.
 * 02/09/2023 22.6.5   Add Son_CreateSCSale() to replace MB_CreateSCSale() to process SON only.  
 * 02/10/2023 22.6.5.2 Clear old sales before applying new one.
 * 02/15/2023 22.6.7   Modify Son_LoadGrGr() to return error correctly when GrGr file is empty.
 * 01/04/2024 23.5.0.1 Modify Son_MergeLien3() to set default HO_FL='2'.
 * 02/05/2024 23.6.0   Modify Son_MergeSitus() & Son_MergeMAdr() to populate UnitNox.
 * 09/17/2024 24.1.4   Modify Son_MergeLien3() to add ExeType.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "CharRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "UseCode.h"
#include "Update.h"
#include "PQ.h"
#include "Tax.h"

#define _SONOMA_     1
#include "LoadMB.h"
#include "MBExtrn.h"
#include "doGrgr.h"
#include "MergeSon.h"


extern   APN_RNG     asExclApns[];
extern   XREFTBL     asDeed[];
extern   int         iNumDeeds;

/******************************** Son_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Son_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64], acOwner[64];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false, false);
   memset(acTmp, 0, 128);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1420018000", 10))
   //   iTmp = 0;
#endif

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name, double quote, '*'
      if (((*pTmp < '1' || *pTmp == '*') && iTmp == 0) || (*pTmp == 34))
      {
         pTmp++;
         continue;
      }

      if (isalpha(*pTmp))
         acTmp[iTmp++] = *pTmp & 0x5F;    // Make upper case
      else
         acTmp[iTmp++] = *pTmp;

      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acName2[0] = 0;
   acSave1[0] = 0;

   if (pTmp=strstr(acTmp, " 1/2 INT"))
      *pTmp = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP);
      return;
   }

   // Save owner name for NAME1
   memcpy(acOwner, acTmp, SIZ_NAME1);
   acOwner[SIZ_NAME1] = 0;

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
      // If TRUST appears after number, save from number forward
      if (strstr((char *)&acTmp[iTmp], " TRUST") )
      {
         iTmp--;
         strcpy(acSave1, (char *)&acTmp[iTmp]);
         acTmp[iTmp] = 0;
      }
   }

   if ((pTmp = strchr(acTmp, '(')) || (pTmp = strchr(acTmp, '%')) )
      *pTmp = 0;        // Drop what in parenthesis

   // Filter out words
   // MONDANI NELLIE M ESTATE OF
   if ((pTmp=strstr(acTmp, " TRUSTE")) || (pTmp=strstr(acTmp, " ET AL")) ||
      (pTmp=strstr(acTmp, " TR OF"))   || (pTmp=strstr(acTmp, " ET AL"))  || 
      (pTmp=strstr(acTmp, " ETAL"))    || (pTmp=strstr(acTmp, " TRET AL")) || 
      (pTmp=strstr(acTmp, " **"))      || (pTmp=strstr(acTmp, " CO-TR"))   )
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " LAND TRUST"))
   {
      strcpy(acSave1, " LAND TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " FAMILY "))
             || (pTmp=strstr(acTmp, " INCOME TR")) || (pTmp=strstr(acTmp, " LIVING TR")) )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " ESTATE OF")) || (pTmp=strstr(acTmp, " LIFE ESTATE")) ||
              (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTS OF"))  )
   {  // MONDANI NELLIE M ESTATE OF
      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TR"))
   {
      if (*(pTmp+3) <= ' ')
         *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Save Name1
   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(acTmp1, myOwner.acName1);
      if (myOwner.acVest[0] > ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, SIZ_NAME_SWAP);
      else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
   {
      // Couldn't split names, use Name1
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
   }

}

/******************************** Son_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Son_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "013212023000", 9) )
   //   iTmp = 0;
#endif

   // Update DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   } else if (!memcmp(apTokens[MB_ROLL_CAREOF], "DBA ", 4))
   {
      pTmp = apTokens[MB_ROLL_CAREOF];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   }

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ' && memcmp(apTokens[MB_ROLL_CAREOF], "DBA ", 4))
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, iTmp);

   // Parse mail address
   parseMAdr1_1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
     sMailAdr.strName[SIZ_M_STREET] = 0;
     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
      vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
   }

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], SIZ_M_CITY);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], SIZ_M_ZIP+SIZ_M_ZIP4);

      iTmp = strlen(apTokens[MB_ROLL_M_ZIP]);
      if (iTmp == 10 && *(apTokens[MB_ROLL_M_ZIP]+5) == '-')
         sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
      else if (iTmp == 9)
         sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP], apTokens[MB_ROLL_M_ZIP]+5);
      else
         sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);

      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }

}

/*****/
void Son_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;
   bool  bForeign=false;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "056400065", 9) || !memcmp(pOutbuf, "007113047", 9))
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == '#') *pLine1 = ' ';
   blankRem(pLine1);
   blankRem(pLine2);

   // Ignore blank address
   if (!*pLine1 && !*pLine2)
      return;

   if (*pLine1 == '0' || *pLine2 == '0')
   {
      LogMsg("??? Mail Addr of : %.12s may be bad [%s][%s]", pOutbuf, pLine1, pLine2);
      if (!memcmp(pLine1, "0N", 2))
         pLine1 += 2;
   }

   acAddr1[0] = 0;
   p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else if (!memcmp(pLine4, "ITALY", 5))
      {
         bForeign = true;
         p1 = pLine1;
         strcpy(acAddr1, pLine1);
         sprintf(acAddr2, "%s, %s %s", pLine2, pLine3, pLine4);
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (*pLine1 > '0' && *pLine1 <= '9')
            p1 = pLine1;
         else
         {
            // If last word of line 3 is number and first word of line 1 is alpha, 
            // line 1 more likely be CareOf
            if (strlen(pLine1) > 2)
               p0 = pLine1;
            else
               p0 = NULL;
            p1 = pLine2;
         }
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      updateCareOf(pOutbuf, p0, strlen(p0));

      //iTmp = strlen(p0);
      //if (iTmp > SIZ_CARE_OF)
      //   iTmp = SIZ_CARE_OF;
      //memcpy(pOutbuf+OFF_CARE_OF, p0, iTmp);
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ')
         {
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
            vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
         }
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      } else
      {
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
      }
   }

   if (!bForeign)
      strcpy(acAddr2, p2);
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   if (!bForeign)
      parseAdr2_1(&sMailAdr, acAddr2);
   else
   {
      sprintf(sMailAdr.City, "%s %s", pLine3, pLine4);
   }
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      vmemcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);     
   }
}

/******************************** Son_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Son_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdSitus);
      // Get first rec
      if (!memcmp(&acRec[iSkipQuote], "Asmt", 4))
         pRec = fgets(acRec, 512, fdSitus);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Replace tab char with 0
   if (pRec)
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Remove old situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));
      iTmp = sprintf(acTmp, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      strcpy(acAddr1, acTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
      }
   }

   /* Do not parse street name ?????
   // Parse street name
   ADR_REC  sAdr;
   memset((void *)&sAdr, 0, sizeof(ADR_REC));
   parseAdrND(&sAdr, apTokens[MB_SITUS_STRNAME]);
   if (sAdr.strDir[0] >= 'E' && *apTokens[MB_SITUS_STRDIR] < 'E')
   {
      strcat(acAddr1, sAdr.strDir);
      strcat(acAddr1, " ");
      *(pOutbuf+OFF_S_DIR) = sAdr.strDir[0];
   }

   strcat(acAddr1, sAdr.strName);
   blankPad(sAdr.strName, SIZ_S_STREET);
   */

   strcpy(acTmp, apTokens[MB_SITUS_STRNAME]);
   strcat(acAddr1, acTmp);
   blankPad(acTmp, SIZ_S_STREET);
   memcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET);

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);

      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
         Sfx2Code(acTmp, acCode);
      else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } 
   /*
   else if (sAdr.strSfx[0] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, sAdr.strSfx);

      iTmp = GetSfxCodeX(sAdr.strSfx, acTmp);
      if (iTmp > 0)
         Sfx2Code(acTmp, acCode);
      else
      {
         LogMsg0("*** Invalid suffix: %s", sAdr.strSfx);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   }
   */

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009146018", 9))
   //   acTmp[0] = 0;
#endif

   if (isalnum(*apTokens[MB_SITUS_UNIT]) || *apTokens[MB_SITUS_UNIT] == '#')
   {
      strcat(acAddr1, " ");
      if (isdigit(*apTokens[MB_SITUS_UNIT]))
         strcat(acAddr1, "#");
      else if ((*apTokens[MB_SITUS_UNIT]) != '#')
         strcat(acAddr1, "STE ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNOX);
   } else if (*apTokens[MB_SITUS_UNIT] > ' ')
      LogMsg("Invalid situs Unit#: %s [%s]", apTokens[MB_SITUS_UNIT], apTokens[MB_SITUS_ASMT]);

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1);   
      blankPadz(acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
      {
         iTmp = sprintf(acTmp, "%s CA", myTrim(acAddr1));
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      }
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/*****/
int Son_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128], *pTmp;
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004130013", 9))
   //   acTmp[0] = 0;
#endif
   // 
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

#ifdef _DEBUG
   // Check for '-'
   //if (pTmp = isCharIncluded(acAddr1, '-', 0))
   //   pTmp++;
#endif
   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   if (sSitusAdr.lStrNum > 0)
   {
      pTmp = strchr(acAddr1, ' ');
      *pTmp = 0;

      // Save original StrNum
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
      memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   }

   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, sSitusAdr.UnitNox, SIZ_S_UNITNOX);
   }

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/******************************** Son_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Son_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg("Skip Sale rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   if (iTmp)
      return 1;

   while (!iTmp)
   {
      // Parse input
      if (pRec)
         iRet = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "001012014", 9))
      //   iRet = 0;
#endif

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         // New format change 06.2006
         // Old format: 1/1/1993 0:00:00
         // pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
         // New format: 1993-01-01 00:00:00
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, YYYY_MM_DD);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax - new format on 06/21/2006 is no longer has $ but we can
         // still call dollar2Num() since it's very fast.
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         /* Do not use Confidential Sale Price
         } else
         {
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            }
         */
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] == '1' ||
             *apTokens[MB_SALES_GROUPSALE] == 'T' ||
             *apTokens[MB_SALES_GROUPSALE] == 'Y')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         MB_MergeSale(&sCurSale, pOutbuf, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Son_MergeChar ******************************
 *
 * Note: need code table for Heating and Colling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

int Son_MergeChar(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], *pTmp;
   long     lTmp, lBldgSqft, lGarSqft, lOldYrBlt, lOldBldgSqft, lOldGarSqft;
   int      iRet, iLoop, iBeds, iFBath, iHBath, iFp, iUnitSeq, iOldUnitSeq;
   MB_CHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=iUnitSeq=iOldUnitSeq=0;
   lBldgSqft=lGarSqft=0;
   lTmp=0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "173020005", 9))
   //   iTmp = 0;
#endif

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 512, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   lOldYrBlt = 1700;
   lOldBldgSqft=lOldGarSqft=10;
   while (!iLoop)
   {
      pChar = (MB_CHAR *)pRec;

      // Quality Class
      acCode[0] = 0;
      if (pChar->QualityClass[0] > '0')
      {
         memcpy(acTmp, pChar->QualityClass, MBSIZ_CHAR_QUALITY);
         iRet = blankRem(acTmp, MBSIZ_CHAR_QUALITY);
         pTmp = strchr(acTmp, ' ');
         if (pTmp) *pTmp = 0;

         if (strstr(acTmp, "AVG"))
         {
            strcpy(acCode, "A");
            if (iRet > 3)
               *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0];
         } else if (isalpha(acTmp[0]))
         {
            *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0];
            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            else if (pTmp && isdigit(*(pTmp+1)))
               iRet = Quality2Code(pTmp+1, acCode, NULL);
         } else if (acTmp[0] > '0' && acTmp[0] <= '9')
            iRet = Quality2Code(acTmp, acCode, NULL);

         blankPad(acCode, SIZ_BLDG_QUAL);
         memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);

         // Yrblt
         lTmp = atoin(pChar->YearBuilt, MBSIZ_CHAR_YRBLT);
         if (lTmp > lOldYrBlt && lTmp <= lLienYear)
         {
            memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);
            lOldYrBlt = lTmp;
         } else if (!lTmp || lTmp > lLienYear)
            memset(pOutbuf+OFF_YR_BLT, ' ', SIZ_YR_BLT);

         // BldgSqft
         lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
         if (lBldgSqft > lOldBldgSqft)
         {
            sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
            memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
            lOldBldgSqft = lBldgSqft;
         }

         // Garage Sqft
         lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
         if (lGarSqft > lOldGarSqft)
         {
            sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
            memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
            *(pOutbuf+OFF_PARK_TYPE) = 'Z';
            lOldGarSqft = lGarSqft;
         }
      }
/*
      // Heating
      if (pChar->Heating[0] > ' ')
      {
         iTmp = 0;
         while (asHeating[iTmp].iLen > 0)
         {
            if (pChar->Heating[0] == asHeating[iTmp].acSrc[0])
            {
               *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }

      // Cooling
      if (pChar->Cooling[0] > ' ')
      {
         iTmp = 0;
         while (asCooling[iTmp].iLen > 0)
         {
            if (pChar->Cooling[0] == asCooling[iTmp].acSrc[0])
            {
               *(pOutbuf+OFF_AIR_COND) = asCooling[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }
      // Pool
      if (pChar->NumPools[0] > ' ')
      {
         iTmp = 0;
         while (asPool[iTmp].iLen > 0)
         {
            if (pChar->NumPools[0] == asPool[iTmp].acSrc[0])
            {
               *(pOutbuf+OFF_POOL) = asPool[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }
   */

      // New format doesn't have DocNo
      //if (pChar->DocNo[0] == ' ')
      {
         // Beds
         iBeds += atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
         if (iBeds > 0)
         {
            sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
            memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
         }

         // Baths
         iFBath += atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS-1);
         if (iFBath > 0)
         {
            sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
            memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
         } 

        // Half bath
         iHBath += atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
         if (iHBath > 0)
         {
            sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
            memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
         }
         // Fireplace
         iFp += atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
         if (iFp > 0)
         {
            sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
            memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
         }
      }

      iUnitSeq = atoin(pChar->UnitSeqNo, MBSIZ_CHAR_UNITSEQNO);
      if (iUnitSeq > iOldUnitSeq)
      {
         sprintf(acTmp, "%*d", SIZ_UNITS, iUnitSeq);
         memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
         iOldUnitSeq = iUnitSeq;
      }
      
      // Get next Char rec
      pRec = fgets(acRec, 512, fdChar);
      if (!pRec)
         break;

      iLoop = memcmp(pOutbuf, pRec, iApnLen);
   }

   lCharMatch++;
   return 0;
}

/******************************* Son_MergeStdChar *****************************
 *
 * Merge ???_Char.dat in STDCHAR format
 *
 * Note: 
 *   - Copy from MB_MergeStdChar() and modify for SON.
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the one that has value.
 *   - If XLAT_CODE tables are not provided, copy data as is.
 *   - Update data only, don't wipe out existing data if no new value.
 *
 *****************************************************************************/

int Son_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lTmp;
   int      iLoop, iBeds, iFBath, iHBath, iFp, iTmp;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   pChar=(STDCHAR *)pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Quality Class
   if (pChar->BldgClass > ' ')
   {
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
      *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   }

   // Yrblt
   lTmp = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // YrEff
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "173730004", 9) )
   //   iTmp = 0;
#endif

   // LotSqft - can overwrite value from roll file
   ULONG lLotSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lLotSqft > 100)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = atoin(pChar->LotAcre, SIZ_CHAR_SQFT);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      } else if (*(pOutbuf+OFF_LOT_ACRES+8) = ' ')
      {
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(double)(lLotSqft*SQFT_MF_1000+0.5));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   } 

   // BldgSqft
   long lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 100)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   long lSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   } else
      memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);

   // Patio Sqft
   lSqft = atoin(pChar->PatioSqft, SIZ_CHAR_SQFT);
   if (lSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_PATIO_SF, lSqft);
      memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
   } else
      memset(pOutbuf+OFF_PATIO_SF, ' ', SIZ_PATIO_SF);

   // Parking type
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];  

   // Heating
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
      
   // Cooling 
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Pools
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   }

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   }

   // Fireplace
   iFp = atoin(pChar->Fireplace, SIZ_CHAR_SIZE2);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   }

   // HasSeptic or HasSewer
   if (pChar->HasSewer > ' ')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWell
   if (pChar->HasWater > ' ')
      *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Units
   iTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (pChar->Units[0] > ' ')
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Stories
   if (pChar->Stories[0] > ' ')
      memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************** Son_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************

int Son_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdExe);

      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      iTmp = memcmp(pOutbuf, pTmp+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   iRet = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1' || *apTokens[MB_EXE_HOEXE] == 'T')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   if (memcmp(apTokens[MB_EXE_EXEAMT], "99999999", 8))
   {
      lTmp = atol(apTokens[MB_EXE_EXEAMT]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Son_MergeTax ******************************
 *
 * Note: Like other files, the format of dollar amount and date have been 
 *       changed.  Header is no longer available.
 *
 *****************************************************************************

int Son_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double	dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iRet;

   // Merge tax data
   while (!iTmp)
   {
      iTmp = ParseStringNQ(pRec, cDelim, MB_TAX_ROLLCAT+1, apTokens);
      if (iTmp < MB_TAX_ROLLCAT)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
         return -1;
      }

      // Tax Amt - calling dollar2Num() just in case county changes their mind again
      dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
      dTax1 = atof(acTmp);
      dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
      dTax2 = atof(acTmp);
      dTmp = dTax1+dTax2;
      if (dTax1 == 0.0 || dTax2 == 0.0)
         dTmp *= 2;

      if (dTmp > 0.0)
      {
         sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
         memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
      } else
         memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/********************************* Son_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Son_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse roll record
   iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Skip excluding APN
   //if (memcmp(apTokens[iApnFld], asExclApns[0].acApn, asExclApns[0].iLen) >= 0 &&
   //    memcmp(apTokens[iApnFld], asExclApns[1].acApn, asExclApns[1].iLen) <= 0 )
   //    return 1;

   // Ignore APN starts with 800-999 except 910 & 911 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910 && iTmp != 911))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "49SON", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   } 

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

//#ifdef _DEBUG
//   if (!memcmp(apTokens[iApnFld], "001021008000", 9) )
//      iTmp = 0;
//#endif
   // Legal
   iTmp = replChar(apTokens[MB_ROLL_LEGAL], 221, 47);
   if (iTmp)
      LogMsg("***Fixing Legal [%.12s]: %s", pOutbuf, apTokens[MB_ROLL_LEGAL]);

   iTmp = replUnPrtChar(apTokens[MB_ROLL_LEGAL], ' ');
   iTmp = updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   acTmp[SIZ_USE_CO] = 0;
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

#ifdef _DEBUG
   //if (!memcmp(apTokens[iApnFld], "173730004", 9) )
   //   iTmp = 0;
#endif

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[iApnFld], "910004151000", 9) )
   //   iTmp = 0;
#endif
   // Recorded Doc
   memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
   memset(pOutbuf+OFF_TRANSFER_DT,  ' ', SIZ_TRANSFER_DT);
   if (*(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
   {
      vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], SIZ_TRANSFER_DOC);
      // New format change 06.2006
      // Old format: 1/1/1993 0:00:00
      // pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      // New format: 1993-01-01 00:00:00
      // pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, YYYY_MM_DD);
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);     // 02/19/2016
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   }

   // Owner
   try {
      Son_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   } catch(...) {
      LogMsg("***** Exeception occured in Son_MergeOwner()");
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[iApnFld], "004161088000", 9) )
   //   iTmp = 0;
#endif
   // Mailing
   try {
      if (*apTokens[MB_ROLL_M_ADDR] > '0' && *apTokens[MB_ROLL_M_ADDR] <= '9')
         Son_MergeMAdr(pOutbuf);
      else
         Son_MergeMAdr(pOutbuf, apTokens[MB_ROLL_M_ADDR1], apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3], apTokens[MB_ROLL_M_ADDR4]);
   } catch(...) {
      LogMsg("***** Exeception occured in Son_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************** Son_Load_Roll *****************************
 *
 * This function has not been tested.
 *
 ******************************************************************************/

int Son_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A) Omit(2,1,C,GT,\"9\")");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   if (!_access(acCChrFile, 0))
   {
      fdChar = fopen(acCChrFile, "r");
      if (fdChar == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCChrFile);
         return -2;
      }
   } else
      LogMsg("*** Missing Char file %s", acCChrFile);

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }
   
   // Open Sales file
   //LogMsg("Open Sales file %s", acSalesFile);
   //fdSale = fopen(acSalesFile, "r");
   //if (fdSale == NULL)
   //{
   //   LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
   //   return -2;
   //}
   fdSale = NULL;

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Open Tax file
   if (!_access(acTaxFile, 0))
   {
      LogMsg("Open Tax file %s", acTaxFile);
      fdTax = fopen(acTaxFile, "r");
      if (fdTax == NULL)
      {
         LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
         return -2;
      }
   } else
      fdTax = (FILE *)NULL;

   // Open lien file
   /*
   fdLienExt = NULL;
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Lien file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   }
   */

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Son_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Son_MergeSitus(acBuf);

            // Merge Lien
            //lRet = 1;
            //if (fdLienExt)
            //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

            // Merge Exe if not found in LienExt
            //if (fdExe && lRet)
            //   lRet = MB_MergeExe(acBuf);

            // Merge Char
            if (fdChar)
               //lRet = Son_MergeChar(acBuf);
               lRet = Son_MergeStdChar(acBuf);

            // Remove old sale data
            //if (bClearSales)
            //   ClearOldSale(acBuf);

            // Merge Sales
            //if (fdSale)
            //   lRet = Son_MergeSale(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Son_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Son_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Son_MergeStdChar(acRec);
               //lRet = Son_MergeChar(acRec);

            // Merge Sales
            //if (fdSale)
            //   lRet = Son_MergeSale(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("Retired record? : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof && isdigit(acRollRec[1]))
   {
      // Create new R01 record
      iRet = Son_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Merge Situs
         if (fdSitus)
            lRet = Son_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Son_MergeStdChar(acRec);
            //lRet = Son_MergeChar(acRec);

         // Merge Sales
         //if (fdSale)
         //   lRet = Son_MergeSale(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTax(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }


   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   //if (fdSale)
   //   fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   //LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   //LogMsg("Number of Exe skiped:       %u", lExeSkip);
   //LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u\n", lCnt);
   return 0;
}

/******************************** CreateSonRoll ****************************
 *
 * Use this function to load LDR roll 2007 and before
 *
 ****************************************************************************/

int CreateSonRoll(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Use TMP file only if output needs resort
   //sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "RMP");
   lRet = sortFile(acRollFile, acTmpFile, "S(2,12,C,A) Omit(2,1,C,GT,\"9\")");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return 2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   if (!_access(acCharFile, 0))
   {
      fdChar = fopen(acCharFile, "r");
      if (fdChar == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCharFile);
         return 2;
      }
   } else
      LogMsg("*** Missing Char file %s", acCharFile);

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return 2;
   }
   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return 2;
   }
   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return 2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new R01 record
      iRet = Son_MergeRoll(acBuf, acRec, MAX_RECSIZE, CREATE_R01|CREATE_LIEN);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Son_MergeSitus(acBuf);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Son_MergeChar(acBuf);

         // Merge Sales
         if (fdSale)
            lRet = Son_MergeSale(acBuf);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTax(acBuf);

         iNewRec++;
#ifdef _DEBUG
         //iRet = replChar(acBuf, 0, ' ', iRecLen);
         //if (iRet)
         //   iRet = 0;
#endif
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdSale)
      fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Son_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Son_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

   // Skip excluding APN
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910 && iTmp != 911))
      return 1;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L_ASMT], strlen(apTokens[L_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L_FEEPARCEL], strlen(apTokens[L_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "49SON", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values
   // Land
   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lGrow = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);
   long lFixt   = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L_TAXAMT1]);
   double dTax2 = atof(apTokens[L_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));

   // TRA - standardize it to 6 digits
   lTmp = atol(apTokens[L_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L_STATUS];

#ifdef _DEBUG
   //if (!memcmp(apTokens[iApnFld], "001221006000", 9) )
   //   iTmp = 0;
#endif
   // Legal - replace known bad char
   iTmp = replChar(apTokens[L_PARCELDESCRIPTION], 221, 47);
   if (iTmp)
      LogMsg("***Fixing Legal [%.12s]: %s", pOutbuf, apTokens[L_PARCELDESCRIPTION]);

   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   iTmp = strlen(apTokens[L_USECODE]);
   if (iTmp > 0)
   {
      if (iTmp > SIZ_USE_CO)
         iTmp = SIZ_USE_CO;
      memcpy(pOutbuf+OFF_USE_CO, apTokens[L_USECODE], iTmp);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L_USECODE], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Son_MergeOwner(pOutbuf, apTokens[L_OWNER]);

   // Situs
   Son_MergeSitus(pOutbuf, apTokens[L_SITUS1], apTokens[L_SITUS2]);

   // Mailing
   Son_MergeMAdr(pOutbuf, apTokens[L_MAILADDRESS1], apTokens[L_MAILADDRESS2], apTokens[L_MAILADDRESS3], apTokens[L_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L_TAXABILITY], true, true);

   return 0;
}

/******************************** Son_MergeZone ******************************
 *
 * Merge Zoning from roll file
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Son_MergeZone(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 1024, fdZone);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdZone);
         fdZone = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Roll rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 1024, fdZone);
         lZoneSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdZone);
         fdZone = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input roll record for APN=%s", apTokens[0]);
         iRet = -1;
      }

      return iRet;
   }

   // Merge data
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   lZoneMatch++;

   // Get next record
   pRec = fgets(acRec, 1024, fdZone);

   return 0;
}

/***************************** Son_Load_LDR() *******************************
 *
 * Load LDR into 1900-byte record.
 * Input: TR601 tab-delimited record format.
 *
 ****************************************************************************/

int Son_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     iRet, lRet=0, lCnt=0;

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   iRet = sortFile(acRollFile, acTmpFile, "S(1,12,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")");
   if (iRet < 100000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Sales file
   //LogMsg("Open Sales file %s", acSalesFile);
   //fdSale = fopen(acSalesFile, "r");
   //if (fdSale == NULL)
   //{
   //   LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
   //   return -2;
   //}

   // Open Zoning file
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acBuf, acRec, myCounty.acCntyCode, myCounty.acCntyCode);
   if (acBuf[0] > ' ')
   {
      // Sort on ASMT
      sprintf(acTmpFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = sortFile(acBuf, acTmpFile, "S(#1,C,A)");

      LogMsg("Open Zoning file %s", acTmpFile);
      fdZone = fopen(acTmpFile, "r");
      if (fdZone == NULL)
      {
         LogMsg("***** Error opening Zoning file: %s\n", acTmpFile);
         return -2;
      }
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (acRec[0] >= 'A')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);    // Skip header

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      lLDRRecCount++;

      // Create new R01 record
      iRet = Son_MergeLien(acBuf, acRec);
      if (!iRet)
      {
         // Merge Char
         if (fdChar)
            lRet = MB_MergeStdChar(acBuf, NULL, NULL, NULL);
            //lRet = Son_MergeChar(acBuf);

         // Merge Sales
         //if (fdSale)
         //   lRet = Son_MergeSale(acBuf);

         // Merge Zoning
         if (fdZone)
            lRet = Son_MergeZone(acBuf);

         // Save last recording date
         //lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         //if (lRet > lLastRecDate && lRet < lToday)
         //   lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   //if (fdSale)
   //   fclose(fdSale);
   if (fdZone)
      fclose(fdZone);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Zoning matched:   %u", lZoneMatch);
   //LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   //LogMsg("Number of Sale matched:     %u\n", lSaleMatch);

   LogMsg("Number of Zoning skiped:    %u", lZoneSkip);
   //LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   //LogMsg("Number of Sale skiped:      %u\n", lSaleSkip);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return lRet;
}

/******************************* Son_convertChar() **************************
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************

int Son_convertChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmpFile[256], acTmp[256], *pRec;
   int      iRet, iTmp, iCnt=0, iNumFlds;
   MB_CHAR  myCharRec;

   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   strcpy(acTmpFile, pInfile);
   pRec = strrchr(acTmpFile, '.');
   if (pRec)
      strcpy(pRec, ".tmp");

   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   LogMsg("Converting CHAR file %s to %s", pInfile, acTmpFile);

   // Skip first record - header
   // New format 2006/06/21 doesn't have header
   // pRec = fgets(acBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);
      if (!pRec)
         break;

      iNumFlds = ParseStringNQ(pRec, cDelim, MB_CHAR_UNITSEQNO+1, apTokens);
      memset((void *)&myCharRec, ' ', sizeof(MB_CHAR));
      memcpy(myCharRec.Asmt, apTokens[MB_CHAR_ASMT], strlen(apTokens[MB_CHAR_ASMT]));

      iTmp = atoi(apTokens[MB_CHAR_POOLS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.NumPools, acTmp, iRet);
      } else if (*apTokens[MB_CHAR_POOLS] >= 'A')
      {
         iTmp = strlen(apTokens[MB_CHAR_POOLS]);
         if (iTmp > MBSIZ_CHAR_POOLS) iTmp = MBSIZ_CHAR_POOLS;
         memcpy(myCharRec.NumPools, apTokens[MB_CHAR_POOLS], iTmp);
      }

      memcpy(myCharRec.LandUseCat, apTokens[MB_CHAR_USECAT], strlen(apTokens[MB_CHAR_USECAT]));
      memcpy(myCharRec.QualityClass, apTokens[MB_CHAR_QUALITY], strlen(apTokens[MB_CHAR_QUALITY]));
      memcpy(myCharRec.YearBuilt, apTokens[MB_CHAR_YRBLT], strlen(apTokens[MB_CHAR_YRBLT]));
      memcpy(myCharRec.BuildingSize, apTokens[MB_CHAR_BLDGSQFT], strlen(apTokens[MB_CHAR_BLDGSQFT]));
      memcpy(myCharRec.SqFTGarage, apTokens[MB_CHAR_GARSQFT], strlen(apTokens[MB_CHAR_GARSQFT]));
      memcpy(myCharRec.Heating, apTokens[MB_CHAR_HEATING], strlen(apTokens[MB_CHAR_HEATING]));
      memcpy(myCharRec.Cooling, apTokens[MB_CHAR_COOLING], strlen(apTokens[MB_CHAR_COOLING]));
      memcpy(myCharRec.HeatingSource, apTokens[MB_CHAR_HEATING_SRC], strlen(apTokens[MB_CHAR_HEATING_SRC]));
      memcpy(myCharRec.CoolingSource, apTokens[MB_CHAR_COOLING_SRC], strlen(apTokens[MB_CHAR_COOLING_SRC]));

      iTmp = atoi(apTokens[MB_CHAR_BEDS]);
      if (iTmp > 0)
         memcpy(myCharRec.NumBedrooms, apTokens[MB_CHAR_BEDS], strlen(apTokens[MB_CHAR_BEDS]));

      iTmp = atoi(apTokens[MB_CHAR_FBATHS]);
      if (iTmp > 0)
         memcpy(myCharRec.NumFullBaths, apTokens[MB_CHAR_FBATHS], strlen(apTokens[MB_CHAR_FBATHS]));

      iTmp = atoi(apTokens[MB_CHAR_HBATHS]);
      if (iTmp > 0)
         memcpy(myCharRec.NumHalfBaths, apTokens[MB_CHAR_HBATHS], strlen(apTokens[MB_CHAR_HBATHS]));

      iTmp = atoi(apTokens[MB_CHAR_FP]);
      if (iTmp > 0)
         memcpy(myCharRec.NumFireplaces, apTokens[MB_CHAR_FP], strlen(apTokens[MB_CHAR_FP]));

      memcpy(myCharRec.FeeParcel, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));

      memcpy(myCharRec.DocNo, apTokens[MB_CHAR_DOCNO], strlen(apTokens[MB_CHAR_DOCNO]));

      iTmp = atoi(apTokens[MB_CHAR_BLDGSEQNO]);
      if (iTmp > 0)
         memcpy(myCharRec.BldgSeqNo, apTokens[MB_CHAR_BLDGSEQNO], strlen(apTokens[MB_CHAR_BLDGSEQNO]));

      iTmp = atoi(apTokens[MB_CHAR_UNITSEQNO]);
      if (iTmp > 0)
         memcpy(myCharRec.UnitSeqNo, apTokens[MB_CHAR_UNITSEQNO], strlen(apTokens[MB_CHAR_UNITSEQNO]));

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec, fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");

   printf("\n");
   return iRet;
}

/******************************* Son_convertChar() **************************
 *
 * Get LandUse from roll file.
 * Return pointer to LandUse if found, NULL otherwise
 *
 ****************************************************************************/

char *GetLandUse(char *pLandUse, char *pApn)
{
   static   char acRec[1024], *pRec=NULL;
   int      iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do {
         pRec = fgets(acRec, 1024, fdRoll);
      } while (acRec[1] > '9' || acRec[1] < '0');
   }

   *pLandUse = 0;
   do
   {
      if (!pRec)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         return NULL;      // EOF
      }

      // Sompare and skip double quote
      iTmp = memcmp(pApn, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
         pRec = fgets(acRec, 1024, fdRoll);
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return NULL;

#ifdef _DEBUG
   char *pTmp = pRec;
#endif

   // Replace tab char with 0
   iTmp = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad roll record for APN=%.*s (#tokens=%d)", iApnLen, pApn, iTmp);
      return NULL;
   }

   strcpy(pLandUse, apTokens[MB_ROLL_USECODE]);

   return pLandUse;
}

/**************************** Son_ConvStdChar() *****************************
 *
 * This function convert old Son_Char.csv (Pre 20160216) to STDCHAR format
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

int Son_ConvStdChar1(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmpFile[256], acTmp[256], acLandUse[32], acCode[16], *pRec;
   int      iRet, iCmp, iTmp, iCnt=0, iNumFlds, iUseMatch=0;
   bool     bKeep;
   STDCHAR  myCharRec;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A) Omit(2,1,C,GT,\"9\")");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }

   // Open char file
   LogMsg("Open Char file %s", pInfile);
   sprintf(acTmpFile, "%s\\%s\\%s_char.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   //iRet = sortFile(pInfile, acCChrFile, "S(#1,C,A) Omit(2,1,C,GT,\"9\")");
   iRet = sortFile(pInfile, acTmpFile, "S(#16,C,A) Omit(2,1,C,GT,\"9\")");
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", acTmpFile);
      return -2;
   }

   // Open output file
   if (!(fdOut = fopen(acCChrFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acCChrFile);
      return -2;
   }

   LogMsg("Converting CHAR file %s to %s", pInfile, acCChrFile);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);
      if (!pRec)
         break;

      iNumFlds = ParseStringNQ(pRec, cDelim, MB_CHAR_NUMFLOORS+1, apTokens);

      bKeep = false;
      if (!memcmp(myCharRec.Apn, apTokens[MB_CHAR_ASMT], iApnLen))
         bKeep = true;

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[MB_CHAR_ASMT], strlen(apTokens[MB_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));

      memcpy(myCharRec.LandUseCat, apTokens[MB_CHAR_USECAT], strlen(apTokens[MB_CHAR_USECAT]));

      if (*apTokens[MB_CHAR_YRBLT] > '0')
         memcpy(myCharRec.YrBlt, apTokens[MB_CHAR_YRBLT], strlen(apTokens[MB_CHAR_YRBLT]));

      // Quality Class
      if (*apTokens[MB_CHAR_QUALITY] > ' ')
      {
         iTmp = blankRem(apTokens[MB_CHAR_QUALITY]);
         memcpy(myCharRec.QualityClass, apTokens[MB_CHAR_QUALITY], iTmp);

         // Parse this into BldgClass & BldgQual
         acCode[0] = 0;
         memcpy(acTmp, myCharRec.QualityClass, iTmp);
         acTmp[iTmp] = 0;

         if (strstr(acTmp, "AVG"))
         {
            strcpy(acCode, "A");
            if (iTmp > 3)
               myCharRec.BldgClass = acTmp[0];
         } else if (isalpha(acTmp[0]))
         {
            myCharRec.BldgClass = acTmp[0];
            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
         } else if (acTmp[0] > '0' && acTmp[0] <= '9')
            iRet = Quality2Code(acTmp, acCode, NULL);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      }

      // Heating
      iTmp = atol(apTokens[MB1_CHAR_HEATING]);
      if (iTmp > 0 && iTmp < 99)
      {
         sprintf(acTmp, "%.2d", iTmp);
         iTmp = 0;
         iCmp = -1;
         while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(acTmp, asHeating[iTmp].acSrc, asHeating[iTmp].iLen)) > 0)
            iTmp++;

         if (!iCmp)
            myCharRec.Heating[0] = asHeating[iTmp].acCode[0];

         iTmp = blankRem(apTokens[MB_CHAR_HEATING_SRC]);
         memcpy(myCharRec.HeatingSrc, apTokens[MB_CHAR_HEATING_SRC], iTmp);
      }

      // Cooling
      iTmp = atol(apTokens[MB1_CHAR_COOLING]);
      if (iTmp > 0 && iTmp < 99)
      {
         sprintf(acTmp, "%.2d", iTmp);
         iTmp = 0;
         iCmp = -1;
         while (asCooling[iTmp].iLen > 0 && (iCmp=memcmp(acTmp, asCooling[iTmp].acSrc, asCooling[iTmp].iLen)) > 0)
            iTmp++;

         if (!iCmp)
            myCharRec.Cooling[0] = asCooling[iTmp].acCode[0];

         iTmp = blankRem(apTokens[MB_CHAR_COOLING_SRC]);
         memcpy(myCharRec.CoolingSrc, apTokens[MB_CHAR_COOLING_SRC], iTmp);
      }

      // Pool
      iTmp = atoi(apTokens[MB_CHAR_POOLS]);
      if (iTmp > 0 && iTmp < 99)
      {
         sprintf(acTmp, "%.2d", iTmp);
         iTmp = 0;
         iCmp = -1;
         while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(acTmp, asPool[iTmp].acSrc, asPool[iTmp].iLen)) )
            iTmp++;

         if (!iCmp)
            myCharRec.Pool[0] = asPool[iTmp].acCode[0];
      }

      iTmp = atol(apTokens[MB_CHAR_BLDGSQFT]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      iTmp = atol(apTokens[MB_CHAR_GARSQFT]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB_CHAR_BEDS]);
      if (iTmp > 0)
         memcpy(myCharRec.Beds, apTokens[MB_CHAR_BEDS], strlen(apTokens[MB_CHAR_BEDS]));

      iTmp = atoi(apTokens[MB_CHAR_FBATHS]);
      if (iTmp > 0)
      {
         memcpy(myCharRec.FBaths, apTokens[MB_CHAR_FBATHS], strlen(apTokens[MB_CHAR_FBATHS]));
         memcpy(myCharRec.Bath_4Q, apTokens[MB_CHAR_FBATHS], strlen(apTokens[MB_CHAR_FBATHS]));
      }
      iTmp = atoi(apTokens[MB_CHAR_HBATHS]);
      if (iTmp > 0)
      {
         memcpy(myCharRec.HBaths, apTokens[MB_CHAR_HBATHS], strlen(apTokens[MB_CHAR_HBATHS]));
         memcpy(myCharRec.Bath_2Q, apTokens[MB_CHAR_HBATHS], strlen(apTokens[MB_CHAR_HBATHS]));
      }

      iTmp = atoi(apTokens[MB_CHAR_FP]);
      if (iTmp > 0)
         memcpy(myCharRec.Fireplace, apTokens[MB_CHAR_FP], strlen(apTokens[MB_CHAR_FP]));

      iTmp = atoi(apTokens[MB_CHAR_BLDGSEQNO]);
      if (iTmp > 0)
         memcpy(myCharRec.BldgSeqNo, apTokens[MB_CHAR_BLDGSEQNO], strlen(apTokens[MB_CHAR_BLDGSEQNO]));

      iTmp = atoi(apTokens[MB_CHAR_UNITSEQNO]);
      if (iTmp > 0)
         memcpy(myCharRec.UnitSeqNo, apTokens[MB_CHAR_UNITSEQNO], strlen(apTokens[MB_CHAR_UNITSEQNO]));

      // Check for new file layout
      if (iNumFlds > MB_CHAR_NUMFLOORS)
      {
         iTmp = atoi(apTokens[MB_CHAR_TOTALROOMS]);
         if (iTmp > 0)
            memcpy(myCharRec.Rooms, apTokens[MB_CHAR_TOTALROOMS], strlen(apTokens[MB_CHAR_TOTALROOMS]));

         // Stories
         iTmp = atoi(apTokens[MB_CHAR_NUMFLOORS]);
         if (iTmp > 0)
         {
            iRet = sprintf(acTmp, "%d.0", iTmp);
            memcpy(myCharRec.Stories, acTmp, iRet);
         }
      }

      // Get LandUse
      if (fdRoll && !bKeep)
      {
         pRec = GetLandUse(acLandUse, myCharRec.Apn);
         if (pRec)
         {
            iUseMatch++;
            memcpy(myCharRec.LandUse, acLandUse, strlen(acLandUse));
            //LogMsg("Apn=%.12s, UseCode=%s", myCharRec.Apn, acLandUse);
         } 
      } else if (bKeep)
      {
         memcpy(myCharRec.LandUse, acLandUse, strlen(acLandUse));
         //iUseMatch++;
      }
      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec, fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);
   if (fdRoll) fclose(fdRoll);

   LogMsg("Total CHAR records output:     %d", iCnt);
   LogMsg("      records with Usecode:    %d", iUseMatch);
   return iRet;
}

/**************************** Son_ConvStdChar ********************************
 *
 * Copy from MergeMno.cpp to convert new char file. 02/19/2016
 * Other counties has similar format: MER, YOL, HUM, MAD, MNO, SON
 *
 *****************************************************************************/

int Son_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_char.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort char file %s to %s", pInfile, acTmpFile);
   // FeeParcel and Asmt are the same.  We can sort on either one.
   sprintf(acTmp, "S(1,12,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") B(302,R)");
   iRet = sortFile(pInfile, acTmpFile, acTmp);
   if (iRet < 500)
   {
      LogMsg("***** Input file is too small.");
      return 1;
   }

   if (!(fdIn = fopen(acTmpFile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Get header
   pRec = fgets(acBuf, 4096, fdIn);
   char cCharDelim = acBuf[9];

   // Start loop
   while (!feof(fdIn))
   {
      // Get next record
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec || acBuf[0] > '9')
         break;

      iFldCnt = ParseStringNQ(pRec, cCharDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < SON_CHAR_HASWELL)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[SON_CHAR_ASMT], strlen(apTokens[SON_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[SON_CHAR_FEEPARCEL], strlen(apTokens[SON_CHAR_FEEPARCEL]));

      // Format APN
      if (*apTokens[SON_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[SON_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[SON_CHAR_FEEPARCEL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[SON_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Units Count
      iTmp = atoi(apTokens[SON_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 
 
      // Stories/NumFloors
      iTmp = atoi(apTokens[SON_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 30)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Pool 
      iTmp = blankRem(apTokens[SON_CHAR_POOLSPA]);
      if (iTmp > 1)
      {
         pRec = findXlatCode(apTokens[SON_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass 
      pRec = _strupr(apTokens[SON_CHAR_QUALITYCLASS]);
      vmemcpy(myCharRec.QualityClass, apTokens[SON_CHAR_QUALITYCLASS], SIZ_CHAR_QCLS);

      if (*apTokens[SON_CHAR_QUALITYCLASS] >= 'A' && *apTokens[SON_CHAR_QUALITYCLASS] <= 'Z')
      {
         acCode[0] = ' ';
         strcpy(acTmp, apTokens[SON_CHAR_QUALITYCLASS]);
         if (!memcmp(acTmp, "AV", 2))
         {
            strcpy(acCode, "A");
         } else if (strstr(acTmp, "AV"))
         {
            strcpy(acCode, "A");
            if (strlen(acTmp) > 3)
               myCharRec.BldgClass = acTmp[0];
         } else if (strstr(acTmp, "GO") || strstr(acTmp, "GD"))
         {
            strcpy(acCode, "G");
            if (strlen(acTmp) > 3)
               myCharRec.BldgClass = acTmp[0];
         } else if (strstr(acTmp, "EX"))
         {
            strcpy(acCode, "E");
            if (strlen(acTmp) > 3)
               myCharRec.BldgClass = acTmp[0];
         } else if (strstr(acTmp, "VGD"))
         {
            strcpy(acCode, "V");
            if (strlen(acTmp) > 3)
               myCharRec.BldgClass = acTmp[0];
         } else if (strstr(acTmp, "LOW"))
         {
            strcpy(acCode, "P");
            if (strlen(acTmp) > 3)
               myCharRec.BldgClass = acTmp[0];
         } else 
         {
            myCharRec.BldgClass = acTmp[0];

            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            else if (isdigit(acTmp[2]))
               iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
            else if (strlen(acTmp) > 3 && acTmp[2] == '-' && isdigit(acTmp[3]))
               iRet = Quality2Code((char *)&acTmp[3], acCode, NULL);
         } 

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } 

      // YrBlt
      int iYrBlt = atoi(apTokens[SON_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[SON_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[SON_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atol(apTokens[SON_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[SON_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport Sqft
      int iCarport = atoi(apTokens[SON_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Patio SF
      iTmp = atoi(apTokens[SON_CHAR_PATIOSF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // Land SF
      iTmp = atoi(apTokens[SON_CHAR_LOTSQFT]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.LotSqft, acTmp, iRet);
         
         iRet = sprintf(acTmp, "%d", (long)((double)iTmp*SQFT_MF_1000));
         memcpy(myCharRec.LotAcre, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "001042001000", 9))
      //   iRet = 0;
#endif

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[SON_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[SON_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[SON_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[SON_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[SON_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[SON_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[SON_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[SON_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[SON_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // Total rooms
      iTmp = atoi(apTokens[SON_CHAR_TOTALROOMS]);
      if (iTmp > 0 && iTmp < 1000)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // FirePlace - 01, 02, 05, 06, 99
      if (*apTokens[SON_CHAR_FIREPLACE] >= '0' && *apTokens[SON_CHAR_FIREPLACE] <= '9')
      {
         pRec = findXlatCode(apTokens[SON_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      } 

      // Haswell - not avail
      blankRem(apTokens[SON_CHAR_HASWELL]);
      if (*(apTokens[SON_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Lot Sqft
      if (iFldCnt >= SON_CHAR_LOTSQFT)
      {
         iTmp = atoi(apTokens[SON_CHAR_LOTSQFT]);
         if (iTmp > 1)
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(iTmp*1000)/SQFT_PER_ACRE;
            iTmp = sprintf(acTmp, "%d", (long)(dTmp+0.5));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum, UnitSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/******************************* Son_ExtrSale() *****************************
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************

int Son_ExtrSale()
{
   SCSAL_REC SaleRec;
   char      acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   char      acTmp[256], acRec[512], *pRec;

   FILE      *fdOut;

   int       iTmp;
   double    dTmp;
   long      lCnt=0, lPrice, lTmp;

   LogMsg("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acRec, 512, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      pRec = fgets(acRec, 512, fdSale);
      if (!pRec)
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTmp = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTmp = ParseStringIQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);

      if (iTmp < MB_SALES_CONFCODE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTmp);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

      // Docnum
      lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%.5s%0.7ld", apTokens[MB_SALES_DOCNUM], lTmp);
         memcpy(SaleRec.DocNum, acTmp, SALE_SIZ_DOCNUM);

         // Doc date - input format YYYY-MM-DD
         if (dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, YYYY_MM_DD))
            memcpy(SaleRec.DocDate, acTmp, 8);
         else
            continue;
      } else
         continue;

      // Tax
      dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
      lPrice = 0;
      if (acTmp[0] > '0')
      {
         memcpy(SaleRec.StampAmt, acTmp, strlen(acTmp));

         dTmp = atof(acTmp);
         lPrice = (dTmp * SALE_FACTOR);
         if (lPrice < 100)
            sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         else
            sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         memcpy(SaleRec.SalePrice, acTmp, SIZ_SALE1_AMT);
      }

      // Doc code - accept following code only
      // 1 (Sale-Reappraise), 8 (Split/Combo w Sale-Reappraise), 12 (Direct Enrollment Transfer) 
      // 10 (mobilehome transfer)
      memcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], strlen(apTokens[MB_SALES_DOCCODE]));
      if (isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         iTmp = atoi(apTokens[MB_SALES_DOCCODE]);
         if (iTmp ==1 || iTmp == 12 || (iTmp == 8 && lPrice > 0))
            SaleRec.DocType[0] = '1';
         else if (iTmp == 10)
            memcpy(SaleRec.DocType, "75", 2);
         else 
         {
            SaleRec.NoneSale_Flg = 'Y';
            if (iTmp == 52)                        // Transfer - Default
               memcpy(SaleRec.DocType, "52", 2);
            else if (iTmp == 2 || iTmp == 4)       // Transfer
               memcpy(SaleRec.DocType, "75", 2);
            else if (iTmp == 5 || iTmp == 6)       // Partial Transfer
               memcpy(SaleRec.DocType, "75", 2);
         }
      } else if (!memcmp(apTokens[MB_SALES_DOCCODE], "GD", 2))
      {
         SaleRec.DocType[0] = '1';
      } else if (lPrice == 0)
         SaleRec.NoneSale_Flg = 'Y';

      // Transfer Type
      if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      // Group sale?
      if (*apTokens[MB_SALES_GROUPSALE] == '1')
      {
         SaleRec.NumOfPrclXfer[0] = 'M';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

      // Seller
      strcpy(acTmp, apTokens[MB_SALES_SELLER]);
      iTmp = blankRem(acTmp);
      if (iTmp > SALE_SIZ_SELLER)
         iTmp = SALE_SIZ_SELLER;
      memcpy(SaleRec.Seller1, acTmp, iTmp);

      // Buyer
      strcpy(acTmp, apTokens[MB_SALES_BUYER]);
      iTmp = blankRem(acTmp);
      if (iTmp > SALE_SIZ_BUYER)
         iTmp = SALE_SIZ_BUYER;
      memcpy(SaleRec.Name1, acTmp, iTmp);

      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   // Sort output file and dedup
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "DAT");
   LogMsg("Sorting %s to %s.", acTmpFile, acOutFile);

   // Sort on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A) F(TXT) DUPO(1,34) ");
   lCnt = sortFile(acTmpFile, acOutFile, acTmp);

   // Update cumulative sale file
   if (lCnt > 0)
   {
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");      // accummulated sale file
      LogMsg("Append %s to %s.", acCSalFile, acTmpFile);
      iTmp = appendTxtFile(acCSalFile, acTmpFile);
      if (!iTmp)
      {
         // Resort SLS file
         sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         lCnt = sortFile(acTmpFile, acOutFile, acTmp);
         if (lCnt > 0)
         {
            iTmp = remove(acCSalFile);
            if (!iTmp)
               iTmp = rename(acOutFile, acCSalFile);
            else
               LogMsg("***** Error removing %s.  Please rerun with -Xs option", acCSalFile);
         } else
            iTmp = -10;
      } else
      {
         CopyFile(acOutFile, acCSalFile, false);
         iTmp = 0;
      }
   } else
      iTmp = -11;

   LogMsg("Total Sale records exported: %d.", lCnt);
   return iTmp;
}


/****************************** Son_CreateGrGrRec ****************************
 *
 * Create SON GrGr record.
 * For multi-apn transaction, APN field can more than 1 APN
 * Grantor & Grantee fields may have more than 1 name
 *
 * Return >0 if successful, <0 if error
 *
 *****************************************************************************/

int Son_CreateGrGrRec(char *pRec, FILE *fdGrOut)
{
   char     *pTmp, acTmp[1024], acDate[16], acOutbuf[1024], *apItems[SON_GRGR_FLDCNT+2];
   int      lTax, iTmp, iRet, lSalePrice, iApnCnt;

   GRGR_DOC *pGrGrRec = (GRGR_DOC *)&acOutbuf[0];

   iTmp = ParseStringNQ1(pRec, ',', SON_GRGR_FLDCNT+2, apItems);

   if (*(apItems[SON_GRGR_DOCNUM]+4) == 'I')
      return -1;

   if (iTmp > SON_GRGR_GRANTEE)
   {
      if (*apItems[SON_GRGR_APN] < '0')
         return 0;

      // Init new record
      memset(acOutbuf, ' ', sizeof(GRGR_DOC));
      pGrGrRec->NoneSale = 'Y';
      pGrGrRec->NoneXfer = 'Y';

      iTmp = remChar(apItems[SON_GRGR_APN], '-');
      if (iTmp < iApnLen || !memcmp(apItems[SON_GRGR_APN], "000000000", 9))
      {
         LogMsg("*** WARNING: bad APN = %s", apItems[SON_GRGR_APN]);
         return 0;
      }

      // Check for multi APN
      iApnCnt = ParseString(apItems[SON_GRGR_APN], ',', MAX_FLD_TOKEN, apTokens);
      if (iApnCnt > 1)
         memcpy(pGrGrRec->APN, apTokens[0], strlen(apTokens[0]));
      else
         memcpy(pGrGrRec->APN, apItems[SON_GRGR_APN], strlen(apItems[SON_GRGR_APN]));

      // Est. sale price
      lSalePrice = atol(apItems[SON_GRGR_SALEPRICE]);
      if (lSalePrice > 0)
      {
         iTmp = sprintf(acTmp, "%*d", SIZ_GR_SALE, lSalePrice);
         memcpy(pGrGrRec->SalePrice, acTmp, iTmp);
      }

      // Doc Tax
      lTax = atol(apItems[SON_GRGR_COUNTYTAX]);
      if (lTax > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTax);
         memcpy(pGrGrRec->DocTax, acTmp, iTmp);
      }

      // City tax
      lTax = atol(apItems[SON_GRGR_CITYTAX]);
      if (lTax > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTax);
         memcpy(pGrGrRec->CityTax, acTmp, iTmp);
      }

      // DocType
      memcpy(pGrGrRec->DocTitle, apItems[SON_GRGR_DOCTYPE], strlen(apItems[SON_GRGR_DOCTYPE]));
      if (lSalePrice > 0 && !strcmp(apItems[SON_GRGR_DOCTYPE], "DEED"))
      {
         pGrGrRec->DocType[0] = '1';
         pGrGrRec->NoneSale = 'N';
         pGrGrRec->NoneXfer = 'N';
      } else
      {
         strcpy(acTmp, apItems[SON_GRGR_DOCTYPE]);
         iRet = XrefCodeIndex((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
         if (iRet >= 0)
         {
            pGrGrRec->NoneSale = asDeed[iRet].acFlags[0];
            pGrGrRec->NoneXfer = asDeed[iRet].acOther[0];
            iRet = sprintf(acTmp, "%d", asDeed[iRet].iIdxNum);
            memcpy(pGrGrRec->DocType, acTmp, iRet);
         } else if (acTmp[0] > ' ')
         {
            LogMsg("*** SON GRGR - Unknown DocTitle: %s [%s]", acTmp, apItems[SON_GRGR_APN]);
            memcpy(pGrGrRec->DocType, "19", 2);
         }
      }

      // DocDate, DocNum
      if (*apItems[SON_GRGR_DOCNUM] > ' ')
      {
         pTmp = dateConversion(apItems[SON_GRGR_RECDATE], acDate, MM_DD_YYYY_1);
         if (pTmp)
         {
            memcpy(pGrGrRec->DocDate, pTmp, 8);
            iRet = atol(pTmp);
            if (iRet > lLastRecDate)
               lLastRecDate = iRet;
         } else
            LogMsg("*** Invalid RecDate: %s - %s", apItems[SON_GRGR_RECDATE], apTokens[0]);

         iRet = sprintf(acTmp, "%.4sR%.6s", apItems[SON_GRGR_DOCNUM], apItems[SON_GRGR_DOCNUM]+4);
         memcpy(pGrGrRec->DocNum, acTmp, iRet);    
      }

      // Grantor
      pTmp = apItems[SON_GRGR_GRANTEE];
      iRet = ParseString(apItems[SON_GRGR_GRANTOR], ',', SON_GRGR_FLDCNT, apItems);
      for (iTmp = 0; iTmp < iRet && iTmp < 2; iTmp++)
         memcpy(pGrGrRec->Grantor[iTmp], apItems[iTmp], strlen(apItems[iTmp]));

      // Grantee
      iRet = ParseString(pTmp, ',', SON_GRGR_FLDCNT, apItems);
      for (iTmp = 0; iTmp < iRet && iTmp < 2; iTmp++)
         memcpy(pGrGrRec->Grantee[iTmp], apItems[iTmp], strlen(apItems[iTmp]));
      iTmp = sprintf(acTmp, "%d", iRet);
      memcpy(pGrGrRec->NameCnt, acTmp, iTmp);

      if (iApnCnt > 1)
         pGrGrRec->MultiApn = 'Y';
      pGrGrRec->CRLF[0] = '\n';
      pGrGrRec->CRLF[1] = '\0';
      fputs(acOutbuf, fdGrOut);
      for (iTmp = 1; iTmp < iApnCnt; iTmp++)
      {
         memset(pGrGrRec->APN, ' ', sizeof(pGrGrRec->APN));
         memcpy(pGrGrRec->APN, apTokens[iTmp], strlen(apTokens[iTmp]));
         fputs(acOutbuf, fdGrOut);
      }

      iRet = iApnCnt;
   } else
      iRet = -1;

   return iRet;
}

/* 
*/

int Son_LoadGrGrCsv(char *pInfile, FILE *fdOut)
{
   char     *pTmp, acTmp[256], acRec[4096], acOutbuf[1024];
   int      iRet, iCnt, lCnt;

   FILE     *fdIn;
   GRGR_DOC  *pGrGrRec = (GRGR_DOC *)&acOutbuf[0];

   LogMsg("Process GrGr file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("*** Error opening file: %s", pInfile);
      return 0;
   }

   // Remove header
   pTmp = fgets(acRec, 4096, fdIn);

   // Update GrGr file date
   iRet = getFileDate(acTmp);
   if (iRet > lLastGrGrDate)
      lLastGrGrDate = iRet;

   // Initialize pointers
   iCnt = lCnt = 0;
   while (fdIn && !feof(fdIn))
   {
      pTmp = fgets(acRec, 4096, fdIn);
      if (!pTmp) break;

      iRet = Son_CreateGrGrRec(acRec, fdOut);
      if (iRet > 0)
         iCnt += iRet;

      lCnt++;
      if (!(lCnt % 1000))
         printf("\r%d", lCnt);
   }

   fclose(fdIn);
   LogMsg("Total processed records  : %u", lCnt);
   LogMsg("         output records  : %u", iCnt);

   return lCnt;
}

/********************************* Son_LoadGrGr *****************************
 *
 * If successful, return 0.  Otherwise error.
 *
 ****************************************************************************/

int Son_LoadGrGr(LPCSTR pCnty) 
{
   char     *pTmp, acTmp[256], acInFile[_MAX_PATH];
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   struct   _finddata_t  sFileInfo;

   FILE     *fdOut;

   int      iCnt, iRet;
   long     lCnt, lHandle;
   LONGLONG    lTmp;

   // Get raw file name
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);

   // Prepare backup folder
   sprintf(acGrGrBak, acGrGrBakTmpl, pCnty, acToday);
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      LogMsg("*** No new file avail for processing: %s", acGrGrIn);
      return -1;
   }

   // Create Output file - Son_GrGr.dat
   sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "Dat");

   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      _findclose(lHandle);
      return -2;
   }

   // Create backup folder
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   lLastRecDate = iCnt = lCnt = 0;
   while (!iRet)
   {
      sprintf(acInFile, "%s\\%s", acGrGrIn, sFileInfo.name);
      lTmp = getFileDate(acInFile);
      if (lTmp > lLastGrGrDate)
         lLastGrGrDate = (long)lTmp;

      lTmp = getFileSize(acInFile);
      if (lTmp > 50)
      {
         // Parse input file
         iRet = Son_LoadGrGrCsv(acInFile, fdOut);
         if (iRet < 0)
            LogMsg("*** Skip %s", acInFile);
         else
            lCnt += iRet;

         // Move input file to backup folder
         sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
         rename(acInFile, acTmp);
      } else
      {
         LogMsg("*** Skip empty file: %s", acInFile);
      }

      iCnt++;
      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }


   // Close handle
   _findclose(lHandle);

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("-------------------------");
   LogMsg("Total files processed   : %u", iCnt);
   LogMsg("Total records processed : %u\n", lCnt);

   // Sort output
   if (lCnt > 0)
   {
      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, DocNum asc, RecDate asc
      sprintf(acTmp,"S(17,12,C,A, 1,10,C,A) F(TXT) DUPO(B2048, 1, 130) ");
      strcpy(acGrGrIn, acGrGrOut);
      sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "Srt");

      // Sort Son_GrGr.dat to Son_GrGr.srt
      lCnt = sortFile(acGrGrIn, acGrGrOut, acTmp);

      // Update cumulative sale file
      char acCGrGrFile[_MAX_PATH];
      sprintf(acCGrGrFile, acGrGrTmpl, pCnty, pCnty, "sls"); // Son_GrGr.sls
      if (lCnt > 0)
      {
         iRet = 0;
         if (!_access(acCGrGrFile, 0))
         {
            // Append Son_GrGr.srt to Son_GrGr.sls for backup
            sprintf(acInFile, "%s+%s", acGrGrOut, acCGrGrFile);

            // Sort by APN, RecDate, DocNum, Sale price
            sprintf(acTmp, "S(%d,12,C,A,%d,8,C,A,%d,12,C,A,%d,10,C,D) F(TXT) OMIT(%d,1,C,EQ,\" \") DUPO(B2048,1,130) ", 
               OFF_GD_APN, OFF_GD_DOCDATE, OFF_GD_DOCNUM, OFF_GD_SALE, OFF_GD_APN);

            // Sort on APN asc, DocDate asc, DocNum asc
            sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "dat"); // Son_GrGr.dat
            lCnt = sortFile(acInFile, acGrGrOut, acTmp);
            if (lCnt > 0)
            {
               if (!_access(acCGrGrFile, 0))
               {
                  sprintf(acTmp, acGrGrTmpl, pCnty, pCnty, "sav"); // Son_GrGr.sav
                  if (!_access(acTmp, 0))
                     DeleteFile(acTmp);

                  LogMsg("Rename old cum GRGR file: %s to %s", acCGrGrFile, acTmp);
                  rename(acCGrGrFile, acTmp);
               }

               // Rename Dat to SLS file
               LogMsg("Rename cum GRGR file: %s --> %s", acGrGrOut, acCGrGrFile);
               iRet = rename(acGrGrOut, acCGrGrFile);
            } else
            {
               iRet = -3;
               LogMsg("***** Error sorting output file in Son_LoadGrGr()");
            }
         } else
         {
            // Rename Srt to SLS file
            LogMsg("Rename cum GRGR file: %s --> %s", acGrGrOut, acCGrGrFile);
            iRet = rename(acGrGrOut, acCGrGrFile);
         }
      } 
   } else
      iRet = -1;

   LogMsg("Total output records after dedup: %u", lCnt);
   LogMsg("             Last recording date: %d.", lLastRecDate);

   return iRet;
}

/********************************* Son_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Son_MergeLien4(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L4_DOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L4_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L4_ASMT], strlen(apTokens[L4_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L4_FEEPARCEL], strlen(apTokens[L4_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L4_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L4_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "49SON", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L4_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L4_TRA], strlen(apTokens[L4_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L4_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L4_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L4_FIXTURESVALUE]);
   long lGrow  = atoi(apTokens[L4_GROWING]);
   long lPers  = atoi(apTokens[L4_PPVALUE]);
   long lPP_MH = atoi(apTokens[L4_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L4_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L4_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L4_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L4_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L4_EXEMPTIONCODE1], strlen(apTokens[L4_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L4_EXEMPTIONCODE2], strlen(apTokens[L4_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L4_EXEMPTIONCODE3], strlen(apTokens[L4_EXEMPTIONCODE3]));

   // Legal
#ifdef _DEBUG
   //if (!memcmp(apTokens[iApnFld], "001221006000", 9) )
   //   iTmp = 0;
#endif
   // Legal - replace known bad char
   iTmp = replChar(apTokens[L4_PARCELDESCRIPTION], 221, 47);
   if (iTmp)
      LogMsg("***Fixing Legal [%.12s]: %s", pOutbuf, apTokens[L4_PARCELDESCRIPTION]);

   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L4_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   if (*apTokens[L4_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L4_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L4_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L4_ACRES]);
   lTmp = atol(apTokens[L4_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   //if (*apTokens[L4_ISAGPRESERVE] == '1')
   //   *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Son_MergeOwner(pOutbuf, apTokens[L4_OWNER]);

   // Situs
   //Son_MergeSitus(pOutbuf, apTokens[L4_SITUS1], apTokens[L4_SITUS2]);

   // Mailing
   Son_MergeMAdr(pOutbuf, apTokens[L4_MAILADDRESS1], apTokens[L4_MAILADDRESS2], apTokens[L4_MAILADDRESS3], apTokens[L4_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   //iTmp = updateTaxCode(pOutbuf, apTokens[L4_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016

   //// Garage size
   //dTmp = atof(apTokens[L4_GARAGESIZE]);
   //if (dTmp > 0.0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_GAR_SQFT, dTmp);
   //   memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   //   *(pOutbuf+OFF_PARK_TYPE) = '2';                 // GARAGE/CARPORT
   //}

   //// Number of parking spaces
   //if (*apTokens[L4_GARAGE] == '0' || *apTokens[L4_GARAGE] == 'N')
   //   *(pOutbuf+OFF_PARK_TYPE) = 'H';                 // None
   //else if (*apTokens[L4_GARAGE] > '0' && *apTokens[L4_GARAGE] <= '9')
   //{
   //   iTmp = atol(apTokens[L4_GARAGE]);
   //   sprintf(acTmp, "%d", iTmp);
   //   vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
   //   if (dTmp > 100)
   //      *(pOutbuf+OFF_PARK_TYPE) = 'Z';              // Garage
   //   else
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
   //} else
   //{
   //   if (*(apTokens[L4_GARAGE]) == 'C')
   //      *(pOutbuf+OFF_PARK_TYPE) = 'C';              // Carport
   //   else if (*(apTokens[L4_GARAGE]) == 'A')
   //   {
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L4_GARAGE], "DOU", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '2';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L4_GARAGE], "TRI", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '3';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L4_GARAGE], "SIN", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '1';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L4_GARAGE], "GC", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT
   //   else if (!_memicmp(apTokens[L4_GARAGE], "GS", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT ?
   //   else if (!_memicmp(apTokens[L4_GARAGE], "DE", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = 'D';              // Detached
   //   else if (*(apTokens[L4_GARAGE]) == 'S')
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Space ?
   //}

   //// YearBlt
   //lTmp = atol(apTokens[L4_YEARBUILT]);
   //if (lTmp > 1800 && lTmp < lToyear)
   //{
   //   iTmp = sprintf(acTmp, "%d", lTmp);
   //   memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   //}

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // Acres
   dTmp = atof(apTokens[L4_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Lot size
   lTmp = atol(apTokens[L4_LANDSIZE]);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      if (!dTmp)
      {
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(lTmp*SQFT_MF_1000));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   } else if (dTmp > 0.0)
   {
      //lTmp = (dTmp+0.0005)*SQFT_PER_ACRE;
      lTmp = (long)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   //// Total rooms
   //iTmp = atol(apTokens[L4_TOTALROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   //}

   //// Stories
   //iTmp = atol(apTokens[L4_STORIES]);
   //if (iTmp > 0 && iTmp < 100)
   //{
   //   sprintf(acTmp, "%d.0", iTmp);
   //   vmemcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   //}

   //// Units
   //iTmp = atol(apTokens[L4_UNITS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%d", iTmp);
   //   vmemcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   //}

   //// Beds
   //iTmp = atol(apTokens[L4_BEDROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BEDS, iTmp);
   //   memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   //}

   //// Baths
   //iTmp = atol(apTokens[L4_BATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_F, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   //}

   //// HBaths
   //iTmp = atol(apTokens[L4_HALFBATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_H, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   //}

   //// Heating
   //int iCmp;
   //if (*apTokens[L4_HEATING] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L4_HEATING], asHeating_LDR[iTmp].acSrc, asHeating_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_HEAT) = asHeating_LDR[iTmp].acCode[0];
   //}

   //// Cooling
   //if (*apTokens[L4_AC] == 'C')
   //   *(pOutbuf+OFF_AIR_COND) = 'C';
   //else if (*apTokens[L4_AC] > ' ')
   //   LogMsg("*** Unknown A/C: %s", apTokens[L4_AC]);

   //// Pool/Spa
   //if (*apTokens[L4_POOLSPA] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L4_POOLSPA], asPool_LDR[iTmp].acSrc, asPool_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_POOL) = asPool_LDR[iTmp].acCode[0];
   //}

   //// Fire place
   //if (*apTokens[L4_FIREPLACE] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   pTmp = _strupr(apTokens[L4_FIREPLACE]);
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L4_FIREPLACE], asFP_LDR[iTmp].acSrc, asFP_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_FIRE_PL) = asPool[iTmp].acCode[0];
   //}

   //// Quality Class
   //acTmp1[0] = 0;
   //if (*apTokens[L4_QUALITYCLASS] > '0' || strlen(apTokens[L4_QUALITYCLASS]) > 1)
   //{
   //   strcpy(acTmp, apTokens[L4_QUALITYCLASS]);
   //   remCharEx(acTmp, " ,'?");
   //   pTmp = _strupr(acTmp);

   //   //memcpy(myCharRec.QualityClass, pTmp, strlen(pTmp));
   //   if (acTmp[0] == 'A' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'A';
   //   else if (acTmp[0] == 'F' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'F';
   //   else if (!memcmp(pTmp, "POOR", 4) || !memcmp(pTmp, "LOW", 3))
   //      acTmp1[0] = 'P';
   //   else if (*pTmp == 'G')
   //      acTmp1[0] = 'G';
   //   else if (isalpha(*pTmp))
   //   {
   //      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
   //      if (isdigit(acTmp[1]))
   //         iRet = Quality2Code(&acTmp[1], acTmp1, NULL);
   //      else if (isdigit(acTmp[2]))
   //         iRet = Quality2Code(&acTmp[2], acTmp1, NULL);
   //      else if (isalpha(acTmp[1]))
   //      {
   //         switch (acTmp[1])
   //         {
   //            case 'L':
   //            case 'P':
   //               acTmp1[0] = 'P';
   //               break;
   //            case 'A':
   //               acTmp1[0] = 'A';
   //               break;
   //            case 'F':
   //               acTmp1[0] = 'F';
   //               break;
   //            case 'G':
   //               acTmp1[0] = 'G';
   //               break;
   //         }
   //      }
   //   } else if (isdigit(*pTmp))
   //   {
   //      iTmp = atol(pTmp);
   //      if (iTmp < 100)
   //         iRet = Quality2Code(pTmp, acTmp1, NULL);
   //   }

   //   if (acTmp1[0] > '0')
   //      *(pOutbuf+OFF_BLDG_QUAL) = acTmp1[0];
   //}

   return 0;
}
int Son_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input LDR record for APN=%s (#tokens=%d)", apTokens[L3_ASMT], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "49SON", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: 
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lFixtRP+lGrow+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      
      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

#ifdef _DEBUG
   //if (!memcmp(apTokens[iApnFld], "001221006000", 9) )
   //   iTmp = 0;
#endif

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SON_Exemption);

   // Legal - replace known bad char
   iTmp = replChar(apTokens[L3_PARCELDESCRIPTION], 221, 47);
   if (iTmp)
      LogMsg("***Fixing Legal [%.12s]: %s", pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Son_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   //Son_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Son_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // DocNum
   if (*apTokens[L3_CURRENTDOCNUM] > '0')
   {
      vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);     // 2002-03-18 00:00:00.000
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   }

   // Ag Preserve
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   return 0;
}

/******************************** Son_Load_LDR4 *****************************
 *
 * LDR 2016, use Son_MergeLien4()
 * LDR 2017, use Son_MergeLien3()
 * LDR 2019, same as 2017 but add Agpreserve
 *
 ****************************************************************************/

int Son_Load_LDR4(bool bUseValueFile, int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
      if (!iRet)
         return -1;
   }

   // Open LDR file
   LogMsg("Open LDR file %s", acRollFile);
   fdLDR = fopen(acRollFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Zoning file
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acBuf, acRec, myCounty.acCntyCode, myCounty.acCntyCode);
   if (acBuf[0] > ' ')
   {
      // Sort on ASMT
      sprintf(acTmpFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = sortFile(acBuf, acTmpFile, "S(#1,C,A)");

      LogMsg("Open Zoning file %s", acTmpFile);
      fdZone = fopen(acTmpFile, "r");
      if (fdZone == NULL)
      {
         LogMsg("***** Error opening Zoning file: %s\n", acTmpFile);
         return -2;
      }
   }

   // Open Value file
   fdLienExt = NULL;
   if (bUseValueFile)
   {
      sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmpFile, 0))
      {
         LogMsg("Open Lien file %s", acTmpFile);
         fdLienExt = fopen(acTmpFile, "r");
         if (fdLienExt == NULL)
         {
            LogMsg("***** Error opening lien file: %s\n", acTmpFile);
            return -7;
         }
      }
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
      if (cDelim == '|')
         strcat(acRec, "DEL(124) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      // Create new R01 record
      if (lLienYear == 2016)
         iRet = Son_MergeLien4(acBuf, acRec);
      else
         iRet = Son_MergeLien3(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         if (fdLienExt)
            lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Situs
         if (fdSitus)
            lRet = Son_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = MB_MergeStdChar(acBuf, NULL, NULL, NULL);

         // Merge Zoning
         if (fdZone)
            lRet = Son_MergeZone(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdZone)
      fclose(fdZone);
   if (fdLDR)
      fclose(fdLDR);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Zoning matched:   %u", lZoneMatch);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Zoning skiped:    %u", lZoneSkip);
   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************* Son_MakeDocLink *******************************
 *
 * Format DocLink
 *
 ******************************************************************************/

void Son_MakeDocLink(char *pDocLink, char *pDoc, char *pDate)
{
   int   iDocNum;
   char  acTmp[256], acDocName[256], acDocNum[32];

#ifdef _DEBUG
   //if (!memcmp(pDoc, "2021R013922", 11))
   //   iDocNum = 0;
#endif

   *pDocLink = 0;
   if (*pDoc > ' ' && *pDate > ' ' && *(pDoc+4) == 'R')
   {
      iDocNum = atoin((char *)pDoc+5, 6);       // DocNum can be 6 or 7 digits
      sprintf(acDocNum, "%.6d", iDocNum);       // Reformat DocNum
      sprintf(acDocName, "%.4s\\%.3s\\%.4s%s", pDate, acDocNum, pDate, acDocNum);
      sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocName);

      try
      {
         if (!_access(acTmp, 0))
            strcpy(pDocLink, &acDocName[0]);
         else
            *pDocLink = 0;
      } catch (...)
      {
         LogMsg("*** Bad file name: %s", acTmp);
      }
   }
}

/********************************* Son_ExtrVal ******************************
 *
 * Extract values from tax roll Son_Tax.csv
 * SON only has FixtureRP.
 *
 ****************************************************************************/

int Son_CreateValueRec(char *pOutbuf)
{
   char     acTmp[256];
   LONGLONG lTmp;
   int      iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, apTokens[T3_ASMT], strlen(apTokens[T3_ASMT]));

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[T3_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atol(apTokens[T3_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // PP value
   long lPers = atol(apTokens[T3_CURRENTPERSONALPROPVALUE]);
   long lPP_MH = atol(apTokens[T3_CURRENTPERSONALPROPMHVALUE]);

   // Fixture
   long lFixtRP = atol(apTokens[T3_CURRENTFIXEDIMPRVALUE]);

   // Growing
   long lGrow = atol(apTokens[T3_CURRENTGROWINGIMPRVALUE]);

   // Total other
   long lOthers = lPers + lFixtRP + lGrow + lPP_MH;
   if (lOthers > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lOthers);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%u", lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixtRP > 0)
      {
         iTmp = sprintf(acTmp, "%u", lFixtRP);
         memcpy(pLienRec->extra.MB.FixtureRP, acTmp, iTmp);
      }
      if (lGrow > 0)
      {
         iTmp = sprintf(acTmp, "%u", lGrow);
         memcpy(pLienRec->extra.MB.GrowImpr, acTmp, iTmp);
      }
      if (lPP_MH > 0)
      {
         iTmp = sprintf(acTmp, "%u", lPP_MH);
         memcpy(pLienRec->extra.MB.PP_MobileHome, acTmp, iTmp);
      }
   }

   // Gross
   lTmp = lOthers + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exe 
   pLienRec->acHO[0] = '2';            // N
   lTmp = atol(apTokens[T3_EXEMPTIONAMT1])+atol(apTokens[T3_EXEMPTIONAMT2]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      if (lTmp == 7000)
         pLienRec->acHO[0] = '1';      // Y
   } 

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Son_ExtrVal()
{
   char      *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   int       iRet;
   long      lOut=0, lCnt=0;
   FILE      *fdOut, *fdIn;

   LogMsg0("Extracting final values from equalized file.");

   // Open input file
   GetIniString(myCounty.acCntyCode, "TaxFile", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg("Open tax roll file %s", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax roll file: %s\n", acTmpFile);
      return -2;
   }  

   // Open out file
   sprintf(acTmpFile, acValueTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open file %s for output", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Drop header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Start loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp > '9')
         break;

      // Parse input record
      iTokens = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < MB_TAX_NETVAL)
      {
         LogMsg("*** Bad record: %s", acRec);
         continue;
      }

      // Take CS only
      iRet = atoin(apTokens[T3_ASMT], 3);
      if (iRet >= 800 && iRet < 900)
         continue;

      iRet = atol(apTokens[T3_TAXYEAR]);
      if (iRet == lTaxYear)
      {
         // Create new base record
         iRet = Son_CreateValueRec(acBuf);
         if (!iRet)
         {
            fputs(acBuf, fdOut);
            lOut++;
         } else
         {
            LogMsg("---> Drop record %d ", lCnt); 
         }
      } else
         LogMsg("---> Ignore prior year %d [%s] ", iRet, apTokens[T3_ASMT]); 

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records output:    %u", lOut);

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return 0;
}

/***************************** Son_CreateSCSale ******************************
 *
 * Extract sale data from ???_Sales.csv and output to ???_SALE.SLS
 * Drop all records without DocNum or DocDate
 * Drop all internal records (not 'R')
 *
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Son_CreateSCSale(int iDateFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTax;
   long     lCnt=0, lPrice, lTmp;

   LogMsg0("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   do {
      lTmp = ftell(fdSale);
      pTmp = fgets(acRec, 1024, fdSale);
   } while (pTmp && !isdigit(*(pTmp+1)));    // Skip quote on 1st char
   
   if (fseek(fdSale, lTmp, SEEK_SET))
   {
      LogMsg("***** Error setting current position in sale file using fseek()");
      fclose(fdSale);
      return -2;
   }

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (strlen(acRec) < 14)
         break;

      if (iSkipQuote)
         quoteRem(acRec);

      // Replace null char with space
      replNull(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;
      if (*(apTokens[MB_SALES_DOCNUM]+4) != 'R')
         continue;

      // Ignore DocNum start with 1900
      if (!memcmp(apTokens[MB_SALES_DOCNUM], "1900R", 5))
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "910000694000", 9))
      //   iTmp = 0;
#endif
      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
      {
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }

      // Docnum - 02/09/2023
      if (*(apTokens[MB_SALES_DOCNUM]+5) == 'R' || *(apTokens[MB_SALES_DOCNUM]+5) == 'O')
         lTmp = atoin(apTokens[MB_SALES_DOCNUM]+6, 6);
      else
         lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%.5s%.6d", apTokens[MB_SALES_DOCNUM], lTmp);
         memcpy(SaleRec.DocNum, acTmp, iTmp);
      } 

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1' || *apTokens[MB_SALES_GROUPSALE] == 'T')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001130011000", 9))
      //   iTmp = 0;
#endif
      // Confirmed sale price
      dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
      if (acTmp[0] > '0')
      {
         lPrice = atol(acTmp);
         if (lPrice < 1000)
         {
            // This doesn't look normal, but keep it for reference only
            iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            lPrice = 0;
         } else
            iTmp = sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      } 

      // Do not use confirmed sale price
      if (!bUseConfSalePrice)
         lPrice = 0;

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001253017000", 9))
      //   iTmp = 0;
#endif
      // Tax
      dTax = 0;
      dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
      if (acTmp[0] > '0')
      {
         dTax = atof(acTmp);

         // Save DocTax
         iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTax);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);

         // Calculate sale price
         lTmp = (long)(dTax * SALE_FACTOR);
         iTmp = ((int)dTax/100)*100;

         // Check for bad DocTax
         if (dTax > 100000)
         {
            if (iTmp == (int)dTax)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTax);
               lPrice = iTmp;
            } else if (lPrice > 100000 && lPrice == (lPrice/100)*100)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTax);
            } else
            {
               LogMsg("??? (1) Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f, CP=%d.  Need investigation.  Ignore price.", SaleRec.Apn, SaleRec.DocNum, dTax, lPrice);
               lPrice = 0;
            }
         } else if (lPrice > 0 && (long)dTax >= lPrice+5000)
         {
            // Even though sale tax is questionable, we still rely on it if it is reasonable
            if (iTmp == (int)dTax)
            {
               LogMsg("*** (2) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTax);
               lPrice = iTmp;
            } else
            {
               if (lPrice == (lPrice/100)*100)
                  LogMsg("*** (2) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTax);
               else
               {
                  LogMsg("??? (2) Questionable Sale Tax Amt for %.12s: DocNum=%.12s, Price=%d, \ttax=%.2f).  Need investigation.", SaleRec.Apn, SaleRec.DocNum, lPrice, dTax);
                  lPrice = lTmp;
               }
            }
         } else if (iTmp == (int)dTax && iTmp > 10000)
         {
            if (lTmp != (lTmp/100)*100)
            {
               LogMsg("*** (3) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTax);
               lPrice = iTmp;
            } else
               lPrice = lTmp;
         } else if (lTmp == (lTmp/100)*100)
            lPrice = lTmp;
         else if (lTmp > 1000 && lPrice == 0)
            lPrice = lTmp;

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Ignore sale price.", 
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTax, apTokens[MB_SALES_DOCCODE]);
               lPrice = 0;
            }
         }  
      } 

      // Ignore sale price if less than 1000
      if (lPrice >= 10000)
         sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
      else if (lPrice >= 1000)
         sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
      else
         memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
      memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001253017000", 9))
      //   iTmp = 0;
#endif

      // Doc code - accept following code only
      int iDocCode = 0;
      if (isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
         if (iTmp >= 0)
         {
            memcpy(SaleRec.DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
            SaleRec.NoneSale_Flg = pDocTbl[iTmp].flag;
         } else if (bDebug)
            LogMsg("*** Unknown DocCode: %s", apTokens[MB_SALES_DOCCODE]);
      } else if (dTax > 0.0 && SaleRec.DocType[0] == ' ')
      {
         SaleRec.DocType[0] = '1';
         SaleRec.NoneSale_Flg = 'N';
      } else
         SaleRec.NoneSale_Flg = 'Y';

      // Save original DocCode
      vmemcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);

      // Transfer Type
      if (*apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         pTmp = _strupr(apTokens[MB_SALES_XFERTYPE]);
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(pTmp, asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            } else if (*pTmp == 'F' || !memcmp(pTmp, "GD", 2))
            {
               SaleRec.SaleCode[0] = 'F';
               break;
            }
            iTmp++;
         }

         if (SaleRec.SaleCode[0] <= ' ')
            LogMsg("*** Unknown XFERTYPE: %s DocNum=%s [%s] ", pTmp, apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_ASMT]);
      }

      if (SaleRec.DocDate[0] > ' ' && SaleRec.DocNum[0] > ' ')
      {
         // Seller
         strcpy(acTmp, apTokens[MB_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Seller1, acTmp, SALE_SIZ_SELLER, iTmp);

         // Buyer
         strcpy(acTmp, apTokens[MB_SALES_BUYER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Name1, acTmp, SALE_SIZ_BUYER, iTmp);

         SaleRec.CRLF[0] = 10;
         SaleRec.CRLF[1] = 0;
         fputs((char *)&SaleRec,fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) OMIT(20,1,C,EQ,\" \",OR,31,1,C,EQ,\" \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -2;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            // Save old cumsale file
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);
            iTmp = rename(acCSalFile, acTmpFile);
   
            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -2;
      } else
         iTmp = rename(acOutFile, acCSalFile);

      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acCSalFile, acTmpFile);
      }
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp)
   {
      iErrorCnt++;
      if (iTmp == -2)
         LogMsg("***** Error sorting output file");
      else
         LogMsg("***** Error renaming to %s", acCSalFile);
   }

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                          output: %d.", lTmp);
   LogMsg("         Latetest recording date: %d.", lLastRecDate);
   return iTmp;
}

/*********************************** loadSon ********************************
 *
 * Options:
 *    -CSON -L -Xl -Mg -Xs|-Ms -Xv -Xa(load lien)
 *    -CSON -U -G|-Mg -Xsi|-Ms -Xa (load update)
 *
 ****************************************************************************/

int loadSon(int iSkip)
{
   int   iRet=0;
   char  acTmp[_MAX_PATH];

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;
   iApnLen = myCounty.iApnLen;

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      TC_SetDateFmt(MM_DD_YYYY_1);
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Extract old LDR 2007 and before
   //iRet = MB_ExtrProp8(myCounty.acCntyCode, acRollFile, ',', MB_ROLL_TAXABILITY);
   // 02/10/2023
   //iRet = FixCumSale(acCSalFile, SALE_FLD_DOCNUM+CNTY_SON);

   // Extract final value
   if (iLoadFlag & EXTR_FVAL)                      // -Xf
      iRet = Son_ExtrVal();

   // Load Grgr
   if (iLoadFlag & LOAD_GRGR)                      // -G
   {
      // Load special table for GrGr
      GetIniString(myCounty.acCntyCode, "Son_GrGr_Xref", "", acTmp, _MAX_PATH, acIniFile);
      iNumDeeds = LoadXrefTable(acTmp, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

      LogMsg("Load %s GrGr file", myCounty.acCntyCode);
      iRet = Son_LoadGrGr(myCounty.acCntyCode);
      if (!iRet)
         iLoadFlag |= MERG_GRGR;                   // Trigger merge grgr
      else if (iRet == -1)
      {
         bGrGrAvail = false;
         if (bSendMail &&  !(iLoadFlag & (LOAD_LIEN|LOAD_UPDT)))
            return iRet;
      }
   }

   // Extract lien file - LDR2016
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acValueFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acValueFile, 0);      // 2016 
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode, NULL, LAST_CHAR_N);
   }

   // Fix DocType if needed
   if (iLoadFlag & FIX_CSTYP)
   {
      iRet = FixDocType(acCSalFile, (IDX_TBL5 *)&SON_DocCode[0], true /* remove rec w/o DocNum */);
      if (iRet)
         return iRet;
   }

   // Extract Sale file from Son_Sales.txt to Son_Sale.dat and append to Son_Sale.sls
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // 02/09/2023 
      if (iSaleDateFmt > 0)
         iRet = Son_CreateSCSale(iSaleDateFmt, true, &SON_DocCode[0]);
      else
         iRet = Son_CreateSCSale(MM_DD_YYYY_1, true, &SON_DocCode[0]);
      // 02/10/2021
      //if (iSaleDateFmt > 0)
      //   iRet = MB_CreateSCSale(iSaleDateFmt, 4, 3, true, &SON_DocCode[0]);
      //else
      //   iRet = MB_CreateSCSale(MM_DD_YYYY_1, 4, 3, true, &SON_DocCode[0]);             
      //iRet = MB_CreateSCSale(MM_DD_YYYY_1, 4, 3, false, (IDX_TBL5 *)&SON_DocCode[0]);   // 02/19/2016
      //iRet = MB_CreateSCSale(YYYY_MM_DD, 4, 3, false, (IDX_TBL5 *)&SON_DocCode[0]);
      //iRet = MB_CreateSCSale(YYYY_MM_DD, 4, 3);
      //iRet = Son_ExtrSale();
      if (iRet)
         return iRet;

      iLoadFlag |= MERG_CSAL;
   }

   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      iRet = Son_ConvStdChar(acCharFile);
      if (iRet <= 0)
      {
         LogMsg("***** Error converting Char file %s", acCharFile);
         return -1;
      }
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      if (lLienYear > 2007)
      {
         //iRet = Son_Load_LDR(iSkip);
         if (acValueFile[0] > 'A')
            iRet = Son_Load_LDR4(true, iSkip); 
         else
            iRet = Son_Load_LDR4(false, iSkip); 
      } else
         iRet = CreateSonRoll(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Son_Load_Roll(iSkip);
   }

   // Only use -Mo if you know what you are doing
   if (!iRet && bMergeOthers)                      // -Mo
   {
      char acTmpFile[_MAX_PATH];

      // Merge other values
      sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmpFile, 0))
         iRet = PQ_MergeOthers(myCounty.acCntyCode, acTmpFile, GRP_MB, iSkip);
      else
         LogMsg("***** Lien extract is missing: %s.  Please locate the file and retry", acTmpFile);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Son_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   // Merge GrGr
   if (!iRet && (iLoadFlag & MERG_GRGR))           // -Mg
   {
      iRet = MergeGrGrDoc(myCounty.acCntyCode, 2);
      if (iRet)
         iLoadFlag = 0;
      else
         iLoadFlag |= LOAD_UPDT;
   }

   // Format DocLinks - Doclinks are concatenate fields separated by comma 
   if (!iRet && (iLoadFlag & (LOAD_UPDT|LOAD_LIEN|MERG_GRGR)) )
      iRet = updateDocLinks(Son_MakeDocLink, myCounty.acCntyCode, iSkip, 0);

   return iRet;
}
