#ifndef  _MERGEMPA_H_
#define  _MERGEMPA_H_

// MPA_Char.csv
#define  MPA_CHAR_FEEPARCEL            0
#define  MPA_CHAR_POOLSPA              1
#define  MPA_CHAR_CATTYPE              2
#define  MPA_CHAR_QUALITYCLASS         3
#define  MPA_CHAR_YRBLT                4
#define  MPA_CHAR_BUILDINGSIZE         5
#define  MPA_CHAR_ATTACHGARAGESF       6
#define  MPA_CHAR_DETACHGARAGESF       7
#define  MPA_CHAR_CARPORTSF            8
#define  MPA_CHAR_HEATING              9
#define  MPA_CHAR_COOLINGCENTRALAC     10
#define  MPA_CHAR_COOLINGEVAPORATIVE   11
#define  MPA_CHAR_COOLINGROOMWALL      12
#define  MPA_CHAR_COOLINGWINDOW        13
#define  MPA_CHAR_STORIESCNT           14
#define  MPA_CHAR_UNITSCNT             15
#define  MPA_CHAR_TOTALROOMS           16
#define  MPA_CHAR_EFFYR                17
#define  MPA_CHAR_PATIOSF              18
#define  MPA_CHAR_BEDROOMS             19
#define  MPA_CHAR_BATHROOMS            20
#define  MPA_CHAR_HALFBATHS            21
#define  MPA_CHAR_FIREPLACE            22
#define  MPA_CHAR_ASMT                 23
#define  MPA_CHAR_BLDGSEQNUM           24
#define  MPA_CHAR_HASWELL              25
#define  MPA_CHAR_LOTSQFT              26
#define  MPA_CHAR_PARKINGSPACES        27
#define  MPA_CHAR_FLDS                 28

// NEV has no fireplace data - 20180618
static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "1",    "1", 1,               // 1
   "2",    "2", 1,               // 2
   "3",    "3", 1,               // 3
   "4",    "4", 1,               // 4+
   "FX2",  "2", 3,               // 2 Fireplaces
   "FX3",  "3", 3,               // 3 Fireplaces
   "FPWS", "W", 4,               // Fireplace & Wood stove
   "FP&W", "W", 4,               // Fireplace & Wood stove
   "FP/W", "W", 4,               // Fireplace & Wood stove
   "FPX3", "3", 3,               // 3 Fireplaces
   "FPIN", "I", 4,               // Fireplace Insert
   "FPW/I","I", 5,               // Fireplace Insert
   "F",    "Y", 1,               // Fireplace
   "GA",   "G", 2,               // Gas
   "MA",   "L", 2,               // Masonry
   "MU",   "M", 2,               // Multiple
   "N",    "N", 1,               // None
   "PE",   "S", 2,               // PelletStove
   "PS",   "S", 2,               // PelletStove
   "UN",   "U", 2,               // Unknown Type
   "WA",   "O", 2,               // Wall
   "WD",   "W", 2,               // Woodstove
   "WS",   "W", 2,               // Woodstove
   "WSX2", "W", 4,               // 2 Woodstove
   "WSX3", "W", 4,               // 3 Woodstove
   "ZC",   "Z", 1,               // ZeroClearance
   "Z",    "U", 1,               // Unknown
   "",   "", 0
};

//static XLAT_CODE  asSewer[] =
//{
//   // Value, lookup code, value length
//   "E", "E", 1,               // Engineered system
//   "O", "Z", 1,               // Other than sewer or septic
//   "S", "Y", 1,               // Sewer
//   "T", "S", 1,               // Septic tank
//   "",   "",  0
//};

//static XLAT_CODE  asWaterSrc[] =
//{
//   // Value, lookup code, value length
//   "1", "P", 2,               // Community
//   "2", "W", 2,               // Well
//   "4", "Y", 2,               // Other
//   "",   "",  0
//};

static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "B",    "8", 1,              // Baseboard
   "C",    "Z", 1,              // Central
   "EV",   "X", 1,              // Evaporative
   "E",    "7", 1,              // Electric
   "FH",   "B", 2,              // FAH
   "FO",   "B", 1,              // Forced air
   "F",    "C", 1,              // Floor
   "GR",   "A", 2,              // Gravity
   "H/C",  "Z", 3,              // Central
   "HVAC", "3", 3,              // Heat/Vent/Air Cond
   "HP",   "G", 2,              // Heat Pump
   "H",    "Y", 1,              // Heated
   "MO",   "V", 2,              // Monitor
   "M",    "X", 1,              // Mini split
   "N/A",  "9", 3,              // Unknown
   "N",    "L", 1,              // None
   "PE",   "X", 2,              // Perim
   "P",    "6", 1,              // Portable
   "R",    "I", 1,              // Radiant
   "SO",   "K", 2,              // Solar
   "SP",   "J", 2,              // Space
   "W",    "D", 2,              // Wall Unit
   "UNK",  "9", 3,              // Unknown
   "Z",    "9", 1,              // Unknown
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "0",  "N", 1,              // None
   "1",  "C", 1,              // Pool/Spa
   "2",  "C", 1,              // 2 Pools
   "C",  "D", 1,              // Community
   "FG", "F", 2,              // Fiberglass
   "GN", "G", 2,              // Gunite
   "GU", "G", 2,              // Gunite Pool
   "ID", "I", 2,              // Indoor
   "P&S","C", 3,              // Pool/Spa
   "PAG","X", 3,              // Permanent Above Ground
   "PH", "C", 2,              // Pool & Hot Tub
   "PO", "P", 2,              // Pool (any)
   "VI", "V", 2,              // Vinyl pool
   "VY", "V", 2,              // Vinyl (In Ground)
   "",   "",  0
};

IDX_TBL5 MPA_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "01", "1 ", 'N', 2, 2,     // 
   "02", "74", 'Y', 2, 2,     // 
   "05", "1 ", 'N', 2, 2,     // 
   "08", "75", 'Y', 2, 2,     // 
   "10", "75", 'Y', 2, 2,     // 
   "","",0,0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 MPA_Exemption[] = 
{
   "E01", "H", 3,1,
   "E02", "D", 3,1,     // DISABLED VET (LOW INCOME)
   "E04", "V", 3,1,     // VETERAN
   "E05", "R", 3,1,     // RELIGIOUS/CHURCH
   "E10", "P", 3,1,
   "E11", "X", 3,1,     // LESSEE/LESSOR
   "E12", "I", 3,1,     // WELFARE: HOSPITAL/CLINIC
   "E15", "M", 3,1,     // FREE MUSEUM/FREE LIBRARY
   "E17", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E33", "W", 3,1,     // WELFARE (LOW INCOME HOUSING)
   "P19", "X", 3,1,     // PROP 19 - FOR BYVT 19 REPORTING ONLY
   "","",0,0
};

#endif
