/**************************************************************************
 *
 * Notes: 
 *    1) Review Mer_ExtrLien() before loading LDR.
 *    2) Currently output from -Xs has not been used. 
 *
 * Options:
 *    -L  (load lien)
 *    -U  (load update)
 *    -Xl (extract lien value)
 *    -Xs (extract sale data)  
 *    -O  (overwrite log file)
 *
 * Normal update: -CMER -U -Xs -Xa -Mr -O
 * LDR          : -CMER -L -Xl -Xa -[M|X]s -Mr -O

 * Revision
 * 11/16/2005 1.1.0    Adding Cooling, Hating, and Pool tables
 * 02/28/2007 1.9.2    Sort roll file.
 * 08/22/2007 1.9.15   Add option to create cum sale and lien extract file.
 * 08/24/2007 1.9.16   Fix LDR problem by merging two separate lien files
 *                     to create a lien extract file.
 * 01/25/2008 1.10.3.1 Add code to support standard usecode
 * 03/04/2008 1.10.5.1 Add code to output update records.  Use updFileCmp()
 *                     instead of modify Mer_MergeRoll().
 * 06/25/2008 8.1.0    MER is now sending TR601 file format for LDR.  New function
 *                     MB_ExtrTR601() is created in LoadMB.cpp to extract lien values.
 *                     Remove Mer_ExtrLien() and Mer_AddLien2().
 * 06/29/2008 8.1.1    Add function Mer_Load_LDR(), Mer_MergeLien(), Mer_MergeSitus(),
 *                     Mer_MergeMAdr() for TR601 input format.
 * 03/30/2009 8.7.1    Use MB_MergeSale() to do sale update instead of MergeSale3() to fix 
 *                     problem that non-sale transaction overwrites sale transaction that  
 *                     occurs in the same day  since we only keep one transaction per day.
 * 04/02/2009 8.7.3    Add debug message and more error check.
 * 05/21/2009 8.7.6    Remove reference to OldApn & NewApn files.  We no longer need them.
 * 07/14/2009 9.1.2    Fix Heating/Cooling bug in Mer_MergeChar().  Modify Mer_MergeLien()
 *                     to add other values and replace replChar() with replNull().
 * 07/12/2010 10.0.3   Check for Tax file availability before process it.
 * 07/22/2010 10.1.0   Fix Mer_MergeMAdr() by using new blankRem() that considers hyphen '-'
 *                     as blank if it is preceeded or followed by blank space.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 12/11/2010 10.5.1   Adjust tax calculation for accurate amt.
 * 04/05/2011 10.6.1   Add Mer_ConvertChar() to collect BldgSeqNo & UnitSeqNo.  Output file is sorted
 *                     by APN, BldgSeqNo & UnitSeqNo in ascending.
 * 06/08/2011 10.9.1   Exclude HomeSite from OtherVal.  Replace Mer_MergeExe() with MB_MergeExe()
 *                     and Mer_MergeTax() with MB_MergeTax(). 
 * 06/30/2011 11.0.0   Save original House Number in S_HSENO.
 * 06/28/2012 12.1.1   Modify Mer_MergeMAdr() to add zip code to M_CTY_ST_D and remove extra space
 *                     from S_CTY_ST_D in Mer_MergeSitus()
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 09/13/2013 13.12.2  Add Mer_ConvStdChar() & Mer_MergeStdChar() to support new CHAR format.
 *                     Change to new STDCHAR instead of old MB_CHAR.  Use ApplyCumSale()
 *                     for sale update.  Adding new fields Patio, Rooms, Units & Stories, 
 * 09/18/2013 13.12.4  Adding YrEff.
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 01/08/2014 13.19.0  Add DocCode[] table.
 * 01/20/2014 13.20.1  Modify DocCode[] to set Transfer-Reappraise a sale event.
 * 06/16/2014 14.0.1   Modify Mer_Load_LDR() to use Mer_MergeStdChar() and taking out sale update.
 *                     Use MB_MergeCurRoll() to update transfers in Load_LDR().
 * 11/06/2014 14.9.1   Fix memory issue in Mer_ConvChar()
 * 03/05/2015 14.14.3  Modify Mer_ConvStdChar() & Mer_MergeStdChar() to support another change to CHAR format.
 * 03/10/2015 14.14.4  Modify Mer_ConvStdChar() & Mer_MergeStdChar() to add new CHAR field.
 * 04/21/2015 14.15.2  Fix Mer_MergeSitus() to prevent no record matched.
 * 06/17/2015 15.0.1   Modify Mer_MergeMAdr() to add DBA when DBA is at the beginning of MailAddr_1 or MailAddr_2.
 * 07/14/2015 15.0.4   Fix DBA in Mer_MergeMAdr().
 * 07/31/2015 15.2.0   Fix Mer_MergeMAdr() to remove old DBA before putting new one in.
 * 11/18/2015 15.7.1   Fix GARSQFT problem in Mer_MergeStdChar()
 * 01/04/2016 15.9.0   Add option to load TC file by calling Load_TC()
 * 01/13/2016 15.10.1  Remove old situs in Cal_MergeSitus() before update to avoid remnant from old addr.
 * 02/14/2016 15.12.0  Modify Mer_MergeMAdr() to fix overwrite bug and remove Name2 if it the same as Name1.
 * 06/24/2016 16.0.0   Modify Mer_MergeOwner() to keep owner name the same as county provided except when name is too long.
 *                     Modify MergeLien() to process new layout "L3_" which has some CHAR fields in LDR file.
 *                     Populate PARK_SPACE using LDR data.  Fix Mer_MergeMAdr() and modify Load_Roll() to call the 
 *                     same version of MergeMAdr() as in Load_LDR().
 * 07/06/2016 16.0.2   Add Mer_ExtrTC601() to extract corrected values for LDR.  Also modify Load_LDR() to use value
 *                     from lien extract instead of LDR file if ValueFile is define in INI.
 * 07/17/2016 16.0.4   Replace Mer_ExtrTC601() with MB_ExtrTC601().
 * 03/27/2018 17.9.0   Add -Ut option to update tax.
 * 06/28/2018 18.0.0   Update BATH_4Q & BATH_2Q in Mer_MergeStdChar()
 * 06/25/2019 19.0.0   Fix Garage Sqft in Mer_ConvStdChar() to combine garage 1 & 2.
 *                     Delete Mer_MergeChar() and make use of Mer_MergeStdChar().
 *                     Modify Mer_MergeLien() to add AgPreserve flag.
 *                     Modify Mer_Load_LDR() to remove unused code.
 * 04/27/2020 19.9.0   Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 06/19/2020 20.0.0   Modify Mer_Load_Roll() to stop PARK_SPACE being wiped out.
 *            20.0.0.1 Modify Mer_MergeSitus() to update situs zip and clean up code.
 * 10/13/2020 20.3.2   Add Mer_MakeDocLink() to format DocLink with local doc image.
 * 11/01/2020 20.4.2   Modify Mer_MergeRoll() to populate default PQZoning.
 * 01/06/2021 20.5.0   Copy MB_CreateSCSale() to Mer_CreateSCSale() and fix DocNum for MER special case.
 * 08/09/2021 21.1.7   Modify Mer_MergeStdChar() to populate QualityClass.  
 * 08/26/2021 21.2.0   Default bUseConfSalePrice=false so we don't use confidential sale price.
 * 05/01/2023 22.8.1   Modify MB_CreateSCSale() to accept Docs even if DocNum & DocDate may not match.
 *                     Modify Mer_MakeDocLink() to use the year on DocNum instead of DocDate.
 * 08/13/2024 24.1.0   Modify BldgSeqNo in Mer_ConvStdChar().  Modify Mer_MergeLien() to add ExeType.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "PQ.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "CharRec.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "MergeMer.h"
#include "Sendmail.h"

#define _MERCED_     1

#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "Tax.h"

/******************************* Mer_MakeDocLink *******************************
 *
 * Format DocLink 
 *
 ******************************************************************************/

void Mer_MakeDocLink(char *pDocLink, char *pDoc, char *pDate)
{
   int   iDocNum;
   char  acTmp[256], acDocName[256], acDocNum[32];

   *pDocLink = 0;
   if (*pDoc > ' ' && *pDate > ' ' && *(pDoc+4) == 'R')
   {
      iDocNum = atoin((char *)pDoc+5, 7);       // DocNum can be 6 or 7 digits
      sprintf(acDocNum, "%.6d", iDocNum);       // Reformat DocNum
      sprintf(acDocName, "%.4s\\%.3s\\%.4s%s", pDoc, acDocNum, pDoc, acDocNum);
      sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocName);

      try
      {
         if (!_access(acTmp, 0))
            strcpy(pDocLink, &acDocName[0]);
         else
            *pDocLink = 0;
      } catch (...)
      {
         LogMsg("*** Bad file name: %s", acTmp);
      }
   }
}

/******************************* Mer_ExtrTC601 *******************************
 *
 * Extract lien data from CURRENT SECURED.07.06.16.TC601.txt (2016)
 *
 ****************************************************************************/
//
//int Mer_ExtrTC601Rec(char *pOutbuf, char *pRollRec)
//{
//   int      iRet, lTmp;
//   char     acTmp[64];
//   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;
//
//   // Parse string ignoring quote
//   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
//   if (iRet < V_PRIORNETVALUE)
//   {
//      LogMsg("***** Error: bad input record for APN=%s", apTokens[V_ASMT]);
//      return -1;
//   }
//
//#ifdef _DEBUG
//   //if (!memcmp(apTokens[V_ASMT], "002006058", 9))
//   //   iRet = 0;
//#endif
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', sizeof(LIENEXTR));
//
//   // Start copying data
//   memcpy(pLienExtr->acApn, apTokens[V_ASMT], strlen(apTokens[V_ASMT]));
//
//   // Year assessed
//   vmemcpy(pLienExtr->acYear, apTokens[V_TAXYEAR], 4);
//
//   // Land
//   long lLand = atoi(apTokens[V_CURRENTMARKETLANDVALUE]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
//      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = atoi(apTokens[V_CURRENTSTRUCTURALIMPRVALUE]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
//      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: FixtureRealProperty, PPBusiness, PPMH
//   long lFixt  = atoi(apTokens[V_CURRENTFIXEDIMPRVALUE]);
//   long lGrow  = atoi(apTokens[V_CURRENTGROWINGIMPRVALUE]);
//   long lPers  = atoi(apTokens[V_CURRENTPERSONALPROPVALUE]);
//   long lPP_MH = atoi(apTokens[V_CURRENTPERSONALPROPMHVALUE]);
//   lTmp = lFixt+lGrow+lPers+lPP_MH;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
//      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);
//
//      if (lFixt > 0)
//      {
//         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
//         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
//      }
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
//         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
//      }
//      if (lGrow > 0)
//      {
//         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
//         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
//      }
//      if (lPP_MH > 0)
//      {
//         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
//         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
//      }
//   }
//
//   // Gross total
//   lTmp += (lLand+lImpr);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
//      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
//   }
//
//   // Exemption
//   long lExe1 = atol(apTokens[V_CURRENTEXEMPTIONAMT1]);
//   long lExe2 = atol(apTokens[V_CURRENTEXEMPTIONAMT2]);
//   long lExe3 = atol(apTokens[V_CURRENTEXEMPTIONAMT3]);
//   lTmp = lExe1+lExe2+lExe3;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
//      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
//   }  
//
//   if (!memcmp(apTokens[V_CURRENTEXEMPTIONCODE1], "E01", 3))
//      pLienExtr->acHO[0] = '1';      // 'Y'
//   else
//      pLienExtr->acHO[0] = '2';      // 'N'
//
//   // Save exemption code
//   vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[V_CURRENTEXEMPTIONCODE1], SIZ_LIEN_EXECODEX);
//   vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[V_CURRENTEXEMPTIONCODE2], SIZ_LIEN_EXECODEX);
//   vmemcpy(pLienExtr->extra.MB.ExeCode3, apTokens[V_CURRENTEXEMPTIONCODE3], SIZ_LIEN_EXECODEX);
//
//   pLienExtr->LF[0] = 10;
//   pLienExtr->LF[1] = 0;
//
//   return 0;
//}
//
//int Mer_ExtrTC601(LPCSTR pCnty, LPCSTR pLDRFile, int iChkLastChar)
//{
//   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH];
//   int      iRet, iNewRec=0, lCnt=0;
//
//   LogMsg("Extract LDR Value for %s", pCnty);
//
//   // Open lien file
//   if (!pLDRFile)
//   {
//      GetIniString(pCnty, "ValueFile", "", acBuf, _MAX_PATH, acIniFile);
//      if (!acBuf[0])
//         return -1;
//   } else
//      strcpy(acBuf, pLDRFile);
//
//   LogMsg("Open Lien Date Roll file %s", acBuf);
//   sprintf(acTmpFile, "%s\\%s\\%s_Value.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//
//   // Sort on ASMT
//   strcpy(acRec, "S(1,12,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
//   iRet = sortFile(acBuf, acTmpFile, acRec);
//   if (iRet < 1)
//      return -1;
//
//   // Open ValueFile
//   fdValue = fopen(acTmpFile, "r");
//   if (fdValue == NULL)
//   {
//      LogMsg("***** Error opening sorted LDR value file: %s\n", acTmpFile);
//      return -2;
//   }
//
//   // Create lien extract
//   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
//   LogMsg("Open lien extract file %s", acOutFile);
//   fdLienExt = fopen(acOutFile, "w");
//   if (fdLienExt == NULL)
//   {
//      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
//      return -3;
//   }
//
//   // Get 1st rec
//   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdValue);
//
//   // Merge loop
//   while (pTmp && !feof(fdValue))
//   {
//      // Replace null char with space
//      iRet = replNull(acRec, ' ', 0);
//
//      // Create new record
//      iRet = Mer_ExtrTC601Rec(acBuf, acRec);
//    
//      if (!iRet)
//      {
//         // Write to output
//         fputs(acBuf, fdLienExt);
//         iNewRec++;
//      }
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//
//      // Get next roll record
//      if (!iChkLastChar)
//         pTmp = fgets(acRec, MAX_RECSIZE, fdValue);
//      else if (iChkLastChar == LAST_CHAR_N)
//      {
//         iRet = myGetStrDC(acRec, MAX_RECSIZE, fdValue);
//         if (iRet < 1)
//            break;
//      }
//   }
//   iRet = 0;
//
//   // Close files
//   if (fdValue)
//      fclose(fdValue);
//   if (fdLienExt)
//      fclose(fdLienExt);
//
//   LogMsg("Total output records:       %u", iNewRec);
//   LogMsg("Total records processed:    %u", lCnt);
//   printf("\nTotal output records: %u", lCnt);
//
//   lRecCnt = lCnt;
//   return iRet;
//}

/******************************* Mer_ConvChar() *****************************
 *
 * Same as MB_ConvertChar() with BldgSeqNo & UnitSeqNo.  Output file is sorted
 * by APN, BldgSeqNo & UnitSeqNo in ascending.
 * Other counties has similar format: MER (without LandSqft), YOL, HUM, MAD, MNO, SBT
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

int Mer_ConvChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acOutBuf[1024], acInBuf[1024], acTmpFile[256], acTmp[256], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   MB_CHAR  *pCharRec = (MB_CHAR *)&acOutBuf;

   LogMsg("\nConverting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acInBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acInBuf, 1024, fdIn);

      if (!pRec)
         break;
#ifdef _DEBUG
      //if (!memcmp(acBuf, "004050042000", 9))
      //   iRet = 0;
#endif

      iFldCnt = ParseStringNQ(pRec, cDelim, MB_CHAR_UNITSEQNO+5, apTokens);
      if (iFldCnt < MB_CHAR_UNITSEQNO)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset((void *)&acOutBuf, ' ', sizeof(MB_CHAR));
      memcpy(pCharRec->Asmt, apTokens[MB_CHAR_ASMT], strlen(apTokens[MB_CHAR_ASMT]));

      iTmp = atoi(apTokens[MB_CHAR_POOLS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->NumPools, acTmp, iRet);
      } else if (*apTokens[MB_CHAR_POOLS] >= 'A')
      {
         blankRem(apTokens[MB_CHAR_POOLS]);
         iTmp = strlen(apTokens[MB_CHAR_POOLS]);
         if (iTmp > MBSIZ_CHAR_POOLS) iTmp = MBSIZ_CHAR_POOLS;
         memcpy(pCharRec->NumPools, apTokens[MB_CHAR_POOLS], iTmp);
      }

      blankRem(apTokens[MB_CHAR_USECAT]);
      memcpy(pCharRec->LandUseCat, apTokens[MB_CHAR_USECAT], strlen(apTokens[MB_CHAR_USECAT]));

      blankRem(apTokens[MB_CHAR_QUALITY]);
      memcpy(pCharRec->QualityClass, apTokens[MB_CHAR_QUALITY], strlen(apTokens[MB_CHAR_QUALITY]));

      iTmp = atoi(apTokens[MB_CHAR_YRBLT]);
      if (iTmp > 1600 && iTmp <= lLienYear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YearBuilt, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB_CHAR_BLDGSQFT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->BuildingSize, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB_CHAR_GARSQFT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->SqFTGarage, acTmp, iRet);
      }

      blankRem(apTokens[MB_CHAR_HEATING]);
      memcpy(pCharRec->Heating, apTokens[MB_CHAR_HEATING], strlen(apTokens[MB_CHAR_HEATING]));
      blankRem(apTokens[MB_CHAR_COOLING]);
      memcpy(pCharRec->Cooling, apTokens[MB_CHAR_COOLING], strlen(apTokens[MB_CHAR_COOLING]));
      blankRem(apTokens[MB_CHAR_HEATING_SRC]);
      memcpy(pCharRec->HeatingSource, apTokens[MB_CHAR_HEATING_SRC], strlen(apTokens[MB_CHAR_HEATING_SRC]));
      blankRem(apTokens[MB_CHAR_COOLING_SRC]);
      memcpy(pCharRec->CoolingSource, apTokens[MB_CHAR_COOLING_SRC], strlen(apTokens[MB_CHAR_COOLING_SRC]));

      iTmp = atoi(apTokens[MB_CHAR_BEDS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->NumBedrooms, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB_CHAR_FBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->NumFullBaths, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB_CHAR_HBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->NumHalfBaths, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB_CHAR_FP]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->NumFireplaces, acTmp, iRet);
      }

      memcpy(pCharRec->FeeParcel, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));

      blankRem(apTokens[MB_CHAR_HASSEPTIC]);
      if (*(apTokens[MB_CHAR_HASSEPTIC]) > '0')
         pCharRec->HasSeptic = 'Y';

      blankRem(apTokens[MB_CHAR_HASSEWER]);
      if (*(apTokens[MB_CHAR_HASSEWER]) > '0')
         pCharRec->HasSewer = 'Y';

      blankRem(apTokens[MB_CHAR_HASWELL]);
      if (*(apTokens[MB_CHAR_HASWELL]) > '0')
         pCharRec->HasWell = 'Y';

      iTmp = atoi(apTokens[MB_CHAR_BLDGSEQNO]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%.2d", iTmp);
         memcpy(pCharRec->BldgSeqNo, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB_CHAR_UNITSEQNO]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%.2d", iTmp);
         memcpy(pCharRec->UnitSeqNo, acTmp, iRet);
      }

      pCharRec->CRLF[0] = '\n';
      pCharRec->CRLF[1] = '\0';
      fputs(acOutBuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A) F(TXT)");
      strcpy(pInfile, acCChrFile);
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/**************************** Mer_ConvStdChar ********************************
 *
 * This function replaces Mer_ConvChar() which handles old format.
 * Copy from MergeAma.cpp to convert new char file.
 *
 *****************************************************************************/

int Mer_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip header
   do {
      pRec = fgets(acBuf, 4096, fdIn);
   } while (pRec && !isdigit(*pRec));

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec)
         break;
      if (!isdigit(*pRec))
         continue;

      // Replace NULL with empty string
      replStrAll(acBuf, "NULL", "");
      iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < MER_CHAR_BLDGSEQNUM)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[MER_CHAR_ASMT], strlen(apTokens[MER_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[MER_CHAR_FEEPARCEL], strlen(apTokens[MER_CHAR_FEEPARCEL]));

      // Format APN
      if (*apTokens[MER_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[MER_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         if (bDebug)
            LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[MER_CHAR_FEEPARCEL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[MER_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
      {
         LogMsg("*** BldgSeqNo too big: %d [%s]", iTmp, apTokens[MER_CHAR_ASMT]);
         iRet = sprintf(acTmp, "%4d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      }

      // Rooms
      iTmp = atoi(apTokens[MER_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - 01, 02, 03, 99
      iTmp = blankRem(apTokens[MER_CHAR_POOLSPA]);
      if (iTmp > 1)
      {
         pRec = findXlatCode(apTokens[MER_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass - CD050D, MD060A
      iTmp = blankRem(apTokens[MER_CHAR_QUALITYCLASS]);
      pRec = _strupr(apTokens[MER_CHAR_QUALITYCLASS]);
      if (iTmp > SIZ_CHAR_QCLS)
         iTmp = SIZ_CHAR_QCLS;
      memcpy(myCharRec.QualityClass, apTokens[MER_CHAR_QUALITYCLASS], iTmp);
      strcpy(acTmp, apTokens[MER_CHAR_QUALITYCLASS]);
      acCode[0] = 0;
      if (isalpha(acTmp[0]) && (isdigit(acTmp[2]) || isdigit(acTmp[1])) )
      {
         myCharRec.BldgClass = acTmp[0]; 
         iTmp = 1;
         while (acTmp[iTmp] && !isdigit(acTmp[iTmp]))
            iTmp++;

         if (acTmp[iTmp] >= '0')
            iRet = Quality2Code((char *)&acTmp[iTmp], acCode, NULL);
      } else if (acTmp[0] > '0' && acTmp[0] <= '9')
         iRet = Quality2Code(acTmp, acCode, NULL);
      if (acCode[0] > ' ')
         myCharRec.BldgQual = acCode[0];

      int iYrBlt = atoi(apTokens[MER_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[MER_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[MER_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[MER_CHAR_UNITSCNT]);
      //if (iTmp > 0 && iBldgSize > iTmp*100)
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 
      //else if (iTmp > 1)
      //   LogMsg("*** Questionable UnitsCnt: %d, BldgSqft=%d, Apn=%s", iTmp, iBldgSize, apTokens[MER_CHAR_ASMT]);

      // Stories/NumFloors
      iTmp = atoi(apTokens[MER_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 20)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[MER_CHAR_ATTACHGARAGESF]);
      int iDetGar = atoi(apTokens[MER_CHAR_DETACHGARAGESF]);
      int iCarport = atoi(apTokens[MER_CHAR_CARPORTSF]);

      if (iAttGar > 100 && iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar+iDetGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'Z';
      } else if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      } else if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport Sqft
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "078140041000", 9))
      //   iRet = 0;
#endif

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[MER_CHAR_HEATING]);
      if (iTmp == 1)
      {
         pRec = findXlatCode(apTokens[MER_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[MER_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[MER_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[MER_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[MER_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[MER_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[MER_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[MER_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace - 1, 2, 99, y
      if (*apTokens[MER_CHAR_FIREPLACE] > '0' && *apTokens[MER_CHAR_FIREPLACE] < '9')
         myCharRec.Fireplace[0] = *apTokens[MER_CHAR_FIREPLACE];

      // Patio SF
      iTmp = atoi(apTokens[MER_CHAR_PATIOSF]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // Haswell - 1, 0
      blankRem(apTokens[MER_CHAR_HASWELL]);
      if (*(apTokens[MER_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum, UnitSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/******************************** Mer_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Mer_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iRet;
   char  acTmp1[128], acTmp[128], *pTmp, *pTmp1;
   char  acName1[64], acName2[64], acOwner[64];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);
   strcpy(acTmp1, pNames);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006501041000", 9))
   //   iTmp = 0;
#endif

   // blankout MRS
   //if (pTmp=strstr(acTmp1, " MRS"))
   //   memcpy(pTmp, "    ", 4);

   // Remove multiple spaces
   iTmp = 0;
   pTmp = acTmp1;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.'
      if (*pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ' || acTmp[iTmp-1] == '&')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acName2[0] = 0;
   strcpy(acOwner, acTmp);
   if (pTmp=strstr(acTmp, " 1/2 INT"))
      *pTmp = 0;
   
   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Check for year that goes before TRUST
   iTmp =0;   
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP);
      return;
   }

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
      // If TRUST appears after number, save from number forward
      if (strstr((char *)&acTmp[iTmp], " TRUST") )
         acTmp[iTmp-1] = 0;
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
      if ((pTmp=strstr(acName2, " ETAL")) || (pTmp=strstr(acName2, " ET AL")) )
         *pTmp = 0;
   } else if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Filter out words, things in parenthesis 
   // MONDANI NELLIE M ESTATE OF

   if ( (pTmp = strchr(acTmp, '(')) || (pTmp=strstr(acTmp, " TRUS"))  || (pTmp=strstr(acTmp, ",TRUS"))  ||
      (pTmp=strstr(acTmp, " COTRUST")) || (pTmp=strstr(acTmp, " ESTATE OF")) || (pTmp=strstr(acTmp, " LIFE ESTATE")) ||
      (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTS OF")) || 
      (pTmp=strstr(acTmp, " ETAL"))  || (pTmp=strstr(acTmp, " ET AL")) || (pTmp=strstr(acTmp, " COGUARDIAN")) )
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " JT")) || (pTmp=strstr(acTmp, " CP")) || (pTmp=strstr(acTmp, " CO-TR")))
      *pTmp = 0;

   if (pTmp=strstr(acTmp, "&-") )
      *(pTmp+1) = ' ';

   if ((pTmp=strstr(acTmp, " SUCCS-TR")) || (pTmp=strstr(acTmp, " SUCCESSOR")) || 
      (pTmp=strstr(acTmp, " SURVIVOR")) )
   {
      *pTmp = 0; 
      strcpy(acName1, acTmp); 
   } else if (pTmp=strstr(acTmp, " FAMILY TRUST &"))
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      // RUMAN RICKY L & RUMAN FAMILY TRUST &
      // RUMAN RICKY L & NANCY FAMILY TRUST & MARIE
      // VERMETTE JAMES & THERESA A & ZENDER ROBERT A &
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST &"))
   {  // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE  
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      *(pTmp+6) = 0;
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
         *pTmp1 = 0;
      else
         *pTmp = 0;

      strcpy(acName1, acTmp);
      strcpy(acName2, pTmp+9);
   } else if (pTmp=strstr(acTmp, " LAND TRUST"))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " FAMILY "))
             || (pTmp=strstr(acTmp, " INCOME TR")) || (pTmp=strstr(acTmp, " LIVING TR")) )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " ESTATE OF"))
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   //if (pTmp = strstr(acSave1, " THE"))
   //   *pTmp = 0;

   // Split name into two if '/' presents
   //if (pTmp = strchr(acName1, '/'))
   //{  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
   //   // Avoid 1/2 INT
   //   if (!isdigit(*(pTmp-1)))
   //   {
   //      *pTmp++ = 0;
   //      if (pTmp1 = strchr(pTmp, '/'))
   //         *pTmp1 = 0;
   //      strcpy(acName2, pTmp);
   //   }
   //}

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;

         // Don't create Name2 if all owners fit in Name1
         if (strlen(acOwner) > SIZ_NAME1)
         {
            strcpy(acName2, pTmp1);
            // If Name1=Name2, drop Name2
            if (!memcmp(acName1, acName2, strlen(acName2)))
               acName2[0] = 0;
            else
               strcpy(acOwner, acName1);
         } else
            acName2[0] = 0;
      }
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      // If name is not swapable, use Name1 instead
      if (iRet == -1)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
      else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
   {
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
   }

   // Save Name1
   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);

   // Process Name2 when there is more than one word
   if (acName2[0] && strchr((char *)&acName2[1], ' ') && myOwner.acName2[0] <= ' ')
   {
#ifdef _DEBUG
      LogMsg("--> Has Name2: %.12s: %s: %s", pOutbuf, acOwner, acName2);
#endif
      vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);
   }
}

/******************************** Mer_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Mer_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64];
   int      iTmp;
   char *    pTmp;
   ADR_REC  sMailAdr;

   // Skip test address
   if (!memcmp(apTokens[MB_ROLL_M_ADDR], "XXX", 3) || !memcmp(apTokens[MB_ROLL_M_ADDR], "000", 3))
      return;

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   } 

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
     sMailAdr.strName[SIZ_M_STREET] = 0;
     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], SIZ_M_CITY);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      int iZipLen = strlen(apTokens[MB_ROLL_M_ZIP]);
      if (iZipLen >= 5)
      {
         if (iZipLen == 10 && *(apTokens[MB_ROLL_M_ZIP]+5) == '-')
         {
            memcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], 5);
            memcpy(pOutbuf+OFF_M_ZIP4, apTokens[MB_ROLL_M_ZIP]+6, 4);
            sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
         } else if (iZipLen == 9)
         {
            memcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], iZipLen);
            sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP], apTokens[MB_ROLL_M_ZIP]+5);
         } else
         {
            vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], SIZ_M_ZIP+SIZ_M_ZIP4);
            sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
         }
      }

      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

/********************************* Mer_MergeMAdr *****************************
 *
 * Notes:
 *    - Mail addr is provided in various form.  We try the best we can since there is no way to have 100% covered
 *    - MailAdr1 may contain DBA.  Make sure to capture DBA and put it in appropriate place
 *
 *    - Sample Input:
 *      AMERICAS SERVICING CO           3476 STATEVIEW BLVD             FORECLOSURE MAC#7801-013        FT MILL SC 29715
 *      ALFRED F & IRENE G DEMELO       DBA WALGREENS #6418-S-P         300 WILMOT RD MS#1435           DEERFIELD IL 60015-5121
 *      AD VALOREM TAX DEPT (UNIT 3505) DBA BEACON OIL COMPANY          ONE VALERO WAY                  SAN ANTONIO TX 78249-1616
 *      ATTN  DAVE ARNAIZ               PMB 330                         PO BOX 2818                     ALPHARETTA GA 30023
 *      C/O BANK OF AMERICA             ATTN: BURR WOLFF LP             505 CITY PARKWAY WEST STE 100   ALPHARETTA GA 30023
 *      C/O CITIMORTGAGE, INC.          MAILSTOP 02-25                  27555 FARMINGTON RD             FARMINGTON HILLS MI 48334-3357
 *      RE: LOAN# 0089214605/BALTAZAR   2300 BROOKSTONE CENTRE PKWY     PO BOX 84013                    PO BOX 84013
 *
 *****************************************************************************/

void Mer_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "020142045", 9) )
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         // Update DBA
         strcpy(acTmp, pLine1+4);
         iTmp = strlen(acTmp);
         if (iTmp > SIZ_DBA) iTmp = SIZ_DBA;
         memcpy(pOutbuf+OFF_DBA, acTmp, iTmp);
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         // Update DBA
         strcpy(acTmp, pLine1+4);
         iTmp = strlen(acTmp);
         if (iTmp > SIZ_DBA) iTmp = SIZ_DBA;
         memcpy(pOutbuf+OFF_DBA, acTmp, iTmp);
         p1 = pLine2;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }
   } else if (*pLine2 >= 'A' || *(pLine2+1) >= 'A')
   {
      p1 = pLine1;
      if (*pLine2 == ' ')
         p2 = pLine2+1;
      else
         p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
      /*
      if (!_memicmp(p0, "C/O", 3))
         pTmp = p0+4;
      else if (!_memicmp(p0, "ATTN", 4))
         pTmp = p0+5;
      else if (*p0 == '%')
         pTmp = p0+1;

      while (*pTmp == ' ')
         pTmp++;
      iTmp = strlen(pTmp);
      if (iTmp > SIZ_CARE_OF)
         iTmp = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, pTmp, iTmp);
      */
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, '-', SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      } else
      {
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
      }
   }

   strcpy(acAddr2, p2);
   blankRem(acAddr2);

   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001072002000", 9) )
   //   iTmp = 0;
#endif

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      if (sMailAdr.Zip[0] >= '0')
         vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      if (sMailAdr.Zip4[0] >= '0')
         vmemcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip, SIZ_M_ZIP4);
   }
}

/******************************** Mer_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Mer_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      //for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      //   pRec = fgets(acRec, 512, fdSitus);

      // Get first rec
      do {
         pRec = fgets(acRec, 512, fdSitus);
      } while (!isdigit(*pRec ));
   }

   do 
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF 
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   if (pRec)
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old Situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      // Save original StrNum
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      iTmp = sprintf(acTmp, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      strcpy(acAddr1, acTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
      }
   }

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);

      vmemcpy(pOutbuf+OFF_S_STREET, apTokens[MB_SITUS_STRNAME], SIZ_S_STREET);

      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
      {
         Sfx2Code(acTmp, acCode);
         vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      } else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         iBadSuffix++;
      }
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apTokens[MB_SITUS_STRNAME]);
      if (sAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      if (sAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, strlen(sAdr.strSfx));

      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      memcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], strlen(apTokens[MB_SITUS_UNIT]));
   }

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1);   
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
      {
         if (*apTokens[MB_SITUS_ZIP] == '9') 
         {
            vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], SIZ_S_ZIP);
            sprintf(acTmp, "%s CA %s", acAddr1, apTokens[MB_SITUS_ZIP]);
         } else
            sprintf(acTmp, "%s CA", acAddr1);
         iTmp = blankRem(acTmp);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      }
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int Mer_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "042020017000", 9))
   //   acTmp[0] = 0;
#endif
   // 
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   memcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.HseNo, strlen(sSitusAdr.HseNo));
   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/******************************** Mer_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Mer_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do 
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   while (!iTmp)
   {
      // Replace tab char with 0
      if (pRec)
         iRet = ParseStringNQ(pRec, ',', MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "005030053", 9))
      //   iRet = 0;
#endif

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         /* Do not use Confidential Sale Price
         } else
         {
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            }
         */
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         MB_MergeSale(&sCurSale, pOutbuf, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/***************************** Mer_MergeStdChar ******************************
 *
 * This function replaces Mer_MergeChar() which uses MB_CHAR structure.
 *
 * Note: need code table for Heating and Merling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

int Mer_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   int      iTmp, iLoop, iBeds, iFBath, iHBath, iFp, lBldgSqft;
   STDCHAR  *pChar;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 512, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass; 
   *(pOutbuf+OFF_BLDG_QUAL) = pChar->BldgQual; 
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Condition
   *(pOutbuf+OFF_IMPR_COND) = pChar->ImprCond[0];

   // Yrblt
   memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // YrEff
   memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   long lSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
   } else
   {
      memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = ' ';
   }

   // Heating
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];

   // Cooling
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];
   
   // Pool
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      memcpy(pOutbuf+OFF_BATH_4Q, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      memcpy(pOutbuf+OFF_BATH_2Q, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Fireplace
   iFp = atoin(pChar->Fireplace, SIZ_CHAR_SIZE2);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   } else
      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);
   
   // Rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Units
   iTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (pChar->Units[0] > ' ')
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Stories
   if (pChar->Stories[0] > ' ')
      memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);

   // Patio
   if (pChar->PatioSqft[0] > ' ')
      memcpy(pOutbuf+OFF_PATIO_SF, pChar->PatioSqft, SIZ_CHAR_SQFT);

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);
   lCharMatch++;

   return 0;
}

/******************************** Mer_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************

int Mer_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do 
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         return 1;      // EOF 
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, ',');
      pTmp += 2;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 0;
   }

   iRet = ParseStringNQ(pRec, ',', MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Mer_MergeTax ******************************
 *
 * Note: Add 0.0001 to total tax amt before convert float to long value.
 *
 ****************************************************************************

int Mer_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double	dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iRet;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "008290028000", 9))
   //   iTmp = 0;
#endif

   // Merge tax data
   while (!iTmp)
   {
      iTmp = ParseStringNQ(pRec, ',', MB_TAX_ROLLCAT+1, apTokens);
      if (iTmp < MB_TAX_ROLLCAT)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
         return -1;
      }

      // If same tax year, apply values
      //if (!memcmp(myCounty.acYearAssd, apTokens[MB_TAX_YEAR], 4))
      {
         dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
         dTax1 = atof(acTmp);
         dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
         dTax2 = atof(acTmp);
         dTmp = dTax1+dTax2+0.0001;
         if (dTax1 == 0.0 || dTax2 == 0.0)
            dTmp *= 2;

         if (dTmp > 0.0)
         {
            iTmp = (long)(dTmp*100.0);
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, iTmp);
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/********************************* Mer_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mer_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace tab char with 0
   iTokens = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN starts with 800-999 except 910 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "24MER", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Megabyte:
   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      if (iNumUseCodes > 0)
      {
         pTmp = Cnty2StdUse(acTmp);
         if (pTmp)
            memcpy(pOutbuf+OFF_USE_STD, pTmp, 3);
         else
         {
            memcpy(pOutbuf+OFF_USE_STD, USE_MISC, 3);
            logUsecode(acTmp, pOutbuf);
         }
      }
   }

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], strlen(apTokens[MB_ROLL_DOCNUM]));
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   }

   // Owner
   try {
      Mer_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   } catch(...) {
      LogMsg("***** Exeception occured in Mer_MergeOwner()");
   }

   // Mailing 
   try {
      if (iTokens > MB_ROLL_M_ADDR4)
         Mer_MergeMAdr(pOutbuf, apTokens[MB_ROLL_M_ADDR1], apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3], apTokens[MB_ROLL_M_ADDR4]);
      else
         Mer_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Mer_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/******************************** Mer_Load_Roll *****************************
 *
 * Use this version to avoid PARK_SPACE being wiped out.
 *
 ****************************************************************************/

int Mer_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Fix broken records in roll file
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "RMP");
   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A)");
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small --> %s", acRollFile);
      return -1;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Open Tax file
   if (!_access(acTaxFile, 0))
   {
      LogMsg("Open Tax file %s", acTaxFile);
      fdTax = fopen(acTaxFile, "r");
      if (fdTax == NULL)
      {
         LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
         return -6;
      }
   } else
      fdTax = NULL;

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
      replNull(acRollRec);
      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Mer_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Mer_MergeSitus(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Mer_MergeStdChar(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acBuf);        // Mer_Tax.csv

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Mer_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Mer_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Mer_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      replNull(acRollRec);
      iRet = Mer_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Mer_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Mer_MergeStdChar(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTax(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("      records output:       %u", lRecCnt);
   LogMsg("      new records:          %u", iNewRec);
   LogMsg("      updated records:      %u", iRollUpd);
   LogMsg("      retired records:      %u", iRetiredRec);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Tax skiped:       %u", lTaxSkip);

   LogMsg("Last recording date:        %u\n", lLastRecDate);

   return 0;
}

int Mer_Load_Roll_Old(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iNewRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0, lTmp;

   // Rename old R01 to S01 if S01 is not present
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   if (_access(acTmpFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acTmpFile);
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "RMP");
   lTmp = sortFile(acRollFile, acTmpFile, "S(#1,C,A)");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -1;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -3;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -5;
   }

   // Open Tax file
   if (!_access(acTaxFile, 0))
   {
      LogMsg("Open Tax file %s", acTaxFile);
      fdTax = fopen(acTaxFile, "r");
      if (fdTax == NULL)
      {
         LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
         return -6;
      }
   } else
      fdTax = NULL;

   // Open lien file
   fdLienExt = NULL;
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Lien file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating R01 file: %s\n", acTmpFile);
      return -10;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Skip header record
   do {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   } while (acRec[0] != '0');

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new R01 record
      iRet = Mer_MergeRoll(acBuf, acRec, MAX_RECSIZE, CREATE_R01|CREATE_LIEN);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lTmp = Mer_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lTmp = Mer_MergeStdChar(acBuf);

         // Merge Taxes
         if (fdTax)
            lTmp = MB_MergeTax(acBuf);

         // Merge lien values
         lTmp = 1;
         if (fdLienExt)
            lTmp = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Exe if not found in LienExt
         if (fdExe && lTmp)
            lTmp = MB_MergeExe(acBuf);

         // Save last recording date
         lTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            break;
         }
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total processed records:    %u", lCnt);
   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   lRecCnt = iNewRec;

   // Force email
   if (!lSitusMatch || !lCharMatch || !lTaxMatch)
   {
      bSendMail = true;
      lRet = 1;
      LogMsg("***** Check situs, char, and tax file for unmatched reason");
   }

   return lRet;
}

/********************************* Mer_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * L3_GARAGE contains # park spaces
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mer_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "24MER", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&MER_Exemption);

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "014183014000", 9))
   //   iTmp = 0;
#endif
   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp * SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Mer_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   Mer_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Mer_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*apTokens[L3_CURRENTDOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 

   // Number of parking spaces
   iTmp = atol(apTokens[L3_GARAGE]);
   if (iTmp > 0 && iTmp < 99)
   {
      sprintf(acTmp, "%d", iTmp);
      vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
   }

   // AgPreserved
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   return 0;
}

// 2015
//int Mer_MergeLien(char *pOutbuf, char *pRollRec)
//{
//   char     acTmp[256], acTmp1[64];
//   long     lTmp;
//   double   dTmp;
//   int      iRet=0, iTmp;
//
//   // Replace null char with space
//   iRet = replNull(pRollRec, ' ', 0);
//
//   // Parse input rec
//   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
//   if (iRet < L_DTS)
//   {
//      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
//      return -1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//
//   // Start copying data
//   memcpy(pOutbuf, apTokens[L_ASMT], strlen(apTokens[L_ASMT]));
//
//   // Copy ALT_APN
//   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L_FEEPARCEL], strlen(apTokens[L_FEEPARCEL]));
//
//   // Format APN
//   iRet = formatApn(apTokens[L_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//   // Create MapLink and output new record
//   iRet = formatMapLink(apTokens[L_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // Create index map link
//   if (getIndexPage(acTmp, acTmp1, &myCounty))
//      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//   // County code
//   memcpy(pOutbuf+OFF_CO_NUM, "24MER", 5);
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // Land
//   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: Growing Impr, Fixture, PersProp, PPMH
//   long lFixtr = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
//   long lGrow  = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);
//   long lPers  = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
//   long lPP_MH = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);
//   lTmp = lFixtr+lGrow+lPers+lPP_MH;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//
//      if (lFixtr > 0)
//      {
//         sprintf(acTmp, "%d         ", lFixtr);
//         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
//      }
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%d         ", lPers);
//         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
//      }
//      if (lGrow > 0)
//      {
//         sprintf(acTmp, "%d         ", lGrow);
//         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
//      }
//      if (lPP_MH > 0)
//      {
//         sprintf(acTmp, "%d         ", lPP_MH);
//         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
//      }
//   }
//
//   // Gross total
//   lTmp += (lLand+lImpr);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//   // Tax
//   double dTax1 = atof(apTokens[L_TAXAMT1]);
//   double dTax2 = atof(apTokens[L_TAXAMT2]);
//   dTmp = dTax1+dTax2;
//   if (dTax1 == 0.0 || dTax2 == 0.0)
//      dTmp *= 2;
//
//   if (dTmp > 0.0)
//   {
//      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
//      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
//   } else
//      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
//
//   // Exemption
//   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
//   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
//   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
//   lTmp = lExe1+lExe2+lExe3;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
//      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
//   }  
//   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
//      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
//   else
//      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
//
//   // Save exemption code
//   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
//   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
//   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));
//
//   // TRA
//   memcpy(pOutbuf+OFF_TRA, apTokens[L_TRA], strlen(apTokens[L_TRA]));
//
//   // status
//   *(pOutbuf+OFF_STATUS) = *apTokens[L_STATUS];
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "030330020", 9))
//   //   iTmp = 0;
//#endif
//   // Legal
//   updateLegal(pOutbuf, apTokens[L_PARCELDESCRIPTION]);
//
//   // UseCode
//   iTmp = strlen(apTokens[L_USECODE]);
//   if (iTmp > 0)
//   {
//      if (iTmp > SIZ_USE_CO)
//         iTmp = SIZ_USE_CO;
//      memcpy(pOutbuf+OFF_USE_CO, apTokens[L_USECODE], iTmp);
//   
//      // Std Usecode
//      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L_USECODE], iTmp, pOutbuf);
//   }
//
//   // Acres
//   dTmp = atof(apTokens[L_ACRES]);
//   if (dTmp > 0.0)
//   {
//      // Lot Sqft
//      lTmp = (long)(dTmp * SQFT_PER_ACRE);
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      // Format Acres
//      lTmp = (long)(dTmp * ACRES_FACTOR);
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   // AgPreserved
//   if (*apTokens[L_ISAGPRESERVE] == '1')
//      *(pOutbuf+OFF_AG_PRE) = 'Y';
//
//   // Owner
//   Mer_MergeOwner(pOutbuf, apTokens[L_OWNER]);
//
//   // Situs
//   Mer_MergeSitus(pOutbuf, apTokens[L_SITUS1], apTokens[L_SITUS2]);
//
//   // Mailing
//   Mer_MergeMAdr(pOutbuf, apTokens[L_MAILADDRESS1], apTokens[L_MAILADDRESS2], apTokens[L_MAILADDRESS3], apTokens[L_MAILADDRESS4]);
//
//   // SetTaxcode, Prop8 flag, FullExe flag
//   iTmp = updateTaxCode(pOutbuf, apTokens[L_TAXABILITY], true, true);
//
//   return 0;
//}

/********************************* Mer_Load_LDR *****************************
 *
 * Load TR601 LDR into 1900-byte record.
 *
 ****************************************************************************/

int Mer_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   //char     acCurRoll[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     iRet, lRet=0, lCnt=0;

   lLastFileDate = getFileDate(acRollFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   sprintf(acRec, "S(#%d,C,A) DEL(%d)", L3_ASMT+1, cLdrSep);
   iRet = sortFile(acRollFile, acTmpFile, acRec);
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open LDR file %s", acTmpFile);
   fdLDR = fopen(acTmpFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }

   // Open current roll file - to update recently transfer
   //GetIniString(myCounty.acCntyCode, "RollFile", "", acBuf, _MAX_PATH, acIniFile);
   //sprintf(acCurRoll, acBuf, myCounty.acCntyCode, myCounty.acCntyCode);
   //LogMsg("Open current roll file %s", acCurRoll);
   //fdRoll = fopen(acCurRoll, "r");
   //if (fdRoll == NULL)
   //{
   //   LogMsg("***** Error opening current roll file: %s.  Please check INI file in [%s] section\n", acCurRoll, myCounty.acCntyCode);
   //   return -2;
   //}  

   // Open Value file
   //sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (!_access(acTmpFile, 0))
   //{
   //   LogMsg("Open Lien file %s", acTmpFile);
   //   fdLienExt = fopen(acTmpFile, "r");
   //   if (fdLienExt == NULL)
   //   {
   //      LogMsg("***** Error opening lien file: %s\n", acTmpFile);
   //      return -7;
   //   }
   //} else
   //   fdLienExt = NULL;

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating R01 file: %s\n", acTmpFile);
      return -1;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdLDR))
   {
      // Create new R01 record
      iRet = Mer_MergeLien(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         //if (fdLienExt)
         //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Char
         if (fdChar)
            lRet = Mer_MergeStdChar(acBuf);

         // Merge transfer from current roll
         //if (fdRoll)
         //   lRet = MB_MergeCurRoll(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!isdigit(acRec[1]))
         break;      // EOF
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdChar)
      fclose(fdChar);
   //if (fdRoll)
   //   fclose(fdRoll);
   //if (fdLienExt)
   //   fclose(fdLienExt);
   
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/*
 * This is a copy of MB_CreateSCSale() and modified for MER
 */
int Mer_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], acDocNum[16], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp, lBadDocs=0, lFixDocs=0;

   LogMsg0("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      sprintf(acTmp, "*** %s - Error opening Sales file.  Errno=%d", myCounty.acCntyCode, errno);
      sprintf(acRec, "Please review log file for more info.");
      mySendMail(acIniFile, acTmp, acRec, NULL);

      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Replace null char with space
      replNull(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Ignore DocNum start with 1900
      if (!memcmp(apTokens[MB_SALES_DOCNUM], "1900R", 5))
         continue;

      if (*(4+apTokens[MB_SALES_DOCNUM]) == 'I')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "052202011000", 9))
      //   iTmp = 0;
#endif
      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
      {
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }

      // Docnum
      acDocNum[0] = 0;
      if (pTmp = strchr(apTokens[MB_SALES_DOCNUM], '`'))
         *pTmp = 0;
      memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));

      // Verify DocNum (DocNum is mostly correct, but DocDate may be wrong)
      if (memcmp(SaleRec.DocNum, SaleRec.DocDate, 4))
      {
         // Correct DocNum
         if (!memcmp(&SaleRec.DocNum[5], &SaleRec.DocDate[2], 2))
         {
            iTmp = sprintf(acDocNum, "%.4sR00%.5s", SaleRec.DocDate, &SaleRec.DocNum[7]);
            if (bDebug)
               LogMsg("Fix DocNum APN=%.12s DocNum=%.12s & DocDate=%.8s, New DocNum=%s", SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, acDocNum);
            memcpy(SaleRec.DocNum, acDocNum, iTmp);
            lFixDocs++;
         } else
         {
            LogMsg("??? Bad Doc APN=%.12s, DocNum=%.12s, DocDate=%.8s, DocTax=%s, Price=%s, DocCode=%s", SaleRec.Apn, SaleRec.DocNum, 
            SaleRec.DocDate, apTokens[MB_SALES_TAXAMT], apTokens[MB_SALES_PRICE], apTokens[MB_SALES_DOCCODE]);
            lBadDocs++;
         }
      }

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1' || *apTokens[MB_SALES_GROUPSALE] == 'T')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001130011000", 9))
      //   iTmp = 0;
#endif
      // Confirmed sale price
      dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
      if (acTmp[0] > '0')
      {
         lPrice = atol(acTmp);
         if (lPrice < 1000)
         {
            // This doesn't look normal, but keep it for reference only
            iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            lPrice = 0;
         } else
            iTmp = sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      } 

      // Do not use confirmed sale price
      if (!bUseConfSalePrice)
         lPrice = 0;

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001160016000", 9))
      //   iTmp = 0;
#endif
      // Tax
      dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
      if (acTmp[0] > '0')
      {
         dTmp = atof(acTmp);

         // Save DocTax
         iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);

         // Calculate sale price
         lTmp = (long)(dTmp * SALE_FACTOR);
         iTmp = ((int)dTmp/100)*100;

         // Check for bad DocTax
         if (dTmp > 100000)
         {
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else if (lPrice > 100000 && lPrice == (lPrice/100)*100)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTmp);
            } else
            {
               LogMsg("??? (1) Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f, CP=%d.  Need investigation.  Ignore price.", SaleRec.Apn, SaleRec.DocNum, dTmp, lPrice);
               lPrice = 0;
            }
         } else if (lPrice > 0 && (long)dTmp >= lPrice+5000)
         {
            // Even though sale tax is questionable, we still rely on it if it is reasonable
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** (2) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               if (lPrice == (lPrice/100)*100)
                  LogMsg("*** (2) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTmp);
               else
               {
                  LogMsg("??? (2) Questionable Sale Tax Amt for %.12s: DocNum=%.12s, Price=%d, \ttax=%.2f).  Need investigation.", SaleRec.Apn, SaleRec.DocNum, lPrice, dTmp);
                  lPrice = lTmp;
               }
            }
         } else if (iTmp == (int)dTmp && iTmp > 10000)
         {
            if (lTmp != (lTmp/100)*100)
            {
               LogMsg("*** (3) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
               lPrice = lTmp;
         } else if (lTmp == (lTmp/100)*100)
            lPrice = lTmp;
         else if (lTmp > 1000 && lPrice == 0)
            lPrice = lTmp;

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Ignore sale price.", 
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE]);
               lPrice = 0;
            }
         }  
      } 

      // Ignore sale price if less than 1000
      if (lPrice >= 10000)
         sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
      else if (lPrice >= 1000)
         sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
      else
         memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
      memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);

      // Doc code 
      if (isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         if (pDocTbl)
         {
            iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
            if (iTmp >= 0)
            {
               memcpy(SaleRec.DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
               if (lPrice < 1000)
                  SaleRec.NoneSale_Flg = pDocTbl[iTmp].flag;
            } else if (bDebug)
               LogMsg("*** Unknown DocCode: %s", apTokens[MB_SALES_DOCCODE]);
         } else
         {
            iTmp = atoi(apTokens[MB_SALES_DOCCODE]);
            if (iTmp == 1 || lPrice > 0)
               SaleRec.DocType[0] = '1';
         }
      } 

      // Save original DocCode
      vmemcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);

      // Transfer Type
      if (*apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         strcpy(acTmp, apTokens[MB_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Seller1, acTmp, SALE_SIZ_SELLER, iTmp);

         // Buyer
         strcpy(acTmp, apTokens[MB_SALES_BUYER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Name1, acTmp, SALE_SIZ_BUYER, iTmp);

         SaleRec.CRLF[0] = 10;
         SaleRec.CRLF[1] = 0;
         fputs((char *)&SaleRec,fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(20,1,C,EQ,\" \",OR,31,1,C,EQ,\" \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -2;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            // Save old cumsale file
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);
            iTmp = rename(acCSalFile, acTmpFile);
   
            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -2;
      } else
         iTmp = rename(acOutFile, acCSalFile);

      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acCSalFile, acTmpFile);
      }
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp)
   {
      iErrorCnt++;
      if (iTmp == -2)
         LogMsg("***** Error sorting output file");
      else
         LogMsg("***** Error renaming to %s", acCSalFile);
   }

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                          output: %d.", lTmp);
   LogMsg("            Number of bad DocNum: %d.", lBadDocs);
   LogMsg("            Number of fix DocNum: %d.", lFixDocs);
   LogMsg("         Latetest recording date: %d.", lLastRecDate);
   return iTmp;
}

/*********************************** loadMer ********************************
 *
 * Options:
 *    -L  (load lien)
 *    -U  (load update)
 *    -Xl (extract lien value)
 *    -Xs (extract sale data)  
 *    -O  (overwrite log file)
 *
 * LDR load:      -CMER -L -Xl -Xs|-Ms [-Xa] -Mr -O
 * Normal update: -CMER -U -Xs|-Ms [-Xa] -Mr -O
 *
 ****************************************************************************/

int loadMer(int iSkip)
{
   int   iRet=0;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;

   iApnLen = myCounty.iApnLen;

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      TC_SetDateFmt(MM_DD_YYYY_1);
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Extract lien file
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acValueFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acValueFile, 0);      // 2016 
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode, NULL, 0);
   }

   // Create/Update cum sale file
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Do not use confirmed sale price
      //bUseConfSalePrice = false;

      // 01/06/2014
      //iRet = MB_CreateSCSale(MM_DD_YYYY_1, 0, 0, false, (IDX_TBL5 *)&MER_DocCode[0]);
      // 01/05/2021
      iRet = Mer_CreateSCSale(MM_DD_YYYY_1, 0, 0, false, (IDX_TBL5 *)&MER_DocCode[0]);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Convert CHAR file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      if (!_access(acCharFile, 0))
      {
         iRet = Mer_ConvStdChar(acCharFile);
         if (iRet <= 0)
         {
            LogMsg("***** Error converting Char file %s", acCharFile);
            return -1;
         }
      } else
         LogMsg("***** Error: Missing CHAR file %s", acCharFile);
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      //iRet = CreateMerRoll(iSkip);
      iRet = Mer_Load_LDR(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      if (acRollFile[0])
      {
         LogMsg0("Load %s roll update file", myCounty.acCntyCode);
         iRet = Mer_Load_Roll(iSkip);      
      } else
      {
         LogMsg("***** Please specify Rollfile in [%s] section of %s", myCounty.acCntyCode, acIniFile);
         iRet = -1;
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Ama_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);

      // Format DocLinks - Doclinks are concatenate fields separated by comma 
      if (!iRet)
         iRet = updateDocLinks(Mer_MakeDocLink, myCounty.acCntyCode, iSkip, 0);
   }

   return iRet;
}