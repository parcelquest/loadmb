#ifndef  _MB_VALUE_H_
#define  _MB_VALUE_H_

int parseValues(LPSTR pInbuf, LPSTR pOutbuf, int iType=0);
int parseValues(LPSTR pInbuf, LPSTR pOutbuf, int iType, int iCnty);

int MB_ExtrBaseYearValues(LPCSTR pCnty, LPSTR pValueFile, LPSTR pOutTmpl, int iSkipRows);
int MBT_ExtrCurValues(LPCSTR pCnty, LPSTR pTaxFile, LPSTR pOutTmpl, int iSkipRows);
int MBR_ExtrCurValues(LPCSTR pCnty, LPSTR pRollFile, LPSTR pOutTmpl, int iSkipRows);

int MB_ExtrMergeValues(LPCSTR pCnty, LPSTR pValueFile, LPSTR pExeFile, LPSTR pOutFile, int iSkipRows);
int MB_ExtrMergeValues(LPCSTR pCnty, LPSTR pValueFile, LPSTR pExeFile, LPSTR pLienFile, LPSTR pOutFile, int iSkipRows);

int MB_ExtrValues(LPCSTR pCnty, LPSTR pValueFile, LPSTR pExeFile, LPSTR pOutFile, int iSkipRows);
int MB_ExtrValues(LPCSTR pCnty, LPSTR pValueFile, LPSTR pExeFile, LPSTR pLienFile, LPSTR pOutFile, int iSkipRows);
int MB_ResetPVReason(char *pBYVFile, char *pTmpFile);
int MB_FixValues(char *pInFile, char *pCnty);

int updLienValue1(LPSTR pOutbuf);
int updLienValue(LPSTR pOutbuf, bool bUpdAll);
int updExeValue(LPSTR pOutbuf, int iSkipHdr);

#endif