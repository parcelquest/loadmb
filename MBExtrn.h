#ifndef _myLoadExtrn_
#define _myLoadExtrn_
extern   char  acRollFile[_MAX_PATH], acCharFile[_MAX_PATH], acExeFile[_MAX_PATH], acTmpPath[_MAX_PATH];
extern   char  acSitusFile[_MAX_PATH], acSalesFile[_MAX_PATH], acTaxFile[_MAX_PATH];
extern   FILE  *fdLDR, *fdRoll, *fdChar, *fdSitus, *fdExe, *fdSale, *fdTax, *fdLienExt, *fdValue, *fdName, *fdXfer, *fdZone;
extern   long  lSitusMatch, lCharMatch, lExeMatch, lSaleMatch, lTaxMatch, lZoneMatch, lRollMatch;
extern   long  lGrGrMatch, lApprMatch, lValueMatch, lNameMatch;
extern   long  lSitusSkip, lCharSkip, lExeSkip, lSaleSkip, lTaxSkip, lValueSkip, lNameSkip, lZoneSkip;

extern   IDX_TBL     asSaleTypes[];
extern   XREFTBL     asDeed[];
extern   COUNTY_INFO myCounty;

extern   char  *apTokens[], cDelim, cValDelim, cLdrSep, cInZoneDelim;
extern   int   iTokens, iLoadFlag, iSaleDateFmt, iNumDeeds;
extern   long  lOptMisc;

extern   char  acIniFile[],  acRawTmpl[], acGrGrTmpl[], acEGrGrTmpl[], acESalTmpl[], acDETmpl[], acGrGrBakTmpl[],
               acLienTmpl[], acRawTmplS[], acApprFile[], acValueFile[], acCSalFile[], acSaleTmpl[],
               acNameFile[], acCSalFile[], acCChrFile[], acDocPath[], acToday[], acXferTmpl[],
               sTaxTmpl[], sTaxOutTmpl[], acValueTmpl[];
               
extern   int   iRecLen, iApnLen, iAsrRecLen, iGrGrApnLen, iLdrGrp, iLoadTax, iTaxGrp, iErrorCnt;
extern   int   iNoMatch, iBadCity, iBadSuffix, iApnFld, iHdrRows, iValHdr, iSkipQuote, iValSkipQuote, iMaxLegal;
extern   long  lRecCnt, lAssrRecCnt;
extern   long  lLastRecDate, lToday, lLastFileDate, lLastTaxFileDate, lLastGrGrDate, lProcDate, lLienYear, lToyear;
extern   bool  bDebug, bApnXlat, bFmtDoc, bClean, bCreateUpd, bFixTRA, bClearChars, bClearSales, bMergeOthers, bUpdPrevApn;
extern   bool  bSendMail, bNewTax, bGrGrAvail, bSaleImport, bGrgrImport, bTaxImport, bUnzip, bUseConfSalePrice;
extern   long  lTaxYearIdx, lTaxAmt1Idx, lTaxAmt2Idx, lTaxFlds, lLDRRecCount, lTaxYear;

int updateTaxCode(LPSTR pOutbuf, LPSTR pTaxCode, bool bProp8, bool bFullExempt);
int MB_MergeCurRoll(char *pOutbuf);

#endif