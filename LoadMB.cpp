/**************************************************************************
 *
 *  Usage: LoadMB -C<County code> [-A] [-D] [-E] [-G] [-M?] [-N] [-O] [-L?] [-U?] [-X?] [-S<n>]
 *          -A : Merge attribute files
 *          -D : Debug mode, output more detail msg
 *          -Dn: Do not generate update file .CHK
 *          -Dc: Create update file .UPD for debug
 *          -Dr: Don't remove temp file
 *          -E : Email on error
 *          -Fc: Remove characteristics before update
 *          -Fd: Fix DOCNUM in sale history file
 *          -Ft: Fix TRA
 *          -M : Merge GrGr file (for testing only)
 *          -Ma: Merge attribute/characteristics
 *          -Mg: Merge GrGr
 *          -Mo: Merge other values
 *          -Mp: Merge public parcel
 *          -Ms: Merge cummulative sale
 *          -Mz: Merge zoning file
 *          -L : Load Lien date roll (create lien file and load roll).
 *          -Lyyyymmdd: load data for specific date
 *          -N : Do not encode suffix and city
 *          -O : Overwrite logfile (default append)
 *          -Ps: Remove old sale in R01 before update
 *          -T : Load tax file
 *          -U : Update roll file using old S01 file
 *          -Up: Update previous APN field
 *          -Us: Update sale history
 *          -X8: Extract prop8.
 *          -Xa: Extract attribute from attr file.
 *          -Xc: Extract cumulative sale from mainframe sale file.
 *          -Xg: Extract GrGr data.
 *          -Xl: Extract lien value from lien file.
 *          -Xs[i]: Extract sale data [and import to SQL].
 *          -Xv[i]: Extract value [for import into SQL]
 *          -G[i] : Load and create GrGr file [for import]
 *          -Sn: Number of records skip (default 1)
 *
 * History:
 * 08/30/2005 1.0.0    Copy from LoadOne and start with BUT
 * 10/20/2005 1.0.3    Fix Mailing StrNum bug that displays 7.
 * 12/09/2005 1.2.8    Output new and retured APN to file to later apply change
 *                     to the GIS or assessor map.
 * 04/02/2006 1.3.12 - Change made due to SON_CHAR format change.
 * 05/16/2006 1.2.13 - Reset automation flag State='W' before processing and set
 *                     it to 'P' if successful, 'F' if fail
 * 07/25/2007 1.9.9    Add MB_CreateSale() to convert csv to dat (SALE_REC format).
 *                     Adding acSaleTmpl and acCSalFile.
 * 04/17/2008 1.10.7   Add -Nu option to skip creating update file
 * 05/06/2008 1.10.9.1 Modify MB_ExtrLien() and MB_CreateLienRec() to make it work
 *                     with AMA.  It should work with default MB lien file.
 * 05/29/2008 1.11     Update county table LastRecDate, LastFileDate (roll), and LastGrGrDate.
 * 06/09/2008 1.12     Add email option.
 *
 * 06/25/2008 8.1.0    Reversioning load program and use two digit year as major version.
 *                     Add new function MB_ExtrTR601() to extract lien value from
 *                     TR601 format.  TR601 format is tab delimited and may contain null characters.
 * 07/18/2008 8.2.0    Adding second level usecode translation and fix bug that
 *                     causes error open roll file
 * 08/07/2008 8.2.5    Fix MB_ExtrLien() to get lien file from [Countycode] instead of [Data]
 * 10/13/2008 8.4.0    Verify record count.
 * 12/10/2008 8.5.0    Set County.Status='W' when process a county.  This prevents 
 *                     ChkCnty program from running it in parallel.  
 * 01/03/2009 8.5.2    Do not set file date.  This will be done in ChkCnty.
 * 03/02/2009 8.6      Add MB_MergeSale() to replace MergeSale3() from SaleRec.cpp
 *                     Add MB_ConvChar(), cDelim, and acCChrFile.
 * 03/27/2009 8.7      Adding -Fs to fix sale data problem (i.e. clear old sales)
 * 03/30/2009 8.7.1    Modify MB_MergeSale() to clear out sale price on Prop 58 doc.
 * 04/02/2009 8.7.3    Modify MB_ConvChar() to overwrite input with output fle name
 *                     to make it compatible with convertChar().
 * 06/08/2009 8.7.7    Add SkipQuote and remove quotes in buyer/seller names input in
 *                     MB_CreateSale().
 * 06/16/2009 8.7.8.4  Modify MB_ConvChar() to bypass sorting if input file is empty.
 * 06/26/2009 9.1.0    Add Prop 8 flag to MB_ExtrTR601Rec() and replace replChar()
 *                     with replNull() in MB_ExtrTR601().
 * 07/24/2009 9.1.3    Modify MB_ConvChar() to support multi-format.
 * 07/29/2009 9.1.4    Modify MB_CreateLienRec() and MB_ExtrTR601Rec() to store
 *                     PP_MH as Personal Property and PP as Business Property.
 * 01/16/2010 9.3.0    Fix MB?_ExtrTR601() and MB_CreateLienRec() to check Prop8 based 
 *                     on TAXABILITY field, not ASMT.  Create MB_ExtrProp8Ldr() to extract
 *                     Prop8 flag from LDR file.  Add option -X8 to extract prop8.
 * 01/18/2010 9.3.1    Add LastRecsChg to County table and update it after compare.
 * 02/19/2010 9.4.0    Add lTaxYearIdx, lTaxAmt1Idx, lTaxAmt2Idx, lTaxFlds.
 * 02/24/2010 9.5.0    Add default sendmail flag.  This can be overide by command line option.
 * 06/25/2010 10.0.0   Add lLDRRecCount to update number of LDR records received from county.
 *                     Stop using sale data to update transfer.
 * 07/05/2010 10.0.1   Modify MB_MergeSale() to allow update TRANSFER.  Fix bug in MergeGrGrFile1().
 * 07/18/2010 10.1.0   Modify MB_CreateLienRec(), MB1_ExtrTR601Rec(), and MB2_ExtrTR601Rec() to
 *                     save TAXIBILITY in LIENEXTR.  Add MB_CreateSCSale() to extract sale to
 *                     SCSAL_REC format.  Bypass record count check when loading LDR.
 * 08/02/2010 10.3.0   Modify LoadCountyInfo() to support multiple special format. 
 *                     Log max legal length for easy tracking.
 * 11/01/2010 10.3.6   Add IUseTbl to county option in INI file. This is numeric version of UseCode
 *                     table which is new to MNO.
 * 11/01/2010 10.4.0   Adding processing date to log file.
 * 11/29/2010 10.5.0   Add new function updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags.
 * 01/15/2011 10.5.2   Add -Y option to overwrite LDR year.
 * 03/25/2011 10.6.0   Add bGrGrAvail and standardize sending email when GrGr is not available.
 * 04/15/2011 10.7.1   Add new MB_ExtrProp8() to support counties that use CSV format.
 *                     Modify MB_CreateSCSale() to create .sls file if not exist.
 * 05/01/2011 10.8.0   Modify Prop8 extract to handle both CSV and TAB file format.
 * 05/18/2011 10.8.1.2 Fix bug in MB_CreateSCSale() to return 0 if success.
 * 06/02/2011 10.9.0   Add MB_MergeExe() and MB_MergeTax() to replace county specific ones.
 *                     Modify MB_CreateSCSale() to detect date format if not specified.
 * 06/10/2011 10.9.1   Modify MB_CreateSCSale(), MB_MergeExe(), and MB_MergeTax() to work with
 *                     all MegaByte counties.
 * 06/13/2011 10.10.1  Modify MB_MergeTax() to support multi tax record for single APN
 *                     in a same year (CAL) by checking ChargeDate and PaidDate. Newer ChargeDate
 *                     or PaidDate means newer record.
 * 06/29/2011 11.0.0   Modify MB_CreateSCSale() to get more accurate sale price by ignoring
 *                     any sale price < 10000.
 * 07/12/2011 11.1.3   Change log file name for LDR processing.
 * 08/08/2011 11.2.5   Modify MergeGrGrFile1() to merge sale only if there is sale price.
 * 08/18/2011 11.3.0   Modify MB_CreateSCSale() adding DocTypeFmt=2, special case for PLA.
 *                     Also put out more msg about questionable tax amt.
 * 08/29/2011 11.3.1.2 Add MB_MergeTaxG2() to support LAK & SHA with new tax format.
 *                     Modify MB_CreateSCSale() to sort output file including sale price desc.
 * 09/01/2011 11.3.2   Increase buffer in MB_MergeTax() to avoid buffer overrun.
 *                     Modify MB_CreateSCSale() to support SHA.
 * 09/29/2011 11.6.5   Extract sales for import into spatial db on -Xsi after county processing is done.
 * 10/20/2011 11.7.5   Modify MB_CreateSCSale() to drop records without Recording Date.
 *                     Also changing sort order to put sale price before DocNum.
 * 11/09/2011 11.8.5   Add support to load Tax data. Clean up MB_ExtrProp8Ldr().
 *                     Add -T option and MB_Load_TaxRollInfo() to load Tax file.
 *                     Add doSaleImport() & doTaxImport() to do bulk import into SQL.
 *                     These functions will call spSalePrep or spPrep_TRI to verify table existence
 *                     before import.  Existing data will be truncated.
 * 01/01/2012 11.9.1   Rename MB_ParseTRI() to MB_ParseTaxBase() and use TAXBASE instead of TAXMSTR.
 *                     Add functions to load Redemption, TaxCode, and TaxCodeMstr files.
 * 01/20/2012 11.10.1  Add MB_MergeExe2() to replace MB_MergeExe().
 * 01/30/2012 11.11.0  Allow loading LDR for prior year.
 * 03/12/2012 11.12.0  Copy doSaleImport() from LoadOne.cpp over to import GrGr data.
 * 03/25/2012 11.13.0  Add -Mt option to merge transfer into cum sale file.
 *                     Add MB_UpdateXfer() to extract transfers from roll file and append to xfer file.
 * 03/31/2012          Modify MB_UpdateXfer() to output fixed length APN and remove
 *                     records with blank DOCDATE or DOCNUM.
 * 05/21/2012 11.14.3  Rename MB_ParseTaxBase() to MB_ParseTaxTRI() to parse TaxRollInfo.txt and 
 *                     modify MB_ParseTaxBase() to parse tax record from ???_Tax.txt file.  Add
 *                     MB_LoadTaxBase() to load ???_Tax.txt.
 * 05/22/2012 11.15.0  Modify MB_Load_Tax*() to allow passing header row count since counties are inconsistent
 *.                    in attaching header to data files (HUM).
 * 05/24/2012 11.15.1  Blank out TAX_CODE in updateTaxCode() when TaxCode=0.
 *                     Add MB1_CreateSCSale() to load sale file w/o ConfirmSalePrice (Hum_Sales.csv).
 * 06/25/2012 12.1.1   Add option -Dc to allow create UPD file for debug.  Replace -Un with -Dn.
 * 07/09/2012 12.1.2   Modify MB_CreateSCSale() to keep DocNum format the same if it includes '-' after first 5 digits.
 * 07/18/2012 12.2.2   Call PQ_ChkBadR01() to check for bad character if load successful.
 *                     Add MB_ExtrZoning(), fdZone, lZoneMatch, and lZoneSkip.
 * 09/02/2012 12.3.0   Modify MB_CreateSCSale() to add DocNum format for COL.
 * 10/11/2012 12.3.3   Add LoadImp().
 * 10/13/2012 12.4.0   Add option -Gi to import GRGR data to SQL table.
 * 10/19/2012 12.4.1   Add -Fd option to fix DOCNUM in CSALE file.  Modify MB_CreateSCSale()
 *                     adding DocNumFmt=3 to support SON.
 * 01/02/2013 12.6.0   Update DocLink to sale history table.
 * 05/22/2013 12.9.0   Modify MB_CreateSCSale() to add optional DocTbl for DocType translation
 * 07/17/2013 13.2.5   Modify MB_CreateSCSale() to add DocTypeFmt=4 for SON.
 * 07/22/2013 13.4.7   Adding option to create unknown Usecode file.
 * 07/25/2013 13.7.7   Modify MB_CreateSCSale() to assign DocType using DocCode table if available.
 *                     Otherwise use old method.
 * 07/29/2013 13.8.8   Create flg file on successful.
 * 08/06/2013 13.8.10  Add -Xv option to extract value file for SQL import.
 * 08/26/2013 13.10.13 Modify MB_MergeTax() & MB_MergeTaxG2() to check for RollChg and set flag to 'Y'
 *            13.11.13 Add -Dr option and auto remove temp files after successful processing.
 * 09/04/2013 13.12.1  Extract base year value one time at LDR. Add MB_MerggeTaxRC()
 *                     to update tax and values if roll change found.
 * 09/30/2013 13.13.0  Call LoadVestings() to populate Vesting table.
 * 10/02/2013 13.15.0  Replace loadVestingTbls() with loadVesting() and use combined vesting file.
 * 10/09/2013 13.16.0  Add MB1_StdChar() & MB_MergeStdChar()
 * 10/17/2013 13.17.2   Modify MB_MergeStdChar() to fix Quality code problem.
 *            13.17.2.1 Modify MB_MergeStdChar() set ParkType='Z' instead of '1'.
 * 10/23/2013 13.17.4  Add -Z option for unzip.
 * 10/31/2013 13.18.0  Add MB1_ConvStdChar() that encoded Heating, Cooling, and Pool if table provided.  
 *                     This function can be used to replace MB1_StdChar().
 * 11/08/2013 13.18.1  Add MB_ConvStdChar() to create standard char file for counties using MB_* structure (BUT)
 *                     Modify MB1_ConvStdChar() to take paQual as parameter.  Modify MB_MergeStdChar()
 *                     to use data as is, use the first matched record, not sum up in case multi-parcel.
 * 11/27/2013 13.18.3  Use standard local path for document access.
 * 01/08/2014 13.19.0  Modify MB_CreateSCSale() to change the way of choosing sale price between CP and
 *                     DocTax.  Also adding option -Fx to fix DocType in cumsale file.
 *                     Modify MB_MergeSale() not to merge non-sale transaction.
 * 01/18/2014 13.20.0  Fix bug that program doesn't create diff file .chk which in turn didn't update SQL.
 *                     This occurs when -Xv option is used.
 * 02/23/2014 13.20.4  If there is no change, do not update Product table. This in turn
 *                     won't trigger the build index process.
 * 02/28/2014 13.21.0  Change logic of update and post processing to fix bad char before copy to
 *                     AsrFile and only do it if loading LDR or roll update. Also move GRGR check
 *                     outside of loading block to make sure email is sent when GRGR data not available.
 * 03/04/2014 13.22.0  Now import GRGR data to SQL using SCSAL_REC format ???_Grgr.sls file.
 * 04/17/2014 13.23.0  Move Load Value to county module since each county have different data set.
 * 04/18/2014 13.24.0  Move MergeGrGrFile1() to doGrgr.cpp
 * 06/16/2014 14.0.1   Add MB_MergeCurRoll() to update transfers (MER).
 * 07/26/2014 14.0.6   Modify MB_ExtrTR601() to add option to check last char in the record.
 * 09/11/2014 14.4.0   Modify MB_ExtrTR601() to support MON 2014 LDR rerun.
 * 09/15/2014 14.4.1   Add doValueImport() to import Value File to Pcv_Values table.
 *                     Replace EXTR_GRGR with EXTR_IGRGR. Add -Xvi for value import.
 *                     Add runSP() to execute a store procedure.
 * 09/19/2014 14.4.2.1 Change email subject for import value and log msg.
 * 11/04/2014 14.8.0   Modify MB_ConvChar() to avoid memory overun.
 * 12/03/2014 14.10.0  Add updateDocLinks() to replace But_FmtDocLinks().
 *							  Modify doValueImport() to support remote SQL server.
 *                     Add -G[yyyymmdd] option to process GrGr daily on specific date (MNO)
 * 12/10/2014 14.11.0  Add special handling to import GRGR of BUT and MNO in GRGR_DEF format.
 * 01/31/2015 14.12.0  Use See4c 7.0 to support Office365 email. Add option to use [Machine]
 *                     and [CO_DATA] token in INI file.
 * 02/04/2015 14.12.1  Replace GetPrivateProfileString() with GetIniString() which will 
 *                     translate [Machine] to local machine name and [CO_DATA] with
 *                     CO_DATA token defined in INI file.  
 * 02/09/2015 14.12.2  Add global lTaxabilityIdx and modify MB_MergeTaxG2() to update Tax code.
 * 02/12/2015 14.13.0  Modify MB_CreateSCSale() to use ConfirmSalePrice if DocTax not available.
 * 02/13/2015 14.14.0  Take out ConfirmSalePrice since it's confidential.
 * 03/13/2015 14.15.0  Remove updateDocLinks().
 * 06/11/2015 14.17.0  Fix bug that overwrite the value of acValueFile in MergeInit().
 * 06/18/2015 15.0.1   Modify MB_CreateSCSale() to use SalePrice if DocTax is questionable - SON
 * 09/11/2015 15.3.0   Add -Mr option to merge area data.
 * 10/13/2015 15.4.0   Add MB_ParseTaxBaseG1() and MB_ParseTaxBaseG2() to replace MB_ParseTaxBase()
 *                     to add TRA, Owner and mailing info.  Modify MB_Load_TaxBase()  to import
 *                     Owner info to _Tax_Owner table.
 * 10/15/2015 15.4.1   Change Owner to Name1 in MB_ParseTaxBase 1 & 2 since TAXOWNER is changed.
 * 10/23/2015 15.4.3   Add support for loading TEH converting from CRES to MB.
 *                     Modify MB_ParseTaxBaseG2() to check for '%' in mailaddr for CareOf.
 *                     Remove NULL from input string in all MB_Load_Tax*().
 * 11/03/2015 15.5.0   Modify MB_CreateSCSale() to use ConfSalePrice if DocTax not available.
 * 12/03/2015 15.8.0   Modify MB_CreateSCSale() to send email when input file is bad.
 * 01/04/2016 15.9.0   Add Load_TC() to Tax.cpp to process TC files.
 * 03/07/2016 15.13.0  Rename "Detail" to "Items" in MB_Load_TaxCode() which effectively 
 *                     create tax output Tax_Items.csv instead of Tax_Detail.csv
 * 03/09/2016 15.14.0  Automatically update last roll file date unless specify SetFileDate=N under county section.
 *                     Add lLastTaxFileDate.
 * 03/22/2016 15.14.2  Add GLE.
 * 04/07/2016 15.14.4  Add MB_Load_TR601Owner() to extract owner info from TR601 for Tax purpose.
 * 04/11/2016 15.15.0  Fix bug in MB_Load_TaxBase() by initialize fdOwner when not used.
 * 06/24/2016 16.0.0   Modify MB_ExtrTR601() and add MB3_ExtrTR601Rec() to support L3_ layout (MER)
 * 07/01/2016 16.0.1   Fix sorting bug in MB_ExtrTR601() when LdgGrp=3.
 * 07/08/2016 16.0.3   Remove -La & -Ua option.  No longer support CDASSR
 * 07/13/2016 16.0.4   Modify MB_ExtrTR601 & MB3_ExtrTR601Rec() to remove hyphen in ASMT.
 *                     Add MB_ExtrTC601() & MB_ExtrTC601Rec() to load TC601 file.
 * 07/28/2016 16.1.0   Turn ON option to import value. Modify doValueImoprt() to make it work 
 *                     with shared functions. Add SqlExt.cpp to share runSP() and execSqlCmd()
 * 08/04/2016 16.2.0   Add MB3_ParseOwnerRec() and modify MB_Load_TR601Owner() to support new LDR layout
 *                     Add default date and delqYear to MB_ParseTaxBaseG1() & MB_ParseTaxBaseG2().
 *                     Add TaxAmt to MB_ParseTaxAgency().  Remove MB_Load_TaxRollInfo()
 * 08/10/2016 16.3.0   Add MB_UpdateDelqYear() to update DelqYear in Tax_Base table. Add MB4_ExtrTR601Rec()
 *                     to parse new L4_ LDR record. Modify MB_ExtrTR601() to support L4 format.
 *                     Modify all ParseTaxBase() functions to take only current lien year record.
 *                     Modify MB_Load_TaxBase() to use MB_UpdateDelqYear() when Co3_Tax.txt doesn't include DelqYear.
 * 08/14/2016          Add MB_MergeTaxG1() to merge tax for group 1 counties (AMA).
 *                     Modify MergeIni() to load TaxGrp from INI file and update tax field indexes.
 * 08/15/2016 16.4.0   Add iErrorCnt to capture some data parsing error and email to developer.
 * 08/17/2016 16.4.0.2 Modify MB_ParseTaxBaseG1(), MB_ParseTaxBaseG2(). Add DefaultNum to TAXDELQ in MB_ParseTaxDelq()
 * 08/19/2016 16.4.1   Add -Up option to allow update PREV_APN using last year file. (GLE & TEH)
 * 08/29/2016 16.4.2   Modify MB3_ExtrTR601Rec() to remove hyphen from TRA before update.
 * 09/15/2016 16.4.3   Modify MergeIni() to initialize iTaxYear.
 * 10/07/2016 16.5.0   Force UNASSIGNED if UseCode is empty.  Change DefaultNo to Default_No in MB_ParseTaxDelq().
 * 10/31/2016 16.6.1   Add lTaxYear as global.  Set DelqStatus[0] = '1' when there is RedeemDate
 * 11/01/2016 16.6.2   Modify MB_MergeStdChar() to set output to blank when data is not available.
 * 11/24/2016 16.7.0   Modify MB1_CreateSCSale() to set Full/Partial and Multi Parcel Transfer
 * 12/01/2016 16.7.2   Add PatioSqft in MB_MergeStdChar().
 * 12/29/2016 16.7.4   Add MOD
 * 01/05/2017 16.8.0   Modify MB_Load_TaxBase() to add DateFmt since each county may use different format.
 * 01/06/2017 16.8.0.2 Modify all MB_ParseTaxBase() to create due date since county files doesn't include.
 * 01/09/2017 16.9.0.1 Modify MB_ParseTaxBase*() to populate Penalty only if it is past due and update TotalDue.
 * 02/07/2017 16.9.1   Modify MB_CreateSCSale() to drop all records without DocNum or DocDate, adding DocNum type 5 
 *                     to process HUM sale ignoring all bad DocNum.  Modify MB_ParseTaxBase*() to check for tax amt
 *                     with '-' and '$' and ignoring "UNSEC" records. If installment is negative, don't set due date 
 *                     or due amt. Fix MB_ParseTaxDelq() to add "00" to APN if APN length is less than defined (GLE) and  
 *                     also set DelqStatus.
 * 02/27/2017 16.9.3   Add log msg to MB_Load_TaxBase()
 *                     Fix doValueImport() by using BulkInsert instead of BulkImport procedure.
 * 03/01/2017 16.9.4   Add DNX
 * 03/04/2017 16.10.0  Modify MB_ParseTaxBase*() to remove BillNum since it Tax_Items doesn't have matching BillNum
 * 03/08/2017 16.11.0  Modify MB_ParseTaxBase*() to update TotalDue that combines all taxes and penalties.
 *                     Also fix DueDate problem on MB_ParseTaxBaseG*().
 * 03/09/2017 16.12.0  Modify MB_ParseTaxBase*() to set DueDate only if not paid.
 * 03/15/2017 16.13.1  Modify MB_Load_TaxCodeMstr() to support loading CO3_TaxDist.txt
 *                     Modify MB_ParseTaxAgency() to format phone number.
 * 03/24/2017 16.14.0  Modify MB_Load_TaxBase() to check for new tax file before processing.
 * 03/31/2017 16.14.2  Modify MB_Load_TaxBase() to return actual error code from isNewTaxFile() when <= 0
 * 04/06/2017 16.14.4  Modify MB_ParseTaxBaseG2() to update PenAmt if paid after due date.
 * 05/16/2017 16.15.5  Add TUO.  Modify MB_MergeTaxG2() to support TUO.
 * 05/21/2017 16.16.0  Fix TotalDue in MB_ParseTaxBase*() when 2nd inst past due.
 * 06/14/2017 16.18.3  Modify MB_ParseTaxBaseG1() to set BillNum=Asmt and format APN using 
 *                     the first 9 digits of Asmt+000.  Add new MB_ParseTaxDetailG1().
 *                     Modify MB_Load_TaxCode() to support special case in AMA.
 * 06/16/2017 16.19.0  Modify MB_Load_TaxCode() to require flag to update BillNum using info from TaxBase.
 *                     Modify MB_ParseTaxBaseG1() & MB_ParseTaxDetailG1() to conditionally set APN for corrected bill.
 * 06/27/2017 17.0.2   Add MB_ParseTaxBaseG3() to be used with MOD
 * 06/30/2017 17.1.0   Do not set county status when loading tax alone to avoid messing up production automation.
 * 07/02/2017 17.1.1   Modify MB_ParseTaxDelq() to set TaxYear base on DefaultDate.
 * 07/12/2017 17.1.3.1 Set Status=R before exit except when loading tax.
 * 07/19/2017 17.1.5   Modify MB_ExtrTC601Rec() to use delimiter from cLdrSep instead of hardcode tab char.
 *                     Modify MB_ExtrTC601() to pass delimiter on sort command.
 * 07/23/2017 17.2.0   Reset Status=R after loading GrGr regardless of success or failure.
 * 08/03/2017 17.2.4   Add MB_FindTotalDelqAmt() & MB_Load_TaxDelq() to populate Default Amt with total 
 *                     amount owed from Tax file insteadl of default amt from Redemption file.
 * 08/09/2017 17.2.5   Add MB5_ExtrTR601Rec() for MNO.
 * 02/23/2018 17.6.0   SJX conversion
 * 03/23/2018 17.7.1   Add -Ps option to purge old sales.  This will set bClearSales=true.
 *            17.7.1.1 Fix import sale problem when roll file has no change.
 * 03/26/2018 17.8.0   Add -Lt & -Ut to load or update tax tables.
 * 03/27/2018 17.9.0   Output to appropriate log file.
 * 04/08/2018 17.9.0.4 Modify MergeInit() to set lTaxYear.
 * 04/10/2018 17.10.1  Add acGrGrBakTmpl[] to standardize GrGr backup folder.
 * 04/11/2018 17.10.2  Modify MB_MergeTax() to remove null and quotes.  Modify MB_CreateSCSale()
 *                     to remove null char to fix bad records.
 * 04/25/2018 17.11.4  Modify MB_ParseTaxBase() & MB_ParseTaxBaseG2() to add option to feeParcel as BillNum
 * 05/03/2018 17.12.0  Modify MB_Load_TaxBase() to add TRA.
 * 05/07/2018 17.13.0  Modify MB_ParseTaxBaseG1() & MB_ParseTaxBaseG2() to add Cost to TotalDue.
 * 06/17/2018 17.16.0  Add EDX & NEV
 * 06/28/2018 18.0.0   Modify MB_ExtrTC601Rec() to handle APN format for AMA.
 * 07/09/2018 18.1.1   Modify MB_ExtrTC601() to add option skip sorting input file.
 * 07/20/2018 18.2.0   Use TaxYear defined in INI as base year for loading because tax file is
 *                     always behind roll file.  Default for TaxYear is LienYear when not defined.
 *            18.2.1   Add -Tyyyy option to allow loading tax data for specified year.
 * 07/31/2018 18.2.2   Fix bug in ParseCmd() on -T option.
 * 08/02/2018 18.2.3   Modify MB_ExtrTR601() to filter out blank APN record.
 * 08/07/2018 18.2.4   Add MB8_ExtrTR601Rec() and modify MB_ExtrTR601() for 2018 MNO LDR.
 * 08/18/2018 18.3.0   Remove -Mt and add -Mz to merge zoning
 * 09/03/2018 18.4.1   Add -Lz to load zipcode file.
 * 09/04/2018 18.5.0   Modify MB_Load_TaxBase() to skip import and return 1 if no tax value.
 * 09/11/2018 18.5.1   Add MB_Update_TaxPaid() to update tax paid only.
 * 09/29/2018 18.5.4   Add -Mr option to merge lot area
 * 10/03/2018 18.6.0   Clean up DocNum in MB_CreateSCSale() for AMA
 * 10/23/2018 18.7.0   Fix null char in MB_Load_TaxBase() for NEV
 * 11/04/2018 18.7.3   Remove unused option -Us and add -Mn option to merge sale data from NDC for DNX.
 *                     Fix problem when loading sale without merge, the status in county table doesn't get reset.
 * 12/06/2018 18.8.0   Add -Te option for extracting special parcels from tax file
 * 12/11/2018 18.9.0   Modify MB_ParseTaxBase*() to initialize Inst?Status.
 * 02/09/2019 18.10.1  Add MB_LoadZoning() to merge zoning data from Denise.
 * 02/14/2019 18.10.2  Add option -Nm (don't send mail)
 * 02/28/2019 18.10.3  Add SIE.  Remove NULL char in MB_MergeTaxG2()
 * 03/30/2019 18.11.0  Modify MB_MergeZoneEx() to populate OFF_ZONE_X1 & X1 when ZONING is too long
 *                     Adding OFF_ZONE_X1 & OFF_ZONE_X2
 * 08/04/2019 19.1.2   Modify MB_CreateSCSale() to drop all transaction where DocNum begin with '1900R'.
 * 09/12/2019 19.2.5   Add ALP
 * 10/03/2019 19.4.0   Turn iLoadFlag to global and remove iLoadFlag as parameter in loadXXX()
 * 10/25/2019 19.4.2   Add MB_ParseTaxBaseG3() and modify MB_Load_TaxBase() to support new ALP layout
 * 10/31/2019 19.5.0.1 Modify MB_Load_TaxCode() to update tb_TotalRate if import Tax_Items successfully.
 * 11/13/2019 19.6.0   Update LDRRecCount in County table using lRecCnt which includes public parcels.
 *                     lLDRRecCount is only residential.
 * 03/25/2020 19.8.0   Modify MB_LoadZoning() to remove Zoning before apply new one.
 * 03/26/2020 19.8.0.1 Remove MB_LoadZoning() and add MergeZoning.cpp
 * 04/03/2020 19.8.1   Add -Xn option for loading NDC sale data (DNX).
 * 04/09/2020 19.8.3   Add "PhoneTech" to [Mail] section.
 * 04/27/2020 19.9.0   Remove -Ut and use -T to load both full & partial tax file.
 *            19.9.0.1 Keep -Ut
 * 07/05/2020 20.1.1   Remove -Xvi and move import value option to UV<n>.CMD 
 *                     Add doSaleImportEx() to replace doSaleImport() where the new function support both Windows & Linux.
 * 08/10/2020 20.2.6   Add INY & TRI
 * 08/19/2020 20.3.0   Modify MB_ParseTaxBaseG1() to fix double charge COST to TotalDue.
 * 10/16/2020 20.4.0   Now process -Mz in _tmain() after loading roll file without error.
 * 01/22/2021 20.6.0   Add ORG_DocCode[] to bypass compile error in SaleRec.cpp
 *                     Modify MB_ParseTaxBase() to ignore records with RollChgNum > 0 to avoid duplicate.
 * 02/09/2021 20.6.1   Add LAS
 * 02/11/2021 20.7.0   Add iSaleDateFmt to support SaleDateFmt setting in INI file for county specific.
 * 02/14/2021 20.7.1   Change INI item from "SqlSalesFile" to "SqlSaleFile" in doSaleImportEx()
 * 02/17/2021 20.7.2   Fix bug in doSaleImportEx() when importing sale to Linux server.
 * 02/23/2021 20.7.4   Modify MB_CreateSCSale() to change the way we skip hdr records.
 * 02/24/2021 20.7.5   Fix bug in MB_CreateSCSale() - skip quote when check for hdr record.
 * 03/10/2021 20.8.0   Add MPA
 * 05/20/2021 20.9.0   Set County.State='W' when merge sale or GrGr alone to avoid other program from working on it.
 * 06/22/2021 21.0.0   Modify MB_ParseTaxBase*() to to process tax file during LDR season while loading 
 *                     current roll year and previous tax year.
 * 07/20/2021 21.1.0   Create -D option to turn ON debug mode.  This overwrites option in INI file.
 *                     Initialize bDebug in ParseCmd() and check if not set in MergeInit().
 * 08/09/2021 21.1.7   Modify MB_MergeStdChar() to populate QualityClass.
 * 08/19/2021 21.1.9   Add MergeKin.cpp
 * 08/26/2021 21.2.0   Default bUseConfSalePrice=false so we don't use confidential sale price.
 * 10/14/2021 21.2.8   Add cInZoneDelim to specify delimiter within PQZONING.
 * 10/22/2021 21.3.0   Add MergeIny.cpp
 * 11/17/2021 21.3.1   Remove old UPD file when -Dc is used.
 * 01/15/2022 21.5.0   Add -I[s|g] to import sale or grgr
 * 03/05/2022 21.5.1   Add acValueTmpl[]
 * 03/12/2022 21.6.1   Compare S01 vs R01 when -Mz is used.
 * 04/13/2022 21.8.0   Reset county status=R when GRGR or Daily file not available.
 * 04/15/2022 21.8.1   Add UseConfSalePrice to county section in INI file to initialize bUseConfSalePrice.
 * 04/25/2022 21.9.0   Allow setting county specific backup folder for GrGr files. If county not defined, use default.
 * 04/30/2022 21.9.1   Add MEN & SUT
 * 07/14/2022 22.0.2   Add new global cValDelim, iValHdr, iValSkipQuote
 * 09/10/2022 22.2.2   Fix DueDate & InstStatus in MB_ParseTaxBaseG1().  Only affect AMA.
 * 11/11/2022 22.3.0   Add -Pz option to remove PQZoning.
 * 11/17/2022 22.4.0   Add -Xf & -Mf option to extract/merge final value file and create .V01 file.
 * 12/07/2022 22.5.0   Modify MB_MergeExe2() & MB_CreateSCSale() to stop updating when record length is smaller than 14.
 *                     Add TUL.
 * 01/31/2023 22.6.0   Modify -Ps option to remove all sale & transfer from R01/S01 before loading county.
 * 02/15/2023 22.6.8   Update asSaleTypes[] by adding unknown type to transfer type code.
 * 03/25/2023 22.7.0   Add -Ext option to export SQL command for tax update.
 *                     Add sTaxSqlTmpl, bExpSql.  Modify MergeInit() to load sTaxSqlTmpl
 * 04/19/2023 22.8.0   Modify -Ext option to create sql command file for blob storage when BRoot is defined.
 * 05/05/2023 22.8.2   Use MaxUpdate in county table for iMaxChgAllowed.
 * 07/01/2023 23.0.3   Modify MergeInit() to fix ValDelim when it was a tab.
 * 07/10/2023 23.0.5   Modify MB_ExtrTR601() to support lender group 31 (AMA).
 * 07/22/2023 23.0.6   Add MB17_ExtrTR601Rec() and modify MB_ExtrTR601() to support lender group 17 (YOL).
 * 10/18/2023 23.4.0   Modify MB_Load_TaxBase(), MB_Load_TaxCode(), MB_Load_TaxCodeMstr(), MB_UpdateDelqYear()
 *                     to skip header records automatically, not relying on passing parameter.
 *                     Modify MB_ParseTaxBase*() & MB_ParseTaxDetail*() to process current tax year bills only.  
 *                     Populate BillNum using FeeParcel only if UseFeePrcl=Y.
 * 01/03/2024 23.5.0   Modify MB?_ExtrTR601Rec() to put PP_MH and FixtRP in the correct field.
 * 03/20/2024 23.7.1   Make lProcDate default to lToday.  It can be changed by -LyyyyMMdd option.
 * 03/23/2024 23.7.3   Modify MB_MergeStdChar() to update LotAcre/LotSqft if available.
 * 06/01/2024 23.9.0   If there is error accessing city file, stop program immediately.
 * 08/10/2024 24.1.0   Load Jurisdiction table when -Mz is used.
 * 10/22/2024 24.2.0   Set default UseFeePrcl=N.  This can be changed in INI file.
 * 10/30/2024 24.3.1   Change PQ_RemoveQZoning() to PQ_RemovePQZoning().
 * 11/21/2024 24.4.0   Modify MB_ParseTaxBase() to take current year tax bill only.
 * 01/22/2025 24.5.0   Modify MB_ParseTaxBaseG2() to ignore ROLL CHANGED record.  Keep one bill only.
 *
 **************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "Logs.h"
#include "getopt.h" 
#include "CountyInfo.h"
#include "R01.h"
#include "PQ.h"
#include "RecDef.h"
#include "FormatApn.h"
#include "Tables.h"
#include "SaleRec.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "Usecode.h"
#include "LoadMB.h"
#include "Update.h"
#include "CharRec.h"
#include "MB_TaxInfo.h"
#include "Tax.h"
#include "LoadValue.h"
#include "Sendmail.h"
#include "SqlExt.h"
#include "MergeZoning.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CWinApp theApp;
using namespace std;

XREFTBL  asDeed[MAX_DEED_ENTRIES];
int      iNumDeeds;
XREFTBL  asInst[MAX_INST_ENTRIES];
int      iNumInst;
XREFTBL  asTRA[MAX_TRA_ENTRIES];
int      iNumTRA;
APN_RNG  asExclApns[2];

char     *apTokens[MAX_FLD_TOKEN], cDelim, cValDelim, cLdrSep, cInZoneDelim;
int      iTokens;
char     acLookupTbl[_MAX_PATH], acToday[16];

char     acRawTmpl[_MAX_PATH], acRawTmplS[_MAX_PATH], acApprFile[_MAX_PATH], acGrGrTmpl[_MAX_PATH], 
         acEGrGrTmpl[_MAX_PATH], acGrGrBakTmpl[_MAX_PATH],          
         acDETmpl[_MAX_PATH], acSaleTmpl[_MAX_PATH], acESalTmpl[_MAX_PATH], acAttrTmpl[_MAX_PATH],
         acLogFile[_MAX_PATH], acIniFile[_MAX_PATH], acCntyTbl[_MAX_PATH], acLienTmpl[_MAX_PATH],
         acRollFile[_MAX_PATH], acCharFile[_MAX_PATH], acExeFile[_MAX_PATH],
         acSitusFile[_MAX_PATH], acSalesFile[_MAX_PATH], acCSalFile[_MAX_PATH], acTaxFile[_MAX_PATH],
         acValueFile[_MAX_PATH], acNameFile[_MAX_PATH], acTmpPath[_MAX_PATH], acUseTbl[_MAX_PATH],
         acDocPath[_MAX_PATH], acCChrFile[_MAX_PATH], acXferTmpl[_MAX_PATH],
         sTCTmpl[_MAX_PATH], sTCMTmpl[_MAX_PATH], sTaxTmpl[_MAX_PATH], sRedTmpl[_MAX_PATH], acValueTmpl[_MAX_PATH],
         sTRITmpl[_MAX_PATH], sTFTmpl[_MAX_PATH], sTaxOutTmpl[_MAX_PATH], sTaxSqlTmpl[_MAX_PATH], sImportLogTmpl[_MAX_PATH];

FILE     *fdGrGr, *fdLDR, *fdRoll, *fdChar, *fdSitus, *fdExe, *fdSale, *fdCSale, *fdTax, *fdName, *fdValue, 
         *fdLienExt, *fdXfer, *fdDelq;

int      iRecLen, iAsrRecLen, iApnLen, iGrGrApnLen, iSkip, iApnFld, iLdrGrp, iTaxGrp, iLoadTax, iSaleDateFmt,
         iNoMatch, iLoadFlag, iHdrRows, iValHdr, iSkipQuote, iValSkipQuote, iMaxLegal, iMaxChgAllowed, iErrorCnt;
bool     bMergeAttr, bLoadGrGr, bUpdRoll, bMergeGrGr, bLoadLien, bDontUpd, bCreateUpd, bSetLastFileDate,
         bEnCode, bUseSfxXlat, bApnXlat, bClearSales, bClearPQZ, bDebug, bOverwriteLogfile, bClean, 
         bSendMail, bFixTRA, bClearChars, bMergeOthers, bNewTax, bGrGrAvail, bUseConfSalePrice, bExpSql,
         bSaleImport, bTaxImport, bGrgrImport, bMixSale, bRemTmpFile, bUnzip, bUpdPrevApn;

long     lRecCnt, lAssrRecCnt, lLastRecDate, lLastFileDate, lLastGrGrDate, lLastTaxFileDate, lToday, lToyear, 
         lLienDate, lLienYear, lProcDate, lSitusMatch, lCharMatch, lExeMatch, lSaleMatch, lTaxMatch, lGrGrMatch, 
         lValueMatch, lNameMatch, lApprMatch, lRollMatch, lSitusSkip, lCharSkip, lExeSkip, lSaleSkip, lTaxSkip, 
         lValueSkip, lNameSkip, lTaxYearIdx, lTaxAmt1Idx, lTaxAmt2Idx, lTaxFlds, lLDRRecCount, lOptMisc,
         lTaxabilityIdx, lRollChgIdx, lPenDate1Idx, lPaidDate1Idx, lTaxYear;

COUNTY_INFO myCounty;

hlAdo    hl;
hlAdoRs  m_AdoRs;
bool     m_bConnected;

IDX_TBL  asSaleTypes[] =
{
   "FV", "F",               // Full value
   "WL", "I",               // With other properties less lien
   "LL", "N",               // Less lien
   "WP", "W",               // With other properties (group sale)
   "NT", "U",               // No transferor/Unknown
   "", ""
};

IDX_TBL2 asDocType[] =
{
   0,  "   ",
   1,  "1  ",
   2,  "57 ",
   3,  "   ",
   4,  "   ",
   5,  "43 ",
   6,  "51 ",
   7,  "   ",
   8,  "   ",
   9,  "6  ",
   10, "67 ",
   11, "3  ",
   12, "   ",
   13, "9  ",
   14, "   ",
   15, "41 ",
   16, "   ",
   17, "   ",
   18, "40 ",
   19, "77 ",
   20, "   ",
   21, "   ",
   22, "55 ",
   23, "   ",
   24, "75 ",
   25, "40 ",
   26, "   ",
   27, "   ",
   28, "   ",
   29, "   ",
   30, "   ",
   31, "   ",
   32, "   ",
   33, "   ",
   34, "   ",
   35, "   ",
   36, "   ",
   37, "   ",
   38, "   ",
   39, "   ",
   40, "   ",
   41, "   ",
   42, "   ",
   43, "   ",
   44, "   ",
   45, "   ",
   46, "   ",
   47, "   ",
   48, "   ",
   49, "   ",
   50, "   ",
   51, "75 ",
   52, "   ",
   53, "   ",
   54, "   ",
   55, "   ",
   56, "   ",
   57, "   ",
   58, "   ",
   59, "   ",
   60, "   ",
   61, "   ",
   62, "   ",
   63, "   ",
   64, "   ",
   65, "   ",
   66, "   ",
   67, "   ",
   68, "   ",
   69, "   ",
   70, "   ",
   71, "   ",
   72, "   ",
   73, "   ",
   74, "   ",
   75, "   ",
   76, "   ",
   77, "   ",
   78, "   ",
   79, "   ",
   80, "   ",
   81, "   ",
   82, "   ",
   83, "   ",
   84, "   ",
   85, "   ",
   86, "   ",
   87, "   ",
   88, "   ",
   89, "   ",
   90, "   ",
   91, "   ",
   92, "   ",
   93, "   ",
   94, "   ",
   95, "   ",
   96, "   ",
   97, "   ",
   98, "   ",
   99, "   "
};

IDX_SALE ORG_DocCode[] =
{// DOCTITLE, DOCTYPE, ISSALE, ISXFER, TITLELEN, TYPELEN
   "","",0,0,0
};

int   CopySales(int iSkip);
int   loadAlp(int);
int   loadAma(int);
int   loadBut(int);
int   loadCal(int);
int   loadCol(int);
int   loadDnx(int);
int   loadEdx(int);
int   loadGle(int);
int   loadHum(int);
int   loadImp(int);
int   loadIny(int);
int   loadKin(int);
int   loadLak(int);
int   loadLas(int);
int   loadMad(int);
int   loadMen(int);
int   loadMer(int);
int   loadMno(int);
int   loadMod(int);
int   loadMon(int);
int   loadMpa(int);
int   loadNap(int);
int   loadNev(int);
int   loadPla(int);
int   loadPlu(int);
int   loadSbt(int);
int   loadSha(int);
int   loadSis(int);
int   loadSie(int);
int   loadSjx(int);
int   loadSon(int);
int   loadSta(int);
int   loadSut(int);
int   loadTeh(int);
int   loadTri(int);
int   loadTul(int);
int   loadTuo(int);
int   loadYol(int);
int   loadYub(int);
void  But_MakeDocLink(LPSTR pDocLink, LPSTR pDoc, LPSTR pDate);
void  Mno_MakeDocLink(LPSTR pDocLink, LPSTR pDoc, LPSTR pDate);

/*************************** MB_UpdateDelqYear *****************************
 *
 * Update Delq year in Tax_Base
 * Use by: BUT
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int MB_FindTotalDelqAmt(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     *apItems[MAX_FLD_TOKEN];
   long     iTmp, iYear;
   double	dTax1, dTax2, dPen1, dPen2, dTaxTotal, dPenTotal, dFeeTotal;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 2048, fdTax);
   }

   do 
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF 
      }

      // Find matching APN
      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Delq rec %.80s", pRec);
         pRec = fgets(acRec, 2048, fdTax);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 0;

   dPenTotal=dFeeTotal=dTaxTotal = 0;
   do
   {
      iTmp = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apItems);
      if (iTmp < MB2_TX_RATEUSED)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, acRec, iTmp);
         iErrorCnt++;
      }

      // Add prior year amt
      iYear = atol(apItems[MB2_TX_TAXYEAR]);
      if (iYear < lLienYear)
      {
         dTax1 = atof(apItems[MB2_TX_TAXAMT1]);
         dTax2 = atof(apItems[MB2_TX_TAXAMT2]);
         dPen1 = atof(apItems[MB2_TX_PENAMT1]);
         dPen2 = atof(apItems[MB2_TX_PENAMT2]);

         // Total Fees 
         dFeeTotal += atof(apItems[MB2_TX_COST2]);

         // Total tax
         dTaxTotal += dTax1+dTax2;

         // Total Penalties
         dPenTotal += dPen1+dPen2;
      }

      // Get next record
      pRec = fgets(acRec, 2048, fdTax);
   } while (pRec && !memcmp(pOutbuf, pRec, iApnLen));

   if (dTaxTotal > 0.0)
   {
      sprintf(pOutRec->Def_Amt, "%.2f", dTaxTotal+dFeeTotal+dPenTotal);
      sprintf(pOutRec->Fee_Amt, "%.2f", dFeeTotal);
      sprintf(pOutRec->Pen_Amt, "%.2f", dPenTotal);
   }

   return 0;
}

/*************************** MB_UpdateDelqYear *****************************
 *
 * Update Delq year in Tax_Base
 * Use by: BUT
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int MB_UpdateDelqYear(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   long     lRedDate, lDefDate;
   int      iTmp;
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;

   // Get rec
   if (!pRec)
   {
      do {
         pRec = fgets(acRec, 2048, fdDelq);
      } while (pRec && memcmp(pRec, "DEF", 3));
   }

   do 
   {
      if (!pRec)
      {
         fclose(fdDelq);
         fdDelq = NULL;
         return 1;      // EOF 
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+7, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Delq rec %.80s", pRec);
         pRec = fgets(acRec, 2048, fdDelq);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 0;

   iTmp = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB_TRE_EXISTSPAYMENTHOLD)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, acRec, iTmp);
      iErrorCnt++;
      return -1;
   }

   // Default date
   lDefDate = 0;
   // Default date
   if (*apTokens[MB_TRE_DEFAULTDATE] >= '0' && dateConversion(apTokens[MB_TRE_DEFAULTDATE], acTmp, MM_DD_YYYY_1))
   {
      strcpy(pOutRec->Def_Date, acTmp);
      lDefDate = atol(acTmp);
      acTmp[4] = 0;
      strcpy(pOutRec->DelqYear, acTmp);
      pOutRec->isDelq[0] = '1';
   }

   // Redemption date
   if (*apTokens[MB_TRE_REDEEMEDDATE] >= '0' && dateConversion(apTokens[MB_TRE_REDEEMEDDATE], acTmp, MM_DD_YYYY_1))
   {
      lRedDate = atol(acTmp);
      if (lRedDate > lDefDate)
         pOutRec->isDelq[0] = '0';
   }

   // Get next record
   pRec = fgets(acRec, 2048, fdDelq);

   return 0;
}

/*****************************************************************************
 *
 *
 *****************************************************************************/

void MB_ParseOwnerMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

   TAXOWNER *pOwner = (TAXOWNER *)pOutbuf;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "079130028000", 9))
   //   iTmp = 0;
#endif

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         // Update DBA
         strcpy(pOwner->Dba, pLine1+4);
         p1 = pLine3;
      } else if (!_memicmp(pLine2, "DBA ", 4) )
      {
         // Update DBA
         strcpy(pOwner->Dba, pLine2+4);
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         // Update DBA
         strcpy(pOwner->Dba, pLine1+4);
         p1 = pLine2;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
         {
            p1 = pLine1;
         } else
         {
            // If last word of line 3 is number and first word of line 1 is alpha,
            // line 1 more likely be CareOf
            p0 = pLine1;
            p1 = pLine2;
         }
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      strcpy(pOwner->CareOf, p0);
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      iTmp = blankRem(acAddr1, SIZ_M_ADDR_D);
      strcpy(pOwner->MailAdr[0], acAddr1);
   }

   strcpy(acAddr2, p2);
   iTmp = blankRem(acAddr2);
   strcpy(pOwner->MailAdr[1], acAddr2);
}

/*****************************************************************************
 *
 *
 *****************************************************************************/

int MB1_ParseOwnerRec(char *pOutbuf, char *pRollRec)
{
   int      iRet;
   TAXOWNER *pOwner = (TAXOWNER *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L1_USERID)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L1_ASMT]);
      iErrorCnt++;
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset((void *)pOwner, 0, sizeof(TAXOWNER));

   // Start copying data
   strcpy(pOwner->Apn, apTokens[L1_ASMT]);
   strcpy(pOwner->BillNum, apTokens[L1_FEEPARCEL]);

   // Tax year
   strcpy(pOwner->TaxYear, apTokens[L1_TAXYEAR]);

   // Owner names
   strcpy(pOwner->Name1, apTokens[L1_OWNER]);

   // Mail addr
   MB_ParseOwnerMAdr(pOutbuf, apTokens[L1_MAILADDRESS1], apTokens[L1_MAILADDRESS2], apTokens[L1_MAILADDRESS3], apTokens[L1_MAILADDRESS4]);

   return 0;
}

/*****************************************************************************
 *
 * For BUT
 *
 *****************************************************************************/

int MB2_ParseOwnerRec(char *pOutbuf, char *pRollRec)
{
   int      iRet;
   TAXOWNER *pOwner = (TAXOWNER *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L2_USERID)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L2_ASMT]);
      iErrorCnt++;
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset((void *)pOwner, 0, sizeof(TAXOWNER));

   // Start copying data
   strcpy(pOwner->Apn, apTokens[L2_ASMT]);
   strcpy(pOwner->BillNum, apTokens[L2_FEEPARCEL]);

   // Tax year
   strcpy(pOwner->TaxYear, apTokens[L2_TAXYEAR]);

   // Owner names
   strcpy(pOwner->Name1, apTokens[L2_OWNER]);

   // Mail addr
   MB_ParseOwnerMAdr(pOutbuf, apTokens[L2_MAILADDRESS1], apTokens[L2_MAILADDRESS2], apTokens[L2_MAILADDRESS3], apTokens[L2_MAILADDRESS4]);

   return 0;
}

/*****************************************************************************
 *
 * 2016 LDR
 *
 *****************************************************************************/

int MB3_ParseOwnerRec(char *pOutbuf, char *pRollRec)
{
   int      iRet;
   TAXOWNER *pOwner = (TAXOWNER *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      iErrorCnt++;
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset((void *)pOwner, 0, sizeof(TAXOWNER));

   // Start copying data
   strcpy(pOwner->Apn, apTokens[L3_ASMT]);
   strcpy(pOwner->BillNum, apTokens[L3_FEEPARCEL]);

   // Tax year
   strcpy(pOwner->TaxYear, apTokens[L3_TAXYEAR]);

   // Owner names
   strcpy(pOwner->Name1, apTokens[L3_OWNER]);

   // Mail addr
   MB_ParseOwnerMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   return 0;
}

/*************************** MB_Load_TaxOwner() ******************************
 *
 * Since tax file doesn't include owner info, we get it from LDR file
 *
 *****************************************************************************/

int MB_Load_TR601Owner(bool bImport, int iMinToken)
{
   char     acBuf[1024], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], *pTmp;

   int      iRet, iOutRec=0;
   long     lCnt=0;
   FILE     *fdOwner;

   // Open roll file
   GetIniString(myCounty.acCntyCode, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
   LogMsgD("\nExtract Owner data from Roll file %s", acBuf);

   // Sort on ASMT
   sprintf(acTmpFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (iLdrGrp == 3 || iLdrGrp == 4)
      iRet = sortFile(acBuf, acTmpFile, "S(#3,C,A) DEL(9)");
   else
      iRet = sortFile(acBuf, acTmpFile, "S(1,12,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")");
   if (iRet <= 0)
      return -1;

   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -1;
   }

   // Open Output file
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Owner");
   LogMsg("Open Owner extract file %s", acOutFile);
   fdOwner = fopen(acOutFile, "w");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error creating owner extract file: %s\n", acOutFile);
      return -2;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Replace null char with space
      iRet = replNull(acRec, ' ', 0);

      // Create new record
      switch (iLdrGrp)
      {
         case 1:
            iRet = MB1_ParseOwnerRec(acBuf, acRec);
            break;
         case 3:
            iRet = MB3_ParseOwnerRec(acBuf, acRec);
            break;
         default:
            iRet = MB2_ParseOwnerRec(acBuf, acRec);
            break;
      }
   
      if (!iRet)
      {
         Tax_CreateTaxOwnerCsv(acRec, (TAXOWNER *)&acBuf[0]);

         // Write to output
         fputs(acRec, fdOwner);
         iOutRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      if (iMinToken > 0)
      {
         //iRet = myGetStrTC(acRec, cLdrSep, L_USERID, MAX_RECSIZE, fdRoll);
         iRet = myGetStrTC(acRec, cLdrSep, iMinToken, MAX_RECSIZE, fdRoll);
         if (iRet < 1)
            break;
      } else
         pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOwner)
      fclose(fdOwner);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_OWNER);
   } else
      iRet = 0;


   LogMsgD("\nTotal lien records extracted:    %u", lCnt);

   return 0;
}

/******************************* updateDocLinks *****************************
 *
 * Format DocLinks using county specific formatting function
 * for Doclink1, Doclink2, Doclink3, DocXferlink
 *
 ****************************************************************************

int updateDocLinks(void (*pfMakeDocLink)(LPSTR, LPSTR, LPSTR), int iSkip, int iUpdFlg)
{
   char     acBuf[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH],
            acDocLink[256], acDocLinks[256], acDoc[32];

   HANDLE   fhIn, fhOut;

   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0, lLinkCnt=0;
   int      iTmp, iRet = 0;

   LogMsg("Format Doclinks ...");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "T01");

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }
   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   memset(acBuf, ' ', MAX_RECSIZE);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (true)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

#ifdef _DEBUG
//      if (!memcmp(acBuf, "08529006", 8))
//         bRet = true;
#endif

      // Reset old links
      memset((char *)&acBuf[OFF_DOCLINKS], ' ', SIZ_DOCLINKS);
      memset((char *)&acBuf[OFF_DOCLINKX], ' ', SIZ_DOCLINKX);
      acDocLinks[0] = 0;

      // Doc #1
      memcpy(acDoc, (char *)&acBuf[OFF_SALE1_DOC], SIZ_SALE1_DOC);
      myTrim(acDoc, SIZ_SALE1_DOC);
      pfMakeDocLink(acDocLink, acDoc, (char *)&acBuf[OFF_SALE1_DT]);
      if (*acDocLink)
      {
         lLinkCnt++;
         strcat(acDocLinks, acDocLink);
      }
      strcat(acDocLinks, ",");

      // Doc #2
      memcpy(acDoc, (char *)&acBuf[OFF_SALE2_DOC], SIZ_SALE2_DOC);
      myTrim(acDoc, SIZ_SALE2_DOC);
      pfMakeDocLink(acDocLink, acDoc, (char *)&acBuf[OFF_SALE2_DT]);
      if (*acDocLink)
      {
         lLinkCnt++;
         strcat(acDocLinks, acDocLink);
      }
      strcat(acDocLinks, ",");

      // Doc #3
      memcpy(acDoc, (char *)&acBuf[OFF_SALE3_DOC], SIZ_SALE3_DOC);
      myTrim(acDoc, SIZ_SALE3_DOC);
      pfMakeDocLink(acDocLink, acDoc, (char *)&acBuf[OFF_SALE3_DT]);
      if (*acDocLink)
      {
         lLinkCnt++;
         strcat(acDocLinks, acDocLink);
      }
      strcat(acDocLinks, ",");

      // Transfer Doc
      memcpy(acDoc, (char *)&acBuf[OFF_TRANSFER_DOC], SIZ_TRANSFER_DOC);
      myTrim(acDoc, SIZ_TRANSFER_DOC);
      pfMakeDocLink(acDocLink, acDoc, (char *)&acBuf[OFF_TRANSFER_DT]);
      if (*acDocLink)
      {
         lLinkCnt++;
         strcat(acDocLinks, acDocLink);
      }

      // Update with formatted transfer DOCNUM
      if (iUpdFlg == 1 && acDoc[0] > ' ')
         memcpy((char *)&acBuf[OFF_TRANSFER_DOC], acDoc, strlen(acDoc));

      // Update DocLinks
      iTmp = strlen(acDocLinks);
      if (iTmp > 10)
      {
         if (iTmp > SIZ_DOCLINKS)
         {
            memcpy((char *)&acBuf[OFF_DOCLINKS], acDocLinks, SIZ_DOCLINKS);
            memcpy((char *)&acBuf[OFF_DOCLINKX], &acDocLinks[SIZ_DOCLINKS], iTmp-SIZ_DOCLINKS);
         } else
            memcpy((char *)&acBuf[OFF_DOCLINKS], acDocLinks, iTmp);
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename out file
   if (lCnt > 10000)
   {
      if (remove(acRawFile))
      {
         LogMsg("***** Error removing file: %s (%d)", acRawFile, errno);
         iRet = errno;
      } else if (rename(acOutFile, acRawFile))
      {
         LogMsg("***** Error renaming temp file: %s --> %s (%d)", acOutFile, acRawFile, errno);
         iRet = errno;
      }
   }

   LogMsg("FmtDocLinks completed.  %d doclinks assigned.\n", lLinkCnt);
   return iRet;
}

/*********************************** updateTaxCode *****************************
 *
 * Return taxcode if updated
 *
 *****************************************************************************/

int updateTaxCode(LPSTR pOutbuf, LPSTR pTaxCode, bool bProp8, bool bFullExempt)
{
   char  acTmp[32];
   int   iTmp=0;

   if (*pTaxCode)
   {
      iTmp = atol(pTaxCode);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%.*d", SIZ_TAX_CODE, iTmp);
         memcpy(pOutbuf+OFF_TAX_CODE, acTmp, SIZ_TAX_CODE);

         if (bProp8 && iTmp > 799 && iTmp < 900)
            *(pOutbuf+OFF_PROP8_FLG) = 'Y';     // Set Prop8 flag
         else if (bFullExempt && iTmp == 3)   
            *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';   // Set FullExempt
      } else
         memset(pOutbuf+OFF_TAX_CODE, ' ', SIZ_TAX_CODE);
   }
   return iTmp;
}

/******************************** MB_MergeExe ********************************
 *
 * Merge exemption value
 *
 *****************************************************************************/

int MB_MergeExe(char *pOutbuf)
{
   return MB_MergeExe2(pOutbuf, iHdrRows);
}

/******************************** MB_MergeExe2 *******************************
 *
 * Merge exemption value
 *
 *****************************************************************************/

int MB_MergeExe2(char *pOutbuf, int iSkipHdr)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iSkipHdr; iTmp++)
         pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
      if (acRec[0] == '"')
         iTmp = 1;
      else
         iTmp = 0;
      if (iTmp != iSkipQuote)
      {
         LogMsg("*** Overwrite INI SkipQuote value to %d (from %d)", iTmp, iSkipQuote);
         iSkipQuote = iTmp;
      }
   }

   do
   {
      if (!pRec  || strlen(acRec) < 14)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      iTmp = memcmp(pOutbuf, pTmp+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   iRet = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      iErrorCnt++;
      return -1;
   }

   // EXE code
   if (*apTokens[MB_EXE_CODE] > ' ')
      memcpy(pOutbuf+OFF_EXE_CD1, apTokens[MB_EXE_CODE], strlen(apTokens[MB_EXE_CODE]));

   // HO Exe
   myLTrim(apTokens[MB_EXE_HOEXE]);
   if (*apTokens[MB_EXE_HOEXE] == '1' || *apTokens[MB_EXE_HOEXE] == 'T')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      long lGross = atoin(pOutbuf+OFF_GROSS, SIZ_GROSS);

      if (lTmp > lGross)
      {
         if (bDebug)
            LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** MB_MergeTaxRC ******************************
 *
 * Assuming tax file contains no text identifier, so no check for double quote.
 * Tax file must include TAXABILITY similar to BUT.  Input is already sorted in 
 * Asmt & RollChgNum
 *
 * Use ChrgDate or PaidDate to test for new record.
 * Update value if roll change occured
 *
 *****************************************************************************/

int MB_MergeTaxRC(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *apItems[64];
   int      iRet=0, iTmp, iCnt, iRollYear;
   double	dTmp, dTax1, dTax2;
   boolean  bRollChg;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do {
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);
      } while (*pRec < '0' || *pRec > '9');
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Merge tax data
   int iBook = atoin(pOutbuf, 3);
   bRollChg = false;
   while (!iTmp)
   {
      iCnt = ParseStringIQ(pRec, cDelim, 64, apItems);
      if (iCnt < MB_TAX_TAXABILITY)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iCnt);
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);
         iErrorCnt++;
         iRet = -1;
         break;
      }

      iRollYear = atol(apItems[MB_TAX_YEAR]);
      if (iRollYear == lLienYear)
      {
         // Ignore refund amount
         if (*apItems[MB_TAX_TAXAMT1] != '(')
            dTax1 = atof(apItems[MB_TAX_TAXAMT1]);
         else
            dTax1 = 0;
         if (*apItems[MB_TAX_TAXAMT2] != '(')
            dTax2 = atof(apItems[MB_TAX_TAXAMT2]);
         else
            dTax2 = 0;

         dTmp = dTax1+dTax2;

         // Don't double unsecured parcel range 800-879
         if ((iBook < 800) || (iBook > 879))
         {
            if (dTax1 == 0.0 || dTax2 == 0.0)
               dTmp *= 2;
         }

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

         // Assign RollChg flag
         if (*apItems[MB_TAX_ROLLCHGNUM] > ' ')
         {
            *(pOutbuf+OFF_ROLLCHG_FLG) = 'Y';
            bRollChg = true;
         } else if (bRollChg)
         {
            // Update value
            long lLand = atol(apItems[MB_TAX_LAND]);
            if (lLand > 0)
            {
               sprintf(acTmp, "%*d", SIZ_LAND, lLand);
               memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
            } else
               memset(pOutbuf+OFF_LAND, ' ', SIZ_LAND);

            long lImpr = atol(apItems[MB_TAX_IMPR]);
            if (lImpr > 0)
            {
               sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
               memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
            } else
               memset(pOutbuf+OFF_IMPR, ' ', SIZ_IMPR);

            long lVal = atol(apItems[MB_TAX_PERSPROP]);
            if (lVal > 0)
            {
               sprintf(acTmp, "%d          ", lVal);
               memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
            } else
               memset(pOutbuf+OFF_PERSPROP, ' ', SIZ_PERSPROP);

            long lOther = lVal;

            lVal = atol(apItems[MB_TAX_FIXEDIMPR]);
            if (lVal > 0)
            {
               sprintf(acTmp, "%d          ", lVal);
               memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
            } else
               memset(pOutbuf+OFF_FIXTR, ' ', SIZ_FIXTR);
            lOther += lVal;

            lVal = atol(apItems[MB_TAX_GROWIMPR]);
            if (lVal > 0)
            {
               sprintf(acTmp, "%d          ", lVal);
               memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
            } else
               memset(pOutbuf+OFF_GR_IMPR, ' ', SIZ_GR_IMPR);
            lOther += lVal;

            lVal = atol(apItems[MB_TAX_PP_MH]);
            if (lVal > 0)
            {
               sprintf(acTmp, "%d          ", lVal);
               memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
            } else
               memset(pOutbuf+OFF_PP_MH, ' ', SIZ_PP_MH);
            lOther += lVal;

            if (lOther > 0)
            {
               sprintf(acTmp, "%*d", SIZ_OTHER, lOther);
               memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
            }

            // Gross
            long lGross = lOther + lLand + lImpr;
            if (lGross > 0)
            {
               sprintf(acTmp, "%*d", SIZ_GROSS, lGross);
               memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
            }

            // Ratio
            if (lImpr > 0)
            {
               __int64 lBig;
               lBig = (__int64)lImpr*100;
               iTmp = sprintf(acTmp, "%*d", SIZ_RATIO, lBig/(lLand+lImpr));
               memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
            }

            // Exemption
            lVal = atol(apItems[MB_TAX_EXEAMT1]);
            lVal += atol(apItems[MB_TAX_EXEAMT2]);
            lVal += atol(apItems[MB_TAX_EXEAMT3]);
            if (lVal > 0 && lVal < lGross)
            {
               sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lVal);
               memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
            } else
               memset(pOutbuf+OFF_EXE_TOTAL, ' ', SIZ_EXE_TOTAL);
         }
      }

      // Get next tax record
      pRec = fgets(acRec, MAX_RECSIZE, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/******************************** MB_MergeTax ********************************
 *
 * Used by BUT, MAD, MER, MNO, MON, NAP, SBT, SON, YOL, YUB
 *
 *****************************************************************************/

int MB_MergeTax(char *pOutbuf)
{
   static   char acRec[MAX_RECSIZE], *pRec=NULL;
   char     acTmp[256];
   int      iRollYear, iRet=0, iTmp, iCnt;
   long     lTestDate, lTmp;
   double	dTmp, dTax1, dTax2;
   boolean  bDone;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; !iTaxGrp && iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);

      // Get first rec
      pRec = fgets(acRec, MAX_RECSIZE, fdTax);
      if (acRec[0] == '"')
         iTmp = 1;
      else
         iTmp = 0;
      if (iTmp != iSkipQuote)
      {
         LogMsg("*** Overwrite INI SkipQuote value to %d (from %d)", iTmp, iSkipQuote);
         iSkipQuote = iTmp;
      }
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Merge tax data
   int iBook = atoin(pOutbuf, 3);
   bDone = false;
   lTestDate = 0;
   while (!iTmp && !bDone)
   {
      // Replace null with space
      replNull(pRec);
      // Remove quotes
      quoteRem(pRec);

      iCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iCnt < lTaxFlds)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iCnt);
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);
         iErrorCnt++;
         iRet = -1;
         break;
      }

      // Some counties don't provide TAX_YEAR field (i.e. HUM)
      if (iCnt > lTaxYearIdx)
         iRollYear = atol(apTokens[lTaxYearIdx]);
      else
         iRollYear = lLienYear;

      if (iRollYear == lLienYear)
      {
         // Ignore refund amount
         if (*apTokens[lTaxAmt1Idx] != '(')
         {
            dollar2Num(apTokens[lTaxAmt1Idx], acTmp);
            dTax1 = atof(acTmp);
         } else
            dTax1 = 0;
         if (*apTokens[lTaxAmt2Idx] != '(')
         {
            dollar2Num(apTokens[lTaxAmt2Idx], acTmp);
            dTax2 = atof(acTmp);
         } else
            dTax2 = 0;

         dTmp = dTax1+dTax2;

         // Don't double unsecured parcel range 800-879
         if ((iBook < 800) || (iBook > 879))
         {
            if (dTax1 == 0.0 || dTax2 == 0.0)
               dTmp *= 2;
         }

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

         // CAL
         if (*apTokens[MB_TAX_PENDATE1] > '0' && dateConversion(apTokens[MB_TAX_PENDATE1], acTmp, MM_DD_YYYY_1))
            lTmp = atol(acTmp);
         else if (*apTokens[MB_TAX_PAIDDATE1] > '0' && dateConversion(apTokens[MB_TAX_PAIDDATE1], acTmp, MM_DD_YYYY_1))
            lTmp = atol(acTmp);
         else
            lTmp = 0;

         if (lTmp > 0)
         {
            if (lTmp > lTestDate)
               lTestDate = lTmp;
            else
               bDone = true;
         }

         // Assign RollChg flag
         //if (iCnt > MB_TAX_NETVAL && *apTokens[MB_TAX_ROLLCHGNUM] > ' ')
         //   *(pOutbuf+OFF_ROLLCHG_FLG) = 'Y';
      }

      // Get next tax record
      pRec = fgets(acRec, MAX_RECSIZE, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/******************************** MB_MergeTaxG1 ******************************
 *
 * Merge tax info for counties with new tax file using Tax group 1 layout
 *
 * AMA
 * 
 *****************************************************************************/

int MB_MergeTaxG1(char *pOutbuf, int iSkipHdr)
{
   static   char acRec[MAX_RECSIZE], *pRec=NULL;
   char     acTmp[256];
   int      iRollYear, iRet=0, iTmp;
   long     lTestDate, lTmp;
   double	dTmp, dTax1, dTax2;
   boolean  bDone;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iSkipHdr; iTmp++)
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);

      // Get first rec
      pRec = fgets(acRec, MAX_RECSIZE, fdTax);
      if (acRec[0] == '"')
         iTmp = 1;
      else
         iTmp = 0;
      if (iTmp != iSkipQuote)
      {
         LogMsg("*** Overwrite INI SkipQuote value to %d (from %d)", iTmp, iSkipQuote);
         iSkipQuote = iTmp;
      }
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Merge tax data
   int iBook = atoin(pOutbuf, 3);
   bDone = false;
   lTestDate = 0;
   while (!iTmp && !bDone)
   {
      if (iSkipQuote)
         iTmp = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iTmp = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

      if (iTmp < T1_USERID)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);
         iErrorCnt++;
         iRet = -1;
         break;
      }

      // Some counties don't provide TAX_YEAR field (i.e. HUM)
      if (iTmp > T1_TAXYEAR)
         iRollYear = atol(apTokens[T1_TAXYEAR]);
      else
         iRollYear = lLienYear;

      if (iRollYear == lLienYear)
      {
         // Ignore refund amount
         if (*apTokens[T1_TAXAMT1] != '(')
         {
            dollar2Num(apTokens[T1_TAXAMT1], acTmp);
            dTax1 = atof(acTmp);
         } else
            dTax1 = 0;
         if (*apTokens[T1_TAXAMT2] != '(')
         {
            dollar2Num(apTokens[T1_TAXAMT2], acTmp);
            dTax2 = atof(acTmp);
         } else
            dTax2 = 0;

         dTmp = dTax1+dTax2;

         // Don't double unsecured parcel range 800-879
         if ((iBook < 800) || (iBook > 879))
         {
            if (dTax1 == 0.0 || dTax2 == 0.0)
               dTmp *= 2;
         }

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
#ifdef _DEBUG
         //if (!memcmp(pOutbuf, "001010190000", 9) )
         //   iTmp = 0;
#endif

         // Taxibility
         iTmp = updateTaxCode(pOutbuf, apTokens[T1_TAXABILITY], true, true);

         // CAL
         if (*apTokens[T1_PENCHRGDATE1] > '0' && dateConversion(apTokens[T1_PENCHRGDATE1], acTmp, MM_DD_YYYY_1))
            lTmp = atol(acTmp);
         else if (*apTokens[T1_PAYMENTDATE1] > '0' && dateConversion(apTokens[T1_PAYMENTDATE1], acTmp, MM_DD_YYYY_1))
            lTmp = atol(acTmp);
         else
            lTmp = 0;

         if (lTmp > 0)
         {
            if (lTmp > lTestDate)
               lTestDate = lTmp;
            else
               bDone = true;
         }

         // Assign RollChg flag
         //if (*apTokens[lRollChgIdx] > ' ')
         //   *(pOutbuf+OFF_ROLLCHG_FLG) = 'Y';
      }

      // Get next tax record
      pRec = fgets(acRec, MAX_RECSIZE, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/******************************** MB_MergeTaxG2 ******************************
 *
 * Merge tax info for counties with new tax file using Tax group 2 layout
 *
 * COL, GLE, HUM, LAK, PLU, SHA, SIS, TEH, TUO
 *
 *****************************************************************************/

int MB_MergeTaxG2(char *pOutbuf, int iSkipHdr)
{
   static   char acRec[MAX_RECSIZE], *pRec=NULL;
   char     acTmp[256];
   int      iRollYear, iTmp, iRet=0;
   long     lTestDate, lTmp;
   double	dTmp, dTax1, dTax2;
   boolean  bDone;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iSkipHdr; iTmp++)
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);

      // Get first rec
      pRec = fgets(acRec, MAX_RECSIZE, fdTax);
      if (acRec[0] == '"')
         iTmp = 1;
      else
         iTmp = 0;
      if (iTmp != iSkipQuote)
      {
         LogMsg("*** Overwrite INI SkipQuote value to %d (from %d)", iTmp, iSkipQuote);
         iSkipQuote = iTmp;
      }
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Merge tax data
   int iBook = atoin(pOutbuf, 3);
   bDone = false;
   lTestDate = 0;
   while (!iTmp && !bDone)
   {
      replNull(pRec, ' ');
      if (iSkipQuote)
         iTmp = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iTmp = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

      if (iTmp < T2_ISONLYFIRSTPAID)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);
         iErrorCnt++;
         iRet = -1;
         break;
      }

      // Tax year
      iRollYear = atol(apTokens[T2_TAXYEAR]);
      if (iRollYear == lLienYear)
      {
         // Ignore refund amount
         if (*apTokens[T2_TAXAMT1] != '(')
         {
            dollar2Num(apTokens[T2_TAXAMT1], acTmp);
            dTax1 = atof(acTmp);
         } else
            dTax1 = 0;
         if (*apTokens[T2_TAXAMT2] != '(')
         {
            dollar2Num(apTokens[T2_TAXAMT2], acTmp);
            dTax2 = atof(acTmp);
         } else
            dTax2 = 0;

         dTmp = dTax1+dTax2;

         // Don't double unsecured parcel range 800-879
         if ((iBook < 800) || (iBook > 879))
         {
            if (dTax1 == 0.0 || dTax2 == 0.0)
               dTmp *= 2;
         }

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
#ifdef _DEBUG
         //if (!memcmp(pOutbuf, "001010190000", 9) )
         //   iTmp = 0;
#endif

         // Taxibility
         iTmp = updateTaxCode(pOutbuf, apTokens[T2_TAXABILITY], true, true);

         // CAL
         if (*apTokens[T2_PENCHRGDATE1] > '0' && dateConversion(apTokens[T2_PENCHRGDATE1], acTmp, MM_DD_YYYY_1))
            lTmp = atol(acTmp);
         else if (*apTokens[T2_PAYMENTDATE1] > '0' && dateConversion(apTokens[T2_PAYMENTDATE1], acTmp, MM_DD_YYYY_1))
            lTmp = atol(acTmp);
         else
            lTmp = 0;

         if (lTmp > 0)
         {
            if (lTmp > lTestDate)
               lTestDate = lTmp;
            else
               bDone = true;
         }

         // Assign RollChg flag
         //if (*apTokens[lRollChgIdx] > ' ')
         //   *(pOutbuf+OFF_ROLLCHG_FLG) = 'Y';
      }

      // Get next tax record
      pRec = fgets(acRec, MAX_RECSIZE, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/******************************** MB_MergeSale *******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * This special version to support MB counties that ignore sale price on Prop 58 (code 17).
 * Only update seller if that is the sale from assessor.
 *    1) If docnum < doc1num, ignore this sale. 
 *    2) If docdate < doc1date, ignore this sale
 *    3) If docdate = doc1date, update current sale only if sale price is present
 *       or current sale is a GD and last one is not.  We want to keep it to one
 *       transaction per day.
 *    4) If docdate > doc1date, insert new sale 
 *
 * 01/09/2014 Skip none-sale record.
 *
 *****************************************************************************/

int MB_MergeSale(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag, bool bUpdtXfer)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt, iTmp;
   char  acDocType[8];

   // Skip known none-sale transaction
   if (pSaleRec->NoneSale_Flg == 'Y')
      return -2;

   // Check case #1
   iTmp = memcmp(pSaleRec->acDocNum, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   if (iTmp < 0)
      return -1;

   lCurSaleDt = atoin(pSaleRec->acDocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   // Check case #2
   if (lCurSaleDt < lLstSaleDt)
      return -1;

   // Do not use sale price on Individual Grant Deed
   int iDocType = atoin(pSaleRec->acDocType, 3);
   if (iDocType != 17)
      lPrice = atoin(pSaleRec->acSalePrice, SIZ_SALE1_AMT);
   else
   {
      lPrice = 0;
      memset(pSaleRec->acSalePrice, ' ', SIZ_SALE1_AMT);
      memset(pSaleRec->acSaleCode,  ' ', SALE_SIZ_SALECODE);
   }

   // Convert DocType
   if (pSaleRec->acDocType[0] > ' ')
      findDocType(pSaleRec->acDocType, acDocType, (IDX_TBL2 *)&asDocType[0]);
   else
      strcpy(acDocType, "   ");

   // Check case #3
   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (!lPrice && lLastAmt > 0)
         return 0;
      // We want to keep GD
      if (!lPrice && acDocType[0] != '1' && *(pOutbuf+OFF_SALE1_DOCTYPE) == '1')
         return 0;
   } else // case #4
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE1+2) = *(pOutbuf+OFF_AR_CODE1+1);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE1+1) = *(pOutbuf+OFF_AR_CODE1);
      iTmp = 1;
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->acDocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->acDocDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->acSalePrice, SIZ_SALE1_AMT);
   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, acDocType, strlen(acDocType));
   memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->acSaleCode, SALE_SIZ_SALECODE);

   if (bSaleFlag)
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);
   else
      memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

   // Only update transfer if being asked for
   if (bUpdtXfer || *(pOutbuf+OFF_TRANSFER_DT) == ' ')
   {
      // Only update transfer if current sale is newer
      lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
      if (lCurSaleDt >= lLstSaleDt)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->acDocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->acDocDate, SIZ_TRANSFER_DT);
      }
   }

   if (bSaleFlag)
      *(pOutbuf+OFF_AR_CODE1) = 'A';    // Assessor
   else
      *(pOutbuf+OFF_AR_CODE1) = 'R';    // Recorder

   return iTmp;
}

/****************************** MB_MergeCurRoll ******************************
 *
 * Merge last recorded info to Transfer fields
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int MB_MergeCurRoll(char *pOutbuf)
{
   static   char acRec[MAX_RECSIZE], *pRec=NULL;
   char     acTmp[256], *pTmp, *apItems[MAX_FLD_TOKEN];
   int      iRet=0, iTmp;

   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
      pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "008290028000", 9))
   //   iTmp = 0;
#endif

   do 
   {
      if (!pRec)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         return 1;      // EOF 
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec: %s", pRec);
         pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);
   else
      iRet = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad roll record for APN=%s", apItems[iApnFld]);
      iErrorCnt++;
      return -1;
   }

   // Recorded Doc - Not consistent with sale file - use only when matched
   if (*apItems[MB_ROLL_DOCNUM] > '0')
   {
      pTmp = dateConversion(apItems[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apItems[MB_ROLL_DOCNUM], strlen(apItems[MB_ROLL_DOCNUM]));
      }
   } 

   lRollMatch++;

   // Get next record
   pRec = fgets(acRec, MAX_RECSIZE, fdRoll);

   return 0;
}

/******************************** MB_ExtrLien *******************************
 *
 * Format lien extract record.  This function may not work with all counties.
 * Check lien file layout first before use.
 * Design for and first use by AMA, SON.
 *
 ****************************************************************************

int MB_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   remChar(apTokens[L_ASMT], '-');
   memcpy(pLienExtr->acApn, apTokens[L_ASMT], strlen(apTokens[L_ASMT]));

   // TRA
   lTmp = atol(apTokens[L_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LIEN_FIXT);
   }

   // Improve
   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_LIEN_FIXT);
   }

   // Other values
   long lGrow = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);

   // Fixtures
   long lFixt   = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);

   // Business Property
   long lBP   = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);

   // Personal Property
   long lPP   = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lBP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lBP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lBP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  
   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Prop 8 - correcting by spn 01/16/2010
   lTmp = atoin(apTokens[L_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Taxability
   if (lTmp > 0)
   {
      iRet = strlen(apTokens[L_TAXABILITY]);
      if (iRet > SIZ_LIEN_TAXCODE)
         iRet = SIZ_LIEN_TAXCODE;
      memcpy(pLienExtr->acTaxCode, apTokens[L_TAXABILITY], iRet);
   }

   // Save Exe code
   memcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
   memcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
   memcpy(pLienExtr->extra.MB.ExeCode3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************** MB_ExtrLien *******************************
 *
 * Extract lien data from ???_lien.csv
 * Currently not used 8/15/2016
 *
 ****************************************************************************

int MB_ExtrLien(LPCSTR pCnty, LPCSTR pLDRFile)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg("\nExtract lien roll for %s", pCnty);

   // Open lien file
   if (!pLDRFile)
   {
      GetIniString(pCnty, "LienFile", "", acRec, _MAX_PATH, acIniFile);
      sprintf(acBuf, acRec, pCnty, pCnty);
   } else
      strcpy(acBuf, pLDRFile);

   LogMsg("Open Lien Date Roll file %s", acBuf);
   sprintf(acTmpFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Sort on ASMT
   iRet = sortFile(acBuf, acTmpFile, "S(#1,C,A)");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acTmpFile);
      return 2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new record
      iRet = MB_CreateLienRec(acBuf, acRec);
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/****************************** MB_ExtrTR601Rec *****************************
 *
 * Format lien extract record from TR601 file.  This function may not work with 
 * all counties.  Check lien file layout first before use.
 * Design for and first use by MER.
 *
 ****************************************************************************/

int MB1_ExtrTR601Rec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L1_USERID)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L1_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Check tax year
   lTmp = atol(apTokens[L1_TAXYEAR]);
   if (lTmp != lLienYear)
      return 1;

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[L1_ASMT], strlen(apTokens[L1_ASMT]));

   // TRA
   lTmp = atol(apTokens[L1_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L1_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L1_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lGrow = atoi(apTokens[L1_CURRENTGROWINGIMPRVALUE]);
   long lFixt = atoi(apTokens[L1_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L1_CURRENTPERSONALPROPVALUE]);
   long lPP_MH= atoi(apTokens[L1_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L1_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L1_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L1_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  
   if (!memcmp(apTokens[L1_EXEMPTIONCODE1], "E01", 3))
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Prop 8 - correcting by spn 01/16/2010
   lTmp = atoin(apTokens[L1_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Taxability
   if (lTmp > 0)
      vmemcpy(pLienExtr->acTaxCode, apTokens[L1_TAXABILITY], SIZ_LIEN_TAXCODE);

   // Save Exe code
   vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L1_EXEMPTIONCODE1], SIZ_LIEN_EXECODEX);
   vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L1_EXEMPTIONCODE2], SIZ_LIEN_EXECODEX);
   vmemcpy(pLienExtr->extra.MB.ExeCode3, apTokens[L1_EXEMPTIONCODE3], SIZ_LIEN_EXECODEX);

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int MB2_ExtrTR601Rec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L2_USERID)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L2_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L2_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[L2_ASMT], strlen(apTokens[L2_ASMT]));

   // TRA
   lTmp = atol(apTokens[L2_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L2_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L2_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lGrow = atoi(apTokens[L2_CURRENTGROWINGIMPRVALUE]);
   long lFixt = atoi(apTokens[L2_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L2_CURRENTPERSONALPROPVALUE]);
   long lPP_MH= atoi(apTokens[L2_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L2_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L2_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L2_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  
   if (!memcmp(apTokens[L2_EXEMPTIONCODE1], "E01", 3))
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Prop 8 - correcting by spn 01/16/2010
   lTmp = atoin(apTokens[L2_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Taxability
   if (lTmp > 0)
      vmemcpy(pLienExtr->acTaxCode, apTokens[L2_TAXABILITY], SIZ_LIEN_TAXCODE);

   // Save Exe code
   vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L2_EXEMPTIONCODE1], SIZ_LIEN_EXECODEX);
   vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L2_EXEMPTIONCODE2], SIZ_LIEN_EXECODEX);
   vmemcpy(pLienExtr->extra.MB.ExeCode3, apTokens[L2_EXEMPTIONCODE3], SIZ_LIEN_EXECODEX);

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

// 2016 AMA,BUT,GLE,HUM,LAK,MAD,MER,MNO,SHA,STA,TEH,YOL,YUB
// 2020 MOD,SON
int MB3_ExtrTR601Rec(char *pOutbuf, char *pRollRec, bool bRemHyphen)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCNUM)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L3_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Remove hyphen from APN
   if (bRemHyphen)
   {
      remChar(apTokens[L3_TRA], '-');
      remChar(apTokens[L3_ASMT], '-');
   }

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // TRA
   lTmp = atol(apTokens[L3_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixt  = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixt+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.FixtureRP, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  

   iRet = 1;
   if (lExe1 > 0)
   {
      pLienExtr->acHO[0] = '1';      // 'Y'
      memcpy(pLienExtr->extra.MB.ExeCode1, "E01", 3);
      iRet = 2;
   } else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
   {
      if (iRet == 1)
         vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L3_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
      else
         vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L3_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
   }

   // Taxability
   lTmp = atoin(apTokens[L3_TAXABILITYFULL], 3);
   if (lTmp > 0)
   {
      // Prop 8 
      if (lTmp > 799 && lTmp < 900)
         pLienExtr->SpclFlag = LX_PROP8_FLG;
      vmemcpy(pLienExtr->acTaxCode, apTokens[L3_TAXABILITYFULL], SIZ_LIEN_TAXCODE);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int MB4_ExtrTR601Rec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L4_UNITS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L4_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L4_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[L4_ASMT], strlen(apTokens[L4_ASMT]));

   // TRA
   lTmp = atol(apTokens[L4_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L4_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L4_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixt  = atoi(apTokens[L4_FIXTURESVALUE]);
   long lGrow  = atoi(apTokens[L4_GROWING]);
   long lPers  = atoi(apTokens[L4_PPVALUE]);
   long lPP_MH = atoi(apTokens[L4_MHPPVALUE]);
   lTmp = lFixt+lGrow+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L4_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L4_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L4_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  

   if (!memcmp(apTokens[L4_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L4_EXEMPTIONCODE1], SIZ_LIEN_TAXCODE);
   vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L4_EXEMPTIONCODE2], SIZ_LIEN_TAXCODE);
   vmemcpy(pLienExtr->extra.MB.ExeCode3, apTokens[L4_EXEMPTIONCODE3], SIZ_LIEN_TAXCODE);

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

// 2017 - MNO
int MB5_ExtrTR601Rec(char *pOutbuf, char *pRollRec, bool bRemHyphen)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L5_UNITS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L5_ASMT], iRet);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L5_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Remove hyphen from APN
   if (bRemHyphen)
   {
      remChar(apTokens[L5_TRA], '-');
      remChar(apTokens[L5_ASMT], '-');
   }

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[L5_ASMT], strlen(apTokens[L5_ASMT]));

   // TRA
   lTmp = atol(apTokens[L5_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = dollar2Num(apTokens[L5_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[L5_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixt  = dollar2Num(apTokens[L5_FIXTURESVALUE]);
   long lFixtRP= dollar2Num(apTokens[L5_FIXTURESRP]);
   long lPers  = dollar2Num(apTokens[L5_PPVALUE]);
   long lPP_MH = dollar2Num(apTokens[L5_MHPPVALUE]);
   lTmp = lFixt+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.FixtureRP, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = dollar2Num(apTokens[L5_HOX]);
   long lExe2 = dollar2Num(apTokens[L5_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  

   iRet = 1;
   if (lExe1 > 0)
   {
      pLienExtr->acHO[0] = '1';      // 'Y'
      memcpy(pLienExtr->extra.MB.ExeCode1, "E01", 3);
      iRet = 2;
   } else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L5_OTHEREXEMPTIONCODE] > ' ')
   {
      if (iRet == 1)
         vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L5_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
      else
         vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L5_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
   }

   // Taxability
   lTmp = atoin(apTokens[L5_TAXABILITYFULL], 3);
   if (lTmp > 0)
   {
      // Prop 8 
      if (lTmp > 799 && lTmp < 900)
         pLienExtr->SpclFlag = LX_PROP8_FLG;
      vmemcpy(pLienExtr->acTaxCode, apTokens[L5_TAXABILITYFULL], SIZ_LIEN_TAXCODE);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int MB8_ExtrTR601Rec(char *pOutbuf, char *pRollRec, bool bRemHyphen)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L8_UNITS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L8_ASMT], iRet);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L8_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Remove hyphen from APN
   if (bRemHyphen)
   {
      remChar(apTokens[L8_TRA], '-');
      remChar(apTokens[L8_ASMT], '-');
   }

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[L8_ASMT], strlen(apTokens[L8_ASMT]));

   // TRA
   lTmp = atol(apTokens[L8_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = dollar2Num(apTokens[L8_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[L8_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixt  = dollar2Num(apTokens[L8_FIXTURESVALUE]);
   long lFixtRP= dollar2Num(apTokens[L8_FIXTURESRP]);
   long lPers  = dollar2Num(apTokens[L8_PPVALUE]);
   long lPP_MH = dollar2Num(apTokens[L8_MHPPVALUE]);
   lTmp = lFixt+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = dollar2Num(apTokens[L8_HOX]);
   long lExe2 = dollar2Num(apTokens[L8_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  

   iRet = 1;
   if (lExe1 > 0)
   {
      pLienExtr->acHO[0] = '1';      // 'Y'
      memcpy(pLienExtr->extra.MB.ExeCode1, "E01", 3);
      iRet = 2;
   } else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L8_OTHEREXEMPTIONCODE] > ' ')
   {
      if (iRet == 1)
         vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L8_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
      else
         vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L8_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
   }

   // Taxability
   lTmp = atoin(apTokens[L8_TAXABILITYFULL], 3);
   if (lTmp > 0)
   {
      // Prop 8 
      if (lTmp > 799 && lTmp < 900)
         pLienExtr->SpclFlag = LX_PROP8_FLG;
      vmemcpy(pLienExtr->acTaxCode, apTokens[L8_TAXABILITYFULL], SIZ_LIEN_TAXCODE);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

// 2023 - YOL
int MB17_ExtrTR601Rec(char *pOutbuf, char *pRollRec)
{
   int      iRet;
   ULONG    lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L17_CURRENTDOCNUM)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L17_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L17_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[L17_ASMT], strlen(apTokens[L17_ASMT]));

   // TRA
   lTmp = atol(apTokens[L17_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   ULONG lLand = atol(apTokens[L17_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   ULONG lImpr = atol(apTokens[L17_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   ULONG lFixt  = atol(apTokens[L17_FIXTURESVALUE]);
   ULONG lFixtRP= atol(apTokens[L17_FIXTURESRP]);
   ULONG lGrow  = atol(apTokens[L17_GROWINGVALUE]);
   ULONG lPers  = atol(apTokens[L17_PPVALUE]);
   ULONG lPP_MH = atol(apTokens[L17_MHPPVALUE]);
   lTmp = lFixt+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.FixtureRP, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   ULONG lExe1 = atol(apTokens[L17_HOX]);
   ULONG lExe2 = atol(apTokens[L17_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  

   iRet = 1;
   if (lExe1 > 0)
   {
      pLienExtr->acHO[0] = '1';      // 'Y'
      memcpy(pLienExtr->extra.MB.ExeCode1, "E01", 3);
      iRet = 2;
   } else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L17_OTHEREXEMPTIONCODE] > ' ')
   {
      if (iRet == 1)
         vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L17_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
      else
         vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L17_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
   }

   // Taxability
   lTmp = atoln(apTokens[L17_TAXABILITYFULL], 3);
   if (lTmp > 0)
   {
      // Prop 8 
      if (lTmp > 799 && lTmp < 900)
         pLienExtr->SpclFlag = LX_PROP8_FLG;
      vmemcpy(pLienExtr->acTaxCode, apTokens[L17_TAXABILITYFULL], SIZ_LIEN_TAXCODE);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************* MB_ExtrTR601 *******************************
 *
 * Extract lien data from AGENCYCDCURRSEC_TR601.TAB
 *
 ****************************************************************************/

int MB_ExtrTR601(LPCSTR pCnty, LPCSTR pLDRFile, int iChkLastChar)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;
   bool     bRemHyphen=false;

   LogMsg0("Extract lien roll for %s, group %d", pCnty, iLdrGrp);

   // Check for special case
   GetIniString(pCnty, "RemHyphen", "", acBuf, _MAX_PATH, acIniFile);
   if (acBuf[0] == 'Y')
      bRemHyphen = true;

   // Open lien file
   if (!pLDRFile)
   {
      GetIniString(pCnty, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
      if (!acBuf[0])
         return -1;
   } else
      strcpy(acBuf, pLDRFile);

   LogMsg("Open Lien Date Roll file %s", acBuf);
   sprintf(acTmpFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Sort on ASMT
   if (iLdrGrp == 3 || iLdrGrp == 4 || iLdrGrp == 5 || iLdrGrp == 8 || iLdrGrp == 17)
   {
      if (cLdrSep == 9)
         sprintf(acRec, "S(#%d,C,A) DEL(9)", L3_ASMT+1);
      else
         sprintf(acRec, "S(#%d,C,A)", L3_ASMT+1);
   } else
      strcpy(acRec, "S(1,12,C,A)");

   strcat(acRec, " OMIT(2,1,C,LT,\"0\",OR,2,1,C,GT,\"9\")");

   iRet = sortFile(acBuf, acTmpFile, acRec);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acTmpFile);
      return -2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Get 1st rec
   do
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   } while (pTmp && *pTmp < '0');

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Replace null char with space
      iRet = replNull(acRec, ' ', 0);

      // Create new record
      switch (iLdrGrp)
      {
         case 1:
            iRet = MB1_ExtrTR601Rec(acBuf, acRec);
            break;
         case 2:
            iRet = MB2_ExtrTR601Rec(acBuf, acRec);
            break;
         case 3:
            iRet = MB3_ExtrTR601Rec(acBuf, acRec, bRemHyphen);
            break;
         case 31:
            iRet = MB3_ExtrTR601Rec(acBuf, acRec, bRemHyphen);
            break;
         case 4:
            iRet = MB4_ExtrTR601Rec(acBuf, acRec);
            break;
         case 5:
            iRet = MB5_ExtrTR601Rec(acBuf, acRec, bRemHyphen);
            break;
         case 8:
            iRet = MB8_ExtrTR601Rec(acBuf, acRec, bRemHyphen);
            break;
         case 17:
            iRet = MB17_ExtrTR601Rec(acBuf, acRec);
            break;
         default:
            LogMsg("***** Unknown LDRGRP: %d", iLdrGrp);
            iRet = -1;
            goto ExtrTR601_Exit;
      }
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%d", lCnt);

      // Get next roll record
      if (!iChkLastChar)
         pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      else if (iChkLastChar == LAST_CHAR_N)
      {
         iRet = myGetStrDC(acRec, MAX_RECSIZE, fdRoll);
         if (iRet < 1)
            break;
      }

      // EOF
      if (acRec[1] > '9')
         break;
   }
   iRet = 0;

ExtrTR601_Exit:

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return iRet;
}

/******************************* MB_ExtrTC601 *******************************
 *
 * Extract lien data from CURRENT SECURED.07.06.16.TC601.txt (MER)
 * or AGENCYCDCURRSEC_TC601.txt (AMA)
 *
 ****************************************************************************/

int MB_ExtrTC601Rec(char *pOutbuf, char *pRollRec, bool bRemHyphen)
{
   int      iRet, lTmp;
   char     acTmp[64], acApn[32];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < V_PRIORNETVALUE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[V_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[V_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Remove hyphen from APN
   if (bRemHyphen)
      remChar(apTokens[V_ASMT], '-');

   // Copy APN
   iRet = iApnLen-strlen(apTokens[V_ASMT]);
   sprintf(acApn, "%.*s%s", iRet, "000", apTokens[V_ASMT]);
   memcpy(pLienExtr->acApn, acApn, iApnLen);

   // Year assessed
   vmemcpy(pLienExtr->acYear, apTokens[V_TAXYEAR], 4);

   // Land
   long lLand = atoi(apTokens[V_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[V_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixt  = atoi(apTokens[V_CURRENTFIXEDIMPRVALUE]);
   long lGrow  = atoi(apTokens[V_CURRENTGROWINGIMPRVALUE]);
   long lPers  = atoi(apTokens[V_CURRENTPERSONALPROPVALUE]);
   long lPP_MH = atoi(apTokens[V_CURRENTPERSONALPROPMHVALUE]);
   lTmp = lFixt+lGrow+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[V_CURRENTEXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[V_CURRENTEXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[V_CURRENTEXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  

   if (!memcmp(apTokens[V_CURRENTEXEMPTIONCODE1], "E01", 3))
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Save exemption code
   vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[V_CURRENTEXEMPTIONCODE1], SIZ_LIEN_EXECODEX);
   vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[V_CURRENTEXEMPTIONCODE2], SIZ_LIEN_EXECODEX);
   vmemcpy(pLienExtr->extra.MB.ExeCode3, apTokens[V_CURRENTEXEMPTIONCODE3], SIZ_LIEN_EXECODEX);

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int MB_ExtrTC601(LPCSTR pCnty, LPCSTR pLDRFile, int iChkLastChar, bool bSortInput)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;
   bool     bRemHyphen=false;

   // Check for special case
   GetIniString(pCnty, "RemHyphen", "", acBuf, _MAX_PATH, acIniFile);
   if (acBuf[0] == 'Y')
      bRemHyphen = true;

   LogMsg0("Extract LDR Value for %s", pCnty);

   // Open lien file
   LogMsg("Open Lien Date Roll file %s", pLDRFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Value.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Sort on ASMT
   if (bSortInput)
   {
      sprintf(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") DEL(%d)", cLdrSep);
      iRet = sortFile((char *)pLDRFile, acTmpFile, acRec);
      if (iRet < 1)
         return -1;
   } else
      strcpy(acTmpFile, pLDRFile);

   // Open ValueFile
   fdValue = fopen(acTmpFile, "r");
   if (fdValue == NULL)
   {
      LogMsg("***** Error opening sorted LDR value file: %s\n", acTmpFile);
      return -2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Get 1st rec
   do
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdValue);
   } while (pTmp && !isdigit(*pTmp));

   // Merge loop
   while (pTmp && !feof(fdValue))
   {
      // Replace null char with space
      iRet = replNull(acRec, ' ', 0);

      // Create new record
      iRet = MB_ExtrTC601Rec(acBuf, acRec, bRemHyphen);
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      if (!iChkLastChar)
         pTmp = fgets(acRec, MAX_RECSIZE, fdValue);
      else if (iChkLastChar == LAST_CHAR_N)
      {
         iRet = myGetStrDC(acRec, MAX_RECSIZE, fdValue);
         if (iRet < 1)
            break;
      }
   }
   iRet = 0;

   // Close files
   if (fdValue)
      fclose(fdValue);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return iRet;
}

/******************************* MB_ConvChar() ******************************
 *
 * This version is to replace convertChar().
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 * Note: do not use this function for MON, SHA & LAK.
 *
 ****************************************************************************/

int MB_ConvChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acOutBuf[1024], acInBuf[1024], acTmpFile[256], acTmp[256], *pRec;
   int      iRet, iTmp, iCnt=0;
   MB_CHAR  *pCharRec = (MB_CHAR *)&acOutBuf;

   LogMsg("\nConverting char file %s\n", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   iTmp = 0;
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acInBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acInBuf, 1024, fdIn);

      if (!pRec)
         break;
#ifdef _DEBUG
      //if (!memcmp(acBuf, "004050042000", 9))
      //   iRet = 0;
#endif

      iRet = ParseStringNQ(pRec, cDelim, MB_CHAR_HASWELL+1, apTokens);
      if (iRet < MB_CHAR_HASWELL)
      {
         if (iRet > 1)
            LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset(acOutBuf, ' ', sizeof(MB_CHAR));
      memcpy(pCharRec->Asmt, apTokens[MB_CHAR_ASMT], strlen(apTokens[MB_CHAR_ASMT]));

      iTmp = atoi(apTokens[MB_CHAR_POOLS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->NumPools, acTmp, iRet);
      } else if (*apTokens[MB_CHAR_POOLS] >= 'A')
      {
         blankRem(apTokens[MB_CHAR_POOLS]);
         iTmp = strlen(apTokens[MB_CHAR_POOLS]);
         if (iTmp > MBSIZ_CHAR_POOLS) iTmp = MBSIZ_CHAR_POOLS;
         memcpy(pCharRec->NumPools, apTokens[MB_CHAR_POOLS], iTmp);
      }

      blankRem(apTokens[MB_CHAR_USECAT]);
      memcpy(pCharRec->LandUseCat, apTokens[MB_CHAR_USECAT], strlen(apTokens[MB_CHAR_USECAT]));

      blankRem(apTokens[MB_CHAR_QUALITY]);
      memcpy(pCharRec->QualityClass, apTokens[MB_CHAR_QUALITY], strlen(apTokens[MB_CHAR_QUALITY]));

      iTmp = atoi(apTokens[MB_CHAR_YRBLT]);
      if (iTmp > 1600 && iTmp <= lLienYear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YearBuilt, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB_CHAR_BLDGSQFT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->BuildingSize, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB_CHAR_GARSQFT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->SqFTGarage, acTmp, iRet);
      }

      blankRem(apTokens[MB_CHAR_HEATING]);
      memcpy(pCharRec->Heating, apTokens[MB_CHAR_HEATING], strlen(apTokens[MB_CHAR_HEATING]));
      blankRem(apTokens[MB_CHAR_COOLING]);
      memcpy(pCharRec->Cooling, apTokens[MB_CHAR_COOLING], strlen(apTokens[MB_CHAR_COOLING]));
      blankRem(apTokens[MB_CHAR_HEATING_SRC]);
      memcpy(pCharRec->HeatingSource, apTokens[MB_CHAR_HEATING_SRC], strlen(apTokens[MB_CHAR_HEATING_SRC]));
      blankRem(apTokens[MB_CHAR_COOLING_SRC]);
      memcpy(pCharRec->CoolingSource, apTokens[MB_CHAR_COOLING_SRC], strlen(apTokens[MB_CHAR_COOLING_SRC]));

      iTmp = atoi(apTokens[MB_CHAR_BEDS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->NumBedrooms, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB_CHAR_FBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->NumFullBaths, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB_CHAR_HBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->NumHalfBaths, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB_CHAR_FP]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->NumFireplaces, acTmp, iRet);
      }

      memcpy(pCharRec->FeeParcel, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));

      blankRem(apTokens[MB_CHAR_HASSEPTIC]);
      if (*(apTokens[MB_CHAR_HASSEPTIC]) > '0')
         pCharRec->HasSeptic = 'Y';

      blankRem(apTokens[MB_CHAR_HASSEWER]);
      if (*(apTokens[MB_CHAR_HASSEWER]) > '0')
         pCharRec->HasSewer = 'Y';

      blankRem(apTokens[MB_CHAR_HASWELL]);
      if (*(apTokens[MB_CHAR_HASWELL]) > '0')
         pCharRec->HasWell = 'Y';

      pCharRec->CRLF[0] = '\n';
      pCharRec->CRLF[1] = '\0';
      fputs(acOutBuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
      strcpy(pInfile, acCChrFile);
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/****************************** MB_ConvStdChar *******************************
 *
 * Convert MB chars layout to STDCHAR format.
 * For BUT
 *
 *****************************************************************************/

int MB_ConvStdChar(char *pInfile, XLAT_CODE *paHeating, XLAT_CODE *paCooling, XLAT_CODE *paPool, XLAT_CODE *paQual)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmpFile[256], acTmp[256], acCode[32], *pRec;
   int      iRet, iTmp, iCmp, iCnt=0;
   STDCHAR  myCharRec;

   XLAT_CODE *pHeating, *pCooling, *pPool, *pQual;

   LogMsg("\nConverting char file %s\n", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   iTmp = 0;
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);

      if (!pRec)
         break;
#ifdef _DEBUG
      //if (!memcmp(acBuf, "004441011000", 9))
      //   iRet = 0;
#endif

      if (cDelim == ',')
         iRet = ParseStringNQ(pRec, cDelim, MB_CHAR_HASWELL+1, apTokens);
      else
         iRet = ParseStringIQ(pRec, cDelim, MB_CHAR_HASWELL+1, apTokens);
      if (iRet != MB_CHAR_HASWELL+1)
      {
         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));

      // Apn
      memcpy(myCharRec.Apn, apTokens[MB_CHAR_ASMT], strlen(apTokens[MB_CHAR_ASMT]));
      
      // Fee parcel
      memcpy(myCharRec.FeeParcel, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));
      
      // Pool/Spa
      if (*apTokens[MB_CHAR_POOLS] > ' ')
      {
         if (!paPool)
         {
            myCharRec.Pool[0] = *apTokens[MB1_CHAR_POOLS];
         } else
         {
            pPool = paPool;
            iTmp = 0;
            iCmp = -1;
            while (pPool->iLen > 0 && (iCmp=memcmp(apTokens[MB_CHAR_POOLS], pPool->acSrc, pPool->iLen)))
               pPool++;

            if (!iCmp)
               myCharRec.Pool[0] = pPool->acCode[0];
         }
      }

      // Use cat
      iTmp = blankRem(apTokens[MB_CHAR_USECAT]);
      memcpy(myCharRec.LandUseCat, apTokens[MB_CHAR_USECAT], iTmp);

      // Quality Class
      iTmp = remChar(apTokens[MB_CHAR_QUALITY], ' ');
      if (iTmp > 0)
      {
         _strupr(apTokens[MB_CHAR_QUALITY]);
         memcpy(myCharRec.QualityClass, apTokens[MB_CHAR_QUALITY], iTmp);
         strcpy(acTmp, apTokens[MB_CHAR_QUALITY]);

         if (paQual)
         {
            pQual = paQual;
            iCmp = -1;
            while (pQual->iLen > 0 && (iCmp=memcmp(acTmp, pQual->acSrc, pQual->iLen)) > 0)
               pQual++;

            if (!iCmp)
               myCharRec.BldgQual = pQual->acCode[0];
         }
         if (myCharRec.BldgQual == ' ')
         {
            acCode[0] = 0;
            if (isalpha(acTmp[0]) && isdigit(acTmp[1]))
            {
               myCharRec.BldgClass = acTmp[0];
               if (isdigit(acTmp[1]))
                  iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
               else if (isdigit(acTmp[2]))
                  iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
            } else if (acTmp[0] > '0' && acTmp[0] <= '9')
               iRet = Quality2Code(acTmp, acCode, NULL);

            if (acCode[0] > '0')
               myCharRec.BldgQual = acCode[0];
         }
      }

      // YrBlt
      iTmp = atoi(apTokens[MB_CHAR_YRBLT]);
      if (iTmp > 1600 && iTmp != 1900)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // BldgSqft
      long lBldgSqft = atoi(apTokens[MB_CHAR_BLDGSQFT]);
      if (lBldgSqft > 100)
      {
         iRet = sprintf(acTmp, "%d", lBldgSqft);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // GarSqft
      iTmp = atoi(apTokens[MB_CHAR_GARSQFT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
      }

      // Heating
      if (*apTokens[MB_CHAR_HEATING] > ' ' && paHeating)
      {
         pHeating = paHeating;
         iCmp = -1;
         while (pHeating->iLen > 0 && (iCmp=memcmp(apTokens[MB_CHAR_HEATING], pHeating->acSrc, pHeating->iLen)) > 0)
            pHeating++;

         if (!iCmp)
            myCharRec.Heating[0] = pHeating->acCode[0];
      }

      // Cooling 
      if (*apTokens[MB_CHAR_COOLING] > ' ' && paCooling)
      {
         pCooling = paCooling;
         iCmp = -1;
         while (pCooling->iLen > 0 && (iCmp=memcmp(apTokens[MB_CHAR_COOLING], pCooling->acSrc, pCooling->iLen)) > 0)
            pCooling++;

         if (!iCmp)
            myCharRec.Cooling[0] = pCooling->acCode[0];
      }

      // Heating src
      if (*apTokens[MB_CHAR_HEATING_SRC] > ' ')
      {
         iTmp = remChar(apTokens[MB_CHAR_HEATING_SRC], ' ');
         memcpy(myCharRec.HeatingSrc, apTokens[MB_CHAR_HEATING_SRC], (iTmp>sizeof(myCharRec.HeatingSrc)?sizeof(myCharRec.HeatingSrc):iTmp));

         /*
         iTmp = 0;
         iCmp = -1;
         while (asHeatingSrc[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[MB_CHAR_HEATING_SRC], asHeatingSrc[iTmp].acSrc, asHeatingSrc[iTmp].iLen)))
            iTmp++;

         if (!iCmp)
            myCharRec.Heating[0] = asHeatingSrc[iTmp].acCode[0];
         */
      }

      // Cooling src
      if (*apTokens[MB_CHAR_COOLING_SRC] > ' ')
      {
         iTmp = remChar(apTokens[MB_CHAR_COOLING_SRC], ' ');
         memcpy(myCharRec.CoolingSrc, apTokens[MB_CHAR_COOLING_SRC], (iTmp>sizeof(myCharRec.CoolingSrc)?sizeof(myCharRec.CoolingSrc):iTmp));

         /*
         iTmp = 0;
         iCmp = -1;
         while (asCooling[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[MB_CHAR_COOLING_SRC], asCoolingSrc[iTmp].acSrc, asCoolingSrc[iTmp].iLen)))
            iTmp++;

         if (!iCmp)
            myCharRec.Cooling[0] = asCoolingSrc[iTmp].acCode[0];
         */
      }

      // Beds
      iTmp = atoi(apTokens[MB_CHAR_BEDS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Baths
      iTmp = atoi(apTokens[MB_CHAR_FBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half baths
      iTmp = atoi(apTokens[MB_CHAR_HBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // Fireplace
      iTmp = atoi(apTokens[MB_CHAR_FP]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Fireplace, acTmp, iRet);
      }

      // Sewer
      blankRem(apTokens[MB_CHAR_HASSEPTIC]);
      if (*(apTokens[MB_CHAR_HASSEPTIC]) > '0')
      {
         myCharRec.HasSeptic = 'Y';
         myCharRec.HasSewer = 'S';
      } else
      {
         blankRem(apTokens[MB_CHAR_HASSEWER]);
         if (*(apTokens[MB_CHAR_HASSEWER]) > '0')
            myCharRec.HasSewer = 'Y';
      }

      // Water
      blankRem(apTokens[MB_CHAR_HASWELL]);
      if (*(apTokens[MB_CHAR_HASWELL]) > '0')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      try {
         fputs((char *)&myCharRec.Apn[0], fdOut);
      } catch (...)
      {
         LogMsg("??? Exception occurs in MB_ConvStdChar(),  APN=%s", apTokens[MB_CHAR_ASMT]);
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/***************************** MB1_ConvStdChar *******************************
 *
 * Convert MB1 chars layout to STDCHAR format.
 * For PLU
 *
 *****************************************************************************/

int MB1_ConvStdChar(char *pInfile, XLAT_CODE *paHeating, XLAT_CODE *paCooling, XLAT_CODE *paPool, XLAT_CODE *paQual)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmpFile[256], acTmp[256], acCode[32], *pRec;
   int      iRet, iTmp, iCmp, iCnt=0;
   STDCHAR  myCharRec;

   XLAT_CODE *pHeating, *pCooling, *pPool, *pQual;

   LogMsg("\nConverting char file %s\n", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   iTmp = 0;
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);

      if (!pRec)
         break;
#ifdef _DEBUG
      //if (!memcmp(acBuf, "005440006000", 9))
      //   iRet = 0;
#endif

      if (cDelim == '|')
         iRet = ParseStringIQ(pRec, cDelim, MB1_CHAR_HASWELL+1, apTokens);
      else
         iRet = ParseStringNQ(pRec, cDelim, MB1_CHAR_HASWELL+1, apTokens);
      if (iRet < MB1_CHAR_HASWELL)
      {
         if (iRet > 1)
            LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[MB1_CHAR_ASMT], strlen(apTokens[MB1_CHAR_ASMT]));

      // Numpools
      if (*apTokens[MB1_CHAR_POOLS] > ' ')
      {
         if (!paPool)
         {
            myCharRec.Pool[0] = *apTokens[MB1_CHAR_POOLS];
         } else
         {
            pPool = paPool;
            iTmp = 0;
            iCmp = -1;
            while (pPool->iLen > 0 && (iCmp=memcmp(apTokens[MB1_CHAR_POOLS], pPool->acSrc, pPool->iLen)))
               pPool++;

            if (!iCmp)
               myCharRec.Pool[0] = pPool->acCode[0];
         }
      }

      blankRem(apTokens[MB1_CHAR_USECAT]);
      memcpy(myCharRec.LandUseCat, apTokens[MB1_CHAR_USECAT], strlen(apTokens[MB1_CHAR_USECAT]));

      // QualityClass
      iTmp = remChar(apTokens[MB_CHAR_QUALITY], ' ');
      if (iTmp > 0)
      {
         _strupr(apTokens[MB_CHAR_QUALITY]);
         memcpy(myCharRec.QualityClass, apTokens[MB_CHAR_QUALITY], iTmp);
         strcpy(acTmp, apTokens[MB_CHAR_QUALITY]);

         if (paQual)
         {
            pQual = paQual;
            iCmp = -1;
            while (pQual->iLen > 0 && (iCmp=memcmp(acTmp, pQual->acSrc, pQual->iLen)) > 0)
               pQual++;

            if (!iCmp)
               myCharRec.BldgQual = pQual->acCode[0];
         }
         if (myCharRec.BldgQual == ' ')
         {
            acCode[0] = 0;
            if (isalpha(acTmp[0]) && isdigit(acTmp[1]))
            {
               myCharRec.BldgClass = acTmp[0];
               if (isdigit(acTmp[1]))
                  iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
               else if (isdigit(acTmp[2]))
                  iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
            } else if (acTmp[0] > '0' && acTmp[0] <= '9')
               iRet = Quality2Code(acTmp, acCode, NULL);

            if (acCode[0] > '0')
               myCharRec.BldgQual = acCode[0];
         }
      }

      // Yrblt
      iTmp = atoi(apTokens[MB1_CHAR_YRBLT]);
      if (iTmp > 1600 && iTmp <= lLienYear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB1_CHAR_BLDGSQFT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB1_CHAR_GARSQFT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
      }

      // Heating
      if (*apTokens[MB1_CHAR_HEATING] > ' ')
      {
         if (!paHeating)
         {
            iTmp = strlen(apTokens[MB1_CHAR_HEATING]);
            memcpy(myCharRec.Heating, apTokens[MB1_CHAR_HEATING], (iTmp>sizeof(myCharRec.Heating)?sizeof(myCharRec.Heating):iTmp)); 
         } else
         {
            pHeating = paHeating;
            iCmp = -1;
            while (pHeating->iLen > 0 && (iCmp=memcmp(apTokens[MB1_CHAR_HEATING], pHeating->acSrc, pHeating->iLen)))
               pHeating++;

            if (!iCmp)
               myCharRec.Heating[0] = pHeating->acCode[0];
         }
      }

      // Cooling 
      if (*apTokens[MB1_CHAR_COOLING] > ' ')
      {
         if (!paCooling)
         {
            iTmp = strlen(apTokens[MB1_CHAR_COOLING]);
            memcpy(myCharRec.Cooling, apTokens[MB1_CHAR_COOLING], (iTmp>sizeof(myCharRec.Cooling)?sizeof(myCharRec.Cooling):iTmp)); 
         } else
         {
            pCooling = paCooling;
            iCmp = -1;
            while (pCooling->iLen > 0 && (iCmp=memcmp(apTokens[MB1_CHAR_COOLING], pCooling->acSrc, pCooling->iLen)))
               pCooling++;

            if (!iCmp)
               myCharRec.Cooling[0] = pCooling->acCode[0];
         }
      }

      // HeatingSrc/CoolingSrc
      iTmp = blankRem(apTokens[MB1_CHAR_HEATING_SRC]);
      memcpy(myCharRec.HeatingSrc, apTokens[MB1_CHAR_HEATING_SRC], (iTmp>sizeof(myCharRec.HeatingSrc)?sizeof(myCharRec.HeatingSrc):iTmp));
      iTmp = blankRem(apTokens[MB1_CHAR_COOLING_SRC]);
      memcpy(myCharRec.CoolingSrc, apTokens[MB1_CHAR_COOLING_SRC], (iTmp>sizeof(myCharRec.CoolingSrc)?sizeof(myCharRec.CoolingSrc):iTmp));

      iTmp = atoi(apTokens[MB1_CHAR_BEDS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB1_CHAR_FBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB1_CHAR_HBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      iTmp = atoi(apTokens[MB1_CHAR_FP]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Fireplace, acTmp, iRet);
      }

      memcpy(myCharRec.FeeParcel, apTokens[MB1_CHAR_FEE_PRCL], strlen(apTokens[MB1_CHAR_FEE_PRCL]));

      // Sewer
      blankRem(apTokens[MB1_CHAR_HASSEPTIC]);
      if (*(apTokens[MB1_CHAR_HASSEPTIC]) == '1' || *(apTokens[MB1_CHAR_HASSEPTIC]) == 'T')
      {
         myCharRec.HasSeptic = 'Y';
         myCharRec.HasSewer = 'S';
      } else
      {
         blankRem(apTokens[MB1_CHAR_HASSEWER]);
         if (*(apTokens[MB1_CHAR_HASSEWER]) > '0')
            myCharRec.HasSewer = 'Y';
      }

      // Water
      blankRem(apTokens[MB1_CHAR_HASWELL]);
      if (*(apTokens[MB1_CHAR_HASWELL]) > '0')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Pools
      iTmp = atoi(apTokens[MB1_CHAR_POOLS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Pool, acTmp, iRet);
      }

      // Stories
      iTmp = atoi(apTokens[MB1_CHAR_NUMFLOORS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }


      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/******************************* MB_MergeStdChar *****************************
 *
 * Merge ???_Char.dat in STDCHAR format
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the one that has value.
 *   - Used by PLU, SON
 *   - If XLAT_CODE tables are not provided, copy data as is.
 *   - Update data only, don't wipe out existing data if no new value.
 *
 *****************************************************************************/

int MB_MergeStdChar(char *pOutbuf, XLAT_CODE *paHeating, XLAT_CODE *paCooling, XLAT_CODE *paPool)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft, lTmp;
   int      iLoop, iBeds, iFBath, iHBath, iBldgNum, iTmp, iCmp;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Yrblt
   lTmp = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // YrEff
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      if (pChar->ParkType[0] > ' ')
         *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];  
      else
         *(pOutbuf+OFF_PARK_TYPE) = 'Z';  
   }

   // Patio Sqft - PLU
   lTmp = atoin(pChar->PatioSqft, SIZ_CHAR_SQFT);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*d", SIZ_PATIO_SF, lTmp);
      memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
   }

   // LotSqft
   long lLotSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lLotSqft > 100)
   {
      memcpy(pOutbuf+OFF_LOT_SQFT, pChar->LotSqft, SIZ_LOT_SQFT);
      memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_ACRES);
   } 

   // Heating
   if (pChar->Heating[0] > ' ')
   {
      if (paHeating)
      {
         iTmp = 0;
         iCmp = -1;
         while (paHeating->iLen > 0 && (iCmp=memcmp(pChar->Heating, paHeating->acSrc, paHeating->iLen)) > 0)
            paHeating++;

         if (!iCmp)
            *(pOutbuf+OFF_HEAT) = paHeating->acCode[0];
         else
            *(pOutbuf+OFF_HEAT) = ' ';
      } else
         *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   } else
      *(pOutbuf+OFF_HEAT) = ' ';
      
   // Cooling 
   if (pChar->Cooling[0] > ' ')
   {
      if (paCooling)
      {
         iTmp = 0;
         iCmp = -1;
         while (paCooling->iLen > 0 && (iCmp=memcmp(pChar->Cooling, paCooling->acSrc, paCooling->iLen)) > 0)
            paCooling++;

         if (!iCmp)
            *(pOutbuf+OFF_AIR_COND) = paCooling->acCode[0];
         else
            *(pOutbuf+OFF_AIR_COND) = ' ';
      } else
         *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];
   } else
      *(pOutbuf+OFF_AIR_COND) = ' ';

   // Pools
   if (pChar->Pool[0] > ' ')
   {
      if (!paPool)
         *(pOutbuf+OFF_POOL) = pChar->Pool[0];
      else
      {
         iTmp = 0;
         iCmp = -1;
         while (paCooling->iLen > 0 && (iCmp=memcmp(pChar->Cooling, paCooling->acSrc, paCooling->iLen)) > 0)
            paCooling++;

         if (!iCmp)
            *(pOutbuf+OFF_POOL) = paCooling->acCode[0];
         else
            *(pOutbuf+OFF_POOL) = pChar->Pool[0];
      }
   } else
      *(pOutbuf+OFF_POOL) = ' ';

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   }

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   }

   // Fireplace
   if (pChar->Fireplace[0] > ' ')
      memcpy(pOutbuf+OFF_FIRE_PL, pChar->Fireplace, SIZ_FIRE_PL);
   else
      memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);

   // HasSeptic or HasSewer
   *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Units
   iTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   } else
      memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

   // Stories
   if (pChar->Stories[0] > ' ')
      memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
   else
      memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

   // Has Elevator
   if (pChar->HasElevator > '0')
      *(pOutbuf+OFF_ELEVATOR) = pChar->HasElevator;
   else
      *(pOutbuf+OFF_ELEVATOR) = ' ';

   // BldgNum
   iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/********************************* sqlConnect *******************************
 *
 * Used by MergeBut.cpp
 *
 ****************************************************************************/

bool sqlConnect(LPCSTR strDb, hlAdo *phDb)
{
   bool bRet = true;
   char acServer[256], acTmp[256];

   try
   {
      // open the database connection
      //if (!m_bConnected)
      //{
         GetIniString("Database", "CntyProvider", "", acServer, 256, acIniFile);
         if (acServer[0] == '\0')
         {
            LogMsg("***** Please add CntyProvider to [Database] section in %s then try again", acIniFile);
            return false;
         }
         sprintf(acTmp, acServer, strDb);
         LogMsg("Connecting to %s", acTmp);
         bRet = phDb->Connect(acTmp);
      //}
   }

   // catch any ADO errors
   AdoCatch(e)
   {
      LogMsg("***** SQL connect error: %s", ComError(e));
      bRet = false;
   }

   return bRet;
}

/*********************************** execSqlCmd *****************************
 *
 *
 ****************************************************************************/

//int execSqlCmd(LPCTSTR strCmd)
//{
//   int iRet = 0;
//   CString strSql = strCmd;
//
//   try
//   {
//      hl.ExecuteCommand(strSql);
//   }
//
//   AdoCatch (e)
//   {
//      LogMsg("***** Error executing command [%s] : %s", strSql, ComError(e));
//      iRet = -3;
//   }
//   return iRet;
//}
//
//int execSqlCmd(LPCTSTR strCmd, hlAdo *phDb)
//{
//   int iRet = 0;
//   CString strSql = strCmd;
//
//   try
//   {
//      phDb->ExecuteCommand(strSql);
//   }
//
//   AdoCatch (e)
//   {
//      LogMsg("***** Error executing command [%s] : %s", strSql, ComError(e));
//      iRet = -3;
//   }
//   return iRet;
//}

/******************************** getCountyInfo *****************************
 *
 * Retrieve info from SQL server
 *
 ****************************************************************************/

int getCountyInfo()
{
   char  acTmp[256], acServer[256];
   int   iRet;

   GetIniString("Database", "Provider", "", acServer, 128, acIniFile);
   if (acServer[0] == '\0')
      return -1;

   try
   {
      // open the database connection
      if (!m_bConnected)
         m_bConnected = hl.Connect(acServer);

      sprintf(acTmp, "SELECT * FROM County WHERE CountyCode='%s'", myCounty.acCntyCode);
      m_AdoRs.Open(hl, acTmp);

      if (m_AdoRs.next())
      {
         CString	sTmp = m_AdoRs.GetItem("LastRecCount");
         myCounty.iLastRecCnt = atoi(sTmp);
         sTmp = m_AdoRs.GetItem("GrGr_Delay");
         myCounty.iGrGrDelay = atoi(sTmp);
         sTmp = m_AdoRs.GetItem("GrGr_Freq");
         myCounty.GrGrFreq = sTmp.GetAt(0);
         sTmp = m_AdoRs.GetItem("Roll_Delay");
         myCounty.iRollDelay = atoi(sTmp);
         sTmp = m_AdoRs.GetItem("Roll_Freq");
         myCounty.RollFreq = sTmp.GetAt(0);

         sTmp = m_AdoRs.GetItem("MaxUpdate");
         myCounty.iMaxUpdate = atoi(sTmp);
         m_AdoRs.Close();
      }
      iRet = 0;
   } AdoCatch(e)
   {
      LogMsg("***** Error exec cmd: %s", acServer);
      LogMsgD("%s\n", ComError(e));
      iRet = -1;
   }

   return iRet;
}

/********************************* updateTable ******************************
 *
 * Update LastBldDate, LastRecDate, RecCount, State
 *
 ****************************************************************************/

bool updateTable(LPCTSTR strCmd)
{
   bool bRet = false;
   char acServer[256];

   GetIniString("Database", "Provider", "", acServer, 128, acIniFile);
   if (acServer[0] == '\0')
      return bRet;

   LogMsg((LPSTR)strCmd);
   try
   {
      // open the database connection
      if (!m_bConnected)
         m_bConnected = hl.Connect(acServer);
   }

   // catch any ADO errors
   AdoCatch(e)
   {
      LogMsg("%s", ComError(e));
      return bRet;
   }

   // Update production tables
   int iRet;
   iRet = execSqlCmd(strCmd, NULL);
   if (!iRet)
      bRet = true;
   return bRet;
}

/******************************** LoadCountyInfo ******************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int LoadCountyInfo(char *pCntyCode, char *pCntyTbl)
{
   char  acTmp[_MAX_PATH], *pTmp, *pFlds[MAX_CNTY_FLDS];
   int   iRet=0, iTmp;
   FILE  *fd;

   fd = fopen(pCntyTbl, "r");
   if (fd)
   {
      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
         {
            if (!_memicmp(acTmp, pCntyCode, 3))
            {
               iTmp = ParseString(myTrim(acTmp), ',', MAX_CNTY_FLDS, pFlds);
               if (iTmp > 0)
               {
                  strcpy(myCounty.acCntyCode, pCntyCode);
                  strcpy(myCounty.acStdApnFmt, pFlds[FLD_APN_FMT]);
                  strcpy(myCounty.acSpcApnFmt[0], pFlds[FLD_SPC_FMT]);
                  strcpy(myCounty.acCase[0], pFlds[FLD_SPC_BOOK]);
                  strcpy(myCounty.acCntyID, pFlds[FLD_CNTY_ID]);
                  strcpy(myCounty.acCntyName, pFlds[FLD_CNTY_NAME]);
                  if (iTmp > FLD_YR_ASSD+2)
                  {
                     strcpy(myCounty.acSpcApnFmt[1], pFlds[FLD_SPC_FMT+2]);
                     strcpy(myCounty.acCase[1], pFlds[FLD_SPC_BOOK+2]);
                     strcpy(myCounty.acYearAssd, pFlds[FLD_YR_ASSD+2]);
                  } else
                     strcpy(myCounty.acYearAssd, pFlds[FLD_YR_ASSD]);

                  iRet = atoi(myCounty.acCntyID);
                  myCounty.iCntyID = iRet;
                  sprintf(myCounty.acFipsCode, "06%.3d", iRet*2-1);
                  myCounty.iApnLen = atoi(pFlds[FLD_APN_LEN]);
                  myCounty.iBookLen = atoi(pFlds[FLD_BOOK_LEN]);
                  myCounty.iPageLen = atoi(pFlds[FLD_PAGE_LEN]);
                  myCounty.iCmpLen = atoi(pFlds[FLD_CMP_LEN]);
                  iRet = 1;
               } else
               {
                  LogMsg("Bad county table file: %s", pCntyTbl);
               }
               break;
            }
         } else
            break;
      }

      fclose(fd);

      if (1 != iRet)
         LogMsg("County not found.  Please verify %s", pCntyTbl);
   } else
      LogMsg("Error opening county table %s", pCntyTbl);

   return iRet;
}

/********************************** MergeInit ********************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int MergeInit(char *pCnty)
{
   char  acTmp[_MAX_PATH], acCityFile[_MAX_PATH], *pTmp;
   char  acUseCodeTmpl[_MAX_PATH];
   int   iRet, iTmp;

   // System vars
   GetIniString("System", "TmpPath", "", acTmpPath, _MAX_PATH, acIniFile);
   sprintf(acTmp, "%s\\%s", acTmpPath, myCounty.acCntyCode);
   if (_access(acTmp, 0))
      _mkdir(acTmp);

   iRet = GetIniString("System", "ImportLog", "", sImportLogTmpl, _MAX_PATH, acIniFile);
   if (!iRet)
   {
      iRet = GetIniString("System", "LogPath", "", sImportLogTmpl, _MAX_PATH, acIniFile);
      strcat(sImportLogTmpl, "%s");
   }

   // Get raw file name
   GetIniString("Data", "RawFile", "", acRawTmpl, _MAX_PATH, acIniFile);
   iRecLen = GetPrivateProfileInt("Data", "RecSize", 1900, acIniFile);
   GetIniString("Data", "AsrFile", "", acRawTmplS, _MAX_PATH, acIniFile);
   iAsrRecLen = GetPrivateProfileInt(pCnty, "AsrSize", 3150, acIniFile);
   iGrGrApnLen = GetPrivateProfileInt(pCnty, "GrGrApnLen", 0, acIniFile);

   // Cum sale file
   iRet = GetIniString(pCnty, "CSalFile", "", acCSalFile, _MAX_PATH, acIniFile);
   if (!iRet)
   {
      iRet = GetIniString("Data", "CSalFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acTmp, pCnty, pCnty);
   }

   // Cum char file
   iRet = GetIniString(pCnty, "CChrFile", "", acCChrFile, _MAX_PATH, acIniFile);
   if (!iRet)
   {
      iRet = GetIniString("Data", "CChrFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acCChrFile, acTmp, pCnty, pCnty);
   }

   // Roll file
   bSetLastFileDate = true;
   if (iLoadFlag & (LOAD_LIEN|EXTR_LIEN|LOAD_ASSR))
   {
      GetIniString(pCnty, "LienFile", "", acTmp, _MAX_PATH, acIniFile);
      if (!acTmp[0])
         GetIniString("Data", "LienFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acRollFile, acTmp, pCnty, pCnty);
      if (lLienYear > 1900)
      {
         sprintf(acTmp, "%d\\", lLienYear);
         replStr(acRollFile, "[year]", acTmp);
      } else
         replStr(acRollFile, "[year]", "");

      // Make sure roll file is available
      if (_access(acRollFile, 0))
      {
         LogMsg("***** Input file is missing %s.  Please check LOADMB.INI", acRollFile);
         return -1;
      }

      if (GetIniString(pCnty, "LienVal", "", acTmp, _MAX_PATH, acIniFile) > 10)
         sprintf(acValueFile, acTmp, pCnty);
   } else
   {
      GetIniString(pCnty, "RollFile", "", acTmp, _MAX_PATH, acIniFile);
      if (!acTmp[0])
         GetIniString("Data", "RollFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acRollFile, acTmp, pCnty, pCnty);

      if (GetIniString(pCnty, "CurrVal", "", acTmp, _MAX_PATH, acIniFile) > 10)
         sprintf(acValueFile, acTmp, pCnty);
      if (iLoadFlag & LOAD_UPDT)
      {
         GetPrivateProfileString(pCnty, "SetFileDate", "Y", acTmp, _MAX_PATH, acIniFile);
         if (acTmp[0] == 'N')
            bSetLastFileDate = false;
      }
   }

   // Char file
   iRet = GetIniString(pCnty, "CharFile", "", acCharFile, _MAX_PATH, acIniFile);
   if (iRet < 10)
   {
      iRet = GetIniString("Data", "CharFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acCharFile, acTmp, pCnty, pCnty);
   }

   // Situs file
   iRet = GetIniString(pCnty, "SitusFile", "", acSitusFile, _MAX_PATH, acIniFile);
   if (iRet < 10)
   {
      iRet = GetIniString("Data", "SitusFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acSitusFile, acTmp, pCnty, pCnty);
   }

   // Sale file
   iRet = GetIniString(pCnty, "SaleFile", "", acSalesFile, _MAX_PATH, acIniFile);
   if (iRet < 10)
   {
      iRet = GetIniString("Data", "SaleFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acSalesFile, acTmp, pCnty, pCnty);
   }

   // Sale date format
   iSaleDateFmt = 0;
   iRet = GetIniString(pCnty, "SaleDateFmt", "", acTmp, _MAX_PATH, acIniFile);
   if (iRet > 1)
   {
      if (!_memicmp(acTmp, "DD_MMM_YYYY", 11))
         iSaleDateFmt = DD_MMM_YYYY;
      else if (!_memicmp(acTmp, "MM_DD_YYYY_1", 12))
         iSaleDateFmt = MM_DD_YYYY_1;
      else if (!_memicmp(acTmp, "MM_DD_YYYY_2", 12))
         iSaleDateFmt = MM_DD_YYYY_2;
      else if (!_memicmp(acTmp, "MMM_DD_YYYY", 12))
         iSaleDateFmt = MMM_DD_YYYY;
      else if (!_memicmp(acTmp, "MMDDYY1", 7))
         iSaleDateFmt = MMDDYY1;
      else if (!_memicmp(acTmp, "MMDDYY2", 7))
         iSaleDateFmt = MMDDYY2;
      else if (!_memicmp(acTmp, "YY2YYYY", 7))
         iSaleDateFmt = YY2YYYY;
      else if (!_memicmp(acTmp, "YYMMDD1", 7))
         iSaleDateFmt = YYMMDD1;
      else if (!_memicmp(acTmp, "YYMMDD2", 7))
         iSaleDateFmt = YYMMDD2;
      else if (!_memicmp(acTmp, "YYYY_MM_DD", 10))
         iSaleDateFmt = YYYY_MM_DD;
      else if (!_memicmp(acTmp, "MMDDYYYY", 8))
         iSaleDateFmt = MMDDYYYY;
      else
         LogMsg("***** Invalid sale date format: %s\n", acTmp);
   }

   // Exe file
   iRet = GetIniString(pCnty, "ExeFile", "", acExeFile, _MAX_PATH, acIniFile);
   if (iRet < 10)
   {
      iRet = GetIniString("Data", "ExeFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acExeFile, acTmp, pCnty, pCnty);
   }

   // Tax file
   iRet = GetIniString(pCnty, "TaxFile", "", acTaxFile, _MAX_PATH, acIniFile);
   if (iRet < 10)
   {
      iRet = GetIniString("Data", "TaxFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acTaxFile, acTmp, pCnty, pCnty);
   }

   iRet = GetIniString(pCnty, "NameFile", "", acNameFile, _MAX_PATH, acIniFile);
   if (iRet < 10)
   {
      iRet = GetIniString("Data", "NameFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acNameFile, acTmp, pCnty, pCnty);
   }

   if (GetIniString(pCnty, "ApprFile", "", acTmp, _MAX_PATH, acIniFile) > 10)
      sprintf(acApprFile, acTmp, pCnty);

   fdRoll = (FILE *)NULL;
   fdChar = (FILE *)NULL;
   fdSitus= (FILE *)NULL;
   fdSale= (FILE *)NULL;
   fdExe  = (FILE *)NULL;
   fdValue=fdName=fdTax=(FILE *)NULL;


   // Get GrGr file template
   iRet = GetIniString(pCnty, "GrGrOut", "", acGrGrTmpl, _MAX_PATH, acIniFile);
   if (!iRet)
      GetIniString("Data", "GrGrOut", "", acGrGrTmpl, _MAX_PATH, acIniFile);

   // GrGr backup template
   iRet = GetIniString(pCnty, "GrGrBak", "", acGrGrBakTmpl, _MAX_PATH, acIniFile);
   if (!iRet)
      GetIniString("Data", "GrGrBak", "", acGrGrBakTmpl, _MAX_PATH, acIniFile);

   // GrGr matched export template
   GetIniString(pCnty, "GrGrExp", "", acEGrGrTmpl, _MAX_PATH, acIniFile);
   if (!acEGrGrTmpl[0])
      GetIniString("Data", "GrGrExp", "", acEGrGrTmpl, _MAX_PATH, acIniFile);

   // Get Sale file template - for sale file extract
   iRet = GetIniString(pCnty, "SaleOut", "", acSaleTmpl, _MAX_PATH, acIniFile);
   if (!iRet)
      GetIniString("Data", "SaleOut", "", acSaleTmpl, _MAX_PATH, acIniFile);

   // GrGr for Data Entry template
   iRet = GetIniString(pCnty, "DEOut", "", acDETmpl, _MAX_PATH, acIniFile);
   if (!iRet)
      GetIniString("Data", "DEOut", "", acDETmpl, _MAX_PATH, acIniFile);

   // Sale export template
   iRet = GetIniString(pCnty, "SaleExp", "", acESalTmpl, _MAX_PATH, acIniFile);
   if (!iRet)
      GetIniString("Data", "SaleExp", "", acESalTmpl, _MAX_PATH, acIniFile);

   // Lien template name
   GetIniString(pCnty, "LienOut", "", acLienTmpl, _MAX_PATH, acIniFile);
   if (acLienTmpl[0] < 'A')
      GetIniString("Data", "LienOut", "", acLienTmpl, _MAX_PATH, acIniFile);

   // Value export template
   GetIniString("Data", "ValueOut", "", acValueTmpl, _MAX_PATH, acIniFile);

   // Get DocPath
   GetIniString(pCnty, "DocPath", "", acTmp, _MAX_PATH, acIniFile);
   if (!iRet)
      GetIniString("System", "DocPath", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] > ' ')
      sprintf(acDocPath, acTmp, myCounty.acCntyCode);

   // Load suffix table
   GetPrivateProfileString(pCnty, "UseSfxDev", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
   {
      bUseSfxXlat = true;
      iRet = GetIniString(pCnty, "SfxDevTbl", "", acTmp, _MAX_PATH, acIniFile);
      if (iRet < 10)
         GetIniString("System", "SfxDevTbl", "", acTmp, _MAX_PATH, acIniFile);
   } else if (acTmp[0] == 'C')
   {
      // Use custom county table
      bUseSfxXlat = true;
      GetIniString(pCnty, "SuffixTbl", ".\\Suffix_Simple.txt", acTmp, _MAX_PATH, acIniFile);
   } else if (acTmp[0] == 'S')
   {
      bUseSfxXlat = true;
      GetIniString("System", "SfxSmlTbl", ".\\Suffix_Simple.txt", acTmp, _MAX_PATH, acIniFile);
   } else
   {
      bUseSfxXlat = false;
      GetIniString("System", "SuffixTbl", ".\\Suffix.txt", acTmp, _MAX_PATH, acIniFile);
   }
   iRet = LoadSuffixTbl(acTmp, bUseSfxXlat);

   GetPrivateProfileString(pCnty, "ApnXlat", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bApnXlat = true;
   else
      bApnXlat = false;

   // Get Lookup file name
   GetIniString("System", "LookUpTbl", "", acLookupTbl, _MAX_PATH, acIniFile);

   // Get county info
   GetIniString("System", "CountyTbl", ".\\CountyInfo.csv", acTmp, _MAX_PATH, acIniFile);
   printf("Loading county table: %s\n", acTmp);
   iRet = LoadCountyInfo(pCnty, acTmp);

   if (iRet == 1)
   {
      if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
      {
         // Load city table
         GetIniString(pCnty, "CityFile", "", acCityFile, _MAX_PATH, acIniFile);
         if (acCityFile[0] < 'A')
         {
            GetIniString("Data", "CityFile", "", acTmp, _MAX_PATH, acIniFile);
            sprintf(acCityFile, acTmp, pCnty);
         }
         printf("Loading city table: %s\n", acCityFile);
         if (strstr(acCityFile, "_N2CX"))
            iRet = Load_N2CX(acCityFile);
         else
            iRet = LoadCities(acCityFile);

         // If there is problem accessing City file, file system may have problem.  You may need to reboot this server.
         if (iRet < 1)
         {
            LogMsg("***** File system may have problem. Try to reboot this server.");
            return iRet;
         }
      }

      if (!lLienYear)
         lLienYear = atol(myCounty.acYearAssd);
      else
         sprintf(myCounty.acYearAssd, "%d", lLienYear);

      strcpy(acTmp, myCounty.acYearAssd);
      strcat(acTmp, "0101");
      lLienDate = atol(acTmp);
   } else
      return iRet;

   // Load Juristion table
   if (iLoadFlag & MERG_ZONE)
   {
      GetIniString(pCnty, "JurisTbl", "", acCityFile, _MAX_PATH, acIniFile);
      if (acCityFile[0] < 'A')
      {
         GetIniString("Data", "JurisTbl", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acCityFile, acTmp, pCnty);
      }
      if (!_access(acCityFile, 0))
      {
         printf("Loading Jurisdiction table: %s\n", acCityFile);
         iRet = LoadJurisList(acCityFile);
      }
   }

   // Tax Year
   if (!lTaxYear)
      lTaxYear = GetPrivateProfileInt(pCnty, "TaxYear", lLienYear, acIniFile);

   // Get Usecode template
   GetIniString(pCnty, "IUseTbl", "", acTmp, _MAX_PATH, acIniFile);
   iNumUseCodes = 0;
   if (acTmp[0])
   {
      sprintf(acUseCodeTmpl, acTmp, pCnty);
      if (!_access(acUseCodeTmpl, 0))
         iRet = LoadIUseTbl(acUseCodeTmpl);
   } else
   {
      GetIniString("System", "UseTbl", "", acTmp, _MAX_PATH, acIniFile);
      if (acTmp[0])
      {
         sprintf(acUseTbl, acTmp, pCnty);
         if (!_access(acUseTbl, 0))
            iRet = LoadUseTbl(acUseTbl);
      }
   }

   // Get 2nd Usecode table if defined
   GetIniString(pCnty, "UseTblUseCode", "", acTmp, _MAX_PATH, acIniFile);
   iNumUseCodes1 = 0;
   if (acTmp[0])
   {
      sprintf(acUseCodeTmpl, acTmp, pCnty);
      if (!_access(acUseCodeTmpl, 0))
         iRet = LoadUseTbl(acUseCodeTmpl, &pUseTable1, &iNumUseCodes1);
   }
   
   // Set debug flag
   if (!bDebug)
   {
      GetPrivateProfileString(pCnty, "Debug", "N", acTmp, _MAX_PATH, acIniFile);
      if (acTmp[0] == 'Y')
         bDebug = true;
   }

   // Clean up flag: bClean
   GetPrivateProfileString(pCnty, "Clean", "Y", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bClean = true;
   else
      bClean = false;

   // Exclude APNs
   GetIniString(pCnty, "ExclApn", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] > '0')
   {
      if (pTmp = strchr(acTmp, '-'))
      {
         *pTmp++ = 0;
         strcpy(asExclApns[0].acApn, acTmp);
         asExclApns[0].iLen = strlen(acTmp);
         strcpy(asExclApns[1].acApn, pTmp);
         asExclApns[1].iLen = strlen(pTmp);
      }
   } else
      asExclApns[0].iLen = 0;

   // Init directions
   InitDirs();

   lSitusMatch=lCharMatch=lExeMatch=lSaleMatch=lTaxMatch=lValueMatch=0;
   lSitusSkip=lCharSkip=lExeSkip=lSaleSkip=lTaxSkip=lValueSkip=0;
   lAssrRecCnt=0;

   // Default delimiter is comma and number of header rows is 1.  This value can be changed in INI file
   // if county has other values.
   GetPrivateProfileString(pCnty, "LdrSep", "9", acTmp, _MAX_PATH, acIniFile); 
   if (acTmp[0] == '9')
      cLdrSep = 9;
   else
      cLdrSep = acTmp[0];

   GetPrivateProfileString(pCnty, "Delimiter", ",", acTmp, _MAX_PATH, acIniFile);   
   cDelim = acTmp[0];
   GetPrivateProfileString(pCnty, "ValDelim", "", acTmp, _MAX_PATH, acIniFile);   
   if (acTmp[0] == '9')
      cValDelim = 9;
   else if (acTmp[0] > ' ')
      cValDelim = acTmp[0];
   else
      cValDelim = cDelim;
   iHdrRows = GetPrivateProfileInt(pCnty, "HdrRows", 1, acIniFile);
   iSkipQuote = GetPrivateProfileInt(pCnty, "SkipQuote", 1, acIniFile);
   iValHdr = GetPrivateProfileInt(pCnty, "ValHdr", 0, acIniFile);
   iValSkipQuote = GetPrivateProfileInt(pCnty, "ValSkipQuote", 0, acIniFile);

   // Zone delimiter - PQZoning delimiter within Zone_Code
   GetPrivateProfileString("Data", "InZoneDelim", "~", acTmp, _MAX_PATH, acIniFile);   
   cInZoneDelim = acTmp[0];

   // Default LDR file group is 2, NAP is 1
   iLdrGrp = GetPrivateProfileInt(pCnty, "LdrGrp", 0, acIniFile);
   if (!iLdrGrp)
      iLdrGrp = GetPrivateProfileInt("Data", "LdrGrp", 2, acIniFile);

   // Default Tax group is 0
   iTaxGrp = GetPrivateProfileInt(pCnty, "TaxGrp", 0, acIniFile);

   // Check for new tax format
   if (iTaxGrp == 2)
   {
      lTaxFlds = T2_USERID;
      lTaxAmt1Idx = T2_TAXAMT1;
      lTaxAmt2Idx = T2_TAXAMT2;
      lTaxYearIdx = T2_TAXYEAR;
      lRollChgIdx = T2_ROLLCHGNUM;
      lPenDate1Idx = T2_PENCHRGDATE1;
      lPaidDate1Idx= T2_PAYMENTDATE1;
      lTaxabilityIdx = T2_TAXABILITY;
   } else if (iTaxGrp == 1)
   {
      lTaxFlds = T1_USERID;
      lTaxAmt1Idx = T1_TAXAMT1;
      lTaxAmt2Idx = T1_TAXAMT2;
      lTaxYearIdx = T1_TAXYEAR;
      lRollChgIdx = T1_EXISTSROLLCHG;
      lPenDate1Idx = T1_PENCHRGDATE1;
      lPaidDate1Idx= T1_PAYMENTDATE1;
      lTaxabilityIdx = T1_TAXABILITY;
   } else
   {
      lTaxFlds = MB_TAX_ROLLCAT;
      lTaxAmt1Idx = MB_TAX_TAXAMT1;
      lTaxAmt2Idx = MB_TAX_TAXAMT2;
      lTaxYearIdx = MB_TAX_YEAR;
      lRollChgIdx = MB_TAX_ROLLCHGNUM;
      lPenDate1Idx = MB_TAX_PENDATE1;
      lPaidDate1Idx= MB_TAX_PAIDDATE1;
      lTaxabilityIdx = MB_TAX_TAXABILITY;
   }

   // Check default send mail flag
   if (!bSendMail)
   {
      GetPrivateProfileString(pCnty, "SendMail", "", acTmp, _MAX_PATH, acIniFile);
      if (!acTmp[0])
         GetIniString("System", "SendMail", "", acTmp, _MAX_PATH, acIniFile);
      if (acTmp[0] == 'Y')
         bSendMail = true;
   }

   // Get max changed records allowed
   iMaxChgAllowed = GetPrivateProfileInt(pCnty, "MaxChgAllowed", 0, acIniFile);

   // Create output folder
   GetIniString("Data", "TaxOut", "", sTaxOutTmpl, _MAX_PATH, acIniFile);
   strcpy(acTmp, sTaxOutTmpl);
   pTmp = strrchr(acTmp, '\\');
   *pTmp = 0;
   sprintf(sTRITmpl, acTmp, pCnty);
   if (_access(sTRITmpl, 0))
   {
      iTmp = _mkdir(sTRITmpl);
      if (iTmp)
      {
         LogMsg("***** Error creating output folder: %s", sTRITmpl);
         return -1;
      }
   }

   // Tax file template
   GetIniString(pCnty, "Tax", "", sTaxTmpl, _MAX_PATH, acIniFile);
   if (sTaxTmpl[0] > ' ')
   {
      GetIniString(pCnty, "TaxRollInfo", "", sTRITmpl, _MAX_PATH, acIniFile);
      GetIniString(pCnty, "TaxCode", "", sTCTmpl, _MAX_PATH, acIniFile);
      GetIniString(pCnty, "TaxCodeMstr", "", sTCMTmpl, _MAX_PATH, acIniFile);
      GetIniString(pCnty, "Redemption", "", sRedTmpl, _MAX_PATH, acIniFile);
      GetIniString(pCnty, "TrustFund", "", sTFTmpl, _MAX_PATH, acIniFile);
   } else
   {
      GetIniString("Data", "TaxRollInfo", "", sTRITmpl, _MAX_PATH, acIniFile);
      GetIniString("Data", "TaxCode", "", sTCTmpl, _MAX_PATH, acIniFile);
      GetIniString("Data", "TaxCodeMstr", "", sTCMTmpl, _MAX_PATH, acIniFile);
      GetIniString("Data", "Tax", "", sTaxTmpl, _MAX_PATH, acIniFile);
      GetIniString("Data", "Redemption", "", sRedTmpl, _MAX_PATH, acIniFile);
      GetIniString("Data", "TrustFund", "", sTFTmpl, _MAX_PATH, acIniFile);
   }  

   // 03/23/2023
   GetIniString("Data", "TaxSql", "", sTaxSqlTmpl, _MAX_PATH, acIniFile);

   // Check AutoImport option
   GetPrivateProfileString(pCnty, "ImportSale", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "ImportSale", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bSaleImport = true;
   else
      bSaleImport = false;

   GetPrivateProfileString(pCnty, "ImportGrGr", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "ImportGrGr", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bGrgrImport = true;
   else
      bGrgrImport = false;

   GetPrivateProfileString(pCnty, "ImportTax", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "ImportTax", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bTaxImport = true;
   else
      bTaxImport = false;

   GetPrivateProfileString(pCnty, "MixSale", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "MixSale", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bMixSale = true;
   else
      bMixSale = false;

   GetPrivateProfileString(pCnty, "UseConfSalePrice", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "UseConfSalePrice", "N", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bUseConfSalePrice = true;
   else
      bUseConfSalePrice = false;

   iTmp = GetIniString("Data", "XferFile", "", acXferTmpl, _MAX_PATH, acIniFile);
   if (!iTmp)
   {
      LogMsg("***** Transfer file wasn't defined.  Please check LOADMB.INI for XferFile= in [Data] section.");
   }

   if (acValueFile[0] <= ' ')
   {
      GetIniString(pCnty, "ValueFile", "", acValueFile, _MAX_PATH, acIniFile);
      if (acValueFile[0] < ' ')
      {
         GetIniString("Data", "ValueFile", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acValueFile, acTmp, pCnty, pCnty);
      }
   }

   // Get default TaxYear
   iTaxYear = GetPrivateProfileInt(pCnty, "TaxYear", lLienYear, acIniFile);

   char acVestTbl[_MAX_PATH];
   GetIniString(pCnty, "VestingTbl", "", acVestTbl, _MAX_PATH, acIniFile);
   if (acVestTbl[0] < ' ')
      GetIniString("System", "VestingTbl", "", acVestTbl, _MAX_PATH, acIniFile);

   iRet = loadVesting(pCnty, acVestTbl);

   return iRet;
}

/***************************** MB_CreateSCSale *******************************
 *
 * Extract sale data from ???_Sales.csv and output to ???_SALE.DAT
 * Drop all records without DocNum or DocDate
 *
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * DateFmt: 
 *    0 : Default. Auto format by checking input date
 *    MM_DD_YYYY_1 (MON)
 *    YYYY_MM_DD   (HUM)
 *
 * DocTypeFmt: 
 *    0 : Default. Set DocType=1 (GD) if DocCode=1 or sale price > 0
 *    1 : AMA, BUT, MAD, SON, SBT
 *    2 : PLA
 *    3 : SHA, STA
 *
 * DocNumFmt: 
 *    0 : Default. Copy DocNum as is
 *    1 : Format DocNum second part of DocNum to 7 digits (i.e. 2010R1234 = 2010R0001234)
 *    2 : Format DocNum second part of DocNum to n digits (i.e. 2010R0034 = 2010R34) (COL)
 *    3 : Remove all nonnumeric after 5th character and format to 6 digits (SON)
 *    4 : Format to yyyyR9999999 if R is in pos 4 or 5 of original DocNum (SBT)
 *    5 : Format DocNum second part of DocNum to 5 digits (i.e. 2010R1234 = 2010R01234) (HUM)
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int MB_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], acDocNum[16], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp;

   LogMsg0("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      sprintf(acTmp, "*** %s - Error opening Sales file.  Errno=%d", myCounty.acCntyCode, errno);
      sprintf(acRec, "Please review log file \"%s\" for more info on %s", acLogFile, acSalesFile);
      mySendMail(acIniFile, acTmp, acRec, NULL);

      return -1;
   }

   // Skip header
   //for (iTmp = 0; iTmp < iHdrRows; iTmp++)
   //   pTmp = fgets(acRec, 1024, fdSale);
   do {
      lTmp = ftell(fdSale);
      pTmp = fgets(acRec, 1024, fdSale);
   } while (pTmp && !isdigit(*(pTmp+1)));    // Skip quote on 1st char
   
   if (fseek(fdSale, lTmp, SEEK_SET))
   {
      LogMsg("***** Error setting current position in sale file using fseek()");
      fclose(fdSale);
      return -2;
   }

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (strlen(acRec) < 14)
         break;

      if (iSkipQuote)
         quoteRem(acRec);

      // Replace null char with space
      replNull(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Ignore DocNum start with 1900
      if (!memcmp(apTokens[MB_SALES_DOCNUM], "1900R", 5))
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "052202011000", 9))
      //   iTmp = 0;
#endif
      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
      {
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }

      // Docnum
      acDocNum[0] = 0;
      if (pTmp = strchr(apTokens[MB_SALES_DOCNUM], '`'))
         *pTmp = 0;

      if (!iDocNumFmt)
      {
         // AMA, MON, NAP, PLA, SIS, SHA, LAK, MNO, YUB
         memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));
      } else if (iDocNumFmt == 1)
      {  // BUT, MAD
         if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R' && strchr(apTokens[MB_SALES_DOCNUM]+5, '-'))
         {
            memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));
         } else
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
            if (lTmp > 0)
            {
               sprintf(acTmp, "%.5s%0.7ld", apTokens[MB_SALES_DOCNUM], lTmp);
               memcpy(SaleRec.DocNum, acTmp, SALE_SIZ_DOCNUM);
            }
         }
      } else if (iDocNumFmt == 2)
      {  // COL
         if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R')
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%.5s%d", apTokens[MB_SALES_DOCNUM], lTmp);
               memcpy(SaleRec.DocNum, acTmp, iTmp);
            }
         }
      } else if (iDocNumFmt == 3)
      {  // SON
         iTmp = replNonNum(apTokens[MB_SALES_DOCNUM]+5, ' ');

         lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%.5s%0.6ld   ", apTokens[MB_SALES_DOCNUM], lTmp);
            memcpy(SaleRec.DocNum, acTmp, SALE_SIZ_DOCNUM);
         }
      } else if (iDocNumFmt == 4)
      {  // SBT,SON
         if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R')
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%.5s%0.7d", apTokens[MB_SALES_DOCNUM], lTmp);
               memcpy(SaleRec.DocNum, acTmp, iTmp);
            }
         } else if (*(apTokens[MB_SALES_DOCNUM]+5) == 'R')
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+6, 6);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%.4sR%0.7d", SaleRec.DocDate, lTmp);
               memcpy(SaleRec.DocNum, acTmp, iTmp);
            }
         }
      } else if (iDocNumFmt == 5)
      {  // HUM
         strcpy(acTmp, apTokens[MB_SALES_DOCNUM]);
         iTmp = 0;
         if (acTmp[4] == 'R')
         {
            if (!strpbrk(acTmp, "-+*`./"))
            {
               if (!strpbrk(acTmp, "DAT"))
                  iTmp = sprintf(acDocNum, "%.5s%.5d", acTmp, atoi(&acTmp[5]));
               else if (bDebug)
                  LogMsg("Remove 2. %s", acTmp);
            } else if (bDebug)
               LogMsg("Remove 1. %s", acTmp);
         } else if (strlen(acTmp) == 9 && isdigit(acTmp[4]) && !memcmp(acTmp, SaleRec.DocDate, 4))
            iTmp = sprintf(acDocNum, "%.4sR%.5s", acTmp, &acTmp[4]);
         else if (strlen(acTmp) == 11 && acTmp[5] == 'R')
            iTmp = sprintf(acDocNum, "%.4s%.6s", SaleRec.DocDate, &acTmp[5]);
         else if (bDebug)
            LogMsg("Remove 3. %s", acTmp);

         memcpy(SaleRec.DocNum, acDocNum, iTmp);
      }

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1' || *apTokens[MB_SALES_GROUPSALE] == 'T')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001130011000", 9))
      //   iTmp = 0;
#endif
      // Confirmed sale price
      dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
      if (acTmp[0] > '0')
      {
         lPrice = atol(acTmp);
         if (lPrice < 1000)
         {
            // This doesn't look normal, but keep it for reference only
            iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            lPrice = 0;
         } else
            iTmp = sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      } 

      // Do not use confirmed sale price
      if (!bUseConfSalePrice)
         lPrice = 0;

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001160016000", 9))
      //   iTmp = 0;
#endif
      // Tax
      dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
      if (acTmp[0] > '0')
      {
         dTmp = atof(acTmp);

         // Save DocTax
         iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);

         // Calculate sale price
         lTmp = (long)(dTmp * SALE_FACTOR);
         iTmp = ((int)dTmp/100)*100;

         // Check for bad DocTax
         if (dTmp > 100000)
         {
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else if (lPrice > 100000 && lPrice == (lPrice/100)*100)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTmp);
            } else
            {
               LogMsg("??? (1) Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f, CP=%d.  Need investigation.  Ignore price.", SaleRec.Apn, SaleRec.DocNum, dTmp, lPrice);
               lPrice = 0;
            }
         } else if (lPrice > 0 && (long)dTmp >= lPrice+5000)
         {
            // Even though sale tax is questionable, we still rely on it if it is reasonable
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** (2) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               if (lPrice == (lPrice/100)*100)
                  LogMsg("*** (2) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTmp);
               else
               {
                  LogMsg("??? (2) Questionable Sale Tax Amt for %.12s: DocNum=%.12s, Price=%d, \ttax=%.2f).  Need investigation.", SaleRec.Apn, SaleRec.DocNum, lPrice, dTmp);
                  lPrice = lTmp;
               }
            }
         } else if (iTmp == (int)dTmp && iTmp > 10000)
         {
            if (lTmp != (lTmp/100)*100)
            {
               LogMsg("*** (3) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
               lPrice = lTmp;
         } else if (lTmp == (lTmp/100)*100)
            lPrice = lTmp;
         else if (lTmp > 1000 && lPrice == 0)
            lPrice = lTmp;

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Ignore sale price.", 
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE]);
               lPrice = 0;
            }
         }  
      } 

      // Ignore sale price if less than 1000
      if (lPrice >= 10000)
         sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
      else if (lPrice >= 1000)
         sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
      else
         memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
      memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);

      // Doc code - accept following code only
      int iDocCode = 0;
      if (isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         if (pDocTbl)
         {
            iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
            if (iTmp >= 0)
            {
               memcpy(SaleRec.DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
               if (lPrice < 1000)
                  SaleRec.NoneSale_Flg = pDocTbl[iTmp].flag;
            } else if (bDebug)
               LogMsg("*** Unknown DocCode: %s", apTokens[MB_SALES_DOCCODE]);
         } else
         {
            iDocCode = atoi(apTokens[MB_SALES_DOCCODE]);
            if (iDocTypeFmt == 1)      // AMA, BUT, MAD
            {
               if (iDocCode ==1 || iDocCode == 12 || (iDocCode == 8 && lPrice > 0))
                  SaleRec.DocType[0] = '1';
               else if (iDocCode == 10)
                  memcpy(SaleRec.DocType, "75", 2);
               else 
               {
                  SaleRec.NoneSale_Flg = 'Y';
                  if (iDocCode == 52)                        // Transfer - Default
                     memcpy(SaleRec.DocType, "52", 2);
                  else if (iDocCode == 2 || iDocCode == 4)       // Transfer
                     memcpy(SaleRec.DocType, "75", 2);
                  else if (iDocCode == 5 || iDocCode == 6)       // Partial Transfer
                     memcpy(SaleRec.DocType, "75", 2);
               }
            } else if (iDocTypeFmt == 2)  // PLA
            {
               switch (iDocCode)
               {
                  case 1:  // GD, Transfer reappr
                  case 5:  // New mobile home                  
                     SaleRec.DocType[0] = '1';
                     break;
                  case 2:  // Partial Transfer
                  case 10: // Timeshare - reappr
                  case 11: // Transfer - no reappr
                  case 12: // Partial Transfer - no reappr
                  case 15: // Strawman transfer
                     memcpy(SaleRec.DocType, "75", 2);
                     break;
                  case 3:
                     // Foreclosure
                     memcpy(SaleRec.DocType, "77", 2);
                     break;
                  case 4:
                     // Sheriff's deed
                     memcpy(SaleRec.DocType, "25", 2);
                     break;
                  case 8:
                     // Tax deed
                     memcpy(SaleRec.DocType, "67", 2);
                     break;
                  default:
                     break;
               }
            } else if (iDocTypeFmt == 3)  // SHA
            {
               switch (iDocCode)
               {
                  case 1:  // GD, Transfer reappr
                     SaleRec.DocType[0] = '1';
                     break;
                  case 2:  // Partial Transfer
                     memcpy(SaleRec.DocType, "57", 2);
                     break;
                  case 3:  // Non-Reappraisal event                  
                  case 5:  // Intermarrital
                  case 6:  // Vesting/Name change
                  case 11: // Add/Delete JT
                     SaleRec.NoneSale_Flg = 'Y';
                     break;
                  case 10:
                     // Tax deed
                     memcpy(SaleRec.DocType, "67", 2);
                     break;
                  case 19:
                     // Foreclosure
                     memcpy(SaleRec.DocType, "77", 2);
                     break;
                  case 60:
                     // Split/Combine
                     if (lPrice > 0)
                        SaleRec.DocType[0] = '1';
                     break;
                  default:
                     if (!lPrice)
                        SaleRec.NoneSale_Flg = 'Y';
                     break;
               }
            } else if (iDocTypeFmt == 4)  // SON
            {
               if (iDocCode ==1 || (lPrice > 0 && isdigit(SaleRec.DocNum[5])))
                  SaleRec.DocType[0] = '1';
               else 
               {
                  if (iDocCode == 4)                           // Transfer
                  {
                     SaleRec.NoneSale_Flg = 'Y';
                     memcpy(SaleRec.DocType, "75", 2);
                  } else if (iDocCode == 5 || iDocCode == 6)   // Internal Doc
                  {
                     SaleRec.NoneSale_Flg = 'Y';
                     memcpy(SaleRec.DocType, "74", 2);
                  }
               }
            } else if (iDocCode == 1 || lPrice > 0)
               SaleRec.DocType[0] = '1';
         }
      } else if (!memcmp(apTokens[MB_SALES_DOCCODE], "GD", 2))
         SaleRec.DocType[0] = '1';

      // Save original DocCode
      vmemcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);

      // Transfer Type
      if (*apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         strcpy(acTmp, apTokens[MB_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Seller1, acTmp, SALE_SIZ_SELLER, iTmp);

         // Buyer
         strcpy(acTmp, apTokens[MB_SALES_BUYER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Name1, acTmp, SALE_SIZ_BUYER, iTmp);

         SaleRec.CRLF[0] = 10;
         SaleRec.CRLF[1] = 0;
         fputs((char *)&SaleRec,fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(20,1,C,EQ,\" \",OR,31,1,C,EQ,\" \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -2;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            // Save old cumsale file
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);
            iTmp = rename(acCSalFile, acTmpFile);
   
            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -2;
      } else
         iTmp = rename(acOutFile, acCSalFile);

      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acCSalFile, acTmpFile);
      }
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp)
   {
      iErrorCnt++;
      if (iTmp == -2)
         LogMsg("***** Error sorting output file");
      else
         LogMsg("***** Error renaming to %s", acCSalFile);
   }

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                          output: %d.", lTmp);
   LogMsg("         Latetest recording date: %d.", lLastRecDate);
   return iTmp;
}

// This function is similar to MB_CreateSCSale() but processes different sale file layout (HUM)
int MB1_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp;

   LogMsg("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB1_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB1_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB1_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB1_SALES_DOCNUM] == ' ' || *apTokens[MB1_SALES_DOCDATE] == ' ')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB1_SALES_ASMT], strlen(apTokens[MB1_SALES_ASMT]));

      // Docnum
      if (!iDocNumFmt)
      {
         // IMP, LAK, MON, NAP, PLA, SIS, SHA
         memcpy(SaleRec.DocNum, apTokens[MB1_SALES_DOCNUM], strlen(apTokens[MB1_SALES_DOCNUM]));
      } else if (iDocNumFmt == 1)
      {  // AMA, BUT, MAD
         lTmp = atoin(apTokens[MB1_SALES_DOCNUM]+5, 7);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%.5s%0.7ld", apTokens[MB1_SALES_DOCNUM], lTmp);
            memcpy(SaleRec.DocNum, acTmp, SALE_SIZ_DOCNUM);
         }
      }

      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[MB1_SALES_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB1_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB1_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB1_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
         memcpy(SaleRec.DocDate, acTmp, 8);

      // Group sale?
      myLTrim(apTokens[MB1_SALES_GROUPSALE]);
      if (*apTokens[MB1_SALES_GROUPSALE] == '1' || *apTokens[MB1_SALES_GROUPSALE] == 'T')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB1_SALES_GROUPASMT] > ' ')
            vmemcpy(SaleRec.PrimaryApn, apTokens[MB1_SALES_GROUPASMT], iApnLen);
      }

      // Tax
      dollar2Num(apTokens[MB1_SALES_TAXAMT], acTmp);
      if (acTmp[0] > '0')
      {
         dTmp = atof(acTmp);
         lTmp = (long)(dTmp * SALE_FACTOR);

         // Check for bad DocTax
         if (dTmp > 100000)
         {
            iTmp = ((int)dTmp/100)*100;
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               LogMsg("??? Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f.  Need investigation.  Ignore price.", SaleRec.Apn, SaleRec.DocNum, dTmp);
               lPrice = 0;
            }
         } else
         {
            lPrice = lTmp;
            iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
            memcpy(SaleRec.StampAmt, acTmp, iTmp);
         }

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB1_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
               LogMsg("*** Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s, DOCTYPE=%.3s", 
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB1_SALES_DOCCODE], SaleRec.DocType);
         }  

         // Ignore sale price if less than 1000
         if (lPrice >= 10000)
            sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
         else if (lPrice >= 1000)
            sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
         else
            memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
         memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);
      }

      // Doc code - accept following code only
      int iDocCode = 0;
      if (pDocTbl)
      {
         iTmp = findDocType(apTokens[MB1_SALES_DOCCODE], pDocTbl);
         if (iTmp >= 0)
         {
            memcpy(SaleRec.DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
            if (lPrice < 100)
               SaleRec.NoneSale_Flg = pDocTbl[iTmp].flag;
         }
      } else
      if (isdigit(*apTokens[MB1_SALES_DOCCODE]))
      {
         iDocCode = atoi(apTokens[MB1_SALES_DOCCODE]);
         if (iDocTypeFmt == 1)      // AMA, BUT, MAD
         {
            if (iDocCode ==1 || iDocCode == 12 || (iDocCode == 8 && lPrice > 0))
               SaleRec.DocType[0] = '1';
            else if (iDocCode == 10)
               memcpy(SaleRec.DocType, "75", 2);
            else 
            {
               SaleRec.NoneSale_Flg = 'Y';
               if (iDocCode == 52)                            // Transfer - Default
                  memcpy(SaleRec.DocType, "52", 2);
               else if (iDocCode == 2 || iDocCode == 4)       // Transfer
                  memcpy(SaleRec.DocType, "75", 2);
               else if (iDocCode == 5 || iDocCode == 6)       // Partial Transfer
                  memcpy(SaleRec.DocType, "57", 2);
            }
         } else if (iDocTypeFmt == 2)  // PLA
         {
            switch (iDocCode)
            {
               case 1:  // GD, Transfer reappr
               case 5:  // New mobile home                  
                  SaleRec.DocType[0] = '1';
                  break;
               case 2:  // Partial Transfer
               case 12: // Partial Transfer - no reappr
                  memcpy(SaleRec.DocType, "57", 2);
                  break;
               case 10: // Timeshare - reappr
               case 11: // Transfer - no reappr
               case 15: // Strawman transfer
                  memcpy(SaleRec.DocType, "75", 2);
                  break;
               case 3:
                  // Foreclosure
                  memcpy(SaleRec.DocType, "77", 2);
                  break;
               case 4:
                  // Sheriff's deed
                  memcpy(SaleRec.DocType, "25", 2);
                  break;
               case 8:
                  // Tax deed
                  memcpy(SaleRec.DocType, "67", 2);
                  break;
               default:
                  break;
            }
         } else if (iDocTypeFmt == 3)  // SHA
         {
            switch (iDocCode)
            {
               case 1:  // GD, Transfer reappr
                  SaleRec.DocType[0] = '1';
                  break;
               case 2:  // Partial Transfer
                  memcpy(SaleRec.DocType, "57", 2);
                  break;
               case 3:  // Non-Reappraisal event                  
               case 5:  // Intermarrital
               case 6:  // Vesting/Name change
               case 11: // Add/Delete JT
                  SaleRec.NoneSale_Flg = 'Y';
                  break;
               case 10:
                  // Tax deed
                  memcpy(SaleRec.DocType, "67", 2);
                  break;
               case 19:
                  // Foreclosure
                  memcpy(SaleRec.DocType, "77", 2);
                  break;
               case 60:
                  // Split/Combine
                  if (lPrice > 0)
                     SaleRec.DocType[0] = '1';
                  break;
               default:
                  if (!lPrice)
                     SaleRec.NoneSale_Flg = 'Y';
                  break;
            }
         } else 
         {
            if (iDocCode == 1 || lPrice > 0)
               SaleRec.DocType[0] = '1';
         }
      } else if (!memcmp(apTokens[MB1_SALES_DOCCODE], "GD", 2))
         SaleRec.DocType[0] = '1';

      // Save original DocCode
      vmemcpy(SaleRec.DocCode, apTokens[MB1_SALES_DOCCODE], SALE_SIZ_DOCCODE);

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001160016000", 9))
      //   iTmp = 0;
#endif

      // Full/Partial
      if (!memcmp(SaleRec.DocType, "57", 2) || !memcmp(SaleRec.DocType, "58", 2))
         SaleRec.SaleCode[0] = 'P';

      // Transfer Type
      if (lPrice > 0 && *apTokens[MB1_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!_memicmp(apTokens[MB1_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      // Only output record with DocDate
      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         strcpy(acTmp, apTokens[MB1_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Seller1, acTmp, SALE_SIZ_SELLER);

         // Buyer
         strcpy(acTmp, apTokens[MB1_SALES_BUYER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Name1, acTmp, SALE_SIZ_BUYER);

         SaleRec.CRLF[0] = 10;
         SaleRec.CRLF[1] = 0;
         fputs((char *)&SaleRec,fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(27,2,C,EQ,\"  \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -1;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            DeleteFile(acCSalFile);
   
            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -1;
      } else
         iTmp = rename(acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
         DeleteFile(acCSalFile);
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp)
      iErrorCnt++;

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                       output:    %d.", lTmp);
   return iTmp;
}

/********************************** ExtrProp8 ********************************
 *
 * Extract Prop8 flag from LDR file
 *
 *****************************************************************************/

int MB_ExtrProp8Ldr(char *pCnty, char *pLDRFile)
{
   char  *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char  acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int   iRet, iTaxable, lCnt=0, iProp8Cnt=0;
   FILE  *fdOut;

   LogMsg("\nExtract Prop8 flag from lien roll for %s", pCnty);

   // Open lien file
   if (!pLDRFile)
   {
      GetIniString(pCnty, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
      if (lLienYear > 1900)
      {
         sprintf(acRec, "%d\\", lLienYear);
         replStr(acBuf, "[year]", acRec);
      } else
         replStr(acBuf, "[year]", "");
   } else
      strcpy(acBuf, pLDRFile);

   if (iLdrGrp == 1)
      iTaxable = L1_TAXABILITY;
   else if (iLdrGrp == 2)
      iTaxable = L2_TAXABILITY;
   else
      iTaxable = L_TAXABILITY;

   LogMsg("Open Lien Date Roll file %s", acBuf);
   sprintf(acTmpFile, "%s\\%s\\%s_%s.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acYearAssd);

   // Sort on ASMT
   iRet = sortFile(acBuf, acTmpFile, "S(#1,C,A)");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acTmpFile);
      return 2;
   }

   // Create Prop8 extract
   GetIniString("Data", "Prop8Exp", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRec, pCnty, pCnty, myCounty.acYearAssd);
   LogMsg("Open lien extract file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // print header
   fputs("APN\n", fdOut);

   // Merge loop
   while (pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll))
   {
      if (!isdigit(acRec[1]))
         break;      // EOF

      // Replace null char with space
      iRet = replNull(acRec, ' ', 0);

      // parse input
      if (cLdrSep == 9)
         iRet = ParseStringIQ(acRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
      else
         iRet = ParseStringNQ(acRec, cLdrSep, MAX_FLD_TOKEN, apTokens);

      if (iRet < L_DTS)
      {
         LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
         return -1;
      }
    
      // Check Taxability
      iRet = atol(apTokens[iTaxable]);
      if (iRet > 799 && iRet < 900)
      {
         remChar(apTokens[L_ASMT], '-');
         sprintf(acBuf, "%s\n", apTokens[L_ASMT]);
         fputs(acBuf, fdOut);
         iProp8Cnt++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOut)
      fclose(fdOut);

   LogMsgD("\nTotal prop8 records output: %u", iProp8Cnt);
   LogMsgD("Total records processed:    %u\n", lCnt);

   return 0;

}

/********************************** ExtrProp8 ********************************
 *
 * Extract Prop8 flag from LDR file
 *
 *****************************************************************************/

int MB_ExtrProp8(char *pCnty, char *pLDRFile, char cDelim, int iTaxable)
{
   char  *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char  acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int   iRet, lCnt=0, iProp8Cnt=0;
   bool  bUseTab = false;
   FILE  *fdOut;

   LogMsg("\nExtract Prop8 flag from lien roll for %s", pCnty);

   // Open lien file
   if (!pLDRFile)
   {
      LogMsg("***** Missing input roll file for %s", pCnty);
      return -1;
   }


   LogMsg("Open Lien Date Roll file %s", acBuf);
   sprintf(acTmpFile, "%s\\%s\\%s_%s.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acYearAssd);

   // Sort on ASMT
   iRet = sortFile(pLDRFile, acTmpFile, "S(#1,C,A)");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acTmpFile);
      return 2;
   }

   // Create Prop8 extract
   GetIniString("Data", "Prop8Exp", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRec, pCnty, pCnty, myCounty.acYearAssd);
   LogMsg("Open lien extract file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // print header
   fputs("APN\n", fdOut);

   // Merge loop
   while (pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll))
   {
      if (!isdigit(acRec[1]))
         break;      // EOF

      iRet = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iRet < MB_ROLL_PPMOBILHOME)
      {
         LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
         return -1;
      }
    
      // Check Taxability
      iRet = atol(apTokens[iTaxable]);
      if (iRet > 799 && iRet < 900)
      {
         sprintf(acBuf, "%s\n", apTokens[iApnFld]);
         fputs(acBuf, fdOut);
         iProp8Cnt++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOut)
      fclose(fdOut);

   LogMsgD("\nTotal prop8 records output: %u", iProp8Cnt);
   LogMsgD("Total records processed:    %u\n", lCnt);

   return 0;

}

/********************************** MB_ExtrZoning ****************************
 *
 * Extract Zoning from roll file
 *
 *****************************************************************************/

int MB_ExtrZoning(int iZoneToken)
{
   char  *pTmp, acBuf[_MAX_PATH], acRec[MAX_RECSIZE], acZoningFile[_MAX_PATH];
   char  acTmpFile[_MAX_PATH];
   int   iRet, lCnt=0, iZoneCnt=0;
   FILE  *fdOut;

   LogMsg("\nExtract Zoning from roll for %s", myCounty.acCntyCode);

   // Open roll file
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRec, _MAX_PATH, acIniFile);
   if (acRec[0] > ' ' && !_access(acRec, 0))
   {
      // Just to be safe ...
      sprintf(acBuf, acRec, myCounty.acCntyCode, myCounty.acCntyCode);
   } else
   {
      LogMsg("***** Missing input roll file for %s", myCounty.acCntyCode);
      return -1;
   }

   LogMsg("Open Roll file %s", acBuf);
   sprintf(acTmpFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Sort on ASMT
   iRet = sortFile(acBuf, acTmpFile, "S(#1,C,A)");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acTmpFile);
      return -2;
   }

   // Create Zoning extract
   GetIniString("Data", "ZoneExt", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acZoningFile, acRec, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Zoning extract file %s", acZoningFile);
   fdOut = fopen(acZoningFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Zoning extract file: %s\n", acZoningFile);
      return -2;
   }

   // print header
   fputs("APN|ZONING\n", fdOut);

   // Merge loop
   while (pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll))
   {
      if (!isdigit(acRec[1]))
         break;      // EOF

      iRet = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iRet < MB_ROLL_PPMOBILHOME)
      {
         LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
         return -1;
      }
    
      // Check Zoning
      if (*apTokens[iZoneToken] > ' ')
      {
         sprintf(acBuf, "%s|%s\n", apTokens[iApnFld], apTokens[iZoneToken]);
         fputs(acBuf, fdOut);
         iZoneCnt++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOut)
      fclose(fdOut);

   LogMsgD("\nTotal Zoning records output: %u", iZoneCnt);
   LogMsgD("Total records processed:    %u\n", lCnt);

   return 0;

}

/************************************** Usage ********************************
 *
 *
 *****************************************************************************/

void Usage()
{
   printf("\nUsage: LoadMB -C<County code> [-A] [-D] [-E[xt]] [-F] [-G] [-H] [-L?] [-M?] [-N] [-O] [-T?] [-U?] [-X?] [-Y?] [-S<n>]\n");
   printf("\t-A  : Merge attribute file\n");
   printf("\t-D  : Duplicate sale records from old file\n");
   printf("\t-Dn : Don't create SQL update CHK file\n");
   printf("\t-Dc : Create debug UPD file\n");
   printf("\t-E  : Email on error\n");
   printf("\t-Ext: Export SQL command for tax update\n");
   printf("\t-Fc : Fix CHARS\n");
   printf("\t-Fd : Fix DOCNUM in cumsale file\n");
   printf("\t-Fs : Fix Sales\n");
   printf("\t-Ft : Fix TRA\n");
   printf("\t-G  : Load and create GrGr file\n");
   printf("\t-Gyyyymmdd : Load GrGr file for specific day\n");
   printf("\t-Mg : Merge GrGr file\n");
   printf("\t-Mn : Merge NDC sale file\n");
   printf("\t-L  : Load Lien date roll (create lien file and load roll).\n");
   printf("\t-Ld : Load daily file.\n");
   printf("\t-N  : Do not encode suffix and city\n");
   printf("\t-O  : Overwrite logfile (default append)\n");
   printf("\t-Ps : Purge sale data\n");
   printf("\t-Pz : Purge PQZoning\n");
   printf("\t-Sn : Number of records skip (default 1)\n");
   printf("\t-T  : Load tax files\n");
   printf("\t-Tyyyymmdd  : Load specific tax year\n");
   printf("\t-Tu : Update Tax tables\n");
   printf("\t-U  : Update roll file using old S01 file\n");
   printf("\t-Up : Update previous APN\n");
   printf("\t-Us : Update cum sale file\n");
   printf("\t-X8 : Extract Prop8 APN to be imported to SQL.\n");
   printf("\t-Xa : Extract attribute from attr file.\n");
   printf("\t-Xg : Extract GrGr data.\n");
   printf("\t-Xl : Extract lien value from lien file.\n");
   printf("\t-Xn : Extract sale data from NDC recorder database.\n");
   printf("\t-Xs : Extract sale data from county sale file.\n");
   printf("\t-Xv : Extract value file.\n");
   printf("\t-Ynnnn: Process year.  This overwrites default year in CountyInfo.csv\n");
   exit(1);
}

/********************************** ParseCmd() *******************************
 *
 *
 *****************************************************************************/

void ParseCmd(int argc, char* argv[])
{
   char  chOpt;                // gotten option character
   char *pszParam;

   // Initialize globals
   bGrGrAvail = true;
   bEnCode = true;
   bClearSales = false;
   bOverwriteLogfile = false;
   bSendMail = false;
   bFixTRA = false;
   bClearChars = false;
   bMergeOthers = false;
   bDontUpd = false;
   bCreateUpd = false;
   bDebug=false;
   bRemTmpFile = true;
   bUnzip = false;
   bUpdPrevApn = false;
   bClearPQZ = false;
   bExpSql = false;

   iSkip = 1;
   iLoadFlag=lOptMisc = 0;
   lLienYear = 0;
   iLoadTax = 0;
   lTaxYear = 0;
   lProcDate = lToday;

   if (argv[1][0] != '-')
   {
      strcpy(myCounty.acCntyCode, argv[1]);
      return;
   }

   while (1)
   {
      chOpt = GetOption(argc, argv, "ABC:D:E:F:G:I:L:M:N:OP:S:T:U:X:Y:Z?", &pszParam);
      if (chOpt > 1)
      {
         // chOpt is valid argument
         switch (chOpt)
         {
            case 'A':   // Merge attribute data
               iLoadFlag |= MERG_ATTR;
               break;

            case 'C':   // county code
               if (pszParam != NULL)
                  strcpy(myCounty.acCntyCode, pszParam);
               else
               {
                  printf("Missing county code\n");
                  Usage();
               }
               break;

            case 'D':   // Debug info
               if (pszParam != NULL)
               {
                  if (*pszParam == 'n')
                     bDontUpd = true;
                  else if (*pszParam == 'c')
                     bCreateUpd = true;
                  else if (*pszParam == 'r')
                     bRemTmpFile = false;
               } else
                  bDebug = true;
               break;

            case 'E':                           
               if (pszParam != NULL)
               {
                  if (!strcmp(pszParam, "xt"))
                     bExpSql = true;            // Export SQL command for import
               } else
                  bSendMail = true;             // Email if error occurs

               break;

            case 'F':   // Fix data field
               if (pszParam != NULL)
               {
                  if (*pszParam == 't')
                     bFixTRA = true;
                  else if (*pszParam == 'c')
                     bClearChars = true;
                  else if (*pszParam == 'd')
                     iLoadFlag |= FIX_CSDOC;
                  else if (*pszParam == 'x')
                     iLoadFlag |= FIX_CSTYP;    // Fix DocType in cumsale

               }
               break;

            case 'G':   // Load GrGr data
               iLoadFlag |= LOAD_GRGR;        
               if (pszParam != NULL)
               {
                  if (*pszParam == 'i')
                     iLoadFlag |= EXTR_IGRGR;       // Import GRGR  data into SQL
                  else if (strlen(pszParam) == 8)
                  {
                     lProcDate = atol(pszParam);
                  }
               }
               break;

            case 'I':                           // Generate import file
               if (pszParam != NULL)
               {
                  if (*pszParam == 's')         // Import sale
                     iLoadFlag |= EXTR_ISAL;
                  else if (*pszParam == 'g')    // Import Grgr
                     iLoadFlag |= EXTR_IGRGR;
               }
               break;

            case 'L':   // Load lien date roll
               if (pszParam != NULL)
               {
                  if (*pszParam == 's')
                     iLoadFlag |= LOAD_SALE;
                  else if (*pszParam == 'r')
                     iLoadFlag |= LOAD_LIEN;    // Create PQ4 data using lien roll
                  else if (*pszParam == 'g')
                     iLoadFlag |= LOAD_GRGR;   
                  else if (strlen(pszParam) == 8)
                  {
                     lProcDate = atol(pszParam);
                     if (lProcDate > lLienDate && lProcDate <= lToday)
                        iLoadFlag |= LOAD_DAILY; // Load daily update file
                     else
                        printf("***** Unknown option -L%s (ignore)\n", pszParam);
                  } else if (*pszParam == 'd')  // -Ld
                  {
                     iLoadFlag |= LOAD_DAILY;   // Load daily update file
                  } else if (*pszParam == 't')  // Load tax file
                  {
                     iLoadTax = TAX_LOADING;
                     bRemTmpFile = false;  
                  } else if (*pszParam == 'z')
                     iLoadFlag |= LOAD_ZIP;
                  else
                     printf("***** Unknown option -L%s (ignore)\n", pszParam);
               } else
                  iLoadFlag |= LOAD_LIEN;
               break;

            case 'M':   // Merge GrGr data
               if (pszParam != NULL)
               {
                  if (*pszParam == 's')
                     iLoadFlag |= MERG_CSAL;    // Merge cummulative sale
                  else if (*pszParam == 'g')
                     iLoadFlag |= MERG_GRGR;    // Merge GrGr
                  else if (*pszParam == 'a')
                     iLoadFlag |= MERG_ATTR;    // Merge attribute/characteristics
                  else if (*pszParam == 'p')
                     iLoadFlag |= MERG_PUBL;    // Merge public parcel
                  else if (*pszParam == 'r')
                     iLoadFlag |= MERG_GISA;    // Merge acreage from GIS basemap ORG
                  else if (*pszParam == 'z')
                     iLoadFlag |= MERG_ZONE;    // Merge zoning file
                  else if (*pszParam == 'o')
                     bMergeOthers = true;       // Merge other values
                  else if (*pszParam == 'n')
                     iLoadFlag |= UPDT_XSAL;    // Merge NDC sale UPDT_XSAL
                  else if (*pszParam == 'f')                     
                     lOptMisc |= M_OPT_MERGVAL; // Merge final value
              } else
                  iLoadFlag |= MERG_GRGR;
               break;

            case 'N':   // Do not encode
               if (pszParam != NULL)
               {
                  if (*pszParam == 'm')         // Do not send mail             
                     bSendMail = false;
               } else
                  bEnCode = false;
               break;

            case 'O':   // Overwrite logfile
               bOverwriteLogfile = true;
               break;

            case 'P':                           // Purge old data
               if (pszParam)
               {
                  if (*pszParam == 's')         // clear sale data
                     bClearSales = true;
                  else if (*pszParam == 'z')    // Clear PQZoning
                     bClearPQZ = true;
               }
               break;

            case 'S':   // Skip records
               if (pszParam != NULL)
                  iSkip = atoi(pszParam);
               else
                  Usage();
               break;

            case 'T':   // Load tax file - Do not remove temp files when loading tax
               iLoadTax = TAX_LOADING;
               bRemTmpFile = false;  
               if (pszParam && *pszParam == 'e')
                  iLoadTax = TAX_EXTRACT;
               else if (pszParam && *pszParam == 'u')
                  iLoadTax = TAX_UPDATING;
               else if (pszParam && *pszParam > '0')
               {
                  lTaxYear = atol(pszParam);
                  if (lTaxYear > lToyear || lTaxYear < 2000)
                  {
                     printf("Invalid Tax year option %s\n", pszParam);
                     Usage();
                  }
               } 

               break;

            case 'U':   // Update roll file
               if (pszParam != NULL)
               {
                  if (*pszParam == 'p')
                     bUpdPrevApn = true;
                  else if (*pszParam == 't')  // Update tax table
                     iLoadTax = TAX_UPDATING;
               } else
                  iLoadFlag |= LOAD_UPDT;
               break;

            case 'X':   // Extract data only
               if (pszParam != NULL)
               {
                  if (*pszParam == 'l')
                     iLoadFlag |= EXTR_LIEN;
                  else if (*pszParam == 'a')
                     iLoadFlag |= EXTR_ATTR;
                  else if (*pszParam == 'n')
                     iLoadFlag |= EXTR_NRSAL;
                  else if (*pszParam == 'f')    // Extract final value
                     iLoadFlag |= EXTR_FVAL;
                  else if (*pszParam == 's')
                  {
                     iLoadFlag |= EXTR_SALE;
                     if (*(pszParam+1) == 'i')
                        iLoadFlag |= EXTR_ISAL;
                  } else if (*pszParam == '8')    // Extract prop8 file
                     iLoadFlag |= EXTR_PRP8;
                  else if (*pszParam == 'v')
                  {
                     iLoadFlag |= EXTR_VALUE;
                     if (*(pszParam+1) == 'i')
                        iLoadFlag |= EXTR_IVAL;
                  } 
                  else
                  {
                     printf("Invalid extract option %s\n", pszParam);
                     Usage();
                  }
               } else
                  Usage();
               break;

            case 'Y':
               if (*pszParam > '0')
               {
                  lLienYear = atol(pszParam);
                  if (lLienYear > lToyear || lLienYear < 2000)
                  {
                     printf("Invalid LDR year option %s\n", pszParam);
                     Usage();
                  }
               } else
                  Usage();
               break;

            case 'Z':   // Unzip file before processing
               bUnzip = true;
               break;

            case '?':   // usage info
            default:
               Usage();
               break;
         }
      }
      if (chOpt == 0)
      {
         // end of argument list
         break;
      }
      if ((chOpt == 1) || (chOpt == -1))
      {
         // standalone param or error
         LogMsg("Argument [%s] not recognized\n", pszParam);
         break;
      }
   }

   if (iLoadFlag & LOAD_LIEN)
   {
      bCreateUpd = false;
      bDontUpd = true;
   }
}

/******************************** updateSaleDB ******************************
 *
 * This function will call store procedure to do bulk insert
 *
 * Return 0 if success, otherwise error code
 *
 ****************************************************************************/

int updateSaleDB(char *strCmd)
{                 
   static bool  bConnected=false;
   static hlAdo hDB;

   char acServer[256];
   int  iRet;

   iRet = GetIniString("Database", "SaleProvider", "", acServer, 128, acIniFile);
   if (!iRet)
   {
      LogMsg("***** Missing SaleProvider in INI file.  Please review %s", acIniFile);
      return -1;
   }

   LogMsg("Execute command on: %s", acServer);
   LogMsg(strCmd);
   try
   {
      // open the database connection
      if (!bConnected)
         bConnected = hDB.Connect(acServer);
   }

   // catch any ADO errors
   AdoCatch(e)
   {
      LogMsg("%s", ComError(e));
      return -2;
   }

   // Update county profile table
   iRet = execSqlCmd(strCmd, &hDB);
   return iRet;
}

/********************************** runSP ***********************************
 *
 * This function will execute a SQL command then exit
 *
 * Return 0 if success, otherwise error code
 *
 ****************************************************************************/
//
//int runSP(char *strCmd, char *pDBName, char *pServerName)
//{                 
//   hlAdo hDB;
//
//   char acServer[256], acTmp[256];
//   int  iRet;
//
//   iRet = GetIniString("Database", "DBProvider", "", acTmp, 128, acIniFile);
//   if (!iRet)
//   {
//      LogMsg("***** Missing DBProvider in INI file.  Please review %s", acIniFile);
//      return -1;
//   }
//   sprintf(acServer, acTmp, pDBName, pServerName);
//
//   LogMsg("Execute command on: %s", acServer);
//   LogMsg(strCmd);
//   try
//   {
//      // open the database connection
//      hDB.Connect(acServer);
//   }
//
//   // catch any ADO errors
//   AdoCatch(e)
//   {
//      LogMsg("%s", ComError(e));
//      return -2;
//   }
//
//   // Update county profile table
//   iRet = execSqlCmd(strCmd, &hDB);
//
//   // Release command
//   hDB.~hlAdo();
//
//   return iRet;
//}

/******************************** doSaleImport ******************************
 *
 * This function will call store procedure to do bulk insert
 *
 * iType values:
 *    1: Sale data
 *    2: Grgr data
 *    3: 
 *
 * Return 0 if success
 *
 ****************************************************************************/

int doSaleImportEx(char *pCnty, char *pDbName, int iType)
{
   char sErrFile[128], sTblName[32], sImportTmpl[_MAX_PATH];
   char sFmtFile[128], sSrcFile[128], acTmp[1024], sServerType[32];
   int  iRet=1;

   GetIniString("Database", "SaleServerType", "W", sServerType, _MAX_PATH, acIniFile);
   GetIniString("Data", "SaleImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);

   switch (iType)
   {
      case 1:
         LogMsg("Import Sale data to: %s using %s", pCnty, pDbName);
         sprintf(sFmtFile, sImportTmpl, "Sales");
         sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "Sale");
         sprintf(sTblName, "%s_Sales", pCnty);

         // Delete old log files
         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         sprintf(acTmp, "%s.Error.txt", sErrFile);
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         GetIniString("Data", "SqlSaleFile", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(sSrcFile, acTmp, pDbName, pCnty);
         if (_access(sSrcFile, 0))
         {
            LogMsg("***** Missing import file: %s", sSrcFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spSalePrep] '%s'", pCnty);
         iRet = updateSaleDB(acTmp);

         if (sServerType[0] == 'L')
         {
            GetIniString("Data", "LSaleImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);
            GetIniString("System", "LImportLog", "", sImportLogTmpl, _MAX_PATH, acIniFile);
            sprintf(sFmtFile, sImportTmpl, "Sales");
            sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "Sale");
            GetIniString("Data", "LSqlSaleFile", "", acTmp, _MAX_PATH, acIniFile);
            sprintf(sSrcFile, acTmp, pDbName, pCnty);
         }

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, sSrcFile, sFmtFile, sErrFile);
         iRet = updateSaleDB(acTmp);

         // Update DocLink
         GetIniString("Database", "HSDocLink", "", acTmp, 128, acIniFile);
         sprintf(sFmtFile, acTmp, pCnty);
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing sql script file: %s to update DocLink", sFmtFile);
         } else
         {
            if (sServerType[0] == 'L')
            {
               GetIniString("Database", "LHSDocLink", "", acTmp, 128, acIniFile);
               sprintf(sFmtFile, acTmp, pCnty);
            }
            sprintf(acTmp, "EXEC sp_executesqlfromfile '%s', NULL, 0", sFmtFile);
            iRet = updateSaleDB(acTmp);    
         }
         break;

      case 2:
         LogMsg("Import Grgr data to: %s using %s", pCnty, pDbName);
         sprintf(sFmtFile, sImportTmpl, "GrGr");
         sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "GrGr");
         sprintf(sTblName, "%s_GrGr", pCnty);

         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         sprintf(acTmp, "%s.Error.txt", sErrFile);
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         GetIniString("Data", "SqlGrGrFile", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(sSrcFile, acTmp, pDbName, pCnty);
         if (_access(sSrcFile, 0))
         {
            LogMsg("***** Missing import file: %s", sSrcFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spGrGrPrep] '%s'", pCnty);
         iRet = updateSaleDB(acTmp);

         // Reformat file name for import into Linux server
         if (sServerType[0] == 'L')
         {
            GetIniString("System", "LImportLog", "", sImportLogTmpl, _MAX_PATH, acIniFile);
            GetIniString("Data", "LSaleImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);
            sprintf(sFmtFile, sImportTmpl, "Sales");
            sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "Sale");
            GetIniString("Data", "LSqlGrGrFile", "", acTmp, _MAX_PATH, acIniFile);
            sprintf(sSrcFile, acTmp, pDbName, pCnty);
         }

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, sSrcFile, sFmtFile, sErrFile);
         iRet = updateSaleDB(acTmp);

         // Update DocLink
         GetIniString("Database", "GrDocLink", "", acTmp, 128, acIniFile);
         sprintf(sFmtFile, acTmp, pCnty);
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing sql script file: %s to update DocLink", sFmtFile);
         } else
         {
            if (sServerType[0] == 'L')
            {
               GetIniString("Database", "LGrDocLink", "", acTmp, 128, acIniFile);
               sprintf(sFmtFile, acTmp, pCnty);
            }
            sprintf(acTmp, "EXEC sp_executesqlfromfile '%s', NULL, 0", sFmtFile);
            iRet = updateSaleDB(acTmp);    
         }
         break;

      default:
         break;
   }

   return iRet;
}

int doSaleImport(char *pCnty, char *pDbName, char *pInFile, int iType)
{
   char sErrFile[128], sTblName[32], sImportTmpl[_MAX_PATH];
   char sFmtFile[128], acTmp[1024];
   int  iRet=1;

   GetIniString("Data", "SaleImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);

   switch (iType)
   {
      case 1:
         LogMsg("Import Sale data to: %s using %s", pCnty, pDbName);
         sprintf(sFmtFile, sImportTmpl, "Sales");
         sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "Sale");
         sprintf(sTblName, "%s_Sales", pCnty);

         // Delete old log files
         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         sprintf(acTmp, "%s.Error.txt", sErrFile);
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         if (_access(pInFile, 0))
         {
            LogMsg("***** Missing import file: %s", pInFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spSalePrep] '%s'", pCnty);
         iRet = updateSaleDB(acTmp);

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
         iRet = updateSaleDB(acTmp);    

         // Update DocLink
         GetIniString("Database", "HSDocLink", "", acTmp, 128, acIniFile);
         sprintf(sFmtFile, acTmp, pCnty);
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing sql script file: %s to update DocLink", sFmtFile);
         } else
         {
            sprintf(acTmp, "EXEC sp_executesqlfromfile '%s', NULL, 0", sFmtFile);
            iRet = updateSaleDB(acTmp);    
         }
         break;

      case 2:
         LogMsg("Import Grgr data to: %s using %s", pCnty, pDbName);
         sprintf(sFmtFile, sImportTmpl, "Sales");
         sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "GrGr");
         sprintf(sTblName, "%s_GrGr", pCnty);

         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         sprintf(acTmp, "%s.Error.txt", sErrFile);
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         if (_access(pInFile, 0))
         {
            LogMsg("***** Missing import file: %s", pInFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spGrGrPrep] '%s'", pCnty);
         iRet = updateSaleDB(acTmp);

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
         iRet = updateSaleDB(acTmp);

         // Update DocLink
         GetIniString("Database", "GrDocLink", "", acTmp, 128, acIniFile);
         sprintf(sFmtFile, acTmp, pCnty);
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing sql script file: %s to update DocLink", sFmtFile);
         } else
         {
            sprintf(acTmp, "EXEC sp_executesqlfromfile '%s', NULL, 0", sFmtFile);
            iRet = updateSaleDB(acTmp);    
         }
         break;

      default:
         break;
   }

   return iRet;
}

/******************************** doValueImport *****************************
 *
 * This function will call store procedure to do bulk insert
 *
 *
 * Return 0 if success
 *
 ****************************************************************************/

int doValueImport(char *pCnty, char *pInFile)
{
   char sErrFile[128], sTblName[32], sDBName[32], acServer[32];
   char sFmtFile[128], sProviderTmpl[255], acTmp[1024];
   int  iRet=1;

   iRet = GetIniString("Database", "DBProvider", "", sProviderTmpl, 255, acIniFile);
   if (!iRet)
   {
      LogMsg("***** Missing DBProvider in INI file.  Please review %s", acIniFile);
      return -1;
   }

   LogMsg("Import Value data to Pcl_Values for %s", pCnty);
   sprintf(sErrFile, sImportLogTmpl, "Value", pCnty, "Value");
   strcpy(sTblName, "Pcl_Values");

   // Delete old log files
   if (!_access(sErrFile, 0))
      DeleteFile(sErrFile);
   sprintf(acTmp, "%s.Error.txt", sErrFile);
   if (!_access(acTmp, 0))
      DeleteFile(acTmp);

   if (_access(pInFile, 0))
   {
      LogMsg("***** Missing import file: %s", pInFile);
      return -1;
   }

   GetIniString("Data", "ValueImportTmpl", "", sFmtFile, _MAX_PATH, acIniFile);
   if (_access(sFmtFile, 0))
   {
      LogMsg("***** Missing import format file: %s", sFmtFile);
      return -1;
   }

   // Prepare to import - Create/truncate table
   GetIniString("Database", "ValueDB", "", sDBName, _MAX_PATH, acIniFile);
   iRet = GetIniString("Database", "DBSvr2", "", acServer, _MAX_PATH, acIniFile);
   if (iRet > 1 && sDBName[0] > ' ')
   {
      sprintf(acTmp, "EXEC [dbo].[spValuePrep] '%s', %d ", pCnty, lLienYear);
      iRet = runSP(acTmp, sDBName, acServer, sProviderTmpl);

      // Calling store procedure
      sprintf(acTmp, "EXEC [dbo].[spBulkInsert] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
      iRet = runSP(acTmp, sDBName, acServer, sProviderTmpl);    
   }
   iRet = GetIniString("Database", "DBSvr1", "", acServer, _MAX_PATH, acIniFile);
   if (iRet > 1 && sDBName[0] > ' ')
   {
      sprintf(acTmp, "EXEC [dbo].[spValuePrep] '%s', %d ", pCnty, lLienYear);
      iRet = runSP(acTmp, sDBName, acServer, sProviderTmpl);

      // Calling store procedure
      sprintf(acTmp, "EXEC [dbo].[spBulkInsert] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
      iRet = runSP(acTmp, sDBName, acServer, sProviderTmpl);    
   }
   iRet = GetIniString("Database", "DBSvr3", "", acServer, _MAX_PATH, acIniFile);
   if (iRet > 1 && sDBName[0] > ' ')
   {
      sprintf(acTmp, "EXEC [dbo].[spValuePrep] '%s', %d ", pCnty, lLienYear);
      iRet = runSP(acTmp, sDBName, acServer, sProviderTmpl);

      // Calling store procedure
      sprintf(acTmp, "EXEC [dbo].[spBulkInsert] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
      iRet = runSP(acTmp, sDBName, acServer, sProviderTmpl);    
   }

   return iRet;
}

/**************************** MB_ParseTaxBaseG3 ******************************
 *
 * For ALP "AGENCYCDCURRSEC_TR.TAB"
 *
 *****************************************************************************/

int MB_ParseTaxBaseG3(char *pOutbuf, char *pInbuf, int iDateFmt)
{
   char     acTmp[256], *pTmp;
   int      iTmp;
   double	dTmp, dTax1, dTax2, dPaid1, dPaid2, dPen1, dPen2, dCost1, dCost2, dTaxTotal, dDueTotal, dFeeTotal;
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, 9, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB3_TX_FLDS)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Ignore unsecured record
   if (*(apTokens[MB3_TX_ROLLCATEGORY]+1) == 'U')
      return 1;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // Tax Year
   iTaxYear = atol(apTokens[MB3_TX_TAXYEAR]);
   if (iTaxYear != lTaxYear)
   //if (iTaxYear > lTaxYear || iTaxYear < lTaxYear-1)
      return iTaxYear;

   sprintf(pOutRec->TaxYear, "%d", iTaxYear);
   strcpy(pOutRec->OwnerInfo.TaxYear, pOutRec->TaxYear);

   // APN
   strcpy(pOutRec->Apn, apTokens[MB3_TX_ASMT]);
   strcpy(pOutRec->OwnerInfo.Apn, apTokens[MB3_TX_ASMT]);

   // BillNum
   if  (m_bUseFeePrcl)
      strcpy(pOutRec->BillNum, apTokens[MB3_TX_FEEPARCEL]);

   // TRA
   iTmp = atol(apTokens[MB3_TX_TRA]);
   sprintf(pOutRec->TRA, "%0.6d", iTmp);

   // Record type
   if (*apTokens[MB3_TX_ROLLCATEGORY] == 'S')
   {
      pOutRec->isSupp[0] ='1';
      pOutRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
   } else if (*(apTokens[MB3_TX_ROLLCATEGORY]+1) == 'S')
   {
      pOutRec->isSecd[0] ='1';
      pOutRec->BillType[0] = BILLTYPE_SECURED;
   }

   // Initialize
   dTax1=dTax2=0.0;
   pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   // Check for Tax amount
   if (*apTokens[MB3_TX_TAXAMT1] != '(')
   {
      if (strchr(apTokens[MB3_TX_TAXAMT1], '$'))
         dollar2Num(apTokens[MB3_TX_TAXAMT1], acTmp);
      else
         strcpy(acTmp, apTokens[MB3_TX_TAXAMT1]);
      dTax1 = atof(acTmp);
   }
  
#ifdef _DEBUG
   //if (!memcmp(apTokens[MB3_TX_ASMT], "306151006", 9))
   //   iTmp = 0;
#endif

   if (*apTokens[MB3_TX_TAXAMT2] != '(')
   {
      if (strchr(apTokens[MB3_TX_TAXAMT2], '$'))
         dollar2Num(apTokens[MB3_TX_TAXAMT2], acTmp);
      else
         strcpy(acTmp, apTokens[MB3_TX_TAXAMT2]);
      dTax2 = atof(acTmp);
   }

   // Due date
   if (dTax1 > 0.0)
   {
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);
      if (*apTokens[MB3_TX_DUEDATE1] >= '0' && dateConversion(apTokens[MB3_TX_DUEDATE1], acTmp, iDateFmt))
         strcpy(pOutRec->DueDate1, acTmp);
      else
         InstDueDate(pOutRec->DueDate1, 1, lTaxYear);
   } else 
   {
      dTax1 = 0.0;
      pOutRec->Inst1Status[0] = TAX_BSTAT_NOTAX;
   }
   if (dTax2 > 0.0)
   {
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);
      if (*apTokens[MB3_TX_DUEDATE2] >= '0' && dateConversion(apTokens[MB3_TX_DUEDATE2], acTmp, iDateFmt))
         strcpy(pOutRec->DueDate2, acTmp);
      else
         InstDueDate(pOutRec->DueDate2, 2, lTaxYear);
   } else 
   {
      dTax2 = 0.0;
      pOutRec->Inst2Status[0] = TAX_BSTAT_NOTAX;
   }

   dTaxTotal = dTax1+dTax2;
   if (dTaxTotal)
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal);

   // Paid
   dPaid1 = atof(apTokens[MB3_TX_PAIDAMT1]);
   if (dPaid1 > 0.0)
   {
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt1, "%.2f", dPaid1);
   }
   dPaid2 = atof(apTokens[MB3_TX_PAIDAMT2]);
   if (dPaid2 > 0.0)
   {
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt2, "%.2f", dPaid2);
   }

   // Penalty
   dCost1 = dCost2 = dPen1 = dPen2 = 0.0;
   if (*apTokens[MB3_TX_PENCHRGDATE1] > ' ')
   {
      pTmp = dateConversion(apTokens[MB3_TX_PENCHRGDATE1],acTmp, iDateFmt);
      if (pTmp) 
      {
         dPen1 = atof(apTokens[MB3_TX_PENAMT1]);
         dCost1 = atof(apTokens[MB3_TX_COST1]);
      }
   }
   if (*apTokens[MB3_TX_PENCHRGDATE2] > ' ')
   {
      pTmp = dateConversion(apTokens[MB3_TX_PENCHRGDATE2],acTmp, iDateFmt);
      if (pTmp)
      {
         dPen2 = atof(apTokens[MB3_TX_PENAMT2]);
         dCost2 = atof(apTokens[MB3_TX_COST2]);
      }
   }

   // Penalty
   dDueTotal = 0.0;
   if (dTax1 > dPaid1)
   {
      dDueTotal = dTax1;
      if (ChkDueDate(1, pOutRec->DueDate1))
      {
         dDueTotal += dPen1+dCost1;
         sprintf(pOutRec->PenAmt1, "%.2f", dPen1);
         pOutRec->Inst1Status[0] = TAX_BSTAT_PASTDUE;
      } else
         pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   }
   if (dTax2 > dPaid2)
   {
      dDueTotal += dTax2;
      if (ChkDueDate(2, pOutRec->DueDate2))
      {
         dDueTotal += dPen2+dCost2;
         sprintf(pOutRec->PenAmt2, "%.2f", dPen2);
         pOutRec->Inst2Status[0] = TAX_BSTAT_PASTDUE;
      } else
         pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   }

   // Total Fee
   dFeeTotal = atof(apTokens[MB3_TX_TOTALFEES]);
   if (dFeeTotal > 0.0)
   {
      dTmp  = atof(apTokens[MB3_TX_TOTALFEESPAID1]);
      dTmp += atof(apTokens[MB3_TX_TOTALFEESPAID2]);
      dFeeTotal -= dTmp;
      iTmp = sprintf(pOutRec->TotalFees, "%.2f", dFeeTotal);
   }

   // Total Due
   if (dDueTotal > 0.0)
      sprintf(pOutRec->TotalDue, "%.2f", dDueTotal+dFeeTotal);

   // Paid Date
   if (*apTokens[MB3_TX_PAYMENTDATE1] >= '0' && dateConversion(apTokens[MB3_TX_PAYMENTDATE1], acTmp, iDateFmt))
   {
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      strcpy(pOutRec->PaidDate1, acTmp);
      if (dPen1 > 0.0 && memcmp(acTmp, pOutRec->DueDate1, 8) > 0)
         sprintf(pOutRec->PenAmt1, "%.2f", dPen1);
   }  

   if (*apTokens[MB3_TX_PAYMENTDATE2] >= '0' && dateConversion(apTokens[MB3_TX_PAYMENTDATE2], acTmp, iDateFmt))
   {
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      strcpy(pOutRec->PaidDate2, acTmp);
      if (dPen2 > 0.0 && memcmp(acTmp, pOutRec->DueDate2, 8) > 0)
         sprintf(pOutRec->PenAmt2, "%.2f", dPen2);
   } 

   // Default date
   pOutRec->isDelq[0] = '0';
   if (dTaxTotal > 0.0 && *apTokens[MB3_TX_DEFAULTDATE] >= '0' && dateConversion(apTokens[MB3_TX_DEFAULTDATE], acTmp, iDateFmt))
   {
      strcpy(pOutRec->Def_Date, acTmp);
      acTmp[4] = 0;
      strcpy(pOutRec->DelqYear, acTmp);
      pOutRec->isDelq[0] = '1';
#ifdef _DEBUG
      //if (*apTokens[MB3_TX_ROLLCATEGORY] != 'D')
      //   iTmp = 0;
#endif
   }

   // Redemption date
   if (*apTokens[MB3_TX_REDEEMEDDATE] >= '0' && dateConversion(apTokens[MB3_TX_REDEEMEDDATE], acTmp, iDateFmt))
   {
      pOutRec->isDelq[0] = '0';
      pOutRec->DelqStatus[0] = '1';
#ifdef _DEBUG
      //if (*apTokens[MB3_TX_ROLLCATEGORY] != 'D')
      //   iTmp = 0;
#endif
   }

   // Owner
   vmemcpy(pOutRec->OwnerInfo.Name1, apTokens[MB3_TX_OWNER], TAX_NAME, 0);

   // Mailing addr
   int iAdr = 0;
   if (!_memicmp(apTokens[MB3_TX_MAILADDRESS1], "C/O", 3) || *apTokens[MB3_TX_MAILADDRESS1] == '%')
   {
      strcpy(pOutRec->OwnerInfo.CareOf, apTokens[MB3_TX_MAILADDRESS1]);
      iAdr = 1;
   } else if (!_memicmp(apTokens[MB3_TX_MAILADDRESS2], "C/O", 3) || *apTokens[MB3_TX_MAILADDRESS2] == '%')
   {
      strcpy(pOutRec->OwnerInfo.CareOf, apTokens[MB3_TX_MAILADDRESS2]);
      iAdr = 2;
   }

   iTmp = 0;
   if (!_memicmp(apTokens[MB3_TX_MAILADDRESS1], "DBA", 3))
   {
      if (strlen(apTokens[MB3_TX_MAILADDRESS1]) > 4)
         strcpy(pOutRec->OwnerInfo.Dba, apTokens[MB3_TX_MAILADDRESS1]);
      iTmp = 1;
   } else if (!_memicmp(apTokens[MB3_TX_MAILADDRESS2], "DBA", 3))
   {
      if (strlen(apTokens[MB3_TX_MAILADDRESS2]) > 4)
         strcpy(pOutRec->OwnerInfo.Dba, apTokens[MB3_TX_MAILADDRESS2]);
      iTmp = 2;
   }

   if (iAdr < iTmp)
      iAdr = iTmp;

   for (iTmp = 0; iAdr < 4; iAdr++)
   {
      // Ignore bad address
      if (strlen(apTokens[MB3_TX_MAILADDRESS1+iAdr]) < 4)
         break;
      strcpy(pOutRec->OwnerInfo.MailAdr[iTmp++], apTokens[MB3_TX_MAILADDRESS1+iAdr]);
   }

   // Return 1 if no tax
   if (dTaxTotal > 0)
      return 0;
   else
      return 10;
}

/**************************** MB_ParseTaxBaseG2 ******************************
 *
 * RollCategory: 
 *    CU,SU,DU: unsecured
 *    CS,SS,DS: secured
 *
 * Use ChrgDate or PaidDate to test for new record.
 * ALP, COL, GLE, HUM, LAK, LAS, MOD, PLU, SHA, SIS, TEH, TUO
 *
 *****************************************************************************/

int MB_ParseTaxBaseG2(char *pOutbuf, char *pInbuf, int iDateFmt)
{
   char     acTmp[256], *pTmp;
   int      iTmp;
   double	dTmp, dTax1, dTax2, dPaid1, dPaid2, dPen1, dPen2, dCost1, dCost2, dTaxTotal, dDueTotal, dFeeTotal;
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB2_TX_RATEUSED)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Ignore unsecured record
   if (*(apTokens[MB2_TX_ROLLCATEGORY]+1) == 'U')
      return 1;

   // Check for dup APN
   if (!memcmp(pOutbuf, apTokens[MB2_TX_ASMT], iApnLen) && *(apTokens[MB2_TX_ROLLCHGNUM]) > ' ')
      return 11;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // Tax Year
   iTaxYear = atol(apTokens[MB2_TX_TAXYEAR]);
   //if (iTaxYear > lTaxYear || iTaxYear < lTaxYear-1)
   if (iTaxYear != lTaxYear)
      return iTaxYear;

   sprintf(pOutRec->TaxYear, "%d", iTaxYear);
   strcpy(pOutRec->OwnerInfo.TaxYear, pOutRec->TaxYear);

   // APN
   strcpy(pOutRec->Apn, apTokens[MB2_TX_ASMT]);
   strcpy(pOutRec->OwnerInfo.Apn, apTokens[MB2_TX_ASMT]);

   // BillNum
   if  (m_bUseFeePrcl)
      strcpy(pOutRec->BillNum, apTokens[MB2_TX_FEEPARCEL]);

   // TRA
   iTmp = atol(apTokens[MB2_TX_TRA]);
   sprintf(pOutRec->TRA, "%0.6d", iTmp);

   // Record type
   if (*apTokens[MB2_TX_ROLLCATEGORY] == 'S')
   {
      pOutRec->isSupp[0] ='1';
      pOutRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
   } else if (*(apTokens[MB2_TX_ROLLCATEGORY]+1) == 'S')
   {
      pOutRec->isSecd[0] ='1';
      pOutRec->BillType[0] = BILLTYPE_SECURED;
   }

   // Initialize
   dTax1=dTax2=0.0;
   pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   // Check for Tax amount
   if (*apTokens[MB2_TX_TAXAMT1] != '(')
   {
      if (strchr(apTokens[MB2_TX_TAXAMT1], '$'))
         dollar2Num(apTokens[MB2_TX_TAXAMT1], acTmp);
      else
         strcpy(acTmp, apTokens[MB2_TX_TAXAMT1]);
      dTax1 = atof(acTmp);
   }
  
#ifdef _DEBUG
   //if (!memcmp(apTokens[MB2_TX_ASMT], "306151006", 9))
   //   iTmp = 0;
#endif

   if (*apTokens[MB2_TX_TAXAMT2] != '(')
   {
      if (strchr(apTokens[MB2_TX_TAXAMT2], '$'))
         dollar2Num(apTokens[MB2_TX_TAXAMT2], acTmp);
      else
         strcpy(acTmp, apTokens[MB2_TX_TAXAMT2]);
      dTax2 = atof(acTmp);
   }

   // Due date
   if (dTax1 > 0.0)
   {
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);
      if (*apTokens[MB2_TX_DUEDATE1] >= '0' && dateConversion(apTokens[MB2_TX_DUEDATE1], acTmp, iDateFmt))
         strcpy(pOutRec->DueDate1, acTmp);
      else
         InstDueDate(pOutRec->DueDate1, 1, lTaxYear);
   } else 
   {
      dTax1 = 0.0;
      pOutRec->Inst1Status[0] = TAX_BSTAT_NOTAX;
   }
   if (dTax2 > 0.0)
   {
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);
      if (*apTokens[MB2_TX_DUEDATE2] >= '0' && dateConversion(apTokens[MB2_TX_DUEDATE2], acTmp, iDateFmt))
         strcpy(pOutRec->DueDate2, acTmp);
      else
         InstDueDate(pOutRec->DueDate2, 2, lTaxYear);
   } else 
   {
      dTax2 = 0.0;
      pOutRec->Inst2Status[0] = TAX_BSTAT_NOTAX;
   }

   dTaxTotal = dTax1+dTax2;

   // Paid
   dPaid1 = atof(apTokens[MB2_TX_PAIDAMT1]);
   if (dPaid1 > 0.0)
   {
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt1, "%.2f", dPaid1);
   }
   dPaid2 = atof(apTokens[MB2_TX_PAIDAMT2]);
   if (dPaid2 > 0.0)
   {
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt2, "%.2f", dPaid2);
   }

   // Penalty
   dCost1 = dCost2 = dPen1 = dPen2 = 0.0;
   if (*apTokens[MB2_TX_PENCHRGDATE1] > ' ')
   {
      pTmp = dateConversion(apTokens[MB2_TX_PENCHRGDATE1],acTmp, iDateFmt);
      if (pTmp) 
      {
         dPen1 = atof(apTokens[MB2_TX_PENAMT1]);
         dCost1 = atof(apTokens[MB2_TX_COST1]);
      }
   }
   if (*apTokens[MB2_TX_PENCHRGDATE2] > ' ')
   {
      pTmp = dateConversion(apTokens[MB2_TX_PENCHRGDATE2],acTmp, iDateFmt);
      if (pTmp)
      {
         dPen2 = atof(apTokens[MB2_TX_PENAMT2]);
         dCost2 = atof(apTokens[MB2_TX_COST2]);
      }
   }

   // Penalty
   dDueTotal = 0.0;
   if (dTax1 > dPaid1)
   {
      dDueTotal = dTax1;
      if (ChkDueDate(1, pOutRec->DueDate1))
      {
         dDueTotal += dPen1+dCost1;
         sprintf(pOutRec->PenAmt1, "%.2f", dPen1);
         pOutRec->Inst1Status[0] = TAX_BSTAT_PASTDUE;
      } else
         pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   }
   if (dTax2 > dPaid2)
   {
      dDueTotal += dTax2;
      if (ChkDueDate(2, pOutRec->DueDate2))
      {
         dDueTotal += dPen2+dCost2;
         sprintf(pOutRec->PenAmt2, "%.2f", dPen2);
         pOutRec->Inst2Status[0] = TAX_BSTAT_PASTDUE;
      } else
         pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   }

   // Total Fee
   dFeeTotal = atof(apTokens[MB2_TX_TOTALFEES]);
   if (dFeeTotal > 0.0)
   {
      dTmp  = atof(apTokens[MB2_TX_TOTALFEESPAID1]);
      dTmp += atof(apTokens[MB2_TX_TOTALFEESPAID2]);
      dFeeTotal -= dTmp;
      iTmp = sprintf(pOutRec->TotalFees, "%.2f", dFeeTotal);
   }

   // Total Due
   if (dDueTotal > 0.0)
      sprintf(pOutRec->TotalDue, "%.2f", dDueTotal+dFeeTotal);

   // Paid Date
   if (*apTokens[MB2_TX_PAYMENTDATE1] >= '0' && dateConversion(apTokens[MB2_TX_PAYMENTDATE1], acTmp, iDateFmt))
   {
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      strcpy(pOutRec->PaidDate1, acTmp);
      if (dPen1 > 0.0 && memcmp(acTmp, pOutRec->DueDate1, 8) > 0)
         sprintf(pOutRec->PenAmt1, "%.2f", dPen1);
   }  

   if (*apTokens[MB2_TX_PAYMENTDATE2] >= '0' && dateConversion(apTokens[MB2_TX_PAYMENTDATE2], acTmp, iDateFmt))
   {
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      strcpy(pOutRec->PaidDate2, acTmp);
      if (dPen2 > 0.0 && memcmp(acTmp, pOutRec->DueDate2, 8) > 0)
         sprintf(pOutRec->PenAmt2, "%.2f", dPen2);
   } 

   // Default date
   pOutRec->isDelq[0] = '0';
   if (dTaxTotal > 0.0 && *apTokens[MB2_TX_DEFAULTDATE] >= '0' && dateConversion(apTokens[MB2_TX_DEFAULTDATE], acTmp, iDateFmt))
   {
      strcpy(pOutRec->Def_Date, acTmp);
      acTmp[4] = 0;
      strcpy(pOutRec->DelqYear, acTmp);
      pOutRec->isDelq[0] = '1';
#ifdef _DEBUG
      //if (*apTokens[MB2_TX_ROLLCATEGORY] != 'D')
      //   iTmp = 0;
#endif
   }

   // Redemption date
   if (*apTokens[MB2_TX_REDEEMEDDATE] >= '0' && dateConversion(apTokens[MB2_TX_REDEEMEDDATE], acTmp, iDateFmt))
   {
      pOutRec->isDelq[0] = '0';
      pOutRec->DelqStatus[0] = '1';
#ifdef _DEBUG
      //if (*apTokens[MB2_TX_ROLLCATEGORY] != 'D')
      //   iTmp = 0;
#endif
   }

   // Owner
   vmemcpy(pOutRec->OwnerInfo.Name1, apTokens[MB2_TX_OWNER], TAX_NAME, 0);

   // Mailing addr
   int iAdr = 0;
   if (!_memicmp(apTokens[MB2_TX_MAILADDRESS1], "C/O", 3) || *apTokens[MB2_TX_MAILADDRESS1] == '%')
   {
      strcpy(pOutRec->OwnerInfo.CareOf, apTokens[MB2_TX_MAILADDRESS1]);
      iAdr = 1;
   } else if (!_memicmp(apTokens[MB2_TX_MAILADDRESS2], "C/O", 3) || *apTokens[MB2_TX_MAILADDRESS2] == '%')
   {
      strcpy(pOutRec->OwnerInfo.CareOf, apTokens[MB2_TX_MAILADDRESS2]);
      iAdr = 2;
   }

   iTmp = 0;
   if (!_memicmp(apTokens[MB2_TX_MAILADDRESS1], "DBA", 3))
   {
      if (strlen(apTokens[MB2_TX_MAILADDRESS1]) > 4)
         strcpy(pOutRec->OwnerInfo.Dba, apTokens[MB2_TX_MAILADDRESS1]);
      iTmp = 1;
   } else if (!_memicmp(apTokens[MB2_TX_MAILADDRESS2], "DBA", 3))
   {
      if (strlen(apTokens[MB2_TX_MAILADDRESS2]) > 4)
         strcpy(pOutRec->OwnerInfo.Dba, apTokens[MB2_TX_MAILADDRESS2]);
      iTmp = 2;
   }

   if (iAdr < iTmp)
      iAdr = iTmp;

   for (iTmp = 0; iAdr < 4; iAdr++)
   {
      // Ignore bad address
      if (strlen(apTokens[MB2_TX_MAILADDRESS1+iAdr]) < 4)
         break;
      strcpy(pOutRec->OwnerInfo.MailAdr[iTmp++], apTokens[MB2_TX_MAILADDRESS1+iAdr]);
   }

   // Return 1 if no tax
   if (dTaxTotal > 0)
   {
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal);
      return 0;
   } else
      return 10;
}

/**************************** MB_ParseTaxBaseG1 ******************************
 *
 * Use ChrgDate or PaidDate to test for new record.
 * AMA
 *
 *****************************************************************************/

int MB_ParseTaxBaseG1(char *pOutbuf, char *pInbuf, int iDateFmt)
{
   char     acTmp[256], acApn[32], *pTmp;
   int      iTmp;
   double	dTmp, dTax1, dTax2, dPaid1, dPaid2, dPen1, dPen2, dCost1, dCost2, dTaxTotal, dDueTotal, dFeeTotal;
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB1_TX_RATEUSED)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   //iTmp = atoin(apTokens[MB1_TX_ASMT], 3);
   //if (!iTmp || (iTmp >= 800 && iTmp != 910))
   //   return 1;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // Tax Year
   iTaxYear = atol(apTokens[MB1_TX_TAXYEAR]);
   if (iTaxYear != lTaxYear)
      return iTaxYear;

   sprintf(pOutRec->TaxYear, "%d", iTaxYear);
   strcpy(pOutRec->OwnerInfo.TaxYear, pOutRec->TaxYear);

#ifdef _DEBUG
   //if (!memcmp(apTokens[MB1_TX_ASMT], "001090004", 9))
   //   iTmp = 0;
#endif

   // APN
   iTmp = atol(apTokens[MB1_TX_ASMT]+9);
   if (iTmp == 1 || iTmp == 100)
      sprintf(acApn, "%.9s000", apTokens[MB1_TX_ASMT]);
   else
      strcpy(acApn, apTokens[MB1_TX_ASMT]);

   strcpy(pOutRec->Apn, acApn);
   strcpy(pOutRec->OwnerInfo.Apn, apTokens[MB1_TX_ASMT]);
   if (m_bUseFeePrcl)
   {
      strcpy(pOutRec->BillNum, apTokens[MB1_TX_ASMT]);
      strcpy(pOutRec->OwnerInfo.BillNum, apTokens[MB1_TX_ASMT]);
   }
   pOutRec->PaidStatus[0] = *apTokens[MB1_TX_STATUS];

   // BillType
   if (!memcmp(apTokens[MB1_TX_BILLTYPE], "SB", 2) || !memcmp(apTokens[MB1_TX_BILLTYPE], "SR", 2))
   {
      pOutRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
      pOutRec->isSupp[0] ='1';
   } else
   {
      pOutRec->isSecd[0] ='1';
      if (!memcmp(apTokens[MB1_TX_BILLTYPE], "BR", 2)          // Roll correction refund
         || !memcmp(apTokens[MB1_TX_BILLTYPE], "AB", 2)        // Addiditional bill due to roll change
         || !memcmp(apTokens[MB1_TX_BILLTYPE], "CR", 2))       // Current year refund due to roll change
         pOutRec->BillType[0] = BILLTYPE_ROLL_CORRECTION;
      else if (!memcmp(apTokens[MB1_TX_BILLTYPE], "SP", 2)     // Original bill
            || !memcmp(apTokens[MB1_TX_BILLTYPE], "NO", 2)     // New owner as result of roll change (bill to new owner)
            || !memcmp(apTokens[MB1_TX_BILLTYPE], "CB", 2)     // Corrected bill
         || !memcmp(apTokens[MB1_TX_BILLTYPE], "NB", 2))       // New bill due to roll change (bill to owner as of LDR)
         pOutRec->BillType[0] = BILLTYPE_SECURED;
      else
         pOutRec->BillType[0] = BILLTYPE_SECURED;              // RR, PR, CR
   }

   // TRA
   iTmp = atol(apTokens[MB1_TX_TRA]);
   sprintf(pOutRec->TRA, "%0.6d", iTmp);

   // Check for Tax amount
   if (*apTokens[MB1_TX_TAXAMT1] != '(')
   {
      if (strchr(apTokens[MB1_TX_TAXAMT1], '$'))
         dollar2Num(apTokens[MB1_TX_TAXAMT1], acTmp);
      else
         strcpy(acTmp, apTokens[MB1_TX_TAXAMT1]);
      dTax1 = atof(acTmp);
   } else
   {
      dollar2Num(apTokens[MB1_TX_TAXAMT1]+1, acTmp);
      dTax1 = -(atof(acTmp));
   }

   if (*apTokens[MB1_TX_TAXAMT2] != '(')
   {
      if (strchr(apTokens[MB1_TX_TAXAMT2], '$'))
         dollar2Num(apTokens[MB1_TX_TAXAMT2], acTmp);
      else
         strcpy(acTmp, apTokens[MB1_TX_TAXAMT2]);
      dTax2 = atof(acTmp);
   } else
   {
      dollar2Num(apTokens[MB1_TX_TAXAMT2], acTmp);
      dTax2 = -atof(acTmp);
   }

   // Tax amt
   if (dTax1)
   {
      // 08/19/2020 - Added extra value to total
      //dTax1 += atof(apTokens[MB1_TX_COST1]);
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);
   }
   if (dTax2)
   {
      // 08/19/2020 - Added extra value to total
      //dTax2 += atof(apTokens[MB1_TX_COST2]);
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);
   }

   dTaxTotal = dTax1+dTax2;
   if (dTaxTotal)
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal);

   // Init status
   pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   // Paid
   dPaid1 = atof(apTokens[MB1_TX_PAIDAMT1]);
   if (dPaid1 > 0.0)
   {
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt1, "%.2f", dPaid1);
   }
   dPaid2 = atof(apTokens[MB1_TX_PAIDAMT2]);
   if (dPaid2 > 0.0)
   {
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt2, "%.2f", dPaid2);
   }

   // Penalty
   dCost1 = dCost2 = dPen1 = dPen2 = 0.0;
   if (*apTokens[MB1_TX_PENCHRGDATE1] > ' ')
   {
      pTmp = dateConversion(apTokens[MB1_TX_PENCHRGDATE1],acTmp, iDateFmt);
      if (pTmp) 
      {
         dPen1 = atof(apTokens[MB1_TX_PENAMT1]);
         dCost1 = atof(apTokens[MB1_TX_COST1]);
      }
   }
   if (*apTokens[MB1_TX_PENCHRGDATE2] > ' ')
   {
      pTmp = dateConversion(apTokens[MB1_TX_PENCHRGDATE2],acTmp, iDateFmt);
      if (pTmp)
      {
         dPen2 = atof(apTokens[MB1_TX_PENAMT2]);
         dCost2 = atof(apTokens[MB1_TX_COST2]);
      }
   }

   // Due date
   if (dTax1 > 0.0)
   {
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);
      if (*apTokens[MB1_TX_DUEDATE1] >= '0' && dateConversion(apTokens[MB1_TX_DUEDATE1], acTmp, iDateFmt))
         strcpy(pOutRec->DueDate1, acTmp);
      else
         InstDueDate(pOutRec->DueDate1, 1, lTaxYear);
   } else 
   {
      dTax1 = 0.0;
      pOutRec->Inst1Status[0] = TAX_BSTAT_NOTAX;
   }
   if (dTax2 > 0.0)
   {
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);
      if (*apTokens[MB1_TX_DUEDATE2] >= '0' && dateConversion(apTokens[MB1_TX_DUEDATE2], acTmp, iDateFmt))
         strcpy(pOutRec->DueDate2, acTmp);
      else
         InstDueDate(pOutRec->DueDate2, 2, lTaxYear);
   } else 
   {
      dTax2 = 0.0;
      pOutRec->Inst2Status[0] = TAX_BSTAT_NOTAX;
   }

   // Penalty
   dDueTotal = 0.0;
   if (dTax1 > dPaid1)
   {
      dDueTotal = dTax1;
      if (ChkDueDate(1, pOutRec->DueDate1))
      {
         dDueTotal += dPen1+dCost1;
         sprintf(pOutRec->PenAmt1, "%.2f", dPen1);
         pOutRec->Inst1Status[0] = TAX_BSTAT_PASTDUE;
      } else
         pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   } else if (!dTax1)
         pOutRec->Inst1Status[0] = TAX_BSTAT_NOTAX;

   if (dTax2 > dPaid2)
   {
      dDueTotal += dTax2;
      if (ChkDueDate(2, pOutRec->DueDate2))
      {
         dDueTotal += dPen2+dCost2;
         sprintf(pOutRec->PenAmt2, "%.2f", dPen2);
         pOutRec->Inst2Status[0] = TAX_BSTAT_PASTDUE;
      } else
         pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   } else if (!dTax2)
         pOutRec->Inst2Status[0] = TAX_BSTAT_NOTAX;

   // Total Fee
   dFeeTotal = atof(apTokens[MB1_TX_TOTALFEES]);
   if (dFeeTotal > 0.0)
   {
      dTmp  = atof(apTokens[MB1_TX_TOTALFEESPAID1]);
      dTmp += atof(apTokens[MB1_TX_TOTALFEESPAID2]);
      dFeeTotal -= dTmp;
      iTmp = sprintf(pOutRec->TotalFees, "%.2f", dFeeTotal);
   }

   // TotalDue 
   if (dDueTotal > 0.0)
      sprintf(pOutRec->TotalDue, "%.2f", dDueTotal+dFeeTotal);

   // Paid Date
   if (*apTokens[MB1_TX_PAYMENTDATE1] >= '0' && dateConversion(apTokens[MB1_TX_PAYMENTDATE1], acTmp, iDateFmt))
   {
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      strcpy(pOutRec->PaidDate1, acTmp);
   } else if (dTax1 < 0.0)
      pOutRec->Inst1Status[0] = TAX_BSTAT_REFUND;

   if (*apTokens[MB1_TX_PAYMENTDATE2] >= '0' && dateConversion(apTokens[MB1_TX_PAYMENTDATE2], acTmp, iDateFmt))
   {
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      strcpy(pOutRec->PaidDate2, acTmp);
   } else if (dTax2 < 0.0)
      pOutRec->Inst2Status[0] = TAX_BSTAT_REFUND;

   // Default date
   pOutRec->isDelq[0] = '0';
   if (dTaxTotal > 0.0 && *apTokens[MB1_TX_DEFAULTDATE] >= '0' && dateConversion(apTokens[MB1_TX_DEFAULTDATE], acTmp, iDateFmt))
   {
      strcpy(pOutRec->Def_Num, apTokens[MB1_TX_DEFAULTNUM]);
      strcpy(pOutRec->Def_Date, acTmp);
      acTmp[4] = 0;
      strcpy(pOutRec->DelqYear, acTmp);
      pOutRec->isDelq[0] = '1';
   }

   // Redemption date
   if (*apTokens[MB1_TX_REDEEMEDDATE] >= '0' && dateConversion(apTokens[MB1_TX_REDEEMEDDATE], acTmp, iDateFmt))
   {
      pOutRec->isDelq[0] = '0';
      pOutRec->DelqStatus[0] = '1';
   }
  
   // Owner
   vmemcpy(pOutRec->OwnerInfo.Name1, apTokens[MB1_TX_OWNER], TAX_NAME, 0);

   // Mailing addr
   int iAdr = 0;
   if (!memcmp(apTokens[MB1_TX_MAILADDRESS1], "C/O", 3))
   {
      strcpy(pOutRec->OwnerInfo.CareOf, apTokens[MB1_TX_MAILADDRESS1]);
      iAdr = 1;
   } else if (!memcmp(apTokens[MB1_TX_MAILADDRESS2], "C/O", 3))
   {
      strcpy(pOutRec->OwnerInfo.CareOf, apTokens[MB1_TX_MAILADDRESS2]);
      iAdr = 2;
   }

   iTmp = 0;
   if (!memcmp(apTokens[MB1_TX_MAILADDRESS1], "DBA", 3))
   {
      if (strlen(apTokens[MB1_TX_MAILADDRESS1]) > 4)
         strcpy(pOutRec->OwnerInfo.Dba, apTokens[MB1_TX_MAILADDRESS1]);
      iTmp = 1;
   } else if (!memcmp(apTokens[MB1_TX_MAILADDRESS2], "DBA", 3))
   {
      if (strlen(apTokens[MB1_TX_MAILADDRESS2]) > 4)
         strcpy(pOutRec->OwnerInfo.Dba, apTokens[MB1_TX_MAILADDRESS2]);
      iTmp = 2;
   }

   if (iAdr < iTmp)
      iAdr = iTmp;

   for (iTmp = 0; iAdr < 4; iAdr++)
      strcpy(pOutRec->OwnerInfo.MailAdr[iTmp++], apTokens[MB1_TX_MAILADDRESS1+iAdr]);

   // Return 1 if no tax
   if (dTaxTotal > 0)
      return 0;
   else
      return 10;
}

/**************************** MB_ParseTaxBase ********************************
 *
 * Use ChrgDate or PaidDate to test for new record.
 * BUT - doesn't have cost.  However, if delinquent, inst2 is added by $31 (003362008000) 5/6/2018
 *
 *****************************************************************************/

int MB_ParseTaxBase(char *pOutbuf, char *pInbuf, int iDateFmt)
{
   char     acTmp[256], *pTmp;
   int      iTmp;
   double	dTmp, dTax1, dTax2, dPaid1, dPaid2, dPen1, dPen2, dTaxTotal, dDueTotal;
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB_TX_TAXABILITY)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // Tax Year
   iTaxYear = atol(apTokens[MB_TX_TAXYEAR]);
   // 11/21/2024 - Sony
   if (iTaxYear != lTaxYear)
   //if (iTaxYear > lTaxYear || iTaxYear < lTaxYear-1)
      return iTaxYear;

   // Ignore roll change record
   if (*apTokens[MB_TX_ROLLCHGNUM] > ' ')
      return 11;

   sprintf(pOutRec->TaxYear, "%d", iTaxYear);
   strcpy(pOutRec->OwnerInfo.TaxYear, pOutRec->TaxYear);

   // APN
   strcpy(pOutRec->Apn, apTokens[MB_TX_ASMT]);
   strcpy(pOutRec->OwnerInfo.Apn, apTokens[MB_TX_ASMT]);

   // BillNum
   if  (m_bUseFeePrcl)
      strcpy(pOutRec->BillNum, apTokens[MB_TX_FEEPARCEL]);

   sprintf(pOutRec->TaxYear, "%d", lTaxYear);
   strcpy(pOutRec->OwnerInfo.TaxYear, pOutRec->TaxYear);
   pOutRec->isSecd[0] ='1';
   pOutRec->BillType[0] = BILLTYPE_SECURED;

   // TRA

   // Check for Tax amount
   if (*apTokens[MB_TX_TAXAMT1] != '(')
   {
      if (strchr(apTokens[MB_TX_TAXAMT1], '$'))
         dollar2Num(apTokens[MB_TX_TAXAMT1], acTmp);
      else
         strcpy(acTmp, apTokens[MB_TX_TAXAMT1]);
      dTax1 = atof(acTmp);
   } else
   {
      dollar2Num(apTokens[MB_TX_TAXAMT1]+1, acTmp);
      dTax1 = -(atof(acTmp));
   }

   if (*apTokens[MB_TX_TAXAMT2] != '(')
   {
      if (strchr(apTokens[MB_TX_TAXAMT2], '$'))
         dollar2Num(apTokens[MB_TX_TAXAMT2], acTmp);
      else
         strcpy(acTmp, apTokens[MB_TX_TAXAMT2]);
      dTax2 = atof(acTmp);
   } else
   {
      dollar2Num(apTokens[MB_TX_TAXAMT2], acTmp);
      dTax2 = -atof(acTmp);
   }

   if (dTax1)
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);
   if (dTax2)
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);

   dTaxTotal = dTax1+dTax2;
   if (dTaxTotal > 0.0)
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal);
   pOutRec->isDelq[0] = '0';

   // Penalty
   dPen1 = dPen2 = 0.0;
   if (*apTokens[MB_TX_PENCHRGDATE1] > ' ')
   {
      pTmp = dateConversion(apTokens[MB_TX_PENCHRGDATE1],acTmp, MM_DD_YYYY_1);
      if (pTmp) 
         dPen1 = atof(apTokens[MB_TX_PENAMT1]);
   }
   if (*apTokens[MB_TX_PENCHRGDATE2] > ' ')
   {
      pTmp = dateConversion(apTokens[MB_TX_PENCHRGDATE2],acTmp, MM_DD_YYYY_1);
      if (pTmp)
         dPen2 = atof(apTokens[MB_TX_PENAMT2]);
   }

   // Init status
   pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   // Paid
   dPaid1 = atof(apTokens[MB_TX_PAIDAMT1]);
   if (dPaid1 > 0.0)
   {
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt1, "%.2f", dPaid1);
   }
   dPaid2 = atof(apTokens[MB_TX_PAIDAMT2]);
   if (dPaid2 > 0.0)
   {
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt2, "%.2f", dPaid2);
   }

   // Total due
   dDueTotal = 0.0;
   if (dTax1 > dPaid1)
   {
      dDueTotal = dTax1;
      if (ChkDueDate(1, lTaxYear))
      {
         dDueTotal += dPen1;
         sprintf(pOutRec->PenAmt1, "%.2f", dPen1);
         pOutRec->Inst1Status[0] = TAX_BSTAT_PASTDUE;
      } 

      InstDueDate(pOutRec->DueDate1, 1, lTaxYear);
   }
   if (dTax2 > dPaid2)
   {
      dDueTotal += dTax2;
      if (ChkDueDate(2, lTaxYear))
      {
         dDueTotal += dPen2;
         sprintf(pOutRec->PenAmt2, "%.2f", dPen2);
         pOutRec->Inst2Status[0] = TAX_BSTAT_PASTDUE;
      } 

      InstDueDate(pOutRec->DueDate2, 2, lTaxYear);
   }

   if (dDueTotal > 0.0)
      sprintf(pOutRec->TotalDue, "%.2f", dDueTotal);

   // Fee Paid
   dTmp = atof(apTokens[MB_TX_TOTALFEES]);
   if (dTmp > 0.0)
      iTmp = sprintf(pOutRec->TotalFees, "%.2f", dTmp);

#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_TX_ASMT], "003076011000", 12))
   //   iTmp = 0;
#endif

   // Paid Date
   if (*apTokens[MB_TX_PAYMENTDATE1] >= '0' && dateConversion(apTokens[MB_TX_PAYMENTDATE1], acTmp, iDateFmt))
   {
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      strcpy(pOutRec->PaidDate1, acTmp);
   } else if (dTax1 > 0.0)
   {
      InstDueDate(pOutRec->DueDate1, 1, lTaxYear);
   }

   if (*apTokens[MB_TX_PAYMENTDATE2] >= '0' && dateConversion(apTokens[MB_TX_PAYMENTDATE2], acTmp, iDateFmt))
   {
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      strcpy(pOutRec->PaidDate2, acTmp);
   } else if (dTax2 > 0.0)
   {
      InstDueDate(pOutRec->DueDate2, 2, lTaxYear);
   }

   // Pen charge date
   //if (*apTokens[MB_TX_PENCHRGDDATE1] >= '0' && dateConversion(apTokens[MB_TX_PENCHRGDDATE1], acTmp, iDateFmt))
   //   strcpy(pOutRec->DueDate1, acTmp);
   //if (*apTokens[MB_TX_PENCHRGDDATE2] >= '0' && dateConversion(apTokens[MB_TX_PENCHRGDDATE2], acTmp, iDateFmt))
   //   strcpy(pOutRec->DueDate2, acTmp);

   // Default date - no data
   //if (*apTokens[MB_TX_DEFAULTDATE] >= '0' && dateConversion(apTokens[MB_TX_DEFAULTDATE], acTmp, iDateFmt))
   //{
   //   strcpy(pOutRec->Def_Date, acTmp);
   //   acTmp[4] = 0;
   //   strcpy(pOutRec->DelqYear, acTmp);
   //}

   // Calculate unpaid amt
   //dTmp = (dTaxTotal + dPen1 + dPen2) - (dPaid1 + dPaid2);
   //if (dTmp > 0.0)
   //   sprintf(pOutRec->TotalDue, "%.2f", dTmp);

   //pOutRec->isDelq[0] = '0';

   
#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_TX_ASMT], "005080014506", 12))
   //   iTmp = 0;
#endif

   // Owner
   //vmemcpy(pOutRec->OwnerInfo.Name1, apTokens[MB_TX_OWNER], TAX_NAME, 0);

   // Mailing addr
   //int iAdr = 0;
   //if (!memcmp(apTokens[MB_TX_MAILADDRESS1], "C/O", 3))
   //{
   //   strcpy(pOutRec->OwnerInfo.CareOf, apTokens[MB_TX_MAILADDRESS1]);
   //   iAdr = 1;
   //} else if (!memcmp(apTokens[MB_TX_MAILADDRESS2], "C/O", 3))
   //{
   //   strcpy(pOutRec->OwnerInfo.CareOf, apTokens[MB_TX_MAILADDRESS2]);
   //   iAdr = 2;
   //}

   //iTmp = 0;
   //if (!memcmp(apTokens[MB_TX_MAILADDRESS1], "DBA", 3))
   //{
   //   if (strlen(apTokens[MB_TX_MAILADDRESS1]) > 4)
   //      strcpy(pOutRec->OwnerInfo.Dba, apTokens[MB_TX_MAILADDRESS1]);
   //   iTmp = 1;
   //} else if (!memcmp(apTokens[MB_TX_MAILADDRESS2], "DBA", 3))
   //{
   //   if (strlen(apTokens[MB_TX_MAILADDRESS2]) > 4)
   //      strcpy(pOutRec->OwnerInfo.Dba, apTokens[MB_TX_MAILADDRESS2]);
   //   iTmp = 2;
   //}

   //if (iAdr < iTmp)
   //   iAdr = iTmp;

   //for (iTmp = 0; iAdr < 4; iAdr++)
   //   strcpy(pOutRec->OwnerInfo.MailAdr[iTmp++], apTokens[MB_TX_MAILADDRESS1+iAdr]);

   // Return 1 if no tax
   if (dTaxTotal > 0)
      return 0;
   else
      return 10;

   return 0;
}

/**************************** MB_Load_TaxBase ****************************
 *
 * Create import file for ???_Tax.txt and import into ???_Tax_Base table
 * Group0: BUT (Default)
 * Group1: AMA
 * Group2: COL, GLE, HUM, LAK, MOD, PLU, SHA, SIS, TEH
 * Group3: ALP
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int MB_Load_TaxBase(bool bImport, bool bInclOwner, int iMB_Group, int iSkipHdr, int iDateFmt)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acBaseFile[_MAX_PATH], acOwnerFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0, lPriorYear=0, lUnSecd=0, lDupApn=0, lNoTax=0,lTmp;
   FILE     *fdIn, *fdBase, *fdOwner, *fdR01;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];

   LogMsg0("Loading TaxBase group %d", iMB_Group);
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   sprintf(acInFile, sTaxTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   lLastTaxFileDate = getFileDate(acInFile);

   // Only process if new tax file
   iRet = isNewTaxFile(acInFile, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Open input file
   LogMsg("Open Tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acInFile);
      return -2;
   }  

   // Open delq file to get delq info
   sprintf(acInFile, sRedTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Redemption file %s", acInFile);
   fdDelq = fopen(acInFile, "r");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error opening Redemption file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Open output file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acBaseFile);
      return -4;
   }

   if (bInclOwner)
   {
      NameTaxCsvFile(acOwnerFile, myCounty.acCntyCode, "Owner");
      LogMsg("Open output file %s", acOwnerFile);
      fdOwner = fopen(acOwnerFile, "w");
      if (fdOwner == NULL)
      {
         LogMsg("***** Error creating output file: %s\n", acOwnerFile);
         return -5;
      }
   } else
      fdOwner = NULL;

   // Open raw file
   fdR01 = OpenR01(myCounty.acCntyCode);

   // Skip header
   do {
      lTmp = ftell(fdIn);
      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
   } while (pTmp && !isdigit(*pTmp));
   
   if (fseek(fdIn, lTmp, SEEK_SET))
   {
      LogMsg("***** Error setting current position in sale file using fseek()");
      fclose(fdIn);
      return -2;
   }

   // 10/17/2023 Skip header record
   //if (iSkipHdr >= 0)
   //   iTmp = iSkipHdr;
   //else
   //   iTmp = iHdrRows;
   //for (iRet = 0; iRet < iTmp; iRet++)
   //   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Init variables
   GetPrivateProfileString(myCounty.acCntyCode, "UseFeePrcl", "N", acRec, _MAX_PATH, acIniFile);
   if (acRec[0] == 'Y')
      m_bUseFeePrcl = true;
   else
      m_bUseFeePrcl = false;

   lTaxMatch = 0;
   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Replace null char in string (NEV)
      replNull(acRec);

      // Replace NULL value
      replStrAll(acRec, "|NULL", "|");

      // Create new TAXBASE record
      if (iMB_Group == 1)
         iRet = MB_ParseTaxBaseG1(acBuf, acRec, iDateFmt);
      else if (iMB_Group == 2)
         iRet = MB_ParseTaxBaseG2(acBuf, acRec, iDateFmt);
      else if (iMB_Group == 3)
         iRet = MB_ParseTaxBaseG3(acBuf, acRec, iDateFmt);
      else 
      {
         // BUT,
         iRet = MB_ParseTaxBase(acBuf, acRec, iDateFmt);

         // Update DelqYear
         if (!iRet && fdDelq)
            MB_UpdateDelqYear(acBuf);
      }

      if (!iRet)
      {
         lTaxMatch++;
         // Update TRA
         if (fdR01)
            UpdateTRA(acBuf, fdR01);

         // Create delimited record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         fputs(acRec, fdBase);
         lOut++;

         if (bInclOwner)
         {
            Tax_CreateTaxOwnerCsv(acRec, &pTaxBase->OwnerInfo);
            fputs(acRec, fdOwner);
         }
      } else if (iRet > 1900)
         lPriorYear++;
         //LogMsg("---> Load_TaxBase: drop prior tax year %d [%.40s]", iRet, acRec); 
      else if (iRet == 1)
         lUnSecd++;
         //LogMsg("---> Load_TaxBase: drop unsecured record [%.40s]", acRec); 
      else if (iRet == 10)
         lNoTax++;
      else if (iRet == 11)
         lDupApn++;
      else 
         LogMsg("---> Load_TaxBase: drop record %d [%.40s]", lCnt, acRec); 

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdDelq)
      fclose(fdDelq);
   if (fdBase)
      fclose(fdBase);
   if (fdOwner)
      fclose(fdOwner);
   if (fdR01)
      fclose(fdR01);

   LogMsg("Total records processed:       %u", lCnt);
   LogMsg("Total output records:          %u", lOut);
   LogMsg("Total duplicate APN dropped:   %u", lDupApn);
   LogMsg("Total unsecd recs dropped:     %u", lUnSecd);
   LogMsg("Total prior year recs dropped: %u", lPriorYear);
   LogMsg("Total No tax dropped:          %u", lNoTax);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport && lTaxMatch > 100)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (bInclOwner)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_OWNER);      
   } else if (lTaxMatch < 100)
   {
      LogMsg("*** Invalid tax file.  Skip import (%d)", lTaxMatch);
      iRet = 1;
   } else
      iRet = 0;

   return iRet;
}

/****************************** MB_Update_TaxPaid ****************************
 *
 * Update ???_Tax_Base table table
 * Group0: BUT (Default)
 * Group1: AMA
 * Group2: COL, GLE, HUM, LAK, MOD, PLU, SHA, SIS, TEH
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int MB_Update_TaxPaid(bool bImport, int iMB_Group, int iSkipHdr, int iDateFmt)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acPaidFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet, iTmp;
   long     lOut=0, lCnt=0, lPriorYear=0, lUnSecd=0;
   FILE     *fdIn, *fdBase;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];

   LogMsg("Loading TaxBase group %d", iMB_Group);
   NameTaxCsvFile(acPaidFile, myCounty.acCntyCode, "Paid");
   sprintf(acInFile, sTaxTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   lLastTaxFileDate = getFileDate(acInFile);

   // Only process if new tax file
   iRet = isNewTaxFile(acInFile, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Open input file
   LogMsg("Open Tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acInFile);
      return -2;
   }  

   // Open delq file to get delq info
   sprintf(acInFile, sRedTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Redemption file %s", acInFile);
   fdDelq = fopen(acInFile, "r");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error opening Redemption file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Open output file %s", acPaidFile);
   fdBase = fopen(acPaidFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acPaidFile);
      return -4;
   }

   // Skip header record
   if (iSkipHdr >= 0)
      iTmp = iSkipHdr;
   else
      iTmp = iHdrRows;

   for (iRet = 0; iRet < iTmp; iRet++)
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Init variables
   GetPrivateProfileString(myCounty.acCntyCode, "UseFeePrcl", "N", acRec, _MAX_PATH, acIniFile);
   if (acRec[0] == 'Y')
      m_bUseFeePrcl = true;
   else
      m_bUseFeePrcl = false;

   lTaxMatch = 0;
   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Replace NULL
      replStrAll(acRec, "|NULL", "|");

      // Create new TAXBASE record
      if (iMB_Group == 1)
         iRet = MB_ParseTaxBaseG1(acBuf, acRec, iDateFmt);
      else if (iMB_Group == 2)
         iRet = MB_ParseTaxBaseG2(acBuf, acRec, iDateFmt);
      else 
         iRet = MB_ParseTaxBase(acBuf, acRec, iDateFmt);

      if (!iRet)
      {
         lTaxMatch++;

         // Update TaxBase
         iRet = updateTaxBase(myCounty.acCntyCode, acBuf, acRec);
         //Tax_UpdateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);

         if (!iRet)
         {
            strcat(acRec, "\n");
            fputs(acRec, fdBase);
         }
         if (pTaxBase->PaidAmt1[0] > '0' || pTaxBase->PaidAmt2[0] > '0')
         {
            LogMsg0("Update %s, paid1: %s, Amt1: %s, paid2: %s, Amt2: %s", pTaxBase->Apn, pTaxBase->PaidDate1,  pTaxBase->PaidAmt1,  pTaxBase->PaidDate2,  pTaxBase->PaidAmt2);
            lOut++;
         }
      } else if (iRet > 1900)
         lPriorYear++;
         //LogMsg("---> Load_TaxBase: drop prior tax year %d [%.40s]", iRet, acRec); 
      else if (iRet == 1)
         lUnSecd++;
         //LogMsg("---> Load_TaxBase: drop unsecured record [%.40s]", acRec); 
      //else 
      //   LogMsg("---> Load_TaxBase: drop record %d [%.40s]", lCnt, acRec); 

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdDelq)
      fclose(fdDelq);
   if (fdBase)
      fclose(fdBase);

   LogMsg("Total records processed:       %u", lCnt);
   LogMsg("Total output records:          %u", lOut);
   LogMsg("Total unsecd recs dropped:     %u", lUnSecd);
   LogMsg("Total prior year recs dropped: %u", lPriorYear);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport && lTaxMatch > 100)
   {
      //iRet = doTaxUpdate(myCounty.acCntyCode, TAX_BASE);
   } else if (lTaxMatch < 100)
   {
      LogMsg("*** Invalid tax file.  Skip import (%d)", lTaxMatch);
      iRet = 1;
   } else
      iRet = 0;

   return iRet;
}

/***************************** MB_ParseTaxDetail *****************************
 *
 *
 *****************************************************************************/

int MB_ParseTaxDetail(char *pOutbuf, char *pInbuf)
{
   int      iTmp;
   double	dTax1, dTax2;
   TAXDETAIL *pOutRec = (TAXDETAIL *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB_TC_USERID)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDETAIL));

   // Tax Year - Keep current year data only
   iTaxYear = atol(apTokens[MB_TC_TAXYR]);
   if (iTaxYear != lTaxYear)
      return iTaxYear;
   strcpy(pOutRec->TaxYear, apTokens[MB_TC_TAXYR]);

   // APN
   strcpy(pOutRec->Apn, apTokens[MB_TC_ASMT]);

   // Tax amount
   dTax1 = atof(apTokens[MB_TC_TAXAMT1]);
   dTax2 = atof(apTokens[MB_TC_TAXAMT2]);
   sprintf(pOutRec->TaxAmt, "%.2f", dTax1+dTax2+0.001);

   // Tax Rate
   strcpy(pOutRec->TaxRate, apTokens[MB_TC_RATE]);

   // Tax code
   strcpy(pOutRec->TaxCode, apTokens[MB_TC_TAXCODE]);

   return 0;
}

/*************************** MB_ParseTaxDetailG1 *****************************
 *
 * Use ASMT for BillNum.
 * Use first 9 digits of ASMT for APN then add 000 to it.
 * Specific for AMA to handle correction bill (APN=044020113000)
 *
 *****************************************************************************/

int MB_ParseTaxDetailG1(char *pOutbuf, char *pInbuf)
{
   char     acApn[32];
   int      iTmp;
   double	dTax1, dTax2;
   TAXDETAIL *pOutRec = (TAXDETAIL *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB_TC_USERID)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDETAIL));

   // Tax Year - Keep current year data only
   iTaxYear = atol(apTokens[MB_TC_TAXYR]);
   if (iTaxYear != lTaxYear)
      return iTaxYear;
   strcpy(pOutRec->TaxYear, apTokens[MB_TC_TAXYR]);

#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_TC_ASMT], "044020113", 9))
   //   iTmp = 0;
#endif
   // APN
   iTmp = atol(apTokens[MB_TC_ASMT]+9);
   if (iTmp == 1 || iTmp == 100)
      sprintf(acApn, "%.9s000", apTokens[MB_TC_ASMT]);
   else
      strcpy(acApn, apTokens[MB_TC_ASMT]);

   strcpy(pOutRec->Apn, acApn);
   strcpy(pOutRec->BillNum, apTokens[MB_TC_ASMT]);

   // Tax amount
   dTax1 = atof(apTokens[MB_TC_TAXAMT1]);
   dTax2 = atof(apTokens[MB_TC_TAXAMT2]);
   sprintf(pOutRec->TaxAmt, "%.2f", dTax1+dTax2+0.001);

   // Tax Rate
   strcpy(pOutRec->TaxRate, apTokens[MB_TC_RATE]);

   // Tax code
   strcpy(pOutRec->TaxCode, apTokens[MB_TC_TAXCODE]);

   return 0;
}

/**************************** MB_Load_TaxCode ********************************
 *
 * Create Tax Detail file to import into Tax_Items table
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int MB_Load_TaxCode(bool bImport, int iSkipHdr, bool bUpdBillNum)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet, iGrp=0;
   long     lOut=0, lCnt=0, lTmp;
   FILE     *fdOut, *fdIn;

   LogMsg0("Loading Tax Detail");
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Items");
   sprintf(acInFile, sTCTmpl, myCounty.acCntyCode, myCounty.acCntyCode);

   // Open input file
   LogMsg("Open TaxCode file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening TaxCode file: %s\n", acInFile);
      return -2;
   }  
   lLastTaxFileDate = getFileDate(acInFile);

   // Open Output file
   LogMsg("Open Detail file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Detail file: %s\n", acOutFile);
      return -4;
   }

   // Skip header
   do {
      lTmp = ftell(fdIn);
      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
   } while (pTmp && !isdigit(*pTmp));
   
   if (fseek(fdIn, lTmp, SEEK_SET))
   {
      LogMsg("***** Error setting current position in sale file using fseek()");
      fclose(fdIn);
      return -2;
   }

   // 10/17/2023 Skip header record
   //int iTmp;
   //if (iSkipHdr >= 0)
   //   iTmp = iSkipHdr;
   //else
   //   iTmp = iHdrRows;
   //for (iRet = 0; iRet < iTmp; iRet++)
   //   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Replace NULL
      replStrAll(acRec, "|NULL", "|");

      // Create tax detail record
      if (iGrp == 1)
         iRet = MB_ParseTaxDetailG1(acBuf, acRec);
      else
         iRet = MB_ParseTaxDetail(acBuf, acRec);
      if (!iRet)
      {
         // Create delimited record for import
         Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acBuf);

         // Output record			
         lOut++;
         fputs(acRec, fdOut);
      } else if (iRet < 0)
      {
         LogMsg("---> Load_TaxCode: drop record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet && bUpdBillNum)
      {
         // Update BillNum on Tax_Items using data from Tax_Base
         iRet = update_TI_BillNum(myCounty.acCntyCode);
      }

      // Update TotalRate
      if (!iRet)
         iRet = doUpdateTotalRate(myCounty.acCntyCode);
   } else
      iRet = 0;

   return iRet;
}

/***************************** MB_ParseTaxAgency *****************************
 *
 *
 *****************************************************************************/

int MB_ParseTaxAgency(char *pOutbuf, char *pInbuf)
{
   int      iTmp;
   double   dTmp;
   TAXAGENCY *pResult, *pOutRec = (TAXAGENCY *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB_TCM_ISSB863TC)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Ignore inactive code
   if (*apTokens[MB_TCM_STATUS] == 'I')
      return 1;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXAGENCY));

   // Phone
   if (iNumTaxDist > 1)
   {
      pResult = findTaxAgency(apTokens[MB_TCM_TAXCODE], 0);
      if (pResult)
         strcpy(pOutRec->Phone, pResult->Phone);
   } else
   {
      if (*apTokens[MB_TCM_AGENCYTELEPHONE] <= '9' && *apTokens[MB_TCM_AGENCYTELEPHONE] > '0')
      {
         if (*apTokens[MB_TCM_AGENCYTELEPHONE] == '(')
            strcpy(pOutRec->Phone, apTokens[MB_TCM_AGENCYTELEPHONE]);
         else if (strchr(apTokens[MB_TCM_AGENCYTELEPHONE], '-'))
            strcpy(pOutRec->Phone, apTokens[MB_TCM_AGENCYTELEPHONE]);
         else
            sprintf(pOutRec->Phone, "(%.3s) %.3s-%s", apTokens[MB_TCM_AGENCYTELEPHONE], apTokens[MB_TCM_AGENCYTELEPHONE]+3, apTokens[MB_TCM_AGENCYTELEPHONE]+6);
      }
   }

   // Tax code
   strcpy(pOutRec->Code, apTokens[MB_TCM_TAXCODE]);
   pOutRec->TC_Flag[0] = '0';

   // Tax Desc
   strcpy(pOutRec->Agency, apTokens[MB_TCM_DESCR]);

   // Tax Rate
   strcpy(pOutRec->TaxRate, apTokens[MB_TCM_SECUREDBILLRATE]);

   // Tax amount
   dTmp = atof(apTokens[MB_TCM_DOLLARAMT]);
   if (dTmp > 0.0)
      strcpy(pOutRec->TaxAmt, apTokens[MB_TCM_DOLLARAMT]);
      
   return 0;
}

/***************************** MB_Load_TaxCodeMstr ***************************
 *
 * Load TaxCodeMstr file to create import file for Tax_Agency table.
 *
 *****************************************************************************/

int MB_Load_TaxCodeMstr(bool bImport, int iSkipHdr)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0, lTmp;
   FILE     *fdOut, *fdIn;

   LogMsg0("Loading TaxCodeMstr");

   // Load Tax Agency table
   iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acBuf, _MAX_PATH, acIniFile);
   if (iRet > 0)
      iRet = LoadTaxCodeTable(acBuf);

   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Agency");
   sprintf(acInFile, sTCMTmpl, myCounty.acCntyCode, myCounty.acCntyCode);

   // Open input file
   LogMsg("Open TaxCodeMstr file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening TaxCodeMstr file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Open Agency file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acOutFile);
      return -4;
   }

   // Skip header
   do {
      lTmp = ftell(fdIn);
      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
   } while (pTmp && !isdigit(*pTmp));
   
   if (fseek(fdIn, lTmp, SEEK_SET))
   {
      LogMsg("***** Error setting current position in sale file using fseek()");
      fclose(fdIn);
      return -2;
   }

   // 10/17/2023 Skip header record
   //int iTmp;
   //if (iSkipHdr >= 0)
   //   iTmp = iSkipHdr;
   //else
   //   iTmp = iHdrRows;
   //for (iRet = 0; iRet < iTmp; iRet++)
   //   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Replace NULL
      replStrAll(acRec, "|NULL", "|");

      // Create new R01 record
      iRet = MB_ParseTaxAgency(acBuf, acRec);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acBuf);

         // Output record			
         lOut++;
         fputs(acRec, fdOut);
      } else
      {
         if (bDebug)
            LogMsg("---> Load_TaxCodeMstr: drop record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
   } else
      iRet = 0;

   return iRet;
}

/***************************** MB_ParseTaxDelq *******************************
 *
 *
 *****************************************************************************/

int MB_ParseTaxDelq(char *pOutbuf, char *pInbuf)
{
   char     acTmp[256];
   int      iTmp;
   double   dTmp;
   TAXDELQ *pOutRec = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB_TRE_EXISTSPAYMENTHOLD)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));

   // APN
   strcpy(pOutRec->Apn, apTokens[MB_TRE_FEEPARCEL]);

   // BillNum
   strcpy(pOutRec->BillNum, apTokens[MB_TRE_FEEPARCEL]);

   // Default Number
   strcpy(pOutRec->Default_No, apTokens[MB_TRE_DEFAULTNUM]);

   // Default date
   if (dateConversion(apTokens[MB_TRE_DEFAULTDATE], acTmp, MM_DD_YYYY_1))
   {
      strcpy(pOutRec->Def_Date, acTmp);
      // Tax Year
      iTmp = atoin(acTmp, 4);
      sprintf(pOutRec->TaxYear, "%d", iTmp-1);
   } else
      LogMsg("***** Bad default date: %s, APN=%s", apTokens[MB_TRE_DEFAULTDATE], apTokens[MB_TRE_FEEPARCEL]);

   // Default amount
   dTmp = atof(apTokens[MB_TRE_DEFAULTAMT]) + 0.001;
   sprintf(pOutRec->Def_Amt, "%.2f", dTmp);

   // Red. date
   pOutRec->isDelq[0] = '0';
   if (*apTokens[MB_TRE_REDEEMEDDATE] >= '0' && dateConversion(apTokens[MB_TRE_REDEEMEDDATE], acTmp, MM_DD_YYYY_1))
   {
      strcpy(pOutRec->Red_Date, acTmp);

      // Red. amount
      dTmp = atof(apTokens[MB_TRE_FULLREDEMPTIONPAID]) + 0.001;
      sprintf(pOutRec->Red_Amt, "%.2f", dTmp);
      pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
   } else if (*apTokens[MB_TRE_PAYPLANNUM] > '0')
      pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
   else
   {
      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
      pOutRec->isDelq[0] = '1';
   }

   // Pen amt

   // Fee amt
   dTmp = atof(apTokens[MB_TRE_TOTALFEES]) + 0.001;
   sprintf(pOutRec->Fee_Amt, "%.2f", dTmp);

   // Update date
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   return 0;
}

/************************** MB_Load_TaxRedemption ****************************
 *
 * Load Redemption file to create import file for Tax_Delq table
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int MB_Load_TaxRedemption(bool bImport, int iSkipHdr)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0, lTmp;
   FILE     *fdOut, *fdIn;

   LogMsg0("Loading Tax Redemption");
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");
   sprintf(acInFile, sRedTmpl, myCounty.acCntyCode, myCounty.acCntyCode);

   // Open input file
   LogMsg("Open Tax Redemption file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax Redemption file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Open Delinquent file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Detail file: %s\n", acOutFile);
      return -4;
   }

   // Skip header record
   do {
      lTmp = ftell(fdIn);
      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
   } while (pTmp &&  memcmp(pTmp, "DEF", 3));
   
   if (fseek(fdIn, lTmp, SEEK_SET))
   {
      LogMsg("***** Error setting current position in sale file using fseek()");
      fclose(fdIn);
      return -2;
   }

   //int iTmp;
   //if (iSkipHdr >= 0)
   //   iTmp = iSkipHdr;
   //else
   //   iTmp = iHdrRows;
   //for (iRet = 0; iRet < iTmp; iRet++)
   //   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Replace null char in string (NEV)
      replNull(acRec);

      // Replace NULL
      replStrAll(acRec, "|NULL", "|");

#ifdef _DEBUG
      //if (strstr(acRec, "022510003000"))
      //   iTmp = 0;
#endif
      // Create new R01 record
      iRet = MB_ParseTaxDelq(acBuf, acRec);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acBuf);

         // Output record			
         lOut++;
         fputs(acRec, fdOut);
      } else
      {
         LogMsg("---> Load_TaxRed: drop record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/************************** MB_Load_TaxRedemption ****************************
 *
 * Load Redemption file to create import file for Tax_Delq table.  This version
 * links with CO3_Tax.csv to sum up total default amt from all unpaid tax bills.
 * To match up between TAX.CSV & REDEMPTION.CSV, both files have to be sorted on ASMT.
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int MB_Load_TaxDelq(bool bImport, int iSkipHdr)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acRedFile[_MAX_PATH], acTaxFile[_MAX_PATH];
   int      iRet;
   long     lOut=0, lCnt=0;

   FILE     *fdOut, *fdRed;

   LogMsg("Loading Redemption file ...");

   sprintf(acRedFile, sRedTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acTaxFile, sTaxTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acBuf, "%s\\%s\\%s_Redemption.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Resort Redemption file based on FeeParcel
   iRet = sortFile(acRedFile, acBuf, "S(#2,C,A,#1,C,A) DEL(124) OM(#2,C,LT,\"0\",OR,#2,C,GT,\"9\") F(TXT) ");
   if (iRet <= 0)
      return -1;

   // Open Redemption file
   LogMsg("Open Tax Redemption file %s", acBuf);
   fdRed = fopen(acBuf, "r");
   if (fdRed == NULL)
   {
      LogMsg("***** Error opening Tax Redemption file: %s\n", acBuf);
      return -2;
   }  

   // Sort tax file
   sprintf(acBuf, "%s\\%s\\%s_tax.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acTaxFile, acBuf, "S(#1,C,A,#2,C,A) DEL(124) OM(#1,C,LT,\"0\",OR,#1,C,GT,\"9\") F(TXT) ");
   // Open Tax file
   LogMsg("Open Tax file %s", acBuf);
   fdTax = fopen(acBuf, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acBuf);
      return -2;
   }  

   // Open Output file
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open Delinquent file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Detail file: %s\n", acOutFile);
      return -4;
   }

   // Init variables

   // Merge loop 
   while (!feof(fdRed))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRed);
      if (!pTmp)
         break;

      // Replace NULL
      replStrAll(acRec, "|NULL", "|");

#ifdef _DEBUG
      //if (strstr(acRec, "022510003000"))
      //   iTmp = 0;
#endif
      // Create new R01 record
      iRet = MB_ParseTaxDelq(acBuf, acRec);
      if (!iRet)
      {
         // Find total delq amount from tax file and update buffer
         if (fdTax)
            iRet = MB_FindTotalDelqAmt(acBuf);

         // Create delimited record
         Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acBuf);

         // Output record			
         lOut++;
         fputs(acRec, fdOut);
      } else
      {
         LogMsg("---> Load_TaxRed: drop record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRed)
      fclose(fdRed);
   if (fdTax)
      fclose(fdTax);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/********************************** MB_UpdateXfer *****************************
 *
 * Append transfers to existing file.
 * SHA
 *
 * Return 0 if successful, else error.
 *
 ******************************************************************************/

int MB_UpdateXfer(char *pCnty)
{
   int   iRet, iTmp, lCnt=0, lOut=0;
   char  acXferFile[_MAX_PATH], acOutFile[_MAX_PATH];
   char  acRollRec[MAX_RECSIZE], acOutRec[512], acTmp[512];
   char  sApn[16], sDocNum[16];
   char  *pTmp;

   LogMsg("Create xfer file for %s", pCnty);

   // Create transfer file
   sprintf(acXferFile, acXferTmpl, pCnty, pCnty, "txt");
   sprintf(acOutFile, acXferTmpl, pCnty, pCnty, "out");
   LogMsg("Open xfer file %s", acOutFile);
   fdXfer = fopen(acOutFile, "w");
   if (fdXfer == NULL)
   {
      LogMsg("***** Error opening Xfer file: %s\n", acOutFile);
      return -1;
   }
          
   // Open roll file      
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   while (!feof(fdRoll))
   {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Parse input
      if (cDelim == ',')
         iRet = ParseStringNQ(acRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iRet = ParseStringIQ(acRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iRet < MB_ROLL_PPMOBILHOME)
      {
         LogMsg("***** MB_UpdateXfer(): bad input record for APN=%s", apTokens[iApnFld]);
         continue;
      }

      // Ignore APN starts with 800-999 except 910 (MH)
      iTmp = atoin(apTokens[iApnFld], 3);
      if (!iTmp || (iTmp >= 800 && iTmp != 910  && iTmp != 920))
         continue;

      // Recorded Doc
      if (*apTokens[MB_ROLL_DOCNUM] > '0')
      {
         pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            strcpy(sApn, apTokens[iApnFld]);
            blankPadz(sApn, SIZ_APN_S); 
            strcpy(sDocNum, apTokens[MB_ROLL_DOCNUM]);
            blankPadz(sDocNum, SIZ_TRANSFER_DOC); 
            sprintf(acOutRec, "%s|%s|%s|%s\n", sApn, acTmp, sDocNum, acToday);
            fputs(acOutRec, fdXfer);
            lOut++;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdRoll);
   fclose(fdXfer);

   LogMsg("Total records processed:    %d", lCnt);
   LogMsg("              output:       %d", lOut);

   iRet = -99;
   if (lOut > 0)
   {
      // Sort output
      sprintf(acOutRec, acXferTmpl, pCnty, pCnty, "Srt");
      if (!_access(acXferFile, 0))
      {
         sprintf(acRollRec, "%s+%s", acXferFile, acOutFile); 
         iRet = sortFile(acRollRec, acOutRec, "S(#1,C,A,#2,C,A,#4,C,D,#3,C,D) DEL(124) OM(#2,C,LT,\"1700\",OR,#3,C,LT,\"0\") DUPO(#1,#2) ");
         if (iRet > 0)
         {                
            // Backup current file then rename output file to current
            sprintf(acOutFile, acXferTmpl, pCnty, pCnty, "bak");
            if (!_access(acOutFile, 0))
               DeleteFile(acOutFile);
            LogMsg("Save original xfer file to %s", acOutFile);
            rename(acXferFile, acOutFile);

            // Rename output file
            iRet = rename(acOutRec, acXferFile);
         }
      } else
         iRet = rename(acOutFile, acXferFile); 
   }

   return iRet;
}

/************************************** main *********************************
 *
 *
 *****************************************************************************/

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
   int   iRet=1, iTmp;
   char  acTmp[256], acLogPath[256], acVersion[64];
   char  acSubj[256], acBody[512], acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH];

   // initialize MFC and print and error on failure
   if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
   {
      cerr << _T("Fatal Error: MFC initialization failed") << endl;
      return -1;
   }

   iRet = LoadString(theApp.m_hInstance, APP_VERSION_INFO, acVersion, 64);
   printf("%s\n\n", acVersion);
   sprintf(acTmp, "%s Options: ", acVersion);
   for (iTmp = 1; iTmp < argc; iTmp++)
   {
      strcat(acTmp, argv[iTmp]);
      strcat(acTmp, " ");
   }

   if (argc < 2)
      Usage();

   // Get today date - yyyymmdd
   getCurDate(acToday);
   lToday = atoi(acToday);
   lToyear= atoin(acToday, 4);
   lRecCnt = 0;
   lLastRecDate = 0;
   iMaxLegal = 0;
   iErrorCnt = 0;

   // Parse command line
   ParseCmd(argc, argv);

   // Find INI file
   _getcwd(acIniFile, _MAX_PATH);
   strcat(acIniFile, "\\LoadMB.ini");

   // If not found INI file in working folder, check default location
   if (_access(acIniFile, 0))
   {
      strcpy(acIniFile, "C:\\Tools\\LoadMB.ini");
      if (_access(acIniFile, 0))
      {
         GetExePath(acIniFile, argv[0]);
         strcat(acIniFile, "\\LoadMB.ini");
      }
   }

   // Get log path
   iRet = GetIniString("System", "LogPath", "", acLogPath, _MAX_PATH, acIniFile);

   printf("Initializing %s\n", acIniFile);

   // open log file
   if (iLoadFlag & LOAD_LIEN)
   {
      if (lLienYear > 0)
         sprintf(acLogFile, "%s\\Load_%s_%d.ldr", acLogPath, myCounty.acCntyCode, lLienYear);
      else
         sprintf(acLogFile, "%s\\Load_%s_%d.ldr", acLogPath, myCounty.acCntyCode, lToyear);
      lLDRRecCount = 0;
   } else if (iLoadTax == TAX_LOADING && !iLoadFlag)
      sprintf(acLogFile, "%s\\LoadTax_%s_%s.log", acLogPath, myCounty.acCntyCode, acToday);
   else if (iLoadTax == TAX_UPDATING)
      sprintf(acLogFile, "%s\\UpdtTax_%s_%s.log", acLogPath, myCounty.acCntyCode, acToday);
   else
      sprintf(acLogFile, "%s\\Load_%s_%s.log", acLogPath, myCounty.acCntyCode, acToday);
   if (bOverwriteLogfile)
      open_log(acLogFile, "w");
   else
      open_log(acLogFile, "a+");

   if (myCounty.acCntyCode[0] < 'A')
   {
      LogMsgD("***** Missing county code.  Try again!!!\n");
      Usage();
   }

   // Open Usecode log
   if (!openUsecodeLog(myCounty.acCntyCode, acIniFile))
      LogMsg("***** %s", getLastErrorLog());

   LogMsg0("%s\n", acTmp);
   LogMsgD("Loading %s", myCounty.acCntyCode);

   iTmp = MergeInit(myCounty.acCntyCode);
   if (iTmp < 1)
   {
      LogMsg("***** Error initializing LoadMB");
      if (iTmp < 0)
         sprintf(acBody, "File system might have problem.  You may need to reboot production server.");
      else
         sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);

      sprintf(acSubj, "*** %s - Error initializing LoadMB.", myCounty.acCntyCode);
      mySendMail(acIniFile, acSubj, acBody, NULL);
      close_log();
      return 1;
   }

   // Extract prop8
   if (iLoadFlag & EXTR_PRP8)
   {
      if (strstr(acRollFile, ".csv"))
         iRet = MB_ExtrProp8(myCounty.acCntyCode, acRollFile, ',', MB_ROLL_TAXABILITY);
      else
         iRet = MB_ExtrProp8Ldr(myCounty.acCntyCode, NULL);
      goto Main_Exit;
   }

   // Fix cumsale
   if (iLoadFlag & FIX_CSDOC)
   {
      if (!_access(acCSalFile, 0))
      {
         iTmp = FixCumSale(acCSalFile, SALE_FLD_DOCNUM, false);
      } else
         LogMsg("***** Cannot fix cumsale file (%s). Invalid filename or file not exist", acCSalFile);
   }
   
   // Remove all sales and transfer from R01/S01 file
   if (bClearSales)
      iTmp = PQ_FixR01(myCounty.acCntyCode, PQ_REM_XFER);

   // Load lookup table
   iRet = LoadLUTable(acLookupTbl, "[Quality]", NULL, MAX_ATTR_ENTRIES);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
      close_log();
      return 1;
   }

   if (iLoadFlag & LOAD_LIEN)
      LogMsg("LDR %s processing.", myCounty.acYearAssd);
   else if (iLoadFlag & LOAD_UPDT)
      LogMsg("Regular update using %s base year.", myCounty.acYearAssd);
   else if (iLoadTax & (TAX_LOADING|TAX_UPDATING))
      LogMsg("Loading %d tax file.", lLienYear);

   // Reset automation flag to 'W' for working
   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT|LOAD_GRGR|EXTR_SALE|MERG_CSAL|MERG_GRGR))
   {
      sprintf(acTmp, "UPDATE County SET Status='W' WHERE (CountyCode='%s')", myCounty.acCntyCode);
      updateTable(acTmp);
   }
   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      sprintf(acTmp, "UPDATE Products SET State='W' WHERE (Prefix='S%s')", myCounty.acCntyCode);
      updateTable(acTmp);
   }
   if (iLoadFlag & (LOAD_ASSR|UPDT_ASSR))
   {
      sprintf(acTmp, "UPDATE Products SET State='W' WHERE (Prefix='%s')", myCounty.acCntyCode);
      updateTable(acTmp);
   }

   // Retrieve last record count
   iTmp = getCountyInfo() ;
   if (iTmp < 0)
   {
      close_log();
      return 1;
   }

   if (myCounty.iMaxUpdate > 0)
      iMaxChgAllowed = myCounty.iMaxUpdate;

   // Open export file
   if (bExpSql && iLoadTax == TAX_LOADING)
   {
      iRet = OpenTaxExpSql(myCounty.acCntyCode, "TaxUpd", "w");
      if (iRet)
         return iRet;
   } else
      bExpSql = false;

   LogMsg("Start processing...");
   if (!_memicmp(myCounty.acCntyCode, "AMA", 3))
      iRet = loadAma(1);
   else if (!_memicmp(myCounty.acCntyCode, "ALP", 3))
      iRet = loadAlp(1);
   else if (!_memicmp(myCounty.acCntyCode, "BUT", 3))
      iRet = loadBut(1);
   else if (!_memicmp(myCounty.acCntyCode, "CAL", 3))
      iRet = loadCal(1);
   else if (!_memicmp(myCounty.acCntyCode, "COL", 3))
      iRet = loadCol(1);
   else if (!_memicmp(myCounty.acCntyCode, "DNX", 3))
      iRet = loadDnx(1);
   else if (!_memicmp(myCounty.acCntyCode, "EDX", 3))
      iRet = loadEdx(1);
   else if (!_memicmp(myCounty.acCntyCode, "GLE", 3))
      iRet = loadGle(1);
   else if (!_memicmp(myCounty.acCntyCode, "HUM", 3))
      iRet = loadHum(1);
   else if (!_memicmp(myCounty.acCntyCode, "IMP", 3))
      iRet = loadImp(1);
   else if (!_memicmp(myCounty.acCntyCode, "INY", 3))
      iRet = loadIny(1);
   else if (!_memicmp(myCounty.acCntyCode, "KIN", 3))
      iRet = loadKin(1);
   else if (!_memicmp(myCounty.acCntyCode, "LAK", 3))
      iRet = loadLak(1);
   else if (!_memicmp(myCounty.acCntyCode, "LAS", 3))
      iRet = loadLas(1);
   else if (!_memicmp(myCounty.acCntyCode, "MAD", 3))
      iRet = loadMad(1);
   else if (!_memicmp(myCounty.acCntyCode, "MEN", 3))
      iRet = loadMen(1);
   else if (!_memicmp(myCounty.acCntyCode, "MER", 3))
      iRet = loadMer(1);
   else if (!_memicmp(myCounty.acCntyCode, "MNO", 3))
      iRet = loadMno(1);
   else if (!_memicmp(myCounty.acCntyCode, "MOD", 3))
      iRet = loadMod(1);
   else if (!_memicmp(myCounty.acCntyCode, "MON", 3))
      iRet = loadMon(1);
   else if (!_memicmp(myCounty.acCntyCode, "MPA", 3))
      iRet = loadMpa(1);
   else if (!_memicmp(myCounty.acCntyCode, "NAP", 3))
      iRet = loadNap(1);
   else if (!_memicmp(myCounty.acCntyCode, "NEV", 3))
      iRet = loadNev(1);
   else if (!_memicmp(myCounty.acCntyCode, "PLA", 3))
      iRet = loadPla(1);
   else if (!_memicmp(myCounty.acCntyCode, "PLU", 3))
      iRet = loadPlu(1);
   else if (!_memicmp(myCounty.acCntyCode, "SBT", 3))
      iRet = loadSbt(1);
   else if (!_memicmp(myCounty.acCntyCode, "SHA", 3))
      iRet = loadSha(1);
   else if (!_memicmp(myCounty.acCntyCode, "SIE", 3))
      iRet = loadSie(1);
   else if (!_memicmp(myCounty.acCntyCode, "SIS", 3))
      iRet = loadSis(1);
   else if (!_memicmp(myCounty.acCntyCode, "SJX", 3))
      iRet = loadSjx(1);
   else if (!_memicmp(myCounty.acCntyCode, "SON", 3))
      iRet = loadSon(1);
   else if (!_memicmp(myCounty.acCntyCode, "STA", 3))
      iRet = loadSta(1);
   else if (!_memicmp(myCounty.acCntyCode, "SUT", 3))
      iRet = loadSut(1);
   else if (!_memicmp(myCounty.acCntyCode, "TEH", 3))
      iRet = loadTeh(1);
   else if (!_memicmp(myCounty.acCntyCode, "TRI", 3))
      iRet = loadTri(1);
   else if (!_memicmp(myCounty.acCntyCode, "TUL", 3))
      iRet = loadTul(1);
   else if (!_memicmp(myCounty.acCntyCode, "TUO", 3))
      iRet = loadTuo(1);
   else if (!_memicmp(myCounty.acCntyCode, "YOL", 3))
      iRet = loadYol(1);
   else if (!_memicmp(myCounty.acCntyCode, "YUB", 3))
      iRet = loadYub(1);
   else
   {
      iRet = -1;
      LogMsgD("***** Invalid county code.  This program only works with Megabyte counties *****");
   }

   if (bExpSql)
   {
      char sFile1[_MAX_PATH], sFile2[_MAX_PATH], sRootDir1[64], sRootDir2[64];
      
      CloseTaxExpSql();

      sprintf(sFile1, sTaxSqlTmpl, myCounty.acCntyCode,  "TaxUpd");
      iRet = getFileSize(sFile1);
      if (iRet < 500)
      {
         // Remove empty file and clear tax loading flag
         DeleteFile(sFile1);
         iLoadTax = 0;
         iRet = 0;
      } else
      {
         iRet = GetIniString("Data", "WRoot", "", sRootDir1, 256, acIniFile);
         iRet = GetIniString("Data", "LRoot", "", sRootDir2, 256, acIniFile);
         if (iRet > 1)
         {
            sprintf(sFile2, sTaxSqlTmpl, myCounty.acCntyCode,  "TaxUpdL");
            iRet = ConvertTaxSqlFile(sFile1, sFile2, sRootDir1, sRootDir2, "L");
         }

         iRet = GetIniString("Data", "BRoot", "", sRootDir2, 256, acIniFile);
         if (iRet > 0)
         {
            sprintf(sFile2, sTaxSqlTmpl, myCounty.acCntyCode,  "TaxUpdB");
            iRet = ConvertTaxSqlFile(sFile1, sFile2, sRootDir1, sRootDir2, "B");
         }
      }
   }

   LogMsg0("\nProcessing complete.  Set status and validating result.");

   // Set state='F' if fail, 'P' for data processed and ready for product build 
   if (iRet)
   {
      if (iLoadFlag & (LOAD_ASSR|UPDT_ASSR))
      {
         sprintf(acTmp, "UPDATE Products SET State='F' WHERE (Prefix='%s')", myCounty.acCntyCode);
         updateTable(acTmp);
      }
      if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
      {
         sprintf(acTmp, "UPDATE Products SET State='F' WHERE (Prefix='S%s')", myCounty.acCntyCode);
         updateTable(acTmp);
      }

      if (bSendMail)
      {
         if (!bGrGrAvail)
         {
            if (iLoadFlag & LOAD_DAILY)
               sprintf(acSubj, "*** LoadMB [%s] - Daily file is not available.", myCounty.acCntyCode);
            else
               sprintf(acSubj, "*** LoadMB [%s] - No new GRGR file.", myCounty.acCntyCode);
            sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);

            // Reset county status
            sprintf(acTmp, "UPDATE County SET Status='R' WHERE (CountyCode='%s')", myCounty.acCntyCode);
            updateTable(acTmp);
         } else 
         {
            sprintf(acSubj, "*** LoadMB - Error loading %s", myCounty.acCntyCode);
            sprintf(acBody, "%s\nSee %s for more info", getLastErrorLog(), acLogFile);
         }
         mySendMail(acIniFile, acSubj, acBody, NULL);
         bSendMail = false;
      }

      // Stop all other tasks
      iLoadFlag = 0;
   } else if (iLoadTax == TAX_LOADING || iLoadTax == TAX_UPDATING)
   {
      GetIniString("Mail", "MailChgTo", "", acTmp, 256, acIniFile);
      if (iLoadTax == TAX_LOADING)
         sprintf(acSubj, "Loading county tax for %s", myCounty.acCntyCode);
      else
         sprintf(acSubj, "Updating county tax for %s", myCounty.acCntyCode);
      sprintf(acBody, "Tax has been loaded.");
      if (bSendMail)
         mySendMail(acIniFile, acSubj, acBody, acTmp);
   } else
   {
      // Merge zoning
      if (iLoadFlag & MERG_ZONE)                      // -Mz
      {
         iRet = MergeZoning(myCounty.acCntyCode, iSkip);
      } else if (bClearPQZ)
      {
         iRet = PQ_RemovePQZoning(myCounty.acCntyCode, iSkip);
      }

      // Merge final value
      if (lOptMisc & M_OPT_MERGVAL)                   // -Mf
         iRet = PQ_MergeValueExt(myCounty.acCntyCode, GRP_MB, iSkip);

      // Update acreage using GIS data
      if (iLoadFlag & MERG_GISA)                      // -Mr
      {
         GetPrivateProfileString(myCounty.acCntyCode, "OverwriteSqft", "N", acTmp, 10, acIniFile);
         if (acTmp[0] == 'Y')
            iRet = PQ_MergeLotArea(myCounty.acCntyCode, true);
         else
            iRet = PQ_MergeLotArea(myCounty.acCntyCode, false);
      }

      if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT|MERG_GRGR|LOAD_DAILY|UPDT_XSAL))
      {
         // Check for bad character
         iTmp = PQ_ChkBadR01(myCounty.acCntyCode, acRawTmpl, iRecLen, ' ');

         // Copy file for assessor
         sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
         GetIniString(myCounty.acCntyCode, "AsrFile", "", acTmpFile, _MAX_PATH, acIniFile);
         if (acTmpFile[0] > ' ' && !_access(acOutFile, 0))
         {
            LogMsg("Copying file %s ==> %s", acOutFile, acTmpFile);
            iTmp = CopyFile(acOutFile, acTmpFile, false);
            if (!iTmp)
            {
               LogMsg("***** Fail copying file %s ==> %s", acOutFile, acTmpFile);
               iLoadFlag = 0;
            } else
            {
               iLoadFlag |= UPDT_ASSR;
               lAssrRecCnt = lRecCnt;
            }
         }

         // Copy roll for assessor
         GetIniString(myCounty.acCntyCode, "CopyRoll", "", acOutFile, _MAX_PATH, acIniFile);
         if (acOutFile[0] > ' ')
         {
            LogMsg("Copying file %s ==> %s", acRollFile, acOutFile);
            iTmp = CopyFile(acRollFile, acOutFile, false);
            if (!iTmp)
            {
               LogMsg("***** Fail copying file %s ==> %s", acRollFile, acOutFile);
               iLoadFlag = 0;
            } else
            {
               iLoadFlag |= UPDT_ASSR;
               lAssrRecCnt = lRecCnt;
            }
         }

         // Check number of output records when update roll.  Ignore it when LDR.
         if ((lRecCnt > (myCounty.iLastRecCnt-((myCounty.iLastRecCnt/100)*5)))
            || (iLoadFlag & LOAD_LIEN) )
         {
            // Generate update file
            int iRecChg=-1;
            if ((!bDontUpd || bCreateUpd) && (iLoadFlag & (LOAD_UPDT|LOAD_DAILY)))
            {
               // Remove old UPD file
               if (bCreateUpd)
                  updFileRemove(myCounty.acCntyCode, acRawTmpl);
               GetPrivateProfileString(myCounty.acCntyCode, "CreateAPNFile", "N", acTmp, _MAX_PATH, acIniFile);
               if (acTmp[0] == 'Y')
                  iRecChg = chkS01R01(myCounty.acCntyCode, acRawTmpl, iRecLen, iApnLen, bCreateUpd, true);
               else
                  iRecChg = chkS01R01(myCounty.acCntyCode, acRawTmpl, iRecLen, iApnLen, bCreateUpd, false);

               if (iRecChg > 0)
               {
                  // Store number of recs changed to County table
                  sprintf(acTmp, "UPDATE County SET LastRecsChg=%d WHERE (CountyCode='%s')", iRecChg, myCounty.acCntyCode);
                  updateTable(acTmp);

                  // If number of changed records greater than allowed, we have to import manually
                  if (iRecChg > iMaxChgAllowed)
                  {
                     // Email production
                     LogMsg("*** WARNING: Number of changed records is too big for %s (%d) ***", myCounty.acCntyCode, iRecChg);
                     LogMsg("***          Please run CDDEXTR -Q -TWEBIMPORT -C%s ***", myCounty.acCntyCode);

                     GetIniString("Mail", "MailChgTo", "", acTmp, 256, acIniFile);
                     //LogMsg("Send email to %s", acTmp);

                     if (bSendMail)
                     {
                        sprintf(acSubj, "LoadMB [%s] - %d recs changed.  Use CddExtr to update SQL server.", myCounty.acCntyCode, iRecChg);
                        sprintf(acBody, "Msg from %s.\nSee log file Load_%s.log for more info.", acVersion, myCounty.acCntyCode);
                        mySendMail(acIniFile, acSubj, acBody, acTmp);
                     }
                  }
               } 
            }

            // Only trigger next production step if there is new change
            if (iRecChg)
            {
               // Update Products table
               sprintf(acTmp, "UPDATE Products SET LastRecsCount=%d,State='P' WHERE (Prefix='S%s')", lRecCnt, myCounty.acCntyCode);
               updateTable(acTmp);

               // Update CtyProfiles
               sprintf(acTmp, "UPDATE ctyProfiles SET LastBldDate=%s,LastRecCount=%d,LastRecDate=%d,State='P' WHERE (CountyCode='%s')", acToday, lRecCnt, lLastRecDate, myCounty.acCntyCode);
               updateTable(acTmp);
            }

            // Update county table
            char sGrGrDt[32], sFileDt[32], sTaxFileDt[32];

            sGrGrDt[0] = 0;
            sFileDt[0] = 0;
            sTaxFileDt[0] = 0;

            if (lLastFileDate > 0)
               sprintf(sFileDt, ",LastFileDate=%d", lLastFileDate);
            if (lLastGrGrDate > 0)
               sprintf(sGrGrDt, ",LastGrGrDate=%d", lLastGrGrDate);

            // LastFileDate is set by ChkCnty.  But we provide option to do it here for manual run
            if (iLoadFlag & LOAD_LIEN)
               sprintf(acTmp, "UPDATE County SET LastBldDate=%s,LastRecCount=%d,LDRRecCount=%d,LastRecDate=%d %s %s %s WHERE (CountyCode='%s')", 
                  acToday, lRecCnt, lLDRRecCount, lLastRecDate, sGrGrDt, sFileDt, sTaxFileDt, myCounty.acCntyCode);
            else if (bSetLastFileDate)
               sprintf(acTmp, "UPDATE County SET LastBldDate=%s,LastRecCount=%d,LastRecDate=%d %s %s %s WHERE (CountyCode='%s')", 
                  acToday, lRecCnt, lLastRecDate, sGrGrDt, sFileDt, sTaxFileDt, myCounty.acCntyCode);
            else
               sprintf(acTmp, "UPDATE County SET LastBldDate=%s,LastRecCount=%d,LastRecDate=%d WHERE (CountyCode='%s')", 
                  acToday, lRecCnt, lLastRecDate, myCounty.acCntyCode);
            updateTable(acTmp);

            // Reset county status
            sprintf(acTmp, "UPDATE County SET Status='R' WHERE (CountyCode='%s')", myCounty.acCntyCode);
            updateTable(acTmp);
         } else if (lRecCnt > 0)
         {
            sprintf(acSubj, "*** LoadMB WARNING: Number of records is too small for %s new=%d old=%d ***", myCounty.acCntyCode, lRecCnt, myCounty.iLastRecCnt);
            LogMsg(acSubj);
            GetIniString("Mail", "Support", "Sony 714-247-9732", acTmp, 64, acIniFile);
            sprintf(acBody, "Msg from %s\nInput file may be corrupted.  Please check FTP folder to verify file size or call %s", acVersion, acTmp);

            // Send mail
            if (bSendMail)
            {
               bSendMail = false;
               mySendMail(acIniFile, acSubj, acBody, NULL);
            }
            iRet = 1;   // trigger not to create flag file
         }            

         if (iMaxLegal > 0)
            LogMsg("Max Legal Length:     %d\n", iMaxLegal);
      } else if (iLoadFlag & (LOAD_GRGR|EXTR_SALE))
      {
         // Reset county status
         sprintf(acTmp, "UPDATE County SET Status='R' WHERE (CountyCode='%s')", myCounty.acCntyCode);
         updateTable(acTmp);
      }

      if (iLoadTax && lLastTaxFileDate > 0)
      {
         if (bTaxImport)
            sprintf(acTmp, "UPDATE County SET LastTaxImpDate=%s,LastTaxFileDate=%d WHERE (CountyCode='%s')", acToday, lLastTaxFileDate, myCounty.acCntyCode);
         else
            sprintf(acTmp, "UPDATE County SET LastTaxFileDate=%d WHERE (CountyCode='%s')", lLastTaxFileDate, myCounty.acCntyCode);
         updateTable(acTmp);
      }

      if (iLoadFlag & (LOAD_ASSR|UPDT_ASSR))
      {
         if (lAssrRecCnt > 0)
         {
            sprintf(acTmp, "UPDATE Products SET LastRecsCount=%d,State='P' WHERE (Prefix='%s')", lAssrRecCnt, myCounty.acCntyCode);
            updateTable(acTmp);
         }
      }

      if (!bGrGrAvail)
      {
         // send email
         if (bSendMail)
         {
            if (iLoadFlag & LOAD_DAILY)
               sprintf(acSubj, "*** LoadMB [%s] - Daily file is not available.", myCounty.acCntyCode);
            else
               sprintf(acSubj, "*** LoadMB [%s] - No new GRGR file.", myCounty.acCntyCode);
            sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
            mySendMail(acIniFile, acSubj, acBody, NULL);
            bSendMail = false;
         }
      }
   }

   // Reset county status
   //if (!bLoadTax)
   //{
   //   sprintf(acTmp, "UPDATE County SET Status='R' WHERE (CountyCode='%s')", myCounty.acCntyCode);
   //   updateTable(acTmp);
   //}

   // Create flag file
   if (!iRet && CreateFlgFile(myCounty.acCntyCode, acIniFile))
      LogMsg("***** Error creating flag file *****");

   // Import sale file
   if (iLoadFlag & EXTR_ISAL)
   {
      char sDbName[64];

      if (iLoadFlag & LOAD_LIEN)
         sprintf(sDbName, "LDR%d", lLienYear);
      else
         sprintf(sDbName, "UPD%d", lLienYear);

      GetIniString("Data", "SqlSaleFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acOutFile, acTmp, sDbName, myCounty.acCntyCode);
      iTmp = createSaleImport(myCounty.acCntyCode, acCSalFile, acOutFile, TYPE_SCSAL_REC, false);
      if (iTmp > 0 && bSaleImport)
         iTmp = doSaleImportEx(myCounty.acCntyCode, sDbName, 1);
      else
         iTmp = 0;

      // send email
      if (iTmp && bSendMail)
      {
         sprintf(acSubj, "*** LoadMB [%s] - Error importing sale file.", myCounty.acCntyCode);
         sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
         mySendMail(acIniFile, acSubj, acBody, NULL);
         bSendMail = false;
      }
   }

   // Import GrGr file
   if (iLoadFlag & EXTR_IGRGR)
   {
      char sDbName[64];

      if (iLoadFlag & LOAD_LIEN)
         sprintf(sDbName, "LDR%d", lLienYear);
      else
         sprintf(sDbName, "UPD%d", lLienYear);

      GetIniString("Data", "SqlGrgrFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acOutFile, acTmp, sDbName, myCounty.acCntyCode);

      sprintf(acCSalFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      if (strstr(myCounty.acCntyCode, "BUT"))
         iTmp = createSaleImport(But_MakeDocLink, myCounty.acCntyCode, acCSalFile, acOutFile, TYPE_GRGR_DEF, false);
      else if (strstr(myCounty.acCntyCode, "MNO"))
         iTmp = createSaleImport(Mno_MakeDocLink, myCounty.acCntyCode, acCSalFile, acOutFile, TYPE_GRGR_DEF, false);
      else
         iTmp = createSaleImport(myCounty.acCntyCode, acCSalFile, acOutFile, TYPE_GRGR_DOC, false);

      if (iTmp > 0 && bGrgrImport)
         iTmp = doSaleImportEx(myCounty.acCntyCode, sDbName, 2);
      else
         iTmp = 0;

      // send email
      if (iTmp && bSendMail)
      {
         sprintf(acSubj, "*** LoadMB [%s] - Error importing Grgr file.", myCounty.acCntyCode);
         sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
         mySendMail(acIniFile, acSubj, acBody, NULL);
         bSendMail = false;
      }
   }

   // Import Value file
   if (iLoadFlag & EXTR_IVAL)
   {
      LogMsg0("*** Please use UV<n>.CMD command to update value table. ***");
      //iTmp = doValueImport(myCounty.acCntyCode, acValueFile);

      //// send email
      //if (iTmp && bSendMail)
      //{
      //   sprintf(acSubj, "*** LoadMB [%s] - Error importing Value file.", myCounty.acCntyCode);
      //   sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
      //   GetIniString("Mail", "MailChgTo", "", acTmp, 256, acIniFile);
      //   mySendMail(acIniFile, acSubj, acBody, acTmp);
      //   bSendMail = false;
      //}
   }

   // Check for other error during processing
   if (iErrorCnt > 0 && bSendMail)
   {
      sprintf(acSubj, "*** LoadMB [%s] - Data error found", myCounty.acCntyCode);
      sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
      mySendMail(acIniFile, acSubj, acBody, NULL);
   }

Main_Exit:
   // Remove temp files
   if (!iRet && bRemTmpFile)
      iRet = RemoveTempFiles(myCounty.acCntyCode, acTmpPath);

   // Clean up memory allocation
   if (iNumUseCodes > 0)
   {
      if (pUseTable)
         delete pUseTable;
      else if (pIUseTable)
         delete pIUseTable;
   }

   if (iNumUseCodes1 > 0 && pUseTable1)
      delete pUseTable1;

   // Close log
   closeUsecodeLog();
   close_log();

   return iRet;
}
