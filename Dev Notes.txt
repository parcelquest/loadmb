
AMA Notes
1) Calculate sale price from transfer tax.  Confirmed sale price is not reliable.
2) Talk to Dave about partial sale.
3) Create SaleType table for Transfer Type
4) Fernando put Quality in wrong place (BldgClass instead). (001030007000)

TransferType
      12116
      19962
06    2
CS    1
DI    2
FV    8242  Full
G0    1
GD    472
LL    66    partial
QD    7
TD    34
TR    1
WL    5     Incomplete
WP    1780  Multiple

4) Create translation table for DocCode -> DocType
DocCode
      105
      20388
01    10364
02    756
03    614
04    2581
05    1678
06    3283
07    633
08    82
09    328
10    21
11    3
12    195
13    9
14    1319
40    101
70    3
95    14
97    27
98    26
99    2
GD    155
TD    4

QualityClass
      4257
.66   1
0     1
1     1
1.0   2
1.5   1
1935  1
1976  1
1978  2
1979  1
1980  1
1981  1
1991  1
1992  1
2     12
2.0   2
2A    1
3     25
3.0   1
3.5   15
4     108
4.0   17
4.5   35
5     296
5.0   57
5.5   425
5.5.  1
56.5  1
5A    2
5B    1
6     784
6.0   299
6.0MH 3
6.5   1229
6.5 MH   1
6.5B  1
6.5MH 6
67.5  1
6A    5
6B    2
7     450
7.0   364
7.0 MH   2
7.0B  1
7.0MH 12
7.5   283
7.5 MH   2
7.5C  1
7.5MH 11
7A 2
7C 3
8  49
8 (MH)   1
8 MH  4
8.0   17
8.0MH 2
8.5   7
8A    1
9     1
9 MH  1
9.0   3
9.0MH 1
9.5   1
A4B   1
Avg   23
Avg.  1
Basic 2
C4 2
C4.5  2
C4.5A 3
C4A   2
C4B   2
C5 5
C5.5  1
C5.5A 1
C5A   4
C5B   1
C5C   1
C6    7
C6.0A 1
C6.5  3
C6.5A 1
C6.5B 4
C6.5D 1
C6A   3
C6B   7
C6C   2
C7    1
C7.5  1
C7.5A 2
C7.5C 1
C7A   2
C7B   2
C7C   1
C8 2
C8A   1
C8D   2
D     1
D 6   1
D1 1
D1.5  1
D1.5B 2
D1A   3
D2    9
D2.0B 3
D2A   11
D2B   12
D2D   2
D3    30
D3-5A 1
D3.0  1
D3.0A 1
D3.0B 2
D3.5  8
D3.5A 10
D3.5B 14
D3.5C 2
D35A  1
D3A   11
D3B   36
D3C   6
D4    135
D4.0  5
D4.0A 3
D4.0B 3
D4.0C 1
D4.5  86
D4.5A 28
D4.5B 18
D4.5C 5
D4.6  1
D4A   39
D4B   34
D4C   8
D4D   1
D5    233
D5.0  8
D5.0A 5
D5.0B 5
D5.0C 2
D5.5  281
D5.5A 85
D5.5AC   1
D5.5B 63
D5.5C 25
D5.5D 2
D5.5H 1
D5.8B 1
D58   1
D5A   99
D5B   69
D5C   23
D5D   2
D5OB  1
D6    564
D6,5A 1
D6-B  1
D6.0  21
D6.0A 5
D6.0B 9
D6.0B1   1
D6.0C 5
D6.2  1
D6.5  636
D6.5A 90
D6.5B 79
D6.5C 31
D6.5D 3
D6.6A 1
D6.6C 1
D60   2
D6A   173
D6AB  1
D6B   100
D6C   35
D6D   5
D7    381
D7.0  21
D7.0A 1
D7.0B 4
D7.0C 3
D7.0D 1
D7.5  149
D7.5A 12
D7.5B 10
D7.5C 5
D7.5D 3
D7.8B 1
D705  3
D70D  1
D7A   37
D7AB  1
D7B   47
D7C   25
D7D   5
D8    33
D8.0  4
D8.5  6
D8A   2
D8B   3
D8C   1
D8D   2
D9    4
DE6   1
Fair  3
Good  11
S     1

Heating  Expr1
   2273
   7338
01 3249
02 171
03 200
04 471
05 9
06 8
08 73
09 36
1  16
2  7
4  35
5  1

Cooling  Expr1
   2578
   7968
01 2998
02 157
03 11
04 63
05 26
06 83
07 2
99 1

CoolingSource  Expr1
   13885
   2

HeatingSource  Expr1
   13884
   3

LandUseCategory   Expr1
1  301
11 1
2  509
3  6
4  13070
