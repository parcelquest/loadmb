/**************************************************************************
 *
 * Notes:
 *    - 2009 LDR is in XLSX format.  We need to convert to CSV before processing.
 *    - 2010 LDR - Ask Nick to convert to CSV or use Xls2Csv program to convert XLS file to CSV.
 *                 Xls2Csv <infile> <outfile> <AGENCYCDCURRSEC_TR601> 12
 *    - 2011 LDR - file is in TAB delimited format.
 *
 * Revision
 * 05/14/2009 8.7.5    MNO is converted to MB system.  Copy from NAP and customize for MNO
 * 05/21/2009 8.7.5.2  Fix double suffixes in situs addr.
 * 07/08/2009 9.1.1    Modify Mno_Load_LDR(), Mno_MergeSitus(), Mno_MergeLien() & Mno_ExtrTR601Rec() 
 *                     to support MNO LDR for the first time in CSV format.
 * 08/03/2009 9.1.4.1  Fix potential bug in Mno_MergeRoll() that may overide SALE1_DT.
 * 07/26/2010 10.2.0   Add LDRRecCount.
 * 07/27/2010 10.2.1   Reformat TRA to standard 6-digit number.Rename Mno_LoadRoll() to Mno_Load_Roll().
 * 11/01/2010 10.3.6   Modify Mno_MergeRoll() and Mno_MergeLien() to support new USECODE
 *                     table. Update asSfxTbl[] to add more suffixes.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 02/09/2011 10.5.4   Modify Mno_FormatApn() to support some weird old APN format, 
 *                     Mno_ExtrSaleMatched() to remove SalePrice check since ther is no saleprice
 *                     in the new MDB format.  Create Mno_ConvertGrGr() to convert MDB GrGr. Modify
 *                     loadMno() to change load GrGr logic.
 * 06/08/2011 10.9.1   Exclude HomeSite from OtherVal.  Replace Mno_MergeExe() with MB_MergeExe()
 *                     and Mno_MergeTax() with MB_MergeTax(). Replace MB_CreateSale() with MB_CreateSCSale().
 *                     Merge previous with current sale file and create Mno_Sale.sls.
 * 07/20/2011 11.2.1   Add S_HSENO. One problem is StrNum in MNO can be as long as 20 chars.
 *                     We may have to increase this length in the future to cover everything.
 *                     Scan input LDR record for null and use LdrSep defined in INI file to parse record.
 * 07/21/2011 11.2.1.1 Use Mno_Situs.csv to populate LDR. Fix STDUSE bug in MergeLien() 
 *                     that assigned 500 to STDUSE when county use is blank. Remove single quote
 *                     in street name in Mno_MergeSitus(). Add '#' to Unit#.
 * 07/25/2011 11.2.2   Add more suffixes to asSfxTbl[].
 * 12/02/2012 12.5.3   Change params when calling MergeGrGrFile() to update sale price if avail.
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 05/22/2013 12.9.0   Move all global constant and variables to MergeMno.h. Modify Mno_FormatApn()
 *                     to get more GrGr APN matched. Fix bad char in situs StrName. Modify
 *                     Mno_MergeRoll() to ignore "Administrative Parcel". Remove sale update
 *                     from Mno_Load_Roll() and use ApplyCumSale(). Replace Mno_ExtrSaleMatched()
 *                     with GrGr_ExtrSaleMatched(). Standardize DocNum to match sale file with GrGr
 *                     as yyyyR1234567.  Use MergeGrGrExpFile() to merge GRGR to R01.
 * 05/29/2013 12.9.2   Merge sale & Grgr regardless of new file avail or not.
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 02/23/2014 13.20.4  Check for MERG_CSAL or MERG_GRGR before update R01.
 * 03/28/2014 13.22.1  Fix bug in Mno_FormatApn() and modify Mno_ConvertGrGr() not to append new data 
 *                     to Mon_Grgr.sls since we receive complete file every time.
 *                     Add option to ApplyCumSale() to clear GRGR to remove old GRGR data.
 * 07/17/2014 14.0.4   Modify Mno_MergeSitus() to move suffix from StrName to its own field
 *                     and to add S_Zip. Add Mno_MergeLien1(), Mno_Load_LDR1(), 
 *                     Mno_ExtrLien() and Mno_ExtrLienRec() to handle new LDR data format.
 * 12/04/2014 14.10.0  Add option to process GrGr on specific daily file.
 * 12/10/2014 14.11.0  Remove unused function Mno_ExtrSaleMatched().
 *                     Add Mno_MakeDocLink(), this is a place holder for future implementation.
 * 12/21/2014 14.11.1  Modify LoadMno() to fix GrGr file extension since county may use different
 *                     file name for the zip file.
 * 07/31/2015 15.2.0   Number of LDR fields reduces to 25. Add Mno_MergeLien2() and Mno_ExtrLienRec2() 
 *                     to support new layout. Fix Mno_MergeMAdr() to remove old DBA before putting new one in.
 * 11/03/2015 15.5.0   Add Mno_ConvStdChar() and Mno_MergeStdChar() (from HUM) to support new CHAR format.
 * 11/17/2015 15.6.3   Fix PatioSqft bug in Mno_MergeStdChar().
 * 12/03/2015 15.8.0   Cleanup unused code.
 * 01/04/2016 15.9.0   Add option to load TC file by calling Load_TC()
 * 02/14/2016 15.12.0  Modify Mno_MergeMAdr() to fix overwrite bug.
 *                     Modify Mno_MergeRoll() to make LEGAL upper case, use CareOf for Owner1
 *                     if Owner1 is not available, use Address1-4 is M_Addr is blank.
 * 04/04/2016 15.14.2  Use findXlatCodeA() on asHeating[].
 * 08/29/2016 16.4.2.1 Modify Mno_ConvStdChar to add all GarSqft from different ParkingType into 
 *                     myCharRec.GarSqft.  Add Mno_Load_LDR3() to process new LDR format.
 * 08/09/2017 17.2.5   Add Mno_MergeLien5() to support 2017 LDR layout. MNO has dropped GROWINGVALUE.
 *                     Also, in the new layout, all values are format as dollar amount.
 * 03/27/2018 17.9.0   Add -Ut option to update tax.
 * 04/10/2018 17.10.1  Modify loadMno(), Mno_LoadGrGrCsv() & Mno_LoadGrGr() to handle new GRGR file.   
 *                     We now use TransferHistory.csv instead of MonoOfficialRecords. Add Mno_CreateGrGrRec().
 *            17.10.2  Modify Mno_MergeRoll() to reformat DocNum to yyyyR9999999 if DocNum[4]='R'.
 * 08/07/2018 18.2.4   Modify Mno_MergeOwner() remove extra double quote. Add Mno_MergeLien8(). 
 *                     Modify Mno_Load_LDR3() to support new 2018 LDR layout. 
 * 10/31/2018 18.7.1   Remove extra 'X' in QualityClass in Mno_ConvStdChar()
 *                     Fix known bad char in MergeRoll().
 * 06/20/2019 18.12.0  Modify Mno_MergeRoll() to fix known bad char in legal.
 * 07/20/2019 19.1.0   Remove duplicate code in MergeLien3().
 * 07/30/2019 19.1.1   Add Mno_ExtrLienRec11() & Mno_MergeLien11(). Modify Mno_ExtrLien() & Mno_Load_LDR() 
 *                     to support new layout 2019.
 * 08/09/2019 19.1.3   Modify Mno_MergeSitus() to remove space in front of UNITNO.
 * 08/27/2019 19.2.0   Fix crashing bug in Mno_MergeSitus().
 * 04/27/2020 19.9.0   Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 08/09/2020 20.2.6   Add Mno_ExtrLienRec13() & Mno_MergeLien13() to support new LDR layout.
 * 11/01/2020 20.4.2   Modify Mno_MergeRoll() to populate default PQZoning.
 * 08/04/2021 21.1.5   Modify Mno_MergeStdChar() to populate LOTSQFT only if it's empty.  Trim left
 *                     blank on addr1 before processing in Mno_MergeMAdr().  Add Mno_ExtrLienRec16()
 *                     & Mno_MergeLien16() to support new LDR layout.  Modify Mno_Load_LDR() for 2021 LDR.
 * 08/10/2022 22.1.4   Modify sort command in Mno_Load_LDR() for 2022.  
 *                     Add Mno_ExtrLienRec18() & Mno_MergeLien18() to support new LDR layout.
 * 10/17/2022 22.2.6   Remove LEGAL and stop updating it until further notice. Fix Owner name starts with space.
 * 08/09/2023 23.1.7   Add Mno_MergeOthers() to merge USECODE, NBHCODE & ZONING from roll file to LDR.
 *                     Add Mno_ExtrLienRec19() & Mno_MergeLien19() to support new LDR record.
 *                     Modify Mno_Load_LDR() to check for duplicate record and use roll file for missing fields.
 * 08/01/2024 24.0.4   LDR layout changed.  Add Mno_ExtrLienRec20() & Mno_MergeLien20().
 *                     Modify Mno_Load_LDR() and Mno_ExtrLien() to support new file layout.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doZip.h"
#include "doOwner.h"
#include "doGrGr.h"
#include "formatApn.h"

#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "PQ.h"
#include "Charrec.h"
#include "MergeMno.h"
#include "Tax.h"

char acApnFile[_MAX_PATH];

/******************************* Mno_MergeChar *******************************
 *
 * Merge Mno_Char.dat in STDCHAR format
 *
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the one that has value.
 *
 *****************************************************************************/

int Mno_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   ULONG    lSqft;
   int      iLoop, iBeds, iFBath, iHBath, iBldgNum, iRooms, iUnits;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   while (!iLoop)
   {
      // Quality Class
      if (pChar->QualityClass[0] > ' ')
      {
         *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
         *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
         memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);
      }

      // YrBlt
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

      // YrEff
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

      // BldgSqft
      lSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      } else
         memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

      // Garage Sqft
      lSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
      } else
      {
         memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = ' ';
      }

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "001040017000", 9))
      //   lSqft = 0;
#endif

      // LotSqft
      lSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10 && *(pOutbuf+OFF_LOT_SQFT+8) == ' ')
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lSqft);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
         
         lSqft = atoin(pChar->LotAcre, SIZ_CHAR_SQFT);
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lSqft);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }

      // PatioSqft
      lSqft = atoin(pChar->PatioSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_PATIO_SF, lSqft);
         memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
      } else
         memset(pOutbuf+OFF_PATIO_SF, ' ', SIZ_BLDG_SF);

      // Heating
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   
      // Cooling 
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

      // Total Rooms
      iRooms = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
      if (iRooms > 0)
      {
         sprintf(acTmp, "%*d", SIZ_ROOMS, iRooms);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      } else
         memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

      // Beds
      iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      } else
         memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

      // Bath
      iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      } else
         memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

      // Half bath
      iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
      if (iHBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      } else
         memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

      // Fireplace
      *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

      // HasSeptic or HasSewer
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

      // HasWell
      *(pOutbuf+OFF_WATER) = pChar->HasWater;

      // Pools
      *(pOutbuf+OFF_POOL) = pChar->Pool[0];

      // Units count
      iUnits = atoin(pChar->Units, SIZ_CHAR_UNITS);
      if (iUnits > 0)
      {
         sprintf(acTmp, "%*d", SIZ_UNITS, iUnits);
         memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
      } else
         memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

      // Stories
      if (pChar->Stories[0] > ' ')
         memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
      else
         memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

      // BldgSeqNum
      iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);

      lCharMatch++;

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (!pRec)
         break;
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (!iLoop && (iBeds > 0 && iBldgNum > 0))
         break;
   }

   return 0;
}

/******************************* Mno_MakeDocLink *******************************
 *
 * Format DocLink
 *    S_DOCLINK = 'dt;'+SUBSTRING(S_DOCNUM,1,4)+'.'+cast(cast(SUBSTRING(S_DOCNUM,6,7) as int) as varchar(6))+';2470'
 *       WHERE S_DOCNUM IS NOT NULL     
 *       and SUBSTRING(S_DOCNUM, 5, 1) = 'R'
 *       and SUBSTRING(S_DOCNUM, 6, 1) <= '9'
 *       and S_DOCDATE > 20000701
 *
 ******************************************************************************/

void Mno_MakeDocLink(LPSTR pDocLink, LPSTR pDoc, LPSTR pDate)
{
   char  acDocName[256];
   int   lDocDate, lDocNum;

   *pDocLink = 0;
   lDocDate = atoin(pDate, 8);
   if (*pDoc > ' ' && *(pDoc+4) == 'R' && *(pDoc+5) <= '9' && lDocDate > 20000701)
   {
      lDocNum = atoin(pDoc+5, 7);
      sprintf(acDocName, "dt;%.4s.%.6d;2470", pDoc, lDocNum);     
      strcpy(pDocLink, acDocName);
   }
}

/*****************************************************************************
 *
 *****************************************************************************/

void Mno_FormatApn(LPSTR pApn, LPSTR pFmtApn, int *piApnCnt)
{
   char acApn[64], acTmp[64], acTmp2[64], *pTmp, *pTmp1;
   int  iRet, iTmp, iLen;

   *pFmtApn = 0;
   *piApnCnt = 0;
   if (!pApn || !*pApn || *pApn == '-')
      return;

   if (pTmp = isCharIncluded(pApn, 0))
      return;

   strcpy(acTmp, pApn);

   // Check for multiple APN
   *piApnCnt = 1;

   // Take first APN and ignore the rest
   if (pTmp = strchr(acTmp, ';'))
   {
      iRet = countChar(acTmp, ';');
      *piApnCnt = iRet;
      *pTmp = 0;
   }
   if (pTmp = strchr(acTmp, ','))
   {
      blankRem(acTmp);
      iRet = ParseString(acTmp, ',', MAX_FLD_TOKEN, apTokens);
      if (strlen(apTokens[iRet-1]) > 7)
         *piApnCnt = iRet;
      *pTmp = 0;
   }
   if (pTmp = strchr(acTmp, ' '))
   {
      blankRem(acTmp);
      iRet = ParseString(acTmp, ' ', MAX_FLD_TOKEN, apTokens);
      if (strlen(apTokens[iRet-1]) > 7)
         *piApnCnt = iRet;
      *pTmp = 0;
   }
   if (pTmp = strchr(acTmp, '.'))
      *pTmp = 0;

   // acTmp should now contains only one APN
   if ((pTmp = strchr(acTmp, '*')) || (pTmp = strchr(acTmp, '+')) || (pTmp = strchr(acTmp, '`')))
      strcpy(pTmp, pTmp+1);

   // If last char is '-', remove it
   iRet = strlen(acTmp);
   if (acTmp[iRet-1] == '-')
      acTmp[iRet-1] = 0;

   // Make first part with 2 digits
   if (acTmp[1] == '-')
   {
      sprintf(acTmp2, "0%s", acTmp);
      strcpy(acTmp, acTmp2);
   }

   // Drop if APN is less than 7 digits
   if (strlen(acTmp) < 7)
   {
      *piApnCnt = 0;
      return;
   }

   // Original                 Format              Translate
   // --------                 ------              ---------
   // 60-230-14            ==> 060230014000
   // 8-095-03             ==> 008095003000
   // 02-60-45             ==> 002060045000
   // 26-09034             ==> 002609034000000 ==> 026090034000
   // 19210-01             ==> 019210010000
   // 33-160-66-02         ==> 003316066020000 ?
   // 32-160-01-201        ==> 003216001201000 ?
   // 35-086-03-021-07     ==> 003508603021070 ==> ?
   // 35-242-07-000-000    ==> 003524207000000 ==> 035242007000
   // 31-220-06-0010       ==> 003122006001000 ==> 219010000000
   // 15-086-03-0004-45    ==> 001508603000445 ==> 233004045000
   // 00-32-100-01-0015-00 ==> 003210001001500 ==> 203015000000
   // 00-15-086093-0008-35 ==> 001508609300083 ==> ?
   // 3324009              ==> 003324009000000 ==> 033240009000
   // 3102010010           ==> 003102010010000 ?
   // 003205003008500      ==> 003205003008500 ==> 344085000000
   // 19/181/01            ==> 019181001000
   // 02-361-03/04         ==> 002361003000
   // 15-086-03-0022-10/15-086-03-0019-52
   //                      ==> 001508603002210 ==> 233022010000
   // 15-08/6-03-13987     ==> ?
   // 08-184-03 / 14-A
   // 22-320-23/24/25/28
   // 16/212-08
   // 22-470-2-
   // 15-010-12-0017
   // 26-2720=-08
   // 40=110=10=0004       ==> 004011010000400 ==> 330004000000
   // 024-100-008-000

   // Replace '=' with '-'
   replChar(acTmp, '=', '-');

   // Replace '--' with '-'
   replStr(acTmp, "--", "-");

   // If there is '/' in string and no '-' preceeded it, replace all with '-'
   // If there is '/' in string and no '-' in it, replace all with '-'
   // If there is '/' in string and more than one '-' preceeded it, terminate string right there
   pTmp = strchr(acTmp, '/');
   if (pTmp)
   {
      *pTmp = 0;        // Terminate string here
      if (!(pTmp1 = strchr(acTmp, '-')) || !(pTmp1 = strchr(pTmp+1, '-')))
      {
         *pTmp = '-';
         replChar(acTmp, '/', '-');
      } else
         *piApnCnt = 2;
   }

    // Save it
   strcpy(acApn, acTmp);
   iLen = strlen(acTmp);
   iRet = ParseString(acTmp, '-', MAX_FLD_TOKEN, apTokens);
   switch(iRet)
   {
      case 1:
         if (strlen(acApn) == 12)
            sprintf(acTmp2, "%s000", acApn);
         else if (acApn[0] > '0')
            sprintf(acTmp2, "00%s000000", acApn);
         else
            sprintf(acTmp2, "%s00000000", acApn);
         break;
      case 2:
         iTmp = strlen(apTokens[0]);
         if (iTmp < 3)
         {
            if (acApn[0] > '0')
               sprintf(acTmp2, "00%.3d%s000000000", atol(apTokens[0]), apTokens[1]);
            else
               sprintf(acTmp2, "%.3d%s00000000000", atol(apTokens[0]), apTokens[1]);
         } else if (iTmp > 3)
         {
            if (acApn[0] > '0')
               sprintf(acTmp2, "0%s%s000000000", apTokens[0], apTokens[1]);
            else
               sprintf(acTmp2, "%s%s000000000", apTokens[0], apTokens[1]);
         } else
         {
            if (acApn[0] > '0')
               sprintf(acTmp2, "00%s%s000000000", apTokens[0], apTokens[1]);
            else
               sprintf(acTmp2, "%s%s000000000", apTokens[0], apTokens[1]);
         }
         break;
      case 3:
         // Format each tokens as %.3d then add "000" to the end
         sprintf(acTmp2, "%.3d%.3d%.3d000000", atol(apTokens[0]), atol(apTokens[1]), atol(apTokens[2]));
         break;
      default: // 4, 5, 6, 7
         if (iRet == 4 && iLen == 15)
            sprintf(acTmp2, "%s%s%s%s", apTokens[0], apTokens[1], apTokens[2], apTokens[3]);
         else
         {
            // Should starts and ends with "00"
            if (iRet == 4 && acApn[3] == '-' && acApn[7] == '-' && acApn[11] == '-')
               sprintf(acTmp2, "%s000", acApn);
            else if (!memcmp(acApn, "00", 2))
               sprintf(acTmp2, "%s000000000", acApn);
            else if (acApn[0] == '0')
               sprintf(acTmp2, "0%s000000000", acApn);
            else
               sprintf(acTmp2, "00%s000000000", acApn);
            remChar(acTmp2, '-');
         }
         break;
   }

   acTmp2[15] = 0;
   strcpy(pFmtApn, acTmp2);
}

/******************************** Mno_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Mno_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1;
   char  acTmp[128], *pTmp, *pTmp1;
   char  acOwners[64];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002361013", 9) )
   //   iTmp = 0;
#endif

   // Remove multiple spaces
   if (*pNames == '"')
   {
      pTmp = strcpy(acOwners, pNames+1);
      acOwners[strlen(pTmp)-1] = 0;
   } else
      pTmp = strcpy(acOwners, pNames);

   replStrAll(acOwners, "\"\"", "\"");
   replChar(acOwners, ',', ' ');
   replChar(acOwners, '.', ' ');
   iTmp = iTmp1 = 0;
   while (*pTmp)
   {
      // Mark name with numeric value
      if (isdigit(*pTmp))
         iTmp1++;

      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }
      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ' || acTmp[iTmp-1] == '"')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Drop ET AL
   if ((pTmp=strstr(acTmp, " ETAL &")) || (pTmp=strstr(acTmp, " ET AL &")) )
   {
      memset(pTmp, ' ', 6);
      blankRem(pTmp);
   }

   if (pTmp=strstr(acTmp, "(ET AL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ET AL"))
      *pTmp = 0;
   strcpy(acOwners, acTmp);
   
   // Drop everything from these words
   if (pTmp=strstr(acTmp, " CO-TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, ",FAM"))
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " TRUST &")))
   {
      memset(pTmp, ' ', 6);
      blankRem(pTmp);
   }

   if (pTmp = strchr(acTmp, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
         *pTmp1++ = 0;
   }

   // Now parse owners
   acTmp[63] = 0;
   splitOwner(acTmp, &myOwner, 0);

   iTmp = strlen(acOwners);
   if (iTmp > SIZ_NAME1)
      iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acOwners, iTmp);

   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
}

/******************************** Mno_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Mno_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "061134014", 9))
   //   iTmp = 0;
#endif

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      iTmp = strlen(pTmp);
      if (iTmp > SIZ_DBA)
         iTmp = SIZ_DBA;
      memcpy(pOutbuf+OFF_DBA, pTmp, iTmp);
   } 

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1_1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
     sMailAdr.strName[SIZ_M_STREET] = 0;
     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, _strupr(apTokens[MB_ROLL_M_CITY]), SIZ_M_CITY);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, _strupr(apTokens[MB_ROLL_M_ST]), 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], 9);

      iTmp = atol(apTokens[MB_ROLL_M_ZIP]);
      if (iTmp > 100000)
         sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP], apTokens[MB_ROLL_M_ZIP]+5);
      else if (iTmp > 400)
         sprintf(acTmp, "%s %s %.5s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
      else
         sprintf(acTmp, "%s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST]);

      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

void Mno_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4=NULL)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2, sAdr1[128];
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "031130003000", 9) )
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   strcpy(sAdr1, pLine1);
   lTrim(sAdr1);

   if (sAdr1[0] == ' ' || sAdr1[0] == '0' || *pLine2 == '0')
      return;
   if (sAdr1[0] == '#')
      sAdr1[0] = ' ';

   acAddr1[0] = 0;
   p0 = NULL;
   if (pLine4 && *pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(sAdr1, "C/O", 3)  ||
          !_memicmp(sAdr1, "ATTN", 4) ||
          sAdr1[0] == '%')
      {
         p0 = sAdr1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(sAdr1[0]))
            p1 = sAdr1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(sAdr1, "C/O", 3)  ||
          !_memicmp(sAdr1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = sAdr1;
         p1 = pLine2;
      } else if (!memcmp(sAdr1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, sAdr1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(sAdr1, "STE"))
            p1 = sAdr1;
         else
         {
            sprintf(acAddr1, "%s %s", sAdr1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", sAdr1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = sAdr1;
            p2 = pLine2;
         } else if (isdigit(sAdr1[0]))
         {
            p1 = sAdr1;
         } else
         {
            p1 = pLine2;
            p0 = sAdr1;
         }
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = sAdr1;
      p2 = pLine2;
   } else
   {
      p2 = sAdr1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
      /*
      if (!_memicmp(p0, "C/O", 3))
         pTmp = p0+4;
      else if (!_memicmp(p0, "ATTN", 4))
         pTmp = p0+5;
      else if (*p0 == '%')
         pTmp = p0+1;
      else
         pTmp = p0;

      while (*pTmp == ' ')
         pTmp++;
      iTmp = strlen(pTmp);
      if (iTmp > SIZ_CARE_OF)
         iTmp = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, pTmp, iTmp);
      */
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      parseMAdr1_1(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      } else
      {
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
      }
   }

   strcpy(acAddr2, p2);
   remChar(acAddr2, ',');     // Remove comma
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      iTmp = strlen(sMailAdr.Zip);
      memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, iTmp);
      if (isdigit(sMailAdr.Zip4[0]))
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, strlen(sMailAdr.Zip4));
   }
}

/******************************** Mno_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Mno_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acAddr1[256], acSuffix[16], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;
   STRSFX   *pSfx;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do {
         pRec = fgets(acRec, 512, fdSitus);
      } while (pRec && !isdigit(*pRec));
   }

   pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input string
   _strupr(pRec);
   iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old data
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      // Save original house number
      //memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
      }
   }

   quoteRem(apTokens[MB_SITUS_STRNAME]);
   iTmp = strlen(apTokens[MB_SITUS_STRNAME]);
   if (iTmp > 0)
   {
      // Fix known bad char in StrName
      if (pTmp = strchr(apTokens[MB_SITUS_STRNAME], 241))
         *pTmp = 'N';

      acSuffix[0] = 0;
      if (*apTokens[MB_SITUS_STRTYPE] > ' ')
      {
         strcpy(acSuffix, apTokens[MB_SITUS_STRTYPE]);

         // Modify asSfxTbl[] table to add new suffixes
         pSfx = GetSfxCode(acSuffix, &asSfxTbl[0]);
         if (pSfx)
         {
            memcpy(pOutbuf+OFF_S_SUFF, pSfx->pSfxCode, strlen(pSfx->pSfxCode));
            strcpy(acSuffix, pSfx->pStrSfx);
         } else
         {
            LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
            iBadSuffix++;
         }        
      } else
      {
         // Check last word in StrName for suffix
         if (pTmp = strrchr(apTokens[MB_SITUS_STRNAME], ' '))
         {
            strcpy(acSuffix, pTmp+1);
            pSfx = GetSfxCode(acSuffix, &asSfxTbl[0]);
            if (pSfx)
            {
               memcpy(pOutbuf+OFF_S_SUFF, pSfx->pSfxCode, strlen(pSfx->pSfxCode));
               strcpy(acSuffix, pSfx->pStrSfx);
               *pTmp = 0;
            } else
               acSuffix[0] = 0;
         }
      }

      vmemcpy(pOutbuf+OFF_S_STREET, apTokens[MB_SITUS_STRNAME], SIZ_S_STREET);
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      if (acSuffix[0] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, acSuffix);
      }

      if (*apTokens[MB_SITUS_UNIT] > ' ')
      {
         if (*apTokens[MB_SITUS_UNIT] != '#')
            iTmp = sprintf(acTmp, " #%s", apTokens[MB_SITUS_UNIT]);
         else
            iTmp = sprintf(acTmp, " %s", apTokens[MB_SITUS_UNIT]);
         strcat(acAddr1, acTmp);
         memcpy(pOutbuf+OFF_S_UNITNO, &acTmp[1], iTmp-1);
         memcpy(pOutbuf+OFF_S_UNITNOX, &acTmp[1], iTmp-1);
      }
   }

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1, pOutbuf);
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
      {
         iTmp = sprintf(acTmp, "%s, CA %s", myTrim(acAddr1), apTokens[MB_SITUS_ZIP]);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
      }
   }

   // Situs zip
   if (*apTokens[MB_SITUS_ZIP] > ' ')
   {
      lTmp = atol(apTokens[MB_SITUS_ZIP]);
      if (lTmp >= 90000 && lTmp < 99999)
         memcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], 5);
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/* Not use
int Mno_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001060015", 9))
   //   acTmp[0] = 0;
#endif

   if (*pLine1 > '0')
   {
      // Change case
      _strupr(pLine1);

      strcpy(acAddr1, pLine1);
      blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

      memset(&sSitusAdr, 0, sizeof(ADR_REC));
      parseMAdr1(&sSitusAdr, acAddr1);

      if (sSitusAdr.lStrNum > 0)
      {
         char *pTmp = strchr(acAddr1, ' ');
         *pTmp = 0;

         // Save original StrNum
         memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
         memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
         memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
      }

      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

      memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.strSfx[0] > ' ')
      {
         Sfx2Code(sSitusAdr.strSfx, acTmp);
         memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
      }

      if (sSitusAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));
   }

   // Situs city
   _strupr(pLine2);
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] >= 'A')
   {
      char *pTmp;
      int  iTmp, iCityIdx;

      sSitusAdr.State[0] = 0;    // Prevent long city name
      iCityIdx = City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (iCityIdx > 0)
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         iCityIdx = atol(acTmp);
         if (pTmp = GetCityName(iCityIdx))
         {
            iTmp = sprintf(acTmp, "%s CA", pTmp);
            memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
         }
      } else
         memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   } else
      memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));

   return 0;
}
*/

/******************************** Mno_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Mno_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdSale);
      pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg("Skip Sale rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   while (!iTmp)
   {
      // Replace tab char with 0
      if (pRec)
         iRet = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "381014000000", 9))
      //   iRet = 0;
#endif

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' &&
          *apTokens[MB_SALES_DOCDATE] > ' ' &&
          apTokens[MB_SALES_DOCNUM][4] == 'R')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         /* Do not use Confidential Sale Price
         } else
         {
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            }
         */
         }

         // DocType - need translation before production
         // 01, 02, 03, 09, 10, 11, 12, 58, 81
         // Right now we only know 01 is GD, the rest needs investigation
         iTmp = atoi(apTokens[MB_SALES_DOCCODE]);
         if (iTmp > 0 && iTmp < 100)
         {
            iTmp = sprintf(acTmp, "%d", iTmp);
            memcpy(sCurSale.acDocType, acTmp, iTmp);
         }

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strcpy(acTmp, apTokens[MB_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         if (iTmp > SALE_SIZ_SELLER)
            iTmp = SALE_SIZ_SELLER;
         memcpy(sCurSale.acSeller, acTmp, iTmp);

         MB_MergeSale(&sCurSale, pOutbuf, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Mno_MergeChar ******************************
 *
 * Note: need code table for Heating and Colling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

int Mno_MergeChar(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], *pTmp;
   long     lBldgSqft, lGarSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iFp;
   MB_CHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 512, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   pChar = (MB_CHAR *)pRec;

   // Quality Class
   memcpy(acTmp, pChar->QualityClass, MBSIZ_CHAR_QUALITY);
   acTmp[MBSIZ_CHAR_QUALITY] = 0;
   pTmp = strchr(acTmp, ' ');
   if (pTmp) *pTmp = 0;

   acCode[0] = 0;
   if (!_memicmp(acTmp, "AVG", 3))
      strcpy(acCode, "A");
   else if (isalpha(acTmp[0]))
   {
      *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0];
      if (isdigit(acTmp[1]))
         iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
      else if (pTmp && isdigit(*(pTmp+1)))
         iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
   } else if (acTmp[0] > '0' && acTmp[0] <= '9')
      iRet = Quality2Code(acTmp, acCode, NULL);

   if (acCode[0] > ' ')
   {
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }

   // Yrblt
   iTmp = atoin(pChar->YearBuilt, MBSIZ_CHAR_YRBLT);
   if (iTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);
   //else
   //   memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } 
   //else
   //   memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = '2';
   } 
   //else
   //{
   //   memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
   //   *(pOutbuf+OFF_PARK_TYPE) = ' ';
   //}

   // Heating
   if (pChar->Heating[0] > ' ')
   {
      iTmp = 0;
      while (asHeating[iTmp].iLen > 0)
      {
         if (pChar->Heating[0] == asHeating[iTmp].acSrc[0])
         {
            *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }

   // Cooling
   //if (pChar->Cooling[0] > ' ')
   //{
   //   iTmp = 0;
   //   while (asCooling[iTmp].iLen > 0)
   //   {
   //      if (pChar->Cooling[0] == asCooling[iTmp].acSrc[0])
   //      {
   //         *(pOutbuf+OFF_AIR_COND) = asCooling[iTmp].acCode[0];
   //         break;
   //      }
   //      iTmp++;
   //   }
   //}
   // Pool
   if (pChar->NumPools[0] > ' ')
   {
      iTmp = 0;
      while (asPool[iTmp].iLen > 0)
      {
         if (pChar->NumPools[0] == asPool[iTmp].acSrc[0])
         {
            *(pOutbuf+OFF_POOL) = asPool[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }

   // Beds
   iBeds = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001011002", 9))
   //   iFp = 0;
#endif
   // Fireplace
   iFp = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
   //if (iFp > 0)
   //{
   //   sprintf(acTmp, "%d", SIZ_FIRE_PL, iFp);
   //   memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   //} 
   //else
   //   memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);

   // HasSeptic or HasSewer
   if (pChar->HasSeptic > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
   else if (pChar->HasSewer > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWell;

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 512, fdChar);

   return 0;
}

/******************************** Mno_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************

int Mno_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdExe);
      pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 0;
   }

   iRet = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Mno_MergeTax ******************************
 *
 * Note:
 *
 ****************************************************************************

int Mno_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double   dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdTax);
      pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iRet;

   // Merge tax data
   while (!iTmp)
   {
      iTmp = ParseStringNQ(pRec, cDelim, MB_TAX_ROLLCAT+1, apTokens);
      if (iTmp < MB_TAX_ROLLCAT)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
         return -1;
      }

      // If same tax year, apply values
      //if (!memcmp(myCounty.acYearAssd, apTokens[MB_TAX_YEAR], 4))
      {
         dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
         dTax1 = atof(acTmp);
         dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
         dTax2 = atof(acTmp);
         dTmp = dTax1+dTax2;
         if (dTax1 == 0.0 || dTax2 == 0.0)
            dTmp *= 2;

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/********************************* Mno_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mno_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace tab char with 0
   if (cDelim == '|')
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN starts with 800-999 except 910 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910))
      return 1;

   // Ignore administrative parcels
   if (!_memicmp(apTokens[MB_ROLL_OWNER], "Assessor Administrative", 20))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "26MNO", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   lTmp = atol(apTokens[MB_ROLL_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "035245001000", 9))
   //   iTmp = 0;
#endif

   // Legal - Stop using legal as of 10/17/2022 per county request (see email)
   removeLegal(pOutbuf);
   //if (*apTokens[MB_ROLL_LEGAL] > ' ')
   //{
   //   // Fix known bad char
   //   if ((pTmp = strchr(apTokens[MB_ROLL_LEGAL], 241)) || (pTmp = strchr(apTokens[MB_ROLL_LEGAL], 209)) )
   //      *pTmp = 'N';
   //   updateLegal(pOutbuf, _strupr(apTokens[MB_ROLL_LEGAL]));
   //}

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   if (*apTokens[MB_ROLL_USECODE] > ' ')
   {
      strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
      blankPadz(acTmp, SIZ_USE_CO);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO);

      // Standard UseCode
      iTmp = atol(acTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);
   } else
   {
      memset(pOutbuf+OFF_USE_CO, ' ', SIZ_USE_CO);
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
   }
   
   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      if (*(apTokens[MB_ROLL_DOCNUM]+4) == 'R' && *(apTokens[MB_ROLL_DOCNUM]+5) <= '9')
         sprintf(acTmp, "%.4sR%.7d", apTokens[MB_ROLL_DOCNUM], atol(apTokens[MB_ROLL_DOCNUM]+5));
      else
         strcpy(acTmp, apTokens[MB_ROLL_DOCNUM]);
      vmemcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, SIZ_TRANSFER_DOC);

      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   }

   // Owner
   try {   
      lTrim(apTokens[MB_ROLL_OWNER]);
      if (*apTokens[MB_ROLL_OWNER] > ' ')
         Mno_MergeOwner(pOutbuf, _strupr(apTokens[MB_ROLL_OWNER]));
      //else if (*(apTokens[MB_ROLL_OWNER]+1) > ' ')
      //   Mno_MergeOwner(pOutbuf, _strupr(apTokens[MB_ROLL_OWNER]+1));
      else if (*apTokens[MB_ROLL_CAREOF] > ' ')
      {
         LogMsg("*** No owner name, use CareOf for owner: %.12s", pOutbuf);
         Mno_MergeOwner(pOutbuf, _strupr(apTokens[MB_ROLL_CAREOF]));
      } else
         LogMsg("*** No owner name, no CareOf: %.12s", pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Mno_MergeOwner()");
   }

   // Mailing
   try {
      if (*apTokens[MB_ROLL_M_ADDR] > ' ')
         Mno_MergeMAdr(pOutbuf);
      else if (*apTokens[MB_ROLL_M_ADDR1] > ' ')
         Mno_MergeMAdr(pOutbuf, apTokens[MB_ROLL_M_ADDR1], apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3], apTokens[MB_ROLL_M_ADDR4]);
   } catch(...) {
      LogMsg("***** Exeception occured in Mno_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/******************************** Mno_MergeOthers ****************************
 *
 * Merge usecode, neighborhood code from roll file
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Mno_MergeOthers(char *pOutbuf)
{
   static   char acRec[MAX_RECSIZE], *pRec=NULL;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Roll rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iTmp = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB_ROLL_M_ADDR4)
   {
      LogMsg("***** Error: bad input record for APN=%.12s (tokens=%d)", pOutbuf, iTmp);
      return -1;
   }
   lRollMatch++;

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   if (*apTokens[MB_ROLL_USECODE] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[MB_ROLL_USECODE], SIZ_USE_CO);

      // Standard UseCode
      iTmp = atol(apTokens[MB_ROLL_USECODE]);
      updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);
   } else
   {
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
   }
   
   // Update neightborhood
   if (*apTokens[MB_ROLL_NBHCODE] > ' ')
      vmemcpy(pOutbuf+OFF_NBH_CODE, apTokens[MB_ROLL_NBHCODE], SIZ_NBH_CODE);

   // Get next record
   pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
   if (!pRec)
   {
      fclose(fdRoll);
      fdRoll = NULL;
   }

   return 0;
}

/********************************** Mno_Load_Roll *****************************
 *
 * Normal update might start right before GrGr.  This could overwrite the normal
 * update process.
 *
 ******************************************************************************/

int Mno_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acRollFile, acTmpFile, "S(1,12,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -3;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -4;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -6;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -7;
   }

   // Open lien file
   fdLienExt = NULL;
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Lien file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -9;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -10;
   }

   // Drop header record - No need, it's been sorted
   //pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

#ifdef _DEBUG
      //if (!memcmp(acBuf, "001210007000", 10))
      //   iTmp = 0;
#endif

NextRollRec:
      iTmp = memcmp(acBuf, (char *)&acRollRec[0], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Mno_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Mno_MergeSitus(acBuf);

            // Merge Lien
            lRet = 1;
            if (fdLienExt)
               lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

            // Merge Exe if not found in LienExt
            if (fdExe && lRet)
               lRet = MB_MergeExe(acBuf);

            // Merge Char
            if (fdChar)
               //lRet = Mno_MergeChar(acBuf);
               lRet = Mno_MergeStdChar(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Mno_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Mno_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               //lRet = Mno_MergeChar(acRec);
               lRet = Mno_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Mno_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Mno_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            //lRet = Mno_MergeChar(acRec);
            lRet = Mno_MergeStdChar(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTax(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   printf("\n");

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   return 0;
}

/******************************** Mno_MergeLien2 *****************************
 *
 * Process data for 2015
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mno_MergeLien2(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], acApn[20];
   long     lTmp;
   double   dTmp;
   int      iRet, iTmp;

   // Parse input rec
   if (cLdrSep == 9)
      iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < MNO_L2_EXEMPTIONAMT1)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[MNO_L2_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   iRet = remChar(apTokens[MNO_L2_ASMT], '-');

   // Start copying data
   iRet = strlen(apTokens[MNO_L2_ASMT]);
   if (iRet < iApnLen)
   {
      memcpy(acApn, "0000", iApnLen - iRet);
      memcpy(&acApn[iApnLen - iRet], apTokens[MNO_L2_ASMT], iRet);
   } else
      memcpy(acApn, apTokens[MNO_L2_ASMT], iRet);
   memcpy(pOutbuf, acApn, iApnLen);
   acApn[iApnLen] = 0;

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "26MNO", 5);

   // Status
   *(pOutbuf+OFF_STATUS) = *apTokens[MNO_L2_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "202001000000", 9))
   //   iTmp = 0;
#endif

   // Copy ALT_APN
   iRet = remChar(apTokens[MNO_L2_FEEPARCEL], '-');
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[MNO_L2_FEEPARCEL], iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[MNO_L2_CURRENTMARKETLAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[MNO_L2_CURRENTSTRUCTURALIMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[MNO_L2_CURRENTFIXEDIMPR]);
   long lPers  = atoi(apTokens[MNO_L2_CURRENTPERSONALPROP]);
   long lPP_MH = atoi(apTokens[MNO_L2_CURRENTPERSONALPROPMH]);
   lTmp = lFixtr+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[MNO_L2_EXEMPTIONAMT1]);
   if (lExe1 > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lExe1);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }
   if (!memcmp(apTokens[MNO_L2_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[MNO_L2_EXEMPTIONCODE1], strlen(apTokens[MNO_L2_EXEMPTIONCODE1]));

   // TRA
   lTmp = atol(apTokens[MNO_L2_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[MNO_L2_STATUS];

   // Legal - not available in 2015 LDR
   //updateLegal(pOutbuf, apTokens[MNO_L2_PARCELDESCRIPTION]);

   // UseCode
   iTmp = atol(apTokens[MNO_L2_USECODE]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[MNO_L2_USECODE], SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   //if (iTmp > 0)
   //{
   //   // Std Usecode
   //   updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);

   //   iTmp = strlen(apTokens[MNO_L2_USECODE]);
   //   if (iTmp > 0)
   //   {
   //      if (iTmp > SIZ_USE_CO)
   //         iTmp = SIZ_USE_CO;
   //      memcpy(pOutbuf+OFF_USE_CO, apTokens[MNO_L2_USECODE], iTmp);
   //   }
   //} else
   //   memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MNO_L2_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved - not avail.
   //if (*apTokens[MNO_L2_ISAGPRESERVE] == '1')
   //   *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Mno_MergeOwner(pOutbuf, apTokens[MNO_L2_OWNER]);

   // Mailing
   Mno_MergeMAdr(pOutbuf, apTokens[MNO_L2_MAILADDRESS1], apTokens[MNO_L2_MAILADDRESS2], apTokens[MNO_L2_MAILADDRESS3]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MNO_L2_TAXABILITY], true, true);

   return 0;
}

/******************************** Mno_MergeLien1 *****************************
 *
 * Process data for 2014
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mno_MergeLien1(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], acApn[20];
   long     lTmp;
   double   dTmp;
   int      iRet, iTmp;

   // Parse input rec
   replStrAll(pRollRec, "|NULL", "|");
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < MNO_L_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   iRet = strlen(apTokens[MNO_L_ASMT]);
   if (iRet < iApnLen)
   {
      memcpy(acApn, "0000", iApnLen - iRet);
      memcpy(&acApn[iApnLen - iRet], apTokens[MNO_L_ASMT], iRet);
   } else
      memcpy(acApn, apTokens[MNO_L_ASMT], iRet);
   memcpy(pOutbuf, acApn, iApnLen);
   acApn[iApnLen] = 0;

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "26MNO", 5);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "202001000000", 9))
   //   iTmp = 0;
#endif

   // Copy ALT_APN
   iRet = strlen(apTokens[MNO_L_FEEPARCEL]);
   if (iRet > 8 && iRet < iApnLen)
   {
      memcpy(acTmp, "0000", iApnLen - iRet);
      memcpy(&acTmp[iApnLen - iRet], apTokens[MNO_L_FEEPARCEL], iRet);
      memcpy(pOutbuf+OFF_ALT_APN, acTmp, iApnLen);
   } else if (iRet <= SIZ_ALT_APN)
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MNO_L_FEEPARCEL], iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values
   // Land
   long lLand = atoi(apTokens[MNO_L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[MNO_L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[MNO_L_CURRENTFIXEDIMPRVALUE]);
   //long lGrow  = atoi(apTokens[MNO_L_CURRENTGROWINGIMPRVALUE]);
   long lGrow = 0;      // Not avail LDR 2014
   long lPers  = atoi(apTokens[MNO_L_CURRENTPERSONALPROPVALUE]);
   long lPP_MH = atoi(apTokens[MNO_L_CURRENTPERSONALPROPMHVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[MNO_L_TAXAMT1]);
   double dTax2 = atof(apTokens[MNO_L_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[MNO_L_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[MNO_L_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[MNO_L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }
   if (!memcmp(apTokens[MNO_L_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[MNO_L_EXEMPTIONCODE1], strlen(apTokens[MNO_L_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[MNO_L_EXEMPTIONCODE2], strlen(apTokens[MNO_L_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[MNO_L_EXEMPTIONCODE3], strlen(apTokens[MNO_L_EXEMPTIONCODE3]));

   // TRA
   lTmp = atol(apTokens[MNO_L_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[MNO_L_STATUS];

   // Legal - Stop using legal as of 10/17/2022
   //updateLegal(pOutbuf, apTokens[MNO_L_PARCELDESCRIPTION]);

   // UseCode
   iTmp = atol(apTokens[MNO_L_USECODE]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[MNO_L2_USECODE], SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   //if (iTmp > 0)
   //{
   //   // Std Usecode
   //   updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);

   //   iTmp = strlen(apTokens[MNO_L_USECODE]);
   //   if (iTmp > 0)
   //   {
   //      if (iTmp > SIZ_USE_CO)
   //         iTmp = SIZ_USE_CO;
   //      memcpy(pOutbuf+OFF_USE_CO, apTokens[MNO_L_USECODE], iTmp);
   //   }
   //}

   // Acres
   dTmp = atof(apTokens[MNO_L_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[MNO_L_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Mno_MergeOwner(pOutbuf, apTokens[MNO_L_OWNER]);

   // Situs - Use situsfile instead
   // Mno_MergeSitus(pOutbuf, apTokens[MNO_L_SITUS1], apTokens[MNO_L_SITUS2]);

   // Mailing
   Mno_MergeMAdr(pOutbuf, apTokens[MNO_L_MAILADDRESS1], apTokens[MNO_L_MAILADDRESS2], apTokens[MNO_L_MAILADDRESS3]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MNO_L_TAXABILITY], true, true);

   return 0;
}

/******************************** Mno_Load_LDR1 *****************************
 *
 * Load MNO_yyyy.txt file into 1900-byte record.
 *
 ****************************************************************************/

int Mno_Load_LDR1(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     iRet, lRet=0, lCnt=0;

   lLastFileDate = getFileDate(acRollFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -4;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating R01 file: %s\n", acTmpFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (!isdigit(acRec[0]))
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      lLDRRecCount++;

      // Create new R01 record
      if (lLienYear >= 2015)
         iRet = Mno_MergeLien2(acBuf, acRec);
      else
         iRet = Mno_MergeLien1(acBuf, acRec);      // 2014

      if (!iRet)
      {
         // Merge Char
         if (fdChar)
            lRet = Mno_MergeStdChar(acBuf);
            //lRet = Mno_MergeChar(acBuf);

         // Merge Situs
         if (fdSitus)
            lRet = Mno_MergeSitus(acBuf);

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/****************************** Mno_ExtrTR601Rec ****************************
 *
 * Format lien extract record from TR601 file.  This function may not work with
 * all counties.  Check lien file layout first before use.
 *
 ****************************************************************************/

int Mno_ExtrTR601Rec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp, iLen;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "001060037000", 12))
   //   iRet = 0;
#endif

   // Parse string ignoring quote
   //iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   iLen = strlen(apTokens[L_ASMT]);
   memcpy(pLienExtr->acApn,  apTokens[L_ASMT], iLen);

   // TRA
   lTmp = atol(apTokens[L_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lGrow = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);
   long lFixt   = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);
   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Prop 8
   lTmp = atoin(apTokens[L_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Save Exe code
   memcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
   memcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
   memcpy(pLienExtr->extra.MB.ExeCode3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************* Mno_ExtrTR601 ******************************
 *
 * Extract lien data from ???_lien.csv
 *
 ****************************************************************************/

int Mno_ExtrTR601(LPCSTR pCnty, LPCSTR pLDRFile=NULL)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg("Extract lien roll for %s", pCnty);

   // Open lien file
   if (!pLDRFile)
   {
      GetIniString(pCnty, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
      if (!acBuf[0])
         return -1;
   } else
      strcpy(acBuf, pLDRFile);

   LogMsg("Open Lien Date Roll file %s", acBuf);
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Sort on ASMT
   iRet = sortFile(acBuf, acTmpFile, "S(#1,C,A)");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acTmpFile);
      return -2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Replace null char with space
      //iRet = replChar(acRec, 0, ' ', 0);

      // Create new record
      iRet = Mno_ExtrTR601Rec(acBuf, acRec);

      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Mno_ExtrLien *******************************
 *
 * Extract lien data from Mno_yyyy.txt
 * Use this function to extract data from SQL export file as of LDR 2014.
 *
 ****************************************************************************/

int Mno_ExtrLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp, iLen;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "001060037000", 12))
   //   iRet = 0;
#endif

   // Parse string ignoring quote
   if (cLdrSep == 9)
      iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < MNO_L_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[MNO_L_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   iLen = remChar(apTokens[MNO_L_ASMT], '-');
   //iLen = strlen(apTokens[MNO_L_ASMT]);
   if (iLen < iApnLen)
   {
      memcpy(pLienExtr->acApn, "0000", iApnLen - iLen);
      memcpy(&pLienExtr->acApn[iApnLen - iLen], apTokens[MNO_L_ASMT], iLen);
   } else
      memcpy(pLienExtr->acApn, apTokens[MNO_L_ASMT], iLen);

   // TRA
   lTmp = atol(apTokens[MNO_L_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[MNO_L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[MNO_L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   //long lGrow = atoi(apTokens[MNO_L_CURRENTGROWINGIMPRVALUE]);
   long lGrow = 0;   // Not avail in 2014
   long lFixt = atoi(apTokens[MNO_L_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[MNO_L_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[MNO_L_CURRENTPERSONALPROPMHVALUE]);
   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Prop 8
   lTmp = atoin(apTokens[MNO_L_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Save Exe code
   memcpy(pLienExtr->extra.MB.ExeCode1, apTokens[MNO_L_EXEMPTIONCODE1], strlen(apTokens[MNO_L_EXEMPTIONCODE1]));
   memcpy(pLienExtr->extra.MB.ExeCode2, apTokens[MNO_L_EXEMPTIONCODE2], strlen(apTokens[MNO_L_EXEMPTIONCODE2]));
   memcpy(pLienExtr->extra.MB.ExeCode3, apTokens[MNO_L_EXEMPTIONCODE3], strlen(apTokens[MNO_L_EXEMPTIONCODE3]));

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/*****************************************************************************
 *
 * Extract lien data from yyyy_secured_roll.csv
 * Use this function to extract data for LDR 2015.
 *
 ****************************************************************************/

int Mno_ExtrLienRec2(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp, iLen;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "001060037000", 12))
   //   iRet = 0;
#endif

   // Parse string ignoring quote
   if (cLdrSep == 9)
      iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < MNO_L2_EXEMPTIONAMT1)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[MNO_L2_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   iLen = remChar(apTokens[MNO_L2_ASMT], '-');
   memcpy(pLienExtr->acApn, apTokens[MNO_L2_ASMT], iLen);

   // TRA
   lTmp = atol(apTokens[MNO_L2_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[MNO_L2_CURRENTMARKETLAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[MNO_L2_CURRENTSTRUCTURALIMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lGrow = 0;   // Not avail in 2015
   long lFixt = atoi(apTokens[MNO_L2_CURRENTFIXEDIMPR]);
   long lPP   = atoi(apTokens[MNO_L2_CURRENTPERSONALPROP]);
   long lMH   = atoi(apTokens[MNO_L2_CURRENTPERSONALPROPMH]);
   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Prop 8
   lTmp = atoin(apTokens[MNO_L2_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Save Exe code
   memcpy(pLienExtr->acExCode, apTokens[MNO_L2_EXEMPTIONCODE1], strlen(apTokens[MNO_L2_EXEMPTIONCODE1]));
   memcpy(pLienExtr->extra.MB.ExeCode1, apTokens[MNO_L2_EXEMPTIONCODE1], strlen(apTokens[MNO_L2_EXEMPTIONCODE1]));

   // Exe amount
   lTmp = atol(apTokens[MNO_L2_EXEMPTIONAMT1]);
   if (lTmp > 0)
      memcpy(pLienExtr->acExAmt, apTokens[MNO_L2_EXEMPTIONAMT1], strlen(apTokens[MNO_L2_EXEMPTIONAMT1]));

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/*****************************************************************************
 *
 * Extract lien data from yyyy_secured_601.txt
 * DocNum & DocDate are bad.
 *
 * Use this function to extract data for LDR 2019.
 *
 ****************************************************************************/

int Mno_ExtrLienRec11(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L11_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L11_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   vmemcpy(pLienExtr->acApn, apTokens[L11_ASMT], SIZ_APN_S);

#ifdef _DEBUG
   //if (!memcmp(pLienExtr->acApn, "001020020000", 12))
   //   iRet = 0;
#endif

   // TRA
   lTmp = atol(apTokens[L11_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L11_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L11_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value
   long lGrow = atoi(apTokens[L11_GROWINGVALUE]);   
   long lFixt = atoi(apTokens[L11_FIXTURESVALUE]);
   long lFixtRP = atoi(apTokens[L11_FIXTURESRP]);
   long lPP   = atoi(apTokens[L11_PPVALUE]);
   long lMH   = atoi(apTokens[L11_MHPPVALUE]);
   lTmp = lGrow+lFixt+lFixtRP+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.FixtureRP, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Prop 8
   lTmp = atoin(apTokens[L11_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Exe amount
   long lExe = atol(apTokens[L11_HOX]);
   if (lExe > 0)
   {
      pLienExtr->acHO[0] = '1';
      memcpy(pLienExtr->acExAmt, apTokens[L11_HOX], strlen(apTokens[L11_HOX]));
   } else
      pLienExtr->acHO[0] = '2';

   // Save Exe code
   memcpy(pLienExtr->acExCode, apTokens[L11_OTHEREXEMPTIONCODE], strlen(apTokens[L11_OTHEREXEMPTIONCODE]));
   memcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L11_OTHEREXEMPTIONCODE], strlen(apTokens[L11_OTHEREXEMPTIONCODE]));

   lTmp = atol(apTokens[L11_OTHEREXEMPTION]);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%d", lTmp+lExe);
      memcpy(pLienExtr->acExAmt, acTmp, iRet);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int Mno_ExtrLienRec13(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L13_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L13_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   vmemcpy(pLienExtr->acApn, apTokens[L13_ASMT], SIZ_APN_S);

#ifdef _DEBUG
   //if (!memcmp(pLienExtr->acApn, "001020020000", 12))
   //   iRet = 0;
#endif

   // TRA
   lTmp = atol(apTokens[L13_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[L13_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[L13_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value
   long lGrow = atol(apTokens[L13_GROWINGVALUE]);   
   long lFixt = atol(apTokens[L13_FIXTURESVALUE]);
   long lFixtRP = atol(apTokens[L13_FIXTURESRP]);
   long lPP   = atol(apTokens[L13_PPVALUE]);
   long lMH   = atol(apTokens[L13_MHPPVALUE]);
   lTmp = lGrow+lFixt+lFixtRP+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.FixtureRP, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Prop 8
   lTmp = atol(apTokens[L13_TAXABILITYFULL]);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Exe amount
   long lExe = atol(apTokens[L13_HOX]);
   if (lExe > 0)
   {
      pLienExtr->acHO[0] = '1';
      memcpy(pLienExtr->acExCode, "E01", 3);
      memcpy(pLienExtr->acExAmt, apTokens[L13_HOX], strlen(apTokens[L13_HOX]));
   } else
      pLienExtr->acHO[0] = '2';

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int Mno_ExtrLienRec16(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L16_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L16_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   vmemcpy(pLienExtr->acApn, apTokens[L16_ASMT], SIZ_APN_S);

#ifdef _DEBUG
   //if (!memcmp(pLienExtr->acApn, "001020020000", 12))
   //   iRet = 0;
#endif

   // TRA
   lTmp = atol(apTokens[L16_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[L16_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[L16_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value
   long lGrow = atol(apTokens[L16_GROWINGVALUE]);   
   long lFixt = atol(apTokens[L16_FIXTURESVALUE]);
   long lFixtRP = atol(apTokens[L16_FIXTURESRP]);
   long lPP   = atol(apTokens[L16_PPVALUE]);
   long lMH   = atol(apTokens[L16_MHPPVALUE]);
   lTmp = lGrow+lFixt+lFixtRP+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.FixtureRP, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Prop 8
   lTmp = atol(apTokens[L16_TAXABILITYFULL]);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Exe amount
   long lExe = atol(apTokens[L16_HOX]);
   if (lExe > 0)
   {
      pLienExtr->acHO[0] = '1';
      memcpy(pLienExtr->acExCode, "E01", 3);
      memcpy(pLienExtr->acExAmt, apTokens[L16_HOX], strlen(apTokens[L16_HOX]));
   } else
      pLienExtr->acHO[0] = '2';

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int Mno_ExtrLienRec18(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L18_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L18_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   vmemcpy(pLienExtr->acApn, apTokens[L18_ASMT], SIZ_APN_S);

#ifdef _DEBUG
   //if (!memcmp(pLienExtr->acApn, "001020020000", 12))
   //   iRet = 0;
#endif

   // TRA
   lTmp = atol(apTokens[L18_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[L18_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[L18_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value
   long lFixt = atol(apTokens[L18_FIXTURESVALUE]);
   long lFixtRP = atol(apTokens[L18_FIXTURESRP]);
   long lPP   = atol(apTokens[L18_PPVALUE]);
   long lMH   = atol(apTokens[L18_MHPPVALUE]);
   lTmp = lFixt+lFixtRP+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.FixtureRP, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Prop 8
   lTmp = atol(apTokens[L18_TAXABILITY]);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Exe amount
   long lExe = atol(apTokens[L18_HOX]);
   if (lExe > 0)
   {
      pLienExtr->acHO[0] = '1';
      memcpy(pLienExtr->acExCode, "E01", 3);
      memcpy(pLienExtr->acExAmt, apTokens[L18_HOX], strlen(apTokens[L18_HOX]));
   } else
      pLienExtr->acHO[0] = '2';

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int Mno_ExtrLienRec19(char *pOutbuf, char *pRollRec)
{
   int      iRet;
   ULONG    lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L19_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L19_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   vmemcpy(pLienExtr->acApn, apTokens[L19_ASMT], SIZ_APN_S);

#ifdef _DEBUG
   //if (!memcmp(pLienExtr->acApn, "001020020000", 12))
   //   iRet = 0;
#endif

   // TRA
   lTmp = atol(apTokens[L19_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[L19_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[L19_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value
   long lFixt = atol(apTokens[L19_FIXTURESVALUE]);
   long lFixtRP = atol(apTokens[L19_FIXTURESRP]);
   long lPP   = atol(apTokens[L19_PPVALUE]);
   long lMH   = atol(apTokens[L19_MHPPVALUE]);
   long lGrow = atol(apTokens[L19_GROWINGVALUE]);
   lTmp = lFixt+lFixtRP+lPP+lMH+lGrow;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.FixtureRP, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Tax code
   vmemcpy(pLienExtr->acTaxCode, apTokens[L19_TAXABILITY], SIZ_LIEN_TAXCODE);
   lTmp = atol(apTokens[L19_TAXABILITY]);
   // Prop 8
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Exe amount
   long lExe = atol(apTokens[L19_HOX]);
   if (lExe > 0)
   {
      pLienExtr->acHO[0] = '1';
      memcpy(pLienExtr->acExCode, "E01", 3);
      memcpy(pLienExtr->acExAmt, apTokens[L19_HOX], strlen(apTokens[L19_HOX]));
   } else
      pLienExtr->acHO[0] = '2';

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int Mno_ExtrLienRec20(char *pOutbuf, char *pRollRec)
{
   int      iRet;
   ULONG    lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L20_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L20_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   vmemcpy(pLienExtr->acApn, apTokens[L20_ASMT], SIZ_APN_S);

#ifdef _DEBUG
   //if (!memcmp(pLienExtr->acApn, "001020020000", 12))
   //   iRet = 0;
#endif

   // TRA
   lTmp = atol(apTokens[L20_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[L20_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[L20_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value
   long lFixt = atol(apTokens[L20_FIXTURESVALUE]);
   long lFixtRP = atol(apTokens[L20_FIXTURESRP]);
   long lPP   = atol(apTokens[L20_PPVALUE]);
   long lMH   = atol(apTokens[L20_MHPPVALUE]);
   long lGrow = atol(apTokens[L20_GROWINGVALUE]);
   lTmp = lFixt+lFixtRP+lPP+lMH+lGrow;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.FixtureRP, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Tax code
   vmemcpy(pLienExtr->acTaxCode, apTokens[L20_TAXABILITY], SIZ_LIEN_TAXCODE);
   lTmp = atol(apTokens[L20_TAXABILITY]);
   // Prop 8
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Exe amount
   long lExe1 = atol(apTokens[L20_HOX]);
   long lExe2 = atol(apTokens[L20_OTHEREXEMPTION]);
   if (lExe1 > 0)
   {
      pLienExtr->acHO[0] = '1';
      memcpy(pLienExtr->extra.MB.ExeCode1, "E01", 3);
   } else
   {
      pLienExtr->acHO[0] = '2';
      if (*apTokens[L20_OTHEREXEMPTIONCODE] > ' ')
         vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L20_OTHEREXEMPTIONCODE], SIZ_LIEN_EXECODEX);
   }

   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%u", lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, iRet);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int Mno_ExtrLien(LPCSTR pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg0("Extract LDR roll for %s", pCnty);

   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acRollFile);
      return -2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Replace null char with space
      //iRet = replChar(acRec, 0, ' ', 0);

      // Create new record
      if (lLienYear >= 2024)
         iRet = Mno_ExtrLienRec20(acBuf, acRec);
      else if (lLienYear == 2023)
         iRet = Mno_ExtrLienRec19(acBuf, acRec);
      else if (lLienYear == 2022)
         iRet = Mno_ExtrLienRec18(acBuf, acRec);
      else if (lLienYear == 2021)
         iRet = Mno_ExtrLienRec16(acBuf, acRec);
      else if (lLienYear == 2020)
         iRet = Mno_ExtrLienRec13(acBuf, acRec);
      else if (lLienYear == 2019)
         iRet = Mno_ExtrLienRec11(acBuf, acRec);
      else if (lLienYear >= 2015)
         iRet = Mno_ExtrLienRec2(acBuf, acRec);
      else
         iRet = Mno_ExtrLienRec(acBuf, acRec);

      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/*********************************** Mno_FixR01 ********************************
 *
 * Input/Output:  R01 file
 *
 ******************************************************************************/

int Mno_FixR01(char *pCnty, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acTmp[32], *pTmp;
   char     acOutFile[_MAX_PATH], acRawFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet;
   int      iRet;
   long     lCnt=0;

   // Setup file names
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "TMP");

   if (_access(acRawFile, 0))
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");

   LogMsg("Fix %s file for %s.  Correcting APN", acRawFile, pCnty);

   // Open R01 file
   LogMsg("Open R01 file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   bRet = true;
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      // Format APN
      pTmp = &acBuf[0];
      iRet = sprintf(acTmp, "0%.2s%.3s0%.2s%.3s ", pTmp, pTmp+2, pTmp+5, pTmp+7);
      memcpy(&acBuf[OFF_APN_S], acTmp, iRet);

      iRet = sprintf(acTmp, "%.3s-%.3s-%.3s-%.3s  ", pTmp, pTmp+3, pTmp+6, pTmp+9);
      memcpy(&acBuf[OFF_APN_D], acTmp, iRet);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }
   printf("\n");

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   sprintf(acTmp, acRawTmpl, pCnty, pCnty, "T01");
   if (!_access(acTmp, 0))
      remove(acTmp);
   LogMsg("Rename %s to %s", acRawFile, acTmp);
   iRet = rename(acRawFile, acTmp);
   LogMsg("Rename %s to %s", acOutFile, acRawFile);
   iRet = rename(acOutFile, acRawFile);

   LogMsgD("Total output records:       %u", lCnt);

   return iRet;
}

/*********************************** Mno_FixR01 ********************************
 *
 * Input/Output:  R01 file
 *
 ******************************************************************************/

extern   COUNTY_INFO myCounty;
int Mno_ApnXlat(char *pCnty, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acApnRec[256], acTmp[32], *pTmp;
   char     acOutFile[_MAX_PATH], acRawFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   int      iRet, iTmp;
   long     lCnt=0;

   // Setup file names
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "T01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "TMP");

   LogMsg("Fix %s file for %s.  Correcting APN", acRawFile, pCnty);

   // Open R01 file
   LogMsg("Open R01 file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Open Apn file
   LogMsg("Open Apn file %s", acApnFile);
   FILE *fdApn = fopen(acApnFile, "r");
   if (fdApn == NULL)
   {
      LogMsg("***** Error opening Apn file: %s\n", acApnFile);
      return 2;
   }
   // Get 1st rec
   pTmp = fgets((char *)&acApnRec[0], 256, fdApn);

   // Copy skip record
   bRet = true;
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   bEof = false;
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextApnRec:
      iTmp = memcmp(acBuf, (char *)&acApnRec[2], 13);
      if (!iTmp)
      {
         // Save previous APN
         memcpy(&acBuf[OFF_PREV_APN], acBuf, 13);
         memcpy(acBuf, (char *)&acApnRec[32], 12);
         acBuf[12] = ' ';
         pTmp = &acBuf[0];
         iRet = sprintf(acTmp, "%.3s-%.3s-%.3s-%.3s  ", pTmp, pTmp+3, pTmp+6, pTmp+9);
         memcpy(&acBuf[OFF_APN_D], acTmp, iRet);

         // Read next 
         pTmp = fgets(acApnRec, 256, fdApn);

         if (!pTmp)
            bEof = true;
      } else if (iTmp > 0)
      {
         // Get next roll record
         pTmp = fgets(acApnRec, 256, fdApn);

         if (!pTmp)
            bEof = true;
         else
            goto NextApnRec;
      } else
      {
         pTmp = &acBuf[0];
         memcpy(&acBuf[OFF_PREV_APN], acBuf, 13);

         // Format APN
         iRet = sprintf(acTmp, "0%.2s%.3s0%.2s%.3s ", pTmp, pTmp+2, pTmp+5, pTmp+7);
         memcpy(&acBuf[OFF_APN_S], acTmp, iRet);

         iRet = sprintf(acTmp, "%.3s-%.3s-%.3s-%.3s  ", pTmp, pTmp+3, pTmp+6, pTmp+9);
         memcpy(&acBuf[OFF_APN_D], acTmp, iRet);
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Do the rest of the file
   while (bRet && iRecLen == nBytesRead)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      pTmp = &acBuf[0];
      iRet = sprintf(acTmp, "0%.2s%.3s0%.2s%.3s ", pTmp, pTmp+2, pTmp+5, pTmp+7);
      memcpy(&acBuf[OFF_APN_S], acTmp, iRet);

      iRet = sprintf(acTmp, "%.3s-%.3s-%.3s-%.3s  ", pTmp, pTmp+3, pTmp+6, pTmp+9);
      memcpy(&acBuf[OFF_APN_D], acTmp, iRet);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   printf("\n");

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);
   if (fdApn)
      fclose(fdApn);

   // Resort output file
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   lCnt = sortFile(acOutFile, acRawFile, "S(1,12,C,A) F(FIX,1900) BYPASS(1900,R)");

   LogMsgD("Total output records:       %u", lCnt);

   return iRet;
}

/******************************* Mno_LoadGrGrCsv ****************************
 *
 * If successful, return 0.  Otherwise error.
 *
 ****************************************************************************/

int Mno_CreateGrGrRec(char *pRec, FILE *fdGrOut)
{
   static   char sDocNum[16];
   static   int  iApnCnt = 0;
   char     *pTmp, acTmp[1024], acDate[16], acOutbuf[1024], *apItems[MNO_GR_FLDCNT+2];
   int      iTmp, iRet, lSalePrice;

   GRGR_DOC *pGrGrRec = (GRGR_DOC *)&acOutbuf[0];

   iTmp = ParseStringNQ1(pRec, ',', MNO_GR_FLDCNT+2, apItems);
   if (iTmp >= MNO_GR_FLDCNT)
   {
      if (*apItems[MNO_GR_ASMT] < '0')
         return 0;

      memset(acOutbuf, ' ', sizeof(GRGR_DOC));

      // APN
      memcpy(pGrGrRec->APN, apItems[MNO_GR_ASMT], strlen(apItems[MNO_GR_ASMT]));

      // Est. sale price
      lSalePrice = atol(apItems[MNO_GR_SALEPRICE]);
      if (lSalePrice > 0)
      {
         iTmp = sprintf(acTmp, "%*d", SIZ_GR_SALE, lSalePrice);
         memcpy(pGrGrRec->SalePrice, acTmp, iTmp);
      }

      // Doc Tax
      double dTax = atof(apItems[MNO_GR_DOCTRANTAX])+0.0001;
      if (dTax > 0.1)
      {
         iTmp = sprintf(acTmp, "%.2f", dTax);
         memcpy(pGrGrRec->DocTax, acTmp, iTmp);
      }

      // DocType
      memcpy(pGrGrRec->DocTitle, apItems[MNO_GR_DOCCODE], strlen(apItems[MNO_GR_DOCCODE]));
      int iTmp = findDocType(apItems[MNO_GR_DOCCODE], &MNO_DocCode[0]);
      if (iTmp >= 0)
      {
         memcpy(pGrGrRec->DocType, MNO_DocCode[iTmp].pCode, MNO_DocCode[iTmp].iCodeLen);
         pGrGrRec->NoneSale = MNO_DocCode[iTmp].flag;
      } else if (pGrGrRec->DocTitle[0] > ' ')
      {
         LogMsg("*** MNO GRGR - Unknown DocCode: %s [%s]", apItems[MNO_GR_DOCCODE], apItems[MNO_GR_ASMT]);
         pGrGrRec->NoneSale = 'Y';
      }

      // DocDate, DocNum
      if (*apItems[MNO_GR_DOCNUM] > ' ')
      {
         pTmp = dateConversion(apItems[MNO_GR_EVENTDATE], acDate, MM_DD_YYYY_1);
         if (pTmp)
         {
            memcpy(pGrGrRec->DocDate, pTmp, 8);
            iTmp = atol(pTmp);
            if (iTmp > lLastRecDate)
               lLastRecDate = iTmp;
         } else
            LogMsg("*** Invalid RecDate: %s - %s", apItems[MNO_GR_EVENTDATE], apItems[MNO_GR_ASMT]);

         iTmp = strlen(apItems[MNO_GR_DOCNUM]);
         if (iTmp == 12)
         {
            memcpy(pGrGrRec->DocNum, apItems[MNO_GR_DOCNUM], iTmp);    
            if (!iApnCnt)
            {
               iApnCnt = 1;
               strcpy(sDocNum, apItems[MNO_GR_DOCNUM]);
            } else if (!memcmp(sDocNum, apItems[MNO_GR_DOCNUM], 12))
            {
               iApnCnt++;
               pGrGrRec->MultiApn = 'Y';
            } else
            {
               iApnCnt = 1;
               strcpy(sDocNum, apItems[MNO_GR_DOCNUM]);
            }
         } else if (iTmp == 10)
         {
            iApnCnt = 1;
            iTmp = sprintf(sDocNum, "%.4sR0%.6s", apItems[MNO_GR_DOCNUM], apItems[MNO_GR_DOCNUM]+4);            
            memcpy(pGrGrRec->DocNum, sDocNum, iTmp);    
         } else if (iTmp == 11 && *(apItems[MNO_GR_DOCNUM]+4) == 'R')
         {
            iApnCnt = 1;
            iTmp = sprintf(sDocNum, "%.5s0%.6s", apItems[MNO_GR_DOCNUM], apItems[MNO_GR_DOCNUM]+5);            
            memcpy(pGrGrRec->DocNum, sDocNum, iTmp);    
         } else if (iTmp == 9 && *(apItems[MNO_GR_DOCNUM]+4) == 'R')
         {
            iApnCnt = 1;
            iTmp = sprintf(sDocNum, "%.5s000%.4s", apItems[MNO_GR_DOCNUM], apItems[MNO_GR_DOCNUM]+5);            
            memcpy(pGrGrRec->DocNum, sDocNum, iTmp);    
         } else
            LogMsg("*** Bad DocNum: %s [%s]", apItems[MNO_GR_DOCNUM], apItems[MNO_GR_ASMT]);
      }

      // Grantor
      pTmp = apItems[MNO_GR_TRANSFEREE];
      memcpy(pGrGrRec->Grantor[0], apItems[MNO_GR_TRANSFEROR], strlen(apItems[MNO_GR_TRANSFEROR]));

      // Grantee
      memcpy(pGrGrRec->Grantee[0], apItems[MNO_GR_TRANSFEREE], strlen(apItems[MNO_GR_TRANSFEREE]));

      pGrGrRec->CRLF[0] = '\n';
      pGrGrRec->CRLF[1] = '\0';
      fputs(acOutbuf, fdGrOut);
      iRet = 0;
   } else
      iRet = -1;

   return iRet;
}

/***************************** Mno_LoadGrGrCsv ********************************
 *
 * Return number of records output
 *
 ******************************************************************************/

int Mno_LoadGrGrCsv(char *pInfile, char *pOutfile)
{
   char     *pTmp, acRec[4096], acOutbuf[1024], acTmpFile[_MAX_PATH];
   int      iRet, iCnt, lCnt;

   FILE     *fdIn, *fdOut;
   GRGR_DOC *pGrGrRec = (GRGR_DOC *)&acOutbuf[0];

   LogMsg("Process GrGr file %s", pInfile);

   // Sort input file by DocNum
   sprintf(acTmpFile, "%s\\MNO\\Daily_GrGr.srt", acTmpPath);
   iCnt = sortFile(pInfile, acTmpFile, "S(#3,C,A) ");
   if (iCnt < 1)
      return -1;

   LogMsg("Open GrGr input file %s", acTmpFile);
   if (!(fdIn = fopen(acTmpFile, "r")))
   {
      LogMsg("*** Error opening file: %s", acTmpFile);
      return -1;
   }

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", pOutfile);
      return -2;
   }

   // Update GrGr file date
   iRet = getFileDate(pInfile);
   if (iRet > lLastGrGrDate)
      lLastGrGrDate = iRet;

   // Initialize pointers
   iCnt = lCnt = 0;
   while (!feof(fdIn))
   {
      pTmp = fgets(acRec, 4096, fdIn);
      if (!pTmp || *pTmp > '9') break;

      iRet = Mno_CreateGrGrRec(acRec, fdOut);
      if (!iRet)
         iCnt++;

      lCnt++;
      if (!(lCnt % 1000))
         printf("\r%d", lCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Total processed records  : %u", lCnt);
   LogMsg("Total output records     : %u", iCnt);

   return iCnt;
}

/*
int Mno_LoadGrGrCsv_Old(char *pInfile, char *pOutfile)
{
   FILE     *fdIn, *fdOut; 
   char     acBuf[2048], acBuf1[2048], acTmp[256], acTmp2[256], *pRec, *pdest, *pTmp;
   GRGR_DEF myGrGrRec;
   int      iRet, iTmp, result, iCnt=0, ReadGrGr=0;
   long     lSalePrice, lTax;

   LogMsg("Open GrGr input file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening GrGr file %s", pInfile);
      return -1;
   }

   LogMsg("Open GrGr output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");
   if (fdOut == NULL)
   {
      LogMsg("Error creating GrGr output file: %s\n", pOutfile);
      return -2;
   }

   // Update GrGr file date
   iTmp = getFileDate(pInfile);
   if (iTmp > lLastGrGrDate)
      lLastGrGrDate = iTmp;

   memset(acBuf1, 0, 2048);
   pRec = fgets(acBuf, 2048, fdIn);

   while (!feof(fdIn))
   {
      //if (iCnt >= 263184)
      //   iRet = 0;

      // Read a record - in this file, a record may be on more than one line.
      // It's safe to use myGetStrQC() instead of fgets() as normal
      //iRet = myGetStrQC(acBuf, ',', 2048, fdIn);     
      //if (iRet <= 100)
      //   break;
      
      pRec = fgets(acBuf, 2048, fdIn);
      iRet = ParseStringNQ(acBuf, ',', MNO_GR_FLDCNT+2, apTokens);
      if (iRet < MNO_GR_FLDCNT)
      {
         LogMsg0("*** Invalid GrGr rec: %s", acBuf);
         continue;
      }
      memset((void *)&myGrGrRec, ' ', sizeof(GRGR_DEF));

      strcpy(acTmp, apTokens[MNO_GR_APN_NUMBER]);

#ifdef _DEBUG
      //if (!memcmp(acTmp, "24-110-17", 9) )
      //   iTmp = 0;
#endif

      // Check for multiple APN
      if (strstr(_strupr(acTmp), "VOID") != NULL)
         acTmp[0] = 0;

      if (pTmp = isCharIncluded(acTmp, 0))
         *pTmp = 0;

      // Take first APN and ignore the rest
      if (pTmp = strchr(acTmp, '.'))
         *pTmp = 0;
      if (pTmp = strchr(acTmp, ';'))
         *pTmp = 0;
      if (pTmp = strchr(acTmp, ' '))
         *pTmp = 0;
      if (pTmp = strchr(acTmp, ','))
         *pTmp = 0;
      if (pTmp = strchr(acTmp, '/'))
         *pTmp = 0;

      if ((pTmp = strchr(acTmp, '*')) || (pTmp = strchr(acTmp, '+')) || (pTmp = strchr(acTmp, '`')))
         strcpy(pTmp, pTmp+1);

      // 99-999-99-9999-99
      iRet = strlen(acTmp);

#ifdef _DEBUG
      //if (strchr(acTmp, ' '))
      //   pTmp = acTmp;
#endif

      if (iRet > 8)
      {
         if (acTmp[2] == '-')
         {
            if (acTmp[0] == '0' && acTmp[1] == '0')
            {
               strcpy(acTmp2, &acTmp[3]);
               strcpy(acTmp, acTmp2);
            }
            if (iRet == 6)
            {
               if (acTmp[6] != '-')
                  acTmp[0] = 0;
            }
            if (iRet == 10)
            {
               if (acTmp[10] != '-')
                  acTmp[0] = 0;
            }
         } else
         {
            if (acTmp[1] == '-')
            {
               strcpy(acTmp2, "0");
               strcat(acTmp2, acTmp);
               strcpy(acTmp, acTmp2);
            } else
               acTmp[0] = 0;
         }
      }

      // Fill in with trailing zero
      remChar(acTmp, '-');
      strcat(acTmp, "0000000000000");
      acTmp[13] = 0;

      // Drop all records without APN
      if (memcmp(acTmp, "0000000000000", 13))
      {
         memcpy(myGrGrRec.APN, acTmp, 13);
         memcpy(myGrGrRec.SourceTable, apTokens[MNO_GR_SOURCETABLE], strlen(apTokens[MNO_GR_SOURCETABLE]));

         blankRem(apTokens[MNO_GR_GRANTOR]);
         memcpy(myGrGrRec.Grantors[0].Name, apTokens[MNO_GR_GRANTOR], strlen(apTokens[MNO_GR_GRANTOR]));
         //memcpy(myGrGrRec.Grantors[0].NameType, "O", 1);
         blankRem(apTokens[MNO_GR_GRANTEE]);
         memcpy(myGrGrRec.Grantees[0].Name, apTokens[MNO_GR_GRANTEE], strlen(apTokens[MNO_GR_GRANTEE]));
         //memcpy(myGrGrRec.Grantees[0].NameType, "E", 1);

         memcpy(myGrGrRec.DocTitle, apTokens[MNO_GR_INSTRUMENT_TYPE], strlen(apTokens[MNO_GR_INSTRUMENT_TYPE]));

         if (strlen(apTokens[MNO_GR_AW_FILE_DATE]) > 8)
         {
            strcpy(acTmp, apTokens[MNO_GR_AW_FILE_DATE]);
            remChar(acTmp, '-');
            memcpy(myGrGrRec.DocDate, acTmp, 8);
         } else if (strlen(apTokens[MNO_GR_OR_INSTRUMENT_DATE]) > 0)
         {
            strcpy(acTmp, apTokens[MNO_GR_OR_INSTRUMENT_DATE]);
            remChar(acTmp, '-');
            memcpy(myGrGrRec.DocDate, acTmp, 8);
         }

         if (strlen(apTokens[MNO_GR_AW_INSTRUMENT_NUMBER]) > 0)
         {
            remChar(apTokens[MNO_GR_AW_INSTRUMENT_NUMBER], ' ');
            if (strlen(apTokens[MNO_GR_AW_INSTRUMENT_NUMBER]) < 10)
            {
               if (myGrGrRec.DocDate)
               {
                  strncpy(acTmp, myGrGrRec.DocDate,4);
                  strncpy(acTmp+5, "00", 2);
                  strcpy(acTmp+7, apTokens[MNO_GR_AW_INSTRUMENT_NUMBER]);
                  memcpy(myGrGrRec.DocNum, acTmp, 10);
               }
            } else
               memcpy(myGrGrRec.DocNum, apTokens[MNO_GR_AW_INSTRUMENT_NUMBER], strlen(apTokens[MNO_GR_AW_INSTRUMENT_NUMBER]));
         } else if (strlen(apTokens[MNO_GR_OR_INSTRUMENT_NUMBER]) > 0)
         {
            iRet = remChar(apTokens[MNO_GR_OR_INSTRUMENT_NUMBER], ' ');
            if (iRet < 10)
            {
               if (myGrGrRec.DocDate)
               {
                  strncpy(acTmp, myGrGrRec.DocDate,4);
                  strncpy(acTmp+5, "00", 2);
                  strcpy(acTmp+7, apTokens[MNO_GR_OR_INSTRUMENT_NUMBER]);
                  memcpy(myGrGrRec.DocNum, acTmp, 10);
               }
            } else
               memcpy(myGrGrRec.DocNum, apTokens[MNO_GR_OR_INSTRUMENT_NUMBER], 10);
         }

         // AW_DTT -> Tax
         double dTax=0.0;
         if (*apTokens[MNO_GR_AW_DTT] >= ' ')
         {
            strcpy(acTmp, apTokens[MNO_GR_AW_DTT]);
            if (pTmp = strchr(acTmp, '/'))
               *pTmp = 0;
            remChar(acTmp, '$');
            remChar(acTmp, ',');
            dTax = atof(acTmp);
            if (dTax > 0.0)
            {
               lTax = (long)(dTax*100.0);
               sprintf(acTmp, "%*d", SIZ_GR_TAX, lTax);
               memcpy(myGrGrRec.Tax, acTmp, SIZ_GR_TAX);
            }
         }
         // AW_IndicatedPrice -> Saleprice
         if (*apTokens[MNO_GR_AW_INDICATED_PRICE] >= ' ')
         {
            strcpy(acTmp, apTokens[MNO_GR_AW_INDICATED_PRICE]);
            remChar(acTmp, '$');
            remChar(acTmp, ',');
            lSalePrice = atoi(acTmp);
            if (lSalePrice > 0)
            {
               sprintf(acTmp, "%*d", SIZ_GR_SALE, lSalePrice);
               memcpy(myGrGrRec.SalePrice, acTmp, SIZ_GR_SALE);
            } else if (dTax > 0.0)
            {
               lSalePrice = (long)(dTax*SALE_FACTOR);
               sprintf(acTmp, "%*d", SIZ_GR_SALE, lSalePrice);
               memcpy(myGrGrRec.SalePrice, acTmp, SIZ_GR_SALE);
            }
         }

         if (strlen(apTokens[MNO_GR_AW_FILE_DATE]) > 0)
         {
            strcpy(acTmp, apTokens[MNO_GR_AW_FILE_DATE]);
            remChar(acTmp, '-');
            pdest = strchr(acTmp, ' ');
            result = pdest - acTmp;

            memcpy(myGrGrRec.DocDate, acTmp, result);
         }

         iRet = atoin(myGrGrRec.DocDate, 8);
         if (iRet > lLastRecDate && iRet < lToday)
            lLastRecDate = iRet;

         // Subdivision -> Legal/reference data
         if (strlen(apTokens[MNO_GR_AW_SUBDIVISION]) > 0)
            memcpy(myGrGrRec.ReferenceData, apTokens[MNO_GR_AW_SUBDIVISION], strlen(apTokens[MNO_GR_AW_SUBDIVISION]));

         if (strlen(apTokens[MNO_GR_AW_PARCEL_COUNT]) > 0)
         {
            iTmp = atoin(apTokens[MNO_GR_AW_PARCEL_COUNT], MNOSIZ_GR_AW_PARCEL_COUNT);
            if (iTmp > 0)
            {
               sprintf(acTmp, "%*d", SIZ_GR_PRCLCNT, iTmp);
               memcpy(myGrGrRec.ParcelCount, acTmp, SIZ_GR_PRCLCNT);
            }
         }

         sprintf(acTmp, "%d    ", 1);
         memcpy(myGrGrRec.NameCnt, acTmp, SIZ_GR_NAMECNT);

         // Use AW first.  If blank, use OR
         // If InstrumentNumber is < 10 digits, format it to 6 digits (i.e. 003456)
         // then prepend it with Instrument year

         myGrGrRec.CRLF[0] = '\n';
         myGrGrRec.CRLF[1] = '\0';
         iRet = fputs((char *)&myGrGrRec, fdOut);
      }
      memset(acBuf,0,2048);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsgD("\nNumber of GrGr record output:  %d\n", iCnt);

   return iCnt;
}
*/

/**************************** Mno_ExtrSaleMatched ***************************
 *
 * Extract data to GrGr_Exp.dat or GrGr_Exp.sls.  This output is to be merged
 * into R01 file.  Extract only records with ApnMatch=Y.
 * Input: pMode can be "w" or "a+" (overwrite or append)
 * Output: GrGr_Exp.dat or GrGr_Exp.sls in SALE_REC1 format
 * Return 0 if successful, otherwise error
 *
 ****************************************************************************

int Mno_ExtrSaleMatched(char *pGrGrFile, char *pESaleFile, char *pMode, bool bOwner=false)
{
   char     *pTmp, acBuf[2048];
   long     lCnt=0, lDocSkip=0, iTmp;

   CString   sTmp, sApn, sType;
   FILE     *fdGrGr;
   SALE_REC1 SaleRec;
   GRGR_DEF *pGrGr = (GRGR_DEF *)&acBuf[0];

   LogMsg("Extract matched grgr from %s to %s", pGrGrFile, pESaleFile);

   // Open output file
   if (!(fdSale = fopen(pESaleFile, pMode)))
   {
      LogMsg("***** Error creating %s file", pESaleFile);
      return -1;
   }

   // Open input file
   if (!(fdGrGr = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error creating %s file", pGrGrFile);
      return -1;
   }


   // Loop through
   while (!feof(fdGrGr))
   {
      // Get input rec
      pTmp = fgets(acBuf, 2048, fdGrGr);
      if (!pTmp)
         break;         // EOF

      // Drop all record without APN matched
      if (pGrGr->APN_Matched != 'Y')
         continue;

      // Drop records without sale price
      // Ignore this check since new file has no sale price 02/04/2011
      //lPrice = atoin(pGrGr->SalePrice, SIZ_GR_SALE);
      //if (lPrice < 1000)
      //   continue;

      // Take DEED or TRUSTEE DEED with sale price only
      iTmp = findDocType(pGrGr->DocTitle, (IDX_TBL5 *)&MNO_DocTitle[0]);
      if (iTmp >= 0)
      {
         memcpy(SaleRec.acDocType, MNO_DocTitle[iTmp].pCode, MNO_DocTitle[iTmp].iCodeLen);
         SaleRec.NoneSale_Flg = MNO_DocTitle[iTmp].flag;
      } else
      {
         if (bDebug)
            LogMsg("** Skip doc type: %.25s APN=%.14s", pGrGr->DocTitle, pGrGr->APN);
         lDocSkip++;
         continue;
      }
      
      //if (memcmp(pGrGr->DocTitle, "DEED  ", 6) && memcmp(pGrGr->DocTitle, "TRUSTEE", 7)
      //    && memcmp(pGrGr->DocTitle, "BILL OF SALE", 12))
     // {
     //    if (bDebug)
     //       LogMsg("** Skip doc type: %.25s APN=%.14s", pGrGr->DocTitle, pGrGr->APN);
     //    lDocSkip++;
     //    continue;
     // }

      memset((void *)&SaleRec, 32, sizeof(SALE_REC1));

      memcpy(SaleRec.acApn, pGrGr->APN, SALE_SIZ_APN);
      memcpy(SaleRec.acDocDate, pGrGr->DocDate, SALE_SIZ_DOCDATE);
      memcpy(SaleRec.acDocNum, pGrGr->DocNum, SALE_SIZ_DOCNUM);
      memcpy(SaleRec.acStampAmt, pGrGr->Tax, SIZ_GR_TAX);
      memcpy(SaleRec.acSalePrice, pGrGr->SalePrice, SIZ_GR_SALE);

      // Get grantors grantees
      iTmp = blankRem(pGrGr->Grantors[0].Name, SALE_SIZ_SELLER);
      memcpy(SaleRec.acSeller, pGrGr->Grantors[0].Name, iTmp);

      // Copy Owner name
      if (bOwner)
      {
         memcpy(SaleRec.acName1, pGrGr->Grantees[0].Name, SALE_SIZ_NAME);
         memcpy(SaleRec.acName2, pGrGr->Grantees[1].Name, SALE_SIZ_NAME);
      }

      SaleRec.CRLF[0] = '\n';
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec, fdSale);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdGrGr);
   fclose(fdSale);

   LogMsg("Total output matched records: %ld", lCnt);
   LogMsg("      skipped records       : %ld", lDocSkip);
   return 0;
}

/***************************** Mno_LoadGrGr ***********************************
 *
 * Return 0 if successful
 *
 ******************************************************************************/

int Mno_LoadGrGr(char *pGrGr)
{
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acTmp[256];
   int      iCnt, iRet;

   // Create Output file - Mno_GrGr.dat
   sprintf(acGrGrOut, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   lLastRecDate = 0;

   // Parse input file
   iCnt = Mno_LoadGrGrCsv(pGrGr, acGrGrOut);
   if (iCnt < 0)
      LogMsg("*** Bad input file: %s", pGrGr);

   // Sort output
   if (iCnt > 0)
   {
      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, RecDate asc, DocNum asc, Source (AW/OR/MG)
      sprintf(acTmp,"S(17,12,C,A, 1,12,C,A) F(TXT) DUPO(B2048, 1, 130)");
      sprintf(acGrGrIn, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

      // Sort GrGr_Exp.tmp to GrGr_Exp.srt
      iCnt = sortFile(acGrGrOut, acGrGrIn, acTmp);
      if (iCnt > 0)
         iRet = 0;
      else
         iRet = 1;
   } else
      iRet = 1;

   LogMsgD("\nTotal output records after dedup: %u", iCnt);
   LogMsg("              Last recording date: %d.", lLastRecDate);

   return iRet;
}

/****************************************************************************
 *
 * County sends us whole file daily, but we need to append to cummulative file
 * to keep sale price from older record.  New one doesn't include stamp amount.
 * We set the date limit every year to output only the records that we might need.
 * Set back to beginning of the year every LDR or set it back to 19600101 to  
 * load the whole file. 
 *
 * Return 0 if successful, else error code
 *
 ****************************************************************************/

int Mno_ConvertGrGr(char *pMdbFile)
{
   hlAdo    hSale;
   hlAdoRs  rsGrGr;
   char     acTmp[_MAX_PATH], acSaleTbl[64], acTmpFile[_MAX_PATH], *pTmp;
   char     acMdbProvider[_MAX_PATH], sDateLimit[12];
   bool     bRet;
   int      iRet, lCnt=0;
   GRGR_DEF myGrGrRec;

   LogMsg("Loading GrGr ...");
   iRet = GetIniString(myCounty.acCntyCode, "GrGrFrom", "", sDateLimit, _MAX_PATH, acIniFile);
   if (iRet != 8)
      strcpy(sDateLimit, "20091208");

   GetIniString("Database", "MdbProvider", "", acMdbProvider, _MAX_PATH, acIniFile);
   GetIniString("MNO", "GrGrTbl", "", acSaleTbl, _MAX_PATH, acIniFile);

   // Prepare output file
   sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Tmp");
   if (!(fdSale = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating Sale file: %s", acTmpFile);
      return -1;
   }

   // Open file
   LogMsg("Open Sale file %s", pMdbFile);
   try
   {
      strcpy(acTmp, acMdbProvider);
      strcat(acTmp, pMdbFile);

      bRet = hSale.Connect(acTmp);
      if (bRet)
      {
         sprintf(acTmp, "SELECT * FROM %s WHERE (APN_Number IS NOT NULL) AND (Instrument_Type='DEED') ", acSaleTbl);
         LogMsg("%s", acTmp);
         rsGrGr.Open(hSale, acTmp);
         LogMsg("Successfully open record set");
      }
   } AdoCatch(e)
   {
      LogMsg("***** Error opening %s (%s)", pMdbFile, ComError(e));
      return -1;
   }

   CString  sApn, sTmp;
   char     acFmtApn[64];

   // Start processing
   while (rsGrGr.next())
   {
      // Init output buffer
      memset(&myGrGrRec, ' ', sizeof(GRGR_DEF));

      // Get sale date & format it to yyyymmdd
      sTmp = rsGrGr.GetItem("Instrument_Date");
      if (pTmp = dateConversion(sTmp.GetBuffer(0), acTmp, MM_DD_YYYY_1))
      {
         if (strlen(pTmp) != 8)
         {
            LogMsg("*** Invalid date: %s on APN %s", sTmp, sApn);
            continue;
         } 
         else if (memcmp(pTmp, sDateLimit, 8) < 0)
            continue;
         else
            memcpy(myGrGrRec.DocDate, acTmp, 8);
      }

      // Source table
      sTmp = rsGrGr.GetItem("SourceTable");
      memcpy(myGrGrRec.SourceTable, sTmp, sTmp.GetLength());

      // Format APN
      sApn = rsGrGr.GetItem("APN_Number");
#ifdef _DEBUG
      //if (sApn == "002-410-008-000")
      //   iRet = 0;
#endif

      Mno_FormatApn(sApn.GetBuffer(0), acFmtApn, &iRet);
      if (!iRet)
         continue;
      if (iRet > 1)
      {
         sprintf(acTmp, "%*d", SIZ_GR_PRCLCNT, iRet);
         memcpy(myGrGrRec.ParcelCount, acTmp, SIZ_GR_PRCLCNT);
      }
      memcpy(myGrGrRec.APN, acFmtApn, strlen(acFmtApn));

      // Save unformatted APN
      iRet = sApn.GetLength();
      if (iRet > SIZ_GR_UNFMTAPN)
         iRet = SIZ_GR_UNFMTAPN;
      memcpy(myGrGrRec.Unformatted_APN, sApn, iRet);

      /*
      // Get sale price
      sTmp = rsGrGr.GetItem("SalesPriceDTT");
      memcpy(myGrGrRec.SalePrice, sTmp, sTmp.GetLength());

      // Doc Tax
      sTmp = rsGrGr.GetItem("DocTranTax");
      memcpy(myGrGrRec.SalePrice, sTmp, sTmp.GetLength());

      // Sale Type
      sTmp = rsGrGr.GetItem("TransferType");
      memcpy(myGrGrRec.SalePrice, sTmp, sTmp.GetLength());

      */
      
      // Doc Num - Reformat DocNum to match with sale file yyyyR1234567
      try
      {
         sTmp = rsGrGr.GetItem("Instrument_Number");
         iRet = sTmp.GetLength();
         if (iRet < 8)
         {
            iRet = atol(sTmp);
            iRet = sprintf(acTmp, "%.4sR%.7d", myGrGrRec.DocDate, iRet);
         } else
         {
            strcpy(acTmp, sTmp.Mid(5));
            iRet = atol(acTmp);
            iRet = sprintf(acTmp, "%.4sR%.7d", sTmp, iRet);
         }
         memcpy(myGrGrRec.DocNum, acTmp, iRet);
      } AdoCatch(e)
      {
         LogMsg("***** Error getting Instrument_Number %s (%s)", sApn, ComError(e));
      }

      // Doc title/Type
      sTmp = rsGrGr.GetItem("Instrument_Type");
      memcpy(myGrGrRec.DocTitle, sTmp, sTmp.GetLength());

      // Grantor
      sTmp = rsGrGr.GetItem("Grantor");
      memcpy(myGrGrRec.Grantors[0].Name, sTmp, sTmp.GetLength());

      // Grantee
      sTmp = rsGrGr.GetItem("Grantee");
      memcpy(myGrGrRec.Grantees[0].Name, sTmp, sTmp.GetLength());

      myGrGrRec.CRLF[0] = '\n';
      myGrGrRec.CRLF[1] = 0;

      // Output to file
      fputs(&myGrGrRec.DocNum[0], fdSale);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   rsGrGr.Close();

   // Sort output
   if (lCnt > 0)
   {
      char acSortCtl[256], acSlsFile[256], acSrtFile[256];

      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, DocNum asc, RecDate asc, Source (AW/OR/MG)
      //sprintf(acTmp,"S(82,10,C,A, 92,10,C,A) F(TXT) DUPO(B%d, 1,101) ", sizeof(GRGR_DEF)+64);
      sprintf(acSortCtl,"S(17,15,C,A,37,8,C,A,1,12,C,D,125,10,C,D,1109,2,C,A) F(TXT) DUPO(B%d,1,44) ", sizeof(GRGR_DEF)+64);
      sprintf(acSrtFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Srt");

      // Sort Mno_GrGr.tmp to Mno_GrGr.srt
      lCnt = sortFile(acTmpFile, acSrtFile, acSortCtl);

      // Translate APN 
      // Input:  acSrtFile & acApnFile
      // Output: acTmpFile (Mno_Grgr.xlt)
      sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "xlt");
      iRet = GrGr_XlatApn(acTmpFile, acApnFile, acSrtFile, 0, 15, 32);

      // Match with roll file - Set APN_Matched & Owner_Matched flag
      iRet = GrGr_MatchRoll(acTmpFile, iApnLen);

      // Update cumulative sale file
      if (lCnt > 0)
      {
         // We always receive whole GRGR file from the county. 
         // So, there is no need to append to Mno_GrGr.sls
         sprintf(acSlsFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
         sprintf(acTmp, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);
         MoveFile(acSlsFile, acTmp);
         MoveFile(acTmpFile, acSlsFile);

         // Extract to GrGr_Exp.dat
         sprintf(acTmpFile, acEGrGrTmpl, myCounty.acCntyCode, "dat");
         //iRet = Mno_ExtrSaleMatched(acSlsFile, acTmpFile, "w", false);
         lCnt = GrGr_ExtrSaleMatched(acSlsFile, acTmpFile, (IDX_TBL5 *)&MNO_DocTitle[0]);
         if (lCnt > 0)
            iRet = 0;
         else
            iRet = 1;
      } else
         iRet = 1;
   } else
      iRet = 1;

   LogMsgD("\nTotal output records after dedup: %u", lCnt);

   return iRet;
}

/****************************************************************************
 *
 * Reformat DOCNUM for prior records and output only DEED.
 *
 * Return 0 if successful, else number of records output
 *
 ****************************************************************************/

int Mno_FixGrGrDoc(char *pInfile, char *pOutfile)
{
   char     acTmp[256], acOutbuf[2048], *pRec;
   long     lCnt=0, lOut=0, iRet;
   FILE     *fdIn, *fdOut;

   GRGR_DEF *pOutRec = (GRGR_DEF *)&acOutbuf[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** ConvertSaleData(): Missing input file: %s", pInfile);
      return -1;
   }

   LogMsg("Convert %s to %s.", pInfile, pOutfile);

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   LogMsg("Create output sale file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", pOutfile);
      return -3;
   }

   // Convert loop
   while (!feof(fdIn))
   {
      if (!(pRec = fgets(acOutbuf, 2048, fdIn)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0010100230", 10))
      //   iRet = 0;
#endif
      // Keep only DEED, TRUSTEE DEED, and BILL OF SALE 
      if (memcmp(pOutRec->DocTitle, "DEED  ", 6) && memcmp(pOutRec->DocTitle, "TRUSTEE", 7)
          && memcmp(pOutRec->DocTitle, "BILL OF SALE", 12))
      {
         lCnt++;
         continue;
      }

      // Reformat sale
      if (pOutRec->DocNum[4] != 'R')
      {
         iRet = atoin(&pOutRec->DocNum[4], 6);
         sprintf(acTmp, "%.4sR%.7d", pOutRec->DocNum, iRet);
         memcpy(pOutRec->DocNum, acTmp, 12);
      }

      // Write to output file
      fputs(acOutbuf, fdOut);
      lOut++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   return 0;
}

/**************************** Mno_ConvStdChar ********************************
 *
 * Copy from MergeHum.cpp to convert new char file. 11/02/2015
 * Other counties has similar format: MER, YOL, HUM, MAD, SBT
 *
 *****************************************************************************/

int Mno_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   long     lGarSqft;

   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_char.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort char file %s to %s", pInfile, acTmpFile);
   // FeeParcel and Asmt are the same.  We can sort on either one.
   iRet = sortFile(pInfile, acTmpFile, "S(#1,C,A) DEL(124) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
   if (iRet < 500)
   {
      LogMsg("***** Input file is too small.");
      return 1;
   }

   if (!(fdIn = fopen(acTmpFile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Start loop
   while (!feof(fdIn))
   {
      // Get next record
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec || acBuf[0] > '9')
         break;

      replStrAll(acBuf, "|NULL", "|");
      iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < MNO_CHAR_HASWELL)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[MNO_CHAR_ASMT], strlen(apTokens[MNO_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[MNO_CHAR_FEEPARCEL], strlen(apTokens[MNO_CHAR_FEEPARCEL]));

      // Format APN
      if (*apTokens[MNO_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[MNO_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         if (bDebug)
            LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[MNO_CHAR_FEEPARCEL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[MNO_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[MNO_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - not avail
      iTmp = blankRem(apTokens[MNO_CHAR_POOLSPA]);
      if (iTmp > 1)
      {
         pRec = findXlatCode(apTokens[MNO_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass 
      iTmp = remChar(apTokens[MNO_CHAR_QUALITYCLASS], ' ');    // Remove all blanks before process
      pRec = _strupr(apTokens[MNO_CHAR_QUALITYCLASS]);
      if (*pRec == 'X') pRec++;

      vmemcpy(myCharRec.QualityClass, pRec, SIZ_CHAR_QCLS);

      if (*pRec > '0' && *pRec <= 'Z')
      {
         acCode[0] = ' ';
         strcpy(acTmp, pRec);
         if (isalpha(acTmp[0])) 
         {
            myCharRec.BldgClass = acTmp[0];

            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            else if (isdigit(acTmp[2]))
               iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
            else if (!_memicmp(acTmp, "ST", 2))
               memcpy(myCharRec.QualityClass, "ST", 2);     // Steel
         } else if (isdigit(acTmp[0]))
            iRet = Quality2Code(acTmp, acCode, NULL);
         else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[MNO_CHAR_QUALITYCLASS], apTokens[MNO_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (*pRec > ' ' && *pRec != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[MNO_CHAR_QUALITYCLASS], apTokens[MNO_CHAR_ASMT]);

      // YrBlt
      int iYrBlt = atoi(apTokens[MNO_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[MNO_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[MNO_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[MNO_CHAR_UNITSCNT]);
      //if (iTmp > 0 && iBldgSize > iTmp*100)
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 
      //else if (iTmp > 1)
      //   LogMsg("*** Questionable UnitsCnt: %d, BldgSqft=%d, Apn=%s", iTmp, iBldgSize, apTokens[MNO_CHAR_ASMT]);

      // Stories/NumFloors
      iTmp = atoi(apTokens[MNO_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 99)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[MNO_CHAR_ATTACHGARAGESF]);
      lGarSqft = iAttGar;
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[MNO_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         lGarSqft += iAttGar;
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport Sqft
      int iCarport = atoi(apTokens[MNO_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         lGarSqft += iCarport;
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      iTmp = atol(myCharRec.GarSqft);
      if (lGarSqft > iTmp)
      {
         iRet = sprintf(acTmp, "%d", lGarSqft);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
      }

      // Patio SF
      iTmp = atoi(apTokens[MNO_CHAR_PATIOSF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[MNO_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCodeA(apTokens[MNO_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[MNO_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[MNO_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[MNO_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[MNO_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[MNO_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[MNO_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[MNO_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace - 01, 02, 05, 06, 99
      if (*apTokens[MNO_CHAR_FIREPLACE] >= '0' && *apTokens[MNO_CHAR_FIREPLACE] <= '9')
      {
         pRec = findXlatCode(apTokens[MNO_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      } 

      // Haswell - not avail
      blankRem(apTokens[MNO_CHAR_HASWELL]);
      if (*(apTokens[MNO_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "001040017000", 9))
      //   iRet = 0;
#endif

      // Lot Sqft
      if (iFldCnt >= MNO_CHAR_LOTSQFT)
      {
         ULONG    lSqft;
         lSqft = (ULONG)atol(apTokens[MNO_CHAR_LOTSQFT]);
         if (lSqft > 1)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(lSqft*SQFT_MF_1000);
            iTmp = sprintf(acTmp, "%*u", SIZ_CHAR_SQFT, (long)(dTmp+0.5));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum, UnitSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/******************************** Mno_MergeLien3 *****************************
 *
 * For 2016 LDR 2016_Secured_Roll.txt
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mno_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   iRet = remChar(apTokens[L3_ASMT], '-');
   memcpy(pOutbuf, apTokens[L3_ASMT], iRet);

   // Copy ALT_APN
   iRet = remChar(apTokens[L3_FEEPARCEL], '-');
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], iRet);

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "26MNO", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   iTmp = remChar(apTokens[L3_TRA], '-');
   iTmp = atol(apTokens[L3_TRA]);
   sprintf(acTmp, "%.6d", iTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Legal - Stop using legal as of 10/17/2022
   //updateLegal(pOutbuf, _strupr(apTokens[L3_PARCELDESCRIPTION]));

   // UseCode
   iTmp = atol(apTokens[L3_LANDUSE1]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], 3);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   //if (*apTokens[L3_ISAGPRESERVE] == '1')
   //   *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Mno_MergeOwner(pOutbuf, _strupr(apTokens[L3_OWNER]));

   // Situs
   //Mno_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Mno_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*apTokens[L3_CURRENTDOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 

   // Number of parking spaces
   if (*apTokens[L3_GARAGE] == '0' || *apTokens[L3_GARAGE] == 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'H';              // None
   else if (*apTokens[L3_GARAGE] > '0' && *apTokens[L3_GARAGE] <= '9')
   {
      iTmp = atol(apTokens[L3_GARAGE]);
      sprintf(acTmp, "%d", iTmp);
      vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
   }

   // YearBlt
   lTmp = atol(apTokens[L3_YEARBUILT]);
   if (lTmp > 1800 && lTmp < lToyear)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // Garage size
   //dTmp = atof(apTokens[L3_GARAGESIZE]);
   //if (dTmp > 0.0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_GAR_SQFT, dTmp);
   //   memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   //   if (*(pOutbuf+OFF_PARK_TYPE) == ' ' || *(pOutbuf+OFF_PARK_TYPE) == 'H')
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';           // GARAGE/CARPORT
   //}

   // Total rooms
   //iTmp = atol(apTokens[L3_TOTALROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   //}

   // Stories
   //iTmp = atol(apTokens[L3_STORIES]);
   //if (iTmp > 0 && iTmp < 100)
   //{
   //   sprintf(acTmp, "%d.0", iTmp);
   //   vmemcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   //}

   // Units
   //iTmp = atol(apTokens[L3_UNITS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%d", iTmp);
   //   vmemcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   //}

   // Beds
   //iTmp = atol(apTokens[L3_BEDROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BEDS, iTmp);
   //   memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   //}

   // Baths
   //iTmp = atol(apTokens[L3_BATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_F, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   //}

   // HBaths
   //iTmp = atol(apTokens[L3_HALFBATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_H, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   //}

   // Rooms
   //iTmp = atol(apTokens[L3_TOTALROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   //}

   // Heating
   //int iCmp;
   //if (*apTokens[L3_HEATING] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_HEATING], asHeating[iTmp].acSrc, asHeating[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
   //}

   // Cooling
   //if (*apTokens[L3_AC] == 'C')
   //   *(pOutbuf+OFF_AIR_COND) = 'C';
   //else if (*apTokens[L3_AC] > ' ')
   //   LogMsg("*** Unknown A/C: %s", apTokens[L3_AC]);

   // Pool/Spa
   //if (*apTokens[L3_POOLSPA] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_POOLSPA], asPool[iTmp].acSrc, asPool[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_POOL) = asPool[iTmp].acCode[0];
   //}

   // Fire place
   //if (*apTokens[L3_FIREPLACE] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   pTmp = _strupr(apTokens[L3_FIREPLACE]);
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_FIREPLACE], asFirePlace[iTmp].acSrc, asFirePlace[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_FIRE_PL) = asPool[iTmp].acCode[0];
   //}

   // Quality Class
   //acTmp1[0] = 0;
   //if (*apTokens[L3_QUALITYCLASS] > '0' || strlen(apTokens[L3_QUALITYCLASS]) > 1)
   //{
   //   strcpy(acTmp, apTokens[L3_QUALITYCLASS]);
   //   remCharEx(acTmp, " ,'?");
   //   pTmp = _strupr(acTmp);

   //   //memcpy(myCharRec.QualityClass, pTmp, strlen(pTmp));
   //   if (acTmp[0] == 'A' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'A';
   //   else if (acTmp[0] == 'F' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'F';
   //   else if (!memcmp(pTmp, "POOR", 4) || !memcmp(pTmp, "LOW", 3))
   //      acTmp1[0] = 'P';
   //   else if (*pTmp == 'G')
   //      acTmp1[0] = 'G';
   //   else if (isalpha(*pTmp))
   //   {
   //      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
   //      if (isdigit(acTmp[1]))
   //         iRet = Quality2Code(&acTmp[1], acTmp1, NULL);
   //      else if (isdigit(acTmp[2]))
   //         iRet = Quality2Code(&acTmp[2], acTmp1, NULL);
   //      else if (isalpha(acTmp[1]))
   //      {
   //         switch (acTmp[1])
   //         {
   //            case 'L':
   //            case 'P':
   //               acTmp1[0] = 'P';
   //               break;
   //            case 'A':
   //               acTmp1[0] = 'A';
   //               break;
   //            case 'F':
   //               acTmp1[0] = 'F';
   //               break;
   //            case 'G':
   //               acTmp1[0] = 'G';
   //               break;
   //         }
   //      }
   //   } else if (isdigit(*pTmp))
   //   {
   //      iTmp = atol(pTmp);
   //      if (iTmp < 100)
   //         iRet = Quality2Code(pTmp, acTmp1, NULL);
   //   }

   //   if (acTmp1[0] > '0')
   //      *(pOutbuf+OFF_BLDG_QUAL) = acTmp1[0];
   //}

   return 0;
}

/******************************** Mno_MergeLien5 *****************************
 *
 * For 2017 LDR 2017_Secured_601.txt
 * No GrowingValue and values are formatted with dollar sign.
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mno_MergeLien5(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L5_UNITS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L5_ASMT], iRet);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   iRet = remChar(apTokens[L5_ASMT], '-');
   memcpy(pOutbuf, apTokens[L5_ASMT], iRet);

   // Copy ALT_APN
   iRet = remChar(apTokens[L5_FEEPARCEL], '-');
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L5_FEEPARCEL], iRet);

   // Format APN
   iRet = formatApn(apTokens[L5_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L5_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "26MNO", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L5_ASMTSTATUS];

   // TRA
   iTmp = remChar(apTokens[L5_TRA], '-');
   iTmp = atol(apTokens[L5_TRA]);
   sprintf(acTmp, "%.6d", iTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = dollar2Num(apTokens[L5_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[L5_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   long lFixtr = dollar2Num(apTokens[L5_FIXTURESVALUE]);
   long lFixtRP= dollar2Num(apTokens[L5_FIXTURESRP]);
   long lPers  = dollar2Num(apTokens[L5_PPVALUE]);
   long lPP_MH = dollar2Num(apTokens[L5_MHPPVALUE]);
   lTmp = lFixtr+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = dollar2Num(apTokens[L5_HOX]);
   long lExe2 = dollar2Num(apTokens[L5_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L5_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L5_OTHEREXEMPTIONCODE], strlen(apTokens[L5_OTHEREXEMPTIONCODE]));

   // Legal - Stop using legal as of 10/17/2022
   //updateLegal(pOutbuf, _strupr(apTokens[L5_PARCELDESCRIPTION]));

   // UseCode
   iTmp = atol(apTokens[L5_LANDUSE1]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L5_LANDUSE1], 3);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   Mno_MergeOwner(pOutbuf, _strupr(apTokens[L5_OWNER]));

   // Situs
   //Mno_MergeSitus(pOutbuf, apTokens[L5_SITUS1], apTokens[L5_SITUS2]);

   // Mailing
   Mno_MergeMAdr(pOutbuf, apTokens[L5_MAILADDRESS1], apTokens[L5_MAILADDRESS2], apTokens[L5_MAILADDRESS3], apTokens[L5_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L5_TAXABILITYFULL], true, true);

   // Number of parking spaces
   if (*apTokens[L5_GARAGE] == '0' || *apTokens[L5_GARAGE] == 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'H';              // None
   else if (*apTokens[L5_GARAGE] > '0' && *apTokens[L5_GARAGE] <= '9')
   {
      iTmp = atol(apTokens[L5_GARAGE]);
      sprintf(acTmp, "%d", iTmp);
      if (iTmp > 99)
         LogMsg("*** Bad parking spaces: %d, APN=%s", iTmp, apTokens[L5_ASMT]);
      else
      {
         vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
         *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
      }
   }

   // YearBlt
   lTmp = atol(apTokens[L5_YEARBUILT]);
   if (lTmp > 1800 && lTmp < lToyear)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // Acres
   dTmp = atof(apTokens[L5_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Lot size
   lTmp = atol(apTokens[L5_LANDSIZE]);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      if (!dTmp)
      {
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(lTmp*SQFT_MF_1000));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   } else if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Garage size
   //dTmp = atof(apTokens[L5_GARAGESIZE]);
   //if (dTmp > 0.0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_GAR_SQFT, dTmp);
   //   memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   //   if (*(pOutbuf+OFF_PARK_TYPE) == ' ' || *(pOutbuf+OFF_PARK_TYPE) == 'H')
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';           // GARAGE/CARPORT
   //}

   // Total rooms
   //iTmp = atol(apTokens[L5_TOTALROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   //}

   // Stories
   //iTmp = atol(apTokens[L5_STORIES]);
   //if (iTmp > 0 && iTmp < 100)
   //{
   //   sprintf(acTmp, "%d.0", iTmp);
   //   vmemcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   //}

   // Units
   //iTmp = atol(apTokens[L5_UNITS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%d", iTmp);
   //   vmemcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   //}

   // Beds
   //iTmp = atol(apTokens[L5_BEDROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BEDS, iTmp);
   //   memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   //}

   // Baths
   //iTmp = atol(apTokens[L5_BATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_F, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   //}

   // HBaths
   //iTmp = atol(apTokens[L5_HALFBATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_H, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   //}

   // Rooms
   //iTmp = atol(apTokens[L5_TOTALROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   //}

   // Heating
   //int iCmp;
   //if (*apTokens[L5_HEATING] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L5_HEATING], asHeating[iTmp].acSrc, asHeating[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
   //}

   // Cooling
   //if (*apTokens[L5_AC] == 'C')
   //   *(pOutbuf+OFF_AIR_COND) = 'C';
   //else if (*apTokens[L5_AC] > ' ')
   //   LogMsg("*** Unknown A/C: %s", apTokens[L5_AC]);

   // Pool/Spa
   //if (*apTokens[L5_POOLSPA] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L5_POOLSPA], asPool[iTmp].acSrc, asPool[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_POOL) = asPool[iTmp].acCode[0];
   //}
   // Fire place
   //if (*apTokens[L5_FIREPLACE] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   pTmp = _strupr(apTokens[L5_FIREPLACE]);
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L5_FIREPLACE], asFirePlace[iTmp].acSrc, asFirePlace[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_FIRE_PL) = asPool[iTmp].acCode[0];
   //}
   // Quality Class
   //acTmp1[0] = 0;
   //if (*apTokens[L5_QUALITYCLASS] > '0' || strlen(apTokens[L5_QUALITYCLASS]) > 1)
   //{
   //   strcpy(acTmp, apTokens[L5_QUALITYCLASS]);
   //   remCharEx(acTmp, " ,'?");
   //   pTmp = _strupr(acTmp);

   //   //memcpy(myCharRec.QualityClass, pTmp, strlen(pTmp));
   //   if (acTmp[0] == 'A' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'A';
   //   else if (acTmp[0] == 'F' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'F';
   //   else if (!memcmp(pTmp, "POOR", 4) || !memcmp(pTmp, "LOW", 3))
   //      acTmp1[0] = 'P';
   //   else if (*pTmp == 'G')
   //      acTmp1[0] = 'G';
   //   else if (isalpha(*pTmp))
   //   {
   //      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
   //      if (isdigit(acTmp[1]))
   //         iRet = Quality2Code(&acTmp[1], acTmp1, NULL);
   //      else if (isdigit(acTmp[2]))
   //         iRet = Quality2Code(&acTmp[2], acTmp1, NULL);
   //      else if (isalpha(acTmp[1]))
   //      {
   //         switch (acTmp[1])
   //         {
   //            case 'L':
   //            case 'P':
   //               acTmp1[0] = 'P';
   //               break;
   //            case 'A':
   //               acTmp1[0] = 'A';
   //               break;
   //            case 'F':
   //               acTmp1[0] = 'F';
   //               break;
   //            case 'G':
   //               acTmp1[0] = 'G';
   //               break;
   //         }
   //      }
   //   } else if (isdigit(*pTmp))
   //   {
   //      iTmp = atol(pTmp);
   //      if (iTmp < 100)
   //         iRet = Quality2Code(pTmp, acTmp1, NULL);
   //   }

   //   if (acTmp1[0] > '0')
   //      *(pOutbuf+OFF_BLDG_QUAL) = acTmp1[0];
   //}

   return 0;
}

/******************************** Mno_MergeLien8 *****************************
 *
 * For LDR 2018_Secured_601.txt
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mno_MergeLien8(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L8_UNITS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L8_ASMT], iRet);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   iRet = remChar(apTokens[L8_ASMT], '-');
   memcpy(pOutbuf, apTokens[L8_ASMT], iRet);

   // Copy ALT_APN
   iRet = remChar(apTokens[L8_FEEPARCEL], '-');
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L8_FEEPARCEL], iRet);

   // Format APN
   iRet = formatApn(apTokens[L8_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L8_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "26MNO", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L8_ASMTSTATUS];

   // TRA
   iTmp = remChar(apTokens[L8_TRA], '-');
   iTmp = atol(apTokens[L8_TRA]);
   sprintf(acTmp, "%.6d", iTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = dollar2Num(apTokens[L8_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[L8_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   long lFixtr = dollar2Num(apTokens[L8_FIXTURESVALUE]);
   long lFixtRP= dollar2Num(apTokens[L8_FIXTURESRP]);
   long lPers  = dollar2Num(apTokens[L8_PPVALUE]);
   long lPP_MH = dollar2Num(apTokens[L8_MHPPVALUE]);
   lTmp = lFixtr+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = dollar2Num(apTokens[L8_HOX]);
   long lExe2 = dollar2Num(apTokens[L8_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L8_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L8_OTHEREXEMPTIONCODE], strlen(apTokens[L8_OTHEREXEMPTIONCODE]));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002361008", 9) )
   //   iTmp = 0;
#endif

   // Legal
   if (*apTokens[L8_PARCELDESCRIPTION] > ' ')
   {
      pTmp = strcpy(acTmp, _strupr(apTokens[L8_PARCELDESCRIPTION]));
      if (acTmp[0] == '"')
      {
         pTmp = &acTmp[1];
         acTmp[strlen(pTmp)] = 0;
      } 

      char *pTmp1 = strchr(pTmp, 0xA4);
      if (pTmp1)
         *pTmp1 = 'N';

      // Stop using legal as of 10/17/2022
      //updateLegal(pOutbuf, pTmp);
   }

   // UseCode
   iTmp = atol(apTokens[L8_LANDUSE1]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L8_LANDUSE1], 3);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   Mno_MergeOwner(pOutbuf, _strupr(apTokens[L8_OWNER]));

   // Situs
   //Mno_MergeSitus(pOutbuf, apTokens[L8_SITUS1], apTokens[L8_SITUS2]);

   // Mailing
   Mno_MergeMAdr(pOutbuf, apTokens[L8_MAILADDRESS1], apTokens[L8_MAILADDRESS2], apTokens[L8_MAILADDRESS3], apTokens[L8_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L8_TAXABILITYFULL], true, true);

   // Number of parking spaces
   if (*apTokens[L8_GARAGE] == '0' || *apTokens[L8_GARAGE] == 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'H';              // None
   else if (*apTokens[L8_GARAGE] > '0' && *apTokens[L8_GARAGE] <= '9')
   {
      iTmp = atol(apTokens[L8_GARAGE]);
      sprintf(acTmp, "%d", iTmp);
      if (iTmp > 99)
         LogMsg("*** Bad parking spaces: %d, APN=%s", iTmp, apTokens[L8_ASMT]);
      else
      {
         vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
         *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
      }
   }

   // YearBlt
   lTmp = atol(apTokens[L8_YEARBUILT]);
   if (lTmp > 1800 && lTmp < lToyear)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // Acres
   dTmp = atof(apTokens[L8_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Lot size
   lTmp = atol(apTokens[L8_LANDSIZE]);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      if (!dTmp)
      {
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(lTmp*SQFT_MF_1000));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   } else if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   return 0;
}

/******************************* Mno_MergeLien11 *****************************
 *
 * For LDR 2019_Secured_601.txt
 * Required steps to convert 2018_Secured_601.xlsx to 2018_Secured_601.txt:
 *   1) Format APN, Feeparcel, and TRA with 0 prefix
 *   2) Save to text file (tab delimited)
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mno_MergeLien11(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L11_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L11_ASMT], iRet);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L11_ASMT], iApnLen);

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L11_FEEPARCEL], iApnLen);

   // Format APN
   iRet = formatApn(apTokens[L11_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L11_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "26MNO", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L11_ASMTSTATUS];

   // TRA
   vmemcpy(pOutbuf+OFF_TRA, apTokens[L11_TRA], 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = dollar2Num(apTokens[L11_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[L11_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   long lFixtr = dollar2Num(apTokens[L11_FIXTURESVALUE]);
   long lFixtRP= dollar2Num(apTokens[L11_FIXTURESRP]);
   long lPers  = dollar2Num(apTokens[L11_PPVALUE]);
   long lPP_MH = dollar2Num(apTokens[L11_MHPPVALUE]);
   long lGrowVal = dollar2Num(apTokens[L11_GROWINGVALUE]);
   lTmp = lFixtr+lPers+lPP_MH+lFixtRP+lGrowVal;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = dollar2Num(apTokens[L11_HOX]);
   long lExe2 = dollar2Num(apTokens[L11_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L11_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L11_OTHEREXEMPTIONCODE], strlen(apTokens[L11_OTHEREXEMPTIONCODE]));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001100061000", 9) )
   //   iTmp = 0;
#endif

   // Legal
   if (*apTokens[L11_PARCELDESCRIPTION] > ' ')
   {
      pTmp = strcpy(acTmp, _strupr(apTokens[L11_PARCELDESCRIPTION]));
      if (acTmp[0] == '"')
      {
         pTmp = &acTmp[1];
         acTmp[strlen(pTmp)] = 0;
      } 

      char *pTmp1;
      if ((pTmp1 = strchr(pTmp, 0xD1)) || (pTmp1 = strchr(pTmp, 0xF1)))
         *pTmp1 = 'N';

      // Stop using legal as of 10/17/2022
      //updateLegal(pOutbuf, pTmp);
   }

   // UseCode
   iTmp = atol(apTokens[L11_LANDUSE1]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L11_LANDUSE1], 3);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   Mno_MergeOwner(pOutbuf, _strupr(apTokens[L11_OWNER]));

   // Situs
   //Mno_MergeSitus(pOutbuf, apTokens[L11_SITUS1], apTokens[L11_SITUS2]);

   // Mailing
   Mno_MergeMAdr(pOutbuf, apTokens[L11_MAILADDRESS1], apTokens[L11_MAILADDRESS2], apTokens[L11_MAILADDRESS3], apTokens[L11_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L11_TAXABILITY], true, true);

   // Number of parking spaces
   if (*apTokens[L11_GARAGE] == '0' || *apTokens[L11_GARAGE] == 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'H';              // None
   else if (*apTokens[L11_GARAGE] > '0' && *apTokens[L11_GARAGE] <= '9')
   {
      iTmp = atol(apTokens[L11_GARAGE]);
      sprintf(acTmp, "%d", iTmp);
      if (iTmp > 99)
         LogMsg("*** Bad parking spaces: %d, APN=%s", iTmp, apTokens[L11_ASMT]);
      else
      {
         vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
         *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
      }
   }

   // YearBlt
   lTmp = atol(apTokens[L11_YEARBUILT]);
   if (lTmp > 1800 && lTmp < lToyear)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // Acres
   dTmp = atof(apTokens[L11_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Lot size
   lTmp = atol(apTokens[L11_LANDSIZE]);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      if (!dTmp)
      {
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(lTmp*SQFT_MF_1000));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   } else if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   return 0;
}

/******************************** Mno_Load_LDR3 *****************************
 *
 * Load LDR 2019
 *
 ****************************************************************************/

int Mno_Load_LDR3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   switch (iLdrGrp)
   {
      case 11:    // 2019
         strcpy(acRec, "S(#2,C,A) DEL(9) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")");
         break;
      default:
         strcpy(acRec, "S(#3,C,A) DEL(9) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")");
         break;
   }

   iRet = sortFile(acTmpFile, acRollFile, acRec);  
   if (!iRet)
      return -1;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
      if (cDelim == '|')
         strcat(acRec, "DEL(124) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      // Create new R01 record
      switch (iLdrGrp)
      {
         case 3:
            iRet = Mno_MergeLien3(acBuf, acRec);      // 2016
            break;
         case 5:
            iRet = Mno_MergeLien5(acBuf, acRec);      // 2017
            break;
         case 8:
            iRet = Mno_MergeLien8(acBuf, acRec);      // 2018
            break;
         case 11:
            iRet = Mno_MergeLien11(acBuf, acRec);     // 2019
            break;
         default:
            LogMsg("***** Invalid LdrGrp (%d).  Please check INI file", iLdrGrp);
            iRet = -99;
            break;
      }

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Mno_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Mno_MergeStdChar(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else if (iRet == -99)
         break;

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Situs skiped:     %u\n", lSitusSkip);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return iRet;
}

/******************************* Mno_MergeLien13 *****************************
 *
 * For LDR "2020 Secured Tax Roll.txt".  This layout doesn't have ParcelDescription & Other exemption
 * We need to pull those fields from mno_roll.csv &m mno_exe.csv
 *
 * Required steps to convert .xlsx to .txt:
 *   1) Format APN, Feeparcel, and TRA with 0 prefix
 *   2) Save to text file (tab delimited)
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mno_MergeLien13(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L13_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L13_ASMT], iRet);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L13_ASMT], iApnLen);

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L13_FEEPARCEL], iApnLen);

   // Format APN
   iRet = formatApn(apTokens[L13_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L13_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "26MNO", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L13_ASMTSTATUS];

   // TRA
   vmemcpy(pOutbuf+OFF_TRA, apTokens[L13_TRA], 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[L13_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[L13_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   long lFixtr = atol(apTokens[L13_FIXTURESVALUE]);
   long lFixtRP= atol(apTokens[L13_FIXTURESRP]);
   long lPers  = atol(apTokens[L13_PPVALUE]);
   long lPP_MH = atol(apTokens[L13_MHPPVALUE]);
   long lGrowVal = atol(apTokens[L13_GROWINGVALUE]);
   lTmp = lFixtr+lPers+lPP_MH+lFixtRP+lGrowVal;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L13_HOX]);
   if (lExe1 > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lExe1);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   //if (lExe2 > 0 && *apTokens[L13_OTHEREXEMPTIONCODE] > ' ')
   //   memcpy(pOutbuf+iTmp, apTokens[L13_OTHEREXEMPTIONCODE], strlen(apTokens[L13_OTHEREXEMPTIONCODE]));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001100061000", 9) )
   //   iTmp = 0;
#endif

   // Legal
   //if (*apTokens[L13_PARCELDESCRIPTION] > ' ')
   //{
   //   pTmp = strcpy(acTmp, _strupr(apTokens[L13_PARCELDESCRIPTION]));
   //   if (acTmp[0] == '"')
   //   {
   //      pTmp = &acTmp[1];
   //      acTmp[strlen(pTmp)] = 0;
   //   } 

   //   char *pTmp1;
   //   if ((pTmp1 = strchr(pTmp, 0xD1)) || (pTmp1 = strchr(pTmp, 0xF1)))
   //      *pTmp1 = 'N';

   //   updateLegal(pOutbuf, pTmp);
   //}

   // UseCode
   iTmp = atol(apTokens[L13_LANDUSE1]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L13_LANDUSE1], 3);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   Mno_MergeOwner(pOutbuf, _strupr(apTokens[L13_OWNER]));

   // Situs
   //Mno_MergeSitus(pOutbuf, apTokens[L13_SITUS1], apTokens[L13_SITUS2]);

   // Mailing
   Mno_MergeMAdr(pOutbuf, apTokens[L13_MAILADDRESS1], apTokens[L13_MAILADDRESS2], apTokens[L13_MAILADDRESS3], apTokens[L13_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L13_TAXABILITYFULL], true, true);

   // Number of parking spaces
   if (*apTokens[L13_GARAGE] == '0' || *apTokens[L13_GARAGE] == 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'H';              // None
   else if (*apTokens[L13_GARAGE] > '0' && *apTokens[L13_GARAGE] <= '9')
   {
      iTmp = atol(apTokens[L13_GARAGE]);
      sprintf(acTmp, "%d", iTmp);
      if (iTmp > 99)
         LogMsg("*** Bad parking spaces: %d, APN=%s", iTmp, apTokens[L13_ASMT]);
      else
      {
         vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
         *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
      }
   }

   // YearBlt
   lTmp = atol(apTokens[L13_YEARBUILT]);
   if (lTmp > 1800 && lTmp < lToyear)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // Acres
   dTmp = atof(apTokens[L13_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lTmp = (long)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   return 0;
}

/******************************* Mno_MergeLien16 *****************************
 *
 * For LDR "601_Secured.txt".  This version includes ParcelDescription.
 *
 * Required steps to convert .xlsx to .txt:
 *   1) Format APN, Feeparcel, and TRA with 0 prefix
 *   2) Save to text file (tab delimited)
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mno_MergeLien16(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L16_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L16_ASMT], iRet);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L16_ASMT], iApnLen);

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L16_FEEPARCEL], iApnLen);

   // Format APN
   iRet = formatApn(apTokens[L16_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L16_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "26MNO", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L16_ASMTSTATUS];

   // TRA
   vmemcpy(pOutbuf+OFF_TRA, apTokens[L16_TRA], 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[L16_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[L16_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   long lFixtr = atol(apTokens[L16_FIXTURESVALUE]);
   long lFixtRP= atol(apTokens[L16_FIXTURESRP]);
   long lPers  = atol(apTokens[L16_PPVALUE]);
   long lPP_MH = atol(apTokens[L16_MHPPVALUE]);
   long lGrowVal = atol(apTokens[L16_GROWINGVALUE]);
   lTmp = lFixtr+lPers+lPP_MH+lFixtRP+lGrowVal;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%u         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%u         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
      // Growing
      if (lGrowVal > 0)
      {
         sprintf(acTmp, "%u         ", SIZ_GR_IMPR, lGrowVal);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L16_HOX]);
   long lExe2 = atol(apTokens[L16_OTHEREXEMPTION]);
   if (lExe1 > 0 || lExe2 > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe1+lExe2);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save other exemption code
   if (lExe2 > 0 && *apTokens[L16_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L16_OTHEREXEMPTIONCODE], strlen(apTokens[L16_OTHEREXEMPTIONCODE]));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001100061000", 9) )
   //   iTmp = 0;
#endif

   // Legal - Stop using legal as of 10/17/2022
   //if (*apTokens[L16_PARCELDESCRIPTION] > ' ')
   //   updateLegal(pOutbuf, _strupr(apTokens[L16_PARCELDESCRIPTION]));

   // UseCode
   iTmp = atol(apTokens[L16_LANDUSE1]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L16_LANDUSE1], 3);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   Mno_MergeOwner(pOutbuf, _strupr(apTokens[L16_OWNER]));

   // Mailing
   Mno_MergeMAdr(pOutbuf, apTokens[L16_MAILADDRESS1], apTokens[L16_MAILADDRESS2], apTokens[L16_MAILADDRESS3], apTokens[L16_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L16_TAXABILITYFULL], true, true);

   // Recorded Doc
   if (*apTokens[L16_CURRENTDOCNUM] > '0' && *(apTokens[L16_CURRENTDOCNUM]+4) == 'R')
   {
      if (!memcmp(apTokens[L16_CURRENTDOCNUM], apTokens[L16_CURRENTDOCDATE], 4))
      {
         if (dateConversion(apTokens[L16_CURRENTDOCDATE], acTmp, YYYY_MM_DD))
         {
            memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
            sprintf(acTmp1, "%.4sR%.7d", apTokens[L16_CURRENTDOCNUM], atol(apTokens[L16_CURRENTDOCNUM]+5));
            vmemcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp1, SIZ_TRANSFER_DOC);
         }
      }
   }

   // Number of parking spaces
   if (*apTokens[L16_GARAGE] == '0' || *apTokens[L16_GARAGE] == 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'H';              // None
   else if (*apTokens[L16_GARAGE] > '0' && *apTokens[L16_GARAGE] <= '9')
   {
      lTmp = atol(apTokens[L16_GARAGESIZE]);
      if (lTmp > 100)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lTmp);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      }

      iTmp = atol(apTokens[L16_GARAGE]);
      sprintf(acTmp, "%d", iTmp);
      if (iTmp > 99)
         LogMsg("*** Bad parking spaces: %d, APN=%s", iTmp, apTokens[L16_ASMT]);
      else
      {
         vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
         if (lTmp > 100)
            *(pOutbuf+OFF_PARK_TYPE) = 'Z';              // Garage
         else
            *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
      }
   }

   // YearBlt
   lTmp = atol(apTokens[L16_YEARBUILT]);
   if (lTmp > 1800 && lTmp < lToyear)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001040017000", 9))
   //   iTmp = 0;
#endif

   // Acres
   lTmp = atol(apTokens[L16_LANDSIZE]);
   dTmp = atof(apTokens[L16_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lTmp = (ULONG)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   } else if (lTmp > 100)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      if (lTmp < 200000)
         dTmp = (double)(lTmp*1000)/SQFT_PER_ACRE;
      else
         dTmp = (double)(lTmp/SQFT_PER_ACRE)*1000;
      iTmp = sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp+0.5));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   return 0;
}

/******************************* Mno_MergeLien18 *****************************
 *
 * For LDR "601_Secured.txt".  
 *
 * Required steps to convert .xlsx to .txt:
 *   1) Format APN, Feeparcel, and TRA with 0 prefix
 *   2) Save to text file (tab delimited)
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mno_MergeLien18(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L18_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L18_ASMT], iRet);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L18_ASMT], iApnLen);

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L18_FEEPARCEL], iApnLen);

   // Format APN
   iRet = formatApn(apTokens[L18_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L18_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "26MNO", 5);

   // status
   *(pOutbuf+OFF_STATUS) = 'A';

   // TRA
   vmemcpy(pOutbuf+OFF_TRA, apTokens[L18_TRA], 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[L18_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[L18_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   long lFixtr = atol(apTokens[L18_FIXTURESVALUE]);
   long lFixtRP= atol(apTokens[L18_FIXTURESRP]);
   long lPers  = atol(apTokens[L18_PPVALUE]);
   long lPP_MH = atol(apTokens[L18_MHPPVALUE]);
   lTmp = lFixtr+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%u         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%u         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L18_HOX]);
   long lExe2 = atol(apTokens[L18_OTHEREXEMPTION]);
   if (lExe1 > 0 || lExe2 > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe1+lExe2);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save other exemption code
   if (lExe2 > 0 && *apTokens[L18_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L18_OTHEREXEMPTIONCODE], strlen(apTokens[L18_OTHEREXEMPTIONCODE]));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001100061000", 9) )
   //   iTmp = 0;
#endif

   // Legal - Stop using legal as of 10/17/2022
   //if (*apTokens[L18_PARCELDESCRIPTION] > ' ')
   //   updateLegal(pOutbuf, _strupr(apTokens[L18_PARCELDESCRIPTION]));

   // UseCode
   iTmp = atol(apTokens[L18_LANDUSE]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L18_LANDUSE], 3);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   Mno_MergeOwner(pOutbuf, _strupr(apTokens[L18_OWNER]));

   // Mailing
   Mno_MergeMAdr(pOutbuf, apTokens[L18_MAILADDRESS1], apTokens[L18_MAILADDRESS2], apTokens[L18_MAILADDRESS3], apTokens[L18_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L18_TAXABILITY], true, true);

   // Recorded Doc - Not available
   //if (*apTokens[L18_CURRENTDOCNUM] > '0' && *(apTokens[L18_CURRENTDOCNUM]+4) == 'R')
   //{
   //   if (!memcmp(apTokens[L18_CURRENTDOCNUM], apTokens[L18_CURRENTDOCDATE], 4))
   //   {
   //      if (dateConversion(apTokens[L18_CURRENTDOCDATE], acTmp, YYYY_MM_DD))
   //      {
   //         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   //         sprintf(acTmp1, "%.4sR%.7d", apTokens[L18_CURRENTDOCNUM], atol(apTokens[L18_CURRENTDOCNUM]+5));
   //         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp1, SIZ_TRANSFER_DOC);
   //      }
   //   }
   //}

   // Number of parking spaces
   if (*apTokens[L18_GARAGE] == '0' || *apTokens[L18_GARAGE] == 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'H';              // None
   else if (*apTokens[L18_GARAGE] > '0' && *apTokens[L18_GARAGE] <= '9')
   {
      lTmp = atol(apTokens[L18_GARAGESIZE]);
      if (lTmp > 100)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lTmp);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      }

      iTmp = atol(apTokens[L18_GARAGE]);
      sprintf(acTmp, "%d", iTmp);
      if (iTmp > 99)
         LogMsg("*** Bad parking spaces: %d, APN=%s", iTmp, apTokens[L18_ASMT]);
      else
      {
         vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
         if (lTmp > 100)
            *(pOutbuf+OFF_PARK_TYPE) = 'Z';              // Garage
         else
            *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
      }
   }

   // YearBlt
   lTmp = atol(apTokens[L18_YEARBUILT]);
   if (lTmp > 1800 && lTmp < lToyear)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001040017000", 9))
   //   iTmp = 0;
#endif

   // Acres
   lTmp = atol(apTokens[L18_LANDSIZE]);
   dTmp = atof(apTokens[L18_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lTmp = (ULONG)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   } else if (lTmp > 100)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      if (lTmp < 200000)
         dTmp = (double)(lTmp*1000)/SQFT_PER_ACRE;
      else
         dTmp = (double)(lTmp/SQFT_PER_ACRE)*1000;
      iTmp = sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp+0.5));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   return 0;
}

/******************************* Mno_MergeLien19 *****************************
 *
 * For LDR "601_Secured_2023.txt".  
 *
 * Required steps to convert .xlsx to .txt:
 *   1) Format APN, Feeparcel, and TRA with 0 prefix
 *   2) Save to text file (tab delimited)
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mno_MergeLien19(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L19_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L19_ASMT], iRet);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L19_ASMT], iApnLen);

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L19_FEEPARCEL], iApnLen);

   // Format APN
   iRet = formatApn(apTokens[L19_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L19_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "26MNO", 5);

   // status
   *(pOutbuf+OFF_STATUS) = 'A';

   // TRA
   vmemcpy(pOutbuf+OFF_TRA, apTokens[L19_TRA], 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[L19_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[L19_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   long lFixtr = atol(apTokens[L19_FIXTURESVALUE]);
   long lFixtRP= atol(apTokens[L19_FIXTURESRP]);
   long lPers  = atol(apTokens[L19_PPVALUE]);
   long lPP_MH = atol(apTokens[L19_MHPPVALUE]);
   long lGrow  = atol(apTokens[L19_GROWINGVALUE]);
   lTmp = lFixtr+lPers+lPP_MH+lFixtRP+lGrow;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%u         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%u         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%u         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L19_HOX]);
   if (lExe1 > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe1);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001100061000", 9) )
   //   iTmp = 0;
#endif

   // Legal - Stop using legal as of 10/17/2022
   //if (*apTokens[L19_PARCELDESCRIPTION] > ' ')
   //   updateLegal(pOutbuf, _strupr(apTokens[L19_PARCELDESCRIPTION]));

   // UseCode - not in L19
   //iTmp = atol(apTokens[L19_LANDUSE]);
   //if (iTmp > 0)
   //{
   //   vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L19_LANDUSE], 3);

   //   // Std Usecode
   //   updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);
   //} else
   //   memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   Mno_MergeOwner(pOutbuf, _strupr(apTokens[L19_OWNER]));

   // Mailing
   Mno_MergeMAdr(pOutbuf, apTokens[L19_MAILADDRESS1], apTokens[L19_MAILADDRESS2], apTokens[L19_MAILADDRESS3], apTokens[L19_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L19_TAXABILITY], true, true);

   // Recorded Doc - Not available
   if (*apTokens[L19_CURRENTDOCNUM] > '0' && *(apTokens[L19_CURRENTDOCNUM]+4) == 'R')
   {
      if (dateConversion(apTokens[L19_CURRENTDOCDATE], acTmp, MM_DD_YYYY_1))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         sprintf(acTmp1, "%.4sR%.7d", apTokens[L19_CURRENTDOCNUM], atol(apTokens[L19_CURRENTDOCNUM]+5));
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp1, SIZ_TRANSFER_DOC);
      }
   }

   // Number of parking spaces
   if (*apTokens[L19_GARAGE] == '0' || *apTokens[L19_GARAGE] == 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'H';              // None
   else if (*apTokens[L19_GARAGE] > '0' && *apTokens[L19_GARAGE] <= '9')
   {
      lTmp = atol(apTokens[L19_GARAGESIZE]);
      if (lTmp > 100)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lTmp);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      }

      iTmp = atol(apTokens[L19_GARAGE]);
      sprintf(acTmp, "%d", iTmp);
      if (iTmp > 99)
         LogMsg("*** Bad parking spaces: %d, APN=%s", iTmp, apTokens[L19_ASMT]);
      else
      {
         vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
         if (lTmp > 100)
            *(pOutbuf+OFF_PARK_TYPE) = 'Z';              // Garage
         else
            *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
      }
   }

   // YearBlt
   lTmp = atol(apTokens[L19_YEARBUILT]);
   if (lTmp > 1800 && lTmp < lToyear)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001040017000", 9))
   //   iTmp = 0;
#endif

   // Acres
   lTmp = atol(apTokens[L19_LANDSIZE]);
   dTmp = atof(apTokens[L19_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lTmp = (ULONG)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   } else if (lTmp > 10)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      if (lTmp < 200000)
         dTmp = (double)(lTmp*1000)/SQFT_PER_ACRE;
      else
         dTmp = (double)(lTmp/SQFT_PER_ACRE)*1000;
      iTmp = sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp+0.5));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   return 0;
}

/******************************* Mno_MergeLien20 *****************************
 *
 * For LDR "601_Secured_2024.txt".  
 *
 * Required steps to convert .xlsx to .txt:
 *   1) Format APN, Feeparcel, and TRA with 0 prefix
 *   2) Save to text file (tab delimited)
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mno_MergeLien20(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L20_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L20_ASMT], iRet);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L20_ASMT], iApnLen);

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L20_FEEPARCEL], iApnLen);

   // Format APN
   iRet = formatApn(apTokens[L20_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L20_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "26MNO", 5);

   // status
   *(pOutbuf+OFF_STATUS) = 'A';

   // TRA
   vmemcpy(pOutbuf+OFF_TRA, apTokens[L20_TRA], 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[L20_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[L20_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   long lFixtr = atol(apTokens[L20_FIXTURESVALUE]);
   long lFixtRP= atol(apTokens[L20_FIXTURESRP]);
   long lPers  = atol(apTokens[L20_PPVALUE]);
   long lPP_MH = atol(apTokens[L20_MHPPVALUE]);
   long lGrow  = atol(apTokens[L19_GROWINGVALUE]);
   lTmp = lFixtr+lPers+lPP_MH+lFixtRP+lGrow;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%u         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%u         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%u         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L20_HOX]);
   long lExe2 = atol(apTokens[L20_OTHEREXEMPTION]);
   if (lExe1 > 0 || lExe2 > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe1+lExe2);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save other exemption code
   if (*apTokens[L20_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L20_OTHEREXEMPTIONCODE], strlen(apTokens[L20_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&MNO_Exemption);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001100061000", 9) )
   //   iTmp = 0;
#endif

   // Legal - Stop using legal as of 10/17/2022
   //if (*apTokens[L20_PARCELDESCRIPTION] > ' ')
   //   updateLegal(pOutbuf, _strupr(apTokens[L20_PARCELDESCRIPTION]));

   // UseCode
   //iTmp = atol(apTokens[L20_LANDUSE]);
   //if (iTmp > 0)
   //{
   //   vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L20_LANDUSE], 3);

   //   // Std Usecode
   //   updateStdUse(pOutbuf+OFF_USE_STD, iTmp, pOutbuf);
   //} else
   //   memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   Mno_MergeOwner(pOutbuf, _strupr(apTokens[L20_OWNER]));

   // Mailing
   Mno_MergeMAdr(pOutbuf, apTokens[L20_MAILADDRESS1], apTokens[L20_MAILADDRESS2], apTokens[L20_MAILADDRESS3], apTokens[L20_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L20_TAXABILITY], true, true);

   // Recorded Doc - Not available
   //if (*apTokens[L20_CURRENTDOCNUM] > '0' && *(apTokens[L20_CURRENTDOCNUM]+4) == 'R')
   //{
   //   if (!memcmp(apTokens[L20_CURRENTDOCNUM], apTokens[L20_CURRENTDOCDATE], 4))
   //   {
   //      if (dateConversion(apTokens[L20_CURRENTDOCDATE], acTmp, YYYY_MM_DD))
   //      {
   //         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   //         sprintf(acTmp1, "%.4sR%.7d", apTokens[L20_CURRENTDOCNUM], atol(apTokens[L20_CURRENTDOCNUM]+5));
   //         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp1, SIZ_TRANSFER_DOC);
   //      }
   //   }
   //}

   // Number of parking spaces
   if (*apTokens[L20_GARAGE] == '0' || *apTokens[L20_GARAGE] == 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'H';              // None
   else if (*apTokens[L20_GARAGE] > '0' && *apTokens[L20_GARAGE] <= '9')
   {
      lTmp = atol(apTokens[L20_GARAGESIZE]);
      if (lTmp > 100)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lTmp);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      }

      iTmp = atol(apTokens[L20_GARAGE]);
      sprintf(acTmp, "%d", iTmp);
      if (iTmp > 99)
         LogMsg("*** Bad parking spaces: %d, APN=%s", iTmp, apTokens[L20_ASMT]);
      else
      {
         vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
         if (lTmp > 100)
            *(pOutbuf+OFF_PARK_TYPE) = 'Z';              // Garage
         else
            *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
      }
   }

   // YearBlt
   lTmp = atol(apTokens[L20_YEARBUILT]);
   if (lTmp > 1800 && lTmp < lToyear)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001040017000", 9))
   //   iTmp = 0;
#endif

   // Acres
   lTmp = atol(apTokens[L20_LANDSIZE]);
   dTmp = atof(apTokens[L20_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lTmp = (ULONG)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   } else if (lTmp > 100)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      if (lTmp < 200000)
         dTmp = (double)(lTmp*1000)/SQFT_PER_ACRE;
      else
         dTmp = (double)(lTmp/SQFT_PER_ACRE)*1000;
      iTmp = sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp+0.5));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   return 0;
}

/******************************** Mno_Load_LDR ******************************
 *
 * Load LDR 2020
 *
 ****************************************************************************/

int Mno_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048], acApn[32];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acLienFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);

   // Sort roll file on ASMT
   sprintf(acLienFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   //2021 - strcpy(acRec, "S(#3,C,A) DEL(9) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")");
   sprintf(acRec, "S(#2,C,A) DEL(%d) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")", cLdrSep);
   iRet = sortFile(acTmpFile, acLienFile, acRec);  
   if (!iRet)
      return -1;

   // Open Lien file
   LogMsg("Open lien file %s", acLienFile);
   fdLDR = fopen(acLienFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acLienFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -4;
   }

   // Open roll file
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRollFile, _MAX_PATH, acIniFile);
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acRollFile, acTmpFile, "S(1,12,C,A) Omit(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")");
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   acApn[0] = 0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      // Create new R01 record
      if (lLienYear >= 2024)
         iRet = Mno_MergeLien20(acBuf, acRec);     // 2024
      else if (lLienYear == 2023)
         iRet = Mno_MergeLien19(acBuf, acRec);     // 2023
      else if (lLienYear == 2022)
         iRet = Mno_MergeLien18(acBuf, acRec);     // 2022
      else if (lLienYear == 2021)
         iRet = Mno_MergeLien16(acBuf, acRec);     // 2021
      else
         iRet = Mno_MergeLien13(acBuf, acRec);     // 2020

      // Check for duplicate
      if (!memcmp(acApn, acBuf, iApnLen))
      {
         LogMsg("*** Duplicate APN found: %.12s", acApn);
         iRet = 1;
      } else
         memcpy(acApn, acBuf, iApnLen);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Mno_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Mno_MergeStdChar(acBuf);

         // Merge UseCode, Zoning, NBH Code
         if (fdRoll)
            lRet = Mno_MergeOthers(acBuf);
         
         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else if (iRet == -99)
         break;

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp)
         break;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 
   
   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Roll matched:     %u\n", lRollMatch);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return iRet;
}

/*********************************** loadMno ********************************
 *
 * Options:
 *    -CMno -L -Xl -Xs -Xa -Mg (load lien)
 *    -CMno -U -[G|Mg] -[Xs|Ms] [-Xa]  (load update)
 *
 ****************************************************************************/

int loadMno(int iSkip)
{
   char  *pTmp, acTmp[_MAX_PATH], acZipFile[_MAX_PATH], acGrGrFile[_MAX_PATH];
   int   iRet;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;

   // Get APN xlation file
   GetIniString(myCounty.acCntyCode, "ApnXlat", "", acApnFile, _MAX_PATH, acIniFile);

   iApnLen = myCounty.iApnLen;
   iRet = 0;

   // Fix DocNum in old sale file
   //iRet = FixCumSale(acCSalFile, SALE_FLD_DOCNUM|CNTY_MNO, false);

   // Fix APN format in R01 file
   //iRet = Mno_ApnXlat( myCounty.acCntyCode, iSkip);

   //iRet = convertSaleData(myCounty.acCntyCode, acCSalFile, 1);

   // Fix DocNum in GrGr_Exp.sls - 20121201 
   //char acTmp1[_MAX_PATH];
   //sprintf(acTmp, acEGrGrTmpl, myCounty.acCntyCode, "sls");
   //sprintf(acTmp1, acEGrGrTmpl, myCounty.acCntyCode, "dat");
   //iRet = Mno_FixGrGrDoc(acTmp, acTmp1);

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Extract lien file
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = Mno_ExtrLien(myCounty.acCntyCode);    // 2019
      //iRet = Mno_ExtrLien(myCounty.acCntyCode);  // 2015
      //iRet = MB_ExtrTR601(myCounty.acCntyCode);  // 2016

   // Fix cumsale
   if (iLoadFlag & FIX_CSTYP)
   {
      iRet = FixDocType(acCSalFile, (IDX_TBL5 *)&MNO_DocCode[0], true /* remove rec w/o DocNum */);
      if (iRet)
         return iRet;
   }

   // Create/Update cum sale file from Mno_Sales.csv
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      iRet = MB_CreateSCSale(MM_DD_YYYY_1, 0, 0, true, (IDX_TBL5 *)&MNO_DocCode[0]);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;       // Trigger merge sale
   }

   if (iLoadFlag & LOAD_GRGR)                      // -G
   {
      char sToday[16];

      sprintf(sToday, "%d", lProcDate);
      GetIniString(myCounty.acCntyCode, "GrGrIn", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acZipFile, acTmp, atoin(sToday, 4), atoin(&sToday[4], 2), atoin(&sToday[6], 2), "zip");

      if (!_access(acZipFile, 0))
      {
         // If input is ZIP file, unzip first.
         doZipInit();
         if (!_access(acZipFile, 0))
         {
            strcpy(acTmp, acZipFile);
            pTmp = strrchr(acTmp, '\\');
            if (pTmp)
               *pTmp = 0;

            // Set unzip folder
            setUnzipToFolder(acTmp);

            // Set to replace file if exist
            setReplaceIfExist(true);

            LogMsg("Unzip input file %s", acZipFile);
            iRet = startUnzip(acZipFile);
            if (iRet)
            {
               LogMsg("***** Error while unzipping %s", acZipFile);
               iRet = -1;
            }
         } 
         // Shut down Zip server
         doZipShutdown();

         // Back up processed file
         char  acDate[32];
         dateString(acDate, 0);
         sprintf(acTmp, acGrGrBakTmpl, "MNO", acDate);

         // Create backup folder
         if (_access(acTmp, 0))
            _mkdir(acTmp);

         // Move file
         pTmp = strrchr(acZipFile, '\\');
         strcat(acTmp, pTmp);
         rename(acZipFile, acTmp);
      }

      /* MDB format
      GetIniString(myCounty.acCntyCode, "GrGrIn", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acZipFile, acTmp, atoin(sToday, 4), atoin(&sToday[4], 2), atoin(&sToday[6], 2), "zip");
      sprintf(acMdbFile, acTmp, atoin(sToday, 4), atoin(&sToday[4], 2), atoin(&sToday[6], 2), "mdb");

      if (!_access(acZipFile, 0))
      {
         // If input is ZIP file, unzip first.
         doZipInit();
         if (!_access(acZipFile, 0))
         {
            strcpy(acTmp, acZipFile);
            pTmp = strrchr(acTmp, '\\');
            if (pTmp)
               *pTmp = 0;

            // Set unzip folder
            setUnzipToFolder(acTmp);

            // Set to replace file if exist
            setReplaceIfExist(true);

            LogMsg("Unzip input file %s", acZipFile);
            iRet = startUnzip(acZipFile);
            if (iRet)
            {
               LogMsg("***** Error while unzipping %s", acZipFile);
               iRet = -1;
            } else
            {
               // Prevent multi-ext
               pTmp = strstr(acMdbFile, ".mdb");
               *(pTmp+4) = 0;
            }
         } 
         // Shut down Zip server
         doZipShutdown();
      }

      // New file format
      if (!_access(acGrGrFile, 0))
      {
         // Load MDB file - return number of records
         iRet = Mno_ConvertGrGr(acGrGrFile);
         if (!iRet)
         {
            char  acDate[32];

            iLoadFlag |= MERG_GRGR;       // Trigger merge grgr

            // Back up processed file
            pTmp = strrchr(acGrGrFile, '\\');
            if (pTmp)
               *++pTmp = 0;
            dateString(acDate, 0);
            sprintf(acTmp, "GrGr_%s", acDate);
            strcat(acGrGrFile, acTmp);

            // Create backup folder
            if (_access(acGrGrFile, 0))
               _mkdir(acMdbFile);

            // Move file
            pTmp = strrchr(acZipFile, '\\');
            strcat(acGrGrFile, pTmp);
            rename(acZipFile, acGrGrFile);
         }
      } else
      {
         LogMsg("*** No GrGr file avail for processing: %s", acGrGrFile);
         iRet = 1;
         bGrGrAvail = false;
      }
      */

      // CSV file format
      GetIniString(myCounty.acCntyCode, "GrGrCsv", "", acGrGrFile, _MAX_PATH, acIniFile);
      if (!_access(acGrGrFile, 0))
      {
         // Create Mno_GrGr.dat
         LogMsg("Load %s GrGr file", myCounty.acCntyCode);
         iRet = Mno_LoadGrGr(acGrGrFile);
         
         if (!iRet)
            iLoadFlag |= MERG_GRGR;                // Trigger merge grgr
      } else
      {
         bGrGrAvail = false;
         LogMsg("*** No new GrGr file found.");
         if (bSendMail &&  !(iLoadFlag & (LOAD_LIEN|LOAD_UPDT)))
            return iRet;
      }
   }
   
   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      iRet = Mno_ConvStdChar(acCharFile);
      if (iRet <= 0)
      {
         LogMsg("***** Error extracting attributes data from %s", acCharFile);
         return -1;
      }
   }

   if (iLoadFlag & LOAD_LIEN)
   {
      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      iRet = Mno_Load_LDR(iSkip); 
   } else if (iLoadFlag & LOAD_UPDT)
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Mno_Load_Roll(iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL))           // -Ms
   {
      // Apply Mno_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE|CLEAR_UPD_GRGR);
   }

   if (!iRet && (iLoadFlag & MERG_GRGR))           // -Mg
   {
      iRet = MergeGrGrDoc(myCounty.acCntyCode, 2);
      if (iRet)
         iLoadFlag = 0;
      else
      {
         if (!(iLoadFlag & LOAD_LIEN))
            iLoadFlag |= LOAD_UPDT;
      }
   }

   //if (!iRet && (iLoadFlag & MERG_GRGR))
   //{
   //   LogMsg("Update %s roll with GrGr file", myCounty.acCntyCode);
   //   iGrGrApnLen = myCounty.iApnLen;              // Use regular APN len when merge to R01

   //   // Update Seller and Sale price if available
   //   sprintf(acTmp, acEGrGrTmpl, myCounty.acCntyCode, "Dat");

   //   // Convert Mno_Grgr.sls to Grgr_exp.dat if needed
   //   if (iLoadFlag & LOAD_LIEN)
   //   {
   //      char acSlsFile[_MAX_PATH];

   //      sprintf(acSlsFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   //      iRet = GrGr_ExtrSaleMatched(acSlsFile, acTmp, (IDX_TBL5 *)&MNO_DocTitle[0]);
   //   }

   //   // Input: Grgr_Exp.dat
   //   iRet = MergeGrGrExpFile(acTmp, true, false, true);
   //   if (!iRet && !(iLoadFlag & LOAD_LIEN))
   //      iLoadFlag |= LOAD_UPDT;                   // Trigger buildcda
   //}

   return iRet;
}