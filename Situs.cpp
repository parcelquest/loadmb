/******************************************************************************
 *
 * 07/12/2019  Add new version of getCityZip() that returns CityCode.  Remove an
 *             unused version of getCityZip().
 * 03/22/2022  Remove UpdateCityZip(), modify initSitus() & loadZip2City()
 *
 ******************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "Logs.h"
#include "Utils.h"
#include "doSort.h"

#define  _SITUS_SRC_    1
#include "Situs.h"

extern   COUNTY_INFO myCounty;

FILE  *fdCity;
long  lCitySkip, lCityMatch;
int   iZipCnt;
ZIP_CITY asZip2City[256];

/********************************* initSitus *********************************
 *
 * Get City & Zip code
 *
 *****************************************************************************/

int initSitus(char *pIniFile, char *pCnty)
{
   char acTmp[_MAX_PATH], acCityZip[_MAX_PATH];
   int  iRet=0;

   LogMsg0("Load City/Zip file for %s", pCnty);

   GetIniString(pCnty, "CityZip", "", acCityZip, _MAX_PATH, pIniFile);
   if (acCityZip[0] < ' ')
   {
      GetIniString("Data", "CityZip", "", acTmp, _MAX_PATH, pIniFile);
      sprintf(acCityZip, acTmp, pCnty, pCnty, "txt");
   }

   if (!_access(acCityZip, 0))
   {
      LogMsg("Open City/Zip file %s", acCityZip);
      fdCity = fopen(acCityZip, "r");
      if (fdCity == NULL)
      {
         LogMsg("***** Error opening CityZip file: %s\n", acCityZip);
         iRet = -1;
      } 
   }

   return iRet;
}

void closeSitus()
{
   if (fdCity)
   {
      fclose(fdCity);
      fdCity = NULL;
   }
}

/********************************* getCityZip *******************************
 *
 * Get City & Zip code. Compare APN using provided iCmpLen.
 * Return 0 if found
 *
 *****************************************************************************/

int getCityZip(char *pApn, char *pCity, char *pZip, int iCmpLen)
{
   static   char acRec[256], *pRec=NULL, *myApn, *myCity, *myZip;
   char     *asTmp[8];
   int      iRet, iLoop;

   iRet=0;

   // If no situs file opened, return not found
   if (!fdCity)
      return 1;

   // Get first rec for first call
   if (!pRec)
   {
      // Skip header
      do {
         pRec = fgets(acRec, 256, fdCity);
      } while (pRec && !isdigit(acRec[0]));

      if (pRec)
      {
         iRet = ParseStringNQ(acRec, '|', 8, asTmp);
         myApn = asTmp[0];
         myCity = asTmp[1];
         myZip = asTmp[2];
      }
   }

   do
   {
      if (!pRec)
      {
         fclose(fdCity);
         fdCity = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pApn, myApn, iCmpLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 256, fdCity);
         if (pRec)
         {
            iRet = ParseStringNQ(acRec, '|', 8, asTmp);
            myApn = asTmp[0];
            myCity = asTmp[1];
            myZip = asTmp[2];
         }
         lCitySkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "10225005", 8))
   //   iTmp = 0;
#endif

   // City
   strcpy(pCity, myCity);
   strcpy(pZip, myZip);
   lCityMatch++;

   // Get next rec
   pRec = fgets(acRec, 256, fdCity);
   if (pRec)
   {
      iRet = ParseStringNQ(acRec, '|', 8, asTmp);
      myApn = asTmp[0];
      myCity = asTmp[1];
      myZip = asTmp[2];
   }

   return 0;
}

/********************************* getCityZip *******************************
 *
 * This function is created for SLO where Slo_Cityzip.txt contains city code (07/2019.)
 * This file needs update at least once a year at LDR time to update new parcels.
 *
 * Get City, Zip code, and City code. Compare APN using provided iCmpLen.
 * Return 0 if found
 *
 *****************************************************************************/

int getCityZip(char *pApn, char *pCity, char *pZip, char *pCityCode, int iCmpLen)
{
   static   char acRec[256], *pRec=NULL;
   char     *asTmp[8];
   int      iRet, iLoop;

   iRet=0;

   // If no situs file opened, return not found
   if (!fdCity)
      return 1;

   // Get first rec for first call
   if (!pRec)
   {
      // Skip header
      do {
         pRec = fgets(acRec, 256, fdCity);
      } while (pRec && !isdigit(acRec[0]));
   }

   do
   {
      if (!pRec)
      {
         fclose(fdCity);
         fdCity = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pApn, acRec, iCmpLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 256, fdCity);
         lCitySkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "10225005", 8))
   //   iTmp = 0;
#endif

   // City
   iRet = ParseStringNQ(acRec, '|', 8, asTmp);
   strcpy(pCity, asTmp[1]);
   strcpy(pZip, asTmp[2]);
   if (iRet > 3)
      strcpy(pCityCode, asTmp[3]);
   lCityMatch++;

   // Get next rec
   pRec = fgets(acRec, 256, fdCity);

   return 0;
}

//int getCityZip(CITYZIP *pCityZip, char *pApn, int iCmpLen)
//{
//   static   char acRec[256], *pRec=NULL, *myApn, *myCity, *myZip, bVerify;
//   char     *asTmp[8];
//   int      iRet, iLoop;
//
//   iRet=0;
//
//   // If no situs file opened, return not found
//   if (!fdCity)
//      return 1;
//
//   // Get first rec for first call
//   if (!pRec)
//   {
//      pRec = fgets(acRec, 256, fdCity);
//      if (pRec)
//      {
//         iRet = ParseStringNQ(acRec, '|', 8, asTmp);
//         myApn = asTmp[0];
//         myCity = asTmp[1];
//         myZip = asTmp[2];
//         bVerify = *asTmp[3];
//      }
//   }
//
//   do
//   {
//      if (!pRec)
//      {
//         fclose(fdCity);
//         fdCity = NULL;
//         return 1;      // EOF
//      }
//
//      // Compare Apn
//      iLoop = memcmp(pApn, myApn, iCmpLen);
//      if (iLoop > 0)
//      {
//         pRec = fgets(acRec, 256, fdCity);
//         if (pRec)
//         {
//            iRet = ParseStringNQ(acRec, '|', 8, asTmp);
//            myApn = asTmp[0];
//            myCity = asTmp[1];
//            myZip = asTmp[2];
//            bVerify = *asTmp[3];
//         }
//         lCitySkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 1;
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "10225005", 8))
//   //   iTmp = 0;
//#endif
//
//   // City
//   strcpy(pCityZip->acCity, myCity);
//   strcpy(pCityZip->acZip, myZip);
//   pCityZip->bVerify = bVerify;
//   lCityMatch++;
//
//   // Get next rec
//   pRec = fgets(acRec, 256, fdCity);
//   if (pRec)
//   {
//      iRet = ParseStringNQ(acRec, '|', 8, asTmp);
//      myApn = asTmp[0];
//      myCity = asTmp[1];
//      myZip = asTmp[2];
//      bVerify = *asTmp[3];
//   }
//
//   return 0;
//}

/*********************************** getCity *********************************
 *
 *****************************************************************************/

ZIP_CITY *getCity(char *pZip)
{
   int iTmp, iZip;
   ZIP_CITY *pRet = (ZIP_CITY *)NULL;

   if (!isdigit(*pZip))
      return pRet;

   iZip = atoin(pZip, 5);
   if (iZip < 90000 || iZip > 99999)
      return pRet;

   for (iTmp = 0; iTmp < iZipCnt; iTmp++)
   {
      if (iZip == asZip2City[iTmp].iZipCode)
      {
         pRet = &asZip2City[iTmp];
         break;
      }
   }

   return pRet;
}

/*********************************** getCity *********************************
 *
 * Get city name using given zip code
 * Return pointer to City name.  If pCity is valid, copy city name to tht buffer
 *
 *****************************************************************************/

char *getCity(char *pZip, char *pCity, char *pCityCode)
{
   int iTmp, iZip;
   char *pRet = NULL;

   if (!isdigit(*pZip))
      return pRet;

   iZip = atoin(pZip, 5);
   if (iZip < 90000 || iZip > 99999)
      return pRet;

   for (iTmp = 0; iTmp < iZipCnt; iTmp++)
   {
      if (iZip == asZip2City[iTmp].iZipCode)
      {
         pRet = asZip2City[iTmp].acCity;
         if (pCity)
            strcpy(pCity, pRet);
         if (pCityCode && asZip2City[iTmp].iCityCode > 0)
            strcpy(pCityCode, asZip2City[iTmp].acCityCode);
         break;
      }
   }

   return pRet;
}

/*********************************** getZip **********************************
 *
 * Find zip code with given city name
 * Return pointer to City name.  If pCity is valid, copy city name to tht buffer
 *
 *****************************************************************************/

char *getZip(char *pCity, char *pZip)
{
   int iTmp;
   char *pRet = NULL;

   if (!pCity)
      return pRet;

   for (iTmp = 0; iTmp < iZipCnt; iTmp++)
   {
      if (!memcmp(asZip2City[iTmp].acCity, pCity, asZip2City[iTmp].iCityLen))
      {
         pRet = asZip2City[iTmp].acZip;
         if (pZip)
            strcpy(pZip, pRet);
         break;
      }
   }

   return pRet;
}

/******************************** loadZip2City *******************************
 *
 * Load Zip2City file to array.
 *
 *****************************************************************************/

int loadZip2City(char *pIniFile, char *pCnty)
{
   char acTmp[_MAX_PATH], acZip2City[_MAX_PATH], *pTmp;
   FILE *fdZip2City;

   LogMsg0("Load Zip2City file");

   GetIniString(pCnty, "Zip2City", "", acTmp, _MAX_PATH, pIniFile);
   if (acTmp[0] < ' ')
   {
      GetIniString("Data", "Zip2City", "", acTmp, _MAX_PATH, pIniFile);
      sprintf(acZip2City, acTmp, pCnty, pCnty);
   }

   if (!_access(acZip2City, 0))
   {
      LogMsg("Open Zip2City file %s", acZip2City);
      fdZip2City = fopen(acZip2City, "r");
      if (fdZip2City == NULL)
      {
         LogMsg("***** Error opening Zip2City file: %s\n", acZip2City);
         return -1;
      } 
   } else
   {
      LogMsg("***** Zip2City file is missing: %s\n", acZip2City);
      return -2;
   } 

   // Load data
   iZipCnt = 0;
   while (!feof(fdZip2City))
   {
      if (pTmp = fgets(acTmp, _MAX_PATH, fdZip2City))
      {
         acTmp[5] = 0;
         strcpy(asZip2City[iZipCnt].acZip, acTmp);
         asZip2City[iZipCnt].iZipCode = atoi(acTmp);
         pTmp = strchr(&acTmp[6], ',');
         if (!pTmp)
            strncpy(asZip2City[iZipCnt++].acCity, myTrim(&acTmp[6]), 31);
         else
         {
            *pTmp++ = 0;
            strcpy(asZip2City[iZipCnt].acCity, &acTmp[6]);
            strcpy(asZip2City[iZipCnt].acCityCode, myTrim(pTmp));
            asZip2City[iZipCnt++].iCityCode = atoi(pTmp);
         }
         asZip2City[iZipCnt].iCityLen = strlen(&acTmp[6]);
      }
   }

   // Close
   fclose(fdZip2City);

   LogMsg("Zip2City loaded %d records\n", iZipCnt);
   return iZipCnt;
}

