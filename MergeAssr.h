#ifndef  _MERGEASSR_H_
#define  _MERGEASSR_H_

#include "Recdef.h"
/*
#define  OFF_3K_CO_ID        1-1
#define  OFF_3K_CO_NUM       4-1
// Roll
#define  OFF_3K_ASSMT        6-1
#define  OFF_3K_FEE_PRCL     18-1
#define  OFF_3K_TRA          30-1
#define  OFF_3K_DESC         36-1
#define  OFF_3K_ZONE         86-1
#define  OFF_3K_USE          94-1
#define  OFF_3K_NBHCD        102-1
#define  OFF_3K_INACRES      106-1
#define  OFF_3K_RDOCNUM      115-1
#define  OFF_3K_RDOCDT       127-1
#define  OFF_3K_OUTFIXAPN    137-1     // Fixed 8-char of APN - Lake only
#define  OFF_3K_HASSEPTIC    145-1     // Y/N
#define  OFF_3K_TAXFULL      146-1
#define  OFF_3K_NAME         149-1
#define  OFF_3K_CAREOF       199-1
#define  OFF_3K_DBA          249-1
#define  OFF_3K_M_STR        299-1
#define  OFF_3K_M_CITY       349-1
#define  OFF_3K_M_ST         384-1
#define  OFF_3K_M_ZIP        386-1
#define  OFF_3K_S_STR        396-1
#define  OFF_3K_S_STRNUM     420-1
#define  OFF_3K_S_SFX        430-1
#define  OFF_3K_S_DIR        434-1
#define  OFF_3K_S_UNIT       436-1
#define  OFF_3K_S_COMM       440-1
#define  OFF_3K_S_ZIP        444-1
// Lien values
#define  OFF_3K_LAND         454-1
#define  OFF_3K_HOMESITE     463-1
#define  OFF_3K_IMPR         472-1
#define  OFF_3K_GROW         481-1
#define  OFF_3K_FIXT         490-1
#define  OFF_3K_FIXT_RP      499-1
#define  OFF_3K_PERS         508-1
#define  OFF_3K_MHPERS       517-1
// Exe
#define  OFF_3K_EXCNT        526-1
#define  OFF_3K_STAT1        527-1
#define  OFF_3K_EXCD1        528-1
#define  OFF_3K_ISHO1        531-1
#define  OFF_3K_MAXAMT1      532-1
#define  OFF_3K_PCT1         541-1
#define  OFF_3K_STAT2        544-1
#define  OFF_3K_EXCD2        545-1
#define  OFF_3K_ISHO2        548-1
#define  OFF_3K_MAXAMT2      549-1
#define  OFF_3K_PCT2         558-1
#define  OFF_3K_STAT3        561-1
#define  OFF_3K_EXCD3        562-1
#define  OFF_3K_ISHO3        565-1
#define  OFF_3K_MAXAMT3      566-1
#define  OFF_3K_PCT3         575-1
#define  OFF_3K_STAT4        578-1
#define  OFF_3K_EXCD4        579-1
#define  OFF_3K_ISHO4        582-1
#define  OFF_3K_MAXAMT4      583-1
#define  OFF_3K_PCT4         592-1
#define  OFF_3K_STAT5        595-1
#define  OFF_3K_EXCD5        596-1
#define  OFF_3K_ISHO5        599-1
#define  OFF_3K_MAXAMT5      600-1
#define  OFF_3K_PCT5         609-1
// Char
#define  OFF_3K_POOLS        612-1
#define  OFF_3K_USECAT       614-1
#define  OFF_3K_QUALCLS      616-1
#define  OFF_3K_HASSEWER     622-1
#define  OFF_3K_YRBLT        623-1
#define  OFF_3K_BDGSZ        627-1
#define  OFF_3K_GARSZ        636-1
#define  OFF_3K_HEAT         645-1
#define  OFF_3K_COOL         647-1
#define  OFF_3K_HTSRC        649-1
#define  OFF_3K_CLSRC        651-1
#define  OFF_3K_BEDS         654-1
#define  OFF_3K_FBATHS       658-1
#define  OFF_3K_HBATHS       662-1
#define  OFF_3K_FIREPL       666-1
// Sales
#define  OFF_3K_TOTSALES     670-1
#define  OFF_3K_SALECNT      672-1

#define  OFF_3K_SALESTART    673-1
#define  OFF_3K_DOCNO1       673-1
#define  OFF_3K_EVENTDT1     685-1
#define  OFF_3K_DOCCD1       695-1
#define  OFF_3K_SELLER1      697-1
#define  OFF_3K_BUYER1       747-1
#define  OFF_3K_CONFPRC1     797-1
#define  OFF_3K_DOCTAX1      810-1
#define  OFF_3K_GRPSL1       821-1
#define  OFF_3K_GRPASMT1     822-1
#define  OFF_3K_TRANTYP1     834-1
#define  OFF_3K_ADJRSNC1     836-1
#define  OFF_3K_CONFCD1      838-1
#define  OFF_3K_DOCNO2       840-1
#define  OFF_3K_EVENTDT2     852-1
#define  OFF_3K_DOCCD2       862-1
#define  OFF_3K_SELLER2      864-1
#define  OFF_3K_BUYER2       914-1
#define  OFF_3K_CONFPRC2     964-1
#define  OFF_3K_DOCTAX2      977-1
#define  OFF_3K_GRPSL2       988-1
#define  OFF_3K_GRPASMT2     989-1
#define  OFF_3K_TRANTYP2     1001-1
#define  OFF_3K_ADJRSNC2     1003-1
#define  OFF_3K_CONFCD2      1005-1
#define  OFF_3K_DOCNO3       1007-1
#define  OFF_3K_EVENTDT3     1019-1
#define  OFF_3K_DOCCOD3      1029-1
#define  OFF_3K_SELLER3      1031-1
#define  OFF_3K_BUYER3       1081-1
#define  OFF_3K_CONFPR3      1131-1
#define  OFF_3K_DOCTAX3      1144-1
#define  OFF_3K_GRPSL3       1155-1
#define  OFF_3K_GRPASMT3     1156-1
#define  OFF_3K_TRANTYP3     1168-1
#define  OFF_3K_ADJRSNC3     1170-1
#define  OFF_3K_CONFCD3      1172-1
// Sequential number
#define  OFF_3K_RECNO        1174-1
// Tax
#define  OFF_3K_TAXAMT1      1180-1
#define  OFF_3K_TAXAMT2      1191-1
#define  OFF_3K_PEN1         1202-1
#define  OFF_3K_PEN2         1213-1
#define  OFF_3K_PENDT1       1224-1
#define  OFF_3K_PENDT2       1234-1
#define  OFF_3K_PAIDAMT1     1244-1
#define  OFF_3K_PAIDAMT2     1255-1
#define  OFF_3K_TOTPAID1     1266-1
#define  OFF_3K_TOTPAID2     1277-1
#define  OFF_3K_TOTFEES      1288-1
#define  OFF_3K_PAIDDT1      1299-1
#define  OFF_3K_PAIDDT2      1309-1
#define  OFF_3K_TAXYEAR      1319-1
#define  OFF_3K_ROLLCAT      1323-1
// Flags, calculated/derived amounts
#define  OFF_3K_MAILBRK      1325-1    // "B" OR "N" OR BLANK IF NO MAIL STRT
#define  OFF_3K_MAILCA       1326-1    // MAIL ADDR IN CALIFORNIA - Y" OR "N"
#define  OFF_3K_MAILUSA      1327-1    // MAIL ADDR IN USA - "Y" OR "N"
#define  OFF_3K_HAVESITUS    1328-1    // "S" = HAVE SITUS
#define  OFF_3K_SITUSBRK     1329-1    // "B" = SITUS BROKEN
#define  OFF_3K_HAVESALE     1330-1    // "S" = HAVE SALE AMT
#define  OFF_3K_EXCPFLG      1331-1    // "E" = SALE AMT > GROSS AMT
#define  OFF_3K_GROSS        1332-1
#define  OFF_3K_TOTEX        1341-1
#define  OFF_3K_NET          1350-1
#define  OFF_3K_LANDIMP      1359-1
#define  OFF_3K_IMPPERC      1368-1    // IMP % OF LAND + IMP
#define  OFF_3K_DRV_ACRES    1371-1    // 9(6).999
#define  OFF_3K_LOTSQFT      1381-1
#define  OFF_3K_SALEAMT      1390-1
#define  OFF_3K_EXCPAMT      1399-1    // (SALEAMT - GROSS), IF (SALEAMT > GROSS)
#define  OFF_3K_LVALACRE     1408-1    // 9(7).99 - LAND VALUE / ACRE
#define  OFF_3K_LVALSQFT     1418-1    // 9(7).99 - LAND VALUE / LOT SQ FT
#define  OFF_3K_IVALSQFT     1428-1    // 9(7).99 - IMPROV VAL / IMP SQ FT
#define  OFF_3K_SVALSQFT     1438-1    // 9(7).99 - SALE VAL / IMP SQ FT
// Situs - formatted
#define  OFF_3K_FS_STRNO     1448-1
#define  OFF_3K_FS_FRA       1455-1
#define  OFF_3K_FS_DIR       1458-1
#define  OFF_3K_FS_STR       1460-1
#define  OFF_3K_FS_SFX       1490-1
#define  OFF_3K_FS_UNIT      1495-1
#define  OFF_3K_FS_TONUM     1503-1
#define  OFF_3K_FS_CITY      1509-1
#define  OFF_3K_FM_STRNO     1526-1
#define  OFF_3K_FM_FRA       1533-1
#define  OFF_3K_FM_DIR       1536-1
#define  OFF_3K_FM_STR       1538-1
#define  OFF_3K_FM_SFX       1568-1
#define  OFF_3K_FM_UNIT      1573-1
#define  OFF_3K_FM_TONUM     1581-1
#define  OFF_3K_FM_CITY      1587-1
#define  OFF_3K_FM_ST        1604-1
#define  OFF_3K_FM_ZIP       1606-1    // Zip+Zip4
#define  OFF_3K_ADRFLG       1615-1
// Massage name
#define  OFF_3K_MSGNAME1     1616-1    // Name1 after massage
#define  OFF_3K_NORMNAME1    1666-1    // Normalize name1
#define  OFF_3K_NRMCLNNAME1  1691-1    // Cleaned and normalize name1
#define  OFF_3K_SWAPNAME1    1741-1    // Swapped name1 first middle last
#define  OFF_3K_SALCODE      1791-1    // Salutation code
#define  OFF_3K_SALUTATION   1793-1
#define  OFF_3K_NAMESFXCD    1797-1
#define  OFF_3K_NAMESFX      1799-1
#define  OFF_3K_NAMEFLG      1802-1
// Current assessed values
#define  OFF_3K_CLAND        1803-1
#define  OFF_3K_CHOMESITE    1812-1
#define  OFF_3K_CIMPR        1821-1
#define  OFF_3K_CGROW        1830-1
#define  OFF_3K_CFIXT        1839-1
#define  OFF_3K_CFIXT_RP     1848-1
#define  OFF_3K_CPERS        1857-1
#define  OFF_3K_CMHPERS      1866-1
// Tax 2-6
#define  OFF_3K_TX2AMT1      1875-1
#define  OFF_3K_TX2AMT2      1886-1
#define  OFF_3K_TX2PEN1      1897-1
#define  OFF_3K_TX2PEN2      1908-1
#define  OFF_3K_TX2PENCGDT1  1919-1
#define  OFF_3K_TX2PENCHDT2  1929-1
#define  OFF_3K_TX2PAIDAMT1  1939-1
#define  OFF_3K_TX2PAIDAMT2  1950-1
#define  OFF_3K_TX2TOTPD1    1961-1
#define  OFF_3K_TX2TOTPD2    1972-1
#define  OFF_3K_TX2TOTAL     1983-1
#define  OFF_3K_TX2PDDT1     1994-1
#define  OFF_3K_TX2PDDT2     2004-1
#define  OFF_3K_TX2YEAR      2014-1
#define  OFF_3K_TX2CAT       2018-1
#define  OFF_3K_TX3AMT1      2020-1
#define  OFF_3K_TX3AMT2      2031-1
#define  OFF_3K_TX3PEN1      2042-1
#define  OFF_3K_TX3PEN2      2053-1
#define  OFF_3K_TX3PENCGDT1  2064-1
#define  OFF_3K_TX3PENCGDT2  2074-1
#define  OFF_3K_TX3PAIDAMT1  2084-1
#define  OFF_3K_TX3PAIDAMT2  2095-1
#define  OFF_3K_TX3TOTPD1    2106-1
#define  OFF_3K_TX3TOTPD2    2117-1
#define  OFF_3K_TX3TOTAL     2128-1
#define  OFF_3K_TX3PDDT1     2139-1
#define  OFF_3K_TX3PDDT2     2149-1
#define  OFF_3K_TX3YEAR      2159-1
#define  OFF_3K_TX3CAT       2163-1
#define  OFF_3K_TX4AMT1      2165-1
#define  OFF_3K_TX4AMT2      2176-1
#define  OFF_3K_TX4PEN1      2187-1
#define  OFF_3K_TX4PEN2      2198-1
#define  OFF_3K_TX4PENCGDT1  2209-1
#define  OFF_3K_TX4PENCGDT2  2219-1
#define  OFF_3K_TX4PAIDAMT1  2229-1
#define  OFF_3K_TX4PAIDAMT2  2240-1
#define  OFF_3K_TX4TOTPD1    2251-1
#define  OFF_3K_TX4TOTPD2    2262-1
#define  OFF_3K_TX4TOTAL     2273-1
#define  OFF_3K_TX4PDDT1     2284-1
#define  OFF_3K_TX4PDDT2     2294-1
#define  OFF_3K_TX4YEAR      2304-1
#define  OFF_3K_TX4CAT       2308-1
#define  OFF_3K_TX5AMT1      2310-1
#define  OFF_3K_TX5AMT2      2321-1
#define  OFF_3K_TX5PEN1      2332-1
#define  OFF_3K_TX5PEN2      2343-1
#define  OFF_3K_TX5PENCGDT1  2354-1
#define  OFF_3K_TX5PENCGDT2  2364-1
#define  OFF_3K_TX5PAIDAMT1  2374-1
#define  OFF_3K_TX5PAIDAMT2  2385-1
#define  OFF_3K_TX5TOTPD1    2396-1
#define  OFF_3K_TX5TOTPD2    2407-1
#define  OFF_3K_TX5TOTAL     2418-1
#define  OFF_3K_TX5PDDT1     2429-1
#define  OFF_3K_TX5PDDT2     2439-1
#define  OFF_3K_TX5YEAR      2449-1
#define  OFF_3K_TX5CAT       2453-1
#define  OFF_3K_TX6AMT1      2455-1
#define  OFF_3K_TX6AMT2      2466-1
#define  OFF_3K_TX6PEN1      2477-1
#define  OFF_3K_TX6PEN2      2488-1
#define  OFF_3K_TX6PENCGDT1  2499-1
#define  OFF_3K_TX6PENCGDT2  2509-1
#define  OFF_3K_TX6PAIDAMT1  2519-1
#define  OFF_3K_TX6PAIDAMT2  2530-1
#define  OFF_3K_TX6TOTPD1    2541-1
#define  OFF_3K_TX6TOTPD2    2552-1
#define  OFF_3K_TX6TOTAL     2563-1
#define  OFF_3K_TX6PPDT1     2574-1
#define  OFF_3K_TX6PPDT2     2584-1
#define  OFF_3K_TX6YEAR      2594-1
#define  OFF_3K_TX6CAT       2598-1
#define  OFF_3K_HASWELL      2600-1    // Y/N
#define  OFF_3K_G_G_COUNT    2601-1
#define  OFF_3K_1DT          2602-1
#define  OFF_3K_1DOC         2610-1
#define  OFF_3K_1TYPE        2626-1
#define  OFF_3K_1AMT         2636-1
#define  OFF_3K_1GRANTOR1    2646-1
#define  OFF_3K_1GRANTOR2    2666-1
#define  OFF_3K_1GRANTEE1    2686-1
#define  OFF_3K_1GRANTEE2    2706-1
#define  OFF_3K_1APN_MCH     2726-1
#define  OFF_3K_1NAME_MCH    2727-1
#define  OFF_3K_2DT          2728-1
#define  OFF_3K_2DOC         2736-1
#define  OFF_3K_2TYPE        2752-1
#define  OFF_3K_2AMT         2762-1
#define  OFF_3K_2GRANTOR1    2772-1
#define  OFF_3K_2GRANTOR2    2792-1
#define  OFF_3K_2GRANTEE1    2812-1
#define  OFF_3K_2GRANTEE2    2832-1
#define  OFF_3K_2APN_MCH     2852-1
#define  OFF_3K_2NAME_MCH    2853-1
#define  OFF_3K_3DT          2854-1
#define  OFF_3K_3DOC         2862-1
#define  OFF_3K_3TYPE        2878-1
#define  OFF_3K_3AMT         2888-1
#define  OFF_3K_3GRANTOR1    2898-1
#define  OFF_3K_3GRANTOR2    2918-1
#define  OFF_3K_3GRANTEE1    2938-1
#define  OFF_3K_3GRANTEE2    2958-1
#define  OFF_3K_3APN_MCH     2978-1
#define  OFF_3K_3NAME_MCH    2979-1
#define  OFF_3K_APN_DASH     2980-1
#define  OFF_3K_LASTSALEDT   2995-1
#define  OFF_3K_LASTSALEAMT  3003-1

#define  SIZ_3K_CO_ID        3
#define  SIZ_3K_CO_NUM       2
#define  SIZ_3K_ASSMT        9
#define  SIZ_3K_APN          9
#define  SIZ_3K_TRA          6
#define  SIZ_3K_DESC         50
#define  SIZ_3K_ZONE         8
#define  SIZ_3K_USE          8
#define  SIZ_3K_NBHCD        4
#define  SIZ_3K_INACRES      9
#define  SIZ_3K_RDOCNUM      12
#define  SIZ_3K_RDOCDT       10
#define  SIZ_3K_OUTFIXAPN    8
#define  SIZ_3K_TAXFULL      3
#define  SIZ_3K_NAME         50
#define  SIZ_3K_CAREOF       50
#define  SIZ_3K_DBA          50
#define  SIZ_3K_M_STR        50
#define  SIZ_3K_M_CITY       35
#define  SIZ_3K_M_ST         2
#define  SIZ_3K_M_ZIP        10
#define  SIZ_3K_S_STR        24
#define  SIZ_3K_S_STRNUM     10
#define  SIZ_3K_S_SFX        4
#define  SIZ_3K_S_DIR        2
#define  SIZ_3K_S_UNIT       4
#define  SIZ_3K_S_COMM       4
#define  SIZ_3K_S_ZIP        10
#define  SIZ_3K_LAND         9
#define  SIZ_3K_HOMESITE     9
#define  SIZ_3K_IMPR         9
#define  SIZ_3K_GROW         9
#define  SIZ_3K_FIXT         9
#define  SIZ_3K_FIXTRL       9
#define  SIZ_3K_PERS         9
#define  SIZ_3K_MHPERS       9

#define  SIZ_3K_EXCNT        1
#define  SIZ_3K_STATUS       1
#define  SIZ_3K_EXECD        3
#define  SIZ_3K_HOFLG        1
#define  SIZ_3K_EXEAMT       9
#define  SIZ_3K_EXEPCT       3

#define  SIZ_3K_POOLS        2
#define  SIZ_3K_USECAT       2
#define  SIZ_3K_QUALCLS      6
#define  SIZ_3K_YRBLT        4
#define  SIZ_3K_BDGSZ        9
#define  SIZ_3K_GARSZ        9
#define  SIZ_3K_HEAT         2
#define  SIZ_3K_COOL         2
#define  SIZ_3K_HTSRC        2
#define  SIZ_3K_CLSRC        3
#define  SIZ_3K_BEDS         4
#define  SIZ_3K_FBATHS       4
#define  SIZ_3K_HBATHS       4
#define  SIZ_3K_FIREPL       4

#define  SIZ_3K_TOTSALES     2

#define  SIZ_3K_SLCNT        1
#define  SIZ_3K_SDOCNUM      12
#define  SIZ_3K_SALEDATE     10
#define  SIZ_3K_SALECODE     2
#define  SIZ_3K_SELLER       50
#define  SIZ_3K_BUYER        50
#define  SIZ_3K_CONFPRICE    13
#define  SIZ_3K_DOCTAX       11
#define  SIZ_3K_GRPSALE      1
#define  SIZ_3K_GRPASMT      12
#define  SIZ_3K_TRANSTYPE    2
#define  SIZ_3K_AR_CODE      2
#define  SIZ_3K_CONFCODE     2

#define  SIZ_3K_RECNO        6

#define  SIZ_3K_TAXAMT       11
#define  SIZ_3K_PENAMT       11
#define  SIZ_3K_PENDATE      10
#define  SIZ_3K_PAIDAMT      11
#define  SIZ_3K_TOTPAID      11
#define  SIZ_3K_TOTALFEES    11
#define  SIZ_3K_PAIDDATE     10
#define  SIZ_3K_TAXYEAR      4
#define  SIZ_3K_ROLLCAT      2

#define  SIZ_3K_OUTMSW       1
#define  SIZ_3K_MAILBUT      1
#define  SIZ_3K_MAILUSA      1
#define  SIZ_3K_SITFLAG      1
#define  SIZ_3K_BRKFLAG      1
#define  SIZ_3K_SALEFLAG     1
#define  SIZ_3K_EXCPFLAG     1
#define  SIZ_3K_GROSS        9
#define  SIZ_3K_TOTEX        9
#define  SIZ_3K_NET          9
#define  SIZ_3K_LANDIMP      9
#define  SIZ_3K_IMPPERC      3
#define  SIZ_3K_DRV_ACRES   10
#define  SIZ_3K_LOTSQFT      9
#define  SIZ_3K_SALEAMT      9
#define  SIZ_3K_EXCPAMT      9
#define  SIZ_3K_LVALACRE     10
#define  SIZ_3K_LVALSQFT     10
#define  SIZ_3K_IVALSQFT     10
#define  SIZ_3K_SVALSQFT     10

#define  SIZ_3K_FS_STRNO     7
#define  SIZ_3K_FS_FRA       3
#define  SIZ_3K_FS_DIR       2
#define  SIZ_3K_FS_STR       30
#define  SIZ_3K_FS_SFX       5
#define  SIZ_3K_FS_UNIT      8
#define  SIZ_3K_FS_TONUM     6
#define  SIZ_3K_FS_CITY      17
#define  SIZ_3K_FM_STRNO     7
#define  SIZ_3K_FM_FRA       3
#define  SIZ_3K_FM_DIR       2
#define  SIZ_3K_FM_STR       30
#define  SIZ_3K_FM_SFX       5
#define  SIZ_3K_FM_UNIT      8
#define  SIZ_3K_FM_TONUM     6
#define  SIZ_3K_FM_CITY      17
#define  SIZ_3K_FM_ST        2
#define  SIZ_3K_FM_ZIP       9    // Zip+Zip4
#define  SIZ_3K_OUTADRFL     1

#define  SIZ_3K_MSGNAME1     50
#define  SIZ_3K_NORMNAME1    25
#define  SIZ_3K_NRMCLNNAME1  50
#define  SIZ_3K_SWAPNAME1    50
#define  SIZ_3K_SALCODE      2
#define  SIZ_3K_SALUTATION   4
#define  SIZ_3K_NAMESFXCD    2
#define  SIZ_3K_NAMESFX      3
#define  SIZ_3K_NAMEFLG      1

#define  SIZ_3K_CLAND        9
#define  SIZ_3K_CHOMESITE    9
#define  SIZ_3K_CSTRUC       9
#define  SIZ_3K_CGROW        9
#define  SIZ_3K_CFIXT        9
#define  SIZ_3K_CFIXTRL      9
#define  SIZ_3K_CPERS        9
#define  SIZ_3K_CMHPERS      9

#define  SIZ_3K_HAVE_WELL    1
#define  SIZ_3K_G_G_COUNT    1

#define  SIZ_3K_GDOCDATE     8
#define  SIZ_3K_GDOCNUM      16
#define  SIZ_3K_GDOCTYPE     10
#define  SIZ_3K_GSALEPRICE   10
#define  SIZ_3K_GRANTOR      20

#define  SIZ_3K_APN_DASH     15
#define  SIZ_3K_LASTSALEDT   8
#define  SIZ_3K_LASTSALEAMT  10

#define  MAX_EXE_RECS        5
#define  MAX_TAX_RECS        6
#define  MAX_SALE_RECS       3
#define  MAX_GRGR_RECS       3

typedef struct _tMBExe
{
   char  Status;                 // Asmt status
   char  Code[SIZ_3K_EXECD];     // Exemption code
   char  HO_Flg;                 // HO flag
   char  MaxAmt[SIZ_3K_EXEAMT];  // Max exemption amount
   char  Percent[SIZ_3K_EXEPCT]; // Percent exempt
} MB_EXE;

typedef struct _tMBSale
{
   char  DocNum[SIZ_3K_SDOCNUM];
   char  SaleDate[SIZ_3K_SALEDATE];
   char  SaleCode[SIZ_3K_SALECODE];
   char  Seller[SIZ_3K_SELLER];
   char  Buyer[SIZ_3K_BUYER];
   char  SalePrice[SIZ_3K_CONFPRICE];
   char  DocTax[SIZ_3K_DOCTAX];
   char  GroupSale;
   char  GroupAsmt[SIZ_3K_GRPASMT];
   char  TransType[SIZ_3K_TRANSTYPE];
   char  AdjReasonCode[SIZ_3K_AR_CODE];
   char  ConfirmCode[SIZ_3K_CONFCODE];
} MB_SALE;

typedef struct _tMBTax
{
   char  TaxAmt1[SIZ_3K_TAXAMT];
   char  TaxAmt2[SIZ_3K_TAXAMT];
   char  PenAmt1[SIZ_3K_PENAMT];
   char  PenAmt2[SIZ_3K_PENAMT];
   char  PenChrgDt1[SIZ_3K_PENDATE];
   char  PenChrgDt2[SIZ_3K_PENDATE];
   char  PaidAmt1[SIZ_3K_PAIDAMT];
   char  PaidAmt2[SIZ_3K_PAIDAMT];
   char  TotPaid1[SIZ_3K_TOTPAID];
   char  TotPaid2[SIZ_3K_TOTPAID];
   char  TotFees[SIZ_3K_TOTALFEES];
   char  PaidDt1[SIZ_3K_PAIDDATE];
   char  PaidDt2[SIZ_3K_PAIDDATE];
   char  TaxYear[SIZ_3K_TAXYEAR];
   char  RollCat[SIZ_3K_ROLLCAT];
} MB_TAX;

typedef struct _tMBGrGr
{
   char  Apn[SIZ_3K_APN];
   char  DocDate[SIZ_3K_GDOCDATE];
   char  DocNum[SIZ_3K_GDOCNUM];
   char  DocType[SIZ_3K_GDOCTYPE];
   char  SalePrice[SIZ_3K_GSALEPRICE];
   char  Grantors[2][SIZ_3K_GRANTOR];
   char  Grantees[2][SIZ_3K_GRANTOR];
   char  ApnMatch;
   char  OwnerMatch;
   char  CRLF[2];
} MB_GRGR;
#define  GRGR_SIZE   (sizeof(MB_GRGR)-(SIZ_3K_APN+2))
*/

int  MB_CreateAssr(int iSkip=1);
int  MB_MergeAssr(int iSkip=1);
int  MB_CreateAssr1(int iSkip=1);
int  MB_MergeAssr1(int iSkip=1);
int  MB_CreateAssrTR601(int iSkip=1, int iInputType=0);  // 0=comma delimited, 1=tab delimited

#endif