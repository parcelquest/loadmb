#include "stdafx.h"
#include "logs.h"
#include "CountyInfo.h"

#define  _FORMAT_APN
#include "FormatApn.h"

int atoin(char *pStr, int iLen, bool bRemComma=false);
extern   char  acIniFile[];

/********************************* formatVen *********************************
 *
 * Return: buffer length
 *
 *****************************************************************************/

int formatVen(char *pApn, char *pFmtApn)
{
   int   iTmp;
   char  cTmp;

   cTmp = *(pApn+9);
   iTmp = sprintf(pFmtApn, "%.3s-%c-%.3s-%.3s", pApn, cTmp, pApn+3, pApn+6);
   
   return iTmp;
}

/********************************* formatSut *********************************
 *
 * Return: buffer length
 *
 *****************************************************************************/

int formatSut(char *pApn, char *pFmtApn)
{
   int   iTmp;

   iTmp = sprintf(pFmtApn, "%.3s-%.3s-%.3s-%s", pApn, pApn+3, pApn+6, pApn+9);
   
   return iTmp;
}

/********************************* formatApn *********************************
 *
 * Return: buffer length
 * 09/02/2008 Modify filter for "- " to fix APN format for SUT.
 * 05/01/2012 Fix bug for SCR.  Return correct APN length.
 * 07/31/2023 Modify formatApn() not to format beyond original APN length.
 * 08/07/2023 Add special case for SUT
 *
 *****************************************************************************/

int formatApn(char *pApn, char *pFmtApn, COUNTY_INFO *pCounty)
{
   char acApn[32], acTmp[40], acMask[40], *pTmp;
   int  i, iCnt, iLen;  

   if (pCounty->iCntyID == 51)            // SUT
   {
      iCnt = formatSut(pApn, pFmtApn);
      return iCnt;
   }

   if (pCounty->iCntyID == 56)            // VEN
   {
      iCnt = formatVen(pApn, pFmtApn);
      return iCnt;
   }

   if (pCounty->iCntyID == 30)            // ORG
   {
      if (memcmp(pApn, pCounty->acCase[0], 3) >= 0)
      {
         if (memcmp(pApn, pCounty->acCase[1], 3) >= 0)
            strcpy(acMask, pCounty->acSpcApnFmt[1]);
         else
            strcpy(acMask, pCounty->acSpcApnFmt[0]);
      } else
         strcpy(acMask, pCounty->acStdApnFmt);
   } else
      strcpy(acMask, pCounty->acStdApnFmt);
   memcpy(acApn, pApn, pCounty->iApnLen);
   acApn[pCounty->iApnLen] = 0;
   iLen = pCounty->iApnLen-1;

   // Remove trailing space
   while (acApn[iLen] == ' ')
      acApn[iLen--] = 0;

   iCnt = 0;
   for (i = 0; i <= iLen; i++)
   {
      if (!acApn[i])
         break;

      if (acMask[iCnt] == '-' && acTmp[iCnt-1] != '-')
         acTmp[iCnt++] = '-';

      // Y is used instead of A to drop space holder for alpha within APN
      if (acMask[iCnt] == 'Y')
      {
         if (acApn[i] != ' ')
            acTmp[iCnt++] = acApn[i];
      } else
         acTmp[iCnt++] = acApn[i];
   }
   acTmp[iCnt] = 0;

#ifdef _DEBUG
   //if (!memcmp(pApn, "33110061", 8) )
   //   i = 0;
#endif
   // Check for optional part
   if (acMask[iCnt-1] != '9')
   {
      if (acTmp[iCnt-1] == '-')
         acTmp[--iCnt] = 0;
      else
      {
         pTmp = strrchr(acTmp, '-');
         pTmp++;
         i = atoi(pTmp);
         if (i == 0 && *pTmp == '0')
            *(--pTmp) = 0;

         // Remove last "- " if there is nothing after that.
         if (pTmp = strstr(acTmp, "- "))
         {
            if (*(pTmp+2) > ' ')
            {
               while (*pTmp)
                  pTmp++;
            } else
               *pTmp = 0;
         }

         iCnt = strlen(acTmp);
      }
   } else
   {
      iCnt = strlen(acTmp);
      if (acTmp[iCnt-1] == '-')
      {
         iCnt--;
         acTmp[iCnt] = 0;
      }
   }

   strcpy(pFmtApn, acTmp);
   return iCnt;
}

/********************************** formatMapLink ***************************
 *
 * 12/06/2021 Add special case for SBX 097\100
 * Return: buffer length
 *
 ****************************************************************************/

int formatMapLink(char *pApn, char *pMapLink, COUNTY_INFO *pCounty)
{
   char  acBook[8], acPage[8], acTmp[64];
   int   iBook, iPage, iTmp, iPageLen, iRet;

   // Default page length
   iPageLen = pCounty->iPageLen;

   memcpy(acBook, pApn, pCounty->iBookLen);
   memcpy(acPage, pApn+pCounty->iBookLen, iPageLen);

   // Check for blank in book field
   if (acBook[pCounty->iBookLen-1] == ' ')
      acBook[pCounty->iBookLen-1] = 0;
   else
      acBook[pCounty->iBookLen] = 0;

   iBook = atoi(acBook);

   if (!memcmp(pCounty->acCntyCode, "ALA", 3))
   {
      if (!memcmp(acBook, "0000", 4))
         acBook[3] = 0;
   } else if (!memcmp(pCounty->acCntyCode, "FRE", 3))
   {
      if (iBook >= 700 && iBook < 800)
      {
         iPageLen = 3;
         memcpy(acPage, pApn+pCounty->iBookLen, iPageLen);
      }
   } else if (!memcmp(pCounty->acCntyCode, "LAK", 3))
   {
      iPageLen = 3;
      memcpy(acPage, pApn+pCounty->iBookLen, iPageLen);
      for (iTmp = 0; iTmp < MAXLAKEBLK; iTmp++)
      {
         if (memcmp(gLakeApnBk[iTmp].pApn, pApn, pCounty->iApnLen) > 0)
            break;
         iPageLen = gLakeApnBk[iTmp].iPageLen;
      }
      // This is temporary work to make both new and old applications work together.
      // The correct page length should be 3 digits.  Will revisit this area later.
      if (iPageLen == 3)
      {
         // Remove leading 0
         if (acPage[0] == '0')
         {
            acPage[0] = acPage[1];
            acPage[1] = acPage[2];
         } else
            LogMsg("##### LAK format logic is broken.  This function needs revision [%.8s]", pApn);
         iPageLen = 2;
      }
   } else if (!memcmp(pCounty->acCntyCode, "LAS", 3))
   {
      if (iBook == 999)
      {
         iPageLen = 3;
         memcpy(acPage, pApn+pCounty->iBookLen, iPageLen);
      }
   } else if (!memcmp(pCounty->acCntyCode, "NEV", 3))
   {
      if (iBook < 99)
      {
         sprintf(acBook, "%.2d", iBook);
      }
   } else if (!memcmp(pCounty->acCntyCode, "SBX", 3))
   {
      if (iBook == 4 || iBook == 98 || iBook == 118 || iBook == 128 || (iBook > 499 && iBook < 753) )
      {
         iPageLen = 3;
         memcpy(acPage, pApn+pCounty->iBookLen, iPageLen);
      } else if (iBook == 97)
      {
         iPage = atoin(pApn+pCounty->iBookLen, iPageLen+1);
         if (iPage == 100)
         {
            iPageLen = 3;
            memcpy(acPage, pApn+pCounty->iBookLen, iPageLen);
         }
      }
   } else if (!memcmp(pCounty->acCntyCode, "SDX", 3))
   {
      if (iBook == 760)
      {
         iPageLen = 3;
         memcpy(acPage, pApn+pCounty->iBookLen, iPageLen);
      }
   } else if (!memcmp(pCounty->acCntyCode, "SFX", 3))
   {
      if (acPage[2] < 'A' || acPage[2] > 'Z')
         iPageLen = 2;
   } else if (!memcmp(pCounty->acCntyCode, "SLO", 3))
   {
      /*
      memcpy(acTmp, pApn, 6);
      acTmp[6] = 0;
      iPageLen = 0;
      for (iTmp = g_aBkNdx[iBook].iStart; iTmp < g_aBkNdx[iBook].iEnd; iTmp++)
      {
         iRet = memcmp(pApn, g_asBkPg[iTmp], 6);
         if (!iRet)
         {
            iPageLen = 3;
            break;
         }
         if (iRet < 0)
            break;
      }

      if (iRet)
      {
         acTmp[5] = 0;
         for (iTmp = g_aBkNdx[iBook].iStart; iTmp < g_aBkNdx[iBook].iEnd; iTmp++)
         {
            iRet = memcmp(pApn, g_asBkPg[iTmp], 5);
            if (!iRet)
            {
               iPageLen = 2;
               break;
            }
            if (iRet < 0)
               break;
         }
      }

      // If no map found, we default to 2-dogit page and look for that map later.
      if (!iPageLen)
      {
         LogMsg("Missing map: %s", acTmp);
         iPageLen = 2;
      }
      */
   }

   // Standard formating
   acPage[iPageLen] = 0;
   iRet = sprintf(acTmp, "%s\\%s%s  ", acBook, acBook, acPage);
   strcpy(pMapLink, acTmp);

   return iRet;
}

/********************************* loadMapIndex ******************************
 *
 * Return: Number of entries if OK, 0 or less means error
 *
 *****************************************************************************/

int loadMapIndex(char *pCntyCode, char *pMapList)
{
   char  acTmp[_MAX_PATH], *pTmp, acMapList[_MAX_PATH];
   int   iBook, iCurBook=-1, iCnt=0;
   FILE  *fd;

   if (!pMapList)
   {
      GetPrivateProfileString(pCntyCode, "MAPLST", "", acMapList, _MAX_PATH, acIniFile);
      if (!acMapList[0])
         return -1;
   } else
      strcpy(acMapList, pMapList);

   fd = fopen(acMapList, "r");
   if (fd)
   {
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, 8, fd);
         if (pTmp)
         {
            iBook = atoin(acTmp, 3);
            if (iBook != iCurBook)
            {
               if (iCnt != 0)
                  g_aBkNdx[iCurBook].iEnd = iCnt;
               iCurBook = iBook;
               g_aBkNdx[iBook].iBook = iBook;
               g_aBkNdx[iBook].iStart = iCnt;
            }

            // Save entry
            strcpy(g_asBkPg[iCnt++], acTmp);

         } else
         {
            g_aBkNdx[iCurBook].iEnd = iCnt;
            break;
         }
      }

      fclose(fd);

      if (iCnt < 100)
         LogMsg("Map list not found.  Please verify %s", acMapList);
   } else
      LogMsg("Error opening MapList file %s", acMapList);

   return iCnt;
}

/*****************************************************************************
 *
 * Input  = MapLink
 * Output = Index page
 *
 * To convert MAPLINK into index page, all you have to do is turn all page 
 * digits into "0"
 * ex: MAPLINK="001\00101", Index page="001\00100"
 *             "123\12305", Index page="123\12300"
 *             "029A\029A1301", Index page="029A\029A0000"
 *             "000\0000300" -> "000\0000
 *
 *****************************************************************************/

char *getIndexPage(char *pMapLink, char *pIndexPage, COUNTY_INFO *pCounty)
{
   char  *pTmp;
   int   iBookLen;

   strcpy(pIndexPage, pMapLink);
   
   pTmp = strchr(pIndexPage, '\\');
   iBookLen = pTmp - pIndexPage;  // find book len
   pTmp = pIndexPage+(iBookLen<<1)+1;
   while (*pTmp >= '0')
   {
      *pTmp = '0';
      pTmp++;
   }
   return pIndexPage;
}