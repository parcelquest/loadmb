#ifndef _MERGEMNO_H_
#define _MERGEMNO_H_

#define MNO_CHAR_FEEPARCEL                0
#define MNO_CHAR_POOLSPA                  1
#define MNO_CHAR_CATTYPE                  2
#define MNO_CHAR_QUALITYCLASS             3
#define MNO_CHAR_YRBLT                    4
#define MNO_CHAR_BUILDINGSIZE             5
#define MNO_CHAR_ATTACHGARAGESF           6
#define MNO_CHAR_DETACHGARAGESF           7
#define MNO_CHAR_CARPORTSF                8
#define MNO_CHAR_HEATING                  9
#define MNO_CHAR_COOLINGCENTRALAC         10
#define MNO_CHAR_COOLINGEVAPORATIVE       11
#define MNO_CHAR_COOLINGROOMWALL          12
#define MNO_CHAR_COOLINGWINDOW            13
#define MNO_CHAR_STORIESCNT               14
#define MNO_CHAR_UNITSCNT                 15
#define MNO_CHAR_TOTALROOMS               16
#define MNO_CHAR_EFFYR                    17
#define MNO_CHAR_PATIOSF                  18
#define MNO_CHAR_BEDROOMS                 19
#define MNO_CHAR_BATHROOMS                20
#define MNO_CHAR_HALFBATHS                21
#define MNO_CHAR_FIREPLACE                22
#define MNO_CHAR_ASMT                     23
#define MNO_CHAR_BLDGSEQNUM               24
#define MNO_CHAR_HASWELL                  25
#define MNO_CHAR_LOTSQFT                  26

// Old layout
#define MNO_GR_PRSERV							0
#define MNO_GR_SOURCETABLE						1		
#define MNO_GR_APN_NUMBER						2
#define MNO_GR_GRANTOR							3
#define MNO_GR_GRANTEE							4
#define MNO_GR_INSTRUMENT_TYPE				5				
#define MNO_GR_OR_PRFOLDER						6
#define MNO_GR_OR_INSTRUMENT_NUMBER			7			
#define MNO_GR_OR_INSTRUMENT_DATE			8
#define MNO_GR_OR_FILE_DATE					9
#define MNO_GR_OR_VOLUME						10
#define MNO_GR_OR_PAGE							11
#define MNO_GR_OR_COMMENTS						12
#define MNO_GR_OR_SUB_BLOCK_LOT				13
#define MNO_GR_OR_TOWN_RANGE_SEC				14
#define MNO_GR_OR_MAP_TYPE						15
#define MNO_GR_OR_MAP_NUMBER					16
#define MNO_GR_OR_RECORDED_MAP_TYPE			17
#define MNO_GR_OR_MAP_BOOK						18
#define MNO_GR_OR_MAP_PAGE						19
#define MNO_GR_OR_PROJECT_NAME				20
#define MNO_GR_OR_STREET						21
#define MNO_GR_AW_PRFOLDER						22
#define MNO_GR_AW_STATUS						23
#define MNO_GR_AW_INSTRUMENT_NUMBER			24
#define MNO_GR_AW_FILE_DATE					25
#define MNO_GR_AW_SIGN_DATE					26
#define MNO_GR_AW_FORWARD						27
#define MNO_GR_AW_PARCEL_COUNT				28
#define MNO_GR_AW_PERCENT_OWNER				29
#define MNO_GR_AW_COMMENTS						30		
#define MNO_GR_AW_INDICATED_PRICE			31
#define MNO_GR_AW_BASE_YEAR					32
#define MNO_GR_AW_DTT							33
#define MNO_GR_AW_MAMMOTH						34
#define MNO_GR_AW_HOX							35
#define MNO_GR_AW_PROFILE						36
#define MNO_GR_AW_BOOK							37
#define MNO_GR_AW_SUBDIVISION					38
#define MNO_GR_AW_CHG_DATE						39
#define MNO_GR_AW_FORM_TYPE					40

//  New layout 04/09/2018
#define MNO_GR_ASMT           1
#define MNO_GR_DOCNUM         2
#define MNO_GR_EVENTDATE      3
#define MNO_GR_DOCCODE        4
#define MNO_GR_TRANSFEROR     5
#define MNO_GR_TRANSFEREE     6
#define MNO_GR_ACRES          7
#define MNO_GR_SIZETYPE       8
#define MNO_GR_DOCTRANTAX     9
#define MNO_GR_LANDUSE1       10
#define MNO_GR_LANDUSE2       11
#define MNO_GR_DEEDAMT1       12
#define MNO_GR_DEEDAMT2       13
#define MNO_GR_XFERTYPE       14
#define MNO_GR_DTS            15
#define MNO_GR_SALEPRICE      16
#define MNO_GR_FLDCNT         17

#define MNOSIZ_GR_PRSERV						8
#define MNOSIZ_GR_SOURCETABLE					2
#define MNOSIZ_GR_APN_NUMBER					17
#define MNOSIZ_GR_GRANTOR						50
#define MNOSIZ_GR_GRANTEE						50
#define MNOSIZ_GR_INSTRUMENT_TYPE			30
#define MNOSIZ_GR_OR_PRFOLDER					30			
#define MNOSIZ_GR_OR_INSTRUMENT_NUMBER		10
#define MNOSIZ_GR_OR_INSTRUMENT_DATE		15
#define MNOSIZ_GR_OR_FILE_DATE				15
#define MNOSIZ_GR_OR_VOLUME					5		
#define MNOSIZ_GR_OR_PAGE						5
#define MNOSIZ_GR_OR_COMMENTS					200
#define MNOSIZ_GR_OR_SUB_BLOCK_LOT			5
#define MNOSIZ_GR_OR_TOWN_RANGE_SEC			5
#define MNOSIZ_GR_OR_MAP_TYPE					5
#define MNOSIZ_GR_OR_MAP_NUMBER				5
#define MNOSIZ_GR_OR_RECORDED_MAP_TYPE		5		
#define MNOSIZ_GR_OR_MAP_BOOK					5
#define MNOSIZ_GR_OR_MAP_PAGE					5
#define MNOSIZ_GR_OR_PROJECT_NAME			30
#define MNOSIZ_GR_OR_STREET					24
#define MNOSIZ_GR_AW_PRFOLDER					30
#define MNOSIZ_GR_AW_STATUS					5
#define MNOSIZ_GR_AW_INSTRUMENT_NUMBER		10
#define MNOSIZ_GR_AW_FILE_DATE				15
#define MNOSIZ_GR_AW_SIGN_DATE				15
#define MNOSIZ_GR_AW_FORWARD					5
#define MNOSIZ_GR_AW_PARCEL_COUNT			5			
#define MNOSIZ_GR_AW_PERCENT_OWNER			5
#define MNOSIZ_GR_AW_COMMENTS					200
#define MNOSIZ_GR_AW_INDICATED_PRICE		10
#define MNOSIZ_GR_AW_BASE_YEAR				4
#define MNOSIZ_GR_AW_DTT						10
#define MNOSIZ_GR_AW_MAMMOTH					10
#define MNOSIZ_GR_AW_HOX						10
#define MNOSIZ_GR_AW_PROFILE					10
#define MNOSIZ_GR_AW_BOOK						10
#define MNOSIZ_GR_AW_SUBDIVISION				200
#define MNOSIZ_GR_AW_CHG_DATE					15
#define MNOSIZ_GR_AW_FORM_TYPE				5

#define MNO_L_ASMT                        0
#define MNO_L_TAXYEAR                     1
#define MNO_L_ROLLCATEGORY                2
#define MNO_L_FEEPARCEL                   3
#define MNO_L_ORIGINATINGASMT             4
#define MNO_L_STATUS                      5
#define MNO_L_TRA                         6
#define MNO_L_TAXABILITY                  7
#define MNO_L_ACRES                       8
#define MNO_L_USECODE                     9
#define MNO_L_CURRENTMARKETLANDVALUE      10
#define MNO_L_CURRENTFIXEDIMPRVALUE       11
#define MNO_L_CURRENTSTRUCTURALIMPRVALUE  12
#define MNO_L_CURRENTPERSONALPROPVALUE    13
#define MNO_L_CURRENTPERSONALPROPMHVALUE  14
#define MNO_L_CURRENTNETVALUE             15
#define MNO_L_BILLEDMARKETLANDVALUE       16
#define MNO_L_BILLEDFIXEDIMPRVALUE        17
#define MNO_L_BILLEDSTRUCTURALIMPRVALUE   18
#define MNO_L_BILLEDPERSONALPROPVALUE     19
#define MNO_L_BILLEDPERSONALPROPMHVALUE   20
#define MNO_L_BILLEDNETVALUE              21
#define MNO_L_OWNER                       22
#define MNO_L_ASSESSEE                    23
#define MNO_L_MAILADDRESS1                24
#define MNO_L_MAILADDRESS2                25
#define MNO_L_MAILADDRESS3                26
#define MNO_L_EXEMPTIONCODE1              27
#define MNO_L_EXEMPTIONAMT1               28
#define MNO_L_EXEMPTIONCODE2              29
#define MNO_L_EXEMPTIONAMT2               30
#define MNO_L_EXEMPTIONCODE3              31
#define MNO_L_EXEMPTIONAMT3               32
#define MNO_L_BILLDATE                    33
#define MNO_L_DUEDATE1                    34
#define MNO_L_DUEDATE2                    35
#define MNO_L_TAXAMT1                     36
#define MNO_L_TAXAMT2                     37
#define MNO_L_PENAMT1                     38
#define MNO_L_PENAMT2                     39
#define MNO_L_COST1                       40
#define MNO_L_COST2                       41
#define MNO_L_COLLECTIONNUM1              42
#define MNO_L_COLLECTIONNUM2              43
#define MNO_L_UNSDELINQPENAMTPAID1        44
#define MNO_L_SECDELINQPENAMTPAID2        45
#define MNO_L_TOTALFEESPAID1              46
#define MNO_L_TOTALFEESPAID2              47
#define MNO_L_TOTALFEES                   48
#define MNO_L_PRORATIONFACTOR             49
#define MNO_L_PRORATIONPCT                50
#define MNO_L_DAYSTOFISCALYEAREND         51
#define MNO_L_DAYSOWNED                   52
#define MNO_L_SITUS1                      53
#define MNO_L_SITUS2                      54
#define MNO_L_ASMTROLLYEAR                55
#define MNO_L_PARCELDESCRIPTION           56
#define MNO_L_BILLCOMMENTSLINE1           57
#define MNO_L_BILLCOMMENTSLINE2           58
#define MNO_L_DEFAULTNUM                  59
#define MNO_L_SECDELINQFISCALYEAR         60
#define MNO_L_PRIORTAXPAID1               61
#define MNO_L_PENINTERESTCODE             62
#define MNO_L_FOURPAYPLANNUM              63
#define MNO_L_EXISTSLIEN                  64
#define MNO_L_EXISTSCORTAC                65
#define MNO_L_EXISTSCOUNTYCORTAC          66
#define MNO_L_EXISTSBANKRUPTCY            67
#define MNO_L_EXISTSREFUND                68
#define MNO_L_EXISTSDELINQUENTVESSEL      69
#define MNO_L_EXISTSNOTES                 70
#define MNO_L_EXISTSCRITICALNOTE          71
#define MNO_L_EXISTSROLLCHG               72
#define MNO_L_ISPENCHRGCANCELED1          73
#define MNO_L_ISPENCHRGCANCELED2          74
#define MNO_L_ISPERSONALPROPERTYPENALTY   75
#define MNO_L_ISAGPRESERVE                76
#define MNO_L_ISCARRYOVER1STPAID          77
#define MNO_L_ISCARRYOVERRECORD           78
#define MNO_L_ISFAILURETOFILE             79
#define MNO_L_ISOWNERSHIPPENALTY          80
#define MNO_L_ISELIGIBLEFOR4PAY           81
#define MNO_L_ISNOTICE4SENT               82
#define MNO_L_ISPARTPAY                   83
#define MNO_L_ISFORMATTEDADDRESS          84
#define MNO_L_ISADDRESSCONFIDENTIAL       85
#define MNO_L_SUPLCNT                     86
#define MNO_L_ORIGINALDUEDATE1            87
#define MNO_L_ORIGINALDUEDATE2            88
#define MNO_L_ISPEN1REFUND                89
#define MNO_L_ISPEN2REFUND                90
#define MNO_L_ISDELINQPENREFUND           91
#define MNO_L_ISREFUNDAUTHORIZED          92
#define MNO_L_ISNODISCHARGE               93
#define MNO_L_ISAPPEALPENDING             94
#define MNO_L_OTHERFEESPAID               95
#define MNO_L_FOURPAYREASON               96
#define MNO_L_DATEDISCHARGED              97
#define MNO_L_ISONLYFIRSTPAID             98
#define MNO_L_ISALLPAID                   99
#define MNO_L_DTS                         100
#define MNO_L_USERID                      101

// 2015
#define MNO_L2_ASMT                       0
#define MNO_L2_TAXYEAR							1
#define MNO_L2_ROLLCATEGORY					2
#define MNO_L2_ROLLTYPE							3
#define MNO_L2_FEEPARCEL						4
#define MNO_L2_ORIGINATINGASMT				5
#define MNO_L2_STATUS							6
#define MNO_L2_TRA								7
#define MNO_L2_TAXABILITY						8
#define MNO_L2_ACRES								9
#define MNO_L2_USECODE							10
#define MNO_L2_CURRENTMARKETLAND				11
#define MNO_L2_CURRENTFIXEDIMPR				12
#define MNO_L2_CURRENTSTRUCTURALIMPR		13
#define MNO_L2_CURRENTPERSONALPROP			14
#define MNO_L2_CURRENTPERSONALPROPMH		15
#define MNO_L2_CURRENTNETVALUE				16
#define MNO_L2_OWNER								17
#define MNO_L2_ASSESSEE							18
#define MNO_L2_MAILADDRESS1					19
#define MNO_L2_MAILADDRESS2					20
#define MNO_L2_MAILADDRESS3					21
#define MNO_L2_MAILADDRESS4					22
#define MNO_L2_EXEMPTIONCODE1					23
#define MNO_L2_EXEMPTIONAMT1					24

static XLAT_CODE  asHeating[] =
{  // 11/03/2015
   // Value, lookup code, value length
   "1", "B", 1,               // FAU (Force air)
   "2", "D", 1,               // Wall
   "3", "D", 1,               // Double Wall
   "4", "C", 1,               // Floor
   "5", "S", 1,               // Wood/pellet stove
   "6", "E", 1,               // Hydronic
   "7", "F", 1,               // Baseboard
   "8", "J", 1,               // Space
   "9", "X", 1,               // Other
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{  // 11/03/2015
   // Value, lookup code, value length
   "N",  "N", 1,              // None
   "PTS","S", 3,              // Portable spa
   "BIS","S", 3,              // Built-in spa
   "BPS","C", 3,              // Built-in pool and spa
   "BIP","P", 3,              // Built-in pool
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{  // 11/03/2015
   // Value, lookup code, value length
   "0", "N", 1,                // None
   "1", "L", 1,                // Masonry
   "2", "Z", 1,                // Zero Clearance
   "3", "W", 1,                // Wood Stove
   "4", "S", 1,                // Pellet Stove
   "5", "I", 1,                // Fireplace Insert
   "6", "G", 1,                // LPG Fireplace (Gas Log)
   "7", "O", 1,                // Other
   "",   "", 0
};

static STRSFX asSfxTbl[] = 
{
   "RD",  "ROAD","2",  4,
   "DR",  "DRIV","5",  4,
   "BLVD","BLVD","4",  4,
   "LN",  "LANE","21", 4,
   "ST",  "STRE","1",  4,
   "CIR", "CIRC","10", 4,
   "CIR", "CIR", "10", 3,
   "TRL", "TRAI","35", 4,
   "TRL", "TRL", "35", 3,
   "WAY", "WAY", "38", 3,
   "AVE", "AVEN","3",  3,
   "AVE", "AVE", "3",  3,
   "PL",  "PLAC","27", 2,
   "PL",  "PL",  "27", 2,
   "CT",  "COUR","14", 4,
   "CT",  "CT",  "14", 2,
   "PARK","PARK","26", 4,
   "TER", "TERR","33", 4,
   "LOOP","LOOP","22", 4,
   "PT",  "POIN","169",4,
   "PT",  "PT",  "169",2,
   "RD",  "RD",  "2",  2,
   "DR",  "DR",  "5",  2,
   "LN",  "LN",  "21", 2,
   "ST",  "ST",  "1",  2,
   "RUN", "RUN", "183",3,
   "CUT", "CUTO","15", 4,
   "HWY", "HWY", "20", 3,
   "PKWY","PKWY","28", 4,
   "TER", "TER", "33", 3,
   "","","",0
};

// Use to translate sale's doc code to standard DocType
IDX_TBL5 MNO_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "01", "1 ", 'N', 2, 2,        // Grand Deed - Transfer (DTT)
   "02", "75", 'Y', 2, 2,        // Transfer (No DTT)
   "03", "74", 'Y', 2, 2,        // Non-Reappraisable Event
   "04", "57", 'N', 2, 2,        // Partial interest transfer (DTT)
   "05", "75", 'Y', 2, 2,        // Partial interest transfer (no DTT)
   "10", "74", 'Y', 2, 2,        // Condominium-New
   "11", "74", 'Y', 2, 2,        // Maintenance
   "12", "75", 'Y', 2, 2,        // Partial interest transfer, Non-Reappraisable Event
   "15", "9 ", 'N', 2, 2,        // Corr to previous appraisal
   "16", "74", 'Y', 2, 2,        // Commercial Addition/Remodel
   "18", "40", 'Y', 2, 2,        // Easement
   "25", "74", 'Y', 2, 2,        // Garage/Carport-New
   "53", "74", 'Y', 2, 2,        // Possessory Interest New/Renewal
   "54", "74", 'Y', 2, 2,        // ?
   "65", "74", 'Y', 2, 2,        // Reference Document
   "80", "1 ", 'N', 2, 2,        // Taxable Government Land
   "","",0,0,0
};

// Use to translate GrGr's doc title to standard DocType
IDX_TBL5 MNO_DocTitle[] =
{  // DocTitle, Index, Non-sale, len1, len2
   "DEED ",    "1 ", 'N', 5, 2,  // GRAND DEED
   "ASGT",     "7 ", 'Y', 4, 2,  // ASSIGNMENT 
   "TAXL",     "46", 'N', 4, 2,  // TAX LEIN
   "NOTD",     "52", 'Y', 4, 2,  // NOTICE OF DEFAULT
   "AFF",      "6 ", 'Y', 3, 2,  // AFFIDAVIT
   "DEED-NC",  "13", 'Y', 7, 2,  // DEED - NO CHANGE OF OWNERSHIP
   "DEED-NO",  "13", 'N', 7, 2,  // DEED NO CHARGE FOR RECORDING
   "DEEDTX",   "67", 'N', 6, 2,  // TAX DEED
   "DEEDLF",   "78", 'Y', 6, 2,  // DEED IN LIEU OF FORCLOSURE
   "DEEDTR",   "27", 'Y', 6, 2,  // TRUSTEE'S DEED
   "TAXIRS",   "46", 'N', 6, 2,  // IRS TAX LEIN
   "ML",       "46", 'N', 2, 2,  // MECHANIC LIEN
   "","",' ',0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military
// Z=Full exemption

IDX_TBL4 MNO_Exemption[] = 
{
   "E01", "H", 3,1,
   "E02", "X", 3,1,     // DUMMY BOE < 40K
   "E03", "X", 3,1,     // TRIBAL HOUSING
   "E04", "D", 3,1,     // DISABLED VETERAN
   "E05", "D", 3,1,     // DISABLED VETERAN
   "E06", "W", 3,1,     // WELFARE
   "E07", "C", 3,1,     // CHURCH
   "E08", "W", 3,1,     // LESSORS
   "E09", "P", 3,1,     // PUBLIC SCHOOL
   "E11", "R", 3,1,
   "E12", "U", 3,1, 
   "E13", "E", 3,1,
   "E14", "M", 3,1,
   "E30", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E60", "X", 3,1,     // LOW VALUE
   "E98", "X", 3,1,     // OTHER
   "","",0,0
};

#endif