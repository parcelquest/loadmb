/*****************************************************************************
 *
 *
 * Revision:
 * 10/03/2007  Change Timber code from "A" to "T" for SIE and TEH
 * 12/11/2007  Change parcel type for ALP
 * 04/24/2008  Change Ag/Timber code from A/T to Y.
 * 09/07/2010  Change status from ' ' to 'R' for GLE, LAS, MOD, SIE, TOU when type=99
 *             Add status 'P' when type=98 for DNX, GLE, INY, LAS, MOD, SIE, TRI
 *             Change status from 'A' to 'P' for TEH & TUO when type=98
 * 09/27/2013  Add isVestChk() to check for name which should not parse.
 *             Add LoadVesting() & loadVestingTbls() to load vesting tables.
 *             Modify findVesting() to return Vesting code location in the Name.
 * 10/02/2013  Add updateVesting() to allow quick update to R01 record.
 * 04/15/2014  Add new version of findXlatCode() to support single char compare
 *             in unsorted table.
 * 08/26/2014  Add findXlatCodeA() to search for value in unsorted table
 * 07/16/2021  Modify all findXlatCode*() to do case insensitive comparision.
 * 07/01/2024  Add makeExeType() & findExeType().
 * 07/17/2024  Modify makeExeType() to avoid duplicate entry.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "Utils.h"
#include "Logs.h"
#include "Tables.h"
#include "Prodlib.h"
#include "XlatTbls.h"

PARC_TYPE asParcType_ALP[]=
{
    7,'A',' ',' ',
    8,'S',' ',' ',
    9,'A',' ',' ',
   10,'A',' ',' ',
   11,'O',' ',' ',
   12,'A','Y',' ',
   14,'O',' ',' ',
   98,'P',' ',' ',
   99,'R',' ',' '
};

PARC_TYPE asParcType_DNX[]=
{
// 2012-
    1,'A',' ',' ',
    2,'A',' ',' ',
    3,'X',' ',' ',      // Ignore by Sony
    4,'A',' ',' ',
    5,'A',' ',' ',
    6,'A',' ',' ',
    7,'A',' ',' ',
    8,'A',' ',' ',
   10,'A',' ',' ',
   11,'A',' ',' ',
   12,'A','Y',' ',
   13,'A',' ',' ',
   14,'A',' ',' ',
   17,'A',' ',' ',
   98,'P',' ',' ',
   99,'R',' ',' '

/* 2010-2012
   10,'A',' ',' ',
   12,'A','Y',' ',
   98,'P',' ',' ',
   99,'R',' ',' '
*/
};

PARC_TYPE asParcType_GLE[]=
{
    1,'A',' ',' ',
    6,'A',' ',' ',
   10,'A',' ',' ',
   11,'A',' ',' ',
   12,'A','Y',' ',
   13,'A',' ',' ',
   14,'A',' ','Y',
   15,'A',' ','Y',
   16,'A','Y',' ',
   17,'A',' ',' ',
   18,'A',' ',' ',
   20,'A',' ',' ',
   30,'O',' ',' ',
   92,'A',' ',' ',
   98,'P',' ',' ',
   99,'R',' ',' '
};

PARC_TYPE asParcType_INY[]=
{
    1,'A',' ',' ',
   10,'A',' ',' ',
   11,'A',' ','Y',
   12,'A','Y',' ',
   13,'A',' ',' ',
   16,'A',' ',' ',
   18,'A',' ',' ',
   19,'A',' ',' ',
   30,'A',' ',' ',
   35,'A',' ',' ',
   40,'O',' ',' ',      // Mines
   98,'P',' ',' ',
   99,'R',' ',' '
};

PARC_TYPE asParcType_LAS[]=
{
    1,'A',' ',' ',
   10,'A',' ',' ',
   11,'A',' ','Y',
   12,'A','Y',' ',
   13,'A',' ',' ',
   16,'A',' ',' ',
   18,'A',' ',' ',
   19,'A',' ',' ',
   98,'P',' ',' ',
   99,'R',' ',' '
};

PARC_TYPE asParcType_MOD[]=
{
    7,'A',' ',' ',
   10,'A',' ',' ',
   11,'A','Y',' ',
   12,'A',' ',' ',
   13,'A',' ',' ',
   14,'A',' ','Y',
   17,'A',' ',' ',
   20,'O',' ',' ',
   49,'A',' ',' ',
   98,'P',' ',' ',
   99,'R',' ',' '
};

PARC_TYPE asParcType_SIE[]=
{
    1,'O',' ',' ',
    2,'A',' ',' ',
    3,'A',' ',' ',
    4,'A',' ',' ',
    5,'A',' ',' ',
    6,'A',' ',' ',
    7,'A',' ',' ',
    8,'A',' ',' ',
    9,'O',' ',' ',
   10,'A',' ',' ',
   11,'A',' ','Y',
   12,'A',' ',' ',
   13,'A','Y',' ',
   14,'A','Y','Y',      // in both AG and TPZ
   15,'A','Y','Y',
   18,'A',' ',' ',
   20,'O',' ',' ',
   98,'P',' ',' ',
   99,'R',' ',' '
};

PARC_TYPE asParcType_TEH[]=
{
   01,'O',' ',' ',
   02,'A',' ',' ',
   03,'A',' ',' ',
   10,'A',' ',' ',
   11,'A',' ','Y',
   12,'A','Y',' ',
   13,'A',' ','Y',
   14,'A',' ','Y',
   15,'A',' ',' ',
   16,'A',' ',' ',
   20,'O',' ',' ',
   98,'P',' ',' ',      // replacement parcel, old parcel will be 99
   99,'R',' ',' '
};

PARC_TYPE asParcType_TRI[]=
{
   10,'A',' ',' ',
   11,'A',' ','Y',
   12,'A','Y',' ',
   14,'A',' ',' ',
   15,'A',' ',' ',
   19,'A',' ',' ',            // pending to retire next roll year, currently still active
   29,'O',' ',' ',
   50,'O',' ',' ',
   59,'O',' ',' ',
   60,'A',' ',' ',
   61,'A',' ',' ',
   62,'A',' ',' ',
   98,'P',' ',' ',
   99,'R',' ',' '
};

PARC_TYPE asParcType_TUO[]=
{
    1,'A',' ',' ',
    2,'O',' ',' ',
    3,'O',' ',' ',
    4,'O',' ',' ',
    5,'O',' ',' ',
    6,'O',' ',' ',
    7,'O',' ',' ',
    8,'O',' ',' ',
    9,'O',' ',' ',
   10,'A',' ',' ',
   11,'O',' ',' ',
   12,'O',' ',' ',
   13,'O',' ',' ',
   14,'A',' ','Y',
   16,'A','Y',' ',
   17,'A',' ',' ',      // Taxable mobile home
   18,'A',' ',' ',
   //98,'A',' ',' ',
   98,'P',' ',' ',
   99,'R',' ',' '
};

VEST_CODE asVestCode[]=
{
   " JT",    "JT",    3,
   " TC",    "TC",    3,
   " CP",    "CP",    3,
   " J/T",   "JT",    4,
   " T/C",   "TC",    4,
   " C/P",   "CP",    4,
   " HWCP",  "CP",    5,
   " HWJT",  "JT",    5,
   " L EST", "LE",    6,
   " ESTATE","LE",    7,
   "", "", 0
};

VEST_CODE asVestChk[] =
{
   "CO",      "",     2,
   "CT",      "",     2,
   "GV",      "",     2,
   "PA",      "",     2,
   "",        "",     0
};

#define  MAX_VESTMAIN      200
#define  MAX_VESTLW        100

XLAT_CODE  asVestMain[MAX_VESTMAIN];
int        iVestMain;
XLAT_CODE  asVestLW[MAX_VESTLW];
int        iVestLW;

#define  MAX_PARC_TYPE     10
#define  MAX_GLE_VEST_CODE 3
#define  MAX_LAS_VEST_CODE 5
#define  MAX_TEH_VEST_CODE 5
#define  MAX_TRI_VEST_CODE 5
#define  MAX_ALP_VEST_CODE 5
#define  MAX_SIE_VEST_CODE 5
#define  MAX_TUO_VEST_CODE 5

char asVestCode_ALP[5][5]=
{
   "JT", "TC", "CP", "HWCP", "HWJT"
};

char asVestCode_LAS[5][5]=
{
   "JT", "TC", "CP", "HWCP", "HWJT"
};

char asVestCode_TEH[5][5]=
{
   "JT", "TC", "CP", "HWCP", "HWJT"
};

char asVestCode_TRI[5][5]=
{
   "JT", "TC", "CP", "HWCP", "HWJT"
};

char asVestCode_GLE[MAX_GLE_VEST_CODE][4]=
{
   "J/T", "T/C", "C/P"
};

char asVestCode_SIE[6][5]=
{
   "JT", "TC", "CP", "SS", "HWCP", "HWJT"
};

char asVestCode_TUO[5][5]=
{
   "JT", "TC", "CP", "HWCP", "HWJT"
};

char getParcStatus(PARC_TYPE *psPrclType, char *pParcType)
{
   int   iTmp, iIdx=0;
   char  bRet = ' ';

   iTmp = atoi(pParcType);
   while (psPrclType->iParcType < iTmp)
      psPrclType++;

   if (psPrclType->iParcType == iTmp)
      bRet = psPrclType->cParcStatus;

   return bRet;
}

char getParcStatus_ALP(char *pParcType)
{
   int   iTmp, iIdx=0;
   char  bRet = ' ';

   iTmp = atoin(pParcType, 2);
   while (asParcType_ALP[iIdx].iParcType < iTmp)
      iIdx++;

   if (asParcType_ALP[iIdx].iParcType == iTmp)
      bRet = asParcType_ALP[iIdx].cParcStatus;

   return bRet;
}

char getParcStatus_DNX(char *pParcType)
{
   int   iTmp, iIdx=0;
   char  bRet = ' ';

   iTmp = atoin(pParcType, 2);
   while (asParcType_DNX[iIdx].iParcType < iTmp)
      iIdx++;

   if (asParcType_DNX[iIdx].iParcType == iTmp)
      bRet = asParcType_DNX[iIdx].cParcStatus;

   return bRet;
}

char getParcStatus_GLE(char *pParcType)
{
   int   iTmp, iIdx=0;
   char  bRet = ' ';

   iTmp = atoi(pParcType);
   while (asParcType_GLE[iIdx].iParcType < iTmp)
      iIdx++;

   if (asParcType_GLE[iIdx].iParcType == iTmp)
      bRet = asParcType_GLE[iIdx].cParcStatus;

   return bRet;
}

char getParcStatus_LAS(char *pParcType)
{
   int   iTmp, iIdx=0;
   char  bRet = ' ';

   iTmp = atoi(pParcType);
   while (asParcType_LAS[iIdx].iParcType < iTmp)
      iIdx++;

   if (asParcType_LAS[iIdx].iParcType == iTmp)
      bRet = asParcType_LAS[iIdx].cParcStatus;

   return bRet;
}

char getParcStatus_TEH(char *pParcType)
{
   int   iTmp, iIdx=0;
   char  bRet = ' ';

   iTmp = atoi(pParcType);
   while (asParcType_TEH[iIdx].iParcType < iTmp)
      iIdx++;

   if (asParcType_TEH[iIdx].iParcType == iTmp)
      bRet = asParcType_TEH[iIdx].cParcStatus;

   return bRet;
}
char getParcStatus_TRI(char *pParcType)
{
   int   iTmp, iIdx=0;
   char  bRet = ' ';

   iTmp = atoi(pParcType);
   while (asParcType_TRI[iIdx].iParcType < iTmp)
      iIdx++;

   if (asParcType_TRI[iIdx].iParcType == iTmp)
      bRet = asParcType_TRI[iIdx].cParcStatus;

   return bRet;
}

char *getVestingCode_LAS(char *pVesting, char *pVestCode)
{
   int   iIdx;

   *pVestCode = 0;
   for (iIdx=0; iIdx < MAX_LAS_VEST_CODE; iIdx++)
   {
      if (!strcmp(asVestCode_LAS[iIdx], pVesting))
      {
         strcpy(pVestCode, pVesting);
         break;
      }
   }

   return pVestCode;
}

char *getVestingCode_TEH(char *pVesting, char *pVestCode)
{
   int   iIdx;

   *pVestCode = 0;
   for (iIdx=0; iIdx < MAX_LAS_VEST_CODE; iIdx++)
   {
      if (!strcmp(asVestCode_TEH[iIdx], pVesting))
      {
         strcpy(pVestCode, pVesting);
         break;
      }
   }

   return pVestCode;
}

char *getVestingCode_GLE(char *pVesting, char *pVestCode)
{
   int   iIdx;

   *pVestCode = 0;
   for (iIdx=0; iIdx < MAX_GLE_VEST_CODE; iIdx++)
   {
      if (!memcmp(asVestCode_GLE[iIdx], pVesting, 3))
      {
         strcpy(pVestCode, asVestCode_LAS[iIdx]);
         break;
      }
   }

   return pVestCode;
}

char *getVestingCode_SIE(char *pVesting, char *pVestCode)
{
   int   iIdx;

   *pVestCode = 0;
   for (iIdx=0; iIdx < MAX_SIE_VEST_CODE; iIdx++)
   {
      if (!strcmp(asVestCode_SIE[iIdx], pVesting))
      {
         strcpy(pVestCode, pVesting);
         break;
      }
   }

   return pVestCode;
}

char *getVestingCode_DNX(char *pVesting, char *pVestCode)
{
   int   iIdx;

   *pVestCode = 0;
   for (iIdx=0; iIdx < MAX_GLE_VEST_CODE; iIdx++)
   {
      if (!memcmp(asVestCode_GLE[iIdx], pVesting, 3))
      {
         strcpy(pVestCode, asVestCode_LAS[iIdx]);
         break;
      }
   }

   return pVestCode;
}

char *getVestingCode_TRI(char *pVesting, char *pVestCode)
{
   int   iIdx;

   *pVestCode = 0;
   for (iIdx=0; iIdx < MAX_TRI_VEST_CODE; iIdx++)
   {
      if (!memcmp(asVestCode_TRI[iIdx], pVesting, 3))
      {
         strcpy(pVestCode, asVestCode_TRI[iIdx]);
         break;
      }
   }

   return pVestCode;
}

char *getVestingCode_ALP(char *pVesting, char *pVestCode)
{
   int   iIdx;

   *pVestCode = 0;
   for (iIdx=0; iIdx < MAX_ALP_VEST_CODE; iIdx++)
   {
      if (!strcmp(asVestCode_ALP[iIdx], pVesting))
      {
         strcpy(pVestCode, pVesting);
         break;
      }
   }

   return pVestCode;
}

char *getVestingCode_TUO(char *pVesting, char *pVestCode)
{
   int   iIdx;

   *pVestCode = 0;
   for (iIdx=0; iIdx < MAX_TUO_VEST_CODE; iIdx++)
   {
      if (!strcmp(asVestCode_TUO[iIdx], pVesting))
      {
         strcpy(pVestCode, pVesting);
         break;
      }
   }

   return pVestCode;
}

void getParcType(char *pStatus, char *pTimPre, char *pAgPre, char *pParcType, char *pCnty)
{
   PARC_TYPE *pList;
   int   iTmp;

   if (!memcmp(pCnty, "ALP", 3))
      pList = asParcType_ALP;
   else if (!memcmp(pCnty, "DNX", 3))
      pList = asParcType_DNX;
   else if (!memcmp(pCnty, "GLE", 3))
      pList = asParcType_GLE;
   else if (!memcmp(pCnty, "LAS", 3))
      pList = asParcType_LAS;
   else if (!memcmp(pCnty, "MOD", 3))
      pList = asParcType_MOD;
   else if (!memcmp(pCnty, "TEH", 3))
      pList = asParcType_TEH;
   else if (!memcmp(pCnty, "TRI", 3))
      pList = asParcType_TRI;
   else if (!memcmp(pCnty, "SIE", 3))
      pList = asParcType_SIE;
   else if (!memcmp(pCnty, "TUO", 3))
      pList = asParcType_TUO;
   else
      pList = asParcType_LAS;

   iTmp = atoi(pParcType);
   while (pList->iParcType < iTmp)
      pList++;

   if (pList->iParcType == iTmp)
   {
      *pStatus = pList->cParcStatus;
      *pTimPre = pList->cTimPre;
      *pAgPre = pList->cAgrPre;
   } else
   {
      *pStatus = ' ';
      *pTimPre = ' ';
      *pAgPre = ' ';
   }
}

/*********************************************************************************
 *
 * Look for translate code within names
 * To use this function, pTable has to be in sorted order. If table is not sorted,
 * use findXlatCodeA() 
 *
 *********************************************************************************/

char *findXlatCode(char *pValue, XLAT_CODE *pTable)
{
   char *pRet=NULL;
   int   iTmp;

   while (pTable->iLen > 0)
   {
      iTmp = _memicmp(pValue, pTable->acSrc, pTable->iLen);
      if (iTmp == 0)
      {
         pRet = pTable->acCode;
         break;
      } else if (iTmp < 0)
         break;   // not found
      pTable++;
   }

   return pRet;
}

char *findXlatCodeA(char *pValue, XLAT_CODE *pTable)
{
   char *pRet=NULL;
   int   iTmp;

   while (pTable->iLen > 0)
   {
      iTmp = _memicmp(pValue, pTable->acSrc, pTable->iLen);
      if (iTmp == 0)
      {
         pRet = pTable->acCode;
         break;
      } 
      pTable++;
   }

   return pRet;
}

// Compare whole string on sorted list
char *findXlatCodeS(char *pValue, XLAT_CODE *pTable)
{
   char *pRet=NULL;
   int   iTmp;

   while (pTable->iLen > 0)
   {
      iTmp = _stricmp(pValue, pTable->acSrc);
      if (iTmp == 0)
      {
         pRet = pTable->acCode;
         break;
      } else if (iTmp < 0)
         break;   // not found
      pTable++;
   }

   return pRet;
}

char *findXlatCode(char cValue, XLAT_CODE *pTable)
{
   char *pRet=NULL;

   while (pTable->iLen > 0)
   {
      if (cValue == pTable->acSrc[0])
      {
         pRet = pTable->acCode;
         break;
      }
      pTable++;
   }

   return pRet;
}

/*********************************************************************************
 *
 * Look for vesting code within names
 *
 *********************************************************************************/

bool isVestChk(char *pVesting)
{
   bool  bRet=false;
   int   iTmp=0;

   while (asVestChk[iTmp].iVestingLen > 0)
   {
      if (!memcmp(pVesting, asVestChk[iTmp].acVestSrc, asVestChk[iTmp].iVestingLen))
      {
         bRet = true;
         break;
      }
      iTmp++;
   }

   return bRet;
}

/*********************************************************************************
 *
 * Look for vesting code within names
 *
 *********************************************************************************

char *findVesting(char *pNames, char *pVestCode)
{
   char *pRet=NULL;
   int   iTmp=0;

   while (asVestCode[iTmp].iVestingLen > 0)
   {
      if (pRet = strstr(pNames, asVestCode[iTmp].acVestSrc))
      {
         strcpy(pVestCode, asVestCode[iTmp].acVestCode);
         break;
      }
      iTmp++;
   }

   return pRet;
}

/*********************************************************************************
 *
 * Look for vesting code within names
 *
 *********************************************************************************/

char *findVesting(char *pName, char *pVestCode)
{
   int   iCnt;
   char  *pTmp, *pRet;

   iCnt = 0;
   pRet = NULL;
   *pVestCode = 0;
   while (asVestMain[iCnt].iLen > 0)
   {
      if (pRet = strstr(pName, asVestMain[iCnt].acSrc))
      {
         strcpy(pVestCode, asVestMain[iCnt].acCode);
         break;
      }
      iCnt++;
   }

   if (!pRet)
   {
      if (pTmp = strrchr(pName, ' '))
      {
         iCnt = 0;
         while (asVestLW[iCnt].iLen > 0)
         {
            if (!strcmp(pTmp, asVestLW[iCnt].acSrc))
            {
               strcpy(pVestCode, asVestLW[iCnt].acCode);
               pRet = pTmp;
               break;
            }
            iCnt++;
         }
      }
   }

   return pRet;
}

/*********************************** loadVesting *****************************
 *
 * CntyCode is reserved for future implementation of county specific
 *
 * Return number of vestings loaded, -1 if error.
 *
 *****************************************************************************/

int loadVesting(char *pCntyCode, char *pVestFile)
{
   char  acTmp[128], *pTmp, *apItems[4];
   int   iCnt, iRet;
   FILE  *fd;

   LogMsg("Loading Vesting table: %s", pVestFile);

   if (!(fd = fopen(pVestFile, "r")))
   {
      LogMsg("Error opening vesting file %s", pVestFile);
      return -1;
   }

   // Search for [STD-TBL]
   while (!feof(fd))
   {
      if (!(pTmp = fgets(acTmp, 128, fd)))
         break;
      if (!memcmp(acTmp, "[STD_TBL]", 9))
         break;
   }

   // Load data into main vesting table
   iRet = 0;
   while (!feof(fd))
   {
      if (!(pTmp = fgets(acTmp, 128, fd)))
         break;

      iCnt = ParseString(acTmp, '|', 4, apItems);
      if (iCnt > 1)
      {  
         strncpy(asVestMain[iRet].acSrc, apItems[0], MAX_XLATCODE_SRC);
         strncpy(asVestMain[iRet].acCode, apItems[1], MAX_XLATCODE_CODE);
         asVestMain[iRet].iLen = strlen(apItems[0]);
         iRet++;
      } else 
         break;

      if (iRet >= MAX_VESTMAIN)
      {
         LogMsg("***** Too many extries in main vesting table");
         break;
      }

   }
   LogMsg("There are %d entries loaded in main vesting table", iRet);

   // Terminate table
   strcpy(asVestMain[iRet].acSrc, "");
   strcpy(asVestMain[iRet].acCode, "");
   asVestMain[iRet].iLen = 0;

   // Search for [STD-LW]
   while (!feof(fd))
   {
      if (!(pTmp = fgets(acTmp, 128, fd)))
         break;
      if (!memcmp(acTmp, "[STD_LW]", 8))
         break;
   }

   // Load data into main vesting table
   iRet = 0;
   while (!feof(fd))
   {
      if (!(pTmp = fgets(acTmp, 128, fd)))
         break;

      iCnt = ParseString(acTmp, '|', 4, apItems);
      if (iCnt > 1)
      {  
         strncpy(asVestLW[iRet].acSrc, apItems[0], MAX_XLATCODE_SRC);
         strncpy(asVestLW[iRet].acCode, apItems[1], MAX_XLATCODE_CODE);
         asVestLW[iRet].iLen = strlen(apItems[0]);
         iRet++;
      } else 
         break;
   }
   LogMsg("There are %d entries loaded in LW vesting table", iRet);

   // Terminate table
   strcpy(asVestLW[iRet].acSrc, "");
   strcpy(asVestLW[iRet].acCode, "");
   asVestLW[iRet].iLen = 0;

   fclose(fd);
   return iRet;
}

/********************************* UpdateVesting *****************************
 *
 * Use this function to update vesting in R01 record. Since its purpose is to 
 * update R01 record, return string won't be terminated by 0. County code is
 * provided for future development.
 *
 * Return number of bytes copied to Vesting.
 *
 *****************************************************************************/

int updateVesting(char *pCntyCode, char *pName, char *pVesting, char *pEtal)
{
   char  *pTmp, sTmp[8];
   int   iRet = 0;

   sTmp[0] = 0;
   pTmp = findVesting(pName, sTmp);
   if (pTmp)
   {
      iRet = strlen(sTmp);
      memcpy(pVesting, sTmp, iRet);

      if (pEtal && !memcmp(sTmp, "EA", 2))
         *pEtal = 'Y';
   }

   return iRet;
}

/*********************************************************************************
 *
 * Look for exemption type using exemption code
 *
 *********************************************************************************/

char *findExeType(char *pExeCode, char *pExeType, IDX_TBL4 *pExeTypeTbl)
{
   bool  bFound=false;
   int   iTmp  =0;
   char  *pRet = NULL;

   if (*pExeCode >= '0')
   {
      while (pExeTypeTbl->iNameLen > 0)
      {
         if (!_memicmp(pExeCode, pExeTypeTbl->pName, pExeTypeTbl->iNameLen))
         {
            bFound = true;
            strcpy(pExeType, pExeTypeTbl->pCode);
            pRet = pExeType;
            break;
         }
         pExeTypeTbl++;   
         iTmp++;
      }
   }

   return pRet;
}

#define  SIZ_EXE_TYPE          10
char *makeExeType(char *pExeType, char *pExeCode1, char *pExeCode2, char *pExeCode3, IDX_TBL4 *pExeXlat)
{
   char  *pRet=NULL, acTmp[32], acTmp1[32];
   int   iTmp;

   if (*pExeCode1 > ' ')
   {
      pRet = findExeType(pExeCode1, acTmp, pExeXlat);
      if (pRet)
      {
         sprintf(acTmp1, "~%s~", acTmp);
         if (*pExeCode2 > ' ')
         {
            pRet = findExeType(pExeCode2, acTmp, pExeXlat);
            if (pRet)
            {
               // Avoid duplicate
               if (!strchr(acTmp1, acTmp[0]))
               {
                  strcat(acTmp1, acTmp);
                  strcat(acTmp1, "~");
               }
            }
            if (*pExeCode3 > ' ')
            {
               pRet = findExeType(pExeCode3, acTmp, pExeXlat);
               if (!strchr(acTmp1, acTmp[0]))
               {
                  strcat(acTmp1, acTmp);
                  strcat(acTmp1, "~");
               }
            }
         }

         // Populate ExeType
         iTmp = strlen(acTmp1);
         vmemcpy(pExeType, acTmp1, SIZ_EXE_TYPE);
         pRet = pExeType;
      } 
   }

   return pRet;
}
