/***************************************************************************
 *
 * Usage:
 *    -CEDX -L [-Xs[i]|-Ms] -Xl  [-Mr] (load lien)
 *    -CEDX -U  [-Xs[i]|-Ms] [-Z] [-Lz] [-T|Ut] [-Mr] (load update)
 *
 * Revision
 * 06/17/2017 17.16.0   Create MergeEdx.cpp
 * 08/02/2018 18.2.3    Fine tune Edx_Load_LDR() & Edx_Load_Roll() and ready for production.
 *                      Add Edx_MergeOwnerRec() & CombineOwner() to use names from owner file
 * 08/24/2018 18.4.0    Modify Edx_MergeLien() to append LANDUSE2 to LANDUSE1.
 * 09/03/2018 18.4.1    Turn on updating CHAR in both Load_LDR() and Load_Roll().
 *                      Add Edx_UpdatePrevApn() to merge old APN from EDC_OldNew_Final.dat
 *                      Add Edx_MergeMAdrEx(), Edx_MergeLienEx(), Edx_MergeOthers(), Edx_Load_LDR_Ex()
 *                      to support special LDR 2018 file. Add -Lz option to load zipcode file.
 * 09/05/2018 18.5.0    Validate TotalRooms in Edx_ConvStdChar()
 * 09/09/2018 18.5.1    Modify Edx_MergeStdChar() to update LotSqft only if it's blank and not update Sewer. 
 *                      Modify Edx_MergeLien() to load latest AgencyCD file (L10)
 * 11/08/2018 18.7.4    Add code to load tax using data from TaxSCrape.
 * 07/31/2019 19.1.1    Add -Up option to merge Prev_Apn.
 * 08/02/2019 19.1.2    Add Edx_MergeLien3() & modify Edx_Load_LDR() for LDR 2019.
 * 09/23/2019 19.3.1    Modify Edx_MergeLien3() and Edx_MergeRoll() to ignore book 555 (dummy parcels)
 * 04/27/2020 19.9.0    Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 06/14/2020 19.10.2   Add -Xn & -Mn options
 * 11/01/2020 20.4.2    Modify Edx_MergeRoll() & Edx_MergeLienEx() to populate default PQZoning.
 * 02/10/2021 20.7.0    Modify loadEdx() to use sale date format specify in INI file.
 * 03/17/2021 20.8.1    Replace '\' with '/' in Edx_MergeOwner().
 * 07/07/2021 21.0.6    Modify Edx_MergeStdChar() to populate QualityClass.
 * 07/20/2021 21.1.0    Modify Edx_MergeRoll() to update TRANSFER only with valid DOCNUM.
 *                      If MB_ROLL_M_ADDR is blank, use MB_ROLL_M_ADDR1 to format mailing addr.
 * 07/23/2022 22.1.0    Add EDX_FindUseCode2(). Modify Edx_MergeLien3(), Edx_MergeRoll, Edx_Load_Roll(),  
 *                      Edx_Load_LDR() to combine UseCode1 and UseCode2 (from external file) to populate USE_CO.
 * 01/30/2023 22.5.7    Add code to remove sales from R01 and clean up sale file in loadEdx().
 * 02/14/2024 23.6.3    Modify Edx_MergeMAdr() to make sure StrName & City are upper case.
 * 08/11/2024 24.1.0    Modify Edx_MergeLien3() to add ExeType.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "CharRec.h"
#include "PQ.h"
#include "Situs.h"
#include "Tax.h"
#include "doZip.h"
#include "SqlExt.h"
#include "MergeEdx.h"
#include "NdcExtr.h"

extern long lLotSqftCount;
FILE   *fdUseCode;
long   lUCMatch, lUCSkip;


/**************************** EDX_FindUseCode2 *******************************
 *
 * Search for UseCode2 using APN from EDX_UseCode2.csv
 *
 * Return 1 if found
 * 
 *****************************************************************************/

int EDX_FindUseCode2(char *pApn, char *pUseCode2)
{
   static   char acRec[64], *pRec=NULL;
   int      iLoop;

   *pUseCode2 = 0;
   if (!fdUseCode)
      return 0;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 64, fdUseCode);

   do
   {
      if (!pRec)
      {
         fclose(fdUseCode);
         fdUseCode = NULL;
         return 0;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pApn, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg("*** Skip UseCode2: %s", pRec);
         pRec = fgets(acRec, 64, fdUseCode);
         lUCSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   lUCMatch++;

   // Copy UseCode2
   acRec[15] = 0;                   // Remove LF
   strcpy(pUseCode2, &acRec[13]);

   // Get next rec
   pRec = fgets(acRec, 64, fdUseCode);
   if (!pRec)
   {
      fclose(fdUseCode);
      fdUseCode = NULL;
   }

   return 1;
}

/**************************** Edx_ConvStdChar ********************************
 *
 * Copy from MergeHum.cpp to convert new char file.
 * 
 *****************************************************************************/

int Edx_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], *pRec;
   int      iBeds, iFBaths, iRet, iTmp, iFldCnt, iCnt=0;
   double   dTmp;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_char.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort char file %s to %s", pInfile, acTmpFile);
   iRet = sortFile(pInfile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,EQ,\" \")");
   if (iRet < 500)
   {
      LogMsg("***** Input file is too small.");
      return 1;
   }

   if (!(fdIn = fopen(acTmpFile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Start loop
   while (!feof(fdIn))
   {
      // Get next record
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec || acBuf[0] > '9')
         break;

      replNull(acBuf);
      iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < EDX_CHAR_HASWELL)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[EDX_CHAR_ASMT], strlen(apTokens[EDX_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[EDX_CHAR_FEEPARCEL], strlen(apTokens[EDX_CHAR_FEEPARCEL]));

      // Format APN
      if (*apTokens[EDX_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[EDX_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         if (bDebug)
            LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[EDX_CHAR_FEEPARCEL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[EDX_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Beds
      iBeds = atoi(apTokens[EDX_CHAR_BEDROOMS]);
      if (iBeds > 0)
      {
         iRet = sprintf(acTmp, "%d", iBeds);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iFBaths = atoi(apTokens[EDX_CHAR_BATHROOMS]);
      if (iFBaths > 0)
      {
         iRet = sprintf(acTmp, "%d", iFBaths);
         memcpy(myCharRec.FBaths, acTmp, iRet);
         memcpy(myCharRec.Bath_4Q, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[EDX_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
         memcpy(myCharRec.Bath_2Q, acTmp, iRet);
      }

      // Rooms
      iTmp = atoi(apTokens[EDX_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         if (iTmp < 20 || (iBeds > 0 && iTmp < iBeds*10))
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(myCharRec.Rooms, acTmp, iRet);
         } else 
            LogMsg("*** Bad TotalRooms: %d (Beds=%d) [%s]", iTmp, iBeds, apTokens[EDX_CHAR_ASMT]);
      } 

      // Pool - prepare for future - currently not avail 20180618
      iTmp = blankRem(apTokens[EDX_CHAR_POOLSPA]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[EDX_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass 
      vmemcpy(myCharRec.QualityClass, _strupr(apTokens[EDX_CHAR_QUALITYCLASS]), SIZ_CHAR_QCLS);
      if (*apTokens[EDX_CHAR_QUALITYCLASS] > '0' && *apTokens[EDX_CHAR_QUALITYCLASS] <= 'Z')
      {
         acCode[0] = ' ';
         strcpy(acTmp, apTokens[EDX_CHAR_QUALITYCLASS]);
         if (isalpha(acTmp[0])) 
         {
            if (isdigit(acTmp[1]))
            {
               myCharRec.BldgClass = acTmp[0];
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            } else 
            {
               myCharRec.BldgClass = acTmp[1];
               if (isdigit(acTmp[2]))
                  iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
               else
                  LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[EDX_CHAR_QUALITYCLASS], apTokens[EDX_CHAR_ASMT]);
            }
         } else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[EDX_CHAR_QUALITYCLASS], apTokens[EDX_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (*apTokens[EDX_CHAR_QUALITYCLASS] > ' ' && *apTokens[EDX_CHAR_QUALITYCLASS] != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[EDX_CHAR_QUALITYCLASS], apTokens[EDX_CHAR_ASMT]);

      // YrBlt
      int iYrBlt = atoi(apTokens[EDX_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[EDX_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[EDX_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[EDX_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // Stories/NumFloors
      dTmp = atof(apTokens[EDX_CHAR_STORIESCNT]);
      if (dTmp > 0.0 && dTmp < 99.9)
      {
         iRet = sprintf(acTmp, "%.1f", dTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[EDX_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[EDX_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport Sqft
      int iCarport = atoi(apTokens[EDX_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Patio SF
      iTmp = atoi(apTokens[EDX_CHAR_PATIOSF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "001042001000", 9))
      //   iRet = 0;
#endif

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[EDX_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[EDX_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[EDX_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[EDX_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[EDX_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[EDX_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // FirePlace - 01, 02, 05, 06, 99
      if (*apTokens[EDX_CHAR_FIREPLACE] >= '0' && *apTokens[EDX_CHAR_FIREPLACE] <= '9')
      {
         pRec = findXlatCode(apTokens[EDX_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      } 

      // Haswell - not avail
      blankRem(apTokens[EDX_CHAR_HASWELL]);
      if (*(apTokens[EDX_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Lot Sqft
      if (iFldCnt >= EDX_CHAR_LOTSQFT)
      {
         ULONG    lSqft;
         lSqft = (ULONG)atol(apTokens[EDX_CHAR_LOTSQFT]);
         if (lSqft > 1)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(lSqft*SQFT_MF_1000);
            iTmp = sprintf(acTmp, "%d", (long)(dTmp+0.5));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d\n", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsg("Sorting char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum, UnitSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/******************************** Edx_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Edx_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4, bool bAdrOnly=false)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *pCareOf, *pDba, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64], acDba[128];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "500501015000", 9))
   //   iTmp = 0;
#endif

   // Initialize
   if (!bAdrOnly)
      removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   pCareOf = pDba = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         pCareOf = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         pCareOf = pLine2;
         p1 = pLine3;
         if (!_memicmp(pLine1, "DBA ", 4) )
            pDba = pLine1+4;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         p1 = pLine3;
         sprintf(acDba, "%s %s", pLine1+4, pLine2);
         pDba = &acDba[0];
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         pCareOf = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         pDba = pLine1+4;
         p1 = pLine2;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
   }

   // Update DBA
   if (pDba && !bAdrOnly)
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

   // Check for C/O
   if (pCareOf && !bAdrOnly)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, _strupr(acAddr1), strlen(acAddr1));

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   strcpy(acAddr2, p2);
   quoteRem(acAddr2);
   iTmp = blankRem(acAddr2);
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, _strupr(acAddr2), SIZ_M_CTY_ST_D, iTmp);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}

void Edx_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, false, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "500501015000", 9))
   //   iTmp = 0;
#endif

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   quoteRem(acAddr1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_M_ADDR_D, _strupr(acAddr1), strlen(acAddr1));

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      pTmp = _strupr(apTokens[MB_ROLL_M_CITY]);
      quoteRem(apTokens[MB_ROLL_M_CITY]);
      blankRem(apTokens[MB_ROLL_M_CITY]);

      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], SIZ_M_CITY);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      int iZip, iZip4=0;
      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {

         iZip = atoin(apTokens[MB_ROLL_M_ZIP], 5);
         iTmp = strlen(apTokens[MB_ROLL_M_ZIP]);
         if (iTmp == 9)
            iZip4 = atoin(apTokens[MB_ROLL_M_ZIP]+5, 4);

         sprintf(acTmp, "%.5d", iZip);
         memcpy(pOutbuf+OFF_M_ZIP, acTmp, 5);
         if (iZip4 > 0)
         {
            sprintf(acTmp, "%.4d", iZip4);
            memcpy(pOutbuf+OFF_M_ZIP4, acTmp, 4);
         }
      }

      if (iZip > 0)
      {
         if (iZip4 > 0)
            iTmp = sprintf(acTmp, "%s %s %.5d-%.4d", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], iZip, iZip4);
         else
            iTmp = sprintf(acTmp, "%s %s %.5d", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], iZip);
      } else
         iTmp = sprintf(acTmp, "%s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST]);

      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
   }

}

void Edx_MergeMAdrEx(char *pOutbuf, char *pMailStr, char *pMailZip)
{
   char     acTmp[256], acAddr1[128], acCity[64], acState[64], acCountry[64];
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, false, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004034005000", 9))
   //   iTmp = 0;
#endif

   // Mail address
   strcpy(acAddr1, pMailStr);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // Mail CitySt
   if (strlen(pMailZip) >= 5)
   {
      iTmp = remChar(pMailZip, ' ');
      memcpy(pOutbuf+OFF_M_ZIP, pMailZip, SIZ_M_ZIP);
      if (strlen(pMailZip+5) == 4)
         memcpy(pOutbuf+OFF_M_ZIP4, pMailZip+5, SIZ_M_ZIP4);

      if (iTmp == 6)
         strcpy(acTmp, pMailZip);
      else
         sprintf(acTmp, "%.5s", pMailZip);
      iTmp = locateCity(acTmp, acCity, acState, acCountry);
      if (iTmp > 0)
      {
         vmemcpy(pOutbuf+OFF_M_CITY, acCity, SIZ_M_CITY);

         if (acState[0] > ' ')
         {
            memcpy(pOutbuf+OFF_M_ST, acState, SIZ_M_ST);
            iTmp = sprintf(acAddr1, "%s %.2s %.5s", acCity, acState, pMailZip);
         } else
         {
            memset(pOutbuf+OFF_M_ZIP, ' ', SIZ_M_ZIP);
            iTmp = sprintf(acAddr1, "%s %s", acCity, acCountry);
         }

         iTmp = blankRem(acAddr1);
         vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, SIZ_M_CTY_ST_D, iTmp);
      }
   }
}

/******************************** Edx_MergeSitus *****************************
 *
 * Merge Situs address
 * FIDDLETOWN, KIRKWOOD, PIONEER
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Edx_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp, *apItems[16];
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do
      {
         pRec = fgets(acRec, 512, fdSitus);
      } while (!isdigit(acRec[1]));
   }

   pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse situs input
   replNull(acRec);
   iTmp = ParseStringIQ(pRec, cDelim, 16, apItems);

   if (iTmp < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old Situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apItems[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, apItems[MB_SITUS_STRNUM], strlen(apItems[MB_SITUS_STRNUM]));

      sprintf(acTmp, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
      pTmp = strchr(acTmp, ' ');
      *(pTmp+1) = 0;
      strcpy(acAddr1, acTmp);

      if (pTmp = strchr(apItems[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apItems[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apItems[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apItems[MB_SITUS_STRDIR], strlen(apItems[MB_SITUS_STRDIR]));
      }
   }

   char acStrSfx[16], acStrName[64];
   strcpy(acStrName, apItems[MB_SITUS_STRNAME]);
   acStrSfx[0] = 0;
   if (*apItems[MB_SITUS_STRTYPE] > ' ')
   {
      // Fix specific spelling
      if (!strcmp(apItems[MB_SITUS_STRTYPE], "PA"))
         strcpy(acStrSfx, "PATH");
      else if (!strcmp(apItems[MB_SITUS_STRTYPE], "GR"))
         sprintf(acStrName, "%s GRADE", apItems[MB_SITUS_STRNAME]);
      else
         strcpy(acStrSfx, apItems[MB_SITUS_STRTYPE]);
   }
   vmemcpy(pOutbuf+OFF_S_STREET, acStrName, SIZ_S_STREET);
   strcat(acAddr1, acStrName);

   // Encode suffix
   if (acStrSfx[0] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, acStrSfx);
      iTmp = GetSfxCodeX(acStrSfx, acTmp);
      if (iTmp > 0)
      {
         sprintf(acCode, "%d", iTmp);
      } else
      {
         LogMsg0("*** Invalid suffix: %s", apItems[MB_SITUS_STRTYPE]);
         iBadSuffix++;
         acCode[0] = 0;
      }
      vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   }

   if (*apItems[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apItems[MB_SITUS_UNIT]);
      memcpy(pOutbuf+OFF_S_UNITNO, apItems[MB_SITUS_UNIT], strlen(apItems[MB_SITUS_UNIT]));
   }

   iTmp = blankRem(acAddr1, SIZ_S_ADDR_D);
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D, iTmp);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006181030000", 9) || !memcmp(pOutbuf, "027112029000", 9))
   //   iTmp = 0;
#endif

   // Situs city
   if (*apItems[MB_SITUS_COMMUNITY] > ' ')
   {
      int iZip = atoin(apItems[MB_SITUS_ZIP], 5);

      if (*apItems[MB_SITUS_COMMUNITY] == 'E' && iZip == 95762)
         iTmp = 0;

      //Abbr2Code(apItems[MB_SITUS_COMMUNITY], acTmp, acAddr1, iZip, apItems[MB_SITUS_ASMT]);   
      Abbr2Code(apItems[MB_SITUS_COMMUNITY], acTmp, acAddr1, apItems[MB_SITUS_ASMT]);   
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      if (*apItems[MB_SITUS_ZIP] > '0')
         vmemcpy(pOutbuf+OFF_S_ZIP, apItems[MB_SITUS_ZIP], SIZ_S_ZIP);

      if (acAddr1[0] > ' ')
         sprintf(acTmp, "%s CA %s", myTrim(acAddr1), apItems[MB_SITUS_ZIP]);
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int Edx_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004130013", 9))
   //   acTmp[0] = 0;
#endif
   // 
   strcpy(acAddr1, pLine1);
   int iTmp = blankRem(acAddr1);
   if (iTmp < 5)
      return 1;

   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

#ifdef _DEBUG
   // Check for '-'
   //char *pTmp;
   //if (pTmp = isCharIncluded(acAddr1, '-', 0))
   //   pTmp++;
#endif

   // 2830 G ST #STE D-1
   // 3980 CEDAR APTS 1-4 ST
   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1_4(&sSitusAdr, acAddr1);

   memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));

   memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
   memcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/****************************** Edx_MergeStdChar *****************************
 *
 * Merge Edx_Char.dat in STDCHAR format
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the one that has value.
 *
 *****************************************************************************/

int Edx_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lSqft;
   int      iLoop, iBeds, iFBath, iHBath, iBldgNum, iRooms, iUnits;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   while (!iLoop)
   {
      // Quality Class
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
      *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
      memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

      // YrBlt
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

      // YrEff
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

      // BldgSqft
      lSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      } else
         memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

      // Garage Sqft
      lSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
      } else
      {
         memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = ' ';
      }

      // LotSqft
      lSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10 && *(pOutbuf+OFF_LOT_SQFT) == ' ')
      {
         sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lSqft);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
         
         lSqft = atoin(pChar->LotAcre, SIZ_CHAR_SQFT);
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lSqft);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      } else
      {
         // Don't overwrite values from roll file
         //memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
         //memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      }

      // PatioSqft
      lSqft = atoin(pChar->PatioSqft, SIZ_CHAR_SQFT);
      if (lSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_PATIO_SF, lSqft);
         memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
      } else
         memset(pOutbuf+OFF_PATIO_SF, ' ', SIZ_BLDG_SF);

      // Heating
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   
      // Cooling 
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

      // Total Rooms
      iRooms = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
      if (iRooms > 0)
      {
         sprintf(acTmp, "%*d", SIZ_ROOMS, iRooms);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      } else
         memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

      // Beds
      iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      } else
         memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

      // Bath
      iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      } else
         memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

      // Half bath
      iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
      if (iHBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      } else
         memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

      // Fireplace
      *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

      // HasSeptic or HasSewer - Sewer only appears in AgencyCD file
      //*(pOutbuf+OFF_SEWER) = pChar->HasSewer;

      // HasWell
      *(pOutbuf+OFF_WATER) = pChar->HasWater;

      // Pools
      *(pOutbuf+OFF_POOL) = pChar->Pool[0];

      // Units count
      iUnits = atoin(pChar->Units, SIZ_CHAR_UNITS);
      if (iUnits > 0)
      {
         sprintf(acTmp, "%*d", SIZ_UNITS, iUnits);
         memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
      } 

      // Stories
      if (pChar->Stories[0] > ' ')
         memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
      else
         memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

      // BldgSeqNum
      iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);

      lCharMatch++;

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (!pRec)
         break;
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (!iLoop && (iBeds > 0 && iBldgNum > 0))
         break;
   }

   return 0;
}

/******************************** Edx_MergeOwner *****************************
 *
 * Merge owner from roll file.  This function can only be used when both name1 & name2 are populate.
 *
 * Return 0 if successful, >0 if error
 *
 ******************************************************************************/

void Edx_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf=NULL, char *pDba=NULL)
{
   int   iTmp, iRet;
   char  acTmp1[128], acTmp[128], *pTmp;
   char  acName1[128], acOwner[128];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, true, true);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "011101017000", 9))
   //   iTmp = 0;
#endif

   // CareOf
   if (pCareOf && *pCareOf > ' ')
      iTmp = updateCareOf(pOutbuf, pCareOf, SIZ_CARE_OF);

   // DBA
   if (pDba && *pDba > ' ')
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

   // Remove multiple spaces
   strcpy(acName1, pNames);
   if (pTmp = strchr(acName1, '\\'))
      *pTmp = '/';
   iTmp = blankRem(acName1);
   
   // Check for year that goes before TRUST
   iTmp =0;
   while (acName1[iTmp])
   {
      if (isdigit(acName1[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
      memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);
      return;
   }

   // Update vesting
   updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Save owner name
   strncpy(acOwner, acName1, SIZ_NAME1);
   acOwner[SIZ_NAME1] = 0;
   strcpy(acTmp, acName1);

   // Filter out words
   if ((pTmp=strstr(acTmp, " ETAL")) || 
       (pTmp=strstr(acTmp, " ET AL")) || 
       (pTmp=strstr(acTmp, " & FBO")) ||
       (pTmp=strstr(acTmp, " TRUSTEE")) ||
       (pTmp=strstr(acTmp, " SUC CO TR")) ||
       (pTmp=strstr(acTmp, " SUC TR")) )
      *pTmp = 0;

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(acTmp1, myOwner.acName1);
      if (myOwner.acVest[0] > ' ' && *(pOutbuf+OFF_VEST) == ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
         iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
      else
         iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
      iTmp = vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);

   iTmp = strlen(acOwner);
   if (acOwner[iTmp-1] == '&')
      iTmp -= 1;
   memcpy(pOutbuf+OFF_NAME1, acOwner, iTmp);
}

/******************************** Edx_MergeOwner *****************************
 *
 * Merge owner from owner file. 
 * Currently owner file may include CareOf & DBA.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

void Edx_CombineOwner(char *pOutName1, char *pOutName2, char *pInName1, char *pInName2, char *pSwapName)
{
   int   iTmp, iMrgFlg;
   char  acTmp[64], acName1[64], acName2[64];
   char  *pTmp;

   OWNER myOwner;

   // Initialize
   *pOutName1 = 0;
   *pOutName2 = 0;
   sprintf(acName1, "%s ", pInName1);
   sprintf(acName2, "%s ", pInName2);

   // Don't swap or merge Trust
   if (strstr(acName1, " TR ") || strstr(acName2, " TR ")
      || strstr(acName1, " TRUST") || strstr(acName2, " TRUST")
      || strstr(acName1, " LLC")
      || strstr(acName1, " DTR") )
   {
      blankRem(acName1);
      strcpy(pOutName1, acName1);
      if (acName2[0] > ' ')
      {
         blankRem(acName2);
         strcpy(pOutName2, acName2);
      }
      strcpy(acTmp, acName1);
      iTmp = splitOwner(acTmp, &myOwner, 5);
      if (iTmp < 0)     
         strcpy(pSwapName, acName1);
      else
         strcpy(pSwapName, myOwner.acSwapName);

      return;
   } 

   // Merge name only if exist
   iMrgFlg = MergeName2(acName1, acName2, acTmp, ' ');

   if (iMrgFlg == 1)
   {
      // Name merged OK - drop name2
      strcpy(acName1, acTmp);
      acName2[0] = 0;
   } else if (iMrgFlg == 2)
   {
      // Same name - drop name2
      acName2[0] = 0;
   } else if (iMrgFlg == 99 || !iMrgFlg)
   {
      // Cannot merge - Keep them as is
      if (!strcmp(acName1, acName2))
         acName2[0] = 0;
   }

   remChar(acName1, ',');
   pTmp = myTrim(acName1);

   // Now parse owners
   strcpy(acTmp, pTmp);
   iTmp = splitOwner(acTmp, &myOwner, 5);
   if (iTmp < 0)     
      strcpy(pSwapName, acName1);
   else
      strcpy(pSwapName, myOwner.acSwapName);

   strcpy(pOutName1, acName1);
   if (acName2[0] > ' ')
   {
      remChar(acName2, ',');
      strcpy(pOutName2, acName2);
   }
}

int Edx_MergeOwnerRec(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     *pTmp, *apItems[20], sName1[64], sName2[64], sNames[128], sTitle1[32], sTitle2[32], 
            sNewName1[64], sNewName2[64], sTmp[1024];
   int      iLoop, iTmp, iItems;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdName);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001238012", 9))
   //   iTmp = 0;
#endif

StartLoop:

   do
   {
      if (!pRec)
      {
         fclose(fdName);
         fdName = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         //if (bDebug)
         //   LogMsg0("Skip Owner rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdName);
         lNameSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Parse owner record
   iItems = ParseStringIQ(acRec, cDelim, 20, apItems);
   if (iItems < EDX_OWNR_OWNERFULL)
   {
      LogMsg("***** Error: bad owner record for APN=%s (#tokens=%d)", acRec, iItems);
      return -1;
   }

   // Ignore CareOf
   if (*apItems[EDX_OWNR_OWNERLAST] == '%')
   {
      pRec = fgets(acRec, 1024, fdName);
      goto StartLoop;
   }

   lNameMatch++;

   // Initialize
   sNames[0] = 0;
   sNewName2[0] = 0;
   sTitle2[0] = 0;
   strcpy(sTitle1, apItems[EDX_OWNR_TITLE]);

   // Get next record and check for 2nd owner
   sTmp[0] = 0;
   if (!feof(fdName))
   {
      pRec = fgets(sTmp, 1024, fdName);
   }

#ifdef _DEBUG
   if (!memcmp(pOutbuf, "025603007", 9))
      iTmp = 0;
#endif

   if (pRec && !memcmp(sTmp, acRec, iApnLen))
   {
      // Process name1
      if (*apItems[EDX_OWNR_OWNERFIRST] > ' ')
      {
         if (strchr(apItems[EDX_OWNR_OWNERLAST], ','))
            sprintf(sName1, "%s %s %s", apItems[EDX_OWNR_OWNERLAST], apItems[EDX_OWNR_OWNERFIRST], apItems[EDX_OWNR_OWNERMIDDLE]);
         else
            sprintf(sName1, "%s %s %s", apItems[EDX_OWNR_OWNERLAST], apItems[EDX_OWNR_OWNERFIRST], apItems[EDX_OWNR_OWNERMIDDLE]);
      } else
         strcpy(sName1, apItems[EDX_OWNR_OWNERLAST]);

      // Process Name2
      iItems = ParseStringIQ(sTmp, cDelim, 20, apItems);
      if (*apItems[EDX_OWNR_OWNERFIRST] > ' ')
         sprintf(sName2, "%s %s %s", apItems[EDX_OWNR_OWNERLAST], apItems[EDX_OWNR_OWNERFIRST], apItems[EDX_OWNR_OWNERMIDDLE]);
      else
         strcpy(sName2, apItems[EDX_OWNR_OWNERLAST]);
      strcpy(sTitle2, apItems[EDX_OWNR_TITLE]);

      // Drop Name2 if it's the same as Name1
      if (!strcmp(sName1, sName2))
      {
         sName2[0] = 0;
         sTitle2[0] = 0;
      }

      if (sName2[0] == '%')
      {
         sNewName2[0] = 0;
         strcpy(sNewName1, sName1);
         if (strchr(sName1, ',') && !strstr(sName1, " TR ") && !strstr(sName1, " GROUP"))
            swapName(sName1, sNames, false);
         else
            strcpy(sNames, sName1);
      } else //if (!strstr(sName1, " TR "))
      {
         remChar(sName1, ',');
         remChar(sName2, ',');

         // Merge owner - Only handle record with 2 owners
         Edx_CombineOwner(sNewName1, sNewName2, sName1, sName2, sNames);
      //} else
      //{
      //   strcpy(sNewName1, sName1);
      //   strcpy(sNewName2, sName2);
      //   strcpy(sNames, sName1);
      }

      // If names can't combined, add title to name1
      // If both names has title, keep them separate
      if (sTitle1[0] > ' ' && sTitle2[0] > ' ')
      {
         sprintf(sNewName1, "%s %s", sName1, sTitle1);
         strcpy(sName1, sNewName1);
         sprintf(sNewName2, "%s %s", sName2, sTitle2);
         strcpy(sName2, sNewName2);
      } else if ((sName2[0] <= ' ' || sNewName2[0] <= ' ') && sTitle1[0] > ' ')
      {
         sprintf(sName1, "%s %s", sNewName1, sTitle1);
         sName2[0] = 0;
      } else
      {
         strcpy(sName1, sNewName1);
         strcpy(sName2, sNewName2);
      }

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdName);
      while (!memcmp(pOutbuf, pRec, iApnLen))
      {
         // More than 1 name records
         pRec = fgets(acRec, 1024, fdName);
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      }
   } else
   {
      sName2[0] = 0;

      // Process single name
      if (*apItems[EDX_OWNR_OWNERFIRST] > ' ')
      {
         remChar(apItems[EDX_OWNR_OWNERLAST], ',');
         if ((strlen(apItems[EDX_OWNR_OWNERLAST])+strlen(apItems[EDX_OWNR_OWNERFIRST])+strlen(apItems[EDX_OWNR_OWNERMIDDLE])) > 50)
         {
            strcpy(sName1, apItems[EDX_OWNR_OWNERLAST]);
            if (*apItems[EDX_OWNR_OWNERFIRST] == '&')
               strcpy(sName2, apItems[EDX_OWNR_OWNERFIRST]+2);
            else
               strcpy(sName2, apItems[EDX_OWNR_OWNERFIRST]);
            strcpy(sNames, apItems[EDX_OWNR_OWNERLAST]);
         } else
         {
            if (sTitle1[0] > '9')
               sprintf(sName1, "%s %s %s %s", apItems[EDX_OWNR_OWNERLAST], apItems[EDX_OWNR_OWNERFIRST], 
                  apItems[EDX_OWNR_OWNERMIDDLE], apItems[EDX_OWNR_TITLE]);
            else
               sprintf(sName1, "%s %s %s", apItems[EDX_OWNR_OWNERLAST], apItems[EDX_OWNR_OWNERFIRST], apItems[EDX_OWNR_OWNERMIDDLE]);
            sprintf(sNames, "%s %s %s", apItems[EDX_OWNR_OWNERFIRST], apItems[EDX_OWNR_OWNERMIDDLE], apItems[EDX_OWNR_OWNERLAST]);
         }
      } else
      { 
         OWNER myOwner;
         pTmp = strchr(apItems[EDX_OWNR_OWNERLAST], ',');
         if (pTmp)
         {
            remChar(apItems[EDX_OWNR_OWNERLAST], ',');
            sprintf(sName1, "%s %s", apItems[EDX_OWNR_OWNERLAST], apItems[EDX_OWNR_TITLE]);
            iTmp = splitOwner(apItems[EDX_OWNR_OWNERLAST], &myOwner, 5);
            if (iTmp >= 0)
               strcpy(sNames, myOwner.acSwapName);
            else
               strcpy(sNames, sName1);
            //pTmp = swapName(apItems[EDX_OWNR_OWNERLAST], sNames, false);
         } else
         {
            sprintf(sName1, "%s %s", apItems[EDX_OWNR_OWNERLAST], apItems[EDX_OWNR_TITLE]);
            strcpy(sNames, sName1);
         }
      }

      iTmp = blankRem(sName1);

      // Save current record
      strcpy(acRec, sTmp);
   }

   // Remove old names
   removeNames(pOutbuf, false, false);

   // Upate owners
   remChar(sName1, ',');
   vmemcpy(pOutbuf+OFF_NAME1, sName1, SIZ_NAME1);
   if (sName2[0] > ' ')
   {
      remChar(sName2, ',');
      vmemcpy(pOutbuf+OFF_NAME2, sName2, SIZ_NAME2);
   }

   // Update swapname
   char *p1, *p2, *p3, *p4;
   if (sTitle1[0] < ' ' && 
      ((p1=strstr(sName1, " TR ")) || 
       (p2=strstr(sName1, " LLC")) ||
       (p3=strstr(sName1, " ESTATE")) ||
       (p4=strstr(sName1, " INC")) ))
   {
      if (p1)
         memcpy(pOutbuf+OFF_VEST, "TR", 2);
      else if (p2)
         memcpy(pOutbuf+OFF_VEST, "CO", 2);
      else if (p3)
         memcpy(pOutbuf+OFF_VEST, "ES", 2);
      else
         memcpy(pOutbuf+OFF_VEST, "CO", 2);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, sName1, SIZ_NAME1);
   } else if (!memcmp(sNames, "U.S.A.", 6))
   {
      memcpy(pOutbuf+OFF_VEST, "GV", 2);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames, SIZ_NAME1);
   } else
   {
      remChar(sNames, ',');
      vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames, SIZ_NAME1);
   }

   // Update Vesting
   if (sTitle1[0] > '9')
   {
      sprintf(sTmp, " %s", sTitle1);
      pTmp = findVesting(sTmp, sTitle1);
      if (pTmp)
         vmemcpy(pOutbuf+OFF_VEST, sTitle1, SIZ_VEST);
      else
         iTmp = 0;
   }

   return 0;
}

/********************************* Edx_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Edx_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse input
   iTmp = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad input record for APN=%s (tokens=%d)", apTokens[iApnFld], iTmp);
      return -1;
   }

   // Ignore APN starts with 800-999 except 910 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || iTmp == 555 ||  (iTmp >= 800 && iTmp != 910 ))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "09EDX", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   _strupr(apTokens[MB_ROLL_LEGAL]);
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "025553002000", 9))
   //   iTmp = 0;
#endif

   // UseCode
   if (*apTokens[MB_ROLL_USECODE] > ' ')
   {
      char sUseCode[32];
  
      // Std Usecode - iTmp is dummy here, not used
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[MB_ROLL_USECODE], iTmp, pOutbuf);
      strcpy(sUseCode, apTokens[MB_ROLL_USECODE]);

      // Check for UseCode2 - If found, append to UseCode1
      if (iRet = EDX_FindUseCode2(pOutbuf, acTmp))
         strcpy(&sUseCode[2], acTmp);
      memset(pOutbuf+OFF_USE_CO, ' ', SIZ_USE_CO);
      vmemcpy(pOutbuf+OFF_USE_CO, sUseCode, SIZ_USE_CO);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      lLotSqftCount++;

      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0' && *(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
   {
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp && !memcmp(acTmp, apTokens[MB_ROLL_DOCNUM], 4))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], strlen(apTokens[MB_ROLL_DOCNUM]));
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
      }
   }

   // Owner
   try {
      Edx_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER], apTokens[MB_ROLL_CAREOF], apTokens[MB_ROLL_DBA]);
   } catch(...) {
      LogMsg("***** Exeception occured in Edx_MergeOwner()");
   }

   // Mailing
   try {
      if (*apTokens[MB_ROLL_M_ADDR] > ' ')
         Edx_MergeMAdr(pOutbuf);
      else if (*apTokens[MB_ROLL_M_ADDR1] > ' ')
         Edx_MergeMAdr(pOutbuf, apTokens[MB_ROLL_M_ADDR1], apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3], apTokens[MB_ROLL_M_ADDR4]);
   } catch(...) {
      LogMsg("***** Exeception occured in Edx_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************* Edx_Load_Roll ******************************
 *
 * Handle Edx_Roll.csv file and the like
 *
 ******************************************************************************/

int Edx_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Load %s roll update file", myCounty.acCntyCode);

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Fix broken records in roll file
   sprintf(acTmpFile, "%s\\%s\\%s_roll.fix", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = RebuildCsv(acRollFile, acTmpFile, cDelim, MB_ROLL_M_ADDR4);
   lLastFileDate = getFileDate(acRollFile);

   // Sort roll file
   sprintf(acRollFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acTmpFile, acRollFile);
   iRet = sortFile(acTmpFile, acRollFile, "S(#1,C,A)");
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"Z\") DEL(124) ");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   fdTax = fopen(acTmpFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
      return -2;
   }

   // Open Owner file
   LogMsg("Open Owner file %s", acNameFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Owner.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acNameFile, acTmpFile, "S(#1,C,A,#2,C,D) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acNameFile, acTmpFile, "S(#1,C,A,#2,C,D) ");

   fdName = fopen(acTmpFile, "r");
   if (fdName == NULL)
   {
      LogMsg("***** Error opening Owner file: %s\n", acTmpFile);
      return -2;
   }

   // Get UseCode2 file
   GetIniString(myCounty.acCntyCode, "UseCode2", "", acTmpFile, _MAX_PATH, acIniFile);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open UseCode2 file file %s", acTmpFile);
      fdUseCode = fopen(acTmpFile, "r");
      if (fdUseCode == NULL)
      {
         LogMsg("***** Error opening UseCode2 file: %s\n", acTmpFile);
         return -2;
      }
   } else
      fdUseCode = NULL;

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
      replNull(acRollRec);
      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Edx_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Edx_MergeSitus(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Edx_MergeStdChar(acBuf);

            // Merge Owner
            //if (fdName)
            //   lRet = Edx_MergeOwnerRec(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acBuf);        // Edx_Tax.csv

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Edx_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Edx_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Edx_MergeStdChar(acRec);

            // Merge Owner
            //if (fdName)
            //   lRet = Edx_MergeOwnerRec(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      replNull(acRollRec);
      iRet = Edx_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Edx_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Edx_MergeStdChar(acRec);

         // Merge Owner
         //if (fdName)
         //   lRet = Edx_MergeOwnerRec(acBuf);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTax(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fdName)
      fclose(fdName);
   if (fdUseCode)
      fclose(fdUseCode);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("      new records:          %u", iNewRec);
   LogMsg("      updated records:      %u", iRollUpd);
   LogMsg("      retired records:      %u", iRetiredRec);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   //LogMsg("Number of Owner matched:    %u", lNameMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   //LogMsg("Number of Owner skiped:     %u", lNameSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u\n", lRecCnt);
   return 0;
}

/********************************* Edx_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Edx_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);
   replStrAll(pRollRec, "NULL", "");
   
   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L10_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L10_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L10_ASMT], strlen(apTokens[L10_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L10_FEEPARCEL], strlen(apTokens[L10_FEEPARCEL]));

   // Prev APN
   //iRet = sprintf(acTmp, "%.6s%.2d100", apTokens[L10_ASMT], atoin(apTokens[L10_ASMT]+6, 3));
   //memcpy(pOutbuf+OFF_PREV_APN, acTmp, iRet);

   // Format APN
   iRet = formatApn(apTokens[L10_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   if (acTmp[0] >= '8')
   {
      iRet = formatMapLink(apTokens[L10_FEEPARCEL], acTmp, &myCounty);
      sprintf(acTmp1, "%.3s\\%.3s00", apTokens[L10_ASMT], apTokens[L10_ASMT]);
   } else
   {
      iRet = formatMapLink(apTokens[L10_ASMT], acTmp, &myCounty);
      getIndexPage(acTmp, acTmp1, &myCounty);
   }
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
   memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, strlen(acTmp1));

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "09EDX", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L10_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L10_TRA], strlen(apTokens[L10_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L10_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L10_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L10_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L10_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L10_GROWING]);
   long lPers  = atoi(apTokens[L10_PPVALUE]);
   long lPP_MH = atoi(apTokens[L10_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L10_HOX]);
   long lExe2 = atol(apTokens[L10_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L10_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L10_OTHEREXEMPTIONCODE], strlen(apTokens[L10_OTHEREXEMPTIONCODE]));

   // Legal
   updateLegal(pOutbuf, apTokens[L10_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L10_LANDUSE1] > ' ')
   {
      lTmp = atol(apTokens[L10_LANDUSE2]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%s%.2d", myTrim(apTokens[L10_LANDUSE1]), lTmp);
         vmemcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO, iTmp);
      } else
         vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L10_LANDUSE1], SIZ_USE_CO);

   
      // Std Usecode - iTmp is dummy here, not used
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L10_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L10_ACRES]);
   lTmp = atol(apTokens[L10_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, iTmp);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, iTmp);
   } else if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, iTmp);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, iTmp);
   }

   // AgPreserved
   if (*apTokens[L10_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Edx_MergeOwner(pOutbuf, apTokens[L10_OWNER]);

   // Situs
   //Edx_MergeSitus(pOutbuf, apTokens[L10_SITUS1], apTokens[L10_SITUS2]);

   // Mailing
   Edx_MergeMAdr(pOutbuf, apTokens[L10_MAILADDRESS1], apTokens[L10_MAILADDRESS2], apTokens[L10_MAILADDRESS3], apTokens[L10_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L10_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*apTokens[L10_CURRENTDOCNUM] > '0' && !memcmp(apTokens[L10_CURRENTDOCDATE], apTokens[L10_CURRENTDOCNUM], 4))
   {
      pTmp = dateConversion(apTokens[L10_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp && isNumber(apTokens[L10_CURRENTDOCNUM]+5))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L10_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 

   // # of Units
   lTmp = atol(apTokens[L10_UNITS]);
   if (lTmp > 0 && lTmp < 9999)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, iTmp);
   }

   // Garage
   iTmp = iTrim(apTokens[L10_GARAGE]);
   if (strchr(apTokens[L10_GARAGE], '-'))
   {
      int iGar=0, iCarp=0;
      char *pTmp1;

      if (pTmp = strchr(apTokens[L10_GARAGE], ' '))
      {
         //*pTmp++ = 0;
         pTmp++;
         if (pTmp1 = strchr(pTmp, '-'))
         {
            pTmp1++;
            if (*pTmp1 == 'G')
               iGar = atol(pTmp);
            else if (*pTmp1 == 'C')
               iCarp = atol(pTmp);
            else
               iTmp = 0;
         } else
            iTmp = 0;
      }

      *pTmp = 0;
      if (pTmp1 = strchr(apTokens[L10_GARAGE], '-'))
      {
         pTmp1++;
         if (*pTmp1 == 'G')
            iGar += atol(pTmp);
         else if (*pTmp1 == 'C')
            iCarp += atol(pTmp);
         else
            iTmp = 0;
      } else
         iTmp = 0;

      if (iGar > 0)
      {
         iTmp = sprintf(acTmp, "%d", iGar+iCarp);
         memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, iTmp);
         if (iCarp > 0)
            *(pOutbuf+OFF_PARK_TYPE) = '2';
         else
            *(pOutbuf+OFF_PARK_TYPE) = 'Z';
      } else if (iCarp > 0)
      {
         iTmp = sprintf(acTmp, "%d", iCarp);
         memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, iTmp);
         *(pOutbuf+OFF_PARK_TYPE) = 'C';
      }
   } else if (*apTokens[L10_GARAGE] > '0' && iTmp < 4)
   {
      lTmp = atol(apTokens[L10_GARAGE]);
      iTmp = sprintf(acTmp, "%d", lTmp);
      vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE, iTmp);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001012035000", 9))
   //   iTmp = 0;
#endif

   // Sewer
   if (*apTokens[L10_SEWER] == 'Y')
      *(pOutbuf+OFF_SEWER) = 'Y';

   return 0;
}

/********************************* Edx_MergeLien3 *****************************
 *
 * For 2019 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Edx_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);
   replStrAll(pRollRec, "NULL", "");
   
   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Skip book 555
   if (!memcmp(apTokens[L3_ASMT], "555", 3))
      return 1;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Prev APN
   //iRet = sprintf(acTmp, "%.6s%.2d100", apTokens[L3_ASMT], atoin(apTokens[L3_ASMT]+6, 3));
   //memcpy(pOutbuf+OFF_PREV_APN, acTmp, iRet);

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   if (acTmp[0] >= '8')
   {
      iRet = formatMapLink(apTokens[L3_FEEPARCEL], acTmp, &myCounty);
      sprintf(acTmp1, "%.3s\\%.3s00", apTokens[L3_ASMT], apTokens[L3_ASMT]);
   } else
   {
      iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
      getIndexPage(acTmp, acTmp1, &myCounty);
   }
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
   memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, strlen(acTmp1));

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "09EDX", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&EDX_Exemption);

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      char sUseCode[32];
  
      // Std Usecode - iTmp is dummy here, not used
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
      strcpy(sUseCode, apTokens[L3_LANDUSE1]);

      // Check for UseCode2 - If found, append to UseCode1
      if (iRet = EDX_FindUseCode2(pOutbuf, acTmp))
         strcpy(&sUseCode[2], acTmp);
      vmemcpy(pOutbuf+OFF_USE_CO, sUseCode, SIZ_USE_CO);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, iTmp);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, iTmp);
   } else if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, iTmp);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, iTmp);
   }

   // AgPreserved
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Edx_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   //Edx_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Edx_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*apTokens[L3_CURRENTDOCNUM] > '0' && !memcmp(apTokens[L3_CURRENTDOCDATE], apTokens[L3_CURRENTDOCNUM], 4))
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp && isNumber(apTokens[L3_CURRENTDOCNUM]+5))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 

   // # of Units
   lTmp = atol(apTokens[L3_UNITS]);
   if (lTmp > 0 && lTmp < 9999)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, iTmp);
   }

   // Garage
   // 1C, 1-CP, 1C1G, SP, T2, RV2
   iTmp = iTrim(apTokens[L3_GARAGE]);
   if (iTmp > 0)
   {
      int iGar, iCarp=0;
      char *pTmp1=apTokens[L3_GARAGE];

      // Assume garage
      iGar = atol(pTmp1);

      // ?-CP
      if (pTmp = strchr(apTokens[L3_GARAGE], '-'))
      {
         if (*(pTmp+1) == 'C')
         {
            iGar = 0;
            iCarp = *(pTmp-1) - 48;
         }
      } else if (*(pTmp1+1) == 'C' && *(pTmp1+3) == 'G')
      {
         iCarp = *(pTmp1) - 48;
         iGar  = *(pTmp1+2) - 48;
      } else if (iGar > 9)
         iGar = 0;

      if (iGar > 0)
      {
         iTmp = sprintf(acTmp, "%d", iGar+iCarp);
         memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, iTmp);
         if (iCarp > 0)
            *(pOutbuf+OFF_PARK_TYPE) = '2';
         else
            *(pOutbuf+OFF_PARK_TYPE) = 'Z';
      } else if (iCarp > 0)
      {
         iTmp = sprintf(acTmp, "%d", iCarp);
         memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, iTmp);
         *(pOutbuf+OFF_PARK_TYPE) = 'C';
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001012035000", 9))
   //   iTmp = 0;
#endif

   return 0;
}

/********************************* Edx_MergeLien *****************************
 *
 * For 2018 LDR EL_DORADO_COUNTY_SECURED_2018_ROLL_COMPLETE.TXT
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Edx_MergeLienEx(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replStrAll(pRollRec, "#N/A", " ");

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < EDX_LDR_COUNT)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[0]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Format APN
   memcpy(pOutbuf+OFF_APN_D, apTokens[EDX_LDR_NEW_FMTAPN], strlen(apTokens[EDX_LDR_NEW_FMTAPN]));

   iRet = remChar(apTokens[EDX_LDR_NEW_FMTAPN], '-');
   memcpy(pOutbuf, apTokens[EDX_LDR_NEW_FMTAPN], iRet);

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[EDX_LDR_NEW_FMTAPN], iRet);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "526550022000", 9))
   //   iTmp = 0;
#endif
   // Prev APN
   memcpy(pOutbuf+OFF_PREV_APN, apTokens[EDX_LDR_OLDAPN], strlen(apTokens[EDX_LDR_OLDAPN]));

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[EDX_LDR_NEW_FMTAPN], acTmp, &myCounty);
   getIndexPage(acTmp, acTmp1, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
   memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, strlen(acTmp1));

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "09EDX", 5);

   // status
   *(pOutbuf+OFF_STATUS) = 'A';

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[EDX_LDR_TRA], strlen(apTokens[EDX_LDR_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[EDX_LDR_LAND_VALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[EDX_LDR_STRUCTURE_VALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Timber, have no value
   long lFixtr  = atoi(apTokens[EDX_LDR_EQUIPMENT_VALUE]);
   long lOthImpr= atoi(apTokens[EDX_LDR_IMPR_OTHER_VALUE]);
   long lGrow   = atoi(apTokens[EDX_LDR_PLANTS_TREE_VALUE]);
   long lMineral= atoi(apTokens[EDX_LDR_MINERAL_GRAZING_VALUE]);
   long lPers   = atoi(apTokens[EDX_LDR_BUSINV_VALUE]);          
   long lPP_MH  = atoi(apTokens[EDX_LDR_PP_OTHER]);

   lTmp = lFixtr+lOthImpr+lGrow+lMineral+lPers+lPP_MH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_OTHER);
      }
      if (lOthImpr > 0)
      {
         sprintf(acTmp, "%d         ", lOthImpr);
         memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_OTHER);
      }
      if (lMineral > 0)
      {
         sprintf(acTmp, "%d         ", lMineral);
         memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_OTHER);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_OTHER);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_OTHER);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[EDX_LDR_EXEMPTION_1_AMOUNT]);
   long lExe2 = atol(apTokens[EDX_LDR_EXEMPTION_2_AMOUNT]);
   long lExe3 = atol(apTokens[EDX_LDR_EXEMPTION_3_AMOUNT]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = atol(apTokens[EDX_LDR_EXEMPTION_1_CODE]);
   if (lExe1 == 7000 || lExe2 == 7000)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      *(pOutbuf+OFF_EXE_CD1) = *apTokens[EDX_LDR_EXEMPTION_1_CODE];
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Legal
   updateLegal(pOutbuf, apTokens[EDX_LDR_LEADLINE]);

   // UseCode
   if (*apTokens[EDX_LDR_USECODE_1] > ' ')
   {
      if (*apTokens[EDX_LDR_USECODE_2] > ' ')
      {
         sprintf(acTmp, "%s%.2d", myTrim(apTokens[EDX_LDR_USECODE_1]), atol(apTokens[EDX_LDR_USECODE_2]));
         vmemcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO);
      } else
         vmemcpy(pOutbuf+OFF_USE_CO, apTokens[EDX_LDR_USECODE_1], SIZ_USE_CO);

   
      // Std Usecode - iTmp is dummy here, not used
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[EDX_LDR_USECODE_1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres - x1000
   lTmp = atol(apTokens[EDX_LDR_ACREAGE]);
   if (lTmp > 0)
   {
      // Format Acres
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      // Lot Sqft
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, (long)((double)lTmp*SQFT_FACTOR_1000));
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Zoning
   if (*apTokens[EDX_LDR_CUR_PRIMARY_ZONING] > ' ')
   {
      if (*apTokens[EDX_LDR_CUR_SECONDARY_ZONING] > ' ')
      {
         iTmp = sprintf(acTmp, "%s;%s", apTokens[EDX_LDR_CUR_PRIMARY_ZONING], apTokens[EDX_LDR_CUR_SECONDARY_ZONING]);
         vmemcpy(pOutbuf+OFF_ZONE, acTmp, SIZ_ZONE, iTmp);
         if (*(pOutbuf+OFF_ZONE_X1) == ' ')
            vmemcpy(pOutbuf+OFF_ZONE_X1, acTmp, SIZ_ZONE_X1, iTmp);
      } else
      {
         vmemcpy(pOutbuf+OFF_ZONE, apTokens[EDX_LDR_CUR_PRIMARY_ZONING], SIZ_ZONE);
         if (*(pOutbuf+OFF_ZONE_X1) == ' ')
            vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[EDX_LDR_CUR_PRIMARY_ZONING], SIZ_ZONE_X1);
      }
   }

   // Owner
   char sName1[64], sName2[64], sNameSwap[64];

   if (*apTokens[EDX_LDR_NAME_2] == '%' || !memcmp(apTokens[EDX_LDR_NAME_2], "C/O", 3) || !memcmp(apTokens[EDX_LDR_NAME_2], "ATTN", 4))
   {
      updateCareOf(pOutbuf, apTokens[EDX_LDR_NAME_2], strlen(apTokens[EDX_LDR_NAME_2]));
      *apTokens[EDX_LDR_NAME_2] = 0;
   } else if (!memcmp(apTokens[EDX_LDR_NAME_2], "DBA", 3))
   {
      vmemcpy(pOutbuf+OFF_DBA, apTokens[EDX_LDR_NAME_2], SIZ_DBA);
      *apTokens[EDX_LDR_NAME_2] = 0;
   }
   Edx_CombineOwner(sName1, sName2, apTokens[EDX_LDR_NAME_1], apTokens[EDX_LDR_NAME_2], sNameSwap);
   vmemcpy(pOutbuf+OFF_NAME1, sName1, SIZ_NAME1);
   vmemcpy(pOutbuf+OFF_NAME_SWAP, sNameSwap, SIZ_NAME_SWAP);
   if (sName2[0] > ' ')
      vmemcpy(pOutbuf+OFF_NAME2, sName2, SIZ_NAME2);

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, sName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
   if (!iTmp && sName2[0] > ' ')
      iTmp = updateVesting(myCounty.acCntyCode, sName2, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Situs - via situs file
   //Edx_MergeSitus(pOutbuf, apTokens[EDX_LDR_SITUS1], apTokens[EDX_LDR_SITUS2]);

   // Mailing 
   Edx_MergeMAdrEx(pOutbuf, apTokens[EDX_LDR_MAILING_STREET], apTokens[EDX_LDR_MAILING_ZIP_CODE]);

   // SetTaxcode, Prop8 flag, FullExe flag - via roll file
   //iTmp = updateTaxCode(pOutbuf, apTokens[EDX_LDR_TAXABILITYFULL], true, true);

   // # of Units
   lTmp = atol(apTokens[EDX_LDR_TOT_LIVING_UNITS]);
   if (lTmp > 1 && lTmp < 9999)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, iTmp);
   }

   return 0;
}

/***************************** Edx_UpdatePrevApn ****************************
 *
 * Update PREV_APN. This function is used at LDR 
 * If success, rename output file to S01.
 *
 ****************************************************************************/

int Edx_UpdatePrevApn(char *pOutbuf, FILE *fdApn)
{
   static   char acRec[512], acOldApn[32], acNewApn[32], *pRec=NULL;
   char     *pTmp, *apItems[16];
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do
      {
         pRec = fgets(acRec, 512, fdApn);
         // Parse input
         iTmp = ParseStringIQ(pRec, ',', 16, apItems);
         strcpy(acOldApn, apItems[EDX_APN_OLD]);
         strcpy(acNewApn, apItems[EDX_APN_NEW]);
      } while (!isdigit(acRec[1]));
   }

   pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdApn);
         fdApn = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf+OFF_APN_D, acNewApn, 15);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip APN rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdApn);
         // Parse input
         iTmp = ParseStringIQ(pRec, ',', 16, apItems);
         strcpy(acOldApn, apItems[EDX_APN_OLD]);
         strcpy(acNewApn, apItems[EDX_APN_NEW]);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006181030000", 9) || !memcmp(pOutbuf, "027112029000", 9))
   //   iTmp = 0;
#endif
   
   iTmp = remChar(acOldApn, '-');
   memcpy(pOutbuf+OFF_PREV_APN, acOldApn, iTmp);

   // Get next record
   pRec = fgets(acRec, 512, fdApn);
   iTmp = ParseStringIQ(pRec, ',', 16, apItems);
   strcpy(acOldApn, apItems[EDX_APN_OLD]);
   strcpy(acNewApn, apItems[EDX_APN_NEW]);

   return 0;
}

/******************************** Edx_MergeOthers ****************************
 *
 * Merge legal,taxcode, neighborhood code from roll file
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Edx_MergeOthers(char *pOutbuf)
{
   static   char acRec[MAX_RECSIZE], *pRec=NULL;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Roll rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iTmp = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MB_ROLL_M_ADDR4)
   {
      LogMsg("***** Error: bad input record for APN=%s (tokens=%d)", apTokens[MB_ROLL_ASMT], iTmp);
      return -1;
   }
   lRollMatch++;

   // Merge legal
   if (*apTokens[MB_ROLL_LEGAL] > ' ')
   {
      iTmp = updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);
      if (iTmp > iMaxLegal)
         iMaxLegal = iTmp;
   }

   // Update tax code
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   // Update neightborhood
   if (*apTokens[MB_ROLL_NBHCODE] > ' ')
      vmemcpy(pOutbuf+OFF_NBH_CODE, apTokens[MB_ROLL_NBHCODE], SIZ_NBH_CODE);

   // Get next record
   pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
   if (!pRec)
   {
      fclose(fdRoll);
      fdRoll = NULL;
   }

   return 0;
}

/******************************** Edx_Load_LDR ******************************
 *
 *
 ****************************************************************************/

int Edx_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdOldNew;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg0("Loading LDR file: %s", acTmpFile);

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")");  // 2016
      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(1,13,C,A) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Old/New APN file
   iRet = GetIniString(myCounty.acCntyCode, "OldNewApn", "", acTmpFile, _MAX_PATH, acIniFile);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Old/New APN file %s", acTmpFile);
      fdOldNew = fopen(acTmpFile, "r");
      if (fdOldNew == NULL)
      {
         LogMsg("***** Error opening Old/New APN file: %s\n", fdOldNew);
         return -2;
      }
   } else
      fdOldNew = NULL;

   // Get UseCode2 file
   GetIniString(myCounty.acCntyCode, "UseCode2", "", acTmpFile, _MAX_PATH, acIniFile);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open UseCode2 file file %s", acTmpFile);
      fdUseCode = fopen(acTmpFile, "r");
      if (fdUseCode == NULL)
      {
         LogMsg("***** Error opening UseCode2 file: %s\n", acTmpFile);
         return -2;
      }
   } else
      fdUseCode = NULL;

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   do 
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   } while (pTmp && *pTmp < '0');

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      // Create new R01 record
      if (lLienYear >= 2019)
         iRet = Edx_MergeLien3(acBuf, acRec);
      else
         iRet = Edx_MergeLien(acBuf, acRec);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Edx_MergeSitus(acBuf);

         // Merge old APN
         if (fdOldNew)
            lRet = Edx_UpdatePrevApn(acBuf, fdOldNew);

         // Merge Char 
         if (fdChar)
            lRet = Edx_MergeStdChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdOldNew)
      fclose(fdOldNew);
   if (fdUseCode)
      fclose(fdUseCode);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of UseCode2 matched: %u", lUCMatch);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of UseCode2 skipped: %u", lUCSkip);
   LogMsg("Number of Situs skipped:    %u", lSitusSkip);
   LogMsg("Number of Char skipped:     %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

int Edx_Load_LDR_Ex(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;
   HANDLE   fhOut;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg0("Loading LDR file: %s", acTmpFile);

   // Sort roll file on ASMT
   sprintf(acOutFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acOutFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acOutFile, "S(#3,C,A) DEL(9) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")");  // 2016
      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open LDR file %s", acOutFile);
   fdLDR = fopen(acOutFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acOutFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(1,13,C,A) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Fix broken records in roll file
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRollFile, _MAX_PATH, acIniFile);
   sprintf(acTmpFile, "%s\\%s\\%s_roll.fix", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = RebuildCsv(acRollFile, acTmpFile, cDelim, MB_ROLL_M_ADDR4);
   lLastFileDate = getFileDate(acRollFile);

   // Sort roll file
   sprintf(acRollFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acTmpFile, acRollFile);
   iRet = sortFile(acTmpFile, acRollFile, "S(#1,C,A)");
   // Open updated roll file for Legal info
   LogMsg("Open roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   do 
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   } while (pTmp && *pTmp < '0');

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      lLDRRecCount++;
      // Create new R01 record
      iRet = Edx_MergeLienEx(acBuf, acRec);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Edx_MergeSitus(acBuf);

         // Merge other info from roll file
         if (fdRoll)
            lRet = Edx_MergeOthers(acBuf);
         
         // Merge Char 
         if (fdChar)
            lRet = Edx_MergeStdChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp)
         break;
   }

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Roll matched:     %u", lRollMatch);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/*************************** Edx_CleanupHistSale ******************************
 *
 * Clean up sale file.  remove all record with bad DocNum or DocDate
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Edx_CleanupHistSale(char *pInfile)
{
   char     acInbuf[1024], acOutFile[_MAX_PATH], *pRec, sTmp1[32];
   long     lCnt=0, lOut=0, iTmp;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];

   LogMsg0("Fix sale history file %s", pInfile);
   if (_access(pInfile, 0))
   {
      LogMsg("***** Edx_CleanupHistSale(): Missing input file: %s", pInfile);
      return -1;
   }

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0282630300", 9))
      //   iTmp = 0;
#endif
      memcpy(sTmp1, &pInRec->DocNum, 12);
      myTrim(sTmp1, 12);
      if (sTmp1[4] == 'R')
      {
         if (!strpbrk(sTmp1, "-+*`./"))
         {
            if (!strpbrk(sTmp1, "DAT"))
            {
               iTmp = atoi(&sTmp1[5]);
               sprintf(sTmp1, "%.5d", iTmp);
               memcpy(&pInRec->DocNum[5], sTmp1, 5);
               fputs(acInbuf, fdOut);
               lOut++;
            } else
               LogMsg("Remove 2. %s", sTmp1);
         } else
            LogMsg("Remove 1. %s", sTmp1);
      } else if (strlen(sTmp1) == 9 && isdigit(sTmp1[4]) && !memcmp(sTmp1, pInRec->DocDate, 4))
      {
         pInRec->DocNum[4] = 'R';
         memcpy(&pInRec->DocNum[5], &sTmp1[4], 5);
         fputs(acInbuf, fdOut);
         lOut++;
      } else if (strlen(sTmp1) == 11 && sTmp1[5] == 'R')
      {
         iTmp = sprintf(sTmp1, "%.4s%.6s  ", pInRec->DocDate, &pInRec->DocNum[5]);
         memcpy(pInRec->DocNum, sTmp1, iTmp);
         fputs(acInbuf, fdOut);
         lOut++;
      } else
         LogMsg("Remove %.12s", pInRec->DocNum);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if (lOut != lCnt)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".org");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Rename output file
      iTmp = rename(acOutFile, pInfile);
   } else
      LogMsg("Good sale file, no record drop!");

   LogMsg("Total input records:     %u\n", lCnt);
   LogMsg("    records cleaned:     %u", lOut);

   return iTmp;
}

 /*******************************************************************************
 *
 * Test APN 006132028000, 001211014000
 *
 ********************************************************************************/

void Edx_ConvDocNum(char *pDocNum, char *pDocDate)
{
   int   iTmp;
   char sTmp[32];

   if (*(pDocNum+4) == '-')
      return;

   if (*(pDocNum+7) != ' ')
      iTmp = 0;

   iTmp = sprintf(sTmp, "%.4sR%.7s", pDocDate, pDocNum);
   memcpy(pDocNum, sTmp, iTmp);
}

int Edx_ConvertApnSale(char *pInfile, char *pOutfile)
{
   FILE *fdIn, *fdOut;
   int   iTmp1, iCnt=0, iDrop=0;
   char  acBuf[2048], acApn[32], *pBuf;
   SCSAL_REC *pSale = (SCSAL_REC *)&acBuf[0];

   LogMsg("Convert Cum Sale file");
   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");

   while (!feof(fdIn))
   {
      pBuf = fgets(acBuf, 2048, fdIn);
      if (!pBuf)
         break;

      // Ignore test records
      if (!memcmp(pSale->DocNum, "99999", 5) || !memcmp(pSale->DocNum, "0000-", 5) || pSale->DocNum[5] == ' ' || pSale->DocNum[0] > '9' || !memcmp(acBuf, "000", 3) )
      {
         iDrop++;
         continue;
      }

      if (strlen(acBuf) > 512)
      {
         LogMsg("Bad sale rec: %s", acBuf);
         continue;
      }

//#ifdef _DEBUG
//      if (!memcmp(pSale->Apn, "027431011", 9))
//         iTmp1 = 0;
//#endif

      // Format new APN
      iTmp1 = atoin(&acBuf[6], 2);
      sprintf(acApn, "%.6s%.3d000", acBuf, iTmp1);
      memcpy(pSale->OtherApn, acBuf, 12);
      memcpy(acBuf, acApn, 12);

      // Clean up Sale Code
      if (pSale->SaleCode[0] < 'F')
         pSale->SaleCode[0] = ' ';

      // Convert DocNum
      Edx_ConvDocNum(pSale->DocNum, pSale->DocDate);

      fputs(acBuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Number of records converted %d", iCnt);
   LogMsg("                  dropped   %d", iDrop);

   return 0;
}

/*************************** Edx_ConvertApnRoll *****************************
 *
 * Convert old APN format to MB.  Keep old APN in PREV_APN.
 * 
 ****************************************************************************/

int Edx_ConvertApnRoll(char *pInfile, char *pOutfile, int iRecordLen)
{
   FILE *fdIn, *fdOut;
   int   iTmp1, iCnt=0;
   char  acBuf[2048], acTmp[32], acApn[32];

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "rb");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "wb");

   fread(acBuf, 1, iRecordLen, fdIn);
   fwrite(acBuf, 1, iRecordLen, fdOut);

   while (!feof(fdIn))
   {
      fread(acBuf, 1, iRecordLen, fdIn);

      // Format new APN
      iTmp1 = atoin(&acBuf[6], 2);
      sprintf(acApn, "%.6s%.3d000", acBuf, iTmp1);

      // Save old APN to previous APN
      memcpy(&acBuf[OFF_PREV_APN], &acBuf[0], 12);

      sprintf(acTmp, "%.3s-%.3s-%.3d-000", acBuf, &acBuf[3], iTmp1);
      memcpy(&acBuf[OFF_APN_D], acTmp, 15);

      memcpy(acBuf, acApn, 12);
      fwrite(acBuf, 1, iRecordLen, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert Roll APN completed with %d records", iCnt);

   return 0;
}

/***************************** Edx_CreateSCSale *****************************
 *
 *
 ****************************************************************************/

int Edx_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], acSaleRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC *pSale = (SCSAL_REC *)&acSaleRec[0];

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp;

   LogMsg0("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      // Remove null char
      iTmp = replNull(acRec);
      //if (iTmp < 42)
      //   iTmp = 0;

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Ignore bad DocNum
      if (!isdigit(*(apTokens[MB_SALES_DOCNUM]+5)) || *(apTokens[MB_SALES_DOCNUM]+4) != 'R')
      {
         if (bDebug)
            LogMsg("*** Bad DocNum: %s [%s]", apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_ASMT]);
         continue;
      }

      // Reset output record
      memset(acSaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(pSale->Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

      // Docnum
      memcpy(pSale->DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));

      // Doc date
      pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      if (pTmp)
      {
         memcpy(pSale->DocDate, acTmp, 8);
         lTmp = atol(acTmp);
         if (lTmp > lLastRecDate)
            lLastRecDate = lTmp;
      }

      // Group sale
      if (*apTokens[MB_SALES_GROUPSALE] == '1')
      {
         pSale->SaleCode[0] = 'P';
         pSale->MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            vmemcpy(pSale->PrimaryApn, apTokens[MB_SALES_GROUPASMT], iApnLen);
      }

#ifdef _DEBUG
      //if (!memcmp(pSale->Apn, "027071031000", 9))
      //   lPrice = 0;
#endif

      // Sale price
      lPrice = atol(apTokens[MB_SALES_PRICE]);

      // Tax
      dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
      if (acTmp[0] > '0')
      {
         dTmp = atof(acTmp);
         lTmp = (long)(dTmp * SALE_FACTOR);

         // Check for bad DocTax
         if (dTmp > 100000)
         {
            iTmp = ((int)dTmp/100)*100;
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", pSale->Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               LogMsg("??? Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f.  Need investigation.  Ignore price.", pSale->Apn, pSale->DocNum, dTmp);
               lPrice = 0;
            }
         } else
         {
            //lPrice = lTmp;
            iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
            memcpy(pSale->StampAmt, acTmp, iTmp);
         }

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
               LogMsg("*** Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d, \tTax=%.2f, \tDOCCODE=%s, DOCTYPE=%.3s", 
                  pSale->Apn, pSale->DocNum, pSale->DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE], pSale->DocType);
         }  

         // Ignore sale price if less than 1000
         if (lPrice >= 10000)
            sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
         else if (lPrice >= 1000)
            sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
         else
            memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
         memcpy(pSale->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
      }

      // Doc code 
      iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
      if (iTmp >= 0)
      {
         if (pDocTbl[iTmp].pCode[0] > '0')
         {
            memcpy(pSale->DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
            if (lPrice <= 100)
               pSale->NoneSale_Flg = pDocTbl[iTmp].flag;
         } else if (lPrice > 100)
            pSale->DocType[0] = '1';
         else
            pSale->NoneSale_Flg = 'Y';            
      } else if (lPrice > 100)
         pSale->DocType[0] = '1';

      // Save original DocCode
      vmemcpy(pSale->DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);

#ifdef _DEBUG
      //if (!memcmp(pSale->Apn, "001160016000", 9))
      //   iTmp = 0;
#endif

      // Full/Partial
      if (!memcmp(pSale->DocType, "57", 2))
         pSale->SaleCode[0] = 'P';

      // Transfer Type
      if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!_memicmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               pSale->SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      // Only output record with DocDate
      if (pSale->DocDate[0] > ' ')
      {
         // Seller
         strcpy(acTmp, apTokens[MB_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         vmemcpy(pSale->Seller1, acTmp, SALE_SIZ_SELLER);

         // Buyer
         strcpy(acTmp, apTokens[MB_SALES_BUYER]);
         iTmp = blankRem(acTmp);
         vmemcpy(pSale->Name1, acTmp, SALE_SIZ_BUYER);

         pSale->CRLF[0] = 10;
         pSale->CRLF[1] = 0;
         fputs(acSaleRec, fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc, Prev APN
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,481,1,C,D) F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -1;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            DeleteFile(acCSalFile);
   
            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -1;
      } else
         iTmp = rename(acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
         DeleteFile(acCSalFile);
      iTmp = rename(acOutFile, acCSalFile);
   }

   LogMsg("Number of records processed: %d", lCnt);
   LogMsg("                     output: %d", lTmp);
   LogMsg("        last recording date: %d\n", lLastRecDate);
   return iTmp;
}

/*********************************** loadEdx ********************************
 *
 * Options:
 *    -CEDX -L [-Xs[i]|-Ms] -Xl  [-Mr] (load lien)
 *    -CEDX -U  [-Xs[i]|-Ms] [-Z] [-Lz] [-T|Ut] [-Mr] (load update)
 *
 ****************************************************************************/

int loadEdx(int iSkip)
{
   int   iRet=0;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;
   iApnLen = myCounty.iApnLen;
   lLotSqftCount = 0;

   if (bUpdPrevApn)
   {
      // If not defined, use current apn length
      iRet = GetPrivateProfileInt(myCounty.acCntyCode, "PrevApnLen", iApnLen, acIniFile);
      iRet = updatePrevApn(myCounty.acCntyCode, iRet, iSkip);
   }

   // Loading Tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Remove sales from R01
   //iRet = PQ_FixR01(myCounty.acCntyCode, PQ_REM_SALES);
   //iRet = FixCumSale(acCSalFile, SALE_FLD_CLEANUP);

   // Load zipcode table
   if (iLoadFlag & LOAD_ZIP)                       // -Lz
   {
      char  sZipFile[_MAX_PATH], sTmpFile[_MAX_PATH];
      int   iZipLen;

      GetIniString(myCounty.acCntyCode, "ZipFile", "", sTmpFile, _MAX_PATH, acIniFile);
      iZipLen = GetPrivateProfileInt(myCounty.acCntyCode, "ZipRecSize", 53, acIniFile);

      if (!_access(sTmpFile, 0))
      {
         LogMsg0("Import zipcode to SQL");

         // Convert ebcdic to ascii
         sprintf(sZipFile, "%s\\%s\\ZipCode.asc", acTmpPath, myCounty.acCntyCode);
         LogMsg("Translate %s to Ascii %s", sTmpFile, sZipFile);

         iRet = doEBC2ASCAddCR(sTmpFile, sZipFile, iZipLen);
         if (!iRet)
         {
            // Load data into SQL table
            iRet = importZipcode(sZipFile);
            if (iRet)
               iLoadFlag = 0;
         } else
         {
            iLoadFlag = 0;
            LogMsgD("***** ERROR converting EBCDIC to ASCII file %s to %s", sTmpFile, sZipFile);
         }
      }
   }

   // Create/Update cum sale file from Edx_Sales.csv
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // EDX sale file can have different date format, use all default settings
      // MB1_CreateSCSale() is similar to MB_CreateSCSale() except it handles
      // file w/o ConfirmedSalesPrice (Edx_Sales.csv)
      // Edx_Sales.csv
      // 02/10/2021
      if (iSaleDateFmt > 0)
         iRet = Edx_CreateSCSale(iSaleDateFmt, 0, 0, true, &EDX_DocCode[0]);
      else
         iRet = Edx_CreateSCSale(MM_DD_YYYY_1, 0, 0, true, &EDX_DocCode[0]);    

      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract NDC recorder sale 
   if (iLoadFlag & EXTR_NRSAL)                     // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSalesFile, _MAX_PATH, acIniFile);
      if (!_access(acSalesFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSalesFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSalesFile);
         return -1;
      }
   }

   // Extract lien file - LDR2016
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acValueFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acValueFile, 0);      // 2016 
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode);
   }

   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Load Char file
      if (!_access(acCharFile, 0))
      {
         iRet = Edx_ConvStdChar(acCharFile);
         if (iRet <= 0)
            LogMsg("*** WARNING: Error converting Char file %s.  Use existing data.", acCharFile);
      } else
      {
         LogMsg("*** WARNING: CHAR file does not exist: %s: ", acCharFile);
         LogMsg("    -Xa option is ignore.  Please verify input file");
      }
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Create Lien file
      iRet = Edx_Load_LDR(iSkip);     
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      iRet = Edx_Load_Roll(iSkip);
      LogMsg("Total LotSqft populated: %d", lLotSqftCount);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Edx_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   // Apply NDC sale file to R01
   if (!iRet && (iLoadFlag & UPDT_XSAL))           // -Mn
   {
      // Apply Col_Ash.sls to R01 file
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSaleNR(iSkip, acCSalFile, SALE_USE_SCUPDXFR);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   return iRet;
}
