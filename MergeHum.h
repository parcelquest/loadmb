#ifndef  _MERGEHUM_H_
#define  _MERGEHUM_H_

// 08/21/2015
#define  HUM_CHAR_FEEPARCEL            0
#define  HUM_CHAR_POOLSPA              1
#define  HUM_CHAR_CATTYPE              2
#define  HUM_CHAR_QUALITYCLASS         3
#define  HUM_CHAR_YRBLT                4
#define  HUM_CHAR_BUILDINGSIZE         5
#define  HUM_CHAR_ATTACHGARAGESF       6
#define  HUM_CHAR_DETACHGARAGESF       7
#define  HUM_CHAR_CARPORTSF            8
#define  HUM_CHAR_HEATING              9
#define  HUM_CHAR_COOLINGCENTRALAC     10
#define  HUM_CHAR_COOLINGEVAPORATIVE   11
#define  HUM_CHAR_COOLINGROOMWALL      12
#define  HUM_CHAR_COOLINGWINDOW        13
#define  HUM_CHAR_STORIESCNT           14
#define  HUM_CHAR_UNITSCNT             15
#define  HUM_CHAR_TOTALROOMS           16
#define  HUM_CHAR_EFFYR                17
#define  HUM_CHAR_PATIOSF              18
#define  HUM_CHAR_BEDROOMS             19
#define  HUM_CHAR_BATHROOMS            20
#define  HUM_CHAR_HALFBATHS            21
#define  HUM_CHAR_FIREPLACE            22
#define  HUM_CHAR_ASMT                 23
#define  HUM_CHAR_BLDGSEQNUM           24
#define  HUM_CHAR_HASWELL              25
#define  HUM_CHAR_LOTSQFT              26

static XLAT_CODE  asBldgType[] =
{
   // Value, lookup code, value length
   "01", " ", 2,               // Residential
   "02", " ", 2,               // Commercial
   "03", " ", 2,               // Industrial
   "04", " ", 2,               // Agricultural
   "",   "",  0
};

static XLAT_CODE  asBldgUsedFor[] = 
{
   // Value, lookup code, value length
   "01", " ", 2,               // SFR
   "02", " ", 2,               // RES. Condo
   "03", " ", 2,               // RES. Multi
   "04", " ", 2,               // MFG Home
   "05", " ", 2,               // 
   "06", " ", 2,               // Office
   "07", " ", 2,               // Bank
   "08", " ", 2,               // Restaurant
   "09", " ", 2,               // Market
   "10", " ", 2,               // Gas Station
   "11", " ", 2,               // Mini Storage
   "12", " ", 2,               // Medical Office
   "13", " ", 2,               // Hospital
   "14", " ", 2,               // Auto sales
   "15", " ", 2,               // Church
   "16", " ", 2,               // Industrial
   "17", " ", 2,               // Light Ind.
   "19", " ", 2,               // Winery
   "20", " ", 2,               // Retail
   "",   "",  0
};

static XLAT_CODE  asCond[] =
{
   // Value, lookup code, value length
   "1", "E", 2,               // Excellent
   "2", "G", 2,               // Good
   "3", "A", 2,               // Average
   "4", "F", 2,               // Fair
   "5", "P", 2,               // Poor
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{  // Updated 8/21/2015
   // Value, lookup code, value length
   "01", "1", 2,               // Fireplace
   "02", "2", 2,               // Fireplace 2+
   "05", "H", 2,               // Hearth & Stove
   "06", "H", 2,               // Hearth & Stove 2+
   "99", "O", 2,               // Other
   "",   "", 0
};

static XLAT_CODE  asParkType[] =
{
   // Value, lookup code, value length
   "C", " C", 1,               // CARPORT
   "G",  "Z", 1,               // Garage
   "N",  "H", 1,               // None
   "O",  "O", 1,               // Others
   "",   "",  0
};

static XLAT_CODE  asView[] =
{
   "0", " ", 1,               // N/A
   "1", "F", 1,               // Lake Access
   "2", "M", 1,               // Golf course
   "3", "L", 1,               // Greenbelt/Park
   "4", "6", 1,               // Runway access
   "5", "E", 1,               // River vista
   "6", "7", 1,               // Busy Street
   "7", "X", 1,               // Interior
   "",  "",  0
};

static XLAT_CODE  asSewer[] =
{
   // Value, lookup code, value length 
   "E", "E", 1,               // Engineered system
   "O", "Z", 1,               // Other than sewer or septic
   "S", "Y", 1,               // Sewer
   "T", "S", 1,               // Septic tank
   "",   "",  0
};

static XLAT_CODE  asWaterSrc[] = 
{
   // Value, lookup code, value length
   "1", "P", 2,               // Community
   "2", "W", 2,               // Well
   "4", "Y", 2,               // Other
   "",   "",  0
};

static XLAT_CODE  asHeating[] =
{  // Verified 08/21/2015
   // Value, lookup code, value length
   "01", "F", 2,               // Baseboard
   "02", "Z", 2,               // Central Forced Air, Electric
   "03", "Z", 2,               // Central Forced Air, Gas
   "04", "C", 2,               // Floor
   "05", "G", 2,               // Heat Pump
   "06", "V", 2,               // Monitor (Tankless Heater)
   "07", "D", 2,               // Wall Unit
   "08", "E", 2,               // H20/Oil
   "09", "K", 2,               // Solar Assist
   "10", "S", 2,               // Wood Only
   "FC", "Z", 2,               // Central Forced Air
   "",   "",  0
};

static XLAT_CODE  asCooling[] =
{
   // Value, lookup code, value length
   "01", "C", 2,               // Central
   "02", "E", 2,               // Evaporative
   "03", "X", 2,               // Others
   "04", "L", 2,               // Wall Unit
   "05", "H", 2,               // Heat Pump
   "",   "",  0
};

// HUM has no pool data - 20131106
static XLAT_CODE  asPool[] =
{  // Updated 8/21/2015
   // Value, lookup code, value length
   "01", "X", 2,               // Above Ground
   "02", "A", 2,               // Concrete In-Ground
   "03", "V", 2,               // Vinyl In-Ground
   "04", "C", 2,               // Spa/Hot Tub
   "",   "",  0
};

static char asVestCode3[28][4]=
{
   " HW"," JT"," TC"," SM"," SE"," SW"," MA"," MM"," MS"," MW",
   " UM"," UN"," UW"," WD"," WR"," WS"," WH"," CP"," CR"," PT",
   " PL"," SB"," JV"," MI"," SI"," SO"," ID",""
};

static char asVestCode4[32][6]=
{
   "HWJT","HWCP","HWTC","MCTC","MCJT","MMSE","MMTC","MSJT","MWSE","MWJT",
   "SMJT","SMSE","SMTC","SWJT","SWSE","SWTC","TRJT","TRTC","UMJT","UMSE",
   "UMTC","UWJT","UWTC","WDJT","WDLE","WSJT","WSTC","WHJT","WHCP","WHTC",
   "SISE",""
};

#define  WH2HW_MARKER   27
static char asVestCode5[32][8]=
{
   " HW JT"," HW CP"," HW TC"," MC TC"," MC JT"," MM SE"," MM TC"," MS JT"," MW SE"," MW JT",
   " SM JT"," SM SE"," SM TC"," SW JT"," SW SE"," SW TC"," TR JT"," TR TC"," UM JT"," UM SE",
   " UM TC"," UW JT"," UW TC"," WD JT"," WD LE"," WS JT"," WS TC"," WH JT"," WH CP"," WH TC",
   " SI SE",""
};

IDX_TBL5 HUM_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "01", "1 ", 'N', 2, 2,     // 100% transfer
   "02", "57", 'N', 2, 2,     // Partial Interest Transfer
   "03", "75", 'Y', 2, 2,     // Non-Reappraisable Event
   "05", "74", 'Y', 2, 2,     // New Mobile Home
   "06", "74", 'Y', 2, 2,     // Mh Vol Conv To Lpt
   "07", "75", 'Y', 2, 2,     // Transfer Of P.I.
   "08", "74", 'Y', 2, 2,     // Clerical Functions
   "09", "74", 'Y', 2, 2,     // Split/Combo
   "13", "74", 'Y', 2, 2,     // New Single Family Dwelling
   "14", "74", 'Y', 2, 2,     // New Garage/Carport
   "15", "74", 'Y', 2, 2,     // New Swimming Pool/Spa
   "16", "74", 'Y', 2, 2,     // Dwelling Add'n/Conversion
   "17", "74", 'Y', 2, 2,     // Gar Add'n/Conv/Patio/Deck
   "18", "74", 'Y', 2, 2,     // Misc Add'n
   "23", "74", 'Y', 2, 2,     // New Multiple Bldg
   "24", "74", 'Y', 2, 2,     // Add'n/Conv To Multiples
   "25", "74", 'Y', 2, 2,     // Misc Multiples
   "33", "74", 'Y', 2, 2,     // New Rural Bldg
   "34", "74", 'Y', 2, 2,     // Rural Bldg Add'n
   "35", "74", 'Y', 2, 2,     // Misc Rural Add'n, N.L.I.
   "36", "74", 'Y', 2, 2,     // Vineyard Add'n/Deletion
   "37", "74", 'Y', 2, 2,     // Land Improvements
   "44", "74", 'Y', 2, 2,     // New Industrial Bldg
   "45", "74", 'Y', 2, 2,     // Add'n/Conv Industrial
   "46", "74", 'Y', 2, 2,     // Misc Industrial
   "53", "74", 'Y', 2, 2,     // New Comm'l Bldg
   "54", "74", 'Y', 2, 2,     // Add'n/Conv Comm'l
   "55", "74", 'Y', 2, 2,     // Misc Comm'l
   "63", "74", 'Y', 2, 2,     // Ag Pres-New Bldg
   "64", "74", 'Y', 2, 2,     // Ag Pres-Add'n/Conv
   "65", "74", 'Y', 2, 2,     // Ag Pres-Misc
   "66", "74", 'Y', 2, 2,     // Ag Pres Vines Inc/Dec
   "70", "74", 'Y', 2, 2,     // Lien Date Update
   "71", "74", 'Y', 2, 2,     // Prop 8
   "72", "74", 'Y', 2, 2,     // Calamity
   "74", "74", 'Y', 2, 2,     // Board Order Value
   "75", "74", 'Y', 2, 2,     // Solar Improvement
   "79", "74", 'Y', 2, 2,     // Tpz Non-Renewal Update
   "80", "74", 'Y', 2, 2,     // Fed-State-Co  Property
   "81", "74", 'Y', 2, 2,     // Gov't Land Outside
   "86", "74", 'Y', 2, 2,     // No Change In Value
   "90", "74", 'Y', 2, 2,     // Prop 110 Exclusion
   "91", "74", 'Y', 2, 2,     // Prop 193 Exclusion
   "92", "74", 'Y', 2, 2,     // Prop 58 Exclusion
   "93", "74", 'Y', 2, 2,     // Prop 60 Exclusion
   "94", "74", 'Y', 2, 2,     // Fixtures (Suppl Only)
   "95", "74", 'Y', 2, 2,     // Bus Prop State Review
   "96", "74", 'Y', 2, 2,     // Demo/Removal
   "","",0,0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 HUM_Exemption[] = 
{
   "E01", "H", 3,1,
   "E11", "V", 3,1,
   "E12", "D", 3,1,     // BASIC DISABLED VETERANS EXEMPTION
   "E14", "D", 3,1,     // LOW INCOME DISABLED VETERANS EXEMPTION
   "E21", "W", 3,1,
   "E22", "I", 3,1,
   "E31", "C", 3,1,     // 
   "E32", "R", 3,1,     // 
   "E40", "U", 3,1,     // PARTIAL COLLEGE EXEMPTION
   "E41", "U", 3,1,     // COLLEGE EXEMPTION
   "E42", "U", 3,1,     // PARTIAL COLLEGE EXEMPTION
   "E43", "U", 3,1,     // PARTIAL COLLEGE EXEMPTION
   "E51", "P", 3,1,
   "E52", "X", 3,1,     // PUBLIC AGENCY
   "E53", "M", 3,1,
   "E54", "S", 3,1,
   "E57", "L", 3,1,
   "E58", "X", 3,1,     // TRIBAL HOUSING EXEMPTION
   "E59", "X", 3,1,     // LESSOR'S EXEMPTION
   "E60", "X", 3,1,     // LOW VALUE EXEMPTION
   "E61", "E", 3,1,     // CEMETERY EXEMPTION
   "E62", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E63", "E", 3,1,     // PARTIAL CEMETERY EXEMPTION
   "E66", "X", 3,1,     // LOW VALUE UNSECURED
   "E67", "X", 3,1,     // LATE FILED HISTORICAL AIRCRAFT
   "E70", "W", 3,1,
   "E93", "X", 3,1,     // 4% VESSEL EXEMPTION
   "E94", "X", 3,1,     // LATE FILED 4% VESSEL EXEMPTION
   "","",0,0
};

#endif