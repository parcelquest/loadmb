#include "stdafx.h"
#include "Logs.h"
#include "Tables.h"
#include "CountyInfo.h"

#include "R01.h"
#include "Utils.h"
#include "RecDef.h"
#include "SaleRec.h"
#include "OutRecs.h"
#include "MBExtrn.h"

/******************************************************************************
 *
 * Return 0 if success.  Otherwise error;
 *
 ******************************************************************************/

int createSaleOutrec(SALES_REC *pSales, LPSTR acSql)
{
   char     acTmp[MAX_RECSIZE];

   quoteRem(pSales->Seller1);
   quoteRem(pSales->Buyer1);
   quoteRem(pSales->Seller2);
   quoteRem(pSales->Buyer2);
   quoteRem(pSales->M_Addr1);
   quoteRem(pSales->M_Addr2);
   sprintf(acTmp, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s",
               pSales->Apn,
               pSales->DocNum,
               pSales->DocDate,
               pSales->DocCode,
               pSales->SaleCode,
               pSales->Seller1,
               pSales->Buyer1,
               pSales->Seller2,
               pSales->Buyer2,
               pSales->Price,
               pSales->TaxAmt,
               pSales->GrpSale,
               pSales->GrpAsmt,
               pSales->XferType,
               pSales->NumXfer,
               pSales->M_Addr1,
               pSales->M_Addr2,
               pSales->M_Zip,
               pSales->ARCode);

   blankRem(acTmp);
   strcpy(acSql, acTmp);
   strcat(acSql, "\n");

   return 0;
}

/******************************************************************************
 *
 * Type contains bitmap value.  Following value can OR together for multiple types
 *
 * iType 1: generate Sale import
 *       2: generate GrGr import
 *       4: move other APN to group asmt (for KER)
 *
 * Return: Number of output records
 *
 ******************************************************************************/

int createSaleImport(LPCSTR CountyCode, LPCSTR pInfile, LPCSTR pOutfile, int iType)
{
   char     *pTmp, acInfile[_MAX_PATH], acOutfile[_MAX_PATH], acRec[MAX_RECSIZE], acTmp[MAX_RECSIZE];
   BOOL     bEof;
   int      iRet;
   long     lCnt=0;
   FILE     *fdSqlSales;

   // Input record
   SCSAL_REC *pSale;
   // Output record
   SALES_REC  mySales;

   if (pInfile && *pInfile > ' ')
   {
      strcpy(acInfile, pInfile);
   } else if (iType & 1)
   {
      LogMsg("Import sale file ...");
      GetPrivateProfileString("Data", "SaleOut", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acInfile, acTmp, CountyCode, CountyCode, "sls");
   } else if (iType & 2)
   {
      LogMsg("Import GrGr file ...");
      GetPrivateProfileString("Data", "GrGrOut", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acInfile, acTmp, CountyCode, CountyCode, "sls");
   } else
   {
      LogMsg("??? Incorrect file type: %d", iType);
      return -999;
   }

   //if (iType & SALE_IMPORT)
   //   strcpy(acOutfile, acSqlSalesFile);
   //else
   //   strcpy(acOutfile, acSqlGrgrFile);
   strcpy(acOutfile, pOutfile);

   // Open Sales file
   LogMsg("Open Sales/GrGr file %s", acInfile);
   fdSales = fopen(acInfile, "r");
   if (fdSales == NULL)
   {
      LogMsg("***** Error opening Sales/GrGr file: %s\n", acInfile);
      return -1;
   }

   if (!(fdSqlSales = fopen(acOutfile, "w")))
   {
      LogMsg("***** Error creating output file %s", acOutfile);
      return -2;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdSales);
   bEof = (pTmp ? false:true);

   // Merge loop
   while (pTmp && !feof(fdSales))
   {
      pSale = (SCSAL_REC *)&acRec[0];

      memset(&mySales, 0, sizeof(SALES_REC));
      strncpy(mySales.Apn, pSale->Apn, SALE_SIZ_APN);

#ifdef _DEBUG
//   if (!memcmp(mySales.Apn, "026252023", 9))
//    iRet = 0;
#endif

      strncpy(mySales.DocNum, pSale->DocNum, SALE_SIZ_DOCNUM);
      strncpy(mySales.DocDate, pSale->DocDate, SALE_SIZ_DOCDATE);
      strncpy(mySales.DocCode, pSale->DocType, SALE_SIZ_DOCTYPE);
      strncpy(mySales.SaleCode, pSale->SaleCode, SALE_SIZ_SALECODE);
      strncpy(mySales.Seller1, pSale->Seller1, SALE_SIZ_SELLER);
      strncpy(mySales.Seller2, pSale->Seller2, SALE_SIZ_SELLER);
      strncpy(mySales.Buyer1, pSale->Name1, SALE_SIZ_BUYER);
      strncpy(mySales.Buyer2, pSale->Name2, SALE_SIZ_BUYER);
      strncpy(mySales.M_Addr1, pSale->MailAdr1, SALE_SIZ_M_ADR1);
      strncpy(mySales.M_Addr2, pSale->MailAdr2, SALE_SIZ_M_ADR2);

      if (iType & 4)
      {
         strncpy(mySales.GrpAsmt, pSale->OtherApn, SALE_SIZ_APN);
         mySales.GrpSale[0] = 'O';
      }

      long lTmp = atoin(pSale->NumOfPrclXfer, SALE_SIZ_NOPRCLXFR);
      if (lTmp > 0)
         sprintf(mySales.NumXfer, "%d", lTmp);

      long lPrice = atoin(pSale->SalePrice, SALE_SIZ_SALEPRICE);
      if (lPrice > 0)
         sprintf(mySales.Price, "%d", lPrice);

      strncpy(mySales.SaleCode, pSale->SaleCode, SALE_SIZ_SALECODE);
      if (iType & 2)
         mySales.ARCode[0] = 'R';
      else
         mySales.ARCode[0] = 'A';

      acTmp[0] = 0;
      iRet = createSaleOutrec(&mySales, acTmp);
      fputs(acTmp, fdSqlSales);

      if (!(++lCnt % 1000))
         printf("\rSales records: %u", lCnt);

      // Get next record
      pTmp = fgets(acRec, MAX_RECSIZE, fdSales);
   }

   printf("\rSales records: %u\n", lCnt);
   LogMsg("Total Sales records : %u", lCnt);

   if (fdSales)
      fclose(fdSales);
   if (fdSqlSales)
      fclose(fdSqlSales);

   return lCnt;
}

