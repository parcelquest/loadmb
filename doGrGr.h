#ifndef  _DOGRGR_H_
#define  _DOGRGR_H_

int Sale_MatchRoll(LPCSTR pSaleFile, int iKeyLen, int iKeyPos=0, bool bUseOtherApn=false);
int GrGr_MatchRoll(LPCSTR pGrGrFile, int iMatchApnLen=0);
int GrGr_ExtrSaleMatched(LPCSTR pGrGrFile, LPCSTR pESaleFile=NULL, bool bOwner=false);
int GrGr_ExtrSaleMatched(LPCSTR pGrGrFile, LPCSTR pESaleFile, IDX_TBL5 *pDocTbl=NULL);
int MergeGrGrFile(char *pCnty, bool bUpdtSeller=true, bool bUpdtOwner=false, bool bUpdtPrice=false);
// Merge GRGR_EXP record to R01
int MergeGrGrExp(char *pExpRec, char *pOutbuf, bool bUpdtSeller=true, bool bUpdtOwner=false, bool bUpdtPrice=false);
int MergeGrGrExpFile(char *pGrGrFile, bool bUpdtSeller=true, bool bUpdtOwner=false, bool bUpdtPrice=true);
int GrGr_XlatApn(char *pCnty, char *pApnFile, char *pGrGrFile, int iOldApnOffset, int iOldApnLen, int iNewApnOffset);
// Merge GRGR_DOC record to R01
int MergeGrGrDoc(char *pCnty, int iVersion=0);
// Merge GRGR_DEF records to R01
int MergeGrGrDef(char *pCnty, bool bExactTitle, int iVersion=0);

// Convert GRGR_DEF to SCSALE format
int GrGr_To_Sale(LPCSTR pGrGrFile, LPCSTR pSaleFile, IDX_TBL5 *pDocTbl);

// Verify function
int GrGr_ChkApnLen(LPCSTR pGrGrFile, LPCSTR pOutFile, int iLen, int iOffset=0 /* zero base */);
int moveGrGrFiles(LPCSTR pIniFile, LPCSTR pCnty, bool bAddDate);
int FixCumGrGr(char *pInfile, int iType, bool bRemove=false);
int FixGrGrDoc(char *pInfile, int iType, bool bRemove=false);
int FixGrGrDef(char *pInfile, int iType, bool bRemove=false);

#endif