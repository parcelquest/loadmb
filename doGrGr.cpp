/*****************************************************************************
 *
 * 06/01/2017 Modify MergeGrGrDoc() by using char buffer instead of fixed length struct
 *            to avoid buffer overflow.
 * 03/31/2018 Fix DocNum bug in MergeGrGrDefRec().  Add FixCumGrGr() to fix DocNum in GrGr.Sls file
 * 04/10/2018 Modify MergeGrGrDocRec2() to ignore records where DocNum[4] != 'R'.
 * 07/11/2018 Modify MergeGrGrDef() to handle case when first char of DocTitle is hyphen
 * 12/03/2018 Add FixGrGrDoc() to fix GrGr file for RIV.
 * 12/07/2018 Add FixGrGrDef() to fix GrGr file for MON.
 * 07/30/2019 Modify MergeGrGrDoc() & MergeGrGrDefRec() to update GRGR record newer than lien date only
 * 08/08/2019 Modify MergeGrGrDocRec1() to ignore record length less than GRGR_DOC.
 * 07/07/2020 Add last GrGr recording date to MergeGrGrDoc().
 * 07/08/2020 Set lRecCnt=lCnt in MergeGrGrExpFile() to force build index when running with -G alone.
 * 08/05/2020 Modify MergeGrGrDocRec() & MergeGrGrDefRec() to update seller as needed.
 * 11/14/2020 Modify FixGrGrDoc() to clean up bad records in Riv_GrGr.Sls
 * 11/17/2021 Modify MergeGrGrDef() to support iVersion=2 and set SALE_CODE='P' on multiapn.
 * 04/03/2022 Modify GrGr_ExtrSaleMatched() to display last recording date in log file.
 * 07/11/2024 Modify MergeGrGrDoc() to remove check for LienDate.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "Utils.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "SaleRec.h"
#include "Logs.h"
#include "Utils.h"
#include "doSort.h"
#include "doZip.h"

extern   COUNTY_INFO myCounty;
extern   char  acRawTmpl[], acTmpPath[], acGrGrTmpl[], acEGrGrTmpl[];
extern   int   iRecLen, iApnLen, iNoMatch, iGrGrApnLen;
extern   long  lRecCnt, lLastRecDate, lToday, lLienDate;
extern   bool  bDebug;

extern   int      iNumDeeds;
extern   XREFTBL  asDeed[];
extern   FILE  *fdGrGr, *fdSale;

/******************************* Sale_MatchRoll *****************************
 *
 * Match cum sale file against R01 and replace sale APN with roll APN.
 * Used by CCX, KER
 *
 * Params:
 *    - pSaleFile: in file name (required)
 *    - iKeyLen: number of bytes compared (optional)
 *    - iKeyPos: starting position of APN field in R01 (optional, default 0)
 *      This pos can be of APN, AltApn or PrevApn.
 *
 * Return #recs output if successful
 *
 * 12/02/2015  Sort sale file before matching if key field is OtherApn
 *
 ****************************************************************************/

int Sale_MatchRoll(LPCSTR pSaleFile, int iKeyLen, int iKeyPos, bool bUseOtherApn)
{
   char     acBuf[2048], acRoll[2048], acTmpFile[_MAX_PATH], acRawFile[_MAX_PATH], acSaleFile[_MAX_PATH], acTmp[256];
   char     *pTmp, *pKey;
   FILE     *fdIn, *fdOut;
   int      iRet, iTmp, iNoApn, iApnMatch, iApnUnmatch, lCnt, iMatchApnLen;
   HANDLE   hRoll;
   DWORD    nBytesRead;
   bool     bEof;

   SCSAL_REC *pSale = (SCSAL_REC *)&acBuf[0];

   LogMsg0("Match roll file and replace APN for %s", pSaleFile);
   if (!iKeyLen)
      iMatchApnLen = iApnLen;
   else
      iMatchApnLen = iKeyLen;

   // Open input file
   if (bUseOtherApn)
   {
      // Sort sale file on OtherApn
      strcpy(acTmp, pSaleFile);
      sprintf(acSaleFile, "%s\\%s\\Cum_Sale.srt", acTmpPath, myCounty.acCntyCode);
      sprintf(acBuf, "S(%d,%d,C,A) ", SALE_FLD_OTH_APN, iMatchApnLen);
      iRet = sortFile(acTmp, acSaleFile, acBuf);
      if (iRet <= 0)
         return -4;
   } else
      strcpy(acSaleFile, pSaleFile);

   if (!(fdIn = fopen(acSaleFile, "r")))
   {
      LogMsg("***** Error opening %s", acSaleFile);
      return -1;
   }

   // Create output temp file
   strcpy(acTmpFile, pSaleFile);
   pTmp = strrchr(acTmpFile, '.');
   strcpy(pTmp, ".tmp");
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating %s", acTmpFile);
      fclose(fdIn);
      return -2;
   }

   // Open roll file
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (_access(acRawFile, 0))
      {
         LogMsg("***** Missing input file %s.  Please check!", acRawFile);
         fclose(fdIn);
         fclose(fdOut);
         return -3;
      }
   }

   // If compare field is not standard APN, resort roll file
   if (iKeyPos > 1)
   {
      strcpy(acTmp, acRawFile);
      sprintf(acRawFile, "%s\\%s\\AltApn.srt", acTmpPath, myCounty.acCntyCode);
      sprintf(acBuf, "S(%d,%d,C,A) F(FIX,%d)", iKeyPos+1, iMatchApnLen, iRecLen);
      iRet = sortFile(acTmp, acRawFile, acBuf);
      if (iRet <= 0)
         return -4;
   }

   LogMsg("Open input file %s", acRawFile);
   hRoll = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (hRoll == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening %s in Sale_MatchRoll().  errno=%d", acRawFile, _errno);
      fclose(fdIn);
      fclose(fdOut);
      return -3;
   }

   // Skip first record as needed
   iRet = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);
   if (!memcmp(acRoll, "999999", 6))
      iRet = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);

   // Initialize counters
   lCnt=iNoApn=iApnMatch=iApnUnmatch=0;
   bEof = false;

   if (bUseOtherApn)
      pKey = pSale->OtherApn;
   else
      pKey = pSale->Apn;

   // Loop through input file
   while (!bEof)
   {
      if (!(lCnt++ % 1000))
         printf("\r%u", lCnt);

      // Read input record
      if (!(pTmp = fgets(acBuf, 2048, fdIn)))
         break;

      // Skip if it's already matched
      if (pSale->ApnMatched == 'Y')
      {
         fputs(acBuf, fdOut);
         iApnMatch++;
         continue;
      }

      pSale->ApnMatched = ' ';

      // If no APN, skip
      if (pKey[iMatchApnLen-1] == ' ')
      {
         iNoApn++;
         continue;
      }
#ifdef _DEBUG
      //if (!memcmp(pKey, "     34225", iMatchApnLen))
      //   iRet = 0;
#endif
      // Match APN
      do
      {
         // Compare
         iRet = memcmp(pKey, &acRoll[iKeyPos], iMatchApnLen);
         if (!iRet)
         {
            memcpy(pSale->Apn, acRoll, iApnLen);
            pSale->ApnMatched = 'Y';
            iApnMatch++;
            break;
         } else
         { 
            // Get next roll record
            if (iRet == 1)
            {
               // Read R01 record
               iTmp = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);
               // Check for EOF
               if (!iTmp)
               {
                  LogMsg("***** Error reading roll file (%f)", GetLastError());
                  bEof = true;
                  break;
               }

               // EOF ?
               if (!nBytesRead)
               {
                  bEof = true;
                  break;
               }
            } else
               iApnUnmatch++;
         } 
      } while (iRet > 0);
      
      // Output record
      fputs(acBuf, fdOut);
   }

   // Check for leftover
   while (!feof(fdIn))
   {
      // Read input record
      if (!(pTmp = fgets(acBuf, 2048, fdIn)))
         break;

      // If no APN, skip
      if (pSale->Apn[0] == ' ')
         iNoApn++;

      // Output record
      fputs(acBuf, fdOut);

      if (!(lCnt++ % 1000))
         printf("\r%u", lCnt);
   }

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);
   if (hRoll)
      CloseHandle(hRoll);

   // If everything OK, rename output file to original file
   if (iApnMatch > 0)
   {
      if (bUseOtherApn)
      {
         // Resort on APN
         iRet = sortFile(acTmpFile, (char *)pSaleFile, "S(1,14,C,A,27,8,C,A,57,10,C,D) DUPO(1,34)");
      } else
      {
         if (remove(pSaleFile))
            LogMsg("***** Error removing sale file: %s (%d)", pSaleFile, errno);
         else if (rename(acTmpFile, pSaleFile))
            LogMsg("***** Error renaming temp file: %s --> %s (%d)", acTmpFile, pSaleFile, errno);
      }
   }

   LogMsg("Number of roll records read: %d", lCnt);
   LogMsg("              APN unmatched: %d", iApnUnmatch);
   LogMsg("                APN matched: %d", iApnMatch);
   LogMsg("                     No APN: %d\n", iNoApn);
   printf("\n");

   return iApnMatch;
}

/******************************* GrGr_MatchRoll *****************************
 *
 * Match GrGr file against R01 and populate APNMatch and Owner match field.
 * Used by MNO, VEN, SCL
 *
 * Return #recs output if successful
 *
 ****************************************************************************/

int GrGr_MatchRoll(LPCSTR pGrGrFile, int iMatchApnLen)
{
   char     acBuf[2048], acRoll[2048], acTmpFile[_MAX_PATH], acTmp[256];
   char     *pTmp;
   FILE     *fdIn, *fdOut;
   GRGR_DEF *pGrGr = (GRGR_DEF *)&acBuf[0];
   int      iRet, iTmp, iNoApn, iApnMatch, iApnUnmatch, iOwnerMatch, iNameCnt, lCnt;
   HANDLE   hRoll;
   DWORD    nBytesRead;
   bool     bEof;

   LogMsg("Match roll file for %s", pGrGrFile);
   if (!iMatchApnLen)
      iMatchApnLen = iApnLen;

   // Open input file
   if (!(fdIn = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error opening %s", pGrGrFile);
      return -1;
   }

   // Create output temp file
   strcpy(acTmpFile, pGrGrFile);
   pTmp = strrchr(acTmpFile, '.');
   strcpy(pTmp, ".tmp");
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating %s", acTmpFile);
      fclose(fdIn);
      return -2;
   }

   // Open roll file
   sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   if (_access(acTmp, 0))
   {
      sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (_access(acTmp, 0))
      {
         LogMsg("***** Missing input file %s.  Please check!", acTmp);
         fclose(fdIn);
         fclose(fdOut);
         return -3;
      }
   }

   LogMsg("Open input file %s", acTmp);
   hRoll = CreateFile(acTmp, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (hRoll == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening %s in GrGr_MatchRoll().", acTmp);
      fclose(fdIn);
      fclose(fdOut);
      return -3;
   }

   // Skip first record
   iRet = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);
   iRet = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);

   // Initialize counters
   lCnt=iNoApn=iApnMatch=iApnUnmatch=iOwnerMatch = 0;
   bEof = false;

   // Loop through input file
   while (!bEof)
   {
      if (!(lCnt++ % 1000))
         printf("\r%u", lCnt);

      // Read input record
      if (!(pTmp = fgets(acBuf, 2048, fdIn)))
         break;

      pGrGr->APN_Matched = 'N';
      pGrGr->Owner_Matched = 'N';

      // If no APN, skip
      if (pGrGr->APN[0] == ' ')
      {
         iNoApn++;
         fputs(acBuf, fdOut);    // No APN, output anyway for data entry
         continue;
      }
#ifdef _DEBUG
      //if (!memcmp(pGrGr->APN, "0104015000000", 13))
      //   iRet = 0;
#endif
      // Match APN
      do
      {
         // Compare
         iRet = memcmp(pGrGr->APN, acRoll, iMatchApnLen);
         if (!iRet)
         {
            pGrGr->APN_Matched = 'Y';
            iApnMatch++;

            // Match owner - match last name or the first 10 bytes of grantors
            int   iIdx, iLen;
            char  acOwner[64];

            memcpy(acOwner, (char *)&acRoll[OFF_NAME1], SIZ_NAME1);
            acOwner[SIZ_NAME1] = 0;
            if (pTmp = strchr(acOwner, ' '))
            {
               iLen = pTmp - (char *)&acOwner[0];
               if (iLen < 3) iLen = 10;
            }

            iNameCnt = atoin(pGrGr->NameCnt, SIZ_GR_NAMECNT);
            for (iIdx = 0; iIdx < iNameCnt; iIdx++)
            {
               // Compare grantors only
               if (!memcmp((char *)&pGrGr->Grantors[iIdx].Name[0], acOwner, iLen))
               {
                  pGrGr->Owner_Matched = 'Y';
                  iOwnerMatch++;
                  break;
               }
            }

            // Populate with roll data - to be done when needed

            break;
         } else
         { 
            if (iRet == 1)
            {
               // Read R01 record
               iTmp = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);
               // Check for EOF
               if (!iTmp)
               {
                  LogMsg("***** Error reading roll file (%f)", GetLastError());
                  bEof = true;
                  break;
               }

               // EOF ?
               if (!nBytesRead)
               {
                  bEof = true;
                  break;
               }
            }
         }
      } while (iRet > 0);
      
      // Output record
      fputs(acBuf, fdOut);
   }

   // Check for leftover
   while (!feof(fdIn))
   {
      // Read input record
      if (!(pTmp = fgets(acBuf, 2048, fdIn)))
         break;

      // If no APN, skip
      if (pGrGr->APN[0] == ' ')
         iNoApn++;

      pGrGr->APN_Matched = 'N';
      pGrGr->Owner_Matched = 'N';

      // Output record
      fputs(acBuf, fdOut);

      if (!(lCnt++ % 1000))
         printf("\r%u", lCnt);
   }

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);
   if (hRoll)
      CloseHandle(hRoll);

   // If everything OK, rename output file to original file
   if (remove(pGrGrFile))
      LogMsg("***** Error removing temp file: %s (%d)", pGrGrFile, errno);
   else if (rename(acTmpFile, pGrGrFile))
      LogMsg("***** Error renaming temp file: %s --> %s (%d)", acTmpFile, pGrGrFile, errno);

   LogMsg("Number of roll records read: %d", lCnt);
   LogMsg("                APN matched: %d", iApnMatch);
   LogMsg("              Owner matched: %d", iOwnerMatch);
   LogMsg("                     No APN: %d\n", iNoApn);
   printf("\n");

   return lCnt;
}

/***************************** GrGr_ExtrSaleMatched **************************
 *
 * Extract APN matched records from GrGr file
 * Return number of extracted records if successful.
 * Used by VEN
 *
 *****************************************************************************/

int GrGr_ExtrSaleMatched(LPCSTR pGrGrFile, LPCSTR pESaleFile, bool bOwner)
{
   char      acOutFile[256], acBuf[2048], acSaleRec[512];
   long      lCnt=0, lTax;
   char      *pTmp;

   CString   sTmp, sApn, sType;
   SALE_REC1 *pSaleRec = (SALE_REC1 *)&acSaleRec;
   GRGR_DEF  *pGrGr = (GRGR_DEF *)&acBuf[0];

   // Use default file name if not supplied
   if (pESaleFile)
      strcpy(acOutFile, pESaleFile);
   else
      sprintf(acOutFile, acEGrGrTmpl, myCounty.acCntyCode, "DAT");

   LogMsg("\nExtract matched grgr from %s to %s", pGrGrFile, acOutFile);

   // Open output file
   if (!(fdSale = fopen(acOutFile, "w")))
   {
      LogMsg("***** Error creating %s file", acOutFile);
      return -1;
   }

   // Open input file
   if (!(fdGrGr = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error creating %s file", pGrGrFile);
      return -1;
   }

   // Loop through
   while (!feof(fdGrGr))
   {
      // Get input rec
      pTmp = fgets(acBuf, 2048, fdGrGr);
      if (!pTmp)
         break;         // EOF

      // Drop all record without APN matched
      if (pGrGr->APN_Matched == 'Y')
      {
         memset(acSaleRec, 32, sizeof(SALE_REC1));

         memcpy(pSaleRec->acApn, pGrGr->APN, SALE_SIZ_APN);
         memcpy(pSaleRec->acDocDate, pGrGr->DocDate, SALE_SIZ_DOCDATE);
         memcpy(pSaleRec->acDocNum, pGrGr->DocNum, SALE_SIZ_DOCNUM);
         memcpy(pSaleRec->acDocType, pGrGr->DocTitle, SALE_SIZ_DOCTYPE);

         lTax = atoin(pGrGr->Tax, SIZ_GR_TAX);
         if (lTax > 0)
         {
            memcpy(pSaleRec->acStampAmt, pGrGr->Tax, SIZ_GR_TAX);
            memcpy(pSaleRec->acSalePrice, pGrGr->SalePrice, SIZ_GR_SALE);
         }

         // Get grantors
         memcpy(pSaleRec->acSeller, pGrGr->Grantors[0].Name, SALE_SIZ_SELLER);

         // Get grantees
         if (bOwner)
         {
            memcpy(pSaleRec->acName1, pGrGr->Grantees[0].Name, SALE_SIZ_NAME);
            memcpy(pSaleRec->acName2, pGrGr->Grantees[1].Name, SALE_SIZ_NAME);
         }

         pSaleRec->CRLF[0] = '\n';
         pSaleRec->CRLF[1] = 0;
         fputs(acSaleRec, fdSale);
         lCnt++;
      }
      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdGrGr);
   fclose(fdSale);

   LogMsg("Number of extracted records: %ld\n", lCnt);
   return lCnt;
}

/***************************** GrGr_ExtrSaleMatched **************************
 *
 * This is similar to above version but output to SCSAL_REC format and 
 * translate DocTitle to DocType if translation table provided.  
 * Used by MNO
 *
 *****************************************************************************/

int GrGr_ExtrSaleMatched(LPCSTR pGrGrFile, LPCSTR pESaleFile, IDX_TBL5 *pDocTbl)
{
   char      acOutFile[256], acBuf[2048], acSaleRec[512];
   int       lCnt=0, iTmp;
   char      *pTmp;

   CString   sTmp, sApn, sType;
   SCSAL_REC *pSaleRec = (SCSAL_REC *)&acSaleRec;
   GRGR_DEF  *pGrGr = (GRGR_DEF *)&acBuf[0];

   // Use default file name if not supplied
   if (pESaleFile)
      strcpy(acOutFile, pESaleFile);
   else
      sprintf(acOutFile, acEGrGrTmpl, myCounty.acCntyCode, "DAT");

   LogMsg("\nExtract matched grgr from %s to %s", pGrGrFile, acOutFile);

   // Open output file
   if (!(fdSale = fopen(acOutFile, "w")))
   {
      LogMsg("***** Error creating %s file", acOutFile);
      return -1;
   }

   // Open input file
   if (!(fdGrGr = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error creating %s file", pGrGrFile);
      return -1;
   }

   lLastRecDate = 0;

   // Loop through
   while (!feof(fdGrGr))
   {
      // Get input rec
      pTmp = fgets(acBuf, 2048, fdGrGr);
      if (!pTmp)
         break;         // EOF

      // Drop all record without APN matched
      if (pGrGr->APN_Matched == 'Y')
      {
         memset(acSaleRec, 32, sizeof(SCSAL_REC));

         pSaleRec->ApnMatched = 'Y';
         memcpy(pSaleRec->Apn, pGrGr->APN, SALE_SIZ_APN);
         memcpy(pSaleRec->DocDate, pGrGr->DocDate, SALE_SIZ_DOCDATE);
         memcpy(pSaleRec->DocNum, pGrGr->DocNum, SALE_SIZ_DOCNUM);
         iTmp = atoin(pGrGr->DocDate, SALE_SIZ_DOCDATE);
         if (iTmp > lLastRecDate)
            lLastRecDate = iTmp;

         // Replace hyphen with 'R' for BUT
         if (pSaleRec->DocNum[4] == '-')
            pSaleRec->DocNum[4] = 'R';

         // DocType
         if (pDocTbl)
         {
            iTmp = findDocType(pGrGr->DocTitle, pDocTbl);
            if (iTmp >= 0)
            {
               memcpy(pSaleRec->DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
               pSaleRec->NoneSale_Flg = pDocTbl[iTmp].flag;
            }
         } else
            memcpy(pSaleRec->DocType, pGrGr->DocTitle, SALE_SIZ_DOCTYPE);

         // Sale price
         memcpy(pSaleRec->StampAmt, pGrGr->Tax, SIZ_GR_TAX);
         memcpy(pSaleRec->SalePrice, pGrGr->SalePrice, SIZ_GR_SALE);

         // Get grantors
         memcpy(pSaleRec->Seller1, pGrGr->Grantors[0].Name, SALE_SIZ_SELLER);

         // Get grantees
         memcpy(pSaleRec->Name1, pGrGr->Grantees[0].Name, SALE_SIZ_NAME);
         memcpy(pSaleRec->Name2, pGrGr->Grantees[1].Name, SALE_SIZ_NAME);

         pSaleRec->Etal = pGrGr->MoreName;
         pSaleRec->ARCode = 'R';

         pSaleRec->CRLF[0] = '\n';
         pSaleRec->CRLF[1] = 0;
         fputs(acSaleRec, fdSale);
         lCnt++;
      }
      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdGrGr);
   fclose(fdSale);

   LogMsg("Number of extracted records: %ld\n", lCnt);
   LogMsg("        Last recording date: %d.", lLastRecDate);

   return lCnt;
}

/********************************** MergeGrGr ********************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update owner(false)/seller(true) if requested.
 * If owner is updated, transfer will be updated as well
 * This is the modified version of MergeGrGr() which will not update sale in 
 * the same date.
 *
 *****************************************************************************/

int MergeGrGrExp(char *pExpRec, char *pOutbuf, bool bUpdtSeller, bool bUpdtOwner, bool bUpdtPrice)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lTmp;
   char  *pTmp, acTmp[32];

   SALE_REC1 *pSaleRec = (SALE_REC1 *)pExpRec;

   if (!memcmp(pSaleRec->acDocType, "NOTI", 4) || !memcmp(pSaleRec->acDocType, "CERT", 4))
      return 0;

   lCurSaleDt = atoin(pSaleRec->acDocDate, SIZ_SALE1_DT);

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt > lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->acDocNum, SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->acDocDate, SIZ_TRANSFER_DT);
   }

   // Ignore non-sale document
   if (pSaleRec->NoneSale_Flg == 'Y')
      return 0;

   // If same date and ccurrent has sale price, use it
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   // Only update if current sale is newer
   if (lCurSaleDt < lLstSaleDt)
      return 0;

   lPrice = atoin(pSaleRec->acSalePrice, SIZ_SALE1_AMT);
   if (lCurSaleDt == lLstSaleDt)
   {
      lTmp = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (bUpdtPrice && lPrice > 5000 && lTmp == 0)
         goto UpdateCurrentSale;

      return 0;
   }

   // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);

UpdateCurrentSale:

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->acDocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->acDocDate, SIZ_SALE1_DT);
   if (isdigit(pSaleRec->acDocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->acDocType, 3);
   else if (pTmp=findDocType(pSaleRec->acDocType, acTmp))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pTmp, strlen(pTmp));

   // Remove sale code - 
   // Notes: SALE_SIZ_SALECODE is smaller than  SIZ_SALE1_CODE.  Don't swap them.
   if (pSaleRec->acSaleCode[0] >= '0')
      memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->acSaleCode, SALE_SIZ_SALECODE);
   else
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);

   if (bUpdtSeller && pSaleRec->acSeller[0] > ' ')
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);

   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->acSalePrice, SIZ_SALE1_AMT);

   // Update owner
   if (bUpdtOwner)
   {
      // SALE_SIZ_NAME is 50 bytes, while SIZ_NAME1 is 52 bytes and SIZ_NAME2 is 26 bytes
      // We have to take the smaller one
      if (pSaleRec->acName1[0] > ' ')
      {
         memset(pOutbuf+OFF_NAME1, ' ', SIZ_NAME1);
         memcpy(pOutbuf+OFF_NAME1, pSaleRec->acName1, SALE_SIZ_NAME);
         memcpy(pOutbuf+OFF_NAME2, pSaleRec->acName2, SIZ_NAME2);
      }
   }

   *(pOutbuf+OFF_AR_CODE1) = 'R';
   return 1;
}

/********************************* MergeGrGrFile *****************************
 *
 * Merge GrGr_Exp data to R01 file - TUO
 *
 *****************************************************************************/

int MergeGrGrFile(char *pCnty, bool bUpdtSeller, bool bUpdtOwner, bool bUpdtPrice)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acSaleRec[512];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acGrGrFile[_MAX_PATH];

   HANDLE    fhIn, fhOut;
   SALE_REC1 *pSaleRec = (SALE_REC1 *)&acSaleRec[0];

   int      iTmp, iSaleUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   if (_access(acRawFile, 0))
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "N01");
   sprintf(acGrGrFile, acEGrGrTmpl, pCnty, "DAT");        // GrGr_Exp.dat

   if (!iGrGrApnLen)
      iGrGrApnLen = myCounty.iApnLen;

   LogMsg0("Merge GrGr file %s to %s", acGrGrFile, acRawFile);

   if (_access(acGrGrFile, 0))
   {
      LogMsg("*** %s not available.", acGrGrFile);
      return 1;
   }

   // Open Sale file
   fdSale = fopen(acGrGrFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", acGrGrFile);
      return -2;
   }

   // Get first record
   pTmp = fgets(acSaleRec, 512, fdSale);
   if (!pTmp || feof(fdSale))
   {
      LogMsg("*** No GrGr data available");
      return 0;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            CloseHandle(fhIn);
            CloseHandle(fhOut);
            fhIn = 0;
            fhOut = 0;

            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
               break;
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

            // Open Output file
            acOutFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
            LogMsg("Open output file %s", acOutFile);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
            if (fhOut == INVALID_HANDLE_VALUE)
               break;
         } else
            break;
      }

      // Update sale
GrGr_ReLoad:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "015060025000", 12) )
      //   iTmp = 0;
#endif
      iTmp = memcmp(acBuf, pSaleRec->acApn, iGrGrApnLen);
      if (!iTmp)
      {
         // Merge sale data
         iTmp = MergeGrGrExp(acSaleRec, acBuf, bUpdtSeller, bUpdtOwner, bUpdtPrice);
         if (iTmp > 0)
         {
            iSaleUpd++;
            iTmp = atoin((char *)&acBuf[OFF_SALE1_DT], SIZ_SALE1_DT);
            if (iTmp > lLastRecDate && iTmp < lToday)
               lLastRecDate = iTmp;
         } else
            iNoMatch++;

         // Read next sale record
         pTmp = fgets(acSaleRec, 512, fdSale);
         if (!pTmp)
            bEof = true;    // Signal to stop sale update
         else
            goto GrGr_ReLoad;
      } else
      {
         if (iTmp > 0)        // Sale not match, advance to next sale record
         {
            if (bDebug)
               LogMsg0("*** Sale not match : %.12s > %.12s (%d) ", acBuf, pSaleRec->acApn, lCnt);
            pTmp = fgets(acSaleRec, 512, fdSale);
            if (!pTmp)
               bEof = true;    // Signal to stop sale update
            else
            {
               iNoMatch++;
               goto GrGr_ReLoad;
            }
         }
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = -1;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }


   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename outfile
   LogMsg("Rename output file");
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   if (!_access(acRawFile, 0))
   {
      sprintf(acBuf, acRawTmpl, pCnty, pCnty, "T01");
      if (!_access(acBuf, 0))
         remove(acBuf);
      iTmp = rename(acRawFile, acBuf);
   }
   iTmp = rename(acOutFile, acRawFile);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("      records updated:      %u", iSaleUpd);
   LogMsg("      unmatched records:    %u\n", iNoMatch);
   lRecCnt = lCnt;
   return 0;
}

/******************************** MergeGrGrFile1 *****************************
 *
 * Merge GrGr - Used by BUT 
 * Merge sale only if there is sale price
 *
 *****************************************************************************/

int MergeGrGrFile1(char *pCnty)
{
   char     *pTmp, acBuf[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acGrGrFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   SALE_REC SaleRec;

   int      iTmp, iSaleUpd=0, iRet=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof, bRename;
   long     lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   if (_access(acRawFile, 0))
   {
      // R01 file not avail. - use S01 as input
      bRename = false;
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   } else
   {
      bRename = true;
      sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "N01");
   }

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "N01");
   sprintf(acGrGrFile, acGrGrTmpl, pCnty, "DAT");

   if (!iGrGrApnLen)
      iGrGrApnLen = myCounty.iApnLen;

   if (_access(acGrGrFile, 0))
      return 1;

   // Open Sale file
   LogMsg("Merge GrGr data %s to R01 file", acGrGrFile);
   fdSale = fopen(acGrGrFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", acGrGrFile);
      return -2;
   }

   // Get first record
   pTmp = fgets((char *)&SaleRec, sizeof(SALE_REC), fdSale);
   if (!pTmp || feof(fdSale))
   {
      LogMsg("*** No GrGr data available");
      return 0;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            CloseHandle(fhIn);
            CloseHandle(fhOut);
            fhIn = 0;
            fhOut = 0;

            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
               break;
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

            // Open Output file
            acOutFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
            LogMsg("Open output file %s", acOutFile);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
            if (fhOut == INVALID_HANDLE_VALUE)
               break;
         } else
            break;
      }

      // Update sale
GrGr_ReLoad:
      iTmp = memcmp(acBuf, SaleRec.acApn, iGrGrApnLen);
      if (!iTmp)
      {
         // Merge rec with sale price only
         if (SaleRec.acSalePrice[0] > '0')
         {
            // Merge sale data
            iTmp = MergeSale((SALE_REC *)&SaleRec, acBuf);
            if (iTmp > 0)
               iSaleUpd++;
         }

         // Read next sale record
         pTmp = fgets((char *)&SaleRec, SALEREC_SIZE, fdSale);
         if (!pTmp)
            bEof = true;      // Signal to stop sale update
         else
            goto GrGr_ReLoad;
      } else
      {
         if (iTmp > 0)        // Sale not match, advance to next sale record
         {
            if (bDebug)
               LogMsg0("*** Sale not match : %.12s > %.12s (%d) ", acBuf, SaleRec.acApn, lCnt);
            pTmp = fgets((char *)&SaleRec, SALEREC_SIZE, fdSale);
            if (!pTmp)
               bEof = true;    // Signal to stop sale update
            else
               goto GrGr_ReLoad;
         }
      }

      // Save last recording date
      iTmp = atoin((char *)&acBuf[OFF_SALE1_DT], SIZ_SALE1_DT);
      if (iTmp > lLastRecDate && iTmp < lToday)
         lLastRecDate = iTmp;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         iRet = errno;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }


   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename outfile
   if (bRename)
   {
      LogMsg("Rename output file");
      sprintf(acBuf, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "M01");
      if (!_access(acBuf, 0))
         remove(acBuf);
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
      rename(acRawFile, acBuf);
      rename(acOutFile, acRawFile);
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total unmatched records:    %u", iNoMatch);
   LogMsg("Total GrGr records updated: %u", iSaleUpd);

   return iRet;
}

/****************************** MergeGrGrExp1 ********************************
 *
 * Merge SCSAL_REC record to R01 
 * Input file contains only sale transactions. Drop it if no sale price.
 *
 *****************************************************************************/

int MergeGrGrExp1(char *pExpRec, char *pOutbuf, bool bUpdtSeller, bool bUpdtOwner, bool bUpdtPrice)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lTmp;
   char  *pTmp, acTmp[32];

   SCSAL_REC *pSaleRec = (SCSAL_REC *)pExpRec;

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   // Only update if current sale is newer
   if (lCurSaleDt < lLstSaleDt)
      return 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001031003000", 12))
   //   lTmp = 0;
#endif
   // If same date and ccurrent has sale price, use it
   lPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
   if (lCurSaleDt == lLstSaleDt)
   {
      lTmp = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (bUpdtPrice && lPrice > 5000 && lTmp == 0)
         goto UpdateCurrentSale;

      return 0;
   }

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt > lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }

   // Check sale price
   if (bUpdtPrice && lPrice < 5000)
      return 0;

   // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);

UpdateCurrentSale:

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);
   if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, 3);
   else if (pTmp=findDocType(pSaleRec->DocType, acTmp))
      memcpy(pOutbuf+CDA_OFF_DOC1TYPE, pTmp, strlen(pTmp));

   // Update sale code
   // Notes: SALE_SIZ_SALECODE is smaller than  SIZ_SALE1_CODE.  Don't swap them.
   if (pSaleRec->SaleCode[0] >= '0')
      memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, SALE_SIZ_SALECODE);
   else
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);

   if (bUpdtSeller && pSaleRec->Seller1[0] > ' ')
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

   // Update owner
   if (bUpdtOwner)
   {
      // SALE_SIZ_NAME is 50 bytes, while SIZ_NAME1 is 52 bytes and SIZ_NAME2 is 46 bytes
      // We have to take the smaller one
      if (pSaleRec->Name1[0] > ' ')
      {
         memset(pOutbuf+OFF_NAME1, ' ', SIZ_NAME1);
         memcpy(pOutbuf+OFF_NAME1, pSaleRec->Name1, SALE_SIZ_NAME);
         memcpy(pOutbuf+OFF_NAME2, pSaleRec->Name2, SIZ_NAME2);
      }
   }

   *(pOutbuf+OFF_AR_CODE1) = 'R';
   return 1;
}

/****************************** MergeGrGrExpFile *****************************
 *
 * Merge GrGr_Exp data in SCSAL_REC format to R01 file - MON, MNO & BUT
 * Input file contains only sale transactions.
 *
 *****************************************************************************/

int MergeGrGrExpFile(char *pGrGrFile, bool bUpdtSeller, bool bUpdtOwner, bool bUpdtPrice)
{
   char      *pTmp, acBuf[MAX_RECSIZE], acSaleRec[2048];
   char      cFileCnt=1;
   char      acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE    fhIn, fhOut;
   SCSAL_REC *pSaleRec = (SCSAL_REC *)&acSaleRec[0];

   int      iTmp, iSaleUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof, bRename=true;
   long     lRet=0, lCnt=0, lLastGrGrRecDate=0;

   LogMsg0("Merge GrGr data %s to R01 file", pGrGrFile);
   if (!iGrGrApnLen)
      iGrGrApnLen = myCounty.iApnLen;

   if (_access(pGrGrFile, 0))
   {
      LogMsg("*** Missing input file %s", pGrGrFile);
      return 1;
   }

   // Open Sale file
   LogMsg("Open GrGr file %s ", pGrGrFile);
   fdSale = fopen(pGrGrFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pGrGrFile);
      return -2;
   }

   // Get first record
   pTmp = fgets(acSaleRec, 2048, fdSale);
   if (!pTmp || feof(fdSale))
   {
      LogMsg("*** No GrGr data available");
      return 0;
   }

   // Open Input file
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   if (_access(acRawFile, 0))
   {
      // R01 file not avail. - use S01 as input
      bRename = false;
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   } else
      sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "N01");

   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            CloseHandle(fhIn);
            CloseHandle(fhOut);
            fhIn = 0;
            fhOut = 0;

            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
               break;
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

            // Open Output file
            acOutFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
            LogMsg("Open output file %s", acOutFile);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
            if (fhOut == INVALID_HANDLE_VALUE)
               break;
         } else
            break;
      }

      // Update sale
GrGr_ReLoad:
      iTmp = memcmp(acBuf, pSaleRec->Apn, iGrGrApnLen);
      if (!iTmp)
      {
         // Merge sale data
         iTmp = MergeGrGrExp1(acSaleRec, acBuf, bUpdtSeller, bUpdtOwner, bUpdtPrice);
         if (iTmp > 0)
         {
            iSaleUpd++;
            iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
            if (iTmp > lLastGrGrRecDate && iTmp < lToday)
               lLastGrGrRecDate = iTmp;
         } else
            iNoMatch++;

         // Read next sale record
         pTmp = fgets(acSaleRec, 2048, fdSale);
         if (!pTmp)
            bEof = true;      // Signal to stop sale update
         else
            goto GrGr_ReLoad;
      } else
      {
         if (iTmp > 0)        // Sale not match, advance to next sale record
         {
            if (bDebug)
               LogMsg0("*** Sale not match : %.12s > %.12s (%d) ", acBuf, pSaleRec->Apn, lCnt);
            pTmp = fgets(acSaleRec, 2048, fdSale);
            if (!pTmp)
               bEof = true;    // Signal to stop sale update
            else
               goto GrGr_ReLoad;
         }
      }

      // Save last recording date
      lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error writing to output file: %d\n", GetLastError());
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!bRet)
      {
         LogMsg("***** Error writing to output file: %d\n", GetLastError());
         break;
      }
      lCnt++;
   }


   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename outfile
   if (bRename)
   {
      LogMsg("Rename output file");
      sprintf(acBuf, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "M01");
      if (!_access(acBuf, 0))
         remove(acBuf);
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
      rename(acRawFile, acBuf);
      rename(acOutFile, acRawFile);
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total records skipped:      %u", iNoMatch);
   LogMsg("Total GrGr records updated: %u", iSaleUpd);
   LogMsg("Last GrGr recording date:   %u", lLastGrGrRecDate);
   LogMsg("Last transfer date:         %u", lLastRecDate);
   
   if (lCnt > 1000)
      lRecCnt = lCnt;
   return 0;
}

/********************************* GrGr_XlatApn *******************************
 *
 * Input/Output:  GrGr file
 *
 ******************************************************************************/

extern   COUNTY_INFO myCounty;
int GrGr_XlatApn(char *pOutFile, char *pApnFile, char *pGrGrFile, int iOldApnOffset, int iOldApnLen, int iNewApnOffset)
{
   char     acBuf[MAX_RECSIZE], acApnRec[256], acTmp[32], *pTmp;
   char     acOutFile[_MAX_PATH], acRawFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   int      iRet, iTmp, iGrGrRecLen;
   long     lCnt=0;
   GRGR_DEF *pGrGrRec;

   iGrGrRecLen = sizeof(GRGR_DEF);
   sprintf(acOutFile, acEGrGrTmpl, myCounty.acCntyCode, "TMP");        // GrGr_Exp.tmp
   LogMsg("Fix %s file for %s.  Correcting APN", pGrGrFile, myCounty.acCntyCode);

   // Open input file
   LogMsg("Open input file %s", pGrGrFile);
   fhIn = CreateFile(pGrGrFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", pGrGrFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Open Apn file
   LogMsg("Open Apn file %s", pApnFile);
   FILE *fdApn = fopen(pApnFile, "r");
   if (fdApn == NULL)
   {
      LogMsg("***** Error opening Apn file: %s\n", pApnFile);
      return 2;
   }
   // Get 1st rec
   pTmp = fgets((char *)&acApnRec[0], 256, fdApn);

   // Copy skip record
   bRet = true;
   memset(acBuf, ' ', iRecLen);
   pGrGrRec = (GRGR_DEF *)&acBuf[0];

   // Merge loop
   bEof = false;
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iGrGrRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (iGrGrRecLen != nBytesRead)
         break;

NextApnRec:
      iTmp = memcmp(pGrGrRec->APN, (char *)&acApnRec[iOldApnOffset], iOldApnLen);
      if (!iTmp)
      {
         memcpy(pGrGrRec->APN, (char *)&acApnRec[iNewApnOffset], iApnLen);
      } else if (iTmp > 0)
      {
         // Get next roll record
         pTmp = fgets(acApnRec, 256, fdApn);

         if (!pTmp)
            bEof = true;
         else
            goto NextApnRec;
      } else
      {
         //if (bDebug)
         //   LogMsg0("Unknown APN: %.*s", iGrGrApnLen, pGrGrRec->APN);
      }

      memset(&pGrGrRec->APN[iApnLen], ' ', SIZ_GR_APN-iApnLen);
      bRet = WriteFile(fhOut, acBuf, iGrGrRecLen, &nBytesWritten, NULL);
      lCnt++;
      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   while (bRet && iGrGrRecLen == nBytesRead)
   {
      bRet = ReadFile(fhIn, acBuf, iGrGrRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (iGrGrRecLen != nBytesRead)
         break;

      memset(&pGrGrRec->APN[iApnLen], ' ', SIZ_GR_APN-iApnLen);
      bRet = WriteFile(fhOut, acBuf, iGrGrRecLen, &nBytesWritten, NULL);
      lCnt++;
      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   printf("\n");

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);
   if (fdApn)
      fclose(fdApn);

   // Resort output file
   sprintf(acTmp, "S(%d,%d,C,A) F(FIX,%d)", SIZ_GR_DOCNUM+1, iApnLen, iGrGrRecLen);
   lCnt = sortFile(acOutFile, pOutFile, acTmp);

   LogMsg("Total output records:       %u", lCnt);

   return lCnt;
}

/***************************** GrGr_ExtrSaleMatched **************************
 *
 * Convert GRGR_DEF to SCSAL_REC format and translate DocTitle to DocType
 * Used by TUO
 *
 *****************************************************************************/

int GrGr_To_Sale(LPCSTR pGrGrFile, LPCSTR pSaleFile, IDX_TBL5 *pDocTbl)
{
   char      acBuf[2048], acSaleRec[1024];
   long      lCnt=0, iTmp;
   char      *pTmp;

   CString   sTmp, sApn, sType;
   SCSAL_REC *pSaleRec = (SCSAL_REC *)&acSaleRec;
   GRGR_DEF  *pGrGr = (GRGR_DEF *)&acBuf[0];

   LogMsg("\nExtract matched grgr from %s to %s", pGrGrFile, pSaleFile);

   if (!(fdSale = fopen(pSaleFile, "w")))
   {
      LogMsg("***** Error creating %s file", pSaleFile);
      return -1;
   }

   // Open input file
   if (!(fdGrGr = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error creating %s file", pGrGrFile);
      return -1;
   }

   // Loop through
   while (!feof(fdGrGr))
   {
      // Get input rec
      pTmp = fgets(acBuf, 2048, fdGrGr);
      if (!pTmp)
         break;         // EOF

      memset(acSaleRec, 32, sizeof(SCSAL_REC));

      pSaleRec->ApnMatched = 'Y';
      memcpy(pSaleRec->Apn, pGrGr->APN, SALE_SIZ_APN);
      memcpy(pSaleRec->DocDate, pGrGr->DocDate, SALE_SIZ_DOCDATE);
      memcpy(pSaleRec->DocNum, pGrGr->DocNum, SALE_SIZ_DOCNUM);

      // DocType
      iTmp = findDocType(pGrGr->DocTitle, pDocTbl);
      if (iTmp >= 0)
      {
         memcpy(pSaleRec->DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
         pSaleRec->NoneSale_Flg = pDocTbl[iTmp].flag;
      } else
         pSaleRec->NoneSale_Flg = 'Y';

      // Sale price
      memcpy(pSaleRec->StampAmt, pGrGr->Tax, SIZ_GR_TAX);
      memcpy(pSaleRec->SalePrice, pGrGr->SalePrice, SIZ_GR_SALE);

      // Get grantors
      memcpy(pSaleRec->Seller1, pGrGr->Grantors[0].Name, SALE_SIZ_SELLER);

      // Get grantees
      memcpy(pSaleRec->Name1, pGrGr->Grantees[0].Name, SALE_SIZ_NAME);
      memcpy(pSaleRec->Name2, pGrGr->Grantees[1].Name, SALE_SIZ_NAME);

      pSaleRec->Etal = pGrGr->MoreName;
      pSaleRec->ARCode = 'R';

      pSaleRec->CRLF[0] = '\n';
      pSaleRec->CRLF[1] = 0;
      fputs(acSaleRec, fdSale);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdGrGr);
   fclose(fdSale);

   LogMsg("Number of records processed: %ld\n", lCnt);
   return lCnt;
}

/***************************** MergeGrGrDocRec *******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * This is the modified version of MergeSale() which will not update sale in 
 * the same date without SaleAmt. - LAX
 * 
 *****************************************************************************/

int MergeGrGrDocRec(char *pGrGrRec, char *pOutbuf, bool bUpdtOwner)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   char  *pTmp, acTmp[32];

   GRGR_DOC *pSaleRec = (GRGR_DOC *)pGrGrRec;

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);

   // Update transfers - added 7/2/2014
   if (lCurSaleDt == lLstSaleDt)
   {
      if (memcmp(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC))
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
   } else if (lCurSaleDt > lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }

   // Drop if no sale price 05/13/2010 sn
   lPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT); 
   if (!lPrice || !memcmp(pSaleRec->DocTitle, "CANCEL", 6))
      return 0;

   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt)
      return -1;

   if (lCurSaleDt == lLstSaleDt)
   {
      if (*(pOutbuf+OFF_SALE1_DOC) == ' ' && pSaleRec->DocNum[0] > ' ')
         memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
      if (*(pOutbuf+OFF_SELLER) == ' ' && *pSaleRec->Grantor[0] > ' ')
         memcpy(pOutbuf+OFF_SELLER, pSaleRec->Grantor[0], SIZ_SELLER);

      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (lLastAmt > 0)
         return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE3_DT);
   if (isdigit(pSaleRec->DocTitle[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocTitle, 3);
   else if (pTmp=findDocType(pSaleRec->DocTitle, acTmp))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pTmp, strlen(pTmp));

   // Remove sale code - 
   // Notes: SALE_SIZ_SALECODE is smaller than  SIZ_SALE1_CODE.  Don't swap them.
   if (pSaleRec->SaleCode[0] >= '0')
      memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, SALE_SIZ_SALECODE);
   else
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);

   if (pSaleRec->Grantor[0][0] > ' ')
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->Grantor[0], SIZ_SELLER);

   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
   *(pOutbuf+OFF_AR_CODE1) = 'R';

   if (lCurSaleDt > lLastRecDate)
      lLastRecDate = lCurSaleDt;

   return 1;
}

/***************************** MergeGrGrDocRec2 *******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * This is the modified version of MergeGrGrDocRec() but it will copy DocType 
 * over, only use DocTitle if DocType is blank.
 * 04/10/2018: Ignore records where DocNum[4] != 'R'
 * 
 *****************************************************************************/

int MergeGrGrDocRec2(char *pGrGrRec, char *pOutbuf, bool bUpdtOwner)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   char  *pTmp, acTmp[32];

   GRGR_DOC *pSaleRec = (GRGR_DOC *)pGrGrRec;

   // Ignore non-transfer and internal docs
   if (pSaleRec->NoneXfer == 'Y' || pSaleRec->DocNum[4] != 'R')
      return 0;

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);

   // Update transfers - added 7/2/2014
   if (lCurSaleDt == lLstSaleDt)
   {
      if (memcmp(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC))
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
   } else if (lCurSaleDt > lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }

   // Drop if not a sale 
   if (pSaleRec->NoneSale == 'Y')
      return 0;

   lPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT); 
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt)
      return -1;

   if (lCurSaleDt == lLstSaleDt)
   {
      if (*(pOutbuf+OFF_SALE1_DOC) == ' ' && pSaleRec->DocNum[0] > ' ')
         memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
      if (*(pOutbuf+OFF_SELLER) == ' ' && *pSaleRec->Grantor[0] > ' ')
         memcpy(pOutbuf+OFF_SELLER, pSaleRec->Grantor[0], SIZ_SELLER);

      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (lLastAmt > 0)
         return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE3_DT);
   if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, 3);
   else if (pTmp=findDocType(pSaleRec->DocTitle, acTmp))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pTmp, strlen(pTmp));

   // Remove sale code - 
   // Notes: SALE_SIZ_SALECODE is smaller than  SIZ_SALE1_CODE.  Don't swap them.
   if (pSaleRec->SaleCode[0] >= '0')
      memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, SALE_SIZ_SALECODE);
   else
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);

   if (pSaleRec->Grantor[0][0] > ' ')
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->Grantor[0], SIZ_SELLER);

   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
   *(pOutbuf+OFF_AR_CODE1) = 'R';

   if (lCurSaleDt > lLastRecDate)
      lLastRecDate = lCurSaleDt;

   return 1;
}

/***************************** MergeGrGrDocRec *******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * This is the modified version of MergeGrGr() which only update sale if there
 * is DocType. Ignore sale price. Strip off year in DocNum - RIV
 * 
 *****************************************************************************/

int MergeGrGrDocRec1(char *pGrGrRec, char *pOutbuf, bool bUpdtOwner)
{
   long  lCurSaleDt, lLstSaleDt, lLastAmt, lCurAmt;
   char  acDocNum[32];

   GRGR_DOC *pSaleRec = (GRGR_DOC *)pGrGrRec;

   // Drop if not a transfer
   if (pSaleRec->NoneXfer == 'Y')
      return 0;

   if (strlen(pGrGrRec) < 511)
   {
      LogMsg("*** Bad GrGr rec: %.12s", pSaleRec->APN);
      return -1;
   }

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
   memset(acDocNum, ' ', SIZ_TRANSFER_DOC);
   memcpy(acDocNum, &pSaleRec->DocNum[5], SIZ_TRANSFER_DOC-5);

   // Update transfers 
   if (lCurSaleDt == lLstSaleDt)
   {
      if (memcmp(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC))
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC);
   } else if (lCurSaleDt > lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }

   // Drop if not a sale transaction
   if (pSaleRec->NoneSale == 'Y')
      return 0;

   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt)
      return -1;

   if (lCurSaleDt == lLstSaleDt)
   {
      if (*(pOutbuf+OFF_SALE1_DOC) == ' ' && acDocNum[0] > ' ')
         memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, SIZ_SALE1_DOC);
      if (*(pOutbuf+OFF_SELLER) == ' ' && *pSaleRec->Grantor[0] > ' ')
         memcpy(pOutbuf+OFF_SELLER, pSaleRec->Grantor[0], SIZ_SELLER);

      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      lCurAmt  = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
      if (lLastAmt > 0 || lLastAmt == lCurAmt)
         return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, 3);
   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

   if (pSaleRec->Grantor[0][0] > ' ')
      vmemcpy(pOutbuf+OFF_SELLER, pSaleRec->Grantor[0], SIZ_SELLER);

   *(pOutbuf+OFF_AR_CODE1) = 'R';

   if (lCurSaleDt > lLastRecDate)
      lLastRecDate = lCurSaleDt;

   return 1;
}

/********************************* MergeGrGrDoc ******************************
 *
 * Merge GrGr data (CO3_GrGr.Sls) to R01. 
 * Used by LAX, MNO, RIV, SON
 *
 *****************************************************************************/

int MergeGrGrDoc(char *pCnty, int iVersion)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acSaleRec[MAX_RECSIZE], acTmp[64];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acGrGrFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   GRGR_DOC *pSaleRec = (GRGR_DOC *)acSaleRec;

   int      iTmp, iSaleUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof, bRename=true;
   long     lRet=0, lCnt=0, lSaleDate, lLastGrGrDate=0;

   LogMsg0("Merge GrGr via MergeGrGrDoc()");
   memset(acSaleRec, ' ', sizeof(GRGR_DOC));

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");
      bRename = false;
   }

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "N01");
   sprintf(acGrGrFile, acGrGrTmpl, pCnty, pCnty, "Sls");

   if (!iGrGrApnLen)
      iGrGrApnLen = myCounty.iApnLen;

   if (_access(acGrGrFile, 0))
   {
      LogMsg("*** Missing GrGr file: %s", acGrGrFile);
      return 0;
   }

   // Open GrGr file
   LogMsg("Open GrGr file %s", acGrGrFile);
   fdSale = fopen(acGrGrFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening GrGr file: %s\n", acGrGrFile);
      return 2;
   }

   // Get first record
   pTmp = fgets(acSaleRec, sizeof(GRGR_DOC), fdSale);
   if (!pTmp || feof(fdSale))
   {
      LogMsg("*** No GrGr data available");
      return 0;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file to merge GrGr: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file to merge GrGr: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhIn = 0;
         fhOut = 0;

         LogMsg("Rename output file %s to %s", acOutFile, acRawFile);
         if (bRename)
         {
            sprintf(acTmp, "M0%c", cFileCnt|0x30);
            sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
            if (!_access(acBuf, 0))
               remove(acBuf);
            iTmp = rename(acRawFile, acBuf);
            iTmp = rename(acOutFile, acRawFile);
         } else
         {
            sprintf(acTmp, "R0%c", cFileCnt|0x30);
            sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
            iTmp = rename(acOutFile, acBuf);
         }


         // Check for next input
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               LogMsg("***** Error opening input file to merge GrGr: %s\n", acRawFile);
               break;
            }
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

            // Open Output file
            acOutFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
            LogMsg("Open output file %s", acOutFile);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
            if (fhOut == INVALID_HANDLE_VALUE)
            {
               LogMsg("***** Error opening output file to merge GrGr: %s\n", acOutFile);
               break;
            }
         } else
            break;
      }

#ifdef _DEBUG
      //if (!memcmp(acBuf, "130380013", 9) )
      //   iTmp = 0;
#endif

      // Update sale
GrGr_ReLoad:
      iTmp = memcmp(acBuf, pSaleRec->APN, iGrGrApnLen);
      if (!iTmp)
      {
         lSaleDate = atoin(pSaleRec->DocDate, 8);
         // 07/11/2024 if (lSaleDate > lLienDate)
         {
            // Merge sale data
            switch (iVersion)
            {
               case 0: // LAX
                  iTmp = MergeGrGrDocRec(acSaleRec, acBuf, false);
                  break;
               case 1: // RIV
                  iTmp = MergeGrGrDocRec1(acSaleRec, acBuf, false);
                  break;
               case 2:  // SON, MNO
                  iTmp = MergeGrGrDocRec2(acSaleRec, acBuf, false);
                  break;
               default: 
                  iTmp = MergeGrGrDocRec(acSaleRec, acBuf, false);
                  break;
            }
            if (iTmp > 0)
            {
               iSaleUpd++;
               if (lSaleDate > lLastGrGrDate)
                  lLastGrGrDate = lSaleDate;
            }
         }

         // Read next sale record
         pTmp = fgets(acSaleRec, MAX_RECSIZE, fdSale);
         if (!pTmp)
            bEof = false;    // Signal to stop sale update
         else
            goto GrGr_ReLoad;
      } else
      {
         if (iTmp > 0)        // Sale not match, advance to next sale record
         {
            if (!memcmp(acBuf, "99999999", 8))
            {
               nBytesRead = 0;
               continue;
            } else
            {
               iNoMatch++;
               if (bDebug)
                  LogMsg0("*** Sale not match : %.12s > %.12s (%d) ", acBuf, pSaleRec->APN, lCnt);
               pTmp = fgets(acSaleRec, MAX_RECSIZE, fdSale);
               if (!pTmp)
                  bEof = true;    // Signal to stop sale update
               else
                  goto GrGr_ReLoad;
            }
         }
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = -99;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }


   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
   {
      CloseHandle(fhIn);

      // Rename outfile
      if (bRename)
      {
         sprintf(acTmp, "M0%c", cFileCnt|0x30);
         sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
         if (!_access(acBuf, 0))
            remove(acBuf);
         iTmp = rename(acRawFile, acBuf);
      } else
      {
         sprintf(acTmp, "R0%c", cFileCnt|0x30);
         sprintf(acRawFile, acRawTmpl, pCnty, pCnty, acTmp);
      }
      LogMsg("Rename output file %s to %s", acOutFile, acRawFile);
      iTmp = rename(acOutFile, acRawFile);
   }

   // Update output record count
   if (lCnt > 1000)
      lRecCnt = lCnt;

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total GrGr records updated: %u", iSaleUpd);
   LogMsg("Total GrGr records skipped: %u", iNoMatch);
   LogMsg("Last GrGr recording date:   %u", lLastGrGrDate);
   
   return 0;
}

/***************************** MergeGrGrDefRec *******************************
 *
 * Merge new sale data into R01.  Move other sales accordingly.
 * This version is for FRE 11/04/2018
 *
 *****************************************************************************/

int MergeGrGrDefRec(char *pGrGrRec, char *pOutbuf, char *pDocType, char NoneSale, char NoneXfer, int iVersion)
{
   long  lCurSaleDt, lLstSaleDt, lLastAmt, lCurAmt;
   char  acDocNum[32], acSeller[SIZ_GR_NAME], *pTmp;

   GRGR_DEF *pRec = (GRGR_DEF *)pGrGrRec;

   if (NoneXfer == 'Y')
      return 0;

   // Prepare seller
   if (pRec->Grantors[0].Name[0] > ' ')
   {
      memcpy(acSeller, &pRec->Grantors[0].Name[0], SIZ_SELLER);
      acSeller[SIZ_SELLER] = 0;
      if (pTmp = strchr(acSeller, ','))
         *pTmp = 0;
   } else
      acSeller[0] = 0;

   // Ignore old GrGr transactions
   lCurSaleDt = atoin(pRec->DocDate, SIZ_SALE1_DT);
   lCurAmt = atoin(pRec->SalePrice, SIZ_SALE1_AMT);
   if (lCurSaleDt < lLienDate)
   {
      if (!memcmp(pOutbuf+OFF_SALE1_DT, pRec->DocDate, SIZ_SALE1_DT) && acSeller[0] > ' ')
         vmemcpy(pOutbuf+OFF_SELLER, acSeller, SIZ_SELLER);
      return 0;
   }

   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
   memcpy(acDocNum, pRec->DocNum, SIZ_TRANSFER_DOC);

   // Update transfers 
   if (lCurSaleDt > lLstSaleDt || 
      (lCurSaleDt == lLstSaleDt && memcmp(acDocNum, pOutbuf+OFF_TRANSFER_DOC, SIZ_TRANSFER_DOC) > 0))
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->DocDate, SIZ_TRANSFER_DT);
   }

   // Drop if not a sale transaction
   if (NoneSale == 'Y')
      return 0;

   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt)
      return -1;

   if (lCurSaleDt == lLstSaleDt)
   {
      if (*(pOutbuf+OFF_SALE1_DOC) == ' ' && acDocNum[0] > ' ')
         memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, SIZ_SALE1_DOC);

      if (*(pOutbuf+OFF_SELLER) == ' ' && pRec->Grantors[0].Name[0] > ' ')
         memcpy(pOutbuf+OFF_SELLER, &pRec->Grantors[0].Name[0], SIZ_SELLER);

      lLastAmt= atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (lLastAmt > 0 || lCurAmt == 0 || lCurAmt == lLastAmt)
         return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pRec->DocDate, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pDocType, 3);
   memcpy(pOutbuf+OFF_SALE1_AMT, pRec->SalePrice, SIZ_SALE1_AMT);
   if (pRec->MultiApn == 'Y')
      *(pOutbuf+OFF_SALE1_CODE) = 'P';

   if (pRec->Grantors[0].Name[0] > ' ')
      memcpy(pOutbuf+OFF_SELLER, &pRec->Grantors[0].Name[0], SIZ_SELLER);

   *(pOutbuf+OFF_AR_CODE1) = 'R';

   if (lCurSaleDt > lLastRecDate)
      lLastRecDate = lCurSaleDt;

   return 1;
}

/********************************* MergeGrGrDef ******************************
 *
 * Merge GrGr data (???_GrGr.Sls) to R01. Used by FRE.
 * iVersion: 0=Copy DocNum as is
 *           1=Strip off year in DocNum (skip 5 bytes)
 *           2=Check DocType instead of DocTitle (SIS)
 *
 * Return 0 if successful
 *
 *****************************************************************************/

int MergeGrGrDef(char *pCnty, bool bExactTitle, int iVersion)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acTmp[256], sDocType[8];
   char     cNoneSale, cNoneXfer, cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acGrGrFile[_MAX_PATH], acGrgrRec[2048];

   HANDLE   fhIn, fhOut;
   GRGR_DEF *pGrgrRec = (GRGR_DEF *)&acGrgrRec;

   int      iDeedIdx, iRet, iTmp, iSaleUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof, bRename=true;
   long     lCnt=0;

   LogMsg0("Merge %s GrGr", myCounty.acCntyCode);

   memset(&acGrgrRec, ' ', sizeof(GRGR_DEF));

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");
      bRename = false;
   }

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "N01");
   sprintf(acGrGrFile, acGrGrTmpl, pCnty, pCnty, "Sls");

   if (!iGrGrApnLen)
      iGrGrApnLen = myCounty.iApnLen;

   if (_access(acGrGrFile, 0))
   {
      LogMsg("*** Missing GrGr file: %s", acGrGrFile);
      return 0;
   }

   // Open GrGr file
   LogMsg("Open GrGr file: %s", acGrGrFile);
   fdSale = fopen(acGrGrFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening GrGr file: %s\n", acGrGrFile);
      return -2;
   }

   // Get first record
   pTmp = fgets((char *)&acGrgrRec, 2048, fdSale);
   if (!pTmp || feof(fdSale))
   {
      LogMsg("*** No GrGr data available");
      return 0;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file to merge GrGr: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file to merge GrGr: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iRet=iNoMatch=0;
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhIn = 0;
         fhOut = 0;

         LogMsg("Rename output file %s to %s", acOutFile, acRawFile);
         if (bRename)
         {
            sprintf(acTmp, "M0%c", cFileCnt|0x30);
            sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
            if (!_access(acBuf, 0))
               remove(acBuf);
            iTmp = rename(acRawFile, acBuf);
            iTmp = rename(acOutFile, acRawFile);
         } else
         {
            sprintf(acTmp, "R0%c", cFileCnt|0x30);
            sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
            iTmp = rename(acOutFile, acBuf);
         }


         // Check for next input
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               LogMsg("***** Error opening input file to merge GrGr: %s\n", acRawFile);
               break;
            }
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

            // Open Output file
            acOutFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
            LogMsg("Open output file %s", acOutFile);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
            if (fhOut == INVALID_HANDLE_VALUE)
            {
               LogMsg("***** Error opening output file to merge GrGr: %s\n", acOutFile);
               break;
            }
         } else
            break;
      }

      // Update sale
GrGr_ReLoad:
      iTmp = memcmp(acBuf, pGrgrRec->APN, iGrGrApnLen);
      if (!iTmp)
      {
         if (pGrgrRec->DocTitle[0] > ' ')
         {
#ifdef _DEBUG
            //if (!memcmp(acBuf, "004500051000", 9) ) 
            //   pTmp = 0;
#endif
            
            if (iVersion == 2)
            {
               memcpy(acTmp, pGrgrRec->DocType, SIZ_GR_DOCTYPE);
               myTrim(acTmp, SIZ_GR_DOCTYPE-1);
            } else
            {
               if (pGrgrRec->DocTitle[0] == '-')
                  memcpy(acTmp, &pGrgrRec->DocTitle[1], SIZ_GR_TITLE-1);
               else
                  memcpy(acTmp, pGrgrRec->DocTitle, SIZ_GR_TITLE);
               myTrim(acTmp, SIZ_GR_TITLE-1);
            }

            // Translate DocType
            if (bExactTitle)
               iDeedIdx = XrefCodeIndexEx((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);     // look for exact match
            else
               iDeedIdx = XrefCodeIndex((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);       // Match a portion of title
            if (iDeedIdx >= 0)
            {
               cNoneSale = asDeed[iDeedIdx].acFlags[0];
               cNoneXfer = asDeed[iDeedIdx].acOther[0];
               iTmp = sprintf(sDocType, "%d     ", asDeed[iDeedIdx].iIdxNum);
            } else if (acTmp[0] > ' ')
            {
               iTmp = 0;
               LogMsg("*** Unknown Doc code: [%s] APN=%.12s", acTmp, pGrgrRec->APN);
            }
         }

         if (iTmp > 0)
         {
            // Overwrite assessor sale
            acBuf[OFF_MULTI_APN] = pGrgrRec->MultiApn;
            if (pGrgrRec->MultiApn == 'Y')
               acBuf[OFF_SALE1_CODE] = 'P';

            iTmp = MergeGrGrDefRec(acGrgrRec, acBuf, sDocType, cNoneSale, cNoneXfer, iVersion);
            if (iTmp > 0)
               iSaleUpd++;
         }

         // Read next sale record
         pTmp = fgets(acGrgrRec, 2048, fdSale);
         if (!pTmp)
            bEof = false;    // Signal to stop sale update
         else
            goto GrGr_ReLoad;
      } else
      {
         if (iTmp > 0)        // Sale not match, advance to next sale record
         {
            if (!memcmp(acBuf, "99999999", 8))
            {
               nBytesRead = 0;
               continue;
            } else
            {
               iNoMatch++;
               if (bDebug)
                  LogMsg0("*** Sale not match : %.12s > %.12s (%d) ", acBuf, pGrgrRec->APN, lCnt);
               pTmp = fgets(acGrgrRec, 2048, fdSale);
               if (!pTmp)
                  bEof = true;    // Signal to stop sale update
               else
                  goto GrGr_ReLoad;
            }
         }
      }
#ifdef _DEBUG
      //if (!memcmp(acBuf, "00113025", 8) ) 
      //   iTmp = 0;
#endif

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         iRet = -99;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
   {
      CloseHandle(fhIn);

      // Rename outfile
      if (bRename)
      {
         sprintf(acTmp, "M0%c", cFileCnt|0x30);
         sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
         if (!_access(acBuf, 0))
            remove(acBuf);
         iTmp = rename(acRawFile, acBuf);
      } else
      {
         sprintf(acTmp, "R0%c", cFileCnt|0x30);
         sprintf(acRawFile, acRawTmpl, pCnty, pCnty, acTmp);
      }
      LogMsg("Rename output file %s to %s", acOutFile, acRawFile);
      iTmp = rename(acOutFile, acRawFile);
   }

   // Update output record count
   if (lCnt > 1000)
      lRecCnt = lCnt;
   else
      iRet = 1;

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total GrGr records updated: %u", iSaleUpd);
   LogMsg("Total GrGr records skipped: %u", iNoMatch);
   LogMsg("Last GrGr recording date    %u", lLastRecDate);

   return iRet;
}

/***************************** GrGr_ChkApnLen ********************************
 *
 * Check APN length to make sure it has correct length
 * Output bad APN to out file
 * Return number of records output
 *
 *****************************************************************************/

int GrGr_ChkApnLen(LPCSTR pGrGrFile, LPCSTR pOutFile, int iLen, int iOffset /* zero base */)
{
   char     acBuf[2048], acTmp[32];
   long     lCnt=0, lOut=0;
	int		iTmp;
	FILE		*fdOut;

   LogMsg("\nCheck APN length for %s", pGrGrFile);

   if (!(fdOut = fopen(pOutFile, "w")))
   {
      LogMsg("***** Error creating output file: %s (%d)", pOutFile, _errno);
      return -1;
   }

   // Open input file
   if (!(fdGrGr = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error opening %s file (%d)", pGrGrFile, _errno);
      return -1;
   }

   // Loop through
   while (!feof(fdGrGr))
   {
      // Get input rec
      if (!fgets(acBuf, 2048, fdGrGr))
         break;         // EOF

      memcpy(acTmp, &acBuf[iOffset], SIZ_APN_S);
		iTmp = iTrim(acTmp, SIZ_APN_S);
		if (iTmp > 0 && iTmp != iLen)
		{
			acBuf[80] = 10;
			acBuf[81] = 0;
			fputs(acBuf, fdOut);
			lOut++;
		}

      if (!(++lCnt % 10000))
         printf("\r%u", lCnt);
   }

   fclose(fdGrGr);
   fclose(fdOut);

   LogMsg("Number of records processed: %ld", lCnt);
   LogMsg("Number of records output   : %ld\n", lOut);
   return lOut;
}

/*****************************************************************************
 *
 * Move GrGr files to its subfolder.
 * Return:
 *    -1 : if error
 *     0 : file not available
 *     1 : successfully move
 *
 ****************************************************************************/

int moveGrGrFiles(LPCSTR pIniFile, LPCSTR pCnty, bool bAddDate)
{
   char     acGrGrSrc[_MAX_PATH], acTmp[_MAX_PATH];
   char     acSrc[_MAX_PATH], acDst[_MAX_PATH], acDate[32], *pTmp;
   long     lHandle;
   int      iRet;
   struct   _finddata_t  sFileInfo;

   GetIniString(pCnty, "GrGrSrc", "", acGrGrSrc, _MAX_PATH, pIniFile);
   // Check for file exist
   lHandle = _findfirst(acGrGrSrc, &sFileInfo);
   if (lHandle > 0)
   {
      strcpy(acTmp, acGrGrSrc);
      pTmp = strrchr(acGrGrSrc, '\\');
      if (pTmp)
         *++pTmp = 0;
      else
      {
         _findclose(lHandle);
         return -1;
      }

      // Get today date
      dateString(acDate, 0);

      do
      {
         sprintf(acSrc, "%s%s", acGrGrSrc, sFileInfo.name);
         if (!bAddDate)
            sprintf(acDst, "%sGrGr\\%s", acGrGrSrc, sFileInfo.name);
         else
         {
            strcpy(acTmp, sFileInfo.name);
            pTmp = strrchr(acTmp, '.');
            if (pTmp)
            {
               *pTmp++ = 0;
               sprintf(acDst, "%sGrGr\\%s_%s.%s", acGrGrSrc, acTmp, acDate, pTmp);
            } else
               sprintf(acDst, "%sGrGr\\%s_%s", acGrGrSrc, acTmp, acDate);
         }

         // Remove old file if exist
         if (!_access(acDst, 0))
            remove(acDst);

         // Move file
         iRet = rename(acSrc, acDst);
         if (iRet == -1)
         {
            iRet = -1234;
            break;
         }

         // Find next file
         iRet = _findnext(lHandle, &sFileInfo);
      } while (!iRet);

      // Close handle
      _findclose(lHandle);

      if (iRet != -1234)
         iRet = 1;
   } else
      iRet = 0;

   return iRet;
}

/******************************* FixCumGrGr **********************************
 *
 * Fix specific case in sale file.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int FixCumGrGr(char *pInfile, int iType, bool bRemove)
{
   char     acInbuf[2048], acOutFile[_MAX_PATH], *pRec, sTmp1[32];
   long     lCnt=0, lClean=0, iTmp, lVal;
   FILE     *fdOut;

   GRGR_DEF *pInRec  = (GRGR_DEF *)&acInbuf[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** FixCumGrGrDef(): Missing input file: %s", pInfile);
      return -1;
   }

   switch (iType & 0x0000FFFF)
   {
      case SALE_FLD_DOCNUM:
         LogMsg("Fix DocNum for %s", pInfile);
         break;
      case SALE_FLD_SALEPRICE:
         LogMsg("Fix SalePrice for %s", pInfile);
         break;
      default:
         LogMsg("Invalid county for %s.  Ignore command.", pInfile);
         return -2;
   }

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 2048, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0282630300", 9))
      //   iTmp = 0;
#endif
      switch (iType)
      {
         case SALE_FLD_DOCNUM+CNTY_FRE:      // Reformat DocNum for FRE
            // Format DOCNUM to 1234567
            if (pInRec->DocNum[6] > ' ')
            {
               lVal = atoin(&pInRec->DocNum[5], 7);
               iTmp = atoin(pInRec->DocDate, 4);
               if (lVal > 0 && iTmp > 1980)
               {
                  iTmp = sprintf(sTmp1, "%.7d       ", lVal);
                  memcpy(&pInRec->DocNum[0], sTmp1, iTmp);
                  lClean++;
               }
            }
            break;
         default:
            LogMsg("Invalid county for %s.  Ignore command.", pInfile);
            return -2;
      }

      pInRec->CRLF[0] = '\n';
      pInRec->CRLF[1] = 0;

      fputs(acInbuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if (lClean > 0)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".org");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Rename output file
      iTmp = rename(acOutFile, pInfile);
   } else
      LogMsg("Good sale file, no record drop!");

   LogMsgD("\nTotal input records:     %u\n", lCnt);
   LogMsg("    records cleaned:     %u", lClean);

   return iTmp;
}

/******************************* FixGrGrDoc **********************************
 *
 * Fix specific case in GrGr file.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int FixGrGrDoc(char *pInfile, int iType, bool bRemove)
{
   char     acInbuf[2048], acOutFile[_MAX_PATH], *pRec, sTmp1[32];
   long     lCnt=0, lClean=0, iTmp, lVal;
   bool     bDrop;
   FILE     *fdOut;

   GRGR_DOC *pInRec  = (GRGR_DOC *)&acInbuf[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** FixCumGrGrDef(): Missing input file: %s", pInfile);
      return -1;
   }

   switch (iType & 0x0000FFFF)
   {
      case 0:
         LogMsg("Fix record length for %s", pInfile);
         break;
      case SALE_FLD_DOCNUM:
         LogMsg("Fix DocNum for %s", pInfile);
         break;
      case SALE_FLD_SALEPRICE:
         LogMsg("Fix SalePrice for %s", pInfile);
         break;
      default:
         LogMsg("Invalid county for %s.  Ignore command.", pInfile);
         return -2;
   }

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 2048, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0282630300", 9))
      //   iTmp = 0;
#endif

      bDrop = false;
      switch (iType)
      {
         case SALE_FLD_DOCNUM+CNTY_FRE:      // Reformat DocNum for FRE
            // Format DOCNUM to 1234567
            if (pInRec->DocNum[6] > ' ')
            {
               lVal = atoin(&pInRec->DocNum[5], 7);
               iTmp = atoin(pInRec->DocDate, 4);
               if (lVal > 0 && iTmp > 1980)
               {
                  iTmp = sprintf(sTmp1, "%.7d       ", lVal);
                  memcpy(&pInRec->DocNum[0], sTmp1, iTmp);
                  lClean++;
               }
            }
            break;
         case CNTY_RIV:
            memcpy(sTmp1, pInRec->APN, iApnLen);
            sTmp1[iApnLen] = 0;
            // Remove hyphen from APN
            if (strchr(sTmp1, '-'))
            {
               remChar(sTmp1, '-');
               blankPad(sTmp1, SIZ_GD_APN);
               memcpy(pInRec->APN, sTmp1, SIZ_GD_APN);
            }
            // Check for valid APN
            if (!isNumber(pInRec->APN, iApnLen))
            {
               LogMsg("Drop: %s", acInbuf);
               bDrop = true;
               lClean++;
            }
            break;
         default:
            LogMsg("Invalid county for %s.  Ignore command.", pInfile);
            return -2;
      }

      if (!bDrop)
         fputs(acInbuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if (lClean > 0)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".org");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Rename output file
      iTmp = rename(acOutFile, pInfile);
   } else
      LogMsg("Good sale file, no record drop!");

   LogMsgD("\nTotal input records:     %u\n", lCnt);
   LogMsg("    records cleaned:     %u", lClean);

   return iTmp;
}

int FixGrGrDef(char *pInfile, int iType, bool bRemove)
{
   char     acInbuf[2048], acTmp[32], acOutFile[_MAX_PATH], *pRec;
   long     lCnt=0, lClean=0, iTmp, iLen;
   double   dTmp;
   bool     bDrop;
   FILE     *fdOut;

   GRGR_DEF *pInRec  = (GRGR_DEF *)&acInbuf[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** FixCumGrGrDef(): Missing input file: %s", pInfile);
      return -1;
   }

   switch (iType & 0x0000FFFF)
   {
      case 0:
         LogMsg("Fix record length for %s", pInfile);
         break;
      case SALE_FLD_DOCNUM:
         LogMsg("Fix DocNum for %s", pInfile);
         break;
      case SALE_FLD_SALEPRICE:
         LogMsg("Fix SalePrice for %s", pInfile);
         break;
      case SALE_FLD_STAMPAMT:
         LogMsg("Fix DocTax for %s", pInfile);
         break;
      default:
         LogMsg("Invalid county for %s.  Ignore command.", pInfile);
         return -2;
   }

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 2048, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0282630300", 9))
      //   iTmp = 0;
#endif

      bDrop = false;
      switch (iType)
      {
         case 0:
            iLen = strlen(acInbuf);
            if (iLen < sizeof(GRGR_DEF) -2)
            {
               memset(pInRec->DocType, ' ', SIZ_GR_DOCTYPE);
               pInRec->CRLF[0] = 10;
               pInRec->CRLF[1] = 0;
               lClean++;
            } else
               bDrop = false;
            break;
         case SALE_FLD_STAMPAMT:
            dTmp = atofn(pInRec->Tax, SIZ_GR_TAX);
            if (dTmp > 0.0)
            {
               iTmp = sprintf(acTmp, "%.2f        ", dTmp/100.0);
               memcpy(pInRec->Tax, acTmp, SIZ_GR_TAX);
               lClean++;
            }
            break;
         default:
            LogMsg("Invalid county for %s.  Ignore command.", pInfile);
            return -2;
      }

      if (!bDrop)
         fputs(acInbuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if (lClean > 0)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".org");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Rename output file
      iTmp = rename(acOutFile, pInfile);
   } else
      LogMsg("Good sale file, no record drop!");

   LogMsgD("\nTotal input records:     %u\n", lCnt);
   LogMsg("       records fixed:     %u", lClean);

   return iTmp;
}
