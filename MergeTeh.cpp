/**************************************************************************
 *
 * Options:
 *    -CTEH -L -Xs|-Ms [-Xa](load lien)
 *    -CTEH -U -Xs -Xa (load update)
 *    -CTEH -La (load assr lien)
 *    -CTEH -Ua (load assr update)
 *
 * Notes: InCareOf field in Teh_Roll.csv may contain extended Name1, Name2, or C/O
 *
 * Revision
 * 10/23/2015 15.4.3    First version - Still need CHAR tables updated
 * 11/09/2015 15.6.1    Replace  MB_CreateSCSale() with Teh_CreateSCSale() to handle
 *                      specific DOCNUM in TEH (ignoring DocNum starts with '9' after 'R').
 * 01/29/2016 15.10.2.1 Fix bug in MergeRoll() that causes mismatched when first record is bad.
 *                      Fix bug in MergeStdChar() that messes up PARK_SPACE.
 * 02/14/2016 15.12.0   Modify Imp_MergeMAdr() to fix overwrite bug.
 * 02/15/2016 15.12.1   Fix blank M_ZIP in Teh_MergeMAdr()
 * 04/04/2016 15.14.2   Use findXlatCodeA() on asParkType[].
 * 05/23/2016 15.15.2   Modify MergeSitus() to match with mailing (same strNum & strName).
 *                      If not, use CityZip file for Red Bluff, Corning, and Tehama.
 * 06/03/2016 15.15.3   Ignore book 555 administrative parcel in MergeRoll()
 * 06/26/2016 16.0.1    Remove extra alpha in UseCode before translation.  If no UseCode, set it to "999"
 * 06/29/2016 16.0.1    Modify Teh_MergeOwner() to break out Name2 as appropriate.
 *                      Modify Teh_MergeSitus() to populate city name from known zip code.
 *                      Modify Teh_MergeLien() to support new L3_* record.  Modify Teh_Load_LDR() to 
 *                      use situs file instead of situs from LDR file.
 * 07/07/2016 16.0.2    Modify Teh_MergeChar() to update only when new data is not blank since existing values are from LDR file
 *                      Fix USE_STD in Teh_MergeRoll() for special UseCode by adding extra space before search.
 * 07/17/2016 16.0.4    Add option to use TC601 to update value in Load_LDR()
 * 08/14/2016 16.3.0    Remove unused functions Teh_MergeTax() and Teh_MergeExe().
 * 08/19/2016 16.4.1    Add option to update PREV_APN using last year file.
 * 03/01/2017 16.9.4    Fix Other Value in Teh_MergeRoll() 
 * 06/22/2017 17.0.0    Modify Teh_ConvStdChar() & Teh_MergeChar() to fix number of parking spaces
 * 07/20/2018 18.2.1    Rename Teh_MergeLien() to Teh_MergeLien3() and create Teh_MergeLien1() to support LdrGrp 1.
 *                      Modify Teh_Load_LDR() to support both LdrGrp 1 & 3.  Only use lien extr file when value file is used.
 * 04/01/2019 18.11.1   Verify CHAR file size before processing.
 * 09/06/2019 19.2.2    Modify Load_Roll() to sort input file before processing.
 * 07/12/2020 20.1.4    Add -Mz option
 * 11/01/2020 20.4.2    Modify Teh_MergeRoll() to populate default PQZoning.
 * 02/10/2021 20.7.0    Modify loadTeh() to use sale date format specify in INI file.
 *            20.7.0.1  Reformat Transfer DocNum in Teh_MergeRoll() to YYYYR999999 to match with sale date format.
 * 02/23/2021 20.7.4    Modify Teh_CreateSCSale() to skip hdr records.
 * 05/11/2021 20.8.7    Modify Teh_MergeSitus() to hardcode TRA="003000" with City="TEHAMA" and zipcode=96090
 * 05/12/2021 20.8.8    Modify Teh_MergeOwner() to remove quotes in owner name. Fix TRANSFER_DOC in Teh_MergeRoll().
 * 07/24/2021 21.1.3    Rename Teh_MergeChar() to Teh_MergeStdChar() and add QualityClass to R01 in Teh_MergeStdChar().
 * 08/26/2021 21.2.0    Default bUseConfSalePrice=false so we don't use confidential sale price.
 * 09/15/2021 21.2.3    Reset value of UNITS in Teh_MergeStdChar().
 * 03/14/2022 21.7.0    Modify program to support new data format which includes null in records.
 * 07/21/2023 23.0.6    Handling special cases in Teh_MergeSitus().
 * 10/18/2023 23.4.0    Update skip header value in all tax loading functions in loadTeh().
 * 02/23/2024 23.6.3    Modify Teh_ConvStdChar() to support new CHAR layout.
 * 07/24/2024 24.0.2    Modify Teh_MergeLien1() to add ExeType.
 * 09/19/2024 24.1.5    Fix Status in Teh_MergeLien1() when county uses 'C' instead of 'A'.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"

#include "LoadMB.h"
#include "MergeAssr.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "CharRec.h"
#include "PQ.h"
#include "Situs.h"
#include "Tax.h"
#include "MergeTeh.h"

static long lUseGis, lUseMailCity;

/******************************** Teh_MergeOwner *****************************
 *
 * ROBERTS, RENE' C
 * KNOWLES, DANIEL O'SULLIVAN
 * ROSS, EVAH SANTOS TRUSTEE EVAH SANTOS ROSS TRUST
 * LEWIS, RALPHE E ETAL TRS R & J LEWIS FAMILY TR 6/1
 * WIKEY, DONALD R CO-TRS       WIKEY FAMILY TRUST 7/
 * SAND, ROBT B & SAND, DONNA M ETAL
 * VERWYS, JAMES S & DOROTHY M
 * VESTAL DARELD R JR TR D R VESTAL JR TRUST 10/29/0
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Teh_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iRet;
   char  acOwner[64], acName1[64], acName2[64], acTmp[128], *pTmp, *pTmp1;
   OWNER myOwner;

   // Clear output buffer
   removeNames(pOutbuf, false, false);
   memset(acTmp, 0, 128);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "027390009000", 12))
   //   iTmp = 0;
#endif

   // Init Name1
   strcpy(acName1, pNames);
   myTrim(acName1);
   acName2[0] = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Remove extra &
   if (pTmp = strstr(acName1, "& &"))
      *pTmp = ' ';

   // Check for multiple names - SAND, ROBT B & SAND, DONNA M ETAL
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, ','))
      {
         *pTmp = 0;
         strcpy(acName2, pTmp+2);
      }
   }

   // Save Owner name - This goes into Name1
   if (acName2[0] > ' ')
   {
      iRet = MergeName2(acName1, acName2, acOwner, ',');      
      if (iRet == 1)
         acName2[0] = 0;            // Name merged, drop Name2
      else
      {
         strcpy(acOwner, acName1);
         if (iRet == 2)
            acName2[0] = 0;         // Name1 == Name2, drop Name2
         else
            remChar(acName2, ',');
      }
   } else
      strcpy(acOwner, acName1);

   // Remove special words
   if (pTmp = strstr(acOwner, "DECD"))
      *pTmp = 0;

   // Remove comma and single quote in name
   remCharEx(acOwner, ",\'");

   // Remove extra blank
   iTmp = blankRem(acOwner);
   if (acOwner[iTmp-1] == '/')
      acOwner[iTmp-1] = 0;
   strcpy(acTmp, acOwner);

   // If number appears at the beginning of name, do not parse
   //if (*(pOutbuf+OFF_VEST) > ' ')
   //{
   //   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);
   //   vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
   //   return;
   //}

   // Check for double quote
   iTmp = quoteRem(acOwner);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
   }

   // Filter out words, things in parenthesis
   if ( (pTmp = strchr(acTmp, '('))      || (pTmp=strstr(acTmp, " CO TR"))   || 
        (pTmp=strstr(acTmp, " CO-TR"))   || (pTmp=strstr(acTmp, " COTR"))    ||
        (pTmp=strstr(acTmp, " TRUSTEE")) || (pTmp=strstr(acTmp, " TR ")) || 
        (pTmp=strstr(acTmp, " TTEE"))    || (pTmp=strstr(acTmp, " TRES"))    || 
        (pTmp=strstr(acTmp, " ETAL"))    || (pTmp=strstr(acTmp, " ET AL"))  )
      *pTmp = 0;

   // Filter some more
   pTmp = (char *)&acTmp[strlen(acTmp)-4];
   if (!memcmp(pTmp, " DVA", 4) )
      *pTmp = 0;

   // If there is number goes before REV, keep it.
   if (!memcmp(pTmp, " REV", 4))
   {
      pTmp1 = pTmp;
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
   }

   // Trim trailing number
   iTmp = strlen(acTmp)-1;
   while (iTmp > 0 && isdigit(acTmp[iTmp]))
      acTmp[iTmp--] = 0;

   if ((pTmp=strstr(acTmp, " SUCCS-TR")) || (pTmp=strstr(acTmp, " SUCCESSOR")) ||
      (pTmp=strstr(acTmp, " SURVIVOR")) )
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " FAMILY TRUST &")) || (pTmp=strstr(acTmp, " LIVING TRUST &")) )
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      // RUMAN RICKY L & RUMAN FAMILY TRUST &
      // RUMAN RICKY L & NANCY FAMILY TRUST & MARIE
      // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      *(pTmp+6) = 0;
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
         *pTmp1 = 0;
      else
         *pTmp = 0;

      strcpy(acName2, pTmp+9);
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " FAM TR")) || (pTmp=strstr(acTmp, " FAMILY ")) || 
      (pTmp=strstr(acTmp, " REVOC"))          || (pTmp=strstr(acTmp, " REV TR")) ||
      (pTmp=strstr(acTmp, " REV LIV TR"))     || (pTmp=strstr(acTmp, " REV LIVING")) ||       
      (pTmp=strstr(acTmp, " LIV TRUST"))      || (pTmp=strstr(acTmp, " LIVING ")) ||
      (pTmp=strstr(acTmp, " INCOME TR"))      || (pTmp=strstr(acTmp, " 1992 REV")) 
      )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp = strstr(acOwner, " P/S "))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ( (pTmp=strstr(acTmp, " ESTATE OF")) || (pTmp=strstr(acTmp, " EST OF")) ||
               (pTmp=strstr(acTmp, " ESTS OF")) )
   {  // MONDANI NELLIE M ESTATE OF       
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else 
      strcpy(acName1, acTmp);

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&TEHRON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
         *pTmp1++ = 0;
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = -1;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);

      // If name is swapable, use it
      if (iRet >= 0)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } 

   // Couldn't split names
   if (iRet == -1)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);

   // Save Name2 if exist
   if (acName2[0] > ' ')
      vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);

   // Save Name1
   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);
}

/******************************** Teh_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Teh_MergeMAdr(char *pOutbuf)
{
   char    acTmp[256], acAddr1[64], *pTmp;
   int     iTmp;
   ADR_REC sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Update CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

   // Update DBA
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   } 

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1_1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }

   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
      sMailAdr.strName[SIZ_M_STREET] = 0;
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], SIZ_M_CITY);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         // Take out '-' in zipcode if present
         strcpy(acTmp, apTokens[MB_ROLL_M_ZIP]);
         iTmp = strlen(acTmp);
         if (acTmp[5] == '-' && iTmp == 10)
         {
            strcpy(&acTmp[5], &acTmp[6]);
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         } else if (iTmp > 9)
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);         
         else
            vmemcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);   
      } else
         iTmp = 0;

      if (iTmp == 9)
         sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP], apTokens[MB_ROLL_M_ZIP]+5);
      else
         sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

void Teh_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)

{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2, *pDba;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "005070005000", 9) || !memcmp(pOutbuf, "005230043000", 9) )
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   pDba = p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }

      if (!_memicmp(pLine1, "DBA ", 4) )
         pDba = pLine1;
      else if (!_memicmp(pLine2, "DBA ", 4))
         pDba = pLine2;
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }

      if (!_memicmp(pLine1, "DBA ", 4) )
         pDba = pLine1;
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
   }

   // Update DBA
   if (pDba)
   {
      memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   strcpy(acAddr2, p2);
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);

      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}


/******************************** Teh_MergeSAdr ******************************
 *
 * Merge Situs address.  
 * If no city code and no zip code, call getCityZip() to use data from GIS
 * If zip code avail, call getCity() 
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Teh_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acAddr2[64], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do {
         pRec = fgets(acRec, 512, fdSitus);
      } while (*pRec < ' ' || *pRec > '9');
   }

   // Remove old addr regardless of there is new one or not
   removeSitus(pOutbuf);

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, acRec);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   replNull(acRec);
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   else
      iRet = ParseStringIQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);

   if (lTmp > 0)
   {
      // Save original StrNum
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         vmemcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], SIZ_S_DIR);
      }
   }

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);
      vmemcpy(pOutbuf+OFF_S_STREET, apTokens[MB_SITUS_STRNAME], SIZ_S_STREET, strlen(apTokens[MB_SITUS_STRNAME]));

      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
      {
         Sfx2Code(acTmp, acCode);
         vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      } else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         iBadSuffix++;
      }
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apTokens[MB_SITUS_STRNAME]);
      vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      vmemcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, SIZ_S_SUFF);

      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]); 
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNO);
   }

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);
   memset(acAddr2, ' ', SIZ_S_CTY_ST_D);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001310012000", 9))
   //   iTmp = 0;
#endif

   // Situs city
   char  sCity[32], sCityCode[8];

   // Fix some error
   if (!memcmp(apTokens[MB_SITUS_ZIP], "96002", 5) ||
       !memcmp(apTokens[MB_SITUS_ZIP], "96007", 5) ||
       !memcmp(apTokens[MB_SITUS_ZIP], "95963", 5) )
      *apTokens[MB_SITUS_ZIP] = 0;

   if (*apTokens[MB_SITUS_COMMUNITY] == '-' && *apTokens[MB_SITUS_ZIP] == '9')
   {
      // Compare Zip to City
      pTmp = getCity(apTokens[MB_SITUS_ZIP], sCity, sCityCode);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_S_CITY, sCityCode, strlen(sCityCode));
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], SIZ_S_ZIP);
         sprintf(acAddr2, "%s CA %s", sCity, apTokens[MB_SITUS_ZIP]);
      } else
         LogMsg("*** MergeSitus()->Unknown zip code: %s [%.12s]", apTokens[MB_SITUS_ZIP], pOutbuf);

   // Currently community code is not available
   //} else if (*apTokens[MB_SITUS_COMMUNITY] >= 'A')
   //{  
   //   Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr2, pOutbuf);   
   //   if (acAddr2[0] > ' ')
   //   {
   //      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
   //      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   //      strcat(acAddr2, " CA ");
   //   } else
   //      LogMsg("*** MergeSitus()->Unknown city code: %s [%.12s]", apTokens[MB_SITUS_COMMUNITY], pOutbuf);

   //   if (*apTokens[MB_SITUS_ZIP] == '9')
   //   {
   //      vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], SIZ_S_ZIP);
   //      strcat(acAddr2, apTokens[MB_SITUS_ZIP]);
   //   } else
   //   {
   //      pTmp = getZip(acAddr2);
   //      if (pTmp)
   //      {
   //         vmemcpy(pOutbuf+OFF_S_ZIP, pTmp, SIZ_S_ZIP);
   //         strcat(acAddr2, pTmp);
   //      }
   //   }
   } else if (!memcmp(pOutbuf+OFF_M_STRNUM, pOutbuf+OFF_S_STRNUM, SIZ_M_STRNUM) && !memcmp(pOutbuf+OFF_M_STREET, pOutbuf+OFF_S_STREET, SIZ_M_STREET))
   {
      lUseMailCity++;
      memcpy(acAddr2, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      myTrim(acAddr2, SIZ_M_CITY);
      iTmp = City2Code(acAddr2, acTmp, pOutbuf);
      if (iTmp > 0)
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, 3);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
         sprintf(acTmp, "%s CA %.5s", acAddr2, pOutbuf+OFF_M_ZIP);
         strcpy(acAddr2, acTmp);
      }
   } else if (!memcmp(pOutbuf+OFF_TRA, "003000", 6))
   {
      strcpy(acAddr2, "TEHAMA");
      iTmp = City2Code(acAddr2, acTmp, pOutbuf);
      if (iTmp > 0)
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, 3);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         memcpy(pOutbuf+OFF_S_ZIP, "96090", 5);
         sprintf(acTmp, "%s CA 96090", acAddr2);
         strcpy(acAddr2, acTmp);
      }
   }

   // Get city/zip from GIS extract - Use this option last since it's not reliable on the border
   if (fdCity && *(pOutbuf+OFF_S_CITY) == ' ' && *(pOutbuf+OFF_S_STREET) > ' ')
   {
      char acZip[16], acCity[32];
      iTmp = getCityZip(pOutbuf, acCity, acZip, 9);
      if (!iTmp) // && (!memcmp(acCity, "COR", 3) || !memcmp(acCity, "RED", 3) || !memcmp(acCity, "TEH", 3)) )
      {
         City2Code(acCity, acCode, pOutbuf);
         memcpy(pOutbuf+OFF_S_CITY, acCode, 3);
         memcpy(pOutbuf+OFF_S_ZIP, acZip, strlen(acZip));
         sprintf(acAddr2, "%s CA %s", acCity, acZip);
         lUseGis++;
      }
   }

   iTmp = blankRem(acAddr2, SIZ_S_CTY_ST_D);
   memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

//int Teh_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
//{
//   char     acTmp[256], acAddr1[128];
//   ADR_REC  sSitusAdr;
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "066101003", 9))
//   //   acTmp[0] = 0;
//#endif
//   // 
//   strcpy(acAddr1, pLine1);
//   blankRem(acAddr1);
//   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
//
//   memset(&sSitusAdr, 0, sizeof(ADR_REC));
//   parseMAdr1(&sSitusAdr, acAddr1);
//
//   if (sSitusAdr.lStrNum > 0)
//   {
//      char *pTmp = strchr(acAddr1, ' ');
//      *pTmp = 0;
//
//      // Save original StrNum
//      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
//      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
//      memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
//   }
//
//   if (sSitusAdr.strDir[0] > ' ')
//      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
//
//   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
//   if (sSitusAdr.strSfx[0] > ' ')
//   {
//      Sfx2Code(sSitusAdr.strSfx, acTmp);
//      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
//   }
//
//   if (sSitusAdr.Unit[0] > ' ')
//      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));
//
//   // Situs city
//   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
//   parseAdr2(&sSitusAdr, pLine2);
//   if (sSitusAdr.City[0] > ' ')
//   {
//      City2Code(sSitusAdr.City, acTmp, pOutbuf);
//      if (acTmp[0] > ' ')
//      {
//         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
//         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
//      }
//   }
//
//   return 0;
//}

/******************************** Teh_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Teh_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      pTmp = pRec;
      if (*pTmp == '"')
         pTmp++;

      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002017014", 9))
   //   iRet = 0;
#endif

   if (iTmp)
      return 1;

   while (!iTmp)
   {
      // Parse input
      if (cDelim == ',')
         iRet = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iRet = ParseStringIQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);

      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, YYYY_MM_DD);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         /* Do not use Confidential Sale Price
         } else
         {
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            }
         */
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         MB_MergeSale(&sCurSale, pOutbuf, true, false);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
      {
         pTmp = pRec;
         if (*pTmp == '"')
            pTmp++;
         iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      } else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/***************************** Teh_MergeChar ******************************
 *
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the one that has value.
 *
 *****************************************************************************/

//int Teh_MergeChar(char *pOutbuf)
//{
//   static   char acRec[1024], *pRec=NULL;
//   char     acTmp[256];
//   long     lBldgSqft, lGarSqft;
//   int      iLoop, iBeds, iFBath, iHBath, iFp, iBldgNum;
//   STDCHAR *pChar;
//
//   // Get first Char rec for first call
//   if (!pRec)
//      pRec = fgets(acRec, 1024, fdChar);
//
//   do
//   {
//      if (!pRec)
//      {
//         fclose(fdChar);
//         fdChar = NULL;
//         return 1;      // EOF
//      }
//
//      // Compare Asmt
//      iLoop = memcmp(pOutbuf, pRec, iApnLen);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
//         pRec = fgets(acRec, 1024, fdChar);
//         lCharSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 1;
//
//   pChar = (STDCHAR *)pRec;
//
//   while (!iLoop)
//   {
//      // Quality Class
//      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
//      *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
//
//      // Yrblt
//      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
//      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrBlt, SIZ_YR_BLT);
//
//      // BldgSqft
//      memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);
//      lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
//      if (lBldgSqft > 10)
//      {
//         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
//         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
//      }
//
//      // Garage Sqft
//      lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
//      if (lGarSqft > 10)
//      {
//         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
//         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
//      }
//
//      // Parking type
//      if (pChar->ParkType[0] > ' ')
//         *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
//
//      // ParkSpace
//      lGarSqft = atoin(pChar->ParkSpace, SIZ_CHAR_SIZE4);
//      if (lGarSqft > 0)
//      {
//         sprintf(acTmp, "%*d", SIZ_PARK_SPACE, lGarSqft);
//         memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
//      }
//
//      // Heating
//      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
//   
//      // Cooling 
//      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];
//
//      // Beds
//      memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);
//      iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
//      if (iBeds > 0)
//      {
//         sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
//         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
//      }
//
//      // Bath
//      memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);
//      iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
//      if (iFBath > 0)
//      {
//         sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
//         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
//      }
//
//      // Half bath
//      memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);
//      iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
//      if (iHBath > 0)
//      {
//         sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
//         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
//      }
//
//      // Total rooms
//      iBeds = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
//      if (iBeds > 0)
//      {
//         sprintf(acTmp, "%*d", SIZ_ROOMS, iBeds);
//         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
//      }
//
//      // Fireplace
//      memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);
//      iFp = atoin(pChar->Fireplace, SIZ_CHAR_SIZE2);
//      if (iFp > 0)
//      {
//         sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
//         memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
//      }
//
//      // HasSeptic or HasSewer
//      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;
//
//      // HasWell
//      *(pOutbuf+OFF_WATER) = pChar->HasWater;
//
//      // Pools
//      *(pOutbuf+OFF_POOL) = pChar->Pool[0];
//
//      // BldgNum
//      iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);
//
//      lCharMatch++;
//
//      // Get next Char rec
//      pRec = fgets(acRec, 1024, fdChar);
//      if (!pRec)
//         break;
//      iLoop = memcmp(pOutbuf, pRec, iApnLen);
//      if (!iLoop && (iBeds > 0 && iBldgNum > 0))
//         break;
//   }
//
//   return 0;
//}

/****************************** Teh_MergeStdChar ******************************
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

int Teh_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft;
   int      iLoop, iBeds, iFBath, iHBath, iFp;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Yrblt
   memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
   memcpy(pOutbuf+OFF_YR_EFF, pChar->YrBlt, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } 

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   } 

   // Parking type
   if (pChar->ParkType[0] > ' ')
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // Parking spaces
   if (pChar->ParkSpace[0] > ' ')
      *(pOutbuf+OFF_PARK_SPACE) = pChar->ParkSpace[0];

   // Heating
   if (pChar->Heating[0] > ' ')
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   
   // Cooling 
   if (pChar->Cooling[0] > ' ')
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   }

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   }

   // Total rooms
   iBeds = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iBeds);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Fireplace
   iFp = atoin(pChar->Fireplace, SIZ_CHAR_SIZE2);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   }

   // HasSeptic or HasSewer
   *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Pools
   if (pChar->Pool[0] > ' ')
      *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   lCharMatch++;

   iLoop = 0;
   do
   {
      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         break;      // EOF
      }
      iLoop++;
   } while (!memcmp(pOutbuf, pRec, iApnLen));

   memset(pOutbuf+OFF_UNITS, 32, SIZ_UNITS);
   //if (iLoop > 1)
   //{
   //   int iTmp;

   //   iTmp = sprintf(acTmp, "%d", iLoop);
   //   memcpy(pOutbuf+OFF_UNITS, acTmp, iTmp);
   //}

   return 0;
}

/**** Old version

int Teh_MergeChar(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32];
   long     lTmp, lBldgSqft, lGarSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iFp;
   MB_CHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 512, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   pChar = (MB_CHAR *)pRec;

   // Quality Class
   acCode[0] = 0;
   memcpy(acTmp, pChar->QualityClass, MBSIZ_CHAR_QUALITY);
   acTmp[MBSIZ_CHAR_QUALITY] = 0;
   if (acTmp[0] >= 'A' && acTmp[0] <= 'Z')
   {
      *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0];

      iTmp = 0;
      while (acTmp[iTmp] && !isdigit(acTmp[iTmp]))
         iTmp++;

      if (acTmp[iTmp] > '0')
         iRet = Quality2Code((char *)&acTmp[iTmp], acCode, NULL);
   } else if (acTmp[0] > '0' && acTmp[0] <= '9')
      iRet = Quality2Code(acTmp, acCode, NULL);

   blankPad(acCode, SIZ_BLDG_QUAL);
   memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);

   // Yrblt
   lTmp = atoin(pChar->YearBuilt, MBSIZ_CHAR_YRBLT);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = '2';
   } else
   {
      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = ' ';
   }

   // Heating
   int iCmp;
   if (pChar->Heating[0] > ' ')
   {
      iTmp = 0;
      iCmp = -1;
      while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(pChar->Cooling, asHeating[iTmp].acSrc, asHeating[iTmp].iLen)) > 0)
         iTmp++;

      if (!iCmp)
         *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
   }

   // Cooling
   if (pChar->Cooling[0] > ' ')
   {
      iTmp = 0;
      iCmp = -1;
      while (asCooling[iTmp].iLen > 0 && (iCmp=memcmp(pChar->Cooling, asCooling[iTmp].acSrc, asCooling[iTmp].iLen)) > 0)
         iTmp++;

      if (!iCmp)
         *(pOutbuf+OFF_AIR_COND) = asCooling[iTmp].acCode[0];
   }

   // Pool
   if (pChar->NumPools[0] > ' ')
   {
      iTmp = 0;
      iCmp = -1;
      while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(pChar->NumPools, asPool[iTmp].acSrc, asPool[iTmp].iLen)) > 0)
         iTmp++;

      if (!iCmp)
         *(pOutbuf+OFF_POOL) = asPool[iTmp].acCode[0];
   }

   // Beds
   iBeds = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Fireplace
   iFp = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   } else
      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);

   // HasSeptic or HasSewer
   if (pChar->HasSeptic > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
   else if (pChar->HasSewer > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWell;

   // Asmt Status
   if (pChar->AsmtStatus > ' ')
      *(pOutbuf+OFF_STATUS) = pChar->AsmtStatus;

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 512, fdChar);

   return 0;
}

*/

/********************************* Teh_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Teh_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse input
   replNull(pRollRec);
   if (cDelim == ',')
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Teh_MergeRoll(): bad input record for APN=%s (tokens=%d)", apTokens[iApnFld], iRet);
      return -1;
   }

   // Ignore APN starts with 800-999 except 910 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp == 555)|| (iTmp >= 800 && iTmp != 910  && iTmp != 920))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "52TEH", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }  

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   } else
   {
      memcpy(pOutbuf+OFF_CO_NUM, "52TEH", 5);

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004030039000", 9) )
   //   iTmp = 0;
#endif

   // UseCode 6/26/2016
   if (*apTokens[MB_ROLL_USECODE] > ' ')
   {
      memset(pOutbuf+OFF_USE_CO, ' ', SIZ_USE_CO);
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[MB_ROLL_USECODE], SIZ_USE_CO);

      iTmp = strlen(apTokens[MB_ROLL_USECODE]);
      if (iTmp == 4)
      {
         // Ignore the alpha
         if (acTmp[0] > '9')
            strcpy(acTmp1, apTokens[MB_ROLL_USECODE]+1);
         else
            strcpy(acTmp1, apTokens[MB_ROLL_USECODE]);
      } else
      {
         strcpy(acTmp1, apTokens[MB_ROLL_USECODE]);
         strcat(acTmp1, "  ");         // A trick to deal with sorted list and binary search
      }

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp1, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0' && *(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
   {
      memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);

      lTmp = atol(apTokens[MB_ROLL_DOCNUM]+5);
      if (pTmp && !memcmp(acTmp, apTokens[MB_ROLL_DOCNUM], 4) && lTmp < 99999)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         iTmp = sprintf(acTmp, "%.5s%.6d", apTokens[MB_ROLL_DOCNUM], lTmp);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, iTmp);
      }
   }

   // Owner
   try {
      Teh_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   } catch(...) {
      LogMsg("***** Exeception occured in Teh_MergeOwner()");
   }

   // Mailing
   try {
      Teh_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Teh_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************** Teh_Load_Roll *****************************
 *
 * Don't trust county. Sort roll file before processing (9/6/2019)
 *
 ******************************************************************************/

int Teh_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   sprintf(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") DEL(%d) ", cDelim);
   iRet = sortFile(acRollFile, acTmpFile, acRec);
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      sprintf(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"Z\") DEL(%d) ", cDelim);
   else
      sprintf(acRec, "S(#1,C,A) DEL(%d)", cDelim);
   lRet = sortFile(acSitusFile, acTmpFile, acRec);
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      sprintf(acRec, "S(#2,C,A) OMIT(1,1,C,LT,\"0\") DEL(%d) ", cDelim);
   else
      sprintf(acRec, "S(#2,C,A) DEL(%d)", cDelim);
   lRet = sortFile(acExeFile, acTmpFile, acRec);

   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      sprintf(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\") DEL(%d) ", cDelim);
   else
      sprintf(acRec, "S(#1,C,A) DEL(%d)", cDelim);
   lRet = sortFile(acExeFile, acTmpFile, acRec);

   fdTax = fopen(acTmpFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
      return -2;
   }

   // Open lien file
   fdLienExt = NULL;
   //sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (!_access(acTmpFile, 0))
   //{
   //   LogMsg("Open Lien file %s", acTmpFile);
   //   fdLienExt = fopen(acTmpFile, "r");
   //   if (fdLienExt == NULL)
   //   {
   //      LogMsg("***** Error opening lien file: %s\n", acTmpFile);
   //      return -7;
   //   }
   //}

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record
   //for (iTmp = 0; iTmp < iHdrRows; iTmp++)
   //   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (acRollRec[0] > '9' || acRollRec[0] < '0');

   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "910004173000", 9))
      //   iTmp = 0;
#endif
      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Teh_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Teh_MergeSitus(acBuf);

            // Merge Lien
            //lRet = 1;
            //if (fdLienExt)
            //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

            //// Merge Exe if not found in LienExt
            //if (fdExe && lRet)
            //   lRet = MB_MergeExe2(acBuf, 0);

            // Merge Char
            if (fdChar)
               lRet = Teh_MergeStdChar(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acBuf, 0);
               //lRet = MB_MergeTax(acBuf);

            iRollUpd++;

            // Save last recording date
            //lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
            //if (lRet > lLastRecDate && lRet < lToday)
            //   lLastRecDate = lRet;

            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         }

         // Read next roll record
         do
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
            if (pTmp && cDelim == '"')
               pTmp++;
         } while (pTmp && (memcmp(pTmp, "799", 3) > 0) && (memcmp(pTmp, "910", 3) && memcmp(pTmp, "920", 3)));

         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Teh_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Teh_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe2(acRec, 0);

            // Merge Char
            if (fdChar)
               lRet = Teh_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acRec, iHdrRows);

            // Save last recording date
            //lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            //if (lRet > lLastRecDate && lRet < lToday)
            //   lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         do
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
            if (pTmp && cDelim == '"')
               pTmp++;
         } while (pTmp && (memcmp(pTmp, "799", 3) > 0) && (memcmp(pTmp, "900", 3) < 0 ));

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Teh_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Teh_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Teh_MergeStdChar(acRec);

         // Merge Sales
         //if (fdSale)
         //   lRet = Teh_MergeSale(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, iHdrRows);

         // Save last recording date
         //lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         //if (lRet > lLastRecDate && lRet < lToday)
         //   lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      do
      {
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (pTmp && cDelim == '"')
            pTmp++;
      } while (pTmp && (memcmp(pTmp, "799", 3) > 0) && (memcmp(pTmp, "900", 3) < 0 ));

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Use GIS city:               %u", lUseGis);
   LogMsg("Use Mail city:              %u\n", lUseMailCity);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   return 0;
}

/********************************** Teh_Load_Roll *****************************
 *
 * 
 *
 ******************************************************************************/

int Teh_Load_OldLdr(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet, iTmp, iNewRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SMP");
   lRet = sortFile(acSalesFile, acTmpFile, "S(#1,C,A,#3,DAT,A) F(TXT) ");
   fdSale = fopen(acTmpFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acTmpFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "EMP");
   lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) ");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Write header record
   memset(acBuf, '9', iRecLen);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
#ifdef _DEBUG
      //if (!memcmp(acBuf, "030330020", 9) || !memcmp((char *)&acRollRec[1], "030330020", 9))
      //   iTmp = 0;
#endif

      // Create new R01 record
      iRet = Teh_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Teh_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);

         // Merge Char
         if (fdChar)
            lRet = Teh_MergeStdChar(acRec);

         // Merge Sales
         if (fdSale)
            lRet = Teh_MergeSale(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, iHdrRows);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }

      // Get next roll record
      do
      {
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (pTmp && cDelim == '"')
            pTmp++;
      } while (pTmp && (memcmp(pTmp, "799", 3) > 0) && (memcmp(pTmp, "900", 3) < 0 ));

      if (!pTmp)
         bEof = true;    // Signal to stop

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdSale)
      fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);

   lRecCnt = iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   return 0;
}

/********************************* Teh_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Teh_MergeLien1(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L1_CORTAC)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L1_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Take current year parcels only
   lTmp = atol(apTokens[L1_TAXYEAR]);
   if (lTmp != lLienYear)
      return 99;

   // Start copying data
   memcpy(pOutbuf, apTokens[L1_ASMT], strlen(apTokens[L1_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L1_FEEPARCEL], strlen(apTokens[L1_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L1_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L1_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "52TEH", 5);

   // status
   if (*apTokens[L1_STATUS] == 'C')    // completed
      *(pOutbuf+OFF_STATUS) = 'A';
   else
      *(pOutbuf+OFF_STATUS) = *apTokens[L1_STATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L1_TRA], strlen(apTokens[L1_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values - Land
   long lLand = atoi(apTokens[L1_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L1_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow = atoi(apTokens[L1_CURRENTGROWINGIMPRVALUE]);
   long lFixt   = atoi(apTokens[L1_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L1_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[L1_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L1_TAXAMT1]);
   double dTax2 = atof(apTokens[L1_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L1_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L1_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L1_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L1_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L1_EXEMPTIONCODE1], strlen(apTokens[L1_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L1_EXEMPTIONCODE2], strlen(apTokens[L1_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L1_EXEMPTIONCODE3], strlen(apTokens[L1_EXEMPTIONCODE3]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&TEH_Exemption);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "119161020", 9))
   //   iTmp = 0;
   //strcpy(acTmp, apTokens[L1_PARCELDESCRIPTION]);
#endif
   // Legal
   _strupr(apTokens[L1_PARCELDESCRIPTION]);
   iTmp = updateLegal(pOutbuf, apTokens[L1_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   strcpy(acTmp, apTokens[L1_USECODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres - do not use sizeacresfttype since it's not reliable
   dTmp = atof(apTokens[L1_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Teh_MergeOwner(pOutbuf, apTokens[L1_OWNER]);

   // Situs - Use situs file instead
   //Teh_MergeSitus(pOutbuf, apTokens[L1_SITUS1], apTokens[L1_SITUS2]);

   // Mailing
   Teh_MergeMAdr(pOutbuf, apTokens[L1_MAILADDRESS1], apTokens[L1_MAILADDRESS2], apTokens[L1_MAILADDRESS3], apTokens[L1_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L1_TAXABILITY], true, true);

   return 0;
}
int Teh_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "52TEH", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004030039000", 9))
   //   iTmp = 0;
#endif
   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
      iTmp = strlen(apTokens[L3_LANDUSE1]);
      if (iTmp == 4)
      {
         // Ignore the alpha
         if (acTmp[0] > '9')
            strcpy(acTmp1, apTokens[L3_LANDUSE1]+1);
         else
            strcpy(acTmp1, apTokens[L3_LANDUSE1]);
      } else
      {
         strcpy(acTmp1, apTokens[L3_LANDUSE1]);
         strcat(acTmp1, " ");     // A trick to deal with sorted list and binary search
      }

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp1, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   //if (*apTokens[L3_ISAGPRESERVE] == '1')
   //   *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Teh_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs - Use situs file instead
   //Teh_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Teh_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*apTokens[L3_CURRENTDOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 

   // Number of parking spaces
   if (*apTokens[L3_GARAGE] == '0' || *apTokens[L3_GARAGE] == 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'H';              // None
   else if (*apTokens[L3_GARAGE] > '0' && *apTokens[L3_GARAGE] <= '9')
   {
      iTmp = atol(apTokens[L3_GARAGE]);
      sprintf(acTmp, "%d", iTmp);
      vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
   } else
   {
      switch (*(apTokens[L3_GARAGE]))
      {
         case 'A':                                 // Attached
         case 'D':                                 // Detached
            pTmp = apTokens[L3_GARAGE]+1;
            if (*pTmp > '0' && *pTmp <= '9')
               *(pOutbuf+OFF_PARK_SPACE) = *pTmp;
            if (*(apTokens[L3_GARAGE]) == 'A')
               *(pOutbuf+OFF_PARK_TYPE) = 'I';       
            else
               *(pOutbuf+OFF_PARK_TYPE) = 'L';
            break;
         case 'C':
            *(pOutbuf+OFF_PARK_TYPE) = 'C';       // Carport
            break;
         case 'S':
            *(pOutbuf+OFF_PARK_TYPE) = 'W';       // Space
            break;
         case 'Y':
            *(pOutbuf+OFF_PARK_TYPE) = '4';       // Other ???
            break;
      }
   }

   // Garage size
   dTmp = atof(apTokens[L3_GARAGESIZE]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, dTmp);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      if (*(pOutbuf+OFF_PARK_TYPE) == ' ' || *(pOutbuf+OFF_PARK_TYPE) == 'H')
         *(pOutbuf+OFF_PARK_TYPE) = '2';           // GARAGE/CARPORT
   }

   // Total rooms
   iTmp = atol(apTokens[L3_TOTALROOMS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   return 0;
}

/********************************* Teh_Load_LDR *****************************
 *
 * Load TR601 LDR into 1900-byte record.
 *
 ****************************************************************************/

int Teh_Load_LDR(int iFirstRec /* 1=create header rec */, bool bUseValueFile)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      if (iLdrGrp == 3)
         iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");          // 2016
      else
         iRet = sortFile(acTmpFile, acRollFile, "S(#1,C,A,#12,C,A) DEL(9)");  // 2018

      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Value file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (bUseValueFile && !_access(acTmpFile, 0))
   {
      LogMsg("Open Lien file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   } else
      fdLienExt = NULL;

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
      if (cDelim == '|')
         strcat(acRec, "DEL(124) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      // Create new R01 record
      if (iLdrGrp == 1)
         iRet = Teh_MergeLien1(acBuf, acRec);         // 2018
      else
         iRet = Teh_MergeLien3(acBuf, acRec);         // 2016-2017

      if (!iRet)
      {
         // Merge value from LDR extract
         if (fdLienExt)
            lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Situs
         if (fdSitus)
            lRet = Teh_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Teh_MergeStdChar(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else if (bDebug)
         LogMsg("*** Drop record %.80s", acRec);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   } 

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Situs skiped:     %u\n", lSitusSkip);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/**************************** Teh_ConvStdChar ********************************
 *
 * New format as of 02/23/2024.  This function replaces commented version below.
 *
 *****************************************************************************/

int Teh_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], acQualCls[16], *pRec, cCharSep;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Create output file %s", acTmpFile);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Pull CHAR delimiter from INI file
   GetIniString(myCounty.acCntyCode, "CharSep", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] > ' ')
      cCharSep = acTmp[0];
   else
      cCharSep = cDelim;

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec)
         break;
      if (acBuf[1] < '0' || acBuf[1] > '9')
         continue;
      replNull(acBuf);

      if (cCharSep == '|')
         iFldCnt = ParseStringIQ(pRec, cCharSep, MAX_FLD_TOKEN, apTokens);
      else
         iFldCnt = ParseStringNQ(pRec, cCharSep, MAX_FLD_TOKEN, apTokens);

      if (iFldCnt < TEH_CHAR_HASWELL)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[TEH_CHAR_ASMT], iTrim(apTokens[TEH_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[TEH_CHAR_FEEPRCL], iTrim(apTokens[TEH_CHAR_FEEPRCL]));

      // Format APN
      if (*apTokens[TEH_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[TEH_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[TEH_CHAR_FEEPRCL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[TEH_CHAR_BLDGSEQNO]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[TEH_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - 01, 02, 03, 99
      iTmp = blankRem(apTokens[TEH_CHAR_POOLSPA]);
      if (iTmp > 1)
      {
         pRec = findXlatCode(apTokens[TEH_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[TEH_CHAR_ASMT], "071020033000", 9))
      //   iRet = 0;
#endif

      // QualityClass 
      acQualCls[0] = 0;
      strcpy(acTmp, _strupr(apTokens[TEH_CHAR_QUALITYCLASS]));
      iTmp = remChar(acTmp, ' ');    // Remove all blanks before process
      if (acTmp[0] == 'X')
      {
         if (iTmp > 4 && acTmp[iTmp-1] == 'X')
            acTmp[iTmp-1] = 0;
         strcpy(acQualCls, &acTmp[1]);
      } else if (acTmp[0] > '0')
         strcpy(acQualCls, acTmp);

      if (acQualCls[0] > ' ' && acQualCls[0] <= 'Z')
      {
         acCode[0] = ' ';
         if (isalpha(acQualCls[0])) 
         {
            myCharRec.BldgClass = acQualCls[0];

            if (isdigit(acQualCls[1]))
               iRet = Quality2Code((char *)&acQualCls[1], acCode, NULL);
            else if (isdigit(acQualCls[2]))
               iRet = Quality2Code((char *)&acQualCls[2], acCode, NULL);
            else if (!_memicmp(acTmp, "ST", 2))
               memcpy(myCharRec.QualityClass, "ST", 2);     // Steel
         } else if (isdigit(acQualCls[0]))
            iRet = Quality2Code(acQualCls, acCode, NULL);
         else if (acQualCls[0] != '?')
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", acQualCls, apTokens[TEH_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (acQualCls[0] > ' ' && acQualCls[0] != '?')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[TEH_CHAR_QUALITYCLASS], apTokens[TEH_CHAR_ASMT]);

      int iYrBlt = atoi(apTokens[TEH_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[TEH_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[TEH_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[TEH_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // Stories/NumFloors
      iTmp = atoi(apTokens[TEH_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 20)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[TEH_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[TEH_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport SF
      int iCarport = atoi(apTokens[TEH_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[TEH_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCodeA(apTokens[TEH_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[TEH_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[TEH_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[TEH_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[TEH_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[TEH_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[TEH_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[TEH_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace - 1, 2, 3, 5
      if (*(apTokens[TEH_CHAR_FIREPLACE]) > '0')
      {
         blankRem(apTokens[TEH_CHAR_FIREPLACE]);
         pRec = findXlatCode(apTokens[TEH_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      }

      // Patio SF
      iTmp = atoi(apTokens[TEH_CHAR_PATIOSF]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // Haswell - 1, 0
      blankRem(apTokens[TEH_CHAR_HASWELL]);
      if (*(apTokens[TEH_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Land Sqft
      if (iFldCnt > TEH_CHAR_LANDSQFT)
      {
         iTmp = atoi(apTokens[TEH_CHAR_LANDSQFT]);
         if (iTmp > 1)
         {
            iRet = sprintf(acTmp, "%*d", SIZ_LOT_SQFT, iTmp);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(iTmp*1000)/SQFT_PER_ACRE;
            iTmp = sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp+0.5));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      // ParkSpaces
      if (iFldCnt > TEH_CHAR_PARKSPACES)
      {
         iTmp = atoi(apTokens[TEH_CHAR_PARKSPACES]);
         if (iTmp > 1 && iTmp < 9999)
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(myCharRec.ParkSpace, acTmp, iRet);
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

//int Teh_ConvStdChar(char *pInfile)
//{
//   FILE     *fdIn, *fdOut;
//   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[4], *pRec, *pTmp;
//   int      iRet, iTmp, iFldCnt, iCnt=0;
//   STDCHAR  myCharRec;
//
//   LogMsgD("Teh_ConvStdChar - Converting char file %s", pInfile);
//   if (!(fdIn = fopen(pInfile, "r")))
//      return -1;
//
//   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   if (!(fdOut = fopen(acTmpFile, "w")))
//   {
//      fclose(fdIn);
//      LogMsg("***** Error creating output file %s", acTmpFile);
//      return -2;
//   }
//
//   // Skip first record - header
//   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
//      pRec = fgets(acBuf, 4096, fdIn);
//
//   while (!feof(fdIn))
//   {
//      pRec = fgets(acBuf, 4096, fdIn);
//
//      if (!pRec)
//         break;
//
//      // Parse string
//      replNull(acBuf);
//      replStrAll(acBuf, "NULL", "");
//      if (cDelim == '|')
//         iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
//      else
//         iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
//
//      if (iFldCnt < TEH_CHAR_SALESPRICE)
//      {
//         if (iFldCnt > 1)
//            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
//         continue;
//      }
//
//      // Event Date
//      if (*apTokens[TEH_CHAR_DOCNUM] > ' ' || *apTokens[TEH_CHAR_ASMT] == ' ')
//         continue;
//
//      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
//      memcpy(myCharRec.Apn, apTokens[TEH_CHAR_ASMT], strlen(apTokens[TEH_CHAR_ASMT]));
//      memcpy(myCharRec.FeeParcel, apTokens[TEH_CHAR_FEEPRCL], strlen(apTokens[TEH_CHAR_FEEPRCL]));
//      // Format APN
//      if (*apTokens[TEH_CHAR_ASMT] >= '0')
//      {
//         iRet = formatApn(apTokens[TEH_CHAR_ASMT], acTmp, &myCounty);
//         memcpy(myCharRec.Apn_D, acTmp, iRet);
//      } else
//      {
//         LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[TEH_CHAR_FEEPRCL]);
//         continue;
//      }
//
//      // Bldg#
//      iTmp = atoi(apTokens[TEH_CHAR_BLDGSEQNO]);
//      if (iTmp > 0 && iTmp < 100)
//      {
//         iRet = sprintf(acTmp, "%2d", iTmp);
//         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
//      } else if (iTmp >= 100)
//         LogMsg("*** BldgSeqNo too big: %d", iTmp);
//
//      // BldgSize
//      int iBldgSize = atoi(apTokens[TEH_CHAR_BUILDINGSIZE]);
//      if (iBldgSize > 100)
//      {
//         iRet = sprintf(acTmp, "%d", iBldgSize);
//         memcpy(myCharRec.BldgSqft, acTmp, iRet);
//      }
//
//      // Units Count
//      iTmp = atoi(apTokens[TEH_CHAR_UNITSCNT]);
//      //if (iTmp > 0 && iBldgSize > iTmp*100)
//      if (iTmp > 1)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.Units, acTmp, iRet);
//      } 
//      //else if (iTmp > 1)
//      //   LogMsg("*** Questionable UnitsCnt: %d, BldgSqft=%d, Apn=%s", iTmp, iBldgSize, apTokens[TEH_CHAR_ASMT]);
//
//      // Unit# - currently all 0
//      iTmp = atoi(apTokens[TEH_CHAR_UNITSEQNO]);
//      if (iTmp > 0 && iTmp < 100)
//      {
//         iRet = sprintf(acTmp, "%2d", iTmp);
//         memcpy(myCharRec.UnitSeqNo, acTmp, iRet);
//      } else if (iTmp >= 100)
//         LogMsg("*** UnitSeqNo too big: %d", iTmp);
//
//      // Rooms
//      iTmp = atoi(apTokens[TEH_CHAR_TOTALROOMS]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.Rooms, acTmp, iRet);
//      }
//
//      // Beds
//      iTmp = atoi(apTokens[TEH_CHAR_BEDROOMS]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.Beds, acTmp, iRet);
//      }
//
//      // Full baths
//      iTmp = atoi(apTokens[TEH_CHAR_BATHROOMS]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.FBaths, acTmp, iRet);
//         //memcpy(myCharRec.Bath_4Q, acTmp, iRet);
//      }
//
//      // Half bath
//      iTmp = atoi(apTokens[TEH_CHAR_HALFBATHS]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.HBaths, acTmp, iRet);
//         //memcpy(myCharRec.Bath_2Q, acTmp, iRet);
//      }
//
//      // Pool 
//      iTmp = blankRem(apTokens[TEH_CHAR_POOLSPA]);
//      if (*apTokens[TEH_CHAR_POOLSPA] > ' ')
//      {
//         pRec = findXlatCode(apTokens[TEH_CHAR_POOLSPA], &asPool[0]);
//         if (pRec)
//            myCharRec.Pool[0] = *pRec;
//      }
//
//      // QualityClass
//      iTmp = blankRem(apTokens[TEH_CHAR_QUALITYCLASS]);
//      pRec = _strupr(apTokens[TEH_CHAR_QUALITYCLASS]);
//      if (*apTokens[TEH_CHAR_QUALITYCLASS] > ' ' && *apTokens[TEH_CHAR_QUALITYCLASS] != '?')
//      {
//         iTmp = strlen(apTokens[TEH_CHAR_QUALITYCLASS]);
//         memcpy(myCharRec.QualityClass, apTokens[TEH_CHAR_QUALITYCLASS], iTmp);
//
//         acCode[0] = ' ';
//         strcpy(acTmp, apTokens[TEH_CHAR_QUALITYCLASS]);
//         if (isalpha(acTmp[0])) 
//         {
//            myCharRec.BldgClass = acTmp[0];
//
//            if (isdigit(acTmp[1]))
//               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
//            else if (isdigit(acTmp[2]))
//               iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
//            else if (!_memicmp(acTmp, "ST", 2))
//            {
//               memcpy(myCharRec.QualityClass, "ST   ", 2);     // Steel
//            } else if (!_memicmp(acTmp, "AVG", 3))
//            {
//               acCode[0] = 'A';
//               myCharRec.BldgClass = ' ';
//            } else
//            {
//               pRec = strchr(acTmp, ' ');
//               if (pRec)
//               {
//                  pRec++;
//                  if (*pRec > '0' && *pRec <= '9')
//                     iRet = Quality2Code(pRec, acCode, NULL);
//                  else
//                     LogMsg("*** Unknown QUALITYCLASS: '%s' in [%s]", apTokens[TEH_CHAR_QUALITYCLASS], apTokens[TEH_CHAR_ASMT]);
//               }
//            }
//         } else if (isdigit(acTmp[0]))
//            iRet = Quality2Code(acTmp, acCode, NULL);
//         else
//            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[TEH_CHAR_QUALITYCLASS], apTokens[TEH_CHAR_ASMT]);
//
//         if (acCode[0] > ' ')
//            myCharRec.BldgQual = acCode[0];
//      } else if (*apTokens[TEH_CHAR_QUALITYCLASS] > ' ' 
//         && *apTokens[TEH_CHAR_QUALITYCLASS] != 'U'
//         && *apTokens[TEH_CHAR_QUALITYCLASS] != '\''
//         && *apTokens[TEH_CHAR_QUALITYCLASS] != '?')
//         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[TEH_CHAR_QUALITYCLASS], apTokens[TEH_CHAR_ASMT]);
//
//      // Improved Condition
//      if (*apTokens[TEH_CHAR_CONDITION] > '0')
//      {
//         pRec = findXlatCode(apTokens[TEH_CHAR_CONDITION], &asCond[0]);
//         if (pRec)
//            myCharRec.ImprCond[0] = *pRec;
//      }
//
//      // YrBlt
//      int iYrBlt = atoi(apTokens[TEH_CHAR_YRBLT]);
//      if (iYrBlt > 1600 && iYrBlt <= lToyear)
//      {
//         iRet = sprintf(acTmp, "%d", iYrBlt);
//         memcpy(myCharRec.YrBlt, acTmp, iRet);
//      }
//
//      // YrEff
//      iTmp = atoi(apTokens[TEH_CHAR_EFFYR]);
//      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.YrEff, acTmp, iRet);
//      }
//
//      // Attached SF
//      int iAttGar = atoi(apTokens[TEH_CHAR_ATTACHGARAGESF]);
//      if (iAttGar > 100)
//      {
//         iRet = sprintf(acTmp, "%d", iAttGar);
//         memcpy(myCharRec.GarSqft, acTmp, iRet);
//         myCharRec.ParkType[0] = 'I';
//      }
//
//      // Detached SF
//      int iDetGar = atoi(apTokens[TEH_CHAR_DETACHGARAGESF]);
//      if (iDetGar > 100)
//      {
//         iRet = sprintf(acTmp, "%d", iDetGar);
//         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
//         if (myCharRec.ParkType[0] == ' ')
//         {
//            myCharRec.ParkType[0] = 'L';
//            memcpy(myCharRec.GarSqft, acTmp, iRet);
//         }
//      }
//
//      // Carport SF
//      int iCarport = atoi(apTokens[TEH_CHAR_CARPORTSF]);
//      if (iCarport > 100)
//      {
//         iRet = sprintf(acTmp, "%d", iCarport);
//         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
//         if (myCharRec.ParkType[0] == ' ')
//         {
//            myCharRec.ParkType[0] = 'C';
//            memcpy(myCharRec.GarSqft, acTmp, iRet);
//         }
//      }
//
//#ifdef _DEBUG
//      //if (!memcmp(myCharRec.Apn, "004040014000", 9))
//      //   iRet = 0;
//#endif
//      // Garage Type
//      if (*apTokens[TEH_CHAR_GARAGE] > ' ')
//      {
//         pTmp = apTokens[TEH_CHAR_GARAGE];
//         if (isalpha(*pTmp))
//         {
//            _toupper(*pTmp);
//            pRec = findXlatCodeA(pTmp, &asParkType[0]);
//            if (pRec)
//               myCharRec.ParkType[0] = *pRec;
//            pTmp++;
//         }
//         iTmp = atoi(pTmp);
//         if (iTmp > 0)
//         {
//            iRet = sprintf(acTmp, "%d", iTmp);
//            memcpy(myCharRec.ParkSpace, acTmp, iRet);
//         }
//      }
//
//      // LandSqft is not populated, use Acres (same in AMA)
//      double dAcres = atof(apTokens[TEH_CHAR_ACRES]);
//      long lLotSqft = atol(apTokens[TEH_CHAR_LANDSQFT]);
//      if (dAcres > 0.0)
//      {
//         iRet = sprintf(acTmp, "%u", (unsigned long)(dAcres*1000.0));
//         memcpy(myCharRec.LotAcre, acTmp, iRet);
//
//         iRet = sprintf(acTmp, "%u", (unsigned long)(dAcres * SQFT_PER_ACRE));
//         memcpy(myCharRec.LotSqft, acTmp, iRet);
//      } else if (lLotSqft > 100)
//      {
//         iRet = sprintf(acTmp, "%u", lLotSqft);
//         memcpy(myCharRec.LotSqft, acTmp, iRet);
//
//         if (dAcres == 0.0)
//         {
//            iRet = sprintf(acTmp, "%d", (unsigned long)(lLotSqft*SQFT_MF_1000+0.5));
//            memcpy(myCharRec.LotAcre, acTmp, iRet);
//         }
//      }
//
//      // Heating - translation table has not been verified
//      iTmp = blankRem(apTokens[TEH_CHAR_HEATING]);
//      if (iTmp > 0)
//      {
//         pRec = findXlatCode(apTokens[TEH_CHAR_HEATING], &asHeating[0]);
//         if (pRec)
//            myCharRec.Heating[0] = *pRec;
//      } 
//      
//      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
//      if (*apTokens[TEH_CHAR_COOLINGCENTRALAC] > ' ')
//         myCharRec.Cooling[0] = 'C';
//      else if (*apTokens[TEH_CHAR_COOLINGEVAPORATIVE] > ' ')
//         myCharRec.Cooling[0] = 'E';
//      else if (*apTokens[TEH_CHAR_COOLINGROOMWALL] > ' ')
//         myCharRec.Cooling[0] = 'L';
//      else if (*apTokens[TEH_CHAR_COOLINGWINDOW] > ' ')
//         myCharRec.Cooling[0] = 'W';
//
//      // FirePlace - 1, 2, 3, 5
//      if (*(apTokens[TEH_CHAR_FIREPLACE]) > '0')
//      {
//         blankRem(apTokens[TEH_CHAR_FIREPLACE]);
//         pRec = findXlatCode(apTokens[TEH_CHAR_FIREPLACE], &asFirePlace[0]);
//         if (pRec)
//            myCharRec.Misc.sExtra.FirePlaceType[0] = *pRec;
//      }
//
//      // #fireplaces - This field is temporary and may be removed 5/25/2015
//      iTmp = atol(apTokens[TEH_CHAR_NUMFIREPLACE]);
//      if (iTmp > 0 && iTmp < 9)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.Fireplace, acTmp, iRet);
//      }
//
//      // Sewer
//      blankRem(apTokens[TEH_CHAR_SEWERCODE]);
//      if (*apTokens[TEH_CHAR_SEWERCODE] > '0')
//      {
//         myCharRec.HasSewer = 'Y';
//         pRec = findXlatCode(apTokens[TEH_CHAR_SEWERCODE], &asSewer[0]);
//         if (pRec)
//            myCharRec.Sewer = *pRec;
//      }
//    
//      // Water
//      blankRem(apTokens[TEH_CHAR_HASWELL]);
//      if (*(apTokens[TEH_CHAR_HASWELL]) == 'T')
//      {
//         myCharRec.HasWell = 'Y';
//         myCharRec.HasWater = 'W';
//      //} else if (*(apTokens[TEH_CHAR_WATERSOURCE]) > '0')
//      //{
//      //   blankRem(apTokens[TEH_CHAR_WATERSOURCE]);
//      //   pRec = findXlatCodeA(apTokens[TEH_CHAR_WATERSOURCE], &asWaterSrc[0]);
//      //   if (pRec)
//      //   {
//      //      myCharRec.HasWater = *pRec;
//      //      if (*pRec == 'W')
//      //         myCharRec.HasWell = 'Y';
//      //   }
//      }
//
//      // OfficeSpaceSF
//      iTmp = atoi(apTokens[TEH_CHAR_OFFICESPACESF]);
//      if (iTmp > 10)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.Misc.sExtra.OfficeSqft, acTmp, iRet);
//      }
//
//      // NonConditionSF
//
//      // ViewCode
//
//      // Stories/NumFloors
//      iTmp = atoi(apTokens[TEH_CHAR_STORIESCNT]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d.0", iTmp);
//         memcpy(myCharRec.Stories, acTmp, iRet);
//      }
//
//      // Zoning
//      if (*apTokens[TEH_CHAR_ZONING] >= '0')
//         memcpy(myCharRec.Zoning, apTokens[TEH_CHAR_ZONING], strlen(apTokens[TEH_CHAR_ZONING]));
//
//      // SubDiv Name
//      if (*apTokens[TEH_CHAR_SUBDIVNAME] >= '0')
//      {
//         iTmp = strlen(apTokens[TEH_CHAR_SUBDIVNAME]);
//         if (iTmp > sizeof(myCharRec.Misc.SubDiv))
//            iTmp = sizeof(myCharRec.Misc.SubDiv);
//         memcpy(myCharRec.Misc.SubDiv, apTokens[TEH_CHAR_SUBDIVNAME], iTmp);
//      }
//
//      myCharRec.CRLF[0] = '\n';
//      myCharRec.CRLF[1] = '\0';
//      fputs((char *)&myCharRec.Apn[0], fdOut);
//
//      if (!(++iCnt % 1000))
//         printf("\r%u", iCnt);
//   }
//
//   if (fdIn) fclose(fdIn);
//   if (fdOut) fclose(fdOut);
//
//   LogMsg("Number of records processed: %d\n", iCnt);
//
//   // Sort output on ASMT
//   if (iCnt > 100)
//   {
//      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
//   } else
//   {
//      printf("\n");
//      iRet = 0;
//   }
//   return iRet;
//}

/*************************** Teh_ConvertApnRoll *****************************
 *
 * Convert APN format from Cres to MB.  Keep old APN in PREV_APN.
 * 
 ****************************************************************************/

int Teh_ConvertApnRoll(char *pInfile, char *pOutfile, int iRecordLen)
{
   FILE *fdIn, *fdOut;
   int   iTmp1, iTmp2, iCnt=0;
   char  acBuf[2048], acTmp[32], acApn[32];

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "rb");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "wb");

   fread(acBuf, 1, iRecordLen, fdIn);
   fwrite(acBuf, 1, iRecordLen, fdOut);

   while (!feof(fdIn))
   {
      fread(acBuf, 1, iRecordLen, fdIn);

      // Format new APN
      if (acBuf[9] == 'I')
         iTmp2 = 10*(0x0F & acBuf[8]) + 500;
      else if (acBuf[8] > '1')
         iTmp2 = atoin(&acBuf[8], 2);
      else
         iTmp2 = 0;

      iTmp1 = atoin(&acBuf[6], 2);
      sprintf(acApn, "%.6s%.3d%.3d", acBuf, iTmp1, iTmp2);

      // Save old APN to previous APN
      memcpy(&acBuf[OFF_PREV_APN], &acBuf[0], 12);

      sprintf(acTmp, "%.3s-%.3s-%.3d-%.3d", acBuf, &acBuf[3], iTmp1, iTmp2);
      memcpy(&acBuf[OFF_APN_D], acTmp, 15);

      memcpy(acBuf, acApn, 12);
      fwrite(acBuf, 1, iRecordLen, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert Roll APN completed with %d records", iCnt);

   return 0;
}

void Teh_ConvDocNum(char *pDocNum, char *pDocDate)
{
   int   iTmp;
   char sTmp[16];

   if (*pDocNum == ' ' || *(pDocNum+4) > '9')
      return;

   iTmp = atoin(pDocNum, 7);
   if (iTmp > 99999)
      return;
 
   iTmp = sprintf(sTmp, "%.4sR%.6d", pDocDate, iTmp);
   memcpy(pDocNum, sTmp, iTmp);
}

int Teh_ConvertApnSale(char *pInfile, char *pOutfile)
{
   FILE *fdIn, *fdOut;
   int   iTmp1, iTmp2, iCnt=0;
   char  acBuf[1024], acApn[32], *pBuf;
   SCSAL_REC *pSale = (SCSAL_REC *)&acBuf[0];

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");

   while (!feof(fdIn))
   {
      pBuf = fgets(acBuf, 1024, fdIn);
      if (!pBuf)
         break;

      // Format new APN
      if (acBuf[9] == 'I')
         iTmp2 = 10*(0x0F & acBuf[8]) + 500;
      else if (acBuf[8] > '1')
         iTmp2 = atoin(&acBuf[8], 2);
      else
         iTmp2 = 0;

      iTmp1 = atoin(&acBuf[6], 2);
      sprintf(acApn, "%.6s%.3d%.3d", acBuf, iTmp1, iTmp2);
      memcpy(acBuf, acApn, 12);

      // Convert DocNum
      Teh_ConvDocNum(pSale->DocNum, pSale->DocDate);

      fputs(acBuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert Sale APN completed with %d records", iCnt);

   return 0;
}

int Teh_ConvertApnCity(char *pInfile, char *pOutfile)
{
   FILE *fdIn, *fdOut;
   int   iTmp1, iTmp2, iCnt=0;
   char  acBuf[128], acOut[128], acApn[32], *pTmp;

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");

   while (!feof(fdIn))
   {
      pTmp = fgets(acBuf, 1024, fdIn);
      if (!pTmp)
         break;

      // Format new APN
      if (acBuf[10] == 'I')
         iTmp2 = 10*(0x0F & acBuf[9]) + 500;
      else if (acBuf[9] > '1')
         iTmp2 = atoin(&acBuf[9], 2);
      else
         iTmp2 = 0;

      iTmp1 = atoin(&acBuf[7], 2);
      sprintf(acApn, "%.6s%.3d%.3d", &acBuf[1], iTmp1, iTmp2);
      pTmp = strchr(acBuf, '|');
      sprintf(acOut, "\"%s\"%s", acApn, pTmp);
      fputs(acOut, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Convert APN from CityZip file completed with %d records", iCnt);

   return 0;
}


/***************************** MB_CreateSCSale *******************************
 *
 * Extract sale data from ???_Sales.csv and output to ???_SALE.DAT
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * DateFmt: 
 *    0 : Default. Auto format by checking input date
 *    MM_DD_YYYY_1 (MON)
 *    YYYY_MM_DD   (HUM)
 *
 * DocTypeFmt: 
 *    0 : Default. Set DocType=1 (GD) if DocCode=1 or sale price > 0
 *    1 : AMA, BUT, MAD, SON, SBT
 *    2 : PLA
 *    3 : SHA, STA
 *
 * DocNumFmt: 
 *    0 : Default. Copy DocNum as is
 *    1 : Format DocNum second part of DocNum to 7 digits (i.e. 2010R1234 = 2010R0001234)
 *    2 : Format DocNum second part of DocNum to n digits (i.e. 2010R0034 = 2010R34) (COL)
 *    3 : Remove all nonnumeric after 5th character and format to 6 digits (SON)
 *    4 : Format to yyyyR9999999 if R is in pos 4 or 5 of original DocNum (SBT)
 *    5 : TEH
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Teh_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp;

   LogMsg0("Extract Sale file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   do {
      lTmp = ftell(fdSale);
      pTmp = fgets(acRec, 1024, fdSale);
   } while (pTmp && !isdigit(*pTmp));
   
   if (fseek(fdSale, lTmp, SEEK_SET))
   {
      LogMsg("***** Error setting current position in sale file using fseek()");
      fclose(fdSale);
      return -2;
   }

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      replNull(acRec);
      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || 
         *apTokens[MB_SALES_DOCDATE] == ' ' || 
         *(apTokens[MB_SALES_DOCNUM]+4) != 'R' )
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "052202011000", 9))
      //   iTmp = 0;
#endif
      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
      {
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }

      // Docnum
      if (!iDocNumFmt)
      {
         // MON, NAP, PLA, SIS, SHA, LAK, MNO, YUB
         memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));
      } else if (iDocNumFmt == 1)
      {  // AMA, BUT, MAD
         if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R' && strchr(apTokens[MB_SALES_DOCNUM]+5, '-'))
         {
            memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));
         } else
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
            if (lTmp > 0)
            {
               sprintf(acTmp, "%.5s%0.7ld", apTokens[MB_SALES_DOCNUM], lTmp);
               memcpy(SaleRec.DocNum, acTmp, SALE_SIZ_DOCNUM);
            }
         }
      } else if (iDocNumFmt == 2)
      {  // COL
         if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R')
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%.5s%d", apTokens[MB_SALES_DOCNUM], lTmp);
               memcpy(SaleRec.DocNum, acTmp, iTmp);
            }
         }
      } else if (iDocNumFmt == 3)
      {  // SON
         iTmp = replNonNum(apTokens[MB_SALES_DOCNUM]+5, ' ');

         lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%.5s%0.6ld   ", apTokens[MB_SALES_DOCNUM], lTmp);
            memcpy(SaleRec.DocNum, acTmp, SALE_SIZ_DOCNUM);
         }
      } else if (iDocNumFmt == 4)
      {  // SBT
         if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R')
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%.5s%0.7d", apTokens[MB_SALES_DOCNUM], lTmp);
               memcpy(SaleRec.DocNum, acTmp, iTmp);
            }
         } else if (*(apTokens[MB_SALES_DOCNUM]+5) == 'R')
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+6, 6);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%.4sR%0.7d", SaleRec.DocDate, lTmp);
               memcpy(SaleRec.DocNum, acTmp, iTmp);
            }
         }
      } else if (iDocNumFmt == 5)
      {  // TEH
         lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
         if (lTmp > 0 && lTmp < 999999)
         {
            sprintf(acTmp, "%.5s%0.6ld   ", apTokens[MB_SALES_DOCNUM], lTmp);
            memcpy(SaleRec.DocNum, acTmp, SALE_SIZ_DOCNUM);
         }
      }

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1' || *apTokens[MB_SALES_GROUPSALE] == 'T')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001130011000", 9))
      //   iTmp = 0;
#endif
      // Confirmed sale price
      dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
      if (acTmp[0] > '0')
      {
         lPrice = atol(acTmp);
         if (lPrice < 1000)
         {
            // This doesn't look normal, but keep it for reference only
            iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            lPrice = 0;
         } else
            iTmp = sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      } 

      // Do not use confirmed sale price
      if (!bUseConfSalePrice)
         lPrice = 0;

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001160016000", 9))
      //   iTmp = 0;
#endif
      // Tax
      dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
      if (acTmp[0] > '0')
      {
         dTmp = atof(acTmp);

         // Save DocTax
         iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);

         // Calculate sale price
         lTmp = (long)(dTmp * SALE_FACTOR);
         iTmp = ((int)dTmp/100)*100;

         // Check for bad DocTax
         if (dTmp > 100000)
         {
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else if (lPrice > 100000 && lPrice == (lPrice/100)*100)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTmp);
            } else
            {
               LogMsg("??? (1) Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f, CP=%d.  Need investigation.  Ignore price.", SaleRec.Apn, SaleRec.DocNum, dTmp, lPrice);
               lPrice = 0;
            }
         } else if (lPrice > 0 && (long)dTmp >= lPrice+5000)
         {
            // Even though sale tax is questionable, we still rely on it if it is reasonable
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** (2) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               if (lPrice == (lPrice/100)*100)
                  LogMsg("*** (2) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTmp);
               else
               {
                  LogMsg("??? (2) Questionable Sale Tax Amt for %.12s: DocNum=%.12s, Price=%d, \ttax=%.2f).  Need investigation.", SaleRec.Apn, SaleRec.DocNum, lPrice, dTmp);
                  lPrice = lTmp;
               }
            }
         } else if (iTmp == (int)dTmp && iTmp > 10000)
         {
            if (lTmp != (lTmp/100)*100)
            {
               LogMsg("*** (3) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
               lPrice = lTmp;
         } else if (lTmp == (lTmp/100)*100)
            lPrice = lTmp;
         else if (lTmp > 1000 && lPrice == 0)
            lPrice = lTmp;

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Ignore sale price.", 
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE]);
               lPrice = 0;
            }
         }  
      } 

      // Ignore sale price if less than 1000
      if (lPrice >= 10000)
         sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
      else if (lPrice >= 1000)
         sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
      else
         memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
      memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);

      // Doc code - accept following code only
      int iDocCode = 0;
      if (isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         if (pDocTbl)
         {
            iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
            if (iTmp >= 0)
            {
               memcpy(SaleRec.DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
               if (lPrice < 1000)
                  SaleRec.NoneSale_Flg = pDocTbl[iTmp].flag;
            } else if (bDebug)
               LogMsg("*** Unknown DocCode: %s", apTokens[MB_SALES_DOCCODE]);
         } else
         {
            iDocCode = atoi(apTokens[MB_SALES_DOCCODE]);
            if (iDocTypeFmt == 1)      // AMA, BUT, MAD
            {
               if (iDocCode ==1 || iDocCode == 12 || (iDocCode == 8 && lPrice > 0))
                  SaleRec.DocType[0] = '1';
               else if (iDocCode == 10)
                  memcpy(SaleRec.DocType, "75", 2);
               else 
               {
                  SaleRec.NoneSale_Flg = 'Y';
                  if (iDocCode == 52)                        // Transfer - Default
                     memcpy(SaleRec.DocType, "52", 2);
                  else if (iDocCode == 2 || iDocCode == 4)       // Transfer
                     memcpy(SaleRec.DocType, "75", 2);
                  else if (iDocCode == 5 || iDocCode == 6)       // Partial Transfer
                     memcpy(SaleRec.DocType, "75", 2);
               }
            } else if (iDocTypeFmt == 2)  // PLA
            {
               switch (iDocCode)
               {
                  case 1:  // GD, Transfer reappr
                  case 5:  // New mobile home                  
                     SaleRec.DocType[0] = '1';
                     break;
                  case 2:  // Partial Transfer
                  case 10: // Timeshare - reappr
                  case 11: // Transfer - no reappr
                  case 12: // Partial Transfer - no reappr
                  case 15: // Strawman transfer
                     memcpy(SaleRec.DocType, "75", 2);
                     break;
                  case 3:
                     // Foreclosure
                     memcpy(SaleRec.DocType, "77", 2);
                     break;
                  case 4:
                     // Sheriff's deed
                     memcpy(SaleRec.DocType, "25", 2);
                     break;
                  case 8:
                     // Tax deed
                     memcpy(SaleRec.DocType, "67", 2);
                     break;
                  default:
                     break;
               }
            } else if (iDocTypeFmt == 3)  // SHA
            {
               switch (iDocCode)
               {
                  case 1:  // GD, Transfer reappr
                     SaleRec.DocType[0] = '1';
                     break;
                  case 2:  // Partial Transfer
                     memcpy(SaleRec.DocType, "57", 2);
                     break;
                  case 3:  // Non-Reappraisal event                  
                  case 5:  // Intermarrital
                  case 6:  // Vesting/Name change
                  case 11: // Add/Delete JT
                     SaleRec.NoneSale_Flg = 'Y';
                     break;
                  case 10:
                     // Tax deed
                     memcpy(SaleRec.DocType, "67", 2);
                     break;
                  case 19:
                     // Foreclosure
                     memcpy(SaleRec.DocType, "77", 2);
                     break;
                  case 60:
                     // Split/Combine
                     if (lPrice > 0)
                        SaleRec.DocType[0] = '1';
                     break;
                  default:
                     if (!lPrice)
                        SaleRec.NoneSale_Flg = 'Y';
                     break;
               }
            } else if (iDocTypeFmt == 4)  // SON
            {
               if (iDocCode ==1 || (lPrice > 0 && isdigit(SaleRec.DocNum[5])))
                  SaleRec.DocType[0] = '1';
               else 
               {
                  if (iDocCode == 4)                           // Transfer
                  {
                     SaleRec.NoneSale_Flg = 'Y';
                     memcpy(SaleRec.DocType, "75", 2);
                  } else if (iDocCode == 5 || iDocCode == 6)   // Internal Doc
                  {
                     SaleRec.NoneSale_Flg = 'Y';
                     memcpy(SaleRec.DocType, "74", 2);
                  }
               }
            } else if (iDocCode == 1 || lPrice > 0)
               SaleRec.DocType[0] = '1';
         }
      } else if (!memcmp(apTokens[MB_SALES_DOCCODE], "GD", 2))
         SaleRec.DocType[0] = '1';

      // Save original DocCode
      iTmp = strlen(apTokens[MB_SALES_DOCCODE]);
      if (iTmp > SALE_SIZ_DOCCODE)
         iTmp = SALE_SIZ_DOCCODE;
      memcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], iTmp);

      // Transfer Type
      if (*apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         strcpy(acTmp, apTokens[MB_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         if (iTmp > SALE_SIZ_SELLER)
            iTmp = SALE_SIZ_SELLER;
         memcpy(SaleRec.Seller1, acTmp, iTmp);

         // Buyer
         strcpy(acTmp, apTokens[MB_SALES_BUYER]);
         iTmp = blankRem(acTmp);
         if (iTmp > SALE_SIZ_BUYER)
            iTmp = SALE_SIZ_BUYER;
         memcpy(SaleRec.Name1, acTmp, iTmp);

         // Skip if DocNum not available
         if (SaleRec.DocNum[0] > ' ')
         {
            SaleRec.CRLF[0] = 10;
            SaleRec.CRLF[1] = 0;
            fputs((char *)&SaleRec,fdOut);
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(27,2,C,EQ,\"  \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -2;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            // Save old cumsale file
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);
            iTmp = rename(acCSalFile, acTmpFile);
   
            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -2;
      } else
         iTmp = rename(acOutFile, acCSalFile);

      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acCSalFile, acTmpFile);
      }
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp == -2)
      LogMsg("***** Error sorting output file");
   else if (iTmp)
      LogMsg("***** Error renaming to %s", acCSalFile);

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                          output: %d.", lTmp);
   LogMsg("         Latetest recording date: %d.", lLastRecDate);
   return iTmp;
}

/*********************************** loadTeh ********************************
 *
 * Options:
 *
 *    -CTEH -U -Xsi -T -O [-Ua]   (Normal update)
 *    -CTEH -L -Xl -Ms|Xsi [-La] [-Up] -O (LDR)
 *
 ****************************************************************************/

int loadTeh(int iSkip)
{
   int   iRet=0;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;
   iApnLen = myCounty.iApnLen;

   //char sInfile[_MAX_PATH], sOutfile[_MAX_PATH];
   //sprintf(sInfile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Org");
   //sprintf(sOutfile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   //iRet = Teh_ConvertApnSale(sInfile, sOutfile);
   //sprintf(sInfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   //sprintf(sOutfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   //iRet = Teh_ConvertApnRoll(sInfile, sOutfile, 1900);
   //strcpy(sInfile, "H:\\CO_PROD\\STEH_CD\\raw\\teh_cityzip.csv");
   //strcpy(sOutfile, "H:\\CO_PROD\\STEH_CD\\raw\\teh_cityzip.srt");
   //iRet = Teh_ConvertApnCity(sInfile, sOutfile);

   if (iLoadTax == TAX_LOADING)              // -T or -Lt
   {
      // Load tax base
      iRet = MB_Load_TaxBase(bTaxImport, true, 2, 0);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet |= MB_Load_TaxCodeMstr(bTaxImport, 0);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport, 0);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport, 0);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
      if (!iLoadFlag)
         return iRet;
   }

   // Extract lien file - LDR2016
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acValueFile, 0))
      {
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acValueFile, 0);      // 2016 L3
      } else
         iRet = MB_ExtrTR601(myCounty.acCntyCode);                      // 2018 L1
   }

   // Extract Sale file from Teh_Sales.txt to Teh_Sale.sls
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Do not use confirmed sale price
      //bUseConfSalePrice = false;

      // Doc date - input format MM/DD/YYYY - MM_DD_YYYY_1
      //          - changed to YYYY_MM_DD from 02/10/2021 (can be set in INI file)
      if (iSaleDateFmt > 0)
         iRet = Teh_CreateSCSale(iSaleDateFmt, 3, 5, true, &TEH_DocCode[0]);
      else
         iRet = Teh_CreateSCSale(MM_DD_YYYY_1, 3, 5, true, &TEH_DocCode[0]);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Check file size, skip if it is too small
      if ((iRet = getFileSize(acCharFile)) > 100000)
      {
         iRet = Teh_ConvStdChar(acCharFile);
         if (iRet <= 0)
         {
            LogMsg("***** Error extracting attributes data from %s", acCharFile);
            return -1;
         }
      } else if (iRet < 0)
         LogMsg0("***** Missing CHAR file %s.  Old file will be used.", acCharFile);
      else
         LogMsg0("***** Bad CHAR file %s (size=%d).  Please check this out.", acCharFile, iRet);
   }

   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      // Open CityZip file (Teh_CityZip.srt) - City & Zip extracted from GIS
      iRet = initSitus(acIniFile, myCounty.acCntyCode);
      if (iRet < 0)
         return iRet;

      // Load Zip2City file (Teh_Zip2City.srt) - translate to City name from known zip code
      iRet = loadZip2City(acIniFile, myCounty.acCntyCode);

      if (iLoadFlag & LOAD_LIEN)                   // -L
      {
         // Create Lien file
         LogMsg0("Load %s Lien file", myCounty.acCntyCode);
         iRet = Teh_Load_LDR(iSkip, false);

      } else if (iLoadFlag & LOAD_UPDT)            // -U
      {
         LogMsg0("Load %s roll update file", myCounty.acCntyCode);
         iRet = Teh_Load_Roll(iSkip);
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Teh_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
   }

    // Merge zoning
   //if (!iRet && (iLoadFlag & MERG_ZONE) )          // -Mz
   //   iRet = MergeZoning(myCounty.acCntyCode, iSkip);

   if (bUpdPrevApn)                                // -Up
   {
      // If not defined, use current apn length
      iRet = GetPrivateProfileInt(myCounty.acCntyCode, "PrevApnLen", iApnLen, acIniFile);
      iRet = updatePrevApn(myCounty.acCntyCode, iRet, iSkip);
   }

   return iRet;
}
