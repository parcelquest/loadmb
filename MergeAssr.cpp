/**************************************************************************
 *
 * Revision
 * 09/01/2005  1.0.0    First version by Sony Nguyen
 *                      Adding last known sale fields (price, date, doc#, doctype)
 *                      Sale_Exp.dat will be added by MergeAdr to G01 file
 *
 * 10/17/2006           TCB - Added EXCPAMT = (SALEAMT - GROSS), IF (SALEAMT > GROSS)
 * 11/28/2006 1.7.4     TCB - Add IVALSQFT and fix bug in Assr_MergeChar() by
 *                      removing check for !lCharMatch.
 * 04/10/2007 1.9.3     SPN - Use bFmtDoc to determine whether to format DocNum or 
 *                      keep it as is.
 * 08/10/2007 1.9.13    Add function Asr_MergeAppr() for MON.
 * 08/14/2007 1.9.14    Fix up to avoid buffer overrun.
 * 06/08/2009 8.7.7     Add support multi-row header and various dilimeter.
 * 06/09/2009 8.7.8     Fix Asr_MergeSale().  If SkipQuote = 0, remove all quotes.
 * 02/19/2010 9.4.0     Modify Asr_MergeTax() to process different tax file formats.
 * 09/01/2011 11.3.2    Add Asr_MergeStdChar() to merge STDCHAR into asr output.
 *                      Modify MB_CreateAssr() to work with Asr_MergeStdChar and use it
 *                      in MergeSha.cpp. This can be used for LAK & AMA.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "doSort.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "Tables.h"
#include "SaleRec.h"
#include "doOwner.h"
#include "formatApn.h"
#include "LoadMB.h"
#include "MergeBut.h"
#include "MergeAssr.h"
#include "MergeMon.h"
#include "MBExtrn.h"
#include "CharRec.h"

FILE     *fdAppr;
bool     bFixedApn, bFmtDoc=false;
long     lBkPgNoAppr;

/******************************** Asr_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Asr_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], acSave[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64];
   OWNER myOwner;

   // Clear output buffer if needed
   memset(pOutbuf+OFF_3K_MSGNAME1, ' ', (OFF_3K_CLAND)-(OFF_3K_MSGNAME1));

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acName2[0] = 0;
   acSave[0] = 0;
   acSave1[0] = 0;

   if (pTmp=strstr(acTmp, " 1/2 INT"))
      *pTmp = 0;

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // Save it - Only do it for individual trust
   if (acTmp[iTmp] && !strchr(acTmp, '&'))
   {
      iTmp--;
      strcpy(acSave, (char *)&acTmp[iTmp]);
      acTmp[iTmp] = 0;
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
      if ((pTmp=strstr(acName2, " ETAL")) || (pTmp=strstr(acName2, " ET AL")) )
         *pTmp = 0;
   } else if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Filter out words
   if ((pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " FAMILY TRUST &"))
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      *(pTmp+13) = 0;
      strcpy(acSave1, acTmp);

      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST &"))
   {  // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      *(pTmp+13) = 0;
      strcpy(acSave1, acTmp);

      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
         *(pTmp+6) = 0;
      else
         *pTmp = 0;        // Drop TRUST only if not family trust

      strcpy(acName1, acTmp);
      pTmp1 = pTmp+9;
      iTmp1 = strlen(pTmp1);
      for (iTmp = 0; iTmp < iTmp1; iTmp++)
      {
         if (acName1[iTmp] != *pTmp1)
            break;
         pTmp1++;
      }

      // Skip first word after & if it is the same as last name
      if (iTmp > 2 && *(pTmp1-1) == ' ')
      {
         strcat(acName1, " & ");
         // Search and remove TRUST in second part of name
         if (pTmp=strstr(pTmp1, " TRUST"))
            *pTmp = 0;
         strcat(acName1, pTmp1);
      } else
      {
         strcpy(acName2, pTmp+9);
      }
   } else if ((pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " FAMILY "))
             || (pTmp=strstr(acTmp, " INCOME TR")) || (pTmp=strstr(acTmp, " LIVING TR"))
             || (pTmp=strstr(acTmp, " LAND TRUST")) || (pTmp=strstr(acTmp, " FAM TRUST")))
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUSTE"))
   {  // Drop TRUSTEE
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (acSave[0])
      {
         strcpy(acTmp1, acSave);
         strcpy(acSave, pTmp);
         strcat(acSave, acTmp1);
      } else
      {
         if (isdigit(*(pTmp-1)) )
            while (isdigit(*(--pTmp)));

         strcpy(acSave, pTmp);
      }
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " ESTATE OF"))
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
         strcpy(acName2, pTmp);
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " SUCCESSOR")) )
      *pTmp = 0;
   if ((pTmp=strstr(acName2, " AS ")) || (pTmp=strstr(acName2, " SUCCESSOR")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(acTmp1, myOwner.acName1);

      // Concat what in saved buffer if it is not " TRUST"
      if (acSave[0] && strcmp(acSave, " TRUST"))
      {
         if (acTmp1[strlen(acTmp1)-1] == ' ')
            strcat(acTmp1, (char *)&acSave[1]);      // avoid double blank
         else
            strcat(acTmp1, acSave);
      }

      // Save Name1
      iTmp = strlen(acTmp1);
      if (iTmp > SIZ_3K_MSGNAME1)
         iTmp = SIZ_3K_MSGNAME1;
      memcpy(pOutbuf+OFF_3K_MSGNAME1, acTmp1, iTmp);

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
      {
         iTmp = strlen(acTmp1);
         if (iTmp > SIZ_3K_SWAPNAME1)
            iTmp = SIZ_3K_SWAPNAME1;
         memcpy(pOutbuf+OFF_3K_SWAPNAME1, acTmp1, iTmp);
      } else
      {
         iTmp = strlen(myOwner.acSwapName);
         if (iTmp > SIZ_3K_SWAPNAME1)
            iTmp = SIZ_3K_SWAPNAME1;
         memcpy(pOutbuf+OFF_3K_SWAPNAME1, myOwner.acSwapName, iTmp);
      }
   } else if (acSave1[0])
   {
      iTmp = strlen(acSave1);
      if (iTmp > SIZ_3K_MSGNAME1)
         iTmp = SIZ_3K_MSGNAME1;
      memcpy(pOutbuf+OFF_3K_MSGNAME1, acSave1, iTmp);

      iTmp = strlen(acSave1);
      if (iTmp > SIZ_3K_SWAPNAME1)
         iTmp = SIZ_3K_SWAPNAME1;
      memcpy(pOutbuf+OFF_3K_SWAPNAME1, acSave1, iTmp);
   } else
   {
      if (acSave[0])
      {
         strcat(acName1, acSave);
         acSave[0] = 0;
         iTmp = strlen(acName1);
         if (iTmp > SIZ_3K_MSGNAME1)
            iTmp = SIZ_3K_MSGNAME1;
         memcpy(pOutbuf+OFF_3K_MSGNAME1, acName1, iTmp);

         iTmp = strlen(acName1);
         if (iTmp > SIZ_3K_SWAPNAME1)
            iTmp = SIZ_3K_SWAPNAME1;
         memcpy(pOutbuf+OFF_3K_SWAPNAME1, acName1, iTmp);
      } else
      {
         // Couldn't split names
         iTmp = strlen(pNames);
         if (iTmp > SIZ_3K_MSGNAME1)
            iTmp = SIZ_3K_MSGNAME1;
         memcpy(pOutbuf+OFF_3K_MSGNAME1, pNames, iTmp);

         iTmp = strlen(pNames);
         if (iTmp > SIZ_3K_SWAPNAME1)
            iTmp = SIZ_3K_SWAPNAME1;
         memcpy(pOutbuf+OFF_3K_SWAPNAME1, pNames, iTmp);
      }
   }
}

/***********************************************************************/

void ShaAsr_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64];
   OWNER myOwner;

   // Clear output buffer if needed
   memset(pOutbuf+OFF_3K_MSGNAME1, ' ', (OFF_3K_CLAND)-(OFF_3K_MSGNAME1));

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acName2[0] = 0;
   acSave[0] = 0;

   if (pTmp=strstr(acTmp, " 1/2 INT"))
      *pTmp = 0;

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp || strstr(acTmp, "SHASTA")  )
   {
      iTmp1 = strlen(acTmp);
      if (iTmp1 > SIZ_3K_MSGNAME1) iTmp1 = SIZ_3K_MSGNAME1;
      memcpy(pOutbuf+OFF_3K_MSGNAME1, acTmp, iTmp1);
      memcpy(pOutbuf+OFF_3K_SWAPNAME1, acTmp, iTmp1);
      return;
   }

   // Save it - Only do it for individual trust
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
      iTmp--;
      strcpy(acSave, (char *)&acTmp[iTmp]);
      acTmp[iTmp] = 0;
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
      if ((pTmp=strstr(acName2, " ETAL")) || (pTmp=strstr(acName2, " ET AL")) )
         *pTmp = 0;
   } else if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Filter out words
   // Filter out words, things in parenthesis
   if ( (pTmp = strchr(acTmp, '('))      || (pTmp=strstr(acTmp, " CO TR"))   || 
        (pTmp=strstr(acTmp, " CO-TR"))   || (pTmp=strstr(acTmp, " COTR"))    ||
        (pTmp=strstr(acTmp, " TRUSTE")) || (pTmp=strstr(acTmp, " TR ETAL")) || 
        (pTmp=strstr(acTmp, " TTEE"))    || (pTmp=strstr(acTmp, " TRES"))    || 
        (pTmp=strstr(acTmp, " ETAL"))    || (pTmp=strstr(acTmp, " ET AL"))  )
      *pTmp = 0;

   // Filter some more
   pTmp = (char *)&acTmp[strlen(acTmp)-4];
   if (!memcmp(pTmp, " DVA", 4) || !memcmp(pTmp, " LIV", 4) )
      *pTmp = 0;

   // If there is number goes before REV, keep it.
   if (!memcmp(pTmp, " REV", 4))
   {
      pTmp1 = pTmp;
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      if (pTmp < pTmp1)
         strcpy(acSave, pTmp);
      *pTmp = 0;
   }

   // Trim trailing number
   iTmp = strlen(acTmp)-1;
   while (iTmp > 0 && isdigit(acTmp[iTmp]))
      acTmp[iTmp--] = 0;

   // Check vesting
   pTmp = (char *)&acTmp[strlen(acTmp)-3];
   if (!memcmp(pTmp, " JT", 3) || !memcmp(pTmp, " CP", 3) || !memcmp(pTmp, " SP", 3) )
      *pTmp = 0;
   pTmp = (char *)&acTmp[strlen(acTmp)-6];
   if (!memcmp(pTmp, " SP JR", 6) )
      strcpy(pTmp, " JR");

   if ((pTmp=strstr(acTmp, " FAM TR")) || (pTmp=strstr(acTmp, " FAMILY ")) || 
      (pTmp=strstr(acTmp, " REVOC"))          || (pTmp=strstr(acTmp, " REV TR")) ||
      (pTmp=strstr(acTmp, " REV LIV TR"))     || (pTmp=strstr(acTmp, " REV LIVING")) ||       
      (pTmp=strstr(acTmp, " LIV TRUST"))      || (pTmp=strstr(acTmp, " LIVING ")) ||
      (pTmp=strstr(acTmp, " INCOME TR"))      || (pTmp=strstr(acTmp, " 1992 REV")) 
      )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (acSave[0])
      {
         strcpy(acTmp1, acSave);
         strcpy(acSave, pTmp);
         strcat(acSave, acTmp1);
      } else
      {
         if (isdigit(*(pTmp-1)) )
            while (isdigit(*(--pTmp)));

         strcpy(acSave, pTmp);
      }
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ( (pTmp=strstr(acTmp, " ESTATE OF")) || (pTmp=strstr(acTmp, " EST OF")) ||
               (pTmp=strstr(acTmp, " ESTS OF")) )
   {  // MONDANI NELLIE M ESTATE OF       
      strcpy(acSave, " ESTATE OF");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ( (pTmp=strstr(acTmp, " LIFE EST")) || (pTmp=strstr(acTmp, " LIFE ESTATE")) )
   {  // MONDANI NELLIE M ESTATE OF
      strcpy(acSave, " LIFE EST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
         strcpy(acName2, pTmp);
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;
   if ((pTmp=strstr(acName2, " AS ")) || (pTmp=strstr(acName2, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(acTmp1, myOwner.acName1);

      // Concat what in saved buffer if it is not " TRUST"
      if (acSave[0])
      {
         if (acTmp1[strlen(acTmp1)-1] == ' ')
            strcat(acTmp1, (char *)&acSave[1]);      // avoid double blank
         else
            strcat(acTmp1, acSave);
      }

      // Save Name1
      iTmp = strlen(acTmp1);
      if (iTmp > SIZ_3K_MSGNAME1)
         iTmp = SIZ_3K_MSGNAME1;
      memcpy(pOutbuf+OFF_3K_MSGNAME1, acTmp1, iTmp);

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
      {
         iTmp = strlen(acTmp1);
         if (iTmp > SIZ_3K_SWAPNAME1)
            iTmp = SIZ_3K_SWAPNAME1;
         memcpy(pOutbuf+OFF_3K_SWAPNAME1, acTmp1, iTmp);
      } else
      {
         iTmp = strlen(myOwner.acSwapName);
         if (iTmp > SIZ_3K_SWAPNAME1)
            iTmp = SIZ_3K_SWAPNAME1;
         memcpy(pOutbuf+OFF_3K_SWAPNAME1, myOwner.acSwapName, iTmp);
      }
   } else
   {
      if (acSave[0])
      {
         strcat(acName1, acSave);
         acSave[0] = 0;
         iTmp = strlen(acName1);
         if (iTmp > SIZ_3K_MSGNAME1)
            iTmp = SIZ_3K_MSGNAME1;
         memcpy(pOutbuf+OFF_3K_MSGNAME1, acName1, iTmp);

         iTmp = strlen(acName1);
         if (iTmp > SIZ_3K_SWAPNAME1)
            iTmp = SIZ_3K_SWAPNAME1;
         memcpy(pOutbuf+OFF_3K_SWAPNAME1, acName1, iTmp);
      } else
      {
         // Couldn't split names
         iTmp = strlen(pNames);
         if (iTmp > SIZ_3K_MSGNAME1)
            iTmp = SIZ_3K_MSGNAME1;
         memcpy(pOutbuf+OFF_3K_MSGNAME1, pNames, iTmp);

         iTmp = strlen(pNames);
         if (iTmp > SIZ_3K_SWAPNAME1)
            iTmp = SIZ_3K_SWAPNAME1;
         memcpy(pOutbuf+OFF_3K_SWAPNAME1, pNames, iTmp);
      }
   }
}

/******************************** Asr_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Asr_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[256];
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear buffer
   //memset(pOutbuf+OFF_3K_FM_STRNO, ' ', (OFF_3K_ADRFLG)-(OFF_3K_FM_STRNO));
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf+OFF_3K_ASSMT, "035224008", 9))
   //   iTmp = 0;
#endif

   // Mail address
   if (!memcmp(apTokens[MB_ROLL_M_ADDR], "        ", 8))
   {
      memcpy(pOutbuf+OFF_3K_FM_STR, "NO MAIL ADDR ON FILE", 20);
      return;
   }

   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);

   // Parse mail address
   try {
      parseMAdr1(&sMailAdr, acAddr1);
   } catch(...)
   {
      LogMsg("***** Error in parseMAdr1() %.12s at %s\n", pOutbuf+OFF_3K_ASSMT, acAddr1);
   }

   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_FM_STRNO, sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_3K_FM_STRNO, acTmp, SIZ_3K_FM_STRNO);
      memcpy(pOutbuf+OFF_3K_FM_FRA, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_3K_FM_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_3K_FM_STR, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_3K_FM_SFX, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_3K_FM_UNIT, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] >= 'A')
   {
      iTmp = strlen(apTokens[MB_ROLL_M_CITY]);
      if (iTmp > SIZ_3K_FM_CITY)
         iTmp = SIZ_3K_FM_CITY;
      memcpy(pOutbuf+OFF_3K_FM_CITY, apTokens[MB_ROLL_M_CITY], strlen(apTokens[MB_ROLL_M_CITY]));
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_3K_FM_ST, apTokens[MB_ROLL_M_ST], 2);
      else
         memcpy(pOutbuf+OFF_3K_FM_ST, BLANK32, 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         iTmp = strlen(apTokens[MB_ROLL_M_ZIP]);
         if (iTmp > 9)
            iTmp = 9;
         memcpy(pOutbuf+OFF_3K_FM_ZIP, apTokens[MB_ROLL_M_ZIP], iTmp);
      }
   }

}

/******************************** Asr_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Asr_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   int      iRet=0, iTmp;
   char     acTmp[256], acCode[32], *pTmp;
   long     lTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSitus);
      // Get first rec
      pRec = fgets(acRec, 512, fdSitus);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Match correct APN
      iTmp = memcmp(pOutbuf+5, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   if (pRec)
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old Situs
   memset(pOutbuf+OFF_3K_S_STR, ' ', (OFF_3K_LAND)-(OFF_3K_S_STR));
   memset(pOutbuf+OFF_3K_FS_STRNO, ' ', (OFF_3K_FM_STRNO)-(OFF_3K_FS_STRNO));

   // Merge data
   memcpy(pOutbuf+OFF_3K_S_STR, apTokens[MB_SITUS_STRNAME], strlen(apTokens[MB_SITUS_STRNAME]));
   memcpy(pOutbuf+OFF_3K_S_STRNUM, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));
   memcpy(pOutbuf+OFF_3K_S_SFX, apTokens[MB_SITUS_STRTYPE], strlen(apTokens[MB_SITUS_STRTYPE]));
   memcpy(pOutbuf+OFF_3K_S_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
   memcpy(pOutbuf+OFF_3K_S_UNIT, apTokens[MB_SITUS_UNIT], strlen(apTokens[MB_SITUS_UNIT]));
   memcpy(pOutbuf+OFF_3K_S_COMM, apTokens[MB_SITUS_COMMUNITY], strlen(apTokens[MB_SITUS_COMMUNITY]));
   
   // Situs Zip - not avail in BUT
   memcpy(pOutbuf+OFF_3K_S_ZIP, apTokens[MB_SITUS_ZIP], strlen(apTokens[MB_SITUS_ZIP]));

   // Format Situs
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_FS_STRNO, lTmp);
      memcpy(pOutbuf+OFF_3K_FS_STRNO, acTmp, SIZ_3K_FS_STRNO);
      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_3K_FS_FRA, pTmp+1, strlen(pTmp+1));
   }
   memcpy(pOutbuf+OFF_3K_FS_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
   memcpy(pOutbuf+OFF_3K_FS_STR, apTokens[MB_SITUS_STRNAME], strlen(apTokens[MB_SITUS_STRNAME]));
   memcpy(pOutbuf+OFF_3K_FS_SFX, apTokens[MB_SITUS_STRTYPE], strlen(apTokens[MB_SITUS_STRTYPE]));
   memcpy(pOutbuf+OFF_3K_FS_UNIT, apTokens[MB_SITUS_UNIT], strlen(apTokens[MB_SITUS_UNIT]));
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2City(apTokens[MB_SITUS_COMMUNITY], acCode);   
      memcpy(pOutbuf+OFF_3K_FS_CITY, acCode, strlen(acCode));
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/******************************** Asr_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************/

int Asr_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp, lTotExe=0;
   int      iRet=0, iTmp, iCnt;
   MB_EXE   myExe;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      pTmp += iSkipQuote;
      iTmp = memcmp(pOutbuf+5, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      *(pOutbuf+OFF_3K_EXCNT) = '0';
      return 0;
   }

   iCnt = 0;
   while (!iTmp)
   {
      memset((void *)&myExe, ' ', sizeof(MB_EXE));
      iRet = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
      if (iRet < MB_EXE_EXEPCT)
      {
         LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         pRec = fgets(acRec, 512, fdExe);
         return -1;
      }

      // Asmt status
      myExe.Status = *apTokens[MB_EXE_STATUS];
      // Exe code
      memcpy(myExe.Code, apTokens[MB_EXE_CODE], strlen(apTokens[MB_EXE_CODE]));
      // HO Flag
      myExe.HO_Flg = *apTokens[MB_EXE_HOEXE];

      // Exe Amt
      lTmp = atol(apTokens[MB_EXE_EXEAMT]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_3K_EXEAMT, lTmp);
         memcpy(myExe.MaxAmt, acTmp, SIZ_3K_EXEAMT);
         lTotExe += lTmp;
      }

      // Exe percentage
      if (strlen(apTokens[MB_EXE_EXEPCT]) > 1)
      {
         sprintf(acTmp, "%.*s", SIZ_3K_EXEPCT, apTokens[MB_EXE_EXEPCT]);
         memcpy(myExe.Percent, acTmp, SIZ_3K_EXEPCT);
      }

      // Copy to output record
      if (iCnt < MAX_EXE_RECS)
         memcpy(pOutbuf+OFF_3K_STAT1+sizeof(MB_EXE)*iCnt++, &myExe, sizeof(MB_EXE));

      // Get next record
      pRec = fgets(acRec, 512, fdExe);
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         break;
      }
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      pTmp += iSkipQuote;
      iTmp = memcmp(pOutbuf+5, pTmp, iApnLen);
      lExeMatch++;
   }

   // Total Exe
   sprintf(acTmp, "%*d", SIZ_3K_TOTEX, lTotExe);
   memcpy(pOutbuf+OFF_3K_TOTEX, acTmp, SIZ_3K_TOTEX);

   // Net
   lTmp = atoin(pOutbuf+OFF_3K_GROSS, SIZ_3K_GROSS);
   lTmp = lTmp - lTotExe;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_NET, lTmp);
      memcpy(pOutbuf+OFF_3K_NET, acTmp, SIZ_3K_NET);
   }

   *(pOutbuf+OFF_3K_EXCNT) = iCnt|0x30;
   return 0;
}

/******************************** Asr_MergeChar ******************************
 *
 * Note: need code table for Heating and Colling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *   - Heating - 1..9 ???
 *   - Cooling - 01..07  ???
 *
 *****************************************************************************/

int Asr_MergeChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft, lImp;
   int      iRet, iLoop, iBeds, iFBath, iHBath, iFp;
   MB_CHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf+5, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_ROLL_ASMT],"003480091", 9))
   //   iTmp = 0;
#endif

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (MB_CHAR *)pRec;

   // Pool
   if (pChar->NumPools[0] > ' ')
      memcpy(pOutbuf+OFF_3K_POOLS, pChar->NumPools, MBSIZ_CHAR_POOLS);

   // UseCat
   if (pChar->LandUseCat[0] > ' ')
      memcpy(pOutbuf+OFF_3K_USECAT, pChar->LandUseCat, MBSIZ_CHAR_USECAT);

   // Quality Class
   if (pChar->QualityClass[0] > ' ')
      memcpy(pOutbuf+OFF_3K_QUALCLS, pChar->QualityClass, MBSIZ_CHAR_QUALITY);

   // HasSeptic or HasSewer
   if (pChar->HasSewer == '1')
      *(pOutbuf+OFF_3K_HASSEWER) = 'Y';
   if (pChar->HasSeptic == '1')
      *(pOutbuf+OFF_3K_HASSEPTIC) = 'Y';

   // HasWell
   if (pChar->HasWell == '1')
      *(pOutbuf+OFF_3K_HASWELL) = 'Y';

   // Yrblt
   memcpy(pOutbuf+OFF_3K_YRBLT, pChar->YearBuilt, SIZ_3K_YRBLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_3K_BDGSZ, lBldgSqft);
      memcpy(pOutbuf+OFF_3K_BDGSZ, acTmp, SIZ_3K_BDGSZ);
      lImp = atoin(pOutbuf+OFF_3K_IMPR, SIZ_3K_IMPR);
      if (lImp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_3K_IVALSQFT, lImp/lBldgSqft);
         memcpy(pOutbuf+OFF_3K_IVALSQFT, acTmp, SIZ_3K_IVALSQFT);
      }
   } else
      memcpy(pOutbuf+OFF_3K_BDGSZ, BLANK32, SIZ_3K_BDGSZ);

   // Garage Sqft
   lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_3K_GARSZ, lGarSqft);
      memcpy(pOutbuf+OFF_3K_GARSZ, acTmp, SIZ_3K_GARSZ);
   } else
      memcpy(pOutbuf+OFF_3K_GARSZ, BLANK32, SIZ_3K_GARSZ);

   // Heating
   if (pChar->Heating[0] > ' ')
      memcpy(pOutbuf+OFF_3K_HEAT, pChar->Heating, SIZ_3K_HEAT);
   // Cooling
   if (pChar->Cooling[0] > ' ')
      memcpy(pOutbuf+OFF_3K_COOL, pChar->Cooling, SIZ_3K_COOL);

   // Heating src
   if (pChar->HeatingSource[0] > ' ')
      memcpy(pOutbuf+OFF_3K_HTSRC, pChar->HeatingSource, MBSIZ_CHAR_HEATSRC);
   // Cooling src
   if (pChar->Cooling[0] > ' ')
      memcpy(pOutbuf+OFF_3K_CLSRC, pChar->CoolingSource, MBSIZ_CHAR_COOLSRC);

   // Beds
   iBeds = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_BEDS, iBeds);
      memcpy(pOutbuf+OFF_3K_BEDS, acTmp, SIZ_3K_BEDS);
   } else
      memcpy(pOutbuf+OFF_3K_BEDS, BLANK32, SIZ_3K_BEDS);

   // Bath
   iFBath = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_FBATHS, iFBath);
      memcpy(pOutbuf+OFF_3K_FBATHS, acTmp, SIZ_3K_FBATHS);
   } else
      memcpy(pOutbuf+OFF_3K_FBATHS, BLANK32, SIZ_3K_FBATHS);

   // Half bath
   iHBath = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_HBATHS, iHBath);
      memcpy(pOutbuf+OFF_3K_HBATHS, acTmp, SIZ_3K_HBATHS);
   } else
      memcpy(pOutbuf+OFF_3K_HBATHS, BLANK32, SIZ_3K_HBATHS);

   // Fireplace
   iFp = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_FIREPL, iFp);
      memcpy(pOutbuf+OFF_3K_FIREPL, acTmp, SIZ_3K_FIREPL);
   } else
      memcpy(pOutbuf+OFF_3K_FIREPL, BLANK32, SIZ_3K_FIREPL);

   lCharMatch++;

   // Get next record
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************** Asr_MergeStdChar ***************************
 *
 * Note: need code table for Heating and Colling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *   - Heating - 1..9 ???
 *   - Cooling - 01..07  ???
 *
 *****************************************************************************/

int Asr_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft, lImp;
   int      iRet, iLoop, iBeds, iFBath, iHBath, iFp;
   STDCHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf+5, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_ROLL_ASMT],"003480091", 9))
   //   iTmp = 0;
#endif

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   // Pool
   if (pChar->Pool[0] > ' ')
      memcpy(pOutbuf+OFF_3K_POOLS, pChar->Pool, MBSIZ_CHAR_POOLS);

   // UseCat
   if (pChar->LandUseCat[0] > ' ')
      memcpy(pOutbuf+OFF_3K_USECAT, pChar->LandUseCat, MBSIZ_CHAR_USECAT);

   // Quality Class
   if (pChar->QualityClass[0] > ' ')
      memcpy(pOutbuf+OFF_3K_QUALCLS, pChar->QualityClass, MBSIZ_CHAR_QUALITY);

   // HasSeptic or HasSewer
   *(pOutbuf+OFF_3K_HASSEWER) = pChar->HasSewer;
   *(pOutbuf+OFF_3K_HASSEPTIC) = pChar->HasSeptic;

   // HasWell
   *(pOutbuf+OFF_3K_HASWELL) = pChar->HasWell;

   // Yrblt
   memcpy(pOutbuf+OFF_3K_YRBLT, pChar->YrBlt, SIZ_3K_YRBLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_3K_BDGSZ, lBldgSqft);
      memcpy(pOutbuf+OFF_3K_BDGSZ, acTmp, SIZ_3K_BDGSZ);
      lImp = atoin(pOutbuf+OFF_3K_IMPR, SIZ_3K_IMPR);
      if (lImp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_3K_IVALSQFT, lImp/lBldgSqft);
         memcpy(pOutbuf+OFF_3K_IVALSQFT, acTmp, SIZ_3K_IVALSQFT);
      }
   } else
      memcpy(pOutbuf+OFF_3K_BDGSZ, BLANK32, SIZ_3K_BDGSZ);

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_3K_GARSZ, lGarSqft);
      memcpy(pOutbuf+OFF_3K_GARSZ, acTmp, SIZ_3K_GARSZ);
   } else
      memcpy(pOutbuf+OFF_3K_GARSZ, BLANK32, SIZ_3K_GARSZ);

   // Heating
   if (pChar->Heating[0] > ' ')
      memcpy(pOutbuf+OFF_3K_HEAT, pChar->Heating, SIZ_CHAR_SIZE2);
   // Cooling
   if (pChar->Cooling[0] > ' ')
      memcpy(pOutbuf+OFF_3K_COOL, pChar->Cooling, SIZ_CHAR_SIZE2);

   // Heating src
   if (pChar->HeatingSrc[0] > ' ')
      memcpy(pOutbuf+OFF_3K_HTSRC, pChar->HeatingSrc, SIZ_CHAR_SIZE2);
   // Cooling src
   if (pChar->CoolingSrc[0] > ' ')
      memcpy(pOutbuf+OFF_3K_CLSRC, pChar->CoolingSrc, SIZ_CHAR_SIZE2);

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_BEDS, iBeds);
      memcpy(pOutbuf+OFF_3K_BEDS, acTmp, SIZ_3K_BEDS);
   } else
      memcpy(pOutbuf+OFF_3K_BEDS, BLANK32, SIZ_3K_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_FBATHS, iFBath);
      memcpy(pOutbuf+OFF_3K_FBATHS, acTmp, SIZ_3K_FBATHS);
   } else
      memcpy(pOutbuf+OFF_3K_FBATHS, BLANK32, SIZ_3K_FBATHS);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_HBATHS, iHBath);
      memcpy(pOutbuf+OFF_3K_HBATHS, acTmp, SIZ_3K_HBATHS);
   } else
      memcpy(pOutbuf+OFF_3K_HBATHS, BLANK32, SIZ_3K_HBATHS);

   // Fireplace
   iFp = atoin(pChar->Fireplace, SIZ_CHAR_SIZE2);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_FIREPL, iFp);
      memcpy(pOutbuf+OFF_3K_FIREPL, acTmp, SIZ_3K_FIREPL);
   } else
      memcpy(pOutbuf+OFF_3K_FIREPL, BLANK32, SIZ_3K_FIREPL);

   lCharMatch++;

   // Get next record
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************** Asr_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Asr_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp, iCmp, iCnt, iTotalSales;
   long     lSaleDt, lPrice, lTmp;
   MB_SALE  sCurSale;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iCmp = memcmp(pOutbuf+5, pRec+iSkipQuote, iApnLen);
      if (iCmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iCmp > 0);

   // If not match, return
   if (iCmp)
   {
      *(pOutbuf+OFF_3K_SALECNT) = '0';
      return 0;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf+5, "003534059", 9))
   //   iRet = 0;
#endif

   iCnt=iTotalSales = 0;
   lSaleDt= atoin(pOutbuf+OFF_3K_LASTSALEDT, 8);
   lPrice = 0;
   while (!iCmp)
   {
      // Clear old record
      memset((void *)&sCurSale, ' ', sizeof(MB_SALE));

      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      iTmp = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTmp < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         iRet =  -1;
         break;
      }

      if (iCnt > 0)
      {
         // Max 2 sales can be moved
         if (iCnt == MAX_SALE_RECS)
            iCnt--;

         // Move sale 1&2 to 2&3 ...
         memcpy(pOutbuf+OFF_3K_SALESTART+sizeof(MB_SALE), pOutbuf+OFF_3K_SALESTART, iCnt*sizeof(MB_SALE));
         // Clear sale 1
         memset(pOutbuf+OFF_3K_SALESTART, ' ', sizeof(MB_SALE));
      }

      // Merge data
      if (*apTokens[MB_SALES_DOCNUM] > ' ')
      {
         if (bFmtDoc)
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
            sprintf(acTmp, "%.5s%0.7ld", apTokens[MB_SALES_DOCNUM], lTmp);
         } else
         {
            strcpy(acTmp, apTokens[MB_SALES_DOCNUM]);
            blankPad(acTmp, SIZ_3K_SDOCNUM);
         }
         memcpy(sCurSale.DocNum, acTmp, SIZ_3K_SDOCNUM);
      } else
         memset(sCurSale.DocNum, ' ', SIZ_3K_SDOCNUM);

      pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         memcpy(sCurSale.SaleDate, acTmp, 8);

      strcpy(sCurSale.SaleCode, apTokens[MB_SALES_DOCCODE]);
      strcpy(sCurSale.Seller, apTokens[MB_SALES_SELLER]);
      strcpy(sCurSale.Buyer, apTokens[MB_SALES_BUYER]);
      sprintf(sCurSale.SalePrice, "%.*s%s", SIZ_3K_CONFPRICE-strlen(apTokens[MB_SALES_PRICE]), BLANK32, apTokens[MB_SALES_PRICE]);
      sprintf(sCurSale.DocTax, "%.*s%s", SIZ_3K_DOCTAX-strlen(apTokens[MB_SALES_TAXAMT]), BLANK32, apTokens[MB_SALES_TAXAMT]);
      //strcpy(sCurSale.SalePrice, apTokens[MB_SALES_PRICE]);
      //strcpy(sCurSale.DocTax, apTokens[MB_SALES_TAXAMT]);

      if (*apTokens[MB_SALES_GROUPSALE] > ' ')
      {
         sCurSale.GroupSale = *apTokens[MB_SALES_GROUPSALE];
         strcpy(sCurSale.GroupAsmt, apTokens[MB_SALES_GROUPASMT]);
      }

      strcpy(sCurSale.TransType, apTokens[MB_SALES_XFERTYPE]);
      strcpy(sCurSale.AdjReasonCode, apTokens[MB_SALES_ADJREASON]);
      strncpy(sCurSale.ConfirmCode, apTokens[MB_SALES_CONFCODE], SIZ_3K_CONFCODE);
   
      // Remove null char with blank - null is resulted from strcpy()
      replChar((char *)&sCurSale, 0, ' ', sizeof(MB_SALE));
      memcpy(pOutbuf+OFF_3K_SALESTART, &sCurSale, sizeof(MB_SALE));

      // Save most recent sale info
      dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
      lTmp = atol(acTmp);
      if (lTmp > 0)
      {
         iTmp = atoin(sCurSale.SaleDate, 8);
         if (iTmp > lSaleDt)
         {
            lPrice = lTmp;
            lSaleDt = iTmp;
         }
      }

      iTotalSales++;
      iCnt++;

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }
      iCmp = memcmp(pOutbuf+5, pRec+iSkipQuote, iApnLen);
   }

   lSaleMatch++;

   sprintf(acTmp, "%2d", iTotalSales);
   memcpy(pOutbuf+OFF_3K_TOTSALES, acTmp, 2);
   *(pOutbuf+OFF_3K_SALECNT) = iCnt|0x30;

   // Update most recent sale
   if (lPrice > 0)
   {
      iTmp = sprintf(acTmp, "%ld", lSaleDt);
      memcpy(pOutbuf+OFF_3K_LASTSALEDT, acTmp, iTmp);
      iTmp = sprintf(acTmp, "%*ld", SIZ_3K_LASTSALEAMT, lPrice);
      memcpy(pOutbuf+OFF_3K_LASTSALEAMT, acTmp, iTmp);

      // TCB 10/16/2006
      lTmp = atoin(pOutbuf+OFF_3K_GROSS, SIZ_3K_GROSS);
      if (lPrice > lTmp)
      {
         sprintf(acTmp, "%*ld", SIZ_3K_EXCPAMT, lPrice-lTmp);
         memcpy(pOutbuf+OFF_3K_EXCPAMT, acTmp, SIZ_3K_EXCPAMT);
      }

      // Sale $ per sqft

   }

   return iRet;
}

/******************************** Ama_MergeTax ******************************
 *
 * Note:
 *
 *****************************************************************************/

void Asr_CreateTaxRec(MB_TAX *pTax)
{
   char     acTmp[256], *pTmp;
   int      iTmp;

   iTmp = dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
   sprintf(pTax->TaxAmt1, "%.*s%s", SIZ_3K_TAXAMT-iTmp, BLANK32, acTmp);
   iTmp = dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
   sprintf(pTax->TaxAmt2, "%.*s%s", SIZ_3K_TAXAMT-iTmp, BLANK32, acTmp);

   iTmp = dollar2Num(apTokens[MB_TAX_PENAMT1], acTmp);
   sprintf(pTax->PenAmt1, "%.*s%s", SIZ_3K_PENAMT-iTmp, BLANK32, acTmp);
   iTmp = dollar2Num(apTokens[MB_TAX_PENAMT2], acTmp);
   sprintf(pTax->PenAmt2, "%.*s%s", SIZ_3K_PENAMT-iTmp, BLANK32, acTmp);

   pTmp = dateConversion(apTokens[MB_TAX_PENDATE1], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      strcpy(pTax->PenChrgDt1, acTmp);

   pTmp = dateConversion(apTokens[MB_TAX_PENDATE2], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      strcpy(pTax->PenChrgDt2, acTmp);

   iTmp = dollar2Num(apTokens[MB_TAX_PAIDAMT1], acTmp);
   sprintf(pTax->PaidAmt1, "%.*s%s", SIZ_3K_PAIDAMT-iTmp, BLANK32, acTmp);
   iTmp = dollar2Num(apTokens[MB_TAX_PAIDAMT2], acTmp);
   sprintf(pTax->PaidAmt2, "%.*s%s", SIZ_3K_PAIDAMT-iTmp, BLANK32, acTmp);

   iTmp = dollar2Num(apTokens[MB_TAX_TOTALPAID1], acTmp);
   sprintf(pTax->TotPaid1, "%.*s%s", SIZ_3K_TOTPAID-iTmp, BLANK32, acTmp);
   iTmp = dollar2Num(apTokens[MB_TAX_TOTALPAID2], acTmp);
   sprintf(pTax->TotPaid2, "%.*s%s", SIZ_3K_TOTPAID-iTmp, BLANK32, acTmp);

   iTmp = dollar2Num(apTokens[MB_TAX_TOTALFEES], acTmp);
   sprintf(pTax->TotFees, "%.*s%s", SIZ_3K_TOTALFEES-iTmp, BLANK32, acTmp);

   pTmp = dateConversion(apTokens[MB_TAX_PAIDDATE1], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      strcpy(pTax->PaidDt1, acTmp);
   pTmp = dateConversion(apTokens[MB_TAX_PAIDDATE2], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      strcpy(pTax->PaidDt2, acTmp);

   // Yrblt
   if (strlen(apTokens[MB_TAX_YEAR]) == 4)
      memcpy(pTax->TaxYear, apTokens[MB_TAX_YEAR], SIZ_3K_TAXYEAR);
   else
      memcpy(pTax->TaxYear, BLANK32, SIZ_3K_TAXYEAR);

   strncpy(pTax->RollCat, apTokens[MB_TAX_ROLLCAT], SIZ_3K_ROLLCAT); 
}

void Asr_CreateTaxRec1(MB_TAX *pTax)
{
   char     acTmp[256], *pTmp;
   int      iTmp;

   iTmp = dollar2Num(apTokens[L1_TAXAMT1], acTmp);
   sprintf(pTax->TaxAmt1, "%.*s%s", SIZ_3K_TAXAMT-iTmp, BLANK32, acTmp);
   iTmp = dollar2Num(apTokens[L1_TAXAMT2], acTmp);
   sprintf(pTax->TaxAmt2, "%.*s%s", SIZ_3K_TAXAMT-iTmp, BLANK32, acTmp);

   iTmp = dollar2Num(apTokens[L1_PENAMT1], acTmp);
   sprintf(pTax->PenAmt1, "%.*s%s", SIZ_3K_PENAMT-iTmp, BLANK32, acTmp);
   iTmp = dollar2Num(apTokens[L1_PENAMT2], acTmp);
   sprintf(pTax->PenAmt2, "%.*s%s", SIZ_3K_PENAMT-iTmp, BLANK32, acTmp);

   pTmp = dateConversion(apTokens[L1_PENCHRGDATE1], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      strcpy(pTax->PenChrgDt1, acTmp);

   pTmp = dateConversion(apTokens[L1_PENCHRGDATE2], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      strcpy(pTax->PenChrgDt2, acTmp);

   iTmp = dollar2Num(apTokens[L1_PAIDAMT1], acTmp);
   sprintf(pTax->PaidAmt1, "%.*s%s", SIZ_3K_PAIDAMT-iTmp, BLANK32, acTmp);
   iTmp = dollar2Num(apTokens[L1_PAIDAMT2], acTmp);
   sprintf(pTax->PaidAmt2, "%.*s%s", SIZ_3K_PAIDAMT-iTmp, BLANK32, acTmp);

   iTmp = dollar2Num(apTokens[L1_TOTALFEESPAID1], acTmp);
   sprintf(pTax->TotPaid1, "%.*s%s", SIZ_3K_TOTPAID-iTmp, BLANK32, acTmp);
   iTmp = dollar2Num(apTokens[L1_TOTALFEESPAID2], acTmp);
   sprintf(pTax->TotPaid2, "%.*s%s", SIZ_3K_TOTPAID-iTmp, BLANK32, acTmp);

   iTmp = dollar2Num(apTokens[L1_TOTALFEES], acTmp);
   sprintf(pTax->TotFees, "%.*s%s", SIZ_3K_TOTALFEES-iTmp, BLANK32, acTmp);

   pTmp = dateConversion(apTokens[L1_PAYMENTDATE1], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      strcpy(pTax->PaidDt1, acTmp);
   pTmp = dateConversion(apTokens[L1_PAYMENTDATE2], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      strcpy(pTax->PaidDt2, acTmp);

   // Yrblt
   if (strlen(apTokens[L1_TAXYEAR]) == 4)
      memcpy(pTax->TaxYear, apTokens[L1_TAXYEAR], SIZ_3K_TAXYEAR);
   else
      memcpy(pTax->TaxYear, BLANK32, SIZ_3K_TAXYEAR);

   strncpy(pTax->RollCat, apTokens[L1_ROLLCATEGORY], SIZ_3K_ROLLCAT); 
}

void Asr_CreateTaxRec2(MB_TAX *pTax)
{
   char     acTmp[256], *pTmp;
   int      iTmp;

   iTmp = dollar2Num(apTokens[L2_TAXAMT1], acTmp);
   sprintf(pTax->TaxAmt1, "%.*s%s", SIZ_3K_TAXAMT-iTmp, BLANK32, acTmp);
   iTmp = dollar2Num(apTokens[L2_TAXAMT2], acTmp);
   sprintf(pTax->TaxAmt2, "%.*s%s", SIZ_3K_TAXAMT-iTmp, BLANK32, acTmp);

   iTmp = dollar2Num(apTokens[L2_PENAMT1], acTmp);
   sprintf(pTax->PenAmt1, "%.*s%s", SIZ_3K_PENAMT-iTmp, BLANK32, acTmp);
   iTmp = dollar2Num(apTokens[L2_PENAMT2], acTmp);
   sprintf(pTax->PenAmt2, "%.*s%s", SIZ_3K_PENAMT-iTmp, BLANK32, acTmp);

   pTmp = dateConversion(apTokens[L2_PENCHRGDATE1], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      strcpy(pTax->PenChrgDt1, acTmp);

   pTmp = dateConversion(apTokens[L2_PENCHRGDATE2], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      strcpy(pTax->PenChrgDt2, acTmp);

   iTmp = dollar2Num(apTokens[L2_PAIDAMT1], acTmp);
   sprintf(pTax->PaidAmt1, "%.*s%s", SIZ_3K_PAIDAMT-iTmp, BLANK32, acTmp);
   iTmp = dollar2Num(apTokens[L2_PAIDAMT2], acTmp);
   sprintf(pTax->PaidAmt2, "%.*s%s", SIZ_3K_PAIDAMT-iTmp, BLANK32, acTmp);

   iTmp = dollar2Num(apTokens[L2_TOTALFEESPAID1], acTmp);
   sprintf(pTax->TotPaid1, "%.*s%s", SIZ_3K_TOTPAID-iTmp, BLANK32, acTmp);
   iTmp = dollar2Num(apTokens[L2_TOTALFEESPAID2], acTmp);
   sprintf(pTax->TotPaid2, "%.*s%s", SIZ_3K_TOTPAID-iTmp, BLANK32, acTmp);

   iTmp = dollar2Num(apTokens[L2_TOTALFEES], acTmp);
   sprintf(pTax->TotFees, "%.*s%s", SIZ_3K_TOTALFEES-iTmp, BLANK32, acTmp);

   pTmp = dateConversion(apTokens[L2_PAYMENTDATE1], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      strcpy(pTax->PaidDt1, acTmp);
   pTmp = dateConversion(apTokens[L2_PAYMENTDATE2], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      strcpy(pTax->PaidDt2, acTmp);

   // Yrblt
   if (strlen(apTokens[L2_TAXYEAR]) == 4)
      memcpy(pTax->TaxYear, apTokens[L2_TAXYEAR], SIZ_3K_TAXYEAR);
   else
      memcpy(pTax->TaxYear, BLANK32, SIZ_3K_TAXYEAR);

   strncpy(pTax->RollCat, apTokens[L2_ROLLCATEGORY], SIZ_3K_ROLLCAT); 
}

int Asr_MergeTax(char *pOutbuf)
{
   static   char acRec[MAX_RECSIZE], *pRec=NULL;
   //char     acTmp[256], *pTmp;
   int      iRet=0, iTmp, iCmp, iCnt;
   MB_TAX   sTax;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);
      // Get first rec
      pRec = fgets(acRec, MAX_RECSIZE, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iCmp = memcmp(pOutbuf+5, pRec+iSkipQuote, iApnLen);
      if (iCmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);
         lTaxSkip++;
      }
   } while (iCmp > 0);

   // If not match, return
   if (iCmp)
      return 1;

   // Merge tax data
   iCnt = 0;
   while (!iCmp)
   {
      memset((void *)&sTax, ' ', sizeof(MB_TAX));

      iTmp = ParseStringNQ(pRec, cDelim, lTaxFlds+1, apTokens);
      if (iTmp < lTaxFlds)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         pRec = fgets(acRec, MAX_RECSIZE, fdTax);
         iRet = -1;
         break;
      }
      
      // Create tax record
      if (bNewTax == false)
         Asr_CreateTaxRec(&sTax);
      else
      {
         if (iLdrGrp == 1)
            Asr_CreateTaxRec1(&sTax);
         else
            Asr_CreateTaxRec2(&sTax);
      }

      /* 02/19/2010 - spn
      iTmp = dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
      sprintf(sTax.TaxAmt1, "%.*s%s", SIZ_3K_TAXAMT-iTmp, BLANK32, acTmp);
      iTmp = dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
      sprintf(sTax.TaxAmt2, "%.*s%s", SIZ_3K_TAXAMT-iTmp, BLANK32, acTmp);

      iTmp = dollar2Num(apTokens[MB_TAX_PENAMT1], acTmp);
      sprintf(sTax.PenAmt1, "%.*s%s", SIZ_3K_PENAMT-iTmp, BLANK32, acTmp);
      iTmp = dollar2Num(apTokens[MB_TAX_PENAMT2], acTmp);
      sprintf(sTax.PenAmt2, "%.*s%s", SIZ_3K_PENAMT-iTmp, BLANK32, acTmp);

      pTmp = dateConversion(apTokens[MB_TAX_PENDATE1], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         strcpy(sTax.PenChrgDt1, acTmp);

      pTmp = dateConversion(apTokens[MB_TAX_PENDATE2], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         strcpy(sTax.PenChrgDt2, acTmp);

      iTmp = dollar2Num(apTokens[MB_TAX_PAIDAMT1], acTmp);
      sprintf(sTax.PaidAmt1, "%.*s%s", SIZ_3K_PAIDAMT-iTmp, BLANK32, acTmp);
      iTmp = dollar2Num(apTokens[MB_TAX_PAIDAMT2], acTmp);
      sprintf(sTax.PaidAmt2, "%.*s%s", SIZ_3K_PAIDAMT-iTmp, BLANK32, acTmp);

      iTmp = dollar2Num(apTokens[MB_TAX_TOTALPAID1], acTmp);
      sprintf(sTax.TotPaid1, "%.*s%s", SIZ_3K_TOTPAID-iTmp, BLANK32, acTmp);
      iTmp = dollar2Num(apTokens[MB_TAX_TOTALPAID2], acTmp);
      sprintf(sTax.TotPaid2, "%.*s%s", SIZ_3K_TOTPAID-iTmp, BLANK32, acTmp);

      iTmp = dollar2Num(apTokens[MB_TAX_TOTALFEES], acTmp);
      sprintf(sTax.TotFees, "%.*s%s", SIZ_3K_TOTALFEES-iTmp, BLANK32, acTmp);

      pTmp = dateConversion(apTokens[MB_TAX_PAIDDATE1], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         strcpy(sTax.PaidDt1, acTmp);
      pTmp = dateConversion(apTokens[MB_TAX_PAIDDATE2], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         strcpy(sTax.PaidDt2, acTmp);

      // Yrblt
      if (strlen(apTokens[MB_TAX_YEAR]) == 4)
         memcpy(sTax.TaxYear, apTokens[MB_TAX_YEAR], SIZ_3K_TAXYEAR);
      else
         memcpy(sTax.TaxYear, BLANK32, SIZ_3K_TAXYEAR);

      strncpy(sTax.RollCat, apTokens[MB_TAX_ROLLCAT], SIZ_3K_ROLLCAT); 
      */

      iTmp = replChar((char *)&sTax, 0, ' ', sizeof(MB_TAX));

      if (!iCnt)
         memcpy(pOutbuf+OFF_3K_TAXAMT1, &sTax, sizeof(MB_TAX));
      else if (iCnt < MAX_TAX_RECS)
         memcpy(pOutbuf+OFF_3K_TX2AMT1+sizeof(MB_TAX)*(iCnt-1), &sTax, sizeof(MB_TAX));

      iCnt++;

      // Get next tax record
      pRec = fgets(acRec, MAX_RECSIZE, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iCmp = memcmp(pOutbuf+5, pRec+iSkipQuote, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/******************************** Asr_MergeGrGr ******************************
 *
 * 
 *
 *****************************************************************************/

int Asr_MergeGrGr(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   int      iRet=0, iTmp, iCmp, iCnt;
   long     lSaleDt, lPrice, lTmp;
   MB_GRGR  *pSaleRec;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdGrGr);
      // Get first rec
      pRec = fgets(acRec, 512, fdGrGr);
   }

   pSaleRec = (MB_GRGR *) &acRec[0];
   do
   {
      if (!pRec)
         return 1;      // EOF

      iCmp = memcmp(pOutbuf+5, pRec, SIZ_3K_APN);
      if (iCmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", SIZ_3K_APN, pRec);
         pRec = fgets(acRec, 512, fdGrGr);
         lSaleSkip++;
      }
   } while (iCmp > 0);

   // If not match, return
   if (iCmp)
   {
      *(pOutbuf+OFF_3K_G_G_COUNT) = '0';
      return 0;
   }

   iCnt = 0;
   lSaleDt = atoin(pOutbuf+OFF_3K_LASTSALEDT, SIZ_3K_LASTSALEDT);
   lPrice = 0;
   while (!iCmp)
   {
      if (iCnt < MAX_GRGR_RECS)
      {
         // Merge data
         memcpy(pOutbuf+OFF_3K_1DT+(iCnt*GRGR_SIZE), (char *)&pSaleRec->DocDate[0], GRGR_SIZE);

         // Save most recent sale info
         lTmp = atoin(pSaleRec->SalePrice, SIZ_3K_GSALEPRICE);
         if (lTmp > 0)
         {
            iTmp = atoin(pSaleRec->DocDate, SIZ_3K_LASTSALEDT);
            if (iTmp > lSaleDt)
            {
               lPrice = lTmp;
               lSaleDt = iTmp;
            }
         }

         iCnt++;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdGrGr);
      if (!pRec)
      {
         fclose(fdGrGr);
         fdGrGr = NULL;
         break;
      }
      iCmp = memcmp(pOutbuf+5, pRec, SIZ_3K_APN);
   }

   lGrGrMatch++;

   *(pOutbuf+OFF_3K_G_G_COUNT) = iCnt|0x30;

   // Update most recent sale
   if (lPrice > 0)
   {
      char  acTmp[32];
      iTmp = sprintf(acTmp, "%ld", lSaleDt);
      memcpy(pOutbuf+OFF_3K_LASTSALEDT, acTmp, iTmp);
      iTmp = sprintf(acTmp, "%*ld", SIZ_3K_LASTSALEAMT, lPrice);
      memcpy(pOutbuf+OFF_3K_LASTSALEAMT, acTmp, iTmp);
   }

   return iRet;
}

/********************************* Asr_MergeAppr *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Asr_MergeAppr(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acTmp1[512];
   int      iRet=0, iTmp, iCmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdAppr);
      // Get first rec
      pRec = fgets(acRec, 512, fdAppr);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdAppr);
         fdAppr = NULL;
         return 1;      // EOF
      }

      iCmp = memcmp(pOutbuf+5, pRec+iSkipQuote, MON_SIZ_BOOKPAGE);
      if (iCmp > 0)
         pRec = fgets(acRec, 512, fdAppr);
   } while (iCmp > 0);

   // If not match, return
   if (iCmp)
   {
      lBkPgNoAppr++;
      return iRet;
   }

   strcpy(acTmp1, acRec);
   iTmp = ParseStringNQ(acTmp1, cDelim, MON_OFF_USERID+1, apTokens);
   if (iTmp < MON_OFF_USERID)
   {
      LogMsg("***** Error: bad Appr record for APN=%.*s (#tokens=%d)", 5, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdAppr);
      iRet = -1;
   } else
	{
		if (*apTokens[MON_OFF_NCODE] > ' ')
			memcpy(pOutbuf+MON_OFF_3K_NCODE, apTokens[MON_OFF_NCODE], strlen(apTokens[MON_OFF_NCODE]));
		else
			memset(pOutbuf+MON_OFF_3K_NCODE, ' ', MON_SIZ_NCODE); 

		if (*apTokens[MON_OFF_APPRID] > ' ')
			memcpy(pOutbuf+MON_OFF_3K_APPRID, apTokens[MON_OFF_APPRID], strlen(apTokens[MON_OFF_APPRID]));
		else
			memset(pOutbuf+MON_OFF_3K_APPRID, ' ', MON_SIZ_APPRID); 

		if (*apTokens[MON_OFF_NCSUFFIX] > ' ')
			memcpy(pOutbuf+MON_OFF_3K_NCSUFFIX, apTokens[MON_OFF_NCSUFFIX], strlen(apTokens[MON_OFF_NCSUFFIX]));
		else
			memset(pOutbuf+MON_OFF_3K_NCSUFFIX, ' ', MON_SIZ_NCSUFFIX); 

		if (*apTokens[MON_OFF_AREADESIG] > ' ')
		{
			strUpper(apTokens[MON_OFF_AREADESIG], acTmp);
         iTmp = strlen(acTmp);
         if (iTmp > MON_SIZ_AREADESIG)
            iTmp = MON_SIZ_AREADESIG;
			memcpy(pOutbuf+MON_OFF_3K_AREADESIG, acTmp, iTmp);
      } else
			memset(pOutbuf+MON_OFF_3K_AREADESIG, ' ', MON_SIZ_AREADESIG); 

		if (*apTokens[MON_OFF_AREADESC] > ' ')
		{
			strUpper(apTokens[MON_OFF_AREADESC], acTmp);
         iTmp = strlen(acTmp);
         if (iTmp > MON_SIZ_AREADESC)
            iTmp = MON_SIZ_AREADESC;
			memcpy(pOutbuf+MON_OFF_3K_AREADESC, acTmp, iTmp);
      } else
			memset(pOutbuf+MON_OFF_3K_AREADESC, ' ', MON_SIZ_AREADESC); 

		if (*apTokens[MON_OFF_APPRNAME] > ' ')
		{
			strUpper(apTokens[MON_OFF_APPRNAME], acTmp);
         iTmp = strlen(acTmp);
         if (iTmp > MON_SIZ_APPRNAME)
            iTmp = MON_SIZ_APPRNAME;
			memcpy(pOutbuf+MON_OFF_3K_APPRNAME, acTmp, iTmp);
      } else
			memset(pOutbuf+MON_OFF_3K_APPRNAME, ' ', MON_SIZ_APPRNAME); 

		//if (*apTokens[MON_OFF_DTS] > ' ')
		//	memcpy(pOutbuf+MON_OFF_3K_DTS, apTokens[MON_OFF_DTS], MON_SIZ_DTS);
		//else
		//	memcpy(pOutbuf+MON_OFF_3K_DTS, BLANK32, MON_SIZ_DTS); 

		//if (*apTokens[MON_OFF_USERID] > ' ')
		//	memcpy(pOutbuf+MON_OFF_3K_USERID, apTokens[MON_OFF_USERID], MON_SIZ_USERID);
		//else
		//	memcpy(pOutbuf+MON_OFF_3K_USERID, BLANK32, MON_SIZ_USERID); 
	}

   lApprMatch++;
   return iRet;
}
/********************************* Asr_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Asr_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], *pTmp;
   long     lTmp, lLand, lImpr, lHomeSite, lGrow, lFixt, lFixtRp, lFixtBus,lPPMh, lGross;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace tab char with 0
   iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Asr_MergeRoll(): bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_ROLL_ASMT],"002396012", 9))
   //   iTmp = 0;
#endif

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iAsrRecLen);

      // County code 
      memcpy(pOutbuf+OFF_3K_CO_ID, myCounty.acCntyCode, 3);
      memcpy(pOutbuf+OFF_3K_CO_NUM, myCounty.acCntyID, 2);

      // Asmt, fee parcel
      memcpy(pOutbuf+OFF_3K_ASSMT, apTokens[MB_ROLL_ASMT], strlen(apTokens[MB_ROLL_ASMT]));
      memcpy(pOutbuf+OFF_3K_FEE_PRCL, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[MB_ROLL_ASMT], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_3K_APN_DASH, acTmp, iRet);

      // TRA
      memcpy(pOutbuf+OFF_3K_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));
      // Legal
      if (*apTokens[MB_ROLL_LEGAL] > ' ')
      {
         iTmp = strlen(apTokens[MB_ROLL_LEGAL]);
         if (iTmp > SIZ_3K_DESC)
            iTmp = SIZ_3K_DESC;
         memcpy(pOutbuf+OFF_3K_DESC, apTokens[MB_ROLL_LEGAL], iTmp);
      }
      
      // Land
      lLand = atol(apTokens[MB_ROLL_LAND]);
      sprintf(acTmp, "%*d", SIZ_3K_LAND, lLand);
      memcpy(pOutbuf+OFF_3K_LAND, acTmp, SIZ_3K_LAND);
      memcpy(pOutbuf+OFF_3K_CLAND, acTmp, SIZ_3K_LAND);

      // HomeSite
      lHomeSite = atol(apTokens[MB_ROLL_HOMESITE]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lHomeSite);
      memcpy(pOutbuf+OFF_3K_HOMESITE, acTmp, SIZ_3K_IMPR);
      memcpy(pOutbuf+OFF_3K_CHOMESITE, acTmp, SIZ_3K_IMPR);

      // Improve
      lImpr = atol(apTokens[MB_ROLL_IMPR]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lImpr);
      memcpy(pOutbuf+OFF_3K_IMPR, acTmp, SIZ_3K_IMPR);
      memcpy(pOutbuf+OFF_3K_CIMPR, acTmp, SIZ_3K_IMPR);

      // Grow Improve
      lGrow = atol(apTokens[MB_ROLL_GROWING]);
      sprintf(acTmp, "%*d", SIZ_3K_GROW, lGrow);
      memcpy(pOutbuf+OFF_3K_GROW, acTmp, SIZ_3K_GROW);
      memcpy(pOutbuf+OFF_3K_CGROW, acTmp, SIZ_3K_GROW);

      // Fixtures 
      lFixt = atoi(apTokens[MB_ROLL_FIXTRS]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixt);
      memcpy(pOutbuf+OFF_3K_FIXT, acTmp, SIZ_3K_IMPR);
      memcpy(pOutbuf+OFF_3K_CFIXT, acTmp, SIZ_3K_IMPR);

      // Fixtures real prop
      lFixtRp = atoi(apTokens[MB_ROLL_FIXTR_RP]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixtRp);
      memcpy(pOutbuf+OFF_3K_FIXT_RP, acTmp, SIZ_3K_IMPR);
      memcpy(pOutbuf+OFF_3K_CFIXT_RP, acTmp, SIZ_3K_IMPR);

      // Bus. pers prop
      lFixtBus = atoi(apTokens[MB_ROLL_FIXTR_BUS]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixtBus);
      memcpy(pOutbuf+OFF_3K_PERS, acTmp, SIZ_3K_IMPR);
      memcpy(pOutbuf+OFF_3K_CPERS, acTmp, SIZ_3K_IMPR);

      // Mobile home pers prop
      lPPMh = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lPPMh);
      memcpy(pOutbuf+OFF_3K_MHPERS, acTmp, SIZ_3K_IMPR);
      memcpy(pOutbuf+OFF_3K_CMHPERS, acTmp, SIZ_3K_IMPR);
   } else  // current assessment
   {
      // Land
      lLand = atol(apTokens[MB_ROLL_LAND]);
      sprintf(acTmp, "%*d", SIZ_3K_LAND, lLand);
      memcpy(pOutbuf+OFF_3K_CLAND, acTmp, SIZ_3K_LAND);

      // HomeSite
      lHomeSite = atol(apTokens[MB_ROLL_HOMESITE]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lHomeSite);
      memcpy(pOutbuf+OFF_3K_CHOMESITE, acTmp, SIZ_3K_IMPR);

      // Improve
      lImpr = atol(apTokens[MB_ROLL_IMPR]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lImpr);
      memcpy(pOutbuf+OFF_3K_CIMPR, acTmp, SIZ_3K_IMPR);

      // Grow Improve
      lGrow = atol(apTokens[MB_ROLL_GROWING]);
      sprintf(acTmp, "%*d", SIZ_3K_GROW, lGrow);
      memcpy(pOutbuf+OFF_3K_CGROW, acTmp, SIZ_3K_GROW);

      // Fixtures 
      lFixt = atoi(apTokens[MB_ROLL_FIXTRS]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixt);
      memcpy(pOutbuf+OFF_3K_CFIXT, acTmp, SIZ_3K_IMPR);

      // Fixtures real prop
      lFixtRp = atoi(apTokens[MB_ROLL_FIXTR_RP]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixtRp);
      memcpy(pOutbuf+OFF_3K_CFIXT_RP, acTmp, SIZ_3K_IMPR);

      // Bus. pers prop
      lFixtBus = atoi(apTokens[MB_ROLL_FIXTR_BUS]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixtBus);
      memcpy(pOutbuf+OFF_3K_CPERS, acTmp, SIZ_3K_IMPR);

      // Mobile home pers prop
      lPPMh = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lPPMh);
      memcpy(pOutbuf+OFF_3K_CMHPERS, acTmp, SIZ_3K_IMPR);
   }

   // Gross
   lTmp = lLand+lImpr;
   lGross = lTmp+lHomeSite+lGrow+lFixt+lFixtRp+lFixtBus+lPPMh;
   sprintf(acTmp, "%*d", SIZ_3K_GROSS, lGross);
   memcpy(pOutbuf+OFF_3K_GROSS, acTmp, SIZ_3K_GROSS);

   // Land+Impr
   sprintf(acTmp, "%*d", SIZ_3K_IMPR, lTmp);
   memcpy(pOutbuf+OFF_3K_LANDIMP, acTmp, SIZ_3K_IMPR);

   // Impr%
   if (lTmp > 0 && lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_IMPPERC, 100*(lImpr/lTmp));
      memcpy(pOutbuf+OFF_3K_IMPPERC, acTmp, SIZ_3K_IMPPERC);
   }

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%.*ld", SIZ_3K_INACRES, (long)(dTmp * 100.0001));
      memcpy(pOutbuf+OFF_3K_INACRES, acTmp, SIZ_3K_INACRES);
      sprintf(acTmp, "%.*s%s", SIZ_3K_DRV_ACRES-strlen(apTokens[MB_ROLL_ACRES]), BLANK32, apTokens[MB_ROLL_ACRES]);
      memcpy(pOutbuf+OFF_3K_DRV_ACRES, acTmp, SIZ_3K_DRV_ACRES);
      sprintf(acTmp, "%*ld", SIZ_3K_LOTSQFT, (long)(dTmp * SQFT_PER_ACRE));
      memcpy(pOutbuf+OFF_3K_LOTSQFT, acTmp, SIZ_3K_LOTSQFT);
   }

   // Land $ per acres
   // Land $ per sqft
   // Impr $ per sqft

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      iTmp = strlen(apTokens[MB_ROLL_ZONING]);
      if (iTmp > SIZ_3K_ZONE)
         iTmp = SIZ_3K_ZONE;
      memcpy(pOutbuf+OFF_3K_ZONE, apTokens[MB_ROLL_ZONING], iTmp);
   }

   // UseCode
   iTmp = strlen(apTokens[MB_ROLL_USECODE]);
   if (iTmp > SIZ_3K_USE)
      iTmp = SIZ_3K_USE;
   memcpy(pOutbuf+OFF_3K_USE, apTokens[MB_ROLL_USECODE], iTmp);

   // Neighborhood code
   iTmp = strlen(apTokens[MB_ROLL_NBHCODE]);
   if (iTmp > SIZ_3K_NBHCD)
      iTmp = SIZ_3K_NBHCD;
   memcpy(pOutbuf+OFF_3K_NBHCD, apTokens[MB_ROLL_NBHCODE], iTmp);

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > ' ')
   {
      if (bFmtDoc)
      {
         lTmp = atoin(apTokens[MB_ROLL_DOCNUM]+5, 7);
         iTmp = sprintf(acTmp, "%.5s%0.7ld", apTokens[MB_ROLL_DOCNUM], lTmp);
      } else
      {
         strcpy(acTmp, apTokens[MB_ROLL_DOCNUM]);
         blankPad(acTmp, SIZ_3K_RDOCNUM);
      }
      memcpy(pOutbuf+OFF_3K_RDOCNUM, acTmp, SIZ_3K_RDOCNUM);
   } else
      memcpy(pOutbuf+OFF_3K_RDOCNUM, BLANK32, SIZ_3K_RDOCNUM);

   // Recording date
   pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      memcpy(pOutbuf+OFF_3K_RDOCDT, acTmp, strlen(acTmp));

   // Fix APN
   if (!bFixedApn)
      memset(pOutbuf+OFF_3K_OUTFIXAPN, ' ', SIZ_3K_OUTFIXAPN);
   else
   {
      // Create fixed APN - implement for LAK only
   }

   // Tax in full
   memcpy(pOutbuf+OFF_3K_TAXFULL, apTokens[MB_ROLL_TAXABILITY], strlen(apTokens[MB_ROLL_TAXABILITY]));

   // Owner
   memset(pOutbuf+OFF_3K_NAME, ' ', (OFF_3K_M_STR)-(OFF_3K_NAME));
   memcpy(pOutbuf+OFF_3K_NAME, apTokens[MB_ROLL_OWNER], strlen(apTokens[MB_ROLL_OWNER]));
   memcpy(pOutbuf+OFF_3K_CAREOF, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));
   memcpy(pOutbuf+OFF_3K_DBA, apTokens[MB_ROLL_DBA], strlen(apTokens[MB_ROLL_DBA]));

   // Mailing
   memset(pOutbuf+OFF_3K_M_STR, ' ', (OFF_3K_S_STR)-(OFF_3K_M_STR));
   memset(pOutbuf+OFF_3K_FM_STRNO, ' ', (OFF_3K_ADRFLG)-(OFF_3K_FM_STRNO));
   if (*apTokens[MB_ROLL_M_ADDR] > ' ')
   {
      memcpy(pOutbuf+OFF_3K_M_STR, apTokens[MB_ROLL_M_ADDR], strlen(apTokens[MB_ROLL_M_ADDR]));
      memcpy(pOutbuf+OFF_3K_M_CITY, apTokens[MB_ROLL_M_CITY], strlen(apTokens[MB_ROLL_M_CITY]));
      memcpy(pOutbuf+OFF_3K_M_ST, apTokens[MB_ROLL_M_ST], strlen(apTokens[MB_ROLL_M_ST]));
      memcpy(pOutbuf+OFF_3K_M_ZIP, apTokens[MB_ROLL_M_ZIP], strlen(apTokens[MB_ROLL_M_ZIP]));

      Asr_MergeMAdr(pOutbuf);
      *(pOutbuf+OFF_3K_ADRFLG) = '1';
   } else
   {
      *(pOutbuf+OFF_3K_ADRFLG) = ' ';
   }

   // Format owner
   if (!memcmp(myCounty.acCntyCode, "SHA", 3))
      ShaAsr_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   else
      Asr_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   return 0;
}

/********************************** MB_MergeAssr ******************************
 *
 * This function has not been tested.
 *
 ******************************************************************************/

int MB_MergeAssr(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdRoll;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   iApnLen = myCounty.iApnLen;
   if (!memcmp(myCounty.acCntyCode, "LAK", 3))
      bFixedApn = true;
   else
      bFixedApn = false;

   //sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   //sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   //sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   sprintf(acRawFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, "CO");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open GrGr file
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open GrGr file %s", acTmpFile);
      fdGrGr = fopen(acTmpFile, "r");
      if (fdGrGr == NULL)
      {
         LogMsg("***** Error opening GrGr file: %s\n", acTmpFile);
         return 2;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return 2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return 2;
   }
   // Open Sales file - Need to be sorted 
   LogMsg("Open Sales file %s", acSalesFile);
   sprintf(acTmpFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "SMP");
   lRet = sortFile(acSalesFile, acTmpFile, "S(#1,C,A,#3,DAT,A) F(TXT)");
   fdSale = fopen(acTmpFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acTmpFile);
      return 2;
   }
   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "EMP");
   lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) F(TXT)");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return 2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return 2;
   }

   // TCB Add BookPageAppraiser
   // Open Appraiser file
   LogMsg("Open BookPageAppraiser file %s", acApprFile);
   if (acApprFile[0] > 'A')
   {
      fdAppr = fopen(acApprFile, "r");
      if (fdAppr == NULL)
      {
         LogMsg("** No BookPageAppraiser file: %s\n", acApprFile);
      }
   } else
      fdAppr = NULL;
   
	// Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Drop header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iAsrRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iAsrRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
   }

   iNewRec=iNoMatch=iBadCity=iBadSuffix=0;
   lCharMatch= lSitusMatch= lExeMatch= lSaleMatch= lTaxMatch= 0;
   lCharSkip = lSitusSkip = lExeSkip = lSaleSkip = lTaxSkip = 0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iAsrRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
      iTmp = memcmp((char *)&acBuf[5], (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Asr_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         // Merge Situs
         if (fdSitus)
            lRet = Asr_MergeSitus(acBuf);

         // Merge Exe
         if (fdExe)
            lRet = Asr_MergeExe(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Asr_MergeChar(acBuf);

         // Merge Sales
         if (fdSale)
            lRet = Asr_MergeSale(acBuf);

         // Merge Taxes
         if (fdTax)
            lRet = Asr_MergeTax(acBuf);

         // Merge GrGr
         if (fdGrGr)
            lRet = Asr_MergeGrGr(acBuf);

         // Merge Appraisal
         if (fdAppr)
            lRet = Asr_MergeAppr(acBuf);

         iRollUpd++;

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Asr_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

         // Merge Situs
         if (fdSitus)
            lRet = Asr_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = Asr_MergeExe(acRec);

         // Merge Char
         if (fdChar)
            lRet = Asr_MergeChar(acRec);

         // Merge Sales
         if (fdSale)
            lRet = Asr_MergeSale(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = Asr_MergeTax(acRec);

         // Merge GrGr
         if (fdGrGr)
            lRet = Asr_MergeGrGr(acRec);

         // Merge Appraisal
         if (fdAppr)
            lRet = Asr_MergeAppr(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_3K_RDOCDT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iAsrRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[iSkipQuote], lCnt);
         iRetiredRec++;
         continue;
      }

      // Save last recording date
      lRet = atoin((char *)&acBuf[OFF_3K_RDOCDT], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
      
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Finish remainder of roll file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Asr_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      // Merge Situs
      if (fdSitus)
         lRet = Asr_MergeSitus(acRec);

      // Merge Exe
      if (fdExe)
         lRet = Asr_MergeExe(acRec);

      // Merge Char
      if (fdChar)
         lRet = Asr_MergeChar(acRec);

      // Merge Sales
      if (fdSale)
         lRet = Asr_MergeSale(acRec);

      // Merge Taxes
      if (fdTax)
         lRet = Asr_MergeTax(acRec);

      // Merge GrGr
      if (fdGrGr)
         lRet = Asr_MergeGrGr(acRec);

      // Merge Appraisal
      if (fdAppr)
         lRet = Asr_MergeAppr(acBuf);

		// Save last recording date
      lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      iNewRec++;
      bRet = WriteFile(fhOut, acRec, iAsrRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdGrGr)
      fclose(fdGrGr);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdSale)
      fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fdAppr)
      fclose(fdAppr);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Sort output file
   //sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) B(%d,R)", iAsrRecLen, iAsrRecLen);
   //lRet = sortFile(acTmpFile, acOutFile, acTmp);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Tax matched:      %u", lTaxMatch);
   LogMsg("Number of Appr matched:     %u\n", lApprMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Number of Tax skiped:       %u", lTaxSkip);
   LogMsg("Number of no Appr matched:  %u\n", lBkPgNoAppr);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u", lCnt);

   lAssrRecCnt = lCnt;
   return 0;
}

/******************************** MB_CreateAssr *****************************
 *
 *
 *
 ****************************************************************************/

int MB_CreateAssr(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   iApnLen = myCounty.iApnLen;
   if (!memcmp(myCounty.acCntyCode, "LAK", 3))
      bFixedApn = true;
   else
      bFixedApn = false;

   // Use TMP file only if output needs resort
   //sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   sprintf(acOutFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, "CO");

   // Open GrGr file
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open GrGr file %s", acTmpFile);
      fdGrGr = fopen(acTmpFile, "r");
      if (fdGrGr == NULL)
      {
         LogMsg("***** Error opening GrGr file: %s\n", acTmpFile);
         return 2;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }
   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return 2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return 2;
   }
   // Open Sales file - Need to be sorted on APN and EventDate
   LogMsg("Open Sales file %s", acSalesFile);
   sprintf(acTmpFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "SMP");
   lRet = sortFile(acSalesFile, acTmpFile, "S(#1,C,A,#3,C,A) F(TXT)");
   if (lRet <= 0)
   {
      LogMsg("***** Error sorting Sales file: %s -> %s\n", acSalesFile, acTmpFile);
      return 2;
   }
   fdSale = fopen(acTmpFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acTmpFile);
      return 2;
   }
   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "EMP");
   lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) F(TXT)");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return 2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return 4;

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iAsrRecLen);
      bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
   }

   // Drop header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (acRec[0] == '-')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   lSitusMatch=lCharMatch=lExeMatch=lSaleMatch=lTaxMatch = 0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new R01 record
      iRet = Asr_MergeRoll(acBuf, acRec, iAsrRecLen, CREATE_R01|CREATE_LIEN);

      // Merge Situs
      if (fdSitus)
         lRet = Asr_MergeSitus(acBuf);

      // Merge Exe
      if (fdExe)
         lRet = Asr_MergeExe(acBuf);

      // Merge Char
      if (fdChar)
         lRet = Asr_MergeStdChar(acBuf);

      // Merge Sales
      if (fdSale)
         lRet = Asr_MergeSale(acBuf);

      // Merge Tax
      if (fdTax)
         lRet = Asr_MergeTax(acBuf);

      // Merge GrGr
      if (fdGrGr)
         lRet = Asr_MergeGrGr(acBuf);

      if (!iRet)
      {
         iNewRec++;
#ifdef _DEBUG
         iRet = replChar(acBuf, 0, ' ', iAsrRecLen);
         if (iRet)
            iRet = 0;
#endif
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lCnt++;
         sprintf(acTmp, "%*d", SIZ_3K_RECNO, lCnt);
         memcpy((char *)&acBuf[OFF_3K_RECNO], acTmp, SIZ_3K_RECNO);

         bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
         if (!(lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdGrGr)
      fclose(fdGrGr);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdSale)
      fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);

   // Sort output file
   //LogMsg("Sort output file %s to %s", acTmpFile, acOutFile);
   //sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) B(%d,R)", iAsrRecLen, iAsrRecLen);
   //lRet = sortFile(acTmpFile, acOutFile, acTmp);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Tax matched:      %u", lTaxMatch);
   LogMsg("Number of GrGr matched:     %u\n", lGrGrMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lCnt);

   lAssrRecCnt = lCnt;
   return 0;
}

/******************************** Asr_MergeTR601 *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Asr_MergeTR601(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256];
   long     lTmp, lLand, lImpr, lHomeSite, lGrow, lFixt, lFixtRp, lFixtBus,lPPMh, lGross;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace tab char with 0
   iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Asr_MergeRoll(): bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_ROLL_ASMT],"002396012", 9))
   //   iTmp = 0;
#endif

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iAsrRecLen);

      // County code 
      memcpy(pOutbuf+OFF_3K_CO_ID, myCounty.acCntyCode, 3);
      memcpy(pOutbuf+OFF_3K_CO_NUM, myCounty.acCntyID, 2);

      // Asmt, fee parcel
      memcpy(pOutbuf+OFF_3K_ASSMT, apTokens[L_ASMT], strlen(apTokens[L_ASMT]));
      memcpy(pOutbuf+OFF_3K_FEE_PRCL, apTokens[L_FEEPARCEL], strlen(apTokens[L_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[L_ASMT], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_3K_APN_DASH, acTmp, iRet);

      // TRA
      memcpy(pOutbuf+OFF_3K_TRA, apTokens[L_TRA], strlen(apTokens[L_TRA]));

      // Legal
      if (*apTokens[L_PARCELDESCRIPTION] > ' ')
      {
         iTmp = strlen(apTokens[L_PARCELDESCRIPTION]);
         if (iTmp > SIZ_3K_DESC)
            iTmp = SIZ_3K_DESC;
         memcpy(pOutbuf+OFF_3K_DESC, apTokens[L_PARCELDESCRIPTION], iTmp);
      }
      
      // Land
      lLand = atol(apTokens[L_CURRENTMARKETLANDVALUE]);
      sprintf(acTmp, "%*d", SIZ_3K_LAND, lLand);
      memcpy(pOutbuf+OFF_3K_LAND, acTmp, SIZ_3K_LAND);
      memcpy(pOutbuf+OFF_3K_CLAND, acTmp, SIZ_3K_LAND);

      // HomeSite
      lHomeSite=0;
      //lHomeSite = atol(apTokens[MB_ROLL_HOMESITE]);
      //sprintf(acTmp, "%*d", SIZ_3K_IMPR, lHomeSite);
      //memcpy(pOutbuf+OFF_3K_HOMESITE, acTmp, SIZ_3K_IMPR);
      //memcpy(pOutbuf+OFF_3K_CHOMESITE, acTmp, SIZ_3K_IMPR);

      // Improve
      lImpr = atol(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lImpr);
      memcpy(pOutbuf+OFF_3K_IMPR, acTmp, SIZ_3K_IMPR);
      memcpy(pOutbuf+OFF_3K_CIMPR, acTmp, SIZ_3K_IMPR);

      // Grow Improve
      lGrow = atol(apTokens[L_CURRENTGROWINGIMPRVALUE]);
      sprintf(acTmp, "%*d", SIZ_3K_GROW, lGrow);
      memcpy(pOutbuf+OFF_3K_GROW, acTmp, SIZ_3K_GROW);
      memcpy(pOutbuf+OFF_3K_CGROW, acTmp, SIZ_3K_GROW);

      // Fixtures 
      lFixt = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixt);
      memcpy(pOutbuf+OFF_3K_FIXT, acTmp, SIZ_3K_IMPR);
      memcpy(pOutbuf+OFF_3K_CFIXT, acTmp, SIZ_3K_IMPR);

      // Fixtures real prop
      lFixtRp = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixtRp);
      memcpy(pOutbuf+OFF_3K_FIXT_RP, acTmp, SIZ_3K_IMPR);
      memcpy(pOutbuf+OFF_3K_CFIXT_RP, acTmp, SIZ_3K_IMPR);

      // Bus. pers prop
      lFixtBus=0;
      //lFixtBus = atoi(apTokens[MB_ROLL_FIXTR_BUS]);
      //sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixtBus);
      //memcpy(pOutbuf+OFF_3K_PERS, acTmp, SIZ_3K_IMPR);
      //memcpy(pOutbuf+OFF_3K_CPERS, acTmp, SIZ_3K_IMPR);

      // Mobile home pers prop
      lPPMh = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lPPMh);
      memcpy(pOutbuf+OFF_3K_MHPERS, acTmp, SIZ_3K_IMPR);
      memcpy(pOutbuf+OFF_3K_CMHPERS, acTmp, SIZ_3K_IMPR);
   }

   // Gross
   lTmp = lLand+lImpr;
   lGross = lTmp+lHomeSite+lGrow+lFixt+lFixtRp+lFixtBus+lPPMh;
   sprintf(acTmp, "%*d", SIZ_3K_GROSS, lGross);
   memcpy(pOutbuf+OFF_3K_GROSS, acTmp, SIZ_3K_GROSS);

   // Land+Impr
   sprintf(acTmp, "%*d", SIZ_3K_IMPR, lTmp);
   memcpy(pOutbuf+OFF_3K_LANDIMP, acTmp, SIZ_3K_IMPR);

   // Impr%
   if (lTmp > 0 && lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_IMPPERC, 100*(lImpr/lTmp));
      memcpy(pOutbuf+OFF_3K_IMPPERC, acTmp, SIZ_3K_IMPPERC);
   }

   // Acres
   dTmp = atof(apTokens[L_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%.*ld", SIZ_3K_INACRES, (long)(dTmp * 100.0001));
      memcpy(pOutbuf+OFF_3K_INACRES, acTmp, SIZ_3K_INACRES);
      sprintf(acTmp, "%.*s%s", SIZ_3K_DRV_ACRES-strlen(apTokens[L_ACRES]), BLANK32, apTokens[L_ACRES]);
      memcpy(pOutbuf+OFF_3K_DRV_ACRES, acTmp, SIZ_3K_DRV_ACRES);
      sprintf(acTmp, "%*ld", SIZ_3K_LOTSQFT, (long)(dTmp * SQFT_PER_ACRE));
      memcpy(pOutbuf+OFF_3K_LOTSQFT, acTmp, SIZ_3K_LOTSQFT);
   }

   // Land $ per acres
   // Land $ per sqft
   // Impr $ per sqft

   // Zoning
   //if (*apTokens[MB_ROLL_ZONING] > ' ')
   //{
   //   iTmp = strlen(apTokens[MB_ROLL_ZONING]);
   //   if (iTmp > SIZ_3K_ZONE)
   //      iTmp = SIZ_3K_ZONE;
   //   memcpy(pOutbuf+OFF_3K_ZONE, apTokens[MB_ROLL_ZONING], iTmp);
   //}

   // UseCode
   iTmp = strlen(apTokens[L_USECODE]);
   if (iTmp > SIZ_3K_USE)
      iTmp = SIZ_3K_USE;
   memcpy(pOutbuf+OFF_3K_USE, apTokens[L_USECODE], iTmp);

   // Neighborhood code
   //iTmp = strlen(apTokens[MB_ROLL_NBHCODE]);
   //if (iTmp > SIZ_3K_NBHCD)
   //   iTmp = SIZ_3K_NBHCD;
   //memcpy(pOutbuf+OFF_3K_NBHCD, apTokens[MB_ROLL_NBHCODE], iTmp);

   // Recorded Doc
   /*
   if (*apTokens[MB_ROLL_DOCNUM] > ' ')
   {
      if (bFmtDoc)
      {
         lTmp = atoin(apTokens[MB_ROLL_DOCNUM]+5, 7);
         iTmp = sprintf(acTmp, "%.5s%0.7ld", apTokens[MB_ROLL_DOCNUM], lTmp);
      } else
      {
         strcpy(acTmp, apTokens[MB_ROLL_DOCNUM]);
         blankPad(acTmp, SIZ_3K_RDOCNUM);
      }
      memcpy(pOutbuf+OFF_3K_RDOCNUM, acTmp, SIZ_3K_RDOCNUM);
   } else
      memcpy(pOutbuf+OFF_3K_RDOCNUM, BLANK32, SIZ_3K_RDOCNUM);
   
   // Recording date
   pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      memcpy(pOutbuf+OFF_3K_RDOCDT, acTmp, strlen(acTmp));
   */

   // Fix APN
   if (!bFixedApn)
      memset(pOutbuf+OFF_3K_OUTFIXAPN, ' ', SIZ_3K_OUTFIXAPN);
   else
   {
      // Create fixed APN - implement for LAK only
   }

   // Tax in full
   memcpy(pOutbuf+OFF_3K_TAXFULL, apTokens[L_TAXABILITY], strlen(apTokens[L_TAXABILITY]));

   // Owner
   memset(pOutbuf+OFF_3K_NAME, ' ', (OFF_3K_M_STR)-(OFF_3K_NAME));
   memcpy(pOutbuf+OFF_3K_NAME, apTokens[L_OWNER], strlen(apTokens[L_OWNER]));

   // "C/OCANNERY CO","765 WAVE ST","MONTEREY CA 93940-1016"
   // "ATTN MIKE ZIMMERMAN","DBA SPINDRIFT INN","765 WAVE ST","MONTEREY CA 93940"
   memcpy(pOutbuf+OFF_3K_CAREOF, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));
   memcpy(pOutbuf+OFF_3K_DBA, apTokens[MB_ROLL_DBA], strlen(apTokens[MB_ROLL_DBA]));

   // Mailing
   /*
   memset(pOutbuf+OFF_3K_M_STR, ' ', (OFF_3K_S_STR)-(OFF_3K_M_STR));
   memset(pOutbuf+OFF_3K_FM_STRNO, ' ', (OFF_3K_ADRFLG)-(OFF_3K_FM_STRNO));
   if (*apTokens[MB_ROLL_M_ADDR] > ' ')
   {
      memcpy(pOutbuf+OFF_3K_M_STR, apTokens[MB_ROLL_M_ADDR], strlen(apTokens[MB_ROLL_M_ADDR]));
      memcpy(pOutbuf+OFF_3K_M_CITY, apTokens[MB_ROLL_M_CITY], strlen(apTokens[MB_ROLL_M_CITY]));
      memcpy(pOutbuf+OFF_3K_M_ST, apTokens[MB_ROLL_M_ST], strlen(apTokens[MB_ROLL_M_ST]));
      memcpy(pOutbuf+OFF_3K_M_ZIP, apTokens[MB_ROLL_M_ZIP], strlen(apTokens[MB_ROLL_M_ZIP]));

      Asr_MergeMAdr(pOutbuf);
      *(pOutbuf+OFF_3K_ADRFLG) = '1';
   } else
   {
      *(pOutbuf+OFF_3K_ADRFLG) = ' ';
   }
   */
   // Format owner
   //Asr_MergeOwner(pOutbuf, apTokens[L_OWNER]);
   return 0;
}

/***************************** Asr_MergeTR601_TD *****************************
 *
 * Same as previous function, but for tab delimited input
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Asr_MergeTR601_TD(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256];
   long     lTmp, lLand, lImpr, lHomeSite, lGrow, lFixt, lFixtRp, lFixtBus,lPPMh, lGross;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with blank
   iRet = replNull(pRollRec, ' ', 0);
   //iRet = replChar(pRollRec, 0, ' ', 0);
   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Asr_MergeTR601_TD(): bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_ROLL_ASMT],"002396012", 9))
   //   iTmp = 0;
#endif

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iAsrRecLen);

      // County code 
      memcpy(pOutbuf+OFF_3K_CO_ID, myCounty.acCntyCode, 3);
      memcpy(pOutbuf+OFF_3K_CO_NUM, myCounty.acCntyID, 2);

      // Asmt, fee parcel
      memcpy(pOutbuf+OFF_3K_ASSMT, apTokens[L_ASMT], strlen(apTokens[L_ASMT]));
      memcpy(pOutbuf+OFF_3K_FEE_PRCL, apTokens[L_FEEPARCEL], strlen(apTokens[L_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[L_ASMT], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_3K_APN_DASH, acTmp, iRet);

      // TRA
      memcpy(pOutbuf+OFF_3K_TRA, apTokens[L_TRA], strlen(apTokens[L_TRA]));

      // Legal
      if (*apTokens[L_PARCELDESCRIPTION] > ' ')
      {
         iTmp = strlen(apTokens[L_PARCELDESCRIPTION]);
         if (iTmp > SIZ_3K_DESC)
            iTmp = SIZ_3K_DESC;
         memcpy(pOutbuf+OFF_3K_DESC, apTokens[L_PARCELDESCRIPTION], iTmp);
      }
      
      // Land
      lLand = atol(apTokens[L_CURRENTMARKETLANDVALUE]);
      sprintf(acTmp, "%*d", SIZ_3K_LAND, lLand);
      memcpy(pOutbuf+OFF_3K_LAND, acTmp, SIZ_3K_LAND);
      memcpy(pOutbuf+OFF_3K_CLAND, acTmp, SIZ_3K_LAND);

      // HomeSite
      lHomeSite=0;
      //lHomeSite = atol(apTokens[MB_ROLL_HOMESITE]);
      //sprintf(acTmp, "%*d", SIZ_3K_IMPR, lHomeSite);
      //memcpy(pOutbuf+OFF_3K_HOMESITE, acTmp, SIZ_3K_IMPR);
      //memcpy(pOutbuf+OFF_3K_CHOMESITE, acTmp, SIZ_3K_IMPR);

      // Improve
      lImpr = atol(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lImpr);
      memcpy(pOutbuf+OFF_3K_IMPR, acTmp, SIZ_3K_IMPR);
      memcpy(pOutbuf+OFF_3K_CIMPR, acTmp, SIZ_3K_IMPR);

      // Grow Improve
      lGrow = atol(apTokens[L_CURRENTGROWINGIMPRVALUE]);
      sprintf(acTmp, "%*d", SIZ_3K_GROW, lGrow);
      memcpy(pOutbuf+OFF_3K_GROW, acTmp, SIZ_3K_GROW);
      memcpy(pOutbuf+OFF_3K_CGROW, acTmp, SIZ_3K_GROW);

      // Fixtures 
      lFixt = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixt);
      memcpy(pOutbuf+OFF_3K_FIXT, acTmp, SIZ_3K_IMPR);
      memcpy(pOutbuf+OFF_3K_CFIXT, acTmp, SIZ_3K_IMPR);

      // Fixtures real prop
      lFixtRp = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixtRp);
      memcpy(pOutbuf+OFF_3K_FIXT_RP, acTmp, SIZ_3K_IMPR);
      memcpy(pOutbuf+OFF_3K_CFIXT_RP, acTmp, SIZ_3K_IMPR);

      // Bus. pers prop
      lFixtBus=0;
      //lFixtBus = atoi(apTokens[MB_ROLL_FIXTR_BUS]);
      //sprintf(acTmp, "%*d", SIZ_3K_IMPR, lFixtBus);
      //memcpy(pOutbuf+OFF_3K_PERS, acTmp, SIZ_3K_IMPR);
      //memcpy(pOutbuf+OFF_3K_CPERS, acTmp, SIZ_3K_IMPR);

      // Mobile home pers prop
      lPPMh = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);
      sprintf(acTmp, "%*d", SIZ_3K_IMPR, lPPMh);
      memcpy(pOutbuf+OFF_3K_MHPERS, acTmp, SIZ_3K_IMPR);
      memcpy(pOutbuf+OFF_3K_CMHPERS, acTmp, SIZ_3K_IMPR);
   }

   // Gross
   lTmp = lLand+lImpr;
   lGross = lTmp+lHomeSite+lGrow+lFixt+lFixtRp+lFixtBus+lPPMh;
   sprintf(acTmp, "%*d", SIZ_3K_GROSS, lGross);
   memcpy(pOutbuf+OFF_3K_GROSS, acTmp, SIZ_3K_GROSS);

   // Land+Impr
   sprintf(acTmp, "%*d", SIZ_3K_IMPR, lTmp);
   memcpy(pOutbuf+OFF_3K_LANDIMP, acTmp, SIZ_3K_IMPR);

   // Impr%
   if (lTmp > 0 && lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_3K_IMPPERC, 100*(lImpr/lTmp));
      memcpy(pOutbuf+OFF_3K_IMPPERC, acTmp, SIZ_3K_IMPPERC);
   }

   // Acres
   dTmp = atof(apTokens[L_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%.*ld", SIZ_3K_INACRES, (long)(dTmp * 100.0001));
      memcpy(pOutbuf+OFF_3K_INACRES, acTmp, SIZ_3K_INACRES);
      sprintf(acTmp, "%.*s%s", SIZ_3K_DRV_ACRES-strlen(apTokens[L_ACRES]), BLANK32, apTokens[L_ACRES]);
      memcpy(pOutbuf+OFF_3K_DRV_ACRES, acTmp, SIZ_3K_DRV_ACRES);
      sprintf(acTmp, "%*ld", SIZ_3K_LOTSQFT, (long)(dTmp * SQFT_PER_ACRE));
      memcpy(pOutbuf+OFF_3K_LOTSQFT, acTmp, SIZ_3K_LOTSQFT);
   }

   // Land $ per acres
   // Land $ per sqft
   // Impr $ per sqft

   // Zoning
   //if (*apTokens[MB_ROLL_ZONING] > ' ')
   //{
   //   iTmp = strlen(apTokens[MB_ROLL_ZONING]);
   //   if (iTmp > SIZ_3K_ZONE)
   //      iTmp = SIZ_3K_ZONE;
   //   memcpy(pOutbuf+OFF_3K_ZONE, apTokens[MB_ROLL_ZONING], iTmp);
   //}

   // UseCode
   iTmp = strlen(apTokens[L_USECODE]);
   if (iTmp > SIZ_3K_USE)
      iTmp = SIZ_3K_USE;
   memcpy(pOutbuf+OFF_3K_USE, apTokens[L_USECODE], iTmp);

   // Neighborhood code
   //iTmp = strlen(apTokens[MB_ROLL_NBHCODE]);
   //if (iTmp > SIZ_3K_NBHCD)
   //   iTmp = SIZ_3K_NBHCD;
   //memcpy(pOutbuf+OFF_3K_NBHCD, apTokens[MB_ROLL_NBHCODE], iTmp);

   // Recorded Doc
   /*
   if (*apTokens[MB_ROLL_DOCNUM] > ' ')
   {
      if (bFmtDoc)
      {
         lTmp = atoin(apTokens[MB_ROLL_DOCNUM]+5, 7);
         iTmp = sprintf(acTmp, "%.5s%0.7ld", apTokens[MB_ROLL_DOCNUM], lTmp);
      } else
      {
         strcpy(acTmp, apTokens[MB_ROLL_DOCNUM]);
         blankPad(acTmp, SIZ_3K_RDOCNUM);
      }
      memcpy(pOutbuf+OFF_3K_RDOCNUM, acTmp, SIZ_3K_RDOCNUM);
   } else
      memcpy(pOutbuf+OFF_3K_RDOCNUM, BLANK32, SIZ_3K_RDOCNUM);
   
   // Recording date
   pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
   if (pTmp)
      memcpy(pOutbuf+OFF_3K_RDOCDT, acTmp, strlen(acTmp));
   */

   // Fix APN
   if (!bFixedApn)
      memset(pOutbuf+OFF_3K_OUTFIXAPN, ' ', SIZ_3K_OUTFIXAPN);
   else
   {
      // Create fixed APN - implement for LAK only
   }

   // Tax in full
   memcpy(pOutbuf+OFF_3K_TAXFULL, apTokens[L_TAXABILITY], strlen(apTokens[L_TAXABILITY]));

   // Owner
   memset(pOutbuf+OFF_3K_NAME, ' ', (OFF_3K_M_STR)-(OFF_3K_NAME));
   memcpy(pOutbuf+OFF_3K_NAME, apTokens[L_OWNER], strlen(apTokens[L_OWNER]));

   // "C/OCANNERY CO","765 WAVE ST","MONTEREY CA 93940-1016"
   // "ATTN MIKE ZIMMERMAN","DBA SPINDRIFT INN","765 WAVE ST","MONTEREY CA 93940"
   memcpy(pOutbuf+OFF_3K_CAREOF, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));
   memcpy(pOutbuf+OFF_3K_DBA, apTokens[MB_ROLL_DBA], strlen(apTokens[MB_ROLL_DBA]));

   // Mailing
   /*
   memset(pOutbuf+OFF_3K_M_STR, ' ', (OFF_3K_S_STR)-(OFF_3K_M_STR));
   memset(pOutbuf+OFF_3K_FM_STRNO, ' ', (OFF_3K_ADRFLG)-(OFF_3K_FM_STRNO));
   if (*apTokens[MB_ROLL_M_ADDR] > ' ')
   {
      memcpy(pOutbuf+OFF_3K_M_STR, apTokens[MB_ROLL_M_ADDR], strlen(apTokens[MB_ROLL_M_ADDR]));
      memcpy(pOutbuf+OFF_3K_M_CITY, apTokens[MB_ROLL_M_CITY], strlen(apTokens[MB_ROLL_M_CITY]));
      memcpy(pOutbuf+OFF_3K_M_ST, apTokens[MB_ROLL_M_ST], strlen(apTokens[MB_ROLL_M_ST]));
      memcpy(pOutbuf+OFF_3K_M_ZIP, apTokens[MB_ROLL_M_ZIP], strlen(apTokens[MB_ROLL_M_ZIP]));

      Asr_MergeMAdr(pOutbuf);
      *(pOutbuf+OFF_3K_ADRFLG) = '1';
   } else
   {
      *(pOutbuf+OFF_3K_ADRFLG) = ' ';
   }
   */
   // Format owner
   //Asr_MergeOwner(pOutbuf, apTokens[L_OWNER]);
   return 0;
}

/***************************** MB_CreateAssrTR601 ***************************
 *
 * Create assessor file using TR601 file layout
 *
 ****************************************************************************/

int MB_CreateAssrTR601(int iFirstRec, int iInputType)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   LogMsg("Loading TR601 file for %s", myCounty.acCntyCode);

   iApnLen = myCounty.iApnLen;
   if (!memcmp(myCounty.acCntyCode, "LAK", 3))
      bFixedApn = true;
   else
      bFixedApn = false;

   // Use TMP file only if output needs resort
   sprintf(acOutFile, acRawTmplS, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iAsrRecLen);
      bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (!feof(fdRoll))
   {
      // Create new R01 record
      if (iInputType == 0)
         iRet = Asr_MergeTR601(acBuf, acRec, iAsrRecLen, CREATE_R01|CREATE_LIEN);
      else
         iRet = Asr_MergeTR601_TD(acBuf, acRec, iAsrRecLen, CREATE_R01|CREATE_LIEN);

      if (!iRet)
      {
         iNewRec++;
         lCnt++;
         sprintf(acTmp, "%*d", SIZ_3K_RECNO, lCnt);
         memcpy((char *)&acBuf[OFF_3K_RECNO], acTmp, SIZ_3K_RECNO);

         bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
         if (!(lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp || acRec[iSkipQuote] > '9')
         break;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   printf("\nTotal output records: %u\n", lCnt);

   lAssrRecCnt = lCnt;
   return 0;
}

