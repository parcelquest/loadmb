#ifndef  _MERGEAMA_H_
#define  _MERGEAMA_H_

#define  AMA_CHAR_FEEPRCL                 0
#define  AMA_CHAR_DOCNUM                  1
#define  AMA_CHAR_CATTYPE                 2
#define  AMA_CHAR_BLDGSEQNO               3
#define  AMA_CHAR_UNITSEQNO               4
#define  AMA_CHAR_YRBLT                   5
#define  AMA_CHAR_BUILDINGTYPE            6
#define  AMA_CHAR_EFFYR                   7
#define  AMA_CHAR_BUILDINGSIZE            8
#define  AMA_CHAR_BUILDINGUSEDFOR         9
#define  AMA_CHAR_STORIESCNT              10
#define  AMA_CHAR_UNITSCNT                11
#define  AMA_CHAR_CONDITION               12
#define  AMA_CHAR_BEDROOMS                13
#define  AMA_CHAR_BATHROOMS               14
#define  AMA_CHAR_QUALITYCLASS            15
#define  AMA_CHAR_HALFBATHS               16
#define  AMA_CHAR_CONSTRUCTION            17
#define  AMA_CHAR_FOUNDATION              18
#define  AMA_CHAR_STRUCTURALFRAME         19
#define  AMA_CHAR_STRUCTURALFLOOR         20
#define  AMA_CHAR_EXTERIORTYPE            21
#define  AMA_CHAR_ROOFCOVER               22
#define  AMA_CHAR_ROOFTYPEFLAT            23
#define  AMA_CHAR_ROOFTYPEHIP             24
#define  AMA_CHAR_ROOFTYPEGABLE           25
#define  AMA_CHAR_ROOFTYPESHED            26
#define  AMA_CHAR_INSULATIONCEILINGS      27
#define  AMA_CHAR_INSULATIONWALLS         28
#define  AMA_CHAR_INSULATIONFLOORS        29
#define  AMA_CHAR_WINDOWPANESINGLE        30
#define  AMA_CHAR_WINDOWPANEDOUBLE        31
#define  AMA_CHAR_WINDOWPANETRIPLE        32
#define  AMA_CHAR_WINDOWTYPE              33
#define  AMA_CHAR_LIGHTING                34
#define  AMA_CHAR_COOLINGCENTRALAC        35
#define  AMA_CHAR_COOLINGEVAPORATIVE      36
#define  AMA_CHAR_COOLINGROOMWALL         37
#define  AMA_CHAR_COOLINGWINDOW           38
#define  AMA_CHAR_HEATING                 39
#define  AMA_CHAR_FIREPLACE               40
#define  AMA_CHAR_GARAGE                  41
#define  AMA_CHAR_PLUMBING                42
#define  AMA_CHAR_SOLAR                   43
#define  AMA_CHAR_BUILDER                 44
#define  AMA_CHAR_BLDGDESIGNEDFOR         45
#define  AMA_CHAR_MODELDESC               46
#define  AMA_CHAR_UNFINAREASSF            47
#define  AMA_CHAR_ATTACHGARAGESF          48
#define  AMA_CHAR_DETACHGARAGESF          49
#define  AMA_CHAR_CARPORTSF               50
#define  AMA_CHAR_DECKSSF                 51
#define  AMA_CHAR_PATIOSF                 52
#define  AMA_CHAR_CEILINGHEIGHT           53
#define  AMA_CHAR_FIRESPINKLERS           54
#define  AMA_CHAR_AVGWALLHEIGHT           55
#define  AMA_CHAR_BAY                     56
#define  AMA_CHAR_DOCK                    57
#define  AMA_CHAR_ELEVATOR                58
#define  AMA_CHAR_ESCALATOR               59
#define  AMA_CHAR_ROLLUPDOOR              60
#define  AMA_CHAR_FIELD1                  61
#define  AMA_CHAR_FIELD2                  62
#define  AMA_CHAR_FIELD3                  63
#define  AMA_CHAR_FIELD4                  64
#define  AMA_CHAR_FIELD5                  65
#define  AMA_CHAR_FIELD6                  66
#define  AMA_CHAR_FIELD7                  67
#define  AMA_CHAR_FIELD8                  68
#define  AMA_CHAR_FIELD9                  69
#define  AMA_CHAR_FIELD10                 70
#define  AMA_CHAR_FIELD11                 71
#define  AMA_CHAR_FIELD12                 72
#define  AMA_CHAR_FIELD13                 73
#define  AMA_CHAR_FIELD14                 74
#define  AMA_CHAR_FIELD15                 75
#define  AMA_CHAR_FIELD16                 76
#define  AMA_CHAR_FIELD17                 77
#define  AMA_CHAR_FIELD18                 78
#define  AMA_CHAR_FIELD19                 79
#define  AMA_CHAR_FIELD20                 80
#define  AMA_CHAR_LANDFIELD1              81
#define  AMA_CHAR_LANDFIELD2              82
#define  AMA_CHAR_LANDFIELD3              83
#define  AMA_CHAR_LANDFIELD4              84
#define  AMA_CHAR_LANDFIELD5              85
#define  AMA_CHAR_LANDFIELD6              86
#define  AMA_CHAR_LANDFIELD7              87
#define  AMA_CHAR_LANDFIELD8              88
#define  AMA_CHAR_ACRES                   89
#define  AMA_CHAR_NEIGHBORHOODCODE        90
#define  AMA_CHAR_ZONING                  91
#define  AMA_CHAR_TOPOGRAPHY              92
#define  AMA_CHAR_VIEWCODE                93
#define  AMA_CHAR_POOLSPA                 94
#define  AMA_CHAR_WATERSOURCE             95
#define  AMA_CHAR_SUBDIVNAME              96
#define  AMA_CHAR_SEWERCODE               97
#define  AMA_CHAR_UTILITIESCODE           98
#define  AMA_CHAR_ACCESSCODE              99
#define  AMA_CHAR_LANDSCAPE               100
#define  AMA_CHAR_PROBLEMCODE             101
#define  AMA_CHAR_FRONTAGE                102
#define  AMA_CHAR_LOCATION                103
#define  AMA_CHAR_PLANTEDACRES            104
#define  AMA_CHAR_ACRESUNUSEABLE          105
#define  AMA_CHAR_WATERSOURCEDOMESTIC     106
#define  AMA_CHAR_WATERSOURCEIRRIGATION   107
#define  AMA_CHAR_HOMESITES               108
#define  AMA_CHAR_PROPERTYCONDITIONCODE   109
#define  AMA_CHAR_ROADTYPE                110
#define  AMA_CHAR_HASWELL                 111
#define  AMA_CHAR_HASCOUNTYROAD           112
#define  AMA_CHAR_HASVINEYARD             113
#define  AMA_CHAR_ISUNSECUREDBUILDING     114
#define  AMA_CHAR_HASORCHARD              115
#define  AMA_CHAR_HASGROWINGIMPRV         116
#define  AMA_CHAR_SITECOVERAGE            117
#define  AMA_CHAR_PARKINGSPACES           118
#define  AMA_CHAR_EXCESSLANDSF            119
#define  AMA_CHAR_FRONTFOOTAGESF          120
#define  AMA_CHAR_MULTIPARCELECON         121
#define  AMA_CHAR_LANDUSECODE1            122
#define  AMA_CHAR_LANDUSECODE2            123
#define  AMA_CHAR_LANDSQFT                124
#define  AMA_CHAR_TOTALROOMS              125
#define  AMA_CHAR_NETLEASABLESF           126
#define  AMA_CHAR_BLDGFOOTPRINTSF         127
#define  AMA_CHAR_OFFICESPACESF           128
#define  AMA_CHAR_NONCONDITIONSF          129
#define  AMA_CHAR_MEZZANINESF             130
#define  AMA_CHAR_PERIMETERLF             131
#define  AMA_CHAR_ASMT                    132
#define  AMA_CHAR_ASMTCATEGORY            133
#define  AMA_CHAR_EVENTDATE               134
#define  AMA_CHAR_SALESPRICE              135      // Confirmed

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// I=Hospital
// S=School
// X=Misc exempt

IDX_TBL4 AMA_Exemption[] = 
{
   "E01", "H", 3,1,
   "E11", "V", 3,1,
   "E12", "X", 3,1,     // Aircraft
   "E13", "D", 3,1,
   "E14", "D", 3,1,
   "E15", "D", 3,1,
   "E20", "X", 3,1,
   "E21", "W", 3,1,     // WELFARE
   "E30", "V", 3,1,
   "E31", "C", 3,1,
   "E32", "R", 3,1,
   "E40", "W", 3,1,
   "E41", "W", 3,1,
   "E42", "W", 3,1,
   "E43", "W", 3,1,
   "E50", "C", 3,1,
   "E51", "P", 3,1,
   "E52", "P", 3,1,
   "E53", "M", 3,1,
   "E54", "P", 3,1,
   "E55", "P", 3,1,
   "E60", "X", 3,1,
   "E61", "E", 3,1,
   "E63", "E", 3,1,
   "E64", "E", 3,1,
   "E65", "E", 3,1,
   "E70", "M", 3,1,
   "E71", "W", 3,1,
   "E72", "W", 3,1,
   "E80", "R", 3,1,
   "E81", "C", 3,1,
   "E82", "C", 3,1,
   "E90", "R", 3,1,
   "E91", "R", 3,1,
   "E92", "R", 3,1,
   "E98", "X", 3,1,
   "","",0,0
};

#endif
