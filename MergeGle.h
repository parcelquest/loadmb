
#if !defined(AFX_MERGEGLE_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_)
#define AFX_MERGEGLE_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_

// Copy from LAS
#define  GLE_CHAR_FEEPARCEL            0
#define  GLE_CHAR_POOLSPA              1
#define  GLE_CHAR_CATTYPE              2
#define  GLE_CHAR_QUALITYCLASS         3
#define  GLE_CHAR_YRBLT                4
#define  GLE_CHAR_BUILDINGSIZE         5
#define  GLE_CHAR_ATTACHGARAGESF       6
#define  GLE_CHAR_DETACHGARAGESF       7
#define  GLE_CHAR_CARPORTSF            8
#define  GLE_CHAR_HEATING              9
#define  GLE_CHAR_COOLINGCENTRALAC     10
#define  GLE_CHAR_COOLINGEVAPORATIVE   11
#define  GLE_CHAR_COOLINGROOMWALL      12
#define  GLE_CHAR_COOLINGWINDOW        13
#define  GLE_CHAR_STORIESCNT           14
#define  GLE_CHAR_UNITSCNT             15
#define  GLE_CHAR_TOTALROOMS           16
#define  GLE_CHAR_EFFYR                17
#define  GLE_CHAR_PATIOSF              18
#define  GLE_CHAR_BEDROOMS             19
#define  GLE_CHAR_BATHROOMS            20
#define  GLE_CHAR_HALFBATHS            21
#define  GLE_CHAR_FIREPLACE            22
#define  GLE_CHAR_ASMT                 23
#define  GLE_CHAR_BLDGSEQNUM           24
#define  GLE_CHAR_HASWELL              25
#define  GLE_CHAR_LOTSQFT              26
#define  GLE_CHAR_PARKINGSPACES        27
#define  GLE_CHAR_FLDS                 28

//#define  GLE_CHAR_FEEPRCL                 0
//#define  GLE_CHAR_DOCNUM                  1
//#define  GLE_CHAR_CATTYPE                 2
//#define  GLE_CHAR_BLDGSEQNO               3
//#define  GLE_CHAR_UNITSEQNO               4
//#define  GLE_CHAR_YRBLT                   5
//#define  GLE_CHAR_BLDGTYPE                6
//#define  GLE_CHAR_EFFYR                   7
//#define  GLE_CHAR_BUILDINGSIZE            8
//#define  GLE_CHAR_BUILDINGUSEDFOR         9
//#define  GLE_CHAR_STORIESCNT              10
//#define  GLE_CHAR_UNITSCNT                11
//#define  GLE_CHAR_CONDITION               12
//#define  GLE_CHAR_BEDROOMS                13
//#define  GLE_CHAR_BATHROOMS               14
//#define  GLE_CHAR_QUALITYCLASS            15
//#define  GLE_CHAR_HALFBATHS               16
//#define  GLE_CHAR_CONSTRUCTION            17
//#define  GLE_CHAR_FOUNDATION              18
//#define  GLE_CHAR_STRUCTURALFRAME         19
//#define  GLE_CHAR_STRUCTURALFLOOR         20
//#define  GLE_CHAR_EXTERIORTYPE            21
//#define  GLE_CHAR_ROOFCOVER               22
//#define  GLE_CHAR_ROOFTYPEFLAT            23
//#define  GLE_CHAR_ROOFTYPEHIP             24
//#define  GLE_CHAR_ROOFTYPEGABLE           25
//#define  GLE_CHAR_ROOFTYPESHED            26
//#define  GLE_CHAR_INSULATIONCEILINGS      27
//#define  GLE_CHAR_INSULATIONWALLS         28
//#define  GLE_CHAR_INSULATIONFLOORS        29
//#define  GLE_CHAR_WINDOWPANESINGLE        30
//#define  GLE_CHAR_WINDOWPANEDOUBLE        31
//#define  GLE_CHAR_WINDOWPANETRIPLE        32
//#define  GLE_CHAR_WINDOWTYPE              33
//#define  GLE_CHAR_LIGHTING                34
//#define  GLE_CHAR_COOLINGCENTRALAC        35
//#define  GLE_CHAR_COOLINGEVAPORATIVE      36
//#define  GLE_CHAR_COOLINGROOMWALL         37
//#define  GLE_CHAR_COOLINGWINDOW           38
//#define  GLE_CHAR_HEATING                 39
//#define  GLE_CHAR_FIREPLACE               40
//#define  GLE_CHAR_GARAGE                  41
//#define  GLE_CHAR_PLUMBING                42
//#define  GLE_CHAR_SOLAR                   43
//#define  GLE_CHAR_BUILDER                 44
//#define  GLE_CHAR_BLDGDESIGNEDFOR         45
//#define  GLE_CHAR_MODELDESC               46
//#define  GLE_CHAR_UNFINAREASSF            47
//#define  GLE_CHAR_ATTACHGARAGESF          48
//#define  GLE_CHAR_DETACHGARAGESF          49
//#define  GLE_CHAR_CARPORTSF               50
//#define  GLE_CHAR_DECKSSF                 51
//#define  GLE_CHAR_PATIOSF                 52
//#define  GLE_CHAR_CEILINGHEIGHT           53
//#define  GLE_CHAR_FIRESPINKLERS           54
//#define  GLE_CHAR_AVGWALLHEIGHT           55
//#define  GLE_CHAR_BAY                     56
//#define  GLE_CHAR_DOCK                    57
//#define  GLE_CHAR_ELEVATOR                58
//#define  GLE_CHAR_ESCALATOR               59
//#define  GLE_CHAR_ROLLUPDOOR              60
//#define  GLE_CHAR_FIELD1                  61
//#define  GLE_CHAR_FIELD2                  62
//#define  GLE_CHAR_FIELD3                  63
//#define  GLE_CHAR_FIELD4                  64
//#define  GLE_CHAR_FIELD5                  65
//#define  GLE_CHAR_FIELD6                  66
//#define  GLE_CHAR_FIELD7                  67
//#define  GLE_CHAR_FIELD8                  68
//#define  GLE_CHAR_FIELD9                  69
//#define  GLE_CHAR_FIELD10                 70
//#define  GLE_CHAR_FIELD11                 71
//#define  GLE_CHAR_FIELD12                 72
//#define  GLE_CHAR_FIELD13                 73
//#define  GLE_CHAR_FIELD14                 74
//#define  GLE_CHAR_FIELD15                 75
//#define  GLE_CHAR_FIELD16                 76
//#define  GLE_CHAR_FIELD17                 77
//#define  GLE_CHAR_FIELD18                 78
//#define  GLE_CHAR_FIELD19                 79
//#define  GLE_CHAR_FIELD20                 80
//#define  GLE_CHAR_LANDFIELD1              81
//#define  GLE_CHAR_LANDFIELD2              82
//#define  GLE_CHAR_LANDFIELD3              83
//#define  GLE_CHAR_LANDFIELD4              84
//#define  GLE_CHAR_LANDFIELD5              85
//#define  GLE_CHAR_LANDFIELD6              86
//#define  GLE_CHAR_LANDFIELD7              87
//#define  GLE_CHAR_LANDFIELD8              88
//#define  GLE_CHAR_ACRES                   89
//#define  GLE_CHAR_NEIGHBORHOODCODE        90
//#define  GLE_CHAR_ZONING                  91
//#define  GLE_CHAR_TOPOGRAPHY              92
//#define  GLE_CHAR_VIEWCODE                93
//#define  GLE_CHAR_POOLSPA                 94
//#define  GLE_CHAR_WATERSOURCE             95
//#define  GLE_CHAR_SUBDIVNAME              96
//#define  GLE_CHAR_SEWERCODE               97
//#define  GLE_CHAR_UTILITIESCODE           98
//#define  GLE_CHAR_ACCESSCODE              99
//#define  GLE_CHAR_LANDSCAPE               100
//#define  GLE_CHAR_PROBLEMCODE             101
//#define  GLE_CHAR_FRONTAGE                102
//#define  GLE_CHAR_LOCATION                103
//#define  GLE_CHAR_PLANTEDACRES            104
//#define  GLE_CHAR_ACRESUNUSEABLE          105
//#define  GLE_CHAR_WATERSOURCEDOMESTIC     106
//#define  GLE_CHAR_WATERSOURCEIRRIGATION   107
//#define  GLE_CHAR_HOMESITES               108
//#define  GLE_CHAR_PROPERTYCONDITIONCODE   109
//#define  GLE_CHAR_ROADTYPE                110
//#define  GLE_CHAR_HASWELL                 111
//#define  GLE_CHAR_HASCOUNTYROAD           112
//#define  GLE_CHAR_HASVINEYARD             113
//#define  GLE_CHAR_ISUNSECUREDBUILDING     114
//#define  GLE_CHAR_HASORCHARD              115
//#define  GLE_CHAR_HASGROWINGIMPRV         116
//#define  GLE_CHAR_SITECOVERAGE            117
//#define  GLE_CHAR_PARKINGSPACES           118
//#define  GLE_CHAR_EXCESSLANDSF            119
//#define  GLE_CHAR_FRONTFOOTAGESF          120
//#define  GLE_CHAR_MULTIPARCELECON         121
//#define  GLE_CHAR_LANDUSECODE1            122
//#define  GLE_CHAR_LANDUSECODE2            123
//#define  GLE_CHAR_LANDSQFT                124
//#define  GLE_CHAR_TOTALROOMS              125
//#define  GLE_CHAR_NETLEASABLESF           126
//#define  GLE_CHAR_BLDGFOOTPRINTSF         127
//#define  GLE_CHAR_OFFICESPACESF           128
//#define  GLE_CHAR_NONCONDITIONSF          129
//#define  GLE_CHAR_MEZZANINESF             130
//#define  GLE_CHAR_PERIMETERLF             131
//#define  GLE_CHAR_ASMT                    132
//#define  GLE_CHAR_ASMTCATEGORY            133
//#define  GLE_CHAR_EVENTDATE               134
//#define  GLE_CHAR_SALESPRICE              135      // Confirmed
//#define  GLE_CHAR_CONFIRMATIONCODE        136

//#define  GLE_MH_FEEPARCEL		            0
//#define  GLE_MH_QUALCLS		               1
//#define  GLE_MH_YEARBUILT		            2
//#define  GLE_MH_BLDGGSIZE		            3
//#define  GLE_MH_NUMBEDROOMS		         4
//#define  GLE_MH_NUMFULLBATHS	            5
//#define  GLE_MH_ASMT				            6
//#define  GLE_MH_BLDGTYPE			         7

//static XLAT_CODE  asBldgType[] =
//{
//   // Value, lookup code, value length
//   "C", " ", 1,               // Concrete
//   "M", " ", 1,               // Masonry
//   "S", " ", 1,               // Steel
//   "T", " ", 1,               // Tilt up
//   "W", " ", 1,               // Wood
//   "",   "",  0
//};

static XLAT_CODE  asCond[] =
{
   // Value, lookup code, value length
   "A", "A", 1,               // Average
   "E", "E", 1,               // Excellent
   "F", "F", 1,               // Fair
   "G", "G", 1,               // Good
   "N", "N", 1,               // New
   "P", "P", 1,               // Poor
   "U", "U", 1,               // Unsound
   "",  "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "D1S", "2", 3,             // Double 1 story
   "D2S", "2", 3,             // Double 2 stories
   "DVG", "O", 3,             // Direct Vent Gas
   "FWS", "W", 3,             // Fireplace & Wood Stove
   "GSI", "G", 3,             // Gas Stove Insert
   "GS",  "G", 2,             // Gas Stove
   "PSI", "S", 3,             // Pellet Stove Insert
   "PS",  "S", 2,             // Pellet Stove
   "S1S", "1", 3,             // Single 1 story
   "S2S", "1", 3,             // Single 2 stories
   "WSI", "W", 3,             // Wood Stove Insert
   "WS",  "W", 2,             // Wood Stove
   "NON", "N", 3,             // None
   "ZO",  "Z", 2,             // Zero clearance
   "",   "", 0
};

static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "B",  "8", 1,              // Baseboard
   "C",  "3", 1,              // HVAC - Central
   "EC", "Z", 2,              // Electric - Central 
   "EHC","Z", 3,              // Electric Heating - Central
   "EH", "F", 2,              // Electric Heating
   "E",  "F", 1,              // Electric
   "F",  "C", 1,              // Floor
   "GC", "Z", 2,              // Gas - Central
   "GEC","Z", 3,              // Gas/Electric - Central
   "GE", "N", 2,              // Gas/Electric
   "G",  "N", 1,              // Gas
   "HPC","Z", 3,              // Heat Pump - Central
   "HP", "G", 2,              // Heat Pump
   "N",  "L", 1,              // None
   "W",  "D", 1,              // Wall
   "Z",  "T", 1,              // Zone
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "ABV", "X", 3,     	      // Above gound pool
   "BLT", "B", 3,     	      // Built-in pool
   "SPA", "S", 3,     	      // Spa
   "",   "", 0
};

static XLAT_CODE  asSewer[] =
{
   // Value, lookup code, value length
   "P", "P", 1,               // Public
   "S", "S", 1,               // Septic
   "",   "",  0
};

static XLAT_CODE  asWaterSrc[] =
{
   // Value, lookup code, value length
   "M", "P", 1,               // Municipal
   "W", "W", 1,               // Private Well
   "",  "",  0
};

IDX_TBL5 GLE_DocCode[] =
{  // DocCode, Index, Non-sale, DocCodelen, Indexlen
   "00", "74", 'Y', 2, 2,     // No Beneficial Change 100%
   "01", "1 ", 'N', 2, 2,     // General Sale 100%
   "02", "57", 'N', 2, 2,     // No Beneficial Change Partial Interest
   "03", "57", 'N', 2, 2,     // General Sale Partial Interest
   "04", "40", 'X', 2, 2,     // Easement
   "05", "75", 'Y', 2, 2,     // Property Settlement - Spouses
   "06", "75", 'Y', 2, 2,     // Property Settlement - Spouses
   "07", "75", 'Y', 2, 2,     // Transfer Solely Between Spouses
   "08", "75", 'Y', 2, 2,     // Transfer Solely Between Spouses
   "09", "74", 'Y', 2, 2,     // Split/Combo
   "10", "75", 'Y', 2, 2,     // Prop 58 100% - parent-child transfer
   "11", "75", 'Y', 2, 2,     // Prop 58 Partial Interest
   "12", "19", 'Y', 2, 2,     // Prop 60 - based year value transfer
   "13", "62", 'Y', 2, 2,     // Perfection of Title
   "14", "62", 'Y', 2, 2,     // Perfection of Title
   "15", "72", 'Y', 2, 2,     // Security Interest Transfer Only
   "16", "72", 'Y', 2, 2,     // Security Interest Transfer Only
   "17", "19", 'Y', 2, 2,     // Trust
   "18", "19", 'Y', 2, 2,     // Trust
   "19", "19", 'Y', 2, 2,     // Retained Life Estate
   "20", "19", 'Y', 2, 2,     // Retained Life Estate
   "21", "19", 'Y', 2, 2,     // Original Transfer Create/Delete
   "22", "19", 'Y', 2, 2,     // Original Transfer Create/Delete
   "23", "70", 'N', 2, 2,     // Lessors Interest 35 Years or More
   "24", "70", 'N', 2, 2,     // Lessors Interest 35 Years or More
   "25", "18", 'Y', 2, 2,     // Date of Death - Spouse 100%
   "26", "18", 'Y', 2, 2,     // Date of Death - Spouse 
   "27", "19", 'Y', 2, 2,     // Eminent Domain
   "28", "19", 'Y', 2, 2,     // Leasehold Estate
   "29", "19", 'Y', 2, 2,     // Leasehold Estate
   "30", "70", 'N', 2, 2,     // Lessors Interest
   "31", "70", 'N', 2, 2,     // Lessors Interest
   "32", "3 ", 'N', 2, 2,     // Joint Tenants
   "33", "3 ", 'N', 2, 2,     // Joint Tenants
   "34", "3 ", 'N', 2, 2,     // Tenants in Common
   "35", "3 ", 'N', 2, 2,     // Tenants in Common
   "36", "74", 'Y', 2, 2,     // Less Than a Fee Transfer
   "37", "74", 'Y', 2, 2,     // Less Than a Fee Transfer
   "38", "1 ", 'N', 2, 2,     // Community Property Right of Survivorship
   "39", "1 ", 'N', 2, 2,     // Community Property Right of Survivorship
   "40", "1 ", 'N', 2, 2,     // Legal Entities
   "41", "1 ", 'N', 2, 2,     // Legal Entities
   "42", "19", 'Y', 2, 2,     // Control of Legal Entities More than 50%
   "43", "19", 'Y', 2, 2,     // Control of Legal Entities More than 50%
   "44", "19", 'Y', 2, 2,     // Control of Legal Entities less than 50%
   "45", "19", 'Y', 2, 2,     // Control of Legal Entities less than 50%
   "46", "19", 'Y', 2, 2,     // Termination of Orig Transferor Interest
   "47", "19", 'Y', 2, 2,     // Termination of Orig Transferor Interest
   "48", "67", 'N', 2, 2,     // Sale of Tax Deeded Property
   "49", "67", 'N', 2, 2,     // Sale of Tax Deeded Property
   "50", "78", 'N', 2, 2,     // Foreclosure of Sale in Lieu of
   "51", "78", 'N', 2, 2,     // Foreclosure of Sale in Lieu of
   "52", "74", 'Y', 2, 2,     // Creation/Termination of Life Estate
   "53", "74", 'Y', 2, 2,     // Termination of Life Estate
   "54", "74", 'Y', 2, 2,     // Mineral Rights Only
   "95", "75", 'Y', 2, 2,     // Transfer on Death Deed
   "","",0,0,0
};

// Standard codes
// C=Church
// D=Disabled Veterans
// E=Cemetery
// H=Homeowners
// I=Hospital
// L=Library
// M=Museums
// P=Public School
// R=Religion
// S=School
// T=Tribal Housing
// U=College
// V=Veterans
// W=Welfare
// X=Misc exempt

IDX_TBL4 GLE_Exemption[] = 
{
   "E01", "H", 3,1,
   "E04", "X", 3,1,     // LESSOR'S UNSECURED
   "E05", "X", 3,1,     // LESSOR'S UNSECURED - REPORTING ONLY
   "E06", "X", 3,1,     // LESSOR'S
   "E20", "S", 3,1,
   "E21", "S", 3,1,
   "E30", "V", 3,1,
   "E31", "V", 3,1,
   "E40", "W", 3,1,
   "E41", "W", 3,1,
   "E42", "W", 3,1,
   "E43", "W", 3,1,
   "E44", "W", 3,1,
   "E45", "W", 3,1,
   "E50", "R", 3,1,
   "E51", "R", 3,1,
   "E52", "R", 3,1,
   "E53", "R", 3,1,
   "E54", "R", 3,1,
   "E55", "R", 3,1,
   "E56", "R", 3,1,
   "E57", "R", 3,1,
   "E58", "R", 3,1,
   "E59", "R", 3,1,
   "E60", "M", 3,1,
   "E61", "C", 3,1,
   "E62", "X", 3,1,     // BOE NOT OVER 40,000
   "E63", "D", 3,1,
   "E70", "E", 3,1,
   "E80", "D", 3,1,
   "E81", "D", 3,1,
   "E88", "C", 3,1,
   "E89", "C", 3,1,
   "E90", "C", 3,1,
   "E91", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E92", "X", 3,1,     // DOC VESSEL
   "E98", "X", 3,1,     // OTHER
   "E99", "X", 3,1,     // LOW VALUE PROPERTY EXEMPTION 
   "","",0,0
};


#endif // !defined(AFX_MERGEGLE_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_)
