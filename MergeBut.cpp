/**************************************************************************
 *
 * Use But_Values.csv for different value set such as base year, prop8, Adj base
 * for base year, restricted, CLCA, ...
 *
 * Output RC values for LDR table update 8/22/2013.  If RollChgNum in Tax file is
 * not empty, use current value from Roll file and exemption from Exe file.
 * 1. Create roll record with value from roll file
 * 2. Update record with Tax value from Tax file. Set RC flag if RollChgNum is not empty.
 * 3. If no change, update using LienExt values.  Otherwise, update RC flag in R01 rec
 * 4. Nightly update LDR tables with change from current roll where RC flag is set.
 *
 *
 * Options:
 *    -L   load lien
 *    -La  Create assessor data set using lien file
 *    -U   Load update
 *    -Ua  Update assessor data
 *    -G   Load GrGr (load grgr files from the county and extract
 *         Sale_exp.dat and But_GrGr.dat for DataEntry)
 *    -Xs  Extract sale and update sale history
 *    -Xsi Update sale history and import to SQL
 *    -Xv  Extract and create value import file
 *
 *    -U -Ua -Xsi -Xv (daily update)
 *
 *    Lien options:  -CBUT -L -La [-Xsi] -O [-Xc] [-G|-Mg] [-Xv]
 *    Daily options: -CBUT -U -Ua [-G|-Mg] -O [-Xsi] [-Xv]
 *
 * Revision
 * 09/01/2005 1.0.0    First version by Sony Nguyen
 *                     Adding last known sale fields (price, date, doc#, doctype)
 *                     Sale_Exp.dat will be added by MergeAdr to G01 file
 * 02/07/2006 1.3.11   Merge GrGr data into R01 and accumulate sale data.
 * 06/21/2006 1.7.1    Making APN translation optional.  Set ApnXlat=Y in INI file
 *                     to activate it.
 * 01/22/2007 1.8.1    If field Undisclosed in Document file is not 0, do not use
 *                     Tax value to calculate sale price.
 * 07/25/2007 1.9.9    Modify loadBut() for 2007 LDR processing.  Cum sale are
 *                     loaded during LDR run.  Regular sales and GrGr are updated
 *                     in daily processing.
 * 01/04/2008 1.10.1   Take out transfer update in But_MergeRoll() since this is
 *                     done by But_MergeSale() already.
 * 01/16/2008 1.10.2   Add code to support standard usecode
 * 01/21/2008 1.10.3   Add But_FmtDocLinks() and populate DocLinks if DocPath defined.
 * 02/25/2008 1.10.5   Add code to output update records.
 * 03/05/2008 1.10.6   Filter out all APN 100-999 except 910 & 911
 * 07/14/2008 8.2.5    BUT is now sending TR601 file format for LDR.  Add function
 *                     But_Load_LDR(), But_MergeLien(), But_MergeMAdr() and But_MergeSitus() for TR601 input format.
 * 08/13/2008 8.3.2    Merge GrGr data after update roll file
 * 03/30/2009 8.7.1    Use MB_MergeSale() to do sale update instead of MergeSale3() to fix
 *                     problem that non-sale transaction overwrites sale transaction that
 *                     occurs in the same day  since we only keep one transaction per day.
 * 05/21/2009 8.7.6    Remove reference to OldApn & NewApn files.  We no longer need them.
 * 06/08/2009 8.7.7    Support new input data format with multi-row header and '|' as dilimeter
 * 06/09/2009 8.7.8    Fix YrBlt.  Drop YrBlt 1900.  This is only a place holder.
 *                     Remove garbage in Zoning.
 * 08/06/2010 10.3.0   Convert cumsale format to SC format.  Save EXE_CD & TAXCODE.
 *                     Update TRANSFERS with data from roll file.
 * 08/20/2010 10.3.2   Add check for corrupted roll input file.  Overwrite EXE_TOTAL with GROSS
 *                     if it is greater than GROSS.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 03/25/2011 10.6.0   Add bGrGrAvail and send email notif on GrGr processing.
 * 06/01/2011 10.9.0   Exclude HomeSite from OtherVal.  Replace But_MergeExe() with MB_MergeExe()
 *                     and But_MergeTax() with MB_MergeTax().  Add -Xs option.
 * 06/14/2011 10.10.2  Sort tax file before calling MergeTax().
 * 08/08/2011 11.2.5   Add S_HSENO.  Use regular situs file to update LDR. Modify But_MergeSitus()
 *                     to capture StrSub better.
 * 08/15/2011 11.2.6   Modify MergeRoll() to include book 912 (OIL/GAS WELL) and remove 911 (OIL/GS PINELINE).
 * 08/03/2012 12.2.4   Fix CARE_OF issue in But_MergeMAdr(). Add Plu_MergeZone() to merge Zoning
 *                     from updated roll for LDR processing. Modify Plu_Load_LDR() to merge Zoning.
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 07/28/2013 13.7.8   Fix bug in But_MergeSitus() when first record has no APN.  Also fix swapname
 *                     in But_MergeOwner() when Name starts with number.
 * 08/06/2013 13.8.10  Only update DocLink when load roll file.
 * 08/26/2013 13.10.13 Add But_Load_Roll2() to replace But_Load_Roll(). This function will replace
 *                     LDR values with current values when RollChg flag is set.
 *            13.11.13 Add zipcode to M_CTY_ST_D.
 * 09/04/2013 13.12.1  Modify But_Load_Roll() and update value using MB_MergeTaxRC().
 * 10/01/2013 13.13.2  If DocNum from roll file looks funky, copy as is to TRANSFER_DOC.
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 11/08/2013 13.18.1  Test MB_ConvStdChar() and MB_MergeStdChar() to get them ready.
 * 11/27/2013 13.18.3  Modify But_FmtDocLinks() to format DocLink for local document access.
 * 12/09/2013 13.18.4  Add But_GetRec() to correct problem of incomplete record.
 *                     Modify But_MergeSale() and But_Load_Roll() using But_GetRec().
 * 01/12/2014 13.19.0  Add DocCode[] table. Remove sale update from But_Load_Roll() and
 *                     use ApplyCumSale() to update sale history.
 * 01/18/2014 13.20.0  Modify MergeOwner() not to parse name starts with number and
 *                     MergeRoll() to remove known bad char in legal.
 * 02/23/2014 13.20.4  If GRGR not available, keep processing other options.
 * 04/03/2014 13.22.3  Change Pool code from Jacuzzi to Spa
 * 04/17/2014 13.23.0  Add option to load value file.
 * 07/26/2014 14.0.6   To deal with embedded LF in input records, replace fgets() in But_Load_Roll(),
 *                     But_Load_LDR(), and But_MergeZone(). Also copy MB_CreateSCSale() to
 *                     But_CreateSCSale() and modify it to handle this problem in sale file.
 * 09/15/2014 14.4.1   Save value extract file name to acValueFile so it can be imported later.
 * 10/02/2014 14.5.0   Fix bug to skip import value when extract fail.
 * 10/26/2014 14.6.0   Modify But_ImportGrGr() to import reference data.  Add But_ExtrGrGr()
 *                     to create But_Grgr.sls (1152-byte). But_ExtrGrGr() needs translation DocTitle to DocType.
 * 10/29/2014 14.7.0   When running with -G without -U, updare R01 only when the file is ready.
 * 12/04/2014 14.10.0  Add But_MakeDocLink() and replace But_FmtDocLink() with UpdateDocLinks().
 *                     Remove option to create/update CDASSR file.
 * 12/10/2014 14.10.1  Fix bug when GrGr file name contains space, OTSORT requires double
 *                     quote around file name. Modify But_ImportGrGr() and But_ImportNames().
 * 03/13/2015 14.15.0  Use updateDocLinks() version from R01.cpp
 * 04/24/2015 14.15.3  Remove duplicate record in Value file. Add original VST code to Reason.
 * 04/29/2015 14.15.5  Use MB_ExtrValues() to extract value for import.
 * 05/26/2015 14.16.0  Add MB_Value.h
 * 07/02/2015 15.0.3   Replace MB_ExtrValues() with MB_ExtrMergeValues() to have enrolled and prop8 record combined.
 * 07/15/2015 15.0.5   Modify But_MergeMAdr() to add DBA. Remove unused code in But_Load_Roll() and But_Load_LDR().
 * 07/31/2015 15.2.0   Add DBA to But_MergeMAdr()
 * 08/05/2015 15.2.2   Add UseE48=N option to INI file to allow creating value file with extra Prop8 record.
 * 02/14/2016 15.12.0  Modify But_MergeMAdr() to fix overwrite bug and clean up But_MergeSitus()
 * 03/23/2016 15.14.1  Modify MergeSitus(), MergeZone() and Load_Roll() to skip first records
 *                     unless it starts with '0'.
 * 04/07/2016 15.14.4  Add option to load tax data.
 * 07/08/2016 16.0.3   Remove -La & -Ua option.  No longer support CDASSR.
 *                     Replace But_Load_LDR() with But_Load_LDR3() to support new LDR format.
 *                     Don't use CHAR from LDR file since the county said so.
 * 07/17/2016 16.0.4   Use TC601 file to create lien file and update value.
 * 07/28/2016 16.1.0   Include SqlExt.h
 * 08/14/2016 16.3.0   Clean up But_MergeMAdr() 
 * 10/31/2016 16.6.1   Add But_ConvStdChar() to support new CHAR format.  Add -Xa option.
 * 11/01/2016 16.6.2   Modify But_ConvStdChar() to add Units & Stories
 * 01/05/2017 16.8.0   Pass date format to MB_Load_TaxBase() to fix date not showing up in tax display
 * 07/19/2017 17.1.5   Modify But_Load_LDR3() to use MB_MergeStdChar() instead of But_MergeChar()
 * 11/15/2017 17.4.1   Change params in updateDelqFlag() to update Delq flag in Base record.
 * 05/07/2019 18.11.5  If CHAR file is bad, use old data.  Check for new GRGR, re-extract if needed.
 * 05/10/2019 18.11.6  Modify But_MergeMAdr() to populate M_ADDR_D & M_CTY_ST_D with M_Addr1 & M_Addr2
 *                     when M_Addr is blank.  This is usually foreign addresses.
 * 07/20/2019 19.1.0   Remove duplicate code in MergeLien3().
 * 08/09/2019 19.1.3   Rename But_MergeZone() to But_MergeRollExt() to also update other fields.
 *                     Modify But_MergeLien3() to update AgPreserve flag and remove lien value update using lien extract.
 * 07/17/2020 20.1.5   Modify But_MergeSitus() to work around bad data.
 * 08/04/2020 20.2.5   Modify But_Load_LDR3() to rebuild LDR file before processing to avoid broken line.
 * 08/13/2020 20.2.8   Add -Mz option
 * 10/31/2020 20.4.2   Modify But_MergeRoll() & But_MergeRollExt() to populate default PQZoning.
 * 02/10/2021 20.7.0   Modify loadBut() to use sale date format specify in INI file.
 * 02/17/2021 20.7.2   Modify loadBut() not to combine -G & -Mg options.  Use -Mg to import GrGr separately.
 * 02/23/2021 20.7.4   Modify But_CreateSCSale() to skip hdr records and activate new sale date format in INI file.
 * 04/25/2022 21.9.0   Allow setting county specific backup folder for GrGr files. If not defined, use default.
 * 07/18/2022 22.0.2   Add But_MergeLien17() to support new LDR layout.
 * 07/22/2022 22.0.3   Modify But_Load_LDR3() to remove LDR header record before processing.
 * 05/18/2023 22.8.3   Modify But_ImportApn() since one record now may contain more than one APN.
 *                     Modify But_ImportDoc() to support various date formats and remove check for UNDISCLOSE flag.
 *                     Modify But_ImportGrGr() to ignore import REF files since we are not using this data.
 * 08/01/2023 23.1.0   Modify But_Load_LDR3() to revert back to use MergeLien3().
 * 09/05/2023 23.1.9   Modify But_ConvStdChar() to support new QualityClass format which is now 
 *                     preceeding with 'M' or 'C'.
 * 09/21/2023 23.1.12  Fix random sorting error by adding buffer to DUPO command in But_ImportGrGr().
 * 01/03/2024 23.5.0   Comment out unused function But_Load_Roll2().
 * 03/23/2024 23.7.3   Modify But_ConvStdChar() to add LotAcre/LotSqft.
 * 07/23/2024 24.0.3   Modify But_MergeLien3() to add ExeType.
 * 09/13/2024 24.1.3   Modify But_MergeSitus() to move "COMM" StrType to StrName.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doGrgr.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "LoadMB.h"
#include "MergeBut.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "PQ.h"
#include "MB_Value.h"
#include "LoadValue.h"
#include "SqlExt.h"
#include "CharRec.h"
#include "Tax.h"
//#include "MergeZoning.h"

hlAdo    hButDb;

extern   int   But_MatchGrGr(LPCSTR pProvider);
extern   int   doApnXlat(LPCSTR pProvider, LPCSTR pTable);

/*****************************************************************************
 *
 * Desc: Get a string from a text file and check for last character. If matched,
 *       concatenate with next line.
 *
 * Return number of bytes read.  <0 if read error
 *     0: EOF
 *    -1: File not open, invalid buffer pointer, invalid max buffer length
 *    -2: Buffer is not big enough
 *
 *****************************************************************************/

int But_GetRec(char *pBuf, int iBufLen, char cLastChar, FILE *fd)
{
   char  sTmp[2048];
   bool  bDone=false;
   int   iRet, iTmp, iLen;

   if (!fd || !pBuf || iBufLen <= 0)
      return -1;

   *pBuf = 0;
   iLen = 0;
   do
   {
      if (fgets(sTmp, 2048, fd))
      {
         iTmp = strlen(sTmp);
         iLen += iTmp;     // New output len
         if (iLen > iBufLen)
         {
            iRet = -2;
            bDone = true;
         } else
         {
            if (sTmp[iTmp-1] == '\n')
            {
               sTmp[iTmp-1] = 0;
               iLen--;
            }

            strcat(pBuf, sTmp);
            iRet = iLen;
            if (sTmp[iLen-1] != cLastChar)
               bDone = true;
         }
      } else
      {
         bDone = true;
         iRet = 0;
      }

#ifdef _DEBUG
      //if (!memcmp(pBuf, "072112011000", 9))
      //   iRet = iLen;
#endif
   } while (!bDone);

   return iRet;
}

/******************************** But_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 * 20060217 Modify MergeOwner() to keep Name1 the same as input.  Parse name
 *          to fill swapped name only
 *
 *****************************************************************************/

void But_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], acSave[64], *pTmp, *pTmp1;
   char  acOwner[64], acName1[64], acName2[64];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "028470009000", 9))
   //   iRet = 0;
#endif

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.' || *pTmp == '`')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   // Update vesting
   iRet = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // If name starts with a number, do not parse
   if (acTmp[0] <= '9')
   {
      vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME1);
      return;
   }

   // Save this for later
   strcpy(acOwner, acTmp);

   acName2[0] = 0;
   acSave[0] = 0;
   acSave1[0] = 0;

   if (pTmp=strstr(acTmp, " 1/2 INT"))
      *pTmp = 0;

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // Save it - Only do it for individual trust
   if (iTmp > 5 && acTmp[iTmp] && !strchr(acTmp, '&'))
   {
      iTmp--;
      strcpy(acSave, (char *)&acTmp[iTmp]);
      acTmp[iTmp] = 0;
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
      if ((pTmp=strstr(acName2, " ETAL")) || (pTmp=strstr(acName2, " ET AL")) )
         *pTmp = 0;
   } else if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Filter out words
   if ((pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " FAMILY TRUST &"))
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      *(pTmp+13) = 0;
      strcpy(acSave1, acTmp);

      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST &"))
   {  // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      *(pTmp+13) = 0;
      strcpy(acSave1, acTmp);

      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
         *(pTmp+6) = 0;
      else
         *pTmp = 0;        // Drop TRUST only if not family trust

      strcpy(acName1, acTmp);
      pTmp1 = pTmp+9;
      iTmp1 = strlen(pTmp1);
      for (iTmp = 0; iTmp < iTmp1; iTmp++)
      {
         if (acName1[iTmp] != *pTmp1)
            break;
         pTmp1++;
      }

      // Skip first word after & if it is the same as last name
      if (iTmp > 2 && *(pTmp1-1) == ' ')
      {
         strcat(acName1, " & ");
         // Search and remove TRUST in second part of name
         if (pTmp=strstr(pTmp1, " TRUST"))
            *pTmp = 0;
         strcat(acName1, pTmp1);
      } else
      {
         strcpy(acName2, pTmp+9);
      }
   } else if ((pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " FAMILY "))
             || (pTmp=strstr(acTmp, " INCOME TR")) || (pTmp=strstr(acTmp, " LIVING TR"))
             || (pTmp=strstr(acTmp, " LAND TRUST")) || (pTmp=strstr(acTmp, " FAM TRUST")))
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUSTE"))
   {  // Drop TRUSTEE
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (acSave[0])
      {
         strcpy(acTmp1, acSave);
         strcpy(acSave, pTmp);
         strcat(acSave, acTmp1);
      } else
      {
         if (isdigit(*(pTmp-1)) )
            while (isdigit(*(--pTmp)));

         strcpy(acSave, pTmp);
      }
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " ESTATE OF"))
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
         strcpy(acName2, pTmp);
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " SUCCESSOR")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(acTmp1, myOwner.acName1);
      if (myOwner.acVest[0] > ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // Concat what in saved buffer if it is not " TRUST"
      if (acSave[0] && strcmp(acSave, " TRUST"))
      {
         if (acTmp1[strlen(acTmp1)-1] == ' ')
            strcat(acTmp1, (char *)&acSave[1]);      // avoid double blank
         else
            strcat(acTmp1, acSave);
      }

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, SIZ_NAME_SWAP);
      else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
   {
      if (acSave[0])
      {
         strcat(acName1, acSave);
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
      } else
      {
         // Couldn't split names
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
      }
   }

   // Save Name1
   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);
}

/******************************** But_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void But_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], acZip[16], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002530001", 9))
   //   iTmp = 0;
#endif

   // DBA
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   } 

   // Mail address
   if (!memcmp(apTokens[MB_ROLL_M_ADDR], "--", 2))
      return;

   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   iTmp = blankRem(acAddr1);
   if (acAddr1[0] <= ' ')
   {
      int iIdx = MB_ROLL_M_ADDR1;
      if (!memcmp(apTokens[MB_ROLL_M_ADDR1], "C/O", 3) || !memcmp(apTokens[MB_ROLL_M_ADDR1], "DBA", 3))
         iIdx++;
      strcpy(acAddr1, apTokens[iIdx]);
      iTmp = blankRem(acAddr1);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, iTmp);
      sprintf(acTmp, "%s %s", apTokens[iIdx+1], apTokens[iIdx+2]);
      iTmp = blankRem(acTmp);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
      return;
   } else
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] >= 'A')
   {
      memcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], strlen(apTokens[MB_ROLL_M_CITY]));
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      acZip[0] = 0;
      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], 9);
         if (isNumber(apTokens[MB_ROLL_M_ZIP]))
         {
            iTmp = strlen(apTokens[MB_ROLL_M_ZIP]);
            if (iTmp < 9)
               sprintf(acZip, "%.5s", apTokens[MB_ROLL_M_ZIP]);
            else
               sprintf(acZip, "%.5s-%.4s", apTokens[MB_ROLL_M_ZIP], apTokens[MB_ROLL_M_ZIP]+5);
         } else
            strcpy(acZip, apTokens[MB_ROLL_M_ZIP]);
      }

      sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], acZip);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

void But_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "079130028000", 9))
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         // Update DBA
         strcpy(acTmp, pLine1+4);
         vmemcpy(pOutbuf+OFF_DBA, acTmp, SIZ_DBA);
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         // Update DBA
         strcpy(acTmp, pLine1+4);
         vmemcpy(pOutbuf+OFF_DBA, acTmp, SIZ_DBA);
         p1 = pLine2;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
         {
            p1 = pLine1;
         } else
         {
            // If last word of line 3 is number and first word of line 1 is alpha,
            // line 1 more likely be CareOf
            p0 = pLine1;
            p1 = pLine2;
         }
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      iTmp = blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, iTmp);

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   strcpy(acAddr2, p2);
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002530020000", 9))
   //   iTmp = 0;
#endif

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      iTmp = strlen(sMailAdr.City);
      if (iTmp > SIZ_M_CITY) iTmp = SIZ_M_CITY;
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, iTmp);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);

      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}

/******************************** But_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int But_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acStrName[32], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   pTmp = pRec;
   // Get rec
   if (!pRec)
   {
      // Drop header record & Get 1st rec
      do {
         pRec = fgets((char *)&acRec[0], 512, fdSitus);
      } while (pRec && *pRec != '0');
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Replace tab char with 0
   if (pRec)
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old Situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      strcpy(acAddr1, apTokens[MB_SITUS_STRNUM]);
      if (strchr(apTokens[MB_SITUS_STRNUM], '-') || strchr(apTokens[MB_SITUS_STRNUM], '&'))
      {
         if (pTmp=strchr(acAddr1, ' '))
            remChar(pTmp+1, ' ');
      }

      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));
      iTmp = sprintf(acTmp, "%d", lTmp);
      vmemcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM, iTmp);
      strcat(acAddr1, " ");

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
      {
         if (isalnum(*(pTmp+1)))
            vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, SIZ_S_STR_SUB);
      }
      if (*apTokens[MB_SITUS_STRDIR] > ' ' && isDir(apTokens[MB_SITUS_STRDIR]))
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         vmemcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], SIZ_S_DIR);
      }
   }

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);
      strcpy(acStrName, apTokens[MB_SITUS_STRNAME]);

      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
      {
         Sfx2Code(acTmp, acCode);
         vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      } else if (!memcmp(apTokens[MB_SITUS_STRTYPE], "COMM", 4))
      {
         // Add to StrName
         strcat(acStrName, " COMM");
      } else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         iBadSuffix++;
      }

      vmemcpy(pOutbuf+OFF_S_STREET, acStrName, SIZ_S_STREET);
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apTokens[MB_SITUS_STRNAME]);
      if (sAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      if (sAdr.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, SIZ_S_SUFF);

      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNO);
   }
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1, pOutbuf);
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
         sprintf(acTmp, "%s CA", myTrim(acAddr1));
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int But_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004130013", 9))
   //   acTmp[0] = 0;
#endif
   //
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   strcpy(acTmp, pLine2);
   _strupr(acTmp);
   memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, strlen(acTmp));
   parseAdr2(&sSitusAdr, acTmp);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/******************************** But_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************

int But_MergeSale(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice, lTmp;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale  = (FILE *)NULL;
         return 1;      // EOF
      }

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", iApnLen, pRec+iSkipQuote);
         //pRec = fgets(acRec, 512, fdSale);
         iRet = But_GetRec(acRec, 512, ' ', fdSale);
         if (!iRet)
            pRec = NULL;

         lSaleSkip++;
      }
   } while (iTmp > 0);

   if (iTmp)
      return 1;

   while (!iTmp)
   {
      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      iRet = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "002070031000", 9))
      //   iRet = 0;
#endif

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((void *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         if (*apTokens[MB_SALES_DOCNUM] > ' ')
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
            sprintf(acTmp, "%.5s%0.7ld", apTokens[MB_SALES_DOCNUM], lTmp);
            memcpy(sCurSale.acDocNum, acTmp, SALE_SIZ_DOCNUM);
         } else
            memset(sCurSale.acDocNum, ' ', SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);
         iTmp = findDocType(apTokens[MB_SALES_DOCCODE], &BUT_DocCode[0]);
         if (iTmp >= 0)
         {
            memcpy(sCurSale.acDocType, BUT_DocCode[iTmp].pCode, BUT_DocCode[iTmp].iCodeLen);
            if (lPrice < 100)
               sCurSale.NoneSale_Flg = BUT_DocCode[iTmp].flag;
         }

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         iRet = MB_MergeSale(&sCurSale, pOutbuf, true, true);
         if (iRet >= 0)
         {
            // Group sale?
            if (*apTokens[MB_SALES_GROUPSALE] > '0')
               *(pOutbuf+OFF_MULTI_APN) = 'Y';
            else
               *(pOutbuf+OFF_MULTI_APN) = ' ';
         }
         iRet = 0;
      }

      // Get next sale record
      //pRec = fgets(acRec, 512, fdSale);
      //if (pRec)
      //   iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      iRet = But_GetRec(acRec, 512, ' ', fdSale);
      if (iRet > iApnLen)
         iTmp = memcmp(pOutbuf, &acRec[iSkipQuote], iApnLen);
      else
      {
         fclose(fdSale);
         fdSale  = (FILE *)NULL;
         break;
      }
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** But_MergeChar ******************************
 *
 * Note: need code table for Heating and Colling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *   - Heating - 1..9 ???
 *   - Cooling - 01..07  ???
 *
 *****************************************************************************/

int But_MergeChar(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], *pTmp;
   long     lTmp, lBldgSqft, lGarSqft;
   int      iRet, iLoop, iBeds, iFBath, iHBath, iFp, iTmp;
   MB_CHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
   {
      pRec = fgets(acRec, 512, fdChar);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   pChar = (MB_CHAR *)pRec;

   // Quality Class
   memcpy(acTmp, pChar->QualityClass, MBSIZ_CHAR_QUALITY);
   acTmp[MBSIZ_CHAR_QUALITY] = 0;
   pTmp = strchr(acTmp, ' ');
   if (pTmp) *pTmp = 0;

   acCode[0] = 0;
   if (!_memicmp(acTmp, "AVG", 3))
      strcpy(acCode, "A");
   else if (isalpha(acTmp[0]))
   {
      *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0];
      if (isdigit(acTmp[1]))
         iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
      else if (pTmp && isdigit(*(pTmp+1)))
         iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
   } else if (acTmp[0] > '0' && acTmp[0] <= '9')
      iRet = Quality2Code(acTmp, acCode, NULL);

   blankPad(acCode, SIZ_BLDG_QUAL);
   memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);

   // Yrblt
   lTmp = atoin(pChar->YearBuilt, MBSIZ_CHAR_YRBLT);
   if (lTmp > 0 && lTmp != 1900)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = '2';
   } else
   {
      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = ' ';
   }

   // Heating
   if (pChar->Heating[0] > ' ')
   {
      iTmp = 0;
      while (asHeating[iTmp].iLen > 0)
      {
         if (pChar->Heating[0] == asHeating[iTmp].acSrc[0])
         {
            *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }
   // Cooling

   // Pool
   int iCmp;
   if (pChar->NumPools[0] > ' ')
   {
      iTmp = 0;
      iCmp = -1;
      while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(pChar->NumPools, asPool[iTmp].acSrc, asPool[iTmp].iLen)) )
         iTmp++;

      if (!iCmp)
         *(pOutbuf+OFF_POOL) = asPool[iTmp].acCode[0];
   }

   // Beds
   iBeds = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Fireplace
   iFp = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   } else
      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);

   // HasSeptic or HasSewer
   if (pChar->HasSeptic > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
   else if (pChar->HasSewer > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWell;

   lCharMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdChar);

   return 0;
}

/******************************** But_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************

int But_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      pTmp += iSkipQuote;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 0;
   }

   iRet = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      long lGross = atoin(pOutbuf+OFF_GROSS, SIZ_GROSS);

      if (lTmp > lGross)
      {
         LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** But_MergeTax ******************************
 *
 * Note:
 *
 *****************************************************************************

int But_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double   dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iRet;

   // Merge tax data
   while (!iTmp)
   {
      iTmp = ParseStringNQ(pRec, cDelim, MB_TAX_ROLLCAT+1, apTokens);
      if (iTmp < MB_TAX_ROLLCAT)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         pRec = fgets(acRec, 512, fdTax);
         iRet = -1;
         break;
      }

      // If same tax year, apply values
      //if (!memcmp(myCounty.acYearAssd, apTokens[MB_TAX_YEAR], 4))
      {
         dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
         dTax1 = atof(acTmp);
         dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
         dTax2 = atof(acTmp);
         dTmp = dTax1+dTax2;
         if (dTax1 == 0.0 || dTax2 == 0.0)
            dTmp *= 2;

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/********************************* But_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 * Notes:
 *    1) Official book range 001-079.  Ignore APN starts with 800-999 except 910 (MH) & 912 (OIL/GAS WELL)
 *    2) Take out transfer update since this is done by But_MergeSale() already.
 *
 *****************************************************************************/

int But_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse roll rec
   iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_M_ADDR4)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN starts with 800-999 except 910 (MH) & 912 (OIL/GAS WELL)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910 && iTmp != 912))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "04BUT", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: BUT has Growing and PPMobilhome only.
      // Other fields are not populated.  See Sha_MergeRoll() for more info.
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // Save tax code
      //memcpy(pOutbuf+OFF_TAX_CODE, apTokens[MB_ROLL_TAXABILITY], strlen(apTokens[MB_ROLL_TAXABILITY]));
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "038230033000", 9))
   //   iRet = 0;
#endif
   // Legal - replace known bad char
   iTmp = replChar(apTokens[MB_ROLL_LEGAL], 2, '2');
   iTmp = updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // Zoning
   myBTrim(apTokens[MB_ROLL_ZONING]);
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      // Drop garbage
      if (pTmp = strchr(apTokens[MB_ROLL_ZONING], ' '))
         *pTmp = 0;
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   acTmp[SIZ_USE_CO] = 0;
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0' && *(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
   {
      lTmp = atoin(apTokens[MB_ROLL_DOCNUM]+5, 7);
      if (lTmp > 0)
         sprintf(acTmp, "%.5s%0.7d", apTokens[MB_ROLL_DOCNUM], lTmp);
      else
         strcpy(acTmp, apTokens[MB_ROLL_DOCNUM]);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, strlen(acTmp));

      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
      else
         memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
   }

   // Owner
   try {
      But_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   } catch(...) {
      LogMsg("***** Exeception occured in But_MergeOwner()");
   }

   // Mailing
   try {
      But_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in But_MergeMAdr()");
   }

   // Set Prop8 flag
   //iTmp = atol(apTokens[MB_ROLL_TAXABILITY]);
   //if (iTmp > 799)
   //   *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/******************************** But_ImportApn ******************************
 *
 * 05/18/2023 One record may contain more than one APN.
 *
 *****************************************************************************/

int But_ImportApn(char *pInfile)
{
   char     *pTmp, acRec[MAX_RECSIZE], acDoc[64], acApn[2048], acTmp[256], *apItems[16];
   int      iTmp, iRet, iNumFlds;
   long     lCnt=0;
   FILE     *fdIn;

   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening %s", pInfile);
      return -1;
   }

   pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
   iNumFlds = ParseString(acRec, 9, 16, apItems);
   strcpy(acDoc, apItems[0]);

   while (!feof(fdIn))
   {
      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Parse string on tab character
      iNumFlds = ParseStringNQ(acRec, 9, 16, apItems);
      if (iNumFlds < 2)
         continue;

      if (strcmp(apItems[0], acDoc) || strcmp(apItems[1], acApn))
      {
         strcpy(acDoc, apItems[0]);
         strcpy(acApn, apItems[1]);
         remChar(acApn, '-');

         // Check for multi APN
         iTokens = ParseStringNQ(acApn, ',', MAX_FLD_TOKEN, apTokens);
         if (iTokens < MAX_FLD_TOKEN)
         {
            for (iTmp = 0; iTmp < iTokens; iTmp++)
            {
               if (strlen(apTokens[iTmp]) >= 9)
               {
                  sprintf(acTmp, "INSERT INTO But_GrGr (DocumentNumber, APN) VALUES('%s', '%s')", apItems[0], apTokens[iTmp]);
                  iRet = execSqlCmd(acTmp, &hButDb);
                  if (iRet)
                     LogMsg("***** Error import APN file into But_GrGr: %s", acTmp);
                  else
                     lCnt++;
               } else if (*apTokens[iTmp] > ' ')
                  LogMsg("*** Bad APN. Skip importing record: %s %s", apItems[0], apTokens[iTmp]);
            }
         } else
         {
            LogMsg("***** Bad record. Too many APN in one DocNum: %s %s", apItems[0], apItems[1]);
         }
      } else
         LogMsg("*** Duplicate entry in Doc-Apn: %s %s", apItems[0], apItems[1]);
   }

   fclose(fdIn);
   LogMsg("Number of APN imported:     %d", lCnt);
   return 0;
}

// Old version before 05/17/2023
//int But_ImportApn(char *pInfile)
//{
//   char     *pTmp, acRec[256], acDoc[64], acApn[64], acTmp[256], *apItems[16];
//   int      iRet, iNumFlds;
//   long     lCnt=0;
//   FILE     *fdIn;
//
//   if (!(fdIn = fopen(pInfile, "r")))
//   {
//      LogMsg("***** Error opening %s", pInfile);
//      return -1;
//   }
//
//   pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
//   iNumFlds = ParseString(acRec, 9, 16, apItems);
//   strcpy(acDoc, apItems[0]);
//
//   while (!feof(fdIn))
//   {
//      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
//      if (!pTmp)
//         break;
//
//      // Parse string on tab character
//      iNumFlds = ParseString(acRec, 9, 16, apItems);
//      if (iNumFlds < 2)
//         break;
//
//      // If same docnum appearing twice, it might be mupti-apn transaction
//      //if (!strcmp(apItems[0], acSave))
//      //   LogMsg("*** Multiple entry in Doc-Apn: %s %s", apItems[0], apItems[1]);
//      //else
//      //   strcpy(acSave, apItems[0]);
//
//      if (strcmp(apItems[0], acDoc) || strcmp(apItems[1], acApn))
//      {
//         strcpy(acDoc, apItems[0]);
//         strcpy(acApn, apItems[1]);
//
//         if (strlen(apItems[1]) >= 9)
//         {
//            sprintf(acTmp, "INSERT INTO But_GrGr (DocumentNumber, APN) VALUES('%s', '%s')", apItems[0], apItems[1]);
//            iRet = execSqlCmd(acTmp, &hButDb);
//            if (iRet)
//               LogMsg("***** Error import APN file into But_GrGr: %s", acTmp);
//         } else
//            LogMsg("*** Bad APN. Skip importing record: %s %s", apItems[0], apItems[1]);
//      } else
//         LogMsg("*** Duplicate entry in Doc-Apn: %s %s", apItems[0], apItems[1]);
//
//      lCnt++;
//   }
//
//   fclose(fdIn);
//   LogMsg("Number of APN imported:     %d", lCnt);
//   return 0;
//}

/******************************** But_ImportDoc ******************************
 *
 * 11/21/2014 - Ignore record without APN
 *
 *****************************************************************************/

int But_ImportDoc(char *pInfile)
{
   char     *pTmp, acRec[1024], acTmp[512], *apItems[16];
   char     acRecDt[16];
   int      iRet, iNumFlds, iPages, iDateType;
   long     lCnt=0, lPrice;
   double   dTax;
   FILE     *fdIn;
   hlAdoRs  myRs;

   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening %s", pInfile);
      return -1;
   }

   pTmp = fgets(acRec, 1024, fdIn);
   iNumFlds = ParseStringNQ(acRec, 9, 16, apItems);

   while (!feof(fdIn))
   {
      pTmp = fgets(acRec, 1024, fdIn);
      if (!pTmp)
         break;

      iNumFlds = ParseStringNQ(acRec, 9, 16, apItems);
      if (iNumFlds < 5)
      {
         LogMsg("*** WARNING: Bad number of fields for %s at %d", acRec, lCnt);
         continue;
      }

      if (*(apItems[GRGR_DOC_RECDT]+2) == '/' || *(apItems[GRGR_DOC_RECDT]+1) == '/')
         iDateType = MM_DD_YYYY_1;
      else if (isMonth(apItems[GRGR_DOC_RECDT]))      // Apr 21 2022  8:00AM
         iDateType = MMM_DD_YYYY;
      else if (isWeekDay(apItems[GRGR_DOC_RECDT]))    // Wed Mar 22 08:19:31 PDT 2023
         iDateType = WD_MMM_DD_YYYY;
      else
         iDateType = 0;

      // If bad date, drop it
      pTmp = dateConversion(apItems[GRGR_DOC_RECDT], acRecDt, iDateType);
      if (!pTmp)
         continue;

      iRet = atol(acRecDt);
      if (iRet > lLastRecDate)
         lLastRecDate = iRet;

      dTax = atof(apItems[GRGR_DOC_TAX]);
      // 05/18/2023 - GRGR_DOC_UNDISCLOSE is no longer available
      //iRet = atoi(apItems[GRGR_DOC_UNDISCLOSE]);
      //if (!iRet)
         lPrice = (long)(dTax * SALE_FACTOR);
      //else
      //   lPrice = 0;

      iPages = atoi(apItems[GRGR_DOC_PAGES]);
      if (iPages > 1000)
      {
         LogMsg0("*** Doc has more than 1000 pages - %s (%s,%d)", apItems[GRGR_DOC_DOCNUM], apItems[GRGR_DOC_PAGES], iPages);
         iPages = 1;
      }

      sprintf(acTmp, "SELECT * FROM But_GrGr WHERE (DocumentNumber='%s')", apItems[GRGR_DOC_DOCNUM]);
      try
      {
         myRs.Open(hButDb, acTmp);

#ifdef _DEBUG
      //if (!memcmp(apItems[GRGR_DOC_DOCNUM], "2023-0008896", 12))
      //   iRet = 0;
#endif

         // Update RecordingDate, DocumentTitle, Tax, NumberofPages, SalePrice
         if (myRs.next())
         {  
            remChar(apItems[GRGR_DOC_DOCTITLE], '\'');
            sprintf(acTmp, "UPDATE But_GrGr SET RecordingDate=%s, DocumentTitle='%s', Tax=%.2f, "
               "SalePrice=%ld, NumberofPages=%d WHERE (DocumentNumber='%s')", acRecDt,
               apItems[GRGR_DOC_DOCTITLE], dTax, lPrice, iPages, apItems[GRGR_DOC_DOCNUM]);

            // Make sure record is not locked before update
            myRs.Close();
            iRet = execSqlCmd(acTmp, &hButDb);
            if (iRet)
               LogMsg("***** Error import Doc into But_GrGr");
         } else
            myRs.Close();
      }
      AdoCatch (e)
      {
         LogMsg("***** Error accessing sql [%s] : %s", acTmp, ComError(e));
         iRet = -3;
      }
      lCnt++;
   }

   fclose(fdIn);
   LogMsg("Number of DOC imported: %d", lCnt);

   return 0;
}

/******************************** But_ImportRef ******************************
 *
 * 11/21/2014 - Ignore record without APN
 *
 *****************************************************************************/

int But_ImportRef(char *pInfile)
{
   char     *pTmp, acRec[1024], acTmp[256], *apItems[16];
   int      iRet, iNumFlds;
   long     lCnt=0;
   FILE     *fdIn;
   hlAdoRs  myRs;

   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening %s", pInfile);
      return -1;
   }

   pTmp = fgets(acRec, 1024, fdIn);
   iNumFlds = ParseStringNQ(acRec, 9, 16, apItems);

   while (!feof(fdIn))
   {
      pTmp = fgets(acRec, 1024, fdIn);
      if (!pTmp)
         break;

      iNumFlds = ParseStringNQ(acRec, 9, 16, apItems);
      if (iNumFlds < 2)
      {
         LogMsg("*** WARNING: Bad number of fields for %s at %d", acRec, lCnt);
         continue;
      }

      sprintf(acTmp, "SELECT * FROM But_GrGr WHERE (DocumentNumber='%s')", apItems[GRGR_REF_DOCNUM]);
      try
      {
         myRs.Open(hButDb, acTmp);
         if (myRs.next())
         {  // Update RecordingDate, DocumentTitle, Tax, NumberofPages, SalePrice
            sprintf(acTmp, "UPDATE But_GrGr SET ReferenceData='%s' WHERE (DocumentNumber='%s')",
               apItems[GRGR_REF_REFINFO], apItems[GRGR_REF_DOCNUM]);

            myRs.Close();
            iRet = execSqlCmd(acTmp, &hButDb);
            if (iRet)
               LogMsg("***** Error import Ref into But_GrGr");
         } else
            myRs.Close();
      }
      AdoCatch(e)
      {
         LogMsg("***** Error accessing sql [%s] : %s", acTmp, ComError(e));
         iRet = -3;
      }

      lCnt++;
   }

   fclose(fdIn);
   LogMsg("Number of REFS imported:    %d", lCnt);
   return iRet;
}

/******************************** But_ImportName *****************************
 *
 * 11/21/2014 - Ignore record without APN
 * Names file is not sorted in DocNum order. We have to resort before import.
 *
 *****************************************************************************/

int But_ImportName(char *pInfile)
{
   char     *pTmp, *pTmp1, acRec[1024], acTmp[256], *apItems[16], sLastDoc[32], sSqlCmd[1024];
   char     asENames[256][64], asONames[256][64], asINames[256][64], acInfile[_MAX_PATH];
   int      iRet, iNumFlds, iTmp, iNameCnt, iECnt, iOCnt, iICnt, iLastCnt;
   long     lCnt=0;
   FILE     *fdIn;
   hlAdoRs  myRs;

   if (*pInfile == '"')
   {
      iTmp = strlen(pInfile);
      memcpy(acInfile, pInfile+1, iTmp - 2);
      acInfile[iTmp-2] = 0;
   } else
      strcpy(acInfile, pInfile);

   if (!(fdIn = fopen(acInfile, "r")))
   {
      LogMsg("***** Error opening %s", acInfile);
      return -1;
   }

   sLastDoc[0] = 0;
   iECnt=iOCnt=iICnt=iNameCnt = 0;

   while (!feof(fdIn))
   {
      pTmp = fgets(acRec, 1024, fdIn);
      if (!pTmp)
      {
         if (!lCnt)
         {
            iRet = -1;
         } else
         {
            // Import last doc
            iNameCnt = iOCnt+iECnt+iICnt;
            if (iNameCnt > 0)
            {
               if (iECnt > 2)
                  strcpy(sSqlCmd, "UPDATE But_GrGr SET MoreName='Y', ");
               else
                  strcpy(sSqlCmd, "UPDATE But_GrGr SET ");

               for (iTmp = 0; iECnt > 0 && iTmp < 8; iTmp++)
               {
                  sprintf(acTmp, "Type%d='E', Name%d='%s', ", iTmp+1, iTmp+1, asENames[iTmp]);
                  strcat(sSqlCmd, acTmp);
                  iECnt--;
               }

               iLastCnt = iTmp+1;
               for (iTmp = 0; iOCnt > 0 && iTmp < (10-iLastCnt); iTmp++)
               {
                  sprintf(acTmp, "Type%d='O', Name%d='%s', ", iTmp+iLastCnt, iTmp+iLastCnt, asONames[iTmp]);
                  strcat(sSqlCmd, acTmp);
                  iOCnt--;
               }

               iLastCnt = iLastCnt+iTmp;
               for (iTmp = 0; iICnt > 0 && iTmp < (10-iLastCnt); iTmp++)
               {
                  sprintf(acTmp, "Type%d='I', Name%d='%s', ", iTmp+iLastCnt, iTmp+iLastCnt, asINames[iTmp]);
                  strcat(sSqlCmd, acTmp);
                  iICnt--;
               }

               sprintf(acTmp, "NameCnt=%d WHERE (DocumentNumber='%s')", iNameCnt, sLastDoc);
               strcat(sSqlCmd, acTmp);
               iRet = execSqlCmd(sSqlCmd, &hButDb);
               if (iRet)
                  LogMsg("***** Error update Names for DocNum=%s", sLastDoc);
            }
         }
         break;
      }

      // Skip blank line
      if (strlen(acRec) < 12)
         continue;

      // Remove apos from name
      if (pTmp = strchr(acRec, 39))
      {
         pTmp1 = pTmp+1;
         strcpy(pTmp, pTmp1);
      }
      iNumFlds = ParseString(acRec, 9, 16, apItems);
      if (iNumFlds < 3)
      {
         LogMsg("*** WARNING: Bad number of fields for %s at %d", acRec, lCnt);
         continue;
      }

      // Max name is 50 bytes
      if ((iTmp = strlen(apItems[GRGR_NAM_NAME])) > 50)
         apItems[GRGR_NAM_NAME][50] = 0;

      // Skip these names (tax status)
      if (*apItems[GRGR_NAM_TYPE] == 'T')
         continue;

#ifdef _DEBUG
      //if (!memcmp(apItems[GRGR_NAM_DOCNUM], "2008-0024009", 12))
      //   iTmp = 0;
#endif

      // Form new sqlcmd for new doc
      if (strcmp(sLastDoc, apItems[GRGR_NAM_DOCNUM]))
      {
         iNameCnt = iOCnt+iECnt+iICnt;
         if (iNameCnt > 0)
         {
            if (iECnt > 2)
               strcpy(sSqlCmd, "UPDATE But_GrGr SET MoreName='Y', ");
            else
               strcpy(sSqlCmd, "UPDATE But_GrGr SET ");

            for (iTmp = 0; iECnt > 0 && iTmp < 8; iTmp++)
            {
               sprintf(acTmp, "Type%d='E', Name%d='%s', ", iTmp+1, iTmp+1, asENames[iTmp]);
               strcat(sSqlCmd, acTmp);
               iECnt--;
            }

            iLastCnt = iTmp+1;
            for (iTmp = 0; iOCnt > 0 && iTmp < (10-iLastCnt); iTmp++)
            {
               sprintf(acTmp, "Type%d='O', Name%d='%s', ", iTmp+iLastCnt, iTmp+iLastCnt, asONames[iTmp]);
               strcat(sSqlCmd, acTmp);
               iOCnt--;
            }

            iLastCnt = iLastCnt+iTmp;
            for (iTmp = 0; iICnt > 0 && iTmp < (10-iLastCnt); iTmp++)
            {
               sprintf(acTmp, "Type%d='I', Name%d='%s', ", iTmp+iLastCnt, iTmp+iLastCnt, asINames[iTmp]);
               strcat(sSqlCmd, acTmp);
               iICnt--;
            }

            sprintf(acTmp, "NameCnt=%d WHERE (DocumentNumber='%s')", iNameCnt, sLastDoc);
            strcat(sSqlCmd, acTmp);
            iRet = execSqlCmd(sSqlCmd, &hButDb);
            if (iRet)
               LogMsg("***** Error update Names for DocNum=%s", sLastDoc);
         }
         iNameCnt=iECnt=iOCnt=iICnt=0;
         if (*apItems[GRGR_NAM_TYPE] == 'E')
            strcpy(asENames[iECnt++], apItems[GRGR_NAM_NAME]);
         else if (*apItems[GRGR_NAM_TYPE] == 'O')
            strcpy(asONames[iOCnt++], apItems[GRGR_NAM_NAME]);
         else if (*apItems[GRGR_NAM_TYPE] == 'I')
            strcpy(asINames[iICnt++], apItems[GRGR_NAM_NAME]);
         strcpy(sLastDoc, apItems[GRGR_NAM_DOCNUM]);
      } else
      {
         if (*apItems[GRGR_NAM_TYPE] == 'E' && iECnt < 256)
            strcpy(asENames[iECnt++], apItems[GRGR_NAM_NAME]);
         else if (*apItems[GRGR_NAM_TYPE] == 'O' && iOCnt < 256)
            strcpy(asONames[iOCnt++], apItems[GRGR_NAM_NAME]);
         else if (*apItems[GRGR_NAM_TYPE] == 'I' && iICnt < 256)
            strcpy(asINames[iICnt++], apItems[GRGR_NAM_NAME]);
      }

      lCnt++;
   }

   fclose(fdIn);
   LogMsg("Number of NAMES imported:   %d", lCnt);
   return iRet;
}

/******************************** But_ImportGrGr *****************************
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int But_ImportGrGr(char *pCnty)
{
   char     *pTmp, acTmp[256], acTmp1[256], acGrGrPath[256];
   char     acGrGrIn[_MAX_PATH], acGrGrBak[_MAX_PATH];
   int      iRet, iResult=0;
   long     lCnt, lHandle, lTmp;
   struct   _finddata_t  sFileInfo;

   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   strcpy(acGrGrBak, acGrGrIn);
   pTmp = strrchr(acGrGrBak, '\\');
   if (pTmp)
      *++pTmp = 0;
   strcpy(acGrGrPath, acGrGrBak);

   // Import APN
   sprintf(acTmp, acGrGrIn, "APN");
   lHandle = _findfirst(acTmp, &sFileInfo);
   if (lHandle <= 0)
   {
      LogMsg("*** GrGr file not available for processing");
      return -1;        // If APN file is not available, don't continue
   }

   // Clear old data
   LogMsg("Clear old data in %s_GrGr", myCounty.acCntyCode);
   iRet = execSqlCmd("TRUNCATE TABLE But_GrGr", &hButDb);
   if (iRet)
   {
      LogMsg("***** Error deleting old data from But_GrGr table");
      return iRet;
   }

   // Prepare backup folder
   sprintf(acGrGrBak, acGrGrBakTmpl, pCnty, acToday);
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   lCnt = 0;
   lLastRecDate = 0;

   while (!iRet)
   {
      sprintf(acTmp, "%s%s", acGrGrPath, sFileInfo.name);
      LogMsg("Importing %s", acTmp);

      lTmp = getFileDate(acTmp);
      if (lTmp > lLastGrGrDate)
         lLastGrGrDate = lTmp;

      iRet = But_ImportApn(acTmp);

      // Move file to backup folder if success
      if (!iRet)
      {
         sprintf(acTmp1, "%s\\%s", acGrGrBak, sFileInfo.name);
         rename(acTmp, acTmp1);
      }

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
      lCnt++;
   }
   LogMsg("Number of APN file imported: %d", lCnt);

   // Close handle
   _findclose(lHandle);

   // Import Doc
   sprintf(acTmp, acGrGrIn, "Doc");
   lHandle = _findfirst(acTmp, &sFileInfo);
   if (lHandle > 0)
      iRet = 0;
   else
   {
      LogMsg("*** GrGr DOC file not available for processing (%s)", acTmp);
      return -1;
   }

   lCnt = 0;
   while (!iRet)
   {
      sprintf(acTmp, "%s%s", acGrGrPath, sFileInfo.name);
      LogMsg("Importing %s", acTmp);
      iRet = But_ImportDoc(acTmp);

      // Move file to backup folder if success
      if (!iRet)
      {
         sprintf(acTmp1, "%s\\%s", acGrGrBak, sFileInfo.name);
         rename(acTmp, acTmp1);
      }

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
      lCnt++;
   }
   LogMsg("Number of DOC file imported: %d", lCnt);

   // Close handle
   _findclose(lHandle);

   /* Import Ref - Ignore 05/18/2023 since it's not being used
   sprintf(acTmp, acGrGrIn, "Ref");
   lHandle = _findfirst(acTmp, &sFileInfo);
   if (lHandle > 0)
      iRet = 0;
   else
   {
      LogMsg("*** GrGr REF file not available for processing (%s)", acTmp);
      return -1;
   }

   lCnt = 0;
   while (!iRet)
   {
      sprintf(acTmp, "%s%s", acGrGrPath, sFileInfo.name);
      LogMsg("Importing %s", acTmp);
      iRet = But_ImportRef(acTmp);

      // Move file to backup folder if success
      if (!iRet)
      {
         sprintf(acTmp1, "%s\\%s", acGrGrBak, sFileInfo.name);
         rename(acTmp, acTmp1);
      }

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
      lCnt++;
   }
   LogMsg("Number of REF file imported: %d", lCnt);

   // Close handle
   _findclose(lHandle);
   */

   // Import Name
   sprintf(acTmp, acGrGrIn, "Names");
   lHandle = _findfirst(acTmp, &sFileInfo);
   if (lHandle > 0)
      iRet = 0;
   else
   {
      LogMsg("*** GrGr Name file not available for processing (%s)", acTmp);
      return -1;
   }

   lCnt = 0;
   while (!iRet)
   {
      sprintf(acTmp, "\"%s%s\"", acGrGrPath, sFileInfo.name);
      LogMsg("Importing %s", acTmp);

      // Names file is not sorted in DocNum order. We have to resort before import.
      sprintf(acTmp1, "\"%s\\%s\\%s\"", acTmpPath, pCnty, sFileInfo.name);
      iRet = sortFile(acTmp, acTmp1, "S(#1,C,A) DEL(9) OMIT(1,1,C,GT,\"9\") DUPO(B4096,) F(TXT)");
      if (iRet > 0)
      {
         iRet = But_ImportName(acTmp1);
      }

      // Move file to backup folder if success
      if (!iRet || iRet == -1)
      {
         if (iRet == -1)
            LogMsg("*** Input file %s is empty.  Please verify!", acTmp);

         sprintf(acTmp, "%s%s", acGrGrPath, sFileInfo.name);
         sprintf(acTmp1, "%s\\%s", acGrGrBak, sFileInfo.name);
         rename(acTmp, acTmp1);
      } else
      {
         LogMsg("***** Error Importing %s.  Please verify!", acTmp);
      }

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
      lCnt++;
   }
   LogMsg("Number of NAME file imported: %d\n", lCnt);

   // Close handle
   _findclose(lHandle);

   LogMsg("Finish importing GRGR Files.  Last recording date: %d.", lLastRecDate);

   return 0;
}

/******************************** But_ExtractGrGr ****************************
 *
 * Extract data and append to existing GrGr.CO file.  This output is to be merged
 * into BUTAssr product.  Resort data on APN ascending, Recording Date descending,
 * and DocumentNumber descending.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int But_ExtrGrGrMatched(char *pCnty)
{
   char     acTmp[256], acTmpFile[256], acGrGrFile[256];
   int      iTmp, iGrantors, iGrantees, iNameCnt, iLen;
   long     lCnt=0;

   CString  sTmp, sApn, sType;
   hlAdoRs  myRs;
   FILE     *fdSale;
   MB_GRGR  SaleRec;

   sprintf(acTmpFile, acGrGrTmpl, pCnty, "TMP");
   sprintf(acGrGrFile, acGrGrTmpl, pCnty, "CO");
   LogMsg("Extract matched grgr to %s", acGrGrFile);

   // If old file exist, copy it to TMP file then work on it
   if (!_access(acGrGrFile, 0))
   {
      // Copy current file to TMP file
      if (!CopyFile(acGrGrFile, acTmpFile, false))
      {
         LogMsg("***** Fail copying old GrGr file (%s->%s) for processing", acGrGrFile, acTmpFile);
         return -2;
      }
   }

   // Open output file
   if (!(fdSale = fopen(acTmpFile, "a+")))
   {
      LogMsg("***** Error creating %s file", acTmpFile);
      return -1;
   }

   // Select statement
   //sprintf(acTmp, "SELECT * FROM %s_GrGr WHERE APN_Matched='Y' ORDER BY APN, DocumentNumber DESC", pCnty);
   //sprintf(acTmp, "SELECT * FROM %s_GrGr WHERE APN_Matched='Y' ", pCnty);
   sprintf(acTmp, "SELECT * FROM %s_GrGr WHERE APN IS NOT NULL ", pCnty);
   try
   {
      myRs.Open(hButDb, acTmp);
   }
   AdoCatch(e)
   {
      LogMsg("***** Error executing command [%s] : %s", acTmp, ComError(e));
      return -1;
   }

   // Loop through
   while (myRs.next())
   {
      memset((void *)&SaleRec, 32, sizeof(MB_GRGR));

      sApn = myRs.GetItem("Apn");
      memcpy(SaleRec.Apn, sApn, sApn.GetLength());
      sTmp = myRs.GetItem("RecordingDate");
      memcpy(SaleRec.DocDate, sTmp, sTmp.GetLength());
      sTmp = myRs.GetItem("DocumentNumber");
      memcpy(SaleRec.DocNum, sTmp, sTmp.GetLength());

      sTmp = myRs.GetItem("DocumentTitle");
      iTmp = sTmp.GetLength();
      if (iTmp > SIZ_3K_GDOCTYPE)
         iTmp = SIZ_3K_GDOCTYPE;
      memcpy(SaleRec.DocType, sTmp, iTmp);

      sTmp = myRs.GetItem("SalePrice");
      memcpy(SaleRec.SalePrice, sTmp, sTmp.GetLength());

      sTmp = myRs.GetItem("SalePrice");
      memcpy(SaleRec.SalePrice, sTmp, sTmp.GetLength());

      // Get grantors grantees
      iGrantors=iGrantees = 0;
      sTmp = myRs.GetItem("NameCnt");
      iNameCnt = atoi(sTmp);
      for (iTmp = 1; iTmp <= iNameCnt; iTmp++ )
      {
         sprintf(acTmp, "Type%d", iTmp);
         sType = myRs.GetItem(acTmp);
         if (sType == "O")
         {
            if (iGrantors < 2)
            {
               sprintf(acTmp, "Name%d", iTmp);
               sTmp = myRs.GetItem(acTmp);
               sTmp.Remove(',');
               iLen = sTmp.GetLength();
               if (iLen > SIZ_3K_GRANTOR)
                  iLen = SIZ_3K_GRANTOR;
               memcpy((char *)&SaleRec.Grantors[iGrantors++][0], sTmp, iLen);
            }
         } else if (iGrantees < 2)
         {  // Include type "E" and "I"
            sprintf(acTmp, "Name%d", iTmp);
            sTmp = myRs.GetItem(acTmp);
            sTmp.Remove(',');
            iLen = sTmp.GetLength();
            if (iLen > SIZ_3K_GRANTOR)
               iLen = SIZ_3K_GRANTOR;
            memcpy((char *)&SaleRec.Grantees[iGrantees++][0], sTmp, iLen);
         }
      }

      sTmp = myRs.GetItem("APN_Matched");
      SaleRec.ApnMatch = *((LPCSTR)sTmp);
      sTmp = myRs.GetItem("Owner_Matched");
      SaleRec.OwnerMatch = *((LPCSTR)sTmp);

      SaleRec.CRLF[0] = '\n';
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec, fdSale);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   myRs.Close();
   fclose(fdSale);

   // Rename old file if exist
   if (!_access(acGrGrFile, 0))
   {
      sprintf(acTmp, acGrGrTmpl, pCnty, "SAV");
      if (!_access(acTmp, 0))
         remove(acTmp);
      rename(acGrGrFile, acTmp);
   }

   // Sort output file - remove duplicate
   LogMsg("Sort output file %s to %s", acTmpFile, acGrGrFile);
   sprintf(acTmp, "S(1,9,C,A,10,8,C,D,18,12,C,D) DUPO(1,29)");
   long lRet = sortFile(acTmpFile, acGrGrFile, acTmp);

   LogMsg("Total number of output matched records: %ld (%ld)", lCnt, lRet);

   return 0;
}

/******************************** But_ExtractGrGr ****************************
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int But_ExtrSaleMatched(char *pCnty)
{
   char     acTmp[256], acGrGrDat[256];
   int      iTmp;
   long     lCnt=0;

   CString  sTmp, sApn;
   hlAdoRs  myRs;
   FILE     *fdSale;
   SALE_REC SaleRec;

   sprintf(acGrGrDat, acGrGrTmpl, pCnty, "DAT");
   LogMsg("Extract matched grgr to %s", acGrGrDat);

   // Open output file
   if (!(fdSale = fopen(acGrGrDat, "w")))
   {
      LogMsg("***** Error creating %s file", acGrGrDat);
      return -1;
   }

   // Select statement
   //sprintf(acTmp, "SELECT * FROM %s_GrGr WHERE APN_Matched='Y' ORDER BY APN, RecordingDate", pCnty);
   sprintf(acTmp, "SELECT * FROM %s_GrGr WHERE APN IS NOT NULL ORDER BY APN, RecordingDate", pCnty);
   try
   {
      myRs.Open(hButDb, acTmp);
   }
   AdoCatch(e)
   {
      LogMsg("***** Error executing command [%s] : %s", acTmp, ComError(e));
      return -1;
   }

   // Loop through
   while (myRs.next())
   {
      memset((void *)&SaleRec, 32, sizeof(SALE_REC));
      sApn = myRs.GetItem("Apn");
      memcpy(SaleRec.acApn, sApn, sApn.GetLength());
      sTmp = myRs.GetItem("DocumentNumber");
      memcpy(SaleRec.acDocNum, sTmp, sTmp.GetLength());
      sTmp = myRs.GetItem("RecordingDate");
      memcpy(SaleRec.acDocDate, sTmp, sTmp.GetLength());

      sTmp = myRs.GetItem("DocumentTitle");
      iTmp = sTmp.GetLength();
      if (iTmp > SALE_SIZ_DOCTYPE) iTmp = SALE_SIZ_DOCTYPE;
      memcpy(SaleRec.acDocType, sTmp, iTmp);

      sTmp = myRs.GetItem("SalePrice");
      memcpy(SaleRec.acSalePrice, sTmp, sTmp.GetLength());

      // Since we don't merge seller from GrGr file, skip seller

      // Get stamp amount - Ignore this too

      SaleRec.CRLF[0] = '\n';
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec, fdSale);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   myRs.Close();
   fclose(fdSale);

   // Append to cumulative file
   sprintf(acTmp, acGrGrTmpl, pCnty, "SLS");
   LogMsg("Append to %s", acTmp);
   iTmp = appendTxtFile(acGrGrDat, acTmp);
   if (iTmp < 0)
      LogMsg("***** Error appending %s to cum sale file %s", acGrGrDat, acTmp);

   LogMsg("Total number of output matched records: %ld", lCnt);
   return 0;
}

/********************************** GetRollData *******************************
 *
 * Populate GrGr record with roll data.
 *    iFlag = 1 will close file
 *
 * Return 0 if successful, -1 if error, 1 if not found.
 *
 ******************************************************************************/

int GetRollData(BUTGRGR *pGrgr, int iFlag)
{
   static   HANDLE fhRoll=NULL;
   static   char acBuf[2048];

   char     *pTmp, *pTmp1, acTmp[256];
   DWORD    nBytesRead;
   BOOL     bRet;
   int      iTmp, iRet = 0;

   if (iFlag == 1)
   {
      if (fhRoll)
         CloseHandle(fhRoll);
      fhRoll = NULL;
      return 0;
   }

   if (!fhRoll)
   {
      sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
      if (_access(acTmp, 0))
         sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");

      // Open Input file
      LogMsg("Open input file for GetRollData(): %s", acTmp);
      fhRoll = CreateFile(acTmp, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

      if (fhRoll == INVALID_HANDLE_VALUE)
      {
         fhRoll = NULL;
         return -1;
      }

      memset(acBuf, 0, 2048);
      bRet = ReadFile(fhRoll, acBuf, iRecLen, &nBytesRead, NULL);
      // Skip first record if needed
      if (!memcmp(acBuf, "999999", 6))
         bRet = ReadFile(fhRoll, acBuf, iRecLen, &nBytesRead, NULL);
   }

   pTmp = (char *)&acBuf[0];
   while (bRet)
   {
      if (!(iTmp = memcmp(acBuf, pGrgr->APN, 9)))
      {
         memcpy(pGrgr->S_Num,  pTmp+OFF_S_STRNUM, SIZ_M_STRNUM);
         memcpy(pGrgr->S_Sub,  pTmp+OFF_S_STR_SUB, SIZ_M_STR_SUB);
         memcpy(pGrgr->S_Dir,  pTmp+OFF_S_DIR, SIZ_M_DIR);
         memcpy(pGrgr->S_Name, pTmp+OFF_S_STREET, SIZ_M_STREET);
         // Need translation
         iTmp = atoin(pTmp+OFF_S_SUFF, SIZ_M_SUFF);
         if (iTmp > 0)
         {
            pTmp1 = GetSfxStr(iTmp);
            memcpy(pGrgr->S_Sfx, pTmp1, strlen(pTmp1));
         }
         memcpy(pGrgr->S_Unit, pTmp+OFF_S_UNITNO, SIZ_M_UNITNO);
         // Need translation
         iTmp = atoin(pTmp+OFF_S_CITY, 3);
         if (iTmp > 0)
         {
            pTmp1 = GetCityName(iTmp);
            if (pTmp1)
               memcpy(pGrgr->S_City, pTmp1, strlen(pTmp1));
            else
               iRet = 1;
         }
         memcpy(pGrgr->S_State,pTmp+OFF_S_ST, SIZ_M_ST);
         memcpy(pGrgr->S_Zip,  pTmp+OFF_S_ZIP, SIZ_M_ZIP+SIZ_M_ZIP4);

         memcpy(pGrgr->Legal,  pTmp+OFF_LEGAL, SIZ_LEGAL);
         memcpy(pGrgr->R_Name1,pTmp+OFF_NAME1, SIZ_NAME1-2);
         memcpy(pGrgr->CareOf, pTmp+OFF_CARE_OF, SIZ_CARE_OF);

         memcpy(pGrgr->M_Num,  pTmp+OFF_M_STRNUM, SIZ_M_STRNUM);
         memcpy(pGrgr->M_Sub,  pTmp+OFF_M_STR_SUB, SIZ_M_STR_SUB);
         memcpy(pGrgr->M_Dir,  pTmp+OFF_M_DIR, SIZ_M_DIR);
         memcpy(pGrgr->M_Name, pTmp+OFF_M_STREET, SIZ_M_STREET);
         memcpy(pGrgr->M_Sfx,  pTmp+OFF_M_SUFF, SIZ_M_SUFF);
         memcpy(pGrgr->M_Unit, pTmp+OFF_M_UNITNO, SIZ_M_UNITNO);
         memcpy(pGrgr->M_City, pTmp+OFF_M_CITY, SIZ_M_CITY);
         memcpy(pGrgr->M_State,pTmp+OFF_M_ST, SIZ_M_ST);
         memcpy(pGrgr->M_Zip,  pTmp+OFF_M_ZIP, SIZ_M_ZIP+SIZ_M_ZIP4);

         memcpy(pGrgr->XferDate,pTmp+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
         memcpy(pGrgr->XferDoc, pTmp+OFF_TRANSFER_DOC, SIZ_TRANSFER_DOC);
         break;
      } else if (iTmp > 0)
      {
         iRet = 1;
         break;
      }

      bRet = ReadFile(fhRoll, acBuf, iRecLen, &nBytesRead, NULL);
      if (nBytesRead != iRecLen)
      {
         iRet = 1;
         break;
      }
   }

   return iRet;
}

/********************************** But_ExtrGrGrAll ***************************
 *
 * Make sure to add Most current sale date and price
 *
 ******************************************************************************/

int But_ExtrGrGrAll(char *pCnty)
{
   char     acTmp[256];
   int      iRet, iTmp, iNameCnt, iGrantors, iGrantees;
   long     lCnt=0, lTmp;
   double   dTax;

   CString  sTmp, sApn, sType;
   hlAdoRs  myRs;
   FILE     *fdSale;
   BUTGRGR  GrGrRec;

   sprintf(acTmp, acDETmpl, pCnty, pCnty);
   LogMsg("But_ExtrGrGrAll() - %s for DataEntry", acTmp);

   // Open output file
   if (!(fdSale = fopen(acTmp, "w")))
   {
      LogMsg("***** Error creating %s file", acTmp);
      return -1;
   }

   // Select statement
   sprintf(acTmp, "SELECT * FROM %s_GrGr ORDER BY APN", pCnty);
   try
   {
      myRs.Open(hButDb, acTmp);
   }
   AdoCatch(e)
   {
      LogMsg("***** Error executing command [%s] : %s", acTmp, ComError(e));
      return -1;
   }

   // Loop through
   while (myRs.next())
   {

      memset((void *)&GrGrRec, 32, sizeof(BUTGRGR));
      sApn = myRs.GetItem("Apn");
      memcpy(GrGrRec.APN, sApn, sApn.GetLength());
      sTmp = myRs.GetItem("DocumentNumber");
      memcpy(GrGrRec.DocNum, sTmp, sTmp.GetLength());
      sTmp = myRs.GetItem("RecordingDate");
      memcpy(GrGrRec.DocDate, sTmp, sTmp.GetLength());
      sTmp = myRs.GetItem("DocumentTitle");
      iTmp = sTmp.GetLength();
      if (iTmp > SIZ_GR_TITLE) iTmp = SIZ_GR_TITLE;
      memcpy(GrGrRec.DocTitle, sTmp, iTmp);

      sTmp = myRs.GetItem("Tax");
      dTax = atof(sTmp);
      if (dTax > 0.0)
      {
         lTmp = (long)(dTax * 100);
         sprintf(acTmp, "%0.*ld", SIZ_GR_TAX, lTmp);
         memcpy(GrGrRec.Tax, acTmp, SIZ_GR_TAX);
      }

      sTmp = myRs.GetItem("SalePrice");
      lTmp = atol(sTmp);
      if (lTmp)
      {
         sprintf(acTmp, "%0.*ld", SIZ_GR_SALE, lTmp);
         memcpy(GrGrRec.SalePrice, acTmp, SIZ_GR_SALE);
      }

      sTmp = myRs.GetItem("NumberofPages");
      lTmp = atol(sTmp);
      if (lTmp)
      {
         sprintf(acTmp, "%0.*d", SIZ_GR_NUMPG, lTmp);
         memcpy(GrGrRec.NumPages, acTmp, SIZ_GR_NUMPG);
      }

      sTmp = myRs.GetItem("NameCnt");
      lTmp = atol(sTmp);
      if (lTmp)
      {
         sprintf(acTmp, "%0.*d", SIZ_GR_NAMECNT, lTmp);
         memcpy(GrGrRec.NameCnt, acTmp, SIZ_GR_NAMECNT);
      }

      iGrantors=iGrantees = 0;
      iNameCnt = atoi(sTmp);
      for (iTmp = 1; iTmp <= iNameCnt; iTmp++ )
      {
         sprintf(acTmp, "Type%d", iTmp);
         sType = myRs.GetItem(acTmp);
         if (sType == "O")
         {
            if (iGrantors < 5)
            {
               sprintf(acTmp, "Name%d", iTmp);
               sTmp = myRs.GetItem(acTmp);
               GrGrRec.Grantor[iGrantors][0] = 'O';
               memcpy((char *)&GrGrRec.Grantor[iGrantors++][1], sTmp, sTmp.GetLength());
            }
         } else if (iGrantees < 5)
         {  // Include type "E" and "I"
            sprintf(acTmp, "Name%d", iTmp);
            sTmp = myRs.GetItem(acTmp);
            GrGrRec.Grantee[iGrantees][0] = *((LPCSTR)sType);
            memcpy((char *)&GrGrRec.Grantee[iGrantees++][1], sTmp, sTmp.GetLength());
         }
      }

      sTmp = myRs.GetItem("APN_Matched");
      GrGrRec.APN_Matched = *((LPCSTR)sTmp);
      sTmp = myRs.GetItem("Owner_Matched");
      GrGrRec.Owner_Matched = *((LPCSTR)sTmp);
      sTmp = myRs.GetItem("MoreName");
      GrGrRec.MoreName = *((LPCSTR)sTmp);
      sTmp = myRs.GetItem("ReferenceData");
      memcpy(GrGrRec.ReferenceData, sTmp, sTmp.GetLength());

      if (sApn > "0")
      {
         iRet = GetRollData(&GrGrRec, 0);
         if (iRet < 0)
         {
            LogMsg0("***** Error accessing roll file to populate GrGr for DataEntry [%s]", sApn);
            break;
         } else if (iRet)
            LogMsg0("*** APN not found: %s", sApn);
      }

      GrGrRec.CRLF[0] = '\n';
      GrGrRec.CRLF[1] = 0;
      fputs((char *)&GrGrRec, fdSale);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }
   printf("\r%u\n", lCnt);

   // Close recordset and outfile
   myRs.Close();
   fclose(fdSale);

   // Close roll file
   iRet = GetRollData(&GrGrRec, 1);

   // We don't need to resort output file since the select order already sorted

   LogMsg("Total number of output GrGr records: %ld", lCnt);
   return 0;
}

/********************************** But_ExtrGrGr ******************************
 *
 * Extract all record from SQL and append to But_Grgr.sls
 *
 ******************************************************************************/

int But_ExtrGrGr(char *pCnty)
{
   char     acTmp[_MAX_PATH], acTmpFile[_MAX_PATH], acSlsFile[_MAX_PATH];
   char     acGrGrRec[2048];
   int      iRet, iTmp, iNameCnt, iGrantors, iGrantees;
   long     lCnt=0, lTmp;
   double   dTax;

   CString  sTmp, sApn, sDocNum, sType;
   hlAdoRs  myRs;
   FILE     *fdSale;
   GRGR_DEF *pGrGrRec = (GRGR_DEF *)&acGrGrRec[0];

   sprintf(acTmpFile, acGrGrTmpl, pCnty, pCnty, "dat");
   LogMsg("But_ExtrGrGr() - output to %s", acTmpFile);

   // Load translate table

   // Open output file
   if (!(fdSale = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating %s file", acTmp);
      return -1;
   }

   // Select statement
   sprintf(acTmp, "SELECT * FROM %s_GrGr WHERE APN IS NOT NULL ORDER BY APN", pCnty);
   try
   {
      myRs.Open(hButDb, acTmp);
   }
   AdoCatch(e)
   {
      LogMsg("***** Error executing command [%s] : %s", acTmp, ComError(e));
      return -1;
   }
   iRet = 0;

   // Loop through
   while (myRs.next())
   {
      memset((void *)&acGrGrRec, ' ', sizeof(GRGR_DEF));
      sApn = myRs.GetItem("Apn");
      memcpy(pGrGrRec->APN, sApn, sApn.GetLength());
      sDocNum = myRs.GetItem("DocumentNumber");
      memcpy(pGrGrRec->DocNum, sDocNum, sDocNum.GetLength());
      sTmp = myRs.GetItem("RecordingDate");
      if (sTmp.IsEmpty())
      {
         LogMsg0("---> Missing date on DocNum: %s", sDocNum);
         continue;
      }
      memcpy(pGrGrRec->DocDate, sTmp, sTmp.GetLength());
      sTmp = myRs.GetItem("DocumentTitle");
      iTmp = sTmp.GetLength();
      if (iTmp > SIZ_GR_TITLE) iTmp = SIZ_GR_TITLE;
      memcpy(pGrGrRec->DocTitle, sTmp, iTmp);

      // Translate to PQ DocType

      // Tax amount
      sTmp = myRs.GetItem("Tax");
      dTax = atof(sTmp);
      if (dTax > 0.0)
      {
         lTmp = (long)(dTax * 100);
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pGrGrRec->Tax, acTmp, iTmp);
      }

      sTmp = myRs.GetItem("SalePrice");
      lTmp = atol(sTmp);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pGrGrRec->SalePrice, acTmp, iTmp);
      } else if (dTax > 0.0)
      {
         lTmp = (long)(dTax * SALE_FACTOR);
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pGrGrRec->SalePrice, acTmp, iTmp);
      }

      sTmp = myRs.GetItem("NumberofPages");
      lTmp = atol(sTmp);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pGrGrRec->NumPages, acTmp, iTmp);
      }

      sTmp = myRs.GetItem("NameCnt");
      iNameCnt = atol(sTmp);
      if (iNameCnt > 0)
         memcpy(pGrGrRec->NameCnt, sTmp, sTmp.GetLength());

      iGrantors=iGrantees = 0;
      if (iNameCnt > 10) iNameCnt = 10;
      for (iTmp = 1; iTmp <= iNameCnt; iTmp++ )
      {
         sprintf(acTmp, "Type%d", iTmp);
         sType = myRs.GetItem(acTmp);
         if (sType == "O")
         {
            if (iGrantors < MAX_NAMES)
            {
               sprintf(acTmp, "Name%d", iTmp);
               sTmp = myRs.GetItem(acTmp);
               pGrGrRec->Grantors[iGrantors].NameType[0] = 'O';
               memcpy(pGrGrRec->Grantors[iGrantors++].Name, sTmp, sTmp.GetLength());
            }
         } else if (sType == "E" || sType == "I")
         {  // Include type "E" and "I"
            if (iGrantees < MAX_NAMES)
            {
               sprintf(acTmp, "Name%d", iTmp);
               sTmp = myRs.GetItem(acTmp);
               pGrGrRec->Grantees[iGrantees].NameType[0] = *((LPCSTR)sType);
               memcpy(pGrGrRec->Grantees[iGrantees++].Name, sTmp, sTmp.GetLength());
            } else if (iGrantees == MAX_NAMES)
               pGrGrRec->MoreName = 'Y';
         }
      }

      sTmp = myRs.GetItem("APN_Matched");
      if (sTmp > " ")
         pGrGrRec->APN_Matched = *((LPCSTR)sTmp);
      sTmp = myRs.GetItem("Owner_Matched");
      if (sTmp > " ")
         pGrGrRec->Owner_Matched = *((LPCSTR)sTmp);
      sTmp = myRs.GetItem("ReferenceData");
      if (sTmp > " ")
         memcpy(pGrGrRec->ReferenceData, sTmp, sTmp.GetLength());

      pGrGrRec->CRLF[0] = '\n';
      pGrGrRec->CRLF[1] = 0;
      fputs(acGrGrRec, fdSale);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }
   printf("\r%u\n", lCnt);

   // Close recordset and outfile
   myRs.Close();
   fclose(fdSale);

   // Append to cumulative file
   sprintf(acSlsFile, acGrGrTmpl, pCnty, pCnty, "sls");
   if (lCnt > 0 && !_access(acSlsFile, 0))
   {
      char acSortCtl[256], acSrtFile[256];

      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, DocNum asc, RecDate asc, Source 
      sprintf(acSortCtl,"S(17,15,C,A,37,8,C,A,1,12,C,D,125,10,C,D,1109,2,C,A) F(TXT) DUPO(B%d,1,44) ", sizeof(GRGR_DEF)+64);
      sprintf(acSrtFile, acGrGrTmpl, pCnty, pCnty, "srt");
      sprintf(acTmp, "%s+%s", acTmpFile, acSlsFile);

      // Sort But_GrGr.dat+But_Grgr.Sls to But_GrGr.srt
      lCnt = sortFile(acTmp, acSrtFile, acSortCtl);
      if (lCnt > 0)
      {
         sprintf(acTmpFile, acGrGrTmpl, pCnty, pCnty, "bak");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         MoveFile(acSlsFile, acTmpFile);
         MoveFile(acSrtFile, acSlsFile);

      //   // Extract to GrGr_Exp.dat
      //   sprintf(acTmpFile, acEGrGrTmpl, myCounty.acCntyCode, "dat");
      //   lCnt = GrGr_ExtrSaleMatched(acSlsFile, acTmpFile, (IDX_TBL5 *)&BUT_DocTitle[0]);
      //   if (lCnt > 0)
      //      iRet = 0;
      //   else
      //      iRet = 1;
      }
   } else if (lCnt > 0)
      CopyFile(acTmpFile, acSlsFile, false);

   LogMsg("Total number of output GrGr records: %ld", lCnt);
   return iRet;
}

/********************************* But_Load_Roll ******************************
 *
 * Input file sometimes not sorted.  Be safe, sort it before processing.
 *
 ******************************************************************************/

int But_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Loading roll update file");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open roll file
   sprintf(acBuf, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acRollFile, acBuf, "S(#1,C,A) OMIT(#1,C,GT,\"99\",OR,#1,C,LT,\"0\")  F(TXT)  DEL(124)");

   LogMsg("Open Roll file %s", acBuf);
   fdRoll = fopen(acBuf, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acBuf);
      return -2;
   }

   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Sort Tax file - Sort on Asmt, TaxYear, RollChgNum, ChrgDate1 & 2, PaidDate1 & 2
   strcpy(acRec, acTaxFile);
   sprintf(acTaxFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sorting %s to %s", acRec, acTaxFile);
   iRet = sortFile(acRec, acTaxFile, "S(#1,C,A,#16,C,D,#18,C,D,#7,DAT,A,#15,DAT,A) F(TXT) B(453,R)");
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting Tax file %s to %s", acRec, acTaxFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record & Get 1st rec
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (pTmp && *pTmp != '0');
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   lLastRecDate=iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

#ifdef _DEBUG
      //if (!memcmp(acBuf, "072112011000", 9))
      //   lRet = 0;
#endif

NextRollRec:
      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = But_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = But_MergeSitus(acBuf);

            // Merge Char
            if (fdChar)
               lRet = MB_MergeStdChar(acBuf, NULL, NULL, NULL);
               //lRet = But_MergeChar(acBuf);

            // Merge Taxes - Check for roll change
            if (fdTax)
               lRet = MB_MergeTaxRC(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         iTmp = myGetStrTC(acRollRec, cDelim, MB_ROLL_M_ADDR4, 1024, fdRoll);
         if (iTmp < 20)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = But_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = But_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = MB_MergeStdChar(acRec, NULL, NULL, NULL);
               //lRet = But_MergeChar(acRec);

             // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxRC(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_SALE1_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }

         // Get next roll record
         lCnt++;
         iTmp = myGetStrTC(acRollRec, cDelim, MB_ROLL_M_ADDR4, 1024, fdRoll);
         if (iTmp < 20)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[iSkipQuote], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = But_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = But_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            //lRet = But_MergeChar(acRec);
            lRet = MB_MergeStdChar(acRec, NULL, NULL, NULL);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxRC(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_SALE1_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }

      // Get next roll record
      iTmp = myGetStrTC(acRollRec, cDelim, MB_ROLL_M_ADDR4, 1024, fdRoll);
      if (iTmp < 20)
         bEof = true;    // Signal to stop
      lCnt++;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u\n\n", lRecCnt);

   if (lRecCnt < iRetiredRec)
   {
      LogMsg("***** Input file %s might be corrupted.  Please check.", acRollFile);
      return -999;
   } else
      return 0;
}

/******************************** But_Load_Roll2 ******************************
 *
 * In this function, we create R01 record first
 *
 ******************************************************************************/

//int But_Load_Roll2(int iSkip)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
//   char     acOutFile[_MAX_PATH];
//
//   HANDLE   fhOut;
//   int      iRet, iTmp, iRollUpd=0;
//   DWORD    nBytesWritten;
//   BOOL     bRet;
//   long     lRet=0, lCnt=0;
//
//   // Open roll file
//   LogMsg("Open Roll file %s", acRollFile);
//   fdRoll = fopen(acRollFile, "r");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acRollFile);
//      return -2;
//   }
//
//   lLastFileDate = getFileDate(acRollFile);
//
//   // Open Char file
//   LogMsg("Open Char file %s", acCharFile);
//   fdChar = fopen(acCharFile, "r");
//   if (fdChar == NULL)
//   {
//      LogMsg("***** Error opening Char file: %s\n", acCharFile);
//      return -2;
//   }
//   // Open Situs file
//   LogMsg("Open Situs file %s", acSitusFile);
//   fdSitus = fopen(acSitusFile, "r");
//   if (fdSitus == NULL)
//   {
//      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
//      return -2;
//   }
//   // Open Sales file
//   /*
//   LogMsg("Open Sales file %s", acSalesFile);
//   fdSale = fopen(acSalesFile, "r");
//   if (fdSale == NULL)
//   {
//      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
//      return -2;
//   }
//   */
//   // Open Exe file
//   LogMsg("Open Exe file %s", acExeFile);
//   fdExe = fopen(acExeFile, "r");
//   if (fdExe == NULL)
//   {
//      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
//      return -2;
//   }
//
//   // Sort Tax file - Sort on Asmt, TaxYear, ChrgDate1 & 2, PaidDate1 & 2
//   strcpy(acRollRec, acTaxFile);
//   sprintf(acTaxFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   LogMsg("Sorting %s to %s", acRollRec, acTaxFile);
//   iRet = sortFile(acRollRec, acTaxFile, "S(#1,C,A,#16,C,D,#7,DAT,A,#15,DAT,A) F(TXT) B(360,R)");
//   if (iRet <= 0)
//   {
//      LogMsg("***** ERROR sorting Tax file %s to %s", acRollRec, acTaxFile);
//      return -2;
//   }
//
//   // Open Tax file
//   LogMsg("Open Tax file %s", acTaxFile);
//   fdTax = fopen(acTaxFile, "r");
//   if (fdTax == NULL)
//   {
//      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
//      return -2;
//   }
//
//   // Open lien file
//   fdLienExt = NULL;
//   sprintf(acRollRec, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
//   if (!_access(acRollRec, 0))
//   {
//      LogMsg("Open Lien file %s", acRollRec);
//      fdLienExt = fopen(acRollRec, "r");
//      if (fdLienExt == NULL)
//      {
//         LogMsg("***** Error opening lien file: %s\n", acRollRec);
//         return -7;
//      }
//   }
//
//   // Open Output file - use tmp file so we can sort at the end to R01 file
//   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error opening output file: %s\n", acOutFile);
//      return -4;
//   }
//
//   // Drop header record
//   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
//      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
//
//   // Get 1st rec
//   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
//
//   // Create hdr record
//   memset(acBuf, '9', iRecLen);
//   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//
//   lLastRecDate=iNoMatch=iBadCity=iBadSuffix=0;
//
//   // Merge loop
//   while (pTmp)
//   {
//      // Create roll record
//      iRet = But_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, CREATE_R01);
//      if (!iRet)
//      {
//         // Merge Situs
//         if (fdSitus)
//            lRet = But_MergeSitus(acBuf);
//
//         // Merge Taxes
//         if (fdTax)
//         {
//            lRet = MB_MergeTax(acBuf);
//         }
//
//         // If roll changed, update Exe value
//         if (acBuf[OFF_ROLLCHG_FLG] == 'Y')
//         {
//            iRollUpd++;
//            if (fdExe)
//               lRet = MB_MergeExe(acBuf);
//         } else if (fdLienExt)
//            lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);
//
//         // Merge Char
//         if (fdChar)
//            lRet = But_MergeChar(acBuf);
//
//         // Remove old sale data
//         //if (bClearSales)
//         //   ClearOldSale(acBuf);
//
//         // Merge Sales
//         //if (fdSale)
//         //   lRet = But_MergeSale(acBuf);
//
//         // Save last recording date
//         lRet = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
//         if (lRet > lLastRecDate && lRet < lToday)
//            lLastRecDate = lRet;
//
//         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//         if (!bRet)
//         {
//            LogMsg("Error occurs: %d\n", GetLastError());
//            break;
//         }
//
//         lRecCnt++;
//      }
//
//      // Get next roll record
//      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fdLienExt)
//      fclose(fdLienExt);
//   if (fdChar)
//      fclose(fdChar);
//   if (fdSitus)
//      fclose(fdSitus);
//   //if (fdSale)
//   //   fclose(fdSale);
//   if (fdExe)
//      fclose(fdExe);
//   if (fdTax)
//      fclose(fdTax);
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   LogMsg("Total records processed:    %u", lCnt);
//   LogMsg("Total output records:       %u", lRecCnt);
//   LogMsg("Total roll change records:  %u", iRollUpd);
//   LogMsg("Total bad-city records:     %u", iBadCity);
//   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
//
//   LogMsg("Number of Situs matched:    %u", lSitusMatch);
//   LogMsg("Number of Char matched:     %u", lCharMatch);
//   LogMsg("Number of Exe matched:      %u", lExeMatch);
//   //LogMsg("Number of Sale matched:     %u", lSaleMatch);
//   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);
//
//   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
//   LogMsg("Number of Char skiped:      %u", lCharSkip);
//   LogMsg("Number of Exe skiped:       %u", lExeSkip);
//   //LogMsg("Number of Sale skiped:      %u", lSaleSkip);
//   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);
//
//   LogMsg("Last recording date:        %u", lLastRecDate);
//
//   printf("\nTotal output records: %u\n\n", lRecCnt);
//
//   return 0;
//}

/******************************* But_MakeDocLink *******************************
 *
 * Format DocLink
 *
 ******************************************************************************/

void But_MakeDocLink(LPSTR pDocLink, LPSTR pDoc, LPSTR pDate)
{
   char  acTmp[256], acDocName[256];

   *pDocLink = 0;
   if (*pDoc > ' ' && *(pDoc+5) > ' ' && *pDate > ' ')
   {
      sprintf(acDocName, "%.4s\\%.3s\\%.4s%.7s", pDate, pDoc+5, pDate, pDoc+5);     
      sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocName);

      if (!_access(acTmp, 0))
         strcpy(pDocLink, acDocName);
   }
}

/******************************* But_FmtDocLinks ****************************
 *
 * Format DocLinks as bbb/yyyybbbpppp,bbb/yyyybbbpppp,bbb/yyyybbbpppp,bbb/yyyybbbpppp
 * which are Doclink1, Doclink2, Doclink3, DocXferlink
 *
 ****************************************************************************

int But_FmtDocLinks(int iSkip)
{
   char     acBuf[MAX_RECSIZE], acTmp[_MAX_PATH];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];
   char     *pDate, *pDoc, acDocLink[64], acDocLinks[256];
   char     *pCnty = "BUT";

   HANDLE   fhIn, fhOut;

   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0, lLinkCnt=0;
   int      iRet = 0;

   LogMsg("Format Doclinks ...");

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "T01");

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -1;
   }
   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acOutFile);
      return -1;
   }

   // Copy skip record
   memset(acBuf, ' ', MAX_RECSIZE);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (true)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

#ifdef _DEBUG
//      if (!memcmp(acBuf, "08529006", 8))
//         bRet = true;
#endif

      // Reformat doc#

      // Reset old links
      memset((char *)&acBuf[OFF_DOCLINKS], ' ', SIZ_DOCLINKS);

      // Doc #1
      pDate = (char *)&acBuf[OFF_SALE1_DOC];
      pDoc  = (char *)&acBuf[OFF_SALE1_DOC+5];
      acDocLinks[0] = 0;

      if (*pDoc > ' ')
      {
         sprintf(acDocLink, "%.4s\\%.3s\\%.4s%.7s", pDate, pDoc, pDate, pDoc);
         sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocLink);

         if (!_access(acTmp, 0))
         {
            lLinkCnt++;
            strcat(acDocLinks, acDocLink);
         }
      }

      // Doc #2
      pDate = (char *)&acBuf[OFF_SALE2_DOC];
      pDoc  = (char *)&acBuf[OFF_SALE2_DOC+5];
      strcat(acDocLinks, ",");

      if (*pDoc > ' ')
      {
         sprintf(acDocLink, "%.3s\\%.4s%.7s", pDoc, pDate, pDoc);
         sprintf(acTmp, "%s\\%.4s\\%s.pdf", acDocPath, pDate, acDocLink);

         if (!_access(acTmp, 0))
         {
            lLinkCnt++;
            strcat(acDocLinks, acDocLink);
         }
      }

      // Doc #3
      pDate = (char *)&acBuf[OFF_SALE3_DOC];
      pDoc  = (char *)&acBuf[OFF_SALE3_DOC+5];
      strcat(acDocLinks, ",");

      if (*pDoc > ' ')
      {
         sprintf(acDocLink, "%.3s\\%.4s%.7s", pDoc, pDate, pDoc);
         sprintf(acTmp, "%s\\%.4s\\%s.pdf", acDocPath, pDate, acDocLink);

         if (!_access(acTmp, 0))
         {
            lLinkCnt++;
            strcat(acDocLinks, acDocLink);
         }
      }

      // Transfer
      pDate = (char *)&acBuf[OFF_TRANSFER_DOC];
      pDoc  = (char *)&acBuf[OFF_TRANSFER_DOC+5];
      strcat(acDocLinks, ",");

      if (*pDoc > ' ')
      {
         sprintf(acDocLink, "%.3s\\%.4s%.7s", pDoc, pDate, pDoc);
         sprintf(acTmp, "%s\\%.4s\\%s.pdf", acDocPath, pDate, acDocLink);

         if (!_access(acTmp, 0))
         {
            lLinkCnt++;
            strcat(acDocLinks, acDocLink);
         }
      }

      // Update DocLinks
      iRet = strlen(acDocLinks);
      if (iRet > 10)
         memcpy((char *)&acBuf[OFF_DOCLINKS], acDocLinks, iRet);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename out file
   iRet = 0;
   if (lCnt > 100000)
   {
      if (remove(acRawFile))
      {
         LogMsg("***** Error removing file: %s (%d)", acRawFile, errno);
         iRet = errno;
      } else if (rename(acOutFile, acRawFile))
      {
         LogMsg("***** Error renaming temp file: %s --> %s (%d)", acOutFile, acRawFile, errno);
         iRet = errno;
      }
   }

   LogMsg("FmtDocLinks completed.  %d doclinks assigned.\n", lLinkCnt);
   return iRet;
}

/********************************* But_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int But_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L_ASMT], strlen(apTokens[L_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L_FEEPARCEL], strlen(apTokens[L_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "04BUT", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values
   // Land
   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);
   long lFixt   = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   long lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L_TAXAMT1]);
   double dTax2 = atof(apTokens[L_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      if (lTmp > lGross)
      {
         LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L_TRA], strlen(apTokens[L_TRA]));

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L_STATUS];

   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   strcpy(acTmp, apTokens[L_USECODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002110077000", 9))
   //   iTmp = 0;
#endif
   // Owner
   But_MergeOwner(pOutbuf, apTokens[L_OWNER]);

   // Situs
   //But_MergeSitus(pOutbuf, apTokens[L_SITUS1], apTokens[L_SITUS2]);

   // Mailing
   But_MergeMAdr(pOutbuf, apTokens[L_MAILADDRESS1], apTokens[L_MAILADDRESS2], apTokens[L_MAILADDRESS3], apTokens[L_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L_TAXABILITY], true, true);

   return 0;
}

/***************************** But_MergeRollExt ******************************
 *
 * Merge Zoning, DocNum, DocDate from roll file
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int But_MergeRollExt(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Drop header record & Get 1st rec
      do {
         iTmp = myGetStrTC(acRec, cDelim, MB_ROLL_M_ADDR4, 1024, fdZone);
      } while (acRec[0] != '0');
      pRec = (char *)&acRec[0];
   }

   do
   {
      if (!pRec)
      {
         fclose(fdZone);
         fdZone = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Roll rec %.*s", iApnLen, pRec);
         iRet = myGetStrTC(acRec, cDelim, MB_ROLL_M_ADDR4, 1024, fdZone);
         if (iRet < 1)
         {
            if (iRet == -3)
               LogMsg("***** File may be corrupted.  Too many tokens %.12s", acRec);
            pRec = NULL;
         }
         lZoneSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_M_ADDR4)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdZone);
         fdZone = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input roll record for APN=%s", apTokens[0]);
         iRet = -1;
      }

      return iRet;
   }

   // Merge data
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      char *pTmp;

      pTmp = strchr(apTokens[MB_ROLL_ZONING], ' ');
      if (pTmp)
         *pTmp = 0;

      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
      lZoneMatch++;
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0' && *(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
   {
      char acTmp[64], *pTmp;

      iTmp = atoin(apTokens[MB_ROLL_DOCNUM]+5, 7);
      if (iTmp > 0)
         sprintf(acTmp, "%.5s%0.7d", apTokens[MB_ROLL_DOCNUM], iTmp);
      else
         strcpy(acTmp, apTokens[MB_ROLL_DOCNUM]);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, strlen(acTmp));

      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
      else
         memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
   }

   // Get next record
   iTmp = myGetStrTC(acRec, cDelim, MB_ROLL_M_ADDR4, 1024, fdZone);
   if (iRet < 1)
      pRec = NULL;

   return 0;
}

/***************************** But_Load_LDR() *******************************
 *
 * Load TR601 file into 1900-byte record.
 *
 ****************************************************************************/

int But_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Zoning file
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acTmpFile, acRec, myCounty.acCntyCode, myCounty.acCntyCode);
   if (acTmpFile[0] > ' ')
   {
      LogMsg("Open Zoning file %s", acTmpFile);
      fdZone = fopen(acTmpFile, "r");
      if (fdZone == NULL)
      {
         LogMsg("***** Error opening Zoning file: %s\n", acTmpFile);
         return -2;
      }
   }

   /* 07/25/2014 - Input records may be on multiple rows, do not sort
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acBuf, acRec, myCounty.acCntyCode, myCounty.acCntyCode);
   if (acBuf[0] > ' ')
   {
      // Sort on ASMT
      sprintf(acTmpFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = sortFile(acBuf, acTmpFile, "S(#1,C,A)");

      LogMsg("Open Zoning file %s", acTmpFile);
      fdZone = fopen(acTmpFile, "r");
      if (fdZone == NULL)
      {
         LogMsg("***** Error opening Zoning file: %s\n", acTmpFile);
         return -2;
      }
   }
   */

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   iRet = myGetStrTC(acRec, cLdrSep, L_USERID, MAX_RECSIZE, fdRoll);

   // Init variables
   lLastRecDate=iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!feof(fdRoll))
   {
      lLDRRecCount++;

      // Create new R01 record
      iRet = But_MergeLien(acBuf, acRec);
      if (!iRet)
      {

         // Merge Char
         if (fdChar)
            lRet = But_MergeChar(acBuf);

         // Merge Situs
         if (fdSitus)
            lRet = But_MergeSitus(acBuf);

         // Merge Zoning
         if (fdZone)
            lRet = But_MergeRollExt(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      iRet = myGetStrTC(acRec, cLdrSep, L_USERID, MAX_RECSIZE, fdRoll);
      if (iRet < 1)
         break;
   }

   // Close files
   if (fdZone)
      fclose(fdZone);
   if (fdRoll)
      fclose(fdRoll);
   if (fdSitus)
      fclose(fdSitus);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Zoning matched:   %u", lZoneMatch);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Zoning skiped:    %u", lZoneSkip);
   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);
   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/***************************** But_CreateSCSale ******************************
 *
 * Copy from MB_CreateSCSale() and modify to fix problem that has CR embeded in OWNERNAME
 *
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * DateFmt:
 *    0 : Default. Auto format by checking input date
 *    MM_DD_YYYY_1 (MON)
 *    YYYY_MM_DD   (HUM)
 *
 * DocTypeFmt:
 *    0 : Default. Set DocType=1 (GD) if DocCode=1 or sale price > 0
 *    1 : AMA, BUT, MAD, SON
 *    2 : PLA
 *    3 : SHA, STA
 *
 * DocNumFmt:
 *    0 : Default. Copy DocNum as is
 *    1 : Format DocNum second part of DocNum to 7 digits (i.e. 2010R1234 = 2010R0001234)
 *    2 : Format DocNum second part of DocNum to n digits (i.e. 2010R0034 = 2010R34) (COL)
 *    3 : Remove all nonnumeric after 5th character and format to 6 digits (SON)
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int But_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp;

   LogMsg0("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   do {
      lTmp = ftell(fdSale);
      pTmp = fgets(acRec, 1024, fdSale);
   } while (pTmp && !isdigit(*pTmp));
   
   if (fseek(fdSale, lTmp, SEEK_SET))
   {
      LogMsg("***** Error setting current position in sale file using fseek()");
      fclose(fdSale);
      return -2;
   }

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   lLastRecDate = 0;

   // Loop through record set
   while (!feof(fdSale))
   {
      iTmp = myGetStrTC(acRec, cDelim, MB_SALES_CONFCODE, 1024, fdSale);
      if (iTmp < 1)
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "054550047000", 9))
      //   iTmp = 0;
#endif
      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
      {
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }

      // Docnum
      if (!iDocNumFmt)
      {
         // MON, NAP, PLA, SIS, SHA, LAK, MNO, YUB
         memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));
      } else if (iDocNumFmt == 1)
      {  // AMA, BUT, MAD
         if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R' && strchr(apTokens[MB_SALES_DOCNUM]+5, '-'))
         {
            memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));
         } else
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
            if (lTmp > 0)
            {
               sprintf(acTmp, "%.5s%0.7ld", apTokens[MB_SALES_DOCNUM], lTmp);
               memcpy(SaleRec.DocNum, acTmp, SALE_SIZ_DOCNUM);
            }
         }
      } else if (iDocNumFmt == 2)
      {  // COL
         if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R')
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%.5s%d", apTokens[MB_SALES_DOCNUM], lTmp);
               memcpy(SaleRec.DocNum, acTmp, iTmp);
            }
         }
      } else if (iDocNumFmt == 3)
      {  // SON
         iTmp = replNonNum(apTokens[MB_SALES_DOCNUM]+5, ' ');

         lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%.5s%0.6ld   ", apTokens[MB_SALES_DOCNUM], lTmp);
            memcpy(SaleRec.DocNum, acTmp, SALE_SIZ_DOCNUM);
         }
      } else if (iDocNumFmt == 4)
      {  // SBT
         if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R')
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%.5s%0.7d", apTokens[MB_SALES_DOCNUM], lTmp);
               memcpy(SaleRec.DocNum, acTmp, iTmp);
            }
         } else if (*(apTokens[MB_SALES_DOCNUM]+5) == 'R')
         {
            lTmp = atoin(apTokens[MB_SALES_DOCNUM]+6, 6);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%.4sR%0.7d", SaleRec.DocDate, lTmp);
               memcpy(SaleRec.DocNum, acTmp, iTmp);
            }
         }
      }

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1' || *apTokens[MB_SALES_GROUPSALE] == 'T')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001130011000", 9))
      //   iTmp = 0;
#endif
      // Confirmed sale price
      dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
      if (acTmp[0] > '0')
      {
         lPrice = atol(acTmp);
         if (lPrice < 1000)
         {
            // This doesn't look normal, but keep it for reference only
            iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            lPrice = 0;
         } else
            iTmp = sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      } else
         lPrice = 0;

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001160016000", 9))
      //   iTmp = 0;
#endif
      // Tax
      dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
      if (acTmp[0] > '0')
      {
         dTmp = atof(acTmp);

         // Save DocTax
         iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);

         // Calculate sale price
         lTmp = (long)(dTmp * SALE_FACTOR);
         iTmp = ((int)dTmp/100)*100;

         // Check for bad DocTax
         if (dTmp > 100000)
         {
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else if (lPrice > 100000 && lPrice == (lPrice/100)*100)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTmp);
            } else
            {
               LogMsg("??? (1) Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f, CP=%d.  Need investigation.  Ignore price.", SaleRec.Apn, SaleRec.DocNum, dTmp, lPrice);
               lPrice = 0;
            }
         } else if (lPrice > 0 && (long)dTmp >= lPrice+5000)
         {
            // Even though sale tax is questionable, we still rely on it if it is reasonable
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** (2) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               if (lPrice == (lPrice/100)*100)
                  LogMsg("*** (2) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTmp);
               else
               {
                  LogMsg("??? (2) Questionable Sale Tax Amt for %.12s: DocNum=%.12s, Price=%d, \ttax=%.2f).  Need investigation.", SaleRec.Apn, SaleRec.DocNum, lPrice, dTmp);
                  lPrice = lTmp;
               }
            }
         } else if (iTmp == (int)dTmp && iTmp > 10000)
         {
            if (lTmp != (lTmp/100)*100)
            {
               LogMsg("*** (3) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
               lPrice = lTmp;
         } else
            lPrice = lTmp;

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Ignore sale price.",
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE]);
               lPrice = 0;
            }
         }

         // Ignore sale price if less than 1000
         if (lPrice >= 10000)
            sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
         else if (lPrice >= 1000)
            sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
         else
            memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
         memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);
      }

      // Doc code - accept following code only
      int iDocCode = 0;
      if (isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         if (pDocTbl)
         {
            iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
            if (iTmp >= 0)
            {
               memcpy(SaleRec.DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
               if (lPrice < 100)
                  SaleRec.NoneSale_Flg = pDocTbl[iTmp].flag;
            }
         } else
         {
            iDocCode = atoi(apTokens[MB_SALES_DOCCODE]);
            if (iDocTypeFmt == 1)      // AMA, BUT, MAD
            {
               if (iDocCode ==1 || iDocCode == 12 || (iDocCode == 8 && lPrice > 0))
                  SaleRec.DocType[0] = '1';
               else if (iDocCode == 10)
                  memcpy(SaleRec.DocType, "75", 2);
               else
               {
                  SaleRec.NoneSale_Flg = 'Y';
                  if (iDocCode == 52)                        // Transfer - Default
                     memcpy(SaleRec.DocType, "52", 2);
                  else if (iDocCode == 2 || iDocCode == 4)       // Transfer
                     memcpy(SaleRec.DocType, "75", 2);
                  else if (iDocCode == 5 || iDocCode == 6)       // Partial Transfer
                     memcpy(SaleRec.DocType, "75", 2);
               }
            } else if (iDocTypeFmt == 2)  // PLA
            {
               switch (iDocCode)
               {
                  case 1:  // GD, Transfer reappr
                  case 5:  // New mobile home
                     SaleRec.DocType[0] = '1';
                     break;
                  case 2:  // Partial Transfer
                  case 10: // Timeshare - reappr
                  case 11: // Transfer - no reappr
                  case 12: // Partial Transfer - no reappr
                  case 15: // Strawman transfer
                     memcpy(SaleRec.DocType, "75", 2);
                     break;
                  case 3:
                     // Foreclosure
                     memcpy(SaleRec.DocType, "77", 2);
                     break;
                  case 4:
                     // Sheriff's deed
                     memcpy(SaleRec.DocType, "25", 2);
                     break;
                  case 8:
                     // Tax deed
                     memcpy(SaleRec.DocType, "67", 2);
                     break;
                  default:
                     break;
               }
            } else if (iDocTypeFmt == 3)  // SHA
            {
               switch (iDocCode)
               {
                  case 1:  // GD, Transfer reappr
                     SaleRec.DocType[0] = '1';
                     break;
                  case 2:  // Partial Transfer
                     memcpy(SaleRec.DocType, "57", 2);
                     break;
                  case 3:  // Non-Reappraisal event
                  case 5:  // Intermarrital
                  case 6:  // Vesting/Name change
                  case 11: // Add/Delete JT
                     SaleRec.NoneSale_Flg = 'Y';
                     break;
                  case 10:
                     // Tax deed
                     memcpy(SaleRec.DocType, "67", 2);
                     break;
                  case 19:
                     // Foreclosure
                     memcpy(SaleRec.DocType, "77", 2);
                     break;
                  case 60:
                     // Split/Combine
                     if (lPrice > 0)
                        SaleRec.DocType[0] = '1';
                     break;
                  default:
                     if (!lPrice)
                        SaleRec.NoneSale_Flg = 'Y';
                     break;
               }
            } else if (iDocTypeFmt == 4)  // SON
            {
               if (iDocCode ==1 || (lPrice > 0 && isdigit(SaleRec.DocNum[5])))
                  SaleRec.DocType[0] = '1';
               else
               {
                  if (iDocCode == 4)                           // Transfer
                  {
                     SaleRec.NoneSale_Flg = 'Y';
                     memcpy(SaleRec.DocType, "75", 2);
                  } else if (iDocCode == 5 || iDocCode == 6)   // Internal Doc
                  {
                     SaleRec.NoneSale_Flg = 'Y';
                     memcpy(SaleRec.DocType, "74", 2);
                  }
               }
            } else if (iDocCode == 1 || lPrice > 0)
               SaleRec.DocType[0] = '1';
         }
      } else if (!memcmp(apTokens[MB_SALES_DOCCODE], "GD", 2))
         SaleRec.DocType[0] = '1';

      // Save original DocCode
      iTmp = strlen(apTokens[MB_SALES_DOCCODE]);
      if (iTmp > SALE_SIZ_DOCCODE)
         iTmp = SALE_SIZ_DOCCODE;
      memcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], iTmp);

      // Transfer Type
      if (*apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         strcpy(acTmp, apTokens[MB_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         if (iTmp > SALE_SIZ_SELLER)
            iTmp = SALE_SIZ_SELLER;
         memcpy(SaleRec.Seller1, acTmp, iTmp);

         // Buyer
         strcpy(acTmp, apTokens[MB_SALES_BUYER]);
         iTmp = blankRem(acTmp);
         if (iTmp > SALE_SIZ_BUYER)
            iTmp = SALE_SIZ_BUYER;
         memcpy(SaleRec.Name1, acTmp, iTmp);

         SaleRec.CRLF[0] = 10;
         SaleRec.CRLF[1] = 0;
         fputs((char *)&SaleRec,fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);

   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(27,2,C,EQ,\"  \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -2;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            // Save old cumsale file
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);
            iTmp = rename(acCSalFile, acTmpFile);

            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -2;
      } else
         iTmp = rename(acOutFile, acCSalFile);

      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acCSalFile, acTmpFile);
      }
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp == -2)
      LogMsg("***** Error sorting output file");
   else if (iTmp)
      LogMsg("***** Error renaming to %s", acCSalFile);

   LogMsg("Number of Sale records processed: %d", lCnt);
   LogMsg("                          output: %d", lTmp);
   LogMsg("         Latetest recording date: %d\n", lLastRecDate);
   return iTmp;
}

/********************************* But_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int But_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%.12s (%d)", apTokens[L3_ASMT], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "04BUT", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&BUT_Exemption);

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (iTokens > L3_ISAGPRESERVE && *apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   But_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   //But_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   But_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2019: data is not good
   //if (*apTokens[L3_CURRENTDOCNUM] > '0')
   //{
   //   pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
   //   if (pTmp)
   //   {
   //      memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   //      vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
   //   }
   //} 

   //// Garage size
   //dTmp = atof(apTokens[L3_GARAGESIZE]);
   //if (dTmp > 0.0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_GAR_SQFT, dTmp);
   //   memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   //   *(pOutbuf+OFF_PARK_TYPE) = '2';                 // GARAGE/CARPORT
   //}

   //// Number of parking spaces
   //if (*apTokens[L3_GARAGE] == '0' || *apTokens[L3_GARAGE] == 'N')
   //   *(pOutbuf+OFF_PARK_TYPE) = 'H';                 // None
   //else if (*apTokens[L3_GARAGE] > '0' && *apTokens[L3_GARAGE] <= '9')
   //{
   //   iTmp = atol(apTokens[L3_GARAGE]);
   //   sprintf(acTmp, "%d", iTmp);
   //   vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
   //   if (dTmp > 100)
   //      *(pOutbuf+OFF_PARK_TYPE) = 'Z';              // Garage
   //   else
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
   //} else
   //{
   //   if (*(apTokens[L3_GARAGE]) == 'C')
   //      *(pOutbuf+OFF_PARK_TYPE) = 'C';              // Carport
   //   else if (*(apTokens[L3_GARAGE]) == 'A')
   //   {
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L3_GARAGE], "DOU", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '2';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L3_GARAGE], "TRI", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '3';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L3_GARAGE], "SIN", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '1';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L3_GARAGE], "GC", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT
   //   else if (!_memicmp(apTokens[L3_GARAGE], "GS", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT ?
   //   else if (!_memicmp(apTokens[L3_GARAGE], "DE", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = 'D';              // Detached
   //   else if (*(apTokens[L3_GARAGE]) == 'S')
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Space ?
   //}

   //// YearBlt
   //lTmp = atol(apTokens[L3_YEARBUILT]);
   //if (lTmp > 1800 && lTmp < lToyear)
   //{
   //   iTmp = sprintf(acTmp, "%d", lTmp);
   //   memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   //}

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   //// Total rooms
   //iTmp = atol(apTokens[L3_TOTALROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   //}

   //// Stories
   //iTmp = atol(apTokens[L3_STORIES]);
   //if (iTmp > 0 && iTmp < 100)
   //{
   //   sprintf(acTmp, "%d.0", iTmp);
   //   vmemcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   //}

   //// Units
   //iTmp = atol(apTokens[L3_UNITS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%d", iTmp);
   //   vmemcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   //}

   //// Beds
   //iTmp = atol(apTokens[L3_BEDROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BEDS, iTmp);
   //   memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   //}

   //// Baths
   //iTmp = atol(apTokens[L3_BATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_F, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   //}

   //// HBaths
   //iTmp = atol(apTokens[L3_HALFBATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_H, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   //}

   //// Heating
   //int iCmp;
   //if (*apTokens[L3_HEATING] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_HEATING], asHeating_LDR[iTmp].acSrc, asHeating_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_HEAT) = asHeating_LDR[iTmp].acCode[0];
   //}

   //// Cooling
   //if (*apTokens[L3_AC] == 'C')
   //   *(pOutbuf+OFF_AIR_COND) = 'C';
   //else if (*apTokens[L3_AC] > ' ')
   //   LogMsg("*** Unknown A/C: %s", apTokens[L3_AC]);

   //// Pool/Spa
   //if (*apTokens[L3_POOLSPA] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_POOLSPA], asPool_LDR[iTmp].acSrc, asPool_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_POOL) = asPool_LDR[iTmp].acCode[0];
   //}

   //// Fire place
   //if (*apTokens[L3_FIREPLACE] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   pTmp = _strupr(apTokens[L3_FIREPLACE]);
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_FIREPLACE], asFP_LDR[iTmp].acSrc, asFP_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_FIRE_PL) = asPool[iTmp].acCode[0];
   //}

   //// Quality Class
   //acTmp1[0] = 0;
   //if (*apTokens[L3_QUALITYCLASS] > '0' || strlen(apTokens[L3_QUALITYCLASS]) > 1)
   //{
   //   strcpy(acTmp, apTokens[L3_QUALITYCLASS]);
   //   remCharEx(acTmp, " ,'?");
   //   pTmp = _strupr(acTmp);

   //   //memcpy(myCharRec.QualityClass, pTmp, strlen(pTmp));
   //   if (acTmp[0] == 'A' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'A';
   //   else if (acTmp[0] == 'F' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'F';
   //   else if (!memcmp(pTmp, "POOR", 4) || !memcmp(pTmp, "LOW", 3))
   //      acTmp1[0] = 'P';
   //   else if (*pTmp == 'G')
   //      acTmp1[0] = 'G';
   //   else if (isalpha(*pTmp))
   //   {
   //      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
   //      if (isdigit(acTmp[1]))
   //         iRet = Quality2Code(&acTmp[1], acTmp1, NULL);
   //      else if (isdigit(acTmp[2]))
   //         iRet = Quality2Code(&acTmp[2], acTmp1, NULL);
   //      else if (isalpha(acTmp[1]))
   //      {
   //         switch (acTmp[1])
   //         {
   //            case 'L':
   //            case 'P':
   //               acTmp1[0] = 'P';
   //               break;
   //            case 'A':
   //               acTmp1[0] = 'A';
   //               break;
   //            case 'F':
   //               acTmp1[0] = 'F';
   //               break;
   //            case 'G':
   //               acTmp1[0] = 'G';
   //               break;
   //         }
   //      }
   //   } else if (isdigit(*pTmp))
   //   {
   //      iTmp = atol(pTmp);
   //      if (iTmp < 100)
   //         iRet = Quality2Code(pTmp, acTmp1, NULL);
   //   }

   //   if (acTmp1[0] > '0')
   //      *(pOutbuf+OFF_BLDG_QUAL) = acTmp1[0];
   //}

   return 0;
}

/********************************* But_MergeLien *****************************
 *
 * For 2022 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int But_MergeLien17(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L17_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%.12s (%d)", apTokens[L17_ASMT], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L17_ASMT], strlen(apTokens[L17_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L17_FEEPARCEL], strlen(apTokens[L17_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L17_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L17_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "04BUT", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L17_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L17_TRA], strlen(apTokens[L17_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L17_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L17_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L17_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L17_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L17_GROWINGVALUE]);
   long lPers  = atoi(apTokens[L17_PPVALUE]);
   long lPP_MH = atoi(apTokens[L17_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L17_HOX]);
   long lExe2 = atol(apTokens[L17_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L17_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L17_OTHEREXEMPTIONCODE], strlen(apTokens[L17_OTHEREXEMPTIONCODE]));

   // Legal
   updateLegal(pOutbuf, apTokens[L17_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L17_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L17_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L17_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L17_ACRES]);
   lTmp = atol(apTokens[L17_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001011005000", 9))
   //   iTmp = 0;
#endif

   // AgPreserved
   if (iTokens > L17_ISAGPRESERVE && *apTokens[L17_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   But_MergeOwner(pOutbuf, apTokens[L17_OWNER]);

   // Situs
   //But_MergeSitus(pOutbuf, apTokens[L17_SITUS1], apTokens[L17_SITUS2]);

   // Mailing
   But_MergeMAdr(pOutbuf, apTokens[L17_MAILADDRESS1], apTokens[L17_MAILADDRESS2], apTokens[L17_MAILADDRESS3], apTokens[L17_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L17_TAXABILITYFULL], true, true);

   // Recorded Doc - 2019: data is not good
   if (*apTokens[L17_CURRENTDOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[L17_CURRENTDOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L17_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 

   return 0;
}

/******************************** But_Load_LDR3 *****************************
 *
 * Load LDR 2016, 2023
 *
 ****************************************************************************/

int But_Load_LDR3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acLdrFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acLdrFile, "%s\\%s\\%s_lien.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   
   // Fix CSV file
   //iRet = RebuildCsv(acTmpFile, acLdrFile, cLdrSep, L3_ISAGPRESERVE);
   // 07/18/2022
   iRet = RebuildCsv(acTmpFile, acLdrFile, cLdrSep, L17_FLDS);
   if (iRet <= 0)
      return -1;

   LogMsg0("Load %s Lien file", myCounty.acCntyCode);

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acLdrFile, acRollFile, "S(#3,C,A) OMIT(#1,C,GE,\"A\") DEL(9)");  // 2016
   if (!iRet)
      return -1;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Zoning file
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acTmpFile, acRec, myCounty.acCntyCode, myCounty.acCntyCode);
   if (acTmpFile[0] > ' ')
   {
      LogMsg("Open Zoning file %s", acTmpFile);
      fdZone = fopen(acTmpFile, "r");
      if (fdZone == NULL)
      {
         LogMsg("***** Error opening Zoning file: %s\n", acTmpFile);
         return -2;
      }
   }

   // Open Value file
   //sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (!_access(acTmpFile, 0))
   //{
   //   LogMsg("Open Lien file %s", acTmpFile);
   //   fdLienExt = fopen(acTmpFile, "r");
   //   if (fdLienExt == NULL)
   //   {
   //      LogMsg("***** Error opening lien file: %s\n", acTmpFile);
   //      return -7;
   //   }
   //} else
   //   fdLienExt = NULL;

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      sprintf(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") DEL(%d) ", cDelim);
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   lLastRecDate=iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      // 2023-08-01
      iRet = But_MergeLien3(acBuf, acRec);
      // 2022-07-18
      //iRet = But_MergeLien17(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         //if (fdLienExt)
         //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Situs
         if (fdSitus)
            lRet = But_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = MB_MergeStdChar(acBuf, NULL, NULL, NULL);
            // 20170719 - lRet = But_MergeChar(acBuf);

         // Merge Zoning
         if (fdZone)
            lRet = But_MergeRollExt(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdZone)
      fclose(fdZone);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Zoning matched:   %u", lZoneMatch);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Zoning skiped:    %u", lZoneSkip);
   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/****************************** But_ConvStdChar *******************************
 *
 * Convert MB chars layout to STDCHAR format.
 *
 *****************************************************************************/

int But_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmpFile[256], acTmp[256], acCode[32], *pRec, *pTmp;
   int      iRet, iTmp, iCmp, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("Open input char file: %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output char file: %s", acTmpFile);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   iTmp = 0;
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);

      if (!pRec)
         break;

      if (!isdigit(acBuf[0]))
         continue;

      // Drop all NULL 
      replStrAll(acBuf, "NULL", "");

      if (cDelim == ',')
         iRet = ParseStringNQ(pRec, cDelim, BUT_CHAR_LANDSQFT+1, apTokens);
      else
         iRet = ParseStringIQ(pRec, cDelim, BUT_CHAR_LANDSQFT+1, apTokens);
      if (iRet < BUT_CHAR_LANDSQFT)
      {
         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));

      // Apn
      memcpy(myCharRec.Apn, apTokens[BUT_CHAR_ASMT], strlen(apTokens[BUT_CHAR_ASMT]));
      
      // Fee parcel
      memcpy(myCharRec.FeeParcel, apTokens[BUT_CHAR_FEEPARCEL], strlen(apTokens[BUT_CHAR_FEEPARCEL]));
      
      // BldgSeqNum
      iTmp = atoi(apTokens[BUT_CHAR_BLDGSEQNUM]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      }

      // Use cat
      iTmp = blankRem(apTokens[BUT_CHAR_CATTYPE]);
      memcpy(myCharRec.LandUseCat, apTokens[BUT_CHAR_CATTYPE], iTmp);

      // Quality Class
      iTmp = remChar(apTokens[BUT_CHAR_QUALITYCLASS], ' ');
      if (iTmp > 0)
      {
         _strupr(apTokens[BUT_CHAR_QUALITYCLASS]);
         memcpy(myCharRec.QualityClass, apTokens[BUT_CHAR_QUALITYCLASS], iTmp);
         // MD055A or CD040C
         if (iTmp == 6)
            strcpy(acTmp, apTokens[BUT_CHAR_QUALITYCLASS]+1);
         else
            strcpy(acTmp, apTokens[BUT_CHAR_QUALITYCLASS]);

         acCode[0] = 0;
         if (isalpha(acTmp[0]) && isdigit(acTmp[1]))
         {
            myCharRec.BldgClass = acTmp[0];
            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            else if (isdigit(acTmp[2]))
               iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
         } else if (acTmp[0] > '0' && acTmp[0] <= '9')
            iRet = Quality2Code(acTmp, acCode, NULL);

         if (acCode[0] > '0')
            myCharRec.BldgQual = acCode[0];
      }

      // YrBlt
      iTmp = atoi(apTokens[BUT_CHAR_YEARBUILT]);
      if (iTmp > 1600 && iTmp <= lLienYear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[BUT_CHAR_EFFECTIVEYEAR]);
      if (iTmp > 1600 && iTmp <= lLienYear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSqft
      int iBldgSize = atoi(apTokens[BUT_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 1)
      {
         iTmp = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iTmp);
      }

      ULONG lSqft = (ULONG)atol(apTokens[BUT_CHAR_LANDSQFT]);
      if (lSqft > 10)
      {
         iRet = sprintf(acTmp, "%u", lSqft);
         memcpy(myCharRec.LotSqft, acTmp, iRet);

         // Lot acres
         double dTmp;
         dTmp = (double)(lSqft*SQFT_MF_1000);
         iTmp = sprintf(acTmp, "%u", (long)(dTmp+0.1));
         memcpy(myCharRec.LotAcre, acTmp, iTmp);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[BUT_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[BUT_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport SF
      int iCarport = atoi(apTokens[BUT_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Fireplace
      if (*apTokens[BUT_CHAR_FIREPLACE] > ' ')
      {
         pTmp = findXlatCode(apTokens[BUT_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pTmp)
            myCharRec.Fireplace[0] = *pTmp;
      }

      // Heating
      if (*apTokens[BUT_CHAR_HEATING] > ' ')
      {
         pTmp = findXlatCode(apTokens[BUT_CHAR_HEATING], &asHeating[0]);
         if (pTmp)
            myCharRec.Heating[0] = *pTmp;
      }

#ifdef _DEBUG
      //if (!memcmp(acBuf, "003432014000", 9))
      //   iRet = 0;
#endif

      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[BUT_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[BUT_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[BUT_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[BUT_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Rooms
      iTmp = atoi(apTokens[BUT_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[BUT_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[BUT_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half Baths
      iTmp = atoi(apTokens[BUT_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // Pool/Spa
      if (*apTokens[BUT_CHAR_POOLSPA] > '0')
      {
         iTmp = 0;
         iCmp = -1;
         while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[BUT_CHAR_POOLSPA], asPool[iTmp].acSrc, asPool[iTmp].iLen)) )
            iTmp++;

         if (!iCmp)
            myCharRec.Pool[0] = asPool[iTmp].acCode[0];
      } 

      // Units
      iTmp = atoi(apTokens[BUT_CHAR_UNITSCNT]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      }

      // Stories
      iTmp = atoi(apTokens[BUT_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Water
      blankRem(apTokens[BUT_CHAR_HASWELL]);
      if (*(apTokens[BUT_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      try {
         fputs((char *)&myCharRec.Apn[0], fdOut);
      } catch (...)
      {
         LogMsg("??? Exception occurs in BUT_ConvStdChar(),  APN=%s", apTokens[BUT_CHAR_ASMT]);
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/*********************************** loadBut ********************************
 *
 * Options:
 *    -L   load lien
 *    -La  Create assessor data set using lien file
 *    -U   Load update
 *    -Ua  Update assessor data
 *    -G   Load GrGr (load grgr files from the county and extract
 *         Sale_exp.dat and But_GrGr.dat for DataEntry)
 *    -Xs  Extract sale and update sale history
 *    -Xsi Update sale history and import to SQL
 *    -Xv  Extract and create value import file
 *
 *    -U -Ua -Xsi -Xv (daily update)
 *
 ****************************************************************************/

int loadBut(int iSkip)
{
   int   iRet=0;
   char  acTmp[256], acTmp1[256], acTmpFile[_MAX_PATH], acSlsFile[_MAX_PATH];

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;
   iApnLen = myCounty.iApnLen;

   // Load tax files
   if (iLoadTax == TAX_LOADING)                    // -T
   {
      // Load tax base
      iRet = MB_Load_TaxBase(bTaxImport, false, 0, 2, MM_DD_YYYY_1);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet = MB_Load_TaxCodeMstr(bTaxImport, 5);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport, 2);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport, 5);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode, true, true);

         // Load Owner - use LDR roll
         //iRet |= MB_Load_TR601Owner(true, L_USERID);
         iRet |= doTaxPrep(myCounty.acCntyCode, TAX_OWNER);
      }
   }

   if (!iLoadFlag)
      return iRet;

   // Load Value file
   if (iLoadFlag & EXTR_VALUE)                     // -Xv
   {
      char sDbName[64], sBYVFile[_MAX_PATH];

#ifdef _DEBUG
      // Combine value record
      //sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Vmp");
      //iRet = MB_CombineValue(myCounty.acCntyCode, acValueFile, acTmpFile, 3);
#endif

      sprintf(sBYVFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Value");
      GetPrivateProfileString(myCounty.acCntyCode, "UseE48", "Y", acTmp, _MAX_PATH, acIniFile);
      if (acTmp[0] == 'Y')
         iRet = MB_ExtrMergeValues(myCounty.acCntyCode, acValueFile, acExeFile, sBYVFile, 3);
      else
         iRet = MB_ExtrValues(myCounty.acCntyCode, acValueFile, acExeFile, sBYVFile, 3);
      if (iRet > 0)
      {
         sprintf(sDbName, "LDR%d", lLienYear);
         GetIniString("Data", "SqlValueFile", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acTmp1, acTmp, sDbName, myCounty.acCntyCode);
         lRecCnt = createValueImport(sBYVFile, acTmp1, false);
         if (lRecCnt > 0)
         {
            // Save output file for import
            strcpy(acValueFile, acTmp1);
            iRet = 0;
         } else
            iLoadFlag ^= EXTR_IVAL;                // Turn off import
      }
   }

   // Extract Sale file from But_Sales.csv to But_Sale.sls
   // Create/Update cum sale file
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // 02/10/2021
      if (iSaleDateFmt > 0)
         iRet = But_CreateSCSale(iSaleDateFmt, 1, 1, false, &BUT_DocCode[0]);
      else
         iRet = But_CreateSCSale(MM_DD_YYYY_1, 1, 1, false, &BUT_DocCode[0]);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
      else
         iLoadFlag ^= EXTR_ISAL;
   }

   // Load Grgr
   if (iLoadFlag & LOAD_GRGR)                      // -G
   {
      if (!sqlConnect("BUT", &hButDb))
         return -1;

      LogMsg0("Load %s GrGr file", myCounty.acCntyCode);

      iRet = But_ImportGrGr(myCounty.acCntyCode);
      if (!iRet)
      {
         // Do APN and Owner match
         GetIniString("Database", "CntyProvider", "", acTmp, 256, acIniFile);
         sprintf(acTmp1, acTmp, "BUT");

         // Translate APN
         if (bApnXlat)
         {
            LogMsg("APN translation processing...");
            iRet = doApnXlat(acTmp1, "But_GrGr");
            if (iRet > 0)
               LogMsg("Number of GRGR records has APN translated: %d", iRet);
         }

         // Extract to But_Grgr.sls
         iRet = But_ExtrGrGr(myCounty.acCntyCode);
         if (iRet < 0)
         {
            if (bSendMail &&  !(iLoadFlag & (LOAD_LIEN|LOAD_UPDT)))
               return iRet;
         }

         // Match GrGr with Roll file - set Apn_Match and Owner_Match flags
         //LogMsg("Match GrGr again roll table");
         //iRet = But_MatchGrGr(acTmp1);
         sprintf(acSlsFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
         iRet = GrGr_MatchRoll(acSlsFile, iGrGrApnLen);
         //if (iRet > 0)
         //{
         //   LogMsg("Number of GRGR records matched with roll: %d", iRet);

            // Extract data for mainframe
            //iRet = But_ExtrGrGrMatched(myCounty.acCntyCode);

            // Extract data for data entry
            //iRet = But_ExtrGrGrAll(myCounty.acCntyCode);

            // Extract data for merging into R01
            //iRet = But_ExtrSaleMatched(myCounty.acCntyCode);
            // Extract to GrGr_Exp.dat
            sprintf(acTmpFile, acEGrGrTmpl, myCounty.acCntyCode, "dat");
            iRet = GrGr_ExtrSaleMatched(acSlsFile, acTmpFile, (IDX_TBL5 *)&BUT_DocTitle[0]);
            if (iRet <= 0 && (iLoadFlag & EXTR_IGRGR))
               iLoadFlag ^= EXTR_IGRGR;    // Ignore import GrGr
         //} else
         //   LogMsg("***** Error matching But_GrGr with But_Roll table: %d", iRet);
      } else
      {
         // Check GrGr delay status
         if (iRet == -1)
            bGrGrAvail = false;
      }
   }

   // Extract lien file - LDR2016
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acValueFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acValueFile, 0);      // 2016 
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode, NULL, LAST_CHAR_N);
   }

   // 10/24/2016 sn
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      LogMsg0("Converting char file %s", acCharFile);

      // Load Char file
      if (!_access(acCharFile, 0))
      {
         // Ignore if file size is too small
         iRet = chkFileStart(acCharFile, "Msg", 0);
         if (!iRet)
         {
            iRet = But_ConvStdChar(acCharFile);
            if (iRet <= 0)
               LogMsg("*** WARNING: Error converting Char file %s.  Use existing data.", acCharFile);
         } else
            LogMsg("***** WARNING: Bad Char file %s.  Use existing data.", acCharFile);
      } else
      {
         LogMsg("*** WARNING: CHAR file does not exist: %s: ", acCharFile);
         LogMsg("    -Xa option is ignore.  Please verify input file");
      }
   }

   iRet = 0;
   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Create Lien file
      iRet = But_Load_LDR3(iSkip);
   } else
   if (iLoadFlag & LOAD_UPDT)                      // -U
   {
      iRet = But_Load_Roll(iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);

   // Merge GRGR
   if (!iRet && (iLoadFlag & MERG_GRGR))           // -Mg
   {
      sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
      sprintf(acSlsFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      if (!_access(acTmp, 0) && !_access(acSlsFile, 0))
      {
         // If Grgr_exp.dat is old, re-extract it.
         sprintf(acTmpFile, acEGrGrTmpl, myCounty.acCntyCode, "dat");
         iRet = chkFileDate(acSlsFile, acTmpFile);
         if (iRet == 1 || iRet < 0)
            iRet = GrGr_ExtrSaleMatched(acSlsFile, acTmpFile, (IDX_TBL5 *)&BUT_DocTitle[0]);

         // Input: Grgr_Exp.dat
         // Update Sale price if available
         iRet = MergeGrGrExpFile(acTmpFile, false, false, true);
      }
   }

   // Format DocLinks - Doclinks are concatenate fields separated by comma
   if (!iRet && (iLoadFlag & (LOAD_UPDT|LOAD_LIEN|MERG_CSAL)) )
      iRet = updateDocLinks(But_MakeDocLink, myCounty.acCntyCode, iSkip);

   return iRet;
}