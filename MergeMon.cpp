/**************************************************************************
 *
 * Notes:
 *    1) This file needs attention on LDR.
 *    2) I recommend we only keep books 907 (Mining, Quarries, Min Proc Plant), 
 *       910 (Mobile homes) and 915 (Secured Mobile homes).  The 907's show a 
 *       land assessment value, otherwise I wouldn't keep these either.  If there 
 *       are others you think we should keep, let me know.  
 *       861 and 908 are pipeline easements and pipeline right of ways, but there 
 *       are no properties in 861 and only 2 in 908.  I know we kept the pipeline 
 *       category in BUT but I don't think it's worth keeping here.
 *       Drop 935 & 940 also.
 *    3) Mon_Sale.csv contains full sale history
 *    4) Tax file requires sorting on ASMT before processing.
 *    5) Many GRGR transaction recorded as "DEED" but not a sale. Should we put in R01?
 *
 * Options:
 *    -CMON -L  load lien
 *    -CMON -La create assessor data set using lien file
 *    -CMON -U  load update
 *    -CMON -Ua update assessor data
 *    -CMON -G  load GrGr (load grgr files from the county and extract 
 *              Sale_exp.dat and Mon_GrGr.dat for DataEntry)
 *    -CMON -U -Ua -G (daily update) 
 *    -Mo       Merge other values
 *
 * Revision
 * 09/01/2005 1.0.0    First version by Sony Nguyen
 * 02/16/2007 1.9.1    Add function to process GrGr data.
 * 04/10/2007 1.9.3    Set bFmtDoc=false to tell MB_MergeAssr() to keep DocNum as is.  No format needed.
 * 04/18/2007 1.9.4    Take out '-' in zipcode if present.
 * 05/14/2007 1.9.5.1  Bug fix in zipcode.
 * 07/17/2007 1.9.7    Modify processing sequence due to new layout from the
 *                     county.  LDR load is now similar to CAL.
 * 08/14/2007 1.9.14.4 Fix bug when -La running alone.
 * 01/25/2008 1.10.3.1 Add code to support standard usecode
 * 03/05/2008 1.10.5.1 Add code to output update records.  
 * 03/13/2008 1.10.5.2 Clear out old usecode before assigning new one.  
 * 03/29/2008 1.10.6   Filter out unsecured parcels.  Only keep 907, 910 & 915
 * 07/14/2008 8.1.1    MON is now sending TR601 file format for LDR.  New function
 *                     MB_ExtrTR601() is created in LoadMB.cpp to extract lien values.
 *                     Add function Mon_Load_LDR(), Mon_MergeLien(), Mon_MergeMAdr() for TR601 input format.
 * 07/18/2008 8.2.0    Clean up code
 * 02/26/2009 8.6      Add -Mo option
 * 03/30/2009 8.7.1    Use MB_MergeSale() to do sale update instead of MergeSale3() to fix 
 *                     problem that non-sale transaction overwrites sale transaction that  
 *                     occurs in the same day  since we only keep one transaction per day.
 * 05/21/2009 8.7.6    Remove reference to OldApn & NewApn files.  We no longer need them.
 * 08/03/2009 9.1.5    Use replNull() instead of replChar() to remove null character.
 *                     Fix TransferDoc problem that overides SALE1_DT.
 * 01/18/2010 9.3.1    Stop using NAME_TYPE1 as public parcel flag.
 * 07/08/2010 10.0.2   Modify Mon_MergeLien() to save EXE codes.
 * 11/03/2010 10.3.7   Modify Mon_MergeRoll() and Mon_MergeLien() to set FullExempt=Y
 *                     when TaxabilityFull=3.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 05/14/2011 10.8.1   Add -Xs option to create cum sale.
 * 06/08/2011 10.9.1   Exclude HomeSite from OtherVal. Replace Mon_MergeExe() with MB_MergeExe(),
 *                     and Mon_MergeTax() with MB_MergeTax(). Sort tax file before processing.
 *                     Rename MergeMonRoll() to Mon_Load_Roll().
 * 07/26/2011 11.2.3   Add S_HSNO. Merge situs using monthly file when loading LDR.
 * 09/16/2011 11.5.4   Change log msg and return correct number of records output in Mon_Load_Roll().
 * 07/09/2012 12.1.2   Modify Mon_MergeSitus() to add zip to R01, hardcode "EGR" to "East Garrison". 
 *                     Modify Mon_MergeChar() to sum up values for multi units parcel.
 * 07/10/2012 12.1.2.1 Fix bug in Mon_MergeSitus().
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 05/01/2013 12.8.1   Modify Mon_LoadGrGrXml() and Mon_LoadGrGr() to restart the GRGR process.
 * 05/16/2013 12.8.2   Add Mon_FormatDocNum() to format DOCNUM. Use ApplyCumSale() for sale update
 *                     and MergeGrGrExpFile() to update GRGR. Modify Mon_ExtrSaleMatch() to output
 *                     SCSAL_REC format and only extract DEED and TRUSTEE'S DEED.
 * 06/25/2013 12.9.3   Fix DOCNUM problem in Mon_FormatDocNum().
 * 06/27/2013 13.0.2   Add Mon_DocType[] table to assign DocType to known DocTitle (not just DEED 
 *                     and TRUSTEE DEED). If -Mg is used, resort history file before merge.
 * 06/28/2013 13.0.2.3 Reset TRANSFER and not using DOCNUM & DOCDATE from roll file since
 *                     it's not matching with sale file.
 * 07/12/2013 13.2.4   Modify Mon_CreateSCSale() to take all record.  If DocCode is unknown, 
 *                     set NoneSale_Flg='Y'.
 * 07/31/2013 13.8.9   Modify Mon_MergeOwner() to add Name2 to Name1 on government parcel.
 * 09/18/2013 13.12.4  Adding Units, Rooms, and YrEff. Add Mon_ConvStdChar() and Mon_MergeStdChar().
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 10/20/2013 13.17.3  Modify Mon_ConvStdChar() to parse QualityClass into BldgClass & BldgQual.
 * 10/24/2013 13.17.5  Modify Mon_ConvStdChar() to handle all translation and Mon_MergeStdChar()
 *                     uses the data as is without worry about verification & translation.
 * 10/25/2013 13.17.5.2 Update Heating & Pool table, Modify Mon_ConvStdChar() to add BldgSeqNo & UnitSeqNo 
 *                     due to county layout change.
 * 11/08/2013 13.18.1  Modify Mon_FormatDocNum() to keep DOCNUM as is for transaction 2004 and later.
 * 01/12/2014 13.19.0  Modify Mon_DocCode[] & Mon_DocType[]
 * 01/20/2014 13.20.1  Modify DocCode[] to change some non-sale event.
 * 02/23/2014 13.20.4  Modify Mon_LoadGrGr() to get last file date from file name.
 * 02/25/2014 13.20.5  Modify MON_DocCode[].  Fix QualityClass in Mon_ConvStdChar()
 * 04/02/2014 13.22.2  Modify Mon_ConvStdChar() to load new CHAR file CountyPhyChar.txt
 *                     Modify MergeRoll() to populate Transfer even DocNum invalid.
 * 04/16/2014 13.22.5  Update asView[] and fix Mon_ConvStdChar() to have assigned correctly.
 * 04/29/2014 13.23.2  Modify Mon_CreateSCSale() to fix DocNum problem for transaction between 19970101 and 19991231
 * 07/20/2014 14.0.5   Add Mon_Load_LDR1(), Mon_MergeLien1(), Mon_ExtrLien() to support new LDR layout.
 *                     Add Mon_MergeMisc() to merge Owner, Legal, Zoning, Taxability, and Transfer
 *                     from monthly roll update to LDR roll.
 * 08/26/2014 14.3.2   Rewrite Mon_ConvStdChar() to work with new char file CountyPhyChar.txt.
 *                     Sort & dedup output file only, not input file.
 * 08/27/2014 14.3.2.1 Fix Mon_ConvStdChar() and make sure all tables are sorted. If 
 *                     we have to use unsorted table, use findXlatCodeA() instead.
 * 09/11/2014 14.4.0   Modify Mon_Load_LDR() to support MON 2014 rerun. Replace Mon_MergeLien()
 *                     with Mon_MergeLienL1() and Mon_MergeLienL2().
 * 09/12/2014 14.4.0.1 Fix bug in Mon_MergeLienL1() and Mon_MergeLienL2() to take only
 *                     records of the same lien year, skip older records.
 * 09/18/2014 14.4.2   Fix Mon_MergeLienL1(), Mon_MergeLienL2(), Mon_MergeRoll() to include book 935 & 940.
 *                     Also process owner name in Mon_MergeLienL1() & Mon_MergeLienL2() and 
 *                     add zipcode to M_CTY_ST_D when loading update roll.
 * 12/02/2014 14.9.2   Update log msg
 * 04/26/2015 14.15.3  If char file is empty, log warning and continue.
 * 06/16/2015 14.17.1  Modify Mon_Load_Roll() to sort roll file before processing.
 * 07/31/2015 15.2.0   Modify Mon_MergeOwner() to keep Name1 the same as county provided.
 *                     Add DBA to Mon_MergeMAdr().
 * 01/04/2016 15.9.0   Add option to load TC file by calling Load_TC()
 * 01/13/2016 15.10.1  Remove old situs in Mon_MergeSitus() before update to avoid remnant from old addr.
 * 02/14/2016 15.12.0  Modify Mon_MergeMAdr() to fix overwrite bug.
 * 02/15/2016 15.12.1  Fix bad char in LEGAL in MergeRoll().  Use M_ADDR1 if M_ADDR not available
 * 07/08/2016 16.0.3   Remove -La & -Ua option.  No longer support CDASSR.
 * 07/18/2016 16.0.5   Comment out Load_LDR1().  Remove MergeSale from Load_LDR(), use -Ms instead
 *                     Fix Mon_MergeMAdr() to use ParseMAdr1()  instead of ParseMAdr1().
 *                     Modify Mon_MergeMisc() to copy only fields that LDR file doesn't have.
 * 09/15/2016 16.4.3   Modify Mon_MergeStdChar() to overwrite LotAcres & LotSqft.
 *            16.4.3.1 Calculate LotAcres when char file doesn't have it in Mon_MergeStdChar()
 * 01/23/2018 17.5.2   Modify Mon_ConvStdChar() to change the way processing QualityClass.
 * 03/27/2018 17.9.0   Add -Ut option to update tax.
 * 07/11/2018 18.1.2   Increase buffer size in Mon_MergeStdChar() and sort utput of Mon_LoadGrGr().
 * 12/06/2018 18.8.0   Add -Te option to extract book 799 from tax file.  Modify loadMmon() to append
 *                     tax extract to R01 file for PG&E.
 * 12/07/2018 18.8.1   Add Mon_LoadGrGrCsv() to support new GRGR format (EXTRACT*.TXT). Modify Mon_LoadGrGr()
 *                     to support both XML & TXT file format.
 * 04/23/2019 18.11.4  Increase buffer size for Grantors/Grantees in Mon_CreateGrGrRec().
 * 06/17/2019 18.11.7  Modify Mon_MergeStdChar() to remove update LotSqft since it's already done in MergeRoll().
 *                     Check for empty GRGR file in Mon_LoadGrGrCsv().
 * 04/27/2020 19.9.0   Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 *                     Clean up Mon_MergeOwner() and Mon_MergeRoll().
 * 07/08/2020 20.1.3   Modify Mon_ConvStdChar() to pickup some more QualityClass.  Modify Mon_CreateSCSale()
 *                     to check for partial transfer.
 * 07/17/2020 20.1.5   Modify Mon_MergeSitus() to work around bad data, fix situs direction.
 * 11/01/2020 20.4.2   Modify Mon_MergeRoll() & Mon_MergeMisc() to populate default PQZoning.
 * 05/05/2021 20.8.6   Modify Mon_ConvStdChar() to fix BldgClass.
 * 05/13/2021 20.8.9   Reset BldgClass in Mon_MergeStdChar() if not found in CHAR file.
 * 07/24/2021 21.1.3   Modify Mon_ConvStdChar() & Mon_MergeStdChar() to add QualityClass to R01.
 *                     Fix Zoning bug in Mon_MergeMisc().
 * 04/25/2022 21.9.0   Set backup folder for GrGr files since source files is now in the root, not in DailyIndex folder.
 * 07/22/2022 22.0.3   Modify Mon_MergeMAdr() to update DBA.  Add Mon_Load_LDR3() & Mon_MergeLien3() to support new LDR format.
 * 07/19/2023 23.0.5   Modify Mon_MergeLien3() to skip updating LOT_SQFT if it bigger than 999999999.
 * 07/27/2023 23.0.7   Fix LotSqft in Mon_MergeRoll() and Mon_MergeStdChar().
 * 02/05/2024 23.6.0   Modify Mon_MergeSitus() & Mon_MergeMAdr() to populate UnitNox.
 * 07/18/2024 24.0.2   Modify Mon_MergeOwner() to fix some known bad char in owner name.
 *                     Modify Mon_MergeLien3() to add ExeType.
 * 09/05/2024 24.1.1   Remove flag CLEAR_OLD_XFER in sale update.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "CharRec.h"
#include "doGrgr.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "LoadMB.h"
#include "MergeMon.h"
#include "Markup.h"
#include "PQ.h"

#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "Tax.h"

bool Mon_FormatDocNum(char *pResult, char *pDocNum, char *pDocDate, char *pApn);
void Mon_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf, char *pTaxability);

/******************************** Mon_MergeMisc ******************************
 *
 * Copy various fields from roll file since LDR file doesn't have them.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Mon_MergeMisc(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     *apItems[64], *pTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header
      pRec = fgets(acRec, 1024, fdRoll);
      // Get first rec
      pRec = fgets(acRec, 1024, fdRoll);
   }
         
   pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Zone rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 1024, fdRoll);
         lZoneSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringNQ(acRec, cDelim, 64, apItems);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input Zone record for APN=%s", apItems[0]);
         iRet = -1;
      }

      return iRet;
   }

   // Merge data
   if (*apItems[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apItems[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apItems[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // Update Transfer
   iTmp = atol(apItems[MB_ROLL_DOCDATE]);
   char acTmp[32];
   if (dateConversion(apItems[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1))
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, apItems[MB_ROLL_DOCNUM], strlen(apItems[MB_ROLL_DOCNUM]));
   }
                
   // Taxability - SetTaxcode, Prop8 flag, FullExe flag
   //iTmp = updateTaxCode(pOutbuf, apItems[MB_ROLL_TAXABILITY], true, true);

   // Owner
   //Mon_MergeOwner(pOutbuf, apItems[MB_ROLL_OWNER], apItems[MB_ROLL_CAREOF], apItems[MB_ROLL_TAXABILITY]);

   // Legal
   //updateLegal(pOutbuf, apItems[MB_ROLL_LEGAL]);

   lZoneMatch++;

   // Get next record
   pRec = fgets(acRec, 1024, fdRoll);

   return 0;
}

/******************************** Mon_MergeExe *******************************
 *
 * Merge exemption value
 *
 *****************************************************************************/

int Mon_MergeExe(char *pOutbuf, int iSkipHdr)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp, *apItems[16];
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iSkipHdr; iTmp++)
         pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
      if (acRec[0] == '"')
         iTmp = 1;
      else
         iTmp = 0;
      if (iTmp != iSkipQuote)
      {
         LogMsg("*** Overwrite INI SkipQuote value to %d (from %d)", iTmp, iSkipQuote);
         iSkipQuote = iTmp;
      }
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      iTmp = memcmp(pOutbuf, pTmp+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   iRet = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apItems);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "125132011000", 9))
   //   iTmp = 0;
#endif

   // EXE code
   if (*apItems[MB_EXE_CODE] > ' ')
      memcpy(pOutbuf+OFF_EXE_CD1, apItems[MB_EXE_CODE], strlen(apItems[MB_EXE_CODE]));

   // HO Exe
   myLTrim(apItems[MB_EXE_HOEXE]);
   if (*apItems[MB_EXE_HOEXE] == '1' || *apItems[MB_EXE_HOEXE] == 'T')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apItems[MB_EXE_EXEAMT]);
   if (lTmp > 0 && lTmp < 999999999)
   {
      long lGross = atoin(pOutbuf+OFF_GROSS, SIZ_GROSS);

      if (lTmp > lGross)
      {
         if (bDebug)
            LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Mon_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 * Notes: CareOf can be expanded portion of owner name.  Default parsing method
 * to 3.  If suspect it has different last name in CareOf, use parsing method 4.
 *
 *****************************************************************************/

void Mon_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf, char *pTaxability)
{
   int   iTmp, iRet, iParseMethod=3;
   char  acTmp1[128], acTmp2[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[128], acName2[128], acOwner[128];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);

   memset(acTmp, 0, 128);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002585009", 9))
   //   iTmp = 0;
#endif

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

#ifdef _DEBUG
      //if (*pTmp > 'z' || *pTmp < ' ')
      //   iRet = 0;
#endif
      if (*pTmp == -47 || *pTmp == -91)         // 002381005000
         *pTmp = 'N';
      else if (*pTmp == -57 || *pTmp == -128)   // 003683006000
         *pTmp = 'C';
      else if (*pTmp == -63)                    // 022517040000
         *pTmp = 'A';

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' and single quote too
      if (*pTmp == '.' || *pTmp == 39)
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;
   acName2[0] = 0;
   acSave1[0] = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   if (*pCareOf == ' ' && *(pCareOf+1) > ' ')
      pCareOf++;
   if (*pCareOf > ' ')
   {
      if (!memcmp(pTaxability, "003", 3) || !memcmp(pTaxability, "040", 3))
      {
         // Check for ATTN or C/O
         if (!memcmp(pCareOf, "ATTN", 4) || !memcmp(pCareOf, "C/O", 3) || *pCareOf == '%')
         {
            updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
         } else
         {  // append to Name1 or keep it as Name2
            strcpy(acTmp2, pCareOf);

            if (pTmp = strchr(acTmp2, '%'))
            {
               *(pTmp-2) = 0;
               memcpy(pOutbuf+OFF_CARE_OF, pTmp, strlen(pTmp));
               strcat(acTmp, " ");
               strcat(acTmp, acTmp2);
            } else
            {
               // Not CareOf, append to Name1
               strcat(acTmp, " ");
               strcat(acTmp, acTmp2);
            }
            iTmp = strlen(acTmp);
         }

         // Public parcel
         if (acTmp[iTmp-1] == '&') 
            acTmp[iTmp-1] = 0;
         vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME1);
         *(pOutbuf+OFF_PUBL_FLG) = 'Y';
         return;
      } else if (acTmp[iTmp-1] == '&')
      {
         // Put it in Name2
         acTmp[iTmp] = ' ';
         strcpy(acTmp2, pCareOf);
         if (pTmp = strchr(acTmp2, '('))
         {
            if (*(pTmp-1) == ' ') pTmp--;
            *pTmp = 0;
         }

         // If acTmp2 is a single word, append to acTmp
         if (!(pTmp = strchr(acTmp2, ' ')))
         {
            strcat(acTmp, acTmp2);
            acTmp2[0] = 0;
         } else
         {
            strcpy(acName2, acTmp2);
         }        
      } else
      {
         // CareOf
         updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
      }
   }

   // Save original owner name
   strcpy(acOwner, acTmp);
   strcpy(acName1, acTmp);

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (acTmp[iTmp] >= '0' && acTmp[iTmp] <= '9')
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp || !memcmp(acTmp, "BOARD OF", 8))
   {
      vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME1);
      return;
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
   }

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
         strcpy(acName2, pTmp);
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;
   if ((pTmp=strstr(acName2, " AS ")) || (pTmp=strstr(acName2, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Eliminate last '&' in name
   iTmp = strlen(acName1)-1;
   if (acName1[iTmp] == '&')
      acName1[iTmp] = 0;

   // Save Name1
   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);

   // Now parse owners to swap name
   iRet = 0;
   memset((void *)&myOwner, 0, sizeof(OWNER));
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, iParseMethod);
      pTmp = strchr(acOwner, ' ');

      // If name is not swapable, use Name1 instead
      if (iRet == -1 || *(pOutbuf+OFF_ETAL_FLG) == 'Y' || !memcmp(acOwner, myOwner.acSwapName, pTmp - &acOwner[0]))
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
      else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
   {
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
   }

   // Process Name2 when there is more than one word
   if (acName2[0] && strchr((char *)&acName2[1], ' ') )
      vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);
}

/******************************** Mon_MergeMAdr ******************************
 *
 * Merge Mail address from roll file
 *
 *****************************************************************************/

void Mon_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // CareOf - parse and populate in Mon_MergeOwner()

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   } 

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
      vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001463020", 9) )
   //   iTmp = 0;
#endif

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], SIZ_M_CITY);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         // Take out '-' in zipcode if present
         strcpy(acTmp, apTokens[MB_ROLL_M_ZIP]);
         iTmp = strlen(acTmp);
         if (acTmp[5] == '-' && iTmp == 10)
         {
            strcpy(&acTmp[5], &acTmp[6]);
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         } else if (iTmp > 9)
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);         
         else
            vmemcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);   
      } else
         iTmp = 0;

      if (iTmp == 9)
         sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP], apTokens[MB_ROLL_M_ZIP]+5);
      else
         sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

void Mon_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001463020", 9) )
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   p0 = NULL;

   if (!_memicmp(pLine1, "DBA ", 4))
   {
      vmemcpy(pOutbuf+OFF_DBA, pLine1+4, SIZ_DBA);
      p1 = pLine2;
      p2 = pLine3;
   } else if (!_memicmp(pLine2, "DBA ", 4) )
   {
      vmemcpy(pOutbuf+OFF_DBA, pLine2+4, SIZ_DBA);
      p0 = pLine1;
      p1 = pLine3;
      p2 = pLine4;
   } else if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
         {
            p1 = pLine2;
            if (*pLine1 >= 'A')
               p0 = pLine1;
         }
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      iTmp = blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, iTmp);

      // 07/18/2016
      parseMAdr1(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ')
         {
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
            vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
         }
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   strcpy(acAddr2, p2);
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);

      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}

/******************************** Mon_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Mon_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSitus);

      // Get first rec
      pRec = fgets(acRec, 512, fdSitus);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   else
      iRet = ParseStringIQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old Situs
   removeSitus(pOutbuf);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "020087005000", 9))
   //   lTmp = 0;
#endif

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);

      // Special case - remove garbage
      if (pTmp = strstr(apTokens[MB_SITUS_STRNUM], " 1-"))
      {
          LogMsg("*** Drop trailing str number(1): %s [%s]", apTokens[MB_SITUS_STRNUM], apTokens[MB_SITUS_ASMT]);
         *pTmp = 0;
      }
      strcpy(acAddr1, apTokens[MB_SITUS_STRNUM]);
      iTmp = blankRem(acAddr1);

      // Save original StrNum
      if ((pTmp = strchr(acAddr1, '-')) && (*(pTmp+1) > ' '))
      {
         vmemcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_HSENO);
      } else
      {
         memcpy(pOutbuf+OFF_S_HSENO, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
         if (pTmp = strchr(acAddr1, ' '))
         {
            LogMsg("??? StrNum = %s [%s]", acAddr1, apTokens[MB_SITUS_ASMT]);
            if (isdigit(*(++pTmp)))
            {
               if (!memcmp(pTmp, "1/2", 3))
                  vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp, SIZ_S_STR_SUB);
               else
                  LogMsg("*** Drop trailing str number(2): %s [%s]", pTmp, apTokens[MB_SITUS_ASMT]);
            } else if (GetStrDir(pTmp))
               memcpy(pOutbuf+OFF_S_DIR, pTmp, strlen(pTmp));
            else if (*pTmp == '#')
            {
               vmemcpy(pOutbuf+OFF_S_UNITNO, pTmp, SIZ_S_UNITNO);
               vmemcpy(pOutbuf+OFF_S_UNITNOX, pTmp, SIZ_S_UNITNOX);
            } else if (isalpha(*pTmp) && *(pTmp+1) == 0)
               *(pOutbuf+OFF_S_STR_SUB) = *pTmp;
            else
               vmemcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_HSENO);
         }
      }   

      strcat(acAddr1, " ");
      if (*apTokens[MB_SITUS_STRDIR] > ' ' && isDir(apTokens[MB_SITUS_STRDIR]))
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
      }
   }

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);

      strcpy(acTmp, apTokens[MB_SITUS_STRNAME]);
      blankPad(acTmp, SIZ_S_STREET);
      memcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET);

      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
         sprintf(acCode, "%d      ", iTmp);
      else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apTokens[MB_SITUS_STRNAME]);
      if (sAdr.strName[0] > ' ')
      {
         blankPad(sAdr.strName, SIZ_S_STREET);
         memcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      }
      if (sAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, strlen(sAdr.strSfx));

      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNOX);
   }

   blankPad(acAddr1, SIZ_S_ADDR_D);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1);   
      blankPadz(acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      // Zip
      lTmp = atol(apTokens[MB_SITUS_ZIP]);
      if (lTmp > 90000 && lTmp < 99999)
         memcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], 5);

      if (acAddr1[0] > ' ')
         sprintf(acTmp, "%s CA %.5s", myTrim(acAddr1), pOutbuf+OFF_S_ZIP);
      else if (!memcmp(apTokens[MB_SITUS_COMMUNITY], "EGR", 3))
         sprintf(acTmp, "EAST GARRISON CA %.5s", pOutbuf+OFF_S_ZIP);
      else
         acTmp[0] = 0;

      blankPad(acTmp, SIZ_S_CTY_ST_D);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int Mon_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "020087005000", 9))
   //   acTmp[0] = 0;
#endif
   // 
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, sSitusAdr.UnitNox, SIZ_S_UNITNOX);
   }

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/******************************** Mon_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Mon_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg("Skip Sale rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   while (!iTmp)
   {
      // Replace tab char with 0
      if (pRec)
         iRet = ParseStringNQ(pRec, ',', MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "001015008", 9))
      //   iRet = 0;
#endif

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         /* Do not use Confidential Sale Price
         } else
         {
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            }
         */
         }

         // DocType - need translation before production
         // 01(grant deed), 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         // 0F (Trustee deed)
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         MB_MergeSale(&sCurSale, pOutbuf, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Mon_MergeChar ******************************
 *
 * Note: need code table for Heating and Colling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - A parcel may have multiple entries in CHAR file.  If LandUseCat > 1,
 *     Add all of them together.  Otherwise, use the first one.
 *     For now, just use the first one found.
 *
 *****************************************************************************/

//int Mon_MergeChar(char *pOutbuf)
//{
//   static   char acRec[1024], *pRec=NULL;
//   char     acTmp[256], acCode[32], *pTmp;
//   long     lTmp, lBldgSqft, lGarSqft;
//   int      iRet, iLoop, iBeds, iFBath, iHBath, iFp;
//   MB_CHAR  *pChar;
//
//   iRet=iBeds=iFBath=iHBath=iFp=0;
//   lBldgSqft=lGarSqft=0;
//
//   // Get first Char rec for first call
//   if (!pRec && !lCharMatch)
//   {
//      pRec = fgets(acRec, 1024, fdChar);
//   }
//
//   do
//   {
//      if (!pRec)
//      {
//         fclose(fdChar);
//         fdChar = NULL;
//         return 1;      // EOF
//      }
//
//      // Compare Asmt
//      iLoop = memcmp(pOutbuf, pRec, iApnLen);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
//         pRec = fgets(acRec, 1024, fdChar);
//         lCharSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 1;
//
//   pChar = (MB_CHAR *)pRec;
//
//   // Quality Class
//   memcpy(acTmp, pChar->QualityClass, MBSIZ_CHAR_QUALITY);
//   acTmp[MBSIZ_CHAR_QUALITY] = 0;
//   pTmp = strchr(acTmp, ' ');
//   if (pTmp) *pTmp = 0;
//
//   acCode[0] = 0;
//   if (!_memicmp(acTmp, "AVG", 3))
//      strcpy(acCode, "A");
//   else if (isalpha(acTmp[0])) 
//   {
//      *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0];
//      if (isdigit(acTmp[1]))
//         iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
//      else if (pTmp && isdigit(*(pTmp+1)))
//         iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
//   } else if (acTmp[0] > '0' && acTmp[0] <= '9')
//      iRet = Quality2Code(acTmp, acCode, NULL);
//
//   blankPad(acCode, SIZ_BLDG_QUAL);
//   memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
//
//   // Yrblt
//   lTmp = atoin(pChar->YearBuilt, MBSIZ_CHAR_YRBLT);
//   if (lTmp > 1700 && lTmp <= lToyear)
//      memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);
//   else
//      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);
//
//   // BldgSqft
//   lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
//   if (lBldgSqft > 10)
//   {
//      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
//      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
//   } else
//      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);
//
//   // Garage Sqft
//   lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
//   if (lGarSqft > 10)
//   {
//      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
//      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
//      *(pOutbuf+OFF_PARK_TYPE) = '2';
//   } else
//   {
//      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
//      *(pOutbuf+OFF_PARK_TYPE) = ' ';
//   }
//
//   // Heating
//   /* Do not activate this code until verify the table is correct.  Same for cooling
//   if (pChar->Heating[0] > ' ')
//   {
//      iTmp = 0;
//      iCmp = -1;
//      while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(pChar->Heating, asHeating[iTmp].acSrc, asHeating[iTmp].iLen)) < 0)
//         iTmp++;
//
//      if (!iCmp)
//      {
//         *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
//      }
//   }
//   */
//   // Cooling - No cooling code for MON
//
//   // Beds
//   iBeds = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
//   if (iBeds > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
//      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
//   } else
//      memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);
//
//   // Bath
//   iFBath = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
//   if (iFBath > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
//      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
//   } else
//      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);
//
//   // Half bath
//   iHBath = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
//   if (iHBath > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
//      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
//   } else
//      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);
//
//   // Fireplace
//   iFp = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
//   if (iFp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
//      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
//   } else
//      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);
//   
//   // HasSeptic or HasSewer
//   if (pChar->HasSeptic > '0')
//      *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
//   else if (pChar->HasSewer > '0')
//      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;
//
//   // HasWell
//   *(pOutbuf+OFF_WATER) = pChar->HasWell;
//
//   lCharMatch++;
//
//   // Get next Char rec
//   pRec = fgets(acRec, 1024, fdChar);
//
//   // Check for multi-parcel unit
//   while (!memcmp(pOutbuf, acRec, iApnLen))
//   {
//      // BldgSqft
//      lTmp = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
//      if (lTmp > 10)
//      {
//         lBldgSqft += lTmp;
//         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
//         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
//      }
//
//      // Garage Sqft
//      lTmp = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
//      if (lTmp > 10)
//      {
//         lGarSqft += lTmp;
//         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
//         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
//         *(pOutbuf+OFF_PARK_TYPE) = '2';
//      }
//
//      // Beds
//      iRet = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
//      if (iRet > 0)
//      {
//         iBeds += iRet;
//         sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
//         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
//      }
//
//      // Bath
//      iRet = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
//      if (iRet > 0)
//      {
//         iFBath += iRet;
//         sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
//         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
//      }
//
//      // Half bath
//      iRet = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
//      if (iRet > 0)
//      {
//         iHBath += iRet;
//         sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
//         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
//      } 
//      
//      // Fireplace
//      iRet = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
//      if (iRet > 0)
//      {
//         iFp += iRet;
//         sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
//         memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
//      }
//      
//      // Get next Char rec
//      pRec = fgets(acRec, 1024, fdChar);
//   }
//
//   return 0;
//}

int Mon_MergeStdChar(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   ULONG    lBldgSqft, lGarSqft, lTmp;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath;
   STDCHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 2048, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 2048, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
   {
      *(pOutbuf+OFF_BLDG_CLASS) = ' ';
      *(pOutbuf+OFF_BLDG_QUAL)  = ' ';
      return 1;
   }

   pChar = (STDCHAR *)pRec;

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Yrblt
   if (pChar->YrBlt[0] > '0')
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // YrEff
   if (pChar->YrEff[0] > '0')
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_EFF, BLANK32, SIZ_YR_BLT);

   // LotSqft - value from LotSqft is more accurate than calculate from LotAcres
   ULONG lLotSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lLotSqft > 100)
   {
      if (lLotSqft < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }
      lTmp = atoln(pChar->LotAcre, SIZ_CHAR_SQFT);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      } else if (*(pOutbuf+OFF_LOT_ACRES+8) = ' ')
      {
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(double)(lLotSqft*SQFT_MF_1000+0.5));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   } 

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (pChar->Misc.sExtra.DetGarSqft[0] > ' ')
      lGarSqft += atoin(pChar->Misc.sExtra.DetGarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = '2';
   } else
   {
      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = ' ';
   }

   // ParkType
   if (pChar->ParkType[0] > ' ')
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "119161020000", 9))
   //   iTmp = 0;
#endif

   // Heating
   if (pChar->Heating[0] > ' ')
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];

   // Cooling 
   if (pChar->Cooling[0] > ' ')
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Pool - translation table has not been verified
   if (pChar->Pool[0] > ' ')
      *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Fireplace
   memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);
   if (pChar->Fireplace[0] > ' ')
      *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];
   
   // HasSeptic or HasSewer
   if (pChar->HasSewer > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWater/HasWell
   if (pChar->HasWater > '0')
      *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Units
   iTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (iTmp > 0 && iTmp < 1000)
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   } else
      memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

   // Rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Impr cond
   if (pChar->ImprCond[0] > ' ')
      *(pOutbuf+OFF_IMPR_COND) = pChar->ImprCond[0];
   else
      *(pOutbuf+OFF_IMPR_COND) = ' ';

   // Get next Char rec
   pRec = fgets(acRec, 2048, fdChar);
   lCharMatch++;

   return 0;
}

/******************************** Mon_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************

int Mon_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, ',');
      pTmp += 2;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 0;
   }

   iRet = ParseStringNQ(pRec, ',', MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}
*/

/******************************** Mon_MergeTax ******************************
 *
 * Note:
 *
 ****************************************************************************

int Mon_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double   dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001025011", 9))
   //   iTmp = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iRet;

   // Merge tax data
   while (!iTmp)
   {
      iTmp = ParseStringNQ(pRec, ',', MB_TAX_ROLLCAT+1, apTokens);
      if (iTmp < MB_TAX_ROLLCAT)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
         return -1;
      }

      // If same tax year, apply values
      //if (!memcmp(myCounty.acYearAssd, apTokens[MB_TAX_YEAR], 4))
      {
         dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
         dTax1 = atof(acTmp);
         dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
         dTax2 = atof(acTmp);
         dTmp = dTax1+dTax2;
         if (dTax1 == 0.0 || dTax2 == 0.0)
            dTmp *= 2;

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}
*/

/********************************* Mon_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired/unsecured record, not use
 *
 *****************************************************************************/

int Mon_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

//#ifdef _DEBUG
//   if (!memcmp(pOutbuf, "187071014000", 12))
//      iTmp = 0;
//#endif

   // Replace tab char with 0
   iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // If book >= 800, only keep 907, 910, & 915
   iTmp = atoin(apTokens[iApnFld], 3);
   if (iTmp >= 800 && iTmp != 907 && iTmp != 910 && iTmp != 915 && iTmp != 935 && iTmp != 940)
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "27MON", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%u         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%u         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%u         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%u         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%u         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%u         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "119161020000", 10))
   //   iRet = 0;
#endif

   // Legal
   _strupr(apTokens[MB_ROLL_LEGAL]);
   remUnPrtChar(apTokens[MB_ROLL_LEGAL]);
   quoteRem(apTokens[MB_ROLL_LEGAL]);
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   if (acTmp[0] > ' ')
   {
      // Make uppercase
      _strupr(acTmp);
      if (acTmp[2] > 'Z')
         acTmp[2] = 0;
      else
         acTmp[SIZ_USE_CO] = 0;

      iTmp = strlen(acTmp);
      vmemcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
      if (lTmp < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      } else
         memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (ULONG)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "187071014000", 12))
   //   iTmp = 0;
#endif

   // Recorded Doc
   // Not consistent with sale file - use only when matched
   if (*apTokens[MB_ROLL_DOCNUM] > '0' && *apTokens[MB_ROLL_DOCDATE] > ' ')
   {
      char *pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);

         if (!memcmp(acTmp, apTokens[MB_ROLL_DOCNUM], 4))
         {
            if (Mon_FormatDocNum(acTmp, apTokens[MB_ROLL_DOCNUM], acTmp, apTokens[iApnFld]))
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, SIZ_TRANSFER_DOC);
            else
               vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], SIZ_TRANSFER_DOC);
         } else 
         {
            // Keep the transfer here to make it "County Data" eventhough we know it's wrong
            vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], SIZ_TRANSFER_DOC);
         }
      }
   } 

   // Owner
   try {
      Mon_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER], apTokens[MB_ROLL_CAREOF], apTokens[MB_ROLL_TAXABILITY]);
   } catch(...) {
      LogMsg("***** Execption occured in Mon_MergeOwner()");
   }

   // Mailing
   try {
      if (*apTokens[MB_ROLL_M_ADDR] > ' ')
         Mon_MergeMAdr(pOutbuf);
      else if (*apTokens[MB_ROLL_M_ADDR1] > ' ')
         Mon_MergeMAdr(pOutbuf, apTokens[MB_ROLL_M_ADDR1], apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3], apTokens[MB_ROLL_M_ADDR4]);

   } catch(...) {
      LogMsg("***** Execption occured in Mon_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************* Mon_Load_Roll ******************************
 *
 * Loading roll file.
 *
 ******************************************************************************/

int Mon_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   lLastFileDate = getFileDate(acRollFile);

   // Sort roll file
   sprintf(acRec, "%s\\%s\\Roll.srt", acTmpPath, myCounty.acCntyCode);
   iRet = sortFile(acRollFile, acRec, "S(#1,C,A) OMIT(2,1,C,GT,\"9\")");
   if (!iRet)
      return -2;
   strcpy(acRollFile, acRec);

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Sort tax file
   sprintf(acRec, "%s\\%s\\Tax.srt", acTmpPath, myCounty.acCntyCode);
   iRet = sortFile(acTaxFile, acRec, "S(#1,C,A) B(213,R)");
   if (!iRet)
      return -9;
   strcpy(acTaxFile, acRec);

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }

   // Open lien file
   fdLienExt = NULL;
   //sprintf(acRec, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (!_access(acRec, 0))
   //{
   //   LogMsg("Open Lien file %s", acRec);
   //   fdLienExt = fopen(acRec, "r");
   //   if (fdLienExt == NULL)
   //   {
   //      LogMsg("***** Error opening lien file: %s\n", acRec);
   //      return -7;
   //   }
   //}

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record
   //pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
      iTmp = memcmp(acBuf, (char *)&acRollRec[1], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Mon_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Mon_MergeSitus(acBuf);

            // Merge Lien
            //lRet = 1;
            //if (fdLienExt)
            //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

            // Merge Exe if not found in LienExt
            //if (fdExe && lRet)
            //   lRet = MB_MergeExe(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Mon_MergeStdChar(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Mon_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Mon_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Mon_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Mon_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Mon_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Mon_MergeStdChar(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTax(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);

         iNewRec++;
      }
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u\n", lCnt);
   return 0;
}

/******************************** CreateMonRoll ****************************
 *
 *
 ***************************************************************************

int CreateMonRoll(int iFirstRec)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, iNewRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Use TMP file only if output needs resort
   //sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }
   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return 2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return 2;
   }
   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return 2;
   }
   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return 2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return 4;

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Drop header record
   //pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new R01 record
      iRet = Mon_MergeRoll(acBuf, acRec, MAX_RECSIZE, CREATE_R01|CREATE_LIEN);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Mon_MergeSitus(acBuf);

         // Merge Exe
         if (fdExe)
            lRet = Mon_MergeExe(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Mon_MergeChar(acBuf);

         // Merge Sales
         if (fdSale)
            lRet = Mon_MergeSale(acBuf);

         // Merge Taxes
         if (fdTax)
            lRet = Mon_MergeTax(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
         iNewRec++;
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (acRec[1] > '9')
         break;   // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdSale)
      fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Mon_MatchRoll ******************************
 *
 * Match GrGr file against R01 and populate APNMatch and Owner match field.
 *
 ****************************************************************************/

int Mon_MatchRoll(LPCSTR pGrGrFile)
{
   char     acBuf[2048], acRoll[2048], acTmpFile[_MAX_PATH], acTmp[256];
   char     *pTmp;
   FILE     *fdIn, *fdOut;
   GRGR_DEF *pGrGr = (GRGR_DEF *)&acBuf[0];
   int      iRet, iTmp, iNoApn, iApnMatch, iApnUnmatch, iOwnerMatch;
   HANDLE   hRoll;
   DWORD    nBytesRead;
   bool     bEof;

   // Open input file
   if (!(fdIn = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error opening %s", pGrGrFile);
      return -1;
   }

   // Create output temp file
   strcpy(acTmpFile, pGrGrFile);
   pTmp = strrchr(acTmpFile, '.');
   strcpy(pTmp, ".tmp");
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating %s", acTmpFile);
      fclose(fdIn);
      return -2;
   }

   // Open roll file
   sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   if (_access(acTmp, 0))
   {
      sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (_access(acTmp, 0))
      {
         LogMsg("***** Missing input file %s.  Please check!", acTmp);
         fclose(fdIn);
         fclose(fdOut);
         return -3;
      }
   }

   LogMsg("Open input file %s", acTmp);
   hRoll = CreateFile(acTmp, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (hRoll == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening %s in Mon_MatchRoll().", acTmp);
      fclose(fdIn);
      fclose(fdOut);
      return -3;
   }

   // Skip first record
   iRet = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);
   iRet = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);

   // Initialize counters
   iNoApn=iApnMatch=iApnUnmatch=iOwnerMatch = 0;
   bEof = false;

   // Loop through input file
   while (!bEof)
   {
      // Read input record
      if (!(pTmp = fgets(acBuf, 2048, fdIn)))
         break;

      pGrGr->APN_Matched = 'N';
      pGrGr->Owner_Matched = 'N';

      // If no APN, skip
      if (pGrGr->APN[0] == ' ')
      {
         iNoApn++;
         fputs(acBuf, fdOut);    // No APN, output anyway for data entry
         continue;
      }
#ifdef _DEBUG
      //if (!memcmp(pGrGr->APN, "010222018", 9))
      //   iRet = 0;
#endif
      // Match APN
      do
      {
         // Compare
         iRet = memcmp(pGrGr->APN, acRoll, 9);
         if (!iRet)
         {
            pGrGr->APN_Matched = 'Y';
            iApnMatch++;

            // Match owner - match last name or the first 10 bytes of grantors
            int   iIdx, iLen;
            char  acOwner[64];

            memcpy(acOwner, (char *)&acRoll[OFF_NAME1], SIZ_NAME1);
            acOwner[SIZ_NAME1] = 0;
            if (pTmp = strchr(acOwner, ' '))
            {
               iLen = pTmp - (char *)&acOwner[0];
               if (iLen < 3) iLen = 10;
            }

            for (iIdx = 0; iIdx < atoin(pGrGr->NameCnt, SIZ_GR_NAMECNT); iIdx++)
            {
               // Compare grantors only
               if (pGrGr->Grantors[iIdx].NameType[0] == 'O')
               {
                  if (!memcmp((char *)&pGrGr->Grantors[iIdx].Name[0], acOwner, iLen))
                  {
                     pGrGr->Owner_Matched = 'Y';
                     iOwnerMatch++;
                     break;
                  }
               } else
                  break;
            }

            // Populate with roll data - to be done when needed

            break;
         } else
         {
            // Read R01 record
            iTmp = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);
            // Check for EOF
            if (!iTmp)
            {
               LogMsg("***** Error reading roll file (%f)", GetLastError());
               bEof = true;
               break;
            }

            // EOF ?
            if (!nBytesRead)
            {
               bEof = true;
               break;
            }
         }
      } while (iRet > 0);
      
      // Output record
      fputs(acBuf, fdOut);
   }

   // Check for leftover
   while (!feof(fdIn))
   {
      // Read input record
      if (!(pTmp = fgets(acBuf, 2048, fdIn)))
         break;

      // If no APN, skip
      if (pGrGr->APN[0] == ' ')
         iNoApn++;

      pGrGr->APN_Matched = 'N';
      pGrGr->Owner_Matched = 'N';

      // Output record
      fputs(acBuf, fdOut);
   }

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);
   if (hRoll)
      CloseHandle(hRoll);

   // If everything OK, rename output file to original file
   if (remove(pGrGrFile))
      LogMsg("***** Error removing temp file: %s (%d)", pGrGrFile, errno);
   else if (rename(acTmpFile, pGrGrFile))
      LogMsg("***** Error renaming temp file: %s --> %s (%d)", acTmpFile, pGrGrFile, errno);

   LogMsg("                     APN matched: %d", iApnMatch);
   LogMsg("                   Owner matched: %d", iOwnerMatch);
   LogMsg("                         No APN : %d", iNoApn);

   return 0;
}

/****************************** Mon_ExtrGrGrMatch ***************************
 *
 * Extract data and append to existing Sale_Exp.CO file.  This output is to be merged
 * into MONAssr product.  Extract only records with ApnMatch=Y.  Resort data on 
 * APN ascending, Recording Date descending, and DocumentNumber descending.
 *
 * Return 0 if successful, otherwise error
 *
 ****************************************************************************/

int Mon_ExtrGrGrMatched()
{
   char     acTmp[256], acTmpFile[256], acGrGrFile[256], acBuf[1280];
   long     lCnt=0, lTmp;
   int      iRet;
   char     *pTmp, *pCnty = "MON";

   CString  sTmp, sApn, sType;
   FILE     *fdGrGr;
   MB_GRGR  SaleRec;
   GRGR_DEF *pGrGr = (GRGR_DEF *)&acBuf[0];

   sprintf(acTmpFile, acGrGrTmpl, pCnty, "TMP");
   sprintf(acGrGrFile, acGrGrTmpl, pCnty, "CO");
   LogMsg("Extract matched grgr to %s", acGrGrFile);

   // If old file exist, copy it to TMP file then work on it
   if (!_access(acGrGrFile, 0))
   {
      // Copy current file to TMP file
      if (!CopyFile(acGrGrFile, acTmpFile, false))
      {
         LogMsg("***** Fail copying old GrGr file (%s->%s) for processing", acGrGrFile, acTmpFile);
         return -2;
      }
   }

   // Open output file
   if (!(fdSale = fopen(acTmpFile, "a+")))
   {
      LogMsg("***** Error creating %s file", acTmpFile);
      return -1;
   }

   // Open input file
   sprintf(acGrGrFile, acDETmpl, "SRT");
   if (!(fdGrGr = fopen(acGrGrFile, "r")))
   {
      LogMsg("***** Error opening %s file", acGrGrFile);
      return -1;
   }


   // Loop through
   while (!feof(fdGrGr))
   {
      // Get input rec
      pTmp = fgets(acBuf, 1280, fdGrGr);
      if (!pTmp)
         break;         // EOF

      // Drop all record without APN matched
      if (pGrGr->APN_Matched != 'Y')
         continue;

      // Take DEED or TRUSTEE DEED only
      if (memcmp(pGrGr->DocTitle, "DEED  ", 6) &&
          memcmp(pGrGr->DocTitle, "TRUSTEE DEED", 12) )
         continue;

      memset((void *)&SaleRec, 32, sizeof(MB_GRGR));

      memcpy(SaleRec.Apn, pGrGr->APN, SIZ_3K_APN);
      memcpy(SaleRec.DocDate, pGrGr->DocDate, SIZ_3K_GDOCDATE);
      memcpy(SaleRec.DocNum, pGrGr->DocNum, SIZ_3K_GDOCNUM);
      memcpy(SaleRec.DocType, pGrGr->DocTitle, SIZ_3K_GDOCTYPE);

      // Reformat sale price
      if (pGrGr->SalePrice[0] > ' ')
      {
         lTmp = atoin(pGrGr->SalePrice, SIZ_3K_GSALEPRICE);
         iRet = sprintf(acTmp, "%d", lTmp);
         memcpy(SaleRec.SalePrice, acTmp, iRet);
      }
      
      // Get grantors grantees
      memcpy(SaleRec.Grantors[0], pGrGr->Grantors[0].Name, SIZ_3K_GRANTOR);
      memcpy(SaleRec.Grantors[1], pGrGr->Grantors[1].Name, SIZ_3K_GRANTOR);
      memcpy(SaleRec.Grantees[0], pGrGr->Grantees[0].Name, SIZ_3K_GRANTOR);
      memcpy(SaleRec.Grantees[1], pGrGr->Grantees[1].Name, SIZ_3K_GRANTOR);

      SaleRec.ApnMatch = 'Y';
      SaleRec.OwnerMatch = pGrGr->Owner_Matched;

      SaleRec.CRLF[0] = '\n';
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec, fdSale);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdGrGr);
   fclose(fdSale);

   // Rename old file if exist
   sprintf(acGrGrFile, acGrGrTmpl, pCnty, "CO");
   if (!_access(acGrGrFile, 0))
   {
      sprintf(acTmp, acGrGrTmpl, pCnty, "SAV");
      if (!_access(acTmp, 0))
         remove(acTmp);
      rename(acGrGrFile, acTmp);
   }

   // Sort output file - remove duplicate
   sprintf(acTmp, "S(1,9,C,A,10,8,C,D,18,12,C,D) DUPO(1,29)");

   LogMsg("Sort output file %s to %s", acTmpFile, acGrGrFile);
   long lRet = sortFile(acTmpFile, acGrGrFile, acTmp);

   LogMsg("Total number of output matched records: %ld (%ld)", lCnt, lRet);
   return 0;
}

/****************************** Mon_ExtrSaleMatch ***************************
 *
 * Extract data to Grgr_Exp.dat.  This output is to be merged
 * into R01 file.  
 *
 * Return 0 if successful, otherwise error
 *
 ****************************************************************************/

int Mon_ExtrSaleMatched(char *pGrGrFile, bool bOverwrite)
{
   char      acTmp[256], acTmpFile[256], acBuf[2048];
   long      lCnt=0;
   int       iTmp;
   char      *pTmp, *pCnty = myCounty.acCntyCode;

   CString   sTmp, sApn, sType;
   FILE      *fdGrGr;
   SCSAL_REC SaleRec;
   GRGR_DEF  *pGrGr = (GRGR_DEF *)&acBuf[0];

   sprintf(acTmpFile, acEGrGrTmpl, pCnty, "DAT");
   LogMsg("Extract grgr sales from %s to %s", pGrGrFile, acTmpFile);

   // Open output file
   if (!(fdSale = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating %s file", acTmpFile);
      return -1;
   }

   // Open input file
   if (!(fdGrGr = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error creating %s file", pGrGrFile);
      return -1;
   }

   // Loop through
   while (!feof(fdGrGr))
   {
      // Get input rec
      pTmp = fgets(acBuf, 2048, fdGrGr);
      if (!pTmp)
         break;         // EOF

      memset((void *)&SaleRec, 32, sizeof(SCSAL_REC));

      // Translate DocType
      iTmp = findDocType(pGrGr->DocTitle, (IDX_TBL5 *)&MON_DocType[0]);
      if (iTmp >= 0 && MON_DocType[iTmp].flag == 'N')
      {
         memcpy(SaleRec.DocType, MON_DocType[iTmp].pCode, MON_DocType[iTmp].iCodeLen);
         SaleRec.NoneSale_Flg = MON_DocType[iTmp].flag;
      } else
      {
         if (bDebug)
            LogMsg("*** Ignore DocTitle: %.50s", pGrGr->DocTitle);
         continue;      // Drop this record
      }

      memcpy(SaleRec.Apn, pGrGr->APN, SALE_SIZ_APN);
      memcpy(SaleRec.DocDate, pGrGr->DocDate, SALE_SIZ_DOCDATE);
      memcpy(SaleRec.DocNum, pGrGr->DocNum, SALE_SIZ_DOCNUM);

      // Keep original DocCode for reference
      memcpy(SaleRec.DocCode, pGrGr->DocTitle, 3);
      
      // Sale price
      memcpy(SaleRec.SalePrice, pGrGr->SalePrice, SALE_SIZ_SALEPRICE);
      memcpy(SaleRec.StampAmt, pGrGr->Tax, SALE_SIZ_STAMPAMT);

      // Get grantors grantees
      memcpy(SaleRec.Seller1, pGrGr->Grantors[0].Name, SALE_SIZ_SELLER);
      memcpy(SaleRec.Name1, pGrGr->Grantees[0].Name, SIZ_GR_NAME);
      memcpy(SaleRec.Name2, pGrGr->Grantees[1].Name, SIZ_GR_NAME);

      SaleRec.ARCode = 'R';
      SaleRec.CRLF[0] = '\n';
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec, fdSale);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdGrGr);
   fclose(fdSale);

   // Append to cumulative file
   sprintf(acTmp, acEGrGrTmpl, pCnty, "SLS");
   if (bOverwrite)
   {
      iTmp = CopyFile(acTmpFile, acTmp, false);
      if (!iTmp)
         LogMsg("*** Unable to write to %s.  Please review the error and rerun.", acTmp);
   } else
   {
      LogMsg("Append to %s", acTmp);
      if (appendTxtFile(acTmpFile, acTmp) < 0)
         LogMsg("*** Error appending %s to cum sale file %s", acTmpFile, acTmp);
   }
   LogMsg("Total number of GrGr records extracted: %ld", lCnt);
   return 0;
}

/*********************************** Check **********************************
 *
 * 
 *
 ****************************************************************************/

int Check(BOOL bCorrect, LPCSTR pMsg)
{
   if (!bCorrect )
   {
      LogMsg0("***** Error: %s", pMsg);
      return 0;
   }
   return 0;
}

/******************************* Mon_LoadGrGrXml ****************************
 *
 * Load XML file into fixed length
 *
 * Return number of output record.  <0 if error.
 *
 ****************************************************************************/

int Mon_LoadGrGrXml(LPCSTR pXmlFile, FILE *fdOut) 
{
   // Get pathname
   CString csText;
   CString csNotes;
   CFile file;

   printf("Load GrGr file: %s\n", pXmlFile);
   LogMsg0("Load GrGr file: %s", pXmlFile);

   if ( ! file.Open( pXmlFile, CFile::modeRead ) )
   {
      LogMsg("***** Error unable to open file: %s", pXmlFile);
      return -1;
   }
   int nFileLen = (int)file.GetLength();

   // Allocate buffer for binary file data
   unsigned char* pBuffer = new unsigned char[nFileLen + 2];
   nFileLen = file.Read( pBuffer, nFileLen );
   file.Close();
   pBuffer[nFileLen] = '\0';
   pBuffer[nFileLen+1] = '\0'; // in case 2-byte encoded

   // Windows Unicode file is detected if starts with FEFF
   if ( pBuffer[0] == 0xff && pBuffer[1] == 0xfe )
   {
      // Contains byte order mark, so assume wide char content
      // non _UNICODE builds should perform UCS-2 (wide char) to UTF-8 conversion here
      csText = (LPCWSTR)(&pBuffer[2]);
      csNotes += _T("File starts with hex FFFE, assumed to be wide char format. ");
   } else
   {
      // _UNICODE builds should perform UTF-8 to UCS-2 (wide char) conversion here
      csText = (LPCSTR)pBuffer;
   }
   delete [] pBuffer;

   // If it is too short, assume it got truncated due to non-text content
   if ( csText.GetLength() < nFileLen / 2 - 20 )
   {
      LogMsg("***** Error converting file to string (may contain binary data)");
      return -1;
   }

   // Parse
   CMarkup xml;
   BOOL bResult = xml.SetDoc( csText );
   int iRet, iNameCnt, iGrtor, iGrtee, nNoApn, nTotalQty;
   GRGR_DEF sData;
   char     acTmp[128], *pTmp;

   nNoApn = 0;
   nTotalQty=0;
   while ( xml.FindChildElem(_T("Transaction")) )
   {
      Check(xml.IntoElem(), "Look into Transaction");
      memset((void *)&sData, ' ', sizeof(GRGR_DEF));

      // DocTitle
      if (xml.FindChildElem( _T("DocumentInformation") ) )
      {
         Check(xml.IntoElem(), "Look into DocumentInformation");
         if (xml.FindChildElem( _T("DocumentTypes") ))
         {
            Check(xml.IntoElem(), "Look into DocumentTypes");
            bResult = xml.FindChildElem( _T("DocumentType") );
            if (bResult)
            {
               csNotes = xml.GetChildData();
               csNotes.MakeUpper();
               memcpy(sData.DocTitle, csNotes, csNotes.GetLength());
            }

            bResult = xml.FindChildElem( _T("DocumentTypeDescription") );
            if (bResult && csNotes < "000")
            {
               csNotes = xml.GetChildData();
               if (csNotes > "0")
               {
                  csNotes.MakeUpper();
                  LogMsg0("%.3s - %s", sData.DocTitle, csNotes);
                  if (csNotes.Left(4) == "DEED")
                     memcpy(sData.DocTitle, "006", 3);
                  else if (csNotes.Left(7) == "TRUSTEE")
                     memcpy(sData.DocTitle, "022", 3);
                  else
                     memcpy(sData.DocTitle, csNotes, csNotes.GetLength());
               }
            } 
            Check(xml.OutOfElem(), "Out of DocumentTypes");
         }

         // Page count
         bResult = xml.FindChildElem( _T("ImagePageCount") );
         if (bResult)
         {
            csNotes = xml.GetChildData();
            memcpy(sData.NumPages, csNotes, csNotes.GetLength());
         }

         // Image name
         //iRet = xml.FindChildElem( _T("ImageFileName") );
         //csNotes = xml.GetChildData();

         Check(xml.OutOfElem(), "Out of DocumentInformation");
      }

      // Index data 
      if (xml.FindChildElem( _T("IndexInformation") ) )
      {
         Check(xml.IntoElem(), "Look into IndexInformation");
         bResult = xml.FindChildElem( _T("AssessorParcelNumber") );
         if (bResult)
         {
            csNotes = xml.GetChildData();
            iRet = csNotes.GetLength();
            if (iRet > iApnLen)
               iRet = iApnLen;

            // Ignore bad APN
            if (iRet > 6)
            {
               memcpy(sData.APN, csNotes, iRet);
               if (iRet < iApnLen && sData.APN[0] >= '0')
                  memcpy(&sData.APN[iRet], "000000", iApnLen-iRet);
            }

            if (xml.FindChildElem( _T("IndexNames") ))
            {
               Check(xml.IntoElem(), "Look into IndexNames");
               iNameCnt = iGrtor = iGrtee = 0;
               while (xml.FindChildElem( _T("Name") ))
               {
                  Check(xml.IntoElem(), "Look into Name");
                  iRet = xml.FindChildElem( _T("GrantorGranteeType") );
                  csNotes = xml.GetChildData();
                  pTmp = csNotes.GetBuffer(0);
                  if (*pTmp == 'E')
                  {
                     if (iGrtee < 5)
                     {
                        sData.Grantees[iGrtee].NameType[0] = 'E';
                        pTmp = sData.Grantees[iGrtee++].Name;
                     } else
                        pTmp = NULL;
                  } else
                  {
                     if (iGrtor < 5)
                     {
                        sData.Grantors[iGrtor].NameType[0] = 'O';
                        pTmp = sData.Grantors[iGrtor++].Name;
                     } else
                        pTmp = NULL;
                  }

                  iNameCnt++;
                  if (pTmp)
                  {
                     iRet = xml.FindChildElem( _T("IndexNameLast") );
                     csNotes = xml.GetChildData();
                     memcpy(pTmp, csNotes, csNotes.GetLength());
                  } else
                     sData.MoreName = 'Y';

                  Check(xml.OutOfElem(), "Out of Name");
               }
               Check(xml.OutOfElem(), "Out of IndexNames");

               if (iNameCnt > 0)
               {
                  iRet = sprintf(acTmp, "%d", iNameCnt);
                  memcpy(sData.NameCnt, acTmp, iRet);
               }
            }
            if (xml.FindChildElem( _T("DocumentFees") ))
            {
               Check(xml.IntoElem(), "Look into DocumentFees");
               while (xml.FindChildElem( _T("Fee") ))
               {
                  Check(xml.IntoElem(), "Look into Fee");
                  iRet = xml.FindChildElem( _T("FeeType") );
                  csNotes = xml.GetChildData();
                  if (csNotes == "Taxes")
                  {
                     iRet = xml.FindChildElem( _T("FeeAmount") );
                     csNotes = xml.GetChildData();
                     if (csNotes.GetAt(0) > '0')
                     {
                        double   dTax;
                        long     lPrice;

                        // Format tax value
                        dTax = atof(csNotes);
                        dTax *= 100;
                        sprintf(acTmp, "%*d", SIZ_GR_TAX, (long)dTax);
                        memcpy(sData.Tax, acTmp, SIZ_GR_TAX);

                        // Format and calculate sale price
                        lPrice = (long)(dTax * SALE_FACTOR_100);
                        sprintf(acTmp, "%*d", SIZ_GR_SALE, lPrice);
                        memcpy(sData.SalePrice, acTmp, SIZ_GR_SALE);
                     }
                  }
                  Check(xml.OutOfElem(), "Out of Fee");
               }
               Check(xml.OutOfElem(), "Out of DocumentFees");
            }
            if (xml.FindChildElem( _T("RecordingInformation") ))
            {
               Check(xml.IntoElem(), "Look into RecordingInformation");

               iRet = xml.FindChildElem( _T("RecordingDocumentNumber") );
               csNotes = xml.GetChildData();
               memcpy(sData.DocNum, csNotes, csNotes.GetLength());

               iRet = xml.FindChildElem( _T("RecordingDate") );
               csNotes = xml.GetChildData();
               memcpy(sData.DocDate, csNotes, csNotes.GetLength());

               //iRet = xml.FindChildElem( _T("RecordingFeeTotal") );
               //csNotes = xml.GetChildData();

               //iRet = xml.FindChildElem( _T("RecordingReceiptNumber") );
               //csNotes = xml.GetChildData();
               Check(xml.OutOfElem(), "Out of RecordingInformation");
            }
         }
         Check(xml.OutOfElem(), "Out of IndexInformation");
      }

      if (sData.APN[0] >= '0')
      {
         sData.CRLF[0] = '\n';
         sData.CRLF[1] = 0;
         fputs(&sData.DocNum[0], fdOut);
      } else
         nNoApn++;
      nTotalQty++;
      Check(xml.OutOfElem(), "Out of Transaction");
   }

   LogMsg("Number of transactions: %d", nTotalQty);
   LogMsg("Transactions w/o APN:   %d", nNoApn);

   return nTotalQty;
}

/******************************* Mon_LoadGrGrCsv ****************************
 *
 * Load CSV file into fixed length
 *
 * Return number of output record.  <0 if error.
 *
 ****************************************************************************/

int Mon_CreateGrGrRec(char *pRec, FILE *fdOut)
{
   char     *pTmp, acTmp[2048], acDate[16], acOutbuf[2048], *apItems[64], acApns[2048];
   int      iTmp, iRet, lSalePrice, iApnCnt;

   GRGR_DEF *pGrGrRec = (GRGR_DEF *)&acOutbuf[0];

   iTmp = ParseStringNQ(pRec, '|', MON_GR_FLDCNT+2, apItems);
   if (iTmp >= MON_GR_FLDCNT)
   {
      if (*apItems[MON_GR_ASMT] < '0')
         return 0;

      memset(acOutbuf, ' ', sizeof(GRGR_DEF));

      // APN
      remChar(apItems[MON_GR_ASMT], '-');
      strcpy(acApns, apItems[MON_GR_ASMT]);

      // Doc Tax
      double dTax = atof(apItems[MON_GR_DOCTRANTAX])+0.0001;
      if (dTax > 0.1)
      {
         dTax *= 100;
         sprintf(acTmp, "%*d", SIZ_GR_TAX, (long)dTax);
         memcpy(pGrGrRec->Tax, acTmp, SIZ_GR_TAX);

         // Format and calculate sale price
         lSalePrice = (long)(dTax * SALE_FACTOR_100);
         sprintf(acTmp, "%*d", SIZ_GR_SALE, lSalePrice);
         memcpy(pGrGrRec->SalePrice, acTmp, SIZ_GR_SALE);
      }

      // DocType
      memcpy(pGrGrRec->DocTitle, apItems[MON_GR_DOCTYPE], strlen(apItems[MON_GR_DOCTYPE]));
      int iTmp = findDocType(apItems[MON_GR_DOCTYPE], &MON_DocType[0]);
      if (iTmp >= 0)
      {
         memcpy(pGrGrRec->DocType, MON_DocType[iTmp].pCode, MON_DocType[iTmp].iCodeLen);
         pGrGrRec->NoneSale = MON_DocType[iTmp].flag;
      } else if (pGrGrRec->DocTitle[0] > ' ')
      {
         LogMsg("*** MON GRGR - Unknown DocType: %s [%s]", apItems[MON_GR_DOCTYPE], apItems[MON_GR_ASMT]);
         pGrGrRec->NoneSale = 'Y';
         if (pGrGrRec->DocTitle[0] > '9')
            pGrGrRec->NoneXfer = 'Y';
      }

      // DocDate, DocNum
      if (*apItems[MON_GR_DOCNUM] > ' ')
      {
         pTmp = dateConversion(apItems[MON_GR_DOCDATE], acDate, MM_DD_YYYY_1);
         if (pTmp)
         {
            memcpy(pGrGrRec->DocDate, pTmp, 8);
            iTmp = atol(pTmp);
            if (iTmp > lLastRecDate)
               lLastRecDate = iTmp;
         } else
            LogMsg("*** Invalid RecDate: %s - %s", apItems[MON_GR_DOCDATE], apItems[MON_GR_ASMT]);

         memcpy(pGrGrRec->DocNum, apItems[MON_GR_DOCNUM], strlen(apItems[MON_GR_DOCNUM]));    
      }

      // Grantor
      char sGrantor[2048], sGrantee[2048];
      try
      {
         strcpy(sGrantor, apItems[MON_GR_TRANSFEROR]);
         strcpy(sGrantee, apItems[MON_GR_TRANSFEREE]);
      } catch (...)
      {
         LogMsg("***** Buffer too small, contact programmer to fix this before continuing. [APN=%s]", acApns);
         iErrorCnt++;
         return -1;
      }

      if (sGrantor[0] > ' ')
      {
         iTmp = ParseStringNQ(sGrantor, ',', 63, apItems);
         if (iTmp > MAX_NAMES)
            iTmp = MAX_NAMES;
         for (iRet = 0; iRet < iTmp; iRet++)
         {
            pGrGrRec->Grantors[iRet].NameType[0] = 'O';
            memcpy(pGrGrRec->Grantors[iRet].Name, apItems[iRet], strlen(apItems[iRet]));           
         }
      }

      // Grantee
      if (sGrantee[0] > ' ')
      {
         iTmp = ParseStringNQ(sGrantee, ',', 63, apItems);
         if (iTmp > MAX_NAMES)
         {
            iTmp = MAX_NAMES;
            pGrGrRec->MoreName = 'Y';
         }
         for (iRet = 0; iRet < iTmp; iRet++)
         {
            pGrGrRec->Grantees[iRet].NameType[0] = 'E';
            memcpy(pGrGrRec->Grantees[iRet].Name, apItems[iRet], strlen(apItems[iRet]));           
         }
      }
      pGrGrRec->CRLF[0] = '\n';
      pGrGrRec->CRLF[1] = '\0';

      // Parse APN list
      iApnCnt = ParseStringNQ(acApns, ',', 63, apItems);
      if (iApnCnt > 1)
         pGrGrRec->MultiApn = 'Y';

      for (iTmp = 0; iTmp < iApnCnt; iTmp++)
      {
         sprintf(acTmp, "%s000000", apItems[iTmp]);
         memcpy(pGrGrRec->APN, acTmp, iApnLen);
         fputs(acOutbuf, fdOut);
      }

      iRet = iApnCnt;
   } else
      iRet = -1;

   return iRet;
}

int Mon_LoadGrGrCsv(char *pInfile, FILE *fdOut) 
{
   char     *pTmp, acRec[4096];
   int      iRet, iCnt, lCnt;

   FILE     *fdIn;

   LogMsg("Process GrGr file %s", pInfile);

   // Check for empty file
   if (getFileSize(pInfile) < 300)
   {
      LogMsg("*** Input file too small: %s.  Ignore it.", pInfile);
      return -2;
   }

   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("*** Error opening file: %s", pInfile);
      return -1;
   }

   // Initialize pointers
   iCnt = lCnt = 0;
   while (!feof(fdIn))
   {
      pTmp = fgets(acRec, 4096, fdIn);
      if (!pTmp)
         break;
      if (acRec[1] > '9')
         continue;

      iRet = Mon_CreateGrGrRec(acRec, fdOut);
      if (iRet > 0)
         iCnt += iRet;

      lCnt++;
      if (!(lCnt % 1000))
         printf("\r%d", lCnt);
   }

   fclose(fdIn);

   LogMsg("Total processed records  : %u", lCnt);
   LogMsg("Total output records     : %u", iCnt);

   return iCnt;
}

/********************************* Mon_LoadGrGr *****************************
 *
 * If successful, return 0.  Otherwise error.
 *
 ****************************************************************************/

int Mon_LoadGrGr(LPCSTR pCnty) 
{
   char     *pTmp, acRec[512];
   char     acTmp[256];
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH], acInfile[_MAX_PATH];
   struct   _finddata_t  sFileInfo;

   FILE     *fdOut;

   int      iCnt, iRet, iLoadType;
   long     lCnt, lHandle, lTmp;

   // Get raw file name
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   dateString(acRec, 0);

   // Prepare backup folder
   sprintf(acGrGrBak, acGrGrBakTmpl, pCnty, acRec);

   if (strstr(acGrGrIn, ".XML"))
      iLoadType = 1;
   else
      iLoadType = 2;

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      LogMsg("*** No new GrGr avail for processing: %s", acGrGrIn);
      return -1;
   }

   // Create Output file - Mon_GrGr.dat
   sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "Dat");

   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating GrGr output file: %s\n", acGrGrOut);
      _findclose(lHandle);
      return -2;
   }

   // Create backup folder
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   lLastRecDate = iCnt = lCnt = 0;
   while (!iRet)
   {
      sprintf(acInfile, "%s\\%s", acGrGrIn, sFileInfo.name);
      //lTmp = getFileDate(acXmlFile);
      lTmp = atoin(&sFileInfo.name[6], 8);
      if (lTmp > lLastGrGrDate)
         lLastGrGrDate = lTmp;

      // Parse input file
      if (iLoadType ==1)
         iRet = Mon_LoadGrGrXml(acInfile, fdOut);
      else
         iRet = Mon_LoadGrGrCsv(acInfile, fdOut);

      if (iRet < 0)
         LogMsg("*** Skip %s", acInfile);
      else
         lCnt += iRet;

      // Move input file to backup folder
      sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
      if (iRet = rename(acInfile, acTmp))
         LogMsg("***** Error renaming %s to %s", acInfile, acTmp);

      iCnt++;
      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }

   // Close handle
   _findclose(lHandle);
   iRet = 0;

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("-------------------------");
   LogMsg("Total files processed   : %u", iCnt);
   LogMsg("Total records processed : %u\n", lCnt);

   // Sort output
   if (lCnt > 0)
   {
      char  sCumGrGr[_MAX_PATH];

      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, DocNum asc, RecDate asc
      sprintf(acTmp,"S(17,12,C,A,37,8,C,A,1,12,C,A) F(TXT) OMIT(17,1,C,LT,\"0\",OR,17,1,C,GT,\"9\") DUPO(B2048,1,44) ");

      sprintf(sCumGrGr, acGrGrTmpl, pCnty, pCnty, "Sls");
      sprintf(acGrGrIn, "%s+%s", acGrGrOut, sCumGrGr);
      sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "Srt");

      // Sort Mon_GrGr.dat+Mon_GrGr.sls to Mon_GrGr.srt
      lCnt = sortFile(acGrGrIn, acGrGrOut, acTmp);

      // Update cumulative sale file
      if (lCnt > 0)
      {
         sprintf(acRec, acGrGrTmpl, pCnty, pCnty, "Sav");
         iRet = 0;
         if (!_access(acRec, 0))
         {
            if (!DeleteFile(acRec))
            {
               LogMsg("***** Fail to delete file: %s", acRec);
               iRet = -1;
            }
         } 

         if (!iRet)
         {
            // Rename Mon_GrGr.sls to Mon_GrGr.sav
            rename(sCumGrGr, acRec);

            // Rename Mon_GrGr.srt to Mon_GrGr.Sls
            rename(acGrGrOut, sCumGrGr);

            // Extract to GrGr_Exp.dat
            iRet = Mon_ExtrSaleMatched(sCumGrGr, false);
         }
      } else
         iRet = -1;
   } else
      iRet = -1;

   LogMsg("Total output records after dedup: %u", lCnt);
   LogMsg("             Last recording date: %d.", lLastRecDate);

   return iRet;
}

/********************************* Mon_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mon_MergeLienL1(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

#ifdef _DEBUG
//   if (!memcmp(pRollRec, "011061007000", 12))
//      iTmp = 0;
#endif

   // Replace null char with blank
   iRet = replNull(pRollRec, ' ', 0);

   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L1_USERID)
   {
      LogMsg("***** Mon_MergeLienL1: bad input record for APN=%s", apTokens[L1_ASMT]);
      return -1;
   }

   // If book >= 800, only keep 907, 910, & 915
   iTmp = atoin(apTokens[L1_ASMT], 3);
   if (iTmp >= 800 && iTmp != 907 && iTmp != 910 && iTmp != 915 && iTmp != 935 && iTmp != 940)
      return 1;

   // Check tax year
   lTmp = atol(apTokens[L1_TAXYEAR]);
   if (lTmp != lLienYear)
      return 1;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L1_ASMT], strlen(apTokens[L1_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L1_FEEPARCEL], strlen(apTokens[L1_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L1_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L1_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "27MON", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values - Land
   long lLand = atoi(apTokens[L1_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L1_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow = atoi(apTokens[L1_CURRENTGROWINGIMPRVALUE]);
   long lFixt   = atoi(apTokens[L1_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L1_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[L1_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L1_TAXAMT1]);
   double dTax2 = atof(apTokens[L1_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L1_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L1_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L1_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L1_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L1_EXEMPTIONCODE1], strlen(apTokens[L1_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L1_EXEMPTIONCODE2], strlen(apTokens[L1_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L1_EXEMPTIONCODE3], strlen(apTokens[L1_EXEMPTIONCODE3]));

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L1_TRA], strlen(apTokens[L1_TRA]));

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L1_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "119161020", 9))
   //   iTmp = 0;
   //strcpy(acTmp, apTokens[L1_PARCELDESCRIPTION]);
#endif
   // Legal
   _strupr(apTokens[L1_PARCELDESCRIPTION]);
   iTmp = updateLegal(pOutbuf, apTokens[L1_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   strcpy(acTmp, apTokens[L1_USECODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres - do not use sizeacresfttype since it's not reliable
   dTmp = atof(apTokens[L1_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Mon_MergeOwner(pOutbuf, apTokens[L1_OWNER], "", "");

   // Situs
   //Mon_MergeSitus(pOutbuf, apTokens[L1_SITUS1], apTokens[L1_SITUS2]);

   // Mailing
   Mon_MergeMAdr(pOutbuf, apTokens[L1_MAILADDRESS1], apTokens[L1_MAILADDRESS2], apTokens[L1_MAILADDRESS3], apTokens[L1_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L1_TAXABILITY], true, true);

   return 0;
}

int Mon_MergeLienL2(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

#ifdef _DEBUG
//   if (!memcmp(pRollRec, "011061007000", 12))
//      iTmp = 0;
#endif

   // Replace null char with blank
   iRet = replNull(pRollRec, ' ', 0);

   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L2_DTS)
   {
      LogMsg("***** Mon_MergeLienL2: bad input record for APN=%s", apTokens[L2_ASMT]);
      return -1;
   }

   // If book >= 800, only keep 907, 910, & 915
   iTmp = atoin(apTokens[L2_ASMT], 3);
   if (iTmp >= 800 && iTmp != 907 && iTmp != 910 && iTmp != 915 && iTmp != 935 && iTmp != 940)
      return 1;

   // Check tax year
   lTmp = atol(apTokens[L2_TAXYEAR]);
   if (lTmp != lLienYear)
      return 1;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L2_ASMT], strlen(apTokens[L2_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L2_FEEPARCEL], strlen(apTokens[L2_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L2_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L2_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "27MON", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values - Land
   long lLand = atoi(apTokens[L2_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L2_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow = atoi(apTokens[L2_CURRENTGROWINGIMPRVALUE]);
   long lFixt   = atoi(apTokens[L2_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L2_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[L2_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L2_TAXAMT1]);
   double dTax2 = atof(apTokens[L2_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L2_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L2_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L2_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L2_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L2_EXEMPTIONCODE1], strlen(apTokens[L2_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L2_EXEMPTIONCODE2], strlen(apTokens[L2_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L2_EXEMPTIONCODE3], strlen(apTokens[L2_EXEMPTIONCODE3]));

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L2_TRA], strlen(apTokens[L2_TRA]));

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L2_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030330020", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L2_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   strcpy(acTmp, apTokens[L2_USECODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres - do not use sizeacresfttype since it's not reliable
   dTmp = atof(apTokens[L2_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Mon_MergeOwner(pOutbuf, apTokens[L2_OWNER], "", "");

   // Situs
   //Mon_MergeSitus(pOutbuf, apTokens[L2_SITUS1], apTokens[L2_SITUS2]);

   // Mailing
   Mon_MergeMAdr(pOutbuf, apTokens[L2_MAILADDRESS1], apTokens[L2_MAILADDRESS2], apTokens[L2_MAILADDRESS3], apTokens[L2_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L2_TAXABILITY], true, true);

   return 0;
}

/******************************** Mon_MergeLien3 *****************************
 *
 * For 2022 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mon_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L3_ASMT], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "27MON", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%u         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%u         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%u         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // HOE
   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      vmemcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], SIZ_EXE_CD1);

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&MON_Exemption);

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], 0, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
      if (lTmp < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }

      // Format Acres
      lTmp = (ULONG)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (ULONG)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (iTokens > L3_ISAGPRESERVE && *apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Mon_MergeOwner(pOutbuf, apTokens[L3_OWNER], "", "");
   //if (*apTokens[L3_MAILADDRESS1] == '%')
   //   Mon_MergeOwner(pOutbuf, apTokens[L3_OWNER], apTokens[L3_MAILADDRESS1]);
   //else
   //   Mon_MergeOwner(pOutbuf, apTokens[L3_OWNER], NULL);

   // Mailing
   Mon_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*apTokens[L3_CURRENTDOCNUM] > '0' && !strchr(apTokens[L3_CURRENTDOCNUM], 'I'))
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   }

   return 0;
}

/******************************** Mod_Load_LDR3 *****************************
 *
 * Load LDR 2022 - AGENCYCDCURRSEC_TR601.TAB
 *
 ****************************************************************************/

int Mon_Load_LDR3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acRollFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  
      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Value file
   //sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (!_access(acTmpFile, 0))
   //{
   //   LogMsg("Open Lien file %s", acTmpFile);
   //   fdLienExt = fopen(acTmpFile, "r");
   //   if (fdLienExt == NULL)
   //   {
   //      LogMsg("***** Error opening lien file: %s\n", acTmpFile);
   //      return -7;
   //   }
   //} else
      //fdLienExt = NULL;

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(1,13,C,A) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9' || *pTmp < '0')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!feof(fdRoll))
   {
      // Create new R01 record
      iRet = Mon_MergeLien3(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         //if (fdLienExt)
         //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Situs
         if (fdSitus)
            lRet = Mon_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Mon_MergeStdChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/***************************** Mon_Load_LDR() *******************************
 *
 * Load LDR into 1900-byte record.
 *
 ****************************************************************************/

int Mon_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open LDR file %s", acRollFile);
   fdLDR = fopen(acRollFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acRollFile);
      return -2;
   }  

   // Use update roll to populate Taxability, Prop8, and Zoning
   GetIniString(myCounty.acCntyCode, "RollFile", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg("Open update roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      // Create new R01 record
      if (iLdrGrp == 1)
         iRet = Mon_MergeLienL1(acBuf, acRec);
      else
         iRet = Mon_MergeLienL2(acBuf, acRec);

      if (!iRet)
      {
#ifdef _DEBUG
         //if (!memcmp(acBuf, "117333018000", 9))
         //   iRet = 0;
#endif

         // Merge Char
         if (fdChar)
            lRet = Mon_MergeStdChar(acBuf);

         // Merge Situs
         if (fdSitus)
            lRet = Mon_MergeSitus(acBuf);

         // Merge misc fields from roll update
         if (fdRoll)
            lRet = Mon_MergeMisc(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else if (bDebug)
         LogMsg("*** Skip %s", acRec);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp || acRec[1] > '9')
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Zone matched:     %u\n", lZoneMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Zone skiped:      %u\n", lZoneSkip);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************** Mon_MergeLien1 *****************************
 *
 * Use this function to load 2014 LDR roll.
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

//int Mon_MergeLien1(char *pOutbuf, char *pRollRec)
//{
//   char     acTmp[256], acTmp1[256];
//   long     lTmp;
//   double   dTmp;
//   int      iRet=0, iTmp;
//
//#ifdef _DEBUG
////   if (!memcmp(pRollRec, "011061007000", 12))
////      iTmp = 0;
//#endif
//
//   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
//   if (iRet < MON_L_TRANSTAX)
//   {
//      LogMsg("***** Error: bad input record for APN=%s", pRollRec);
//      return -1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//
//   // Start copying data
//   memcpy(pOutbuf, apTokens[MON_L_ASMT], strlen(apTokens[MON_L_ASMT]));
//
//   // Copy ALT_APN
//   memcpy(pOutbuf+OFF_ALT_APN, apTokens[MON_L_FEEPARCEL], strlen(apTokens[MON_L_FEEPARCEL]));
//
//   // Format APN
//   iRet = formatApn(apTokens[MON_L_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//   // Create MapLink and output new record
//   iRet = formatMapLink(apTokens[MON_L_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // Create index map link
//   if (getIndexPage(acTmp, acTmp1, &myCounty))
//      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//   // County code
//   memcpy(pOutbuf+OFF_CO_NUM, "27MON", 5);
//
//   // Status
//   *(pOutbuf+OFF_STATUS) = 'A';
//
//   // TRA
//   memcpy(pOutbuf+OFF_TRA, apTokens[MON_L_TRA], strlen(apTokens[MON_L_TRA]));
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // Lien values - Land
//   long lLand = atoi(apTokens[MON_L_LAND]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = atoi(apTokens[MON_L_IMP]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: Growing, Fixture, PP, PPMH
//   long lPP   = atoi(apTokens[MON_L_PERSPROP]);
//   if (lPP > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_OTHER, lPP);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//      iTmp = sprintf(acTmp, "%d", lPP);
//      memcpy(pOutbuf+OFF_PERSPROP, acTmp, iTmp);
//   }
//
//   // Gross total
//   lTmp = lPP + lLand + lImpr;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "030330020", 9))
//   //   iTmp = 0;
//#endif
//
//   // UseCode
//   strcpy(acTmp, apTokens[MON_L_LANDUSE1]);
//   acTmp[SIZ_USE_CO] = 0;
//
//   // Standard UseCode
//   if (acTmp[0] > ' ')
//   {
//      _strupr(acTmp);
//      iTmp = strlen(acTmp);
//      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
//      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
//   }
//
//   // Acres - do not use sizeacresfttype since it's not reliable
//   dTmp = atof(apTokens[MON_L_ACRES]);
//   if (dTmp > 0.0)
//   {
//      // Lot Sqft
//      lTmp = (long)(dTmp * SQFT_PER_ACRE);
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      // Format Acres
//      lTmp = (long)(dTmp * ACRES_FACTOR);
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   // Owner
//   //Mon_MergeOwner(pOutbuf, apTokens[MON_L_ASSESSEE], "");
//
//   // Mailing
//   Mon_MergeMAdr(pOutbuf, apTokens[MON_L_ADDRESS1], apTokens[MON_L_ADDRESS2], apTokens[MON_L_ADDRESS3], apTokens[MON_L_ADDRESS4]);
//
//   return 0;
//}
//
//int Mon_Load_LDR1(int iFirstRec /* 1=create header rec */)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
//   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
//
//   HANDLE   fhOut;
//
//   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
//   DWORD    nBytesWritten;
//   BOOL     bRet, bEof;
//   long     lRet=0, lCnt=0;
//
//   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//
//   // Open LDR roll file
//   LogMsg("Open LDR Roll file %s", acRollFile);
//   fdLDR = fopen(acRollFile, "r");
//   if (fdLDR == NULL)
//   {
//      LogMsg("***** Error opening LDR roll file: %s\n", acRollFile);
//      return -1;
//   }  
//
//   // Use update roll to populate Taxability, Prop8, and Zoning
//   GetIniString(myCounty.acCntyCode, "RollFile", "", acTmpFile, _MAX_PATH, acIniFile);
//   LogMsg("Open update roll file %s", acTmpFile);
//   fdRoll = fopen(acTmpFile, "r");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
//      return -2;
//   }  
//
//   // Open Char file
//   LogMsg("Open Char file %s", acCChrFile);
//   fdChar = fopen(acCChrFile, "r");
//   if (fdChar == NULL)
//   {
//      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
//      return -2;
//   }
//
//   // Open Exe file
//   LogMsg("Open Exe file %s", acExeFile);
//   fdExe = fopen(acExeFile, "r");
//   if (fdExe == NULL)
//   {
//      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
//      return -2;
//   }
//
//   // Open Situs file
//   LogMsg("Open Situs file %s", acSitusFile);
//   fdSitus = fopen(acSitusFile, "r");
//   if (fdSitus == NULL)
//   {
//      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
//      return -2;
//   }
//
//   // Open Output file
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error opening output file: %s\n", acOutFile);
//      return -4;
//   }
//
//   // Output first header record
//   if (iFirstRec > 0)
//   {
//      memset(acBuf, '9', iRecLen);
//      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//   }
//
//   // Get 1st rec
//   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
//   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
//   bEof = (pTmp ? false:true);
//
//   // Init variables
//   iNoMatch=iBadCity=iBadSuffix=0;
//
//   // Merge loop 
//   while (!feof(fdLDR))
//   {
//      lLDRRecCount++;
//
//      // Create new R01 record
//      iRet = Mon_MergeLien1(acBuf, acRec);
//      if (!iRet)
//      {
//         // Merge Exe
//         if (fdExe)
//            lRet = Mon_MergeExe(acBuf, iHdrRows);
//
//         // Merge Char
//         if (fdChar)
//            lRet = Mon_MergeStdChar(acBuf);
//
//         // Merge Situs
//         if (fdSitus)
//            lRet = Mon_MergeSitus(acBuf);
//
//         // Merge misc fields from roll update
//         if (fdRoll)
//            lRet = Mon_MergeMisc(acBuf);
//
//         // Save last recording date
//         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
//         if (lRet > lLastRecDate && lRet < lToday)
//            lLastRecDate = lRet;
//
//         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//         if (!(++lCnt % 1000))
//            printf("\r%u", lCnt);
//
//         if (!bRet)
//         {
//            LogMsg("***** Error writing to output file at record %d\n", lCnt);
//            lRet = WRITE_ERR;
//            break;
//         }
//      }
//
//      // Get next roll record
//      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
//      if (!pTmp || acRec[1] > '9')
//         break;
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fdExe)
//      fclose(fdExe);
//   if (fdChar)
//      fclose(fdChar);
//   if (fdLDR)
//      fclose(fdLDR);
//   if (fdSitus)
//      fclose(fdSitus);
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   LogMsg("Total input records:        %u", lLDRRecCount);
//   LogMsg("Total output records:       %u", lCnt);
//   LogMsg("Total bad-city records:     %u", iBadCity);
//   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
//   LogMsg("Number of Situs matched:    %u", lSitusMatch);
//   LogMsg("Number of Char matched:     %u", lCharMatch);
//   LogMsg("Number of Zone matched:     %u", lZoneMatch);
//   LogMsg("Number of Exe matched:      %u\n", lExeMatch);
//
//   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
//   LogMsg("Number of Char skiped:      %u", lCharSkip);
//   LogMsg("Number of Zone skiped:      %u", lZoneSkip);
//   LogMsg("Number of Exe skiped:       %u\n", lExeSkip);
//   printf("\nTotal output records: %u\n", lCnt);
//
//   lRecCnt = lCnt;
//   return 0;
//}

/***************************** Mon_FormatDocNum ******************************
 *
 *  Before 07/01/1997:       199733840743  --> 199733840743
 *  09/01/1997 - 12/31/1999: 20009940022   --> 1999040022
 *  01/01/2000 - 12/31/2003: 200403129137  --> 20030129137
 *  01/01/2004 - present:    2004052194    --> 20040052194
 *
 * Return true if DocNum formatted ok
 *
 *****************************************************************************/

bool Mon_FormatDocNum(char *pResult, char *pDocNum, char *pDocDate, char *pApn)
{
   char  acTmp[32], acDocNum[32];
   int   iLen, lTmp;
   bool  bRet = true;

#ifdef _DEBUG
   //if (!memcmp(pDocNum, "20021081138", 11))
   //   lTmp = 0;
   //if (!memcmp(pApn, "012622060000", 10))
   //   lTmp = 0;
   
#endif

   // Ignore internal doc
   if ((*(pDocNum+4) == 'I') || isalpha(*(pDocNum+5)) )
      return false;

   memset(acDocNum, ' ', SALE_SIZ_DOCNUM);
   myTrim(pDocNum);
   iLen = strlen(pDocNum);
   lTmp = atoin(pDocDate, 6);
   if (lTmp > 200312)
   {
      // 2006070517     --> 2006070517
      lTmp = atol(pDocNum+4);
      sprintf(acDocNum, "%.4s%.6d   ", pDocNum, lTmp);
   } else if (lTmp > 199912)
   {
      // 2002R1026159   --> 2001026159
      // 200100077852   --> 2000077852
      if (iLen > 10)
      {
         lTmp = atol(pDocNum+6);
         if (!memcmp(pDocNum+4, pDocDate+2, 2) || (*(pDocNum+4) == 'R' && *(pDocNum+5) == *(pDocDate+3)) )
            sprintf(acDocNum, "%.4s%.6d   ", pDocDate, lTmp);
         else if (memcmp(pDocNum, pDocDate, 4))
            memcpy(acDocNum, pDocNum, iLen);
         else
            sprintf(acDocNum, "%.4s%.6d   ", pDocDate, lTmp);
      } else 
      {
         lTmp = atol(pDocNum+4);
         if (lTmp > 0)
            sprintf(acDocNum, "%.4s%.6d   ", pDocNum, lTmp);
         else
            bRet = false;
      }
      if (memcmp(pDocNum, pDocDate, 3))
         LogMsg("*** DocNum=%s, DocDate=%s", pDocNum, pDocDate);
   } else if (lTmp > 199708)
   {
      // ? 2004023247 - 19981205
      // 20009940022  - 19990525
      // 199835651501 - 19970829
      if (iLen == 12)
      {
         lTmp = atol(pDocNum+7);    // 199909854296
      } else if (iLen == 11)
         lTmp = atol(pDocNum+6);
      else if (iLen == 7)
         lTmp = atol(pDocNum+2);    // 9856650-19980825
      else
         lTmp = atol(pDocNum+4);    // 999808809-19980218

      if (*pDocNum > '2' || 
         !memcmp(pDocNum+4, pDocDate+2, 2) ||
         (iLen == 12 && !memcmp(pDocNum+5, pDocDate+2, 2)) )
         sprintf(acDocNum, "%.4s%.6d   ", pDocDate, lTmp);     // 9998123456
      else
         sprintf(acDocNum, "%.4s%.6d   ", pDocNum, lTmp);
   } else if (lTmp > 199612)
   {
      // 199835651501 - 19970829
      if (iLen == 12)
      {
         lTmp = 1;
         memcpy(acDocNum, pDocNum, iLen);
      } else if (iLen == 11)
         lTmp = atol(pDocNum+6);
      else if (iLen == 7)
         lTmp = atol(pDocNum+2);    // 9856650-19980825
      else
         lTmp = atol(pDocNum+4);    // 999808809-19980218

      if (*pDocNum > '2' || 
         !memcmp(pDocNum+4, pDocDate+2, 2) ||
         (iLen == 12 && !memcmp(pDocNum+5, pDocDate+2, 2)) )
         sprintf(acDocNum, "%.4s%.6d   ", pDocDate, lTmp);     // 9998123456
      else if (lTmp != 1)
         sprintf(acDocNum, "%.4s%.6d   ", pDocNum, lTmp);
   } else if (*(pDocNum+4) == 'R')
   {
      // 1968R5520695   --> 196805520695
      // 1972R763 146   --> 197207630146
      strcpy(acTmp, pDocNum);
      myTrim(acTmp);
      replChar(acTmp, ' ', '0');
      lTmp = atol(&acTmp[5]);
      sprintf(acDocNum, "%.4s%.8d ", pDocNum, lTmp);      
      //sprintf(acDocNum, "%.4s%.8d ", pDocDate, lTmp);      
   } else if (isdigit(*(pDocNum+5)) && isdigit(*(pDocNum+6)))
   {
      // 194513367      --> 19450013367
      // 198012650087   --> 198012650087
      strcpy(acTmp, pDocNum);
      myTrim(acTmp);
      lTmp = atol(&acTmp[4]);
      sprintf(acDocNum, "%.4s%.6d   ", pDocNum, lTmp);      
      //sprintf(acDocNum, "%.4s%.6d   ", pDocDate, lTmp);      
   } else
   {
      // These are internal doc, can be ignored
      // 198215PM0058
      // 1994IL062593
      // 1989DOD 1/91
      bRet = false;
   }
   memcpy(pResult, acDocNum, SALE_SIZ_DOCNUM);

   if (bRet && memcmp(acDocNum, pDocDate, 4))
#ifdef _DEBUG
      LogMsg("Questionable Transaction: DocNum=%.12s DocDate=%.8s APN=%.12s, DocCode=%s, XferType=%s", acDocNum, pDocDate, pApn, apTokens[MB_SALES_DOCCODE], apTokens[MB_SALES_XFERTYPE]);
#else
      LogMsg("Questionable Transaction: DocNum=%.12s DocDate=%.8s APN=%.12s", acDocNum, pDocDate, pApn);
#endif
   return bRet;
}

/***************************** Mon_CreateSCSale ******************************
 *
 * Extract sale data from ???_Sales.csv and output to ???_SALE.DAT
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * DateFmt: 
 *    MM_DD_YYYY_1 (MON)
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Mon_CreateSCSale(int iDateFmt, bool bAppend)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp;

   LogMsg("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Init global
   lLastRecDate = 0;

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
      {
         memcpy(SaleRec.DocDate, acTmp, 8);
         iTmp = atol(acTmp);
         if (iTmp > lLastRecDate)
            lLastRecDate = iTmp;
      } else
         continue;

      // Docnum - yyyy1234567
      if (!Mon_FormatDocNum(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], SaleRec.DocDate, apTokens[MB_SALES_ASMT]))
         continue;

      // Save original DocCode
      vmemcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);

      // Doc code - most docs contain sale price
      if (SaleRec.DocCode[0] > ' ')
      {
         iTmp = findDocType(SaleRec.DocCode, (IDX_TBL5 *)&MON_DocCode[0]);
         if (iTmp >= 0)
         {
            memcpy(SaleRec.DocType, MON_DocCode[iTmp].pCode, MON_DocCode[iTmp].iCodeLen);
            if (MON_DocCode[iTmp].flag == 'P')
            {
               SaleRec.SaleCode[0] = 'P';
               SaleRec.NoneSale_Flg = 'Y';
            } else
            {
               SaleRec.NoneSale_Flg = MON_DocCode[iTmp].flag;
            }
         } else
         {
            //LogMsg("???Unknown DocCode: %s", apTokens[MB_SALES_DOCCODE]);
            SaleRec.NoneSale_Flg = 'Y';
         }
      } else if (*apTokens[MB_SALES_XFERTYPE] == '1')
      {
         SaleRec.DocType[0] = '1';
         SaleRec.NoneSale_Flg = 'N';
      }

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001130011000", 9))
      //   iTmp = 0;
#endif
      // Tax
      dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
      if (acTmp[0] > '0')
      {
         dTmp = atof(acTmp);
         lPrice = (long)(dTmp * SALE_FACTOR);

         // Check for bad DocTax
         if (dTmp > 100000)
         {
            iTmp = ((int)dTmp/100)*100;
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               LogMsg("??? Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f, CP=%d.  Need investigation.  Ignore price.", SaleRec.Apn, SaleRec.DocNum, dTmp, lPrice);
               lPrice = 0;
            }
         } else
         {
            iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
            memcpy(SaleRec.StampAmt, acTmp, iTmp);
         }

         // Ignore sale price if less than 1000
         if (lPrice >= 10000)
            sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
         else if (lPrice >= 1000)
            sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
         else
         {
            lPrice = 0;
            memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
         }
         memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);
      }

      // Confirmed sale price
      dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
      if (acTmp[0] > '0')
      {
         lTmp = atol(acTmp);
         if (lTmp < 1000)
         {
            // This doesn't look normal, but keep it for reference only
            iTmp = sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lTmp);
            lTmp = 0;
         } else
            iTmp = sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lTmp);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      } else
         lTmp = 0;

      // Use ConfirmedSalePrice if DocTax not avail.
      if (lPrice == 0 && lTmp > 0)
         memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);
     
#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001160016000", 9))
      //   iTmp = 0;
#endif

      // Transfer Type
      if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      // Seller
      if (*apTokens[MB_SALES_SELLER] > ' ')
         vmemcpy(SaleRec.Seller1, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);

      // Buyer
      if (*apTokens[MB_SALES_BUYER] > ' ')
         vmemcpy(SaleRec.Name1, apTokens[MB_SALES_BUYER], SALE_SIZ_BUYER);

      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(27,2,C,EQ,\"  \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -1;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            DeleteFile(acCSalFile);
   
            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -1;
      } else
         iTmp = rename(acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
         DeleteFile(acCSalFile);
      iTmp = rename(acOutFile, acCSalFile);
   }

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                       output:    %d.", lTmp);
   LogMsg("          Last recording date:    %d.", lLastRecDate);
   return iTmp;
}

/**************************** Mon_ConvStdChar ********************************
 *
 * Convert CountyPhyChar.txt to standard layout (STDCHAR).
 *
 *****************************************************************************/

int Mon_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[4], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("*** Missing input file %s", pInfile); 
      return -1;
   }

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 4096, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec)
         break;

      iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < MON_CHAR_POOLSPA)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }
#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "030128015000", 9))
      //   iRet = 0;
#endif

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[MON_CHAR_ASMT], strlen(apTokens[MON_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[MON_CHAR_FEEPRCL], strlen(apTokens[MON_CHAR_FEEPRCL]));
      // Format APN
      if (*apTokens[MON_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[MON_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[MON_CHAR_FEEPRCL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[MON_CHAR_BLDGSEQNO]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Unit# or UnitSeq - Currently no data
      iTmp = atoi(apTokens[MON_CHAR_UNITSEQNO]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.UnitSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** UnitSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[MON_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - 01, 02, 03, 99
      iTmp = blankRem(apTokens[MON_CHAR_POOLSPA]);
      if (iTmp > 1)
      {
         pRec = findXlatCode(apTokens[MON_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "101021012000", 8))
      //   iRet = 0;
#endif
      // QualityClass
      iTmp = blankRem(apTokens[MON_CHAR_QUALITYCLASS]);
      pRec = _strupr(apTokens[MON_CHAR_QUALITYCLASS]);
      vmemcpy(myCharRec.QualityClass, apTokens[MON_CHAR_QUALITYCLASS], SIZ_CHAR_QCLS, iTmp);
      if (*apTokens[MON_CHAR_QUALITYCLASS] == 'X' || (*(apTokens[MON_CHAR_QUALITYCLASS]) > '0' && isdigit(*(apTokens[MON_CHAR_QUALITYCLASS]+1)) ) )
      {
         strcpy(acTmp, apTokens[MON_CHAR_QUALITYCLASS]);
         if (*apTokens[MON_CHAR_QUALITYCLASS] == 'X')
            myCharRec.BldgClass = acTmp[1];
         else
            myCharRec.BldgClass = acTmp[0];

         acCode[0] = ' ';
         if (isdigit(acTmp[1]))
            iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
         else if (isdigit(acTmp[2]))
            iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
         else if (isdigit(acTmp[3]))
            iRet = Quality2Code((char *)&acTmp[3], acCode, NULL);
         else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[MON_CHAR_QUALITYCLASS], apTokens[MON_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (!memcmp(apTokens[MON_CHAR_QUALITYCLASS], "MH", 2))
      {
         strcpy(acTmp, apTokens[MON_CHAR_QUALITYCLASS]);
         //myCharRec.BldgClass = acTmp[0];

         acCode[0] = ' ';
         if (isdigit(acTmp[2]))
         {
            iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
            if (acCode[0] > ' ')
               myCharRec.BldgQual = acCode[0];
         }
      } else if (!memcmp(apTokens[MON_CHAR_QUALITYCLASS], "SS", 2) || 
                 !memcmp(apTokens[MON_CHAR_QUALITYCLASS], "STE", 3))
      {
         myCharRec.BldgClass = 'S';

         strcpy(acTmp, apTokens[MON_CHAR_QUALITYCLASS]);
         acCode[0] = ' ';
         if (isdigit(acTmp[3]))
         {
            iRet = Quality2Code((char *)&acTmp[3], acCode, NULL);
            if (acCode[0] > ' ')
               myCharRec.BldgQual = acCode[0];
         }
      } else if (!memcmp(apTokens[MON_CHAR_QUALITYCLASS], "SPEC", 4) || !memcmp(apTokens[MON_CHAR_QUALITYCLASS], "SPCL", 4))
      {
         strcpy(acTmp, apTokens[MON_CHAR_QUALITYCLASS]);
         myCharRec.BldgClass = 'M';
      } else if (strstr(apTokens[MON_CHAR_QUALITYCLASS], "GOOD") || strstr(apTokens[MON_CHAR_QUALITYCLASS], "GD"))
      {
         myCharRec.BldgQual = 'G';
         if (*apTokens[MON_CHAR_QUALITYCLASS] != 'G')
            myCharRec.BldgClass = *apTokens[MON_CHAR_QUALITYCLASS];
      } else if (!memcmp(apTokens[MON_CHAR_QUALITYCLASS], "AV", 2) || 
                  strstr(apTokens[MON_CHAR_QUALITYCLASS], "AVG") || 
                  strstr(apTokens[MON_CHAR_QUALITYCLASS], "AVE"))
      {
         myCharRec.BldgQual = 'A';     
         if (*apTokens[MON_CHAR_QUALITYCLASS] != 'A')
            myCharRec.BldgClass = *apTokens[MON_CHAR_QUALITYCLASS];
      } else if (strstr(apTokens[MON_CHAR_QUALITYCLASS], "POOR") ||
                 strstr(apTokens[MON_CHAR_QUALITYCLASS], "LOW"))
      {
         myCharRec.BldgQual = 'P';     
         if (*apTokens[MON_CHAR_QUALITYCLASS] != 'P' && *apTokens[MON_CHAR_QUALITYCLASS] != 'L')
            myCharRec.BldgClass = *apTokens[MON_CHAR_QUALITYCLASS];
      } else if (strstr(apTokens[MON_CHAR_QUALITYCLASS], "EXC"))
      {
         myCharRec.BldgQual = 'E';     
         if (*apTokens[MON_CHAR_QUALITYCLASS] != 'E')
            myCharRec.BldgClass = *apTokens[MON_CHAR_QUALITYCLASS];
      } else if (iTmp == 1)
      {
         if (*apTokens[MON_CHAR_QUALITYCLASS] > '0' && *apTokens[MON_CHAR_QUALITYCLASS] <= '9')
            myCharRec.BldgQual = *apTokens[MON_CHAR_QUALITYCLASS];
         else if (*apTokens[MON_CHAR_QUALITYCLASS] >= 'A')
            myCharRec.BldgClass = *apTokens[MON_CHAR_QUALITYCLASS];
      } else if (iTmp > 1 && isdigit(*apTokens[MON_CHAR_QUALITYCLASS])) 
      {
         iRet = Quality2Code(apTokens[MON_CHAR_QUALITYCLASS], acCode, NULL);
         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (isdigit(*(apTokens[MON_CHAR_QUALITYCLASS]+2))) 
      {
         iRet = Quality2Code(apTokens[MON_CHAR_QUALITYCLASS]+2, acCode, NULL);
         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
         if (*apTokens[MON_CHAR_QUALITYCLASS] >= 'A')
            myCharRec.BldgClass = *apTokens[MON_CHAR_QUALITYCLASS];
      } else if (bDebug)
      {
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[MON_CHAR_QUALITYCLASS], apTokens[MON_CHAR_ASMT]);
      }

      // Improved Condition
      if (*apTokens[MON_CHAR_CONDITION] >= '0')
      {
         pRec = findXlatCodeA(apTokens[MON_CHAR_CONDITION], &asCond[0]);
         if (pRec)
            myCharRec.ImprCond[0] = *pRec;
      }

      // YrBlt
      iTmp = atoi(apTokens[MON_CHAR_YRBLT]);
      if (iTmp > 1600 && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[MON_CHAR_EFFYR]);
      if (iTmp > 1600 && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[MON_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[MON_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // Garage Sqft
      int iGarSqft;
      iGarSqft = atoi(apTokens[MON_CHAR_GARAGESF]);
      if (iGarSqft > 100)
      {
         iRet = sprintf(acTmp, "%2d", iGarSqft);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
      }

      // Garage Type - Park space & type are not accurate based on observation
      // But here we just display as county provided
      if (*apTokens[MON_CHAR_GARAGE] > ' ')
      {
         if (!memcmp(apTokens[MON_CHAR_GARAGE], "010", 3))
         {
            myCharRec.ParkType[0] = 'I';
            myCharRec.ParkSpace[0] = '2';
         } else
         {
            iTmp = 0;
            while (asGarage[iTmp].iNameLen > 0)
            {
               if (!memcmp(apTokens[MON_CHAR_GARAGE], asGarage[iTmp].pName, asGarage[iTmp].iNameLen))
               {
                  myCharRec.ParkType[0] = asGarage[iTmp].pCode[0];
                  if (asGarage[iTmp].flag > '0')
                     myCharRec.ParkSpace[0] = asGarage[iTmp].flag;
                  break;
               }
               iTmp++;
            }
         }
      }

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[MON_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[MON_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      }
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[MON_CHAR_COOLING] == 'C')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[MON_CHAR_COOLING] == 'E')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[MON_CHAR_COOLING] == 'R')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[MON_CHAR_COOLING] == 'W')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[MON_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[MON_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[MON_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace
      if (*apTokens[MON_CHAR_FIREPLACE] > ' ')
      {
         pRec = findXlatCode(apTokens[MON_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      }

      // Sewer - only 4 records with "99"
      blankRem(apTokens[MON_CHAR_SEWERCODE]);
      if (*apTokens[MON_CHAR_SEWERCODE] > ' ')
         myCharRec.HasSewer = 'Y';

      // Irrigation water
      blankRem(apTokens[MON_CHAR_WATERSOURCE]);
      if (!memcmp(apTokens[MON_CHAR_WATERSOURCE], "W02", 3))
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'R';     // Shared well
      } if (!memcmp(apTokens[MON_CHAR_WATERSOURCE], "W01", 3))
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'B';     // City water
      } else if (*apTokens[MON_CHAR_WATERSOURCE] > ' ')
         myCharRec.HasWater = 'W';

      // ViewCode
      if (*apTokens[MON_CHAR_VIEWCODE] >= 'A')
      {
         pRec = findXlatCode(apTokens[MON_CHAR_VIEWCODE], &asView[0]);
         if (pRec)
            myCharRec.View[0] = *pRec;
      }

      // LotSqft
      int iLotSqft = atoi(apTokens[MON_CHAR_LANDSQFT]);
      if (iLotSqft > 0)
      {
         iRet = sprintf(acTmp, "%d", iLotSqft);
         memcpy(myCharRec.LotSqft, acTmp, iRet);
      }

      // Acres
      if (*apTokens[MON_CHAR_ACRES] > ' ')
      {
         double dLotAcre = atof(apTokens[MON_CHAR_ACRES]);
         long   lLotAcre;
         lLotAcre = (long)(dLotAcre * 1000.0);
         iRet = sprintf(acTmp, "%d", lLotAcre);
         memcpy(myCharRec.LotAcre, acTmp, iRet);

         if (iLotSqft < 2 && lLotAcre > 2)
         {
            iLotSqft = (long)(dLotAcre * SQFT_PER_ACRE);
            iRet = sprintf(acTmp, "%d", iLotSqft);
            memcpy(myCharRec.LotSqft, acTmp, iRet);
         }
      }

      // Stories
      iTmp = atoi(apTokens[MON_CHAR_STORIES]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Zoning - Only 3 records has data
      if (*apTokens[MON_CHAR_ZONING] > ' ')
         memcpy(myCharRec.Zoning, apTokens[MON_CHAR_ZONING], strlen(apTokens[MON_CHAR_ZONING]));

      // Building type
      if (*apTokens[MON_CHAR_BUILDINGTYPE] > ' ')
      {
         iTmp = strlen(apTokens[MON_CHAR_BUILDINGTYPE]);
         if (iTmp > SIZ_CHAR_BLDGTYPE)
            iTmp = SIZ_CHAR_BLDGTYPE;
         memcpy(myCharRec.BldgType, apTokens[MON_CHAR_BUILDINGTYPE], iTmp);
      }

      // Neighborhood code
      if (*apTokens[MON_CHAR_NEIGHBORHOODCODE] > ' ')
      {
         iTmp = strlen(apTokens[MON_CHAR_NEIGHBORHOODCODE]);
         if (iTmp > SIZ_CHAR_SIZE4)
            iTmp = SIZ_CHAR_SIZE4;
         memcpy(myCharRec.BldgType, apTokens[MON_CHAR_NEIGHBORHOODCODE], iTmp);
      }

      // Misc Sqft - unfinished
      iTmp = atoi(apTokens[MON_CHAR_UNFINAREASSF]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.MiscSqft, acTmp, iRet);
      }

      // UnitSize - not reliable
      //    001027001000: BldgSize=451, UnitSize=13922
      //    001027003000: BldgSize=89,  UnitSize=13922

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Sort on APN, BldgSeq, EffYr(d), location 45 & 85 is used to remove dup.
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D,85,1,C,A,45,2,C,A) DUPO(B8192,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/**************************** Mon_ConvStdChar ********************************
 *
 * Convert Mon_Char.csv to standard layout (STDCHAR).
 *
 *****************************************************************************/

//int Mon_ConvStdChar(char *pInfile)
//{
//   FILE     *fdIn, *fdOut;
//   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[4], *pRec;
//   int      iRet, iTmp, iFldCnt, iCnt=0;
//   STDCHAR  myCharRec;
//
//   LogMsg("\nConverting char file %s", pInfile);
//   if (!(fdIn = fopen(pInfile, "r")))
//   {
//      LogMsg("*** Missing input file %s", pInfile); 
//      return -1;
//   }
//
//   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   if (!(fdOut = fopen(acTmpFile, "w")))
//   {
//      fclose(fdIn);
//      LogMsg("***** Error creating output file %s", acTmpFile);
//      return -2;
//   }
//
//   // Skip first record - header
//   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
//      pRec = fgets(acBuf, 4096, fdIn);
//
//   while (!feof(fdIn))
//   {
//      pRec = fgets(acBuf, 4096, fdIn);
//      if (!pRec)
//         break;
//
//      iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
//      if (iFldCnt < MON_CHAR_COLS)
//      {
//         if (iFldCnt > 1)
//            LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
//         continue;
//      }
//#ifdef _DEBUG
//      //if (!memcmp(myCharRec.Apn, "030128015000", 9))
//      //   iRet = 0;
//#endif
//
//      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
//      memcpy(myCharRec.Apn, apTokens[MON_CHAR_ASMT], strlen(apTokens[MON_CHAR_ASMT]));
//      memcpy(myCharRec.FeeParcel, apTokens[MON_CHAR_FEEPRCL], strlen(apTokens[MON_CHAR_FEEPRCL]));
//      // Format APN
//      iRet = formatApn(apTokens[MON_CHAR_ASMT], acTmp, &myCounty);
//      memcpy(myCharRec.Apn_D, acTmp, iRet);
//
//      // Bldg#
//      iTmp = atoi(apTokens[MON_CHAR_BLDGSEQNO]);
//      if (iTmp > 0 && iTmp < 100)
//      {
//         iRet = sprintf(acTmp, "%2d", iTmp);
//         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
//      } else if (iTmp >= 100)
//         LogMsg("*** BldgSeqNo too big: %d", iTmp);
//
//      // Unit# or UnitSeq - Currently no data
//      iTmp = atoi(apTokens[MON_CHAR_UNITSEQNO]);
//      if (iTmp > 0 && iTmp < 100)
//      {
//         iRet = sprintf(acTmp, "%2d", iTmp);
//         memcpy(myCharRec.UnitSeqNo, acTmp, iRet);
//      } else if (iTmp >= 100)
//         LogMsg("*** UnitSeqNo too big: %d", iTmp);
//
//      // Rooms
//      iTmp = atoi(apTokens[MON_CHAR_TOTALROOMS]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.Rooms, acTmp, iRet);
//      }
//
//      // Pool - 01, 02, 03, 99
//      iTmp = blankRem(apTokens[MON_CHAR_NUMPOOLS]);
//      if (iTmp > 1)
//      {
//         pRec = findXlatCode(apTokens[MON_CHAR_NUMPOOLS], &asPool[0]);
//         if (pRec)
//            myCharRec.Pool[0] = *pRec;
//      }
//
//#ifdef _DEBUG
//      //if (!memcmp(myCharRec.Apn, "013253014000", 9))
//      //   iRet = 0;
//#endif
//      // QualityClass
//      iTmp = blankRem(apTokens[MON_CHAR_QUALITYCLASS]);
//      pRec = _strupr(apTokens[MON_CHAR_QUALITYCLASS]);
//      memcpy(myCharRec.QualityClass, apTokens[MON_CHAR_QUALITYCLASS], iTmp);
//      if (*apTokens[MON_CHAR_QUALITYCLASS] == 'X' || (*(apTokens[MON_CHAR_QUALITYCLASS]) > '0' && isdigit(*(apTokens[MON_CHAR_QUALITYCLASS]+1)) ) )
//      {
//         strcpy(acTmp, apTokens[MON_CHAR_QUALITYCLASS]);
//         if (*apTokens[MON_CHAR_QUALITYCLASS] == 'X')
//            myCharRec.BldgClass = acTmp[1];
//         else
//            myCharRec.BldgClass = acTmp[0];
//
//         acCode[0] = ' ';
//         if (isdigit(acTmp[1]))
//            iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
//         else if (isdigit(acTmp[2]))
//            iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
//         else if (isdigit(acTmp[3]))
//            iRet = Quality2Code((char *)&acTmp[3], acCode, NULL);
//         else
//            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[MON_CHAR_QUALITYCLASS], apTokens[MON_CHAR_ASMT]);
//
//         if (acCode[0] > ' ')
//            myCharRec.BldgQual = acCode[0];
//      } else if (*(apTokens[MON_CHAR_QUALITYCLASS]) >= 'A' && isdigit(*(apTokens[MON_CHAR_QUALITYCLASS]+2)) ) 
//      {
//         strcpy(acTmp, apTokens[MON_CHAR_QUALITYCLASS]);
//         myCharRec.BldgClass = acTmp[0];
//
//         acCode[0] = ' ';
//         iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
//         if (acCode[0] > ' ')
//            myCharRec.BldgQual = acCode[0];
//      } else if (isdigit(*(apTokens[MON_CHAR_QUALITYCLASS])) ) 
//      {
//         acCode[0] = ' ';
//         iRet = Quality2Code(apTokens[MON_CHAR_QUALITYCLASS], acCode, NULL);
//         if (acCode[0] > ' ')
//            myCharRec.BldgQual = acCode[0];
//      } else if (strstr(apTokens[MON_CHAR_QUALITYCLASS], "GOOD") || strstr(apTokens[MON_CHAR_QUALITYCLASS], " GD"))
//      {
//         myCharRec.BldgQual = 'G';
//         myCharRec.BldgClass = *apTokens[MON_CHAR_QUALITYCLASS];
//      } else if (strstr(apTokens[MON_CHAR_QUALITYCLASS], "AV"))
//      {
//         myCharRec.BldgQual = 'A';
//         myCharRec.BldgClass = *apTokens[MON_CHAR_QUALITYCLASS];
//      } else if (*apTokens[MON_CHAR_QUALITYCLASS] > ' ' && *apTokens[MON_CHAR_QUALITYCLASS] != 'U')
//         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[MON_CHAR_QUALITYCLASS], apTokens[MON_CHAR_ASMT]);
//
//      // YrBlt
//      iTmp = atoi(apTokens[MON_CHAR_YRBLT]);
//      if (iTmp > 1600 && iTmp <= lToyear)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.YrBlt, acTmp, iRet);
//      }
//
//      // YrEff
//      iTmp = atoi(apTokens[MON_CHAR_EFFYR]);
//      if (iTmp > 1600 && iTmp <= lToyear)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.YrEff, acTmp, iRet);
//      }
//
//      // BldgSize
//      int iBldgSize = atoi(apTokens[MON_CHAR_BUILDINGSIZE]);
//      if (iBldgSize > 100)
//      {
//         iRet = sprintf(acTmp, "%d", iBldgSize);
//         memcpy(myCharRec.BldgSqft, acTmp, iRet);
//      }
//
//      // Units Count
//      iTmp = atoi(apTokens[MON_CHAR_UNITSCNT]);
//      if (iTmp > 1)
//      {
//         iRet = sprintf(acTmp, "%2d", iTmp);
//         memcpy(myCharRec.Units, acTmp, iRet);
//      } 
//
//      // Garage Sqft
//      int iGarSqft;
//      iGarSqft = atoi(apTokens[MON_CHAR_GARAGESF]);
//      if (iGarSqft > 100)
//      {
//         iRet = sprintf(acTmp, "%2d", iGarSqft);
//         memcpy(myCharRec.GarSqft, acTmp, iRet);
//      }
//
//      // Garage Type - Park space & type are not accurate based on observation
//      // But here we just display as county provided
//      //if (*apTokens[MON_CHAR_GARAGE] > ' ')
//      //{
//      //   if (!memcmp(apTokens[MON_CHAR_GARAGE], "010", 3))
//      //   {
//      //      myCharRec.ParkType[0] = 'I';
//      //      myCharRec.ParkSpace[0] = '2';
//      //   } else
//      //   {
//      //      iTmp = 0;
//      //      while (asGarage[iTmp].iNameLen > 0)
//      //      {
//      //         if (!memcmp(apTokens[MON_CHAR_GARAGE], asGarage[iTmp].pName, asGarage[iTmp].iNameLen))
//      //         {
//      //            myCharRec.ParkType[0] = asGarage[iTmp].pCode[0];
//      //            if (asGarage[iTmp].flag > '0')
//      //               myCharRec.ParkSpace[0] = asGarage[iTmp].flag;
//      //            break;
//      //         }
//      //         iTmp++;
//      //      }
//      //   }
//      //}
//
//      // Heating - translation table has not been verified
//      iTmp = blankRem(apTokens[MON_CHAR_HEATING]);
//      if (iTmp > 0)
//      {
//         pRec = findXlatCode(apTokens[MON_CHAR_HEATING], &asHeating[0]);
//         if (pRec)
//            myCharRec.Heating[0] = *pRec;
//      }
//      
//      // Cooling - CentralAC, Evaporative, RoomWall, Window
//      if (*apTokens[MON_CHAR_COOLING] == 'C')
//         myCharRec.Cooling[0] = 'C';
//      else if (*apTokens[MON_CHAR_COOLING] == 'E')
//         myCharRec.Cooling[0] = 'E';
//      else if (*apTokens[MON_CHAR_COOLING] == 'R')
//         myCharRec.Cooling[0] = 'L';
//      else if (*apTokens[MON_CHAR_COOLING] == 'W')
//         myCharRec.Cooling[0] = 'W';
//
//      // Beds
//      iTmp = atoi(apTokens[MON_CHAR_BEDROOMS]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.Beds, acTmp, iRet);
//      }
//
//      // Full baths
//      iTmp = atoi(apTokens[MON_CHAR_BATHROOMS]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.FBaths, acTmp, iRet);
//      }
//
//      // Half bath
//      iTmp = atoi(apTokens[MON_CHAR_HALFBATHS]);
//      if (iTmp > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iTmp);
//         memcpy(myCharRec.HBaths, acTmp, iRet);
//      }
//
//      // FirePlace
//      if (*apTokens[MON_CHAR_FIREPLACE] > ' ')
//      {
//         pRec = findXlatCode(apTokens[MON_CHAR_FIREPLACE], &asFirePlace[0]);
//         if (pRec)
//            myCharRec.Fireplace[0] = *pRec;
//      }
//
//      // Sewer 
//      if (*apTokens[MON_CHAR_HASSEWER] == '1')
//         myCharRec.HasSewer = 'Y';
//
//      // Septic
//      if (*apTokens[MON_CHAR_HASSEPTIC] == '1')
//      {
//         myCharRec.HasSeptic = 'Y';
//         myCharRec.HasSewer = 'S';
//      }
//
//      // Irrigation water
//      //blankRem(apTokens[MON_CHAR_WATERSOURCE]);
//      //if (!memcmp(apTokens[MON_CHAR_WATERSOURCE], "W02", 3))
//      //{
//      //   myCharRec.HasWell = 'Y';
//      //   myCharRec.HasWater = 'R';     // Shared well
//      //} if (!memcmp(apTokens[MON_CHAR_WATERSOURCE], "W01", 3))
//      //{
//      //   myCharRec.HasWell = 'Y';
//      //   myCharRec.HasWater = 'B';     // City water
//      //} else if (*apTokens[MON_CHAR_WATERSOURCE] > ' ')
//      //   myCharRec.HasWater = 'W';
//
//      if (*apTokens[MON_CHAR_HASWELL] == '1')
//      {
//         myCharRec.HasWell = 'Y';
//         myCharRec.HasWater = 'W';
//      }
//
//      // ViewCode
//      //if (*apTokens[MON_CHAR_VIEWCODE] >= 'A')
//      //{
//      //   pRec = findXlatCode(apTokens[MON_CHAR_VIEWCODE], &asView[0]);
//      //   if (pRec)
//      //      myCharRec.View[0] = *pRec;
//      //}
//
//      // Stories
//      //iTmp = atoi(apTokens[MON_CHAR_STORIES]);
//      //if (iTmp > 0)
//      //{
//      //   iRet = sprintf(acTmp, "%d", iTmp);
//      //   memcpy(myCharRec.Stories, acTmp, iRet);
//      //}
//
//      myCharRec.CRLF[0] = '\n';
//      myCharRec.CRLF[1] = '\0';
//      fputs((char *)&myCharRec.Apn[0], fdOut);
//
//      if (!(++iCnt % 1000))
//         printf("\r%u", iCnt);
//   }
//
//   if (fdIn) fclose(fdIn);
//   if (fdOut) fclose(fdOut);
//
//   LogMsg("Number of records processed: %d", iCnt);
//
//   // Sort output on ASMT
//   if (iCnt > 100)
//   {
//      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
//      // Sort on APN, BldgSeq, EffYr(d), location 45 & 85 is used to remove dup.
//      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D,85,1,C,A,45,2,C,A) DUPO(B8192,)");
//      //iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D,85,1,C,A,45,2,C,A) DUPO(B2000,)");
//   } else
//   {
//      printf("\n");
//      iRet = 0;
//   }
//   return iRet;
//}

/*This version is used to load CountyPhyChar.txt dated 4/1/2-14
int Mon_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[4], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 4096, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);

      if (!pRec)
         break;

      iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < MON_CHAR_PCASMT)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[MON_CHAR_ASMT], strlen(apTokens[MON_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[MON_CHAR_FEEPRCL], strlen(apTokens[MON_CHAR_FEEPRCL]));
      // Format APN
      iRet = formatApn(apTokens[MON_CHAR_ASMT], acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      // Bldg#
      iTmp = atoi(apTokens[MON_CHAR_BLDGSEQNO]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Unit# - currently all 0
      iTmp = atoi(apTokens[MON_CHAR_SEQUENCE]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.UnitSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** UnitSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[MON_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - 01, 02, 03, 99
      iTmp = blankRem(apTokens[MON_CHAR_POOLSPA]);
      if (iTmp > 1)
      {
         pRec = findXlatCode(apTokens[MON_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "013253014000", 9))
      //   iRet = 0;
#endif
      // QualityClass
      iTmp = blankRem(apTokens[MON_CHAR_QUALITYCLASS]);
      pRec = _strupr(apTokens[MON_CHAR_QUALITYCLASS]);
      if (*apTokens[MON_CHAR_QUALITYCLASS] > ' ' && *apTokens[MON_CHAR_QUALITYCLASS] < 'T')
      {
         memcpy(myCharRec.QualityClass, apTokens[MON_CHAR_QUALITYCLASS], iTmp);

         acCode[0] = ' ';
         strcpy(acTmp, apTokens[MON_CHAR_QUALITYCLASS]);
         if (!_memicmp(acTmp, "AV", 2) || !_memicmp(acTmp, "GO", 2))
            acCode[0] = acTmp[0];
         else if (isalpha(acTmp[0])) 
         {
            myCharRec.BldgClass = acTmp[0];

            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            else if (isdigit(acTmp[2]))
               iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
            else if (strstr(acTmp, "AV"))
            {
               acCode[0] = 'A';
            } else if (strstr(acTmp, "GO"))
            {
               acCode[0] = 'G';
            } else if (strstr(acTmp, "EX"))
            {
               acCode[0] = 'E';
            } else if (strstr(acTmp, "VG"))
            {
               acCode[0] = 'V';
            } else 
            {
               pRec = strchr(acTmp, ' ');
               if (pRec)
               {
                  pRec++;
                  if (isdigit(*pRec))
                     iRet = Quality2Code(pRec, acCode, NULL);
                  else if (!_memicmp(pRec, "GO", 2) || !_memicmp(pRec, "AV", 2) || !_memicmp(pRec, "EX", 2) || !_memicmp(pRec, "VG", 2))
                     acCode[0] = *pRec;
                  else
                     LogMsg("*** Unknown QUALITYCLASS: '%s' in [%s]", apTokens[MON_CHAR_QUALITYCLASS], apTokens[MON_CHAR_ASMT]);
               }
            }
         } else if (isdigit(acTmp[0]))
            iRet = Quality2Code(acTmp, acCode, NULL);
         else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[MON_CHAR_QUALITYCLASS], apTokens[MON_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (*apTokens[MON_CHAR_QUALITYCLASS] > ' ' && *apTokens[MON_CHAR_QUALITYCLASS] != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[MON_CHAR_QUALITYCLASS], apTokens[MON_CHAR_ASMT]);

      // Improved Condition
      if (*apTokens[MON_CHAR_CONDITION] >= '0')
      {
         iTmp = 0;
         while (asCond[iTmp].iLen > 0)
         {
            if (!memcmp(apTokens[MON_CHAR_CONDITION], asCond[iTmp].acSrc, asCond[iTmp].iLen))
            {
               myCharRec.ImprCond[0] = asCond[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }

         // Do not use this since table is not sorted
         //pRec = findXlatCode(apTokens[MON_CHAR_CONDITION], &asCond[0]);
         //if (pRec)
         //   myCharRec.ImprCond[0] = *pRec;
      }

      // YrBlt
      iTmp = atoi(apTokens[MON_CHAR_YRBLT]);
      if (iTmp > 1600 && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[MON_CHAR_EFFYR]);
      if (iTmp > 1600 && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[MON_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[MON_CHAR_UNITSCNT]);
      if (iTmp > 0 && iBldgSize > iTmp*100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } else if (iTmp > 1)
         LogMsg("*** Questionable UnitsCnt: %d", iTmp);

      // Attached SF
      int iAttGar = atoi(apTokens[MON_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
      }

      // Detached SF
      int iDetGar = atoi(apTokens[MON_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
      }

      // Carport SF
      int iCarport = atoi(apTokens[MON_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
      }

      // Garage Type
      if (*apTokens[MON_CHAR_GARAGE] > ' ')
      {
         iTmp = 0;
         while (asGarage[iTmp].iNameLen > 0)
         {
            if (!memcmp(apTokens[MON_CHAR_GARAGE], asGarage[iTmp].pName, asGarage[iTmp].iNameLen))
            {
               myCharRec.ParkType[0] = asGarage[iTmp].pCode[0];
               if (asGarage[iTmp].flag > '0')
                  myCharRec.ParkSpace[0] = asGarage[iTmp].flag;
               break;
            }
            iTmp++;
         }

         if (myCharRec.ParkType[0] < '1')
         {
            if (iAttGar > 100)
                myCharRec.ParkType[0] = 'A';
            else if (iDetGar > 100)
                myCharRec.ParkType[0] = 'D';
            else if (iCarport > 100)
                myCharRec.ParkType[0] = 'C';
         }
      }

      // Park space - questionable values, only 11 parcels populated
      iTmp = atoi(apTokens[MON_CHAR_PARKINGSPACES]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.ParkSpace, acTmp, iRet);
      }

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[MON_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[MON_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      }
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[MON_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[MON_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[MON_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[MON_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[MON_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[MON_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[MON_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace
      if (*apTokens[MON_CHAR_FIREPLACE] > ' ')
      {
         pRec = findXlatCode(apTokens[MON_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      }

      // Sewer
      blankRem(apTokens[MON_CHAR_SEWERCODE]);
      if (*apTokens[MON_CHAR_SEWERCODE] > ' ')
         myCharRec.HasSewer = 'Y';

      blankRem(apTokens[MON_CHAR_HASSEPTIC]);
      if (*(apTokens[MON_CHAR_HASSEWER]) > '0')
         myCharRec.HasSewer = 'Y';
      else if (*(apTokens[MON_CHAR_HASSEPTIC]) > '0')
      {
         myCharRec.HasSeptic = 'Y';
         myCharRec.HasSewer = 'S';
      }
      
      blankRem(apTokens[MON_CHAR_HASWELL]);
      if (*(apTokens[MON_CHAR_HASWELL]) > '0')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // OfficeSpaceSF
      iTmp = atoi(apTokens[MON_CHAR_OFFICESPACESF]);
      if (iTmp > 10)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Misc.sExtra.OfficeSqft, acTmp, iRet);
      }

      // NonConditionSF

      // ViewCode
      if (*apTokens[MON_CHAR_VIEWCODE] >= 'A')
      {
         pRec = findXlatCode(apTokens[MON_CHAR_VIEWCODE], &asView[0]);
         if (pRec)
            myCharRec.View[0] = *pRec;
      }

      // Event Date
      if (*apTokens[MON_CHAR_EVENTDATE] >= '0')
      {  
         // Convert date from m/d/yyyy to yyyymmdd
         pRec = dateConversion(apTokens[MON_CHAR_EVENTDATE], acTmp, MM_DD_YYYY_1);
         if (pRec)
            memcpy(myCharRec.Misc.sExtra.RecDate, acTmp, 8);
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D,85,1,C,A,45,2,C,A) DUPO(B2000,)");
      //iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D,461,8,C,D,85,1,C,A,45,2,C,A) DUPO(B2000,)");
      //strcpy(pInfile, acCChrFile);
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}
*/

/******************************* Mon_ExtrLien *******************************
 *
 * Extract lien data from CountyData.txt
 *
 ****************************************************************************/

int Mon_ExtrLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp, iLen;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "001060037000", 12))
   //   iRet = 0;
#endif

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < MON_L_TRANSTAX)
   {
      LogMsg("***** Error: bad input record for APN=%s", pRollRec);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   iLen = strlen(apTokens[MON_L_ASMT]);
   if (iLen < iApnLen)
   {
      memcpy(pLienExtr->acApn, "0000", iApnLen - iLen);
      memcpy(&pLienExtr->acApn[iApnLen - iLen], apTokens[MON_L_ASMT], iLen);
   } else
      memcpy(pLienExtr->acApn, apTokens[MON_L_ASMT], iLen);

   // TRA
   lTmp = atol(apTokens[MON_L_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[MON_L_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[MON_L_IMP]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lPP   = atoi(apTokens[MON_L_PERSPROP]);
   if (lPP > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lPP);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);
      memcpy(pLienExtr->acPP_Val, acTmp, SIZ_OTHER);
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Prop 8
   /* Not avail in 2014 LDR
   lTmp = atoin(apTokens[MON_L_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Save Exe code
   memcpy(pLienExtr->extra.MB.ExeCode1, apTokens[MON_L_EXEMPTIONCODE1], strlen(apTokens[MON_L_EXEMPTIONCODE1]));
   memcpy(pLienExtr->extra.MB.ExeCode2, apTokens[MON_L_EXEMPTIONCODE2], strlen(apTokens[MON_L_EXEMPTIONCODE2]));
   memcpy(pLienExtr->extra.MB.ExeCode3, apTokens[MON_L_EXEMPTIONCODE3], strlen(apTokens[MON_L_EXEMPTIONCODE3]));
   */

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int Mon_ExtrLien(LPCSTR pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg("Extract LDR roll for %s", pCnty);

   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acRollFile);
      return -2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Replace null char with space
      //iRet = replChar(acRec, 0, ' ', 0);

      // Create new record
      iRet = Mon_ExtrLienRec(acBuf, acRec);

      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/*********************************** loadMon ********************************
 *
 * Options:
 *    -CMON -L  load lien
 *    -CMON -La create assessor data set using lien file
 *    -CMON -U  load update
 *    -CMON -Ua update assessor data
 *    -CMON -G  load GrGr (load grgr files from the county and extract 
 *              Sale_exp.dat and Mon_GrGr.dat for DataEntry)
 *    -CMON -U -Xsi -G (daily update) 
 *          -Fs clear old sale
 *
 * Notes: This is new version to handle 2007 LDR file.
 *
 ****************************************************************************/

int loadMon(int iSkip)
{
   int   iRet=0;
   char  acTmp[_MAX_PATH], acLienFile[_MAX_PATH], acCumGrGr[_MAX_PATH];

   sprintf(acCumGrGr, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;

   iApnLen = myCounty.iApnLen;

   // Fix GrGr file - one time
   //iRet = FixGrGrDef(acCumGrGr, 0, false);

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Extract lien file
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acLienFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acLienFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acLienFile, 0);
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode);  // 2013, 2022
      //iRet = Mon_ExtrLien(myCounty.acCntyCode);  // 2014
   }

   // Load GrGr
   if (iLoadFlag & LOAD_GRGR)                      // -G
   {
      // Create Mon_GrGr.dat
      LogMsg0("Load %s GrGr file", myCounty.acCntyCode);
      iRet = Mon_LoadGrGr(myCounty.acCntyCode);
      if (!iRet)
         iLoadFlag |= MERG_GRGR;                   // Trigger merge grgr
      else if (iRet < 0)
      {
         bGrGrAvail = false;
         if (bSendMail &&  !(iLoadFlag & (LOAD_LIEN|LOAD_UPDT)))
            return iRet;
      }
   } else if (iLoadFlag & MERG_GRGR)
   {
      // Extract to Grgr_Exp.dat and copy to Grgr_Exp.Sls
      iRet = Mon_ExtrSaleMatched(acCumGrGr, true); 
   }

   // Create/Update cum sale file
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      iRet = Mon_CreateSCSale(MM_DD_YYYY_1, false);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Load CHAR file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      iRet = Mon_ConvStdChar(acCharFile);
      if (iRet < 0)
      {
         LogMsg("***** Error converting Char file %s", acCharFile);
         iRet = -1;
      } else if (!iRet)
         LogMsg("*** WARNING: Char file is empty %s", acCharFile);
      else
         iRet = 0;
   }

   iRet = 0;
   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // 2014 - Rerun
      sprintf(acLienFile, "%s\\%s\\%s_LIEN.CSV", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      // Get LDR file name
      GetIniString(myCounty.acCntyCode, "LienFile", "", acRollFile, _MAX_PATH, acIniFile);

      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      iRet = sortFile(acRollFile, acLienFile, "S(1,12,C,A) ");
      if (iRet > 0)
      {
         iRet = 0;
         strcpy(acRollFile, acLienFile);
         // 2021
         //iRet = Mon_Load_LDR(iSkip);
         // 2022-2023
         iRet = Mon_Load_LDR3(iSkip);
      } else
      {
         iRet = -1;
         LogMsg("***** Error sorting %s", acRollFile);
      }

      /* 2014
      if (iLoadFlag & LOAD_LIEN)
      {
         LogMsg("Load %s Lien file", myCounty.acCntyCode);
         iRet = Mon_Load_LDR1(iSkip);
      }
      */
   }

   if (!iRet && (iLoadFlag & LOAD_UPDT))           // -U
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Mon_Load_Roll(iSkip);
   }

   // Extract Book 799 from tax file
   if (iLoadTax & TAX_EXTRACT)                     // -Te
   {
      // Add state assessment records
      char sTaxExtr[_MAX_PATH], sR01[_MAX_PATH], sTmpFile[_MAX_PATH];

      GetIniString(myCounty.acCntyCode, "ExtrBook", "", acTmp, _MAX_PATH, acIniFile);
      iRet = TC_CreateR01(myCounty.acCntyCode, acTmp);
      if (!iRet)
      {
         LogMsg("***** No parcel matching book %s.  Please remove -Te option or update ExtrBook in INI file", acTmp);
         return -1;
      }

      sprintf(sR01, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
      sprintf(sTaxExtr, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "XT1");
      sprintf(sTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
      if (!iRet && !_access(sTaxExtr, 0))
      {
         LogMsg0("Appending special book to R01 file");
         iRet = appendFixFile(sTaxExtr, sR01, iRecLen);
         if (iRet > 0)
         {
            iRet = sortFile(sR01, sTmpFile, "S(1,12,C,A) BYPASS(1900,R) F(FIX,1900)");
            if (iRet > 0)
            {
               if (DeleteFileA(sR01))
               {
                  iRet = rename(sTmpFile, sR01);
                  LogMsg("Successfully adding special book to R01 file");
               }
            }
         }
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Set ChkYear flag to validate year on DocNum against DocDate when update sale
      SetChkYearFlg(true);

      // Apply Mon_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   if (!iRet && (iLoadFlag & MERG_GRGR))           // -Mg
   {
      sprintf(acTmp, acEGrGrTmpl, myCounty.acCntyCode, "Dat");
      LogMsg0("Update %s roll with GrGr file %s", myCounty.acCntyCode, acTmp);
      iRet = MergeGrGrExpFile(acTmp, true, false, true);
      if (iRet > 0)
         iRet = 0;      // If file not available, ignore it
      else if (!iRet && !(iLoadFlag & LOAD_LIEN))
         iLoadFlag |= LOAD_UPDT;                   // Trigger buildcda
   }

   if (!iRet && bMergeOthers)
   {
      char acTmpFile[_MAX_PATH];

      // Merge other values
      sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmpFile, 0))
         iRet = PQ_MergeOthers(myCounty.acCntyCode, acTmpFile, GRP_MB, iSkip);
      else
         LogMsg("***** Lien extract is missing: %s.  Please locate the file and retry", acTmpFile);
   }

   return iRet;
}
