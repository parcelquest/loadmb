#ifndef  _SITUS_H_
#define  _SITUS_H_

typedef struct _tZip2City
{
   int   iZipCode;
   int   iCityCode;
   int   iCityLen;
   char  acCity[32];
   char  acCityCode[8];
   char  acZip[12];
} ZIP_CITY;

typedef struct _tCityZip
{  
   char  acApn[20];
   char  acCity[32];
   char  acCityCode[8];
   char  acZip[12];
   char  bVerify;
   char  filler[7];
} CITYZIP;

#if !defined(_SITUS_SRC_)
   extern FILE *fdCity;
   extern long lCitySkip, lCityMatch;
#endif

ZIP_CITY *getCity(char *pZip);
//int  getCityZip(CITYZIP *pCityZip, char *pApn, int iCmpLen);
int  getCityZip(char *pApn, char *pCity, char *pZip, int iCmpLen);
int  getCityZip(char *pApn, char *pCity, char *pZip, char *pCityCode, int iCmpLen);

char *getCity(char *pZip, char *pCity, char *pCityCode=NULL);
char *getZip(char *pCity, char *pZip=NULL);

// Use this function to init ???_Zip2City.Srt
int  loadZip2City(char *pIniFile, char *pCnty);

// Use these function to init ???_CityZip.Srt
int  initSitus(char *pIniFile, char *pCnty);
void closeSitus();

#endif