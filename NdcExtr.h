// Sale data extracted from mp_00_recorder
#define NR_PARCEL_NBR_FMT     1
#define NR_PARCEL_NBR_RAW     2
#define NR_EXT_PROPERTY_ID    3
#define NR_DATE_TRANSFER      4
#define NR_DOC_NBR_FMT        5
#define NR_DOC_TYPE           6
#define NR_DEED_TYPE          7
#define NR_FULL_PART_CODE     8
#define NR_TAX_TRANSFER       9
#define NR_VAL_TRANSFER       10
#define NR_BUYER              11
#define NR_SELLER             12
#define NR_MAIL_ADDR_RAW      13
#define NR_MAIL_HOUSE_NBR     14
#define NR_MAIL_FRACTION      15
#define NR_MAIL_DIR           16
#define NR_MAIL_STREET_NAME   17
#define NR_MAIL_SUF           18
#define NR_MAIL_POST_DIR      19
#define NR_MAIL_UNIT_PRE      20
#define NR_MAIL_UNIT_VAL      21
#define NR_MAIL_CITY          22
#define NR_MAIL_STATE         23
#define NR_MAIL_ZIP           24
#define NR_MAIL_PLUS_4        25
#define NR_MULT_APN_FLAG      26    // Y or M
#define NR_TRAN_TYPE          27    // cons, refi, resa, subd, xfer
#define NR_SALE_OR_TRANSFER   28
#define NR_INSERTDATE         29
#define NR_SITUS1             30
#define NR_SITUS2             31
#define NR_SALE_FLDS          32

//  Situs extract from mp_00_assessor
#define NR_APN                0
#define NR_S_FULL_ADDR        1
#define NR_S_HOUSE_NBR        2
#define NR_S_FRACTION         3
#define NR_S_DIR              4
#define NR_S_STREET_NAME      5
#define NR_S_SUF              6
#define NR_S_POST_DIR         7
#define NR_S_UNIT_PRE         8
#define NR_S_UNIT_VAL         9
#define NR_S_CITY             10
#define NR_S_ZIP              11
#define NR_S_ZIP4             12

int NR_CreateSCSale(char *pCnty, char *pInfile, int iMinApnLen);

