
#if !defined(AFX_MERGESHA_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_)
#define AFX_MERGESHA_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_

#define SHA_CHAR_FEEPARCEL		0
#define SHA_CHAR_CATTYPEA		1
#define SHA_CHAR_BLDGSEQNUM	2
#define SHA_CHAR_CATTYPEB		3
#define SHA_CHAR_UNITSEQNUM	4
#define SHA_CHAR_CATTYPEC		5
#define SHA_CHAR_POOLSPA		6
#define SHA_CHAR_RESOURCEID	7
#define SHA_CHAR_VALUE			8
#define SHA_CHAR_BEDROOMS		9
#define SHA_CHAR_BATHS			10
#define SHA_CHAR_HALFBATHS		11
#define SHA_CHAR_NUMFIELDS		12

#define SHA_MH_FEEPARCEL		0
#define SHA_MH_QUALCLS		   1
#define SHA_MH_YEARBUILT		2
#define SHA_MH_BLDGGSIZE		3
#define SHA_MH_NUMBEDROOMS		4
#define SHA_MH_NUMFULLBATHS	5
#define SHA_MH_ASMT				6
#define SHA_MH_BLDGTYPE			7

static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "0",  "L", 1,               // None
   "BB", "F", 2,               // Baseboard
   "EL", "F", 2,               // Electric
   "FH", "B", 2,               // FAH
   "FL", "C", 2,               // Floor
   "GR", "A", 2,               // Gravity
   "HP", "G", 2,               // Heat Pump
   "MO", "Y", 2,               // Monitor
   "OT", "X", 2,               // Other
   "PE", "Y", 2,               // Perim
   "WL", "D", 2,               // Wall
   "Z",  "9", 1,               // Unknown
   "",   "",  0
};

static XLAT_CODE  asCooling[] =
{
   // Value, lookup code, value length
   "99", "Y", 2,               // Building structure
   "C",  "C", 1,               // Central
   "EV", "E", 2,               // Evaporative
   "FC", "H", 2,               // FAC
   "OT", "X", 2,               // Other
   "WA", "L", 2,               // Wall
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "0",  "N", 1,               // None
   "1",  "P", 1,               // Pool
   "99", "P", 2,               // Pool
   "C",  "D", 1,               // Community Pool
   "FG", "F", 2,               // Fiberglass
   "GN", "G", 2,               // Gunite
   "ID", "I", 2,               // Indoor
   "PH", "C", 2,               // Pool & Hot tub
   "PO", "P", 2,               // Pool any
   "V",  "B", 1,               // Vinyl 
   "VY", "B", 2,               // Vinyl (In Ground)
   "Z",  "U", 1,               // Unknown
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "0",  "N", 1,               // None
   "1",  "1", 1,               // 
   "99", "Y", 2,               // 
   "EV", "Y", 2,               // 
   "GA", "G", 2,               // Gas
   "MA", "L", 2,               // Masonry
   "MU", "M", 2,               // Multiple
   "N",  "N", 1,               // None
   "OT", "O", 2,               // Other
   "PS", "S", 2,               // PelletStove
   "UNK","U", 2,               // Unknown type
   "WS", "W", 2,               // Woodstove
   "Z",  "Z", 1,               // ZeroClearance
   "ZC", "Z", 2,               // ZeroClearance
   "",   "",  0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SHA_Exemption[] = 
{
   "E01", "H", 3,1,
   "E05", "I", 3,1,     // HOSPITAL EXEMPTION
   "E06", "I", 3,1,     // HOSPITAL EXEMPTION LAND
   "E07", "I", 3,1,     // HOSPITAL EXEMPTION STRUCTURE
   "E08", "I", 3,1,     // HOSPITAL EXEMPTION PERSONAL PROPERTY
   "E11", "V", 3,1,
   "E12", "D", 3,1,     // DISABLED VETERANS
   "E13", "D", 3,1,     // DISABLED VETERANS SAME HOUSEHOLD
   "E14", "D", 3,1,     // DISABLED VETERANS LOW INCOME
   "E15", "D", 3,1,     // DISABLED VETERANS LOW INCOME SAME HOUSEHOLD
   "E21", "W", 3,1,     // WELFARE EXEMPTION
   "E22", "W", 3,1,     // WELFARE EXEMPTION LAND
   "E23", "W", 3,1,     // WELFARE EXEMPTION STRUCTURE
   "E24", "W", 3,1,     // WELFARE EXEMPTION PERSONAL PROPERTY
   "E26", "S", 3,1,     // PRIVATE SCHOOL
   "E27", "S", 3,1,     // PRIVATE SCHOOL LAND
   "E28", "S", 3,1,     // PRIVATE SCHOOL STRUCTURE
   "E29", "S", 3,1,     // PRIVATE SCHOOL PERSONAL PROPERTY
   "E31", "C", 3,1,     // CHURCH EXEMPTION
   "E32", "C", 3,1,     // CHURCH EXEMPTION LAND
   "E33", "C", 3,1,     // CHURCH EXEMPTION STRUCTURE
   "E34", "C", 3,1,     // CHURCH EXEMPTION PERSONAL PROPERTY
   "E41", "U", 3,1,     // COLLEGE EXEMPTION
   "E42", "U", 3,1,     // COLLEGE EXEMPTION LAND
   "E43", "U", 3,1,     // COLLEGE EXEMPTION STRUCTURE
   "E44", "U", 3,1,     // COLLEGE EXEMPTION PERSONAL PROPERTY
   "E51", "P", 3,1,     // PUBLIC SCHOOLS
   "E52", "P", 3,1,     // PUBLIC SCHOOLS - LAND
   "E53", "P", 3,1,     // PUBLIC SCHOOLS - STRUCTURE
   "E54", "P", 3,1,     // PUBLIC SCHOOLS - PERSONAL PROPERTY
   "E61", "E", 3,1,     // CEMETERY EXEMPTION
   "E62", "E", 3,1,     // CEMETERY EXEMPTION - LAND
   "E63", "E", 3,1,     // CEMETERY EXEMPTION - STRUCTURE
   "E64", "E", 3,1,     // CEMETERY EXEMPTION - PERSONAL PROPERTY
   "E71", "R", 3,1,     // RELIGIOUS EXEMPTION
   "E72", "R", 3,1,     // RELIGIOUS EXEMPTION - LAND
   "E73", "R", 3,1,     // RELIGIOUS EXEMPTION - STRUCTURE
   "E74", "R", 3,1,     // RELIGIOUS EXEMPTION - PERSONAL PROPERTY
   "E80", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E93", "X", 3,1,     // PENALTY
   "E95", "X", 3,1,     // PI LOW VALUE
   "E96", "X", 3,1,     // LOW VALUE EXEMPTION
   "E98", "X", 3,1,     // OTHER
   "","",0,0
};

#endif // !defined(AFX_MERGESHA_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_)
