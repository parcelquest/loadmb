/********************************************************************
 *
 * 07/02/2021 Modify MergeStdCharRec() to populate QualityClass.
 * 04/23/2024 Remove unused code.
 *
 ********************************************************************/



#include "stdafx.h"
#include "CountyInfo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "CharRec.h"

static   FILE  *fdChar;
static   long  lCharSkip, lCharMatch;

extern   COUNTY_INFO myCounty;

extern   bool  bDebug;
extern   char  acIniFile[], acCChrFile[];
extern   char  acRawTmpl[], acAttrTmpl[];
extern   char  acLookupTbl[], acCharFile[];
extern   int   iRecLen, iRollLen, iCharLen, iApnLen;
extern   int   iNoMatch;

/******************************** MergeCharXRec ******************************
 *
 *
 *****************************************************************************/

//int MergeCharXRec(char *pOutbuf)
//{
//   static   char  acRec[1024], *pRec=NULL;
//   int      iLoop;
//   CHAR_EXT *pChar;
//
//   // Get first Char rec for first call
//   if (!pRec)
//      pRec = fgets(acRec, 1024, fdChar);
//
//   pChar = (CHAR_EXT *)&acRec[0];
//   do
//   {
//      if (!pRec)
//      {
//         fclose(fdChar);
//         fdChar = NULL;
//         return 1;      // EOF
//      }
//
//      // Compare Apn
//      iLoop = memcmp(pOutbuf, pChar->Apn, iApnLen);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip CHAR rec  %.*s", iApnLen, pChar->Apn);
//         pRec = fgets(acRec, 1024, fdChar);
//         lCharSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 0;
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "004020015", 9))
//   //   iRet = 0;
//#endif
//
//   // Merge char rec
//   memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_ACRES);
//   memcpy(pOutbuf+OFF_LOT_SQFT, pChar->LotSqft, SIZ_LOT_SQFT);
//   memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
//   memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);
//   memcpy(pOutbuf+OFF_BLDG_SF, pChar->BldgSqft, SIZ_BLDG_SF);
//   memcpy(pOutbuf+OFF_UNITS, pChar->Units, SIZ_UNITS);
//   memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
//   memcpy(pOutbuf+OFF_BEDS, pChar->Beds, SIZ_BEDS);
//   memcpy(pOutbuf+OFF_BATH_F, pChar->Bath, SIZ_BATH_F);
//   memcpy(pOutbuf+OFF_BATH_H, pChar->HBath, SIZ_BATH_H);
//   memcpy(pOutbuf+OFF_ROOMS, pChar->Rooms, SIZ_ROOMS);
//   memcpy(pOutbuf+OFF_BLDG_CLASS, pChar->BldgClass, SIZ_BLDG_CLASS);
//   memcpy(pOutbuf+OFF_BLDG_QUAL, pChar->BldgQual, SIZ_BLDG_QUAL);
//   memcpy(pOutbuf+OFF_IMPR_COND, pChar->ImprCond, SIZ_IMPR_COND);
//   memcpy(pOutbuf+OFF_FIRE_PL, pChar->FirePlace, SIZ_FIRE_PL);
//   memcpy(pOutbuf+OFF_BLDGS, pChar->Bldgs, SIZ_BLDGS);
//   memcpy(pOutbuf+OFF_AIR_COND, pChar->AC, SIZ_AIR_COND);
//   memcpy(pOutbuf+OFF_HEAT, pChar->Heat, SIZ_HEAT);
//   memcpy(pOutbuf+OFF_GAR_SQFT, pChar->GarSqft, SIZ_GAR_SQFT);
//   memcpy(pOutbuf+OFF_PARK_TYPE, pChar->ParkType, SIZ_PARK_TYPE);
//   memcpy(pOutbuf+OFF_PARK_SPACE, pChar->ParkSpace, SIZ_PARK_SPACE);
//   memcpy(pOutbuf+OFF_VIEW, pChar->View, SIZ_VIEW);
//   memcpy(pOutbuf+OFF_ROOF_TYPE, pChar->RoofType, SIZ_ROOF_TYPE);
//   memcpy(pOutbuf+OFF_ROOF_MAT, pChar->RoofMat, SIZ_ROOF_MAT);
//   memcpy(pOutbuf+OFF_WATER, pChar->Water, SIZ_WATER);
//   memcpy(pOutbuf+OFF_SEWER, pChar->Sewer, SIZ_SEWER);
//   memcpy(pOutbuf+OFF_POOL, pChar->Pool, SIZ_POOL);
//
//   // Read next record
//   pRec = fgets(acRec, 1024, fdChar);
//
//   lCharMatch++;
//  
//   return 0;
//}

/******************************* MergeCharRec ********************************
 *
 * Merge extracted char data into current R01 record
 *
 *****************************************************************************/

//int MergeCharRec(char *pCnty)
//{
//   char     cFileCnt=1;
//   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acAttrFile[_MAX_PATH];
//   char     acBuf[MAX_RECSIZE];
//
//   HANDLE   fhIn, fhOut;
//
//   int      iRet, iUpdCnt;
//   DWORD    nBytesRead;
//   DWORD    nBytesWritten;
//   BOOL     bRet;
//   long     lRet=0, lCnt=0;
//
//   // Load tables
//   iRet = LoadLUTable((char *)&acLookupTbl[0], "[Quality]", NULL, MAX_ATTR_ENTRIES);
//   if (!iRet)
//   {
//      LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
//      return 1;
//   }
//
//   sprintf(acAttrFile, acAttrTmpl, pCnty, "DAT");
//   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
//   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "T01");
//
//   // Open Char file
//   LogMsg("Open Characteristic file %s", acAttrFile);
//   fdChar = fopen(acAttrFile, "r");
//   if (fdChar == NULL)
//   {
//      LogMsg("*** Error opening Char file: %s\n", acAttrFile);
//      return 2;
//   }
//
//   // Open Input file
//   LogMsg("Open input file %s", acRawFile);
//   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhIn == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("*** Error opening input file: %s\n", acRawFile);
//      return 3;
//   }
//
//   // Open Output file
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("*** Error creating output file: %s\n", acOutFile);
//      return 4;
//   }
//
//   // Copy skip record 
//   memset(acBuf, ' ', iRecLen);
//   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
//   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//
//   iUpdCnt=iNoMatch=lCharSkip=lCharMatch=0;
//   cFileCnt = 1;
//   while (ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
//   {
//      if (!nBytesRead)
//      {
//         // EOF
//         /*
//         cFileCnt++;
//         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
//         acOutFile[strlen(acOutFile)-1] = cFileCnt | 0x30;
//         if (!_access(acRawFile, 0))
//         {
//            CloseHandle(fhIn);
//            CloseHandle(fhOut);
//            fhIn = fhOut = 0;
//
//            // Open next Input file
//            LogMsg("Open input file %s", acRawFile);
//            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
//                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//            if (fhIn == INVALID_HANDLE_VALUE)
//            {
//               fhIn = 0;
//               break;
//            }
//
//            if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, 
//                NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
//            {
//               iRet = -4;
//               fhOut = 0;
//               break;
//            }
//
//            if (!ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
//               break;
//         } else
//         */
//            break;
//      }
//
//      // Update CHAR
//      if (fdChar)
//         if (!MergeCharXRec(acBuf))
//            iUpdCnt++;
//
//      // Write output record
//      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   if (fhIn)
//      CloseHandle(fhIn);
//   if (fhOut)
//      CloseHandle(fhOut);
//   if (fdChar)
//      fclose(fdChar);
//
//   // Rename outfile
//   LogMsg("Rename output file");
//   sprintf(acBuf, acRawTmpl, pCnty, pCnty, "M01");
//   if (!_access(acBuf, 0))
//      remove(acBuf);
//   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
//   rename(acRawFile, acBuf);
//   iRet = rename(acOutFile, acRawFile);
//
//   LogMsg("Total output records:       %u", lCnt);
//   LogMsg("Total matched records:      %u", iUpdCnt);
//
//   if (!iRet)
//      iRet = iUpdCnt;
//   return iRet;
//}

/********************************* ExtrHChar *********************************
 *
 * Extract history char data..  
 * If successful, it returns number of record extracted. 0 if missing file, 
 * <0 if error opening files.
 *
 *****************************************************************************/

//long ExtrHChar(char *pInfile, char *pOutfile)
//{
//   int      iLen;
//   long     lCnt=0;
//   unsigned long nBytesRead;
//   char     acBuf[2048], acOutbuf[512], *pInbuf, cFileCnt;
//
//   HANDLE   fhIn;
//   FILE     *fdOut;
//   CHAR_EXT *pRec = (CHAR_EXT *)&acOutbuf;
//
//   LogMsg("Extract history characteristic data from R01 file: %s", pInfile);
//
//   // Check file is exist
//   if (_access(pInfile, 0))
//   {
//      LogMsg("***** Missing input file: %s", pInfile);
//      return 0;
//   }
//
//   LogMsg("Open input file %s", pInfile);
//   if (INVALID_HANDLE_VALUE == (fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, 
//       NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
//   {
//      LogMsg("***** Error opening input file: %s\n", pInfile);
//      return -3;
//   }
//
//   // Skip test record
//   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
//
//   // Open Output file
//   if (!(fdOut = fopen(pOutfile, "w")))
//   {
//      LogMsg("***** Error creating output file: %s\n", pOutfile);
//      return -4;
//   }
//
//   memset(acOutbuf, ' ', sizeof(CHAR_EXT));
//   pRec->CrLf[0] = '\n';
//   pRec->CrLf[1] = 0;
//   pInbuf = acBuf;
//   cFileCnt = 1;
//   iLen = (OFF_LEGAL)-(OFF_LOT_ACRES);
//   while (ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
//   {
//      if (!nBytesRead)
//      {
//         // EOF
//         cFileCnt++;
//         pInfile[strlen(pInfile)-1] = cFileCnt | 0x30;
//         if (!_access(pInfile, 0))
//         {
//            CloseHandle(fhIn);
//            fhIn = 0;
//
//            // Open next Input file
//            LogMsg("Open input file %s", pInfile);
//            fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
//                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//            if (fhIn == INVALID_HANDLE_VALUE)
//            {
//               fhIn = 0;
//               break;
//            }
//            if (!ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
//               break;
//         } else
//            break;
//      }
//
//#ifdef _DEBUG
//      //if (!memcmp(pInbuf, "009600434", 9))
//      //   iTmp = 0;
//#endif
//
//      memcpy(pRec->Apn, pInbuf+OFF_APN_S, SIZ_APN_S);
//      memcpy(pRec->LotAcre, pInbuf+OFF_LOT_ACRES, iLen);
//
//      fputs(acOutbuf, fdOut);
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   if (fhIn)
//      CloseHandle(fhIn);
//   if (fdOut)
//      fclose(fdOut);
//
//   LogMsg("Char extract complete with %ld records", lCnt);
//   return lCnt;
//}

/****************************** MergeStdCharRec ******************************
 *
 *
 *****************************************************************************/

int MergeStdCharRec(char *pOutbuf)
{
   static   char  acRec[2048], *pRec=NULL;
   int      iLoop;
   STDCHAR  *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   pChar = (STDCHAR *)&acRec[0];
   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Apn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip CHAR rec  %.*s", iApnLen, pChar->Apn);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004020015", 9))
   //   iRet = 0;
#endif

   // Merge char rec
   memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_ACRES);
   memcpy(pOutbuf+OFF_LOT_SQFT, pChar->LotSqft, SIZ_LOT_SQFT);
   memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
   memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);
   memcpy(pOutbuf+OFF_BLDG_SF, pChar->BldgSqft, SIZ_BLDG_SF);
   memcpy(pOutbuf+OFF_UNITS, pChar->Units, SIZ_UNITS);
   memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
   memcpy(pOutbuf+OFF_BEDS, pChar->Beds, SIZ_BEDS);
   memcpy(pOutbuf+OFF_BATH_F, pChar->FBaths, SIZ_BATH_F);
   memcpy(pOutbuf+OFF_BATH_H, pChar->HBaths, SIZ_BATH_H);
   memcpy(pOutbuf+OFF_ROOMS, pChar->Rooms, SIZ_ROOMS);

   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   memcpy(pOutbuf+OFF_IMPR_COND, pChar->ImprCond, SIZ_IMPR_COND);
   memcpy(pOutbuf+OFF_FIRE_PL, pChar->Fireplace, SIZ_FIRE_PL);
   memcpy(pOutbuf+OFF_BLDGS, pChar->Bldgs, SIZ_BLDGS);
   memcpy(pOutbuf+OFF_AIR_COND, pChar->Cooling, SIZ_AIR_COND);
   memcpy(pOutbuf+OFF_HEAT, pChar->Heating, SIZ_HEAT);
   memcpy(pOutbuf+OFF_GAR_SQFT, pChar->GarSqft, SIZ_GAR_SQFT);
   memcpy(pOutbuf+OFF_PARK_TYPE, pChar->ParkType, SIZ_PARK_TYPE);

   // Bug fix
   memset(pOutbuf+OFF_PARK_SPACE, ' ', SIZ_PARK_SPACE);
   memcpy(pOutbuf+OFF_PARK_SPACE, pChar->ParkSpace, SIZ_CHAR_SIZE4);

   memcpy(pOutbuf+OFF_VIEW, pChar->View, SIZ_VIEW);
   memcpy(pOutbuf+OFF_ROOF_TYPE, pChar->RoofType, SIZ_ROOF_TYPE);
   memcpy(pOutbuf+OFF_ROOF_MAT, pChar->RoofMat, SIZ_ROOF_MAT);
   *(pOutbuf+OFF_WATER) = pChar->Water;
   *(pOutbuf+OFF_SEWER) = pChar->Sewer;
   *(pOutbuf+OFF_POOL)  = pChar->Pool[0];

   // Read next record
   pRec = fgets(acRec, 1024, fdChar);

   lCharMatch++;
  
   return 0;
}

/******************************* MergeStdChar ********************************
 *
 * Merge STDCHAR char into current R01 record
 *
 *****************************************************************************/

int MergeStdChar(char *pCnty)
{
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acAttrFile[_MAX_PATH];
   char     acBuf[MAX_RECSIZE];

   HANDLE   fhIn, fhOut;

   int      iRet, iUpdCnt;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   // Load tables
   iRet = LoadLUTable((char *)&acLookupTbl[0], "[Quality]", NULL, MAX_ATTR_ENTRIES);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
      return 1;
   }

   sprintf(acAttrFile, acAttrTmpl, pCnty, "DAT");
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "T01");

   // Open Char file
   LogMsg("Open Characteristic file %s", acAttrFile);
   fdChar = fopen(acAttrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("*** Error opening Char file: %s\n", acAttrFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("*** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("*** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record 
   memset(acBuf, ' ', iRecLen);
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iUpdCnt=iNoMatch=lCharSkip=lCharMatch=0;
   cFileCnt = 1;
   while (ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         // EOF
         /*
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         acOutFile[strlen(acOutFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            CloseHandle(fhIn);
            CloseHandle(fhOut);
            fhIn = fhOut = 0;

            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }

            if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, 
                NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
            {
               iRet = -4;
               fhOut = 0;
               break;
            }

            if (!ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
               break;
         } else
         */
            break;
      }

      // Update CHAR
      if (fdChar)
         if (!MergeStdCharRec(acBuf))
            iUpdCnt++;

      // Write output record
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fhOut)
      CloseHandle(fhOut);
   if (fdChar)
      fclose(fdChar);

   // Rename outfile
   LogMsg("Rename output file");
   sprintf(acBuf, acRawTmpl, pCnty, pCnty, "M01");
   if (!_access(acBuf, 0))
      remove(acBuf);
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   rename(acRawFile, acBuf);
   iRet = rename(acOutFile, acRawFile);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total matched records:      %u", iUpdCnt);

   if (!iRet)
      iRet = iUpdCnt;
   return iRet;
}
