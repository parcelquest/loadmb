/**************************************************************************
 *
 * Notes: 
 *    1) In AMA website, PP_MH is called Personal Property and BUS_PP is Business Property.
 *       So we will call the same thing here.
 *    2) New layout for 2011 LDR.  New functions added.
 *
 * Revision
 * 09/01/2005 1.0.0    First version by Sony Nguyen
 * 05/23/2006 1.5.13.1 Fix Ama_MergeSitus() to remove '/' and leading '0' in street name
 * 11/29/2006 1.7.5    Fix Quality bug that leaves junk in output file.
 * 01/29/2007 1.8.2.2  Fix OtherVal algorithm to include Fixtures.
 * 04/20/2007 1.9.4.1  Fix tax amount for the unsecured from 800-870
 * 07/19/2007 1.9.8    Fixup for 2007 LDR.  County now send TR601 file on 
 *                     lien date.  New functions are created for this.
 * 07/30/2007 1.9.10   Fix Ama_MergeLienMAdr() to remove null char from mail state 
 *                     and Ama_MergeChar() to add POOL code to output
 * 01/25/2008 1.10.3.1 Add code to support standard usecode
 * 02/29/2008 1.10.5   Add code to output update records.
 * 05/08/2008 1.10.9.1 Create Ama_Load_Roll() to replace MergeAmaRoll().  This 
 *                     function will create new R01 then merge LDR extract.
 * 07/18/2008 8.2.0    Adding Ama_ConvertChar() to support new CHAR format from AMA.
 * 08/26/2008 8.3.3    Modify Ama_MergeOwner() to store Name1 as come from the county.
 * 01/29/2009 8.5.6    Add option to clear CHARS and modify Ama_ConvertChar() to ignore
 *                     records that has FeeParcel != Asmt.
 * 03/10/2009 8.6.3    Blank out Situs Addr before update.
 * 03/30/2009 8.7.1    Use MB_MergeSale() to do sale update instead of MergeSale3() to fix 
 *                     problem that non-sale transaction overwrites sale transaction that  
 *                     occurs in the same day  since we only keep one transaction per day.
 * 05/21/2009 8.7.6    Remove reference to OldApn & NewApn files.  We no longer need them.
 * 07/29/2009 9.1.4    Add new Ama_MergeMAdr() to replace Ama_MergeLienMAdr().
 * 07/05/2010 10.0.1   Use updateLegal() to populate long legal.
 * 08/07/2010 10.3.1   Remove '-' from FEEPARCEL in LDR file.
 * 11/03/2010 10.3.7   Add TaxCode to R01 record.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 03/15/2011 10.5.6   Add new data field STORIES/NUMFLOORS.
 * 04/21/2011 10.7.1   Add -Xs option to create cum sale file.
 * 06/01/2011 10.9.0   Exclude HomeSite from OtherVal.
 *                     Replace Ama_MergeExe() and Ama_MergeTax() with MB_Merge???() version.
 * 07/26/2011 11.2.3   Add S_HSNO. Merge situs using monthly file when loading LDR.
 *                     Modify Ama_Load_LDR() and Ama_MergeLien() to support new data format.
 * 08/01/2011 11.2.4.1 Set HO_FL='2' if no record found in EXE file.
 * 09/01/2011 11.3.3   Modify all Merge*() functions to support new file layout.  Replace
 *                     ConvertChar() with ExtrChar(), MB_MergeTax() with MB_MergeTaxG2().
 *                     Use ApplyCumSale() to update sales instead of MergeSales(). Modify
 *                     MergeRoll() to skip unsecured parcels.
 * 09/06/2011 11.3.3.1 Modify QualityClass parsing algorithm in Ama_MergeChar()
 * 09/08/2011 11.3.4   Adding LotAcres and LotSqft from CHAR file.
 * 10/20/2011 11.7.5   Change logic to accept both old (.csv) and new (.txt) data format.
 *                     To use new data, just uncomment following entries in INI file:
 *                     NewTax, Delimiter, HdrRows, SkipQuote, and all .txt input file.
 * 04/04/2012 11.14.1  Set AG and TIMBER preserve based on Usecode.  Remove sale update
 *                     from Ama_Load_LDR(). It's now using ApplyCumSale().
 * 05/21/2012 11.14.3  Add option to load tax file into tax database.
 * 07/09/2012 12.1.2   Modify Ama_MergeSitus() to add zip to R01.
 * 07/21/2012 12.2.2   Fix Ama_Load_LDR() to use extracted Ama_Char.dat file instead of Ama_Char.txt.
 *                     Fix Ama_MergeMAdr() to merge CAREOF correctly. 
 * 08/02/2012 12.2.3   Update Colling & Heating table to match with Ama_PCCode.csv
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 *.06/28/2013 13.0.3   Add Ama_MergeCurRoll() to update transfer using data from current roll file.
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 11/08/2013 13.18.1  Modify asHeating, asCooling, and asPool to match with Ama_PCCode.txt.
 * 01/12/2014 13.19.0  Update DocCode[] table. Remove sale update from Ama_Load_Roll() and 
 *                     use ApplyCumSale() to update sale history. 
 * 02/13/2014 13.20.3  Update Ama_DocCode[]
 * 04/04/2014 13.22.4  Add ConvStdChar(), modify MergeChar() to use data from new char file.
 * 04/07/2014          Fix YrBlt & YrEff in ConvStdChar() & MergeChar().
 * 04/08/2014          Fix Garage issue.
 * 04/16/2014 13.22.5  Modify asView[] to have it in sorted order.
 * 04/28/2014 13.23.1  Fix bug in ConvStdChar() and add LotSqft to MergeChar()
 * 07/10/2014 14.0.2   Modify Ama_MergeLien(), Ama_Load_LDR(), add Ama_ExtrLien() and Ama_CreateLienRec()
 *                     to support new LDR format.
 * 01/02/2015 14.11.2  Modify Ama_MergeMAdr() to use the same parseMAdr1() in both functions.
 * 07/03/2015 15.0.3   Fix bug in Ama_Load_LDR() and Ama_ExtrLien() when input file is unicode.
 * 07/31/2015 15.2.0   Add DBA to Ama_MergeMAdr()
 * 10/13/2015 15.4.0   Modify call to MB_Load_TaxBase() to import data into Tax_Base & Tax_Owner tables.
 * 02/14/2016 15.12.0  Modify Ama_MergeMAdr() to fix overwrite bug.
 * 04/04/2016 15.14.2  Resort asParkType[] and use findXlatCodeA() on asHeating[].
 * 07/13/2016 16.0.4   Modify Ama_MergeSitus() to fix strtype "HILL" which should be part of strname.
 *                     Add Ama_MergeLien3() and Ama_Load_LDR3() to support new layout of LDR 2016
 *                     Add option to use TC601 to update value in Load_LDR3()
 * 08/14/2016 16.3.0   Replace MB_MergeTaxG2() with MB_MergeTaxG1().
 * 02/16/2017 16.9.3   Add -Xv option to extract value using LIEN_EXP.AMA for input values
 * 03/20/2017 16.13.1  Use MB_ResetPVReason() to reset reason based on specific requirement
 * 06/16/2017 16.19.0  Modify call to MB_Load_TaxCode() not to update BillNum using TaxBase info.
 * 06/29/2017 17.0.3   Modify Ama_MergeLien3() & Ama_Load_LDR3() for LDR2017.  Fix bug in loadAma()
 *                     not to use acValueFile when extracting value.
 * 06/28/2018 18.0.0   Modify Ama_MergeLien3() to handle different APN format receiving from county.
 *                     2018 LDR is in number format, not string, so watch out for scientific notation.
 * 07/09/2018 18.1.1   Fix ALT_APN format in Ama_MergeLien3().  Modify Ama_Load_LDR3() to process wothout
 *                     sorting input file because it's already sorted. This in turn fixed the verification issue.
 * 09/12/2018 18.5.1   Add TAX_UPDATING option -Ut to update tax paid only
 * 11/30/2018 18.7.7   Modify Ama_Load_Roll() to use assessed values from daily update, not from LDR per county request.
 * 12/31/2018 18.9.1   Modify Ama_ConvStdChar() to apply previous APN to records without APN.
 * 01/04/2019 18.9.3   Modify Ama_ConvStdChar() to ignore records without APN.
 * 05/10/2019 18.11.6  Modify Ama_MergeMAdr() to populate M_ADDR_D & M_CTY_ST_D with M_Addr1 & M_Addr2
 *                     when M_Addr is blank.  This is usually foreign addresses.
 * 06/26/2019 19.0.1   Remove hyphen from TRA in Ama_MergeLien3()
 * 07/20/2019 19.1.0   Remove duplicate code in MergeLien3().
 * 10/31/2020 20.4.2   Modify Ama_MergeRoll() & Ama_MergeStaChar() to populate default PQZoning.
 * 05/05/2021 20.8.6   Fix lookup code in asCond[].
 * 07/02/2021 20.0.4   Add QualityClass to R01 in Ama_MergeStdChar(). Fix M_ZIP4 in Ama_MergeMAdr().
 *                     Add AgPreserved to R01 in Ama_MergeLien3().
 * 02/15/2023 22.6.7.2 Modify Ama_ConvStdChar() to add a special case of QualityClass.
 * 07/10/2023 23.0.5   Add Ama_MergeLien31() to support new LDR file format. Filter out LotSqft > 999999999.
 * 03/20/2024 23.8.1   Modify ConvStdChar() To populate LOT_ACRES using LOT_SQFT.
 * 07/01/2024 24.0.0   Modify Ama_MergeLien3() to add ExeType.
 *                     
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "CharRec.h"
#include "MB_Value.h"
#include "LoadValue.h"
#include "SqlExt.h"

#define _AMADOR_     1
#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "PQ.h"
#include "MBExtrn.h"
#include "MergeAma.h"
#include "Tax.h"

static XLAT_CODE  asHeating[] = 
{
// Value, lookup code, value length
   "01", "Z", 2,               // Central
   "02", "F", 2,               // Electric baseboard
   "03", "G", 2,               // Heat Pump
   "04", "D", 2,               // Wall heater
   "05", "Y", 2,               // Oil furnace
   "06", "K", 2,               // Solar
   "07", "Y", 2,               // Kerosene heater
   "08", "C", 2,               // Floor unit
   "09", "X", 2,               // Unknown
   "10", "E", 2,               // Hydronic heat
   "1" , "F", 1,               // Electric
   "2" , "N", 1,               // Propane
   "3" , "Y", 1,               // Kerosene
   "4" , "S", 1,               // Wood burning
   "5" , "S", 1,               // Wood Pellets
   "6" , "I", 1,               // Radiant heat
   "99", "Q", 2,               // Structure
   "",   "",  0
};

static XLAT_CODE  asCooling[] = 
{
// Value, lookup code, value length
   "01", "C", 2,               // Central
   "02", "E", 2,               // Evaporative Cooler
   "03", "F", 2,               // Attic Fan
   "04", "L", 2,               // Wall Unit
   "05", "X", 2,               // Unknown
   "06", "N", 2,               // None
   "07", "F", 2,               // Whole house Fan 
   "08", "X", 2,               // Multiple type
   "99", "Y", 2,               // Building
   "",   "",  0
};

// Values are verified via Ama_PCCode.txt
static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "01", "B", 2,               // Inground
   "02", "X", 2,               // Above-ground
   "03", "V", 2,               // Vinyl
   "04", "F", 2,               // Fiberglass
   "05", "P", 2,               // Other
   "99", "P", 2,               // Pool
   "",   "",  0
};

static XLAT_CODE  asParkType[] =
{
   // Value, lookup code, value length
   "01", "I", 2,               // ATTACHED
   "02", "L", 2,               // DETACHED
   "03", "E", 2,               // BASEMENT
   "04", "C", 2,               // CARPORT
   "05", "2", 2,               // Multiple
   "06", "Z", 2,               // Other
   "07", "B", 2,               // BUILT-IN
   "99", "Z", 2,               // Other
   "AT", "I", 2,               // ATTACHED
   "BA", "E", 2,               // BASEMENT
   "D",  "L", 1,               // DETACHED
   "N",  "H", 1,               // None
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "01", "U", 2,               // TYPE UNKNOWN
   "02", "W", 2,               // WOOD STOVE
   "03", "I", 2,               // FIREPLACE INSERT
   "04", "P", 2,               // PATENTED CHIMNEY
   "05", "G", 2,               // GAS FP/STOVE
   "06", "F", 2,               // FREESTANDING
   "07", "C", 2,               // CIRCULAR
   "08", "S", 2,               // PELLET STOVE
   "09", "Z", 2,               // ZERO CLEARANCE
   "10", "M", 2,               // MULTIPLE
   "11", "O", 2,               // OTHER
   "12", "O", 2,               // OTHER
   "99", "O", 2,               // OTHER
   "",   "",  0
};

static XLAT_CODE  asView[] =
{
   "09","Y", 2,               // VIEW OF OWN PROPERTY
   "1", "Y", 1,               // View Code
   "2", "N", 1,               // NO VIEW
   "3", "1", 1,               // MINIMAL VIEW
   "4", "4", 1,               // GOOD-LOCAL AREA
   "5", "M", 1,               // Golfcourse
   "6", "R", 1,               // VALLEY VIEW
   "7", "T", 1,               // MOUNTAIN VIEW
   "8", "O", 1,               // PANORAMIC VIEW
   "9", "Y", 1,               // VIEW OF OWN PROPERTY
   "11","Y", 2,               // FILTERED
   "12","Y", 2,               // POSSIBLE
   "",  "",  0
};

static XLAT_CODE  asSewer[] =
{
   // Value, lookup code, value length
   "01", "S", 2,               // SEPTIC TANK
   "02", "L", 2,               // COMM LEACHFIELD
   "03", "A", 2,               // COMMUNITY SEWER
   "04", "S", 2,               // ENG SEPTIC
   "05", "S", 2,               // ALT. SEPTIC
   "09", "X", 2,               // NEEDS SEPTIC
   "17", "U", 2,               // UNKNOWN
   "19", "Y", 2,               // OTHER
   "99", "Y", 2,               // OTHER
   "",   "",  0
};

static XLAT_CODE  asCond[] =
{
   // Value, lookup code, value length
   "01", "G", 2,               // Good
   "02", "A", 2,               // Average
   "03", "F", 2,               // Fair
   "04", "P", 2,               // Poor
   "05", "U", 2,               // Unfinished
   "06", "R", 2,               // Remodelled
   "07", " ", 2,               // ?
   "08", " ", 2,               // ?
   "",   "",  0
};

static XLAT_CODE  asWaterSrc[] = 
{
   // Value, lookup code, value length
   "01", "P", 2,               // Community
   "02", "W", 2,               // Well
   "03", "S", 2,               // Spring
   "04", "Y", 2,               // Other
   "05", "R", 2,               // Shared
   "09", "F", 2,               // Canal
   "99", "Y", 2,               // Rural land water (irrigation water)
   "",   "",  0
};

IDX_TBL5 AMA_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "01", "1 ", 'N', 2, 2,     // SALE - 100% TRANSFER
   "02", "57", 'N', 2, 2,     // PARTIAL INTEREST TRANSFER
   "03", "74", 'Y', 2, 2,     // NON-REAPPRAISABLE EVENT
   "04", "18", 'Y', 2, 2,     // INTERMARITAL
   "05", "51", 'Y', 2, 2,     // VESTING/NAME CHANGE
   "06", "61", 'Y', 2, 2,     // INTO TRUST
   "07", "61", 'Y', 2, 2,     // OUT OF TRUST
   "08", "6 ", 'Y', 2, 2,     // AFFIDAVIT OF DEATH-NON PROP58/SPOUSAL
   "09", "3 ", 'Y', 2, 2,     // ADD/DELETE JOINT TENANT
   "10", "9 ", 'N', 2, 2,     // CORRECTION DEED
   "11", "80", 'N', 2, 2,     // JUDGEMENT/ORDER
   "12", "77", 'N', 2, 2,     // TRUSTEE'S DEED/FORECLOSURE
   "13", "79", 'N', 2, 2,     // ESTATE DISTRIBUTION
   "14", "74", 'Y', 2, 2,     // PROP 58
   "15", "74", 'Y', 2, 2,     // RES SWIMMING POOL/SPA
   "16", "74", 'Y', 2, 2,     // RES SFD ADDITION
   "17", "74", 'Y', 2, 2,     // RES GARAGE
   "18", "74", 'Y', 2, 2,     // RES MISC - SHED
   "19", "74", 'Y', 2, 2,     // SFD
   "20", "74", 'Y', 2, 2,     // RES REMODEL
   "21", "74", 'Y', 2, 2,     // RES REPAIRS AND MAINTENANCE
   "22", "74", 'Y', 2, 2,     // HVAC
   "23", "74", 'Y', 2, 2,     // ELECTRIC
   "24", "74", 'Y', 2, 2,     // RES REROOF
   "25", "74", 'Y', 2, 2,     // RES GASLINE
   "26", "74", 'Y', 2, 2,     // RES PATIO
   "27", "74", 'Y', 2, 2,     // NEW MULTIPLE BLDG
   "28", "74", 'Y', 2, 2,     // ADD'N/CONV TO MULTIPLE
   "29", "74", 'Y', 2, 2,     // MISC MULTIPLE
   "30", "74", 'Y', 2, 2,     // MANUFACTURED HOME
   "31", "74", 'Y', 2, 2,     // TENANT IMPROVEMENT
   "32", "74", 'Y', 2, 2,     // SIGN
   "33", "74", 'Y', 2, 2,     // RURAL - NEW BLDG
   "34", "74", 'Y', 2, 2,     // RURAL BLDG ADD'N
   "35", "74", 'Y', 2, 2,     // RURAL MISC
   "36", "74", 'Y', 2, 2,     // VINEYARD ADD'N/DELETION
   "37", "74", 'Y', 2, 2,     // GRADING & LAND IMPS
   "38", "74", 'Y', 2, 2,     // NEW MH
   "39", "74", 'Y', 2, 2,     // MISC - ALL OTHER
   "40", "74", 'Y', 2, 2,     // SPLIT/COMBO
   "44", "74", 'Y', 2, 2,     // NEW INDUSTRIAL BLDG
   "45", "74", 'Y', 2, 2,     // INDUSTRIAL ADDN/CONV
   "46", "74", 'Y', 2, 2,     // INDUSTRIAL MISCELLANEOUS
   "50", "74", 'Y', 2, 2,     // DEMO/REMOVAL
   "53", "74", 'Y', 2, 2,     // COMMERCIAL - NEW BUILDING
   "54", "74", 'Y', 2, 2,     // COMMERCIAL ADDN/CONV
   "55", "74", 'Y', 2, 2,     // COMMERCIAL MISC
   "56", "74", 'Y', 2, 2,     // NEW COMM'L BLDG
   "58", "74", 'Y', 2, 2,     // NEW COMM'L BLDG
   "63", "74", 'Y', 2, 2,     // AG CONTRACT-NEW BLDG
   "64", "74", 'Y', 2, 2,     // AG CONTRACT-ADD'N/CONV
   "65", "74", 'Y', 2, 2,     // AG CONTRACT-MISC
   "66", "74", 'Y', 2, 2,     // AG CONTRACT- VINES INC/DEC
   "67", "74", 'Y', 2, 2,     // AG CONTRACT LAND IMPS
   "70", "74", 'Y', 2, 2,     // LIEN DATE UPDATE
   "71", "74", 'Y', 2, 2,     // PROP 8
   "72", "74", 'Y', 2, 2,     // CALAMITY-APPLICATION
   "73", "74", 'Y', 2, 2,     // CALAMITY-RESTORATION
   "74", "74", 'Y', 2, 2,     // ASSESSMENT APPEALS
   "75", "74", 'Y', 2, 2,     // SOLAR IMPROVEMENT
   "80", "74", 'Y', 2, 2,     // NON-TAXABLE GOVERNMENT PROP
   "81", "74", 'Y', 2, 2,     // TAXABLE GOVERNMENT LAND
   "95", "74", 'Y', 2, 2,     // CLEAR TITLE
   "96", "74", 'Y', 2, 2,     // DEMO/REMOVAL
   "97", "74", 'Y', 2, 2,     // CREATE LIFE ESTATE
   "98", "74", 'Y', 2, 2,     // REVOKE LIFE ESTATE
   "99", "74", 'Y', 2, 2,     // DOMESTIC PARTNER
   "GD", "1 ", 'N', 2, 2,     // GRANT DEED
   "TD", "27", 'Y', 2, 2,     // TRUST DEED
   "","",0,0,0
};

/******************************** Ama_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Ama_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64], acOwner[64];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.' || *pTmp == '\"' || *pTmp == '\'')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acName2[0] = 0;
   acSave1[0] = 0;
   
   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Save owner name
   strcpy(acOwner, acTmp);

   if (pTmp=strstr(acTmp, " 1/2 INT"))
      *pTmp = 0;
   
   // Check for year that goes before TRUST
   iTmp =0;   
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp1 = strlen(acTmp);
      if (iTmp1 > SIZ_NAME1) iTmp1 = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp1);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp1);
      return;
   }

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
      // If TRUST appears after number, save from number forward
      if (strstr((char *)&acTmp[iTmp], " TRUST") )
      {
         iTmp--;
         strcpy(acSave1, (char *)&acTmp[iTmp]);
         acTmp[iTmp] = 0;
      }
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      //if (pTmp1 = strchr(pTmp, ';'))
      //   *pTmp1 = 0;

      //strcpy(acOwner, acTmp);
      //strcpy(acName2, pTmp);
      //if ((pTmp=strstr(acName2, " ETAL")) || (pTmp=strstr(acName2, " ET AL")) )
      //   *pTmp = 0;
   } else if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Filter out words, things in parenthesis 
   // MONDANI NELLIE M ESTATE OF
   if ( (pTmp = strchr(acTmp, '(')) || (pTmp=strstr(acTmp, " TRUSTE"))  ||
      (pTmp=strstr(acTmp, " ESTATE OF")) || (pTmp=strstr(acTmp, " LIFE ESTATE")) ||
      (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTS OF")) || (pTmp=strstr(acTmp, " TR OF")) ||
      (pTmp=strstr(acTmp, " ET AL"))  || (pTmp=strstr(acTmp, " ETAL"))  || (pTmp=strstr(acTmp, " TRET AL")))
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " CO-TR")) || (pTmp=strstr(acTmp, " CPA/EA")))
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " SUCCS-TR")) || (pTmp=strstr(acTmp, " SUCCESSOR")) || 
      (pTmp=strstr(acTmp, " SURVIVOR")) )
   {
      *pTmp = 0; 
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FAMILY TRUST &"))
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      // RUMAN RICKY L & RUMAN FAMILY TRUST &
      // RUMAN RICKY L & NANCY FAMILY TRUST & MARIE
      // VERMETTE JAMES & THERESA A & ZENDER ROBERT A &
      //strcpy(acSave1, " FAMILY TRUST");
      //strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST &"))
   {  // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      //strcpy(acSave1, " LIVING TRUST");
      //strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      //strcpy(acSave1, " REV TRUST");
      //strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE  
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      *(pTmp+6) = 0;
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
      {
         //strcpy(acSave1, pTmp1);
         *pTmp1 = 0;
      } else
      {
         //strcpy(acSave1, pTmp);
         *pTmp = 0;
      }

      strcpy(acName1, acTmp);
      //strcpy(acName2, pTmp+9);
   } else if (pTmp=strstr(acTmp, " LAND TRUST"))
   {
      strcpy(acSave1, " LAND TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " FAMILY "))
             || (pTmp=strstr(acTmp, " INCOME TR")) || (pTmp=strstr(acTmp, " LIVING ")))
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      //strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      //strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " ESTATE OF"))
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   //if (pTmp = strstr(acSave1, " THE"))
   //   *pTmp = 0;

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      // Avoid 1/2 INT
      // Only split if multi words appear before '/'
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp = 0;
         if (!(pTmp1 = strchr(acName1, ' ')) || strstr(pTmp+1, " INC") || strstr(pTmp+1, " LLC"))
            *pTmp = '/';
         //strcpy(acName2, pTmp);
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;
   //if ((pTmp=strstr(acName2, " AS ")) || (pTmp=strstr(acName2, " AKA ")) )
   //   *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         //if (*pTmp1 == ' ') pTmp1++;
         //strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(myTrim(acName1), ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      strcpy(acTmp1, myOwner.acName1);
      //if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
      //{
      //   iTmp = strlen(myOwner.acName2);
      //   if (iTmp > SIZ_NAME2)
      //      iTmp = SIZ_NAME2;
      //   memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, iTmp);
      //}
      if (myOwner.acVest[0] > ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // Concat what in saved buffer
      if (acSave1[0])
      {
         if (acTmp1[strlen(acTmp1)-1] == ' ' && acSave1[0] == ' ')
            strcat(acTmp1, (char *)&acSave1[1]);      // avoid double blank
         else
            strcat(acTmp1, acSave1);
      }

      // Save Name1
      //iTmp = strlen(acTmp1);
      //if (iTmp > SIZ_NAME1)
      //   iTmp = SIZ_NAME1;
      //memcpy(pOutbuf+OFF_NAME1, acTmp1, iTmp);

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
      {
         iTmp = strlen(acTmp1);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, iTmp);
      } else
      {
         iTmp = strlen(myOwner.acSwapName);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
      }
   } else
   {
      if (acSave1[0])
      {
         strcat(acName1, acSave1);
         acSave1[0] = 0;
         iTmp = strlen(acName1);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         //memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);
         memcpy(pOutbuf+OFF_NAME_SWAP, acName1, iTmp);
      } else
      {
         // Couldn't split names
         iTmp = strlen(acOwner);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         //memcpy(pOutbuf+OFF_NAME1, acOwner, iTmp);
         memcpy(pOutbuf+OFF_NAME_SWAP, acOwner, iTmp);
         acName2[0] = 0;
      }
   }

   // Process Name2 when there is more than one word
   /*
   if (acName2[0] && strchr((char *)&acName2[1], ' ') && myOwner.acName2[0] <= ' ')
   {
      if ((pTmp=strstr(acName2, " REVOC")) || (pTmp=strstr(acName2, " FAMILY "))
         || (pTmp=strstr(acName2, " INCOME TR")) || (pTmp=strstr(acName2, " LIVING TR")) )
      {
         iTmp = strlen(acName2);
         if (iTmp > SIZ_NAME2)
            iTmp = SIZ_NAME2;
         memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);
      } else
      {
         //acSave1[0] = 0;
         //if (pTmp=strstr(acName2, " TRUST"))
         //{
         //   strcpy(acSave1, pTmp);
         //   *pTmp = 0;
         //}

         //iRet = splitOwner(acName2, &myOwner, 3);
         //if (iRet >= 0)
         //   strcpy(acName2, myOwner.acName1);

         //if (acSave1[0])
         //   strcat(acName2, acSave1);

         blankRem(acName2);
         iTmp = strlen(acName2);
         if (iTmp > SIZ_NAME2)
            iTmp = SIZ_NAME2;
         memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);
      }
   }

   if (acName2[0])
   {
      blankRem(acName2);
      iTmp = strlen(acName2);
      if (iTmp > SIZ_NAME2)
         iTmp = SIZ_NAME2;
      memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);
   }
   */

   // Store name1 as is
   iTmp = strlen(acOwner);
   if (iTmp > SIZ_NAME1)
      iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acOwner, iTmp);

}

/******************************** Ama_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Ama_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   } 

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   iTmp = blankRem(acAddr1);
   if (acAddr1[0] <= ' ')
   {
      strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR1]);
      iTmp = blankRem(acAddr1);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, iTmp);
      sprintf(acTmp, "%s %s", apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3]);
      iTmp = blankRem(acTmp);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
      return;
   } else
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "044372009", 9))
   //   iTmp = 0;
#endif

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      memcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], strlen(apTokens[MB_ROLL_M_CITY]));
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         remChar(apTokens[MB_ROLL_M_ZIP], '-');
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], 9);
      }

      if (*(pOutbuf+OFF_M_ZIP4) > ' ')
         sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], pOutbuf+OFF_M_ZIP, pOutbuf+OFF_M_ZIP4);
      else
         sprintf(acTmp, "%s %s %.5s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], pOutbuf+OFF_M_ZIP);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

/******************************** Ama_MergeLienMAdr ******************************
 *
 * Merge Lien Mail address
 *
 *****************************************************************************/

void Ama_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          !_memicmp(pLine1, "DBA ", 4) ||
          !_memicmp(pLine1, "FBO ", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          !_memicmp(pLine1, "DBA ", 4) ||
          !_memicmp(pLine1, "FBO ", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         p1 = pLine2;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
         {
            p1 = pLine1;
         } else
         {
            // If last word of line 3 is number and first word of line 1 is alpha, 
            // line 1 more likely be CareOf
            p0 = pLine1;
            p1 = pLine2;
         }
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
      updateCareOf(pOutbuf, p0, strlen(p0));

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      // 1/2/2015 - Should use same function as in roll update
      parseMAdr1(&sMailAdr, acAddr1);
      //parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "029022024000", 9))
   //   iTmp = 0;
#endif

   strcpy(acAddr2, p2);
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, strlen(sMailAdr.City));

      // State
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, strlen(sMailAdr.State));

      // Zipcode
      memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, strlen(sMailAdr.Zip));
      memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, strlen(sMailAdr.Zip4));
   }
}
void Ama_MergeFmtMAdr(char *pOutbuf, char *pAddr, char *pCity, char *pState, char *pZip)
{
   char    acTmp[256], acAddr1[64], *pTmp;
   int     iTmp;
   ADR_REC sMailAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "131250011000", 10))
   //   iTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, false, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Mail address
   strcpy(acAddr1, pAddr);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }

   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
      sMailAdr.strName[SIZ_M_STREET] = 0;
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*pCity > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, pCity, SIZ_M_CITY);
      if (2 == strlen(pState))
         memcpy(pOutbuf+OFF_M_ST, pState, 2);

      if (*pZip >= '0')
      {
         // Take out '-' in zipcode if present
         strcpy(acTmp, pZip);
         iTmp = strlen(acTmp);
         if (acTmp[5] == '-' && iTmp == 10)
         {
            strcpy(&acTmp[5], &acTmp[6]);
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         } else if (iTmp > 9)
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         else
            vmemcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
      } else
         iTmp = 0;

      if (iTmp == 9)
         sprintf(acTmp, "%s %s %.5s-%.4s", pCity, pState, pZip, pZip+5);
      else
         sprintf(acTmp, "%s %s %s", pCity, pState, pZip);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

/******************************** Ama_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Ama_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acStreet[32], acAddr1[256], *pTmp, *apItems[MAX_FLD_TOKEN];
   long     lTmp;
   int      iRet=0, iTmp;

   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSitus);
      pRec = fgets(acRec, 512, fdSitus);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001110003000", 9) || !memcmp(pOutbuf, "007113047", 9))
   //   iTmp = 0;
#endif

   do 
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF 
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec: %s", pRec);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apItems);
   else
      iRet = ParseStringIQ(pRec, cDelim, MB_SITUS_SEQ+1, apItems);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old data
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apItems[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);

      /*
      strcpy(acAddr1, apItems[MB_SITUS_STRNUM]);
      strcat(acAddr1, " ");

      if ((pTmp = strchr(apItems[MB_SITUS_STRNUM], ' ')) && (*(pTmp+1) != '&'))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));
      */
      strcpy(acAddr1, apItems[MB_SITUS_STRNUM]);
      iTmp = blankRem(acAddr1);

      // Save original StrNum
      if ((pTmp = strchr(acAddr1, '-')) && (*(pTmp+1) >= ' '))
      {
         // Remove all space in between
         iTmp = remChar(acAddr1, ' ');
         memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);
      } else
      {
         if (pTmp = strchr(acAddr1, ' '))
         {
            if (acAddr1[iTmp-1] == '&')
               acAddr1[iTmp-1] = ' ';
            else
               replStr(acAddr1, "AND", "&");
            memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
         } else
            memcpy(pOutbuf+OFF_S_HSENO, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
      }   

      strcat(acAddr1, " ");
      if (*apItems[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apItems[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apItems[MB_SITUS_STRDIR], strlen(apItems[MB_SITUS_STRDIR]));
      }
   }

   if (*apItems[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, apItems[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, apItems[MB_SITUS_STRTYPE]);

      strcpy(acStreet, apItems[MB_SITUS_STRNAME]);

      // Remove '/' in street name
      if (pTmp=strchr(acStreet, '/'))
         *pTmp = 0;

      // Remove leading '0' in street name
      if (pTmp=strstr(acStreet, " 0"))
      {
         pTmp++;
         strcpy(pTmp, pTmp+1);
      }

      // Special case where county put part of strname into strtype
      if (!memcmp(apItems[MB_SITUS_STRTYPE], "HILL", 4))
      {
         strcat(acStreet, " HILL");
         *apItems[MB_SITUS_STRTYPE] = 0;
      }
      vmemcpy(pOutbuf+OFF_S_STREET, acStreet, SIZ_S_STREET);

      if (*apItems[MB_SITUS_STRTYPE] >= 'A')
      {
         iTmp = GetSfxCodeX(apItems[MB_SITUS_STRTYPE], acTmp);
         if (iTmp > 0)
            Sfx2Code(acTmp, acCode);
         else
         {
            LogMsg0("*** Invalid suffix: %s, APN=%.12s", apItems[MB_SITUS_STRTYPE], pOutbuf);
            iBadSuffix++;
            memset(acCode, ' ', SIZ_S_SUFF);
         }
         memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      }
   } else
   {
      ADR_REC sAdr;

      strcpy(acTmp, apItems[MB_SITUS_STRNAME]);

      // Remove '/' in street name
      if (pTmp=strchr(acTmp, '/'))
         *pTmp = 0;

      // Remove leading '0' in street name
      if (pTmp=strstr(acTmp, " 0"))
      {
         pTmp++;
         strcpy(pTmp, pTmp+1);
      }

      parseAdr1S(&sAdr, acTmp);
      if (sAdr.strName[0] > ' ')
      {
         blankPad(sAdr.strName, SIZ_S_STREET);
         memcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      }
      if (sAdr.strSfx[0] > '0')
         memcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, strlen(sAdr.strSfx));

      strcat(acAddr1, apItems[MB_SITUS_STRNAME]);
   }

   if (*apItems[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apItems[MB_SITUS_UNIT]);
      memcpy(pOutbuf+OFF_S_UNITNO, apItems[MB_SITUS_UNIT], strlen(apItems[MB_SITUS_UNIT]));
   }

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

   // Situs city
   if (*apItems[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apItems[MB_SITUS_COMMUNITY], acTmp, acAddr1);   
      blankPadz(acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
      {
         // Zip
         lTmp = atol(apItems[MB_SITUS_ZIP]);
         if (lTmp > 10000 && lTmp < 99999)
            memcpy(pOutbuf+OFF_S_ZIP, apItems[MB_SITUS_ZIP], 5);

         iTmp = sprintf(acTmp, "%s CA %.5s", myTrim(acAddr1), pOutbuf+OFF_S_ZIP);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
      }
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/******************************** Ama_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************

int Ama_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdSales);
      // Get first rec
      pRec = fgets(acRec, 512, fdSales);
   }

   do 
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg("Skip Sale rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdSales);
         lSaleSkip++;
      }
   } while (iTmp > 0);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "020430013000", 9))
   //   iRet = 0;
#endif
   while (!iTmp)
   {
      // Replace tab char with 0
      if (pRec)
         iRet = ParseStringNQ(pRec, ',', MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ' &&
          memcmp(apTokens[MB_SALES_DOCNUM], "19  R ", 6) )
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            // Doc date
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         MB_MergeSale(&sCurSale, pOutbuf, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSales);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Ama_MergeChar ******************************
 *
 * Note: need code table for Heating and Colling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

int Ama_MergeStdChar(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   long     lTmp;
   int      iRet, iLoop, iBeds, iFBath, iHBath, iTmp;
   STDCHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 1024, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "044510005", 9))
   //   iRet = 0;
#endif

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   // Quality Class
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Impr condition
   *(pOutbuf+OFF_IMPR_COND) = pChar->ImprCond[0];

   // Yrblt
   long lYrBlt = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lYrBlt > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // YrEff
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 1900 && lTmp >= lYrBlt)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_EFF);
   else
      memcpy(pOutbuf+OFF_YR_EFF, BLANK32, SIZ_YR_EFF);

   // LotSqft
   long lLotSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lLotSqft > 100 && lLotSqft < 999999999)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_ACRES);
   } 

   // BldgSqft
   long lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 100)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   long lSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   } else
   {
      memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);
   }

   // Park type
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // Heating
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   
   // Cooling 
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Total rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   } else
      memcpy(pOutbuf+OFF_ROOMS, BLANK32, SIZ_ROOMS);
   
   // Fireplace
   memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);
   *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

   // HasSeptic or HasSewer
   if (pChar->HasSeptic > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
   else if (pChar->HasSewer > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;
   else
      *(pOutbuf+OFF_SEWER) = ' ';

   // Pool
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Stories
   iTmp = atoin(pChar->Stories, SIZ_CHAR_SIZE4);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d.0 ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   } else
      memcpy(pOutbuf+OFF_STORIES, BLANK32, SIZ_STORIES);

   // Units
   if (pChar->Units[0] > '0')
      memcpy(pOutbuf+OFF_UNITS, pChar->Units, SIZ_CHAR_UNITS);

   // Zoning
   if (pChar->Zoning[0] > ' ')
   {
      memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         memcpy(pOutbuf+OFF_ZONE_X1, pChar->Zoning, SIZ_ZONE);
   }

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************** Ama_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************

int Ama_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do 
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';
         return 1;      // EOF 
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, ',');
      pTmp += 2;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 0;
   }

   iRet = ParseStringNQ(pRec, ',', MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Ama_MergeTax ******************************
 *
 * Note: 
 *
 *****************************************************************************

int Ama_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double	dTmp, dTax1, dTax2;
   int		iApn;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iRet;

   // Merge tax data
   iApn = atoin(pOutbuf, 3);
   while (!iTmp)
   {
      iTmp = ParseStringNQ(pRec, ',', MB_TAX_ROLLCAT+1, apTokens);
      if (iTmp < MB_TAX_ROLLCAT)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
         return -1;
      }

      // If same tax year, apply values
      //if (!memcmp(myCounty.acYearAssd, apTokens[MB_TAX_YEAR], 4))
      {
         dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
         dTax1 = atof(acTmp);
         dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
         dTax2 = atof(acTmp);
         dTmp = dTax1+dTax2;

         // TCB Don't double parcel range 800-870
         if ((iApn < 800) || (iApn > 879))
         {
            if (dTax1 == 0.0 || dTax2 == 0.0)
               dTmp *= 2;
         }

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/********************************* Ama_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Ama_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace tab char with 0
   // Parse input record
   if (cDelim == ',')
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN starts with 800-999 except 910 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "03AMA", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Megabyte:
   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
      iTmp = atoin(pOutbuf+OFF_USE_STD, 3);
      if (iTmp == 401)
         *(pOutbuf+OFF_AG_PRE) = 'Y';
      else if (iTmp == 403)
         *(pOutbuf+OFF_TIMBER) = 'Y';
      else
      {
         *(pOutbuf+OFF_AG_PRE) = ' ';
         *(pOutbuf+OFF_TIMBER) = ' ';
      }
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   //if (acTmp[0] > ' ')
   //{
   //   _strupr(acTmp);
   //   iTmp = strlen(acTmp);
   //   memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
   //   if (iNumUseCodes > 0)
   //   {
   //      pTmp = Cnty2StdUse(acTmp);
   //      if (pTmp)
   //      {
   //         memcpy(pOutbuf+OFF_USE_STD, pTmp, 3);
   //         iTmp = atoin(pOutbuf+OFF_USE_STD, 3);
   //         if (iTmp == 401)
   //            *(pOutbuf+OFF_AG_PRE) = 'Y';
   //         else if (iTmp == 403)
   //            *(pOutbuf+OFF_TIMBER) = 'Y';
   //         else
   //         {
   //            *(pOutbuf+OFF_AG_PRE) = ' ';
   //            *(pOutbuf+OFF_TIMBER) = ' ';
   //         }
   //      } else
   //      {
   //         memcpy(pOutbuf+OFF_USE_STD, USE_MISC, 3);
   //         logUsecode(acTmp, pOutbuf);
   //      }
   //   }
   //}

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp && !memcmp(acTmp, apTokens[MB_ROLL_DOCNUM], 4))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], SIZ_TRANSFER_DOC);
      }
   }

   // Owner
   try {
      Ama_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   } catch(...) {
      LogMsg("***** Exeception occured in Ama_MergeOwner()");
   }

   // Mailing 
   try {
      Ama_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Ama_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);
   /*
   iTmp = atol(apTokens[MB_ROLL_TAXABILITY]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d   ", iTmp);
      memcpy(pOutbuf+OFF_TAX_CODE, acTmp, SIZ_TAX_CODE);

      if (iTmp > 799 && iTmp < 900)
         *(pOutbuf+OFF_PROP8_FLG) = 'Y';     // Set Prop8 flag
      //else if (iTmp == 3)   
      //   *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';   // Set FullExempt
   }
   */
   return 0;
}

/********************************* Ama_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Ama_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], acApn[16];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with blank
   iRet = replNull(pRollRec, ' ', 0);
   replStrAll(pRollRec, "|NULL", "|");
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L2_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L2_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   iTmp = strlen(apTokens[L2_ASMT]);
   if (iTmp < iApnLen)
   {
      memcpy(acApn, "0000", iApnLen - iTmp);
      memcpy(&acApn[iApnLen - iTmp], apTokens[L2_ASMT], iTmp);
   } else
      memcpy(acApn, apTokens[L2_ASMT], iTmp);
   memcpy(pOutbuf, acApn, iApnLen);

   // Copy ALT_APN
   iTmp = strlen(apTokens[L2_FEEPARCEL]);
   if (iTmp < iApnLen)
   {
      memcpy(pOutbuf+OFF_ALT_APN, "0000", iApnLen - iTmp);
      memcpy(pOutbuf+OFF_ALT_APN+(iApnLen - iTmp), apTokens[L2_FEEPARCEL], iTmp);
   } else
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[L2_FEEPARCEL], iTmp);

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "03AMA", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values - Land
   long lLand = atoi(apTokens[L2_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L2_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow = atoi(apTokens[L2_CURRENTGROWINGIMPRVALUE]);
   long lFixt = atoi(apTokens[L2_CURRENTFIXEDIMPRVALUE]);
   long lBP   = atoi(apTokens[L2_CURRENTPERSONALPROPVALUE]);
   long lPP   = atoi(apTokens[L2_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lBP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lBP > 0)
      {
         sprintf(acTmp, "%d         ", lBP);
         memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L2_TAXAMT1]);
   double dTax2 = atof(apTokens[L2_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L2_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L2_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L2_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L2_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L2_EXEMPTIONCODE1], strlen(apTokens[L2_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L2_EXEMPTIONCODE2], strlen(apTokens[L2_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L2_EXEMPTIONCODE3], strlen(apTokens[L2_EXEMPTIONCODE3]));

   // TRA
   lTmp = atol(apTokens[L2_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L2_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030330020", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L2_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   strcpy(acTmp, apTokens[L2_USECODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Megabyte:
   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
      iTmp = atoin(pOutbuf+OFF_USE_STD, 3);
      if (iTmp == 401)
         *(pOutbuf+OFF_AG_PRE) = 'Y';
      else if (iTmp == 403)
         *(pOutbuf+OFF_TIMBER) = 'Y';
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L2_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Ama_MergeOwner(pOutbuf, apTokens[L2_OWNER]);

   // Situs - using situs file instead

   // Mailing
   Ama_MergeMAdr(pOutbuf, apTokens[L2_MAILADDRESS1], apTokens[L2_MAILADDRESS2], apTokens[L2_MAILADDRESS3], apTokens[L2_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L2_TAXABILITY], true, true);
   return 0;
}

int Ama_MergeLien_Csv(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace tab char with 0
   iRet = ParseString(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L2_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L2_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   iRet = remChar(apTokens[L2_ASMT], '-');
   memcpy(pOutbuf, apTokens[L2_ASMT], iRet);

   // Copy ALT_APN
   iRet = remChar(apTokens[L2_FEEPARCEL], '-');
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L2_FEEPARCEL], iRet);

   // Format APN
   iRet = formatApn(apTokens[L2_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L2_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "03AMA", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values
   // Land
   long lLand = atoi(apTokens[L2_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L2_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow = atoi(apTokens[L2_CURRENTGROWINGIMPRVALUE]);
   long lFixt = atoi(apTokens[L2_CURRENTFIXEDIMPRVALUE]);
   long lBP   = atoi(apTokens[L2_CURRENTPERSONALPROPVALUE]);
   long lPP   = atoi(apTokens[L2_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lBP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lBP > 0)
      {
         sprintf(acTmp, "%d         ", lBP);
         memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L2_TAXAMT1]);
   double dTax2 = atof(apTokens[L2_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // TRA
   lTmp = atol(apTokens[L2_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L2_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030330020", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L2_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   strcpy(acTmp, apTokens[L2_USECODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Megabyte:
   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      if (iNumUseCodes > 0)
      {
         pTmp = Cnty2StdUse(acTmp);
         if (pTmp)
            memcpy(pOutbuf+OFF_USE_STD, pTmp, 3);
         else
         {
            memcpy(pOutbuf+OFF_USE_STD, USE_MISC, 3);
            logUsecode(acTmp, pOutbuf);
         }
      }
   }

   // Acres
   dTmp = atof(apTokens[L2_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Ama_MergeOwner(pOutbuf, apTokens[L2_OWNER]);

   // Situs - using situs file instead

   // Mailing
   //Ama_MergeLienMAdr(pOutbuf);
   Ama_MergeMAdr(pOutbuf, apTokens[L2_MAILADDRESS1], apTokens[L2_MAILADDRESS2], apTokens[L2_MAILADDRESS3], apTokens[L2_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L2_TAXABILITY], true, true);
   /*
   iTmp = atol(apTokens[L2_TAXABILITY]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d   ", iTmp);
      memcpy(pOutbuf+OFF_TAX_CODE, acTmp, SIZ_TAX_CODE);

      if (iTmp > 799 && iTmp < 900)
         *(pOutbuf+OFF_PROP8_FLG) = 'Y';     // Set Prop8 flag
      //else if (iTmp == 3)   
      //   *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';   // Set FullExempt
   }
   */
   return 0;
}

/****************************** Ama_MergeCurRoll *****************************
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Ama_MergeCurRoll(char *pOutbuf)
{
   static   char acRec[MAX_RECSIZE], *pRec=NULL;
   char     acTmp[256], *pTmp, *apItems[MAX_FLD_TOKEN];
   int      iRet=0, iTmp;

   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
      pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "021112006000", 9))
   //   iTmp = 0;
#endif

   do 
   {
      if (!pRec)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         return 1;      // EOF 
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec: %s", pRec);
         pRec = fgets(acRec, MAX_RECSIZE, fdRoll);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);
   else
      iRet = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad roll record for APN=%s", apItems[iApnFld]);
      return -1;
   }

   // Recorded Doc - Not consistent with sale file - use only when matched
   if (*apItems[MB_ROLL_DOCNUM] > '0')
   {
      pTmp = dateConversion(apItems[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp  && !memcmp(acTmp, apItems[MB_ROLL_DOCNUM], 4))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apItems[MB_ROLL_DOCNUM], strlen(apItems[MB_ROLL_DOCNUM]));
      }
   } 

   lRollMatch++;

   // Get next record
   pRec = fgets(acRec, MAX_RECSIZE, fdRoll);

   return 0;
}

/***************************** Ama_Load_LDR() *******************************
 *
 * Load LDR into 1900-byte record.
 *
 ****************************************************************************/

int Ama_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH], acCurRoll[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open LDR file
   LogMsg("Open LDR file %s", acRollFile);
   fdLDR = fopen(acRollFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acRollFile);
      return -2;
   }  

   // Open current roll file - to update recently transfer
   GetIniString(myCounty.acCntyCode, "RollFile", "", acBuf, _MAX_PATH, acIniFile);
   sprintf(acCurRoll, acBuf, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open current roll file %s", acCurRoll);
   fdRoll = fopen(acCurRoll, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening current roll file: %s.  Please check INI file in [%s] section\n", acCurRoll, myCounty.acCntyCode);
      return -2;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -1;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   if (!isdigit(acRec[3]))
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      lLDRRecCount++;

      // Create new R01 record
      iRet = Ama_MergeLien(acBuf, acRec);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Ama_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Ama_MergeStdChar(acBuf);

         // Merge transfer from current roll
         if (fdRoll)
            lRet = Ama_MergeCurRoll(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp || acRec[1] > '9' || strlen(acRec) < 20)
         break;
   }

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   printf("\nTotal output records:       %u\n", lCnt);
   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of roll matched:     %u", lRollMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   lRecCnt = lCnt;
   return 0;
}

// Load LDR for 2010 and before using CSV file
int Ama_Load_LDR_Csv(int iFirstRec)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return 2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return 2;
   }
   
   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      lLDRRecCount++;

      // Create new R01 record
      iRet = Ama_MergeLien_Csv(acBuf, acRec);

      // Merge Situs
      if (fdSitus)
         lRet = Ama_MergeSitus(acBuf);

      // Merge Exe
      if (fdExe)
         lRet = MB_MergeExe(acBuf);

      // Merge Char
      if (fdChar)
         lRet = Ama_MergeStdChar(acBuf);

      if (iRet != 0)
      {
         if (!(pTmp=fgets(acRec, MAX_RECSIZE, fdRoll)) )
            break;
         continue;
      }


      if (!iRet)
      {
         iNewRec++;
#ifdef _DEBUG
         iRet = replChar(acBuf, 0, ' ', iRecLen);
         if (iRet)
            LogMsg("*** Null char found at pos %d on APN=%.12s", iRet, acBuf);
#endif

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_SALE1_DT], SIZ_SALE1_DT);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;
         
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fhOut)
      CloseHandle(fhOut);

   printf("\nTotal output records:       %u\n", lCnt);
   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   lRecCnt = lCnt;
   return 0;
}

/***************************** Ama_Load_Roll() *******************************
 *
 * Load roll into 1900-byte record.
 *
 ****************************************************************************/

int Ama_Load_Roll(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH]; //, acLienFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, iRollUpd=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }  

   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }
   /*
   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSales = fopen(acSalesFile, "r");
   if (fdSales == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -2;
   }
   */
   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }

   // Open Lien file
   //sprintf(acLienFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //LogMsg("Open Lien extract file %s", acLienFile);
   //fdLienExt = fopen(acLienFile, "r");
   //if (fdLienExt == NULL)
   //{
   //   LogMsg("***** Error opening Tax file: %s\n", acLienFile);
   //   return -2;
   //}

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acRec, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   if (_access(acRec, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRec);
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Skip header record
   for (iRet = 0; iRet < iHdrRows; iRet++)
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
#ifdef _DEBUG
      //if (!memcmp(acBuf, "001040005000", 9))
      //   iRet = 0;
#endif

      // Create new R01 record
      iRet = Ama_MergeRoll(acBuf, acRec, 0, CREATE_R01);

      if (!iRet)
      {
         // Merge Lien
         //iRet = 1;
         //if (fdLienExt)
         //   iRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Exe if not found in LienExt
         if (fdExe)
            iRet = MB_MergeExe(acBuf);

         // Merge Situs
         if (fdSitus)
            iRet = Ama_MergeSitus(acBuf);

         // Merge Char 
         if (fdChar)
            iRet = Ama_MergeStdChar(acBuf);

         // Merge Taxes
         if (fdTax)
            iRet = MB_MergeTaxG1(acBuf, iHdrRows);

         iRet = replChar(acBuf, 0, ' ', iRecLen);
         if (iRet)
            LogMsg("*** Null char found at pos %d on APN=%.12s", iRet, acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;
         
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
         iRollUpd++;
      } else
      {
         if (!(pTmp=fgets(acRec, MAX_RECSIZE, fdRoll)) )
            break;
         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lCnt);
   lRecCnt = iRollUpd;
   return 0;
}

/***************************** Ama_ConvertChar() ****************************
 *
 * This function is used to load old CHAR file prior to 8/31/2011
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

int Ama_ConvertChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmpFile[256], *pRec;
   int      iRet, iTmp, iCnt=0;
   STDCHAR  myCharRec;

   printf("\nConverting char file %s\n", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   strcpy(acTmpFile, pInfile);
   pRec = strrchr(acTmpFile, '.');
   if (pRec)
      strcpy(pRec, ".tmp");

   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   pRec = fgets(acBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);
      if (!pRec)
         break;

      iRet = ParseStringNQ(pRec, ',', MB_CHAR_NUMFLOORS+1, apTokens);
      if (iRet < MB_CHAR_NUMFLOORS)
      {
         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      // Taking parcel where FeeParcel=Asmt only
      if (memcmp(apTokens[MB_CHAR_ASMT], apTokens[MB_CHAR_FEE_PRCL], iApnLen))
         continue;

#ifdef _DEBUG
      //if (!memcmp(apTokens[MB_CHAR_ASMT], "044510005", 9))
      //   iRet = 0;
#endif

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[MB_CHAR_ASMT], strlen(apTokens[MB_CHAR_ASMT]));
      if (*apTokens[MB_CHAR_ASMT_STATUS] > ' ')
         myCharRec.AsmtStatus = *apTokens[MB_CHAR_ASMT_STATUS];

      iTmp = atoi(apTokens[MB_CHAR_POOLS]);
      if (iTmp > 0)
      {
         switch (iTmp)
         {
            case 1:
               myCharRec.Pool[0] = 'G';   // Gunite
               break;
            case 2:
               myCharRec.Pool[0] = 'F';   // Fiberglass
               break;
            case 3:
               myCharRec.Pool[0] = 'V';   // Vinyl
               break;
            case 4:
               myCharRec.Pool[0] = 'X';   // PP Abv Ground
               break;
            case 5:
               myCharRec.Pool[0] = 'S';   // Spa
               break;
            default:
               break;
         }
      }

      memcpy(myCharRec.LandUseCat, apTokens[MB_CHAR_USECAT], strlen(apTokens[MB_CHAR_USECAT]));
      memcpy(myCharRec.QualityClass, apTokens[MB_CHAR_QUALITY], strlen(apTokens[MB_CHAR_QUALITY]));
      memcpy(myCharRec.YrBlt, apTokens[MB_CHAR_YRBLT], strlen(apTokens[MB_CHAR_YRBLT]));
      memcpy(myCharRec.BldgSqft, apTokens[MB_CHAR_BLDGSQFT], strlen(apTokens[MB_CHAR_BLDGSQFT]));
      memcpy(myCharRec.GarSqft, apTokens[MB_CHAR_GARSQFT], strlen(apTokens[MB_CHAR_GARSQFT]));
      memcpy(myCharRec.Heating, apTokens[MB_CHAR_HEATING], strlen(apTokens[MB_CHAR_HEATING]));
      memcpy(myCharRec.Cooling, apTokens[MB_CHAR_COOLING], strlen(apTokens[MB_CHAR_COOLING]));
      memcpy(myCharRec.HeatingSrc, apTokens[MB_CHAR_HEATING_SRC], strlen(apTokens[MB_CHAR_HEATING_SRC]));
      memcpy(myCharRec.CoolingSrc, apTokens[MB_CHAR_COOLING_SRC], strlen(apTokens[MB_CHAR_COOLING_SRC]));

      iTmp = atoi(apTokens[MB_CHAR_BEDS]);
      if (iTmp > 0)
         memcpy(myCharRec.Beds, apTokens[MB_CHAR_BEDS], strlen(apTokens[MB_CHAR_BEDS]));

      iTmp = atoi(apTokens[MB_CHAR_FBATHS]);
      if (iTmp > 0)
         memcpy(myCharRec.FBaths, apTokens[MB_CHAR_FBATHS], strlen(apTokens[MB_CHAR_FBATHS]));

      iTmp = atoi(apTokens[MB_CHAR_HBATHS]);
      if (iTmp > 0)
         memcpy(myCharRec.HBaths, apTokens[MB_CHAR_HBATHS], strlen(apTokens[MB_CHAR_HBATHS]));

      iTmp = atoi(apTokens[MB_CHAR_FP]);
      if (iTmp > 0)
         memcpy(myCharRec.Fireplace, apTokens[MB_CHAR_FP], strlen(apTokens[MB_CHAR_FP]));

      memcpy(myCharRec.FeeParcel, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));

      if (*(apTokens[MB_CHAR_HASSEPTIC]) > '0')
         myCharRec.HasSeptic = 'Y';

      if (*(apTokens[MB_CHAR_HASSEWER]) > '0')
         myCharRec.HasSewer = 'Y';

      if (*(apTokens[MB_CHAR_HASWELL]) > '0')
         myCharRec.HasWell = 'Y';

      // New fields - BLDGSEQNO is always 1
      //if (*(apTokens[MB_CHAR_BLDGSEQNO]) > '0')
      //   memcpy(myCharRec.BldgSeqNo, apTokens[MB_CHAR_BLDGSEQNO], strlen(apTokens[MB_CHAR_BLDGSEQNO]));
      // UNITSEQNO has duplicate
      //if (*(apTokens[MB_CHAR_UNITSEQNO]) > '0')
      //   memcpy(myCharRec.UnitSeqNo, apTokens[MB_CHAR_UNITSEQNO], strlen(apTokens[MB_CHAR_UNITSEQNO]));

      // Stories/NumFloors
      iTmp = atoi(apTokens[MB_CHAR_NUMFLOORS]);
      if (iTmp > 0)
         memcpy(myCharRec.Stories, apTokens[MB_CHAR_NUMFLOORS], strlen(apTokens[MB_CHAR_NUMFLOORS]));


      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec, fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   printf("\nSorting char file %s --> %s\n", acTmpFile, acCChrFile);
   iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT) DUPOUT");

   printf("\n");
   return iRet;
}

/****************************** Ama_ExtrChar() ******************************
 *
 * Use this function for new CHAR file layout starting 8/31/2011
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

int Ama_ExtrChar()
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmpFile[256], acTmp[256], *pRec;
   int      iRet, iTmp, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nExtracting char file %s\n", acCharFile);

   if (!(fdIn = fopen(acCharFile, "r")))
   {
      LogMsg("***** Error open input file %s.  Please check for file exist.", acCharFile);
      return -1;
   }

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);

      if (!pRec)
         break;

      if (cDelim == ',')
         iRet = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iRet = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iRet < MB2_CHAR_SIZETYPE)
      {
         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));

      // Apn
      memcpy(myCharRec.Apn, apTokens[MB2_CHAR_ASMT], strlen(apTokens[MB2_CHAR_ASMT]));
      
      // Fee parcel
      memcpy(myCharRec.FeeParcel, apTokens[MB2_CHAR_FEE_PRCL], strlen(apTokens[MB2_CHAR_FEE_PRCL]));
      
      // Pools
      iTmp = atoi(apTokens[MB2_CHAR_POOLS]);
      if (iTmp > 0)
      {
         switch (iTmp)
         {
            case 1:
               myCharRec.Pool[0] = 'G';   // Gunite
               break;
            case 2:
               myCharRec.Pool[0] = 'F';   // Fiberglass
               break;
            case 3:
               myCharRec.Pool[0] = 'V';   // Vinyl
               break;
            case 4:
               myCharRec.Pool[0] = 'X';   // PP Abv Ground
               break;
            case 5:
               myCharRec.Pool[0] = 'S';   // Spa
               break;
            default:
               break;
         }
      }

      // Use cat
      memcpy(myCharRec.LandUseCat, apTokens[MB2_CHAR_USECAT], strlen(apTokens[MB2_CHAR_USECAT]));

      // Quality Class
      memcpy(myCharRec.QualityClass, _strupr(apTokens[MB2_CHAR_QUALITY]), strlen(apTokens[MB2_CHAR_QUALITY]));
      memcpy(myCharRec.Heating, apTokens[MB2_CHAR_HEATING], strlen(apTokens[MB2_CHAR_HEATING]));
      memcpy(myCharRec.Cooling, apTokens[MB2_CHAR_COOLING], strlen(apTokens[MB2_CHAR_COOLING]));
      memcpy(myCharRec.HeatingSrc, apTokens[MB2_CHAR_HEATING_SRC], strlen(apTokens[MB2_CHAR_HEATING_SRC]));
      memcpy(myCharRec.CoolingSrc, apTokens[MB2_CHAR_COOLING_SRC], strlen(apTokens[MB2_CHAR_COOLING_SRC]));

      // YrBlt
      iTmp = atoi(apTokens[MB2_CHAR_YRBLT]);
      if (iTmp > 1600 && iTmp <= lLienYear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // BldgSqft
      iTmp = atoi(apTokens[MB2_CHAR_BLDGSQFT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // GarSqft
      iTmp = atoi(apTokens[MB2_CHAR_GARSQFT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[MB2_CHAR_BEDS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Baths
      iTmp = atoi(apTokens[MB2_CHAR_FBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half baths
      iTmp = atoi(apTokens[MB2_CHAR_HBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // Fireplace
      iTmp = atoi(apTokens[MB2_CHAR_FP]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Fireplace, acTmp, iRet);
      }

      blankRem(apTokens[MB2_CHAR_HASSEPTIC]);
      if (*(apTokens[MB2_CHAR_HASSEPTIC]) > '0')
      {
         myCharRec.HasSeptic = 'Y';
         myCharRec.HasSewer = 'S';
      }

      blankRem(apTokens[MB2_CHAR_HASSEWER]);
      if (*(apTokens[MB2_CHAR_HASSEWER]) > '0')
         myCharRec.HasSewer = 'Y';

      blankRem(apTokens[MB2_CHAR_HASWELL]);
      if (*(apTokens[MB2_CHAR_HASWELL]) > '0')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // BldgSeqNum
      iTmp = atoi(apTokens[MB2_CHAR_BLDGSEQNO]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      }

      // UnitSeqNum
      iTmp = atoi(apTokens[MB2_CHAR_UNITSEQNO]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.UnitSeqNo, acTmp, iRet);
      }

      // Building Type
      if (*apTokens[MB2_CHAR_BLDGTYPE] > ' ')
         memcpy(myCharRec.BldgType, apTokens[MB2_CHAR_BLDGTYPE], strlen(apTokens[MB2_CHAR_BLDGTYPE]));

      // Stories
      if (*apTokens[MB2_CHAR_NUMFLOORS] > '0')
         memcpy(myCharRec.Stories, apTokens[MB2_CHAR_NUMFLOORS], strlen(apTokens[MB2_CHAR_NUMFLOORS]));

      // LotAcres
      double dTmp;
      dTmp = atof(apTokens[MB2_CHAR_ACRES]);
      if (dTmp > 0.0)
      {
         iRet = sprintf(acTmp, "%u", (unsigned long)(dTmp*1000.0));
         memcpy(myCharRec.LotAcre, acTmp, iRet);

         iRet = sprintf(acTmp, "%u", (long)((dTmp * SQFT_PER_ACRE) + 0.5));
         memcpy(myCharRec.LotSqft, acTmp, iRet);
      } else
      {
         // LotSqft
         long lTmp = atol(apTokens[MB2_CHAR_LOTSQFT]);
         if (lTmp > 100)
         {
            iRet = sprintf(acTmp, "%u", lTmp);
            memcpy(myCharRec.LotSqft, acTmp, iRet);
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      printf("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT) DUPOUT");
   } else
      iRet = 0;

   printf("\n");
   return iRet;
}

/**************************** Ama_ConvStdChar ********************************
 *
 * Copy from MergeMon.cpp to convert new char file.
 *
 *****************************************************************************/

int Ama_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[4], acLastApn[32], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 4096, fdIn);

   acLastApn[0] = 0;
   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);

      if (!pRec)
         break;
      if (!isdigit(*pRec))
         continue;

      // Replace NULL with empty string
      replStrAll(acBuf, "NULL", "");
      iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < AMA_CHAR_SALESPRICE)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      // Event Date
      if (*apTokens[AMA_CHAR_DOCNUM] > ' ')
         continue;

      if (*apTokens[AMA_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[AMA_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[AMA_CHAR_FEEPRCL]);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      if (*apTokens[AMA_CHAR_ASMT] == ' ' && acLastApn[0] > ' ')
      {
         if (*apTokens[AMA_CHAR_BLDGSEQNO] <= '1')
         {
            LogMsg("Drop CHAR at %d", iCnt++);
            continue;
         }
         memcpy(myCharRec.Apn, acLastApn, strlen(acLastApn));
      } else
      {
         memcpy(myCharRec.Apn, apTokens[AMA_CHAR_ASMT], strlen(apTokens[AMA_CHAR_ASMT]));
         strcpy(acLastApn, apTokens[AMA_CHAR_ASMT]);
      }
      memcpy(myCharRec.FeeParcel, apTokens[AMA_CHAR_FEEPRCL], strlen(apTokens[AMA_CHAR_FEEPRCL]));

      // Format APN
      iRet = formatApn(acLastApn, acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      // Bldg#
      iTmp = atoi(apTokens[AMA_CHAR_BLDGSEQNO]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Unit# - currently all 0
      iTmp = atoi(apTokens[AMA_CHAR_UNITSEQNO]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.UnitSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** UnitSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[AMA_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - 01, 02, 03, 99
      iTmp = blankRem(apTokens[AMA_CHAR_POOLSPA]);
      if (iTmp > 1)
      {
         pRec = findXlatCode(apTokens[AMA_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass
      iTmp = blankRem(_strupr(apTokens[AMA_CHAR_QUALITYCLASS]));
      vmemcpy(myCharRec.QualityClass, apTokens[AMA_CHAR_QUALITYCLASS], SIZ_CHAR_QCLS, iTmp);
      if (iTmp > SIZ_CHAR_QCLS)
         LogMsg("*** Long QualityClass [%s] on [%.12s]", apTokens[MB_CHAR_QUALITY], apTokens[MB_CHAR_ASMT]);
      if (*apTokens[AMA_CHAR_QUALITYCLASS] > ' ' && *apTokens[AMA_CHAR_QUALITYCLASS] < 'Z')
      {

         acCode[0] = ' ';
         strcpy(acTmp, apTokens[AMA_CHAR_QUALITYCLASS]);
         if (!_memicmp(acTmp, "AV", 2) || !_memicmp(acTmp, "GO", 2))
            acCode[0] = acTmp[0];
         else if (isalpha(acTmp[0])) 
         {
            myCharRec.BldgClass = acTmp[0];

            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            else if (isdigit(acTmp[2]))
               iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
            else if (strstr(acTmp, "AV"))
            {
               acCode[0] = 'A';
            } else if (strstr(acTmp, "GO"))
            {
               acCode[0] = 'G';
            } else if (strstr(acTmp, "EX"))
            {
               acCode[0] = 'E';
            } else if (strstr(acTmp, "VG") || strstr(acTmp, "V G"))
            {
               acCode[0] = 'V';
            } else 
            {
               pRec = strchr(acTmp, ' ');
               if (pRec)
               {
                  pRec++;
                  if (isdigit(*pRec))
                     iRet = Quality2Code(pRec, acCode, NULL);
                  else if (!_memicmp(pRec, "GO", 2) || !_memicmp(pRec, "AV", 2) || !_memicmp(pRec, "EX", 2) || !_memicmp(pRec, "VG", 2))
                     acCode[0] = *pRec;
                  else if (!_memicmp(pRec, "LO", 2))
                     acCode[0] = 'P';
                  else
                     LogMsg("*** Unknown QUALITYCLASS: '%s' in [%s]", apTokens[AMA_CHAR_QUALITYCLASS], apTokens[AMA_CHAR_ASMT]);
               }
            }
         } else if (isdigit(acTmp[0]))
            iRet = Quality2Code(acTmp, acCode, NULL);
         else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[AMA_CHAR_QUALITYCLASS], apTokens[AMA_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (*apTokens[AMA_CHAR_QUALITYCLASS] > ' ' && *apTokens[AMA_CHAR_QUALITYCLASS] != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[AMA_CHAR_QUALITYCLASS], apTokens[AMA_CHAR_ASMT]);

      // Improved Condition
      if (*apTokens[AMA_CHAR_CONDITION] >= '0')
      {
         pRec = findXlatCode(apTokens[AMA_CHAR_CONDITION], &asCond[0]);
         if (pRec)
            myCharRec.ImprCond[0] = *pRec;
      }

      // YrBlt
      int iYrBlt = atoi(apTokens[AMA_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[AMA_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[AMA_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[AMA_CHAR_UNITSCNT]);
      //if (iTmp > 0 && iBldgSize > iTmp*100)
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 
      //else if (iTmp > 1)
      //   LogMsg("*** Questionable UnitsCnt: %d, BldgSqft=%d, Apn=%s", iTmp, iBldgSize, apTokens[AMA_CHAR_ASMT]);

      // Attached SF
      int iAttGar = atoi(apTokens[AMA_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[AMA_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport SF
      int iCarport = atoi(apTokens[AMA_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // LandSqft is not populated, use Acres (same in AMA)
      double dAcres = atof(apTokens[AMA_CHAR_ACRES]);
      long lLotSqft = atol(apTokens[AMA_CHAR_LANDSQFT]);
      if (dAcres > 0.0)
      {
         iRet = sprintf(acTmp, "%u", (unsigned long)(dAcres*1000.0));
         memcpy(myCharRec.LotAcre, acTmp, iRet);

         iRet = sprintf(acTmp, "%u", (long)(dAcres * SQFT_PER_ACRE));
         memcpy(myCharRec.LotSqft, acTmp, iRet);
      } else if (lLotSqft > 100)
      {
         iRet = sprintf(acTmp, "%u", lLotSqft);
         memcpy(myCharRec.LotSqft, acTmp, iRet);

         // Lot acres
         double dTmp;
         dTmp = (double)(lLotSqft*SQFT_MF_1000);
         iTmp = sprintf(acTmp, "%u", (long)(dTmp+0.1));
         memcpy(myCharRec.LotAcre, acTmp, iTmp);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "003351003000", 9))
      //   iRet = 0;
#endif
      // Garage Type
      if (*apTokens[AMA_CHAR_GARAGE] > ' ')
      {
         _toupper(*apTokens[AMA_CHAR_GARAGE]);
         pRec = findXlatCode(apTokens[AMA_CHAR_GARAGE], &asParkType[0]);
         if (pRec)
            myCharRec.ParkType[0] = *pRec;
      }

      // Park space - no data avail
      iTmp = atoi(apTokens[AMA_CHAR_PARKINGSPACES]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.ParkSpace, acTmp, iRet);
      }

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[AMA_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCodeA(apTokens[AMA_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[AMA_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[AMA_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[AMA_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[AMA_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[AMA_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[AMA_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[AMA_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace
      if (*apTokens[AMA_CHAR_FIREPLACE] > ' ')
      {
         pRec = findXlatCode(apTokens[AMA_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      }

      // Sewer
      blankRem(apTokens[AMA_CHAR_SEWERCODE]);
      if (*apTokens[AMA_CHAR_SEWERCODE] > ' ')
         myCharRec.HasSewer = 'Y';

      /*
      blankRem(apTokens[AMA_CHAR_HASSEPTIC]);
      if (*(apTokens[AMA_CHAR_HASSEWER]) > '0')
         myCharRec.HasSewer = 'Y';
      else if (*(apTokens[AMA_CHAR_HASSEPTIC]) > '0')
      {
         myCharRec.HasSeptic = 'Y';
         myCharRec.HasSewer = 'S';
      }
      */

      // Water
      blankRem(apTokens[AMA_CHAR_HASWELL]);
      if (*(apTokens[AMA_CHAR_HASWELL]) > '0')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      } else if (*(apTokens[AMA_CHAR_WATERSOURCE]) > '0')
      {
         blankRem(apTokens[AMA_CHAR_WATERSOURCE]);
         pRec = findXlatCode(apTokens[AMA_CHAR_WATERSOURCE], &asWaterSrc[0]);
         if (pRec)
         {
            myCharRec.HasWater = *pRec;
            if (*pRec == 'W')
               myCharRec.HasWell = 'Y';
         }
      }

      // OfficeSpaceSF
      iTmp = atoi(apTokens[AMA_CHAR_OFFICESPACESF]);
      if (iTmp > 10)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Misc.sExtra.OfficeSqft, acTmp, iRet);
      }

      // NonConditionSF

      // ViewCode
      if (*apTokens[AMA_CHAR_VIEWCODE] >= 'A')
      {
         pRec = findXlatCode(apTokens[AMA_CHAR_VIEWCODE], &asView[0]);
         if (pRec)
            myCharRec.View[0] = *pRec;
      }

      // Stories/NumFloors
      iTmp = atoi(apTokens[AMA_CHAR_STORIESCNT]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Zoning
      if (*apTokens[AMA_CHAR_ZONING] >= '0')
         memcpy(myCharRec.Zoning, apTokens[AMA_CHAR_ZONING], strlen(apTokens[AMA_CHAR_ZONING]));

      // Event Date
      /*
      if (*apTokens[AMA_CHAR_EVENTDATE] >= '0')
      {  
         // Convert date from yyyy-mm-dd to yyyymmdd
         pRec = dateConversion(apTokens[AMA_CHAR_EVENTDATE], acTmp, YYYY_MM_DD);
         if (pRec)
            memcpy(myCharRec.Misc.sExtra.RecDate, acTmp, 8);
      }
      */
      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D,461,8,C,D) DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/**************************** Ama_CreateLienRec *****************************
 *
 * Format lien extract record.  This function may not work with all counties.
 * Check lien file layout first before use.
 *
 ****************************************************************************/

int Ama_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   replStrAll(pRollRec, "|NULL", "|");
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_USERID)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   iRet = strlen(apTokens[L_ASMT]);
   if (iRet < iApnLen)
   {
      memcpy(pLienExtr->acApn, "0000", iApnLen - iRet);
      memcpy(&pLienExtr->acApn[iApnLen - iRet], apTokens[L_ASMT], iRet);
   } else
      memcpy(pLienExtr->acApn, apTokens[L_ASMT], iRet);

   // TRA
   lTmp = atol(apTokens[L_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LIEN_FIXT);
   }

   // Improve
   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_LIEN_FIXT);
   }

   // Other values
   long lGrow = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);

   // Fixtures
   long lFixt   = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);

   // Business Property
   long lBP   = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);

   // Personal Property
   long lPP   = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lBP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lBP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lBP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  
   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Prop 8 - correcting by spn 01/16/2010
   lTmp = atoin(apTokens[L_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Taxability
   if (lTmp > 0)
   {
      iRet = strlen(apTokens[L_TAXABILITY]);
      if (iRet > SIZ_LIEN_TAXCODE)
         iRet = SIZ_LIEN_TAXCODE;
      memcpy(pLienExtr->acTaxCode, apTokens[L_TAXABILITY], iRet);
   }

   // Save Exe code
   memcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
   memcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
   memcpy(pLienExtr->extra.MB.ExeCode3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************* Ama_ExtrLien *******************************
 *
 * Extract lien data from Ama_yyyy.txt
 *
 ****************************************************************************/

int Ama_ExtrLien(LPCSTR pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg("\nExtract lien roll for %s", pCnty);

   // Open lien file
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acRollFile);
      return 2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Skip header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (!isdigit(acRec[3]))
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new record
      iRet = Ama_CreateLienRec(acBuf, acRec);
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (strlen(acRec) < 20)
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Ama_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Ama_MergeLien3(char *pOutbuf, char *pRollRec, bool bRemHyphen)
{
   char     acApn[32], acTmp[256], acTmp1[64], *pTmp;
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Remove hyphen from APN
   if (bRemHyphen)
   {
      remChar(apTokens[L3_ASMT], '-');
      remChar(apTokens[L3_FEEPARCEL], '-');
   }

   // Copy APN
   iTmp = iApnLen-strlen(apTokens[L3_ASMT]);
   sprintf(acApn, "%.*s%s", iTmp, "000", apTokens[L3_ASMT]);
   memcpy(pOutbuf, acApn, iApnLen);

   // Copy ALT_APN
   iTmp = iApnLen-strlen(apTokens[L3_FEEPARCEL]);
   sprintf(acTmp, "%.*s%s", iTmp, "000", apTokens[L3_FEEPARCEL]);
   memcpy(pOutbuf+OFF_ALT_APN, acTmp, iApnLen);

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "03AMA", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   remChar(apTokens[L3_TRA], '-');
   lTmp = atol(apTokens[L3_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%0.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&AMA_Exemption);

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (iTokens > L3_ISAGPRESERVE && *apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Ama_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   //Ama_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Ama_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*(apTokens[L3_CURRENTDOCNUM]+3) > '0')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 

   //// Garage size
   //dTmp = atof(apTokens[L3_GARAGESIZE]);
   //if (dTmp > 0.0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_GAR_SQFT, dTmp);
   //   memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   //   *(pOutbuf+OFF_PARK_TYPE) = '2';                 // GARAGE/CARPORT
   //}

   //// Number of parking spaces
   //if (*apTokens[L3_GARAGE] == '0' || *apTokens[L3_GARAGE] == 'N')
   //   *(pOutbuf+OFF_PARK_TYPE) = 'H';                 // None
   //else if (*apTokens[L3_GARAGE] > '0' && *apTokens[L3_GARAGE] <= '9')
   //{
   //   iTmp = atol(apTokens[L3_GARAGE]);
   //   sprintf(acTmp, "%d", iTmp);
   //   vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
   //   if (dTmp > 100)
   //      *(pOutbuf+OFF_PARK_TYPE) = 'Z';              // Garage
   //   else
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
   //} else
   //{
   //   if (*(apTokens[L3_GARAGE]) == 'C')
   //      *(pOutbuf+OFF_PARK_TYPE) = 'C';              // Carport
   //   else if (*(apTokens[L3_GARAGE]) == 'A')
   //   {
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L3_GARAGE], "DOU", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '2';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L3_GARAGE], "TRI", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '3';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L3_GARAGE], "SIN", 3))
   //   {
   //      *(pOutbuf+OFF_PARK_SPACE) = '1';
   //      if (dTmp > 100)
   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
   //      else
   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
   //   } else if (!_memicmp(apTokens[L3_GARAGE], "GC", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT
   //   else if (!_memicmp(apTokens[L3_GARAGE], "GS", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT ?
   //   else if (!_memicmp(apTokens[L3_GARAGE], "DE", 2))
   //      *(pOutbuf+OFF_PARK_TYPE) = 'D';              // Detached
   //   else if (*(apTokens[L3_GARAGE]) == 'S')
   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Space ?
   //}

   //// YearBlt
   //lTmp = atol(apTokens[L3_YEARBUILT]);
   //if (lTmp > 1800 && lTmp < lToyear)
   //{
   //   iTmp = sprintf(acTmp, "%d", lTmp);
   //   memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   //}

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   //// Total rooms
   //iTmp = atol(apTokens[L3_TOTALROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   //}

   //// Stories
   //iTmp = atol(apTokens[L3_STORIES]);
   //if (iTmp > 0 && iTmp < 100)
   //{
   //   sprintf(acTmp, "%d.0", iTmp);
   //   vmemcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   //}

   //// Units
   //iTmp = atol(apTokens[L3_UNITS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%d", iTmp);
   //   vmemcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   //}

   //// Beds
   //iTmp = atol(apTokens[L3_BEDROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BEDS, iTmp);
   //   memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   //}

   //// Baths
   //iTmp = atol(apTokens[L3_BATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_F, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   //}

   //// HBaths
   //iTmp = atol(apTokens[L3_HALFBATHS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BATH_H, iTmp);
   //   memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   //}

   //// Heating
   //int iCmp;
   //if (*apTokens[L3_HEATING] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_HEATING], asHeating_LDR[iTmp].acSrc, asHeating_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_HEAT) = asHeating_LDR[iTmp].acCode[0];
   //}

   //// Cooling
   //if (*apTokens[L3_AC] == 'C')
   //   *(pOutbuf+OFF_AIR_COND) = 'C';
   //else if (*apTokens[L3_AC] > ' ')
   //   LogMsg("*** Unknown A/C: %s", apTokens[L3_AC]);

   //// Pool/Spa
   //if (*apTokens[L3_POOLSPA] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_POOLSPA], asPool_LDR[iTmp].acSrc, asPool_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_POOL) = asPool_LDR[iTmp].acCode[0];
   //}

   //// Fire place
   //if (*apTokens[L3_FIREPLACE] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   pTmp = _strupr(apTokens[L3_FIREPLACE]);
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_FIREPLACE], asFP_LDR[iTmp].acSrc, asFP_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_FIRE_PL) = asPool[iTmp].acCode[0];
   //}

   //// Quality Class
   //acTmp1[0] = 0;
   //if (*apTokens[L3_QUALITYCLASS] > '0' || strlen(apTokens[L3_QUALITYCLASS]) > 1)
   //{
   //   strcpy(acTmp, apTokens[L3_QUALITYCLASS]);
   //   remCharEx(acTmp, " ,'?");
   //   pTmp = _strupr(acTmp);

   //   //memcpy(myCharRec.QualityClass, pTmp, strlen(pTmp));
   //   if (acTmp[0] == 'A' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'A';
   //   else if (acTmp[0] == 'F' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'F';
   //   else if (!memcmp(pTmp, "POOR", 4) || !memcmp(pTmp, "LOW", 3))
   //      acTmp1[0] = 'P';
   //   else if (*pTmp == 'G')
   //      acTmp1[0] = 'G';
   //   else if (isalpha(*pTmp))
   //   {
   //      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
   //      if (isdigit(acTmp[1]))
   //         iRet = Quality2Code(&acTmp[1], acTmp1, NULL);
   //      else if (isdigit(acTmp[2]))
   //         iRet = Quality2Code(&acTmp[2], acTmp1, NULL);
   //      else if (isalpha(acTmp[1]))
   //      {
   //         switch (acTmp[1])
   //         {
   //            case 'L':
   //            case 'P':
   //               acTmp1[0] = 'P';
   //               break;
   //            case 'A':
   //               acTmp1[0] = 'A';
   //               break;
   //            case 'F':
   //               acTmp1[0] = 'F';
   //               break;
   //            case 'G':
   //               acTmp1[0] = 'G';
   //               break;
   //         }
   //      }
   //   } else if (isdigit(*pTmp))
   //   {
   //      iTmp = atol(pTmp);
   //      if (iTmp < 100)
   //         iRet = Quality2Code(pTmp, acTmp1, NULL);
   //   }

   //   if (acTmp1[0] > '0')
   //      *(pOutbuf+OFF_BLDG_QUAL) = acTmp1[0];
   //}

   return 0;
}

/********************************* Ama_MergeLien *****************************
 *
 * For 2023 LDR "2023AGENCYCDCURRSECURED601TR.txt"
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Ama_MergeLien31(char *pOutbuf, char *pRollRec, bool bRemHyphen)
{
   char     acApn[32], acTmp[256], acTmp1[64];
   ULONG    lTmp;
   double   dTmp;
   int      iRet, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L31_ISAGPRESERVE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L31_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Remove hyphen from APN
   if (bRemHyphen)
   {
      remChar(apTokens[L31_ASMT], '-');
      remChar(apTokens[L31_FEEPARCEL], '-');
   }

   // Copy APN
   iTmp = iApnLen-strlen(apTokens[L31_ASMT]);
   sprintf(acApn, "%.*s%s", iTmp, "000", apTokens[L31_ASMT]);
   memcpy(pOutbuf, acApn, iApnLen);

   // Copy ALT_APN
   iTmp = iApnLen-strlen(apTokens[L31_FEEPARCEL]);
   sprintf(acTmp, "%.*s%s", iTmp, "000", apTokens[L31_FEEPARCEL]);
   memcpy(pOutbuf+OFF_ALT_APN, acTmp, iApnLen);

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "03AMA", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L31_ASMTSTATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // TRA
   remChar(apTokens[L31_TRA], '-');
   lTmp = atol(apTokens[L31_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%0.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L31_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L31_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L31_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L31_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L31_GROWING]);
   long lPers  = atoi(apTokens[L31_PPVALUE]);
   long lPP_MH = atoi(apTokens[L31_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L31_HOX]);
   long lExe2 = atol(apTokens[L31_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L31_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L31_OTHEREXEMPTIONCODE], strlen(apTokens[L31_OTHEREXEMPTIONCODE]));

   // Legal
   updateLegal(pOutbuf, apTokens[L31_PARCELDESCRIPTION]);

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&AMA_Exemption);

   // UseCode
   if (*apTokens[L31_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L31_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L31_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L31_ACRES]);
   lTmp = atol(apTokens[L31_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
      if (lTmp > 999999999)
      {
         LogMsg("*** LotSqft is too big, no update");
      } else
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (iTokens > L31_ISAGPRESERVE && *apTokens[L31_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Ama_MergeOwner(pOutbuf, apTokens[L31_OWNER]);

   // Situs
   //Ama_MergeSitus(pOutbuf, apTokens[L31_SITUS1], apTokens[L31_SITUS2]);

   // Mailing
   Ama_MergeMAdr(pOutbuf, apTokens[L31_MAILADDRESS1], apTokens[L31_MAILADDRESS2], apTokens[L31_MAILADDRESS3], apTokens[L31_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L31_TAXABILITYFULL], true, true);

   return 0;
}

/******************************** Ama_Load_LDR3 *****************************
 *
 * Load LDR 2016
 *
 ****************************************************************************/

int Ama_Load_LDR3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   bool     bRemHyphen;
   long     lLienMatch=0, lCnt=0, lTmp, lRet;

   // Check for special case
   GetIniString(myCounty.acCntyCode, "RemHyphen", "", acBuf, _MAX_PATH, acIniFile);
   if (acBuf[0] == 'Y')
      bRemHyphen = true;
   else
      bRemHyphen = false;
   
   //GetIniString(myCounty.acCntyCode, "LienFile", "", acRollFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   //sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   //lTmp = getFileDate(acRollFile);
   //if (lTmp < lToday)
   //{
   //   iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
   //   if (!iRet)
   //      return -1;
   //}

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Value file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Lien extract file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   } else
      fdLienExt = NULL;

   // Open Situs file
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
      if (cDelim == '|')
         strcat(acRec, "DEL(124) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   LogMsg("Open Situs file %s", acTmpFile);
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      // Create new R01 record
      if (iLdrGrp == 3)
         iRet = Ama_MergeLien3(acBuf, acRec, bRemHyphen);
      else
         iRet = Ama_MergeLien31(acBuf, acRec, bRemHyphen);

      if (!iRet)
      {
         // Merge value from LDR extract
         if (fdLienExt)
         {
            iRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);
            if (!iRet)
               lLienMatch++;
         }

         // Merge Situs
         if (fdSitus)
            iRet = Ama_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            iRet = Ama_MergeStdChar(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp || *pTmp > '9')
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdLienExt)
      fclose(fdLienExt);
   
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of lien matched:     %u", lLienMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Situs matched:    %u\n", lSitusMatch);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Situs skiped:     %u\n", lSitusSkip);
   
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/*********************************** loadAma ********************************
 *
 * Options:
 *    -CAMA -L -Xl [-Usi|-Ms] [-T] [-X8]    (load lien)
 *    -CAMA -U [-Usi|-Ms|-Xsi] [-T]         (load update)
 *    -Fc (clear chars)
 *
 ****************************************************************************/

int loadAma(int iSkip)
{
   int   iRet=0;
   char  acTmp[_MAX_PATH], acTmp1[_MAX_PATH];

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;

   iApnLen = myCounty.iApnLen;

   if (iLoadTax == TAX_LOADING)                    // -T
   {
      // Load taxrollinfo.txt into SQL
      //iRet = MB_Load_TaxRollInfo(bTaxImport);

      // AMA doesn't provide TaxRollInfo, use Ama_Tax.txt to load Tax_Base table
      iRet = MB_Load_TaxBase(bTaxImport, true, 1, iHdrRows);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet = MB_Load_TaxCodeMstr(bTaxImport);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport, 0, false);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
   } else if (iLoadTax == TAX_UPDATING)            // -Ut
   {
      iRet = MB_Update_TaxPaid(bTaxImport, 1, iHdrRows);

      // Load Redemption
      if (!iRet)
         iRet |= MB_Load_TaxRedemption(bTaxImport);

      // Update Delq flag in Tax_Base
      if (!iRet)
         iRet = updateDelqFlag(myCounty.acCntyCode);
   }

   if (!iLoadFlag)
      return iRet;

   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      if (!_access(acCharFile, 0))
      {
         if (strstr(acCharFile, ".csv"))
            iRet = Ama_ConvertChar(acCharFile);
         else
            iRet = Ama_ConvStdChar(acCharFile);

         if (iRet <= 0)
         {
            LogMsg("***** Error converting Char file %s", acCharFile);
            return -1;
         }
      } else
         LogMsg("***** Error: Missing CHAR file %s", acCharFile);
   }

   // Extract lien file - LDR2016
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acTmp, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acTmp, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acTmp, 0, false);      // 2016 
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode, NULL, 0);
   }
   // Extract lien value
   //if (iLoadFlag & EXTR_LIEN)
   //{
      // LDR 2014 - import xlsx file into SQL then export to txt file Ama_2014.txt
      //iRet = Ama_ExtrLien(myCounty.acCntyCode);
      // LDR 2011-2013
      //iRet = MB_ExtrTR601(myCounty.acCntyCode);
      // LDR 2010
      //iRet = MB_ExtrLien(myCounty.acCntyCode, NULL);
   //}

   // Load Value file
   if (iLoadFlag & EXTR_VALUE)                     // -Xv
   {
      char sDbName[64], sBYVFile[_MAX_PATH], sTmpFile[_MAX_PATH];

      sprintf(sBYVFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Value");
      sprintf(sTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = MB_ExtrValues(myCounty.acCntyCode, acValueFile, acExeFile, sTmpFile, sBYVFile, 1);
      
      // Reset reason and VST
      if (iRet > 0)
      {
         sprintf(sTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Vmp");
         iRet = MB_ResetPVReason(sBYVFile, sTmpFile);
      }
      if (iRet > 0)
      {
         sprintf(sDbName, "LDR%d", lLienYear);
         GetIniString("Data", "SqlValueFile", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acTmp1, acTmp, sDbName, myCounty.acCntyCode);
         lRecCnt = createValueImport(sBYVFile, acTmp1, false);
         if (lRecCnt > 0)
         {
            // Save output file for import
            strcpy(acValueFile, acTmp1);
            iRet = 0;
         } else
            iLoadFlag ^= EXTR_IVAL;                // Turn off import
      }
	}

   // Remove characteristics
   if (bClearChars)
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_CLR_CHARS, 0, true);

   // Create/Update cum sale file from Ama_Sales.txt
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Doc date - new input format YYYY-MM-DD 09/01/2011
      // Old format MM/DD/YYYY upto ldr 8/31/2011
      if (cDelim == ',')
         iRet = MB_CreateSCSale(MM_DD_YYYY_1, 1, 1, false);
      else
         //iRet = MB_CreateSCSale(YYYY_MM_DD, 1, 1, false);
         //iRet = MB_CreateSCSale(YYYY_MM_DD,1,1,false,(IDX_TBL5 *)&AMA_DocCode[0]);
         iRet = MB_CreateSCSale(YYYY_MM_DD,1,0,false,(IDX_TBL5 *)&AMA_DocCode[0]);  // 2016/07/15

     if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Load roll
   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      if (iLoadFlag & LOAD_LIEN)                   // -L
      {
         // Create Lien file
         LogMsg0("Load %s Lien roll", myCounty.acCntyCode);

         // LDR 2016-2018
         iRet = Ama_Load_LDR3(iSkip);
         
         // LDR 2011-
         //iRet = Ama_Load_LDR(iSkip);
         // LDR 2010
         //iRet = Ama_Load_LDR_Csv(iSkip);
      } else if (iLoadFlag & LOAD_UPDT)            // -U
      {
         LogMsg("Load %s roll update file", myCounty.acCntyCode);
         iRet = Ama_Load_Roll(iSkip);
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Ama_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }
 
   return iRet;
}