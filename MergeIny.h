#ifndef _MERGEINY_H
#define  _MERGEINY_H

#define TOFF_APN                       1-1
#define TOFF_BILLNUM                   11-1
#define TOFF_TOTALTAX                  17-1
#define TOFF_INSTAMT_1                 27-1
#define TOFF_INSTAMT_2                 37-1
#define TOFF_DEFAULTDT                 47-1
#define TOFF_DELQ_STATUS               55-1
#define TOFF_PAIDDT_1                  57-1
#define TOFF_PAIDDT_2                  63-1

#define TSIZ_APN                       10
#define TSIZ_BILLNUM                   6
#define TSIZ_TOTALTAX                  10
#define TSIZ_INSTAMT                   10
#define TSIZ_DEFAULTDT                 8
#define TSIZ_DELQ_STATUS               2
#define TSIZ_PAIDDT                    6

typedef struct _tCorTac
{
   char  Apn[TSIZ_APN];
   char  BillNum[TSIZ_BILLNUM];
   char  TotalTax[TSIZ_TOTALTAX];
   char  InstAmt1[TSIZ_INSTAMT];
   char  InstAmt2[TSIZ_INSTAMT];
   char  DefaultDt[TSIZ_DEFAULTDT];
   char  Delq_Status[TSIZ_DELQ_STATUS];
   char  PaidDt1[TSIZ_PAIDDT];
   char  PaidDt2[TSIZ_PAIDDT];
   char  CRLF[2];
} CORTAC;

//// 2020 Secured Roll.csv
//#define INY_L_TAXYEAR                      0
//#define INY_L_APN                          1
//#define INY_L_EVENTTYPE                    2
//#define INY_L_EVENTDATE                    3
//#define INY_L_ROLLCASTE                    4
//#define INY_L_ASMTTYPE                     5
//#define INY_L_TRANTYPE                     6
//#define INY_L_CHANGEREASON                 7
//#define INY_L_LAND                         8
//#define INY_L_IMPROVEMENTS                 9
//#define INY_L_LIVINGIMPROVEMENTS           10
//#define INY_L_PERSONALPROPERTY             11
//#define INY_L_TRADEFIXTURES                12
//#define INY_L_ASSESSEDVALUE                13
//#define INY_L_HOMEOWNEREXEMPTIONS          14
//#define INY_L_ALLOTHEREXEMPTIONS           15
//#define INY_L_PENALTY                      16
//#define INY_L_PREVIOUSNETTAXABLE           17
//#define INY_L_CURRENTTAXABLEVALUE          18
//#define INY_L_DIFFERENCEINNETTAXABLE       19
//#define INY_L_ASMTEVENTID                  20
//#define INY_L_ASMTID                       21
//#define INY_L_ASMTTRANID                   22
//#define INY_L_FLDS                         23

// 2020 Annual Assessments with TAG.CSV
//#define INY_L_APN                          0
//#define INY_L_YEARFROM                     1
//#define INY_L_YEARTO                       2
//#define INY_L_PROPTYPE                     3
//#define INY_L_ASMTTYPE                     4
//#define INY_L_TRA                          5
//#define INY_L_PROPDESC                     6
//#define INY_L_UNKNOWN                      7
//#define INY_L_OWNERX                       8     // Owner name - last name first
//#define INY_L_OWNER                        9
//#define INY_L_MADDR1                       10
//#define INY_L_MADDR2                       11
//#define INY_L_SADDR1                       12
//#define INY_L_SCITY                        13
//#define INY_L_SZIP                         14
//#define INY_L_SITUS                        15
//#define INY_L_X1                           16
//#define INY_L_X2                           17
//#define INY_L_HOEXE                        18
//#define INY_L_EXE1                         19
//#define INY_L_EXE2                         20
//#define INY_L_EXE3                         21
//#define INY_L_LAND                         22
//#define INY_L_IMPR                         23
//#define INY_L_PPVAL                        24
//#define INY_L_FIXTURES                     25
//#define INY_L_LIVINGIMPR                   26
//#define INY_L_ASSDVAL                      27
//#define INY_L_TOTALEXE                     28
//#define INY_L_NETVAL                       29
//#define INY_L_ASMTTRANSID                  30
//#define INY_L_ASMTID                       31
//#define INY_L_ASMTEVENTID                  32
//#define INY_L_FLDS                         33

// 2021 SR Certified.csv
#define INY_V_TAXYEAR                      0
#define INY_V_APN                          1
#define INY_V_TRA                          2
#define INY_V_EVENTTYPE                    3
#define INY_V_EVENTDATE                    4
#define INY_V_ROLLCASTE                    5
#define INY_V_ASMTTYPE                     6
#define INY_V_TRANTYPE                     7
#define INY_V_CHANGEREASON                 8
#define INY_V_LAND                         9
#define INY_V_IMPROVEMENTS                 10
#define INY_V_LIVINGIMPROVEMENTS           11
#define INY_V_PERSONALPROPERTY             12
#define INY_V_TRADEFIXTURES                13
#define INY_V_ASSESSEDVALUE                14
#define INY_V_HOMEOWNEREXEMPTIONS          15
#define INY_V_ALLOTHEREXEMPTIONS           16
#define INY_V_PENALTY                      17
#define INY_V_PREVIOUSNETTAXABLE           18
#define INY_V_CURRENTTAXABLEVALUE          19
#define INY_V_DIFFERENCEINNETTAXABLE       20
#define INY_V_ASMTEVENTID                  21
#define INY_V_ASMTID                       22
#define INY_V_ASMTTRANID                   23
#define INY_V_FLDS                         24

// 2024 - 2024 NECA Roll.txt
#define INY_L_APN                          0
#define INY_L_TAXYEAR                      1
#define INY_L_NEXTYEAR                     2
#define INY_L_ASMTTYPE                     3
#define INY_L_ROLLTYPE                     4
#define INY_L_TRA                          5
#define INY_L_USECODEDESC                  6
#define INY_L_COUNTY                       7        
#define INY_L_OWNER                        8    // Primary owner, last name first.  If blank, use recipient.
#define INY_L_RECIPIENT                    9    // Mail owner, first name first
#define INY_L_MADDR1                       10
#define INY_L_MADDR2                       11
#define INY_L_SADDR1                       12
#define INY_L_SCITY                        13
#define INY_L_SSTATE                       14
#define INY_L_SZIP                         15
#define INY_L_SITUS                        16
#define INY_L_TOTALMARKET                  17
#define INY_L_TOTALFBYV                    18
#define INY_L_HOEXE                        19
#define INY_L_DVX                          20
#define INY_L_LDVX                         21
#define INY_L_OTHEREXE                     22
#define INY_L_LAND                         23
#define INY_L_IMPR                         24
#define INY_L_PPVAL                        25
#define INY_L_FIXTURES                     26
#define INY_L_LIVINGIMPR                   27
#define INY_L_ASSDVAL                      28
#define INY_L_TOTALEXE                     29
#define INY_L_PENALTY                      30
#define INY_L_NETVAL                       31
#define INY_L_ASMTTRANID                   32
#define INY_L_ASMTID                       33
#define INY_L_REVOBJID                     34
#define INY_L_PRINTDATE                    35
#define INY_L_AMWCO                        36
#define INY_L_AMWC                         37
#define INY_L_AMC                          38
#define INY_L_AL                           39
#define INY_L_MC                           40
#define INY_L_MCWA                         41
#define INY_L_VALDIFFPCT                   42
#define INY_L_PERSEC2PRCL                  43
#define INY_L_EBCOMPLETE                   44
#define INY_L_FLDS                         45

// 2023 - 2023 NECA Roll.fix
//#define INY_L_APN                          0
//#define INY_L_TAXYEAR                      1
//#define INY_L_NEXTYEAR                     2
//#define INY_L_ASMTTYPE                     3
//#define INY_L_ROLLTYPE                     4
//#define INY_L_TRA                          5
//#define INY_L_USECODEDESC                  6
//#define INY_L_COUNTY                       7        
//#define INY_L_OWNER                        8    // Primary owner, last name first.  If blank, use recipient.
//#define INY_L_RECIPIENT                    9    // Mail owner, first name first
//#define INY_L_MADDR1                       10
//#define INY_L_MADDR2                       11
//#define INY_L_SADDR1                       12
//#define INY_L_SCITY                        13
//#define INY_L_SZIP                         14
//#define INY_L_SITUS                        15
//#define INY_L_TOTALMARKET                  16
//#define INY_L_TOTALFBYV                    17
//#define INY_L_HOEXE                        18
//#define INY_L_DVX                          19
//#define INY_L_LDVX                         20
//#define INY_L_OTHEREXE                     21
//#define INY_L_LAND                         22
//#define INY_L_IMPR                         23
//#define INY_L_PPVAL                        24
//#define INY_L_FIXTURES                     25
//#define INY_L_LIVINGIMPR                   26
//#define INY_L_ASSDVAL                      27
//#define INY_L_TOTALEXE                     28
//#define INY_L_PENALTY                      29
//#define INY_L_NETVAL                       30
//#define INY_L_ASMTTRANID                   31
//#define INY_L_ASMTID                       32
//#define INY_L_REVOBJID                     33
//#define INY_L_PRINTDATE                    34
//#define INY_L_AMWCO                        35
//#define INY_L_AMWC                         36
//#define INY_L_AMC                          37
//#define INY_L_AL                           38
//#define INY_L_MC                           39
//#define INY_L_MCWA                         40
//#define INY_L_VALDIFFPCT                   41
//#define INY_L_PERSEC2PRCL                  42
//#define INY_L_EBCOMPLETE                   43
//#define INY_L_FLDS                         44

// 2022 - NECA Roll.CSV
//#define INY_L_APN                          0
//#define INY_L_TAXYEAR                      1
//#define INY_L_NEXTYEAR                     2
//#define INY_L_ASMTTYPE                     3
//#define INY_L_ROLLTYPE                     4
//#define INY_L_TRA                          5
//#define INY_L_USECODEDESC                  6
//#define INY_L_COUNTY                       7        
//#define INY_L_OWNER                        8    // Primary owner, last name first.  If blank, use recipient.
//#define INY_L_RECIPIENT                    9    // Mail owner, first name first
//#define INY_L_MADDR1                       10
//#define INY_L_MADDR2                       11
//#define INY_L_SADDR1                       12
//#define INY_L_SCITY                        13
//#define INY_L_SZIP                         14
//#define INY_L_SITUS                        15
//#define INY_L_TOTALMARKET                  16
//#define INY_L_TOTALFBYV                    17
//#define INY_L_HOEXE                        18
//#define INY_L_DVX                          19
//#define INY_L_LDVX                         20
//#define INY_L_OTHEREXE                     21
//#define INY_L_LAND                         22
//#define INY_L_IMPR                         23
//#define INY_L_PPVAL                        24
//#define INY_L_FIXTURES                     25
//#define INY_L_LIVINGIMPR                   26
//#define INY_L_ASSDVAL                      27
//#define INY_L_TOTALEXE                     28
//#define INY_L_NETVAL                       29
//#define INY_L_ASMTTRANID                   30
//#define INY_L_ASMTID                       31
//#define INY_L_REVOBJID                     32
//#define INY_L_PRINTDATE                    33
//#define INY_L_AMWCO                        34
//#define INY_L_AMWC                         35
//#define INY_L_AMC                          36
//#define INY_L_AL                           37
//#define INY_L_MC                           38
//#define INY_L_MCWA                         39
//#define INY_L_VALDIFFPCT                   40
//#define INY_L_FLDS                         41

// 2021 Annual Assessment Roll.CSV
//#define INY_L_APN                          0
//#define INY_L_TAXYEAR                      1
//#define INY_L_NEXTYEAR                     2
//#define INY_L_ASMTTYPE                     3
//#define INY_L_ROLLTYPE                     4
//#define INY_L_TRA                          5
//#define INY_L_USECODEDESC                  6
//#define INY_L_OWNERX                       7        // Owner name - last name first, mostly blank
//#define INY_L_OWNER                        8
//#define INY_L_MADDR1                       9
//#define INY_L_MADDR2                       10
//#define INY_L_SADDR1                       11
//#define INY_L_SCITY                        12
//#define INY_L_SZIP                         13
//#define INY_L_SITUS                        14
//#define INY_L_TOTALMARKET                  15
//#define INY_L_TOTALFBYV                    16
//#define INY_L_HOEXE                        17
//#define INY_L_EXE1                         18
//#define INY_L_EXE2                         19
//#define INY_L_OTHEREXE                     20
//#define INY_L_LAND                         21
//#define INY_L_IMPR                         22
//#define INY_L_PPVAL                        23
//#define INY_L_FIXTURES                     24
//#define INY_L_LIVINGIMPR                   25
//#define INY_L_ASSDVAL                      26
//#define INY_L_TOTALEXE                     27
//#define INY_L_NETVAL                       28
//#define INY_L_REVOBJID                     29
//#define INY_L_ASMTTRANID                   30
//#define INY_L_ASMTID                       31
//#define INY_L_FLDS                         32

// AssessmentRollData.csv
//#define R_PIN                          0
//#define R_CLASSTYPE                    1
//#define R_USECODEDESC                  2
//#define R_DBA                          3
//#define R_OWNER                        4
//#define R_M_ADDRESS                    5
//#define R_M_CITY                       6
//#define R_M_STATE                      7
//#define R_M_ZIPCODE                    8
//#define R_S_STREETNUM                  9
//#define R_S_STREETSUB                  10
//#define R_S_STREETNAME                 11
//#define R_S_CITY                       12
//#define R_LEGAL                        13
//#define R_TRA                          14
//#define R_LOTTYPE                      15
//#define R_ACREAGE                      16
//#define R_ASSESSEE                     17
//#define R_LAND                         18
//#define R_IMPR                         19
//#define R_LIVINGIMPR                   20
//#define R_PPVALUE                      21
//#define R_FIXTURES                     22
//#define R_HOX                          23
//#define R_VET_EXE                      24
//#define R_DVET_EXE                     25
//#define R_CHURCH_EXE                   26
//#define R_REL_EXE                      27
//#define R_CEMETERY_EXE                 28
//#define R_PUBSCHOOL_EXE                29
//#define R_PUBLIBRAY_EXE                30
//#define R_PUBMUSEUM_EXE                31
//#define R_WELCOLL_EXE                  32
//#define R_WELHOSP_EXE                  33
//#define R_WELPRISCH_EXE                34
//#define R_WELCHARREL_EXE               35
//#define R_OTHER_EXE                    36
//#define R_FLDS                         37

// 2020 - AssessmentRoll.csv
#define R_PIN                          0
#define R_DOCNUM                       1
#define R_DOCMONTH                     2
#define R_DOCYEAR                      3
#define R_CLASSDESC                    4
#define R_CLASSTYPE                    5
#define R_BASEYEAR                     6
#define R_USECODEDESC                  7
#define R_DBA                          8
#define R_MAILNAME                     9
#define R_M_ADDRESS                    10
#define R_M_CITY                       11
#define R_M_STATE                      12
#define R_M_ZIPCODE                    13
#define R_S_STREETNUM                  14
#define R_S_STREETSUB                  15
#define R_S_DIR                        16
#define R_S_STREETNAME                 17
#define R_S_STREETTYPE                 18
#define R_S_UNITNUM                    19
#define R_S_ZIPCODE                    20
#define R_S_CITY                       21
#define R_LEGAL                        22
#define R_TAXABILITYCODE               23
#define R_TRA                          24
#define R_GEO                          25
#define R_RECORDERSTYPE                26
#define R_RECORDERSBOOK                27
#define R_RECORDERSPAGE                28
#define R_LOTTYPE                      29
#define R_LOT                          30
#define R_BLOCKNUMBER                  31
#define R_PORTIONDIRECTION             32
#define R_SUBDIVISIONNAMEORTRACT       33
#define R_ACREAGE                      34
#define R_FIRSTSECTION                 35
#define R_FIRSTTOWNSHIP                36
#define R_FIRSTRANGE                   37
#define R_FIRSTRANGEDIRECTION          38
#define R_LEGALPARTY1                  39
#define R_LEGALPARTY2                  40
#define R_LEGALPARTY3                  41
#define R_LEGALPARTY4                  42
#define R_LAND                         43
#define R_IMPR                         44
#define R_LIVINGIMPR                   45
#define R_PPVALUE                      46
#define R_FIXTURES                     47
#define R_PPAPPRAISED                  48
#define R_ASSESSEDPENALTY              49
#define R_HOX                          50
#define R_VET_EXE                      51
#define R_DVET_EXE                     52
#define R_CHURCH_EXE                   53
#define R_REL_EXE                      54
#define R_CEMETERY_EXE                 55
#define R_PUBSCHOOL_EXE                56
#define R_PUBLIBRAY_EXE                57
#define R_PUBMUSEUM_EXE                58
#define R_WELCOLL_EXE                  59
#define R_WELHOSP_EXE                  60
#define R_WELPRISCH_EXE                61
#define R_WELCHARREL_EXE               62
#define R_OTHER_EXE                    63
#define R_CAMEFROM                     64
#define R_CLASSCODE                    65 // new 04/26/2022
#define R_FLDS                         66

// SaleData.csv
//#define S_PIN                          0
//#define S_GEO                          1
//#define S_DOCMONTH                     2
//#define S_DOCYEAR                      3
//#define S_DOCNUM                       4
//#define S_DOCDATE                      5
//#define S_DOCTYPE                      6
//#define S_SALEPRICE                    7
//#define S_USECODE                      8
//#define S_BASEYEAR                     9
//#define S_S_LINE1                      10
//#define S_S_LINE2                      11
//#define S_ACRES                        12
//#define S_BLDGAREA                     13
//#define S_EFF_AGE                      14
//#define S_CARPORT                      15
//#define S_ATTACHED                     16
//#define S_DETACHED                     17
//#define S_POOL                         18
//#define S_BEDROOMS                     19
//#define S_BATH_H                       20
//#define S_BATH_3Q                      21
//#define S_BATH_F                       22
//#define S_TOTALBATHS                   23
//#define S_LAND                         24
//#define S_STRUCTURE                    25
//#define S_FLDS                         26

// 2020 - Sales.csv
#define S_PIN                          0
#define S_GEO                          1
#define S_DOCMONTH                     2
#define S_DOCYEAR                      3
#define S_DOCNUM                       4
#define S_DOCDATE                      5
#define S_SENTDATE                     6
#define S_SALEDATE                     7
#define S_DOCTYPE                      8
#define S_SALEPRICE                    9
#define S_ADJPRICE                     10
#define S_USECODE                      11
#define S_BASEYEAR                     12
#define S_BASEYEAR_FLG                 13
#define S_SITUS_LINE1                  14
#define S_SITUS_LINE2                  15
#define S_ACRES                        16    // LotAcres
#define S_DESIGN_TYPE                  17
#define S_CONST_TYPE                   18
#define S_AREA_1_1                     19
#define S_COSTAREA                     20
#define S_BLDGAREA                     21
#define S_EFF_AGE                      22
#define S_CARPORT                      23
#define S_ATTACHED                     24
#define S_DETACHED                     25
#define S_POOL                         26    // Pool
#define S_BEDROOMS                     27
#define S_BATH_H                       28
#define S_BATH_3Q                      29
#define S_BATH_F                       30
#define S_TOTALBATHS                   31
//#define S_LAND                         32
//#define S_STRUCTURE                    33
// 04/26/2022
#define S_SPECADJUSTMENT               32
#define S_LANDVAL                      33
#define S_IMPRVAL                      34
#define S_TREEVINEVAL                  35
#define S_TRANSFERFEE                  36
#define S_FLDS                         37

// CharacteristicsData.csv
//#define C_PIN                          0
//#define C_S_STRNUM                     1
//#define C_S_STRNAME                    2
//#define C_S_ZIPCODE                    3
//#define C_S_CITYNAME                   4
//#define C_EFFYRBLT                     5
//#define C_STORIES                      6
//#define C_ROOFTYPE                     7
//#define C_GARAGETYPE                   8
//#define C_GARAGEAREA                   9
//#define C_CARPORTSIZE                  10
//#define C_GARAGE2TYPE                  11
//#define C_GARAGE2AREA                  12
//#define C_CARPORT2SIZE                 13
//#define C_HASFIREPLACE                 14
//#define C_BATHSFULL                    15
//#define C_BATHS3_4                     16
//#define C_BATHSHALF                    17
//#define C_TOTALBATHS                   18
//#define C_HASCENTRALHEATING            19
//#define C_HASCENTRALCOOLING            20
//#define C_DESIGNTYPE                   21
//#define C_CONSTRUCTIONTYPE             22
//#define C_QUALITYCODE                  23
//#define C_SHAPECODE                    24
//#define C_LIVINGAREA                   25
//#define C_ACTUALAREA                   26
//#define C_HASPOOL                      27
//#define C_BEDROOMCOUNT                 28
//#define C_FLDS                         29

// 2020 - Characteristics.csv
//#define C_PIN                          0
//#define C_S_STRNUM                     1
//#define C_S_STREETSUB                  2
//#define C_S_DIR                        3
//#define C_S_STRNAME                    4
//#define C_S_STREETTYPE                 5
//#define C_S_UNITNUM                    6
//#define C_S_ZIPCODE                    7
//#define C_S_CITY                       8
//#define C_STREETSURFACED               9
//#define C_SEWER                        10    // No data
//#define C_DOMESTICWATER                11    // No data
//#define C_IRRIGATIONWATER              12    // No data
//#define C_WELLWATER                    13    // No data
//#define C_UTILITYELECTRIC              14    // No data
//#define C_UTILITYGAS                   15    // No data
//#define C_EFFYRBLT                     16    // 1905? - should use effective age from sale file
//#define C_STORIES                      17    // 9v99
//#define C_ROOFTYPE                     18
//#define C_GARAGETYPE                   19    // Attached Garage/Detached Garage/Storage
//#define C_GARAGEAREA                   20
//#define C_CARPORTSIZE                  21
//#define C_GARAGE2TYPE                  22
//#define C_GARAGE2AREA                  23
//#define C_CARPORT2SIZE                 24
//#define C_HASFIREPLACE                 25    // Y/N
//#define C_BATHSFULL                    26
//#define C_BATHS3_4                     27
//#define C_BATHSHALF                    28
//#define C_TOTALBATHS                   29
//#define C_HASCENTRALHEATING            30
//#define C_HASCENTRALCOOLING            31
//#define C_DESIGNTYPE                   32
//#define C_CONSTRUCTIONTYPE             33
//#define C_QUALITYCODE                  34
//#define C_SHAPECODE                    35
//#define C_LIVINGAREA                   36
//#define C_ACTUALAREA                   37
//#define C_HASPOOL                      38
//#define C_BEDROOMCOUNT                 39
//#define C_FAIRWAY                      40
//#define C_WATERFRONT                   41
//#define C_FLDS                         42

// 2022
#define  C_APN                                     0
#define  C_CLASSCODEDESCRIPTION                    1
#define  C_SITUSSTREETNUMBER                       2
#define  C_SITUSSTREETNUMBERSUFFIX                 3
#define  C_SITUSSTREETPREDIRECTIONAL               4
#define  C_SITUSSTREETNAME                         5
#define  C_SITUSSTREETTYPE                         6
#define  C_SITUSUNITNUMBER                         7
#define  C_SITUSZIPCODE                            8
#define  C_SITUSCITYNAME                           9
#define  C_STREETSURFACED                          10
#define  C_SEWER                                   11
#define  C_DOMESTICWATER                           12
#define  C_IRRIGATIONWATER                         13
#define  C_WELLWATER                               14
#define  C_UTILITYELECTRIC                         15
#define  C_UTILITYGAS                              16
#define  C_EFFYEARBUILT                            17
#define  C_YEARBUILT                               18
#define  C_NUMBEROFSTORIES                         19
#define  C_ROOFTYPE                                20
#define  C_GARAGETYPE                              21
#define  C_GARAGEAREA                              22
#define  C_CARPORTSIZE                             23
#define  C_GARAGE2TYPE                             24
#define  C_GARAGE2AREA                             25
#define  C_CARPORT2SIZE                            26
#define  C_HASFIREPLACE                            27
#define  C_BATHSFULL                               28
#define  C_BATHS3_4                                29
#define  C_BATHSHALF                               30
#define  C_TOTALBATHS                              31
#define  C_HASCENTRALHEATING                       32
#define  C_HASCENTRALCOOLING                       33
#define  C_DESIGNTYPE                              34
#define  C_CONSTRUCTIONTYPE                        35
#define  C_QUALITYCODE                             36
#define  C_SHAPECODE                               37
#define  C_LIVINGAREA                              38
#define  C_ACTUALAREA                              39
#define  C_HASPOOL                                 40
#define  C_BEDROOMCOUNT                            41
#define  C_FAIRWAY                                 42
#define  C_WATERFRONT                              43
#define  C_ACREAGE                                 44    
#define  C_LANDUNITTYPE                            45
#define  C_COLS                                    46

// CA-Inyo-CurrentAmounts.csv
#define  CA_PIN                                    0
#define  CA_BILLNUMBER                             1
#define  CA_TAXBILLID                              2
#define  CA_INST1DUEDATE                           3
#define  CA_INST2DUEDATE                           4
#define  CA_INST1TAXCHARGE                         5
#define  CA_INST1PENALTYCHARGE                     6
#define  CA_INST1FEECHARGE                         7
#define  CA_INST1INTERESTCHARGE                    8
#define  CA_INST1TAXPAYMENT                        9         // Paid value as negative
#define  CA_INST1PENALTYPAYMENT                    10
#define  CA_INST1FEEPAYMENT                        11
#define  CA_INST1INTERESTPAYMENT                   12
#define  CA_INST1AMOUNTDUE                         13
#define  CA_INST2TAXCHARGE                         14
#define  CA_INST2PENALTYCHARGE                     15
#define  CA_INST2FEECHARGE                         16
#define  CA_INST2INTERESTCHARGE                    17
#define  CA_INST2TAXPAYMENT                        18
#define  CA_INST2PENALTYPAYMENT                    19
#define  CA_INST2FEEPAYMENT                        20
#define  CA_INST2INTERESTPAYMENT                   21
#define  CA_INST2AMOUNTDUE                         22
#define  CA_FLDS                                   23
#define  CA_COLS                                   23

// CA-Mendo-CurrentDistricts.csv
#define  CD_PIN                                    0
#define  CD_TAXRATEAREA                            1
#define  CD_TAXYEAR                                2
#define  CD_BILLNUMBER                             3
#define  CD_BILLTYPE                               4
#define  CD_TAXBILLID                              5
#define  CD_DETAILS                                6
#define  CD_TAXCODE                                7
#define  CD_RATE                                   8
#define  CD_TOTAL                                  9
#define  CD_FLDS                                   10
#define  CD_COLS                                   10

// CA-Mendo-PriorYear.csv
#define  PY_PIN                                    0
#define  PY_DEFAULTNUMBER                          1
#define  PY_DEFAULTYEAR                            2
#define  PY_BILLTYPE                               3
#define  PY_AMOUNTDUE                              4
#define  PY_FLDS                                   5
#define  PY_COLS                                   5

static XLAT_CODE  asRoofType[] =
{
   // Value, lookup code, value length
   "FLAT",     "F", 3,      // Flat
   "GABLE",    "G", 3,      // Gable
   "GAMBREL",  "N", 3,      // Gambrel
   "HIP",      "H", 3,      // Hip
   "MANSARD",  "M", 3,      // Mansard
   "OTHER",    "J", 3,      // Others
   "",   "",  0
};

static XLAT_CODE  asConstType[] =
{
   // Value, lookup code, value length
   "MASONRY BEARING WALLS", "C", 2,
   "PRE-ENGINEERED STEEL",  "A", 2,    // Steel frame
   "WOOD OR LIGHT STEEL",   "Z", 2,
   "FIREPROOF STRUCTURAL",  "A", 2,    // Steel frame
   "REINFORCED CONCRETE",   "B", 2,
   "",   "",  0
};

#define T_PIN                          0
#define T_TAG                          1
#define T_BILLNUMBER                   2
#define T_CLASSCD                      3
#define T_PRIMARYOWNER                 4
#define T_OWNER2                       5
#define T_DELIVERYADDR                 6
#define T_LASTLINE                     7
#define T_SITUS                        8
#define T_LAND                         9
#define T_IMPROVEMENT                  10
#define T_PERSPROPERTY                 11
#define T_FIXTURES                     12
#define T_EXEMPTION                    13
#define T_ACREAGE                      14
#define T_TOTALTAX                     15
#define T_FUNDNAME01                   16
#define T_FUNDRATE01                   17
#define T_FUNDTAX01                    18
#define T_FUNDNAME02                   19
#define T_FUNDRATE02                   20
#define T_FUNDTAX02                    21
#define T_FUNDNAME03                   22
#define T_FUNDRATE03                   23
#define T_FUNDTAX03                    24
#define T_FUNDNAME04                   25
#define T_FUNDRATE04                   26
#define T_FUNDTAX04                    27
#define T_FUNDNAME05                   28
#define T_FUNDRATE05                   29
#define T_FUNDTAX05                    30
#define T_FUNDNAME06                   31
#define T_FUNDRATE06                   32
#define T_FUNDTAX06                    33
#define T_FUNDNAME07                   34
#define T_FUNDRATE07                   35
#define T_FUNDTAX07                    36
#define T_FUNDNAME08                   37
#define T_FUNDRATE08                   38
#define T_FUNDTAX08                    39
#define T_FUNDNAME09                   40
#define T_FUNDRATE09                   41
#define T_FUNDTAX09                    42
#define T_FUNDNAME10                   43
#define T_FUNDRATE10                   44
#define T_FUNDTAX10                    45
#define T_FUNDNAME11                   46
#define T_FUNDRATE11                   47
#define T_FUNDTAX11                    48
#define T_FUNDNAME12                   49
#define T_FUNDRATE12                   50
#define T_FUNDTAX12                    51
#define T_FUNDNAME13                   52
#define T_FUNDRATE13                   53
#define T_FUNDTAX13                    54
#define T_FUNDNAME14                   55
#define T_FUNDRATE14                   56
#define T_FUNDTAX14                    57
#define T_FUNDNAME15                   58
#define T_FUNDRATE15                   59
#define T_FLDS                         60

IDX_TBL4 Iny_Vesting[] =
{
   " TRUST,",        "TR", 5, 2,
   " TRUST ",        "TR", 5, 2,
   " TR,",           "TR", 4, 2,
   " TRS ",          "TR", 4, 2,
   " TRST,",         "TR", 4, 2,
   " LIFE ESTATE,",  "LE", 9, 2,
   " LIFE EST,",     "LE", 9, 2,
   " ESTATE OF,",    "LE", 7, 2,
   " ESTATE OF",     "LE", 7, 2,
   " EST OF",        "LE", 7, 2,
   " M D,",          "MD", 4, 2,
   " MD,",           "MD", 4, 2,
   " MD INC,",       "MD", 4, 2,
   " RPT,",          "RP", 4, 2,
   " BENF",          "BE", 6, 2,
   " ETAL",          "EA", 5, 2,
   " ET AL",         "EA", 6, 2,
   " TRUSTEE",       "TE", 7, 2,
   " CSD",           "CS", 6, 2,
   " PROPERTY TRST", "PR", 10,2,
   "", "", 0, 0
};

// If not match Vesting table, check last word using this table
IDX_TBL4 Iny_VestingLW[] =
{
   " L EST",      "LE", 6, 2,
   " ESTATE",     "LE", 7, 2,
   " HWCP",       "CP", 5, 2,
   " HWJT",       "JT", 5, 2,
   " TR",         "TR", 3, 2,
   " CP",         "CP", 3, 2,
   " JT",         "JT", 3, 2,
   " TC",         "TC", 3, 2,
   "", "", 0, 0
};

IDX_TBL5 INY_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "01", "1 ", 'N', 2, 2,     // GENERAL TRANSFER
   "02", "74", 'N', 2, 2,     // TO PERFECT/CLEAR TITLE
   "03", "57", 'N', 2, 2,     // PARTIAL INTEREST
   "04", "13", 'Y', 2, 2,     // DISTRIBUTE ESTATE
   "05", "74", 'Y', 2, 2,     // DEATH OF OWNER
   "06", "13", 'Y', 2, 2,     // PROP-60 BYV TRANSFER
   "07", "74", 'Y', 2, 2,     // OPTION
   "08", "11", 'N', 2, 2,     // CONTRACT OF SALE
   "09", "74", 'Y', 2, 2,     // COMPLETION CONTRACT
   "10", "74", 'Y', 2, 2,     // NEW TENANT
   "11", "74", 'Y', 2, 2,     // TENANT TO VACANT
   "19", "74", 'Y', 2, 2,     // CORP CONTROL
   "20", "74", 'Y', 2, 2,     // PARTNERSHIP
   "25", "67", 'N', 2, 2,     // TAX DEED
   "26", "77", 'N', 2, 2,     // FORECLOSURE/TRUSTEE SALE
   "27", "78", 'N', 2, 2,     // IN LIEU FORECLOSURE
   "28", "51", 'Y', 2, 2,     // NAME CHANGE
   "29", "44", 'Y', 2, 2,     // NEW/SUB-LEASE
   "30", "44", 'Y', 2, 2,     // LEASE ASSIGNMENT
   "31", "44", 'Y', 2, 2,     // LEASE ACTIVITY
   "32", "74", 'Y', 2, 2,     // END/NEW  ANTICIPATED TERM
   "33", "44", 'Y', 2, 2,     // MEMORANDUM OF LEASE
   "34", "74", 'Y', 2, 2,     // NEW ANTICIPATED TERM
   "35", "44", 'Y', 2, 2,     // LEASE /OPTION TO BUY
   "36", "44", 'Y', 2, 2,     // LEASE/OPT EXPIRES
   "37", "44", 'Y', 2, 2,     // TERMINATION LEASE
   "39", "74", 'Y', 2, 2,     // LSR INT W/ 35+ YR REMAIN
   "40", "74", 'Y', 2, 2,     // INTO LIFE ESTATE
   "41", "74", 'Y', 2, 2,     // DEATH LIFE TENANT- PROP 58
   "42", "74", 'Y', 2, 2,     // REVOCATION OF LIFE ESTATE
   "49", "6 ", 'Y', 2, 2,     // AFFIDAVIT DEATH
   "50", "13", 'Y', 2, 2,     // OTHER INTERESTS
   "51", "74", 'Y', 2, 2,     // MISC REC DOCS
   "60", "18", 'Y', 2, 2,     // INTERSPOUSAL
   "61", "18", 'Y', 2, 2,     // INTERSPOUSAL DIVORCE
   "62", "74", 'Y', 2, 2,     // DEATH- PROP58 GRANTED
   "63", "74", 'Y', 2, 2,     // DEATH OF SPOUSE
   "64", "13", 'Y', 2, 2,     // PARENT/CHILD TRANSFER
   "65", "13", 'Y', 2, 2,     // GRANDPARENT/GRANDCHILD
   "66", "74", 'Y', 2, 2,     // TAX POSTPONMENT
   "67", "74", 'Y', 2, 2,     // REALEASE TAX POSTPONE
   "70", "57", 'Y', 2, 2,     // NO CHANGE PROPORTIONAL INT
   "72", "3 ", 'Y', 2, 2,     // PARTITION TENANCY IN COMMON
   "74", "3 ", 'Y', 2, 2,     // TERMINATE ORIG JT INT
   "75", "3 ", 'Y', 2, 2,     // CREATE JT INCL ONE JT
   "76", "3 ", 'Y', 2, 2,     // TERMINATION JT TO ORIG
   "77", "74", 'Y', 2, 2,     // < 5% INTEREST AND < $10,000
   "80", "74", 'Y', 2, 2,     // INTO TRUST
   "81", "74", 'Y', 2, 2,     // INTO/OUT OF TRUST
   "82", "74", 'Y', 2, 2,     // OUT OF TRUST
   "83", "74", 'Y', 2, 2,     // CHANGE  TRUSTEES
   "85", "40", 'Y', 2, 2,     // CONSERVATION ESMT
   "86", "74", 'Y', 2, 2,     // SEPERATE ASSMT
   "87", "74", 'Y', 2, 2,     // NOTICE OF VIOLATION
   "88", "74", 'Y', 2, 2,     // ANNEXATION
   "89", "74", 'Y', 2, 2,     // ROAD ABANDONMENT
   "90", "74", 'Y', 2, 2,     // LOT LINE- DEED
   "91", "74", 'Y', 2, 2,     // PATENT
   "92", "74", 'Y', 2, 2,     // PARCEL/TRACT MAP/CONDO PLAN
   "93", "74", 'Y', 2, 2,     // CERTIFICATE OF COMPLIANCE
   "94", "74", 'Y', 2, 2,     // MH ON FOUNDATION
   "95", "74", 'Y', 2, 2,     // NEW SET UP
   "96", "74", 'Y', 2, 2,     // RECORD OF SURVEY
   "97", "40", 'Y', 2, 2,     // EASEMENTS
   "98", "74", 'Y', 2, 2,     // MERGER/COMBINATION
   "99", "74", 'Y', 2, 2,     // PARCEL SPLIT
   "","",0,0,0
};

IDX_TBL4 INY_Exemption[] = 
{
   "E01", "H", 3,1,
   "E40", "D", 3,1,
   "E90", "X", 3,1,
   "","",0,0
};

#endif
