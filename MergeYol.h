#ifndef  _MERGEYOL_H_
#define  _MERGEYOL_H_

// 07/21/2021
#define  L15_TAXYEAR                   0
#define  L15_ROLLCATEGORY              1
#define  L15_ASMT                      2
#define  L15_FEEPARCEL                 3
#define  L15_ASMTSTATUS                4
#define  L15_TRA                       5
#define  L15_TAXABILITYFULL            6
#define  L15_OWNER                     7
#define  L15_ASSESSEE                  8
#define  L15_MAILADDRESS1              9
#define  L15_MAILADDRESS2              10
#define  L15_MAILADDRESS3              11
#define  L15_MAILADDRESS4              12
#define  L15_BARCODEZIP                13
#define  L15_SITUS1                    14
#define  L15_SITUS2                    15
#define  L15_PARCELDESCRIPTION         16
#define  L15_LANDVALUE                 17
#define  L15_STRUCTUREVALUE            18
#define  L15_FIXTURESVALUE             19
#define  L15_GROWING                   20
#define  L15_FIXTURESRP                21
#define  L15_MHPPVALUE                 22
#define  L15_PPVALUE                   23
#define  L15_HOX                       24
#define  L15_OTHEREXEMPTION            25
#define  L15_OTHEREXEMPTIONCODE        26
#define  L15_LANDUSE1                  27
#define  L15_LANDSIZE                  28
#define  L15_ACRES                     29
#define  L15_CURRENTDOCNUM             30
#define  L15_CURRENTDOCDATE            31
#define  L15_ISAGPRESERVE              32
#define  L15_FLDS                      33

// 07/13/2015
#define  YOL_CHAR_FEEPARCEL            0
#define  YOL_CHAR_POOLSPA              1
#define  YOL_CHAR_CATTYPE              2
#define  YOL_CHAR_QUALITYCLASS         3
#define  YOL_CHAR_YRBLT                4
#define  YOL_CHAR_BUILDINGSIZE         5
#define  YOL_CHAR_ATTACHGARAGESF       6
#define  YOL_CHAR_DETACHGARAGESF       7
#define  YOL_CHAR_CARPORTSF            8
#define  YOL_CHAR_HEATING              9
#define  YOL_CHAR_COOLINGCENTRALAC     10
#define  YOL_CHAR_COOLINGEVAPORATIVE   11
#define  YOL_CHAR_COOLINGROOMWALL      12
#define  YOL_CHAR_COOLINGWINDOW        13
#define  YOL_CHAR_STORIESCNT           14
#define  YOL_CHAR_UNITSCNT             15
#define  YOL_CHAR_TOTALROOMS           16
#define  YOL_CHAR_EFFYR                17
#define  YOL_CHAR_PATIOSF              18
#define  YOL_CHAR_BEDROOMS             19
#define  YOL_CHAR_BATHROOMS            20
#define  YOL_CHAR_HALFBATHS            21
#define  YOL_CHAR_FIREPLACE            22
#define  YOL_CHAR_ASMT                 23
#define  YOL_CHAR_BLDGSEQNUM           24
#define  YOL_CHAR_HASWELL              25
#define  YOL_CHAR_LANDSQFT             26
#define  YOL_CHAR_PARKSPACES           27

/*
#define  YOL_CHAR_FEEPRCL                 0
#define  YOL_CHAR_DOCNUM                  1
#define  YOL_CHAR_CATTYPE                 2
#define  YOL_CHAR_BLDGSEQNO               3
#define  YOL_CHAR_UNITSEQNO               4
#define  YOL_CHAR_YRBLT                   5
#define  YOL_CHAR_BLDGTYPE                6
#define  YOL_CHAR_EFFYR                   7
#define  YOL_CHAR_BUILDINGSIZE            8
#define  YOL_CHAR_BUILDINGUSEDFOR         9
#define  YOL_CHAR_STORIESCNT              10
#define  YOL_CHAR_UNITSCNT                11
#define  YOL_CHAR_CONDITION               12
#define  YOL_CHAR_BEDROOMS                13
#define  YOL_CHAR_BATHROOMS               14
#define  YOL_CHAR_QUALITYCLASS            15
#define  YOL_CHAR_HALFBATHS               16
#define  YOL_CHAR_CONSTRUCTION            17
#define  YOL_CHAR_FOUNDATION              18
#define  YOL_CHAR_STRUCTURALFRAME         19
#define  YOL_CHAR_STRUCTURALFLOOR         20
#define  YOL_CHAR_EXTERIORTYPE            21
#define  YOL_CHAR_ROOFCOVER               22
#define  YOL_CHAR_ROOFTYPEFLAT            23
#define  YOL_CHAR_ROOFTYPEHIP             24
#define  YOL_CHAR_ROOFTYPEGABLE           25
#define  YOL_CHAR_ROOFTYPESHED            26
#define  YOL_CHAR_INSULATIONCEILINGS      27
#define  YOL_CHAR_INSULATIONWALLS         28
#define  YOL_CHAR_INSULATIONFLOORS        29
#define  YOL_CHAR_WINDOWPANESINGLE        30
#define  YOL_CHAR_WINDOWPANEDOUBLE        31
#define  YOL_CHAR_WINDOWPANETRIPLE        32
#define  YOL_CHAR_WINDOWTYPE              33
#define  YOL_CHAR_LIGHTING                34
#define  YOL_CHAR_COOLINGCENTRALAC        35
#define  YOL_CHAR_COOLINGEVAPORATIVE      36
#define  YOL_CHAR_COOLINGROOMWALL         37
#define  YOL_CHAR_COOLINGWINDOW           38
#define  YOL_CHAR_HEATING                 39
#define  YOL_CHAR_FIREPLACE               40
#define  YOL_CHAR_GARAGE                  41
#define  YOL_CHAR_PLUMBING                42
#define  YOL_CHAR_SOLAR                   43
#define  YOL_CHAR_BUILDER                 44
#define  YOL_CHAR_BLDGDESIGNEDFOR         45
#define  YOL_CHAR_MODELDESC               46
#define  YOL_CHAR_UNFINAREASSF            47
#define  YOL_CHAR_ATTACHGARAGESF          48
#define  YOL_CHAR_DETACHGARAGESF          49
#define  YOL_CHAR_CARPORTSF               50
#define  YOL_CHAR_DECKSSF                 51
#define  YOL_CHAR_PATIOSF                 52
#define  YOL_CHAR_CEILINGHEIGHT           53
#define  YOL_CHAR_FIRESPINKLERS           54
#define  YOL_CHAR_AVGWALLHEIGHT           55
#define  YOL_CHAR_BAY                     56
#define  YOL_CHAR_DOCK                    57
#define  YOL_CHAR_ELEVATOR                58
#define  YOL_CHAR_ESCALATOR               59
#define  YOL_CHAR_ROLLUPDOOR              60
#define  YOL_CHAR_FIELD1                  61
#define  YOL_CHAR_FIELD2                  62
#define  YOL_CHAR_FIELD3                  63
#define  YOL_CHAR_FIELD4                  64
#define  YOL_CHAR_FIELD5                  65
#define  YOL_CHAR_NUMFIREPLACE            66
#define  YOL_CHAR_FIELD7                  67
#define  YOL_CHAR_FIELD8                  68
#define  YOL_CHAR_FIELD9                  69
#define  YOL_CHAR_FIELD10                 70
#define  YOL_CHAR_FIELD11                 71
#define  YOL_CHAR_FIELD12                 72
#define  YOL_CHAR_FIELD13                 73
#define  YOL_CHAR_FIELD14                 74
#define  YOL_CHAR_FIELD15                 75
#define  YOL_CHAR_FIELD16                 76
#define  YOL_CHAR_FIELD17                 77
#define  YOL_CHAR_FIELD18                 78
#define  YOL_CHAR_FIELD19                 79
#define  YOL_CHAR_FIELD20                 80
#define  YOL_CHAR_LANDFIELD1              81
#define  YOL_CHAR_LANDFIELD2              82
#define  YOL_CHAR_LANDFIELD3              83
#define  YOL_CHAR_LANDFIELD4              84
#define  YOL_CHAR_LANDFIELD5              85
#define  YOL_CHAR_LANDFIELD6              86
#define  YOL_CHAR_LANDFIELD7              87
#define  YOL_CHAR_LANDFIELD8              88
#define  YOL_CHAR_ACRES                   89
#define  YOL_CHAR_NEIGHBORHOODCODE        90
#define  YOL_CHAR_ZONING                  91
#define  YOL_CHAR_TOPOGRAPHY              92
#define  YOL_CHAR_VIEWCODE                93
#define  YOL_CHAR_POOLSPA                 94
#define  YOL_CHAR_WATERSOURCE             95
#define  YOL_CHAR_SUBDIVNAME              96
#define  YOL_CHAR_SEWERCODE               97
#define  YOL_CHAR_UTILITIESCODE           98
#define  YOL_CHAR_ACCESSCODE              99
#define  YOL_CHAR_LANDSCAPE               100
#define  YOL_CHAR_PROBLEMCODE             101
#define  YOL_CHAR_FRONTAGE                102
#define  YOL_CHAR_LOCATION                103
#define  YOL_CHAR_PLANTEDACRES            104
#define  YOL_CHAR_ACRESUNUSEABLE          105
#define  YOL_CHAR_WATERSOURCEDOMESTIC     106
#define  YOL_CHAR_WATERSOURCEIRRIGATION   107
#define  YOL_CHAR_HOMESITES               108
#define  YOL_CHAR_PROPERTYCONDITIONCODE   109
#define  YOL_CHAR_ROADTYPE                110
#define  YOL_CHAR_HASWELL                 111
#define  YOL_CHAR_HASCOUNTYROAD           112
#define  YOL_CHAR_HASVINEYARD             113
#define  YOL_CHAR_ISUNSECUREDBUILDING     114
#define  YOL_CHAR_HASORCHARD              115
#define  YOL_CHAR_HASGROWINGIMPRV         116
#define  YOL_CHAR_SITECOVERAGE            117
#define  YOL_CHAR_PARKINGSPACES           118
#define  YOL_CHAR_EXCESSLANDSF            119
#define  YOL_CHAR_FRONTFOOTAGESF          120
#define  YOL_CHAR_MULTIPARCELECON         121
#define  YOL_CHAR_LANDUSECODE1            122
#define  YOL_CHAR_LANDUSECODE2            123
#define  YOL_CHAR_LANDSQFT                124
#define  YOL_CHAR_TOTALROOMS              125
#define  YOL_CHAR_NETLEASABLESF           126
#define  YOL_CHAR_BLDGFOOTPRINTSF         127
#define  YOL_CHAR_OFFICESPACESF           128
#define  YOL_CHAR_NONCONDITIONSF          129
#define  YOL_CHAR_MEZZANINESF             130
#define  YOL_CHAR_PERIMETERLF             131
#define  YOL_CHAR_ASMT                    132
#define  YOL_CHAR_ASMTCATEGORY            133
#define  YOL_CHAR_EVENTDATE               134
#define  YOL_CHAR_SALESPRICE              135      // Confirmed
#define  YOL_CHAR_CONFIRMATIONCODE        136
*/

static XLAT_CODE  asBldgType[] =
{
   // Value, lookup code, value length
   "01", " ", 2,               // Residential
   "02", " ", 2,               // Commercial
   "03", " ", 2,               // Industrial
   "04", " ", 2,               // Agricultural
   "",   "",  0
};

static XLAT_CODE  asBldgUsedFor[] = 
{
   // Value, lookup code, value length
   "01", " ", 2,               // SFR
   "02", " ", 2,               // RES. Condo
   "03", " ", 2,               // RES. Multi
   "04", " ", 2,               // MFG Home
   "05", " ", 2,               // 
   "06", " ", 2,               // Office
   "07", " ", 2,               // Bank
   "08", " ", 2,               // Restaurant
   "09", " ", 2,               // Market
   "10", " ", 2,               // Gas Station
   "11", " ", 2,               // Mini Storage
   "12", " ", 2,               // Medical Office
   "13", " ", 2,               // Hospital
   "14", " ", 2,               // Auto sales
   "15", " ", 2,               // Church
   "16", " ", 2,               // Industrial
   "17", " ", 2,               // Light Ind.
   "19", " ", 2,               // Winery
   "20", " ", 2,               // Retail
   "",   "",  0
};

static XLAT_CODE  asCond[] =
{
   // Value, lookup code, value length
   "1", "E", 1,               // Excellent
   "2", "G", 1,               // Good
   "3", "A", 1,               // Average
   "4", "F", 1,               // Fair
   "5", "P", 1,               // Poor
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "1", "L", 1,               // Masonary
   "2", "Z", 1,               // Zero Clearance
   "3", "W", 1,               // Wood Stove
   "4", "S", 1,               // Pellet Stove
   "5", "O", 1,               // Other
   "",   "", 0
};

static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "1",  "Z", 1,     // Central
   "2",  "X", 1,     // ?
   "3",  "X", 1,     // ?
   "4",  "X", 1,     // ?
   "7",  "X", 1,     // ?
   "8",  "X", 1,     // ?
   "9",  "X", 1,     // ?
   "C",  "Z", 1,     // Central
   "DMS","D", 3,     // Duct-less mini split
   "D ", "D", 1,     // Double wall
   "F ", "C", 1,     // Floor
   "N ", "L", 1,     // No heat
   "O ", "X", 1,     // Other
   "RB", "I", 2,     // Radiant, Baseboard
   "RF", "I", 2,     // Radiant, Floor
   "SP", "J", 2,     // Space heater
   "S ", "X", 1,     // ?
   "U",  "X", 1,     // ?
   "V",  "X", 1,     // ?
   "WS", "S", 2,     // Wood Stove
   "W ", "D", 1,     // Wall
   "X",  "X", 1,     // ?
   "Y ", "Y", 1,     // Yes, heated
   "Z",  "X", 1,     // ?
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "0", " ", 1,     	// None
   "1", "C", 1,     	// Pool/Spa Gunite
   "2", "C", 1,     	// Pool/Spa Fiberglass
   "3", "C", 1,     	// Pool/Spa Other
   "4", "P", 1,		// Pool Other
   "5", "S", 1,     	// Gunite Spa
   "6", "S", 1,     	// Fiberglass In-ground spa
   "7", "S", 1,     	// Fiberglass Above-ground spa
   "8", "S", 1,     	// Redwood In-ground spa
   "9", "S", 1,     	// Redwood Above-ground spa
   "A", "X", 1,     	// Doughboy Above-ground pool
   "B", "C", 1,     	// PoolSpa
   "C", "C", 1,     	// PoolSpa
   "D", "B", 1,     	// Doughboy In-ground pool
   "F", "F", 1,     	// Fiberglass
   "G", "G", 1,     	// Gunite pool
   "M", "C", 1,     	// PoolSpa
   "O", "C", 1,     	// PoolSpa
   "U", "C", 1,     	// PoolSpa
   "V", "V", 1,     	// Vinyl pool
   "X", "C", 1,     	// PoolSpa
   "Y", "C", 1,     	// PoolSpa
   "",   "", 0
};

static XLAT_CODE  asParkType[] =
{
   // Value, lookup code, value length
   "C", " C", 1,               // CARPORT
   "G",  "Z", 1,               // Garage
   "N",  "H", 1,               // None
   "O",  "O", 1,               // Others
   "",   "",  0
};

static XLAT_CODE  asView[] =
{
   "0", " ", 1,               // N/A
   "1", "F", 1,               // Lake Access
   "2", "M", 1,               // Golf course
   "3", "L", 1,               // Greenbelt/Park
   "4", "6", 1,               // Runway access
   "5", "E", 1,               // River vista
   "6", "7", 1,               // Busy Street
   "7", "X", 1,               // Interior
   "",  "",  0
};

static XLAT_CODE  asSewer[] =
{
   // Value, lookup code, value length 
   "E", "E", 1,               // Engineered system
   "O", "Z", 1,               // Other than sewer or septic
   "S", "Y", 1,               // Sewer
   "T", "S", 1,               // Septic tank
   "",   "",  0
};

static XLAT_CODE  asWaterSrc[] = 
{
   // Value, lookup code, value length
   "1", "P", 1,               // Community
   "2", "W", 1,               // Well
   "4", "Y", 1,               // Other
   "",   "",  0
};

#define  YOL_GRGR_DOCNUM        0
#define  YOL_GRGR_RECDATE       1
#define  YOL_GRGR_IMAGE         2
#define  YOL_GRGR_APN           3
#define  YOL_GRGR_DOCTYPE       4
#define  YOL_GRGR_DOCTITLE      5
#define  YOL_GRGR_GRANTOR       6
#define  YOL_GRGR_GRANTEE       7
#define  YOL_GRGR_DOCTAX        8
#define  YOL_GRGR_FLDCNT        9

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 YOL_Exemption[] = 
{
   "E01", "H", 3, 1,    // HOMEOWNERS EXEMPTION
   "E15", "X", 3, 1,    // LOW VALUE ORDINANCE
   "E20", "X", 3, 1,    // HISTORICAL AIRCRAFT
   "E24", "S", 3, 1,    // PART PRIVATE SCHOOL - LAND
   "E25", "S", 3, 1,    // PART PRIVATE SCHOOL - STRUCT 
   "E26", "S", 3, 1,    // PART PRIVATE SCHOOL - FIX/PP 
   "E30", "C", 3, 1,    // CHURCH EXEMPTION 
   "E31", "C", 3, 1,    // PARTIAL CHURCH EX - LAND 
   "E32", "C", 3, 1,    // PARTIAL CHURCH EX - STRUCT 
   "E33", "C", 3, 1,    // PARTIAL CHURCH EX - FIX/PP 
   "E35", "R", 3, 1,    // RELIGIOUS EXEMPTION 
   "E36", "R", 3, 1,    // PARTIAL RELIG EX - LAND 
   "E37", "R", 3, 1,    // PARTIAL RELIG EX - STRUCT 
   "E38", "R", 3, 1,    // PARTIAL RELIG EX - FIX/PP 
   "E40", "X", 3, 1,    // CLINICS
   "E41", "S", 3, 1,    // PRIVATE SCHOOL
   "E42", "I", 3, 1,    // HOSPITAL 
   "E43", "W", 3, 1,    // WELFARE EXEMPTION 
   "E44", "R", 3, 1,    // OTHER RELIGIOUS EXEMPTION 
   "E45", "R", 3, 1,    // PART OTHER REL EX - LAND
   "E46", "R", 3, 1,    // PART OTHER REL EX - STRUCT
   "E47", "R", 3, 1,    // PART OTHER REL EX - FIX/PP
   "E49", "X", 3, 1,    // LOW INCOME HOUSING
   "E50", "D", 3, 1,    // DISABLED VETERANS
   "E51", "D", 3, 1,    // DISABLED VETERANS
   "E52", "D", 3, 1,    // DISABLED VETERANS
   "E53", "D", 3, 1,    // BOE DUMMY DV EXEMPTION
   "E61", "E", 3, 1,    // CEMETERY EXEMPTION
   "E62", "X", 3, 1,    // LEASED TO SCHOOL
   "E63", "L", 3, 1,    // LIBRARY
   "E64", "X", 3, 1,    // REFUND TO UCD
   "E65", "S", 3, 1,    // PART LSD TO SCHOOL - LAND
   "E66", "S", 3, 1,    // PART LSD TO SCHOOL - STRUCT
   "E67", "S", 3, 1,    // PART LSD TO SCHOOL - FIX/PP
   "E74", "W", 3, 1,    // PART WELFARE EX - LAND
   "E75", "W", 3, 1,    // PART WELFARE EX - STRUCT
   "E76", "W", 3, 1,    // PART WELFARE EX - FIX/PP
   "E77", "I", 3, 1,    // PARTIAL HOSPITAL - LAND
   "E78", "I", 3, 1,    // PARTIAL HOSPITAL - STRUCT
   "E79", "I", 3, 1,    // PARTIAL HOSPITAL - FIX/PP
   "E80", "X", 3, 1,    // PART LOW INC HOUSING - LAND 
   "E81", "X", 3, 1,    // PART LOW INC HOUSING - STRUC
   "E82", "X", 3, 1,    // PART LOW INC HOUSING - FIX/PP
   "E85", "X", 3, 1,    // PENALTY
   "E86", "M", 3, 1,    // FREE MUSEUM
   "E87", "M", 3, 1,    // FREE MUSEUM-LAND
   "E88", "M", 3, 1,    // FREE MUSEUM-STRUCT
   "E89", "M", 3, 1,    // FREE MUSEUM-FIX/PP
   "E95", "X", 3, 1,    // 501 PENALTY ON EXEMPT ENTITIES
   "E98", "X", 3, 1,    // OTHER EXEMPTION
    "","",0,0
};

#endif