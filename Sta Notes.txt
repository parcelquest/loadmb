STA Notes:
   * Save BldgSeqNum and count #Bldgs

   - Rename last .R01 file to .O01 before loading with -Xx
   - Run monthly update: LoadMdb -CSTA -U -Xr -Xs
   _ Run LDR           : LoadMdb -CSTA -L -Xx
   - Extract situs from county to update R01 file before geocoding
   - Extract Roll file missing Acres and Zoning.  These data are
     available on TR601.
   - We have no choice but to extract Sta_Roll.csv and process
     like a regular Megabyte county.

 *    - LDR
 *         + Copy AGENCYCDCURRSEC_TR601_Header.TAB+AGENCYCDCURRSEC_TR601.TAB TR601.TAB
 *         + Import TR601.TAB to SecuredRoll table TR601.  In Advance dialog, select
 *           data type as US-ASCII and {tab} delimeter.  Make all field as Text (not Memo).
 *         + Rename last SSTA.R01 to SSTA.O01
 *         + Load lien: -L -CSTA -O -Xx
 *
 *    - Monthly update
 *         + Load roll data from tblParcel, tblNewParcels, tblOwners, tblNewParcelOwners
 *         + Extract roll (-Xr), exe (-Xe), and sale (-Xs)
 *         + Load update (-U).  This option will do all neccessary extract.

SELECT * FROM tblParcel INNER JOIN tblOwners ON tblParcel.RowID = tblOwners.APN_ID
   where tblparcel.asmt='016003003000' ORDER BY tblParcel.Asmt, tblOwners.AsOfSortCode DESC

SELECT Asmt, TaxArea, LegalDesc, UseCode, OwnerName, StreetNo, StreetDir, StreetName, StreetCity, Address1, Address2, Address3, Address4
   FROM tblParcel INNER JOIN tblOwners ON tblParcel.RowID = APN_ID
   WHERE asmt='001008030000' ORDER BY Asmt, AsOfSortCode DESC

SELECT tblParcel.Asmt, tblValues.ExemptCode, tblValues.Value*-1
   FROM tblParcel INNER JOIN tblValues ON tblParcel.RowID = tblValues.APN_ID
   WHERE (tblValues.ValueCode='EX') ORDER BY tblParcel.Asmt

// Validate Acres between roll & char file
SELECT r.Asmt,r.Acres as roll_acres,r.SizeAcresFtType,c.Acres as Char_acres,c.LandSqFt as CHar_Sqft FROM [STA_SEC_TR601] r
inner join sta_Char c on r.asmt=c.asmt
where r.sizeacresfttype='A' and r.acres > 10000.0 order by r.asmt

Owner is parsed with function #3 with some exceptions:
      *ROBIE W JOHN ET UX TRS & ROBIE W JOHN ET UX TRS KA
      *AIKEN RAYMOND E ET UX TRS & AIKEN RAYMOND E ET UX
      *GRAUPNER KARL E TRS & ZETTIA L & GRAUPNER & ZETTIA
      *GLOR DONALD D & VSEIA-GLOR VALORIE ANN
      *VAN VLIET JOHN M & VAN VLIET MARY S
      *O MEARA JACK B TRS & O MEARA MARIANNE H
      *LA ROSA MICHAEL J & LA ROSA NANCY B
      HAYES ALICE L & ROBERT M SR
      *BARNES MARINDABELL TR HEIRS OF

StreetCity     Expr1
               17382
CERES          23125
CROWS LANDING  1105
DENAIR         3855
EMPIRE         2104
EUGENE         328
FARMINGTON     34
GRAYSON        524
HICKMAN        932
HUGHSON        5096
KEYES          1561
KNIGHTS FERRY  401
LA GRANGE      524
MODESTO        146558
NEWMAN         6975
OAKDALE        19521
PATTERSON      11992
RIVERBANK      11185
SALIDA         8222
TURLOCK        39646
VALLEY HOME    462
VERNALIS       4
WATERFORD      5765
WESTLEY        317

Community   Expr1
   2
Cer   13819
COU   528
Cro   677
Den   2414
Emp   1246
Eug   183
FAR   27
Gra   270
Hic   601
Hug   3462
Key   968
Kni   253
LaG   327
Mod   86958
Mon   1
New   4072
NIC   115
Oak   12341
Pat   7623
Riv   6733
Sal   4493
Tur   24461
Val   288
VER   2
War   83
Wat   3415
Wes   229

Heating  Expr1
   29071
C  101590:Z
R  1     :I
S  8     :S
W  22063 :M
Z  786   :T

Cooling  Expr1
   46434
99 3
C  102601:C
E  4258  :E
O  3     :
W  220   :L

NumPools Expr1
   133636
F  8     :F
GS 4251  :C (Pool/Spa)
S  1379  :S (Spa)
A  638   :P (Above ground)
V  292   :V (Vinyl)
0  897   :
   997
G  11421 :P (Pool)

buildingseqnum
2  5248
3  747
6  63
11 6
4  223
13 2
12 3
   14355
10 11
8  29
5  110
1  146435
9  20
7  41

DocCode
01 1712
02 826
03 2
04 4744
05 490
06 491
08 22
10 351
11 80
12 6126
14 4
52 3594
53 1
54 3
58 776
60 13
93 8
GD 1
GX 2
TX 9

10/28/2013
HasSewer
E	2
T	38
S	986

HeatingSrc
G	   4
E	   1
R	   10
B C	2
W C	1
E C	3
DW	   6
W	   20424
F	   346
C	   116611
G C	79
99	   5
G Z	6
S	   6
G W	1
G R	1
B	   3
Z	   740

CoolingSrc
ERW	1
RW	   761
CE	   61
E	   4094
C	   117079


Pool
	145048
0	64
A	343
F	130
G	15714
GS	5830
N	1
S	1373
V	297