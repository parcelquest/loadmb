// YUB 07/18/2014
#if !defined(AFX_MERGEYUB_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_)
#define AFX_MERGEYUB_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_

// New format
#define  YUB_CHAR_FEEPARCEL             0
#define  YUB_CHAR_POOLSPA               1
#define  YUB_CHAR_CATTYPE               2
#define  YUB_CHAR_QUALITYCLASS          3
#define  YUB_CHAR_YEARBUILT             4
#define  YUB_CHAR_BUILDINGSIZE          5
#define  YUB_CHAR_ATTACHGARAGESF        6
#define  YUB_CHAR_DETACHGARAGESF        7
#define  YUB_CHAR_CARPORTSF             8
#define  YUB_CHAR_HEATING               9
#define  YUB_CHAR_COOLINGCENTRALAC      10
#define  YUB_CHAR_COOLINGEVAPORATIVE    11
#define  YUB_CHAR_COOLINGROOMWALL       12
#define  YUB_CHAR_COOLINGWINDOW         13
#define  YUB_CHAR_STORIESCNT            14
#define  YUB_CHAR_UNITSCNT              15
#define  YUB_CHAR_TOTALROOMS            16
#define  YUB_CHAR_EFFECTIVEYEAR         17
#define  YUB_CHAR_PATIOSF               18
#define  YUB_CHAR_BEDROOMS              19
#define  YUB_CHAR_BATHROOMS             20
#define  YUB_CHAR_HALFBATHS             21
#define  YUB_CHAR_FIREPLACE             22
#define  YUB_CHAR_ASMT                  23
#define  YUB_CHAR_BLDGSEQNUM            24
#define  YUB_CHAR_HASWELL               25

static XLAT_CODE  asCond[] =
{
   // Value, lookup code, value length
   "1", "G", 2,               // Good
   "2", "A", 2,               // Average
   "3", "F", 2,               // Fair
   "4", "P", 2,               // Poor
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "1", "L", 2,               // Masonary
   "2", "Z", 2,               // Zero Clearance
   "3", "W", 2,               // Wood Stove
   "4", "S", 2,               // Pellet Stove
   "5", "O", 2,               // Other
   "",   "",  0
};

static XLAT_CODE  asHeating[] = 
{
// Value, lookup code, value length
   "1", "Z", 1,               // Central Forced Air
   "2", "D", 1,               // Wall Unit
   "3", "C", 1,               // Floor Unit
   "4", "I", 1,               // Radiant
   "5", "F", 1,               // Baseboard
   "6", "G", 1,               // Heat Pump
   "7", "X", 1,               // Other
   "",  "",  0
};

static XLAT_CODE  asCooling[] =
{
// Value, lookup code, value length
   "1", "C", 1,               // Central Forced Air
   "2", "L", 1,               // Wall Unit
   "3", "H", 1,               // Heat Pump
   "4", "E", 1,               // Evaporative
   "5", "X", 1,               // Others
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "1", "F", 1,               // Fiberglass
   "2", "G", 1,               // Gunite
   "3", "V", 1,               // Vinyl
   "4", "P", 1,               // Other
   "5", "S", 1,               // Gunite Spa
   "",  "",  0
};

IDX_TBL5 YUB_DocCode[] =
{// DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN
   "01", "1 ", 'N', 2, 2,     // *TRANSFER
   "02", "75", 'Y', 2, 2,     // *TRANSFER GOVT ENTITY
   "03", "75", 'Y', 2, 2,     // *TRANSFER NO REAPPRAISAL
   "04", "57", 'N', 2, 2,     // *TRANSFER PARTIAL INTEREST
   "09", "19", 'Y', 2, 2,     // *SPLIT/COMBO
   "86", "74", 'Y', 2, 2,     // *NO CHANGE IN VALUE
   "92", "75", 'Y', 2, 2,     // *PROP 58 EXCLUSION
   "", "", '\0', 0, 0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 YUB_Exemption[] = 
{
   "E01", "H", 3, 1,    // HOMEOWNERS EXEMPTION
   "E02", "V", 3, 1,    // VETERANS
   "E03", "R", 3, 1,    // RELIGIOUS
   "E04", "C", 3, 1,    // CHURCH
   "E05", "W", 3, 1,    // WELFARE
   "E06", "E", 3, 1,    // CEMETARY
   "E07", "M", 3, 1,    // MUSEUM
   "E08", "D", 3, 1,    // DISABLED VETERAN
   "E09", "D", 3, 1,    // DISABLED VETERAN LOW INCOME
   "E10", "X", 3, 1,    // LESSORS EXEMPTION
   "E11", "X", 3, 1,    // OTHER
   "E12", "Y", 3, 1,    // SOLDIERS & SAILORS
   "E13", "V", 3, 1,    // VETERANS ORGANIZATION
   "E14", "C", 3, 1,    // CHURCH & WELFARE
   "E15", "X", 3, 1,    // HISTORICAL AIRCRAFT
   "E16", "X", 3, 1,    // LOW VALUE-LESS $5000
   "E17", "P", 3, 1,    // PUBLIC SCHOOLS
   "E18", "U", 3, 1,    // COLLEGE EXEMPTION
   "E20", "I", 3, 1,    // WELFARE - HOSPITAL
   "E50", "X", 3, 1,    // PARTIAL BASIC D/V
   "E60", "X", 3, 1,    // PARTIAL LOW INC D/V
   "E70", "W", 3, 1,    // PARTIAL WELFARE
   "E80", "C", 3, 1,    // PARTIAL CHURCH EXEMPTION
   "E90", "R", 3, 1,    // PARTIAL RELIGIOUS EXEMPTION
   "","",0,0
};

#endif