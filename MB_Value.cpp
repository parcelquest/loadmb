/******************************************************************************
 *
 * This module is created to support loading data into Pcl_Values table. Since
 * this task is run once a year, the import into SQL task will be done manually.
 *
 * 09/04/2013 13.12.1   Modify parseValue() to extract base year value.
 *                      Add parseTaxValues() to extract enrolled and Prop8 values.
 *                      Replace loadMBValues() with MB_ExtrBaseYearValues() and 
 *                      MB_ExtrCurValues().
 * 09/05/2013 13.12.1.1 Correct value of MB_TAX_EXEAMT1
 *            13.12.1.2 Duplicate prop8 record to create enrolled record.
 * 11/25/2013 13.18.2   Bug fix in parseTaxValues() to return error if invalid record found.
 * 04/17/2014 13.23.0   Add parseRollValues() & MBR_ExtrCurValues() to process YOL & YUB.
 *                      Rename MB_ExtrCurValues to MBT_ExtrCurValues() used in BUT.
 *                      Fix crashed bug in updExeValue() when file reaches EOF.
 * 08/12/2014 14.2.0    Fix bug in MBT_ExtrCurValues().
 * 04/24/2015 14.15.3   Add Reason code to VALUE_REC and save original VST code here.  
 * 04/27/2015 14.15.4   Modify parseTaxValues() to get current value only. Don't check for Prop 8.
 * 04/29/2015 14.15.5   Add MB_ExtrValues() to extract value and exemption file for pcl_value table.
 * 06/15/2015 15.0.1    Modify MB_ExtrValues() to sort output file.
 * 07/02/2015 15.0.3    Add MB_ExtrMergeValues() to extract and merge record type 1 & 8 to 
 *                      output record type 1 with reason 8.
 * 02/28/2017 16.9.3    Modify parseValues() to ignore reason code 55.  Add updLienValue() and
 *                      new version of MB_ExtrMergeValues() that copies values from lien extract file.
 *                      Add new version of MB_ExtrValues().
 * 03/20/2017 16.13.1   Add MB_ResetPVReason()
 * 04/11/2017 16.14.5   Fix MB_ResetPVReason() for AMA
 * 04/17/2017 16.15.1   Modify MB_ResetPVReason() setting Reason=' ' instead of '0' so fields value
 *                      will be NULL instead of 0 in SQL table.
 * 05/22/2017 16.16.1   Change param name iSkipRows to iSkipHdrRows in MB_ExtrValues()
 *                      Modify updExeValue() to check for EXE value of 999999
 * 05/23/2017 16.16.2   Modify updLienValue() to update EXE using value from LDR.
 *                      Modify MB_ExtrValues() & MB_ExtrMergeValues() to call updExeValue() only if not success in updLienValue()
 * 09/10/2019 19.2.4    Add new version of parseValues() to support county specific VST.
 *                      Modify MB_ExtrValues(), MB_ResetPVReason() to support MOD.
 * 09/27/2019 19.3.1    Add MB_FixValues() to fix values for MOD.  Modify parseValues() & updLienValue() &
 *                      MB_ExtrValues() to handle special case VST=2 for MOD.
 *                      Modify parseValues() to drop VST=0 if TotalValue=0 (MOD).
 *                      Add special case foe ALP.
 * 06/30/2020 20.0.1    Fix updLienValue() to include total exempt amount
 * 05/05/2121 20.8.6    Modify parseValues() to support ALP cases 5, 55, 96.
 * 05/08/2021 20.8.7    Fix parseValues() by setting Reason=5 for Vst of 5 & 96.  Drop Vst 55
 *                      to avoid too many records per parcels (for ALP).
 * 09/15/2021 21.2.3    Modify parseValues() to drop reason 52-55 for COL.
 * 07/15/2022 22.0.2    Modify MB_ExtrValues() & parseValues() to add MAD support.
 * 06/21/2023 23.0.1    Modify MB_ExtrValues() to add EXE amount to MOD.
 * 07/01/2023 23.0.3    Modify parseValues() to trim VST value  to remove leading space.
 *                      Modify updExeValue() & MB_ExtrBaseYearValues() to check for empty line read.
 *                      Modify MB_ExtrValues() to sort value file for better matching with EXE file.
 * 07/18/2023 23.0.5    Remove skip header from MB_ExtrValues() since input file has been resorted.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "Logs.h"
#include "Utils.h"
#include "doSort.h"
#include "Tables.h"
#include "CountyInfo.h"
#include "RecDef.h"
#include "MBExtrn.h"
#include "LoadValue.h"

// _ROLL
#define  MB_ROLL_ASMT                 0
#define  MB_ROLL_FEEPARCEL            1
#define  MB_ROLL_TRA                  2
#define  MB_ROLL_LEGAL                3
#define  MB_ROLL_ZONING               4
#define  MB_ROLL_USECODE              5
#define  MB_ROLL_NBHCODE              6
#define  MB_ROLL_ACRES                7
#define  MB_ROLL_DOCNUM               8
#define  MB_ROLL_DOCDATE              9
#define  MB_ROLL_TAXABILITY           10
#define  MB_ROLL_OWNER                11
#define  MB_ROLL_CAREOF               12
#define  MB_ROLL_DBA                  13
#define  MB_ROLL_M_ADDR               14
#define  MB_ROLL_M_CITY               15
#define  MB_ROLL_M_ST                 16
#define  MB_ROLL_M_ZIP                17
#define  MB_ROLL_LAND                 18
#define  MB_ROLL_HOMESITE             19
#define  MB_ROLL_IMPR                 20
#define  MB_ROLL_GROWING              21
#define  MB_ROLL_FIXTRS               22
#define  MB_ROLL_PERSPROP             23  // New script
#define  MB_ROLL_BUSPROP              24

#define  MB_ROLL_PPMOBILHOME          25
#define  MB_ROLL_M_ADDR1              26
#define  MB_ROLL_M_ADDR2              27
#define  MB_ROLL_M_ADDR3              28
#define  MB_ROLL_M_ADDR4              29

// Tax file
#define  MB_TAX_ASMT                  0
#define  MB_TAX_FEEPARCEL             1
#define  MB_TAX_TAXAMT1               2
#define  MB_TAX_TAXAMT2               3
#define  MB_TAX_PENAMT1               4
#define  MB_TAX_PENAMT2               5
#define  MB_TAX_PENDATE1              6
#define  MB_TAX_PENDATE2              7
#define  MB_TAX_PAIDAMT1              8
#define  MB_TAX_PAIDAMT2              9
#define  MB_TAX_TOTALPAID1            10
#define  MB_TAX_TOTALPAID2            11
#define  MB_TAX_TOTALFEES             12
#define  MB_TAX_PAIDDATE1             13
#define  MB_TAX_PAIDDATE2             14
#define  MB_TAX_YEAR                  15
#define  MB_TAX_ROLLCAT               16
#define  MB_TAX_ROLLCHGNUM            17
#define  MB_TAX_LAND                  18
#define  MB_TAX_FIXEDIMPR             19
#define  MB_TAX_GROWIMPR              20
#define  MB_TAX_IMPR                  21
#define  MB_TAX_PERSPROP              22
#define  MB_TAX_PP_MH                 23
#define  MB_TAX_NETVAL                24
#define  MB_TAX_EXECODE1              25
#define  MB_TAX_EXEAMT1               26
#define  MB_TAX_EXECODE2              27
#define  MB_TAX_EXEAMT2               28
#define  MB_TAX_EXECODE3              29
#define  MB_TAX_EXEAMT3               30
#define  MB_TAX_TAXABILITY            31

// _EXE
#define  MB_EXE_STATUS     0
#define  MB_EXE_ASMT       1
#define  MB_EXE_CODE       2
#define  MB_EXE_HOEXE      3
#define  MB_EXE_EXEAMT     4
#define  MB_EXE_EXEPCT     5

#define  PV_APN            0
#define  PV_VST            1
#define  PV_LAND           2
#define  PV_HOMESITE       3
#define  PV_IMPR           4
#define  PV_GROWIMPR       5
#define  PV_FIXTR          6
#define  PV_FIXTR_RP       7
#define  PV_BUSINV         8
#define  PV_PP_MH          9

/******************************************************************************
 
   - Possible types:
      1    ENROLLED
      2    ADJUSTED BASE FOR ENROLLED
      3    BASE YEAR
      4    ADJ BASE FOR BASE YR
      5    RESTRICTED
      8    PROP 8
      11   FUTURE ENROLLED
      12   FUTURE ADJ BASE FOR ENROLLED
      13   FUTURE BASE YEAR
      14   FUTURE ADJ BASE FOR BASE YR
      15   FUTURE RESTRICTED
      18   FUTURE PROP 8
      52   CLCA (California Land Consv. Act) Non-Renewal
      53   CLCA 423.3
      54   CLCA FSZ (Farmland Security Zone) (423.4)
      55   CLCA
      96   TIMBER VALUE
   - Right now, we are only interested in 1, 3, & 8

   - MOD
      1, 3 � Use values from Mod_Values file
      1, 2, 3 � Overwrite all values with closed roll values
      1, 3, 8 � Use values from Mod_Values file
      1, 3, 5 � Use values from Mod_Values file
      1, 3, 96 or 1, 3, 8, 96 � Use values from Mod_Values file

      Pv_reason          Pv_vst_type
      3                  1
      NULL               2

      8                  1
      NULL               2             
      NULL               8

      5                  1
      NULL               2
      NULL               9

      96                 1
      NULL               2
      NULL               9

 ******************************************************************************/

int parseValues(LPSTR pInbuf, LPSTR pOutbuf, int iType)
{
   VALUE_REC   *pVal = (VALUE_REC *)pOutbuf;
   int         iRet = 0, lVal, lLand, lImpr, lOther, iVst;
   char        sTmp[32];

#ifdef _DEBUG
   //if (!memcmp(pInbuf, "009030003000", 9))
   //   iRet = 0;
#endif

   // Parse input rec
   if (cDelim == ',')
      iTokens = ParseStringNQ(pInbuf, cDelim, 64, apTokens);
   else
      iTokens = ParseStringIQ(pInbuf, cDelim, 64, apTokens);

   lTrim(apTokens[PV_VST]);
   if (iTokens >= PV_PP_MH && *apTokens[PV_VST] > ' ')
   {
      memset(pOutbuf, ' ', sizeof(VALUE_REC));
      memcpy(pVal->Apn, apTokens[PV_APN], strlen(apTokens[PV_APN]));
      iVst = atol(apTokens[PV_VST]);
      if (iType > 0)
      {
         // choose specific value type
         if (iType == iVst)
         {
            if (iVst == VT_BASEYEAR)
               pVal->ValueSetType[0] = '2';
            else
               pVal->ValueSetType[0] = *apTokens[PV_VST];
         } else if (iVst == VT_ADJBASEYR)
            pVal->ValueSetType[0] = '9';
         else
            return -3;        // Ignore other types
      } else
      {
         switch (iVst)
         {
            case 3:
               pVal->ValueSetType[0] = '2';
               break;
            case 1:
            case 8:
               pVal->ValueSetType[0] = *apTokens[PV_VST];
               break;
            case 52:
            case 55:
               break;
            default:
               pVal->ValueSetType[0] = '9';  // Others
         }
      }

      memcpy(pVal->Reason, apTokens[PV_VST], strlen(apTokens[PV_VST]));
      memcpy(pVal->TaxYear, myCounty.acYearAssd, 4);
      lLand = atol(apTokens[PV_LAND]);
      if (lLand > 0)
      {
         iRet = sprintf(sTmp, "%d", lLand);
         memcpy(pVal->Land, sTmp, iRet);
      }

      lImpr = atol(apTokens[PV_IMPR]);
      if (lImpr > 0)
      {
         iRet = sprintf(sTmp, "%d", lImpr);
         memcpy(pVal->Impr, sTmp, iRet);
      }

      lVal = atol(apTokens[PV_HOMESITE]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->HomeSite, sTmp, iRet);
      }
      lOther = lVal;

      lVal = atol(apTokens[PV_GROWIMPR]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->GrowImpr, sTmp, iRet);
      }
      lOther += lVal;

      lVal = atol(apTokens[PV_FIXTR]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->Fixtr, sTmp, iRet);
      }
      lOther += lVal;

      lVal = atol(apTokens[PV_FIXTR_RP]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->Fixtr_RP, sTmp, iRet);
      }
      lOther += lVal;
      
      lVal = atol(apTokens[PV_BUSINV]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->Businv, sTmp, iRet);
      }
      lOther += lVal;

      lVal = atol(apTokens[PV_PP_MH]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->PP_MH, sTmp, iRet);
      }
      lOther += lVal;

      if (lOther > 0)
      {
         iRet = sprintf(sTmp, "%d", lOther);
         memcpy(pVal->Other, sTmp, iRet);
      }

      // Gross
      lVal = lOther + lLand + lImpr;
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->TotalVal, sTmp, iRet);
      }

      if (iVst < 50)
         iRet = 0;
      else
         iRet = iVst;         // This value can be ignored
   } else
   {
      LogMsg("*** Bad input record: %s", pInbuf);
      iRet = -1;
   }

   return iRet;
}

int parseValues(LPSTR pInbuf, LPSTR pOutbuf, int iType, int iCnty)
{
   VALUE_REC   *pVal = (VALUE_REC *)pOutbuf;
   int         iRet = 0, lVal, lLand, lImpr, lOther, iVst;
   char        sTmp[32];

#ifdef _DEBUG
   //if (!memcmp(pInbuf, "001074007000", 9))  
   //   iRet = 0;
#endif

   // Parse input rec
   if (cValDelim == ',')
      iTokens = ParseStringNQ(pInbuf, cValDelim, 64, apTokens);
   else
      iTokens = ParseStringIQ(pInbuf, cValDelim, 64, apTokens);

   if (iTokens >= PV_PP_MH && *apTokens[PV_VST] > ' ')
   {
      memset(pOutbuf, ' ', sizeof(VALUE_REC));
      memcpy(pVal->Apn, apTokens[PV_APN], strlen(apTokens[PV_APN]));
      iVst = atol(apTokens[PV_VST]);
      if (iType > 0)
      {
         // choose specific value type
         if (iType == iVst)
         {
            if (iVst == VT_BASEYEAR)
               pVal->ValueSetType[0] = '2';
            else
               pVal->ValueSetType[0] = *apTokens[PV_VST];
         } else if (iVst == VT_ADJBASEYR)
            pVal->ValueSetType[0] = '9';
         else
            return -3;                          // Ignore other types
      } else
      {
         if (iCnty == 25)                       // MOD
         {
            switch (iVst)
            {
               case 2:
                  pVal->ValueSetType[0] = '0';
                  break;
               case 3:
                  pVal->ValueSetType[0] = '2';
                  break;
               case 1:
               case 8:
                  pVal->ValueSetType[0] = *apTokens[PV_VST];
                  break;
               case 5:
               case 96:
                  pVal->ValueSetType[0] = '9';  // Others
               default:
                  break;
            }
         } else if (iCnty == 2)                 // ALP
         {
            switch (iVst)
            {
               case 3:
                  pVal->ValueSetType[0] = '2';
                  break;
               case 1:
               case 8:
                  pVal->ValueSetType[0] = *apTokens[PV_VST];
                  break;
               case 5:
               //case 55:
               case 96:
                  pVal->ValueSetType[0] = '9';
                  break;
               default:
                  break;
            }
         } else if (iCnty == 6)                 // COL
         {
            switch (iVst)
            {
               case 3:
                  pVal->ValueSetType[0] = '2';
                  break;
               case 1:
               case 8:
                  pVal->ValueSetType[0] = *apTokens[PV_VST];
                  break;
               case 52:
               case 53:
               case 54:
               case 55:
                  break;
               default:
                  pVal->ValueSetType[0] = '9';  // Others
            }
         } else if (iCnty == 20)                // MAD
         {
            switch (iVst)
            {
               case 2:
                  break;
               case 3:
                  pVal->ValueSetType[0] = '2';
                  break;
               case 1:
               case 8:
                  pVal->ValueSetType[0] = *apTokens[PV_VST];
                  break;
               default:
                  pVal->ValueSetType[0] = '9';  // Others
            }
         } else                                 // AMA,BUT,NEV,TUO,YOL,YUB ...
         {
            switch (iVst)
            {
               case 3:
                  pVal->ValueSetType[0] = '2';
                  break;
               case 1:
               case 8:
                  pVal->ValueSetType[0] = *apTokens[PV_VST];
                  break;
               case 52:
               case 55:
                  break;
               default:
                  pVal->ValueSetType[0] = '9';  // Others
            }
         }
      }

      // This change is for ALP
      if (iCnty == 2 && iVst == 96)
         pVal->Reason[0] = '5';
      else
         memcpy(pVal->Reason, apTokens[PV_VST], strlen(apTokens[PV_VST]));

      memcpy(pVal->TaxYear, myCounty.acYearAssd, 4);
      lLand = atol(apTokens[PV_LAND]);
      if (lLand > 0)
      {
         iRet = sprintf(sTmp, "%d", lLand);
         memcpy(pVal->Land, sTmp, iRet);
      }

      lImpr = atol(apTokens[PV_IMPR]);
      if (lImpr > 0)
      {
         iRet = sprintf(sTmp, "%d", lImpr);
         memcpy(pVal->Impr, sTmp, iRet);
      }

      lVal = atol(apTokens[PV_HOMESITE]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->HomeSite, sTmp, iRet);
      }
      lOther = lVal;

      lVal = atol(apTokens[PV_GROWIMPR]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->GrowImpr, sTmp, iRet);
      }
      lOther += lVal;

      lVal = atol(apTokens[PV_FIXTR]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->Fixtr, sTmp, iRet);
      }
      lOther += lVal;

      lVal = atol(apTokens[PV_FIXTR_RP]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->Fixtr_RP, sTmp, iRet);
      }
      lOther += lVal;
      
      lVal = atol(apTokens[PV_BUSINV]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->Businv, sTmp, iRet);
      }
      lOther += lVal;

      lVal = atol(apTokens[PV_PP_MH]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->PP_MH, sTmp, iRet);
      }
      lOther += lVal;

      if (lOther > 0)
      {
         iRet = sprintf(sTmp, "%d", lOther);
         memcpy(pVal->Other, sTmp, iRet);
      }

      // Gross
      lVal = lOther + lLand + lImpr;
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->TotalVal, sTmp, iRet);
      } else if (iCnty == 25 && iVst == 8)
         iVst = 999;          // Drop this

      if (iVst < 99)
         iRet = 0;
      else
         iRet = iVst;         // This value can be ignored
   } else
   {
      LogMsg("*** Bad input record: %s", pInbuf);
      iRet = -1;
   }

   return iRet;
}

/******************************************************************************
 *
 * Return 0 if record is ok
 *
 ******************************************************************************/

int parseRollValues(LPSTR pInbuf, LPSTR pOutbuf)
{
   VALUE_REC   *pVal = (VALUE_REC *)pOutbuf;
   int         iRet = 0, lVal, lLand, lImpr, lOther;
   char        sTmp[32];

#ifdef _DEBUG
   //if (!memcmp(pInbuf, "075250001000", 9))
   //   iRet = 0;
#endif

   // Parse input rec
   if (cDelim == ',')
      iTokens = ParseStringNQ(pInbuf, cDelim, 64, apTokens);
   else
      iTokens = ParseStringIQ(pInbuf, cDelim, 64, apTokens);

   if (iTokens < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("*** Bad input record: %s (tokens=%d)", pInbuf, iTokens);
      return -1;
   }

   // Copying data
   memset(pOutbuf, ' ', sizeof(VALUE_REC));
   memcpy(pVal->Apn, apTokens[MB_ROLL_ASMT], strlen(apTokens[MB_ROLL_ASMT]));
   //if (*apTokens[MB_ROLL_TAXABILITY] == '8')
   //   pVal->ValueSetType[0] = '8';
   //else
      pVal->ValueSetType[0] = '1';

   lLand = atol(apTokens[MB_ROLL_LAND]);
   if (lLand > 0)
   {
      iRet = sprintf(sTmp, "%d", lLand);
      memcpy(pVal->Land, sTmp, iRet);
   }

   lImpr = atol(apTokens[MB_ROLL_IMPR]);
   if (lImpr > 0)
   {
      iRet = sprintf(sTmp, "%d", lImpr);
      memcpy(pVal->Impr, sTmp, iRet);
   }

   lVal = atol(apTokens[MB_ROLL_PERSPROP]);
   if (lVal > 0)
   {
      iRet = sprintf(sTmp, "%d", lVal);
      memcpy(pVal->PersProp, sTmp, iRet);
   }
   lOther = lVal;

   lVal = atol(apTokens[MB_ROLL_GROWING]);
   if (lVal > 0)
   {
      iRet = sprintf(sTmp, "%d", lVal);
      memcpy(pVal->GrowImpr, sTmp, iRet);
   }
   lOther += lVal;

   lVal = atol(apTokens[MB_ROLL_FIXTRS]);
   if (lVal > 0)
   {
      iRet = sprintf(sTmp, "%d", lVal);
      memcpy(pVal->Fixtr, sTmp, iRet);
   }
   lOther += lVal;

   lVal = atol(apTokens[MB_ROLL_BUSPROP]);
   if (lVal > 0)
   {
      iRet = sprintf(sTmp, "%d", lVal);
      memcpy(pVal->Businv, sTmp, iRet);
   }
   lOther += lVal;

   lVal = atol(apTokens[MB_ROLL_PPMOBILHOME]);
   if (lVal > 0)
   {
      iRet = sprintf(sTmp, "%d", lVal);
      memcpy(pVal->PP_MH, sTmp, iRet);
   }
   lOther += lVal;

   if (lOther > 0)
   {
      iRet = sprintf(sTmp, "%d", lOther);
      memcpy(pVal->Other, sTmp, iRet);
   }

   // Gross
   lVal = lOther + lLand + lImpr;
   if (lVal > 0)
   {
      iRet = sprintf(sTmp, "%d", lVal);
      memcpy(pVal->TotalVal, sTmp, iRet);
   }

   // Exemption

   return 0;
}

/******************************************************************************
 *
 * Return 0 if record is ok
 *
 ******************************************************************************/

int parseTaxValues(LPSTR pInbuf, LPSTR pOutbuf)
{
   VALUE_REC   *pVal = (VALUE_REC *)pOutbuf;
   int         iRet = 0, lVal, lLand, lImpr, lOther;
   char        sTmp[32];

#ifdef _DEBUG
   //if (!memcmp(pInbuf, "075250001000", 9))
   //   iRet = 0;
#endif

   // Parse input rec
   if (cDelim == ',')
      iTokens = ParseStringNQ(pInbuf, cDelim, 64, apTokens);
   else
      iTokens = ParseStringIQ(pInbuf, cDelim, 64, apTokens);

   if (iTokens < MB_TAX_TAXABILITY)
   {
      LogMsg("*** Bad input record: %s (tokens=%d)", pInbuf, iTokens);
      return -1;
   }

   // Ignore Roll Change record
   if (*apTokens[MB_TAX_ROLLCHGNUM] > ' ')
      return -2;

   iRet = atol(apTokens[MB_TAX_YEAR]);
   if (iRet != lLienYear)
      return -3;

   // Copying data
   memset(pOutbuf, ' ', sizeof(VALUE_REC));
   memcpy(pVal->Apn, apTokens[MB_TAX_ASMT], strlen(apTokens[MB_TAX_ASMT]));

   // Get current value only - 20150427
   //if (*apTokens[MB_TAX_TAXABILITY] == '8')
   //   pVal->ValueSetType[0] = '8';
   //else
      pVal->ValueSetType[0] = '1';

   lLand = atol(apTokens[MB_TAX_LAND]);
   if (lLand > 0)
   {
      iRet = sprintf(sTmp, "%d", lLand);
      memcpy(pVal->Land, sTmp, iRet);
   }

   lImpr = atol(apTokens[MB_TAX_IMPR]);
   if (lImpr > 0)
   {
      iRet = sprintf(sTmp, "%d", lImpr);
      memcpy(pVal->Impr, sTmp, iRet);
   }

   lVal = atol(apTokens[MB_TAX_PERSPROP]);
   if (lVal > 0)
   {
      iRet = sprintf(sTmp, "%d", lVal);
      memcpy(pVal->PersProp, sTmp, iRet);
   }
   lOther = lVal;

   lVal = atol(apTokens[MB_TAX_GROWIMPR]);
   if (lVal > 0)
   {
      iRet = sprintf(sTmp, "%d", lVal);
      memcpy(pVal->GrowImpr, sTmp, iRet);
   }
   lOther += lVal;

   lVal = atol(apTokens[MB_TAX_FIXEDIMPR]);
   if (lVal > 0)
   {
      iRet = sprintf(sTmp, "%d", lVal);
      memcpy(pVal->Fixtr, sTmp, iRet);
   }
   lOther += lVal;

   lVal = atol(apTokens[MB_TAX_PP_MH]);
   if (lVal > 0)
   {
      iRet = sprintf(sTmp, "%d", lVal);
      memcpy(pVal->PP_MH, sTmp, iRet);
   }
   lOther += lVal;

   if (lOther > 0)
   {
      iRet = sprintf(sTmp, "%d", lOther);
      memcpy(pVal->Other, sTmp, iRet);
   }

   // Gross
   lVal = lOther + lLand + lImpr;
   if (lVal > 0)
   {
      iRet = sprintf(sTmp, "%d", lVal);
      memcpy(pVal->TotalVal, sTmp, iRet);
   }

   // Exemption
   lVal = atol(apTokens[MB_TAX_EXEAMT1]);
   lVal += atol(apTokens[MB_TAX_EXEAMT2]);
   lVal += atol(apTokens[MB_TAX_EXEAMT3]);
   if (lVal > 0)
   {
      iRet = sprintf(sTmp, "%d", lVal);
      memcpy(pVal->Exe, sTmp, iRet);
   }

   return 0;
}

/******************************************************************************
 *
 ******************************************************************************/

int updExeValue(LPSTR pOutbuf, int iSkipHdr)
{
   static    char acRec[512], acSave[512], *pRec=NULL;
   char      acTmp[256], *pTmp;
   long      lTmp;
   int       iRet=0, iTmp;
   VALUE_REC *pVal = (VALUE_REC *)pOutbuf;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iSkipHdr; iTmp++)
         pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
      if (acRec[0] == '"')
         iTmp = 1;
      else
         iTmp = 0;
      if (iTmp != iSkipQuote)
      {
         LogMsg("*** Overwrite INI SkipQuote value to %d (from %d)", iTmp, iSkipQuote);
         iSkipQuote = iTmp;
      }
   }

   do
   {
      if (!pRec || *pRec < '0')
      {
         fclose(fdExe);
         fdExe = NULL;
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      iTmp = memcmp(pOutbuf, pTmp+iSkipQuote, iApnLen);
      if (iTmp > 0)
         pRec = fgets(acRec, 512, fdExe);
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   strcpy(acSave, acRec);
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
   else
      iRet = ParseStringIQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);

   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         iRet = 1;      // EOF
      } else
         iRet = -1;
   } else
   {
      // Exe Amt
      if (memcmp(apTokens[MB_EXE_EXEAMT], "999999", 6))
      {
         lTmp = atol(apTokens[MB_EXE_EXEAMT]);
         if (lTmp > 0)
         {
            iTmp = sprintf(acTmp, "%d", lTmp);
            memcpy(pVal->Exe, acTmp, iTmp);
         }
      } else
      {
         // Total exemption
         memcpy(pVal->Exe, pVal->TotalVal, PV_SIZ_EXE);
      }

      // Restore previous rec to reuse
      strcpy(acRec, acSave);

      lExeMatch++;
      iRet = 0;
   }

   return iRet;
}

/******************************************************************************
 *
 * Update EXE & Others
 *
 ******************************************************************************/

int updLienValue1(LPSTR pOutbuf)
{
   static    char acRec[512], acSave[512], *pRec=NULL;
   char      acTmp[256];
   long      lTmp;
   int       iTmp;
   VALUE_REC *pVal = (VALUE_REC *)pOutbuf;
   LIENEXTR  *pLienExt = (LIENEXTR *)&acRec[0];

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 512, fdLienExt);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdLienExt);
         fdLienExt = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pLienExt->acApn, iApnLen);
      if (iTmp > 0)
         pRec = fgets(acRec, 512, fdLienExt);
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Fixture Amt
   lTmp = atoin(pLienExt->acME_Val, SIZ_LIEN_FIXT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pVal->Fixtr, acTmp, iTmp);
   }

   // PP Amt
   lTmp = atoin(pLienExt->acPP_Val, SIZ_LIEN_FIXT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pVal->PersProp, acTmp, iTmp);
   }

   // Businv Amt
   lTmp = atoin(pLienExt->extra.MB.BusProp, SIZ_LIEN_FIXT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pVal->Businv, acTmp, iTmp);
   }

   // Grow Impr Amt
   lTmp = atoin(pLienExt->extra.MB.GrowImpr, SIZ_LIEN_FIXT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pVal->GrowImpr, acTmp, iTmp);
   }

   // PP_MH Amt
   lTmp = atoin(pLienExt->extra.MB.PP_MobileHome, SIZ_LIEN_FIXT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pVal->PP_MH, acTmp, iTmp);
   }

   // Update EXE value 
   lTmp = atoin(pLienExt->acExAmt, SIZ_LIEN_EXEAMT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pVal->Exe, acTmp, iTmp);
   }

   lRollMatch++;

   return 0;
}

/******************************************************************************
 *
 * Update all
 *
 ******************************************************************************/

int updLienValue(LPSTR pOutbuf, bool bUpdAll)
{
   static    char acRec[512], acSave[512], *pRec=NULL;
   char      acTmp[256];
   long      lTmp, lTotal=0;
   int       iTmp;
   VALUE_REC *pVal = (VALUE_REC *)pOutbuf;
   LIENEXTR  *pLienExt = (LIENEXTR *)&acRec[0];

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 512, fdLienExt);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdLienExt);
         fdLienExt = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pLienExt->acApn, iApnLen);
      if (iTmp > 0)
         pRec = fgets(acRec, 512, fdLienExt);
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   if (bUpdAll)
   {
      // Land
      lTotal = atoin(pLienExt->acLand, SIZ_LIEN_LAND);
      if (lTotal > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTotal);
         memcpy(pVal->Land, acTmp, iTmp);
      }

      // Impr
      lTmp = atoin(pLienExt->acImpr, SIZ_LIEN_IMPR);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pVal->Impr, acTmp, iTmp);
         lTotal += lTmp;
      }
      memset(pVal->Other, ' ', PV_SIZ_FIXTR);

      lTmp = atoin(pLienExt->acOther, SIZ_LIEN_OTHERS);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pVal->Other, acTmp, iTmp);
         lTotal += lTmp;
      }
   }

   // Fixture Amt
   lTmp = atoin(pLienExt->acME_Val, SIZ_LIEN_FIXT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pVal->Fixtr, acTmp, iTmp);
   } else
      memset(pVal->Fixtr, ' ', PV_SIZ_FIXTR);

   // PP Amt
   lTmp = atoin(pLienExt->acPP_Val, SIZ_LIEN_FIXT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pVal->PersProp, acTmp, iTmp);
   } else
      memset(pVal->PersProp, ' ', PV_SIZ_FIXTR);

   // Businv Amt
   lTmp = atoin(pLienExt->extra.MB.BusProp, SIZ_LIEN_FIXT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pVal->Businv, acTmp, iTmp);
   } else
      memset(pVal->Businv, ' ', PV_SIZ_FIXTR);

   // Grow Impr Amt
   lTmp = atoin(pLienExt->extra.MB.GrowImpr, SIZ_LIEN_FIXT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pVal->GrowImpr, acTmp, iTmp);
   } else
      memset(pVal->GrowImpr, ' ', PV_SIZ_FIXTR);

   // PP_MH Amt
   lTmp = atoin(pLienExt->extra.MB.PP_MobileHome, SIZ_LIEN_FIXT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pVal->PP_MH, acTmp, iTmp);
   } else
      memset(pVal->PP_MH, ' ', PV_SIZ_FIXTR);
   
   if (bUpdAll && lTotal > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTotal);
      memcpy(pVal->TotalVal, acTmp, iTmp);
   }

   // Total Exempt
   lTmp = atoin(pLienExt->acExAmt, SIZ_LIEN_EXEAMT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pVal->Exe, acTmp, iTmp);
      lExeMatch++;
   } else
      memset(pVal->Exe, ' ', PV_SIZ_EXE);

   return 0;
}

/******************************************************************************
 *
 * Extract base year values from value file for MB counties
 *
 ******************************************************************************/

int MB_ExtrBaseYearValues(LPCSTR pCnty, LPSTR pValueFile, LPSTR pOutFile, int iSkipRows)
{
   int   lCnt, lOut, iRet, iTmp;
   char  sInbuf[4096], sOutbuf[1024], *pTmp;
   FILE  *fdIn, *fdOut;

   LogMsg0("Extract base year values from Value file for %.3s", pCnty);

   // Open input
   LogMsg("Opening ... %s", pValueFile);
   fdIn = fopen(pValueFile, "r");
   if (!fdIn)
   {
      LogMsg("***** Error opening %s (%d)", pValueFile, _errno);
      return -1;
   }

   // Create output file
   LogMsg("Creating ... %s", pOutFile);
   fdOut = fopen(pOutFile, "w");
   if (!fdOut)
   {
      LogMsg("***** Error creating %s (%d)", pOutFile, _errno);
      return -2;
   }

   // Skip header
   for (iTmp = 0; iTmp < iSkipRows; iTmp++)
      pTmp = fgets(sInbuf, 1024, fdIn);

   lCnt=lOut=0;
   // Loop through it
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets(sInbuf, 1024, fdIn)))
         break;
      if (*pTmp < '0')
         break;

      iRet = parseValues(sInbuf, sOutbuf, 0);
      if (!iRet)
      {
         VALUE_REC *pVal = (VALUE_REC *)&sOutbuf[0];

         //memcpy(pVal->TaxYear, myCounty.acYearAssd, 4);
         pVal->CRLF[0] = '\n';
         pVal->CRLF[1] = 0;

         fputs(sOutbuf, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // CLosing
   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Number of records processed: %d", lCnt);
   LogMsg("Number of records output:    %d", lOut);

   return 0;
}

/******************************************************************************
 *
 * Extract current & prop8 values from Tax file
 *
 ******************************************************************************/

int MBT_ExtrCurValues(LPCSTR pCnty, LPSTR pTaxFile, LPSTR pOutFile, int iSkipRows)
{
   int   lCnt, lOut, iRet, iTmp;
   char  sInbuf[4096], sOutbuf[1024], *pTmp;
   FILE  *fdIn, *fdOut;

   LogMsg("Extract values from tax file for %.3s", pCnty);

   // Open input
   LogMsg("Opening ... %s", pTaxFile);
   fdIn = fopen(pTaxFile, "r");
   if (!fdIn)
   {
      LogMsg("***** Error opening %s (%d)", pTaxFile, _errno);
      return -1;
   }

   // Create output file
   LogMsg("Creating ... %s", pOutFile);
   fdOut = fopen(pOutFile, "w");
   if (!fdOut)
   {
      LogMsg("***** Error creating %s (%d)", pOutFile, _errno);
      return -2;
   }

   // Skip header
   for (iTmp = 0; iTmp < iSkipRows; iTmp++)
      pTmp = fgets(sInbuf, 1024, fdIn);

   lCnt=lOut=0;
   // Loop through it
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets(sInbuf, 1024, fdIn)))
         break;

      iRet = parseTaxValues(sInbuf, sOutbuf);
      if (!iRet)
      {
         VALUE_REC *pVal = (VALUE_REC *)&sOutbuf[0];

         memcpy(pVal->TaxYear, myCounty.acYearAssd, 4);
         pVal->CRLF[0] = '\n';
         pVal->CRLF[1] = 0;
         fputs(sOutbuf, fdOut);

         // Duplicate record for Prop8 parcel
         if (pVal->ValueSetType[0] == '8')
         {
            pVal->ValueSetType[0] = '1';
            fputs(sOutbuf, fdOut);
            lOut++;
         }

         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Closing
   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Number of records processed: %d", lCnt);
   LogMsg("Number of records output:    %d", lOut);

   if (lOut < 1000)
      iRet = -1;
   else
      iRet = 0;
   
   return iRet;
}

/******************************************************************************
 *
 * Extract current & prop8 values from Roll file
 *
 ******************************************************************************/

int MBR_ExtrCurValues(LPCSTR pCnty, LPSTR pRollFile, LPSTR pOutFile, int iSkipRows)
{
   int   lCnt, lOut, iRet, iTmp;
   char  sInbuf[4096], sOutbuf[1024], *pTmp;
   FILE  *fdIn, *fdOut;

   LogMsg("Extract values from Roll file for %.3s", pCnty);

   // Open input
   LogMsg("Opening ... %s", pRollFile);
   fdIn = fopen(pRollFile, "r");
   if (!fdIn)
   {
      LogMsg("***** Error opening %s (%d)", pRollFile, _errno);
      return -1;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Create output file
   LogMsg("Creating ... %s", pOutFile);
   fdOut = fopen(pOutFile, "w");
   if (!fdOut)
   {
      LogMsg("***** Error creating %s (%d)", pOutFile, _errno);
      return -2;
   }

   // Skip header
   for (iTmp = 0; iTmp < iSkipRows; iTmp++)
      pTmp = fgets(sInbuf, 1024, fdIn);

   lCnt=lOut=0;
   // Loop through it
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets(sInbuf, 1024, fdIn)))
         break;
      if (*pTmp < '0')
         break;

      iRet = parseRollValues(sInbuf, sOutbuf);
      if (!iRet)
      {
         VALUE_REC *pVal = (VALUE_REC *)&sOutbuf[0];

         // Update Exe
         if (fdExe)
            iRet = updExeValue(sOutbuf, iHdrRows);

         memcpy(pVal->TaxYear, myCounty.acYearAssd, 4);
         pVal->CRLF[0] = '\n';
         pVal->CRLF[1] = 0;
         fputs(sOutbuf, fdOut);

         // Duplicate record for Prop8 parcel
         if (pVal->ValueSetType[0] == '8')
         {
            pVal->ValueSetType[0] = '1';
            fputs(sOutbuf, fdOut);
            lOut++;
         }

         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Closing
   if (fdIn)
      fclose(fdIn);
   if (fdExe)
      fclose(fdExe);

   fclose(fdOut);

   LogMsg("Number of records processed: %d", lCnt);
   LogMsg("Number of records output:    %d", lOut);

   if (lOut < 1000)
      iRet = -1;
   
   return iRet;
}

/******************************************************************************
 *
 * Extract base year values from value file for MB counties (BUT).
 * This function replaces MB_ExtrBaseYearValues() with option to merge with EXE file.
 *
 * Return number of output records.
 *
 ******************************************************************************/

int MB_ExtrValues(LPCSTR pCnty, LPSTR pValueFile, LPSTR pExeFile, LPSTR pOutFile, int iSkipHdrRows)
{
   int   lCnt, lOut, iRet, iTmp;
   char  sInbuf[4096], sOutbuf[2048], sTmpFile[_MAX_PATH], *pTmp;
   FILE  *fdIn, *fdOut;
   VALUE_REC *pVal = (VALUE_REC *)&sOutbuf[0];

   LogMsg0("Extract base year values from Value file for %.3s", pCnty);

   // Open input
   LogMsg("Opening ... %s", pValueFile);
   fdIn = fopen(pValueFile, "r");
   if (!fdIn)
   {
      LogMsg("***** Error opening %s (%d)", pValueFile, _errno);
      return -1;
   }

   // Open Exe file
   if (pExeFile)
   {
      LogMsg("Open Exe file %s", pExeFile);
      fdExe = fopen(pExeFile, "r");
      if (fdExe == NULL)
      {
         LogMsg("***** Error opening Exe file: %s\n", pExeFile);
         return -2;
      }
   } else
      fdExe = NULL;

   // Create output file
   sprintf(sTmpFile, acRawTmpl, pCnty, pCnty, "vmp");
   LogMsg("Creating ... %s", sTmpFile);
   fdOut = fopen(sTmpFile, "w");
   if (!fdOut)
   {
      LogMsg("***** Error creating %s (%d)", sTmpFile, _errno);
      return -2;
   }

   // Skip header
   for (iTmp = 0; iTmp < iSkipHdrRows; iTmp++)
      pTmp = fgets(sInbuf, 1024, fdIn);

   lCnt=lOut=0;
   // Loop through it
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets(sInbuf, 1024, fdIn)))
         break;

      iRet = parseValues(sInbuf, sOutbuf, 0);
      if (!iRet)
      {
         // Update Exe
         if (fdExe && pVal->ValueSetType[0] == '1')
            iRet = updExeValue(sOutbuf, iHdrRows);

         //memcpy(pVal->TaxYear, myCounty.acYearAssd, 4);
         pVal->CRLF[0] = '\n';
         pVal->CRLF[1] = 0;

         fputs(sOutbuf, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // CLosing
   if (fdExe)
      fclose(fdExe);
   fclose(fdIn);
   fclose(fdOut);

   // Sort output
  if (lOut > 100)
   {
      LogMsg("\nSorting value file %s --> %s", sTmpFile, pOutFile);
      iRet = sortFile(sTmpFile, pOutFile, "S(1,12,C,A)");
   } else
   {
      printf("\n");
      iRet = 0;
   }

   LogMsg("Number of records processed: %d", lCnt);
   LogMsg("Number of records output:    %d", lOut);

   return lOut;
}

/******************************************************************************
 *
 * Extract base year values from value file for MB counties (ALP,AMA,MOD,TUO ...).
 * This function replaces MB_ExtrBaseYearValues() with option to merge with EXE file.
 * Use values from LIEN_EXP.[CO3] instead of from value file.

 * Return number of output records.
 *
 ******************************************************************************/

int MB_ExtrValues(LPCSTR pCnty, LPSTR pValueFile, LPSTR pExeFile, LPSTR pLienFile, LPSTR pOutFile, int iSkipHdrRows)
{
   int   lCnt, lOut, iRet, iTmp, iCnty;
   char  sInbuf[4096], sOutbuf[2048], sTmpFile[_MAX_PATH], sTmp[256], *pTmp;
   FILE  *fdIn, *fdOut;
   VALUE_REC *pVal = (VALUE_REC *)&sOutbuf[0];

   LogMsg0("Extract base year values from Value file for %.3s", pCnty);

   // Sort input file
   sprintf(sTmpFile, "%s\\%s\\%s_Value.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(sTmp, "S(#1,C,A,#2,C,A) DEL(%d) OMIT(#1,C,LT,\"0\",OR,#1,C,GT,\"A\")", cValDelim);
   iRet = sortFile(pValueFile, sTmpFile, sTmp);  

   // Open input
   LogMsg("Opening ... %s", sTmpFile);
   fdIn = fopen(sTmpFile, "r");
   if (!fdIn)
   {
      LogMsg("***** Error opening %s (%d)", sTmpFile, _errno);
      return -1;
   }

   // Open lien file
   LogMsg("Opening ... %s", pLienFile);
   fdLienExt = fopen(pLienFile, "r");
   if (!fdLienExt)
   {
      LogMsg("***** Error opening lien extract %s (%d)", pLienFile, _errno);
      return -1;
   }

   // Open Exe file
   fdExe = NULL;
   if (pExeFile)
   {
      sprintf(sTmpFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      sprintf(sTmp, "S(#2,C,A) DEL(%d)", cDelim);
      iRet = sortFile(pExeFile, sTmpFile, sTmp);  
      if (iRet > 0)
      {
         LogMsg("Open Exe file %s", sTmpFile);
         fdExe = fopen(sTmpFile, "r");
         if (fdExe == NULL)
         {
            LogMsg("***** Error opening Exe file: %s\n", sTmpFile);
            return -2;
         }
      }
   }

   // Create output file
   sprintf(sTmpFile, acRawTmpl, pCnty, pCnty, "vmp");
   LogMsg("Creating ... %s", sTmpFile);
   fdOut = fopen(sTmpFile, "w");
   if (!fdOut)
   {
      LogMsg("***** Error creating %s (%d)", sTmpFile, _errno);
      return -2;
   }

   // Skip header
   //for (iTmp = 0; iTmp < iSkipHdrRows; iTmp++)
   //   pTmp = fgets(sInbuf, 1024, fdIn);

   if (!_memicmp(pCnty, "ALP", 3))
      iCnty = 2;
   else if (!_memicmp(pCnty, "COL", 3))
      iCnty = 6;
   else if (!_memicmp(pCnty, "MAD", 3))
      iCnty = 20;
   else if (!_memicmp(pCnty, "MOD", 3))
      iCnty = 25;
   else
      iCnty = 0;

   lCnt=lOut=0;
   // Loop through it
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets(sInbuf, 1024, fdIn)))
         break;

      iRet = parseValues(sInbuf, sOutbuf, 0, iCnty);
      if (!iRet && pVal->ValueSetType[0] > ' ')
      {
#ifdef _DEBUG
         //if (!memcmp(pTmp, "001011002000", 9))  
         //   pTmp = NULL;
#endif
         // Update lien values
         if (fdLienExt)
         {
            if (pVal->ValueSetType[0] == '1')
               iRet = updLienValue(sOutbuf, false);               // Update Exemption & others

            if (iCnty == 25 && pVal->ValueSetType[0] == '0')
               iRet = updLienValue(sOutbuf, true);                // Update Land & Impr
         } else
            iRet = 1;

         // Update Exe only if not found in LienExt
         if (iRet && fdExe && pVal->ValueSetType[0] == '1')
            iRet = updExeValue(sOutbuf, iHdrRows);

         pVal->CRLF[0] = '\n';
         pVal->CRLF[1] = 0;

         fputs(sOutbuf, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // CLosing
   if (fdExe)
      fclose(fdExe);
   if (fdLienExt)
      fclose(fdLienExt);
   fclose(fdIn);
   fclose(fdOut);

   // Sort output
   if (lOut > 100)
   {
      char acSrtCmd[256];

      iRet = GetIniString(pCnty, "ValueSortCmd", "S(1,12,C,A,259,1,C,D)", acSrtCmd, 256, acIniFile);

      LogMsg("\nSorting value file %s --> %s", sTmpFile, pOutFile);
      iRet = sortFile(sTmpFile, pOutFile, acSrtCmd);
   } else
   {
      printf("\n");
      iRet = 0;
   }

   LogMsg("Number of records processed: %d", lCnt);
   LogMsg("             records output: %d", lOut);
   LogMsg("                EXE matched: %d", lExeMatch);

   return lOut;
}

/******************************************************************************
 *
 * Extract base year values from value file for MB counties (BUT) then merge
 * record of settype=1 and settype=8 to output single record with settype=1 and reason=8
 *
 * Return number of output records.
 *
 ******************************************************************************/

int MB_ExtrMergeValues(LPCSTR pCnty, LPSTR pValueFile, LPSTR pExeFile, LPSTR pOutFile, int iSkipRows)
{
   int   lCnt, lOut, iRet, iTmp;
   char  sInbuf[4096], sOutbuf[2048], sTmpFile[_MAX_PATH], *pTmp;
   FILE  *fdIn, *fdOut;
   VALUE_REC *pVal = (VALUE_REC *)&sOutbuf[0];

   LogMsg0("Merge base year and prop8 Value for %.3s", pCnty);

   // Open input
   LogMsg("Opening ... %s", pValueFile);
   fdIn = fopen(pValueFile, "r");
   if (!fdIn)
   {
      LogMsg("***** Error opening %s (%d)", pValueFile, _errno);
      return -1;
   }

   // Open Exe file
   if (pExeFile)
   {
      LogMsg("Open Exe file %s", pExeFile);
      fdExe = fopen(pExeFile, "r");
      if (fdExe == NULL)
      {
         LogMsg("***** Error opening Exe file: %s\n", pExeFile);
         return -2;
      }
   } else
      fdExe = NULL;

   // Create output file
   sprintf(sTmpFile, acRawTmpl, pCnty, pCnty, "tmp");
   LogMsg("Creating ... %s", sTmpFile);
   fdOut = fopen(sTmpFile, "w");
   if (!fdOut)
   {
      LogMsg("***** Error creating %s (%d)", sTmpFile, _errno);
      return -2;
   }

   // Skip header
   for (iTmp = 0; iTmp < iSkipRows; iTmp++)
      pTmp = fgets(sInbuf, 1024, fdIn);

   // Init
   lCnt=lOut=0;

   // Loop through it
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets(sInbuf, 1024, fdIn)))
         break;

      iRet = parseValues(sInbuf, sOutbuf, 0);
      if (!iRet)
      {
         // Update Exe
         if (fdExe && (pVal->ValueSetType[0] == '1' || pVal->ValueSetType[0] == '8'))
            iRet = updExeValue(sOutbuf, iHdrRows);

         if (pVal->ValueSetType[0] == '8')
         {
            pVal->ValueSetType[0] = '1';
            pVal->Reason[0] = '8';
         }
         pVal->CRLF[0] = '\n';
         pVal->CRLF[1] = 0;
         fputs(sOutbuf, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   if (fdExe)
      fclose(fdExe);
   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Number of records processed: %d", lCnt);
   LogMsg("Number of records output:    %d", lOut);

   // Dedup
   if (lOut > 100)
   {
      LogMsg("\nSorting value file %s --> %s", sTmpFile, pOutFile);
      iRet = sortFile(sTmpFile, pOutFile, "S(1,14,C,A,21,1,C,A,259,1,C,D) DUPO(B4096,1,77)");
   } else
      iRet = lOut;
   
   return iRet;
}

/******************************************************************************
 *
 * Extract base year values from value file for MB counties (AMA) then merge
 * record of settype=1 and settype=8 to output single record with settype=1 and reason=8
 * Use values from LIEN_EXP.[CO3] instead of from value file.
 *
 * Return number of output records.
 *
 ******************************************************************************/

int MB_ExtrMergeValues(LPCSTR pCnty, LPSTR pValueFile, LPSTR pExeFile, LPSTR pLienFile, LPSTR pOutFile, int iSkipRows)
{
   int   lCnt, lOut, iRet, iTmp;
   char  sInbuf[4096], sOutbuf[2048], sTmpFile[_MAX_PATH], *pTmp;
   FILE  *fdIn, *fdOut;
   VALUE_REC *pVal = (VALUE_REC *)&sOutbuf[0];

   LogMsg0("Merge base year and prop8 Value for %.3s", pCnty);

   // Open input
   LogMsg("Opening ... %s", pValueFile);
   fdIn = fopen(pValueFile, "r");
   if (!fdIn)
   {
      LogMsg("***** Error opening %s (%d)", pValueFile, _errno);
      return -1;
   }

   // Open lien file
   fdLienExt = fopen(pLienFile, "r");
   if (!fdLienExt)
   {
      LogMsg("***** Error opening lien extract %s (%d)", pLienFile, _errno);
      return -1;
   }

   // Open Exe file
   if (pExeFile)
   {
      LogMsg("Open Exe file %s", pExeFile);
      fdExe = fopen(pExeFile, "r");
      if (fdExe == NULL)
      {
         LogMsg("***** Error opening Exe file: %s\n", pExeFile);
         return -2;
      }
   } else
      fdExe = NULL;

   // Create output file
   sprintf(sTmpFile, acRawTmpl, pCnty, pCnty, "tmp");
   LogMsg("Creating ... %s", sTmpFile);
   fdOut = fopen(sTmpFile, "w");
   if (!fdOut)
   {
      LogMsg("***** Error creating %s (%d)", sTmpFile, _errno);
      return -2;
   }

   // Skip header
   for (iTmp = 0; iTmp < iSkipRows; iTmp++)
      pTmp = fgets(sInbuf, 1024, fdIn);

   // Init
   lCnt=lOut=0;

   // Loop through it
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets(sInbuf, 1024, fdIn)))
         break;

      iRet = parseValues(sInbuf, sOutbuf, 0);
      if (!iRet)
      {
         // Update lien values
         if (fdLienExt && (pVal->ValueSetType[0] == '1' || pVal->ValueSetType[0] == '8'))
            iRet = updLienValue1(sOutbuf);

         // Update Exe only if not found in LienExt
         if (iRet && fdExe && (pVal->ValueSetType[0] == '1' || pVal->ValueSetType[0] == '8'))
            iRet = updExeValue(sOutbuf, iHdrRows);

         if (pVal->ValueSetType[0] == '8')
         {
            pVal->ValueSetType[0] = '1';
            pVal->Reason[0] = '8';
         }
         pVal->CRLF[0] = '\n';
         pVal->CRLF[1] = 0;
         fputs(sOutbuf, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   if (fdExe)
      fclose(fdExe);
   if (fdLienExt)
      fclose(fdLienExt);
   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Number of records processed: %d", lCnt);
   LogMsg("Number of records output:    %d", lOut);

   // Dedup
   if (lOut > 100)
   {
      LogMsg("\nSorting value file %s --> %s", sTmpFile, pOutFile);
      iRet = sortFile(sTmpFile, pOutFile, "S(1,14,C,A,21,1,C,A,259,1,C,D) DUPO(B4096,1,77)");
   } else
      iRet = lOut;
   
   return iRet;
}

/********************************************************************************
 *
 * Prop 8: 001-140-020, pv_reason codes of 1, 3, 8, need to NULL codes 3 and 8 and set 1 equal to 8.
 * Prop 13: 001-030-004, pv_reason codes of 1 and 3, need to NULL code 3 and set 1 equal to 3.
 * Prop 13 w/one code: 012-080-011, pv_reason code of 1 only, need to set 1 equal to 3.
 * Restricted: 001-140-043, pv_reason codes of 1, 3, 5, need to NULL codes 3 and 5 and set 1 equal to 5.
 *
 * Special Cases
 * Any record with pv_reason=2, set pv_vst_type to NULL since this reason code is not used.
 *
 * Restricted w/code 2: 001-140-044, pv_reason codes of 1, 3, 2, 5, need to NULL codes 3, 2 and 5 and set 1 equal to 5.
 * 
 * Prop 8 w/code 5: 001-170-026, pv_reason codes of 1, 3, 8,5, need to NULL codes 3, 8 and 5 and set 1 equal to 8. Also need to set pv_vst_type of 9 to NULL since we cant have more than three VSTs.
 *
 * Prop 13 w/code 2: 001-190-013, pv_reason codes of 1, 3 and 2, need to NULL codes 3 and 2 and set 1 equal to 3
 *
 *********************************************************************************/

int MB_ResetPVReason(char *pInFile, char *pOutFile)
{
   char     *pTmp, acRec[1024], acApn[32], acReason[4], acPrevVst[4];
   int      lCnt=0;
   FILE     *fdIn, *fdOut;

   // Input record
   VALUE_REC *pVal;

   LogMsg0("Reset PV Reason in %s", pInFile);

   // Open Value file
   LogMsg("Open value file %s", pInFile);
   fdIn = fopen(pInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening value file: %s\n", pInFile);
      return -1;
   }

   LogMsg("Open output file %s", pOutFile);
   if (!(fdOut = fopen(pOutFile, "w")))
   {
      LogMsg("***** Error creating output file %s", pOutFile);
      return -2;
   }

   pVal = (VALUE_REC *)&acRec[0];
   memset(acApn, ' ', 20);
   memset(acReason, ' ', 4);
   memset(acPrevVst, ' ', 4);

   // Merge loop
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], 1024, fdIn);
      if (!pTmp)
         break;

#ifdef _DEBUG
      //if (!memcmp(pTmp, "009030003000", 9))  
      //   pTmp = NULL;
#endif
      // Set reason
      if (memcmp(acApn, pVal->Apn, iApnLen))
      {
         memcpy(acApn, pVal->Apn, iApnLen);
         memcpy(acReason, pVal->Reason, 2);
         memcpy(acPrevVst, pVal->ValueSetType, 2);

         if (pVal->Reason[0] > '1')
            memset(pVal->Reason, ' ', sizeof(pVal->Reason));
         else
            memcpy(pVal->Reason, "3 ", 2);
      } else if (pVal->Reason[0] > '1')
      {
         memset(pVal->Reason, ' ', sizeof(pVal->Reason));
         if (memcmp(pVal->ValueSetType, acPrevVst, 2) > 0)
            memset(pVal->ValueSetType, ' ', 2);
         else
            memcpy(acPrevVst, pVal->ValueSetType, 2);
      }

      if (pVal->Reason[0] == '1' && memcmp(acReason, "1 ", 2) > 0)
         memcpy(pVal->Reason, acReason, 2);

      fputs(acRec, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total Value records for import: %u", lCnt);

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   // Resort output
   LogMsg("\nResorting value file %s --> %s", pOutFile, pInFile);
   lCnt = sortFile(pOutFile, pInFile, "S(1,14,C,A,21,1,C,A,259,1,C,D) DUPO(B4096,1,77)");

   return lCnt;
}

/********************************************************************************
 *
 *
 *********************************************************************************/

int MB_FixValues(char *pInFile, char *pCnty)
{
   char     *pTmp, acRec[1024], acSaveRec[1024], acTmpFile[_MAX_PATH];
   int      lCnt=0, lFix=0;
   FILE     *fdIn, *fdOut;
   VALUE_REC *pVal, *pSaveVal;

   LogMsg0("Fix values for VulueSetType 2 in %s", pInFile);

   // Open Value file
   LogMsg("Open value file %s", pInFile);
   fdIn = fopen(pInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening value file: %s\n", pInFile);
      return -1;
   }

   // Create temp output file
   sprintf(acTmpFile, acRawTmpl, pCnty, pCnty, "tmp");
   LogMsg("Create temp output file %s", acTmpFile);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error Create temp output file %s", acTmpFile);
      return -2;
   }

   pVal = (VALUE_REC *)&acRec[0];
   pSaveVal = (VALUE_REC *)&acSaveRec[0];
   memset(acSaveRec, ' ', sizeof(VALUE_REC));

   // Merge loop
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], 1024, fdIn);
      if (!pTmp)
         break;

#ifdef _DEBUG
      //if (!memcmp(pTmp, "009030003000", 9))  
      //   pTmp = NULL;
#endif

      // Copy data
      if (pSaveVal->Apn[0] > ' ')
      {
         if (!memcmp(pSaveVal->Apn, pVal->Apn, iApnLen))
         {
            memcpy(pVal->Land, pSaveVal->Land, PV_SIZ_LAND);
            memcpy(pVal->Impr, pSaveVal->Impr, PV_SIZ_LAND);
            memcpy(pVal->Fixtr, pSaveVal->Fixtr, PV_SIZ_LAND);
            memcpy(pVal->Businv, pSaveVal->Businv, PV_SIZ_LAND);
            memcpy(pVal->PersProp, pSaveVal->PersProp, PV_SIZ_LAND);
            memcpy(pVal->PP_MH, pSaveVal->PP_MH, PV_SIZ_LAND);
            memcpy(pVal->GrowImpr, pSaveVal->GrowImpr, PV_SIZ_LAND);
            memcpy(pVal->Other, pSaveVal->Other, PV_SIZ_LAND);
            memcpy(pVal->TotalVal, pSaveVal->TotalVal, PV_SIZ_LAND);
            lFix++;
         } else if (pVal->ValueSetType[0] == '0')
         {
            memcpy(acSaveRec, acRec, sizeof(VALUE_REC));
            pVal->Apn[0] = 0;          // Skip this record
         } else
            pSaveVal->Apn[0] = 0;
      } else if (pVal->ValueSetType[0] == '0')
      {
         memcpy(acSaveRec, acRec, sizeof(VALUE_REC));
         pVal->Apn[0] = 0;          // Skip this record
      } 

      if (pVal->Apn[0] > ' ')
         fputs(acRec, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total records process: %u", lCnt);
   LogMsg("        records fixed: %u", lFix);

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   // Resort output
   LogMsg("\nResorting value file %s --> %s", acTmpFile, pInFile);
   lCnt = sortFile(acTmpFile, pInFile, "S(1,14,C,A,259,1,C,D)");

   return lCnt;
}
