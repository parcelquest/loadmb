#ifndef  _MERGEPLU_H_
#define  _MERGEPLU_H_

// Plu_Zone.txt
#define  PLU_ZONE_ASMT           0
#define  PLU_ZONE_ACRES          1
#define  PLU_ZONE_TYPE           2
#define  PLU_ZONE_CODE           3

// 2024
#define  PLU_EXE_STATUS          0
#define  PLU_EXE_ASMT            1
#define  PLU_EXE_CODE            2
#define  PLU_EXE_HOEXE           3
#define  PLU_EXE_MAXAMT          4
#define  PLU_EXE_EXEPCT          5

// 2016
//#define  PLU_L_FEEPARCEL         0
//#define  PLU_L_ASMT              1
//#define  PLU_L_TRA               2
//#define  PLU_L_TAXABILITY        3
//#define  PLU_L_CREATINGDOCNUM    4
//#define  PLU_L_CURRENTDOCNUM     5
//#define  PLU_L_LANDUSE1          6
//#define  PLU_L_LANDUSE2          7
//#define  PLU_L_ASSESSEENAME      8
//#define  PLU_L_ADDRESS1          9
//#define  PLU_L_ADDRESS2          10
//#define  PLU_L_ADDRESS3          11
//#define  PLU_L_ADDRESS4          12
//#define  PLU_L_ZIPFORSORTONLY    13
//#define  PLU_L_ELAND             14
//#define  PLU_L_EIMPR             15
//#define  PLU_L_EPPROP            16
//#define  PLU_L_EHOE              17
//#define  PLU_L_ENONHOE           18
//#define  PLU_L_MAXCNT            19

// 2017
#define  PLU_L_FEEPARCEL         0
#define  PLU_L_ASMT              1
#define  PLU_L_ACRES             2
#define  PLU_L_TRA               3
#define  PLU_L_TAXABILITY        4
#define  PLU_L_CREATINGDOCNUM    5
#define  PLU_L_CURRENTDOCNUM     6
#define  PLU_L_LANDUSE1          7
#define  PLU_L_LANDUSE2          8
#define  PLU_L_ASSESSEENAME      9
#define  PLU_L_ADDRESS1          10
#define  PLU_L_ADDRESS2          11
#define  PLU_L_ADDRESS3          12
#define  PLU_L_ADDRESS4          13
#define  PLU_L_ZIPFORSORTONLY    14
#define  PLU_L_ELAND             15
#define  PLU_L_EIMPR             16
#define  PLU_L_EPPROP            17
#define  PLU_L_EHOE              18
#define  PLU_L_ENONHOE           19
#define  PLU_L_MAXCNT            20

#define  PLU_CHAR_FEEPARCEL            0
#define  PLU_CHAR_CATTYPE              1     // 0 � Old record/not yet updated
                                             // 1 � Rural/Agricultural
                                             // 2 � Commercial
                                             // 3 � Industrial
                                             // 4 � Residential
#define  PLU_CHAR_BLDGSEQNUM           2
#define  PLU_CHAR_YEARBUILT            3
#define  PLU_CHAR_EFFECTIVEYEAR        4
#define  PLU_CHAR_BUILDINGSIZE         5
#define  PLU_CHAR_STORIESCNT           6
#define  PLU_CHAR_UNITSCNT             7
#define  PLU_CHAR_BEDROOMS             8
#define  PLU_CHAR_BATHROOMS            9
#define  PLU_CHAR_QUALITYCLASS         10
#define  PLU_CHAR_HALFBATHS            11
#define  PLU_CHAR_COOLINGCENTRALAC     12
#define  PLU_CHAR_COOLINGEVAPORATIVE   13
#define  PLU_CHAR_COOLINGROOMWALL      14
#define  PLU_CHAR_COOLINGWINDOW        15
#define  PLU_CHAR_HEATING              16
#define  PLU_CHAR_FIREPLACE            17
#define  PLU_CHAR_ATTACHGARAGESF       18
#define  PLU_CHAR_DETACHGARAGESF       19
#define  PLU_CHAR_CARPORTSF            20
#define  PLU_CHAR_PATIOSF              21
#define  PLU_CHAR_POOLSPA              22
#define  PLU_CHAR_HASWELL              23
#define  PLU_CHAR_LANDSQFT             24
#define  PLU_CHAR_TOTALROOMS           25
#define  PLU_CHAR_ASMT                 26

static XLAT_CODE  asPool[] =
{  // 11/29/2018
   // Value, lookup code, value length
   "PL",  "P", 2,               // Pool
   "PS",  "C", 2,               // Pool & Spa
   "SP",  "S", 2,               // Spa
   "IN",  "P", 2,               // Infinity pool
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{  // 11/29/2018
   // Value, lookup code, value length
   "FP",    "Y", 2,             // Fireplace
   "FPWS",  "W", 4,             // Fireplace and Wood Stove
   "MFPWS", "M", 5,             // Multiple Fireplaces/Wood Stove
   "WS",    "W", 2,             // Wood Stove
   "99",    "Y", 2,             // Other
   "",      "", 0
};

static XLAT_CODE  asHeating[] =
{
// Value, lookup code, value length
   "BB", "F", 2,               // Baseboard 
   "FA", "B", 2,               // Forced Air 
   "GT", "4", 2,               // Geo Thermal
   "OI", "5", 2,               // Oil Stove
   "PR", "N", 2,               // Propane
   "SO", "K", 2,               // Solar
   "WH", "D", 2,               // Wall Heater
   "WS", "S", 2,               // Wood Stove
   "",   "",  0
};

static XLAT_CODE  asQual[] =
{
   // Value, lookup code, value length
   "AV",    "A",  2,           
   "EX",    "E",  2,              
   "FAIR",  "F",  4,              
   "GOOD",  "G",  4,             
   "VG",    "V",  2,             
   "POOR",  "P",  4,      
   "ADDN",  " ",  4,
   "CABI",  " ",  4,
   "CHUR",  " ",  4,
   "DII",   " ",  3,
   "EQUI",  " ",  4,
   "GARA",  " ",  4,
   "LIC",   " ",  3,
   "LOG",   " ",  3,
   "STOR",  " ",  4,
   "MTL",   " ",  3,
   "POLE",  " ",  4,
   "RAMA",  " ",  4,
   "RMAD",  " ",  4,
   "-AV",   "A",  3,
   "-GO",   "G",  3,
   "SHED",  " ",  4,
   "SHOP",  " ",  4,
   "STE",   " ",  3,
   "",      "",  0
};

IDX_TBL5 PLU_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "01", "1 ", 'N', 2, 2,     // SALE - 100% TRANSFER
   "02", "57", 'N', 2, 2,     // PARTIAL INTEREST TRANSFER - NON REAPP
   "03", "74", 'Y', 2, 2,     // NON-REAPPRAISABLE EVENT
   "04", "57", 'N', 2, 2,     // PARTIAL INTEREST TRANSFER - REAPP
   "05", "1 ", 'N', 2, 2,     // NEW MOBILE HOME
   "06", "73", 'N', 2, 2,     // TRANSFER PER DEFAULT (TRUSTEE DEED UPON SALE)
   "07", "58", 'N', 2, 2,     // POSSESSORY INTEREST
   "08", "74", 'Y', 2, 2,     // CLERICAL FUNCTION
   "09", "74", 'Y', 2, 2,     // SPLIT/COMBO
   "10", "19", 'Y', 2, 2,     // CIP/NO START
   "11", "75", 'Y', 2, 2,     // TRANSFER ON DEATH DEED
   "13", "1 ", 'N', 2, 2,     // NEW SFR
   "14", "74", 'Y', 2, 2,     // NEW GARAGE/CARPORT
   "15", "74", 'Y', 2, 2,     // MH CONVERTED TO TAXROLL
   //"16", "74", 'Y', 2, 2,     // DWELLING ADDITION/CONVERSION
   //"17", "74", 'Y', 2, 2,     // GARAGE ADDITION/CONV/PATIO/DECK/POOL/SPA
   //"18", "74", 'Y', 2, 2,     // MISC ADDITION
   //"19", "74", 'Y', 2, 2,     // MISC REMODEL
   //"20", "74", 'Y', 2, 2,     // RAMADA
   //"21", "74", 'Y', 2, 2,     // RES REPAIRS AND MAINTENANCE
   //"22", "74", 'Y', 2, 2,     // ADA IMPROVEMENTS
   //"23", "74", 'Y', 2, 2,     // NEW MULTIPLE BLDG
   //"24", "74", 'Y', 2, 2,     // ADDITION/CONV TO MULTIPLE
   //"25", "74", 'Y', 2, 2,     // MISC MULTIPLE
   //"26", "74", 'Y', 2, 2,     // RES PATIO
   //"27", "74", 'Y', 2, 2,     // NEW MULTIPLE BLDG
   //"28", "74", 'Y', 2, 2,     // ADD'N/CONV TO MULTIPLE
   //"29", "74", 'Y', 2, 2,     // MISC MULTIPLE
   "57", "74", 'Y', 2, 2,     // HANGAR
   "58", "74", 'Y', 2, 2,     // NEW AG BLDG
   "80", "74", 'Y', 2, 2,     // NON-TAXABLE GOVERNMENT PROP
   "92", "74", 'Y', 2, 2,     // PROP 58 EXCLUSION
   "","",0,0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 PLU_Exemption[] = 
{
   "E01", "H", 3,1,     // HOMEOWNER EXEMPTION
   "E02", "E", 3,1,     // CEMETERY EXEMPTION
   "E03", "W", 3,1,     // WELFARE EXEMPTION
   "E04", "V", 3,1,     // VETERANS ORGANIZATIONS
   "E05", "C", 3,1,     // CHURCH EXEMPTION
   "E06", "V", 3,1,     // VETERAN EXEMPTION
   "E07", "C", 3,1,     // PARTIAL CHURCH EXEMPTION
   "E08", "M", 3,1,     // MUSEUM EXEMPTION
   "E09", "D", 3,1,     // TOTALLY DISABLED VET EXEMPTION
   "E10", "D", 3,1,     // TOTALLY DISABLED VET EXEMPTION
   "E12", "D", 3,1,     // TOTALLY DISABLED VET EXEMPTION
   "E13", "X", 3,1,     // LOW VALUE EXEMPTION
   "E15", "P", 3,1,     // PUBLIC SCHOOL EXEMPTION
   "E16", "U", 3,1,     // COLLEGE EXEMPTION
   "E17", "X", 3,1,     // LOW INCOME TRIBAL HOUSING
   "E26", "X", 3,1,     // LESSORS EXEMPTION
   "E27", "X", 3,1,     // HISTORICAL AIRCRAFT EXEMPTION
   "E41", "R", 3,1,     // RELIGIOUS EXEMPTION
   "E42", "R", 3,1,     // PARTIAL RELIGIOUS EXEMPTION
   "E45", "X", 3,1,     // OTHER EXEMPTION - FAIRGROUNDS
   "E46", "C", 3,1,     // PARTIAL CHURCH EXEMPTION
   "E47", "C", 3,1,     // PARTIAL CHURCH EXEMPTION
   "E53", "W", 3,1,     // PARTIAL WELFARE EXEMPTION
   "","",0,0
};

#endif