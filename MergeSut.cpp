/******************************************************************************
 *
 * Input files:
 *    LienFile=[CO_DATA]\SUT\2022\2022 Roll Data - All Parcels.txt
 *    BPPFile=[CO_DATA]\SUT\2022\2022_BPP.txt
 *       - This file is extracted from LienFile and contains only BPP records
 *    RollFile=[CO_DATA]\SUT\CA-Sutter-AsmtRoll.csv
 *    CharFile=[CO_DATA]\SUT\CA-Sutter-PropChar.csv
 *    SaleFile=[CO_DATA]\SUT\CA-Sutter-Sales.csv
 *    TaxDetail=[CO_DATA]\SUT\CA-Sutter-CurrentDistricts.csv
 *    TaxPaid=[CO_DATA]\SUT\CA-Sutter-CurrentAmounts.csv
 *    Redemption=[CO_DATA]\SUT\CA-Sutter-PriorYear.csv
 *
 * Operation commands:
 *    - Normal update:  LoadOne -CSUT -U -Xs -Xa -Mz [-Mr]
 *    - Load Lien:      LoadOne -CSUT -L
 *    - Load tax:       LoadOne -CSUT -T
 *
 * Revision:
 * 04/27/2022 21.9.1    Move from LoadOne to process new CSV format
 * 05/10/2022           Fix Sut_FormatPrevApn() and save new APN in ALT_APN.  Correct tax data.
 *                      Ignore unsecured and PI records.  Clean up UseCode.
 *                      Load NDC sale data before county sale to keep history sale in place.
 * 08/04/2022 22.1.3    Add Sut_ReformatApn() & Sut_FormatNewApn() to convert file between now and old APN.  
 *                      Add Sut_MergeBpp() & Sut_MergeOthers() to fill in missing data from LDR file. 
 *                      Modify Sut_MergeOwner() to handle different owner format in LDR file.  
 *                      Modify Sut_MergeLienCsv() to put LIVIMPR into TREEVINES.
 * 08/09/2022 22.1.4    Fix Sut_FormatPrevApn(). Modify Sut_MergeRollCsv() & Sut_Load_RollCsv()
 *                      to hardcode MAPLINK to old BOOK/PAGE format.  Reset this when new maps avail.
 * 11/02/2022 22.2.7    Modify all tax related functions to support new tax layouts and field formats.
 * 12/08/2022 22.5.1    Modify Sut_ExtrLienRec() & Sut_MergeRollCsv() due to change in SUT_LIENEXTR which renames 
 *                      Tree to LivingImpr.  We now put LIVIMPR into OTH_IMPR.
 * 04/14/2023 22.7.2    Modify Sut_Load_LDR_Csv() and add Sut_MergeBpp2() to add PP value to roll record.
 * 07/19/2023 23.0.5    Modify Sut_ConvStdCharCsv() to update QualityClass.  Modify Sut_MergeLienCsv()
 *                      and Sut_ExtrLienRec() to remove comma before caculating values.  Modify Sut_MergeRollCsv()
 *                      and Sut_MergeOthers() to use appropriate parsing function based on delimiter.
 *                      Modify Sut_Load_TaxCurrent() to change sort command due to new record format.
 * 08/03/2023 23.1.1    Modify Sut_MergeRollCsv() to set PUBL_FLG=Y for government parcels, prefill APN
 *                      with '0' if needed for incomplete APN, stop populate PREV_APN.  Modify Sut_Load_RollCsv() 
 *                      to change sort command on AsmtRoll file.
 * 08/07/2023 23.1.4    Modify Sut_MergeRollCsv() & Sut_Load_RollCsv() to support new APN format 999-999-999-999A.
 * 09/19/2023 23.1.11   Add option to extract daily input zip file.  Remove convert UNICODE to ASCII.
 *                      Add skip header to sale and char processing.  Modify Sut_Load_RollCsv() to handle APN compare.
 * 10/03/2023 23.2.1    Modify Sut_Load_RollCsv() to rebuild csv file before processing due to broken records in file.
 * 10/16/2023 23.3.1    Modify Sut_CreateTaxItems() to copy TaxCode up tp 10 bytes.
 * 10/19/2023 23.4.0    Modify tax processing procedure and function to use new tax file.  If TaxMaster is defined,
 *                      use Sut_Load_TaxBase(), otherwise, use Sut_Load_TaxCurrent() to update paid amount.
 * 03/20/2024 23.7.1    Modify loadSut() to use lProcDate to format input zip file instead of lToday.
 * 05/20/2024 23.8.4    Add Sut_RebuildCsv() and modify Sut_Load_RollCsv() to remove unused code.
 * 08/02/2024 24.0.9    Modify Sut_MergeSAdr() to fix missing UNIT# (1395 SANBORN RD E).
 *                      Modify Sut_MergeLienCsv() to add ExeType.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "hlAdo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "LoadMB.h"
#include "MergeSut.h"
#include "UseCode.h"
#include "MBExtrn.h"
#include "Tax.h"
#include "NdcExtr.h"
#include "CharRec.h"
#include "doZip.h"

static   FILE  *fdLien, *fdDelq, *fdChar, *fdBpp;
static   long  lCharSkip, lCharMatch, lSaleSkip, lSaleMatch, lLienSkip, lLienMatch, lUnkMailAdr;
static   long  lSec, lPos;
int      Sut_MergeBpp2(char *pOutbuf);
//int      Sut_MergeSitusX(char *pOutbuf);
//int      Sut_MergeMailingX(char *pOutbuf);
//void     Sut_ParseAdr1(ADR_REC *pAdrRec, char *pAddr);

/******************************* RebuildCsv ********************************
 *
 * This function is copied from RebuildCsv_IQ() in Utils.cpp and modify for SUT
 * where APN may have "-" at position 7.
 *
 * Return number of output records if success, <0 if error.
 *
 ****************************************************************************/

int Sut_RebuildCsv(LPSTR pInfile, unsigned char cDelim, int iTokens)
{
   int   iCnt=0, iRet;
   char  sBuf[65536], sOutfile[_MAX_PATH], *pTmp;
   FILE  *fdInput, *fdOutput;

   LogMsg0("Sut_RebuildCsv() %s", pInfile);

   // Verify file open OK
   fdInput = fopen(pInfile, "r");
   if (!fdInput)
   {
      LogMsg("***** Cannot open input file %s\n", pInfile);
      return -1;
   }

   strcpy(sOutfile, pInfile);
   if (pTmp = strchr(sOutfile, '.'))
   {
      strcpy(pTmp, ".tmp");
   } else
   {
      LogMsg("***** Bad input file %s.  Please check INI file.\n", pInfile);
      return -1;
   }

   fdOutput = fopen(sOutfile, "w");
   if (!fdOutput)
   {
      fclose(fdInput);
      LogMsg("***** Cannot open output file %s\n", sOutfile);
      return -2;
   }
            
   while (!feof(fdInput))
   {
#ifdef _DEBUG
      //if (!memcmp(sBuf, "92082|06300400090000", 9))    // 1403606..1406923
      //   iRet = 0;
#endif
      iRet = myGetStrTC_IQ(sBuf, cDelim, iTokens, 65536, fdInput);
      if (iRet > 0)
      {
         iRet = replChar(sBuf, 13, 32);
         strcat(sBuf, "\n");     // Put CRLF back on
         iRet = strlen(sBuf);
         if (iRet >= 65535)
            LogMsg("*** WARNING: Sut_RebuildCsv() has large string of %d characters.  Please check it out", iRet);
         if (sBuf[6] == '-')
         {
            LogMsg("*** Fix APN %.14s", sBuf);
            memcpy(&sBuf[6], &sBuf[7], 7);
            sBuf[13] = ' ';
         }
         fputs(sBuf, fdOutput);
      } else
      {
         if (iRet == -2)
            LogMsg("***** Record corruption: %.50s", sBuf);
         else if (iRet == -3)
            LogMsg("*** Too many tokens in row: %d", iCnt);
         break;
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdInput);
   fclose(fdOutput);

   // Rename output file
   strcpy(sBuf, pInfile);
   pTmp = strchr(sBuf, '.');
   strcpy(pTmp, ".sav");
   if (!_access(sBuf, 0))
      remove(sBuf);
   iRet = rename(pInfile, sBuf);
   iRet |= rename(sOutfile, pInfile);
   if (!iRet)
      LogMsg("RebuildCsv completed with %d records output.", iCnt);
   else
   {
      LogMsg("***** Error renaming %s to %s.", sOutfile, pInfile);
      iCnt = 0;
   }
   return iCnt;
}

/******************************************************************************
*
* Reformat new APN to old APN
* Return old apn length if successful, 0 if failed
*
*******************************************************************************/

int Sut_FormatPrevApn(char *pApn, char *pPrevApn)
{
   int   iTmp, iRet;

   // Secured parcel 
   // New 999-999-999-000
   // Old  99-999-999
   iTmp = atoin(pApn+9, 3);
#ifdef  _DEBUG
   //if (!memcmp(pApn, "0172300030S0", 12))
   //   iRet = 0;
#endif

   if (!iTmp)
   {
      if (isalpha(*(pApn+10)))
         iRet = sprintf(pPrevApn, "%.8s%c   ", pApn+1, *(pApn+10));
      else
         iRet = sprintf(pPrevApn, "%.8s    ", pApn+1);
   } else if (iTmp == 10)
      iRet = sprintf(pPrevApn, "%.8sT   ", pApn+1);           // Mobile home
   else if (iTmp == 20)
      iRet = sprintf(pPrevApn, "%.8sZ   ", pApn+1);           // Mobile home
   else if (iTmp == 300)
      iRet = sprintf(pPrevApn, "%.8sM   ", pApn+1);           // Gas well
   else if (iTmp == 400)
      iRet = sprintf(pPrevApn, "%.8sQ   ", pApn+1);           // Mineral
   else
      iRet = 0;

   return iRet;
}

/******************************************************************************
 *
 *  SFR
 *  0+8 digit APN+000               Example: Old APN   06-010-001       New PIN:    006-010-001-000
 *                                                     23-192-008-A                 023-192-008-0A0
 *  Mobile Homes
 *  What used to be the APN + T/Z is now
 *
 *  T Acct. - 7+11 digit APN + 010 - Example: Old APN: 33-230-050-T    	New PIN: 	733-230-050-010
 *											                                                   733230050010
 *  Z Acct. - 7+11 digit APN + 020 - Example: Old APN: 22-150-013-Z    	New PIN: 	722-150-013-020
 *												                                                722150013020
 *  Gas Wells
 *  What used to be the APN + M is now
 *
 *  0 + 11 digit APN + 300 - Example: Old APN: 40-030-004-M      			New PIN: 	040-030-004-300														040030004300
 *
 *  Mineral Valuations
 *  What used to be the APN + Q is now
 *
 *  0 + 11 digit APN + 400 - Example: Old APN: 40-100-048-Q     		 	New PIN: 	040-100-048-400
 *												                                                040100048400
 * Return old apn length if successful, 0 if failed
 *
 *******************************************************************************/

int Sut_FormatNewApn(char *pNewApn, char *pOldApn)
{
   int   iTmp, iRet=0;
   char  sApn[32];

#ifdef  _DEBUG
   //if (memcmp(pNewApn, "0080400220A0", 12))
   //   iTmp = 0;
#endif

   strcpy(sApn, pOldApn);
   iTmp = iTrim(sApn);
   if (iTmp == 8)
      sprintf(pNewApn, "0%s000", sApn);
   else if (iTmp == 9)
   {
      switch (sApn[8])
      {
         case 'T':
            sprintf(pNewApn, "7%.8s010", sApn);
            break;
         case 'Z':
            sprintf(pNewApn, "7%.8s020", sApn);
            break;
         case 'M':
            sprintf(pNewApn, "0%.8s300", sApn);
            break;
         case 'Q':
            sprintf(pNewApn, "0%.8s400", sApn);
            break;
         default:
            if  (isalpha(sApn[8]))
               sprintf(pNewApn, "0%.8s0%c0", sApn, sApn[8]);
            else
            {
               LogMsg("*** Unknown APN: %s (%d)", pOldApn, iTmp);
               iRet = 1;
            }
            break;
      }
   } else if (iTmp == 10)
   {
      switch (sApn[9])
      {
         case 'T':
            sprintf(pNewApn, "7%.8s010", sApn);
            break;
         case 'Z':
            sprintf(pNewApn, "7%.8s020", sApn);
            break;
         case 'M':
            sprintf(pNewApn, "0%.8s300", sApn);
            break;
         case 'Q':
            sprintf(pNewApn, "0%.8s400", sApn);
            break;
         default:
            if  (isalpha(sApn[9]))
               sprintf(pNewApn, "0%.8s0%c0", sApn, sApn[9]);
            else
            {
               LogMsg("*** Unknown APN: %s (%d)", pOldApn, iTmp);
               iRet = 1;
            }
            break;
      }
   } else
   {
      LogMsg("*** Unknown APN: %s (%d)", pOldApn, iTmp);
      iRet = 2;
   }

   return iRet;
}

int Sut_ReformatApn(char *pInfile, char *pOutfile, char *pFiletype, int iOldApnLen, int iHdrCnt)
{
   char     acInbuf[2048], acOutbuf[2048], acApn[16], *pRec;
   int      iRet, iCnt;

   FILE     *fdIn, *fdOut;

   LogMsg0("Reformatting APN.");
   LogMsg("Open %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening %s", pInfile);
      return -2;
   }

   LogMsg("Creating output file %s", pOutfile);
   if (!(fdOut = fopen(pOutfile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", pOutfile);
      return -2;
   }

   for (iCnt = 0; iCnt < iHdrCnt; iCnt++)
   {
      pRec = fgets(acInbuf, 2048, fdIn);
      fputs(acInbuf, fdOut);
   }

   while (!feof(fdIn))
   {
      pRec = fgets(acInbuf, 2048, fdIn);
      if (!pRec) break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "009619939", 9))
      //   iTokens = 0;
#endif

      if (*pFiletype == 'G')
      {
         memcpy(acApn, acInbuf, iOldApnLen);
         acApn[iOldApnLen] = 0;
         iRet = Sut_FormatNewApn(acOutbuf, acApn);
         if (!iRet)
            strcat(acOutbuf, &acInbuf[iOldApnLen]);
      } else if (*pFiletype == 'Z')
      {
         memcpy(acApn, acInbuf, iOldApnLen);
         acApn[iOldApnLen] = 0;
         iRet = Sut_FormatNewApn(acOutbuf, acApn);
         if (!iRet)
            strcat(acOutbuf, &acInbuf[iOldApnLen]);
      }

      if (!iRet)
         fputs(acOutbuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut)  fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   return iCnt;
}

/******************************************************************************
 *
 * Remove all known bad chars in string
 *
 ******************************************************************************/

int Sut_ReplBadChar(unsigned char *pBuf)
{
   int  iTmp, iRet=0;
   unsigned char *pTmp = pBuf;

   iTmp = 0;
   while (*pBuf && *pBuf != 10)  // scan till CR
   {
      if (*pBuf == 0xC2 || *pBuf == 0xE2)
      {
         iRet = iTmp;
      } else if (*pBuf == 0x80)
      {
         *pTmp++ = 0x27;
         iRet = iTmp;
      } else if (*pBuf == 0xB0)
      {
         *pTmp++ = '*';
         iRet = iTmp;
      } else
         *pTmp++ = *pBuf;

      pBuf++;
      iTmp++;
   }
   *pTmp = 0;

   return iRet;
}

/******************************* Sut_MergeChar *******************************
 *
 * Merge Sut_Char.dat in STDCHAR format
 *
 *****************************************************************************/

int Sut_MergeStdChar(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft, lTmp;
   int      iLoop, iBeds, iFBath, iHBath;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 2048, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare APN
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 2048, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Yrblt
   lTmp = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // Eff Yrblt
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Garage Sqft (may be carport or detached garage)
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];  
   }

   // Pools
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

#ifdef _DEBUG
   //if (!memcmp(pChar->Apn, "009600002", 9) )
   //   lTmp = 0;
#endif

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      
      *(pOutbuf+OFF_BATH_4Q) = pChar->Bath_4Q[0];
   }

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);

      *(pOutbuf+OFF_BATH_2Q) = pChar->Bath_2Q[0];
   }

   // 3/4 bath
   *(pOutbuf+OFF_BATH_3Q) = pChar->Bath_3Q[0];

   // Stories
   memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);

   // Central Heating-Cooling
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Fireplace
   if (pChar->Fireplace[0] == 'Y')
      memcpy(pOutbuf+OFF_FIRE_PL, " 1", 2);

   // Roof
   *(pOutbuf+OFF_ROOF_MAT) = pChar->RoofMat[0];

   // Water
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Sewer
   *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // Zoning
   //if (pChar->Zoning[0] > ' ')
   //   memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL) = pChar->BldgQual;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Improve condition
   *(pOutbuf+OFF_IMPR_COND) = pChar->ImprCond[0];

   // Acres
   lTmp = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      // Lot Sqft
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = atoin(pChar->LotAcre, SIZ_CHAR_SQFT);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Number of units
   lTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (lTmp > 1)
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 2048, fdChar);

   return 0;
}

/******************************* Sut_ExtrSaleCsv *****************************
 *
 * Extract sale file CA-Sutter-Sales.csv 05/27/2022
 * 05/20/2024 Fix APN
 *
 *****************************************************************************/

int Sut_ExtrSaleCsv()
{
   char      acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acTmp[256], acRec[2048], *pRec;
   long      lOut=0, lCnt=0, lPrice, iTmp;
   FILE      *fdOut;
   SCSAL_REC SaleRec;

   LogMsg0("Loading Sale file");

   // Check for Unicode
   //sprintf(acTmpFile, "%s\\%s\\Sales.txt", acTmpPath, myCounty.acCntyCode);
   //if (!UnicodeToAnsi(acSalesFile, acTmpFile))
   //   strcpy(acSalesFile, acTmpFile);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return 2;
   }

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acRec, 2048, fdSale);

   // Loop through record set
   while (!feof(fdSale))
   {  
      pRec = fgets(acRec, 2048, fdSale);
      if (!pRec)
         break;

      replStrAll(acRec, "NULL", "");
      lCnt++;

      // Parse input rec
      iTokens = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < S_COLS)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[S_CONVEYANCENUMBER] == ' ' || *apTokens[S_CONVEYANCEDATE] == ' ')
         continue;
      if (*apTokens[S_CONVEYANCENUMBER] > '9' || *apTokens[S_CONVEYANCENUMBER] < '2')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      iTmp = iTrim(apTokens[S_PIN]);
      if (iTmp < 12)
      {
         LogMsg("*** Bad sale APN: %s", apTokens[S_PIN]);
         continue;
      } else
         memcpy(SaleRec.Apn, apTokens[S_PIN], iTmp);

      // Doc date
      pRec = dateConversion(apTokens[S_CONVEYANCEDATE], acTmp, YYYY_MM_DD);
      if (pRec)
      {
         memcpy(SaleRec.DocDate, pRec, 8);
         iTmp = atol(acTmp);
         if (iTmp > lLastRecDate)
            lLastRecDate = iTmp;
      } else
      {
         LogMsg("*** Bad sale date: %s [%s]", apTokens[S_CONVEYANCEDATE], apTokens[S_PIN]);
         continue;
      }

      // Docnum - Drop 2017-ASRB-16753, 2018-ASUB-9954, 2022DOD06092022
      if (*(apTokens[S_CONVEYANCENUMBER]+4) == '-')
      {
         iTmp = iTrim(apTokens[S_CONVEYANCENUMBER]);

         // 2017-MH02476, 2017-MH02424-1
         if (!memcmp(apTokens[S_CONVEYANCENUMBER]+5, "MH", 2))
         {
            memcpy(SaleRec.DocNum, apTokens[S_CONVEYANCENUMBER]+5, iTmp-5);
         } else if (iTmp == 12)
         {
            if (isdigit(*(apTokens[S_CONVEYANCENUMBER]+5)))
            {
               // 2013-1320401   
               memcpy(SaleRec.DocNum, apTokens[S_CONVEYANCENUMBER]+5, 7);
            } else 
            {
               // 2018-MISC156, 2015-PM01216, 2013-SB01213, 2015-GW16/17, 2014-GW00335
               LogMsg("*** Bad docnum: %s [%s]", apTokens[S_CONVEYANCENUMBER], apTokens[S_PIN]);
               continue;
            }
         }
      } else
      {
         LogMsg("*** Bad docnum: %s [%s]", apTokens[S_CONVEYANCENUMBER], apTokens[S_PIN]);
         continue;
      }

      // Sale price
      lPrice = atol(apTokens[S_INDICATEDSALEPRICE]);
      if (!lPrice)
         lPrice = atol(apTokens[S_ADJUSTEDSALEPRICE]);

      if (lPrice > 0)
      {
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(SaleRec.SalePrice, acTmp, SIZ_SALE1_AMT);
      }

      // Transfer Type
      //int iIdx = XrefCodeIndex((XREFTBL *)&asDeed[0], apTokens[S_TRANSFERTYPECODE], iNumDeeds);
      if (!_memicmp(apTokens[S_TRANSFERTYPECODE], "DEED", 4))
      {
         if (lPrice > 1000)
            SaleRec.DocType[0] = '1';
         else
            memcpy(SaleRec.DocType, "13", 2);
      } else
         LogMsg("*** Unknown TransferTypeCode: '%s' APN=%s", apTokens[S_TRANSFERTYPECODE], apTokens[S_PIN]);

      // Group sale?

      // Seller

      // Buyer

      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);
      lOut++;

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   LogMsg("Total processed records: %u", lCnt);
   LogMsg("         output records: %u", lOut);
   LogMsg("    Last recording date: %u\n", lLastRecDate);

   // Update cumulative sale file
   if (lOut > 0)
   {
      // Append and resort SLS file
      // Sort on APN asc, DocDate asc, DocNum asc
      sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A) F(TXT) DUPO(1,34) ");
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");     
      if (!_access(acCSalFile, 0))
         sprintf(acRec, "%s+%s", acTmpFile, acCSalFile);
      else
         strcpy(acRec, acTmpFile);
      lCnt = sortFile(acRec, acOutFile, acTmp);
      if (lCnt > 0)
      {
         // Rename old cum sale
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         iTmp = 0;
         if (!_access(acTmpFile, 0))
            iTmp = remove(acTmpFile);
         if (!_access(acCSalFile, 0))
            iTmp = rename(acCSalFile, acTmpFile);
         if (!iTmp)
            iTmp = rename(acOutFile, acCSalFile);
         else
            LogMsg("***** Error removing %s.  Please rerun with -Xs option", acCSalFile);
      }
   } else
      iTmp = -1;

   LogMsg("Total Sale records output: %d.\n", lCnt);
   return iTmp;
}

/******************************* Sut_AddUnits ********************************
 *
 * Use Sut_Units.txt to populate number of units.  This file is generated by 
 * importing the PropertyCharacteristics.csv into SQL then select those APN
 * with multiple count.
 * select pin, count(*) as c from Sut_propchar group by pin
 * having count(*) > 1 order by pin
 *
 *****************************************************************************/

int Sut_AddUnits(char *pOutbuf, FILE **fdUnit)
{
   static   char acRec[512], *apItems[8], *pRec=NULL;
   static   int iItems=0;

   int      iLoop;
   STDCHAR  *pStdChar = (STDCHAR *)pOutbuf;

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 512, *fdUnit);
      iItems = ParseString(acRec, '|', 3, apItems);
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip rec  %s", pRec);
         pRec = fgets(acRec, 512, *fdUnit);
         if (!pRec)
         {
            fclose(*fdUnit);
            *fdUnit = NULL;
            return -1;      // EOF
         } else
            iItems = ParseString(acRec, '|', 3, apItems);
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutBase->Assmnt_No, "44257021", 8))
   //   iTmp = 0;
#endif
   if (iItems > 1)
      memcpy(pStdChar->Units, apItems[1], strlen(apItems[1]));
   
   return 0;
}

/***************************** Sut_ConvStdCharCsv ****************************
 *
 * Convert CA-Sutter-PropChar.csv to STD_CHAR format
 *
 *****************************************************************************/

int Sut_ConvStdCharCsv(char *pCharfile)
{
   char     acInbuf[1024], acOutbuf[2048], *pRec;
   char     acTmpFile[256], acTmp[256], cTmp1;
   double   dTmp;
   int      iRet, iTmp, iBeds, iFBath, iHBath, iBath3Q, iBath4Q, iCnt, lTmp, lSqft;

   STDCHAR  *pStdChar = (STDCHAR *)acOutbuf;
   FILE     *fdChar, *fdOut, *fdUnit;

   LogMsg0("Converting standard char file.");

   GetIniString(myCounty.acCntyCode, "UnitFile", "", acTmpFile, _MAX_PATH, acIniFile);
   fdUnit = (FILE *)NULL;
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Unit file %s", acTmpFile);
      if (!(fdUnit = fopen(acTmpFile, "r")))
         LogMsg("*** Error opening Unit file %s.  Ignore updating number of Units", acTmpFile);
   }

   // Check for Unicode
   //sprintf(acTmpFile, "%s\\%s\\PropChar.txt", acTmpPath, myCounty.acCntyCode);
   //if (!UnicodeToAnsi(pCharfile, acTmpFile))
   //   strcpy(pCharfile, acTmpFile);

   // Open CHAR file
   LogMsg("Open char file %s", pCharfile);
   if (!(fdChar = fopen(pCharfile, "r")))
   {
      LogMsg("***** Error opening input file %s", pCharfile);
      return -1;
   }

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdChar);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acInbuf, 1024, fdChar);

   iCnt = 0;
   while (!feof(fdChar))
   {
      pRec = fgets(acInbuf, 1024, fdChar);
      if (!pRec) break;

      if (*pRec > '9')
         continue;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "009619939", 9))
      //   iTokens = 0;
#endif

      // Remove all "N/A"
      replStrAll(acInbuf, "N/A", "");

      iTokens = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < C_COLS)
      {
         if (iTokens > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iTokens);
         else if (iTokens == 1)
            LogMsg("*** Please check delimiter for: %s [%d]", pRec, iCnt);

         continue;
      } else if (iTokens > C_COLS)
         LogMsg("*** Please check for possible corruption record: %s", pRec);

      // Ignore Unsecured Real Property & Possessory Interest
      if (!_memicmp(apTokens[C_CLASSCODEDESCRIPTION], "Unsecured", 8) ||
          !_memicmp(apTokens[C_CLASSCODEDESCRIPTION], "Possessory", 8))
         continue;

      memset(acOutbuf, ' ', sizeof(STDCHAR));

      iTmp = iTrim(apTokens[C_APN]);
      if (iTmp < 12 || iTmp > 13)
      {
         LogMsg("*** Verify char APN: %s Class=%s", apTokens[C_APN], apTokens[R_CLASSDESCRIPTION]);
         continue;
      } else
         memcpy(pStdChar->Apn, apTokens[C_APN], iTmp);

      // Format APN
      iRet = formatApn(apTokens[C_APN], acTmp, &myCounty);
      memcpy(pStdChar->Apn_D, acTmp, iRet);

      // Yrblt
      lTmp = atol(apTokens[C_YEARBUILT]);
      if (lTmp > 1700)
         memcpy(pStdChar->YrBlt, apTokens[C_YEARBUILT], SIZ_YR_BLT);
      lTmp = atol(apTokens[C_EFFYEARBUILT]);
      if (lTmp > 1900)
         memcpy(pStdChar->YrEff, apTokens[C_EFFYEARBUILT], SIZ_YR_BLT);

      // Stories
      iTmp = atol(apTokens[C_NUMBEROFSTORIES]);
      if (iTmp > 0 && iTmp < 100)
      {
         if (bDebug && iTmp > 9)
            LogMsg("*** Please verify number of stories for %s (%d)", apTokens[C_APN], iTmp);

         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(pStdChar->Stories, acTmp, iRet);
      }

      // Garage Sqft
      // Property may have more than one garage, but we only have space for one
      long lGarSqft = atol(apTokens[C_GARAGEAREA]) + atol(apTokens[C_GARAGE2AREA]);
      long lCarpSqft= atol(apTokens[C_CARPORTSIZE]) + atol(apTokens[C_CARPORT2SIZE]);

      // Garage type
      if (*apTokens[C_GARAGETYPE] == 'A' || *apTokens[C_GARAGE2TYPE] == 'A')
         pStdChar->ParkType[0] = 'I';              // Attached garage
      else if (*apTokens[C_GARAGETYPE] == 'D')
         pStdChar->ParkType[0] = 'L';              // Detached garage
      else if (lCarpSqft > 100)
         pStdChar->ParkType[0] = 'C';              // Carport

      if (lGarSqft > 100 || lCarpSqft > 100)
      {
         if (lGarSqft >= 100 && lCarpSqft > 100)
         {
            lGarSqft += lCarpSqft;
            pStdChar->ParkType[0] = '2';           // Garage/Carport
         }
         iTmp = sprintf(acTmp, "%d", lGarSqft);
         memcpy(pStdChar->GarSqft, acTmp, iTmp);
      }

      // Central Heating-Cooling
      if (*apTokens[C_HASCENTRALHEATING] == 'Y')
         pStdChar->Heating[0] = 'Z';
      if (*apTokens[C_HASCENTRALCOOLING] == 'Y')
         pStdChar->Cooling[0] = 'C';

      // Beds
      iBeds = atol(apTokens[C_BEDROOMCOUNT]);
      if (iBeds > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBeds);
         memcpy(pStdChar->Beds, acTmp, iTmp);
      }

      // Bath
      iBath4Q = atol(apTokens[C_BATHSFULL]);
      iHBath  = atol(apTokens[C_BATHSHALF]);
      iBath3Q = atol(apTokens[C_BATHS3_4]);
      iFBath = iBath3Q+iBath4Q;
      if (iBath3Q > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBath3Q);
         memcpy(pStdChar->Bath_3QX, acTmp, iTmp);
         if (iTmp < 3)
            memcpy(pStdChar->Bath_3Q, acTmp, iTmp);
      }

      if (iBath4Q > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBath4Q);
         memcpy(pStdChar->Bath_4QX, acTmp, iTmp);
         if (iTmp < 3)
            memcpy(pStdChar->Bath_4Q, acTmp, iTmp);
      }

      if (iFBath > 0 && iFBath < 100)
      {
         iTmp = sprintf(acTmp, "%d", iFBath);
         memcpy(pStdChar->FBaths, acTmp, iTmp);
      }

      if (iHBath > 0)
      {
         iTmp = sprintf(acTmp, "%d", iHBath);
         memcpy(pStdChar->Bath_2QX, acTmp, iTmp);
         if (iTmp < 3)
         {
            memcpy(pStdChar->HBaths, acTmp, iTmp);
            memcpy(pStdChar->Bath_2Q, acTmp, iTmp);
         }
      }

      // Fireplace Y/N
      if (*apTokens[C_HASFIREPLACE] > ' ')
         pStdChar->Fireplace[0] = *apTokens[C_HASFIREPLACE];

      // Roof
      if (*apTokens[C_ROOFTYPE] >= 'A')
      {
         _strupr(apTokens[C_ROOFTYPE]);
         pRec = findXlatCodeA(apTokens[C_ROOFTYPE], &asRoofType[0]);
         if (pRec)
            pStdChar->RoofType[0] = *pRec;
      }

      // Pool/Spa Y/N
      if (*apTokens[C_HASPOOL] == 'Y')
         pStdChar->Pool[0] = 'P';       // Pool

      // Electric
      if (*apTokens[C_UTILITYELECTRIC] >= 'A')
      {
         switch (*apTokens[C_UTILITYELECTRIC])
         {
            case 'A':
            case 'D':
               cTmp1 = *apTokens[C_UTILITYELECTRIC];  // AVAIL
               break;
            case 'N':
               cTmp1 = 'N';                           // NON AVAIL
               break;
            default:
               if (!_memicmp(apTokens[C_UTILITYELECTRIC], "Under", 5))
                  cTmp1 = 'D';
               else
                  cTmp1 = ' ';
         }
         pStdChar->HasElectric = cTmp1;
      }

      // Gas
      if (*apTokens[C_UTILITYGAS] >= 'A')
      {
         switch (*apTokens[C_UTILITYGAS])
         {
            case 'A':
            case 'D':
               cTmp1 = *apTokens[C_UTILITYGAS];    // AVAIL
               break;
            case 'N':
               cTmp1 = 'N';                        // NON AVAIL
               break;
            default:
               cTmp1 = ' ';
         }
         pStdChar->HasGas = cTmp1;
      }

      // Water - currently not avail. 5/27/2022
      if (*apTokens[C_DOMESTICWATER] == 'A' || *apTokens[C_DOMESTICWATER] == 'D')
      {
         pStdChar->Water = *apTokens[C_DOMESTICWATER];
         pStdChar->HasWater = 'Y';
      } else if (*apTokens[C_WELLWATER] == 'A' || *apTokens[C_WELLWATER] == 'D')
      {
         pStdChar->Water = *apTokens[C_WELLWATER];
         pStdChar->HasWater = 'W';
         pStdChar->HasWell = 'Y';
      } else if (*apTokens[C_IRRIGATIONWATER] == 'A' || *apTokens[C_IRRIGATIONWATER] == 'D')
      {
         pStdChar->HasWater = 'L';
         pStdChar->Water = *apTokens[C_IRRIGATIONWATER];
      } else if (*apTokens[C_DOMESTICWATER] == 'N' || *apTokens[C_WELLWATER] == 'N' || *apTokens[C_IRRIGATIONWATER] == 'N')
      {
         pStdChar->Water = 'N';
         pStdChar->HasWater = 'N';
      }

      // Sewer
      if (*apTokens[C_SEWER] > ' ')
      {
         pStdChar->Sewer = *apTokens[C_SEWER];
         if (*apTokens[C_SEWER] == 'A' || *apTokens[C_SEWER] == 'D')
            pStdChar->HasSewer = 'Y';
      }

      // View
      if (*apTokens[C_FAIRWAY] == 'Y')
         pStdChar->View[0] = 'H';   
      else if (*apTokens[C_WATERFRONT] == 'Y')
         pStdChar->View[0] = 'W';   

      // BldgSqft
      lSqft = atol(apTokens[C_LIVINGAREA]);
      if (!lSqft)
         lSqft = atol(apTokens[C_ACTUALAREA]);
      if (lSqft > 0)
      {
         iTmp = sprintf(acTmp, "%d", lSqft);
         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
      }

      // Construction type
      if (*apTokens[C_CONSTRUCTIONTYPE] >= 'A' && memcmp(apTokens[C_CONSTRUCTIONTYPE], "N/A", 3))
      {
         _strupr(apTokens[C_CONSTRUCTIONTYPE]);
         pRec = findXlatCodeA(apTokens[C_CONSTRUCTIONTYPE], &asConstType[0]);
         if (pRec)
            pStdChar->ConstType[0] = *pRec;

         // BldgClass 
         if (*apTokens[C_CONSTRUCTIONTYPE] == 'W')
            pStdChar->BldgClass = 'D';
         else if (*apTokens[C_CONSTRUCTIONTYPE] == 'M')
            pStdChar->BldgClass = 'C';
         else if (!memcmp(apTokens[C_CONSTRUCTIONTYPE], "PO", 2))
            pStdChar->BldgClass = 'P';
         else if (!memcmp(apTokens[C_CONSTRUCTIONTYPE], "PR", 2))
            pStdChar->BldgClass = 'S';

         if (strstr(apTokens[C_QUALITYCODE], "Ave"))
            pStdChar->ImprCond[0] = 'A';
         else if (strstr(apTokens[C_QUALITYCODE], "Fair"))
            pStdChar->ImprCond[0] = 'F';
         else if (strstr(apTokens[C_QUALITYCODE], "Good"))
            pStdChar->ImprCond[0] = 'G';
         else if (strstr(apTokens[C_QUALITYCODE], "Exe"))
            pStdChar->ImprCond[0] = 'E';
         else if (strstr(apTokens[C_QUALITYCODE], "Poor"))
            pStdChar->ImprCond[0] = 'P';

         // Quality
         if (!_memicmp(apTokens[C_QUALITYCODE], "Rank", 4))
            dTmp = atof(apTokens[C_QUALITYCODE]+5);
         else
            dTmp = atof(apTokens[C_QUALITYCODE]);
      }

#ifdef _DEBUG
      //if (!memcmp(pStdChar->Apn, "10043008", 8) )
      //   lTmp = 0;
#endif

      if (dTmp > 0.1)
      {
         char acCode[16];

         sprintf(acTmp, "%.1f", dTmp);
         iTmp = Value2Code(acTmp, acCode, NULL);
         if (acCode[0] > ' ')
            pStdChar->BldgQual = acCode[0];
      }

      // QualityClass
      if (pStdChar->BldgClass > ' ' && dTmp > 0.1)
      {
         sprintf(acTmp, "%c%.1f%c", pStdChar->BldgClass, dTmp, *apTokens[C_SHAPECODE]);
         vmemcpy(pStdChar->QualityClass, acTmp, SIZ_CHAR_QCLS, strlen(acTmp));
      }

      // Lot sqft - Lot Acres
      dTmp = atof(apTokens[C_ACREAGE]);
      if (dTmp > 0.001)
      {
         iTmp = sprintf(acTmp, "%d", (long)(dTmp*1000));
         memcpy(pStdChar->LotAcre, acTmp, iTmp);
         lSqft = long(dTmp*SQFT_PER_ACRE);

         iTmp = sprintf(acTmp, "%d", lSqft);
         memcpy(pStdChar->LotSqft, acTmp, iTmp);
      }

      // Add #Units
      if (fdUnit)
      {
         Sut_AddUnits(acOutbuf, &fdUnit);
      }

      pStdChar->CRLF[0] = '\n';
      pStdChar->CRLF[1] = '\0';
      fputs(acOutbuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdChar) fclose(fdChar);
   if (fdOut)  fclose(fdOut);
   if (fdUnit) fclose(fdUnit);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      // Rename old CHAR file if needed
      if (!_access(acCChrFile, 0))
      {
         strcpy(acInbuf, acCChrFile);
         replStr(acInbuf, ".dat", ".sav");
         if (!_access(acInbuf, 0))
         {
            LogMsg("Delete old %s", acInbuf);
            DeleteFile(acInbuf);
         }
         LogMsg("Rename %s to %s", acCChrFile, acInbuf);
         RenameToExt(acCChrFile, "sav");
      }

      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Sort on APN, YrBlt to keep the latest record on top
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,14,C,A,53,4,C,D) F(TXT) ");
   } else
   {
      printf("\n");
      iRet = 0;
   }

   return iRet;
}

/******************************** Sut_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sut_MergeOwner2021(char *pOutbuf, char *pOwner, char *pOwnerX)
{
   int   iTmp;
   char  acTmp[256], acName1[256], *pTmp;
   bool  bAppend=false;

   OWNER    myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   // Check CareOf

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1670301800", 10))
   //   iTmp = 0;
#endif

   // Save name1
   _strupr(pOwner);
   _strupr(pOwnerX);
   if (*pOwnerX > ' ')
   {
      if (strstr(pOwnerX, " ETUX ") || strstr(pOwnerX, " JR ") || 
         strstr(pOwnerX, " III ") || strchr(pOwnerX, '/') )
      {
         vmemcpy(pOutbuf+OFF_NAME_SWAP, pOwner, SIZ_NAME_SWAP);
         vmemcpy(pOutbuf+OFF_NAME1, pOwner, SIZ_NAME1);
         return;
      } else
         strcpy(acName1, pOwnerX);
   } else
      strcpy(acName1, pOwner);
   
   iTmp = iTrim(acName1);
   if (acName1[iTmp-1] == '/')
   {
      if (acName1[iTmp-2] < 'A')
         acName1[iTmp-2] = 0;
      else
         acName1[iTmp-1] = 0;
   }

   // Update Owner1
   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);

   // Remove number in name1
   // FREY LUKE 14.29% 
   pTmp = acName1;
   while (*pTmp)
   {
      // Break where name has numeric value
      if (isdigit(*pTmp))
         break;

      if (*pTmp == '.' || *pTmp == '(' || *pTmp == ')')
         *pTmp = ' ';
      pTmp++;
   }
   *pTmp = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Check for MD, DDS, and DVM
   if (pTmp=strstr(acName1, " MD "))
      *pTmp = 0;
   else if ((pTmp=strstr(acName1, " DDS ")) || (pTmp=strstr(acName1, " DVA ")) || (pTmp=strstr(acName1, " DVM ")) )
      *pTmp = 0;

   // Save Name1
   strcpy(acTmp, acName1);

   // Terminate name
   iTmp = strlen(acTmp);
   if (acTmp[iTmp-1] == '-')
      acTmp[iTmp-1] = 0;

   if ((pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, "CO-TTEE")) || (pTmp=strstr(acTmp, " TTEE"))     ||
       (pTmp=strstr(acTmp, "TTEES"))   || (pTmp=strstr(acTmp, " TRUSTEE")) )
      *pTmp = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00108234", 8) )
   //   iTmp = 0;
#endif

   if ((pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " LIFE EST")) ||
       (pTmp=strstr(acTmp, " ESTATE OF")) )
      *pTmp = 0;

   // Now parse owners
   iTmp = splitOwner(acTmp, &myOwner, 3);

   if (iTmp >= 0)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP);
}

/******************************** Sut_MergeOwner *****************************
 *
 * Only about 275 parcels has primary owner.  Use recipient when owner is blank.
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sut_MergeOwner(char *pOutbuf, char *pPriOwner, char *pRecipient)
{
   int   iTmp;
   char  acTmp[256], acName1[256], *pTmp;
   bool  bAppend=false;

   OWNER    myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   // Check CareOf

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "052141037000", 10))
   //   iTmp = 0;
#endif

   // Save name1
   _strupr(pPriOwner);
   _strupr(pRecipient);
   if (*pPriOwner > ' ')
      strcpy(acName1, pPriOwner);
   else
      strcpy(acName1, pRecipient);
   
   // Remove EST OF
   iTmp = replStr(acName1, "(EST OF)", "");
   if (!iTmp)
      iTmp = replStr(acName1, "(EST)", "");

   // Remove ""
   replStrAll(acName1, "\"\"", "");
   if (pTmp = strrchr(myTrim(acName1), ' '))
   {
      if (!strcmp(pTmp, " ETAL") || !strcmp(pTmp, " ETA") || !strcmp(pTmp, " ET") )
      {
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
         memcpy(pOutbuf+OFF_VEST, "EA", 2);
         if (*(pTmp-1) == '/')
            pTmp--;
         *pTmp = 0;
      }
   }

   if (acName1[2] == 0x3F) acName1[2] = ' ';
   if ((unsigned char)acName1[3] == 0x9B) acName1[3] = ' ';      

   iTmp = blankRem(acName1);
   if (acName1[iTmp-1] == '/')
      acName1[iTmp-1] = 0;

   // Copy from MergeOwner() in LoadRoll()
   char *pName1;
   pName1 = strcpy(acTmp, acName1);
   if (pTmp = strchr(acTmp, '/'))
   {
      *pTmp++ = 0;
      if (memcmp(pTmp, " ETAL", 5) && *pTmp >= 'A')
      {
         sprintf(acName1, "%s & %s", acTmp, pTmp);
         pName1 = acName1;
      } else if (strstr(pTmp, " ETAL"))
      {
         strcpy(pTmp-1, pTmp);
         pName1 = acTmp;
      }
   }
   vmemcpy(pOutbuf+OFF_NAME1, pName1, SIZ_NAME1);

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, pName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
   if (!iTmp)
   {
      if (pTmp = strstr(pName1, "1/2"))
         *pTmp = 0;
      iTmp = splitOwner(pName1, &myOwner, 3);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, pName1, SIZ_NAME_SWAP);

/*
   // Update Owner1
   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);

   // Remove number in name1
   // FREY LUKE 14.29% 
   pTmp = acName1;
   while (*pTmp)
   {
      // Break where name has numeric value
      if (isdigit(*pTmp))
         break;

      if (*pTmp == '.' || *pTmp == '(' || *pTmp == ')')
         *pTmp = ' ';
      pTmp++;
   }
   *pTmp = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Save Name1
   strcpy(acTmp, acName1);

   // Terminate name
   iTmp = strlen(acTmp);
   if (acTmp[iTmp-1] == '-')
      acTmp[iTmp-1] = 0;

   if ((pTmp=strstr(acTmp, " ETAL")) )
      replStr(acTmp, " ETAL", " ");

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00108234", 8) )
   //   iTmp = 0;
#endif

   // Now parse owners
   iTmp = splitOwner(acTmp, &myOwner, 3);

   if (iTmp >= 0)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP);
*/
}

/********************************* Sut_MergeSAdr *****************************
 *
 * 08/13/2021 New layout
 *
 *****************************************************************************/

void Sut_MergeSAdr(char *pOutbuf, char *pSAddr1, char *pCity, char *pZip)
{
   char     *pTmp, *pAddr1, acTmp[256], acCode[16], acAddr1[64], acAddr2[64];
   int      iTmp;
   ADR_REC  sSitusAdr;

   // Clear old Mailing
   removeSitus(pOutbuf);

   // Check for blank address
   if (!memcmp(pSAddr1, "     ", 5))
      return;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "065010025000", 9))
   //   iTmp = 0;
#endif

   if (!memcmp(pSAddr1, "0-", 2))
      strcpy(acAddr1, pSAddr1+2);
   else
      strcpy(acAddr1, pSAddr1);
   blankRem(_strupr(acAddr1));
   pAddr1 = acAddr1;

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1_5(&sSitusAdr, acAddr1);

   // Situs
   if (sSitusAdr.lStrNum > 0 && sSitusAdr.strName[0] >= ' ')
   {
      int   iIdx=0;

      removeSitus(pOutbuf);
      vmemcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, SIZ_S_STRNUM);
      vmemcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.HseNo, SIZ_S_HSENO);
      vmemcpy(pOutbuf+OFF_S_STR_SUB, sSitusAdr.strSub, SIZ_S_STR_SUB);
      strcpy(acTmp, sSitusAdr.strName);
      if (pTmp = strchr(acTmp, '*'))
         *pTmp = ' ';

      // Prepare display field
      vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

      if (sSitusAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, SIZ_S_STREET);
      if (sSitusAdr.strDir[0] > ' ' && isDir(sSitusAdr.strDir))
         vmemcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, SIZ_S_DIR);
      if (sSitusAdr.SfxCode[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, SIZ_S_SUFF);
      if (sSitusAdr.Unit[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
      
      // Situs city
      acCode[0] = acAddr2[0] = 0;
      if (*pCity >= 'A')
      {
         strcpy(acAddr2, pCity);
         City2CodeEx(acAddr2, acCode, sSitusAdr.City, pOutbuf);
         if (acCode[0] > ' ')
         {
            strcpy(acAddr2, sSitusAdr.City);
            memcpy(pOutbuf+OFF_S_CITY, acCode, 3);
            // Turn ON this 2 lines when zipcode is in Iny_City.N2CX
            //if (City2Zip(acAddr2, acTmp))
            //   memcpy(pOutbuf+OFF_S_ZIP, acTmp, 5);
         }
      }

      if (*pZip > ' ')
         vmemcpy(pOutbuf+OFF_S_ZIP, pZip, SIZ_S_ZIP);

      memcpy(pOutbuf+OFF_S_ST, "CA", 2);     
      if (sSitusAdr.City[0] > ' ')
      {
         sprintf(acAddr2, "%s CA %s", sSitusAdr.City, pZip);
         iTmp = blankRem(acAddr2);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
      }
   }
}

/********************************* Sut_MergeMAdr *****************************
 *
 * 08/13/2021 New layout
 *
 *****************************************************************************/

void Sut_MergeMAdr(char *pOutbuf, char *pMAddr1, char *pMAddr2)
{
   char     *pTmp, *pAddr1, acTmp[256], acAddr1[256], acAddr2[256];
   int      iTmp;

   // Clear old Mailing
   removeMailing(pOutbuf);

   // Check for blank address
   if (!memcmp(pMAddr1, "     ", 5))
      return;

   strcpy(acAddr1, pMAddr1);
   strcpy(acAddr2, pMAddr2);
   blankRem(acAddr1);
   blankRem(acAddr2);
   pAddr1 = acAddr1;

   iTmp = 0;
   if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
   {
      if (*pAddr1 == '%')
         pAddr1 += 2;
      else
         pAddr1 += 4;

      // Check for C/O name
      pTmp = strchr(pAddr1, ',');
      if (pTmp && !isdigit(*(pTmp-1)) && *(pOutbuf+OFF_CARE_OF) == ' ')
      {
         *pTmp = 0;
         pAddr1 = pTmp + 1;
         if (*pAddr1 == ' ')
            pAddr1++;
         
         updateCareOf(pOutbuf, pAddr1, strlen(pAddr1));
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0263400200", 10))
   //   iTmp = 0;
#endif
   // Start processing
   vmemcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, SIZ_M_ADDR_D);

   // Parsing mail address
   ADR_REC sMailAdr;
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sMailAdr, pAddr1);

   if (sMailAdr.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);

      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }

      if (sMailAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);
   parseAdr2(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] >= 'A')
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
   if (sMailAdr.State[0] >= 'A' && sMailAdr.State[1] >= 'A')
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
   if (sMailAdr.Zip[0] >= '0')
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
}


/********************************* Sut_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Sut_ExtrLien()
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   long     lCnt, lRead;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Merge loop
   lCnt=lRead=0;
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new lien record
      if (acRollRec[0] != 'R')
      {      
         //Sut_CreateLienRec(acBuf, acRollRec);
         fputs(acBuf, fdLien);
         lCnt++;
      }
      if (!(++lRead % 1000))
         printf("\r%u", lRead);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total records processed:    %d", lRead);
   LogMsg("         records output:    %d\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/*************************** Sut_ExtrRollCorrection *************************
 *
 * Extract values from roll correction file and replace the Lien_Exp.SUT
 *
 ****************************************************************************/

int Sut_CreateRollCorrectionRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp;
   int      iRet, iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Parse input string
   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < SUT_COR_FLDS)
   {
      LogMsg("***** Sut_CreateRollCorrectionRec: bad input record for APN=%s", apTokens[0]);
      return -1;
   }

   // Start copying data
   vmemcpy(pLienRec->acApn, apTokens[SUT_COR_AIN], iApnLen);

   // TRA
   lTmp = atol(apTokens[SUT_COR_TAG]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   vmemcpy(pLienRec->acYear, apTokens[SUT_COR_TAXYEAR], 4);

   // Land
   long lLand = dollar2Num(apTokens[SUT_COR_ASSDLAND]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[SUT_COR_ASSDIMP]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   long lPers = dollar2Num(apTokens[SUT_COR_ASSDPERSONAL]);
   long lFixt = dollar2Num(apTokens[SUT_COR_ASSDFIXTURES]);
   long lLivImpr = dollar2Num(apTokens[SUT_COR_ASSDLIVIMP]);
   long lOther= lPers + lFixt + lLivImpr;
   if (lOther > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lOther);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lLivImpr > 0)
      {
         iTmp = sprintf(acTmp, "%u", sizeof(pLienRec->extra.Sut.LivingImpr), lLivImpr);
         memcpy(pLienRec->extra.Sut.LivingImpr, acTmp, iTmp);
      }
   }

   // Gross total
   long lGross = dollar2Num(apTokens[SUT_COR_ASSESSEDFULL]);
   if (lGross > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lGross);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exempt
   long lExe = dollar2Num(apTokens[SUT_COR_HOX]);
   if (lExe > 0)
   {
      pLienRec->acHO[0] = '1';         // Y
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sut.HO_Exe), lExe);
      memcpy(pLienRec->extra.Sut.HO_Exe, acTmp, iTmp);
   } else
      pLienRec->acHO[0] = '2';         // N

   lTmp = dollar2Num(apTokens[SUT_COR_DVX]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sut.DV_Exe), lTmp);
      memcpy(pLienRec->extra.Sut.DV_Exe, acTmp, iTmp);
   }

   lTmp = dollar2Num(apTokens[SUT_COR_LDVX]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sut.LDV_Exe), lTmp);
      memcpy(pLienRec->extra.Sut.LDV_Exe, acTmp, iTmp);
   }

   lTmp = dollar2Num(apTokens[SUT_COR_OTHEREXMPT]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sut.Other_Exe), lTmp);
      memcpy(pLienRec->extra.Sut.Other_Exe, acTmp, iTmp);
   }

   lExe = dollar2Num(apTokens[SUT_COR_TOTALEXMPT]);
   if (lExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   // Full exemption
   if (lExe > 0 && lExe >= lGross)
      pLienRec->SpclFlag = LX_FULLEXE_FLG;

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Sut_ExtrRollCorrection()
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   long     lCnt, lRead;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Drop header
   pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Merge loop
   lCnt=lRead=0;
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new lien record
      if (memcmp(acRollRec, "0000", 4) > 0)
      {
         Sut_CreateRollCorrectionRec(acBuf, acRollRec);
         fputs(acBuf, fdLien);
         lCnt++;
      }
      if (!(lCnt % 1000))
         printf("\r%u", lRead);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total records output:    %d\n", lCnt);

   return 0;
}

/***************************** Sut_ParseTaxDetail ****************************
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Sut_ParseTaxDetail(FILE *fdDetail, FILE *fdAgency)
{
   char     acTmp[512], acOutbuf[512];
   int      iIdx;
   long     lTmp;
   TAXDETAIL *pDetail = (TAXDETAIL *)acOutbuf;
   TAXAGENCY *pResult, sAgency, *pAgency;

   // Clear output buffer
   memset(acOutbuf, 0, sizeof(TAXDETAIL));
   memset(&sAgency, 0, sizeof(TAXAGENCY));
   pAgency = &sAgency;

   // APN
   strcpy(pDetail->Apn, apTokens[SUT_SEC_APN]);

   // BillNumber
   strcpy(pDetail->BillNum, apTokens[SUT_SEC_BILL_NUM]);

   // Tax Year
   sprintf(pDetail->TaxYear, "%d", lTaxYear);

   iIdx = SUT_SEC_AS_CODE_1;
   while (*apTokens[iIdx] > ' ')
   {
      memset(&sAgency, 0, sizeof(TAXAGENCY));

      // Tax code
      strcpy(pDetail->TaxCode, apTokens[iIdx]);
      strcpy(pAgency->Code, apTokens[iIdx]);

      memset(pDetail->TaxRate, 0, sizeof(pDetail->TaxRate));
      pResult = findTaxAgency(pAgency->Code, 0);
      if (pResult)
      {
         strcpy(pAgency->Agency, pResult->Agency);
         strcpy(pAgency->Phone, pResult->Phone);
         pAgency->TC_Flag[0] = pResult->TC_Flag[0];
         pDetail->TC_Flag[0] = pResult->TC_Flag[0];

         // Tax Rate - Make sure we have latest tax rate table for the tax year
         if (pResult->TaxRate[0] > ' ')
         {
            strcpy(pAgency->TaxRate, pResult->TaxRate);
            strcpy(pDetail->TaxRate, pResult->TaxRate);
         }
      } else
      {
         pAgency->Agency[0] = 0;
         LogMsg("+++ Unknown TaxCode: %s APN=%s", pDetail->TaxCode, pDetail->Apn);
      }

      // Tax amt
      lTmp = atol(apTokens[iIdx+1]);
      lTmp += atol(apTokens[iIdx+2]);

      if (lTmp > 0)
         sprintf(pDetail->TaxAmt, "%.2f", (double)lTmp/100.0);
      else
         strcpy(pDetail->TaxAmt, "0");

      // Generate csv line and write to file
      Tax_CreateDetailCsv(acTmp, pDetail);
      fputs(acTmp, fdDetail);

      // Forming Agency record
      Tax_CreateAgencyCsv(acTmp, pAgency);
      fputs(acTmp, fdAgency);

      iIdx += 3;
   }

   return 0;
}

/***************************** Sut_ParseTaxBase ******************************
 *
 *
 *****************************************************************************/

//int Sut_ParseCurrTaxBase(char *pOutbuf, char *pInbuf)
//{
//   char     acTmp[256], *pTmp;
//   int      iTmp;
//   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;
//
//   // Parse input tax data
//   iTmp = ParseStringIQ(pInbuf, '|', MAX_FLD_TOKEN, apTokens);
//   if (iTmp < SUT_SEC_BRUP)
//   {
//      LogMsg("***** Error: bad TaxBase record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
//      return -1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, 0, sizeof(TAXBASE));
//
//   // If canceled, ignore it
//   if (*apTokens[SUT_SEC_I1_STAT] == 'C' || *apTokens[SUT_SEC_I2_STAT] == 'C')
//   {
//      if (!memcmp(apTokens[SUT_SEC_PAYOR], "CANCELLED", 6))
//         LogMsg("+++ Keep %s reason %s", apTokens[SUT_SEC_APN], apTokens[SUT_SEC_PAYOR]);
//      else if (*(apTokens[SUT_SEC_APN]+9) != 'N')
//         return 1;
//   }
//
//   if (*(apTokens[SUT_SEC_APN]+9) == 'N')
//   {
//      LogMsg("<--- Rename record %s = %.9s0", apTokens[SUT_SEC_APN], apTokens[SUT_SEC_APN]); 
//      *(apTokens[SUT_SEC_APN]+9) = '0';
//   }
//
//   // APN
//   strcpy(pOutRec->Apn, apTokens[SUT_SEC_APN]);
//   strcpy(pOutRec->BillNum, myBTrim(apTokens[SUT_SEC_BILL_NUM]));
//
//   // Bill Type
//   pOutRec->BillType[0] = BILLTYPE_SECURED;
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutRec->Apn, "006232003000", 10))
//   //   iTmp = 0;
//#endif
//
//   // Installment Status
//   if (*apTokens[SUT_SEC_I1_STAT] == 'P')
//      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
//   else if (*apTokens[SUT_SEC_I1_STAT] == 'C')
//      pOutRec->Inst1Status[0] = TAX_BSTAT_CANCEL;
//   else if (*apTokens[SUT_SEC_I1_STAT] == 'N')
//      pOutRec->Inst1Status[0] = TAX_BSTAT_NOTAX;
//   else
//      pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
//
//   if (*apTokens[SUT_SEC_I2_STAT] == 'P')
//      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
//   else if (*apTokens[SUT_SEC_I1_STAT] == 'C')
//      pOutRec->Inst2Status[0] = TAX_BSTAT_CANCEL;
//   else if (*apTokens[SUT_SEC_I1_STAT] == 'N')
//      pOutRec->Inst2Status[0] = TAX_BSTAT_NOTAX;
//   else
//      pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
//
//   // TRA
//   strcpy(pOutRec->TRA, apTokens[SUT_SEC_TRA]);
//
//   // Tax Year
//   iTmp = sprintf(pOutRec->TaxYear, "%d", lTaxYear);
//
//   // Default date - Use data from Delq file instead
//   //pOutRec->isDelq[0] = '0';
//   //if (*apTokens[SUT_SEC_STS_DATE] > ' ' )
//   //{
//   //   apTokens[SUT_SEC_STS_DATE][8] = 0;
//   //   pTmp = dateConversion(apTokens[SUT_SEC_STS_DATE], pOutRec->Def_Date, MMDDYYYY);
//   //   if (pTmp)
//   //   {
//   //      memcpy(pOutRec->DelqYear, pOutRec->Def_Date, 4);
//   //      if (!memcmp(pOutRec->DelqYear, pOutRec->TaxYear, 4))
//   //         pOutRec->isDelq[0] = '1';
//   //   }
//   //}
//
//   // Penalty
//   double dPen1 = atof(apTokens[SUT_SEC_I1_PEN]);
//   double dPen2 = atof(apTokens[SUT_SEC_I2_PEN]);
//   double dTotalDue = 0;
//
//   // Check for Tax amount
//   double dTax1 = atof(apTokens[SUT_SEC_I1_TAX]);
//   if (dTax1 > 0.0)
//      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1/100.0);
//
//   double dTax2 = atof(apTokens[SUT_SEC_I2_TAX]);
//   if (dTax2 > 0.0)
//      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2/100.0);
//
//   double dTaxTotal = atof(apTokens[SUT_SEC_NET_TAX]);
//   if (dTaxTotal > 0.0)
//   {
//      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal);
//
//      // Paid Date
//      if (*apTokens[SUT_SEC_I1_PAIDDATE] >= '0' && dateConversion(apTokens[SUT_SEC_I1_PAIDDATE], acTmp, MMDDYYYY))
//      {
//         strcpy(pOutRec->PaidDate1, acTmp);
//         dPen1 = 0;
//      }
//      if (*apTokens[SUT_SEC_I2_PAIDDATE] >= '0' && dateConversion(apTokens[SUT_SEC_I2_PAIDDATE], acTmp, MMDDYYYY))
//      {
//         strcpy(pOutRec->PaidDate2, acTmp);
//         dPen2 = 0;
//      }
//
//      // Due date
//      if (*apTokens[SUT_SEC_I1_DUEDATE] >= '0' && dateConversion(apTokens[SUT_SEC_I1_DUEDATE], acTmp, MMDDYYYY))
//         strcpy(pOutRec->DueDate1, acTmp);
//      if (*apTokens[SUT_SEC_I2_DUEDATE] >= '0' && dateConversion(apTokens[SUT_SEC_I2_DUEDATE], acTmp, MMDDYYYY, lToyear+1))
//         strcpy(pOutRec->DueDate2, acTmp);
//
//      // If past due date, add penalty to due amt
//      if (dPen1 > 0.0)
//      {
//         if (ChkDueDate(1, pOutRec->DueDate1))
//         {
//            dTotalDue = dTax1+dPen1+dTax2;
//            sprintf(pOutRec->PenAmt1, "%.2f", dPen1/100.0);
//         } else
//            dTotalDue = dTax1;
//      }
//      if (dPen2 > 0.0  && ChkDueDate(2, pOutRec->DueDate2))
//      {
//         dTotalDue += dPen2;
//         sprintf(pOutRec->PenAmt2, "%.2f", dPen2/100.0);
//      }
//   }
//
//   if (dTotalDue > 0.0)
//      sprintf(pOutRec->TotalDue, "%.2f", dTotalDue/100.0);
//
//   // Tax rate
//   strcpy(pOutRec->TotalRate, apTokens[SUT_SEC_TOT_RATE]);
//
//   // Owner Info
//   myTrim(apTokens[SUT_SEC_NAME2]);
//   if ((pTmp = strstr(apTokens[SUT_SEC_NAME1], " /")) || (pTmp = strstr(apTokens[SUT_SEC_NAME1], "/ ")))
//   {
//      *pTmp = 0;
//      strcpy(pOutRec->OwnerInfo.Name1, apTokens[SUT_SEC_NAME1]);
//      strcpy(pOutRec->OwnerInfo.Name2, apTokens[SUT_SEC_NAME2]);
//   } else if (pTmp = strstr(apTokens[SUT_SEC_NAME1], "1/2/"))
//   {
//      *(pTmp+3) = 0;
//      strcpy(pOutRec->OwnerInfo.Name1, apTokens[SUT_SEC_NAME1]);
//      strcpy(pOutRec->OwnerInfo.Name2, apTokens[SUT_SEC_NAME2]);
//   } else if (*apTokens[SUT_SEC_NAME2] > ' ' && (pTmp = strstr(apTokens[SUT_SEC_NAME1], " TTEE")))
//   {
//      iTmp = strlen(apTokens[SUT_SEC_NAME2]);
//      if (iTmp > 15)
//      {
//         strcpy(pOutRec->OwnerInfo.Name1, myTrim(apTokens[SUT_SEC_NAME1]));
//         strcpy(pOutRec->OwnerInfo.Name2, apTokens[SUT_SEC_NAME2]);
//      } else
//      {
//         strcpy(pOutRec->OwnerInfo.Name1, apTokens[SUT_SEC_NAME1]);
//         strcat(pOutRec->OwnerInfo.Name1, apTokens[SUT_SEC_NAME2]);
//      }
//   } else if (*apTokens[SUT_SEC_NAME2] > ' ')
//   {
//      sprintf(acTmp, "%s%s", apTokens[SUT_SEC_NAME1], apTokens[SUT_SEC_NAME2]);
//      iTmp = blankRem(acTmp);
//      if (iTmp >= TAX_NAME)
//      {
//         strcpy(pOutRec->OwnerInfo.Name1, apTokens[SUT_SEC_NAME1]);
//         strcpy(pOutRec->OwnerInfo.Name2, apTokens[SUT_SEC_NAME2]);
//      } else
//         strcpy(pOutRec->OwnerInfo.Name1, acTmp);
//   } else
//      strcpy(pOutRec->OwnerInfo.Name1, myTrim(apTokens[SUT_SEC_NAME1]));
//
//   // CareOf
//   if (*apTokens[SUT_SEC_CAREOF] > ' ')
//      strcpy(pOutRec->OwnerInfo.CareOf, apTokens[SUT_SEC_CAREOF]);
//
//   // DBA
//   if (*apTokens[SUT_SEC_DBA] > ' ')
//      strcpy(pOutRec->OwnerInfo.Dba, apTokens[SUT_SEC_DBA]);
//
//   // Mailing address
//   if (*apTokens[SUT_SEC_M_ADDR] > ' ' && memcmp(apTokens[SUT_SEC_M_ADDR], "UNK", 3))
//   {
//      strcpy(pOutRec->OwnerInfo.MailAdr[0], myTrim(apTokens[SUT_SEC_M_ADDR]));
//      sprintf(acTmp, "%s %s", apTokens[SUT_SEC_M_CITYST], apTokens[SUT_SEC_M_ZIP]);
//      iTmp = blankRem(acTmp);
//      strcpy(pOutRec->OwnerInfo.MailAdr[1], acTmp);
//   }
//
//   pOutRec->isSecd[0] = '1';
//   pOutRec->isSupp[0] = '0';
//
//   return 0;
//}

/****************************** Sut_MergeTaxDelq *****************************
 *
 * Copy DelqYear from delq record to populate Tax_Base
 *
 *****************************************************************************/

int Sut_MergeTaxDelq(char *pOutbuf)
{
   static   char  acRec[2048], *pRec=NULL;
   char     *apItems[MAX_FLD_TOKEN], acTmp[32];
   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   int      iLoop, iTmp;

   // Get first Sale rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 2048, fdDelq);
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pTaxBase->Apn, (pRec+OFF_DELQ_PRCL), iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip delq rec  %.*s", iApnLen, pRec+OFF_DELQ_PRCL);
         pRec = fgets(acRec, 2048, fdDelq);
         if (!pRec)
         {
            fclose(fdDelq);
            fdDelq = NULL;
            return -1;      // EOF
         }

         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Parse input tax data
   iTmp = ParseStringIQ(acRec, '|', MAX_FLD_TOKEN, apItems);
   if (iTmp < SUT_DELQ_REM)
   {
      LogMsg("***** Error: bad TaxDelq record for: %.50s (#tokens=%d)", acRec, iTmp);
      return -1;
   }

   // Default date
   if (dateConversion(apItems[SUT_DELQ_TAX_DEF_DATE], acTmp, MMDDYYYY))
   {
      strcpy(pTaxBase->Def_Date, acTmp);
      memcpy(pTaxBase->DelqYear, acTmp, 4);

      if (*apItems[SUT_DELQ_RDM_STAT] != 'P')
         pTaxBase->isDelq[0] = '1';
   }

   lSaleMatch++;

   return 0;
}

/**************************** Sut_Load_TaxBase *******************************
 *
 * Create import file for Tax_Base, Tax_Items, Tax_Agency & Tax_Owner tables
 *
 * Return 0 if success.
 *
 *****************************************************************************/

//int Sut_Load_CurrTaxBase(bool bImport)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
//   char     acAgencyFile[_MAX_PATH], acOwnerFile[_MAX_PATH], acBaseFile[_MAX_PATH], 
//            acItemsFile[_MAX_PATH], acDelqFile[_MAX_PATH], acSecRollFile[_MAX_PATH];
//
//   int      iRet, iLen;
//   long     lOut=0, lCnt=0, lTmp;
//   FILE     *fdAgency, *fdOwner, *fdBase, *fdDetail, *fdSecRoll;
//   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];
//
//   LogMsg("Loading Current tax file");
//
//   GetIniString(myCounty.acCntyCode, "CurrTax", "", acSecRollFile, _MAX_PATH, acIniFile);
//   lLastTaxFileDate = getFileDate(acSecRollFile);
//   // Only process if new tax file
//   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
//   if (iRet <= 0)
//   {
//      lLastTaxFileDate = 0;
//      return iRet;
//   }
//
//   iLen = GetPrivateProfileInt(myCounty.acCntyCode, "CurrTaxLen", 0, acIniFile);
//   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
//   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
//   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
//   NameTaxCsvFile(acOwnerFile, myCounty.acCntyCode, "Owner");
//
//   pTmp = strrchr(acBaseFile, '\\');
//   *pTmp = 0;
//   if (_access(acBaseFile, 0))
//      _mkdir(acBaseFile);
//   *pTmp = '\\';
//
//   // Convert EBCDIC to ASCII before use
//   if (iLen > 0)
//   {                    
//      strcpy(acBuf, acSecRollFile);
//      strcat(acSecRollFile, ".ASC");
//      iRet = getFileDate(acSecRollFile);
//      if (iRet <= lLastTaxFileDate)
//      {
//         LogMsg("Convert EBC2ASC: %s --> %s", acBuf, acSecRollFile);
//         iRet = doEBC2ASCAddCR(acBuf, acSecRollFile, iLen);
//         if (iRet)
//         {
//            LogMsg("***** Error converting EBC2ASC from %s to %s", acBuf, acSecRollFile);
//            return -1;
//         }
//      }
//   }
//
//   GetIniString(myCounty.acCntyCode, "DelqTax", "", acDelqFile, _MAX_PATH, acIniFile);
//   iLen = GetPrivateProfileInt(myCounty.acCntyCode, "DelqTaxLen", 0, acIniFile);
//   lTmp = getFileDate(acDelqFile);
//
//   // Convert EBCDIC to ASCII before use
//   if (iLen > 0)
//   {                    
//      strcpy(acBuf, acDelqFile);
//      strcat(acDelqFile, ".ASC");
//      iRet = getFileDate(acDelqFile);
//      if (iRet <= lTmp)
//      {
//         LogMsg("Convert EBC2ASC: %s --> %s", acBuf, acDelqFile);
//         iRet = doEBC2ASCAddCR(acBuf, acDelqFile, iLen);
//         if (iRet)
//         {
//            LogMsg("***** Error converting EBC2ASC from %s to %s", acBuf, acDelqFile);
//            return -1;
//         }
//      }
//   }
//
//   // Open input file
//   LogMsg("Open delinquent tax file %s", acDelqFile);
//   fdDelq = fopen(acDelqFile, "r");
//   if (fdDelq == NULL)
//   {
//      LogMsg("***** Error opening delinquent tax file: %s\n", acDelqFile);
//      return -2;
//   }  
//
//   // Open input file
//   LogMsg("Open Current tax file %s", acSecRollFile);
//   fdSecRoll = fopen(acSecRollFile, "r");
//   if (fdSecRoll == NULL)
//   {
//      LogMsg("***** Error opening Current tax file: %s\n", acSecRollFile);
//      return -2;
//   }  
//
//   // Open Detail file
//   LogMsg("Open Detail file %s", acItemsFile);
//   fdDetail = fopen(acItemsFile, "w");
//   if (fdDetail == NULL)
//   {
//      LogMsg("***** Error creating Detail file: %s\n", acItemsFile);
//      return -4;
//   }
//
//   // Open base file
//   LogMsg("Open Base file %s", acBaseFile);
//   fdBase = fopen(acBaseFile, "w");
//   if (fdBase == NULL)
//   {
//      LogMsg("***** Error creating base file: %s\n", acBaseFile);
//      return -4;
//   }
//
//   // Open owner file
//   LogMsg("Open Owner file %s", acOwnerFile);
//   fdOwner = fopen(acOwnerFile, "w");
//   if (fdOwner == NULL)
//   {
//      LogMsg("***** Error creating Owner file: %s\n", acOwnerFile);
//      return -4;
//   }
//
//   // Open agency file
//   sprintf(acTmpFile, "%s\\%s\\%s_Agency.lst", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   LogMsg("Open Agency file %s", acTmpFile);
//   fdAgency = fopen(acTmpFile, "w");
//   if (fdAgency == NULL)
//   {
//      LogMsg("***** Error creating Owner file: %s\n", acTmpFile);
//      return -4;
//   }
//
//   // Init variables
//
//   // Merge loop 
//   while (!feof(fdSecRoll))
//   {
//      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdSecRoll);
//      if (!pTmp)
//         break;
//
//      // Create new R01 record
//      iRet = Sut_ParseCurrTaxBase(acBuf, acRec);
//      if (!iRet)
//      {
//         // Create Detail & Agency records
//         Sut_ParseTaxDetail(fdDetail, fdAgency);
//
//         // Update delq data
//         iRet = Sut_MergeTaxDelq(acBuf);
//
//         // Create TaxBase record
//         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
//         lOut++;
//         fputs(acRec, fdBase);
//
//         // Create Owner record
//         Tax_CreateTaxOwnerCsv(acRec, &pTaxBase->OwnerInfo);
//         fputs(acRec, fdOwner);
//      } else
//      {
//         if (iRet == 1)
//            LogMsg("---> Drop cancel record %d [%.40s]", lCnt, acRec); 
//      }
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   // Close files
//   if (fdSecRoll)
//      fclose(fdSecRoll);
//   if (fdDelq)
//      fclose(fdDelq);
//   if (fdBase)
//      fclose(fdBase);
//   if (fdDetail)
//      fclose(fdDetail);
//   if (fdOwner)
//      fclose(fdOwner);
//   if (fdAgency)
//      fclose(fdAgency);
//
//   LogMsg("Total records processed:    %u", lCnt);
//   LogMsg("Total output records:       %u", lOut);
//
//   // Import into SQL
//   if (bImport)
//   {
//      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
//      if (!iRet)
//      {
//         iRet = doTaxImport(myCounty.acCntyCode, TAX_OWNER);
//         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
//      }
//
//      if (!iRet)
//      {
//         // Dedup Agency file before import
//         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
//         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
//         if (iRet > 0)
//            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
//      }
//
//   } else
//      iRet = 0;
//
//   return iRet;
//}

/****************************** Sut_ParseTaxDelq *****************************
 *
 * Return number of records output
 *
 *****************************************************************************/

int Sut_ParseTaxDelq(char *pOutbuf, char *pInbuf, FILE *fdOut)
{
   char     acTmp[256];
   int      iTmp, iCnt;
   long     lTmp;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, '|', MAX_FLD_TOKEN, apTokens);
   if (iTmp < SUT_DELQ_REM)
   {
      LogMsg("***** Error: bad TaxDelq record for: %.50s (#tokens=%d)", pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));

   // APN
   strcpy(pOutRec->Apn, myTrim(apTokens[SUT_DELQ_PRCL]));

   // Updated date
   if (*apTokens[SUT_DELQ_ACT_DATE] > ' ' && dateConversion(apTokens[SUT_DELQ_ACT_DATE], acTmp, MMDDYYYY))
      strcpy(pOutRec->Upd_Date, acTmp);
   else
      sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);
/*
#define  SUT_DELQ_A0_ASSMT_NBR      9
#define  SUT_DELQ_A0_YR             10
#define  SUT_DELQ_A0_PRCL           11    // Orig. parcel
#define  SUT_DELQ_A0_TYPE           12    // Secu(R)ed, (S)uplemental
#define  SUT_DELQ_A0_PAY_FLAG       13    // N ???
#define  SUT_DELQ_A0_AREA           14    // TRA
#define  SUT_DELQ_A0_TAX            15
#define  SUT_DELQ_A0_PEN            16
#define  SUT_DELQ_A0_COST           17
#define  SUT_DELQ_A0_AUDIT_AMT      18
*/
   iCnt = 0;

/* Need more investigation
   iTmp = SUT_DELQ_A0_ASSMT_NBR;
   while (iTmp < SUT_DELQ_A9_AUDIT_AMT && *apTokens[iTmp+1] > ' ')
   {
      // For now, just process secured record
      if (*apTokens[iTmp+3] == 'R')
      {
         strcpy(pOutRec->Assmnt_No, apTokens[iTmp]);

         // Tax Year
         strcpy(pOutRec->TaxYear, apTokens[iTmp+1]);

         // Default amt
         lTmp = atol(apTokens[iTmp+6]);
         sprintf(pOutRec->Def_Amt, "%.2f", (double)lTmp/100.0);

         // Pen
         lTmp = atol(apTokens[iTmp+7]);
         sprintf(pOutRec->Pen_Amt, "%.2f", (double)lTmp/100.0);

         // Fee
         lTmp = atol(apTokens[iTmp+8]);
         sprintf(pOutRec->Fee_Amt, "%.2f", (double)lTmp/100.0);

         // Create delimited record
         Tax_CreateDelqCsv(acTmp, (TAXDELQ *)pOutRec);

         // Output record			
         fputs(acTmp, fdOut);
         iCnt++;
      }

      iTmp += 10;
   }
*/

   // Default number
   strcpy(pOutRec->Default_No, myTrim(apTokens[SUT_DELQ_TAX_DEF_NR]));

   // Default date
   if (dateConversion(apTokens[SUT_DELQ_TAX_DEF_DATE], acTmp, MMDDYYYY))
   {
      strcpy(pOutRec->Def_Date, acTmp);
      // Tax Year
      iTmp = atoin(acTmp, 4);
      sprintf(pOutRec->TaxYear, "%d", iTmp-1);
   }

   // Default amt
   lTmp = atol(apTokens[SUT_DELQ_TAX_DEF_AMT]);
   sprintf(pOutRec->Def_Amt, "%.2f", (double)lTmp/100.0);

   // Redemption date
   if (dateConversion(apTokens[SUT_DELQ_RDM_DATE], acTmp, MMDDYYYY))
      strcpy(pOutRec->Red_Date, acTmp);

   // Redemption amt
   lTmp = atol(apTokens[SUT_DELQ_RDM_AMT]);
   sprintf(pOutRec->Red_Amt, "%.2f", (double)lTmp/100.0);

   // Tax amt
   lTmp = atol(apTokens[SUT_DELQ_TOT_TAX]);
   sprintf(pOutRec->Tax_Amt, "%.2f", (double)lTmp/100.0);

   // Pen
   lTmp = atol(apTokens[SUT_DELQ_TOT_PEN]);
   sprintf(pOutRec->Pen_Amt, "%.2f", (double)lTmp/100.0);

   // Fee
   long lCost = atol(apTokens[SUT_DELQ_TOT_COST]);
   long lStateFee = atol(apTokens[SUT_DELQ_STATE_FEE]);
   lTmp = lStateFee+lCost;
   sprintf(pOutRec->Fee_Amt, "%.2f", (double)lTmp/100.0);

   if (*apTokens[SUT_DELQ_RDM_STAT] == 'P')
      pOutRec->isDelq[0] = '0';
   else
      pOutRec->isDelq[0] = '1';

   // Create delimited record
   Tax_CreateDelqCsv(acTmp, (TAXDELQ *)pOutRec);

   // Output record			
   fputs(acTmp, fdOut);
   iCnt++;

   return iCnt;
}

/**************************** Sut_Load_Delq **********************************
 *
 * Create import file for sabsdata.www and import into SQL if specified
 *
 * Return 0 if success.
 *
 *****************************************************************************/

//int Sut_Load_TaxDelq(bool bImport)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
//   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH];
//
//   int      iRet, iLen;
//   long     lOut=0, lCnt=0;
//   FILE     *fdOut, *fdIn;
//
//   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");
//   pTmp = strrchr(acOutFile, '\\');
//   *pTmp = 0;
//   if (_access(acOutFile, 0))
//      _mkdir(acOutFile);
//   *pTmp = '\\';
//
//   GetIniString(myCounty.acCntyCode, "DelqTax", "", acInFile, _MAX_PATH, acIniFile);
//   iLen = GetPrivateProfileInt(myCounty.acCntyCode, "DelqTaxLen", 0, acIniFile);
//   lLastFileDate = getFileDate(acInFile);
//
//   // Convert EBCDIC to ASCII before use
//   if (iLen > 0)
//   {                    
//      strcpy(acBuf, acInFile);
//      strcat(acInFile, ".ASC");
//      iRet = getFileDate(acInFile);
//      if (iRet <= lLastFileDate)
//      {
//         LogMsg("Convert EBC2ASC: %s --> %s", acBuf, acInFile);
//         iRet = doEBC2ASCAddCR(acBuf, acInFile, iLen);
//         if (iRet)
//         {
//            LogMsg("***** Error converting EBC2ASC from %s to %s", acBuf, acInFile);
//            return -1;
//         }
//      }
//   }
//
//   // Open input file
//   LogMsg("Open delinquent tax file %s", acInFile);
//   fdIn = fopen(acInFile, "r");
//   if (fdIn == NULL)
//   {
//      LogMsg("***** Error opening delinquent tax file: %s\n", acInFile);
//      return -2;
//   }  
//
//   // Open Output file
//   LogMsg("Open output file %s", acOutFile);
//   fdOut = fopen(acOutFile, "w");
//   if (fdOut == NULL)
//   {
//      LogMsg("***** Error creating output file: %s\n", acOutFile);
//      return -4;
//   }
//
//   // Merge loop 
//   while (!feof(fdIn))
//   {
//      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
//      if (!pTmp || *pTmp < ' ')
//         break;
//
//      // Create new TaxDelq record
//      iRet = Sut_ParseTaxDelq(acBuf, acRec, fdOut);
//      if (iRet > 0)
//      {
//         lOut += iRet;
//      } else
//      {
//         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
//      }
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   // Close files
//   if (fdIn)
//      fclose(fdIn);
//   if (fdOut)
//      fclose(fdOut);
//
//   LogMsg("Total records processed:          %u", lCnt);
//   LogMsg("Total delinquent records output:  %u", lOut);
//
//   printf("\nTotal output records: %u", lCnt);
//
//   // Import into SQL
//   if (bImport && lOut > 0)
//   {
//      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
//   } else
//      iRet = 0;
//
//   return iRet;
//}

/******************************** Sut_MergeOwner *****************************
 *
 * This version parses owners from CA-Sutter-AsmtRoll.csv
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

//void Sut_MergeOwnerX(char *pOutbuf, char *pOwner1, char *pOwner2, char *pOwner3)
//{
//   int   iTmp;
//   char  *pName1, *pName2, *pTmp, acName1[128], acTmp[128];
//   OWNER myOwner;
//
//   // Clear old names
//   removeNames(pOutbuf, false, false);
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "009619813", 9) )
//   //   iTmp = 0;
//#endif
//   cleanOwner(pOwner1);
//   cleanOwner(pOwner2);
//
//   if (*pOwner1 > ' ')
//   {
//      pName1 = _strupr(pOwner1);
//      pName2 = _strupr(pOwner2);
//   } else
//   {
//      pName1 = _strupr(pOwner2);
//      pName2 = _strupr(pOwner1);
//   }
//
//   // Check for duplicate
//   iTmp = blankRem(pName1);
//   if (iTmp > 20 && !memcmp(pName1, pName2, iTmp-5))
//      *pName2 = 0;
//
//   // Fix special case 
//   strcpy(acTmp, pName1);
//   if (!memcmp(&acTmp[iTmp-4], "ETAL", 4) && acTmp[iTmp-5] > ' ')
//   {
//      acTmp[iTmp-4] = ' ';
//      strcpy(&acTmp[iTmp-3], "ETAL");
//      pName1 = acTmp;
//   }
//
//   if (pTmp = strchr(acTmp, '/'))
//   {
//      *pTmp++ = 0;
//      if (memcmp(pTmp, " ETAL", 5) && *pTmp >= 'A')
//      {
//         sprintf(acName1, "%s & %s", acTmp, pTmp);
//         pName1 = acName1;
//      } else if (strstr(pTmp, " ETAL"))
//      {
//         strcpy(pTmp-1, pTmp);
//         pName1 = acTmp;
//      }
//   }
//
//   if (*pName1 > ' ')
//   {
//      pTmp = strchr(pName1, '/');
//      if (pTmp && strlen(pTmp) < 3)
//         *pTmp = 0;
//      vmemcpy(pOutbuf+OFF_NAME1, pName1, SIZ_NAME1);
//
//      // Update vesting
//      iTmp = updateVesting(myCounty.acCntyCode, pName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
//      if (!iTmp)
//      {
//         if (pTmp = strstr(pName1, "1/2"))
//            *pTmp = 0;
//         iTmp = splitOwner(pName1, &myOwner, 3);
//         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
//      } else
//         vmemcpy(pOutbuf+OFF_NAME_SWAP, pName1, SIZ_NAME_SWAP);
//
//   }
//
//   if (*pName2 > ' ')
//   {
//      pTmp = strchr(pName2, '/');
//      if (pTmp && *(pTmp+1) <= ' ')
//         *pTmp = 0;
//      vmemcpy(pOutbuf+OFF_NAME2, pName2, SIZ_NAME2);
//
//      // Update vesting
//      if (*(pOutbuf+OFF_VEST) == ' ')
//         updateVesting(myCounty.acCntyCode, pName2, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
//   }
//
//   if (*pOwner3 > ' ')
//      *(pOutbuf+OFF_ETAL_FLG) = 'Y';
//}

/***************************** Sut_MergeLienCsv ******************************
 *
 * For LDR "2021 ANNUAL ROLLS - SEPARATED.txt".  
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sut_MergeLienCsv(char *pOutbuf, char *pRollRec)
{
   static   char acApn[32];
   char     acTmp[256], acTmp1[64];
   ULONG    lTmp;
   int      iRet;

   // Parse input rec
   iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < SUT_L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[SUT_L_PIN], iRet);
      return -1;
   }

   iRet = iTrim(apTokens[SUT_L_PIN]);
   if (iRet < 12)
   {
      if (iRet == 8)
      {
         Sut_FormatNewApn(acApn, apTokens[SUT_L_PIN]);
      } else
      {
         LogMsg("*** Ignore bad APN: %s", apTokens[SUT_L_PIN]);
         return 1;
      }
   } else if (!strcmp(apTokens[SUT_L_PIN], acApn))
   {
      LogMsg("*** Ignore duplicate APN: %s", apTokens[SUT_L_PIN]);
      return 1;
   } else
      strcpy(acApn, apTokens[SUT_L_PIN]);

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);
   memcpy(pOutbuf, acApn, strlen(acApn));

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   //iRet = formatMapLink(acApn, acTmp, &myCounty);
   iRet = sprintf(acTmp, "%.2s\\%.2s%.2s  ", &acApn[1], &acApn[1], &acApn[3]);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   //if (getIndexPage(acTmp, acTmp1, &myCounty))
   iRet = sprintf(acTmp1, "%.2s\\%.2s00  ", &acApn[1], &acApn[1]);
   memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Format PREV_APN
   iRet = Sut_FormatPrevApn(acApn, acTmp);
   if (iRet > 0)
      memcpy(pOutbuf+OFF_PREV_APN, acTmp, iRet);
   else if (bDebug)
      LogMsg("*** Unable to format PREV_APN for %s", acApn);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "51SUT", 5);

   // status
   *(pOutbuf+OFF_STATUS) = 'A';

   // TRA
   sprintf(acTmp, "%.6d", atol(apTokens[SUT_L_TAG]));
   vmemcpy(pOutbuf+OFF_TRA, acTmp, 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   remChar(apTokens[SUT_L_ASSDLAND], ',');
   ULONG lLand = atol(apTokens[SUT_L_ASSDLAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   remChar(apTokens[SUT_L_ASSDIMP], ',');
   ULONG lImpr = atol(apTokens[SUT_L_ASSDIMP]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   remChar(apTokens[SUT_L_ASSDFIXTURES], ',');
   remChar(apTokens[SUT_L_ASSDPERSONAL], ',');
   remChar(apTokens[SUT_L_ASSDLIVIMP], ',');
   ULONG lFixtr   = atol(apTokens[SUT_L_ASSDFIXTURES]);
   ULONG lPers    = atol(apTokens[SUT_L_ASSDPERSONAL]);
   ULONG lLivImpr = atol(apTokens[SUT_L_ASSDLIVIMP]);
   lTmp = lFixtr+lPers+lLivImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lLivImpr > 0)
      {
         sprintf(acTmp, "%u         ", lLivImpr);
         memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   remChar(apTokens[SUT_L_HOX], ',');
   remChar(apTokens[SUT_L_DVX], ',');
   remChar(apTokens[SUT_L_LDVX], ',');
   remChar(apTokens[SUT_L_OTHEREXMPT], ',');
   remChar(apTokens[SUT_L_TOTALEXMPT], ',');
   ULONG lExe1 = atol(apTokens[SUT_L_HOX]);
   ULONG lExe2 = atol(apTokens[SUT_L_DVX]);
   ULONG lExe3 = atol(apTokens[SUT_L_LDVX]);
   ULONG lExe4 = atol(apTokens[SUT_L_OTHEREXMPT]);
   ULONG lTotalExe = atol(apTokens[SUT_L_TOTALEXMPT]);
   if (lTotalExe > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   // SUT doesn't have multiple exemption on single parcel.
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "44", 2);
   } else
   {
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      if (lExe2 > 0)
         memcpy(pOutbuf+OFF_EXE_CD1, "12", 2);
      else if (lExe3 > 0)
         memcpy(pOutbuf+OFF_EXE_CD1, "13", 2);
      else if (lExe4 > 0)
         memcpy(pOutbuf+OFF_EXE_CD1, "99", 2);
   }

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SUT_Exemption);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "065010025000", 9) )
   //   iRet = 0;
#endif

   // Legal - obtain legal from roll update

   // UseCode - pull from old roll file REDIFILE

   // Owner
   if (*apTokens[SUT_L_PRIMARYOWNER] > ' ' || *apTokens[SUT_L_RECIPIENT] > ' ')
      Sut_MergeOwner(pOutbuf, apTokens[SUT_L_PRIMARYOWNER], apTokens[SUT_L_RECIPIENT]);

   // Situs
   if (*apTokens[SUT_L_SITUSADDR] > ' ')
      Sut_MergeSAdr(pOutbuf, apTokens[SUT_L_SITUSADDR], apTokens[SUT_L_SITUSCITY], apTokens[SUT_L_SITUSPOSTALCD]);

   // Mailing
   if (*apTokens[SUT_L_DELIVERYADDR] > ' ')
      Sut_MergeMAdr(pOutbuf, apTokens[SUT_L_DELIVERYADDR], apTokens[SUT_L_LASTLINE]);

   return 0;
}

/************************** Sut_MergeLienWithChar ****************************
 *
 * For LDR "2024 Roll Data - Secured - with All Prop Characteristics.csv".  
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

//int Sut_MergeLienWithChar(char *pOutbuf, char *pRollRec)
//{
//   static   char acApn[32];
//   char     acTmp[256], acTmp1[64];
//   ULONG    lTmp;
//   int      iRet;
//
//   // Parse input rec
//   iRet = ParseString(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
//   if (iRet < SUT_LC_FLDS)
//   {
//      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[SUT_LC_PIN], iRet);
//      return -1;
//   }
//
//   iRet = iTrim(apTokens[SUT_LC_PIN]);
//   if (iRet < 12)
//   {
//      if (iRet == 8)
//      {
//         Sut_FormatNewApn(acApn, apTokens[SUT_LC_PIN]);
//      } else
//      {
//         LogMsg("*** Ignore bad APN: %s", apTokens[SUT_LC_PIN]);
//         return 1;
//      }
//   } else if (!strcmp(apTokens[SUT_LC_PIN], acApn))
//   {
//      LogMsg("*** Ignore duplicate APN: %s", apTokens[SUT_LC_PIN]);
//      return 1;
//   } else
//      strcpy(acApn, apTokens[SUT_LC_PIN]);
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//   memcpy(pOutbuf, acApn, strlen(acApn));
//
//   // Format APN
//   iRet = formatApn(acApn, acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//   // Create MapLink and output new record
//   //iRet = formatMapLink(acApn, acTmp, &myCounty);
//   iRet = sprintf(acTmp, "%.2s\\%.2s%.2s  ", &acApn[1], &acApn[1], &acApn[3]);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // Create index map link
//   //if (getIndexPage(acTmp, acTmp1, &myCounty))
//   iRet = sprintf(acTmp1, "%.2s\\%.2s00  ", &acApn[1], &acApn[1]);
//   memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//   // Format PREV_APN
//   iRet = Sut_FormatPrevApn(acApn, acTmp);
//   if (iRet > 0)
//      memcpy(pOutbuf+OFF_PREV_APN, acTmp, iRet);
//   else if (bDebug)
//      LogMsg("*** Unable to format PREV_APN for %s", acApn);
//
//   // County code
//   memcpy(pOutbuf+OFF_CO_NUM, "51SUT", 5);
//
//   // status
//   *(pOutbuf+OFF_STATUS) = 'A';
//
//   // TRA
//   sprintf(acTmp, "%.6d", atol(apTokens[SUT_LC_TRA]));
//   vmemcpy(pOutbuf+OFF_TRA, acTmp, 6);
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // Land
//   remChar(apTokens[SUT_LC_ASSDLAND], ',');
//   ULONG lLand = atol(apTokens[SUT_LC_ASSDLAND]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   remChar(apTokens[SUT_LC_ASSDIMP], ',');
//   ULONG lImpr = atol(apTokens[SUT_LC_ASSDIMP]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: Fixture, PersProp, PPMH
//   ULONG lFixtr   = atol(apTokens[SUT_LC_ASSDFIXTURES]);
//   ULONG lPers    = atol(apTokens[SUT_LC_ASSDPERSONAL]);
//   ULONG lLivImpr = atol(apTokens[SUT_LC_ASSDLIVIMP]);
//   lTmp = lFixtr+lPers+lLivImpr;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//
//      if (lFixtr > 0)
//      {
//         sprintf(acTmp, "%u         ", lFixtr);
//         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
//      }
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%u         ", lPers);
//         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
//      }
//      if (lLivImpr > 0)
//      {
//         sprintf(acTmp, "%u         ", lLivImpr);
//         memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
//      }
//   }
//
//   // Gross total
//   lTmp += (lLand+lImpr);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//   // Exemption
//   ULONG lHO_Exe = atol(apTokens[SUT_LC_HOX]);
//   ULONG lDV_Exe = atol(apTokens[SUT_LC_DISABLEVET_EXE]);
//   ULONG lVet_Exe = atol(apTokens[SUT_LC_VET_EXE]);
//   ULONG lCh_Exe = atol(apTokens[SUT_LC_CHURCH_EXE]);
//   ULONG lRel_Exe = atol(apTokens[SUT_LC_RELIGIOUS_EXE])+atol(apTokens[SUT_LC_CHARITYRELIGIOUS_EXE]);
//   ULONG lCem_Exe = atol(apTokens[SUT_LC_CEMETERY_EXE]);
//   ULONG lPS_Exe = atol(apTokens[SUT_LC_PUBLICSCHOOL_EXE]);
//   ULONG lPL_Exe = atol(apTokens[SUT_LC_PUBLICLIBRAY_EXE]);
//   ULONG lPM_Exe = atol(apTokens[SUT_LC_PUBLICMUSEUM_EXE]);
//   ULONG lCol_Exe = atol(apTokens[SUT_LC_COLLEGE_EXE]);
//   ULONG lHos_Exe = atol(apTokens[SUT_LC_HOSPITAL_EXE]);
//   ULONG lPri_Exe = atol(apTokens[SUT_LC_PRIVATESCHOOL_EXE]);
//   ULONG lOth_Exe = atol(apTokens[SUT_LC_OTHER_EXE]);
//   ULONG lTotalExe = lHO_Exe+lDV_Exe+lVet_Exe+lCh_Exe+lRel_Exe+lCem_Exe+lPS_Exe+lPL_Exe+lPM_Exe+lCol_Exe+lHos_Exe+lPri_Exe+lOth_Exe;
//   if (lTotalExe > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
//      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
//   }  
//
//   if (lHO_Exe > 0)
//   {
//      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
//      memcpy(pOutbuf+OFF_EXE_CD1, "44", 2);
//   } else
//   {
//      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
//
//      if (lDV_Exe > 0)
//         memcpy(pOutbuf+OFF_EXE_CD1, "12", 2);
//      else if (lVet_Exe > 0)
//         memcpy(pOutbuf+OFF_EXE_CD1, "10", 2);
//      else if (lCh_Exe > 0)
//         memcpy(pOutbuf+OFF_EXE_CD1, "32", 2);
//      else if (lRel_Exe > 0)
//         memcpy(pOutbuf+OFF_EXE_CD1, "30", 2);
//      else if (lCem_Exe > 0)
//         memcpy(pOutbuf+OFF_EXE_CD1, "09", 2);
//      else if (lPS_Exe > 0)
//         memcpy(pOutbuf+OFF_EXE_CD1, "26", 2);
//      else if (lPL_Exe > 0)
//         memcpy(pOutbuf+OFF_EXE_CD1, "24", 2);
//      else if (lPM_Exe > 0)
//         memcpy(pOutbuf+OFF_EXE_CD1, "99", 2);
//      else if (lCol_Exe > 0)
//         memcpy(pOutbuf+OFF_EXE_CD1, "23", 2);
//      else if (lHos_Exe > 0)
//         memcpy(pOutbuf+OFF_EXE_CD1, "22", 2);
//      else if (lPri_Exe > 0)
//         memcpy(pOutbuf+OFF_EXE_CD1, "21", 2);
//      else if (lOth_Exe > 0)
//         memcpy(pOutbuf+OFF_EXE_CD1, "99", 2);
//   }
//
//   // Create exemption type
//   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SUT_Exemption);
//
//   // Legal - obtain legal from roll update
//   updateLegal(pOutbuf, myLTrim(apTokens[SUT_LC_ASSESSMENTDESCRIPTION]));
//
//   // UseCode - pull from old roll file 
//   if (*apTokens[SUT_LC_CLASSCODE] > ' ')
//   {
//      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[SUT_LC_CLASSCODE], SIZ_USE_CO);
//      strcpy(acTmp, apTokens[SUT_LC_CLASSCODE]);
//      acTmp[3] = 0;
//      iRet = updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 3, pOutbuf);
//      if (!iRet)
//         LogMsg("*** Unknown UseCode: %s, APN:%.12s", acTmp, pOutbuf);
//   }
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "006020003000", 9) )
//   //   iRet = 0;
//#endif
//
//   // Owner
//   Sut_MergeOwnerX(pOutbuf, apTokens[SUT_LC_LEGALPARTY1], apTokens[SUT_LC_LEGALPARTY2], apTokens[SUT_LC_LEGALPARTY3]);
//
//   // Situs
//   //if (*apTokens[SUT_LC_SITUSADDR] > ' ')
//   //   Sut_MergeSAdr(pOutbuf, apTokens[SUT_LC_SITUSADDR], apTokens[SUT_LC_SITUSCITY], apTokens[SUT_LC_SITUSPOSTALCD]);
//   Sut_MergeSitusX(pOutbuf);
//
//   // Mailing
//   //if (*apTokens[SUT_LC_DELIVERYADDR] > ' ')
//   //   Sut_MergeMAdr(pOutbuf, apTokens[SUT_LC_DELIVERYADDR], apTokens[SUT_LC_LASTLINE]);
//   Sut_MergeMailingX(pOutbuf);
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "006380014000", 9))
//   //   iTmp = 0;
//#endif
//
//   return 0;
//}

/****************************** Sut_Load_LDR_Csv ****************************
 *
 * Load LDR 2021
 *
 ****************************************************************************/

int Sut_Load_LDR_Csv(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdLien;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt;

   // If LDR file has been sorted by -Xl, no need to resort
   if (!strstr(acRollFile, "lien.srt"))
   {
      GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);

      // Sort roll file on ASMT
      sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      sprintf(acRec, "S(#1,C,A)  OMIT(#1,C,GT,\"999\",OR,#4,C,GT,\"U\") F(TXT) DEL(%d) ", cLdrSep);
      iRet = sortFile(acTmpFile, acRollFile, acRec);  
      if (!iRet)
         return -1;
   } 

   // Open Lien file
   LogMsg("Open lien file %s", acRollFile);
   fdLien = fopen(acRollFile, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", acCharFile);
      return -2;
   }

   // Open BPP file
   GetIniString(myCounty.acCntyCode, "BPPFile", "", acRec, _MAX_PATH, acIniFile);
   if (!_access(acRec, 0))
   {
      fdBpp = fopen(acRec, "r");
      if (!fdBpp)
         LogMsg("*** Bad BPP file: %s.  This must be a subset of record from LDR file", acRec);
   } else
      fdBpp = NULL;

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);
   } while (pTmp && !isdigit(*pTmp));

   lCnt = 1;

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLien))
   {
      // Create new R01 record
      //if (lLienYear >= 2024)
      //   iRet = Sut_MergeLienWithChar(acBuf, acRec);// 2024
      //else
         iRet = Sut_MergeLienCsv(acBuf, acRec);     // 2020
      if (!iRet)
      {
         if (fdBpp)
            iRet = Sut_MergeBpp2(acBuf);

         if (fdChar)
            iRet = Sut_MergeStdChar(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } 

      // Get next roll record
      do {
         pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);
      } while (pTmp && !isdigit(*pTmp));

      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fdBpp)
      fclose(fdBpp);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg(" Total input records:  %u", lCnt);
   LogMsg("      output records:  %u", lLDRRecCount);
   LogMsg("        Char matched:  %u", lCharMatch);
   LogMsg("         BBP matched:  %u", lLienMatch);
   LogMsg("         Char skiped:  %u", lCharSkip);
   LogMsg("    bad-city records:  %u", iBadCity);
   LogMsg("  bad-suffix records:  %u\n", iBadSuffix);
   
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return lRet;
}

/******************************* Sut_ExtrLienCsv *****************************
 *
 * Extract values from "2021 ANNUAL ROLLS - SECURED.txt"
 *
 *****************************************************************************/

int Sut_ExtrLienRec2021(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp, iTmp;
   char     acTmp[64], acApn[32];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L_APN], iRet);
      return -1;
   }

   // Skip Personal Property
   if (*apTokens[L_ROLLTYPE] == 'P')
   {
      LogMsg("*** Ignore Personal Property APN: %s", apTokens[L_APN]);
      return 1;
   }

   // Copy APN
   strcpy(acApn, apTokens[L_APN]);
   iRet = iTrim(acApn);
   if (iRet != iApnLen)
   {
      LogMsg("*** Ignore bad APN: %s", acApn);
      return 1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Copy APN
   memcpy(pLienExtr->acApn, acApn, iApnLen);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001100061000", 9) )
   //   iTmp = 0;
#endif

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = dollar2Num(apTokens[L_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[L_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   long lFixtr = dollar2Num(apTokens[L_FIXTURES]);
   long lPers  = dollar2Num(apTokens[L_PPVAL]);
   long lGrowVal = dollar2Num(apTokens[L_LIVINGIMPR]);
   lTmp = lFixtr+lPers+lGrowVal;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_PERSPROP);
      }
      if (lGrowVal > 0)
      {
         iTmp = sprintf(acTmp, "%d", lGrowVal);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = dollar2Num(apTokens[L_HOEXE]);
   long lTotalExe = dollar2Num(apTokens[L_TOTALEXE]);
   if (lTotalExe > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_EXE_TOTAL);
   }  

   if (lExe1 > 0)
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}
int Sut_ExtrLienCsv2021(LPCSTR pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;
   FILE     *fdLienExt;

   LogMsg0("Extract LDR values for %s", pCnty);

   GetIniString(myCounty.acCntyCode, "LienFile", "", acRec, _MAX_PATH, acIniFile);

   // Sort roll file on ASMT
   sprintf(acOutFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acRec, acOutFile, "S(#1,N,A) F(TXT) DEL(9) ");  
   if (!iRet)
      return -1;

   LogMsg("Open sorted LDR file %s", acOutFile);
   fdLien = fopen(acOutFile, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acOutFile);
      return -2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Create lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);
   } while (pTmp && !isdigit(*pTmp));

   // Merge loop
   while (pTmp && !feof(fdLien))
   {
      // Create new record
      iRet = Sut_ExtrLienRec2021(acBuf, acRec);
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLien);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Sut_ExtrLienX *******************************
 *
 * Extract values from "2024 Roll Data - Secured - with All Prop Characteristics.csv"
 *
 *****************************************************************************/

//int Sut_ExtrLienRecX(char *pOutbuf, char *pRollRec)
//{
//   int      iRet, lTmp, iTmp;
//   char     acTmp[64], acApn[32];
//   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;
//
//   // Replace null char with space
//   iRet = replNull(pRollRec, ' ', 0);
//
//   // Parse input rec
//   iRet = ParseString(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
//   if (iRet < SUT_LC_FLDS)
//   {
//      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[SUT_LC_APN], iRet);
//      return -1;
//   }
//
//   // Skip Personal Property
//   //if (*apTokens[SUT_LC_ROLLTYPE] == 'P')
//   //{
//   //   LogMsg("*** Ignore Personal Property APN: %s", apTokens[SUT_LC_APN]);
//   //   return 1;
//   //}
//
//   // Copy APN
//   strcpy(acApn, apTokens[SUT_LC_APN]);
//   iRet = iTrim(acApn);
//   if (iRet != iApnLen)
//   {
//      LogMsg("*** Ignore bad APN: %s", acApn);
//      return 1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', sizeof(LIENEXTR));
//
//   // Copy APN
//   memcpy(pLienExtr->acApn, acApn, iApnLen);
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "001100061000", 9) )
//   //   iTmp = 0;
//#endif
//
//   // Year assessed
//   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);
//
//   // Land
//   long lLand = atol(apTokens[SUT_LC_ASSDLAND]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
//      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = atol(apTokens[SUT_LC_ASSDIMP]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
//      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: Fixture, PersProp, PPMH
//   long lFixtr = atol(apTokens[SUT_LC_ASSDFIXTURES]);
//   long lPers  = atol(apTokens[SUT_LC_ASSDPERSONAL]);
//   long lGrowVal = atol(apTokens[SUT_LC_ASSDLIVIMP]);
//   lTmp = lFixtr+lPers+lGrowVal;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
//      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);
//
//      if (lFixtr > 0)
//      {
//         sprintf(acTmp, "%u         ", lFixtr);
//         memcpy(pLienExtr->acME_Val, acTmp, SIZ_FIXTR);
//      }
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%u         ", lPers);
//         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_PERSPROP);
//      }
//      if (lGrowVal > 0)
//      {
//         iTmp = sprintf(acTmp, "%d", lGrowVal);
//         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, iTmp);
//      }
//   }
//
//   // Gross total
//   lTmp += (lLand+lImpr);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
//      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
//   }
//
//   // Exemption
//   ULONG lHO_Exe = atol(apTokens[SUT_LC_HOX]);
//   ULONG lDV_Exe = atol(apTokens[SUT_LC_DISABLEVET_EXE]);
//   ULONG lVet_Exe = atol(apTokens[SUT_LC_VET_EXE]);
//   ULONG lCh_Exe = atol(apTokens[SUT_LC_CHURCH_EXE]);
//   ULONG lRel_Exe = atol(apTokens[SUT_LC_RELIGIOUS_EXE])+atol(apTokens[SUT_LC_CHARITYRELIGIOUS_EXE]);
//   ULONG lCem_Exe = atol(apTokens[SUT_LC_CEMETERY_EXE]);
//   ULONG lPS_Exe = atol(apTokens[SUT_LC_PUBLICSCHOOL_EXE]);
//   ULONG lPL_Exe = atol(apTokens[SUT_LC_PUBLICLIBRAY_EXE]);
//   ULONG lPM_Exe = atol(apTokens[SUT_LC_PUBLICMUSEUM_EXE]);
//   ULONG lCol_Exe = atol(apTokens[SUT_LC_COLLEGE_EXE]);
//   ULONG lHos_Exe = atol(apTokens[SUT_LC_HOSPITAL_EXE]);
//   ULONG lPri_Exe = atol(apTokens[SUT_LC_PRIVATESCHOOL_EXE]);
//   ULONG lOth_Exe = atol(apTokens[SUT_LC_OTHER_EXE]);
//   ULONG lTotalExe = lHO_Exe+lDV_Exe+lVet_Exe+lCh_Exe+lRel_Exe+lCem_Exe+lPS_Exe+lPL_Exe+lPM_Exe+lCol_Exe+lHos_Exe+lPri_Exe+lOth_Exe;
//   if (lTotalExe > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
//      memcpy(pLienExtr->acExAmt, acTmp, SIZ_EXE_TOTAL);
//   }  
//
//   if (lHO_Exe > 0)
//      pLienExtr->acHO[0] = '1';      // 'Y'
//   else
//      pLienExtr->acHO[0] = '2';      // 'N'
//
//   pLienExtr->LF[0] = 10;
//   pLienExtr->LF[1] = 0;
//
//   return 0;
//}
//
//int Sut_ExtrLienX(LPCSTR pCnty)
//{
//   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
//   int      iRet, iNewRec=0, lCnt=0;
//   FILE     *fdLienExt;
//
//   LogMsg0("Extract LDR values for %s", pCnty);
//
//   GetIniString(myCounty.acCntyCode, "LienFile", "", acRec, _MAX_PATH, acIniFile);
//
//   // Sort roll file on ASMT
//   sprintf(acOutFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   iRet = sortFile(acRec, acOutFile, "S(#1,N,A) F(TXT) DEL(9) ");  
//   if (!iRet)
//      return -1;
//
//   LogMsg("Open sorted LDR file %s", acOutFile);
//   fdLien = fopen(acOutFile, "r");
//   if (fdLien == NULL)
//   {
//      LogMsg("***** Error opening LDR file: %s\n", acOutFile);
//      return -2;
//   }
//
//   // Create lien extract
//   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
//   LogMsg("Create lien extract file %s", acOutFile);
//   fdLienExt = fopen(acOutFile, "w");
//   if (fdLienExt == NULL)
//   {
//      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
//      return -3;
//   }
//
//   // Get 1st rec
//   do {
//      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);
//   } while (pTmp && !isdigit(*pTmp));
//
//   // Merge loop
//   while (pTmp && !feof(fdLien))
//   {
//      // Create new record
//      iRet = Sut_ExtrLienRec2021(acBuf, acRec);
//      if (!iRet)
//      {
//         // Write to output
//         fputs(acBuf, fdLienExt);
//         iNewRec++;
//      }
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//
//      // Get next roll record
//      pTmp = fgets(acRec, MAX_RECSIZE, fdLien);
//      if (!isdigit(acRec[1]))
//         break;      // EOF
//   }
//
//   // Close files
//   if (fdLien)
//      fclose(fdLien);
//   if (fdLienExt)
//      fclose(fdLienExt);
//
//   LogMsg("Total output records:       %u", iNewRec);
//   LogMsg("Total records processed:    %u", lCnt);
//   printf("\nTotal output records: %u", lCnt);
//
//   lRecCnt = lCnt;
//   return 0;
//}
//
/******************************* Sut_ExtrLienCsv *****************************
 *
 * Extract values from "2022 Roll Data - All Parcels.txt" and "2022_BPP.txt"
 * Use PP and Fixtures values from BPP file to merge back into LienExt record.
 * 
 * RecType = 1 (default to LIENEXTR), 2=R01
 *
 *****************************************************************************/

int Sut_MergeBpp1(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256], *apItems[256];
   int      iLoop, iItems;
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 2048, fdBpp);
      if (pRec && acRec[4] > '9')
         pRec = fgets(acRec, 2048, fdBpp);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdBpp);
         fdBpp = NULL;
         return 1;      // EOF
      }

      // Compare APN
      iLoop = memcmp(pOutbuf, pRec+3, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip BPP rec  %.50s", pRec);
         pRec = fgets(acRec, 2048, fdBpp);
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Parse input rec
   iItems = ParseStringIQ(acRec, cLdrSep, MAX_FLD_TOKEN, apItems);
   if (iItems < SUT_L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", acRec, iItems);
      return -1;
   }

   // Other value: Fixture, PersProp, GrowingImpr
   long lFixtr = atol(apItems[SUT_L_ASSDFIXTURES]);
   long lPers  = atol(apItems[SUT_L_ASSDPERSONAL]);
   long lGrowVal = atoln(pLienExtr->extra.MB.GrowImpr, SIZ_PERSPROP);
   long lTmp = lFixtr+lPers+lGrowVal;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_PERSPROP);
      }
   }

   long lLand = atoln(pLienExtr->acLand, SIZ_LAND);
   long lImpr = atoln(pLienExtr->acImpr, SIZ_IMPR);

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Update Exemption
   long lTotalExe = atol(apItems[SUT_L_TOTALEXMPT]);
   if (lTotalExe > 0)
   {
      lTmp = atoln(pLienExtr->acExAmt, SIZ_LIEN_EXEAMT);
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe+lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_EXE_TOTAL);
   }

   lLienMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 2048, fdBpp);

   return 0;
}

int Sut_MergeBpp2(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256], *apItems[256];
   int      iLoop, iItems;

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 2048, fdBpp);
      if (pRec && acRec[4] > '9')
         pRec = fgets(acRec, 2048, fdBpp);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdBpp);
         fdBpp = NULL;
         return 1;      // EOF
      }

      // Compare APN
      iLoop = memcmp(pOutbuf, pRec+3, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip BPP rec  %.50s", pRec);
         pRec = fgets(acRec, 2048, fdBpp);
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Parse input rec
   iItems = ParseStringIQ(acRec, cLdrSep, MAX_FLD_TOKEN, apItems);
   if (iItems < SUT_L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", acRec, iItems);
      return -1;
   }

   // Other value: Fixture, PersProp, GrowingImpr
   long lFixtr = atol(apItems[SUT_L_ASSDFIXTURES]);
   long lPers  = atol(apItems[SUT_L_ASSDPERSONAL]);
   long lGrowVal = atoln(pOutbuf+OFF_PERSPROP, SIZ_PERSPROP);
   long lTmp = lFixtr+lPers+lGrowVal;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
   }

   long lLand = atoln(pOutbuf+OFF_LAND, SIZ_LAND);
   long lImpr = atoln(pOutbuf+OFF_IMPR, SIZ_IMPR);

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Update Exemption
   long lTotalExe = atol(apItems[SUT_L_TOTALEXMPT]);
   if (lTotalExe > 0)
   {
      lTmp = atoln(pOutbuf+OFF_EXE_TOTAL, SIZ_LIEN_EXEAMT);
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe+lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   lLienMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 2048, fdBpp);

   return 0;
}

int Sut_ExtrLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, iTmp;
   ULONG    lTmp;
   char     acTmp[64], acApn[32];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse input rec
   if (cLdrSep == ',')
      iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < SUT_L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", pRollRec, iRet);
      return -1;
   }

   // Skip Personal Property
   if (*apTokens[SUT_L_ASMTTYPE] == 'U')
   {
      if (bDebug)
         LogMsg("*** Ignore unsecured APN: %s", apTokens[SUT_L_PIN]);
      return 1;
   }

   // Copy APN
   strcpy(acApn, apTokens[SUT_L_PIN]);
   iRet = iTrim(acApn);
   if (iRet < 12)
   {
      if (iRet == 8)
      {
         Sut_FormatNewApn(acApn, apTokens[SUT_L_PIN]);
      } else
      {
         LogMsg("*** Ignore bad APN: %s (%d)", acApn, iRet);
         return 1;
      }
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Copy APN
   memcpy(pLienExtr->acApn, acApn, strlen(acApn));

   // Format PREV_APN
   iRet = Sut_FormatPrevApn(acApn, acTmp);
   if (iRet > 0)
      memcpy(pLienExtr->acPrevApn, acTmp, iRet);
   else if (bDebug)
      LogMsg("*** Unable to format PREV_APN for %s", acApn);

   // TRA
   vmemcpy(pLienExtr->acTRA, apTokens[SUT_L_TAG], 6);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001100061000", 9) )
   //   iTmp = 0;
#endif

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   remChar(apTokens[SUT_L_ASSDLAND], ',');
   ULONG lLand = atol(apTokens[SUT_L_ASSDLAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   remChar(apTokens[SUT_L_ASSDIMP], ',');
   ULONG lImpr = atol(apTokens[SUT_L_ASSDIMP]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   remChar(apTokens[SUT_L_ASSDFIXTURES], ',');
   remChar(apTokens[SUT_L_ASSDPERSONAL], ',');
   remChar(apTokens[SUT_L_ASSDLIVIMP], ',');
   ULONG lFixtr = atol(apTokens[SUT_L_ASSDFIXTURES]);
   ULONG lPers  = atol(apTokens[SUT_L_ASSDPERSONAL]);
   ULONG lLivImpr = atol(apTokens[SUT_L_ASSDLIVIMP]);
   lTmp = lFixtr+lPers+lLivImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_PERSPROP);
      }
      if (lLivImpr > 0)
      {
         iTmp = sprintf(acTmp, "%d", lLivImpr);
         memcpy(pLienExtr->extra.Sut.LivingImpr, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   remChar(apTokens[SUT_L_HOX], ',');
   remChar(apTokens[SUT_L_DVX], ',');
   remChar(apTokens[SUT_L_LDVX], ',');
   remChar(apTokens[SUT_L_OTHEREXMPT], ',');
   remChar(apTokens[SUT_L_TOTALEXMPT], ',');
   ULONG lExe1 = atol(apTokens[SUT_L_HOX]);
   ULONG lExe2 = atol(apTokens[SUT_L_DVX]);
   ULONG lExe3 = atol(apTokens[SUT_L_LDVX]);
   ULONG lExe4 = atol(apTokens[SUT_L_OTHEREXMPT]);
   ULONG lTotalExe = atol(apTokens[SUT_L_TOTALEXMPT]);
   if (lTotalExe > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_EXE_TOTAL);
   }  

   if (lExe1 > 0)
   {
      pLienExtr->acHO[0] = '1';      // 'Y'
      memcpy(pLienExtr->extra.Sut.HO_Exe, apTokens[SUT_L_HOX], strlen(apTokens[SUT_L_HOX]));
   } else
   {
      pLienExtr->acHO[0] = '2';      // 'N'
      if (lExe2 > 0)
         memcpy(pLienExtr->extra.Sut.DV_Exe, apTokens[SUT_L_DVX], strlen(apTokens[SUT_L_DVX]));
      if (lExe3 > 0)
         memcpy(pLienExtr->extra.Sut.LDV_Exe, apTokens[SUT_L_LDVX], strlen(apTokens[SUT_L_LDVX]));
      if (lExe4 > 0)
         memcpy(pLienExtr->extra.Sut.Other_Exe, apTokens[SUT_L_OTHEREXMPT], strlen(apTokens[SUT_L_OTHEREXMPT]));
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/*
 * Create BPP file separately to merge it back with real parcel
 */
int Sut_ExtrLienCsv(LPCSTR pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH], acTmp[256];
   int      iRet, iNewRec=0, lCnt=0;
   FILE     *fdLienExt;

   LogMsg0("Extract LDR values for %s", pCnty);

   // Sort roll file on ASMT
   sprintf(acRec, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acTmp, "S(#1,C,A) OMIT(#1,C,GT,\"999\",OR,#4,C,GT,\"U\") DEL(%d) ", cLdrSep);
   iRet = sortFile(acRollFile, acRec, acTmp);  
   if (!iRet)
      return -1;

   strcpy(acRollFile, acRec);
   LogMsg("Open sorted LDR file %s", acRollFile);
   fdLien = fopen(acRollFile, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acRollFile);
      return -2;
   }

   // Open BPP file
   GetIniString(myCounty.acCntyCode, "BPPFile", "", acRec, _MAX_PATH, acIniFile);
   if (!_access(acRec, 0))
   {
      LogMsg("Open BPP file %s", acRec);
      fdBpp = fopen(acRec, "r");
      if (!fdBpp)
         LogMsg("*** Bad BPP file: %s.  This must be a subset of record from LDR file", acRec);
   } else
      fdBpp = NULL;

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Create lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);
   } while (pTmp && !isdigit(*pTmp));

   // Merge loop
   while (pTmp && !feof(fdLien))
   {
      // Create new record
      iRet = Sut_ExtrLienRec(acBuf, acRec);
      if (!iRet)
      {
         // Update BPP record
         if (fdBpp)
            Sut_MergeBpp1(acBuf);

         // Write to output
         fputs(acBuf, fdLienExt);
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLien);
      if (pTmp && !isdigit(acRec[2]))
         break;      // EOF
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdBpp)
      fclose(fdBpp);

   LogMsg("Total output records:      %u", iNewRec);
   LogMsg("     records processed:    %u", lCnt);
   LogMsg("            BPP matched:   %u\n", lLienMatch);

   lRecCnt = lCnt;
   return 0;
}

/*************************** Sut_Load_RollCorrection *************************
 *
 * Extract values from "2021 ANNUAL ROLLS COMBINED FOR PARCEL QUEST 12-9-2021.csv"
 * and replace LDR data.
 *
 * Run FixCsv and resort it by AIN before processing
 *
 *****************************************************************************/

int Sut_MergeRollCorrection(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp, iRet;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "0010824200", 10))
   //   lTmp = 0;
#endif

   // Parse input string
   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < SUT_COR_FLDS)
   {
      LogMsg("***** Sut_MergeRollCorrection: bad input record for APN=%s", apTokens[0]);
      return -1;
   }

   // Land
   long lLand = dollar2Num(apTokens[SUT_COR_ASSDLAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[SUT_COR_ASSDIMP]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   long lPers    = dollar2Num(apTokens[SUT_COR_ASSDPERSONAL]);
   long lFixt    = dollar2Num(apTokens[SUT_COR_ASSDFIXTURES]);
   long lLivImpr = dollar2Num(apTokens[SUT_COR_ASSDLIVIMP]);
   lTmp = lPers + lFixt + lLivImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%u         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lLivImpr > 0)
      {
         sprintf(acTmp, "%u         ", lLivImpr);
         memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // HO Exempt
   long lExe = dollar2Num(apTokens[SUT_COR_HOX]);
   if (lExe > 0)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   lExe = dollar2Num(apTokens[SUT_COR_TOTALEXMPT]);
   if (lExe > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Full exemption
   if (lExe > 0 && lExe >= lTmp)
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // TRA
   lTmp = atol(apTokens[SUT_COR_TAG]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   return 0;
}

int Sut_Load_RollCorrection(int iSkip)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acRawFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd, iRollDrop;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open roll file
   LogMsg("Open roll correction file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll correction file: %s\n", acRollFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iSkip > 0)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iRollUpd=iRollDrop=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "004020015", 9))
      //   iRet = 1;
#endif
      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Sut_MergeRollCorrection(acBuf, acRollRec);
         if (!iRet)
            iRollUpd++;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         lCnt++;

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)
      {
         iRollDrop++;         // Ignore roll record that doesn't match

         // Get next record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         lCnt++;

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Write out any record left from S01 
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      if (nBytesRead == iRecLen)
      {
         WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else
         break;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhIn)
      CloseHandle(fhIn);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("      updated records:      %u", iRollUpd);
   LogMsg("      dropped records:      %u\n", iRollDrop);
   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;

   return lRet;
}

/********************************* Sut_MergeSAdr *****************************
 *
 * This version parse mail address from "CA-Sutter-AsmtRoll.csv"
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sut_MergeSAdr(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acStreet[64];
   long     lTmp;
   int      iTmp;

   // Remove old situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[R_SITUSSTREETNUMBER]);
   if (lTmp > 0)
   {
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[R_SITUSSTREETNUMBER], strlen(apTokens[R_SITUSSTREETNUMBER]));
      iTmp = sprintf(acTmp, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      strcpy(acAddr1, acTmp);

      if (*apTokens[R_SITUSSTREETNUMBERSUFFIX] > ' ')
         vmemcpy(pOutbuf+OFF_S_STR_SUB, apTokens[R_SITUSSTREETNUMBERSUFFIX], SIZ_S_STR_SUB);

      if (*apTokens[R_SITUSSTREETPREDIRECTIONAL] > ' ')
      {
         strcat(acAddr1, apTokens[R_SITUSSTREETPREDIRECTIONAL]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[R_SITUSSTREETPREDIRECTIONAL], strlen(apTokens[R_SITUSSTREETPREDIRECTIONAL]));
      }
   }

   strcpy(acStreet, _strupr(apTokens[R_SITUSSTREETNAME]));
   strcat(acAddr1, acStreet);

   if (*apTokens[R_SITUSSTREETTYPE] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[R_SITUSSTREETTYPE]);

      // Make Plaza part of street name
      if (!memcmp(apTokens[R_SITUSSTREETTYPE], "PLAZA", 5) || !memcmp(apTokens[R_SITUSSTREETTYPE], "PARK", 4))
      {
         strcat(acStreet, " ");
         strcat(acStreet, apTokens[R_SITUSSTREETTYPE]);
      } else
      {
         iTmp = GetSfxCodeX(apTokens[R_SITUSSTREETTYPE], acTmp);
         if (iTmp > 0)
         {
            Sfx2Code(acTmp, acCode);
            memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
         } else
         {
            LogMsg0("*** Invalid suffix: %s [%s]", apTokens[R_SITUSSTREETTYPE], apTokens[R_APN]);
            iBadSuffix++;
         }
      }
   } 
   
   vmemcpy(pOutbuf+OFF_S_STREET, acStreet, SIZ_S_STREET);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "616141018", 9))
   //   acTmp[0] = 0;
#endif

   if (isalnum(*apTokens[R_SITUSUNITNUMBER]) || *apTokens[R_SITUSUNITNUMBER] == '#')
   {
      strcat(acAddr1, " ");
      if (isdigit(*apTokens[R_SITUSUNITNUMBER]))
         strcat(acAddr1, "#");
      else if ((*apTokens[R_SITUSUNITNUMBER]) != '#')
         strcat(acAddr1, "STE ");
      strcat(acAddr1, apTokens[R_SITUSUNITNUMBER]);
      memcpy(pOutbuf+OFF_S_UNITNO, apTokens[R_SITUSUNITNUMBER], strlen(apTokens[R_SITUSUNITNUMBER]));
   } else if (*(pOutbuf+OFF_M_UNITNO) > ' ' &&
      !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
      !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) )
   {
      memcpy(pOutbuf+OFF_S_UNITNO, pOutbuf+OFF_M_UNITNO, SIZ_M_UNITNO);
      sprintf(acTmp, " %.*s", SIZ_M_UNITNO, pOutbuf+OFF_M_UNITNO);
      strcat(acAddr1, acTmp);
   } else if (*apTokens[R_SITUSUNITNUMBER] > ' ')
   {
      if (isalnum(*(1+apTokens[R_SITUSUNITNUMBER])))
      {
         strcat(acAddr1, " #");
         strcat(acAddr1, apTokens[R_SITUSUNITNUMBER]+1);
         memcpy(pOutbuf+OFF_S_UNITNO, apTokens[R_SITUSUNITNUMBER]+1, strlen(apTokens[R_SITUSUNITNUMBER])-1);
      } else
         LogMsg("*** Invalid situs Unit#: %s [%s]", apTokens[R_SITUSUNITNUMBER], apTokens[R_APN]);
   }

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Situs city
   if (*apTokens[R_SITUSCITYNAME] > ' ')
   {
      char sCity[32], sZip[32];

      if (!_memicmp(apTokens[R_SITUSCITYNAME], "PLUMAS LAKE", 12) || !_memicmp(apTokens[R_SITUSCITYNAME], "SHERI", 5))
      {
         strcpy(sCity, "YUBA CITY");
         strcpy(acCode, "18");
         strcpy(sZip, "95961");
         iTmp = 18;
      } else
      {
         iTmp = City2CodeEx(_strupr(apTokens[R_SITUSCITYNAME]), acCode, sCity, apTokens[R_APN]);
         strcpy(sZip, apTokens[R_SITUSZIPCODE]);
      }
      if (iTmp > 0)
      {
         vmemcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
      } else
         strcpy(sCity, apTokens[R_SITUSCITYNAME]);

      if (sZip[0] == '9')
         vmemcpy(pOutbuf+OFF_S_ZIP, sZip, SIZ_S_ZIP);

      iTmp = sprintf(acTmp, "%s, CA %s", sCity, sZip);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
   }

   return 0;
}

/******************************* Sut_MergeSitusX *****************************
 *
 * This version parse mail address from "2024 Roll Data - Secured - with All Prop Characteristics.csv"
 * StrName may contain Suffix and Unit# (SANBORN RD E)
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

//void Sut_ParseAdr1(ADR_REC *pAdrRec, char *pAddr)
//{
//   char  acAdr[256], acStrName[256], acTmp[256], *apItems[16];
//   int   iCnt, iTmp, iIdx;
//   bool  bRet;
//
//   acStrName[0] = 0;
//   strcpy(acAdr, pAddr);
//
//   // Tokenize address
//   iCnt = ParseString(acAdr, 32, 16, apItems);
//
//   if (iCnt == 1)
//   {
//      strcpy(acStrName, pAddr);
//   } else
//   {
//      iIdx = 0;
//
//      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
//      if (apItems[iIdx][0] == '#')
//      {
//         strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][0], SIZ_M_UNITNO);
//         strncpy(pAdrRec->UnitNox, (char *)&apItems[iIdx][0], SIZ_M_UNITNOX);
//         iIdx++;
//      }
//
//      if (iIdx >= iCnt)
//      {
//         // Bad address
//         return;
//      }
//
//      // Check for suffix
//      bRet = false;
//      for (iIdx = 1; iIdx < iCnt; iIdx++)
//      {
//         if ((iTmp=GetSfxCodeX(apItems[iIdx], acTmp)))
//         {
//            strcpy(pAdrRec->strSfx, acTmp);
//            sprintf(acTmp, "%d", iTmp);
//            strcpy(pAdrRec->SfxCode, acTmp);
//            bRet = true;         // Done
//            iIdx++;
//            break;
//         } 
//      }
//
//      if (bRet)
//      {
//         if (iIdx < iCnt)
//         {
//            strcpy(pAdrRec->Unit, apItems[iIdx]);
//            strcpy(pAdrRec->UnitNox, apItems[iIdx]);
//            iCnt = iIdx-2;
//         }
//      } else
//         iIdx = 1;
//
//      strcpy(acStrName, apItems[0]);
//      while (iIdx < iCnt)
//      {
//         strcat(acStrName, " ");
//         strcat(acStrName, apItems[iIdx++]);
//      }
//   }
//
//   strncpy(pAdrRec->strName, acStrName, SIZ_M_STREET);
//}
//
//int Sut_MergeSitusX(char *pOutbuf)
//{
//   static   char acRec[512], *pRec=NULL;
//   char     acTmp[256], acCode[32], acAddr1[256], acStreet[64];
//   long     lTmp;
//   int      iTmp;
//   ADR_REC  sSitusAdr;
//
//   // Merge data
//   acAddr1[0] = 0;
//   lTmp = atol(apTokens[SUT_LC_SITUSNUMBER]);
//   if (lTmp > 0)
//   {
//      memcpy(pOutbuf+OFF_S_HSENO, apTokens[SUT_LC_SITUSNUMBER], strlen(apTokens[SUT_LC_SITUSNUMBER]));
//      iTmp = sprintf(acTmp, "%d ", lTmp);
//      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
//      strcpy(acAddr1, acTmp);
//
//      if (*apTokens[SUT_LC_SITUSNUMBER] > ' ')
//         vmemcpy(pOutbuf+OFF_S_STR_SUB, apTokens[SUT_LC_SITUSNUMBERSUFFIX], SIZ_S_STR_SUB);
//
//      if (*apTokens[SUT_LC_SITUSSTREETPREDIRECTIONAL] > ' ')
//      {
//         strcat(acAddr1, apTokens[SUT_LC_SITUSSTREETPREDIRECTIONAL]);
//         strcat(acAddr1, " ");
//         memcpy(pOutbuf+OFF_S_DIR, apTokens[SUT_LC_SITUSSTREETPREDIRECTIONAL], strlen(apTokens[SUT_LC_SITUSSTREETPREDIRECTIONAL]));
//      }
//   }
//
//   strcpy(acStreet, _strupr(apTokens[SUT_LC_SITUSSTREETNAME]));
//   strcat(acAddr1, acStreet);
//
//   if (*apTokens[SUT_LC_SITUSSTREETTYPE] > ' ')
//   {
//      strcat(acAddr1, " ");
//      strcat(acAddr1, apTokens[SUT_LC_SITUSSTREETTYPE]);
//
//      // Make Plaza part of street name
//      if (!memcmp(apTokens[SUT_LC_SITUSSTREETTYPE], "PLAZA", 5) || !memcmp(apTokens[SUT_LC_SITUSSTREETTYPE], "PARK", 4))
//      {
//         strcat(acStreet, " ");
//         strcat(acStreet, apTokens[SUT_LC_SITUSSTREETTYPE]);
//      } else
//      {
//         iTmp = GetSfxCodeX(apTokens[SUT_LC_SITUSSTREETTYPE], acTmp);
//         if (iTmp > 0)
//         {
//            Sfx2Code(acTmp, acCode);
//            memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
//         } else
//         {
//            LogMsg0("*** Invalid suffix: %s [%s]", apTokens[SUT_LC_SITUSSTREETTYPE], apTokens[R_APN]);
//            iBadSuffix++;
//         }
//      }
//   } else
//   {
//#ifdef _DEBUG
//      if (!memcmp(pOutbuf, "065010025000", 9))
//         acTmp[0] = 0;
//#endif
//
//      memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
//      Sut_ParseAdr1(&sSitusAdr, acAddr1);
//
//      strcpy(acStreet, sSitusAdr.strName);
//      vmemcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, SIZ_S_SUFF);
//      vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
//      vmemcpy(pOutbuf+OFF_S_UNITNOX, sSitusAdr.UnitNox, SIZ_S_UNITNOX);
//      vmemcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, SIZ_S_DIR);
//   }
//   
//   vmemcpy(pOutbuf+OFF_S_STREET, acStreet, SIZ_S_STREET);
//
//   if (isalnum(*apTokens[SUT_LC_SITUSUNITNUMBER]) || *apTokens[SUT_LC_SITUSUNITNUMBER] == '#')
//   {
//      strcat(acAddr1, " ");
//      if (isdigit(*apTokens[SUT_LC_SITUSUNITNUMBER]))
//         strcat(acAddr1, "#");
//      else if ((*apTokens[SUT_LC_SITUSUNITNUMBER]) != '#')
//         strcat(acAddr1, "STE ");
//      strcat(acAddr1, apTokens[SUT_LC_SITUSUNITNUMBER]);
//      memcpy(pOutbuf+OFF_S_UNITNO, apTokens[SUT_LC_SITUSUNITNUMBER], strlen(apTokens[SUT_LC_SITUSUNITNUMBER]));
//   } else if (*apTokens[SUT_LC_SITUSUNITNUMBER] > ' ')
//   {
//      if (isalnum(*(1+apTokens[SUT_LC_SITUSUNITNUMBER])))
//      {
//         strcat(acAddr1, " #");
//         strcat(acAddr1, apTokens[SUT_LC_SITUSUNITNUMBER]+1);
//         memcpy(pOutbuf+OFF_S_UNITNO, apTokens[SUT_LC_SITUSUNITNUMBER]+1, strlen(apTokens[SUT_LC_SITUSUNITNUMBER])-1);
//      } else
//         LogMsg("*** Invalid situs Unit#: %s [%s]", apTokens[SUT_LC_SITUSUNITNUMBER], apTokens[R_APN]);
//   }
//
//   iTmp = blankRem(acAddr1);
//   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);
//   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
//
//   // Situs city
//   if (*apTokens[SUT_LC_SITUSCITYNAME] > ' ')
//   {
//      char sCity[32], sZip[32];
//
//      if (!_memicmp(apTokens[SUT_LC_SITUSCITYNAME], "PLUMAS LAKE", 12) || !_memicmp(apTokens[SUT_LC_SITUSCITYNAME], "SHERI", 5))
//      {
//         strcpy(sCity, "YUBA CITY");
//         strcpy(acCode, "18");
//         strcpy(sZip, "95961");
//         iTmp = 18;
//      } else
//      {
//         iTmp = City2CodeEx(_strupr(apTokens[SUT_LC_SITUSCITYNAME]), acCode, sCity, apTokens[R_APN]);
//         strcpy(sZip, apTokens[SUT_LC_SITUSZIPCODE]);
//      }
//      if (iTmp > 0)
//      {
//         vmemcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
//      } else
//         strcpy(sCity, apTokens[SUT_LC_SITUSCITYNAME]);
//
//      if (sZip[0] == '9')
//         vmemcpy(pOutbuf+OFF_S_ZIP, sZip, SIZ_S_ZIP);
//
//      iTmp = sprintf(acTmp, "%s, CA %s", sCity, sZip);
//      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
//   }
//
//   return 0;
//}

/******************************** Sut_MergeMAdr ******************************
 *
 * This version parse mail address from "CA-Sutter-AsmtRoll.csv"
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sut_MergeMAdr(char *pOutbuf)
{
   char        acTmp[256], acAddr1[256], *pAddr1;
   int         iTmp;
   bool        bUnitAvail = false;

   ADR_REC     sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf);

   //if (*apTokens[R_MAILNAME] > ' ')
   //{
   //   iTmp = replUnPrtChar(apTokens[R_MAILNAME]);
   //   updateCareOf(pOutbuf, apTokens[R_MAILNAME], 0);
   //}

   // Check for blank address
   replUnPrtChar(apTokens[R_MAILADDRESS]);
   pAddr1 = apTokens[R_MAILADDRESS];
   if (memcmp(pAddr1, "     ", 5) && memcmp(pAddr1, "UNKNOWN", 7))
   {
      strcpy(acAddr1, pAddr1);
      iTmp = 0;
      if (!memcmp(acAddr1, "C/O", 3))
      {
         iTmp = 4;
         while (!isdigit(*(pAddr1+iTmp)))
         {
            iTmp++;
         }
         pAddr1 += iTmp;
         if (iTmp > 8)
            memcpy(pOutbuf+OFF_CARE_OF, acAddr1, iTmp);
      } else
         pAddr1 = acAddr1;
      memcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, strlen(pAddr1));

      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

      // Parsing mail address
      parseAdr1(&sMailAdr, pAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }

      if (sMailAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));

      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));

      if (sMailAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            memcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, strlen(sMailAdr.Unit));
         }
      }


#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "607224002", 9) )
      //   iTmp = 0;
#endif

      // City/State
      if (*apTokens[R_MAILCITY] > ' ')
      {
         // Remove quote & comma at the end of city name
         strcpy(acAddr1, apTokens[R_MAILCITY]);
         iTmp = iTrim(acAddr1);
         if (acAddr1[iTmp-1] == ',')
            acAddr1[iTmp-1] = 0;

         if (*apTokens[R_MAILSTATE] > ' ')
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY);
            vmemcpy(pOutbuf+OFF_M_ST, apTokens[R_MAILSTATE], 2);

            sprintf(acTmp, "%s, %s %s", acAddr1, apTokens[R_MAILSTATE], apTokens[R_MAILZIPCODE]);
            iTmp = blankRem(acTmp);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
         } else
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY+SIZ_M_ST+SIZ_M_ZIP);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, SIZ_M_CTY_ST_D);
         }
      }

      iTmp = atol(apTokens[R_MAILZIPCODE]);
      if (iTmp > 500)
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[R_MAILZIPCODE], SIZ_M_ZIP);
   }
}

/***************************** Sut_MergeMailingX *****************************
 *
 * This version parse mail address from "2024 Roll Data - Secured - with All Prop Characteristics.csv"
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

//int Sut_MergeMailingX(char *pOutbuf)
//{
//   char        acTmp[256], acAddr1[256], *pAddr1;
//   int         iTmp;
//   bool        bUnitAvail = false;
//
//   ADR_REC     sMailAdr;
//
//   if (*apTokens[SUT_LC_ATTENTIONLINE] > ' ')
//   {
//      iTmp = replUnPrtChar(apTokens[SUT_LC_ATTENTIONLINE]);
//      updateCareOf(pOutbuf, apTokens[SUT_LC_ATTENTIONLINE], 0);
//   }
//
//   // Check for blank address
//   replUnPrtChar(apTokens[SUT_LC_MAILADDRESS]);
//   pAddr1 = apTokens[SUT_LC_MAILADDRESS];
//   if (memcmp(pAddr1, "     ", 5) && memcmp(pAddr1, "UNKNOWN", 7))
//   {
//      strcpy(acAddr1, pAddr1);
//      iTmp = 0;
//      if (!memcmp(acAddr1, "C/O", 3))
//      {
//         iTmp = 4;
//         while (!isdigit(*(pAddr1+iTmp)))
//         {
//            iTmp++;
//         }
//         pAddr1 += iTmp;
//         if (iTmp > 8)
//            memcpy(pOutbuf+OFF_CARE_OF, acAddr1, iTmp);
//      } else
//         pAddr1 = acAddr1;
//      memcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, strlen(pAddr1));
//
//      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
//
//      // Parsing mail address
//      parseAdr1(&sMailAdr, pAddr1);
//      if (sMailAdr.lStrNum > 0)
//      {
//         iTmp = sprintf(acTmp, "%d ", sMailAdr.lStrNum);
//         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
//         if (sMailAdr.strSub[0] > '0')
//         {
//            sprintf(acTmp, "%s  ", sMailAdr.strSub);
//            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
//         }
//      }
//
//      if (sMailAdr.strDir[0] > ' ')
//         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
//
//      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
//
//      if (sMailAdr.strSfx[0] > ' ')
//         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
//
//      if (sMailAdr.Unit[0] > ' ')
//      {
//         if (sMailAdr.Unit[0] == '#')
//            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
//         else
//         {
//            *(pOutbuf+OFF_M_UNITNO) = '#';
//            memcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, strlen(sMailAdr.Unit));
//         }
//      }
//
//
//#ifdef _DEBUG
//      //if (!memcmp(pOutbuf, "607224002", 9) )
//      //   iTmp = 0;
//#endif
//
//      // City/State
//      if (*apTokens[SUT_LC_MAILCITY] > ' ')
//      {
//         // Remove quote & comma at the end of city name
//         strcpy(acAddr1, apTokens[SUT_LC_MAILCITY]);
//         iTmp = iTrim(acAddr1);
//         if (acAddr1[iTmp-1] == ',')
//            acAddr1[iTmp-1] = 0;
//
//         if (*apTokens[SUT_LC_MAILSTATE] > ' ')
//         {
//            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY);
//            vmemcpy(pOutbuf+OFF_M_ST, apTokens[SUT_LC_MAILSTATE], 2);
//
//            sprintf(acTmp, "%s, %s %s", acAddr1, apTokens[SUT_LC_MAILSTATE], apTokens[SUT_LC_MAILZIPCODE]);
//            iTmp = blankRem(acTmp);
//            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
//         } else
//         {
//            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY+SIZ_M_ST+SIZ_M_ZIP);
//            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, SIZ_M_CTY_ST_D);
//         }
//      }
//
//      iTmp = atol(apTokens[SUT_LC_MAILZIPCODE]);
//      if (iTmp > 500)
//         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[SUT_LC_MAILZIPCODE], SIZ_M_ZIP);
//   }
//
//   return 0;
//}

/******************************** Sut_MergeOwner *****************************
 *
 * This version parses owners from CA-Sutter-AsmtRoll.csv
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sut_MergeOwner(char *pOutbuf)
{
   int   iTmp;
   char  *pName1, *pName2, *pTmp, acName1[128], acTmp[128];
   OWNER myOwner;

   // Clear old names, vesting, and DBA
   removeNames(pOutbuf, false, true);

   if (*apTokens[R_DOINGBUSINESSAS] > ' ')
   {
      iTmp = cleanOwner(apTokens[R_DOINGBUSINESSAS]);
      if (iTmp > 0)
         blankRem(apTokens[R_DOINGBUSINESSAS]);
      vmemcpy(pOutbuf+OFF_DBA, _strupr(apTokens[R_DOINGBUSINESSAS]), SIZ_DBA);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009619813", 9) )
   //   iTmp = 0;
#endif
   cleanOwner(apTokens[R_LEGALPARTY1]);
   cleanOwner(apTokens[R_LEGALPARTY2]);

   if (*apTokens[R_LEGALPARTY1] > ' ')
   {
      pName1 = _strupr(apTokens[R_LEGALPARTY1]);
      pName2 = _strupr(apTokens[R_LEGALPARTY2]);
   } else
   {
      pName1 = _strupr(apTokens[R_LEGALPARTY2]);
      pName2 = _strupr(apTokens[R_LEGALPARTY1]);
   }

   // Check for duplicate
   iTmp = blankRem(pName1);
   if (iTmp > 20 && !memcmp(pName1, pName2, iTmp-5))
      *pName2 = 0;

   // Fix special case 
   strcpy(acTmp, pName1);
   if (!memcmp(&acTmp[iTmp-4], "ETAL", 4) && acTmp[iTmp-5] > ' ')
   {
      acTmp[iTmp-4] = ' ';
      strcpy(&acTmp[iTmp-3], "ETAL");
      pName1 = acTmp;
   }

   if (pTmp = strchr(acTmp, '/'))
   {
      *pTmp++ = 0;
      if (memcmp(pTmp, " ETAL", 5) && *pTmp >= 'A')
      {
         sprintf(acName1, "%s & %s", acTmp, pTmp);
         pName1 = acName1;
      } else if (strstr(pTmp, " ETAL"))
      {
         strcpy(pTmp-1, pTmp);
         pName1 = acTmp;
      }
   }

   if (*pName1 > ' ')
   {
      pTmp = strchr(pName1, '/');
      if (pTmp && strlen(pTmp) < 3)
         *pTmp = 0;
      vmemcpy(pOutbuf+OFF_NAME1, pName1, SIZ_NAME1);

      // Update vesting
      iTmp = updateVesting(myCounty.acCntyCode, pName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
      if (!iTmp)
      {
         if (pTmp = strstr(pName1, "1/2"))
            *pTmp = 0;
         iTmp = splitOwner(pName1, &myOwner, 3);
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
      } else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, pName1, SIZ_NAME_SWAP);

   }

   if (*pName2 > ' ')
   {
      pTmp = strchr(pName2, '/');
      if (pTmp && *(pTmp+1) <= ' ')
         *pTmp = 0;
      vmemcpy(pOutbuf+OFF_NAME2, pName2, SIZ_NAME2);

      // Update vesting
      if (*(pOutbuf+OFF_VEST) == ' ')
         updateVesting(myCounty.acCntyCode, pName2, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
   }

   if (*apTokens[R_LEGALPARTY3] > ' ')
      *(pOutbuf+OFF_ETAL_FLG) = 'Y';
}

/****************************** Sut_MergeRollCsv *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sut_MergeRollCsv(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], acApn[32];
   LONGLONG lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Check for bad char
   Sut_ReplBadChar((unsigned char *)pRollRec);

   // Remove extra spaces
   blankRem(pRollRec);

   // Parse roll record
   if (cDelim == ',')
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < R_COLS)
   {
      LogMsg("***** Error: bad input record for APN=%s", pRollRec);
      return -1;
   }

   // Ignore unsecured parcel 
   if (_memicmp(apTokens[R_ROLLCAT], "Sec", 3))
   {
      if (bDebug)
         LogMsg("- Drop %s: %s", apTokens[R_ROLLCAT], apTokens[R_APN]);
      lSec++;
      return 1;
   }

   // Ignore possessory interest & SBE
   if (!_memicmp(apTokens[R_CLASSDESCRIPTION], "POS", 3) || !_memicmp(apTokens[R_CLASSDESCRIPTION], "SBE", 3))
   {
      if (bDebug)
         LogMsg("- Drop %s: %s", apTokens[R_CLASSDESCRIPTION], apTokens[R_APN]);
      lPos++;
      return 1;
   }

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      iTmp = strlen(apTokens[R_APN]);
      if (iTmp < 12)
         iTmp = sprintf(acApn, "%.*s%s", iApnLen-iTmp, "00000", apTokens[R_APN]);
      else
         strcpy(acApn, apTokens[R_APN]);
      
      // Start copying data
      vmemcpy(pOutbuf, acApn, SIZ_APN_S, iTmp);
      vmemcpy(pOutbuf+OFF_ALT_APN, acApn, SIZ_ALT_APN, iTmp);

      // Format APN
      iRet = formatApn(acApn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      //iRet = formatMapLink(acApn, acTmp, &myCounty);
      iRet = sprintf(acTmp, "%.2s\\%.2s%.2s  ", &acApn[1], &acApn[1], &acApn[3]);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      //if (getIndexPage(acTmp, acTmp1, &myCounty))
      iRet = sprintf(acTmp1, "%.2s\\%.2s00  ", &acApn[1], &acApn[1]);
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "51SUTA", 6);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[R_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[R_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other values
      long lFixt     = atoi(apTokens[R_TRADEFIXTURESAMOUNT]);
      long lPP       = atoi(apTokens[R_PERSONALVALUE]);
      long lLivImpr  = atoi(apTokens[R_LIVINGIMPROVEMENTS]);
      lTmp = lFixt+lPP+lLivImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lLivImpr > 0)
         {
            sprintf(acTmp, "%d         ", lLivImpr);
            memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // Temporary remove old exe code
      memset(pOutbuf+OFF_EXE_CD1+2, ' ', 10);

      // HO Exempt
      long lHO_Exe = atol(apTokens[R_HOX]);
      if (lHO_Exe > 0)
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Other Exemption
      long lOth_Exe = atol(apTokens[R_VETERANSEXEMPTION]);
      lOth_Exe += atol(apTokens[R_DISABLEDVETERANSEXEMPTION]);
      lOth_Exe += atol(apTokens[R_CHURCHEXEMPTION]);
      lOth_Exe += atol(apTokens[R_RELIGIOUSEXEMPTION]);
      lOth_Exe += atol(apTokens[R_CEMETERYEXEMPTION]);
      lOth_Exe += atol(apTokens[R_PUBLICSCHOOLEXEMPTION]);
      lOth_Exe += atol(apTokens[R_PUBLICLIBRARYEXEMPTION]);
      lOth_Exe += atol(apTokens[R_PUBLICMUSEUMEXEMPTION]);
      lOth_Exe += atol(apTokens[R_WELFARECOLLEGEEXEMPTION]);
      lOth_Exe += atol(apTokens[R_WELFAREHOSPITALEXEMPTION]);
      lOth_Exe += atol(apTokens[R_WELFAREPRIVATEPAROCHIALSCHOOLEXEMPTION]);
      lOth_Exe += atol(apTokens[R_WELFARECHARITYRELIGIOUSEXEMPTION]);
      lOth_Exe += atol(apTokens[R_OTHEREXEMPTION]);
      lTmp = lOth_Exe+lHO_Exe;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }  
   }

   if (!_memicmp(apTokens[R_CLASSDESCRIPTION], "Government", 5))
      *(pOutbuf+OFF_PUBL_FLG) = 'Y';

   // TRA
   iTmp = atol(apTokens[R_TRA]);
   sprintf(acTmp, "%.6u", iTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, 6);

   // Is Taxable
   if (*apTokens[R_ISTAXABLE] == 'N')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Legal
   if (*apTokens[R_ASSESSMENTDESCRIPTION] > ' ')
   {
      blankRem(apTokens[R_ASSESSMENTDESCRIPTION]);
      iTmp = updateLegal(pOutbuf, _strupr(apTokens[R_ASSESSMENTDESCRIPTION]));
      if (iTmp > iMaxLegal)
         iMaxLegal = iTmp;
   }

   // Populate per bulk customer request 07/20/2022
   lTmp = atol(apTokens[R_TENPERCENTASSESSEDPENALTY]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LATE_PENALTY, lTmp);
      memcpy(pOutbuf+OFF_LATE_PENALTY, acTmp, SIZ_LATE_PENALTY);
   }

   // UseCode 
   removeUseCode(pOutbuf);
   if (*apTokens[R_CLASSCODE] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, _strupr(apTokens[R_CLASSCODE]), SIZ_USE_CO);
      if (acTmp[0] >= '0')
      {
#ifdef _DEBUG
         //if (!memcmp(apTokens[R_APN], "010160004000", 12) )
         //   iTmp = 0;
#endif

         strcpy(acTmp, myTrim(apTokens[R_CLASSCODE]));         
         iRet = updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 3, pOutbuf);
         if (!iRet)
         {
            LogMsg("*** Unknown UseCode: %s [%s]", apTokens[R_CLASSCODE], apTokens[R_APN]);
         }
      } else
         memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
   }

   // Acres
   dTmp = atof(apTokens[R_ACREAGEAMOUNT]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

#ifdef _DEBUG
   //if (!memcmp(acApn, "009000009", 9) )
   //   iTmp = 0;
#endif

   // Owner
   try {
      Sut_MergeOwner(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Sut_MergeOwner()");
   }

   // Situs
   Sut_MergeSAdr(pOutbuf);

   // Mailing
   Sut_MergeMAdr(pOutbuf);

   return 0;
}

/****************************** Sut_Load_RollCsv ******************************
 *
 * Load updated roll file AsmtRoll.csv (10/06/2019)
 *
 ******************************************************************************/

int Sut_Load_RollCsv(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acLastApn[32], acApn[32];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acSrtFile[_MAX_PATH], *pTmp;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0, lCnt=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   HANDLE   fhIn, fhOut;

   LogMsg0("Loading roll update");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Check for Unicode
   //sprintf(acBuf, "%s\\%s\\%s_Roll.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (!UnicodeToAnsi(acRollFile, acBuf))
   //   strcpy(acRollFile, acBuf);

   // Fix broken records in roll file
   //sprintf(acBuf, "%s\\%s\\%s_roll.fix", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   //iRet = RebuildCsv(acRollFile, acBuf, cDelim, R_COLS);
   //lLastFileDate = getFileDate(acRollFile);

   // Sort input
   sprintf(acSrtFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   // Sort on APN ASC, LOTACRES DESC and omit APN > 999 or unsec parcels
   sprintf(acRec, "S(#1,C,A) OMIT(#1,C,GT,\"A\") DUPOUT(B8192,#1) DEL(%d) F(TXT)", cDelim);
   iTmp = sortFile(acRollFile, acSrtFile, acRec);

   // Open roll file
   LogMsg("Open Roll file %s", acSrtFile);
   fdRoll = fopen(acSrtFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acSrtFile);
      return 2;
   }

   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);    
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   strcpy(acLastApn, "              ");
   lSec=lPos = 0;

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(&acRollRec[iSkipQuote], "055240007000", 12) )
      //   iTmp = 0;
#endif

      memcpy(acApn, acRollRec, 14);
      acApn[14] = 0;
      if (pTmp = strchr(acApn, cDelim))
      {
         *pTmp = ' ';
         *(pTmp+1) = 0;
      }

      iTmp = memcmp(acBuf, acApn, iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Sut_MergeRollCsv(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Char
            if (fdChar)
               iTmp = Sut_MergeStdChar(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         // Create new R01 record
         iRet = Sut_MergeRollCsv(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            if (bDebug)
               LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

            // Merge Char
            if (fdChar)
               iTmp = Sut_MergeStdChar(acRec);

            if (memcmp(acLastApn, acRec, iApnLen))
            {
               memcpy(acLastApn, acRec, iApnLen);
               bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
               iNewRec++;
            } else
            {
               LogMsg("---> Duplicate APN: %.12s", acLastApn);
            }
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("Retired record? : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, (char *)&acRollRec[iSkipQuote], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         if (memcmp(acLastApn, acBuf, iApnLen))
         {
            memcpy(acLastApn, acBuf, iApnLen);
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            if (!bRet)
            {
               LogMsg("Error occurs: %d\n", GetLastError());
               break;
            }
         }
      }
   }

   // Do the rest of the file
   while (!bEof && isdigit(acRollRec[0]))
   {
      // Create new R01 record
      iRet = Sut_MergeRollCsv(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Merge Char
         if (fdChar)
            iRet = Sut_MergeStdChar(acRec);

         if (memcmp(acLastApn, acRec, iApnLen))
         {
            memcpy(acLastApn, acRec, iApnLen);
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            iNewRec++;
         } else
         {
            LogMsg("---> Duplicate APN: %.12s", acLastApn);
         }
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Number of records processed: %u", lCnt);
   LogMsg("                new records: %u", iNewRec);
   LogMsg("            updated records: %u", iRollUpd);
   LogMsg("            retired records: %u\n", iRetiredRec);
   LogMsg("           bad-city records: %u", iBadCity);
   LogMsg("         bad-suffix records: %u\n", iBadSuffix);

   LogMsg("             unsecured recs: %u", lSec);
   LogMsg("     possessory or SBE recs: %u\n", lPos);

   LogMsg("Number of Char matched:      %u", lCharMatch);
   LogMsg("Number of Char skiped:       %u\n", lCharSkip);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/****************************** Sut_MergeOthers ******************************
 *
 * Update FULL_EXE_FLG, LEGAL, USE_CODE using update roll file.
 *
 * Return 0 if successful, < 0 if error
 *
 *****************************************************************************/

int Sut_MergeOthers(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   int      iRet, iTmp;

   // Check for bad char
   Sut_ReplBadChar((unsigned char *)pRollRec);

   // Remove extra spaces
   blankRem(pRollRec);

   // Parse roll record
   if (cDelim == ',')
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < R_COLS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[R_APN]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[R_APN], "008040022", 9) )
   //   iTmp = 0;
#endif

   // Is Taxable
   if (*(pOutbuf+OFF_FULL_EXEMPT) == ' ' &&  *apTokens[R_ISTAXABLE] == 'N')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Legal
   if (*(pOutbuf+OFF_LEGAL) == ' ' && *apTokens[R_ASSESSMENTDESCRIPTION] > ' ')
   {
      iTmp = updateLegal(pOutbuf, _strupr(apTokens[R_ASSESSMENTDESCRIPTION]));
      if (iTmp > iMaxLegal)
         iMaxLegal = iTmp;
   }

   // UseCode 
   if (*(pOutbuf+OFF_USE_CO) == ' ' && *apTokens[R_CLASSCODE] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[R_CLASSCODE], SIZ_USE_CO);
      strcpy(acTmp, apTokens[R_CLASSCODE]);
      acTmp[3] = 0;
      iRet = updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 3, pOutbuf);
      if (!iRet && bDebug)
         LogMsg("*** Unknown UseCode: %s, APN:%.12s", acTmp, pOutbuf);
   }

   return 0;
}

/****************************** Sut_LoadOthers ********************************
 *
 * Use CA-Sutter-AsmtRoll.csv to update Legal & UseCode.  Use this function when
 * LDR file doesn't include those fields.
 *
 ******************************************************************************/

int Sut_LoadOthers(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE],
            acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acSrtFile[_MAX_PATH], *pTmp;
   int      iRet, iTmp, iRollUpd=0, lCnt=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   HANDLE   fhIn, fhOut;

   LogMsg0("Loading roll update to populate LDR missing fields");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "X01");
   GetIniString(myCounty.acCntyCode, "Rollfile", "", acRollFile, _MAX_PATH, acIniFile);

   // Check for Unicode
   sprintf(acBuf, "%s\\%s\\%s_Roll.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!UnicodeToAnsi(acRollFile, acBuf))
      strcpy(acRollFile, acBuf);

   // Sort input
   sprintf(acSrtFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   //sprintf(acRec, "S(#1,C,A,#35,C,D) DUPOUT(B8192,#1) DEL(%d) F(TXT)", cDelim);
   sprintf(acRec, "S(#1,C,A) F(TXT) DEL(%d) ", cDelim);
   iTmp = sortFile(acRollFile, acSrtFile, acRec);

   // Open roll file
   LogMsg("Open Roll file %s", acSrtFile);
   fdRoll = fopen(acSrtFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acSrtFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);    
   bEof = (pTmp ? false:true);

   // Copy skip record
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(&acRollRec[iSkipQuote], "006050067000", 9) )
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, &acRollRec[iSkipQuote], iApnLen-1);
      //iTmp = memcmp(acBuf, &acRollRec[iSkipQuote]+1, iApnLen-2);
      if (!iTmp)
      {
         // Update Legal & UseCode
         iRet = Sut_MergeOthers(acBuf, acRollRec);
         if (!iRet)
         {
            iRollUpd++;
         }

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      } else if (iTmp > 0)
      {
         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         goto NextRollRec;
      } else
      {
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!pTmp)
         bEof = true;         // Signal to stop
   }

   // Do the rest of the file
   while (iRecLen == nBytesRead)
   {
      lCnt++;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhIn)
      CloseHandle(fhIn);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:      %u", lCnt);
   LogMsg("      updated records:     %u\n", iRollUpd);

   sprintf(acSrtFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "N01");
   if (!_access(acSrtFile, 0))
      DeleteFile(acSrtFile);

   // Rename output file
   rename(acRawFile, acSrtFile);
   LogMsg("Rename %s to %s", acOutFile, acRawFile);
   iRet = rename(acOutFile, acRawFile);

   return iRet;
}

/*************************** Sut_CreateTaxItems() *****************************
 *
 * Parse current detail tax bill to update TaxBase and create TaxItems and TaxAgency
 *
 ******************************************************************************/

int Sut_CreateTaxItems(char *pTaxBase, FILE *fdDetail, FILE *fdItems, FILE *fdAgency)
{
   static   char acRec[1024], *apItems[16];
   static   long lPos=0;

   int      iRet, iTmp, iItems;
   double   dTaxRate;
   char     acTmp[1024], acDetail[1024], acAgency[1024], *pTmp;

   TAXBASE   *pOutBase   = (TAXBASE *)pTaxBase;
   TAXDETAIL *pOutDetail = (TAXDETAIL *)acDetail;
   TAXAGENCY *pOutAgency = (TAXAGENCY *)acAgency, *pResult;

#ifdef _DEBUG
   //if (!memcmp(pOutBase->Assmnt_No, "44257021", 8))
   //   iTmp = 0;
#endif
   
   dTaxRate = 0;
   iRet = 1;
   while (!feof(fdDetail))
   {
      // Save current position
      lPos = ftell(fdDetail);
      pTmp = fgets(acRec, 1024, fdDetail);
      if (!pTmp || *pTmp > '9')
         break;
     
      // Parse detail rec
      iItems = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apItems);
      if (iItems < CD_COLS)
      {
         LogMsg("***** Error: bad Tax record for APN=%s (#tokens=%d)", acRec, iItems);
         return -1;
      } else if (iItems > CD_COLS)
      {
         LogMsg("*** WARNING: Too many tokens APN=%s (#tokens=%d).", acRec, iItems);
      }

#ifdef _DEBUG
      // 020157000198
      //if (!memcmp(apItems[CD_TAXBILLID], "386088", 6) )
      // 006183011000
      //if (!memcmp(apItems[CD_TAXBILLID], "391382", 6) )
      //   iRet = 0;
#endif

      iTmp = strcmp(pOutBase->Assmnt_No, myTrim(apItems[CD_TAXBILLID]));
      if (!iTmp)
      {
         // Ignore unsecured records
         if (strstr(apItems[CD_BILLTYPE], "Unsec"))
         {
            pOutBase->BillType[0] = BILLTYPE_UNSECURED;
            continue;
         }

         // Ignore records with no tax amount
         if (*apItems[CD_TOTAL] == '0')
         {
            continue;
         }

         memset(acAgency, 0, sizeof(TAXAGENCY));
         memset(acDetail, 0, sizeof(TAXDETAIL));

         iTmp = iTrim(apItems[CA_PIN]);
         if (iTmp >= 12)
            strcpy(pOutDetail->Apn, apItems[CA_PIN]);
         else if (iTmp < 12)
            sprintf(pOutDetail->Apn, "%.*s%s", iApnLen-iTmp, "0000", apItems[CA_PIN]);
         else 
            vmemcpy(pOutDetail->Apn, apItems[CA_PIN], iApnLen);

         //iRet = Sut_FormatPrevApn(apItems[CD_PIN], pOutDetail->Apn);
         //if (iRet != 12)
         //{
         //   LogMsg("*** Invalid APN: %s", apItems[CD_PIN]);
         //   continue;
         //}
         
         strcpy(pOutDetail->Assmnt_No, apItems[CD_TAXBILLID]);
         strcpy(pOutDetail->TaxYear, apItems[CD_TAXYEAR]);
         strcpy(pOutDetail->BillNum, myTrim(apItems[CD_BILLNUMBER]));
         strcpy(pOutDetail->TaxRate, apItems[CD_RATE]);
         strcpy(pOutDetail->TaxAmt, apItems[CD_TOTAL]);
         strcpy(pOutDetail->TaxDesc, myTrim(apItems[CD_DETAILS]));
         vmemcpy(pOutDetail->TaxCode, myTrim(apItems[CD_TAXCODE]), 10);
         //strcpy(pOutDetail->TaxCode, myTrim(apItems[CD_TAXCODE]));
         dTaxRate += atof(pOutDetail->TaxRate);

         // Update TaxBase
         if (!pOutBase->TaxYear[0])
         {
            // pOutBase contains new APN.  We need old APN for current tax roll.
            if (strcmp(pOutBase->Apn, pOutDetail->Apn))
            {
               LogMsg("*** APN mismatched BillID=%s Base->PIN=%s ... Detail->PIN=%s", pOutDetail->BillNum, pOutBase->Apn, pOutDetail->Apn);
               strcpy(pOutBase->Apn, pOutDetail->Apn);
               strcpy(pOutBase->BillNum, pOutDetail->BillNum);
            }

            // Update Tax Year, TRA
            strcpy(pOutBase->TaxYear, pOutDetail->TaxYear);
            iTmp = atol(apItems[CD_TAXRATEAREA]);
            sprintf(pOutBase->TRA, "%.6d", iTmp);

            // Update bill type
            if (!_memicmp(apItems[CD_BILLTYPE], "Sup", 3))
            {
               pOutBase->isSupp[0] = '1';
               pOutBase->isSecd[0] = '0';
               pOutBase->BillType[0] = BILLTYPE_SECURED_SUPPL;
            }
         }

         // Tax code
         if (iNumTaxDist > 1)
         {
            pResult = findTaxAgency(pOutDetail->TaxCode, 0);
            if (pResult)
            {
               strcpy(pOutAgency->Agency, pResult->Agency);
               strcpy(pOutAgency->Code, pResult->Code);
               strcpy(pOutAgency->Phone, pResult->Phone);
               pOutAgency->TC_Flag[0] = pResult->TC_Flag[0];
               pOutDetail->TC_Flag[0] = pResult->TC_Flag[0];
            } else
            {
               strcpy(pOutAgency->Agency, pOutDetail->TaxDesc);
               strcpy(pOutAgency->Code, pOutDetail->TaxCode);
               pOutAgency->Phone[0] = 0;
               pOutAgency->TC_Flag[0] = '0';
               pOutDetail->TC_Flag[0] = '0';
               LogMsg("+++ New Agency: %s:%s [%s]", pOutDetail->TaxCode, pOutDetail->TaxDesc, pOutDetail->Apn);
            }
         }

         // Output detail record
         Tax_CreateDetailCsv(acTmp, (TAXDETAIL *)&acDetail);
         fputs(acTmp, fdItems);

         Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgency);
         fputs(acTmp, fdAgency);
      } else if (iTmp > 0)
      {
         continue;
      } else
      {
         fseek(fdDetail, lPos, SEEK_SET);
         break;
      }
   }

   // Update TaxBase
   sprintf(pOutBase->TotalRate, "%.2f", dTaxRate);

   return iRet;
}

/*************************** Sut_CreateTaxItems() *****************************
 *
 * Parse current detail tax bill to update TaxBase
 *
 ******************************************************************************/

int Sut_Update_BillType(char *pTaxBase, FILE *fdDetail)
{
   static   char acRec[1024], *apItems[16];
   static   long lPos=0;

   int      iRet, iTmp, iItems;
   char     acDetail[1024], *pTmp;

   TAXBASE   *pOutBase   = (TAXBASE *)pTaxBase;
   TAXDETAIL *pOutDetail = (TAXDETAIL *)acDetail;

#ifdef _DEBUG
   //if (!memcmp(pOutBase->Assmnt_No, "44257021", 8))
   //   iTmp = 0;
#endif
   
   iRet = 1;
   while (!feof(fdDetail))
   {
      // Save current position
      lPos = ftell(fdDetail);
      pTmp = fgets(acRec, 1024, fdDetail);
      if (!pTmp || *pTmp > '9')
         break;
     
      // Parse detail rec
      iItems = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apItems);
      if (iItems < CD_COLS)
      {
         LogMsg("***** Error: bad Tax record for APN=%s (#tokens=%d)", acRec, iItems);
         return -1;
      } else if (iItems > CD_COLS)
      {
         LogMsg("*** WARNING: Too many tokens APN=%s (#tokens=%d).", acRec, iItems);
      }

#ifdef _DEBUG
      // 006183011000
      //if (!memcmp(apItems[CD_TAXBILLID], "391382", 6) )
      //   iRet = 0;
#endif

      iTmp = strcmp(pOutBase->Assmnt_No, myTrim(apItems[CD_TAXBILLID]));
      if (!iTmp)
      {
         // Ignore unsecured records
         if (strstr(apItems[CD_BILLTYPE], "Unsec"))
         {
            pOutBase->BillType[0] = BILLTYPE_UNSECURED;
            continue;
         }

         // Ignore records with no tax amount
         if (*apItems[CD_TOTAL] == '0')
         {
            continue;
         }

         iTmp = iTrim(apItems[CA_PIN]);
         if (iTmp != 12)
            continue;

         // Update bill type
         if (!_memicmp(apItems[CD_BILLTYPE], "Sup", 3))
         {
            pOutBase->isSupp[0] = '1';
            pOutBase->isSecd[0] = '0';
            pOutBase->BillType[0] = BILLTYPE_SECURED_SUPPL;
         }
      } else if (iTmp > 0)
      {
         continue;
      } else
      {
         fseek(fdDetail, lPos, SEEK_SET);
         break;
      }
   }

   return iRet;
}

/*************************** Sut_Load_TaxCurrent ******************************
 *
 * Load CA-Inyo-CurrentAmounts.csv & CA-Inyo-CurrentDistricts.csv
 * Load current tax files and create Base, Items, & Agency import.
 *
 ******************************************************************************/

int Sut_Load_TaxCurrent(char *pCurrentTaxFile, char *pDetailFile, bool bImport)
{
   char     *pTmp, acOutbuf[1024], acRec[1024], acCurrent[_MAX_PATH], acDetail[_MAX_PATH], acTmp[256],
            sBase[1024], sAgencyFile[_MAX_PATH], sBaseFile[_MAX_PATH], sItemsFile[_MAX_PATH], sTmpFile[_MAX_PATH];
   long     iTmp, iRet, lOut=0, lCnt=0, lDrop=0, lBad=0, lNoTax=0;
   double	dTax1, dTax2, dPen1, dPen2, dFee1, dFee2, dInt1, dInt2, dTaxDue1, dTaxDue2,
   	      dPaid1, dPaid2, dPaidPen1, dPaidPen2, dPaidFee1, dPaidFee2, dPaidInt1, dPaidInt2, 
            dTotalTax, dTotalDue, dTotalFees;

   TAXBASE  *pOutBase = (TAXBASE *)sBase;
   FILE     *fdCurrent, *fdDetail, *fdBase, *fdItems, *fdAgency;

   LogMsg0("Loading current tax file");

   // Check for Unicode
   //sprintf(sTmpFile, "%s\\%s\\CurrentAmounts.txt", acTmpPath, myCounty.acCntyCode);
   //if (!UnicodeToAnsi(pCurrentTaxFile, sTmpFile))
   //   strcpy(pCurrentTaxFile, sTmpFile);

   // Sort current tax file on TaxBillId
   sprintf(acCurrent, "%s\\%s\\Sut_Current.txt", acTmpPath, myCounty.acCntyCode);
   sprintf(acRec, "S(#3,C,A,#1,C,A) DEL(%d) OMIT(#1,C,GT,\"9999\")", cDelim);
   iRet = sortFile(pCurrentTaxFile, acCurrent, acRec);
   if (!iRet)
      return -1;

   // Check for Unicode
   //sprintf(sTmpFile, "%s\\%s\\CurrentDistricts.txt", acTmpPath, myCounty.acCntyCode);
   //if (!UnicodeToAnsi(pDetailFile, sTmpFile))
   //   strcpy(pDetailFile, sTmpFile);

   // Sort detail file on TaxBillId & PIN
   sprintf(acDetail, "%s\\%s\\Sut_Detail.txt", acTmpPath, myCounty.acCntyCode);
   sprintf(acRec,  "S(#6,C,A,#1,C,A) DEL(%d) OMIT(#1,C,GT,\"9999\")", cDelim);
   iRet = sortFile(pDetailFile, acDetail, acRec);
   if (!iRet)
      return -1;

   // Open current tax file
   LogMsg("Open current tax file %s", acCurrent);
   fdCurrent = fopen(acCurrent, "r");
   if (fdCurrent == NULL)
   {
      LogMsg("***** Error opening current tax file: %s\n", acCurrent);
      return -2;
   }  

   // Open detail file
   LogMsg("Open tax detail file %s", acDetail);
   fdDetail = fopen(acDetail, "r");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error opening tax Detail file: %s\n", acDetail);
      return -2;
   }  

   // Create output file names
   NameTaxCsvFile(sBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(sItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(sAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(sTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   LogMsg("Create TaxBase file %s", sBaseFile);
   fdBase = fopen(sBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating TaxBase file: %s\n", sBaseFile);
      return -4;
   }

   LogMsg("Create TaxItems file %s", sItemsFile);
   fdItems = fopen(sItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating TaxItems file: %s\n", sItemsFile);
      return -4;
   }

   LogMsg("Create TaxAgency file %s", sTmpFile);
   fdAgency = fopen(sTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating TaxAgency file: %s\n", sTmpFile);
      return -4;
   }

   // Init variables
   iNewAgency = 0;
   while (!feof(fdCurrent))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdCurrent);
      if (!pTmp || *pTmp > '9')
         break;

      // Parse input tax data
      iTokens = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < CA_COLS)
      {
         LogMsg("***** Error: bad Tax bill record for taxbillid=%s (#tokens=%d)", acRec, iTokens);
         return -1;
      }

      // Reset buffer
      memset(sBase, 0, sizeof(TAXBASE));

      // Populate TaxBase
      strcpy(pOutBase->Assmnt_No, apTokens[CA_TAXBILLID]);
      //iRet = Sut_FormatPrevApn(myTrim(apTokens[CA_PIN]), acTmp);

      iTmp = iTrim(apTokens[CA_PIN]);
      if (iTmp >= 12)
         strcpy(acTmp, apTokens[CA_PIN]);
      else if (iTmp < 12)
         sprintf(acTmp, "%.*s%s", iApnLen-iTmp, "0000", apTokens[CA_PIN]);
      strcpy(pOutBase->Apn, acTmp);
      strcpy(pOutBase->BillNum, myTrim(apTokens[CA_BILLNUMBER]));

#ifdef _DEBUG
      //if (!memcmp(pOutBase->Assmnt_No, "406809", 6))
      //   iTmp = 0;
      //if (!memcmp(pOutBase->Apn, "006232003000", 10))
      //   iTmp = 0;
#endif

      // Tax year
      //memcpy(pOutBase->TaxYear, pOutBase->BillNum, 4);

      // Due Date - there is no paid date
      pTmp = dateConversion(apTokens[CA_INST1DUEDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
         strcpy(pOutBase->DueDate1, acTmp);
      pTmp = dateConversion(apTokens[CA_INST2DUEDATE], acTmp, YYYY_MM_DD, lLienYear+1);
      if (pTmp)
         strcpy(pOutBase->DueDate2, acTmp);

      // 1st inst charge/paid/due
      strcpy(pOutBase->TaxAmt1, apTokens[CA_INST1TAXCHARGE]);
      if (*apTokens[CA_INST1TAXPAYMENT] == '-')
         strcpy(pOutBase->PaidAmt1, apTokens[CA_INST1TAXPAYMENT]+1);
      else
         strcpy(pOutBase->PaidAmt1, apTokens[CA_INST1TAXPAYMENT]);
      dTax1 = atof(apTokens[CA_INST1TAXCHARGE]);
      dPaid1 = atof(pOutBase->PaidAmt1);
      dTaxDue1 = atof(apTokens[CA_INST1AMOUNTDUE]);

      // 1st penalty/fee/int
      dPen1 = atof(apTokens[CA_INST1PENALTYCHARGE]);
      strcpy(pOutBase->PenAmt1, apTokens[CA_INST1PENALTYCHARGE]);
      dFee1 = atof(apTokens[CA_INST1FEECHARGE]);
      dInt1 = atof(apTokens[CA_INST1INTERESTCHARGE]);

      dPaidPen1=dPaidFee1=dPaidInt1 = 0;
      if (*apTokens[CA_INST1PENALTYPAYMENT] == '-')
         dPaidPen1 = atof(apTokens[CA_INST1PENALTYPAYMENT]+1);
      if (*apTokens[CA_INST1FEEPAYMENT] == '-')
         dPaidFee1 = atof(apTokens[CA_INST1FEEPAYMENT]+1);
      if (*apTokens[CA_INST1INTERESTPAYMENT] == '-')
         dPaidInt1 = atof(apTokens[CA_INST1INTERESTPAYMENT]+1);

      // 2nd inst charge/paid/due
      strcpy(pOutBase->TaxAmt2, apTokens[CA_INST2TAXCHARGE]);
      if (*apTokens[CA_INST2TAXPAYMENT] == '-')
         strcpy(pOutBase->PaidAmt2, apTokens[CA_INST2TAXPAYMENT]+1);
      else
         strcpy(pOutBase->PaidAmt2, apTokens[CA_INST2TAXPAYMENT]);
      dTax2 = atof(apTokens[CA_INST2TAXCHARGE]);
      dPaid2 = atof(pOutBase->PaidAmt2);
      dTaxDue2 = atof(apTokens[CA_INST2AMOUNTDUE]);

      // 2nd penalty/fee/int
      dPen2 = atof(apTokens[CA_INST2PENALTYCHARGE]);
      strcpy(pOutBase->PenAmt2, apTokens[CA_INST2PENALTYCHARGE]);
      dFee2 = atof(apTokens[CA_INST2FEECHARGE]);
      dInt2 = atof(apTokens[CA_INST2INTERESTCHARGE]);
      dPaidPen2=dPaidFee2=dPaidInt2 = 0;
      if (*apTokens[CA_INST2PENALTYPAYMENT] == '-')
         dPaidPen2 = atof(apTokens[CA_INST2PENALTYPAYMENT]+1);
      if (*apTokens[CA_INST2FEEPAYMENT] == '-')
         dPaidFee2 = atof(apTokens[CA_INST2FEEPAYMENT]+1);
      if (*apTokens[CA_INST2INTERESTPAYMENT] == '-')
         dPaidInt2 = atof(apTokens[CA_INST2INTERESTPAYMENT]+1);

      // TaxAmt
      dTotalTax = dTax1+dTax2;
      pOutBase->dTotalTax = dTotalTax;
      sprintf(pOutBase->TotalTaxAmt, "%.2f", dTotalTax);

      dTotalDue = dTaxDue1+dTaxDue2;
      sprintf(pOutBase->TotalDue, "%.2f", dTotalDue);

      dTotalFees = dFee1+dFee2;
      sprintf(pOutBase->TotalFees, "%.2f", dTotalFees);

      // Paid status
      if (dPaid1 >= dTax1)
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      else if (!dTax1)
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else if (dPaid1 < dTax1)
      {
         if (dPen1 > 0)
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      if (dPaid2 >= dTax2)
         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
      else if (!dTax2)
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else if (dPaid2 < dTax2)
      {
         if (dPen2 > 0)
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Set default - these values can be updated in Sut_CreateTaxItems
      pOutBase->isSecd[0] = '1';
      pOutBase->isSupp[0] = '0';
      pOutBase->BillType[0] = BILLTYPE_SECURED;

      // Create tax detail record
      iRet = Sut_CreateTaxItems(sBase, fdDetail, fdItems, fdAgency);

      // BillType: 
      // CSA,OSA: Secured
      // XSA,ZSA: Possessory Interest
      // OSP: Secured supplemental
      // CSP: Secured - change
      // LSP,NSP: Unknown - no tax bill
      /* Remove 11/01/2022 sonce new BILLNUMBER format has been changed
      if (!memcmp(apTokens[CA_BILLNUMBER]+5, "OSA", 3) ||
          !memcmp(apTokens[CA_BILLNUMBER]+5, "CSA", 3))
      {
         pOutBase->isSecd[0] = '1';
         pOutBase->isSupp[0] = '0';
         pOutBase->BillType[0] = BILLTYPE_SECURED;

         // Create detail
         iRet = Sut_CreateTaxItems(sBase, fdDetail, fdItems, fdAgency);
         if (iRet == 1)
         {
            lNoTax++;
            lBad++;
            LogMsg("--- No Detail, taxbillid=%s, APN=%s", pOutBase->Assmnt_No, pOutBase->Apn);
         }
      } else if (!memcmp(apTokens[CA_BILLNUMBER]+5, "OSP", 3))
      {
         // Supplemental bill does not have detail info.  We can skip creating tax items.
         pOutBase->isSecd[0] = '0';
         pOutBase->isSupp[0] = '1';
         pOutBase->BillType[0] = BILLTYPE_SECURED_SUPPL;
         sprintf(pOutBase->TaxYear, "20%.2s", apTokens[CA_BILLNUMBER]);
      } else
      {
         if (bDebug)
            LogMsg("*** Ignore BillNum=%s APN=%s", apTokens[CA_BILLNUMBER], pOutBase->Apn);
         continue;
      }
      */

      if (pOutBase->BillType[0] >= BILLTYPE_UNSECURED && pOutBase->BillType[0] <= BILLTYPE_UNSECURED_INT_OWNER)
      {
         LogMsg("*** Drop unsecured record: BillNum=%s", pOutBase->BillNum);
         lDrop++;
      } else if (pOutBase->dTotalTax == 0)
      {
         LogMsg("*** Drop non-tax record: BillNum=%s", pOutBase->BillNum);
         lDrop++;
      } else
      if (iRet >= 0)
      {
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&sBase);
         fputs(acOutbuf, fdBase);

         lOut++;
      } else
         lBad++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdBase);
   fclose(fdItems);
   fclose(fdAgency);
   fclose(fdCurrent);
   fclose(fdDetail);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("         output records:    %u", lOut);
   LogMsg("           drop records:    %u", lDrop);
   LogMsg("            bad records:    %u", lBad);
   LogMsg("      no detail records:    %u", lNoTax);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", sTmpFile, sAgencyFile);
         iRet = sortFile(sTmpFile, sAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/**************************** Sut_Load_TaxPriorYear *************************
 *
 * Load CA-Inyo-PriorYear.csv
 *
 ****************************************************************************/

int Sut_Load_TaxPriorYear(char *pDelqFile, bool bImport)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutBuf[2048];
   char     acTmpFile[_MAX_PATH];

   int      iRet, lCnt=0;
   FILE     *fdIn, *fdOut;
   TAXDELQ  *pTaxDelq = (TAXDELQ *)&acBuf[0];
   double   dTotalTax;

   LogMsg0("Loading tax prior year file");

   // Check for Unicode
   //sprintf(acTmpFile, "%s\\%s\\PriorYear.txt", acTmpPath, myCounty.acCntyCode);
   //if (!UnicodeToAnsi(pDelqFile, acTmpFile))
   //   strcpy(pDelqFile, acTmpFile);

   sprintf(acTmpFile, "%s\\%s\\Sut_Delq.txt", acTmpPath, myCounty.acCntyCode);
   iRet = sortFile(pDelqFile, acTmpFile, "S(#1,C,A,#3,C,A) DEL(44) OMIT(#4,C,GE,\"U\",OR,#1,C,GT,\"9999\") ");
   if (!iRet)
      return -1;

   // Open input file
   LogMsg("Open tax redemption file %s", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax redemption file: %s\n", acTmpFile);
      return -2;
   }  

   // Create output file
   NameTaxCsvFile(acTmpFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open Delq file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Delq file: %s\n", acTmpFile);
      return -4;
   }

   // Clear output buffer
   memset(acBuf, 0, sizeof(TAXDELQ));
   // Updated date
   sprintf(pTaxDelq->Upd_Date, "%d", lLastTaxFileDate);

   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp > '9')
         break;

      // Bypass BPP records
      if (acRec[10] == 'B' || acRec[1] < '0')
         continue;

      // Parse input record
      iTokens = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < PY_COLS)
      {
         LogMsg("***** Error: bad delq record for APN=%s (#tokens=%d)", acRec, iTokens);
         break;
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[PY_PIN], "729190079010", 9) )
      //   iRet = 0;
#endif

      dTotalTax = atof(apTokens[PY_AMOUNTDUE]);
      if (dTotalTax > 0.0)
      {
         // APN
         //iRet = Sut_FormatPrevApn(myTrim(apTokens[PY_PIN]), pTaxDelq->Apn);
         iRet = iTrim(apTokens[PY_PIN]);
         if (iRet >= 12)
            strcpy(pTaxDelq->Apn, apTokens[PY_PIN]);
         else if (iRet < 12)
            sprintf(pTaxDelq->Apn, "%.*s%s", 12, "0000", apTokens[PY_PIN]);

         // Default Number
         strcpy(pTaxDelq->Default_No, myTrim(apTokens[PY_DEFAULTNUMBER]));
         strcpy(pTaxDelq->TaxYear, apTokens[PY_DEFAULTYEAR]);

         // Default Amt - When Fee & Pen are added to Delq table, we can break DefAmt to separate fields
         sprintf(pTaxDelq->Def_Amt, "%.2f", dTotalTax);
         pTaxDelq->isDelq[0] = '1';

         if (!_memicmp(apTokens[PY_BILLTYPE], "Secured Annual", 10))
         {
            pTaxDelq->isSecd[0] = '1';
            pTaxDelq->isSupp[0] = '0';
         } else if (!_memicmp(apTokens[PY_BILLTYPE], "Secured Supp", 10))
         {
            pTaxDelq->isSecd[0] = '0';
            pTaxDelq->isSupp[0] = '1';
         } else
         {
            pTaxDelq->isSecd[0] = '0';
            pTaxDelq->isSupp[0] = '0';
         }

         // Output delq record for every year
         Tax_CreateDelqCsv(acOutBuf, (TAXDELQ *)acBuf);
         fputs(acOutBuf, fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fdOut)
      fclose(fdOut);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total Delq records processed:    %u", lCnt);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

/***************************** Sut_ParseTaxDetail ****************************
 * 
 * Use Assessment Detail file to create Items, Agency
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Sut_ParseTaxDetail(char *pDetailbuf, char *pAgencybuf, char *pInbuf)
{
   double   dTax;

   SUT_DETAIL *pInRec  = (SUT_DETAIL *)pInbuf;
   TAXDETAIL  *pDetail = (TAXDETAIL *)pDetailbuf;
   TAXAGENCY  *pAgency = (TAXAGENCY *)pAgencybuf, *pResult;

   // Clear output buffer
   memset(pDetailbuf, 0, sizeof(TAXDETAIL));
   memset(pAgencybuf, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(pDetail->Apn, pInRec->Apn, iApnLen);
   myTrim(pDetail->Apn, iApnLen);

   // Tax Year
   memcpy(pDetail->TaxYear, pInRec->TaxYear, 4);

   // Bill Number
   memcpy(pDetail->BillNum, pInRec->BillNum, SIZ_TAD_BILLNUM);

#ifdef _DEBUG
   //if (!memcmp(pDetail->Apn, "101250018", 9) )
   //   iTmp = 0;
#endif

   // Tax Amount
   dTax  = dollar2Double(pInRec->TaxAmt);
   if (!dTax)
      return 1;
   sprintf(pDetail->TaxAmt, "%.2f", dTax);

   // Tax Code
   memcpy(pDetail->TaxCode, pInRec->AsmtCode, SIZ_TAD_ASMTCODE);
   memcpy(pAgency->Code, pInRec->AsmtCode, SIZ_TAD_ASMTCODE);
   dTax = dollar2Double(pInRec->TaxRate);
   if (dTax != 0.0)
      memcpy(pAgency->TaxRate, pInRec->TaxRate, SIZ_TAD_TAXRATE);
   // Search on sorted list start at 0
   pResult = findTaxAgency(pAgency->Code, 0);
   if (pResult)
   {
      strcpy(pAgency->Agency, pResult->Agency);
      strcpy(pAgency->Phone, pResult->Phone);
   } else
   {
      pAgency->Agency[0] = 0;
      LogMsg("+++ Unknown TaxCode: %s [%s]", pDetail->TaxCode, pDetail->Apn);
   }

   return 0;
}

int Sut_Load_TaxDetail(char *pInfile, bool bImport)
{
   char     *pTmp, acItemsRec[MAX_RECSIZE], acAgencyRec[512], acRec[512];
   char     acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0, lDrop=0;
   FILE     *fdItems, *fdAgency, *fdIn;
   TAXDETAIL *pTax = (TAXDETAIL *)&acItemsRec[0];

   LogMsg("Loading Detail file");

   // Open input file
   LogMsg("Open Detail file %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", pInfile);
      return -2;
   }  

   // Open Items file
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create Items & Agency record
      iRet = Sut_ParseTaxDetail(acItemsRec, acAgencyRec, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
         lItems++;
         fputs(acRec, fdItems);

         // Create Agency record
         Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
         fputs(acRec, fdAgency);
      } else
      {
         lDrop++;
         //LogMsg0("---> No tax amount, ignore detail record# %d [%.45s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("                dropped:    %u", lDrop);
   LogMsg("     Total Items output:    %u", lItems);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/***************************** Sut_ParseTaxBase ******************************
 *
 * Use Paid/Unpaid file to create Base, Delq, Owner
 * We don't want to create Delq record since it has only Default Date
 *
 *****************************************************************************/

int Sut_ParseTaxBase(char *pBasebuf, char *pInbuf)
{
   double   dTmp, dTotalTax, dTax1, dTax2, dPenalty, dFees, dInterest, dTotalDue, dTotalPaid;
   long     lTmp;

   TAXBASE  *pBaseRec = (TAXBASE *)pBasebuf;
   SUT_MASTER *pInRec = (SUT_MASTER *)pInbuf;

   // Clear output buffer
   memset(pBasebuf, 0, sizeof(TAXBASE));

   // APN
   memcpy(pBaseRec->Apn, pInRec->Apn, SIZ_TM_APN);
   myTrim(pBaseRec->Apn, SIZ_TM_APN);

#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "006232003000", 9))
   //   lTmp = 0;
#endif

   // Status default
   pBaseRec->isSecd[0] = '1';
   pBaseRec->BillType[0] = BILLTYPE_SECURED;
   pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   // BillNum
   memcpy(pBaseRec->BillNum, pInRec->BillNum, SIZ_TM_BILLNUM);

   // TRA
   lTmp = atoin(pInRec->TRA, SIZ_TM_TRA);
   if (lTmp > 0)
      sprintf(pBaseRec->TRA, "%.6d", lTmp);

   // Tax Year
   memcpy(pBaseRec->TaxYear, pInRec->TaxYear, 4);

   // Tax rate
   dTmp = atofn(pInRec->TaxRate, SIZ_TM_TAX_RATE, true);
   if (dTmp > 0.0)
      sprintf(pBaseRec->TotalRate, "%.6f", dTmp);

   // Check for Tax amount
   dTax1 = atofn(pInRec->Inst1_Amt, SIZ_TM_INST1_AMT, true);
   dTax2 = atofn(pInRec->Inst2_Amt, SIZ_TM_INST2_AMT, true);
   dTotalTax = atofn(pInRec->TotalTax, SIZ_TM_TOTALTAX, true);
   sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);
   sprintf(pBaseRec->TaxAmt1, "%.2f", dTax1);
   sprintf(pBaseRec->TaxAmt2, "%.2f", dTax2);

#ifdef _DEBUG
   //if ((long)dTotalTax != (long)(dTax1+dTax2))
   //   dTmp = dTax1+dTax2;
#endif

   memcpy(pBaseRec->DueDate1, pInRec->DueDate1, SIZ_TM_INST1_DUEDATE);
   memcpy(pBaseRec->DueDate2, pInRec->DueDate2, SIZ_TM_INST2_DUEDATE);

   dPenalty = atofn(pInRec->Penalty, SIZ_TM_PENALTY);
   dInterest = atofn(pInRec->Interest, SIZ_TM_INTEREST);
   dFees = atofn(pInRec->TotalFees, SIZ_TM_FEES);

   dTotalDue = atofn(pInRec->TotalDue, SIZ_TM_TOTALDUE, true);
   dTotalPaid = atofn(pInRec->TotalPaid, SIZ_TM_TOTALPAID, true);

   if (dTotalPaid >= dTotalTax)
   {
      // Both paid
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
      memcpy(pBaseRec->PaidDate1, pInRec->LastPaidDate, SIZ_TM_LAST_PAIDDATE);
      memcpy(pBaseRec->PaidDate2, pInRec->LastPaidDate, SIZ_TM_LAST_PAIDDATE);
      sprintf(pBaseRec->PaidAmt1, "%.2f", dTax1);
      sprintf(pBaseRec->PaidAmt2, "%.2f", dTax2);      
   } else if (dTotalPaid >= dTax1)
   {  // Inst1 paid
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
      memcpy(pBaseRec->PaidDate1, pInRec->LastPaidDate, SIZ_TM_LAST_PAIDDATE);
      sprintf(pBaseRec->PaidAmt1, "%.2f", dTax1);
      sprintf(pBaseRec->TotalDue, "%.2f", dTotalDue);
   } else
   {
      if (!dTax1)
         pBaseRec->Inst1Status[0] = TAX_BSTAT_NOTAX;
      if (!dTax2)
         pBaseRec->Inst2Status[0] = TAX_BSTAT_NOTAX;
      if (dTotalDue > 0.0)
      {
         sprintf(pBaseRec->TotalDue, "%.2f", dTotalDue);
         if (ChkDueDate(1, pBaseRec->DueDate1))
            pBaseRec->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         if (ChkDueDate(2, pBaseRec->DueDate2))
            pBaseRec->Inst2Status[0] = TAX_BSTAT_PASTDUE;
      }
   }

   return 0;
}

/**************************** Sut_Load_TaxBase *******************************
 *
 * Input:  SUTTER COUNTY FY 2023-24 SECURED TAX MASTER (TAX100P).TXT
 * Output: Tax Base
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sut_Load_TaxBase(char *pInfile, bool bImport)
{
   char     *pTmp, acBasebuf[1024], acRec[MAX_RECSIZE], acBaseFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdBase, *fdIn;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBasebuf[0];

   LogMsg("Loading Tax Base");

   // Open input file
   LogMsg("Open Tax master file %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Paid/Unpaid file: %s\n", pInfile);
      return -2;
   }  

   // Open base file
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Sut_ParseTaxBase(acBasebuf, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBasebuf);
         lOut++;
         fputs(acRec, fdBase);
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   } else
      iRet = 0;

   return iRet;
}

/*************************** Sut_Update_TaxBase *******************************
 *
 * Load CA-Sutter-CurrentAmounts.csv to update Tax_Base
 * Create Tax_Base record to update current Tax_Base table.  Match Detail record
 * to get BillType.
 *
 ******************************************************************************/

int Sut_Update_TaxBase(char *pCurrentTaxFile, char *pDetailFile, bool bImport)
{
   char     *pTmp, acOutbuf[1024], acRec[1024], acCurrent[_MAX_PATH], acDetail[_MAX_PATH],
            sBase[1024], sBaseFile[_MAX_PATH];
   long     iTmp, iRet, lOut=0, lCnt=0, lDrop=0, lBad=0, lNoTax=0;
   double	dTax1, dTax2, dPen1, dPen2, dFee1, dFee2, dInt1, dInt2, dTaxDue1, dTaxDue2,
   	      dPaid1, dPaid2, dPaidPen1, dPaidPen2, dPaidFee1, dPaidFee2, dPaidInt1, dPaidInt2, 
            dTotalTax, dTotalDue, dTotalFees;

   TAXBASE  *pOutBase = (TAXBASE *)sBase;
   FILE     *fdCurrent, *fdDetail, *fdBase;

   LogMsg0("Loading current tax file");

   // Sort current tax file on TaxBillId & PIN
   sprintf(acCurrent, "%s\\%s\\Sut_Current.txt", acTmpPath, myCounty.acCntyCode);
   sprintf(acRec, "S(#3,C,A,#1,C,A) DEL(%d) OMIT(#1,C,GT,\"9999\")", cDelim);
   iRet = sortFile(pCurrentTaxFile, acCurrent, acRec);
   if (!iRet)
      return -1;

   // Sort detail file on TaxBillId & PIN
   sprintf(acDetail, "%s\\%s\\Sut_Detail.txt", acTmpPath, myCounty.acCntyCode);
   sprintf(acRec,  "S(#6,C,A,#1,C,A) DEL(%d) OMIT(#1,C,GT,\"9999\")", cDelim);
   iRet = sortFile(pDetailFile, acDetail, acRec);
   if (!iRet)
      return -1;

   // Open current tax file
   LogMsg("Open current tax file %s", acCurrent);
   fdCurrent = fopen(acCurrent, "r");
   if (fdCurrent == NULL)
   {
      LogMsg("***** Error opening current tax file: %s\n", acCurrent);
      return -2;
   }  

   // Open detail file
   LogMsg("Open tax detail file %s", acDetail);
   fdDetail = fopen(acDetail, "r");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error opening tax Detail file: %s\n", acDetail);
      return -2;
   }  

   // Create output file names
   NameTaxCsvFile(sBaseFile, myCounty.acCntyCode, "Base");

   LogMsg("Create TaxBase file %s", sBaseFile);
   fdBase = fopen(sBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating TaxBase file: %s\n", sBaseFile);
      return -4;
   }

   // Init variables
   while (!feof(fdCurrent))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdCurrent);
      if (!pTmp || *pTmp > '9')
         break;

      // Parse input tax data
      iTokens = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < CA_COLS)
      {
         LogMsg("***** Error: bad Tax bill record for taxbillid=%s (#tokens=%d)", acRec, iTokens);
         return -1;
      }

      // Ignore supplemental & unsecured bills
      iTmp = iTrim(apTokens[CA_PIN]);
      if (iTmp != 12)
         return 1;

      // Reset buffer
      memset(sBase, 0, sizeof(TAXBASE));

      // Populate TaxBase
      strcpy(pOutBase->Apn, apTokens[CA_PIN]);
      strcpy(pOutBase->BillNum, myTrim(apTokens[CA_BILLNUMBER]));

#ifdef _DEBUG
      //if (!memcmp(pOutBase->Apn, "006232003000", 10))
      //   iTmp = 0;
#endif

      // 1st inst charge/paid/due
      strcpy(pOutBase->TaxAmt1, apTokens[CA_INST1TAXCHARGE]);
      if (*apTokens[CA_INST1TAXPAYMENT] == '-')
         strcpy(pOutBase->PaidAmt1, apTokens[CA_INST1TAXPAYMENT]+1);
      else
         strcpy(pOutBase->PaidAmt1, apTokens[CA_INST1TAXPAYMENT]);
      dTax1 = atof(apTokens[CA_INST1TAXCHARGE]);
      dPaid1 = atof(pOutBase->PaidAmt1);
      dTaxDue1 = atof(apTokens[CA_INST1AMOUNTDUE]);

      // 1st penalty/fee/int
      dPen1 = atof(apTokens[CA_INST1PENALTYCHARGE]);
      strcpy(pOutBase->PenAmt1, apTokens[CA_INST1PENALTYCHARGE]);
      dFee1 = atof(apTokens[CA_INST1FEECHARGE]);
      dInt1 = atof(apTokens[CA_INST1INTERESTCHARGE]);

      dPaidPen1=dPaidFee1=dPaidInt1 = 0;
      if (*apTokens[CA_INST1PENALTYPAYMENT] == '-')
         dPaidPen1 = atof(apTokens[CA_INST1PENALTYPAYMENT]+1);
      if (*apTokens[CA_INST1FEEPAYMENT] == '-')
         dPaidFee1 = atof(apTokens[CA_INST1FEEPAYMENT]+1);
      if (*apTokens[CA_INST1INTERESTPAYMENT] == '-')
         dPaidInt1 = atof(apTokens[CA_INST1INTERESTPAYMENT]+1);

      // 2nd inst charge/paid/due
      strcpy(pOutBase->TaxAmt2, apTokens[CA_INST2TAXCHARGE]);
      if (*apTokens[CA_INST2TAXPAYMENT] == '-')
         strcpy(pOutBase->PaidAmt2, apTokens[CA_INST2TAXPAYMENT]+1);
      else
         strcpy(pOutBase->PaidAmt2, apTokens[CA_INST2TAXPAYMENT]);
      dTax2 = atof(apTokens[CA_INST2TAXCHARGE]);
      dPaid2 = atof(pOutBase->PaidAmt2);
      dTaxDue2 = atof(apTokens[CA_INST2AMOUNTDUE]);

      // 2nd penalty/fee/int
      dPen2 = atof(apTokens[CA_INST2PENALTYCHARGE]);
      strcpy(pOutBase->PenAmt2, apTokens[CA_INST2PENALTYCHARGE]);
      dFee2 = atof(apTokens[CA_INST2FEECHARGE]);
      dInt2 = atof(apTokens[CA_INST2INTERESTCHARGE]);
      dPaidPen2=dPaidFee2=dPaidInt2 = 0;
      if (*apTokens[CA_INST2PENALTYPAYMENT] == '-')
         dPaidPen2 = atof(apTokens[CA_INST2PENALTYPAYMENT]+1);
      if (*apTokens[CA_INST2FEEPAYMENT] == '-')
         dPaidFee2 = atof(apTokens[CA_INST2FEEPAYMENT]+1);
      if (*apTokens[CA_INST2INTERESTPAYMENT] == '-')
         dPaidInt2 = atof(apTokens[CA_INST2INTERESTPAYMENT]+1);

      // TaxAmt
      dTotalTax = dTax1+dTax2;
      pOutBase->dTotalTax = dTotalTax;
      sprintf(pOutBase->TotalTaxAmt, "%.2f", dTotalTax);

      dTotalDue = dTaxDue1+dTaxDue2;
      sprintf(pOutBase->TotalDue, "%.2f", dTotalDue);

      dTotalFees = dFee1+dFee2;
      sprintf(pOutBase->TotalFees, "%.2f", dTotalFees);

      // Paid status
      if (dPaid1 >= dTax1)
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      else if (!dTax1)
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else if (dPaid1 < dTax1)
      {
         if (dPen1 > 0)
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      if (dPaid2 >= dTax2)
         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
      else if (!dTax2)
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else if (dPaid2 < dTax2)
      {
         if (dPen2 > 0)
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Set default - these values can be updated in Sut_CreateTaxItems
      pOutBase->isSecd[0] = '1';
      pOutBase->isSupp[0] = '0';
      pOutBase->BillType[0] = BILLTYPE_SECURED;

      // Create tax detail record
      iRet = Sut_Update_BillType(sBase, fdDetail);

      if (pOutBase->BillType[0] >= BILLTYPE_UNSECURED && pOutBase->BillType[0] <= BILLTYPE_UNSECURED_INT_OWNER)
      {
         LogMsg("*** Drop unsecured record: BillNum=%s", pOutBase->BillNum);
         lDrop++;
      } else if (pOutBase->dTotalTax == 0)
      {
         LogMsg("*** Drop non-tax record: BillNum=%s", pOutBase->BillNum);
         lDrop++;
      } else
      if (iRet >= 0)
      {
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&sBase);
         fputs(acOutbuf, fdBase);

         lOut++;
      } else
         lBad++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdBase);
   fclose(fdCurrent);
   fclose(fdDetail);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("         output records:    %u", lOut);
   LogMsg("           drop records:    %u", lDrop);
   LogMsg("            bad records:    %u", lBad);
   LogMsg("      no detail records:    %u", lNoTax);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   } else
      iRet = 0;

   return iRet;
}

/******************************************************************************
 *
 * -L = load lien
 * -U = load update
 * -Xl= extract lien
 *
 * Regular update: -CSUT -U -Xsi -T
 *
 ******************************************************************************/

int loadSut(int iSkip)
{
   int   iRet;
   char  acTmpFile[_MAX_PATH], acDetailFile[_MAX_PATH], acBaseFile[_MAX_PATH], acDelqFile[_MAX_PATH],
         acZipFile[_MAX_PATH], *pTmp;

   iApnLen = myCounty.iApnLen;

   GetIniString("SUT", "ZipFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acZipFile, acTmpFile, lProcDate);
   if (!_access(acZipFile, 0))
   {
      strcpy(acTmpFile, acZipFile);
      pTmp = strrchr(acTmpFile, '\\');
      *pTmp = 0;

      // Unzip input files
      doZipInit();  
      setUnzipToFolder(acTmpFile);
      setReplaceIfExist(true);
      iRet = startUnzip(acZipFile);
      doZipShutdown();

      if (iRet)
      {
         LogMsg("***** Error unzipping %s.  Program terminated", acZipFile);
         return -1;
      }

      // Rename ZIP file
      RenameToExt(acZipFile, "Old");

      // Rebuild input file to standardize APN, remove '-' in APN, remove CR in legal
      lLastFileDate = getFileDate(acRollFile);

      iRet = Sut_RebuildCsv(acCharFile, cDelim, C_COLS);
      iRet = Sut_RebuildCsv(acRollFile, cDelim, R_COLS);
      iRet = Sut_RebuildCsv(acSalesFile, cDelim, S_COLS);
   }

   // Load tax
   if (iLoadTax == TAX_LOADING)              // -T or -Lt
   {
      // Load tax agency table
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      // 10/25/2023
      GetIniString(myCounty.acCntyCode, "TaxDetail", "", acDetailFile, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "TaxMaster", "", acBaseFile, _MAX_PATH, acIniFile);
      if (!_access(acBaseFile, 0))
      {
         lLastTaxFileDate = getFileDate(acBaseFile);
         // Only process if new tax file
         iRet = isNewTaxFile(acBaseFile, myCounty.acCntyCode);
         if (iRet <= 0)
         {
            lLastTaxFileDate = 0;
            return iRet;
         }

         // Load tax base & district data
         iRet = Sut_Load_TaxBase(acBaseFile, bTaxImport);
         if (!iRet)
            iRet = Sut_Load_TaxDetail(acDetailFile, bTaxImport);
      } else
      {
         GetIniString(myCounty.acCntyCode, "TaxItems", "", acDetailFile, _MAX_PATH, acIniFile);
         GetIniString(myCounty.acCntyCode, "TaxCurrent", "", acBaseFile, _MAX_PATH, acIniFile);
         lLastTaxFileDate = getFileDate(acBaseFile);
         // Only process if new tax file
         iRet = isNewTaxFile(acBaseFile, myCounty.acCntyCode);
         if (iRet <= 0)
         {
            lLastTaxFileDate = 0;
            return iRet;
         }

         // Load Current Amount & District data
         iRet = Sut_Load_TaxCurrent(acBaseFile, acDetailFile, bTaxImport);
      }

      if (!iRet)
      {
         GetIniString(myCounty.acCntyCode, "Redemption", "", acDelqFile, _MAX_PATH, acIniFile);
         iRet = Sut_Load_TaxPriorYear(acDelqFile, bTaxImport);
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode, true, true, 3);
      }
   }

   //if (iLoadTax == TAX_LOADING)              // -T or -Lt
   //{
   //   // Load tax agency table
   //   iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
   //   if (iRet > 0)
   //      iRet = LoadTaxCodeTable(acTmpFile);

   //   // 05/05/2022
   //   GetIniString(myCounty.acCntyCode, "TaxItems", "", acDetailFile, _MAX_PATH, acIniFile);
   //   GetIniString(myCounty.acCntyCode, "TaxCurrent", "", acBaseFile, _MAX_PATH, acIniFile);
   //   lLastTaxFileDate = getFileDate(acBaseFile);
   //   // Only process if new tax file
   //   iRet = isNewTaxFile(acBaseFile, myCounty.acCntyCode);
   //   if (iRet <= 0)
   //   {
   //      lLastTaxFileDate = 0;
   //      return iRet;
   //   }

   //   // 03/09/2021 - Load Current Amount & District data
   //   iRet = Sut_Load_TaxCurrent(acBaseFile, acDetailFile, bTaxImport);
   //   if (!iRet)
   //   {
   //      GetIniString(myCounty.acCntyCode, "Redemption", "", acDelqFile, _MAX_PATH, acIniFile);
   //      iRet = Sut_Load_TaxPriorYear(acDelqFile, bTaxImport);
   //      if (!iRet)
   //         iRet = updateDelqFlag(myCounty.acCntyCode);
   //   }
   //}

   if (!iLoadFlag)
      return 0;

   if (iLoadFlag & EXTR_LIEN)                         // -Xl
   {
      iRet = Sut_ExtrLienCsv(myCounty.acCntyCode);
   }

   // Load char file
   if (iLoadFlag & EXTR_ATTR)                         // -Xa
   {
      // 04/26/2022
      iRet = Sut_ConvStdCharCsv(acCharFile);
      if (iRet > 0)
         iRet = 0;
   }

   // Load new sale file LDR 2022
   if (iLoadFlag & EXTR_SALE)                         // -Xs
   {
      // Load special table for Sale - ignore this fornow since there is only one doctype "Deed"
      //GetIniString(myCounty.acCntyCode, "Sale_Xref", "", acTmpFile, _MAX_PATH, acIniFile);
      //iNumDeeds = LoadXrefTable(acTmpFile, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

      iRet = Sut_ExtrSaleCsv();
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract NDC recorder sale 
   if (iLoadFlag & EXTR_NRSAL)                        // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSalesFile, _MAX_PATH, acIniFile);
      if (!_access(acSalesFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSalesFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSalesFile);
         return -1;
      }
   }

   iRet = 0;
   if (iLoadFlag & LOAD_LIEN)                         // -L
   {
      // Create PQ4 file
      LogMsg0("Load %s LDR roll", myCounty.acCntyCode);
      iRet = Sut_Load_LDR_Csv(iSkip);
      if (!iRet)
         iRet = Sut_LoadOthers(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)                  // -U
   {
      // Create PQ4 file
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      //iRet = Sut_Load_Roll(iSkip);
      iRet = Sut_Load_RollCsv(iSkip);
   }

   // Apply NDC sale file to R01
   if (!iRet && (iLoadFlag & UPDT_XSAL))              // -Mn
   {
      lLastRecDate = 0;

      // Apply Sut_Ash.sls to R01 file
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSaleNR(iSkip, acTmpFile, SALE_USE_SCUPDXFR);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )             // -Ms
   {
      // Save lLastRecDate
      int iSaveDate = lLastRecDate;
      lLastRecDate = 0;

      // Apply Sut_Sale.sls to R01 file
      //iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_UPD_SALE);
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, 0);

      if (iSaveDate > lLastRecDate)
         lLastRecDate = iSaveDate;
   }

   return iRet;
}
