/*****************************************************************************
 *
 * 08/28/2019 Add updateStdUse1() to handle special case in PLU where Usecode
 *            are mixed up type and length.
 * 08/31/2019 Remove excess code in logUsecode()
 *
 *****************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "Utils.h"
#include "R01.h"
#include "Logs.h"
#include "UseCode.h"

USETBL   *pUseTable;
USETBL   *pUseTable1;
IUSETBL  *pIUseTable;
int      iNumUseCodes, iNumUseCodes1;
extern   char acRawTmpl[];
extern   int  iRecLen;
static   FILE *fdUsecodeLog = NULL;
static   char acUseLog[_MAX_PATH];

/****************************** LoadUseTbl ***********************************
 *
 * Load a comma delimited file into USETBL table for translation
 * File format: County UseCode, Std Use Code  
 * Use Xref*() functions to access and retrieve data.
 *
 *****************************************************************************/

int LoadUseTbl(LPSTR pUseTbl, USETBL **pTbl, int *piUseCnt)
{
   char     acTmp[_MAX_PATH], *pTmp, *apItems[2];
   int      iRet=0, iCnt, *pCnt;
   FILE     *fd;
   USETBL   *pXrefTbl;

   LogMsg("Loading usecode table: %s", pUseTbl);

   if (piUseCnt)
      pCnt = piUseCnt;
   else
      pCnt = &iNumUseCodes;

   *pCnt = 0;

   fd = fopen(pUseTbl, "r");
   if (fd)
   {
      pTmp = fgets(acTmp, _MAX_PATH, fd);
      iCnt = atoi(pTmp);         // First row is number of entries
      if (iCnt <= 0)
      {
         LogMsgD("***** Invalid UseCode count (%d).  Please call Sony!", iCnt);
         fclose(fd);
         return -1;
      }

      *pCnt = iCnt;
      pXrefTbl = new USETBL[iCnt+1];
      if (pTbl)
      {
         *pTbl = pXrefTbl;
      } else
         pUseTable = pXrefTbl;

      // Loading table
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (!pTmp)
            break;

         iCnt = ParseString(acTmp, ',', 2, apItems);

         if (iCnt > 1)
         {
            strcpy(pXrefTbl->acCntyUse, apItems[0]);
            strcpy(pXrefTbl->acStdUse, apItems[1]);
            pXrefTbl->iCodeLen = strlen(apItems[0]);
            iRet++;
            pXrefTbl++;
            if (iRet >= *pCnt)
            {
               pXrefTbl->acCntyUse[0] = 0;
               pXrefTbl->acStdUse[0] = 0;
               pXrefTbl->iCodeLen = 8;
               break;
            }
         } else
         {
            LogMsgD("***** Table entries are inconsistent at: %s", pTmp);
            iRet = -2;
            break;
         }
      }

      fclose(fd);
   } else
   {
      LogMsgD("***** Error opening USECODE table %s", pUseTbl);
      iRet = -1;
   }

   return iRet;
}

/****************************** LoadIUseTbl **********************************
 *
 * Load a comma delimited file into IUSETBL table for translation
 * File format: County UseCode, Std Use Code  
 * Use Xref*() functions to access and retrieve data.
 *
 *****************************************************************************/

int LoadIUseTbl(LPSTR pUseTbl, IUSETBL **pTbl, int *piUseCnt)
{
   char     acTmp[_MAX_PATH], *pTmp, *apItems[2];
   int      iRet=0, iCnt, *pCnt;
   FILE     *fd;
   IUSETBL  *pXrefTbl;

   LogMsg("Loading usecode table: %s", pUseTbl);

   if (piUseCnt)
      pCnt = piUseCnt;
   else
      pCnt = &iNumUseCodes;

   *pCnt = 0;

   fd = fopen(pUseTbl, "r");
   if (fd)
   {
      pTmp = fgets(acTmp, _MAX_PATH, fd);
      iCnt = atoi(pTmp);         // First row is number of entries
      if (iCnt <= 0)
      {
         LogMsgD("***** Invalid UseCode count (%d).  Please call Sony!", iCnt);
         fclose(fd);
         return -1;
      }

      *pCnt = iCnt;
      pXrefTbl = new IUSETBL[iCnt+1];
      if (pTbl)
         *pTbl = pXrefTbl;
      else
         pIUseTable = pXrefTbl;

      // Loading table
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (!pTmp)
            break;

         iCnt = ParseString(acTmp, ',', 2, apItems);

         if (iCnt > 1)
         {
            pXrefTbl->iCntyUse = atol(apItems[0]);
            strcpy(pXrefTbl->acStdUse, apItems[1]);
            iRet++;
            pXrefTbl++;
            if (iRet >= *pCnt)
            {
               pXrefTbl->iCntyUse = -1;
               pXrefTbl->acStdUse[0] = 0;
               break;
            }
         } else
         {
            LogMsgD("***** Table entries are inconsistent at: %s", pTmp);
            iRet = -2;
            break;
         }
      }

      fclose(fd);
   } else
   {
      LogMsgD("***** Error opening USECODE table %s", pUseTbl);
      iRet = -1;
   }

   return iRet;
}

/****************************** LoadUseCodeTable *****************************
 *
 * Load a comma delimited file into XREFTBL table for translation
 * File format: County UseCode, Std Use Code  
 * Use Xref*() functions to access and retrieve data.
 *
 *****************************************************************************

int LoadUseCodeTbl(LPSTR pUseTbl)
{
   char     acTmp[_MAX_PATH], *pTmp, *apItems[2];
   int      iRet=0, iCnt;
   FILE     *fd;
   USETBL   *pXrefTbl;

   LogMsg("Loading usecode table: %s", pUseTbl);

   iNumUseCodes1 = 0;
   fd = fopen(pUseTbl, "r");
   if (fd)
   {
      pTmp = fgets(acTmp, _MAX_PATH, fd);
      iCnt = atoi(pTmp);         // First row is number of entries
      if (iCnt <= 0)
      {
         LogMsgD("***** Invalid UseCode count (%d).  Please call Sony!", iCnt);
         fclose(fd);
         return -1;
      }

      iNumUseCodes1 = iCnt;
      pUseTable1 = new USETBL[iCnt+1];
      pXrefTbl = pUseTable1;

      // Loading table
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (!pTmp)
            break;

         iCnt = ParseString(acTmp, ',', 2, apItems);

         if (iCnt > 1)
         {
            strcpy(pXrefTbl->acStdUse, apItems[1]);
            strcpy(pXrefTbl->acCntyUse, apItems[0]);
            // This codelen is used to compare with source usecode
            pXrefTbl->iCodeLen = strlen(pXrefTbl->acCntyUse);
            iRet++;
            pXrefTbl++;
            if (iRet >= iNumUseCodes1)
            {
               pXrefTbl->acCntyUse[0] = 0;
               pXrefTbl->acStdUse[0] = 0;
               pXrefTbl->iCodeLen = 8;
               break;
            }
         } else
         {
            LogMsgD("***** Table entries are inconsistent at: %s", pTmp);
            iRet = -2;
            break;
         }
      }

      fclose(fd);
   } else
   {
      LogMsgD("***** Error opening USECODE table %s", pUseTbl);
      iRet = -1;
   }

   return iRet;
}

/******************************** Cnty2StdUse *********************************
 *
 * Purpose:
 *    Do binary search on the sorted UseCode list and returns pointer
 *    to the one that matches.  If there is no match, return NULL.
 *
 * Return pointer to Standard Use Code
 *
 ******************************************************************************/

LPSTR Cnty2StdUse(LPCSTR pCntyUse)
{
   int   i = 0;      
   int   iPrev, iMin, iMax, iCmp;
   char  *pRet = NULL;

   if (iNumUseCodes <= 0)
      return pRet;

   //Set upper limits on search.
   iMin=0;
   iMax=iNumUseCodes+1;

   do
   {
      iPrev=i;
      i=(iMin + iMax) >> 1;

      if (i==iPrev)
      {
         i++;
         break;
      }

      iCmp = memcmp(pCntyUse, pUseTable[i].acCntyUse, pUseTable[i].iCodeLen);
      if (iCmp > 0)
         iMin=i;
      else if (iCmp < 0)
         iMax=i;
      else
         break;

      if (iMax==iMin)
         break;
   } while (iCmp != 0);

   if (!iCmp)
      pRet = pUseTable[i].acStdUse;

   return pRet;
}

LPSTR Cnty2StdUse(int iCntyUse)
{
   int   i = 0;      
   int   iPrev, iMin, iMax;
   char  *pRet = NULL;

   if (iNumUseCodes <= 0)
      return pRet;

   //Set upper limits on search.
   iMin=0;
   iMax=iNumUseCodes+1;

#ifdef _DEBUG
   //if (iCntyUse == 212)
   //   iMin = 0;
#endif

   do
   {
      iPrev=i;
      i=(iMin + iMax) >> 1;

      if (i==iPrev)
      {
         i++;
         break;
      }

      if (iCntyUse > pIUseTable[i].iCntyUse)
         iMin=i;
      else if (iCntyUse < pIUseTable[i].iCntyUse)
         iMax=i;
      else
         break;

      if (iMax==iMin)
         break;
   } while (iCntyUse != pIUseTable[i].iCntyUse);

   if (iCntyUse == pIUseTable[i].iCntyUse)
      pRet = pIUseTable[i].acStdUse;

   return pRet;
}

// This version compares string using strcmp() so it requires exact matched.
LPSTR Cnty2StdUse1(LPCSTR pCntyUse)
{
   int   i = 0;      
   int   iPrev, iMin, iMax, iCmp;
   char  *pRet = NULL;

   if (iNumUseCodes <= 0)
      return pRet;

   //Set upper limits on search.
   iMin=0;
   iMax=iNumUseCodes+1;

   do
   {
      iPrev=i;
      i=(iMin + iMax) >> 1;

      if (i==iPrev)
      {
         i++;
         break;
      }

      iCmp = strcmp(pCntyUse, pUseTable[i].acCntyUse);
      if (iCmp > 0)
         iMin=i;
      else if (iCmp < 0)
         iMax=i;
      else
         break;

      if (iMax==iMin)
         break;
   } while (iCmp != 0);

   if (!iCmp)
      pRet = pUseTable[i].acStdUse;

   return pRet;
}

/******************************* updateStdUse1 ********************************
 *
 * Purpose:
 *    Update std usecode to any output buffer.  Match exact string.
 *
 * Return 1 if update successful, else 0.
 *
 ******************************************************************************/

int updateStdUse1(LPSTR pStdUse, LPCSTR pCntyUse, LPCSTR pApn)
{
   char  *pTmp;
   int   iRet = 0;

   if (*pCntyUse > ' ' && *pCntyUse != '?' && iNumUseCodes > 0)
   {
      pTmp = Cnty2StdUse1(pCntyUse);
      if (pTmp)
      {
         memcpy(pStdUse, pTmp, SIZ_USE_STD);
         iRet = 1;
      } else
      {
         memcpy(pStdUse, USE_MISC, SIZ_USE_STD);
         if (pApn)
            logUsecode((char *)pCntyUse, (char *)pApn);
         else
            logUsecode((char *)pCntyUse);
      }
   } else
      memcpy(pStdUse, USE_UNASGN, SIZ_USE_STD);

   return iRet;
}

//LPSTR Cnty2UseCode(LPCSTR pCntyUse)
//{
//   int   i = 0;      
//   int   iPrev, iMin, iMax, iCmp;
//   char  *pRet = NULL;
//
//   if (iNumUseCodes1 <= 0)
//      return pRet;
//
//   //Set upper limits on search.
//   iMin=0;
//   iMax=iNumUseCodes1+1;
//
//
//   do
//   {
//      iPrev=i;
//      i=(iMin + iMax) >> 1;
//
//      if (i==iPrev)
//      {
//         i++;
//         break;
//      }
//
//      iCmp = memcmp(pCntyUse, pUseTable1[i].acCntyUse, pUseTable1[i].iCodeLen);
//      if (iCmp > 0)
//         iMin=i;
//      else if (iCmp < 0)
//         iMax=i;
//      else
//         break;
//
//      if (iMax==iMin)
//         break;
//   } while (iCmp != 0);
//
//   if (!iCmp)
//      pRet = pUseTable1[i].acStdUse;
//
//   return pRet;
//}
//
//LPSTR Std2CntyUse(LPCSTR pStdUse)
//{
//   char  *pRet=NULL;
//
//   return pRet;
//}

/******************************** updateStdUse ********************************
 *
 * Purpose:
 *    Update std usecode to any output buffer
 *
 * Notes: iUseLen is intended to support fixed length format, but never been used.
 *
 * Return 1 if update successful, else 0.
 *
 ******************************************************************************/

int updateStdUse(LPSTR pStdUse, LPCSTR pCntyUse, int iUseLen, LPCSTR pApn)
{
   char  *pTmp;
   int   iRet = 0;

   if (*pCntyUse > ' ' && *pCntyUse != '?' && iNumUseCodes > 0)
   {
      pTmp = Cnty2StdUse(pCntyUse);
      if (pTmp)
      {
         memcpy(pStdUse, pTmp, SIZ_USE_STD);
         iRet = 1;
      } else
      {
         memcpy(pStdUse, USE_MISC, SIZ_USE_STD);
         if (pApn)
            logUsecode((char *)pCntyUse, (char *)pApn);
         else
            logUsecode((char *)pCntyUse);
      }
   } else
      memcpy(pStdUse, USE_UNASGN, SIZ_USE_STD);

   return iRet;
}

int updateStdUse(LPSTR pStdUse, int iCntyUse, LPCSTR pApn)
{
   char  *pTmp, sTmp[32];
   int   iRet = 0;

   if (iCntyUse > 0 && iNumUseCodes > 0)
   {
      pTmp = Cnty2StdUse(iCntyUse);
      if (pTmp)
      {
         memcpy(pStdUse, pTmp, SIZ_USE_STD);
         iRet = 1;
      } else
      {
         memcpy(pStdUse, USE_MISC, SIZ_USE_STD);
         sprintf(sTmp, "%d", iCntyUse);
         if (pApn)
            logUsecode(sTmp, (char *)pApn);
         else
            logUsecode(sTmp);
      }
   } else
      memcpy(pStdUse, USE_UNASGN, SIZ_USE_STD);

   return iRet;
}

/******************************** updateUseCode ********************************
 *
 * Translate LDR usecode to normal county usecode.  This is designed for LAK.
 *
 * Return 1 if update successful, else 0.
 *
 ******************************************************************************/

//int updateUseCode(LPSTR pUseCode, LPCSTR pCntyUse, int iUseLen)
//{
//   char  *pTmp;
//   int   iRet = 0;
//
//   if (*pCntyUse > ' ' && *pCntyUse != '?')
//   {
//      if (iNumUseCodes1 > 0)
//      {
//         pTmp = Cnty2UseCode(pCntyUse);
//         if (pTmp)
//         {
//            memcpy(pUseCode, pTmp, strlen(pTmp));
//            iRet = 1;
//         } else
//         {
//            logUsecode((char *)pCntyUse);
//         }
//      }
//   }
//
//   return iRet;
//}

/****************************** updateR01StdUse *******************************
 *
 * Update std use code on R01 file.  Original R01 will be rename to S01.
 *
 ******************************************************************************/

int updateR01StdUse(char *pCnty, int iSkip)
{
   char     acBuf[2046];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iTmp;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0;

   LogMsgD("Update Std Usecode on R01");

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");

   // Rename files for processing
   if (!_access(acOutFile, 0))            // If R01 is available, rename it to S01
   {
      if (!_access(acRawFile, 0))
         remove(acRawFile);
      iTmp = rename(acOutFile, acRawFile);
      if (iTmp < 0)
      {
         LogMsg("***** Error: Unable to rename file %s to %s", acOutFile, acRawFile);
         return 1;
      }
   } else if (_access(acRawFile, 0))      // If S01 is not available
   {
      LogMsg("***** Error: Missing input file %s.  Please recheck!", acOutFile);
      return 1;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      // Translate use code
      if (acBuf[OFF_USE_CO] == '?')
         memcpy(&acBuf[OFF_USE_STD], USE_UNASGN, SIZ_USE_STD);
      else
         updateStdUse(&acBuf[OFF_USE_STD], &acBuf[OFF_USE_CO], SIZ_USE_CO);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   printf("\n");
   LogMsgD("Total output records:       %u", lCnt);

   return 0;
}

/********************************* logUseCode *********************************
 *
 ******************************************************************************/

void logUsecode(char *pUsecode, char *pApn)
{
   char buffer[512];
   char *bp = buffer;

   sprintf(buffer, "%.14s ->%.10s\n", pApn, pUsecode);
   if (fdUsecodeLog) 
   {
      fputs(buffer, fdUsecodeLog);
      fflush(fdUsecodeLog);
   } else 
      LogMsg(buffer);
}

bool openUsecodeLog(char *pCnty, char *pIniFile)
{
   char sTmp[_MAX_PATH];
   bool bRet = true;

   if (fdUsecodeLog)
      fclose(fdUsecodeLog);

   GetIniString("System", "UsecodeLog", "", sTmp, _MAX_PATH, pIniFile);
   if (sTmp[0] <= ' ')
   {
      LogMsg("*** UsecodeLog has not been set in %s file.", pIniFile);
      return false;
   }

   // Create log file
   sprintf(acUseLog, sTmp, pCnty, pCnty); 
   if ((fdUsecodeLog=fopen(acUseLog, "w"))==NULL) 
   {
      LogMsg("*** Error creating usecode log file %s", acUseLog);
      bRet = false;
      acUseLog[0] = 0;
   }

   return bRet;
}

void closeUsecodeLog()
{
   if (fdUsecodeLog) 
   {
      fflush(fdUsecodeLog);         

      // If file is empty, delete it
      if (_filelength(fdUsecodeLog->_file) < 1)
      {
         fclose(fdUsecodeLog);
         remove(acUseLog);
      } else
         fclose(fdUsecodeLog);

      fdUsecodeLog = NULL;
   }
}
