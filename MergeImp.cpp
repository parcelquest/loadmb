/**************************************************************************
 *
 * Notes:
 *
 * Revision
 * 10/11/2012 12.3.3   Copy from MergeCal() and modify to support IMP by Nick & Travis
 * 10/16/2012 12.4.1   Calling Imp_CreateSCSale() with DocFmt=0 (use DocNum as is)
 *                     Keep original owner name in Name1. .
 * 10/23/2012 12.4.2   Add DocType table and modify Imp_CreateSCSale().
 * 10/24/2012 12.5.1   Rewrite Imp_MergeOwner() to fit it with IMP.
 * 01/22/2013 12.6.0.3 Update Imp_DocType[] with CodeLen & TypeLen
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 05/02/2013 12.8.1   Modify Imp_MergeRoll() to skip duplicate roll record.
 *                     Fix situs direction
 * 07/19/2013 13.3.5   Fix DocDate & Legal in Imp_MergeRoll(). Add Imp_AddLienExe(),
 *                     Imp_CreateLienRec(). Drop calling to MergeLien() & MergeExe()
 *                     from Imp_Load_Roll(). Fix S_ADDR_D in Imp_MergeSitus().
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 01/02/2014 13.19.0  Rename IMP_DocType to IMP_DocCode and update values to
 *                     match with other counties. Add option -Fx to fix DocType.
 *                     Use MB_CreateSCSale() to replace Imp_CreateSCSale().
 * 04/17/2014 13.23.0  Update FullExe & Prop8 flags in Imp_MergeRoll()
 * 11/06/2014 14.9.1   Add log msg to output number of parcels with LotSqft.
 *                     Fix memory issue in Imp_ConvertChar().
 * 04/05/2015 14.15.1  Fix sale amount issue.
 * 09/23/2015 15.3.1   Fix Imp_Load_Roll() due to change in roll file.  Since all input
 *                     files to this function are sorted, set HdrRows=0.
 * 10/06/2015 15.3.2   Modify Imp_Load_Roll() to sort Situs file on Asmt(asc) & Seq(desc)
 * 10/30/2015 15.4.4   Modify Imp_MergeRoll() to ignore INACTIVE parcels.
 * 01/04/2016 15.9.0   Add option to load TC file by calling Load_TC()
 * 02/14/2016 15.12.0  Modify Imp_MergeMAdr() to fix overwrite bug.
 * 07/22/2016 16.0.6   Fix known bad char in Imp_MergeMAdr().  Modify Imp_MergeExe()
 *                     due to change in 2016-2017 Exemption List.txt
 * 10/14/2016 16.6.0   Fix Imp_Load_Roll() by sort roll file on APN & SET_TYPE to avoid record drop.
 * 07/20/2017 17.1.6   Modify Imp_Load_LDR() to sort input file and merge with EXE file. Split Imp_MergeLien()
 *                     from Imp_MergeRoll() to support LDR file. Fix bad known char in Imp_MergeAdr().
 * 03/27/2018 17.9.0   Add -Ut option to update tax.
 * 04/27/2018 17.11.5  Modify Imp_MergeRoll() & Imp_MergeLien() to add FeeParcel to AltApn.
 * 07/10/2018 18.1.1   Modify Imp_MergeAdr() to skip check for city when addr2 is "CA ".
 *                     Modify Imp_MergeLien() to add Fixture & growing values to other value.
 * 08/14/2018 18.2.4   Add Imp_MergePP() & Imp_AddLienPP() to add PP value from AssetSummary file.
 *                     Some PPVal appears in this file but not in roll or value file. Modify Imp_Load_LDR() &
 *                     Imp_ExtrLien() to use PPVal from AssetSummary file.
 * 08/02/2019 19.1.1   Remove SkipQuote and fix StrNum in Imp_MergeSitus().  Add Imp_MergeMAdr1(),
 *                     Imp_MergeLien1(), Imp_MergeMH(), and Imp_Load_LDR1() to handle LDR 2019.
 *            19.1.2   Replace known bad char in Imp_MergeOwner().
 * 04/27/2020 19.9.0   Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 08/12/2020 20.2.6   Add Imp_MergeStdChar() & Imp_ConvStdChar() to extract chars from new files PQ_*.*
 *                     Still await for final update from the county, will clean up unused functions later.
 * 08/21/2020 20.3.1   Add Imp_GetLegal(), Imp_MergeRoll2(), Imp_Load_Roll2() to load new PQ_* update files.
 *                     Add Imp_ConvStdLand(), Imp_MergeLand(), Imp_AddUnitRec() and modify Imp_Load_Roll2().
 * 10/20/2020 20.4.0   Rebuild PQ_Land.txt file before processing to avoid broken lines.
 *            20.4.0.1 Modify Imp_MergeStdChar() to fix UNITS by formating it left justify.
 * 10/29/2020 20.4.1   Add -Ld option to process daily file.
 *            20.4.1.1 Modify Imp_MergeOwner2() to skip header in owner file.
 *            20.4.1.2 Other bug fix in Imp_CreateSCSale2() to format sale price without decimal.
 * 07/22/2021 21.1.1   Fix bug in Imp_CombineOwner() by increase buffer size.  Modify Imp_MergeOwner2()
 *                     not to combine name with "BENEFICIARY".
 * 08/04/2021 21.1.5   Modify Imp_MergeStdChar() to merge land info too.  Modify Imp_AddLandRec(), Imp_MergeRoll2(),
*                      Imp_MergeLien1() to check for big LotSqft.  Modify Imp_Load_LDR2() to merge std char.
 *                     Fix Imp_ExtrLien2() & Imp_CreateLienRec2() and use cDelim to parse value record.
 * 06/15/2022 21.9.4   Add Imp_ExtrCVal() to extract new CHAR file PQ_BldgDetail.txt
 * 06/22/2022 22.0.0   Modify Imp_ExtrCVal() to ignore historical records with DOCNUM.
 * 09/09/2022 22.2.2   Modify Imp_MergeLien1() to put PERSPROP value from secured roll to PP_MH.
 *                     Modify Imp_Load_LDR1() to add CHAR to R01 records.
 * 10/07/2022 22.2.5   Fix TOTALVALUE in Imp_MergeMH(), Imp_MergePP(), Imp_ExtrMH(), and Imp_AddLienPP().
 * 08/08/2023 23.1.5   Modify Imp_MergeOwner2() to use MergeOwner() using OwnerFull.
 *            23.1.6   Modify Imp_Load_Roll2() to sort Ownership file to make primary owner on top.
 * 05/13/2024 23.8.2   Set default return code to 0 in loadImp().
 * 07/03/2024 24.0.0   Modify Imp_MergeRoll2() to remove bad char in LANDUSE2.
 * 07/29/2024 24.0.4   Modify Imp_MergeLdrExe() to add ExeType.
 * 08/14/2024 24.1.0   Modify Imp_ExtrCVal() to add Bed & Baths from Unit file.  Add Imp_ExtrMHChar()
 *                     to extract char for MH.
 * 08/15/2024 24.1.0.2 Clean up Imp_MergeStdChar().  Fix bug in Imp_AddLandRec() not to use pRec as temp variable.
 *                     Modify Imp_ExtrCVal() to output last record.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "doZip.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "doSort.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "CharRec.h"
#include "MergeImp.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "PQ.h"
#include "Tax.h"

int MB_MergeExe(char *pOutbuf);
int MB_MergeTax(char *pOutbuf);

long lLotSqftCount, lAstMatch, lLandMatch, lLegalMatch, lUnitMatch;
FILE *fdLand, *fdLegal, *fdUnit;

/******************************** Imp_GetLegal *******************************
 *
 * Read legal desc from old file extract (2019 LDR)
 * Return length of legal if successful
 *
 *****************************************************************************/

int Imp_GetLegal(char *pApn, char *pLegal)
{
   static   char acRec[2048], *pRec=NULL;
   char     *apItems[4];
   int      iRet, iTmp;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 2048, fdLegal);
      pRec = fgets(acRec, 2048, fdLegal);
   }

   // Init return value
   *pLegal = 0;
   do
   {
      if (!pRec)
      {
         fclose(fdLegal);
         fdLegal = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pApn, pRec, iApnLen);
      if (iTmp > 0)
         pRec = fgets(acRec, 2048, fdLegal);
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 0;

   // Parse input
   if (pRec)
      iRet = ParseStringIQ(pRec, '|', 4, apItems);
   if (iRet < 2)
   {
      LogMsg("***** Error: bad legal record for APN=%s (#tokens=%d)", pApn, iRet);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001033015000", 9))
   //   iTmp = 0;
#endif

   strcpy(pLegal, apItems[1]);
   lLegalMatch++;

   return strlen(pLegal);
}

/***************************** Imp_MergeStdChar ******************************
 *
 *   - If parcel has more than one CHAR record, use the one that has value.
 *
 *****************************************************************************/

int Imp_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft;
   int      iLoop, iBeds, iFBath, iHBath, iBldgNum, iTmp;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001022001000", 9))
   //   iTmp = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         //if (bDebug)
         //   LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Yrblt
   memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // YrEff
   iTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (iTmp > 1600 && iTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   } else
      memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);

   // Parking type
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // ParkSpace
   lGarSqft = atoin(pChar->ParkSpace, SIZ_CHAR_SIZE4);
   if (lGarSqft > 0)
   {
      //sprintf(acTmp, "%*d", SIZ_PARK_SPACE, lGarSqft);
      //memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      memcpy(pOutbuf+OFF_PARK_SPACE, pChar->ParkSpace, SIZ_CHAR_SIZE4);
   } else
      memset(pOutbuf+OFF_PARK_SPACE, ' ', SIZ_PARK_SPACE);

   // Heating
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];

   // Cooling
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

   // Rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   } else
      memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

   // Units
   iTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d      ", iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   } else
      memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

   // Stories
   if (pChar->Stories[0] > ' ')
      memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
   else
      memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

   // Fireplace
   if (pChar->Fireplace[0] > ' ')
      memcpy(pOutbuf+OFF_FIRE_PL, pChar->Fireplace, SIZ_FIRE_PL);
   else
      memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);

   // LotSqft
   ULONG lSqft;
   if (pChar->LotSqft[0] > '0')
   {
      lSqft = atoln(pChar->LotSqft, SIZ_CHAR_SQFT);

      // Lot Sqft
      if (lSqft < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lSqft);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      } else
         LogMsg("*** MergeStdChar->Ignore LotSqft too big %u [%.12s]", lSqft, pOutbuf);
   }

   // LotAcre
   if (pChar->LotAcre[0] > '0')
   {
      lSqft = atol(pChar->LotAcre);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lSqft);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // BldgNum
   iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);

   // Num Bldgs
   memcpy(pOutbuf+OFF_BLDGS, pChar->Bldgs, SIZ_BLDGS);

   // Sewer
   if (pChar->HasSewer > ' ')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // Water
   if (pChar->Water > ' ')
      *(pOutbuf+OFF_WATER) = pChar->Water;

   // View
   if (pChar->View[0] > ' ')
      *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // Pools
   if (pChar->Pool[0] > ' ')
      *(pOutbuf+OFF_VIEW) = pChar->Pool[0];

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************* Imp_MergeLand *******************************
 *
 * Merge Land record into R01
 *
 *****************************************************************************/

int Imp_MergeLand(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   int      iTmp;
   STDCHAR  *pChar = (STDCHAR *)acRec;

   // Get rec
   if (!pRec)
   {
      pRec = fgets(acRec, 2048, fdLand);
      if (pRec && !isdigit(*pRec))
         pRec = fgets(acRec, 2048, fdLand);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdLand);
         fdLand = NULL;
         return 1;      // EOF
      }

      // Skip quote to copare
      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Land rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 2048, fdLand);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Skip if already populate
   if (*(pOutbuf+OFF_LOT_SQFT+8) > ' ')
      return 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "044401018000", 9))
   //   iTmp = 0;
#endif

   // LotSqft
   if (pChar->LotSqft[0] > '0')
   {
      memcpy(pOutbuf+OFF_LOT_SQFT, pChar->LotSqft, SIZ_LOT_SQFT);

      // LotAcre
      memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_ACRES);
   }

   // Num Units
   memcpy(pOutbuf+OFF_UNITS, pChar->Units, SIZ_CHAR_UNITS);

   // Num Bldgs
   memcpy(pOutbuf+OFF_BLDGS, pChar->Bldgs, SIZ_BLDGS);

   // Sewer
   if (pChar->HasSewer > ' ')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // Water
   if (pChar->Water > ' ')
      *(pOutbuf+OFF_WATER) = pChar->Water;

   // View
   if (pChar->View[0] > ' ')
      *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // Pools
   if (pChar->Pool[0] > ' ')
      *(pOutbuf+OFF_VIEW) = pChar->Pool[0];

   // Get next record
   pRec = fgets(acRec, 2048, fdLand);
   lLandMatch++;

   return 0;
}

/******************************* Imp_AddLandRec ******************************
 *
 * Merge Land record into STDCHAR
 *
 *****************************************************************************/

int Imp_AddLandRec(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256], *apItems[256], *pTmp;
   ULONG    lLotSqft;
   double   dTmp;
   int      iRet=0, iTmp;
   STDCHAR  *pChar = (STDCHAR *)pOutbuf;

   // Get rec
   if (!pRec)
   {
      pRec = fgets(acRec, 2048, fdLand);
      if (pRec && !isdigit(*pRec))
         pRec = fgets(acRec, 2048, fdLand);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdLand);
         fdLand = NULL;
         return 1;      // EOF
      }

      // Skip quote to copare
      iTmp = memcmp(pChar->Apn, acRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Land rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 2048, fdLand);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);
   if (iRet < IMP_LAND_USERID)
   {
      LogMsg("***** Error: bad Land record for APN=%.*s (#tokens=%d)", iApnLen, pRec, iRet);
      pRec = fgets(acRec, 2048, fdLand);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "055200004000", 9))
   //   iTmp = 0;
#endif

   // LotSqft
   lLotSqft = atol(apItems[IMP_LAND_LANDSQFT]);

   // LotAcre
   dTmp = atof(apItems[IMP_LAND_ACRES])*1000.0;
   if (!lLotSqft)
      lLotSqft = (ULONG)(dTmp*SQFT_FACTOR_1000);

   if (lLotSqft > 100)
   {
      iTmp = sprintf(acTmp, "%u", lLotSqft);
      memcpy(pChar->LotSqft, acTmp, iTmp);
      if (!dTmp)
         dTmp = (double)(lLotSqft*SQFT_MF_1000);
   }

   if (dTmp > 0.0)
   {
      iTmp = sprintf(acTmp, "%u", (long)dTmp);
      memcpy(pChar->LotAcre, acTmp, iTmp);
   }

   // Num Units
   iTmp = atol(apItems[IMP_LAND_NUMUNITS]);
   if (iTmp > 0 && iTmp < 1000 && pChar->Units[0] == ' ')
   {
      iTmp = sprintf(acTmp, "%d", iTmp);
      memcpy(pChar->Units, acTmp, iTmp);
   }

   // Num Bldgs
   iTmp = atol(apItems[IMP_LAND_NUMBUILDINGS]);
   if (iTmp > 0 && iTmp < 100)
   {
      iTmp = sprintf(acTmp, "%d", iTmp);
      memcpy(pChar->Bldgs, acTmp, iTmp);
   }

   // Sewer
   if (*apItems[IMP_LAND_HASSEWER] == 'T')
      pChar->HasSewer = 'Y';
   else if (*apItems[IMP_LAND_HASSEWER] == 'F')
      pChar->HasSewer = 'N';

   // Has well
   if (*apItems[IMP_LAND_HASWELL] == 'T')
   {
      pChar->HasWell = 'Y';
      pChar->Water = 'W';
   } else if (*apItems[IMP_LAND_HASWELL] == 'F')
      pChar->HasWell = 'N';

   // Water
   if (*apItems[IMP_LAND_WATERSOURCEDOMESTIC] > ' ')
   {
      pTmp = findXlatCodeA(apItems[IMP_LAND_WATERSOURCEDOMESTIC], &asWaterSrc[0]);
      if (pTmp)
      {
         pChar->Water = *pTmp;
         pChar->HasWater = 'Y';
      }
   }

   // View
   if (*apItems[IMP_LAND_VIEWCODE] > ' ')
   {
      pTmp = findXlatCodeA(apItems[IMP_LAND_VIEWCODE], &asViewCode[0]);
      if (pTmp)
         pChar->View[0] = *pTmp;
   }

   // Pools
   if (*apItems[IMP_LAND_NUMPOOLS] > ' ')
   {
      pTmp = findXlatCodeA(apItems[IMP_LAND_NUMPOOLS], &asPool[0]);
      if (pTmp)
         pChar->Pool[0] = *pTmp;
   }

   // Get next record
   pRec = fgets(acRec, 2048, fdLand);
   lLandMatch++;

   return 0;
}

/******************************* Imp_AddUnitRec *****************************
 *
 * Add Beds & Baths into STDCHAR
 *
 *****************************************************************************/

int Imp_AddUnitRec(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL, *apItems[256];
   char     acTmp[256];
   int      iRet=0, iTmp, iBeds, iBaths;
   STDCHAR  *pChar = (STDCHAR *)pOutbuf;

   // Get rec
   if (!pRec)
   {
      pRec = fgets(acRec, 2048, fdUnit);
      if (pRec && !isdigit(*pRec))
         pRec = fgets(acRec, 2048, fdUnit);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "038250005000", 9))
   //   iTmp = 0;
#endif
   do
   {
      if (!pRec)
      {
         fclose(fdUnit);
         fdUnit = NULL;
         return 1;      // EOF
      }

      // Skip quote to copare
      iTmp = memcmp(pChar->Apn, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Unit rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 2048, fdUnit);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);
   if (iRet < IMP_UNIT_USERID)
   {
      LogMsg("***** Error: bad Unit record for APN=%.12s (#tokens=%d)", pRec, iRet);
      pRec = fgets(acRec, 2048, fdUnit);
      return -1;
   }

   // Beds
   iBeds = atol(apItems[IMP_UNIT_NUMBEDROOMS]);
   if (iBeds > 0 && iBeds < 100)
   {
      iTmp = sprintf(acTmp, "%d", iBeds);
      memcpy(pChar->Beds, acTmp, iTmp);
   }

   // Full baths
   iBaths = atol(apItems[IMP_UNIT_NUMFULLBATHS]);
   if (iBaths > 0 && iBaths < 100)
   {
      iTmp = sprintf(acTmp, "%d", iBaths);
      memcpy(pChar->FBaths, acTmp, iTmp);
   }

   // Half baths
   iBaths = atol(apItems[IMP_UNIT_NUMHALFBATHS]);
   if (iBaths > 0 && iBaths < 100)
   {
      iTmp = sprintf(acTmp, "%d", iBaths);
      memcpy(pChar->HBaths, acTmp, iTmp);
   }

   // Total rooms
   int iRooms = atol(apItems[IMP_UNIT_TOTALROOMS]);
   if (iRooms > 0)
   {
      iTmp = sprintf(acTmp, "%d", iRooms);
      memcpy(pChar->Rooms, acTmp, iTmp);
   }

   // BldgSeqNum
   int iBldgNo = atol(apItems[IMP_UNIT_BUILDINGSEQNUM]);
   if (iBldgNo > 0 && iBldgNo < 100 && pChar->BldgSeqNo[0] == ' ')
   {
      iTmp = sprintf(acTmp, "%d", iBldgNo);
      memcpy(pChar->BldgSeqNo, acTmp, iTmp);
   }

   // UnitSeqNum
   int iUnitNo = atol(apItems[IMP_UNIT_UNITSEQNUM]);
   if (iUnitNo > 0 && iUnitNo < 100 && pChar->UnitSeqNo[0] == ' ')
   {
      iTmp = sprintf(acTmp, "%d", iUnitNo);
      memcpy(pChar->UnitSeqNo, acTmp, iTmp);
   }

   // NumFirePlaces
   int iNumFp = atol(apItems[IMP_UNIT_NUMFIREPLACES]);
   if (iNumFp > 0 && iNumFp < 10)
   {
      iTmp = sprintf(acTmp, "%d", iNumFp);
      memcpy(pChar->Fireplace, acTmp, iTmp);
   }

   // UnitSize
   int iUnitSize = atol(apItems[IMP_UNIT_UNITSIZE]);
   if (iUnitSize > 500)
   {
      iTmp = sprintf(acTmp, "%d", iUnitSize);
      memcpy(pChar->UnitSize, acTmp, iTmp);
   }

   // # Floors
   int iFloors = atol(apItems[IMP_UNIT_NUMFLOORS]);
   if (iFloors > 0 && pChar->Stories[0] == ' ')
   {
      iTmp = sprintf(acTmp, "%d.0", iFloors);
      memcpy(pChar->Stories, acTmp, iTmp);
   }

   // Get next record
   pRec = fgets(acRec, 2048, fdUnit);
   lUnitMatch++;

   return 0;
}

/**************************** Imp_ConvStdChar ********************************
 *
 * Convert PQ_Building.txt & PQ_Land.txt to standard CHAR format.
 * Return 0 if success.
 *
 *****************************************************************************/

int Imp_ConvStdChar(char *pCharFile, char *pUnitFile, char *pLandFile)
{
   FILE     *fdIn;
   char     acBuf[4096], acTmp[256], acCode[32], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg0("Converting char file");

   LogMsg("Open Building file %s", pCharFile);
   if (!(fdIn = fopen(pCharFile, "r")))
   {
      LogMsg("***** Error opening Building file %s", pCharFile);
      return -1;
   }

   // Open Land file
   LogMsg("Open Land file %s", pLandFile);
   fdLand = fopen(pLandFile, "r");
   if (fdLand == NULL)
   {
      LogMsg("***** Error opening Land file: %s\n", pLandFile);
      return -1;
   }

   // Open Unit file
   LogMsg("Open Unit file %s", pUnitFile);
   fdUnit = fopen(pUnitFile, "r");
   if (fdUnit == NULL)
   {
      LogMsg("***** Error opening Unit file: %s\n", pUnitFile);
      return -1;
   }

   LogMsg("Create CHAR file %s", acCChrFile);
   if (!(fdChar = fopen(acCChrFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acCChrFile);
      return -2;
   }

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);

      if (!pRec)
         break;
      if (acBuf[0] < '0' || acBuf[0] > '9')
         continue;

      // Parse string
      iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

      if (iFldCnt < IMP_BG_USERID)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[IMP_BG_FEEPARCEL], strlen(apTokens[IMP_BG_FEEPARCEL]));
      memcpy(myCharRec.FeeParcel, apTokens[IMP_BG_FEEPARCEL], strlen(apTokens[IMP_BG_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[IMP_BG_FEEPARCEL], acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      // Bldg#
      iTmp = atoi(apTokens[IMP_BG_BUILDINGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo is too big: %d [%s]", iTmp, apTokens[IMP_BG_FEEPARCEL]);

      // Bldg Type
      if (*apTokens[IMP_BG_BUILDINGTYPE] > ' ')
         myCharRec.BldgType[0] = *apTokens[IMP_BG_BUILDINGTYPE];

      // BldgSize
      int iBldgSize = atoi(apTokens[IMP_BG_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      int iUnitRes, iUnitCom, iUnitInd;
      iUnitRes = atoi(apTokens[IMP_BG_NUMUNITSRESIDENTIAL]);
      iUnitCom = atoi(apTokens[IMP_BG_NUMUNITSCOMMERCIAL]);
      iUnitInd = atoi(apTokens[IMP_BG_NUMUNITSINDUSTRIAL]);
      if (iUnitRes > 0)
         iTmp = iUnitRes;
      else if (iUnitCom > 0)
         iTmp = iUnitCom;
      else if (iUnitInd > 0)
         iTmp = iUnitInd;
      else
         iTmp = 0;

      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "001170007000", 9))
      //   iRet = 0;
#endif
      // Pool - in PQ_Land.txt

      // QualityClass
      strcpy(acTmp, _strupr(apTokens[IMP_BG_QUALITYCLASS]));
      iTmp = blankRem(acTmp);
      iRet = atol(acTmp);
      if (iTmp > 0 && iRet < 100)
      {
         memcpy(myCharRec.QualityClass, acTmp, iTmp);
         acCode[0] = 0;
         iRet = 0;
         if (strstr(acTmp, "AV"))
            acCode[0] = 'A';
         else if (strstr(acTmp, "GO"))
            acCode[0] = 'G';
         else if (!memcmp(acTmp, "MH", 2))
         {
            if (isdigit(acTmp[2]))
               iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
            else if (isdigit(acTmp[3]))
               iRet = Quality2Code((char *)&acTmp[3], acCode, NULL);
         } else if (isdigit(acTmp[0]))
            iRet = Quality2Code((char *)&acTmp[0], acCode, NULL);
         else if (isdigit(acTmp[1]))
            iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
         else if (isdigit(acTmp[2]))
            iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
         else if (acTmp[0] > ' ')
            LogMsg("*** Bad Quality P1: %s (APN=%s)", acTmp, apTokens[0]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual  = acCode[0];
         else if (iRet < 0 && acTmp[0] > ' ')
            LogMsg("*** Bad Quality P2: %s (APN=%s)", acTmp, apTokens[0]);

         if (acTmp[0] >= 'A')
            myCharRec.BldgClass = acTmp[0];
         else if (*apTokens[IMP_BG_BUILDINGCODE] >= 'A' && *apTokens[IMP_BG_BUILDINGCODE] <= 'Z')
         {
            myCharRec.BldgClass = *apTokens[IMP_BG_BUILDINGCODE];
            iTmp = sprintf(acCode, "%c%s", *apTokens[IMP_BG_BUILDINGCODE], acTmp);
            vmemcpy(myCharRec.QualityClass, acCode, SIZ_CHAR_QCLS, iTmp);
         }
      }

      // Improved Condition
      if (*apTokens[IMP_BG_CONDITION] > ' ')
      {
         pRec = findXlatCodeA(apTokens[IMP_BG_CONDITION], &asCondition[0]);
         if (pRec)
            myCharRec.ImprCond[0] = *pRec;
      }

      // YrBlt
      int iYrBlt = atoi(apTokens[IMP_BG_YEARBUILT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[IMP_BG_EFFECTIVEYEAR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // Attached SF
      iTmp = atoi(apTokens[IMP_BG_SQFTGARAGE]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.GarSqft, acTmp, iRet);

         if (*apTokens[IMP_BG_SQFTGARAGE] == '1')           // Attached
            myCharRec.ParkType[0] = 'A';
         else if (*apTokens[IMP_BG_SQFTGARAGE] == '2')      // Detached
            myCharRec.ParkType[0] = 'D';
      }

      // Heating
      if (*apTokens[IMP_BG_HEATING] > ' ')
      {
         pRec = findXlatCodeA(apTokens[IMP_BG_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      }

      // Cooling
      if (*apTokens[IMP_BG_COOLING] > ' ')
      {
         pRec = findXlatCodeA(apTokens[IMP_BG_COOLING], &asCooling[0]);
         if (pRec)
            myCharRec.Cooling[0] = *pRec;
      }

      // FirePlace - in PQ_Units

      // Sewer - in PQ_Land.txt

      // Water - in PQ_Units

      // OfficeSpaceSF
      iTmp = atoi(apTokens[IMP_BG_SQFTOFFICES]);
      if (iTmp > 10)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Misc.sExtra.OfficeSqft, acTmp, iRet);
      }

      // PatioSF
      //iTmp = atoi(apTokens[TUO_PCHAR_PATIOSF]);
      //if (iTmp > 10)
      //{
      //   iRet = sprintf(acTmp, "%d", iTmp);
      //   memcpy(myCharRec.PatioSqft, acTmp, iRet);
      //}

      // ViewCode - in PQ_Land.txt

      // Stories/NumFloors 1, 2, 3, 4, 9, 13, 14
      iTmp = atoi(apTokens[IMP_BG_NUMFLOORS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Zoning - - in PQ_Asmt.txt

      // Neighborhood code - in PQ_Asmt.txt

      // Roof cover
      if (*apTokens[IMP_BG_ROOFCOVER] >= '0')
      {
         iTmp = atol(apTokens[IMP_BG_ROOFCOVER]);
         sprintf(acTmp, "%.2d", iTmp);
         pRec = findXlatCodeA(acTmp, &asRoofMat[0]);
         if (pRec)
            myCharRec.RoofMat[0] = *pRec;
      }

      // Roof type
      if (*apTokens[IMP_BG_ROOFSTYLE] >= 'A')
      {
         pRec = findXlatCodeA(apTokens[IMP_BG_ROOFSTYLE], &asRoofType[0]);
         if (pRec)
            myCharRec.RoofType[0] = *pRec;
      }

      // # Floors
      int iFloors = atol(apTokens[IMP_BG_NUMFLOORS]);
      if (iFloors > 0)
      {
         iTmp = sprintf(acTmp, "%d.0", iFloors);
         memcpy(myCharRec.Stories, acTmp, iTmp);
      }

      // Add Beds & Baths
      if (fdUnit)
         iTmp = Imp_AddUnitRec((char *)&myCharRec);

      if (fdLand)
         iTmp = Imp_AddLandRec((char *)&myCharRec);

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdChar);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdUnit) fclose(fdUnit);
   if (fdLand) fclose(fdLand);
   if (fdIn) fclose(fdIn);
   if (fdChar) fclose(fdChar);

   LogMsg("Number of records processed: %d", iCnt);
   LogMsg("        Land record matched: %d", lLandMatch);
   LogMsg("        Unit record matched: %d\n", lUnitMatch);

   return 0;
}

/******************************** findIdxValue ******************************
 *
 * Return offset of data field, -1 if not found
 *
 ****************************************************************************/

FLD_IDX *findIdxValue(char *pFldName, FLD_IDX *pFldTbl)
{
   FLD_IDX *pRet=NULL;

   if (pFldName[0] >= '0')
   {
      while (pFldTbl->iNameLen > 0)
      {
         if (!_memicmp(pFldName, pFldTbl->pName, pFldTbl->iNameLen))
         {
            pRet = pFldTbl;
            break;
         }

         pFldTbl++; 
      }
   }

   return pRet;
}

/********************************* Imp_GetCVal ******************************
 *
 * Populate CHAR record with values.
 *
 * Return 1 if value assigned, 0 otherwise
 *
 ****************************************************************************/

int Imp_GetCVal(char *pOutbuf, char *pName, char *pValue, FLD_IDX *pFldTbl)
{
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp, lTmp;
   double   dTmp;
   STDCHAR *pChar = (STDCHAR *)pOutbuf;
   FLD_IDX  *pFldRet;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "038250005000", 11))
   //   iRet = 0;
#endif
   pFldRet = findIdxValue(pName, pFldTbl);
   if (pFldRet && pFldRet->iOffset >= 0)
   {
      switch (pFldRet->iType)
      {
         case 1:     // LJ
            lTmp = atol(pValue);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%d", lTmp);
               memcpy(pOutbuf+pFldRet->iOffset, acTmp, iTmp);
            }
            break;
         case 2:     // Format 9.9
            lTmp = atol(pValue);
            // Verify Stories
            if (!_memicmp(pFldRet->pName, "StoriesCnt", 5) && lTmp > 99)
               lTmp = 0;
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%d.0", lTmp);
               memcpy(pOutbuf+pFldRet->iOffset, acTmp, iTmp);
            }
            break;
         case 3:     // Date - formatted to yyyymmdd
            if (*pValue > ' ')
            {
               pTmp = dateConversion(pValue, acTmp, MMM_DD_YYYY);
               if (pTmp)
                  memcpy(pOutbuf+pFldRet->iOffset, acTmp, strlen(acTmp));
            }
            break;
         case 4:
            dTmp = atof(pValue);
            if (dTmp > 0.0)
            {
               iTmp = sprintf(acTmp, "%d", (long)(1000*dTmp));
               memcpy(pOutbuf+pFldRet->iOffset, acTmp, iTmp);
            }
            break;
         case 5:  // Need translation
            iTmp = strlen(pValue);
            if (iTmp > 0)
            {
               if (!_memicmp(pFldRet->pName, "Cooling", 3))
               {
                  if (!_memicmp(pValue, "CentralAC", 3))
                     *(pOutbuf+pFldRet->iOffset) = 'C';
                  else if (!_memicmp(pValue, "Evaporative", 3))
                     *(pOutbuf+pFldRet->iOffset) = 'E';
                  else if (!_memicmp(pValue, "RoomWall", 3))
                     *(pOutbuf+pFldRet->iOffset) = 'L';
                  else if (!_memicmp(pValue, "Window", 3))
                     *(pOutbuf+pFldRet->iOffset) = 'W';
               } else if (!_memicmp(pFldRet->pName, "Fireplace", 5))
               {
                  pTmp = findXlatCodeA(pValue, &asFireplace[0]);
                  if (pTmp)
                     pChar->Fireplace[0] = *pTmp;
               } else if (!_memicmp(pFldRet->pName, "Heating", 3))
               {
                  pTmp = findXlatCodeA(pValue, &asHeating[0]);
                  if (pTmp)
                     pChar->Heating[0] = *pTmp;
               } else if (!_memicmp(pFldRet->pName, "Construction", 3))
               {
                  // Above-Stnd|Standard|Sub-stnd
               } else if (!_memicmp(pFldRet->pName, "RoofType", 6))
               {
                  // Flat|Gable|Hip|Shed
               } else if (!_memicmp(pFldRet->pName, "BuildingCode", 10))
               {
                  memcpy(pChar->BuildingCode, pValue, strlen(pValue));
                  if (*pValue >= 'A' && *pValue <= 'Z' && (*(pValue+1) <= ' ' || isdigit(*(pValue+1))))
                  {
                     pChar->BldgClass = *pValue;
                  }               
               } else if (!_memicmp(pFldRet->pName, "WindowPane", 8))
               {
                  // Double|Single|Triple
               } else
                  memcpy(pChar->filler, pValue, strlen(pValue));
            }
            break;
         case 6:     // GarageSF
            lTmp = atol(pValue);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%d", lTmp);
               memcpy(pOutbuf+pFldRet->iOffset, acTmp, iTmp);
               if (pChar->ParkType[0] == ' ')
               {
                  if (!_memicmp(pFldRet->pName, "AttachGarageSF", 4))
                     pChar->ParkType[0] = 'A';
                  else if (!_memicmp(pFldRet->pName, "CarportSF", 4))
                     pChar->ParkType[0] = 'C';
                  else if (!_memicmp(pFldRet->pName, "DetachGarageSF", 4))
                     pChar->ParkType[0] = 'D';
               }
            }
            break;
         default:    // as is
            iTmp = strlen(pValue);
            if (iTmp > pFldRet->iValueLen)
            {
               LogMsg("*** Field: %s [%s] APN=%.*s", pFldRet->pName, pValue, iApnLen, pOutbuf);
               LogMsg("*** Field value is greater than output buffer (%d > %d).  Please review and make change as needed.", iTmp, pFldRet->iValueLen);
               iTmp = pFldRet->iValueLen;
            }
            memcpy(pOutbuf+pFldRet->iOffset, _strupr(pValue), iTmp);
            break;
      }
      iRet = 1;
   } 

   return iRet;
}

/********************************* Imp_ExtrCVal *****************************
 *
 * Extract CHAR data from PQ_PCBldgDetail.txt and Land file PQ_Land.txt
 * Create unique CHAR record on FeeParcel-BldgSeqNum
 *
 * Return number of records extracted
 *
 ****************************************************************************/

int Imp_ExtrCVal(char *pCharfile, char *pLandfile, char *pUnitfile, char *pOutfile)
{
   char  *pTmp, sInbuf[4096], sOutbuf[2048], sTmpFile[_MAX_PATH], acTmp[256];
   int   iRet, iCnt=0;
   FILE  *fdCharVal, *fdOut;
   STDCHAR *pChar = (STDCHAR *)&sOutbuf[0];

   LogMsg0("Extracting CHAR data");

   // Sort input file on FeeParcel, BldgSeqNum, DocumentNum
   sprintf(sTmpFile, "%s\\%s\\%s_BldgDetail.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acTmp, "S(#1,C,A,#%d,N,A,#%d,C,A) OMIT(#1,C,LT,\"001\") DUPOUT DEL(%d) F(TXT)", IMP_BLDG_SEQNUM+1, IMP_BLDG_DOCNUM+1, cDelim);
   iCnt = sortFile(pCharfile, sTmpFile, acTmp);
   if (iCnt < 1000)
      return -1;

   // Open Char file
   LogMsg("Open Char Value file %s", sTmpFile);
   if (!(fdCharVal = fopen(sTmpFile, "r")))
   {
      LogMsg("***** Error opening Char Value file: %s\n", sTmpFile);
      return -2;
   }

   // Open Land file
   LogMsg("Open Land file %s", pLandfile);
   fdLand = fopen(pLandfile, "r");
   if (fdLand == NULL)
   {
      LogMsg("***** Error opening Land file: %s\n", pLandfile);
      return -1;
   }

   // Open Unit file
   LogMsg("Open Unit file %s", pUnitfile);
   fdUnit = fopen(pUnitfile, "r");
   if (fdUnit == NULL)
   {
      LogMsg("***** Error opening Unit file: %s\n", pUnitfile);
      return -1;
   }

   // Open output file
   LogMsg("Create Char file %s", pOutfile);
   if (!(fdOut = fopen(pOutfile, "w")))
   {
      LogMsg("***** Error creating output file %s", pOutfile);
      return -3;
   }

   pChar->Apn[0] = 0;
   while (!feof(fdCharVal))
   {
      // Get next rec
      if (!(pTmp = fgets(sInbuf, 4096, fdCharVal)))
         break;

      iRet = ParseString(sInbuf, '|', MAX_FLD_TOKEN, apTokens);
      if (iRet < IMP_BLDG_DTS || sInbuf[0] > '9')
      {
         LogMsg("Output last record: %.12s", pChar->Apn);
         pChar->CRLF[0] = '\n';
         pChar->CRLF[1] = '\0';
         fputs(sOutbuf, fdOut);
         iCnt++;
         break;
      }

      // Ignore records with DocNum
      if (*(apTokens[IMP_BLDG_DOCNUM]) > ' ')
         continue;

      if (memcmp(pChar->Apn, apTokens[IMP_BLDG_FEEPARCEL], iApnLen) || 
          memcmp(pChar->BldgSeqNo, apTokens[IMP_BLDG_SEQNUM], strlen(apTokens[IMP_BLDG_SEQNUM])))
      {
         // Save current record
         if (pChar->Apn[0] > ' ')
         {
            char acCode[32];
            acCode[0] = 0;

            // Check Quality Code
            if (pChar->QualityClass[0] > ' ')
            {
               memcpy(acTmp, pChar->QualityClass, SIZ_CHAR_QCLS);
               acTmp[SIZ_CHAR_QCLS] = 0;
               if (strstr(acTmp, "AV"))
                  acCode[0] = 'A';
               else if (strstr(acTmp, "GO"))
                  acCode[0] = 'G';
               else if (!memcmp(acTmp, "MH", 2))
               {
                  if (isdigit(acTmp[2]))
                     iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
                  else if (isdigit(acTmp[3]))
                     iRet = Quality2Code((char *)&acTmp[3], acCode, NULL);
               } else if (isdigit(acTmp[0]))
                  iRet = Quality2Code((char *)&acTmp[0], acCode, NULL);
               else if (isdigit(acTmp[1]))
                  iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
               else if (isdigit(acTmp[2]))
                  iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
               else if (acTmp[0] > ' ')
                  LogMsg("*** Bad Quality P1: %s (APN=%s)", acTmp, apTokens[0]);

               if (acCode[0] > ' ')
                  pChar->BldgQual  = acCode[0];
            } 
#ifdef _DEBUG
            //if (!memcmp(sOutbuf, "055200004000", 11))
            //   iRet = 0;
#endif

            if (fdLand)
               iRet = Imp_AddLandRec(sOutbuf);

            if (fdUnit)
               iRet = Imp_AddUnitRec(sOutbuf);

            pChar->CRLF[0] = '\n';
            pChar->CRLF[1] = '\0';
            fputs(sOutbuf, fdOut);
            iCnt++;
         }

         // Setup new record
         memset(sOutbuf, ' ', sizeof(STDCHAR));
         memcpy(pChar->Apn, apTokens[IMP_BLDG_FEEPARCEL], strlen(apTokens[IMP_BLDG_FEEPARCEL]));
         memcpy(pChar->BldgSeqNo, apTokens[IMP_BLDG_SEQNUM], strlen(apTokens[IMP_BLDG_SEQNUM]));
         iRet = formatApn(apTokens[IMP_BLDG_FEEPARCEL], acTmp, &myCounty);
         memcpy(pChar->Apn_D, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[0], "055200004000", 11))
      //   iRet = 0;
#endif

      // Look for char values - Ignore recs with internal docnum
      iRet = Imp_GetCVal(sOutbuf, apTokens[IMP_BLDG_NAME], myTrim(apTokens[IMP_BLDG_VALUE]), (FLD_IDX *)&tblCharVal[0]);

      if (!(iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdOut);
   fclose(fdCharVal);
   if (fdLand) fclose(fdLand);
   if (fdUnit) fclose(fdUnit);
   LogMsg("Number of records processed: %d", iCnt);
   LogMsg("        Land record matched: %d", lLandMatch);
   LogMsg("        Unit record matched: %d\n", lUnitMatch);

   return iCnt;
}

/**************************** Imp_ConvStdLand ********************************
 *
 * Convert PQ_Building.txt & PQ_Land.txt to standard CHAR format.
 * Return 0 if success.
 *
 *****************************************************************************/

int Imp_ConvStdLand(char *pLandFile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], *pRec;
   int      iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg0("Loading land file");

   // Open Land file
   LogMsg("Open county Land file %s", pLandFile);
   fdIn = fopen(pLandFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Land file: %s\n", pLandFile);
      return -1;
   }

   GetIniString(myCounty.acCntyCode, "LandExtr", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg("Create Land extract file %s", acTmpFile);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating Land extract file %s", acTmpFile);
      return -2;
   }

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 2048, fdIn);

      if (!pRec)
         break;
      if (acBuf[0] < '0' || acBuf[0] > '9')
         continue;

      // Parse string
      iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < IMP_LAND_USERID)
      {
         LogMsg("***** Error: bad Land record for APN=%.12s (#tokens=%d)", pRec, iFldCnt);
         pRec = fgets(pRec, 2048, fdIn);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[IMP_LAND_FEEPARCEL], strlen(apTokens[IMP_LAND_FEEPARCEL]));
      memcpy(myCharRec.FeeParcel, apTokens[IMP_LAND_FEEPARCEL], strlen(apTokens[IMP_LAND_FEEPARCEL]));

      // Format APN
      iTmp = formatApn(apTokens[IMP_LAND_FEEPARCEL], acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iTmp);

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "001460004000", 9))
      //   iTmp = 0;
#endif

      // LotSqft
      ULONG lLotSqft = atol(apTokens[IMP_LAND_LANDSQFT]);

      // LotAcre
      double dTmp = atof(apTokens[IMP_LAND_ACRES])*1000.0;
      if (!lLotSqft)
         lLotSqft = (unsigned long)(dTmp*SQFT_FACTOR_1000);

      if (lLotSqft > 100 && lLotSqft < 999999999)
      {
         iTmp = sprintf(acTmp, "%u", lLotSqft);
         memcpy(myCharRec.LotSqft, acTmp, iTmp);
         if (!dTmp)
            dTmp = (double)(lLotSqft*SQFT_MF_1000);
      }

      if (dTmp > 0.0)
      {
         iTmp = sprintf(acTmp, "%d", (long)dTmp);
         memcpy(myCharRec.LotAcre, acTmp, iTmp);
      }

      // Num Units
      iTmp = atol(apTokens[IMP_LAND_NUMUNITS]);
      if (iTmp > 0 && iTmp < 1000)
      {
         iTmp = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iTmp);
      }

      // Num Bldgs
      iTmp = atol(apTokens[IMP_LAND_NUMBUILDINGS]);
      if (iTmp > 0 && iTmp < 100)
      {
         iTmp = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Bldgs, acTmp, iTmp);
      }

      // Sewer
      if (*apTokens[IMP_LAND_HASSEWER] == 'T')
         myCharRec.HasSewer = 'Y';
      else if (*apTokens[IMP_LAND_HASSEWER] == 'F')
         myCharRec.HasSewer = 'N';

      // Has well
      if (*apTokens[IMP_LAND_HASWELL] == 'T')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.Water = 'W';
      } else if (*apTokens[IMP_LAND_HASWELL] == 'F')
         myCharRec.HasWell = 'N';

      // Water
      if (*apTokens[IMP_LAND_WATERSOURCEDOMESTIC] > ' ')
      {
         pRec = findXlatCodeA(apTokens[IMP_LAND_WATERSOURCEDOMESTIC], &asWaterSrc[0]);
         if (pRec)
         {
            myCharRec.Water = *pRec;
            myCharRec.HasWater = 'Y';
         }
      }

      // View
      if (*apTokens[IMP_LAND_VIEWCODE] > ' ')
      {
         pRec = findXlatCodeA(apTokens[IMP_LAND_VIEWCODE], &asViewCode[0]);
         if (pRec)
            myCharRec.View[0] = *pRec;
      }

      // Pools
      if (*apTokens[IMP_LAND_NUMPOOLS] > ' ')
      {
         pRec = findXlatCodeA(apTokens[IMP_LAND_NUMPOOLS], &asPool[0]);
         if (pRec)
            myCharRec.View[0] = *pRec;
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);
   LogMsg("        Land record matched: %d\n", lLandMatch);

   return 0;
}

/**************************** Imp_CombineOwner *******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Imp_CombineOwner(char *pOutName1, char *pOutName2, char *pInName1, char *pInName2, char *pSwapName)
{
   int   iTmp, iMrgFlg;
   char  acTmp[128], acName1[128], acName2[128];
   char  *pTmp;

   OWNER myOwner;

   // Initialize
   *pOutName1 = 0;
   *pOutName2 = 0;
   strcpy(acName1, pInName1);
   strcpy(acName2, pInName2);

   // Special case
   if (pTmp = strchr(acName1, 0xD1))
      *pTmp = 'N';
   if (pTmp = strchr(acName2, 0xD1))
      *pTmp = 'N';

   // Merge name only if exist
   iMrgFlg = MergeName2(acName1, acName2, acTmp, ',');

   if (iMrgFlg == 1)
   {
      // Name merged OK - drop name2
      strcpy(acName1, acTmp);
      acName2[0] = 0;
   } else if (iMrgFlg == 2)
   {
      // Same name - drop name2
      acName2[0] = 0;
   } else if (iMrgFlg == 99 || !iMrgFlg)
   {
      // Cannot merge - Keep them as is
      if (!strcmp(acName1, acName2))
         acName2[0] = 0;
   }

   remChar(acName1, ',');
   pTmp = myTrim(acName1);

   // Now parse owners
   strcpy(acTmp, pTmp);
   iTmp = splitOwner(acTmp, &myOwner, 5);
   if (iTmp < 0)
      strcpy(pSwapName, acName1);
   else
      strcpy(pSwapName, myOwner.acSwapName);

   strcpy(pOutName1, acName1);
   if (acName2[0] > ' ')
   {
      remChar(acName2, ',');
      strcpy(pOutName2, acName2);
   }
}

/******************************** Imp_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Imp_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64], acVesting[8];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "910101158000", 9))
   //   iTmp = 0;
#endif

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Save original owners to Name1
   vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);

   acName2[0] = 0;
   acSave1[0] = 0;
   acVesting[0] = 0;

   if (pTmp=strstr(acTmp, " 1/2 INT"))
      *pTmp = 0;

   // Check for year that goes before TRUST
   iTmp =0;
   replChar(acTmp, 209, 'N');
   while (acTmp[iTmp])
   {
      if (acTmp[iTmp] > '0' && acTmp[iTmp] <= '9')
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp1 = strlen(acTmp);
      if (iTmp1 > SIZ_NAME1) iTmp1 = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp1);
      return;
   }

   // Check Vesting
   pTmp = strrchr(acTmp, ' ');
   if (pTmp)
   {
      pTmp++;
      if (!strcmp(pTmp, "TC") || !strcmp(pTmp, "T/C"))
      {
         strcpy(acVesting, "TC");
         *pTmp = 0;
      } else if (!strcmp(pTmp, "JT") || !strcmp(pTmp, "JTS"))
      {
         if (!memcmp(pTmp-4, "H/W", 3))
         {
            *(pTmp-4) = 0;
            strcpy(acVesting, "HWJT");
         } else
         {
            strcpy(acVesting, "JT");
            *pTmp = 0;
         }
      } else if (!strcmp(pTmp, "H/W"))
      {
         if (!memcmp(pTmp-3, "JT", 2))
         {
            *(pTmp-3) = 0;
            strcpy(acVesting, "HWJT");
         } else
         {
            strcpy(acVesting, "HW");
            *pTmp = 0;
         }
      } else if (!strcmp(pTmp, "M/W"))
      {
         strcpy(acVesting, "MW");
         *pTmp = 0;
      } else if (!strcmp(pTmp, "S/W"))
      {
         strcpy(acVesting, "SW");
         *pTmp = 0;
      } else if (!strcmp(pTmp, "M/M"))
      {
         strcpy(acVesting, "MM");
         *pTmp = 0;
      } else if (!strcmp(pTmp+1, "M/M"))
      {
         strcpy(acVesting, "MM");
         *pTmp = 0;
      } else if (!strcmp(pTmp, "S/M"))
      {
         strcpy(acVesting, "SM");
         *pTmp = 0;
      } else if (!strcmp(pTmp, "M/W"))
      {
         strcpy(acVesting, "MW");
         *pTmp = 0;
      } else if (!strcmp(pTmp, "UM/M") || !strcmp(pTmp, "UNM/M"))
      {
         strcpy(acVesting, "UM");
         *pTmp = 0;
      } else if (!strcmp(pTmp, "UM/W") || !strcmp(pTmp, "UNM/M"))
      {
         strcpy(acVesting, "UW");
         *pTmp = 0;
      } else if (!strcmp(pTmp, "CP"))
      {
         strcpy(acVesting, "CP");
         *pTmp = 0;
      } else if (!strcmp(pTmp, "TR"))
      {
         if (!memcmp(pTmp-3, "CO", 2))
            *(pTmp-3) = 0;
         else
            *pTmp = 0;
         strcpy(acVesting, "TR");
      }
   }

   if ((pTmp=strstr(acTmp, " JT ")) || (pTmp=strstr(acTmp, " JTS ")) )
   {
      *pTmp = 0;
      if (!memcmp(pTmp+4, "H/W", 3))
         strcpy(acVesting, "HWJT");
      else
         strcpy(acVesting, "JT");
   } else if (pTmp = strstr(acTmp, " H/W "))
   {
      strcpy(acVesting, "HW");
      *pTmp = 0;
   } else if (pTmp = strstr(acTmp, " M/W "))
   {
      strcpy(acVesting, "MW");
      *pTmp = 0;
   } else if (pTmp = strstr(acTmp, " S/W "))
   {
      strcpy(acVesting, "SW");
      *pTmp = 0;
   } else if (pTmp = strstr(acTmp, " M/M "))
   {
      strcpy(acVesting, "MM");
      *pTmp = 0;
   } else if (pTmp = strstr(acTmp, " S/M "))
   {
      strcpy(acVesting, "SM");
      *pTmp = 0;
   } else if (pTmp = strstr(acTmp, " M/W "))
   {
      strcpy(acVesting, "MW");
      *pTmp = 0;
   } else if ((pTmp = strstr(acTmp, " UM/M ")) || (pTmp = strstr(acTmp, " UNM/M ")))
   {
      strcpy(acVesting, "UM");
      *pTmp = 0;
   } else if ((pTmp = strstr(acTmp, " UM/W ")) || (pTmp = strstr(acTmp, " UNM/W ")))
   {
      strcpy(acVesting, "UW");
      *pTmp = 0;
   }

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
      if (pTmp=strstr(acTmp, " TR"))
      {
         *pTmp = 0;
         if (!acVesting[0])
            strcpy(acVesting, "TR");
      } else if (pTmp=strstr(acTmp, " ETAL"))
         *pTmp = 0;

      // If TRUST appears after number, save from number forward
      if (strstr((char *)&acTmp[iTmp], " TRUST") || strstr((char *)&acTmp[iTmp], " LIVING") ||
        strstr((char *)&acTmp[iTmp], " REVOCABLE") )
      {
         iTmp--;
         strcpy(acSave1, (char *)&acTmp[iTmp]);
         acTmp[iTmp] = 0;
         if (!acVesting[0])
            strcpy(acVesting, "TR");
      }
   }

   // Check for more vesting
   if ( (pTmp=strstr(acTmp, " CO TR")) ||
      (pTmp=strstr(acTmp, " CO-TR")) ||
      (pTmp=strstr(acTmp, " COTR")) ||
      (pTmp=strstr(acTmp, " TRUSTEE")) ||
      (pTmp=strstr(acTmp, " TR ")) ||
      (pTmp=strstr(acTmp, " TRES")) ||
      (pTmp=strstr(acTmp, " S-TRUST")) ||
      (pTmp=strstr(acTmp, " S-TRS")) ||
      (pTmp=strstr(acTmp, " TTEE")) )
   {
      *pTmp = 0;
      if (!acVesting[0])
         strcpy(acVesting, "TR");
   }

   if ((pTmp=strstr(acTmp, " EST OF")) ||
      (pTmp=strstr(acTmp, " ESTS OF")) ||
      (pTmp=strstr(acTmp, " ESTATE OF")) ||
      (pTmp=strstr(acTmp, " LIFE ESTATE")) )
   {
      *pTmp = 0;
      if (!acVesting[0])
         strcpy(acVesting, "LE");
   }

   // Filter out words, things in parenthesis
   // MONDANI NELLIE M ESTATE OF
   if ( (pTmp = strchr(acTmp, '(')) ||
      (pTmp=strstr(acTmp, " ETAL"))  || (pTmp=strstr(acTmp, " ET AL")) )
   {
      if (*(pTmp-1) == '&')
         *(pTmp-1) = 0;
      else
         *pTmp = 0;
   }

   if ((pTmp=strstr(acTmp, " TRUST")) || (pTmp=strstr(acTmp, " TRS")) )
   {
      strcpy(acVesting, "TR");
      *pTmp = 0;
   }

   if ((pTmp=strstr(acTmp, " SUCC")) || (pTmp=strstr(acTmp, " SUCCS-TR")) ||
      (pTmp=strstr(acTmp, " SUCCESSOR")) || (pTmp=strstr(acTmp, " SURVIVOR")) )
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
      if (!acVesting[0])
         strcpy(acVesting, "TR");
   } else if ((pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " FAMILY TR"))
             || (pTmp=strstr(acTmp, " INCOME TR")) || (pTmp=strstr(acTmp, " LIVING TR")) )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   if (pTmp = strstr(acSave1, " THE"))
      *pTmp = 0;

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         //LogMsg0("-> %s", acName1);
         //*pTmp++ = 0;

         replStr(acName1, "/", " & ");
      }
   }

   // Drop everything after ';'
   if (pTmp = strchr(acTmp, ';'))
      *pTmp++ = 0;

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030040016000", 9))
   //   iTmp = 0;
#endif

   // Now parse owners
   splitOwner(acName1, &myOwner, 0);
   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);

   if (acVesting[0] > ' ')
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));
   else if (myOwner.acVest[0] > ' ')
      memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));
}

/**************************** Imp_MergeOwner2 ********************************
 *
 * Merge owner from seperate owner file.
 *
 *****************************************************************************/

int Imp_MergeOwner2(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     *pTmp, *apItems[30], sName1[128], sName2[128], sNames[128], sTitle1[32], sTitle2[32],
            sNewName1[128], sNewName2[128], sTmp[1024];
   int      iLoop, iTmp, iItems;

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 1024, fdName);
      if (*pRec > '9')
         pRec = fgets(acRec, 1024, fdName);
   }

#ifdef _DEBUG
   // 001051027    - Name2 is CareOf
   // 022240001520 - Name1 is CareOf
   //if (!memcmp(pOutbuf, "001101014000", 9))
   //   iTmp = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdName);
         fdName = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         //if (bDebug)
         //   LogMsg0("Skip Owner rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdName);
         lNameSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Remove NULL
   replStrAll(acRec, "NULL", "");

   // Parse owner record
   iItems = ParseStringIQ(acRec, cDelim, 30, apItems);
   if (iItems < IMP_OWNR_OWNERFULL)
   {
      LogMsg("***** Error: bad owner record for APN=%s (#tokens=%d)", acRec, iItems);
      return -1;
   }

   lNameMatch++;

   // 08/08/2023
   strcpy(sName1, apItems[IMP_OWNR_OWNERFULL]);
   Imp_MergeOwner(pOutbuf, myTrim(sName1));
   if (*(pOutbuf+OFF_VEST) == ' ' && *apItems[IMP_OWNR_TITLEATTACHED] > ' ')
   {
      if (!memcmp(apItems[IMP_OWNR_TITLEATTACHED], "TR", 2) || !memcmp(apItems[IMP_OWNR_TITLEATTACHED], "SU", 2))
         memcpy(pOutbuf+OFF_VEST, "TR", 2);
      else if (!memcmp(apItems[IMP_OWNR_TITLEATTACHED], "JT", 2))
         memcpy(pOutbuf+OFF_VEST, "JT", 2);
      else if (!memcmp(apItems[IMP_OWNR_TITLEATTACHED], "ET", 2) || !memcmp(apItems[IMP_OWNR_TITLEATTACHED], "& ET", 4))
         memcpy(pOutbuf+OFF_VEST, "EA", 2);
      else if (strstr(apItems[IMP_OWNR_TITLEATTACHED], " TR") || strstr(apItems[IMP_OWNR_TITLEATTACHED], "-TR"))
         memcpy(pOutbuf+OFF_VEST, "TR", 2);
   }
   return 0;

   // Initialize
   sNames[0] = 0;
   sNewName2[0] = 0;
   sTitle2[0] = 0;
   strcpy(sTitle1, apItems[IMP_OWNR_TITLEATTACHED]);

   // Get next record and check for 2nd owner
   sTmp[0] = 0;
   if (!feof(fdName))
   {
      pRec = fgets(sTmp, 1024, fdName);
      replStrAll(sTmp, "NULL", "");
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "910101158", 9))
   //   iTmp = 0;
#endif

   if (pRec && !memcmp(sTmp, acRec, iApnLen))
   {
      // Process name1
      if (*apItems[IMP_OWNR_OWNERFIRST] > ' ')
      {
         if (strchr(apItems[IMP_OWNR_OWNERLAST], ','))
            sprintf(sName1, "%s %s %s", apItems[IMP_OWNR_OWNERLAST], apItems[IMP_OWNR_OWNERFIRST], apItems[IMP_OWNR_OWNERMIDDLE]);
         else
            sprintf(sName1, "%s, %s %s", apItems[IMP_OWNR_OWNERLAST], apItems[IMP_OWNR_OWNERFIRST], apItems[IMP_OWNR_OWNERMIDDLE]);
      } else
         strcpy(sName1, apItems[IMP_OWNR_OWNERLAST]);

      if (pTmp = strchr(sName1, 0xD1))
         *pTmp = 'N';

      // Process Name2
      iItems = ParseStringIQ(sTmp, cDelim, 20, apItems);
      if (*apItems[IMP_OWNR_OWNERFIRST] > ' ')
         sprintf(sName2, "%s, %s %s", apItems[IMP_OWNR_OWNERLAST], apItems[IMP_OWNR_OWNERFIRST], apItems[IMP_OWNR_OWNERMIDDLE]);
      else
         strcpy(sName2, apItems[IMP_OWNR_OWNERLAST]);
      strcpy(sTitle2, apItems[IMP_OWNR_TITLEATTACHED]);
      if (pTmp = strchr(sName2, 0xD1))
         *pTmp = 'N';

      // Drop Name2 if it's the same as Name1
      if (!strcmp(sName1, sName2))
      {
         sName2[0] = 0;
         sTitle2[0] = 0;
      }

      if (sName2[0] == '%')
      {
         sNewName2[0] = 0;
         strcpy(sNewName1, sName1);
         if (strchr(sName1, ',') && !strstr(sName1, " TR "))
            swapName(sName1, sNames, false);
         else
            strcpy(sNames, sName1);
      } else if (!strstr(sName1, " TR ") && !strstr(sName1, " BENEFICIARY"))
         // Merge owner - Only handle record with 2 owners
         Imp_CombineOwner(sNewName1, sNewName2, sName1, sName2, sNames);
      else
      {
         strcpy(sNewName1, sName1);
         strcpy(sNewName2, sName2);
         strcpy(sNames, sName1);
      }

      // If names can't combined, add title to name1
      // If both names has title, keep them separate
      if (sTitle1[0] > ' ' && sTitle2[0] > ' ')
      {
         sprintf(sNewName1, "%s %s", sName1, sTitle1);
         strcpy(sName1, sNewName1);
         sprintf(sNewName2, "%s %s", sName2, sTitle2);
         strcpy(sName2, sNewName2);
      } else if ((sName2[0] <= ' ' || sNewName2[0] <= ' ') && sTitle1[0] > ' ')
      {
         sprintf(sName1, "%s %s", sNewName1, sTitle1);
         sName2[0] = 0;
      } else
      {
         strcpy(sName1, sNewName1);
         strcpy(sName2, sNewName2);
      }

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdName);
      while (!memcmp(pOutbuf, pRec, iApnLen))
      {
         // More than 1 name records
         pRec = fgets(acRec, 1024, fdName);
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      }
   } else
   {
      sName2[0] = 0;

      // Process single name
      if (*apItems[IMP_OWNR_OWNERFIRST] > ' ')
      {
         remChar(apItems[IMP_OWNR_OWNERLAST], ',');
         if ((strlen(apItems[IMP_OWNR_OWNERLAST])+strlen(apItems[IMP_OWNR_OWNERFIRST])+strlen(apItems[IMP_OWNR_OWNERMIDDLE])) > 50)
         {
            strcpy(sName1, apItems[IMP_OWNR_OWNERLAST]);
            if (*apItems[IMP_OWNR_OWNERFIRST] == '&')
               strcpy(sName2, apItems[IMP_OWNR_OWNERFIRST]+2);
            else
               strcpy(sName2, apItems[IMP_OWNR_OWNERFIRST]);
            strcpy(sNames, apItems[IMP_OWNR_OWNERLAST]);
         } else
         {
            if (sTitle1[0] > '9')
               sprintf(sName1, "%s %s %s %s", apItems[IMP_OWNR_OWNERLAST], apItems[IMP_OWNR_OWNERFIRST],
                  apItems[IMP_OWNR_OWNERMIDDLE], apItems[IMP_OWNR_TITLEATTACHED]);
            else
               sprintf(sName1, "%s %s %s", apItems[IMP_OWNR_OWNERLAST], apItems[IMP_OWNR_OWNERFIRST], apItems[IMP_OWNR_OWNERMIDDLE]);
            sprintf(sNames, "%s %s %s", apItems[IMP_OWNR_OWNERFIRST], apItems[IMP_OWNR_OWNERMIDDLE], apItems[IMP_OWNR_OWNERLAST]);
         }
      } else
      {
         OWNER myOwner;
         pTmp = strchr(apItems[IMP_OWNR_OWNERLAST], ',');
         if (pTmp)
         {
            remChar(apItems[IMP_OWNR_OWNERLAST], ',');
            sprintf(sName1, "%s %s", apItems[IMP_OWNR_OWNERLAST], apItems[IMP_OWNR_TITLEATTACHED]);
            iTmp = splitOwner(apItems[IMP_OWNR_OWNERLAST], &myOwner, 5);
            if (iTmp >= 0)
               strcpy(sNames, myOwner.acSwapName);
            else
               strcpy(sNames, sName1);
            //pTmp = swapName(apItems[IMP_OWNR_OWNERLAST], sNames, false);
         } else
         {
            sprintf(sName1, "%s %s", apItems[IMP_OWNR_OWNERLAST], apItems[IMP_OWNR_TITLEATTACHED]);
            strcpy(sNames, sName1);
         }
      }

      iTmp = blankRem(sName1);

      // Save current record for next parcel
      strcpy(acRec, sTmp);
   }

   // Remove old names
   removeNames(pOutbuf, false, false);

   // Upate owners
   remChar(sName1, ',');
   iTmp = blankRem(sName1);
   vmemcpy(pOutbuf+OFF_NAME1, sName1, SIZ_NAME1, iTmp);
   if (sName2[0] > ' ')
   {
      remChar(sName2, ',');
      iTmp = blankRem(sName2);
      vmemcpy(pOutbuf+OFF_NAME2, sName2, SIZ_NAME2, iTmp);
   }

   // Update swapname
   char *p1, *p2, *p3, *p4;
   if (sTitle1[0] < ' ' &&
      ((p1=strstr(sName1, " TR ")) ||
       (p2=strstr(sName1, " LLC")) ||
       (p3=strstr(sName1, " ESTATE")) ||
       (p4=strstr(sName1, " INC")) ))
   {
      if (p1)
         memcpy(pOutbuf+OFF_VEST, "TR", 2);
      else if (p2)
         memcpy(pOutbuf+OFF_VEST, "CO", 2);
      else if (p3)
         memcpy(pOutbuf+OFF_VEST, "ES", 2);
      else
         memcpy(pOutbuf+OFF_VEST, "CO", 2);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, sName1, SIZ_NAME1);
   } else if (!memcmp(sNames, "U.S.A.", 6))
   {
      memcpy(pOutbuf+OFF_VEST, "GV", 2);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames, SIZ_NAME1);
   } else
   {
      remChar(sNames, ',');
      iTmp = blankRem(sNames);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames, SIZ_NAME1, iTmp);
   }

   // Update Vesting
   if (sTitle1[0] > '9')
   {
      sprintf(sTmp, " %s", sTitle1);
      pTmp = findVesting(sTmp, sTitle1);
      if (pTmp)
         vmemcpy(pOutbuf+OFF_VEST, sTitle1, SIZ_VEST);
      else
         iTmp = 0;
   }

   return 0;
}

int Imp_CreateSCSale2(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp;
   bool     bGrpSale;

   LogMsg0("Imp_CreateSCSale2: Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      if (cDelim == '|')
         iTokens = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens <= IMP_SALE_USERID)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[IMP_SALE_DOCNUM] == ' ' || *apTokens[IMP_SALE_DOCDATE] == ' ')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[IMP_SALE_ASMT], strlen(apTokens[IMP_SALE_ASMT]));

      // Docnum
      memcpy(SaleRec.DocNum, apTokens[IMP_SALE_DOCNUM], strlen(apTokens[IMP_SALE_DOCNUM]));

      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[IMP_SALE_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Detect date format
         strcpy(acTmp, apTokens[IMP_SALE_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[IMP_SALE_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[IMP_SALE_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
         memcpy(SaleRec.DocDate, acTmp, 8);

      // Group sale?
      myLTrim(apTokens[IMP_SALE_GROUPSALE]);
      if (*apTokens[IMP_SALE_GROUPSALE] == 'T')
      {
         bGrpSale = true;
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[IMP_SALE_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[IMP_SALE_GROUPASMT], strlen(apTokens[IMP_SALE_GROUPASMT]));
      } else
         bGrpSale = false;

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001160016000", 9))
      //   iTmp = 0;
#endif
      // Confirm sale price
      lPrice=atol(apTokens[IMP_SALE_CONFIRMEDSALESPRICE]);
      if (lPrice > 0)
      {
         sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, SALE_SIZ_SALEPRICE);
      }

      // DocTax
      dTmp = atof(apTokens[IMP_SALE_TAXAMT]);
      if (dTmp > 1.0)
      {

         lTmp = (long)(dTmp * SALE_FACTOR);

         // Check for bad DocTax
         if (dTmp > 100000)
         {
            iTmp = ((int)dTmp/100)*100;
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               LogMsg("??? Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f.  Need investigation.  Ignore price.", SaleRec.Apn, SaleRec.DocNum, dTmp);
               lPrice = 0;
            }
         } else if (lPrice > 0 && (long)dTmp >= lPrice+5000)
         {
            // Even though sale tax is questionable, we still rely on it if it is reasonable
            iTmp = ((int)dTmp/100)*100;
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               iTmp = (lPrice/100)*100;
               if (iTmp == lPrice)
                  LogMsg("*** Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTmp);
               else
               {
                  LogMsg("*** Questionable Sale Tax Amt for %.12s: DocNum=%.12s, Price=%d, \ttax=%.2f).  Need investigation.", SaleRec.Apn, SaleRec.DocNum, lPrice, dTmp);
                  lPrice = lTmp;
               }
            }
         } else
         {
            lPrice = lTmp;
            iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
            memcpy(SaleRec.StampAmt, acTmp, iTmp);
         }

         // Check for questionable sale price
         if (lPrice > 5000000 && !bGrpSale)
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("*** Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s, DOCTYPE=%.3s ==> Ignore price",
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[IMP_SALE_DOCCODE], SaleRec.DocType);
               lPrice = 0;
            }
         }

         // Ignore sale price if less than 1000
         if (lPrice >= 10000)
            iTmp = sprintf(acTmp, "%*u00", SALE_SIZ_SALEPRICE-2, lPrice/100);
         else if (lPrice >= 1000)
            iTmp = sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
         else
            iTmp = 0;
         memcpy(SaleRec.SalePrice, acTmp, iTmp);
      } else if (lPrice > 0)
      {
         memcpy(SaleRec.SalePrice, SaleRec.ConfirmedSalePrice, SALE_SIZ_SALEPRICE);
      }

      // Save original DocCode
      if (isdigit(*apTokens[IMP_SALE_DOCCODE]))
      {
         memcpy(SaleRec.DocCode, apTokens[IMP_SALE_DOCCODE], strlen(apTokens[IMP_SALE_DOCCODE]));

         iTmp = findDocType(apTokens[IMP_SALE_DOCCODE], (IDX_TBL5 *)&IMP_DocCode[0]);
         if (iTmp >= 0)
         {
            memcpy(SaleRec.DocType, IMP_DocCode[iTmp].pCode, IMP_DocCode[iTmp].iCodeLen);
            if (lPrice < 100)
               SaleRec.NoneSale_Flg = IMP_DocCode[iTmp].flag;
         } else if (lPrice < 100)
            SaleRec.NoneSale_Flg = 'Y';
         else
            LogMsg("*** Unknown DocCode %s [%s]", apTokens[IMP_SALE_DOCCODE], apTokens[IMP_SALE_ASMT]);
      }

      // Transfer Type
      if (lPrice > 0 && *apTokens[IMP_SALE_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[IMP_SALE_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         iTmp = blankRem(apTokens[IMP_SALE_SELLER]);
         vmemcpy(SaleRec.Seller1, apTokens[IMP_SALE_SELLER], SALE_SIZ_SELLER, iTmp);

         // Buyer
         iTmp = blankRem(apTokens[IMP_SALE_BUYER]);
         vmemcpy(SaleRec.Name1, apTokens[IMP_SALE_BUYER], SALE_SIZ_BUYER, iTmp);

         SaleRec.CRLF[0] = 10;
         SaleRec.CRLF[1] = 0;
         fputs((char *)&SaleRec,fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);

   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(27,2,C,EQ,\"  \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -1;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            DeleteFile(acCSalFile);

            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -1;
      } else
         iTmp = rename(acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
         DeleteFile(acCSalFile);
      iTmp = rename(acOutFile, acCSalFile);
   }

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                       output:    %d.", lTmp);
   return iTmp;
}

/******************************** Imp_MergeSitus2 ****************************
 *
 * Merge Situs address 2020
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Imp_MergeSitus2(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acUnit[16], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 512, fdSitus);
      if (*pRec > '9' || *pRec < '0')
         pRec = fgets(acRec, 512, fdSitus);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   iRet = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < IMP_SIT_USERID)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001033015000", 9))
   //   iTmp = 0;
#endif

   // Remove old situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = acUnit[0] = 0;
   lTmp = atol(myTrim(apTokens[IMP_SIT_STREETNUM]));
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      sprintf(acAddr1, "%s ", apTokens[IMP_SIT_STREETNUM]);

      if (pTmp = strchr(apTokens[IMP_SIT_STREETNUM], ' '))
      {
         if (*(pTmp+1) == '#')
            strcpy(acUnit, pTmp+1);
         else
            vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, SIZ_S_STR_SUB);
         vmemcpy(pOutbuf+OFF_S_HSENO, acTmp, SIZ_S_HSENO);
      } else
         vmemcpy(pOutbuf+OFF_S_HSENO, apTokens[IMP_SIT_STREETNUM], SIZ_S_HSENO);

      if (*apTokens[IMP_SIT_STREETDIRECTION] > ' ' && isDir(apTokens[IMP_SIT_STREETDIRECTION]))
      {
         strcat(acAddr1, apTokens[IMP_SIT_STREETDIRECTION]);
         vmemcpy(pOutbuf+OFF_S_DIR, apTokens[IMP_SIT_STREETDIRECTION], SIZ_S_DIR);
      }
   }

   char acStr[64];
   strcpy(acStr, apTokens[IMP_SIT_STREET]);
   quoteRem(acStr);

   if (*apTokens[IMP_SIT_STREETTYPE] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, acStr);
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[IMP_SIT_STREETTYPE]);
      vmemcpy(pOutbuf+OFF_S_STREET, acStr, SIZ_S_STREET);

      iTmp = GetSfxCodeX(apTokens[IMP_SIT_STREETTYPE], acTmp);
      if (iTmp > 0)
      {
         Sfx2Code(acTmp, acCode);
         memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      } else
      {
         LogMsg("*** Invalid suffix: %s [%s]", apTokens[IMP_SIT_STREETTYPE], apTokens[IMP_SIT_APN]);
         iBadSuffix++;
      }
   } else
   {
      ADR_REC sAdr;

      parseAdr1S_1(&sAdr, acStr);
      if (sAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);

      if (sAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, strlen(sAdr.strSfx));

      if (sAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sAdr.strDir, strlen(sAdr.strDir));
      strcat(acAddr1, acStr);
   }

   if (*apTokens[IMP_SIT_SPACEAPT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[IMP_SIT_SPACEAPT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[IMP_SIT_SPACEAPT], SIZ_S_UNITNO);
   } else if (acUnit[0] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, acUnit);
      vmemcpy(pOutbuf+OFF_S_UNITNO, acUnit, SIZ_S_UNITNO);
   }

   iTmp = blankRem(acAddr1, SIZ_S_ADDR_D);
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D, iTmp);

   // Situs city
   if (*apTokens[IMP_SIT_COMMUNITY] > ' ')
   {
      char  sCity[64];

      Abbr2Code(apTokens[IMP_SIT_COMMUNITY], acTmp, sCity, pOutbuf);
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (isdigit(*apTokens[IMP_SIT_ZIP]))
         vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[IMP_SIT_ZIP], strlen(apTokens[IMP_SIT_ZIP]));

      if (sCity[0] > ' ')
      {
         iTmp = sprintf(acTmp, "%s, CA %s", myTrim(sCity), apTokens[IMP_SIT_ZIP]);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
      }
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/***************************** MB_CreateSCSale *******************************
 *
 * Extract sale data from ???_Sales.csv and output to ???_SALE.DAT
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * DateFmt:
 *    0 : Default. Auto format by checking input date
 *    MM_DD_YYYY_1 (MON)
 *    YYYY_MM_DD   (HUM)
 *
 * DocTypeFmt:
 *    0 : Default. Set DocType=1 (GD) if DocCode=1 or sale price > 0
 *    1 : AMA, BUT, MAD, SON
 *    2 : PLA
 *    3 : SHA
 *    4 : IMP
 *
 * DocNumFmt:
 *    0 : Default. Copy DocNum as is
 *    1 : Format DocNum second part of DocNum to 7 digits (i.e. 2010R1234 = 2010R0001234)
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Imp_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp;
   bool     bGrpSale;

   LogMsg0("Imp_CreateSCSale: Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      if (cDelim == '|')
         iTokens = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

      // Docnum
      //replCharEx(apTokens[MB_SALES_DOCNUM], "+*`./{}&%$", ' ', 0, true);
      memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));

      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
         memcpy(SaleRec.DocDate, acTmp, 8);

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1' || *apTokens[MB_SALES_GROUPSALE] == 'T')
      {
         bGrpSale = true;
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      } else
         bGrpSale = false;

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001160016000", 9))
      //   iTmp = 0;
#endif
      // Tax
      lPrice=0;
      dTmp = atof(apTokens[MB_SALES_TAXAMT]);
      if (dTmp > 1.0)
      {
         lTmp = (long)(dTmp * SALE_FACTOR);

         // Check for bad DocTax
         if (dTmp > 100000)
         {
            iTmp = ((int)dTmp/100)*100;
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               LogMsg("??? Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f.  Need investigation.  Ignore price.", SaleRec.Apn, SaleRec.DocNum, dTmp);
               lPrice = 0;
            }
         } else if (lPrice > 0 && (long)dTmp >= lPrice+5000)
         {
            // Even though sale tax is questionable, we still rely on it if it is reasonable
            iTmp = ((int)dTmp/100)*100;
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               iTmp = (lPrice/100)*100;
               if (iTmp == lPrice)
                  LogMsg("*** Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTmp);
               else
               {
                  LogMsg("*** Questionable Sale Tax Amt for %.12s: DocNum=%.12s, Price=%d, \ttax=%.2f).  Need investigation.", SaleRec.Apn, SaleRec.DocNum, lPrice, dTmp);
                  lPrice = lTmp;
               }
            }
         } else
         {
            lPrice = lTmp;
            iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
            memcpy(SaleRec.StampAmt, acTmp, iTmp);
         }

         // Check for questionable sale price
         if (lPrice > 5000000 && !bGrpSale)
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("*** Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s, DOCTYPE=%.3s ==> Ignore price",
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE], SaleRec.DocType);
               lPrice = 0;
            }
         }

         // Ignore sale price if less than 1000
         if (lPrice >= 10000)
            sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
         else if (lPrice >= 1000)
            sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
         else
            memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
         memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);
      }

      // Save original DocCode
      if (isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         memcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], strlen(apTokens[MB_SALES_DOCCODE]));

         iTmp = findDocType(apTokens[MB_SALES_DOCCODE], (IDX_TBL5 *)&IMP_DocCode[0]);
         if (iTmp >= 0)
         {
            memcpy(SaleRec.DocType, IMP_DocCode[iTmp].pCode, IMP_DocCode[iTmp].iCodeLen);
            if (lPrice < 100)
               SaleRec.NoneSale_Flg = IMP_DocCode[iTmp].flag;
         } else if (lPrice < 100)
            SaleRec.NoneSale_Flg = 'Y';
         else
            LogMsg("*** Unknown DocCode %s", apTokens[MB_SALES_DOCCODE]);
      }

      // Transfer Type
      if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         strcpy(acTmp, apTokens[MB_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         if (iTmp > SALE_SIZ_SELLER)
            iTmp = SALE_SIZ_SELLER;
         memcpy(SaleRec.Seller1, acTmp, iTmp);

         // Buyer
         strcpy(acTmp, apTokens[MB_SALES_BUYER]);
         iTmp = blankRem(acTmp);
         if (iTmp > SALE_SIZ_BUYER)
            iTmp = SALE_SIZ_BUYER;
         memcpy(SaleRec.Name1, acTmp, iTmp);

         SaleRec.CRLF[0] = 10;
         SaleRec.CRLF[1] = 0;
         fputs((char *)&SaleRec,fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);

   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(27,2,C,EQ,\"  \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -1;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            DeleteFile(acCSalFile);

            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -1;
      } else
         iTmp = rename(acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
         DeleteFile(acCSalFile);
      iTmp = rename(acOutFile, acCSalFile);
   }

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                       output:    %d.", lTmp);
   return iTmp;
}

/******************************** Imp_MergeSitus *****************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Imp_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acUnit[16], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSitus);

      // Get first rec
      pRec = fgets(acRec, 512, fdSitus);
      if (*pRec > '9' || *pRec < '0')
         pRec = fgets(acRec, 512, fdSitus);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   if (pRec)
      iRet = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_SITUS_ZIP+1)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001033015000", 9))
   //   iTmp = 0;
#endif

   // Remove old situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = acUnit[0] = 0;
   lTmp = atol(myTrim(apTokens[MB_SITUS_STRNUM]));
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      sprintf(acAddr1, "%s ", apTokens[MB_SITUS_STRNUM]);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
      {
         if (*(pTmp+1) == '#')
            strcpy(acUnit, pTmp+1);
         else
            vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, SIZ_S_STR_SUB);
         vmemcpy(pOutbuf+OFF_S_HSENO, acTmp, SIZ_S_HSENO);
      } else
         vmemcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], SIZ_S_HSENO);

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         iTmp = sprintf(acTmp, "%c ", *apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, acTmp);
         memcpy(pOutbuf+OFF_S_DIR, acTmp, iTmp);
      }
   }

   char acStr[64];
   strcpy(acStr, apTokens[MB_SITUS_STRNAME]);
   quoteRem(acStr);

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);
      vmemcpy(pOutbuf+OFF_S_STREET, acStr, SIZ_S_STREET);

      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
         Sfx2Code(acTmp, acCode);
      else
      {
         LogMsg0("*** Invalid suffix: %s [%s]", apTokens[MB_SITUS_STRTYPE], apTokens[MB_SITUS_ASMT]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } else
   {
      ADR_REC sAdr;

      parseAdr1S_1(&sAdr, acStr);
      if (sAdr.strName[0] > ' ')
      {
         blankPad(sAdr.strName, SIZ_S_STREET);
         memcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      }
      if (sAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, strlen(sAdr.strSfx));

      if (sAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sAdr.strDir, strlen(sAdr.strDir));

      strcat(acAddr1, acStr);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNO);
   } else if (acUnit[0] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, acUnit);
      vmemcpy(pOutbuf+OFF_S_UNITNO, acUnit, SIZ_S_UNITNO);
   }

   blankRem(acAddr1, SIZ_S_ADDR_D);
   blankPad(acAddr1, SIZ_S_ADDR_D);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      char  sCity[64];

      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, sCity, pOutbuf);
      blankPadz(acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (isdigit(*apTokens[MB_SITUS_ZIP]))
         memcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], strlen(apTokens[MB_SITUS_ZIP]));

      if (sCity[0] > ' ')
         sprintf(acTmp, "%s, CA %s", myTrim(sCity), apTokens[MB_SITUS_ZIP]);
      blankPad(acTmp, SIZ_S_CTY_ST_D);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/******************************** Imp_MergeSale ******************************
 *
 * Note:
 * 1) need to figure out DocType and translate to our index table
 * 2) imp_sales.csv needs to be sorted before processing
 *
 *****************************************************************************/

int Imp_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp, iIdx, iDocType;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      //pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   if (iTmp)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "098047002", 9))
   //   iRet = 0;
#endif

   while (!iTmp)
   {
      // Replace tab char with 0
      if (pRec)
         iRet = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iRet < MB_SALES_XFERTYPE+1)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R')
         {
            iIdx = atoi(apTokens[MB_SALES_DOCNUM]+5);
            sprintf(acTmp, "%.5s%.7d", apTokens[MB_SALES_DOCNUM], iIdx);
            memcpy(sCurSale.acDocNum, acTmp, SALE_SIZ_DOCNUM);
         } else
         {
            strncpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM], SALE_SIZ_DOCNUM);
            blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);
         }

         // DocDate & Sale price - if no docdate, ignore everything else
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, YYYY_MM_DD);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         iDocType = atol(apTokens[MB_SALES_DOCCODE]);

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;

         // Do not use sale price on Doccode 17
         if (acTmp[0] > '0' && iDocType != 17)
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // sale code
         //acTmp[0] = toupper(*apTokens[MB_SALES_XFERTYPE]);
         //if (acTmp[0] == 'F')
         //   sCurSale.acSaleCode[0] = 'F';
         //else if (acTmp[0] == 'L')
         //   sCurSale.acSaleCode[0] = 'L';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         // Skip Prop 58 document -> non-appraisal event

         // Drop Doccodes
         // 4 Stamp
         // 3 non-reappraisal
         if (iDocType != 4 && iDocType != 3)
            MB_MergeSale(&sCurSale, pOutbuf, true, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Imp_MergeChar ******************************
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************

int Imp_MergeChar(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32];
   long     lBldgSqft, lGarSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iFp, iRooms, iYrBlt;
   MB_CHAR  *pChar;

#ifdef _DEBUG
   char     *pTmp = &acRec[0];
#endif

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 512, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (MB_CHAR *)pRec;

   while (!iLoop)
   {
      // Quality Class
      memcpy(acTmp, pChar->QualityClass, MBSIZ_CHAR_QUALITY);
      acTmp[MBSIZ_CHAR_QUALITY] = 0;
      if (isalpha(acTmp[0]))
      {
         *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0];

         iTmp = 0;
         while (acTmp[iTmp] && !isdigit(acTmp[iTmp]))
            iTmp++;

         if (acTmp[iTmp] > '0')
            iRet = Quality2Code((char *)&acTmp[iTmp], acCode, NULL);
         else
            acCode[0] = 0;
      } else if (acTmp[0] > '0' && acTmp[0] <= '9')
         iRet = Quality2Code(acTmp, acCode, NULL);
      else
         acCode[0] = 0;

      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, strlen(acCode));

      // Yrblt
      iYrBlt = atoin(pChar->YearBuilt, MBSIZ_CHAR_YRBLT);
      if (iYrBlt > 1700)
         memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);

      // BldgSqft
      lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
      if (lBldgSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      } else
         memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

      // Garage Sqft
      lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
      if (lGarSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = '2';
      } else
      {
         memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = ' ';
      }

      // Heating
      if (pChar->Heating[0] > ' ')
      {
         iTmp = 0;
         while (asHeating[iTmp].iLen > 0)
         {
            if (!memcmp(pChar->Heating, asHeating[iTmp].acSrc, asHeating[iTmp].iLen))
            {
               *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }
      // Cooling
      if (pChar->Cooling[0] > ' ')
      {
         iTmp = 0;
         while (asCooling[iTmp].iLen > 0)
         {
            if (!memcmp(pChar->Cooling, asCooling[iTmp].acSrc, asCooling[iTmp].iLen) )
            {
               *(pOutbuf+OFF_AIR_COND) = asCooling[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }
      // Pool
      if (pChar->NumPools[0] > ' ')
      {
         iTmp = 0;
         while (asPool[iTmp].iLen > 0)
         {
            if (!memcmp(pChar->NumPools, asPool[iTmp].acSrc, asPool[iTmp].iLen) )
            {
               *(pOutbuf+OFF_POOL) = asPool[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }

      // Beds
      iBeds = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      } else
         memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

      // Bath
      iFBath = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      } else
         memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

      // Half bath
      iHBath = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
      if (iHBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      } else
         memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

      // Total rooms
      iRooms = atoin(pChar->TotalRooms, MBSIZ_CHAR_ROOMS);
      if (iRooms > 0)
      {
         sprintf(acTmp, "%*d", SIZ_ROOMS, iRooms);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      }
      // Take this off since new file doesn't include this field
      //else
      //   memcpy(pOutbuf+OFF_ROOMS, BLANK32, SIZ_ROOMS);

      // Fireplace
      iFp = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
      if (iFp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
         memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
      } else
         memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);

      // HasSeptic or HasSewer
      if (pChar->HasSeptic > '0')
         *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
      else if (pChar->HasSewer > '0')
         *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

      // HasWell
      *(pOutbuf+OFF_WATER) = pChar->HasWell;

      // Pool - ignore
      // There are 35 parcels that has NUMPOOLS=99.  This is invalid data and we
      // cannot use it without investigation


      lCharMatch++;

      // Get next Char rec
      pRec = fgets(acRec, 512, fdChar);
      if (pRec)
         iLoop = memcmp(pOutbuf, pRec, iApnLen);
      else
         break;

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "020027186", 9))
      //   iTmp = 0;
#endif
      // Skip new record if YrBlt is older.
      if (!iLoop && memcmp(pOutbuf+OFF_YR_BLT, &pChar->YearBuilt[0], SIZ_YR_BLT) > 0)
         break;
   }

   return 0;
}

/******************************** Imp_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Imp_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)

{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "056400065", 9) || !memcmp(pOutbuf, "007113047", 9))
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      iTmp = blankRem(acAddr1, SIZ_M_ADDR_D);

      // Remove known bad char
      replUChar((unsigned char *)&acAddr1[0], 0xF1, 'N', iTmp);
      replUChar((unsigned char *)&acAddr1[0], 0xD1, 'N', iTmp);

      quoteRem(acAddr1);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      } else
      {
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
      }
   }

   strcpy(acAddr2, p2);
   iTmp = blankRem(acAddr2);

   // Remove known bad char
   replUChar((unsigned char *)&acAddr1[0], 0xF1, 'N', iTmp);
   replUChar((unsigned char *)&acAddr1[0], 0xD1, 'N', iTmp);
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);

      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);

   }
}

/********************************* Imp_MergeAdr ******************************
 *
 * Updated with 2006 layout
 *
 *****************************************************************************/

void Imp_MergeAdr(char *pOutbuf, int iFlag)
{
   char     acTmp[256], acAddr1[64], acAddr2[64];
   int      iTmp, iStrNo, iZip;
   ADR_REC  sMailAdr;
   ADR_REC  sSitusAdr;
   int iRollMAddr, iRollMCity, iRollMState, iRollMZip, iRollSStreet, iRollSStrNum, iRollSStrDir, iRollSUnit;

   if (iFlag & CREATE_LIEN) {
      iRollMAddr = IMP_ROLL_M_ADDR;
      iRollMCity = IMP_ROLL_M_CITY;
      iRollMState = IMP_ROLL_M_STATE;
      iRollMZip = IMP_ROLL_M_ZIP;
      iRollSStreet = IMP_ROLL_S_STREET;
      iRollSStrNum = IMP_ROLL_S_STRNUM;
      iRollSStrDir = IMP_ROLL_S_DIR;
      iRollSUnit = IMP_ROLL_S_UNIT;
   } else {
      iRollMAddr = MB_ROLL_M_ADDR;
      iRollMCity = MB_ROLL_M_CITY;
      iRollMState = MB_ROLL_M_ST;
      iRollMZip = MB_ROLL_M_ZIP;
      iRollSStreet = MB_SITUS_STRNAME;
      iRollSStrNum = MB_SITUS_STRNUM;
      iRollSStrDir = MB_SITUS_STRDIR;
      iRollSUnit = MB_SITUS_UNIT;
   }

   // Clear old Mailing
   removeMailing(pOutbuf, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));

   // Fix known bad char
   replChar(apTokens[iRollMAddr], 0xF1, 'N');
   replChar(apTokens[iRollMCity], 0xD1, 'N');

   // Mail address
   strcpy(acAddr1, _strupr(apTokens[iRollMAddr]));
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
   iZip = atol(apTokens[iRollMZip]);
   if (iZip > 100000)
      sprintf(acAddr2, "%s %s %.5s-%.4s", _strupr(apTokens[iRollMCity]), _strupr(apTokens[iRollMState]), apTokens[iRollMZip], apTokens[iRollMZip]+5);
   else if (iZip > 400)
      sprintf(acAddr2, "%s %s %.5s", _strupr(apTokens[iRollMCity]), _strupr(apTokens[iRollMState]), apTokens[iRollMZip]);
   else
      sprintf(acAddr2, "%s %s", _strupr(apTokens[iRollMCity]), _strupr(apTokens[iRollMState]));

   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);

   if (sMailAdr.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Mail city/st
   if (*apTokens[iRollMState] >= 'A' && 2 == strlen(apTokens[iRollMState]))
   {
      memcpy(pOutbuf+OFF_M_CITY, apTokens[iRollMCity], strlen(apTokens[iRollMCity]));
      memcpy(pOutbuf+OFF_M_ST, apTokens[iRollMState], 2);
   } else
   {
      parseAdr2_1(&sMailAdr, apTokens[iRollMCity]);
      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
   }

   // Mail zip
   long lZip = atoin(apTokens[iRollMZip], 5);
   if (lZip > 400)
   {
      sprintf(acTmp, "%0.5d", lZip);
      memcpy(pOutbuf+OFF_M_ZIP, acTmp, 5);
      strcpy(sMailAdr.Zip, acTmp);
   }

   // Situs
   //////////////////////////////////////////////////////
   if (iFlag & CREATE_LIEN)
   {
      if (*apTokens[IMP_ROLL_S_ADDR1] > ' ')
      {
         memcpy(pOutbuf+OFF_S_ADDR_D, _strupr(apTokens[IMP_ROLL_S_ADDR1]), strlen(apTokens[IMP_ROLL_S_ADDR1]));
         strcpy(acAddr1, apTokens[IMP_ROLL_S_ADDR1]);
         parseAdr1C(&sSitusAdr, acAddr1);

         iStrNo = atol(apTokens[iRollSStrNum]);
         if (iStrNo > 0)
         {
            // Remove space from StrNum
            remChar(apTokens[iRollSStrNum], ' ');
            sprintf(acAddr1, "%s              ", apTokens[iRollSStrNum]);
            memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
            memcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_HSENO);

            if (*apTokens[iRollSStrDir] > ' ')
               memcpy(pOutbuf+OFF_S_DIR, apTokens[iRollSStrDir], strlen(apTokens[iRollSStrDir]));
            sSitusAdr.lStrNum = iStrNo;
         } else if (sSitusAdr.lStrNum > 0)
         {
            memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
            memcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
            if (sSitusAdr.strDir[0] > ' ')
               memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
         }

         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
         if (sSitusAdr.strSfx[0] > ' ')
            memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
         strcpy(acTmp, apTokens[iRollSUnit]);
         blankRem(acTmp);
         if (acTmp[0] > ' ')
         {
            iTmp = strlen(acTmp);
            // If unit# is longer than defined size, it probably not good.  So drop it.
            if (iTmp <= SIZ_S_UNITNO)
               memcpy(pOutbuf+OFF_S_UNITNO, acTmp, iTmp);
         }
      }

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "001331042000", 9))
      //   iTmp = 0;
#endif

      // Situs city
      //    - First check to see if S_ADDR2 is provided
      //    - Second check to see if street name and number match mailings, if so use mail city
      //    - Third check to see if city code is available, if so translate it
      //    - Last match TRA with city table.  This method is only 90% accurate.
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      if (*apTokens[IMP_ROLL_S_ADDR2] >= 'A' && strcmp(apTokens[IMP_ROLL_S_ADDR2], "CA "))
      {
         strcpy(acAddr2, apTokens[IMP_ROLL_S_ADDR2]);
         _strupr(acAddr2);
         parseAdr2_1(&sSitusAdr, acAddr2);
      } else if (sMailAdr.lStrNum > 0 && sMailAdr.strName[0] > ' ' &&
              (sMailAdr.lStrNum == sSitusAdr.lStrNum) &&
              (!strcmp(myTrim(sMailAdr.strName), myTrim(sSitusAdr.strName))) &&
              (*apTokens[IMP_ROLL_M_CITY] > ' ') )
      {
         strcpy(sSitusAdr.City, apTokens[IMP_ROLL_M_CITY]);
         strcpy(sSitusAdr.Zip, sMailAdr.Zip);
      } else if (*apTokens[IMP_ROLL_S_CITY] >= 'A')
      {
         // void Abbr2Code(char *pCityAbbr, char *pCityCode, char *pCityName, char *pApn)
         Abbr2Code(apTokens[IMP_ROLL_S_CITY], acTmp, sSitusAdr.City, pOutbuf);
      } else
      {
         int iTra = atoi(apTokens[IMP_ROLL_TRA]);
         iTmp = 0;
         acAddr2[0] = 0;
         while (iTra > asImpCity[iTmp].iHiTra)
            iTmp++;
         if (iTra >= asImpCity[iTmp].iLoTra && *asImpCity[iTmp].pCity > ' ')
         {
            strcpy(sSitusAdr.City, asImpCity[iTmp].pCity);
            if (*asImpCity[iTmp].pZip > ' ')
               strcpy(sSitusAdr.Zip, asImpCity[iTmp].pZip);
         } else if (iTra > 0 && sSitusAdr.lStrNum > 0)
         {
            sSitusAdr.City[0] = 0;
            iBadCity++;
            if (bDebug)
               LogMsg0("Unknown TRA: %s APN: %.*s", apTokens[IMP_ROLL_TRA], iApnLen, pOutbuf);
         }
      }

      if (sSitusAdr.City[0] >= 'A')
      {
         City2Code(sSitusAdr.City, acTmp, pOutbuf);
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         iTmp = atol(sSitusAdr.Zip);
         if (iTmp >= 90000)
            memcpy(pOutbuf+OFF_S_ZIP, sSitusAdr.Zip, SIZ_S_ZIP);
         else
            City2Zip(sSitusAdr.City, sSitusAdr.Zip);

         sprintf(acAddr2, "%s, CA %s", GetCityName(atoi(acTmp)), sSitusAdr.Zip);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
      }
   }
   ////////////////////////////
}

/********************************* Imp_MergeAdr ******************************
 *
 * Merge mail addr 2019 LDR
 *
 *****************************************************************************/

void Imp_MergeMAdr1(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], acAddr2[64];
   int      iTmp, iZip;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Fix known bad char
   replChar(apTokens[IMP_ROLL_M_ADDR], 0xF1, 'N');
   replChar(apTokens[IMP_ROLL_M_CITY], 0xD1, 'N');

   // Mail address
   strcpy(acAddr1, _strupr(apTokens[IMP_ROLL_M_ADDR]));
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
   iZip = atol(apTokens[IMP_ROLL_M_ZIP]);
   if (iZip > 100000)
      sprintf(acAddr2, "%s %s %.5s-%.4s", _strupr(apTokens[IMP_ROLL_M_CITY]), _strupr(apTokens[IMP_ROLL_M_STATE]), apTokens[IMP_ROLL_M_ZIP], apTokens[IMP_ROLL_M_ZIP]+5);
   else if (iZip > 400)
      sprintf(acAddr2, "%s %s %.5s", _strupr(apTokens[IMP_ROLL_M_CITY]), _strupr(apTokens[IMP_ROLL_M_STATE]), apTokens[IMP_ROLL_M_ZIP]);
   else
      sprintf(acAddr2, "%s %s", _strupr(apTokens[IMP_ROLL_M_CITY]), _strupr(apTokens[IMP_ROLL_M_STATE]));

   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);

   if (sMailAdr.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Mail city/st
   if (*apTokens[IMP_ROLL_M_STATE] >= 'A' && 2 == strlen(apTokens[IMP_ROLL_M_STATE]))
   {
      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[IMP_ROLL_M_CITY], SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, apTokens[IMP_ROLL_M_STATE], SIZ_M_ST);
   } else
   {
      parseAdr2_1(&sMailAdr, apTokens[IMP_ROLL_M_CITY]);
      if (sMailAdr.State[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
   }

   // Mail zip
   long lZip = atoin(apTokens[IMP_ROLL_M_ZIP], 5);
   if (lZip > 400)
   {
      sprintf(acTmp, "%0.5d", lZip);
      memcpy(pOutbuf+OFF_M_ZIP, acTmp, 5);
   }
}

/********************************* Imp_MergeRoll *****************************
 *
 * This function serves two different input format LDR and update roll. We have
 * to check iFlag to determine record format.
 * Input file: Imp_Roll.txt
 *
 * Take only record with ValueSetType==1
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Imp_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   double   dTmp;
   int      iRet=0, iTmp;
   unsigned long lTmp;

   // Parse input
   if (!iSkipQuote)
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_M_ADDR4)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_ROLL_ASMT], "054605041", 9))
   //   iTmp = 0;
#endif

   // Ignore lines w/ a Value Set Type of 3 (base year) and 8 (Prop 8). Only look at enrolled entry.
   iTmp = atoin(apTokens[MB_ROLL_VALUE_SET_TYPE], 1);
   if (iTmp != 1)
      return 1;

   // Ignore APN starts with 800-999 except 910 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910))
      return 1;

   if (*apTokens[MB_ROLL_STATUS] == 'I')
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "13IMP", 5);
      if (*apTokens[MB_ROLL_STATUS] > ' ')
         *(pOutbuf+OFF_STATUS) = *apTokens[MB_ROLL_STATUS];

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // TRA
      iTmp = atoi(apTokens[MB_ROLL_TRA]);
      if (iTmp > 0)
      {
         memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));
      }

      // HO Exempt - Use values from exemption file in Imp_MergeExe()

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: TradeFixture, LivingImp, PPDeclared, PPUnitVal, PPMH
      lTmp = atoi(apTokens[IMP_ROLL_PPVALUE]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         sprintf(acTmp, "%d          ", lTmp);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // Legal
      if (*apTokens[MB_ROLL_LEGAL] > ' ')
      {
         remUnPrtChar(apTokens[MB_ROLL_LEGAL]);
         iTmp = updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);
      }

      // Tax code
      if (memcmp(apTokens[MB_ROLL_TAXABILITY], "000", 3) > 0)
         vmemcpy(pOutbuf+OFF_TAX_CODE, apTokens[MB_ROLL_TAXABILITY], SIZ_TAX_CODE);
   }

   // Apply FeeParcel to AltApn
   if (strlen(apTokens[MB_ROLL_FEEPARCEL]) != iApnLen)
      LogMsg("*** Invalid FEEPARCEL length on APN= %s", apTokens[iApnFld]);
   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], iApnLen);

   // UseCode
   sprintf(acTmp, "%s", apTokens[MB_ROLL_USECODE]);

   if (acTmp[0] > ' ')
   {
      acTmp[SIZ_USE_CO] = 0;
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   try
   {
      Imp_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   } catch(...)
   {
      LogMsg("***** Execption occured in Imp_MergeOwner()");
   }

   // Mailing - Situs
   try
   {
      Imp_MergeAdr(pOutbuf, iFlag);
   } catch(...)
   {
      LogMsg("***** Execption occured in Imp_MergeAdr()");
   }

   // CareOf
   memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
   {
      memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));
   }

   // DBA
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
      updateCareOf(pOutbuf, apTokens[MB_ROLL_DBA], strlen(apTokens[MB_ROLL_DBA]));
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "054605041", 9))
   //   iTmp = 0;
#endif
   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0' && *apTokens[MB_ROLL_DOCDATE] > ' ')
   {
      iTmp = strlen(apTokens[MB_ROLL_DOCNUM]);
      if (iTmp > SIZ_TRANSFER_DOC)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], 4);
         memcpy(pOutbuf+OFF_TRANSFER_DOC+4, &apTokens[MB_ROLL_DOCNUM][5], iTmp-5);
      } else
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], iTmp);

      if (apTokens[MB_ROLL_DOCDATE][4] == '-')
         dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, YYYY_MM_DD);
      else
         dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);

      memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
      lTmp = atol(acTmp);
      if (lTmp > lLastRecDate && lTmp < lToday)
         lLastRecDate = lTmp;
   }

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      lLotSqftCount++;

      // Lot Sqft
      lTmp = (unsigned long)(dTmp * SQFT_PER_ACRE);
      if (lTmp < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      } else
         LogMsg("*** Ignore LotSqft too big %u", lTmp);

      // Format Acres
      lTmp = (unsigned long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/******************************** Imp_MergeRoll2 *****************************
 *
 * Input file: PQ_Asmt.txt
 *
 * Take only record with ValueSetType==1
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Imp_MergeRoll2(char *pOutbuf, char *pRollRec, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   double   dTmp;
   int      iRet=0, iTmp, iLegal;
   unsigned long lTmp;

   // Parse input
   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   if (iRet < IMP_ASMT_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[0]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[iApnFld], "020100025000", 9))
   //   iTmp = 0;
#endif

   // Ignore APN starts with 800-999 except 910 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 100 && iTmp != 910))
      return 1;

   if (*apTokens[IMP_ASMT_STATUS] != 'A')
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Apply FeeParcel to AltApn
      if (strlen(apTokens[IMP_ASMT_FEEPARCEL]) != iApnLen)
         LogMsg("*** Invalid FEEPARCEL length on APN= %s", apTokens[iApnFld]);
      vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[IMP_ASMT_FEEPARCEL], iApnLen);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "13IMP", 5);
      *(pOutbuf+OFF_STATUS) = *apTokens[IMP_ASMT_STATUS];

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // TRA
      iTmp = atoi(apTokens[IMP_ASMT_TRA]);
      if (iTmp > 0)
      {
         memcpy(pOutbuf+OFF_TRA, apTokens[IMP_ASMT_TRA], strlen(apTokens[IMP_ASMT_TRA]));
      }

      // HO Exempt - Use values from exemption file in Imp_MergeExe()

      // Land
      //long lLand = atoi(apTokens[IMP_ASMT_LAND]);
      //if (lLand > 0)
      //{
      //   sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      //   memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      //}

      // Improve
      //long lImpr = atoi(apTokens[IMP_ASMT_IMPR]);
      //if (lImpr > 0)
      //{
      //   sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      //   memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      //}

      // Other value: TradeFixture, LivingImp, PPDeclared, PPUnitVal, PPMH
      //lTmp = atoi(apTokens[IMP_ASMT_PPVALUE]);
      //if (lTmp > 0)
      //{
      //   sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      //   memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      //   sprintf(acTmp, "%d          ", lTmp);
      //   memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      //}

      // Gross total
      //lTmp += lLand+lImpr;
      //if (lTmp > 0)
      //{
      //   sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      //   memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      //}

      // Ratio
      //if (lImpr > 0)
      //{
      //   sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      //   memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      //}

      // Tax code
      if (*apTokens[IMP_ASMT_TAXABILITYFULL] > ' ')
         vmemcpy(pOutbuf+OFF_TAX_CODE, apTokens[IMP_ASMT_TAXABILITYFULL], SIZ_TAX_CODE);
   }

   // Legal
   if (*apTokens[IMP_ASMT_LEGAL] > ' ')
   {
      iTmp = remUnPrtChar(apTokens[IMP_ASMT_LEGAL]);
      if (iTmp < 48)
         iTmp = updateLegal(pOutbuf, apTokens[IMP_ASMT_LEGAL]);
      else
      {
         // Get legal from Imp_LegalDesc.txt
         iLegal = Imp_GetLegal(apTokens[IMP_ASMT_APN], acTmp);
         if (iLegal > iTmp && !memcmp(apTokens[IMP_ASMT_LEGAL], acTmp, 48))
            iTmp = updateLegal(pOutbuf, acTmp);
         else
            iTmp = updateLegal(pOutbuf, apTokens[IMP_ASMT_LEGAL]);
      }
   }

   // UseCode
   if (*apTokens[IMP_ASMT_LANDUSE2] == 39)
      *apTokens[IMP_ASMT_LANDUSE2] = 0;
   iTmp = sprintf(acTmp, "%s%s      ", apTokens[IMP_ASMT_LANDUSE1], apTokens[IMP_ASMT_LANDUSE2]);
   if (acTmp[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner - use data from PQ_Ownership

   // Mailing - Situs
   //Imp_MergeAdr(pOutbuf, iFlag);

   // CareOf
   //memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
   //if (*apTokens[IMP_ASMT_CAREOF] > ' ')
   //{
   //   memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
   //   updateCareOf(pOutbuf, apTokens[IMP_ASMT_CAREOF], strlen(apTokens[IMP_ASMT_CAREOF]));
   //}

   // DBA
   //if (*apTokens[IMP_ASMT_DBA] > ' ')
   //{
   //   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   //   updateCareOf(pOutbuf, apTokens[IMP_ASMT_DBA], strlen(apTokens[IMP_ASMT_DBA]));
   //}

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001022001000", 9))
   //   iTmp = 0;
#endif
   // Recorded Doc
   if (*(apTokens[IMP_ASMT_CREATINGDOCNUM]+4) == 'R' && *apTokens[IMP_ASMT_CREATINGDOCDATE] > ' ')
   {
      pTmp = dateConversion(apTokens[IMP_ASMT_CREATINGDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         lTmp = atol(acTmp);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[IMP_ASMT_CREATINGDOCNUM], SIZ_TRANSFER_DOC);
      }
   }

   // Acres
   dTmp = atof(apTokens[IMP_ASMT_ACRES]);
   if (dTmp > 0.0)
   {
      lLotSqftCount++;

      // Lot Sqft
      lTmp = (unsigned long)(dTmp * SQFT_PER_ACRE);
      if (lTmp < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      } else
         LogMsg("*** MergeRoll2->Ignore LotSqft too big %u [%.12s]", lTmp, pOutbuf);

      // Format Acres
      lTmp = (unsigned long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[IMP_ASMT_TAXABILITYFULL], true, true);

   return 0;
}

/********************************* Imp_Load_Roll ******************************
 *
 * IMP has no CHAR file
 * All input files has to be resorted to make sure APN matching
 *
 ******************************************************************************/

int Imp_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Reset iHdrRows since all input files are sorted
   iHdrRows = 0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open updated roll file - Sort on field #1 (ASMT)
   lLastFileDate = getFileDate(acRollFile);

   // Rebuild roll file by fixing broken records
   sprintf(acTmpFile, "%s\\%s\\%s_Roll.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = RebuildCsv(acRollFile, acTmpFile, cDelim, MB_ROLL_M_ADDR4);

   // Sort roll file
   sprintf(acRollFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sorting %s to %s", acTmpFile, acRollFile);
   sprintf(acBuf, "S(#1,C,A,#29,C,A) DUPO(#1) DEL(%d) F(TXT)", cDelim);
   iRet = sortFile(acTmpFile, acRollFile, acBuf);
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting VALUE file %s to %s", acTmpFile, acRollFile);
      return -2;
   }

   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   /*LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return -2;
   }*/

   // Open Situs file - need sorting
   strcpy(acTmpFile, acSitusFile);
   sprintf(acSitusFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sorting %s to %s", acTmpFile, acSitusFile);
   // Sort on APN ascending, Seq# decending
   sprintf(acBuf, "S(#1,C,A,#9,C,D) DEL(%d) F(TXT)", cDelim);
   iRet = sortFile(acTmpFile, acSitusFile, acBuf);
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting Situs file %s to %s", acTmpFile, acSitusFile);
      return -2;
   }

   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Exe file - Sort on 2nd field
   strcpy(acTmpFile, acExeFile);
   sprintf(acExeFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsgD("Sorting %s to %s", acTmpFile, acExeFile);
   sprintf(acBuf, "S(#2,C,A) DEL(%d) F(TXT)", cDelim);
   iRet = sortFile(acTmpFile, acExeFile, acBuf);
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting EXE file %s to %s", acTmpFile, acExeFile);
      return -2;
   }

   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Open Tax file - Sort on Asmt, TaxYear, ChrgDate1 & 2, PaidDate1 & 2
   /*
   strcpy(acTmpFile, acTaxFile);
   sprintf(acTaxFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sorting %s to %s", acTmpFile, acTaxFile);
   printf("Sorting %s to %s\n\n", acTmpFile, acTaxFile);
   iRet = sortFile(acTmpFile, acTaxFile, "S(#1,C,A,#16,C,D,#7,DAT,A,#15,DAT,A)");
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting Tax file %s to %s", acTmpFile, acTaxFile);
      return -2;
   }

   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }

   // Open lien file
   fdLienExt = NULL;
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Lien file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   }
   */

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "054605041", 9) ) //|| !memcmp(acRollRec, "047320103000", 9))
      //   iTmp = 0;
#endif

      if (!pTmp || acRollRec[1] > '9')
      {
         bEof = true;
         break;      // EOF
      }

      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Imp_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Imp_MergeSitus(acBuf);

            // Merge Lien
            //lRet = 1;
            //if (fdLienExt)
            //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

            // Merge Exe if not found in LienExt
            //if (fdExe && lRet)
            //   lRet = MB_MergeExe(acBuf);

            // Merge Char
            //if (fdChar)
            //   lRet = Imp_MergeChar(acBuf);

            // Remove old sale data
            //if (bClearSales)
            //   ClearOldSale(acBuf);

            // Merge Taxes
            //if (fdTax)
            //   lRet = MB_MergeTax(acBuf);

            iRollUpd++;

            // Read next roll record, skip duplicate record
            do
            {
               pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
            } while (pTmp && !memcmp(acBuf, pTmp, iApnLen));
         } else
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
            goto NextRollRec;
         }
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Imp_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Imp_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            //if (fdChar)
            //   lRet = Imp_MergeChar(acRec);

            // Merge Taxes
            //if (fdTax)
            //   lRet = MB_MergeTax(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Read next roll record, skip duplicate record
         do
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         } while (pTmp && !memcmp(acRec, pTmp, iApnLen));
         goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Imp_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Imp_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);

         // Merge Char - awaiting permission from county
         //if (fdChar)
         //   lRet = Imp_MergeChar(acRec);

         // Merge Taxes
         //if (fdTax)
         //   lRet = MB_MergeTax(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);

         // Read next roll record, skip duplicate record
         do
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         } while (pTmp && !memcmp(acRec, pTmp, iApnLen));
      } else
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      lCnt++;

      if (!pTmp || acRollRec[iSkipQuote] > '9')
         break;
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   //if (fdTax)
   //   fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   //LogMsg("Number of Char matched:     %u", lCharMatch);
   //LogMsg("Number of Name matched:     %u", lNameMatch);
   LogMsg("Number of Exe matched:      %u\n", lExeMatch);
   //LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   //LogMsg("Number of Char skiped:      %u", lCharSkip);
   //LogMsg("Number of Name skiped:      %u", lNameSkip);
   LogMsg("Number of Exe skiped:       %u\n", lExeSkip);
   //LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u", lRecCnt);

   return 0;
}

int Imp_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004130013", 9))
   //   acTmp[0] = 0;
#endif
   //
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/********************************* Imp_MergeLien *****************************
 *
 * Keep this for processing old LDR format
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Imp_MergeLienOld(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256];
   long     lTmp;
   double   dTmp;
   int      iRet, iTmp;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "002003001000", 12))
   //   iTmp = 0;
#endif

   iRet = replNull(pRollRec, 32, 0);

   // Parse data
   //iRet = ParseString(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   // 2009 format
   //iRet = ParseString(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   // 2011 format
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_USERID)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L_ASMT], strlen(apTokens[L_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L_FEEPARCEL], strlen(apTokens[L_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "13IMP", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values
   // Land
   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);
   long lFixt   = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   long lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L_TAXAMT1]);
   double dTax2 = atof(apTokens[L_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      if (lTmp > lGross)
      {
         LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }
   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));

   // TRA
   iTmp = atol(apTokens[L_TRA]);
   if (iTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d  ", iTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030330020", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   iTmp = strlen(apTokens[L_USECODE]);
   if (iTmp > 0)
   {
      // Std Usecode
      if (strlen(apTokens[L_USECODE]) == 1)
         iTmp = sprintf(acTmp, "000%s", apTokens[L_USECODE]);
      else if (strlen(apTokens[L_USECODE]) == 2)
         iTmp = sprintf(acTmp, "00%s", apTokens[L_USECODE]);
      else if (strlen(apTokens[L_USECODE]) == 3)
         iTmp = sprintf(acTmp, "0%s", apTokens[L_USECODE]);
      else
         strcpy(acTmp, apTokens[L_USECODE]);

      vmemcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO, iTmp);

      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Units

   // Owner
   Imp_MergeOwner(pOutbuf, apTokens[L_OWNER]);

   // Situs
   Imp_MergeSitus(pOutbuf, apTokens[L_SITUS1], apTokens[L_SITUS2]);

   // Mailing
   Imp_MergeMAdr(pOutbuf, apTokens[L_MAILADDRESS1], apTokens[L_MAILADDRESS2], apTokens[L_MAILADDRESS3], apTokens[L_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L_TAXABILITY], true, true);

   return 0;
}

/******************************** Imp_MergeLien *******************************
 *
 * This version is to create R01 record from lien roll 2017-2018 Secured Roll.txt
 *
 ******************************************************************************/

//int Imp_MergeLien(char *pOutbuf, char *pRollRec)
//{
//   char     acTmp[256], acTmp1[256];
//   double   dTmp;
//   long     lTmp;
//   int      iRet=0, iTmp;
//
//   // Remove NULL
//   replStrAll(pRollRec, "NULL", "");
//
//   // Parse input
//   if (!iSkipQuote)
//      iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
//   else
//      iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
//   if (iRet < IMP_ROLL_TAXABILITY)
//   {
//      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
//      return -1;
//   }
//
//#ifdef _DEBUG
//   //if (!memcmp(apTokens[MB_ROLL_ASMT], "054605041", 9))
//   //   iTmp = 0;
//#endif
//
//   // Ignore APN starts with 800-999 except 910 (MH)
//   iTmp = atoin(apTokens[iApnFld], 3);
//   if (!iTmp || (iTmp >= 800 && iTmp != 910  && iTmp != 920))
//      return 1;
//
//   if (*apTokens[IMP_ROLL_STATUS] == 'I')
//      return 1;
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//
//   // Start copying data
//   memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
//
//   // Format APN
//   iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//   // Apply FeeParcel to AltApn
//   if (strlen(apTokens[IMP_ROLL_FEEPARCEL]) != iApnLen)
//      LogMsg("*** Invalid FEEPARCEL length on APN= %s", apTokens[iApnFld]);
//   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[IMP_ROLL_FEEPARCEL], iApnLen);
//
//   // Create MapLink and output new record
//   iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // Create index map link
//   if (getIndexPage(acTmp, acTmp1, &myCounty))
//      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//   // County code
//   memcpy(pOutbuf+OFF_CO_NUM, "13IMP", 5);
//   if (*apTokens[IMP_ROLL_STATUS] > ' ')
//      *(pOutbuf+OFF_STATUS) = *apTokens[IMP_ROLL_STATUS];
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // TRA
//   iTmp = atoi(apTokens[IMP_ROLL_TRA]);
//   if (iTmp > 0)
//      vmemcpy(pOutbuf+OFF_TRA, apTokens[IMP_ROLL_TRA], SIZ_TRA);
//
//   // Land
//   long lLand = atoi(apTokens[IMP_ROLL_LAND]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = atoi(apTokens[IMP_ROLL_IMPR]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//   }
//   // PP
//   long lPPVal = atoi(apTokens[IMP_ROLL_PPVALUE]);
//   if (lPPVal > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_PERSPROP, lPPVal);
//      memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
//   }
//   // Growing
//   long lGrow = atoi(apTokens[IMP_ROLL_GROWING]);
//   if (lGrow > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_GR_IMPR, lGrow);
//      memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
//   }
//   // Fixture
//   long lFixtr = atoi(apTokens[IMP_ROLL_FIXTR]);
//   if (lFixtr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_FIXTR, lFixtr);
//      memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
//   }
//
//   // Other value
//   lTmp = lPPVal+lGrow+lFixtr;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//   }
//
//   // Gross total
//   lTmp += lLand+lImpr;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//   // Legal
//   if (*apTokens[IMP_ROLL_LEGAL] > ' ')
//   {
//      remUnPrtChar(apTokens[IMP_ROLL_LEGAL]);
//      iTmp = updateLegal(pOutbuf, apTokens[IMP_ROLL_LEGAL]);
//   }
//
//   // Tax code
//   if (memcmp(apTokens[IMP_ROLL_TAXABILITY], "000", 3) > 0)
//      vmemcpy(pOutbuf+OFF_TAX_CODE, apTokens[IMP_ROLL_TAXABILITY], SIZ_TAX_CODE);
//
//   // UseCode
//   sprintf(acTmp, "%s%s", apTokens[IMP_ROLL_USECODE1], apTokens[IMP_ROLL_USECODE2]);
//
//   if (acTmp[0] > ' ')
//   {
//      acTmp[SIZ_USE_CO] = 0;
//      _strupr(acTmp);
//      iTmp = strlen(acTmp);
//      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
//      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
//   } else
//      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
//
//   // Owner
//   try
//   {
//      Imp_MergeOwner(pOutbuf, apTokens[IMP_ROLL_OWNER]);
//   } catch(...)
//   {
//      LogMsg("***** Execption occured in Imp_MergeOwner()");
//   }
//
//   // Mailing - Situs
//   try
//   {
//      Imp_MergeAdr(pOutbuf, CREATE_LIEN);
//   } catch(...)
//   {
//      LogMsg("***** Execption occured in Imp_MergeAdr()");
//   }
//
//   // CareOf
//   if (*apTokens[IMP_ROLL_CAREOF] > ' ')
//      updateCareOf(pOutbuf, apTokens[IMP_ROLL_CAREOF], strlen(apTokens[IMP_ROLL_CAREOF]));
//
//   // DBA
//   if (*apTokens[IMP_ROLL_DBA] > ' ')
//      vmemcpy(pOutbuf+OFF_DBA, apTokens[IMP_ROLL_DBA], SIZ_DBA);
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "054605041", 9))
//   //   iTmp = 0;
//#endif
//   // Recorded Doc
//   if (*apTokens[IMP_ROLL_DOCNUM] > '0' && *apTokens[IMP_ROLL_DOCDATE] > ' ')
//   {
//      iTmp = strlen(apTokens[IMP_ROLL_DOCNUM]);
//      if (iTmp > SIZ_TRANSFER_DOC)
//      {
//         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[IMP_ROLL_DOCNUM], 4);
//         memcpy(pOutbuf+OFF_TRANSFER_DOC+4, &apTokens[IMP_ROLL_DOCNUM][5], iTmp-5);
//      } else
//         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[IMP_ROLL_DOCNUM], iTmp);
//
//      if (apTokens[IMP_ROLL_DOCDATE][4] == '-')
//         dateConversion(apTokens[IMP_ROLL_DOCDATE], acTmp, YYYY_MM_DD);
//      else
//         dateConversion(apTokens[IMP_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
//
//      if (acTmp[0] > ' ')
//      {
//         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
//         lTmp = atol(acTmp);
//         if (lTmp > lLastRecDate && lTmp < lToday)
//            lLastRecDate = lTmp;
//      }
//   }
//
//   // Acres
//   dTmp = atof(apTokens[IMP_ROLL_ACRES]);
//   if (dTmp > 0.0)
//   {
//      lLotSqftCount++;
//
//      // Lot Sqft
//      lTmp = (long)(dTmp * SQFT_PER_ACRE);
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      // Format Acres
//      lTmp = (long)(dTmp * ACRES_FACTOR);
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   // SetTaxcode, Prop8 flag, FullExe flag
//   iTmp = updateTaxCode(pOutbuf, apTokens[IMP_ROLL_TAXABILITY], true, true);
//
//   return 0;
//}

/******************************* Imp_MergeLien1 *******************************
 *
 * This version is to create R01 record from lien roll 2017-2018 Secured Roll.txt
 *
 ******************************************************************************/

int Imp_MergeLien1(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256];
   double   dTmp;
   int      iRet=0, iTmp;
   unsigned long lTmp;

   // Remove NULL
   replStrAll(pRollRec, "NULL", "");

   // Parse input
   if (!iSkipQuote)
      iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < IMP_ROLL_TAXABILITY)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_ROLL_ASMT], "003200061000", 9))
   //   iTmp = 0;
#endif

   // Ignore APN starts with 800-999 except 910 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910  && iTmp != 920))
      return 1;

   if (*apTokens[IMP_ROLL_STATUS] == 'I')
      return 1;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));

   // Format APN
   iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Apply FeeParcel to AltApn
   if (strlen(apTokens[IMP_ROLL_FEEPARCEL]) != iApnLen)
      LogMsg("*** Invalid FEEPARCEL length on APN= %s", apTokens[iApnFld]);
   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[IMP_ROLL_FEEPARCEL], iApnLen);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "13IMP", 5);
   if (*apTokens[IMP_ROLL_STATUS] > ' ')
      *(pOutbuf+OFF_STATUS) = *apTokens[IMP_ROLL_STATUS];

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // TRA
   iTmp = atoi(apTokens[IMP_ROLL_TRA]);
   if (iTmp > 0)
      vmemcpy(pOutbuf+OFF_TRA, apTokens[IMP_ROLL_TRA], SIZ_TRA);

   // Land
   long lLand = atoi(apTokens[IMP_ROLL_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[IMP_ROLL_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }
   // PP - This is actually PPMH.  Real PP is from Assestment Summary file
   long lPPVal = atoi(apTokens[IMP_ROLL_PPVALUE]);
   if (lPPVal > 0)
   {
      sprintf(acTmp, "%*u", SIZ_PP_MH, lPPVal);
      memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
   }
   // Growing
   long lGrow = atoi(apTokens[IMP_ROLL_GROWING]);
   if (lGrow > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GR_IMPR, lGrow);
      memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
   }

   // Other value
   lTmp = lPPVal+lGrow;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Legal
   if (*apTokens[IMP_ROLL_LEGAL] > ' ')
   {
      remUnPrtChar(apTokens[IMP_ROLL_LEGAL]);
      iTmp = updateLegal(pOutbuf, apTokens[IMP_ROLL_LEGAL]);
   }

   // Tax code
   if (memcmp(apTokens[IMP_ROLL_TAXABILITY], "000", 3) > 0)
      vmemcpy(pOutbuf+OFF_TAX_CODE, apTokens[IMP_ROLL_TAXABILITY], SIZ_TAX_CODE);

   // UseCode
   sprintf(acTmp, "%s%s", apTokens[IMP_ROLL_USECODE1], apTokens[IMP_ROLL_USECODE2]);

   if (acTmp[0] > ' ')
   {
      acTmp[SIZ_USE_CO] = 0;
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   try
   {
      Imp_MergeOwner(pOutbuf, apTokens[IMP_ROLL_OWNER]);
   } catch(...)
   {
      LogMsg("***** Execption occured in Imp_MergeOwner()");
   }

   // Mailing - Situs
   try
   {
      Imp_MergeMAdr1(pOutbuf);
   } catch(...)
   {
      LogMsg("***** Execption occured in Imp_MergeAdr()");
   }

   // CareOf
   if (*apTokens[IMP_ROLL_CAREOF] > ' ')
      updateCareOf(pOutbuf, apTokens[IMP_ROLL_CAREOF], strlen(apTokens[IMP_ROLL_CAREOF]));

   // DBA
   if (*apTokens[IMP_ROLL_DBA] > ' ')
      vmemcpy(pOutbuf+OFF_DBA, apTokens[IMP_ROLL_DBA], SIZ_DBA);

   // Recorded Doc
   if (*apTokens[IMP_ROLL_DOCNUM] > '0' && *apTokens[IMP_ROLL_DOCDATE] > ' ')
   {
      iTmp = strlen(apTokens[IMP_ROLL_DOCNUM]);
      if (iTmp > SIZ_TRANSFER_DOC)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[IMP_ROLL_DOCNUM], 4);
         memcpy(pOutbuf+OFF_TRANSFER_DOC+4, &apTokens[IMP_ROLL_DOCNUM][5], iTmp-5);
      } else
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[IMP_ROLL_DOCNUM], iTmp);

      if (apTokens[IMP_ROLL_DOCDATE][4] == '-')
         dateConversion(apTokens[IMP_ROLL_DOCDATE], acTmp, YYYY_MM_DD);
      else
         dateConversion(apTokens[IMP_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);

      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         lTmp = atol(acTmp);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001460004000", 9))
   //   iTmp = 0;
#endif
   // Acres
   dTmp = atof(apTokens[IMP_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      lLotSqftCount++;

      // Lot Sqft
      lTmp = (unsigned long)(dTmp * SQFT_PER_ACRE);
      if (lTmp < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      } else
         LogMsg("*** MergeLien1->Ignore LotSqft too big %u [%.12s]", lTmp, pOutbuf);

      // Format Acres
      lTmp = (unsigned long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[IMP_ROLL_TAXABILITY], true, true);

   return 0;
}

/******************************** Imp_MergeMH *********************************
 *
 * Create R01 record from mobilehome file "2019-2020 Mobile Homes.txt"
 *
 ******************************************************************************/

int Imp_MergeMH(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256];
   long     lTmp;
   int      iRet=0, iTmp;

   // Parse input
   if (!iSkipQuote)
      iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < IMP_MH_FLDS)
   {
      LogMsg("***** Error: bad input record for FeeParcel=%s (%d)", apTokens[0], iRet);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_ROLL_ASMT], "054605041", 9))
   //   iTmp = 0;
#endif

   if (*apTokens[IMP_MH_STATUS] == 'I')
      return 1;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   vmemcpy(pOutbuf, apTokens[IMP_MH_APN], iApnLen);

   // Format APN
   iRet = formatApn(apTokens[IMP_MH_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Apply FeeParcel to AltApn
   if (strlen(apTokens[IMP_MH_FEEPARCEL]) != iApnLen)
      LogMsg("*** Invalid FEEPARCEL length on APN= %s", apTokens[iApnFld]);
   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[IMP_MH_FEEPARCEL], iApnLen);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[IMP_MH_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "13IMP", 5);
   if (*apTokens[IMP_MH_STATUS] > ' ')
      *(pOutbuf+OFF_STATUS) = *apTokens[IMP_MH_STATUS];

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[IMP_MH_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[IMP_MH_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // PP
   long lPPVal = atoi(apTokens[IMP_MH_PPMH]);
   if (lPPVal > 0)
   {
      sprintf(acTmp, "%*d", SIZ_PERSPROP, lPPVal);
      memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PERSPROP);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
   }

   // Gross total
   lTmp = lLand+lImpr+lPPVal;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Legal
   if (*apTokens[IMP_MH_LEGAL] > ' ')
   {
      remUnPrtChar(apTokens[IMP_MH_LEGAL]);
      iTmp = updateLegal(pOutbuf, apTokens[IMP_MH_LEGAL]);
   }

   // Owner
   try
   {
      Imp_MergeOwner(pOutbuf, apTokens[IMP_MH_OWNER]);
   } catch(...)
   {
      LogMsg("***** Execption occured in Imp_MergeOwner()");
   }

   // Mailing
   ADR_REC sMailAdr;
   if (*apTokens[IMP_MH_M_ADDR] >  ' ')
   {
      memset(&sMailAdr, 0, sizeof(ADR_REC));
      parseMAdr1(&sMailAdr, apTokens[IMP_MH_M_ADDR]);
      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Mail city/st
   if (*apTokens[IMP_MH_M_CITY] >=  'A')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[IMP_MH_M_CITY], SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, apTokens[IMP_MH_M_STATE], SIZ_M_ST);
      vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[IMP_MH_M_ZIP], SIZ_M_ZIP);
   }

   // CareOf
   if (!memcmp(apTokens[IMP_MH_M_ADDR], "C/O", 3))
   {
      updateCareOf(pOutbuf, apTokens[IMP_MH_M_ADDR], strlen(apTokens[IMP_MH_M_ADDR]));
      *apTokens[IMP_MH_M_ADDR] = 0;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "054605041", 9))
   //   iTmp = 0;
#endif

   return 0;
}

/******************************** Imp_MergePP ********************************
 *
 * Add PPVal using data from Asset Summary file.
 *
 *****************************************************************************/

int Imp_MergePP(char *pOutbuf, FILE *fdAst)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *apItems[16];
   long     lTmp, lOther;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      pRec = fgets(acRec, 512, fdAst);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "038250005000", 9))
   //   iTmp = 0;
#endif
   do
   {
      if (!pRec)
      {
         fclose(fdAst);
         fdAst = NULL;
         return 1;      // EOF
      }

      // Skip quote to copare
      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Ast rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdAst);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringNQ(pRec, cDelim, 16, apItems);
   if (iRet < 2)
   {
      LogMsg("***** Error: bad Asset record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   long lPP = atol(apItems[1]);
   if (lPP > 0)
   {
      sprintf(acTmp, "%*d", SIZ_PERSPROP, lPP);
      memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      lOther = lPP + atoin(pOutbuf+OFF_OTHER, SIZ_OTHER);
      sprintf(acTmp, "%*d", SIZ_OTHER, lOther);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      lTmp = atoin(pOutbuf+OFF_GROSS, SIZ_GROSS);
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp + lPP); 
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      lAstMatch++;
   }

   // Get next record
   pRec = fgets(acRec, 512, fdAst);

   return 0;
}

/******************************** Imp_MergeExe *******************************
 *
 * 2017 LDR - We now do sort/merge in Load_LDR to save time.
 *
 *****************************************************************************/

int Imp_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdExe);
      if (!isdigit(*pRec))
         pRec = fgets(acRec, 512, fdExe);
      if (*pRec == '"')
         iSkipQuote = 1;
      else
         iSkipQuote = 0;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "038250005000", 9))
   //   iTmp = 0;
#endif
   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';
         return 1;      // EOF
      }

      // Skip quote to copare
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   iRet = ParseStringNQ(pRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < IMP_EXE_PCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe - 2010 values are "True" or "False".  Prior year are "1" or "0"
   // 2015: 1|0, 2016: -1|0
   if (*apTokens[IMP_EXE_HOEXE] == '0' || *apTokens[IMP_EXE_HOEXE] == 'F')
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
   else
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'

   // Exe Amt
   if (!memcmp(apTokens[IMP_EXE_EXEAMT], "9999999", 7))
   {
      // Full exempt
      memcpy(pOutbuf+OFF_EXE_TOTAL, pOutbuf+OFF_GROSS, SIZ_EXE_TOTAL);
   } else
   {
      lTmp = atol(apTokens[IMP_EXE_EXEAMT]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Imp_MergeExe2 *******************************
 *
 * 2020 LDR
 *
 *****************************************************************************/

int Imp_MergeExe2(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      pRec = fgets(acRec, 512, fdExe);
      if (!isdigit(*pRec))
         pRec = fgets(acRec, 512, fdExe);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "038250005000", 9))
   //   iTmp = 0;
#endif
   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';
         return 1;      // EOF
      }

      // Skip quote to copare
      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   iRet = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < IMP_EXE_USERID)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe - 2020 values are "True" or "False"
   if (*apTokens[IMP_EXE_ISHOMEOWNERS] == 'Y')
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
   else
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'

   // Exe Amt
   if (!memcmp(apTokens[IMP_EXE_MAXIMUMAMT], "9999999", 7))
   {
      // Full exempt
      memcpy(pOutbuf+OFF_EXE_TOTAL, pOutbuf+OFF_GROSS, SIZ_EXE_TOTAL);
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   } else
   {
      lTmp = atol(apTokens[IMP_EXE_MAXIMUMAMT]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }
   }

   // Exemption code
   vmemcpy(pOutbuf+OFF_EXE_CD1, apTokens[IMP_EXE_EXEMPTIONCODE], SIZ_EXE_CD1);

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/****************************** Imp_MergeValue2 ******************************
 *
 * 2020 LDR
 *
 *****************************************************************************/

int Imp_MergeValue2(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      pRec = fgets(acRec, 512, fdValue);
      if (!isdigit(*pRec))
         pRec = fgets(acRec, 512, fdValue);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "038250005000", 9))
   //   iTmp = 0;
#endif
   do
   {
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         return 1;      // EOF
      }

      // Skip quote to copare
      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Value rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdValue);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < IMP_VAL_USERID)
   {
      LogMsg("***** Error: bad Value record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdValue);
      return -1;
   }

   // Land
   long lLand = atoi(apTokens[IMP_VAL_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[IMP_VAL_STRUCTURE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Fixtures - currently no data 07/27/2020
   long lFixt = atoi(apTokens[IMP_VAL_FIXTURES]);
   if (lFixt > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIXTR, lFixt);
      memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
   }

   // Fixtures - currently no data 07/27/2020
   long lFixtRP = atoi(apTokens[IMP_VAL_FIXTURESREALPROPERTY]);
   if (lFixtRP > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIXTR_RP, lFixtRP);
      memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
   }

   // PPBusiness
   long lPPVal = atoi(apTokens[IMP_VAL_PPBUSINESS]);
   if (lPPVal > 0)
   {
      sprintf(acTmp, "%*d", SIZ_PERSPROP, lPPVal);
      memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
   }

   // PPMH
   long lPPMH = atoi(apTokens[IMP_VAL_PPMH]);
   if (lPPMH > 0)
   {
      sprintf(acTmp, "%*d", SIZ_PP_MH, lPPMH);
      memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
   }

   // Growing
   long lGrow = atoi(apTokens[IMP_VAL_GROWING]);
   if (lGrow > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GR_IMPR, lGrow);
      memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
   }

   // HomeSite
   long lHomesite = atoi(apTokens[IMP_VAL_HOMESITE]);
   if (lHomesite > 0)
   {
      sprintf(acTmp, "%*d", SIZ_HOMESITE, lHomesite);
      memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
   }

   // Other value
   lTmp = lFixt+lFixtRP+lPPVal+lPPMH+lGrow+lHomesite;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdValue);
   lValueMatch++;

   return 0;
}

int Imp_MergeLdrExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdExe);
      if (*pRec == '"')
         iSkipQuote = 1;
      else
         iSkipQuote = 0;
      if (!isdigit(*(pRec+1)))
         pRec = fgets(acRec, 512, fdExe);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001010017", 9))
   //   iTmp = 0;
#endif
   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';
         return 1;      // EOF
      }

      // Skip quote to copare
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   iRet = ParseStringNQ(pRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < IMP_EXE_PCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe - 2010 values are "True" or "False".  Prior year are "1" or "0"
   // 2015: 1|0, 2016: -1|0
   if (*apTokens[IMP_EXE_HOEXE] == '0' || *apTokens[IMP_EXE_HOEXE] == 'F')
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
   else
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'

   // Exe Amt
   if (!memcmp(apTokens[IMP_EXE_EXEAMT], "9999999", 7))
   {
      // Full exempt
      memcpy(pOutbuf+OFF_EXE_TOTAL, pOutbuf+OFF_GROSS, SIZ_EXE_TOTAL);
   } else
   {
      lTmp = atol(apTokens[IMP_EXE_EXEAMT]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }
   }

   if (*apTokens[IMP_EXE_EXECODE] > ' ')
      vmemcpy(pOutbuf+OFF_EXE_CD1, apTokens[IMP_EXE_EXECODE], SIZ_EXE_CD1);

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&IMP_Exemption);

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/********************************* Imp_Load_LDR *****************************
 *
 * Input file has to be sorted before processing.
 *
 ****************************************************************************/

//int Imp_Load_LDR(int iFirstRec /* 1=create header rec */)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acSortCmd[256],
//            acAstFile[_MAX_PATH], acOutFile[_MAX_PATH], *pCnty=myCounty.acCntyCode;
//
//   HANDLE   fhOut;
//   FILE     *fdAst;
//
//   int      iRet, iNewRec=0, iRetiredRec=0;
//   DWORD    nBytesWritten;
//   BOOL     bRet;
//   long     lRet=0, lCnt=0;
//
//   // Get lien file
//   GetIniString(pCnty, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
//   LogMsg0("Loading LDR file %s", acBuf);
//
//   // Sort input file on ASMT
//   sprintf(acRollFile, "%s\\%s\\%s_Lien.srt", acTmpPath, pCnty, pCnty);
//   sprintf(acSortCmd, "S(#2,C,A) F(TXT) DEL(%d)", cLdrSep);
//   iRet = sortFile(acBuf, acRollFile, acSortCmd);
//   if (!iRet) return -1;
//
//   // Open roll file
//   LogMsg("Open Roll file %s", acRollFile);
//   fdRoll = fopen(acRollFile, "r");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acRollFile);
//      return -1;
//   }
//
//   // Get exemption file
//   GetIniString(pCnty, "LienExe", "", acBuf, _MAX_PATH, acIniFile);
//   sprintf(acExeFile, "%s\\%s\\%s_Exe.srt", acTmpPath, pCnty, pCnty);
//   sprintf(acSortCmd, "S(#1,C,A) F(TXT) DEL(%d)", cLdrSep);
//   iRet = sortFile(acBuf, acExeFile, acSortCmd);
//   if (!iRet) return -2;
//
//   // Open Exemption file
//   LogMsg("Open Exe file: %s", acExeFile);
//   fdExe = fopen(acExeFile, "r");
//   if (fdExe == NULL)
//   {
//      LogMsg("***** Error opening exemption file: %s\n", acExeFile);
//      return -2;
//   }
//
//   // Get Asset summary file
//   GetIniString(pCnty, "AssetFile", "", acBuf, _MAX_PATH, acIniFile);
//   sprintf(acAstFile, "%s\\%s\\%s_Ast.srt", acTmpPath, pCnty, pCnty);
//   sprintf(acSortCmd, "S(#2,C,A) F(TXT) OMIT(#1,C,NE,\"%d\",OR,#4,C,EQ,\"0\",OR,#5,C,NE,\"A\") OUTREC(#2,\"|\",#4,CRLF) DEL(%d)", lLienYear, cDelim);
//   iRet = sortFile(acBuf, acAstFile, acSortCmd);
//   if (!iRet) return -2;
//   LogMsg("Open Asset Summary file: %s", acAstFile);
//   fdAst = fopen(acAstFile, "r");
//   if (fdAst == NULL)
//   {
//      LogMsg("***** Error opening Asset Summary file: %s\n", acAstFile);
//      return -2;
//   }
//
//   // Open Output file
//   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error creating output file: %s\n", acOutFile);
//      return -2;
//   }
//
//   // Output first header record
//   if (iFirstRec > 0)
//   {
//      memset(acBuf, '9', iRecLen);
//      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//   }
//
//   // Skip header
//   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
//   // Get first RollRec
//   iRet = myGetStrEQ(acRollRec, MAX_RECSIZE, fdRoll);
//
//   // Init variables
//   iNoMatch=iBadCity=iBadSuffix=0;
//   lExeMatch=lAstMatch=0;
//
//   // Merge loop
//   while (iRet > 0)
//   {
//      lLDRRecCount++;
//
//      // Create new R01 record
//      iRet = Imp_MergeLien(acBuf, acRollRec);
//      if (!iRet)
//      {
//         iNewRec++;
//
//         // Add Exemption
//         if (fdExe)
//            Imp_MergeLdrExe(acBuf);
//
//         // Add PPVal
//         if (fdAst)
//            Imp_MergePP(acBuf, fdAst);
//
//         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//         if (!(++lCnt % 1000))
//            printf("\r%u", lCnt);
//
//         if (!bRet)
//         {
//            LogMsg("***** Error writing to output file at record %d\n", lCnt);
//            lRet = WRITE_ERR;
//            break;
//         }
//      } else
//         iRetiredRec++;
//
//      // Get next roll record
//      //pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
//      iRet = myGetStrEQ(acRollRec, MAX_RECSIZE, fdRoll);
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fdExe)
//      fclose(fdExe);
//   if (fdAst)
//      fclose(fdAst);
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   LogMsg("Total input records:        %u", lLDRRecCount);
//   LogMsg("      output records:       %u", lCnt);
//   LogMsg("      Exe matched:          %u", lExeMatch);
//   LogMsg("      PP matched:           %u\n", lAstMatch);
//
//   LogMsg("Total bad input records:    %u", iRetiredRec);
//   LogMsg("Total bad-city records:     %u", iBadCity);
//   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
//   LogMsg("Last recording date:        %u", lLastRecDate);
//   printf("\nTotal output records: %u\n", lCnt);
//
//   lRecCnt = lCnt;
//   return lRet;
//}

/******************************** Imp_Load_LDR1 *****************************
 *
 * Load LDR file first, then add MH file for LDR 2019
 *
 * Return 0 if successful.
 *
 ****************************************************************************/

int Imp_Load_LDR1(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acSortCmd[256],
            acAstFile[_MAX_PATH], acOutFile[_MAX_PATH], *pCnty=myCounty.acCntyCode;

   HANDLE   fhOut;
   FILE     *fdAst;

   int      iRet, iMHRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   // Get lien file
   GetIniString(pCnty, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
   LogMsg0("Loading LDR file %s", acBuf);

   // Sort input file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_Lien.srt", acTmpPath, pCnty, pCnty);
   sprintf(acSortCmd, "S(#2,C,A) OMIT(#1,C,GT,\"999\") DUPO(1,34) F(TXT) DEL(%d)", cLdrSep);
   iRet = sortFile(acBuf, acRollFile, acSortCmd);
   if (!iRet) return -1;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Get exemption file
   GetIniString(pCnty, "LienExe", "", acBuf, _MAX_PATH, acIniFile);
   sprintf(acExeFile, "%s\\%s\\%s_Exe.srt", acTmpPath, pCnty, pCnty);
   sprintf(acSortCmd, "S(#1,C,A) F(TXT) DEL(%d)", cLdrSep);
   iRet = sortFile(acBuf, acExeFile, acSortCmd);
   if (!iRet) return -2;

   // Open Exemption file
   LogMsg("Open Exe file: %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening exemption file: %s\n", acExeFile);
      return -2;
   }

   // Get Asset summary file
   GetIniString(pCnty, "AssetFile", "", acBuf, _MAX_PATH, acIniFile);
   sprintf(acAstFile, "%s\\%s\\%s_Ast.srt", acTmpPath, pCnty, pCnty);
   sprintf(acSortCmd, "S(#2,C,A) F(TXT) OMIT(#1,C,NE,\"%d\",OR,#4,C,EQ,\"0\",OR,#5,C,NE,\"A\") OUTREC(#2,\"|\",#4,CRLF) DEL(%d)", lLienYear, cDelim);
   iRet = sortFile(acBuf, acAstFile, acSortCmd);
   if (!iRet) return -2;
   LogMsg("Open Asset Summary file: %s", acAstFile);
   fdAst = fopen(acAstFile, "r");
   if (fdAst == NULL)
   {
      LogMsg("***** Error opening Asset Summary file: %s\n", acAstFile);
      return -2;
   }

   // Open Situs file - need sorting
   strcpy(acRollRec, acSitusFile);
   sprintf(acSitusFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   // Sort on APN ascending, Seq# decending
   sprintf(acBuf, "S(#1,C,A,#9,C,D) DEL(%d) F(TXT)", cDelim);
   iRet = sortFile(acRollRec, acSitusFile, acBuf);
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting Situs file %s to %s", acRollRec, acSitusFile);
      return -2;
   }

   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

      // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -2;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Skip header
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   // Get first RollRec
   //iRet = myGetStrEQ(acRollRec, MAX_RECSIZE, fdRoll);
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   lExeMatch=lAstMatch=0;

   // Merge loop
   //while (iRet > 0)
   while (pTmp)
   {
      // Create new R01 record
      iRet = Imp_MergeLien1(acBuf, acRollRec);
      if (!iRet)
      {
         // Add Exemption
         if (fdExe)
            Imp_MergeLdrExe(acBuf);

         // Add PPVal
         if (fdAst)
            Imp_MergePP(acBuf, fdAst);

         // Merge Situs
         if (fdSitus)
            iRet = Imp_MergeSitus2(acBuf);       // 2022
            //lRet = Imp_MergeSitus(acBuf);

         // Add Characteristic
         if (fdChar)
            Imp_MergeStdChar(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else
      {
         iRetiredRec++;
         LogMsg("*** Retired parcel: %s", acRollRec);
      }

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp || *pTmp > '9')
         break;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);

   // Load mobilehome file
   GetIniString(pCnty, "LienMH", "", acBuf, _MAX_PATH, acIniFile);
   LogMsg0("Loading Mobilehome file %s", acBuf);
   LogMsg("Open Mobilehome file %s", acBuf);
   fdRoll = fopen(acBuf, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening Mobilehome file: %s\n", acBuf);
      return -1;
   }

   // Skip header
   pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   while (pTmp)
   {
      // Create new R01 record
      iRet = Imp_MergeMH(acBuf, acRollRec);
      if (!iRet)
      {
         iMHRec++;

         // Add Exemption
         if (fdExe)
            Imp_MergeLdrExe(acBuf);

         // Add PPVal
         if (fdAst)
            Imp_MergePP(acBuf, fdAst);

         // Merge Situs
         if (fdSitus)
            iRet = Imp_MergeSitus2(acBuf);       // 2022
            //iRet = Imp_MergeSitus(acBuf);

         // Add Characteristic
         if (fdChar)
            Imp_MergeStdChar(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else
      {
         iRetiredRec++;
         LogMsg("*** Retired parcel: %s", acRollRec);
      }

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp || *pTmp > '9')
         break;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdExe)
      fclose(fdExe);
   if (fdAst)
      fclose(fdAst);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("      output records:       %u", lLDRRecCount);
   LogMsg("      MH records:           %u", iMHRec);
   LogMsg("      Situs matched:        %u", lSitusMatch);
   LogMsg("      Exe matched:          %u", lExeMatch);
   LogMsg("      Char matched:         %u", lCharMatch);
   LogMsg("      PP matched:           %u\n", lAstMatch);

   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return lRet;
}

/******************************** Imp_Load_LDR2 *****************************
 *
 * Load "2020-2021 Secured Roll.txt" file first, then add "2020-2021 Mobile Homes.txt" for LDR 2020
 *
 * Return 0 if successful.
 *
 ****************************************************************************/

int Imp_Load_LDR2(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acSortCmd[256],
            acOutFile[_MAX_PATH], *pCnty=myCounty.acCntyCode;

   HANDLE   fhOut;

   int      iRet, iMHRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   // Get lien file
   GetIniString(pCnty, "LienFile", "", acBuf, _MAX_PATH, acIniFile);

   // Sort input file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_Lien.srt", acTmpPath, pCnty, pCnty);
   sprintf(acSortCmd, "S(#2,C,A) F(TXT) DEL(%d)", cLdrSep);
   iRet = sortFile(acBuf, acRollFile, acSortCmd);
   if (!iRet) return -1;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Get exemption file
   GetIniString(pCnty, "LienExe", "", acBuf, _MAX_PATH, acIniFile);
   sprintf(acExeFile, "%s\\%s\\%s_Exe.srt", acTmpPath, pCnty, pCnty);
   sprintf(acSortCmd, "S(#1,C,A) F(TXT) DEL(%d)", cLdrSep);
   iRet = sortFile(acBuf, acExeFile, acSortCmd);
   if (!iRet) return -2;

   // Open Exemption file
   LogMsg("Open Exe file: %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening exemption file: %s\n", acExeFile);
      return -2;
   }

   // Open Situs file - need sorting
   strcpy(acRollRec, acSitusFile);
   sprintf(acSitusFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sorting %s to %s", acRollRec, acSitusFile);
   // Sort on APN ascending, Seq# decending
   sprintf(acSortCmd, "S(#1,C,A,#2,C,D) OMIT(#3,C,NE,\"A\") DEL(%d) F(TXT)", cDelim);
   iRet = sortFile(acRollRec, acSitusFile, acSortCmd);
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting Situs file %s to %s", acRollRec, acSitusFile);
      return -2;
   }

   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -2;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first record
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   lExeMatch=lAstMatch=0;

   // Merge loop
   //while (iRet > 0)
   while (pTmp)
   {
      // Create new R01 record
      iRet = Imp_MergeLien1(acBuf, acRollRec);
      if (!iRet)
      {
         // Add Exemption
         if (fdExe)
            Imp_MergeLdrExe(acBuf);

         // Add Characteristic
         if (fdChar)
            Imp_MergeStdChar(acBuf);

         // Merge Situs
         if (fdSitus)
            lRet = Imp_MergeSitus2(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else if (isdigit(acRollRec[1]))
      {
         iRetiredRec++;
         LogMsg("*** Ignore parcel: %s", acRollRec);
      }

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      //iRet = myGetStrEQ(acRollRec, MAX_RECSIZE, fdRoll);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);

   // Load mobilehome file
   GetIniString(pCnty, "LienMH", "", acBuf, _MAX_PATH, acIniFile);
   LogMsg0("Loading Mobilehome file %s", acBuf);
   LogMsg("Open Mobilehome file %s", acBuf);
   fdRoll = fopen(acBuf, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening Mobilehome file: %s\n", acBuf);
      return -1;
   }

   // Skip header
   pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   lCnt = 1;
   while (pTmp)
   {
      // Create new R01 record
      iRet = Imp_MergeMH(acBuf, acRollRec);
      if (!iRet)
      {
         iMHRec++;

         // Add Exemption
         if (fdExe)
            Imp_MergeLdrExe(acBuf);

         // Add Characteristic
         if (fdChar)
            Imp_MergeStdChar(acBuf);

         // Merge Situs
         if (fdSitus)
            lRet = Imp_MergeSitus2(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else if (isdigit(acRollRec[1]))
      {
         iRetiredRec++;
         LogMsg("*** Ignore parcel: %s", acRollRec);
      }

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdExe)
      fclose(fdExe);
   if (fdSitus)
      fclose(fdSitus);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("      output records:       %u", lLDRRecCount);
   LogMsg("      MH records:           %u", iMHRec);
   LogMsg("      Situs matched:        %u", lSitusMatch);
   LogMsg("      Exe matched:          %u", lExeMatch);
   LogMsg("      Char matched:         %u", lCharMatch);
   LogMsg("      PP matched:           %u\n", lAstMatch);

   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return lRet;
}

/****************************** Imp_AddLienPP ********************************
 *
 *
 *****************************************************************************/

int Imp_AddLienPP(char *pOutbuf, FILE *fdAst)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *apItems[16];
   long     lTmp, lOther;
   int      iRet=0, iTmp;
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Get rec
   if (!pRec)
   {
      pRec = fgets(acRec, 512, fdAst);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "038250005000", 9))
   //   iTmp = 0;
#endif
   do
   {
      if (!pRec)
      {
         fclose(fdAst);
         fdAst = NULL;
         return 1;      // EOF
      }

      // Skip quote to copare
      iTmp = memcmp(pLienExtr->acApn, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Ast rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdAst);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringNQ(pRec, cDelim, 16, apItems);
   if (iRet < 2)
   {
      LogMsg("***** Error: bad Asset record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   long lPP = atol(apItems[1]);
   if (lPP > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_PERS, lPP);
      memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_PERS);
      lOther = lPP + atoin(pLienExtr->acOther, SIZ_LIEN_OTHERS);
      sprintf(acTmp, "%*d", SIZ_LIEN_OTHERS, lOther);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_OTHERS);
      lTmp = atoin(pLienExtr->acGross, SIZ_LIEN_GROSS);
      sprintf(acTmp, "%*d", SIZ_LIEN_GROSS, lTmp + lPP);
      memcpy(pLienExtr->acGross, acTmp, SIZ_LIEN_GROSS);

      lAstMatch++;
   }

   // Get next record
   pRec = fgets(acRec, 512, fdAst);

   return 0;
}

/****************************** Imp_AddLienExe *******************************
 *
 *
 *****************************************************************************/

int Imp_AddLienExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *apItems[10];
   long     lTmp;
   int      iRet=0, iTmp;
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Get rec
   if (!pRec)
   {
      // Skip header record as needed
      pRec = fgets(acRec, 512, fdExe);
      if (!isdigit(*pRec))
         pRec = fgets(acRec, 512, fdExe);
      if (*pRec == '"')
         iSkipQuote = 1;
      else
         iSkipQuote = 0;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "038250005000", 9) )
   //   iTmp = 0;
   //if (memcmp(pRec+1, "038250005000", 9) > 0)
   //   iTmp = 0;
#endif
   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         pLienExtr->acHO[0] = '2';
         return 1;      // EOF
      }

      // Skip quote to compare
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdExe);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      pLienExtr->acHO[0] = '2';      // 'N'
      return 1;
   }

   iRet = ParseStringNQ(pRec, cLdrSep, 10, apItems);
   if (iRet < IMP_EXE_DTS)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe - 2010 values are "True" or "False".  Prior year are "1" or "0"
   //          2017 are -1 and 0
   if (*apItems[IMP_EXE_HOEXE] == '0' || *apItems[IMP_EXE_HOEXE] == 'F')
      pLienExtr->acHO[0] = '2';      // 'N'
   else
      pLienExtr->acHO[0] = '1';      // 'Y'

   // Exe Amt
   lTmp = atol(apItems[IMP_EXE_EXEAMT]);
   if (!memcmp(apItems[IMP_EXE_EXEAMT], "9999999", 7))
   {
      // Full exempt
      memcpy(pLienExtr->acExAmt, pLienExtr->acGross, SIZ_EXE_TOTAL);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_EXE_TOTAL);
   }

   // Exe code
   vmemcpy(pLienExtr->extra.MB.ExeCode1, apItems[IMP_EXE_EXECODE], SIZ_LIEN_EXECODEX);

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Imp_ExtrLien ******************************
 *
 *
 ****************************************************************************/

int Imp_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < IMP_ROLL_TAXABILITY)
   {
      LogMsg("***** Error: bad input record for APN=%.50s", pRollRec);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[IMP_ROLL_APN], strlen(apTokens[IMP_ROLL_APN]));

   // TRA
   lTmp = atol(apTokens[IMP_ROLL_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[IMP_ROLL_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LIEN_FIXT);
   }

   // Improve
   long lImpr = atoi(apTokens[IMP_ROLL_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_LIEN_FIXT);
   }

   // Personal Property
   long lPP   = atoi(apTokens[IMP_ROLL_PPVALUE]);
   if (lPP > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_OTHERS, lPP);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_OTHERS);

      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
      memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
   }

   // Gross total
   lTmp = lPP+lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      __int64  lBig;

      lBig = (__int64)lImpr*100;
      sprintf(acTmp, "%*d", SIZ_RATIO, lBig/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Prop 8
   lTmp = atoin(apTokens[IMP_ROLL_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Taxability
   if (lTmp > 0)
      vmemcpy(pLienExtr->acTaxCode, apTokens[IMP_ROLL_TAXABILITY], SIZ_LIEN_TAXCODE);

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int Imp_CreateLienRec2(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < IMP_VAL_USERID)
   {
      LogMsg("***** Error: bad input record for APN=%.50s", pRollRec);
      return -1;
   }

   if (*apTokens[IMP_VAL_VALUESETTYPE] != '1')
      return 1;

#ifdef _DEBUG
   //if (!memcmp(apTokens[IMP_VAL_APN], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[IMP_VAL_APN], strlen(apTokens[IMP_VAL_APN]));

   // Status
   //pLienExtr->AsmtStatus = *apTokens[IMP_ASMT_ASMTSTATUS];

   // TRA
   //lTmp = atol(apTokens[IMP_ASMT_TRA]);
   //if (lTmp > 0)
   //{
   //   sprintf(acTmp, "%.6d", lTmp);
   //   memcpy(pLienExtr->acTRA, acTmp, 6);
   //}

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[IMP_VAL_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LIEN_FIXT);
   }

   // Improve
   long lImpr = atol(apTokens[IMP_VAL_STRUCTURE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_LIEN_FIXT);
   }

   // Other values
   long lGrow = atol(apTokens[IMP_VAL_GROWING]);

   // Fixtures
   long lFixt   = atol(apTokens[IMP_VAL_FIXTURES]);

   // Business Property
   long lBP   = atol(apTokens[IMP_VAL_PPBUSINESS]);

   // Personal Property MH
   long lMH   = atol(apTokens[IMP_VAL_PPMH]);

   lTmp = lGrow+lFixt+lBP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lBP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lBP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lBP);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************** Imp_ExtrLien ******************************
 *
 * Extract lien data from PQ_ValueSet.txt.  Add Exe from PQ_Exemption.txt
 *
 ****************************************************************************/

int Imp_ExtrLien2(char *pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg0("Extract LDR value for %s", pCnty);

   // Sort input file on ASMT
   sprintf(acTmpFile, "%s\\%s\\%s_Lien.srt", acTmpPath, pCnty, pCnty);
   sprintf(acBuf, "S(#1,C,A,#2,C,A) OMIT(#1,C,GE,\"A\",OR,#1,C,LT,\"001\") F(TXT) DEL(%d)", cLdrSep);
   iRet = sortFile(acValueFile, acTmpFile, acBuf);
   if (!iRet) return -1;

   LogMsg("Open %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acTmpFile);
      return -1;
   }

   // Get exemption file
   GetIniString(pCnty, "ExeFile", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acExeFile, "%s\\%s\\%s_Exe.srt", acTmpPath, pCnty, pCnty);
   sprintf(acBuf, "S(#1,C,A) F(TXT) DEL(%d)", cDelim);
   iRet = sortFile(acRec, acExeFile, acBuf);
   if (!iRet) return -2;

   // Open Exemption file
   LogMsg("Open Exe file: %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening exemption file: %s\n", acExeFile);
      return -2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Get first rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new record
      iRet = Imp_CreateLienRec2(acBuf, acRec);

      if (!iRet)
      {
         // Add Exemption
         //if (fdExe)
         //   Imp_MergeLienExe(acBuf);

         // Write to output
         fputs(acBuf, fdLienExt);
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdExe)
      fclose(fdExe);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("      records processed:    %u", lCnt);
   LogMsg("      Exe matched:          %u", lExeMatch);
   printf("\nTotal output records: %u", iNewRec);

   lRecCnt = iNewRec;
   return 0;
}

/******************************** Imp_MergeMH *********************************
 *
 * Create R01 record from mobilehome file "2019-2020 Mobile Homes.txt"
 *
 ******************************************************************************/

int Imp_ExtrMH(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp;
   int      iRet;
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse input
   if (!iSkipQuote)
      iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < IMP_MH_FLDS)
   {
      LogMsg("***** Error: bad input record for FeeParcel=%s (%d)", apTokens[0], iRet);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[MB_ROLL_ASMT], "054605041", 9))
   //   iTmp = 0;
#endif

   if (*apTokens[IMP_MH_STATUS] == 'I')
      return 1;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   vmemcpy(pLienExtr->acApn, apTokens[IMP_MH_APN], iApnLen);

   // Land
   long lLand = atoi(apTokens[IMP_MH_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[IMP_MH_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // PP
   long lPPVal = atoi(apTokens[IMP_MH_PPMH]);
   if (lPPVal > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPPVal);
      memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_OTHERS);
   }

   // Gross total
   lTmp = lLand+lImpr+lPPVal;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "054605041", 9))
   //   iTmp = 0;
#endif

   return 0;
}

/******************************** Imp_ExtrLien ******************************
 *
 * Extract lien data from "Secured Roll.txt".  Add Exe & PP values from other files
 *
 ****************************************************************************/

int Imp_ExtrLien(char *pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acAstFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   FILE     *fdAst;

   LogMsg0("Extract lien roll for %s", pCnty);

   // Get lien file
   GetIniString(pCnty, "LienFile", "", acRec, _MAX_PATH, acIniFile);
   // Sort input file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_Lien.srt", acTmpPath, pCnty, pCnty);
   sprintf(acBuf, "S(#2,C,A) F(TXT) DEL(%d)", cLdrSep);
   iRet = sortFile(acRec, acRollFile, acBuf);
   if (!iRet) return -1;

   LogMsg("Extracting LDR values from file %s", acRollFile);

   // Sort on ASMT
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acTmpFile);
      return -1;
   }

   // Get Asset summary file
   GetIniString(pCnty, "AssetFile", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acAstFile, "%s\\%s\\%s_Ast.srt", acTmpPath, pCnty, pCnty);
   sprintf(acBuf, "S(#2,C,A) F(TXT) OMIT(#1,C,NE,\"%d\",OR,#4,C,EQ,\"0\",OR,#5,C,NE,\"A\") OUTREC(#2,\"|\",#4,CRLF) DEL(%d)", lLienYear, cDelim);
   iRet = sortFile(acRec, acAstFile, acBuf);
   if (!iRet) return -2;
   LogMsg("Open Asset Summary file: %s", acAstFile);
   fdAst = fopen(acAstFile, "r");
   if (fdAst == NULL)
   {
      LogMsg("***** Error opening Asset Summary file: %s\n", acAstFile);
      return -2;
   }

   // Get exemption file
   GetIniString(pCnty, "LienExe", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acExeFile, "%s\\%s\\%s_Exe.srt", acTmpPath, pCnty, pCnty);
   sprintf(acBuf, "S(#1,C,A) F(TXT) DEL(%d)", cLdrSep);
   iRet = sortFile(acRec, acExeFile, acBuf);
   if (!iRet) return -2;

   // Open Exemption file
   LogMsg("Open Exe file: %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening exemption file: %s\n", acExeFile);
      return -2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Skip header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (!isdigit(*(pTmp+1)) )
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new record
      iRet = Imp_CreateLienRec(acBuf, acRec);

      if (!iRet)
      {
         // Add Exemption
         if (fdExe)
            Imp_AddLienExe(acBuf);

         // Add PP value
         if (fdAst)
            Imp_AddLienPP(acBuf, fdAst);

         // Write to output
         fputs(acBuf, fdLienExt);
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      iRet = myGetStrEQ(acRec, MAX_RECSIZE, fdRoll);
      if (iRet < 20 || acRec[0] > '9')
         break;      // EOF
   }

   if (fdRoll)
      fclose(fdRoll);

   // Load mobilehome file
   GetIniString(pCnty, "LienMH", "", acBuf, _MAX_PATH, acIniFile);
   LogMsg0("Extract value from Mobilehome file %s", acBuf);
   LogMsg("Open Mobilehome file %s", acBuf);
   fdRoll = fopen(acBuf, "r");
   // Skip header
   pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
   pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
   while (pTmp)
   {
      // Create new R01 record
      iRet = Imp_ExtrMH(acBuf, acRec);
      if (!iRet)
      {
         if (fdExe)
            Imp_AddLienExe(acBuf);

         iNewRec++;
         fputs(acBuf, fdLienExt);
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp || *pTmp > '9')
         break;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdExe)
      fclose(fdExe);
   if (fdAst)
      fclose(fdAst);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("      records processed:    %u", lCnt);
   LogMsg("      Exe matched:          %u", lExeMatch);
   LogMsg("      PP matched:           %u\n", lAstMatch);
   printf("\nTotal output records: %u", iNewRec);

   lRecCnt = iNewRec;
   return 0;
}

/****************************** Imp_Load_Roll2 ******************************
 *
 * Process roll update based on PQ_Asmt, PQ_ValueSet, PQ_OwnerShip, PQ_Exemption
 *
 ****************************************************************************/

int Imp_Load_Roll2(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acNameFile[_MAX_PATH], acSortCmd[256];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Reset iHdrRows since all input files are sorted
   iHdrRows = 0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open updated roll file - Sort on field #2 (ASMT)
   lLastFileDate = getFileDate(acRollFile);

   // Sort roll file
   sprintf(acTmpFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sorting %s to %s", acRollFile, acTmpFile);
   sprintf(acBuf, "S(#2,C,A) OMIT(#2,C,LT,\"001\") DEL(9) F(TXT)");
   iRet = sortFile(acRollFile, acTmpFile, acBuf);
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting Asmt file %s to %s", acRollFile, acTmpFile);
      return -2;
   }

   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Legal file
   iRet = GetIniString(myCounty.acCntyCode, "LglFile", "", acTmpFile, _MAX_PATH, acIniFile);;
   if (iRet > 0)
   {
      LogMsg("Open Legal file %s", acTmpFile);
      fdLegal = fopen(acTmpFile, "r");
      if (fdLegal == NULL)
      {
         LogMsg("***** Error opening Legal file: %s\n", acTmpFile);
         return -2;
      }
   } 
      
   // Open Land file
   //iRet = GetIniString(myCounty.acCntyCode, "LandExtr", "", acTmpFile, _MAX_PATH, acIniFile);;
   //if (iRet > 0)
   //{
   //   LogMsg("Open Land file %s", acTmpFile);
   //   fdLand = fopen(acTmpFile, "r");
   //   if (fdLand == NULL)
   //   {
   //      LogMsg("***** Error opening Land file: %s\n", acTmpFile);
   //      return -2;
   //   }
   //} 

   // Open Situs file - need sorting
   strcpy(acRollRec, acSitusFile);
   sprintf(acSitusFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sorting %s to %s", acRollRec, acSitusFile);
   // Sort on APN ascending, Seq# decending
   sprintf(acSortCmd, "S(#1,C,A,#2,C,D) OMIT(#3,C,NE,\"A\") DEL(%d) F(TXT)", cDelim);
   iRet = sortFile(acRollRec, acSitusFile, acSortCmd);
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting Situs file %s to %s", acRollRec, acSitusFile);
      return -2;
   }

   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Exe file 
   strcpy(acTmpFile, acExeFile);
   sprintf(acExeFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsgD("Sorting %s to %s", acTmpFile, acExeFile);
   sprintf(acBuf, "S(#1,C,A) DEL(%d) F(TXT)", cDelim);
   iRet = sortFile(acTmpFile, acExeFile, acBuf);
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting EXE file %s to %s", acTmpFile, acExeFile);
      return -2;
   }

   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Open value set file

   // Open ownership file
   iRet = GetIniString(myCounty.acCntyCode, "NameFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acNameFile, "%s\\%s\\%s_Name.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsgD("Sorting %s to %s", acTmpFile, acNameFile);
   sprintf(acBuf, "S(#1,C,A,#5,C,D) DEL(%d) F(TXT)", cDelim);
   iRet = sortFile(acTmpFile, acNameFile, acBuf);

   if (iRet > 0)
   {
      LogMsg("Open Ownership file %s", acNameFile);
      fdName = fopen(acNameFile, "r");
      if (fdName == NULL)
      {
         LogMsg("***** Error opening Ownership file: %s\n", acNameFile);
         return -2;
      }
   } else
   {
      LogMsg("***** ERROR sorting Ownership file %s to %s", acTmpFile, acNameFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "054605041", 9) ) //|| !memcmp(acRollRec, "047320103000", 9))
      //   iTmp = 0;
#endif

      if (!pTmp || acRollRec[0] > '0')
      {
         bEof = true;
         break;      // EOF
      }

      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Imp_MergeRoll2(acBuf, acRollRec, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Imp_MergeSitus2(acBuf);

            // Merge Owner
            if (fdName)
               lRet = Imp_MergeOwner2(acBuf);

            // Merge building info
            if (fdChar)
               lRet = Imp_MergeStdChar(acBuf);

            // Merge Land info
            //if (fdLand)
            //   lRet = Imp_MergeLand(acBuf);

            iRollUpd++;

            // Read next roll record, skip duplicate record
            //do
            //{
               pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
            //} while (pTmp && !memcmp(acBuf, pTmp, iApnLen));
         } else
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
            goto NextRollRec;
         }
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         // Create new R01 record
         iRet = Imp_MergeRoll2(acRec, acRollRec, CREATE_R01);
         if (!iRet)
         {
            if (bDebug)
               LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

            // Merge Situs
            if (fdSitus)
               lRet = Imp_MergeSitus2(acRec);

            // Merge Owner
            if (fdName)
               lRet = Imp_MergeOwner2(acRec);

            // Merge building info
            if (fdChar)
               lRet = Imp_MergeStdChar(acRec);

            // Merge Value
            if (fdValue)
               lRet = Imp_MergeValue2(acRec);

            // Merge Exemption
            if (fdExe)
               lRet = Imp_MergeExe2(acRec);

            // Merge Land info
            //if (fdLand)
            //   lRet = Imp_MergeLand(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Read next roll record, skip duplicate record
         do
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         } while (pTmp && !memcmp(acRec, pTmp, iApnLen));
         goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Imp_MergeRoll2(acRec, acRollRec, CREATE_R01);
      if (!iRet)
      {
         // Merge Exemption
         if (fdExe)
            lRet = Imp_MergeExe2(acRec);

         // Merge Situs
         if (fdSitus)
            lRet = Imp_MergeSitus2(acRec);

         // Merge Owner
         if (fdName)
            lRet = Imp_MergeOwner2(acRec);

         // Merge Char
         if (fdChar)
            lRet = Imp_MergeStdChar(acRec);

         // Merge Value
         if (fdValue)
               lRet = Imp_MergeValue2(acRec);

         // Merge Land info
         //if (fdLand)
         //   lRet = Imp_MergeLand(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);

         // Read next roll record, skip duplicate record
         do
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         } while (pTmp && !memcmp(acRec, pTmp, iApnLen));
      } else
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      lCnt++;

      if (!pTmp || acRollRec[iSkipQuote] > '9')
         break;
   }

   // Copy MH records
   while (iRecLen == nBytesRead)
   {
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   }

   // Close files
   if (fdName)
      fclose(fdName);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdValue)
      fclose(fdValue);
   if (fdLegal)
      fclose(fdLegal);
   //if (fdLand)
   //   fclose(fdLand);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Name matched:     %u", lNameMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Legal matched:    %u\n", lLegalMatch);
   //LogMsg("Number of Land matched:     %u\n", lLandMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Name skiped:      %u", lNameSkip);
   LogMsg("Number of Exe skiped:       %u\n", lExeSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u", lRecCnt);

   return 0;
}

/***************************** Imp_ExtrMHChar ********************************
 *
 * Extract mobile home char from "PQ_MobileHomeAttribute.txt".
 * Return 0 if success.
 *
 *****************************************************************************/

int Imp_ExtrMHChar(char *pCharFile, char *pOutFile)
{
   FILE     *fdIn;
   char     acBuf[4096], acTmp[256], acCode[32], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg0("Extract MH char file");

   LogMsg("Open Building file %s", pCharFile);
   if (!(fdIn = fopen(pCharFile, "r")))
   {
      LogMsg("***** Error opening Building file %s", pCharFile);
      return -1;
   }

   LogMsg("Create CHAR file %s", pOutFile);
   if (!(fdChar = fopen(pOutFile, "a+")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", pOutFile);
      return -2;
   }

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);

      if (!pRec)
         break;
      if (acBuf[0] < '0' || acBuf[0] > '9')
         continue;

      // Parse string
      iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

      if (iFldCnt < IMP_MA_FLDS)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[IMP_MA_APN], strlen(apTokens[IMP_MA_APN]));
      memcpy(myCharRec.FeeParcel, apTokens[IMP_MA_DECALNUM], strlen(apTokens[IMP_MA_DECALNUM]));

      // Format APN
      iRet = formatApn(apTokens[IMP_MA_APN], acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      // BldgSize
      int iBldgSize = atoi(apTokens[IMP_MA_SIZESQFT]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "001170007000", 9))
      //   iRet = 0;
#endif

      // QualityClass
      strcpy(acTmp, _strupr(apTokens[IMP_MA_QUALITYCLASS]));
      if (!memcmp(acTmp, "FAKE", 4))
         acTmp[0] = 0;
      iTmp = blankRem(acTmp);
      iRet = atol(acTmp);
      if (iTmp > 0 && iRet < 100)
      {
         memcpy(myCharRec.QualityClass, acTmp, iTmp);
         acCode[0] = 0;
         iRet = 0;
         if (isdigit(acTmp[0]))
            iRet = Quality2Code((char *)&acTmp[0], acCode, NULL);
         else if (isdigit(acTmp[1]))
            iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
         else if (isdigit(acTmp[2]))
            iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
         else if (acTmp[0] > ' ')
            LogMsg("*** Bad Quality P1: %s (APN=%s)", acTmp, apTokens[0]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual  = acCode[0];
         else if (iRet < 0 && acTmp[0] > ' ')
            LogMsg("*** Bad Quality P2: %s (APN=%s)", acTmp, apTokens[0]);

         if (acTmp[0] >= 'A')
            myCharRec.BldgClass = acTmp[0];
      }

      // Improved Condition
      if (*apTokens[IMP_MA_CONDITIONADJUSTMENT] == '1')
         myCharRec.ImprCond[0] = 'V';
      else if (*apTokens[IMP_MA_CONDITIONADJUSTMENT] == '2')
         myCharRec.ImprCond[0] = 'G';
      else if (*apTokens[IMP_MA_CONDITIONADJUSTMENT] == '3')
         myCharRec.ImprCond[0] = 'F';
      else if (*apTokens[IMP_MA_CONDITIONADJUSTMENT] == '4')
         myCharRec.ImprCond[0] = 'P';
      else if (*apTokens[IMP_MA_CONDITIONADJUSTMENT] == '5')
         myCharRec.ImprCond[0] = 'A';

      // YrBlt
      int iYrBlt = atoi(apTokens[IMP_MA_YEARBUILT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[IMP_MA_EFFECTIVEYEARBUILT]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // Attached/Detached SF
      iTmp = atoi(apTokens[IMP_MA_GARAGESQFT]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.GarSqft, acTmp, iRet);

         if (*apTokens[IMP_MA_GARAGECODE] == 'A')           // Attached
            myCharRec.ParkType[0] = 'A';
         else if (*apTokens[IMP_MA_GARAGECODE] == 'D')      // Detached
            myCharRec.ParkType[0] = 'D';
      }

      // Carport SF - CR, FT, TP
      iTmp = atoi(apTokens[IMP_MA_CARPORTSQFT]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.GarSqft, acTmp, iRet);

         if (myCharRec.ParkType[0] == ' ') 
            myCharRec.ParkType[0] = 'C';
      }

      // Heating
      if (*apTokens[IMP_MA_HEATINGCODE] > ' ')
      {
         if (!memcmp(apTokens[IMP_MA_HEATINGCODE], "FA", 2))         // Force air
            myCharRec.Heating[0] = 'B';
         else if (!memcmp(apTokens[IMP_MA_HEATINGCODE], "FC", 2))    // Forced-air central
            myCharRec.Heating[0] = 'Z';
         else if (!memcmp(apTokens[IMP_MA_HEATINGCODE], "PU", 2))    // Portable Unit
            myCharRec.Heating[0] = '6';
         else if (!memcmp(apTokens[IMP_MA_HEATINGCODE], "SH", 2))    // Suspended Heater
            myCharRec.Heating[0] = 'O';
         else if (!memcmp(apTokens[IMP_MA_HEATINGCODE], "BS", 2))    // Baseboard
            myCharRec.Heating[0] = '8';
      }

      // Cooling
      if (*apTokens[IMP_MA_COOLINGCODE] > ' ')
      {
         if (!memcmp(apTokens[IMP_MA_HEATINGCODE], "CA", 2) ||
             !memcmp(apTokens[IMP_MA_HEATINGCODE], "AC", 2))         // Central air conditioning
            myCharRec.Cooling[0] = 'C';
         else if (!memcmp(apTokens[IMP_MA_HEATINGCODE], "EC", 2))    // Evaporative cooler 
            myCharRec.Cooling[0] = 'E';
         else if (!memcmp(apTokens[IMP_MA_HEATINGCODE], "WA", 2))    // Wall or window mounted ac unit
            myCharRec.Cooling[0] = 'L';
      }

      // Beds
      int iBeds = atol(apTokens[IMP_MA_NUMBEDROOMS]);
      if (iBeds > 0 && iBeds < 100)
      {
         iTmp = sprintf(acTmp, "%d", iBeds);
         memcpy(myCharRec.Beds, acTmp, iTmp);
      }

      // Full baths
      int iBaths = atol(apTokens[IMP_MA_NUMBATHROOMS]);
      if (iBaths > 0 && iBaths < 100)
      {
         iTmp = sprintf(acTmp, "%d", iBaths);
         memcpy(myCharRec.FBaths, acTmp, iTmp);
      }

      // FirePlace 
      if (*apTokens[IMP_MA_FIREPLACECODE] > '0')
      {
         pRec = findXlatCodeA(apTokens[IMP_MA_FIREPLACECODE], &asFireplace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      }

      // Roof cover
      if (*apTokens[IMP_MA_ROOFCODE] >= '0')
      {
         pRec = findXlatCodeA(apTokens[IMP_MA_ROOFCODE], &asRoofMat[0]);
         if (pRec)
            myCharRec.RoofMat[0] = *pRec;
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdChar);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdChar) fclose(fdChar);

   LogMsg("Number of records processed: %d\n", iCnt);

   // Sort output file on APN asc, CharID desc
   strcpy(acTmp, pOutFile);
   pRec = strrchr(acTmp, '.');
   strcpy(pRec, ".srt");
   iCnt = sortFile(pOutFile, acTmp, "S(1,12,C,A) F(TXT)");
   if (iCnt > 1000)
   {
      remove(pOutFile);
      iRet = rename(acTmp, pOutFile);
   } else
      iRet = -1;

   return iRet;
}

/*********************************** loadImp ********************************
 *
 * Options:
 *    -CIMP -L -Xl -Xs[i]  (load lien)
 *    -CIMP -U -Xs[i]      (load update)
 *
 * Notes:
 *    1) In 2009, we received LDR file the same format as AMA.
 *
 ****************************************************************************/

int loadImp(int iSkip)
{
   int   iRet=0;
   char  acTmp[_MAX_PATH], acDailyZip[_MAX_PATH], acTmpFile[_MAX_PATH], *pTmp;

   // Set default APN field
   iApnLen = myCounty.iApnLen;
   lLotSqftCount = 0;

   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Fix DocType if needed
   if (iLoadFlag & FIX_CSTYP)
   {
      iRet = FixDocType(acCSalFile, (IDX_TBL5 *)&IMP_DocCode[0], true /* remove rec w/o DocNum */);
      if (iRet)
         return iRet;
   }

   // Setup roll file for daily load
   if (iLoadFlag & LOAD_DAILY)                     // -Ld
   {
      GetIniString(myCounty.acCntyCode, "DailyFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acTmpFile, acTmp, lProcDate);

      struct   _finddata_t  sFileInfo;
      long lHandle = _findfirst(acTmpFile, &sFileInfo);
      if (lHandle > 0)
      {
         pTmp = strrchr(acTmpFile, '\\');
         *pTmp = 0;
         sprintf(acDailyZip, "%s\\%s", acTmpFile, sFileInfo.name);
         _findclose(lHandle);

         // Unzip daily file
         if (iRet = unzipOne(acDailyZip, acTmpFile, true))
            return iRet;

      } else
      {
         LogMsg("*** Daily file not available, process stop! ***");
         bGrGrAvail = false;
         return -1;
      }
   }

   // Extract lien file
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      // 2019, 2022
      iRet = Imp_ExtrLien(myCounty.acCntyCode);
      // 2020
      //iRet = Imp_ExtrLien2(myCounty.acCntyCode);
   }

   // Create/Update cum sale file from Imp_Sales.csv
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // 2020
      iRet = Imp_CreateSCSale2(YYYY_MM_DD, 4, 0, false);
      //iRet = MB1_CreateSCSale(YYYY_MM_DD,4,0,false,(IDX_TBL5 *)&IMP_DocCode[0]);
      //iRet = Imp_CreateSCSale(YYYY_MM_DD, 4, 0, false);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      char  acLandFile[_MAX_PATH], acUnitFile[_MAX_PATH], acMHFile[_MAX_PATH], acTmpFile[_MAX_PATH], acTmp[_MAX_PATH];

      GetIniString(myCounty.acCntyCode, "LandFile", "", acLandFile, _MAX_PATH, acIniFile);
      strcpy(acTmpFile, acLandFile);
      replStr(acTmpFile, ".txt", ".fix");

      // We need to rebuild this file since it may include CR in Comment field.
      iRet = RebuildCsv(acLandFile, acTmpFile, cDelim, IMP_LAND_FLDS);

      sprintf(acLandFile, "%s\\%s\\%s_Land.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      // Sort by ASMT, DTS desc (new update on top)
      sprintf(acTmp, "S(#1,C,A,#%d,CDA,D) OMIT(#1,C,LT,\"001\") F(TXT) DUPO(B5000,#1) DEL(%d)", IMP_LAND_DTS+1, cDelim);
      iRet = sortFile(acTmpFile, acLandFile, acTmp);
      if (!iRet) return -1;

      // Unit file
      GetIniString(myCounty.acCntyCode, "UnitFile", "", acTmpFile, _MAX_PATH, acIniFile);
      strcpy(acUnitFile, acTmpFile);
      replStr(acUnitFile, ".txt", ".srt");
      sprintf(acTmp, "S(#1,C,A,#%d,CDA,D) OMIT(#1,C,LT,\"001\") F(TXT) DUPO(B5000,#1) DEL(%d)", IMP_UNIT_DTS+1, cDelim);
      iRet = sortFile(acTmpFile, acUnitFile, acTmp);
      if (!iRet) return -1;

      // Load Bldg Detail, Land, Unit
      GetIniString(myCounty.acCntyCode, "CharBldg", "", acCharFile, _MAX_PATH, acIniFile);
      iRet = Imp_ExtrCVal(acCharFile, acLandFile, acUnitFile, acCChrFile);
      if (iRet < 0) return -1;

      // Add mobile home
      GetIniString(myCounty.acCntyCode, "CharMH", "", acMHFile, _MAX_PATH, acIniFile);
      strcpy(acTmpFile, acMHFile);
      replStr(acTmpFile, ".txt", ".fix");
      iRet = RebuildCsv(acMHFile, acTmpFile, cDelim, IMP_MA_FLDS);

      sprintf(acMHFile, "%s\\%s\\%s_MHChar.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      sprintf(acTmp, "S(#1,C,A,#%d,C,D) OMIT(#1,C,LT,\"001\") F(TXT) DUPO(B5000,#1) DEL(%d)", IMP_MA_DTS+1, cDelim);
      iRet = sortFile(acTmpFile, acMHFile, acTmp);
      if (!iRet) return -1;
      iRet = Imp_ExtrMHChar(acMHFile, acCChrFile);
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      iSkipQuote = 1;
      // 2019 iApnFld = IMP_ROLL_APN;
      // 2020
      iApnFld = IMP_ASMT_APN;

      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      iRet = Imp_Load_LDR1(iSkip);                 // 2019, 2022
      //iRet = Imp_Load_LDR2(iSkip);                 // 2018,2020
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      GetIniString(myCounty.acCntyCode, "RollFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acRollFile, acTmp, myCounty.acCntyCode);
      iApnFld = MB_ROLL_ASMT;

      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      //iRet = Imp_Load_Roll(iSkip);               // 2019
      iRet = Imp_Load_Roll2(iSkip);                // 2020
      LogMsg("Total LotSqft populated: %d", lLotSqftCount);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Imp_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   return iRet;
}