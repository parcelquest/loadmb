#ifndef  _MERGEBUT_H_
#define  _MERGEBUT_H_

#include "Recdef.h"

// _ROLL
#define  GRGR_DOC_DOCNUM         0
#define  GRGR_DOC_RECDT          1
#define  GRGR_DOC_DOCTITLE       2
#define  GRGR_DOC_TAX            3
#define  GRGR_DOC_PAGES          4
//#define  GRGR_DOC_UNDISCLOSE     5

#define  GRGR_REF_DOCNUM         0
#define  GRGR_REF_REFINFO        1

#define  GRGR_NAM_DOCNUM         0
#define  GRGR_NAM_TYPE           1
#define  GRGR_NAM_NAME           2

typedef struct _tButGrGr
{
   char  DocNum[SIZ_GR_DOCNUM];           // 1
   char  APN[SIZ_GR_APN];                 // 17
   char  DocDate[SIZ_GR_DOCDATE];         // 37
   char  DocTitle[SIZ_GR_TITLE];          // 45
   char  Tax[SIZ_GR_TAX];                 // 115
   char  SalePrice[SIZ_GR_SALE];          // 125
   char  NumPages[SIZ_GR_NUMPG];          // 135
   char  NameCnt[SIZ_GR_NAMECNT];         // 139
   char  APN_Matched;                     // 143
   char  Owner_Matched;                   // 144
   char  MoreName;                        // 145
   char  Grantor[5][SIZ_GR_NAME+1];       // 146
   char  Grantee[5][SIZ_GR_NAME+1];       // 401
   char  ReferenceData[SIZ_GR_REF];       // 656

   char  S_Num[SIZ_M_STRNUM];
   char  S_Sub[SIZ_M_STR_SUB];
   char  S_Dir[SIZ_M_DIR];
   char  S_Name[SIZ_M_STREET+6];
   char  S_Sfx[SIZ_M_SUFF+1];
   char  S_Unit[SIZ_M_UNITNO+2];
   char  filler1[6];
   char  S_City[SIZ_M_CITY];
   char  S_State[SIZ_M_ST];
   char  S_Zip[SIZ_M_ZIP];
   char  S_Zip4[SIZ_M_ZIP4];
   
   // 846-
   char  Legal[SIZ_GR_LEGAL];
   char  R_Name1[SIZ_NAME1-2];
   char  CareOf[SIZ_GR_NAME];
   char  M_Num[SIZ_M_STRNUM];
   char  M_Sub[SIZ_M_STR_SUB];
   char  M_Dir[SIZ_M_DIR];
   char  M_Name[SIZ_M_STREET+6];      // 30
   char  M_Sfx[SIZ_M_SUFF+1];         // 6
   char  M_Unit[SIZ_M_UNITNO+2];      // 8
   char  filler2[6];
   char  M_City[SIZ_M_CITY];
   char  M_State[SIZ_M_ST];
   char  M_Zip[SIZ_M_ZIP];
   char  M_Zip4[SIZ_M_ZIP4];
   char  XferDate[SIZ_TRANSFER_DT];
   char  XferDoc[SIZ_TRANSFER_DOC];
   char  filler3[45];
   char  CRLF[2];                      // reclen 1150
} BUTGRGR;
int GetRollData(BUTGRGR *pGrgr, int iFlag=0);

#define  BUT_CHAR_FEEPARCEL            0
#define  BUT_CHAR_POOLSPA              1
#define  BUT_CHAR_CATTYPE              2
#define  BUT_CHAR_QUALITYCLASS         3
#define  BUT_CHAR_YEARBUILT            4
#define  BUT_CHAR_BUILDINGSIZE         5
#define  BUT_CHAR_ATTACHGARAGESF       6
#define  BUT_CHAR_DETACHGARAGESF       7
#define  BUT_CHAR_CARPORTSF            8
#define  BUT_CHAR_HEATING              9
#define  BUT_CHAR_COOLINGCENTRALAC     10
#define  BUT_CHAR_COOLINGEVAPORATIVE   11
#define  BUT_CHAR_COOLINGROOMWALL      12
#define  BUT_CHAR_COOLINGWINDOW        13
#define  BUT_CHAR_STORIESCNT           14
#define  BUT_CHAR_UNITSCNT             15
#define  BUT_CHAR_TOTALROOMS           16
#define  BUT_CHAR_EFFECTIVEYEAR        17
#define  BUT_CHAR_PATIOSF              18
#define  BUT_CHAR_BEDROOMS             19
#define  BUT_CHAR_BATHROOMS            20
#define  BUT_CHAR_HALFBATHS            21
#define  BUT_CHAR_FIREPLACE            22
#define  BUT_CHAR_ASMT                 23
#define  BUT_CHAR_BLDGSEQNUM           24
#define  BUT_CHAR_HASWELL              25
#define  BUT_CHAR_LANDSQFT             26

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "F",  "Y", 1,               // Fireplace
   "G",  "G", 1,               // GasStove
   "I",  "I", 1,               // Insert
   "N",  "N", 1,               // None
   "P",  "S", 1,               // PelletStove
   "W",  "W", 1,               // Woodstove
   "",   "",  0
};

static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "B",  "F", 1,              // Baseboard
   "C",  "Z", 1,              // Central
   "F",  "C", 1,              // Floor Furnace
   "P",  " ", 1,              // ?
   "T",  " ", 1,              // ?
   "U",  " ", 1,              // ?
   "W",  "D", 1,              // Wall
   "",   "",  0
};

//static XLAT_CODE  asHeating_LDR[] =
//{
//   // Value, lookup code, value length
//   "B",  "Y", 1,              // Baseboard
//   "C",  "Z", 1,              // Central
//   "F",  "C", 1,              // Floor Furnace
//   "P",  " ", 1,              // Solar
//   "W",  "D", 1,              // Wall
//   "",   "",  0
//};

//static XLAT_CODE  asCooling[] =
//{
//   // Value, lookup code, value length
//   "1",  " ", 1,              //
//   "2",  " ", 1,              //
//   "3",  " ", 1,              //
//   "6",  " ", 1,              //
//   "C",  "C", 1,              // Central
//   "CN", "C", 2,              // Central
//   "E",  "E", 1,              // Evaporative Cooler
//   "M",  " ", 1,              // ?
//   "N",  "N", 1,              // None
//   "OT", "X", 1,              // Other
//   "O",  "X", 1,              // Other
//   "P",  "X", 1,              // Personal Property?
//   "U",  " ", 1,              // ?
//   "W",  "L", 1,              // Wall
//   "Y",  "U", 1,              // Yes
//   "",   "",  0
//};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "01", "P", 2,              // Pool
   "A",  "X", 1,              // Above Ground
   "DB", "X", 2,              // Above Ground
   "F",  "F", 1,              // Fiberglass
   "G",  "G", 1,              // Gunite
   "N",  "N", 1,              // None
   "P",  "C", 1,              // Pool/Spa combo
   "S",  "S", 1,              // Spa only
   "V",  "V", 1,              // Vinyl
   "",   "",  0
};

//static XLAT_CODE  asPool_LDR[] =
//{
//   // Value, lookup code, value length
//   "01", "P", 2,              // Pool
//   "A",  "X", 1,              // Above Ground
//   "F",  "F", 1,              // Fiberglass
//   "G",  "G", 1,              // Gunite
//   "N",  "N", 1,              // None
//   "P",  "C", 1,              // PoolSpaCombo
//   "S",  "S", 1,              // SpaOnly
//   "V",  "V", 1,              // Vinyl
//   "",   "",  0
//};
//
//static XLAT_CODE  asFP_LDR[] =
//{
//   // Value, lookup code, value length
//   "F",  "Y", 1,               // Fireplace
//   "G",  "G", 1,               // GasStove
//   "I",  "I", 1,               // Insert
//   "N",  "N", 1,               // None
//   "P",  "S", 1,               // PelletStove
//   "W",  "W", 1,               // Woodstove
//   "",   "",  0
//};
//
static XLAT_CODE  asQual[] =
{
   // Value, lookup code, value length
   "AV",    "A",  2,
   "EX",    "E",  2,
   "FAIR",  "F",  4,
   "GOOD",  "G",  4,
   "VG",    "V",  2,
   "POOR",  "P",  4,
   "",      "",  0
};

IDX_TBL5 BUT_DocCode[] =
{// DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN
   "01", "1 ", 'N', 2, 2,          // SALE 100%
   "02", "57", 'N', 2, 2,          // PARTIAL SALE
   "03", "13", 'Y', 2, 2,          // NON-SALE TRANSFER
   "04", "13", 'Y', 2, 2,          // PARTIAL NON SALE TRANSFER
   "05", "74", 'Y', 2, 2,          // NEW MOBILE HOME
   "06", "74", 'Y', 2, 2,          // TERMINATE J/T & LE: 100% REAPP
   "07", "74", 'Y', 2, 2,          // SPLIT
   "08", "74", 'Y', 2, 2,          // COMBINATION
   "09", "74", 'Y', 2, 2,          // BOUNDARY LINE MODIFICATION
   "10", "74", 'Y', 2, 2,          // PARTITION IN EQUAL SHARES
   "11", "27", 'Y', 2, 2,          // TRUSTEES DEED/DEED IN LIEU/SHERIFF'S DEED/QUITCLAIM DEED
   "12", "11", 'Y', 2, 2,          // CONTRACT OF SALE
   "13", "74", 'Y', 2, 2,          // DEATH DATE REAPPRAISAL
   "14", "74", 'Y', 2, 2,          // DATE OF DEATH-PARTIAL REAPPRAISAL
   "15", "74", 'Y', 2, 2,          // LEGAL ENTITY CHANGE-PARTIAL INTEREST
   "16", "74", 'Y', 2, 2,          // LEGAL ENTITY CHANGE
   "17", "75", 'Y', 2, 2,          // TRANSFER TO GOV LENDER
   "18", "75", 'N', 2, 2,          // MH ONLY TRANSFER
   "19", "74", 'Y', 2, 2,          // ROAD TAKE
   "20", "74", 'Y', 2, 2,          // LEASE REQUIRES REAPPRAISAL
   "21", "75", 'Y', 2, 2,          // DE MINIMUS TRANSFER (LESS THAN 5% / $10
   "22", "25", 'Y', 2, 2,          // SHERIFFS DEED
   "23", "74", 'Y', 2, 2,          // SECURITY INTER TRF/REFINANCE
   "24", "74", 'Y', 2, 2,          // PERFECT TITLE/ DISTRIBUTE ESTATE
   "25", "74", 'Y', 2, 2,          // CIP
   "26", "74", 'Y', 2, 2,          // CIO- NO SUPP DUE TO SUBSEQUENT TRANSF
   "28", "74", 'Y', 2, 2,          // CHANGE FORM OF TITLE ONLY
   "29", "13", 'Y', 2, 2,          // TRANSFER TO/FROM REVOCABLE TRUST
   "30", "13", 'Y', 2, 2,          // DISTRIBUTION OF ESTATE/TRUST
   "31", "74", 'Y', 2, 2,          // PROP 58 EXCLUSION
   "32", "75", 'Y', 2, 2,          // TRANSFER TO PUBLIC ENTITY
   "33", "74", 'Y', 2, 2,          // RETURN TO ORIGINAL TRANSFEROR
   "34", "3 ", 'Y', 2, 2,          // ADD JOINT TENANT/RETURN TO OJT
   "35", "18", 'Y', 2, 2,          // INTERSPOUSAL
   "36", "74", 'Y', 2, 2,          // CREATE/TERMINATE LIFE ESTATE (EXEMPT)
   "37", "74", 'Y', 2, 2,          // TERMINATE LIFE ESTATE(EXEMPT)
   "38", "74", 'Y', 2, 2,          // PAGE TRANSFER
   "39", "74", 'Y', 2, 2,          // PROP 193 EXCLUSION
   "40", "74", 'Y', 2, 2,          // NEW INDUSTRIAL IMPS/MISC INDUSTRIAL IMP
   "41", "74", 'Y', 2, 2,          // REVIEW REQUESTED/CALAMITY APP
   "42", "13", 'Y', 2, 2,          // DOMESTIC PARTNER TRANSFER
   "43", "74", 'Y', 2, 2,          // CALAMITY RELIEF/REPAIR
   "44", "74", 'Y', 2, 2,          // TFR OF LESS THAN FEE (SUP=Y )
   "45", "74", 'Y', 2, 2,          // VALUE FROM BPS
   "46", "74", 'Y', 2, 2,          // VALUE FROM AUDIT
   "47", "74", 'Y', 2, 2,          // TREES/VINES ON/OFF
   "48", "74", 'Y', 2, 2,          // NEW/MISC AG IMPS
   "49", "74", 'Y', 2, 2,          // NEW SINGLE FAMILY RESIDENCE w/ACTIVE S
   "50", "74", 'Y', 2, 2,          // NEW SINGLE FAMILY RESIDENCE
   "51", "74", 'Y', 2, 2,          // PERM FOUNDATION MH/MH SITE DEV/MH INS
   "52", "74", 'Y', 2, 2,          // MH UTILITY/LOT DEVELOPMENT
   "53", "74", 'Y', 2, 2,          // MOBILE HOME INSTALLATION
   "54", "74", 'Y', 2, 2,          // NON MH SITE/LOT DEVELOPMENT
   "55", "74", 'Y', 2, 2,          // RES ADDITION/CONVERSION
   "56", "74", 'Y', 2, 2,          // REMODEL
   "57", "74", 'Y', 2, 2,          // GARAGE/CARPORT/SHOP
   "58", "74", 'Y', 2, 2,          // DECK/PORCH/PATIO
   "59", "74", 'Y', 2, 2,          // NEW HEAT/COOL
   "60", "74", 'Y', 2, 2,          // PUB WKS LTR/CODE VIOL
   "61", "74", 'Y', 2, 2,          // POOL/SPA/SAUNA
   "62", "74", 'Y', 2, 2,          // WOODSTOVE/FIREPLACE
   "63", "74", 'Y', 2, 2,          // AWNING/COVER
   "64", "74", 'Y', 2, 2,          // DEMOLITION
   "65", "74", 'Y', 2, 2,          // REMOVE MOBILE HOME
   "66", "74", 'Y', 2, 2,          // LAWN SPRINKLER SYSTEM
   "67", "74", 'Y', 2, 2,          // PERMIT RENEWAL
   "68", "74", 'Y', 2, 2,          // REPAIR/SEWER CONN/MISC
   "69", "74", 'Y', 2, 2,          // MULTIPLE MISC PERMITS
   "70", "74", 'Y', 2, 2,          // NOTE AND REFILE
   "71", "74", 'Y', 2, 2,          // AG EXEMPT BLDG PERMIT
   "72", "74", 'Y', 2, 2,          // AG PROP STATEMENT TO RP APPR
   "73", "74", 'Y', 2, 2,          // AG TAXPAYER REPORT OF IMPR CH
   "75", "74", 'Y', 2, 2,          // MULTIPLE FAMILY RESIDENCE
   "77", "74", 'Y', 2, 2,          // NEW COMMERCIAL IMPS/INFILL/INTERIOR WI
   "78", "74", 'Y', 2, 2,          // COMMERCIAL INFILL
   "79", "74", 'Y', 2, 2,          // COMMERCIAL INTERIOR WK/REMODEL
   "80", "74", 'Y', 2, 2,          // SIGN
   "81", "74", 'Y', 2, 2,          // COMMERCIAL FIRE PROT/ANSUL
   "82", "74", 'Y', 2, 2,          // COMMERCIAL MISC CONST
   "83", "74", 'Y', 2, 2,          // COMMERCIAL ELEC PERMIT
   "84", "74", 'Y', 2, 2,          // COMM BPS TO RP APPRAISER
   "85", "74", 'Y', 2, 2,          // INDUSTRIAL PERMIT
   "86", "74", 'Y', 2, 2,          // NO CHANGE IN VALUE
   "88", "74", 'Y', 2, 2,          // RESIDENTIAL BUILDING PERMIT
   "89", "74", 'Y', 2, 2,          // PROP 8 RESTORE BASE VALUE
   "90", "74", 'Y', 2, 2,          // PROP 8 VALUE
   "91", "74", 'Y', 2, 2,          // ROLL CORRECTION
   "92", "74", 'Y', 2, 2,          // ASSESSMENT APPEAL/APPEAL PROP 8 VALU
   "94", "74", 'Y', 2, 2,          // PROP 60 REPLACEMENT RES
   "95", "74", 'Y', 2, 2,          // ROLL UPDATE ONLY
   "96", "74", 'Y', 2, 2,          // LATE PROP 58 True
   "98", "74", 'Y', 2, 2,          // TRF FOR PRORATION ONLY
   "99", "74", 'Y', 2, 2,          // PROBLEM TRANSFER
   "", "", '\0', 0, 0
};

// Use to translate GrGr's doc title to standard DocType
IDX_TBL5 BUT_DocTitle[] =
{  // DocTitle, Index, Non-sale, len1, len2
   "DEED",                       "1 ", 'N', 4, 2,  //
   "ASSIGNMENT",                 "7 ", 'Y',10, 2,  //
   "TRUSTEES DEED",              "27", 'Y',12, 2,  //
   "QUITCLAIM DEED",             "4 ", 'Y', 9, 2,  //
   "AFFIDAVIT",                  "6 ", 'Y', 8, 2,  //
   "NOTICE",                     "74", 'Y', 6, 2,  //
   "AGREEMENT OF SALE",          "8 ", 'Y',15, 2,  //
   "AGREEMENT",                  "19", 'Y', 9, 2,  //
   "EASEMENT",                   "40", 'Y', 8, 2,  //
   "ORDER",                      "80", 'Y', 5, 2,  //
   "ASSESSMENT",                 "74", 'Y', 9, 2,  //
   "SURVEY MAP",                 "74", 'Y', 6, 2,  //
   "MEMORANDUM",                 "19", 'Y', 8, 2,  //
   "LETTERS",                    "74", 'Y', 6, 2,  //
   "DECLARATION",                "19", 'Y', 6, 2,  //
   "RESCISSION",                 "19", 'Y', 6, 2,  //
   "TAX DEED",                   "67", 'N', 6, 2,  //
   "CERTIFICATE",                "74", 'Y', 6, 2,  //
   "JUDGMENT",                   "19", 'Y', 6, 2,  //
   "PARCEL MAP",                 "74", 'Y', 8, 2,  //
   "SPOUSAL PROPERTY ORDER",     "80", 'Y',20, 2,  //
   "COVENANTS",                  "74", 'Y', 8, 2,  //
   "LEASE",                      "44", 'Y', 5, 2,  //
   "SUBDIVISION MAP",            "74", 'Y',13, 2,  //
   "RELEASE OF LIEN",            "19", 'Y',13, 2,  //
   "AMENDED ORDER",              "80", 'Y',10, 2,  //
   "ASSUMPTION",                 "19", 'Y', 9, 2,  //
   "CANCELLATION",               "19", 'Y', 6, 2,  //
   "AMENDED DECLARATION",        "19", 'Y',10, 2,  //
   "PARTNERSHIP",                "19", 'Y',10, 2,  //
   "AMENDED LEASE",              "19", 'Y',10, 2,  //
   "OPTION AGREEMENT",           "19", 'Y',10, 2,  //
   "AMENDED JUDGMENT",           "19", 'Y',10, 2,  //
   "TERMINATION OF LEASE",       "44", 'Y',17, 2,  //
   "LEASE AGREEMENT",            "44", 'Y', 9, 2,  //
   "COVENANT AND RESTRICTIONS",  "74", 'Y',16, 2,  //
   "ARTICLES OF ORGANIZATION",   "74", 'Y',16, 2,  //
   "LIMITED LIABILITY COMPANY",  "74", 'Y',20, 2,  //
   "LAND CONSERVATION",          "74", 'Y',10, 2,  //
   "SHERIFFS DEED",              "25", 'N',10, 2,  //
   "AMENDED LIMITED PARTNERSHIP","19", 'Y',10, 2,  //
   "MAP ADDITIONAL INFORMATION", "74", 'Y', 8, 2,  //
   "MAP OWNERS STATEMENT",       "74", 'Y', 8, 2,  //
   "CONTRACT",                   "19", 'Y', 7, 2,  //
   "MERGER",                     "19", 'Y', 6, 2,  //
   "DECREE",                     "19", 'Y', 6, 2,  //
   "AMENDED EASEMENT",           "40", 'Y',10, 2,  //
   "AMENDED MAP",                "74", 'Y',10, 2,  //
   "ARTICLES OF INCORP",         "74", 'Y', 8, 2,  //
   "RESOLUTION",                 "74", 'Y', 8, 2,  //
   "RIGHT OF WAY",               "74", 'Y',10, 2,  //
   "REVOCATION OF DECLARATION",  "19", 'Y',16, 2,  //
   "LAND CONTRACT",              "19", 'Y',10, 2,  //
   "CONDOMINIUM PLAN",           "19", 'Y',14, 2,  //
   "AMENDED TRUST",              "19", 'Y',10, 2,  //
   "AMENDED MINE",               "74", 'Y',10, 2,  //
   "CONTRACT OF SALE",           "11", 'Y',14, 2,  //
   "CONVEYANCE OF PIPELINE",     "74", 'Y',18, 2,  //
   "REVOCATION OF DEED",         "19", 'Y',16, 2,  //
   "TERMINATION OF TRUST",       "19", 'Y',18, 2,  //
   "RESCISSION OF TAX DEED",     "19", 'Y',20, 2,  //
   "","",' ',0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 BUT_Exemption[] = 
{
   "E01", "H", 3,1,
   "E02", "V", 3,1,     // WIFE OF VETERANS
   "E03", "V", 3,1,     // VETERANS EXEMPTION
   "E04", "D", 3,1,     // DISABLED VET (STANDARD)
   "E05", "D", 3,1,     // DISABLED VET (LOW INCOME)
   "E06", "V", 3,1,     // PENSIONED VET
   "E07", "V", 3,1,     // BALANCE FROM SECURED VET
   "E08", "V", 3,1,     // BALANCE FROM UNSECURED VETERAN
   "E09", "V", 3,1,     // BALANCE VET EXP -OTHER COUNTY
   "E10", "C", 3,1,
   "E11", "W", 3,1,
   "E12", "E", 3,1, 
   "E13", "V", 3,1,
   "E14", "X", 3,1,     // LEASE PUBLIC LOW INCOME HOUSING
   "E15", "X", 3,1,     // LESSORS EXEMPT-ONE TIME
   "E16", "X", 3,1,     // LESSORS EXEMPT-ANNUAL
   "E17", "M", 3,1,
   "E18", "U", 3,1,
   "E19", "R", 3,1,
   "E20", "I", 3,1,
   "E21", "P", 3,1,
   "E23", "V", 3,1,
   "E25", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E26", "M", 3,1,
   "E27", "M", 3,1,
   "E28", "M", 3,1,
   "E30", "X", 3,1,     // TRIBAL LOW INCOME HOUSING
   "E31", "X", 3,1,     // LOW VALUE ORDINANCE
   "E44", "D", 3,1,     // WIDOW OF DISABLED VETERAN (LOW INCOME)
   "E45", "D", 3,1,     // WIDOW OF DISABLED VET (STANDARD)
   "E50", "Y", 3,1,
   "E51", "X", 3,1,     // GOVERNMENT ENTITIES
   "E60", "E", 3,1,     // PARTIAL CEM-LAND
   "E61", "E", 3,1,     // PARTIAL CEM-IMPRV
   "E62", "E", 3,1,     // PARTIAL CEM-PPROP
   "E70", "W", 3,1,
   "E71", "W", 3,1,
   "E72", "W", 3,1,     // PARTIAL WEL-PPROP
   "E77", "X", 3,1,     // LOW INCOME SUPPLEMENT
   "E78", "X", 3,1,     // LOW INCOME SUPPLEMENT
   "E79", "X", 3,1,     // LOW INCOME SUPPLEMENT
   "E80", "R", 3,1,
   "E81", "R", 3,1,
   "E82", "R", 3,1,
   "E87", "C", 3,1,
   "E88", "C", 3,1,
   "E89", "C", 3,1,
   "E90", "I", 3,1,     // PARTIAL HOSP-LAND
   "E91", "I", 3,1,     // PARTIAL HOSP-IMPS
   "E92", "I", 3,1,     // PARTIAL HOSP-PPROP
   "E98", "X", 3,1,     // OTHER
   "","",0,0
};

#endif