#ifndef  _MERGEKIN_H_
#define  _MERGEKIN_H_

// 08/19/2021
static XLAT_CODE  asHeating[] = 
{
   "B",  "8", 1,               // Baseboard
   "C",  "Z", 1,               // Central
   "E",  "7", 1,               // Electric
   "F",  "C", 1,               // Floor 
   "P",  "6", 1,               // Portable 
   "W",  "D", 1,               // Wall 
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "1", "1", 1,               // # fireplace
   "2", "2", 1,               // # fireplaces
   "Y", "Y", 1,               // Yes
   "N", "N", 1,               // No
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "B",   "C", 1,               // Both Pool & Spa
   "P&S", "C", 3,               // Pool & Spa
   "P",   "P", 1,               // Pool
   "S",   "S", 1,               // Spa
   "99",  "P", 2,               // Pool
   "", "",  0
};

// Ignore all doc types not shown in this list
IDX_TBL5 KIN_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "01", "1 ", 'N', 2, 2,     // FULL STAMPS NORMAL TRANSACTION
   "02", "1 ", 'N', 2, 2,     // FULL STAMPS SPLIT
   "03", "1 ", 'N', 2, 2,     // FULL STAMPS SALES W.O.P
   "04", "1 ", 'N', 2, 2,     // FULL STAMPS INTRA-FAM TRANS
   "92", "74", 'Y', 2, 2,     // ?
   "","",0,0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 KIN_Exemption[] = 
{
   "A", "X", 1,1,     // HISTORICAL
   "B", "X", 1,1,     // BUS INV EXEMPT
   "C", "C", 1,1,     // CHURCH
   "D", "D", 1,1,     // DISABLED VETERAN
   "E", "U", 1,1,     // COLLEGE EXEMPT
   "F", "X", 1,1,     // FLOOD RELIEF
   "G", "X", 1,1,     // GOVT ENTITIES
   "H", "H", 1,1,     // HOMEOWNER
   "L", "X", 1,1,     // LOW VALUE PROPERTY
   "M", "M", 1,1,     // MUSEUM
   "N", "Y", 1,1,     // SOLDIERS & SAILORS
   "R", "R", 1,1,     // RELIGIOUS
   "S", "P", 1,1,     // PUBLIC SCHOOL
   "T", "X", 1,1,     // RAILROAD
   "U", "U", 1,1,     // UNIVERSITY
   "V", "V", 1,1,     // VETERAN
   "W", "W", 1,1,     // WELFARE
   "","",0,0
};

#endif