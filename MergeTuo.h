
#if !defined(AFX_MERGETUO_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_)
#define AFX_MERGETUO_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_

// Unassessed - government property
#define  TUO_UA_APN           0
#define  TUO_UA_ENTITY        1
#define  TUO_UA_NAME          2
#define  TUO_UA_LOCATION      3
#define  TUO_UA_ACRES         4
#define  TUO_UA_REM1          5
#define  TUO_UA_REM2          6
#define  TUO_UA_REM3          7

// GrGr
#define  TUO_GR_PRSTAT        0
#define  TUO_GR_PRDOC         1     
#define  TUO_GR_PRSERV        2
#define  TUO_GR_PRTYPE        3
#define  TUO_GR_PRMNAME       4
#define  TUO_GR_PRFOLDER      5           
#define  TUO_GR_PRQUEUE       6
#define  TUO_GR_DOCNUM        7        
#define  TUO_GR_DOCTYPE       8
#define  TUO_GR_DOCDATE       9
#define  TUO_GR_GRANTOR       10
#define  TUO_GR_GRANTEE       11
#define  TUO_GR_BOOK          12
#define  TUO_GR_PAGE          13
#define  TUO_GR_APN           14
#define  TUO_GR_DOCTAX        15
#define  TUO_GR_COMMENTS      16

// GrGr - 07/01/2024
#define  TUO_GR1_DOCNUM       0        
#define  TUO_GR1_DOCDATE      1
#define  TUO_GR1_DOCTYPE      2
#define  TUO_GR1_NUMPAGE      3
#define  TUO_GR1_RELATEDDOC   4
#define  TUO_GR1_GRANTOR      5
#define  TUO_GR1_GRANTEE      6
#define  TUO_GR1_APN          7
#define  TUO_GR1_DOCTAX       8
#define  TUO_GR1_IMAGEPATH    9
#define  TUO_GR1_FLDS         10

// Version 5/9/2017
#define  TUO_CHAR_FEEPARCEL               0
#define  TUO_CHAR_POOLSPA                 1
#define  TUO_CHAR_CATTYPE                 2
#define  TUO_CHAR_QUALITYCLASS            3
#define  TUO_CHAR_YRBLT                   4
#define  TUO_CHAR_BUILDINGSIZE            5
#define  TUO_CHAR_ATTACHGARAGESF          6
#define  TUO_CHAR_DETACHGARAGESF          7
#define  TUO_CHAR_CARPORTSF               8
#define  TUO_CHAR_HEATING                 9
#define  TUO_CHAR_COOLINGCENTRALAC        10
#define  TUO_CHAR_COOLINGEVAPORATIVE      11
#define  TUO_CHAR_COOLINGROOMWALL         12
#define  TUO_CHAR_COOLINGWINDOW           13
#define  TUO_CHAR_STORIESCNT              14
#define  TUO_CHAR_UNITSCNT                15
#define  TUO_CHAR_TOTALROOMS              16
#define  TUO_CHAR_EFFYR                   17
#define  TUO_CHAR_PATIOSF                 18
#define  TUO_CHAR_BEDROOMS                19
#define  TUO_CHAR_BATHROOMS               20
#define  TUO_CHAR_HALFBATHS               21
#define  TUO_CHAR_FIREPLACE               22
#define  TUO_CHAR_ASMT                    23
#define  TUO_CHAR_BLDGSEQNUM              24
#define  TUO_CHAR_HASWELL                 25
#define  TUO_CHAR_LANDSQFT                26
#define  TUO_CHAR_LANDUSECODE1            27

// 08/26/2019 -Phys_char.txt
#define  TUO_PCHAR_FEEPARCEL             0
#define  TUO_PCHAR_DOCNUM                1
#define  TUO_PCHAR_CATTYPE               2
#define  TUO_PCHAR_BLDGSEQNUM            3
#define  TUO_PCHAR_YEARBUILT             4
#define  TUO_PCHAR_BUILDINGTYPE          5
#define  TUO_PCHAR_EFFECTIVEYEAR         6
#define  TUO_PCHAR_BUILDINGSIZE          7
#define  TUO_PCHAR_BUILDINGUSEDFOR       8
#define  TUO_PCHAR_STORIESCNT            9
#define  TUO_PCHAR_UNITSCNT              10
#define  TUO_PCHAR_CONDITION             11
#define  TUO_PCHAR_BEDROOMS              12
#define  TUO_PCHAR_BATHROOMS             13
#define  TUO_PCHAR_QUALITYCLASS          14
#define  TUO_PCHAR_HALFBATHS             15
#define  TUO_PCHAR_CONSTRUCTION          16
#define  TUO_PCHAR_FOUNDATION            17
#define  TUO_PCHAR_STRUCTURALFRAME       18
#define  TUO_PCHAR_STRUCTURALFLOOR       19
#define  TUO_PCHAR_EXTERIORTYPE          20
#define  TUO_PCHAR_ROOFCOVER             21
#define  TUO_PCHAR_ROOFTYPEFLAT          22
#define  TUO_PCHAR_ROOFTYPEHIP           23
#define  TUO_PCHAR_ROOFTYPEGABLE         24
#define  TUO_PCHAR_ROOFTYPESHED          25
#define  TUO_PCHAR_INSULATIONCEILINGS    26
#define  TUO_PCHAR_INSULATIONWALLS       27
#define  TUO_PCHAR_INSULATIONFLOORS      28
#define  TUO_PCHAR_WINDOWPANESINGLE      29
#define  TUO_PCHAR_WINDOWPANEDOUBLE      30
#define  TUO_PCHAR_WINDOWPANETRIPLE      31
#define  TUO_PCHAR_WINDOWTYPE            32
#define  TUO_PCHAR_LIGHTING              33
#define  TUO_PCHAR_COOLINGCENTRALAC      34
#define  TUO_PCHAR_COOLINGEVAPORATIVE    35
#define  TUO_PCHAR_COOLINGROOMWALL       36
#define  TUO_PCHAR_COOLINGWINDOW         37
#define  TUO_PCHAR_HEATING               38
#define  TUO_PCHAR_FIREPLACE             39
#define  TUO_PCHAR_GARAGE                40
#define  TUO_PCHAR_PLUMBING              41
#define  TUO_PCHAR_SOLAR                 42
#define  TUO_PCHAR_BUILDER               43
#define  TUO_PCHAR_BLDGDESIGNEDFOR       44
#define  TUO_PCHAR_MODELDESC             45
#define  TUO_PCHAR_UNFINAREASSF          46
#define  TUO_PCHAR_ATTACHGARAGESF        47
#define  TUO_PCHAR_DETACHGARAGESF        48
#define  TUO_PCHAR_CARPORTSF             49
#define  TUO_PCHAR_DECKSSF               50
#define  TUO_PCHAR_PATIOSF               51
#define  TUO_PCHAR_CEILINGHEIGHT         52
#define  TUO_PCHAR_FIRESPINKLERS         53
#define  TUO_PCHAR_AVGWALLHEIGHT         54
#define  TUO_PCHAR_BAY                   55
#define  TUO_PCHAR_DOCK                  56
#define  TUO_PCHAR_ELEVATOR              57
#define  TUO_PCHAR_ESCALATOR             58
#define  TUO_PCHAR_ROLLUPDOOR            59
#define  TUO_PCHAR_FIELD1                60
#define  TUO_PCHAR_FIELD2                61
#define  TUO_PCHAR_FIELD3                62
#define  TUO_PCHAR_FIELD4                63
#define  TUO_PCHAR_FIELD5                64
#define  TUO_PCHAR_FIELD6                65
#define  TUO_PCHAR_FIELD7                66
#define  TUO_PCHAR_FIELD8                67
#define  TUO_PCHAR_FIELD9                68
#define  TUO_PCHAR_FIELD10               69
#define  TUO_PCHAR_FIELD11               70
#define  TUO_PCHAR_FIELD12               71
#define  TUO_PCHAR_FIELD13               72
#define  TUO_PCHAR_FIELD14               73
#define  TUO_PCHAR_FIELD15               74
#define  TUO_PCHAR_FIELD16               75
#define  TUO_PCHAR_FIELD17               76
#define  TUO_PCHAR_FIELD18               77
#define  TUO_PCHAR_FIELD19               78
#define  TUO_PCHAR_FIELD20               79
#define  TUO_PCHAR_LANDFIELD1            80
#define  TUO_PCHAR_LANDFIELD2            81
#define  TUO_PCHAR_LANDFIELD3            82
#define  TUO_PCHAR_LANDFIELD4            83
#define  TUO_PCHAR_LANDFIELD5            84
#define  TUO_PCHAR_LANDFIELD6            85
#define  TUO_PCHAR_LANDFIELD7            86
#define  TUO_PCHAR_LANDFIELD8            87
#define  TUO_PCHAR_ACRES                 88
#define  TUO_PCHAR_NEIGHBORHOODCODE      89
#define  TUO_PCHAR_ZONING                90
#define  TUO_PCHAR_TOPOGRAPHY            91
#define  TUO_PCHAR_VIEWCODE              92
#define  TUO_PCHAR_POOLSPA               93
#define  TUO_PCHAR_WATERSOURCE           94
#define  TUO_PCHAR_SUBDIVNAME            95
#define  TUO_PCHAR_SEWERCODE             96
#define  TUO_PCHAR_UTILITIESCODE         97
#define  TUO_PCHAR_ACCESSCODE            98
#define  TUO_PCHAR_LANDSCAPE             99
#define  TUO_PCHAR_PROBLEMCODE           100
#define  TUO_PCHAR_FRONTAGE              101
#define  TUO_PCHAR_LOCATION              102
#define  TUO_PCHAR_PLANTEDACRES          103
#define  TUO_PCHAR_ACRESUNUSEABLE        104
#define  TUO_PCHAR_WATERSOURCEDOMESTIC   105
#define  TUO_PCHAR_WATERSOURCEIRRIGATION 106
#define  TUO_PCHAR_HOMESITES             107
#define  TUO_PCHAR_PROPERTYCONDITIONCODE 108
#define  TUO_PCHAR_ROADTYPE              109
#define  TUO_PCHAR_HASWELL               110
#define  TUO_PCHAR_HASCOUNTYROAD         111
#define  TUO_PCHAR_HASVINEYARD           112
#define  TUO_PCHAR_ISUNSECUREDBUILDING   113
#define  TUO_PCHAR_HASORCHARD            114
#define  TUO_PCHAR_HASGROWINGIMPRV       115
#define  TUO_PCHAR_SITECOVERAGE          116
#define  TUO_PCHAR_PARKINGSPACES         117
#define  TUO_PCHAR_EXCESSLANDSF          118
#define  TUO_PCHAR_FRONTFOOTAGESF        119
#define  TUO_PCHAR_MULTIPARCELECON       120
#define  TUO_PCHAR_LANDUSECODE1          121
#define  TUO_PCHAR_LANDUSECODE2          122
#define  TUO_PCHAR_LANDSQFT              123
#define  TUO_PCHAR_TOTALROOMS            124
#define  TUO_PCHAR_NETLEASABLESF         125
#define  TUO_PCHAR_BLDGFOOTPRINTSF       126
#define  TUO_PCHAR_OFFICESPACESF         127
#define  TUO_PCHAR_NONCONDITIONSF        128
#define  TUO_PCHAR_MEZZANINESF           129
#define  TUO_PCHAR_PERIMETERLF           130
#define  TUO_PCHAR_ASMT                  131
#define  TUO_PCHAR_ASMTCATEGORY          132
#define  TUO_PCHAR_EVENTDATE             133
#define  TUO_PCHAR_CONFSALESPRICE        134
#define  TUO_PCHAR_FLDS                  135

#define  TUO_OWNR_ASMT              0
#define  TUO_OWNR_ISPRIMARY         1
#define  TUO_OWNR_TITLE             2
#define  TUO_OWNR_OWNERFIRST        3
#define  TUO_OWNR_OWNERMIDDLE       4
#define  TUO_OWNR_OWNERLAST         5
#define  TUO_OWNR_OWNERSHIPPCT      6
#define  TUO_OWNR_DOCNUM            7
#define  TUO_OWNR_TITLETYPE         8
#define  TUO_OWNR_HWCODE            9
#define  TUO_OWNR_DTS               10
#define  TUO_OWNR_OWNERFULL         11

#define  TUO_VALUE_YEAR             0
#define  TUO_VALUE_TRA              1
#define  TUO_VALUE_LICNO            2
#define  TUO_VALUE_LEGAL            3
#define  TUO_VALUE_SADDR            4
#define  TUO_VALUE_APN              5
#define  TUO_VALUE_LAND             6
#define  TUO_VALUE_IMPR             7
#define  TUO_VALUE_OWNER1           8
#define  TUO_VALUE_PPVAL            9
#define  TUO_VALUE_OWNER2           10
#define  TUO_VALUE_FIXTVAL          11
#define  TUO_VALUE_FIXTTYPE         12
#define  TUO_VALUE_MADDR            13
#define  TUO_VALUE_EXEAMT           14
#define  TUO_VALUE_EXETYPE          15
#define  TUO_VALUE_CITYST           16
#define  TUO_VALUE_NET              17
#define  TUO_VALUE_ZIP              18
#define  TUO_VALUE_BYVVAL           19
#define  TUO_VALUE_COLS             20

static XLAT_CODE  asCond[] =
{
   // Value, lookup code, value length
   "F", "F",  1,              // Fair
   "G", "G",  1,              // Good
   "P", "P",  1,              // Poor
   "U", "U",  1,              // Unusable
   "AV","A",  1,              // Average
   "EX","E",  1,              // Excellent
   "",  "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "1",   "1", 1,             // 1 Fireplace
   "2",   "2", 1,             // 2 Fireplaces
   "3",   "3", 1,             // 3 or more Fireplaces
   "C",   "G", 1,             // Combination FP(s) & Stove(s)
   "F",   "F", 1,             // Free Standing Stove(s)
   "N",   "N", 1,             // No FP or Stove
   "WS",  "W", 2,             // Wood Stove
   "",   "", 0
};

static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "C",  "Z", 1,              // Central
   "B",  "F", 1,              // Baseboard
   "W",  "D", 1,              // Wall
   "E",  "F", 1,              // Electric
   "F",  "B", 1,              // Forced air
   "H",  "G", 1,              // Heat pump
   "O",  "X", 1,              // Other
   "R",  "I", 1,              // Radiant
   "G",  "4", 1,              // Geo Thermal
   "Y",  "9", 1,              // Unknown?
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "1", "P", 1,               // 1 pool
   "2", "P", 1,               // 2 pools
   "PS","C", 2,               // Pool/Spa
   "S", "S", 1,               // Spa
   "",   "", 0
};

static XLAT_CODE  asSewer[] =
{
   // Value, lookup code, value length
   "P", "P", 1,               // Public
   "S", "S", 1,               // Septic
   "",   "",  0
};

static XLAT_CODE  asWaterSrc[] =
{
   // Value, lookup code, value length
   "M", "P", 1,               // Municipal
   "W", "W", 1,               // Private Well
   "D", "Y", 1,
   "N", "N", 1,
   "SW", "Y", 2,
   "",  "",  0
};

static XLAT_CODE  asView[] =
{
   // Value, lookup code, value length
   "GC", "M", 2,              // Golf course
   "L",  "F", 1,              // Lake view
   "M",  "T", 1,              // Mountain view
   "R",  "E", 1,              // River view
   "V",  "A", 1,              // View
   "",   "",  0
};

// Roof material
static XLAT_CODE  asRoofMat[] =
{
   // Value, lookup code, value length
   "CS",          "C", 2,               // Composition shingle
   "C",           "Y", 1,               // Concrete
   "WS",          "A", 2,               // Wood Shingle
   "M",           "L", 1,               // Metal
   "TG",          "F", 2,               // Tar & Gravel
   "T",           "I", 1,               // Tile
   "",   "",  0
};

static IDX_TBL5  asXferTypes[] =
{
   "A",  "F", 'Y', 1, 1,      // General Sale
   "FR", "F", 'Y', 2, 1,      // Full value
   "FV", "F", 'Y', 2, 1,      // Full value
   "WL", "I", 'Y', 2, 1,      // With other properties less lien
   "LL", "N", 'Y', 2, 1,      // Less lien
   "WP", "W", 'Y', 2, 1,      // With other properties
   "M",  "P", 'Y', 1, 1,      // Less than 5%
   "PR", "P", 'Y', 2, 1,      // Partial Revalue
   "",   "",  'Y', 0, 0
};

IDX_TBL5 TUO_DocCode[] =
{  // DocCode, Index, Non-sale, DocCodelen, Indexlen
   "00", "74", 'Y', 2, 2,     // Non Reappraisal 100%
   "01", "1 ", 'N', 2, 2,     // General Sale 100%
   "02", "57", 'N', 2, 2,     // Partial Interest Reappraisal
   "03", "74", 'Y', 2, 2,     // Partial Interest Non Reappraisal
   "04", "74", 'Y', 2, 2,     // Prop 8 Review
   "05", "74", 'Y', 2, 2,     // Stamp
   "09", "74", 'Y', 2, 2,     // Split/Combo
   "10", "75", 'Y', 2, 2,     // Prop 58 100%
   "11", "75", 'Y', 2, 2,     // Prop 58 Partial Interest
   "12", "74", 'Y', 2, 2,     // Prop 60
   "23", "74", 'Y', 2, 2,     // Lessors Interest 35 Years or More
   "24", "74", 'Y', 2, 2,     // Lessors Interest 35 Years or More (partial)
   "","",0,0,0
};

// DocTitle for new GRGR 10/15/2024
IDX_TBL5 TUO_DocTitle[] =
{  // DocTitle, Index, Non-sale, len1, len2
   "21-Q"            , "4 ", 'Y', 4, 2,  // Quit claim Deed
   "21"              , "1 ", 'N', 2, 2,  // Grant Deed
   "11"              , "7 ", 'Y', 2, 2,  // Assignment D/T
   "13"              , "19", 'Y', 2, 2,  // Agreement (all)
   "14"              , "32", 'N', 2, 2,  // Bill of Sale
   "22"              , "40", 'Y', 2, 2,  // Deed Easement
   "23"              , "13", 'N', 2, 2,  // Deed In Lieu - Short sale
   "25"              , "65", 'Y', 3, 2,  // DEED OF TRUST
   "33"              , "19", 'Y', 2, 2,  // Lease memo
   "64"              , "19", 'Y', 2, 2,  // Reconveyance
   "84"              , "19", 'Y', 2, 2,  // Substitution of trustee/Recon
   "86"              , "67", 'N', 2, 2,  // Tax Deed
   "91"              , "27", 'N', 2, 2,  // Trustees deed
   "95"              , "6 ", 'Y', 2, 2,  // Affidavit of death - misc
   "97"              , "8 ", 'Y', 2, 2,  // Agreement of Sale
   "DEED            ", "1 ", 'N', 4, 2,  // DEED
   "TAXDEED         ", "67", 'N', 5, 2,  // TAX DEED
   "TD              ", "27", 'N', 2, 2,  // TRUSTEES DEED
   "BS              ", "32", 'N', 2, 2,  // BILL SALE
   "ABDEA           ", "40", 'Y', 5, 2,  // ABANDOMENT EASEMENT
   "ABDHM           ", "19", 'Y', 5, 2,  // ABANDONMENT HOMESTEAD
   "ABDNHWY         ", "19", 'Y', 5, 2,  // RELINQUISH/ABANDMENT HIGHWAY
   "ABJDGT          ", "19", 'Y', 5, 2,  // ABSTRACT JUDGEMENT
   "ABSTJD          ", "19", 'Y', 5, 2,  // ABSTRACT SUPPORT JUDGEMENT
   "ACCEPT          ", "19", 'Y', 5, 2,  // ACCEPTANCE SUCESSOR TRUSTEE
   "ACCT            ", "19", 'Y', 4, 2,  // ACCOUNT PAYMENT
   "ADJ             ", "19", 'Y', 3, 2,  // RECEIPT ADJUSTMENT
   "ADJUST          ", "19", 'Y', 4, 2,  // ADJUSTMENT FOR UNDER PAYMENT
   "AFF             ", "6 ", 'Y', 3, 2,  // AFFIDAVIT
   "AFFDEATH        ", "6 ", 'Y', 3, 2,  // AFFIDAVIT OF DEATH
   "AFFID           ", "6 ", 'Y', 3, 2,  // AFFADAVIT OF INDENITY
   "AFFMOH          ", "6 ", 'Y', 3, 2,  // AFFIDAVIT OF MOBILEHOME AFFIXIATION
   "AGRMT           ", "19", 'Y', 4, 2,  // AGREEMENT
   "AGRSALE         ", "8 ", 'Y', 4, 2,  // AGREEMENT OF SALE
   "ASSIGN          ", "7 ", 'Y', 4, 2,  // ASSIGNMENT
   "ASSIGNDT        ", "7 ", 'Y', 4, 2,  // ASSIGNMENT DEED OF TRUST
   "ASSIGNRENT      ", "7 ", 'Y', 4, 2,  // ASSIGNMENT OF RENTS
   "ASSLEASE        ", "7 ", 'Y', 4, 2,  // ASSIGNMENT OF LEASE
   "ASSMTLIE        ", "19", 'Y', 4, 2,  // ASSESSMENT LIEN
   "ASSUMP          ", "19", 'Y', 4, 2,  // ASSUMPTION AGREEMENT
   "ATTORN          ", "19", 'Y', 4, 2,  // ATTORNMENT AGREEMENT
   "BT              ", "19", 'Y', 2, 2,  // BALANCE TRANSFER
   "BYLAW           ", "19", 'Y', 4, 2,  // BYLAWS - AMENDED
   "CANCEL          ", "19", 'Y', 4, 2,  // CANCELLATION
   "CERTIF          ", "62", 'Y', 6, 2,  // CERTIFICATE
   "CERTIFDEL       ", "19", 'Y', 7, 2,  // CERTIFICATE OF DELINQUENT TRANSIENT
   "CERTTR          ", "19", 'Y', 6, 2,  // CERTIFICATION OF TRUST
   "COMPRAG         ", "19", 'Y', 4, 2,  // COMMUNITY PROPERTY AGREEMENT
   "CONDEM          ", "19", 'Y', 4, 2,  // FINAL ORDER CONDEMNATION
   "CONDO           ", "19", 'Y', 4, 2,  // CONDOMINIUM PLAN
   "CONSDEED        ", "40", 'Y', 5, 2,  // CONSERVATION EASEMENT DEED
   "CONSENT         ", "19", 'Y', 5, 2,  // CONSENT LIENHOLDER & SUBORDINATION
   "COPIESM         ", "19", 'Y', 4, 2,  // COPIES - MISC
   "CORP            ", "19", 'Y', 4, 2,  // ARTICLES OF INCORPORATION
   "CTFCOMP         ", "19", 'Y', 4, 2,  // CERTIFICATE OF COMPLETION
   "CTORD           ", "80", 'Y', 4, 2,  // COURT ORDER
   "DECCON          ", "19", 'Y', 4, 2,  // DECLARATION CONSERVATORSHIP
   "DECHMS          ", "19", 'Y', 4, 2,  // DECLARATION OF HOMESTEAD
   "DECLN           ", "19", 'Y', 4, 2,  // DECLARATION
   "DECLNTR         ", "19", 'Y', 4, 2,  // DECLARATION OF TRUST
   "DEV             ", "19", 'Y', 3, 2,  // DEVELOPMENT AGREEMENT
   "DISCLAIM        ", "19", 'Y', 4, 2,  // DISCLAIMER OF INTEREST
   "DISPART         ", "19", 'Y', 4, 2,  // DISOLUTION OF PARTNERSHIP
   "DOCUMENTADJUST  ", "19", 'Y', 4, 2,  // DOCUMENT ADJUSTMENT
   "DTR             ", "65", 'Y', 3, 2,  // DEED OF TRUST
   "EASE            ", "40", 'Y', 4, 2,  // EASEMENT
   "ENCPERM         ", "19", 'Y', 4, 2,  // ENCROACHMENT PERMIT
   "FINSTASGT       ", "19", 'Y', 4, 2,  // FINANCIAL STATEMENT - ASSIGNMENT
   "FINSTMT         ", "19", 'Y', 4, 2,  // FINANCING STATEMENT
   "FINTERM         ", "19", 'Y', 4, 2,  // FINANCIAL STATEMENT - TERMINATION
   "HAZAGR          ", "19", 'Y', 4, 2,  // HAZARDOUS AGREEMENT
   "HISPREAGR       ", "19", 'Y', 4, 2,  // HISTORIC PRESERVE AGREEMENT
   "JDGT            ", "19", 'Y', 2, 2,  // JUDGMENT
   "LANDACT         ", "19", 'Y', 4, 2,  // LAND ACT CONTRACT
   "LANDLORD        ", "19", 'Y', 4, 2,  // LANDLORD WAIVER
   "LCONAGR         ", "19", 'Y', 4, 2,  // LESSORS CONSENT & AGREEMENT
   "LCONSENT        ", "19", 'Y', 4, 2,  // LANDLORD CONSENT
   "LEASE           ", "44", 'Y', 5, 2,  // LEASE
   "LEASEMEM        ", "19", 'Y', 6, 2,  // LEASE MEMO
   "LEASEOPT        ", "19", 'Y', 6, 2,  // LEASE OPTION
   "LEASSAG         ", "19", 'Y', 5, 2,  // LESSEE ASSIGNMENT
   "LETTERS         ", "19", 'Y', 4, 2,  // LETTERS
   "LIEN            ", "46", 'Y', 4, 2,  // LIEN
   "LOANMOD         ", "19", 'Y', 4, 2,  // LOAN MODIFICATION AGREEMENT
   "LODE            ", "19", 'Y', 4, 2,  // LODE CLAIM
   "MAINT           ", "19", 'Y', 4, 2,  // MAINTENANCE AGREEMENT
   "MECHLIEN        ", "46", 'Y', 4, 2,  // MECHANICS LIEN
   "MHINSTALL       ", "19", 'Y', 4, 2,  // MOBLIEHOME INSTALL
   "MILDIS          ", "19", 'Y', 4, 2,  // MILITARY DISCHARGE
   "MISC            ", "19", 'Y', 4, 2,  // MISCELLANEOUS
   "MISCREV         ", "19", 'Y', 4, 2,  // MISCELLANEOUS REVENUE
   "MOD             ", "19", 'Y', 3, 2,  // MODIFICATION AGREEMENT
   "MODPTP          ", "19", 'Y', 4, 2,  // MODIFICATION OF PARTNERSHIP
   "MTG             ", "19", 'Y', 3, 2,  // MORTGAGE
   "MULTI           ", "19", 'Y', 4, 2,  // MULTI-TITLE
   "NOTBOND         ", "19", 'Y', 4, 2,  // NOTARY BOND
   "NTABATE         ", "19", 'Y', 5, 2,  // NOTICE OF ABATEMENT LIEN
   "NTABMC          ", "19", 'Y', 5, 2,  // NOTICE OF ABANDON MINING CLAIM
   "NTACT           ", "19", 'Y', 5, 2,  // NOTICE OF ACTION
   "NTASS           ", "19", 'Y', 5, 2,  // NOTICE OF ASSESSMENT LIEN
   "NTBULK          ", "19", 'Y', 5, 2,  // NOTICE OF BULK SALE
   "NTCAN           ", "19", 'Y', 5, 2,  // NOTICE OF CANCELLATION
   "NTCOM           ", "19", 'Y', 5, 2,  // NOTICE OF COMPLETION
   "NTCONS          ", "19", 'Y', 5, 2,  // NOTICE OF CONSENT
   "NTDEF           ", "52", 'Y', 5, 2,  // NOTICE OF DEFAULT
   "NTDELQ          ", "19", 'Y', 5, 2,  // NOTICE OF DELINQUENT
   "NTDISAGR        ", "19", 'Y', 5, 2,  // NOTICE OF OWNERS DISCLOSURE AGREEMENT
   "NTIN            ", "19", 'Y', 4, 2,  // NOTICE INTENT TO HOLD
   "NTINTER         ", "19", 'Y', 6, 2,  // NOTICE OF INTEREST
   "NTINTMIN        ", "19", 'Y', 6, 2,  // NOTICE OF INTENT PRESERVE MINERAL RIGHTS
   "NTINTS          ", "19", 'Y', 6, 2,  // NOTICE OF INTENT SALE
   "NTLANDACT       ", "19", 'Y', 5, 2,  // NOTICE OF NON-RENEWAL LAND ACT
   "NTLEV           ", "19", 'Y', 5, 2,  // NOTICE OF LEVY
   "NTLIENS         ", "19", 'Y', 5, 2,  // NOTICE OF LIEN SALE
   "NTLISP          ", "19", 'Y', 5, 2,  // NOTICE OF LIS PENDENS
   "NTNONR          ", "19", 'Y', 5, 2,  // NOTICE OF NON RESPONSIBILTY
   "NTPROX          ", "19", 'Y', 5, 2,  // NOTICE OF AIRPORT PROXIMITY
   "NTRESTR         ", "19", 'Y', 5, 2,  // NOTICE OF RESIGNATION OF TRUSTEE
   "NTSOLAR         ", "19", 'Y', 5, 2,  // NOTICE OF SOLAR ENERGY CONTRACT
   "NTSUPPAY        ", "19", 'Y', 5, 2,  // NOTICE OF SUPPORT PAYMENT
   "NTTRSAL         ", "19", 'Y', 5, 2,  // NOTICE OF TRUSTEES SALE
   "NTV             ", "19", 'Y', 3, 2,  // NOTICE OF VIOLATION
   "OFFER           ", "19", 'Y', 3, 2,  // OFFER DEDICATE
   "OPT             ", "19", 'Y', 3, 2,  // OPTION PURCHASE
   "ORDER           ", "19", 'Y', 4, 2,  // ORDER
   "ORDERAT         ", "19", 'Y', 4, 2,  // ORDER APPOINT TRUSTEE
   "ORDERBNK        ", "19", 'Y', 4, 2,  // ORDER BANKRUPCY
   "ORDERDIS        ", "19", 'Y', 4, 2,  // ORDER DISTRIBUTION
   "PCOR            ", "19", 'Y', 4, 2,  // CHANGE IN OWNERSHIP
   "PLACER          ", "19", 'Y', 4, 2,  // PLACER CLAIM
   "POO             ", "19", 'Y', 3, 2,  // PROOF OF OWNERSHIP
   "POSTPONE        ", "46", 'Y', 4, 2,  // LIEN - PROP TAX POSTPONEMENT
   "POT             ", "19", 'Y', 3, 2,  // POWER OF ATTORNEY
   "PRELIM          ", "19", 'Y', 4, 2,  // PRELIMINARY 20-DAY NOTICE
   "PROJECT         ", "19", 'Y', 4, 2,  // PROJECT MANUAL
   "RECON           ", "37", 'Y', 4, 2,  // RECONVEYANCE
   "RECONPAR        ", "37", 'Y', 4, 2,  // PARTIAL RECONVEYANCE
   "RECREJ          ", "19", 'Y', 4, 2,  // OFFICIAL RECORDS REJECTION
   "RECSUR          ", "19", 'Y', 4, 2,  // RECORD SURVEY
   "REGAGM          ", "19", 'Y', 4, 2,  // REGULATORY AGREEMENT
   "REL             ", "19", 'Y', 3, 2,  // RELEASES
   "RELAB           ", "19", 'Y', 4, 2,  // RELEASE ABATEMENT LIEN
   "RELASLIEN       ", "19", 'Y', 4, 2,  // RELEASE ASSEMENT LIEN
   "RELASRENT       ", "19", 'Y', 4, 2,  // RELEASE ASSIGNMENT OF RENTS
   "RELDEV          ", "19", 'Y', 4, 2,  // RELEASE OF DEVELOPMENT AGREEMENT
   "RELEQ           ", "19", 'Y', 4, 2,  // RELEASE OF EQUITY
   "RELJDGT         ", "19", 'Y', 4, 2,  // RELEASE OF JUDGMENT
   "RELJDGTPAR      ", "19", 'Y', 4, 2,  // PARTIAL RELEASE OF JUDGMENT
   "RELLEV          ", "19", 'Y', 4, 2,  // RELEASE LEVY
   "RELLIEN         ", "19", 'Y', 4, 2,  // RELEASE LIEN
   "RELLISPEN       ", "19", 'Y', 4, 2,  // RELEASE LIS PENDES
   "RELMLIEN        ", "19", 'Y', 4, 2,  // RELEASE MECHANICS LIEN
   "RELNTAC         ", "19", 'Y', 4, 2,  // REL NOTICE OF ACTION
   "RELOBLIG        ", "19", 'Y', 4, 2,  // RELEASE OF OBLIGATION
   "RELOPT          ", "19", 'Y', 4, 2,  // RELEASE OPTION TO PURCHASE
   "RELPOST         ", "19", 'Y', 4, 2,  // RELEASE PROPERTY TAX POSTPONEMENT
   "RELSEC          ", "19", 'Y', 4, 2,  // RELEASE SECURITY AGREEMENT
   "RENT            ", "19", 'Y', 4, 2,  // RENT LIMIT & TENANCY AGREEMENT
   "REQNT           ", "19", 'Y', 4, 2,  // REQUEST NOTICE OF DEFAULT
   "REQNTDEL        ", "19", 'Y', 4, 2,  // REQUEST NOTICE OF DELINQUENCY
   "REQNTTR         ", "19", 'Y', 4, 2,  // REQUEST NOTICE OF TRUSTEES DEED
   "RESCD           ", "19", 'Y', 5, 2,  // RESCISSION OF DEED
   "RESCDCCR        ", "19", 'Y', 5, 2,  // RESCISSION OF DECLARATION OF COVENENTS CONDITIONS & RESTRICTIONS
   "RESCREC         ", "19", 'Y', 5, 2,  // RESCISSION OF RECONVEYANCE
   "RESIGN          ", "19", 'Y', 5, 2,  // RESIGNATION OF TRUSTEE
   "RESNOA          ", "19", 'Y', 5, 2,  // RESCISSION NOTICE OF ACTION
   "RESO            ", "19", 'Y', 4, 2,  // RESOLUTION/ORDINANCE
   "REVOCD          ", "19", 'Y', 4, 2,  // REVOCATION OF DEED
   "REVOCPOA        ", "19", 'Y', 4, 2,  // REVOCATION POWER OF ATTORNEY
   "REVOCR          ", "19", 'Y', 4, 2,  // REVOCATION RELEASE TAX LIEN
   "REVOCT          ", "19", 'Y', 4, 2,  // REVOCATION OF TRUST AGREEMENT
   "REVOCTD         ", "19", 'Y', 4, 2,  // REVOCATION TRANSFER ON DEATH
   "REVOCWL         ", "19", 'Y', 4, 2,  // REVOCATION WATER LICENSE
   "REVOVNC         ", "19", 'Y', 4, 2,  // REVOCATION NOTICE OF CONSENT
   "RLRO            ", "19", 'Y', 4, 2,  // RELEASE OF LEASE
   "RPL             ", "19", 'Y', 3, 2,  // PARTIAL RELEASE OF LIEN
   "RPTL            ", "19", 'Y', 3, 2,  // PARTIAL RELEASE TAX LIEN
   "RTL             ", "19", 'Y', 3, 2,  // RELEASE TAX LIEN
   "SECAGR          ", "19", 'Y', 3, 2,  // SECURITY AGREEMENT
   "SPOUSAL         ", "19", 'Y', 3, 2,  // SPOUSAL PROPERTY ODER
   "STANDARDLIENNOTI", "19", 'Y', 3, 2,  // STANDARD LIEN NOTIFICATION/JUDGMENTS
   "STMTPTP         ", "19", 'Y', 3, 2,  // STATEMENT OF PARTNERSHIP
   "SUBAGR          ", "19", 'Y', 4, 2,  // SUBDIVISION AGREEMENT
   "SUBCOPY         ", "19", 'Y', 4, 2,  // SUBSCRIPTION COPIES
   "SUBGUAR         ", "19", 'Y', 4, 2,  // SUBDIVISON GUARANTEE
   "SUBMAP          ", "19", 'Y', 4, 2,  // SUBDIVISION MAP
   "SUBRDN          ", "19", 'Y', 4, 2,  // SUBORDINATE AGREEMENT
   "SUBRDT          ", "19", 'Y', 4, 2,  // SUBORDINATE DEED OF TRUST
   "SUBTR           ", "19", 'Y', 4, 2,  // SUBSTITUTION OF TRUSTEE
   "SUPPI           ", "19", 'Y', 4, 2,  // SUPPLEMENTAL INDENTURE
   "SURETY          ", "19", 'Y', 4, 2,  // SURETY BOND
   "TAXCJ           ", "19", 'Y', 4, 2,  // TAX COLLECTION JUDGMENT
   "TAXDELINQ       ", "19", 'Y', 5, 2,  // TAX DELINQUENT LIST
   "TAXDP           ", "19", 'Y', 5, 2,  // TAX DEFAUT PROPERTY
   "TAXLIEN         ", "46", 'Y', 5, 2,  // TAX LIEN
   "TENCA           ", "19", 'Y', 4, 2,  // TENANCY IN COMMON AGREEMENT
   "TERMAGR         ", "19", 'Y', 4, 2,  // TERMINATION OF AGREEMENT
   "TERMJT          ", "19", 'Y', 4, 2,  // TERMINATION OF JOINT TENANCY
   "TERMLE          ", "19", 'Y', 4, 2,  // TERMINATION LIFE ESTATE
   "TERMNTDIS       ", "19", 'Y', 4, 2,  // TERMINATION OF OWNER'S DISCLOSURE AGREEMENT
   "TIMBER          ", "19", 'Y', 4, 2,  // TIMBERLAND PERMIT
   "TOD             ", "19", 'Y', 3, 2,  // TRANSFER ON DEATH DEED
   "TRDC            ", "19", 'Y', 3, 2,  // TRANSFER DECLARATION
   "TT              ", "19", 'Y', 2, 2,  // TRANS TAX COUNTY
   "TTX             ", "19", 'Y', 3, 2,  // TRANSFER TAX
   "WITHLP          ", "19", 'Y', 6, 2,  // WITHDRAWL LIS PENDENS
   "WRIT            ", "19", 'Y', 4, 2,  // WRIT ATTACHMENT
   "","",' ',0,0
};

IDX_TBL5 TUO_DocTitleOld[] =
{  // DocTitle, Index, Non-sale, len1, len2
   "21-Q","4 ", 'Y', 4, 2,  // Quit claim Deed
   "21", "1 ", 'N', 2, 2,  // Grant Deed
   "14", "32", 'N', 2, 2,  // Bill of Sale
   "86", "67", 'N', 2, 2,  // Tax Deed
   "22", "40", 'Y', 2, 2,  // Deed Easement
   "23", "13", 'N', 2, 2,  // Deed In Lieu - Short sale
   "10", "7 ", 'Y', 2, 2,  // Assignment
   "11", "7 ", 'Y', 2, 2,  // Assignment D/T
   "12", "7 ", 'Y', 2, 2,  // Assignment Lien
   "13", "19", 'Y', 2, 2,  // Agreement (all)
   "16", "19", 'Y', 2, 2,  // CNC tax default
   //"25-4", "65", 'Y', 2, 2,  // D/T with Assignment of Rent
   //"25", "65", 'Y', 2, 2,  // D/T
   //"26", "19", 'Y', 2, 2,  // D/T & ASGT
   "31", "46", 'Y', 2, 2,  // Lien
   "32", "44", 'Y', 2, 2,  // Lease
   "36", "46", 'Y', 2, 2,  // Lien - Mechanics
   "45", "52", 'Y', 2, 2,  // Notice of Default
   "52", "80", 'Y', 2, 2,  // Order Final Dist.
   "5",  "6 ", 'Y', 1, 2,  // Affidavit - death spouse
   "67", "19", 'Y', 2, 2,  // Release assmt lien
   "6",  "6 ", 'Y', 1, 2,  // Affidavit - Change of Trustee
   "72", "19", 'Y', 2, 2,  // Release lien
   "73", "19", 'Y', 2, 2,  // Release LIS Pendes
   "74", "19", 'Y', 2, 2,  // Release lien mechs
   "75", "19", 'Y', 2, 2,  // Release tax lien
   "76", "19", 'Y', 2, 2,  // Req Notice of Delinq
   "77", "19", 'Y', 2, 2,  // Req Notice of Default
   "7",  "6 ", 'Y', 1, 2,  // Affidavit - misc
   "83", "19", 'Y', 2, 2,  // Substitution of Trustee
   "84", "19", 'Y', 2, 2,  // Substitution of Trustee
   "87", "19", 'Y', 2, 2,  // Tax Deflt prop
   "88", "46", 'Y', 2, 2,  // Tax lien
   "91", "27", 'N', 2, 2,  // Trustees deed
   "92", "19", 'Y', 2, 2,  // Release - misc
   "95", "6 ", 'Y', 2, 2,  // Affidavit of death - misc
   "97", "8 ", 'Y', 2, 2,  // Agreement of Sale
   "99", "19", 'Y', 2, 2,  // Misc docs
   "","",' ',0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 TUO_Exemption[] = 
{
   "E01", "H", 3,1,     // HOMEOWNERS EXEMPTION
   "E04", "X", 3,1,     // LESSOR/LESSEE UNSECURED
   "E06", "X", 3,1,     // LESSOR/LESSEE SECURED
   "E07", "X", 3,1,     // LOW VALUE EXEMPTION
   "E08", "X", 3,1,     // LOW VALUE EXEMPTION UNSECURED
   "E20", "D", 3,1,     // DISABLED VETERANS
   "E25", "D", 3,1,     // DISABLED VETERAN- INCOME
   "E60", "W", 3,1,     // WELFARE
   "E61", "W", 3,1,     // WELFARE UNSECURED
   "E62", "R", 3,1,     // WELFARE AND RELIGIOUS
   "E63", "I", 3,1,     // WELFARE - HOSPITAL
   "E64", "S", 3,1,     // WELFARE - PRIVATE SCHOOL
   "E65", "I", 3,1,     // WELFARE UNSECURED HOSPITAL
   "E70", "C", 3,1,     // CHURCH
   "E80", "R", 3,1,     // RELIGIOUS
   "E81", "R", 3,1,     // RELIGIOUS UNSECURED
   "E91", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E92", "S", 3,1,     // CHARTER SCHOOLS
   "E93", "M", 3,1,     // MUSEUM
   "E95", "L", 3,1,     // LIBRARY
   "E96", "E", 3,1,     // CEMETERY
   "E98", "X", 3,1,     // OTHER
   "","",0,0
};

#endif // !defined(AFX_MERGETUO_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_)
