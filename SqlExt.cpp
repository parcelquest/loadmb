/****************************************************************************
 *
 * 11/18/2010 1.3.0     Modify locateCity() to support county specific table.
 * 07/13/2016 16.1.0    Add generic version of sqlConnect(), execSqlCmd(), and RunSP()
 * 09/03/2018 18.4.1    Add log msg
 * 06/16/2019 18.12.10  Add new version of locateCity() & locateCityX() using standard ZipCodes table.
 *
 ****************************************************************************/

#include "stdafx.h"
#include "Logs.h"
#include "Utils.h"

#define _SQLEXT_CPP     1
#include "SqlExt.h"

/********************************* sqlConn **********************************
 *
 * Return TRUE if OK, FALSE otherwise.
 *
 ****************************************************************************/

bool sqlConn(LPCSTR pProvider, hlAdo *phDb)
{
   bool bRet=true;

   try
   {
      // open the database connection
      if (!m_bConnected)
      {
         LogMsg("Connecting to %s", pProvider);
         bRet = phDb->Connect(pProvider);
      }
   } catch(_com_error &e)
   {
      LogMsg("***** SQL connect error: %s", ComError(e));
      bRet = false;
   }

   return bRet;
}

/********************************* sqlConn **********************************
 *
 * Return TRUE if OK, FALSE otherwise.
 *
 ****************************************************************************/

bool sqlConnEx(LPCSTR pProvider, hlAdo *phDb)
{
   bool bRet;

   try
   {
      // open the database connection
      LogMsg("Connecting to %s", pProvider);
      bRet = phDb->Connect(pProvider);
   } catch(_com_error &e)
   {
      LogMsg("***** SQL connect error: %s", ComError(e));
      bRet = false;
   }

   return bRet;
}

/********************************* sqlConnect *******************************
 *
 * This function allows user to connect to non-default sql server.
 *
 ****************************************************************************/

bool sqlConnect(LPCSTR pProviderTmpl, LPCSTR strDb, hlAdo *phDb)
{
   bool bRet = true;
   char acTmp[256];

   try
   {
      if (strDb && *strDb > ' ')
         sprintf(acTmp, pProviderTmpl, strDb);
      else
         sprintf(acTmp, pProviderTmpl, "Production");

      LogMsg("Connecting to %s", acTmp);
      if (phDb)
         bRet = phDb->Connect(acTmp);
      else
      {
         bRet = hl.Connect(acTmp);
         m_bConnected = bRet;
      }
   } catch(_com_error &e)
   {
      LogMsg("***** SQL connect error: %s", ComError(e));
      bRet = false;
   }

   return bRet;
}

/*****************************************************************************
 *
 *
 ****************************************************************************/

int execCmd(LPCTSTR strCmd)
{
   int iRet = 0;
   CString strSql = strCmd;

   try
   {
      hl.ExecuteCommand(strSql);
   } catch(_com_error &e)
   {
      LogMsg("***** Error exec cmd: %s", strSql);
      LogMsg("%s\n", ComError(e));
      iRet = -3;
   }
   return iRet;
}

/*****************************************************************************
 *
 *
 ****************************************************************************/

int execCmdEx(LPCTSTR strCmd, hlAdo *phDb)
{
   int iRet = 0;
   CString strSql = strCmd;

   try
   {
      phDb->ExecuteCommand(strSql);
   } catch(_com_error &e)
   {
      LogMsg("***** Error exec cmd: %s", strSql);
      LogMsg("%s\n", ComError(e));
      iRet = -3;
   }
   return iRet;
}

/******************************************************************************
 *
 * This version uses zipcode table from EDX.
 * pZipcode is 6 bytes long.  
 *
 * Return 1 if found, 0 if not.
 *
 ******************************************************************************/

int locateCity(LPCSTR pZipcode, LPSTR pCity, LPSTR pState, LPSTR pCountry, LPCSTR pCounty)
{
   int      iRet;
   char     acTmp[256];
   CString  sTmp;
   hlAdoRs  myRs;

   if (!m_bConnected)
      return 0;

   // Initialize output
   *pCity = *pState = *pCountry = 0;
   if (pCounty)
      sprintf(acTmp, "SELECT * FROM %s_zip WHERE Zip='%s'", pCounty, pZipcode);
   else
      sprintf(acTmp, "SELECT * FROM Zipcode WHERE Zip='%s'", pZipcode);
   try
   {
      myRs.Open(hl, acTmp);

      if (myRs.next())
      {  // found
         sTmp = myRs.GetItem("City");
         if (sTmp > "9")
            strcpy(pCity, sTmp);
         sTmp = myRs.GetItem("State");
         if (sTmp > "9")
            strcpy(pState, sTmp);
         sTmp = myRs.GetItem("Country");
         if (sTmp > "0")
            strcpy(pCountry, sTmp);
         iRet = 1;
      } else
         iRet = 0;

      myRs.Close();
   } catch(_com_error &e)
   {
      LogMsg("***** Error accessing sql [%s] : %s", acTmp, ComError(e));
      iRet = -3;
   }

   return iRet;
}

/******************************************************************************
 *
 * This version uses standard zipcodes table.
 * pZipcode is 5 bytes long.  Result is the primary city of that zipcode.
 *
 * Return 1 if found, 0 if not.
 *
 ******************************************************************************/

int locateCity(LPCSTR pZipcode, LPSTR pCity, LPCSTR pCounty)
{
   int      iRet;
   char     acTmp[256];
   CString  sTmp;
   hlAdoRs  myRs;

   if (!m_bConnected)
      return 0;

   // Initialize output
   *pCity = 0;
   if (pCounty)
      sprintf(acTmp, "SELECT * FROM %s_zipcode WHERE Zipcode='%s'", pCounty, pZipcode);
   else
      sprintf(acTmp, "SELECT * FROM Zipcodes WHERE Zipcode='%s'", pZipcode);
   try
   {
      myRs.Open(hl, acTmp);

      if (myRs.next())
      {  // found
         sTmp = myRs.GetItem("City");
         if (sTmp > "9")
            strcpy(pCity, sTmp);
         iRet = 1;
      } else
         iRet = 0;

      myRs.Close();
   } catch(_com_error &e)
   {
      LogMsg("***** Error accessing sql [%s] : %s", acTmp, ComError(e));
      iRet = -3;
   }

   return iRet;
}

/******************************************************************************
 *
 * This version uses standard zipcodes table.
 * pZipcode is 5 bytes long.  This function returns City, County & State if found
 *
 * Return 1 if found, 0 if not.
 *
 ******************************************************************************/

int locateCityX(LPCSTR pZipcode, LPSTR pCity, LPSTR pState, LPSTR pCounty)
{
   int      iRet;
   char     acTmp[256];
   CString  sTmp;
   hlAdoRs  myRs;

   if (!m_bConnected)
      return 0;

   // Initialize output
   *pCity = *pState = 0;
   sprintf(acTmp, "SELECT * FROM Zipcodes WHERE ZipCode='%s'", pZipcode);
   try
   {
      myRs.Open(hl, acTmp);

      if (myRs.next())
      {  // found
         sTmp = myRs.GetItem("City");
         if (sTmp > "9")
            strcpy(pCity, sTmp);
         sTmp = myRs.GetItem("State");
         if (sTmp > "9")
            strcpy(pState, sTmp);
         sTmp = myRs.GetItem("County");
         if (sTmp > "0")
            strcpy(pCounty, sTmp);
         iRet = 1;
      } else
         iRet = 0;

      myRs.Close();
   } catch(_com_error &e)
   {
      LogMsg("***** Error accessing sql [%s] : %s", acTmp, ComError(e));
      iRet = -3;
   }

   return iRet;
}

/******************************************************************************
 *
 * Import zipcode table into SQL server
 * Return 0 if successful, -1 if not.
 *
 ******************************************************************************/

int importZipcode(LPCSTR pZipfile)
{
   FILE     *fd;
   ZIPTBL   *pZip;
   char     sBuf[256], sCity[SIZ_ZIPCITY+1], sCountry[SIZ_ZIPCOUNTRY+1];
   CString  strCmd;
   int      iRet;

   LogMsg("Importing zipcode table from %s", pZipfile);

   if (!m_bConnected)
   {
      LogMsg("Error: SQL server not connected.  Please notify Sony");
      return -1;
   }

   pZip = (ZIPTBL *)&sBuf[0];
   if (fd = fopen(pZipfile, "r"))
   {
      // Empty table first before insert
      strCmd = "TRUNCATE TABLE Zipcode";
      if (iRet = execCmd(strCmd))
      {
         LogMsg("***** Error deleting table Zipcode for import");
         return -1;
      }

      // Init variables
      sCity[SIZ_ZIPCITY] = 0;
      sCountry[SIZ_ZIPCOUNTRY] = 0;

      // Loop to import
      while (!feof(fd))
      {
         if (!fgets(sBuf, 256, fd))
            break;
         memcpy(sCity, pZip->City, SIZ_ZIPCITY);
         myTrim(sCity, SIZ_ZIPCITY);
         remChar(sCity, 39);                             // Remove single quote
         memcpy(sCountry, pZip->Country, SIZ_ZIPCOUNTRY);
         myTrim(sCountry, SIZ_ZIPCOUNTRY);
         remChar(sCountry, 39);

         if (sBuf[5] == ' ')
            sBuf[5] = 0;
         strCmd.Format("INSERT INTO Zipcode (Zip, City, State, Country) "
            "Values('%.6s','%s', '%.2s', '%s')", sBuf, sCity, pZip->St, sCountry);

         // If fail, break out of loop
         if (iRet = execCmd(strCmd))
            break;
      }

      fclose(fd);
      iRet = 0;
      LogMsg("Importing done!");
   } else
   {
      LogMsg("***** Error opening file %s for import", pZipfile);
      iRet = -1;
   }
   return iRet;
}

/*********************************** execSqlCmd *****************************
 *
 *
 ****************************************************************************/

int execSqlCmd(LPCTSTR strCmd, hlAdo *phDb)
{
   int iRet = 0;
   CString strSql = strCmd;

   try
   {
      if (phDb)
         phDb->ExecuteCommand(strSql);
      else
         hl.ExecuteCommand(strSql);
   } catch(_com_error &e)
   {
      LogMsg("***** Error executing command [%s] : %s", strSql, ComError(e));
      iRet = -3;
   }
   return iRet;
}

/********************************** runSP ***********************************
 *
 * This function will execute a SQL command then exit
 *
 * Return 0 if success, otherwise error code
 *
 ****************************************************************************/

int runSP(char *strCmd, char *pDBName, char *pServerName, char *pProviderTmpl)
{                 
   hlAdo hDB;
   char acServer[256];
   int  iRet;

   sprintf(acServer, pProviderTmpl, pDBName, pServerName);

   LogMsg("Execute command on: %s", acServer);
   LogMsg(strCmd);
   try
   {
      // open the database connection
      hDB.Connect(acServer);
   } catch(_com_error &e)
   {
      LogMsg("%s", ComError(e));
      return -2;
   }

   // Update county profile table
   iRet = execSqlCmd(strCmd, &hDB);

   // Release command
   hDB.~hlAdo();

   return iRet;
}
