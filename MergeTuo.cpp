/**************************************************************************
 *
 * Options:
 *    -CTUO -L -Xs|-Ms -Mg -Xa -Xl  (load lien)
 *    -CTUO -U [-Xsi|-Ms] [-Xa] [-G|-Mg] (load update)
 *
 * Notes: Use OwnerShip.csv file to update owner name
 *
 * Revision
 * 05/16/2017 16.15.5   First version - Copy from MergeGle.cpp
 * 05/19/2017 16.15.6   Add -Xv option to load value (copy code from YOL)
 * 05/23/2017 16.16.1   Change -Xv process using code from AMA to eliminate extra record
 *            16.16.2   Fix bug in Tuo_MergeSitus(), add Tuo_ConvertApn()
 * 06/14/2017 16.18.2   Modify Tuo_MergeRoll() to include book 930 to roll file per county assr request.
 *                      Modify Tuo_ConvStdChar() to ignore invalid QualityClass.
 * 06/17/2017 16.19.1   Modify Tuo_MergeRoll() to include all books 906 and above per county user request.
 * 06/25/2017 17.0.1    Add Tuo_CombineOwner() and modify Tuo_MergeOwnerRec() to update owners
 *                      from ownership file.  Owner in roll file is incomplete.
 * 06/30/2017 17.0.4    Modify Tuo_Load_LDR() to use OwnerName from Ownership file instead of LDR
 *                      because LDR file has only one name.  Check for special case in Tuo_MergeMAdr().
 * 08/01/2017 17.2.2    Modify Tuo_Load_LDR() to update PREV_APN. Modify Tuo_ExtrSaleMatched() to reformat
 *                      DocNum to YYYYR9999999 to match with assessor data.
 * 04/21/2018 17.11.2   Modify Tuo_ConvertApnSale() to reformat DocNum as well as APN.
 *                      Modify Tuo_ExtrSaleMatched() to extract only records that match DocTitle list.
 *                      Modify Tuo_MakeDocLink() since DocNum has been changed
 * 06/27/2018 17.16.0   Fix DocLink format in Tuo_MakeDocLink(). Filter out bad DocNum in Tuo_MergeLien()
 *                      Remove "CO RD" exception in Tuo_MergeMAdr().
 * 11/01/2018 18.7.2    Fix Tuo_MakeDocLink()
 * 12/10/2018 18.8.2    Fix Tuo_ConvStdChar() to skip record without APN.
 * 01/03/2019 18.9.2    Modify Tuo_ExtrSaleMatched() to copy APN from GRGR without reformating.
 *                      Modify Tuo_LoadGrGrCsv() to support different APN format in GRGR "HFW" files
 *                      and standardize them to MB format.  Format DocNum to 7 digit.
 * 03/19/2019 18.10.8   Verify CHAR file size before processing.
 * 06/30/2019 19.0.2    Modify Tuo_MergeLien() to add AgPreserve flag that is new in 2019 LDR file.
 *                      Modify Tuo_ExtrSaleMatched() to ignore APN_Matched flag because parcels may 
 *                      appear in GrGr file before roll file.
 * 08/27/2019 19.2.0    Add Tuo_Phys2StdChar() to replace Tuo_ConvStdChar()
 * 09/07/2019 19.2.3    Remove unused code in Tuo_MergeRoll().  Modify Tuo_ConvStdChar() to work
 *                      with new layout.
 * 06/08/2020 19.10.1   Add -Mz option to merge Zoning.
 * 07/22/2020 20.2.2    Fix bug in Tuo_MergeSitus() that fails when reach EOF.  Also fix bug in 
 *                      Tuo_Load_Roll() that fails to update owner on new parcel.
 * 11/01/2020 20.4.2    Modify Tuo_MergeRoll() to populate default PQZoning.
 * 02/10/2021 20.7.0    Modify loadTuo() to use sale date format specify in INI file.
 * 05/05/2021 20.8.6    Modify Tuo_ConvStdChar() to fix BldgClass.
 * 06/28/2021 21.0.3    Modify loadTuo() to process -Xl before -Xv since MB_ExtrValues() expect output of -Xl.
 * 08/26/2021 21.2.0    Default bUseConfSalePrice=false so we don't use confidential sale price.
 * 03/11/2022 21.6.0    Return error on "no new GRGR" only when user run with -E option.
 * 04/25/2022 21.9.0    Use acGrGrBakTmpl as backup folder for GrGr files. 
 * 06/26/2022 22.0.2    Allow -Xa to process two different char files (regular & LDR version).
 * 06/29/2023 23.0.3    Modify Tuo_Phys2StdChar() to fix QualityClass, Tuo_MergeStdChar() to add NBH code.
 * 02/13/2024 23.6.2    Modify Tuo_ConvStdChar() to support updated CHAR file from TUO.
 * 08/04/2024 24.0.5    Modify Tuo_MergeLien() to add ExeType.
 * 10/15/2024 24.1.8    Add Tuo_FormatApn() & Tuo_LoadGrGr1() to process new GRGR file format.
 *                      Modify Tuo_MatchRoll() to use APN instead of PREV_APN.
 *                      Modify Tuo_LoadGrGrNoZip() to work with Tuo_LoadGrGr1() and output sorted TUO_GrGr.sls
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "doGrGr.h"
#include "doZip.h"
#include "formatApn.h"

#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "CharRec.h"
#include "Situs.h"
#include "PQ.h"
#include "Tax.h"
#include "MergeTuo.h"
//#include "CSpreadSheet.h"
#include "MB_Value.h"
#include "LoadValue.h"
//#include "MergeZoning.h"

/******************************* Tuo_LoadValue ******************************
 *
 * Load value file and create value import file
 * Return #records extracted.
 *
 ****************************************************************************/

//int Tuo_LoadValue(char *pInfile, char *pSheet, char *pExt, char *pOutfile)
//{
//   char     acTmp[256], sOutbuf[1024], acOutfile[_MAX_PATH];
//   int      iRet, iRowCnt, lCnt, iRows, iCols, iSkipRows, iRecCnt;
//   long     lLand, lImpr, lPP, lFixtr, lExe, lTmp, lTotal;
//   FILE     *fdOut;
//
//   CString      strTmp, strApn;
//   CStringArray asRows;
//   VALUE_REC   *pVal = (VALUE_REC *)&sOutbuf[0];
//
//   LogMsgD("\nProcess value file %s on sheet %s", pInfile, pSheet);
//
//   // Open input file - no backup
//   CSpreadSheet sprSht(pInfile, pSheet, false, pExt);
//
//   iRows = sprSht.GetTotalRows();
//   iCols = sprSht.GetTotalColumns();
//
//   if (iCols < TUO_VALUE_COLS)
//   {
//      LogMsg("WARNING: Number of columns is smaller than expected: %d", iCols);
//      return 0;
//   }
//
//   // Open output file
//   sprintf(acOutfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "vmp");
//   if (!(fdOut = fopen(acOutfile, "w")))
//   {
//      LogMsg("ERROR - Unable to create output file: %s", acOutfile);
//      return -1;
//   }
//
//   // Initialize pointers
//   lCnt = 0;
//   iSkipRows = iRecCnt = 0;
//   sprSht.GetFieldNames(asRows);
//
//   // Skip first header row
//   for (iRowCnt = 2; iRowCnt <= iRows; iRowCnt++)
//   {
//      sprSht.ReadRow(asRows, iRowCnt);  
//      if (asRows.GetSize() < iCols)
//      {
//         iSkipRows++;
//         LogMsg("Skip row# %d (wrong number of columns)", iRowCnt);
//         continue;
//      } else
//      {
//         lLand=lImpr=lPP=lFixtr = 0;
//         memset(&sOutbuf, ' ', sizeof(VALUE_REC));
//         strApn = asRows.GetAt(TUO_VALUE_APN);  
//         if (!strApn.IsEmpty())
//         {
//            strcpy(acTmp, strApn);
//		      iRet = remChar(acTmp, '-');
//            memcpy(pVal->Apn, acTmp, iRet);
//         } else
//         {
//            iSkipRows++;
//            LogMsg("Skip row# %d (no APN)", iRowCnt);
//            continue;
//         }
//#ifdef _DEBUG
//         //if (!memcmp(pVal->Apn, "0380212300", 10))
//         //   iRet = 0;
//#endif
//
//         // Tax year
//         strTmp = asRows.GetAt(TUO_VALUE_YEAR);  
//         memcpy(pVal->TaxYear, strTmp, strTmp.GetLength());
//         pVal->ValueSetType[0] = '1';
//         pVal->Reason[0] = '1';
//
//         // License No
//         //strTmp = asRows.GetAt(TUO_VALUE_LICNO);  
//         //if (strTmp > "0")
//         //   memcpy(pVal->Misc, strTmp, strTmp.GetLength());
//
//         // Land
//         strTmp = asRows.GetAt(TUO_VALUE_LAND);  
//         iRet = dollar2Num(strTmp.GetBuffer(0), acTmp);
//         if (iRet > 0)
//         {
//            lLand = atol(acTmp);
//            iRet = sprintf(acTmp, "%d", lLand);
//            memcpy(pVal->Land, acTmp, iRet);
//         }
//
//         // Impr
//         strTmp = asRows.GetAt(TUO_VALUE_IMPR);  
//         iRet = dollar2Num(strTmp.GetBuffer(0), acTmp);
//         if (iRet > 0)
//         {
//            lImpr = atol(acTmp);
//            iRet = sprintf(acTmp, "%d", lImpr);
//            memcpy(pVal->Impr, acTmp, iRet);
//         }
//
//         // Personal Prop
//         strTmp = asRows.GetAt(TUO_VALUE_PPVAL);  
//         iRet = dollar2Num(strTmp.GetBuffer(0), acTmp);
//         if (iRet > 0)
//         {
//            lPP = atol(acTmp);
//            iRet = sprintf(acTmp, "%d", lPP);
//            memcpy(pVal->PersProp, acTmp, iRet);
//         }
//
//         // Fixture
//         strTmp = asRows.GetAt(TUO_VALUE_FIXTVAL);  
//         iRet = dollar2Num(strTmp.GetBuffer(0), acTmp);
//         if (iRet > 0)
//         {
//            lFixtr = atol(acTmp);
//            iRet = sprintf(acTmp, "%d", lFixtr);
//            memcpy(pVal->Fixtr, acTmp, iRet);
//            //strTmp = asRows.GetAt(TUO_VALUE_FIXTTYPE);  
//            //strTmp.TrimLeft();
//            //if (strTmp.GetLength() > 0)
//            //   pVal->Fixtr_Type[0] = strTmp.GetAt(0);
//         }
//
//         // Exemption
//         strTmp = asRows.GetAt(TUO_VALUE_EXEAMT);  
//         iRet = dollar2Num(strTmp.GetBuffer(0), acTmp);
//         if (iRet > 0)
//         {
//            lExe = atol(acTmp);
//            iRet = sprintf(acTmp, "%d", lExe);
//            memcpy(pVal->Exe, acTmp, iRet);
//            //strTmp = asRows.GetAt(TUO_VALUE_EXETYPE);  
//            //strTmp.TrimLeft();
//            //if (strTmp.GetLength() > 0)
//            //   pVal->Exe_Type[0] = strTmp.GetAt(0);
//         }
//
//         // Others
//         lTmp = lFixtr + lPP;
//         if (lTmp > 0)
//         {
//            iRet = sprintf(acTmp, "%d", lTmp);
//            memcpy(pVal->Other, acTmp, iRet);
//         }
//
//         // Gross
//         lTotal = lTmp+lLand+lImpr;
//         if (lTotal > 0)
//         {
//            iRet = sprintf(acTmp, "%d", lTotal);
//            memcpy(pVal->TotalVal, acTmp, iRet);
//         }
//
//         // Net
//         strTmp = asRows.GetAt(TUO_VALUE_NET);  
//         iRet = dollar2Num(strTmp.GetBuffer(0), acTmp);
//         if (iRet > 0)
//         {
//            lTmp = atol(acTmp);
//            iRet = sprintf(acTmp, "%d", lTmp);
//            memcpy(pVal->NetVal, acTmp, iRet);
//         }
//
//         pVal->CRLF[0] = '\n';
//         pVal->CRLF[1] = 0;
//         iRet = fputs((char *)&sOutbuf, fdOut);
//         iRecCnt++;
//
//         // Base Year value - create new record
//         strTmp = asRows.GetAt(TUO_VALUE_BYVVAL);  
//         if (strTmp.Left(15) == "BASE YEAR VALUE")
//         {
//            long lBYV=0;
//            CString sDollar = strTmp.Mid(16);
//            iRet = dollar2Num(sDollar.GetBuffer(0), acTmp);
//            if (iRet > 0)
//            {
//               lBYV = atol(acTmp);
//               // Create Prop8 record
//               if (lBYV > lTotal)
//               {
//                  pVal->Reason[0] = ' ';
//                  pVal->ValueSetType[0] = '8';
//                  memset(pVal->Exe, ' ', sizeof(pVal->Exe));
//                  iRet = fputs((char *)&sOutbuf, fdOut);
//                  iRecCnt++;
//               }
//            }
//
//            if (lBYV > 0)
//            {
//               memset(&pVal->ValueSetType, ' ', sizeof(VALUE_REC) - PV_SIZ_APN);
//               lTmp = atol(acTmp);
//               iRet = sprintf(acTmp, "%d", lTmp);
//               memcpy(pVal->TotalVal, acTmp, iRet);
//               pVal->ValueSetType[0] = '2';
//               pVal->CRLF[0] = '\n';
//               pVal->CRLF[1] = 0;
//               iRet = fputs((char *)&sOutbuf, fdOut);
//               iRecCnt++;
//            }
//         } else
//         {
//            iRet = fputs((char *)&sOutbuf, fdOut);
//            iRecCnt++;
//         }
//      }
//
//      lCnt++;
//      if (!(lCnt % 1000))
//         printf("\r%d", lCnt);
//   }
//
//   fclose(fdOut);
//
//   // Sort output file
//   if (iRecCnt > 0)
//   {
//      // Sort on APN and ValueSetType
//      iRet = sortFile(acOutfile, pOutfile, "S(1,12,C,A, 21,1,C,A) DUPO");
//      if (iRet > 0)
//      {
//         LogMsg("Total record output: %d", iRet);
//         // Remove temp file
//         if (iRet == iRecCnt)
//         {
//            LogMsg("Remove temp file: %s", acOutfile);
//            DeleteFile(acOutfile);
//         }
//      } else
//         iRet = -3;
//   }
//
//   LogMsgD("\nTotal processed records  : %u", lCnt);
//   LogMsgD("        skipped records  : %u", iSkipRows);
//   LogMsgD("         output records  : %u\n", iRecCnt);
//
//   return lCnt;
//}

/******************************** Tuo_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 * BOLLINGER PETER P INV CO ETAL C/O INTER CAL REAL E (001032016000)
 *
 *****************************************************************************/

void Tuo_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf)
{
   int   iTmp, iRet, iVet;
   char  acOwner[64], acName1[64], acName2[64], acTmp[128], *pTmp, *pTmp1;
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);

#ifdef _DEBUG
   // MC DONALD JOHN E &   % MC DONALD ELIZABETH ETAL
   //if (!memcmp(pOutbuf, "001132004000", 9))
   //   iTmp = 0;
#endif

   // Check embeded CareOf
   if (pTmp = strstr(pNames, "C/O"))
   {
      updateCareOf(pOutbuf, pTmp, strlen(pTmp));
      *pTmp = 0;
   }

   // Remove multiple spaces
   strcpy(acName1, pNames);
   iTmp = blankRem(acName1);
   if (pCareOf && *pCareOf == '%')
   {
      if (*(pCareOf+1) == ' ')
         strcpy(acName2, pCareOf+2);
      else
         strcpy(acName2, pCareOf+1);
      *pCareOf = 0;
   } else
      acName2[0] = 0;

   // Update vesting
   iVet = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST);

   // Check for overflow character '&'
   if (acName1[iTmp-1] == '&')
   {
      acName1[iTmp-2] = 0;

      // Check Name2
      if (acName2[0] > ' ')
      {
         // Check vesting
         if (!iVet)
            iVet = updateVesting(myCounty.acCntyCode, acName2, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

            // Check embeded CareOf
            if (pTmp = strstr(acName2, "C/O"))
            {
               updateCareOf(pOutbuf, pTmp, strlen(pTmp));
               *pTmp = 0;
            }

         iTmp = MergeName2(acName1, acName2, acOwner, ' ');
         // Remove Name2 if mergable
         if (iTmp == 1)
            acName2[0] = 0;
         else
         {
            strcpy(acOwner, acName1);
            pTmp = strrchr(acName2, ' ');
            if (pTmp && (!memcmp(pTmp, " JT", 3) || !memcmp(pTmp, " J/T", 4) ||
                         !strcmp(pTmp, " CP")    ||!strcmp(pTmp, " C/P") ||
                         !strcmp(pTmp, " SP")    || !strcmp(pTmp, " TC")))
               *pTmp = 0;
         }
      } else
         strcpy(acOwner, acName1);
   } else
      strcpy(acOwner, acName1);

   strcpy(acTmp, acOwner);
   // Remove ETAL or DVA
   if (pTmp = strrchr(acTmp, ' '))
   {
      if (!strcmp(pTmp, " ETAL") || !strcmp(pTmp, " DVA") )
         *pTmp = 0;
      else if (pTmp = strstr(acTmp, " ET AL"))
         *pTmp = 0;
   }

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (acTmp[iTmp] > '0' && acTmp[iTmp] < 'A')
      {
         iTmp = 0;
         break;
      }
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp || strstr(acOwner, "L/P"))
   {
      vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
      return;
   }

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
      if ( (pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " TR")))
         *pTmp = 0;

      if (strstr((char *)&acTmp[iTmp], " TRUST") ||
         strstr((char *)&acTmp[iTmp], " LIVING") ||
         strstr((char *)&acTmp[iTmp], " REVOCABLE") )
         acTmp[--iTmp] = 0;
   }

   // Filter out words, things in parenthesis
   if ( (pTmp = strchr(acTmp, '('))      || (pTmp=strstr(acTmp, " CO TR"))   ||
        (pTmp=strstr(acTmp, " CO-TR"))   || (pTmp=strstr(acTmp, " COTR"))    ||
        (pTmp=strstr(acTmp, " TRUSTEE")) || (pTmp=strstr(acTmp, " TR ETAL")) ||
        (pTmp=strstr(acTmp, " TTEE"))    || (pTmp=strstr(acTmp, " TRES"))    ||
        (pTmp=strstr(acTmp, " ETAL"))    || (pTmp=strstr(acTmp, " ET AL"))  )
      *pTmp = 0;

   // Filter some more
   pTmp = (char *)&acTmp[strlen(acTmp)-4];
   //if (!memcmp(pTmp, " DVA", 4) || !memcmp(pTmp, " LIV", 4) )
   if (!memcmp(pTmp, " DVA", 4) )
      *pTmp = 0;

   // If there is number goes before REV, keep it.
   if (!memcmp(pTmp, " REV", 4))
   {
      pTmp1 = pTmp;
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
   }

   // Trim trailing number
   iTmp = strlen(acTmp)-1;
   while (iTmp > 0 && isdigit(acTmp[iTmp]))
      acTmp[iTmp--] = 0;

   if ((pTmp=strstr(acTmp, " SUCCS-TR")) || (pTmp=strstr(acTmp, " SUCCESSOR")) ||
      (pTmp=strstr(acTmp, " SURVIVOR")) )
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " FAMILY TRUST &")) || (pTmp=strstr(acTmp, " LIVING TRUST &")) )
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      // RUMAN RICKY L & RUMAN FAMILY TRUST &
      // RUMAN RICKY L & NANCY FAMILY TRUST & MARIE
      // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      //strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      //strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      *(pTmp+6) = 0;
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
         *pTmp1 = 0;
      else
         *pTmp = 0;

      //strcpy(acName2, pTmp+9);
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " FAM TR")) || (pTmp=strstr(acTmp, " FAMILY ")) ||
      (pTmp=strstr(acTmp, " REVOC"))          || (pTmp=strstr(acTmp, " REV TR")) ||
      (pTmp=strstr(acTmp, " REV LIV TR"))     || (pTmp=strstr(acTmp, " REV LIVING")) ||
      (pTmp=strstr(acTmp, " LIV TRUST"))      || (pTmp=strstr(acTmp, " LIVING ")) ||
      (pTmp=strstr(acTmp, " INCOME TR"))      || (pTmp=strstr(acTmp, " 1992 REV"))
      )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ( (pTmp=strstr(acTmp, " ESTATE OF")) || (pTmp=strstr(acTmp, " EST OF")) ||
               (pTmp=strstr(acTmp, " ESTS OF")) )
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   pTmp = strrchr(acName1, ' ');
   if (pTmp && (!memcmp(pTmp, " JT", 3) || !strcmp(pTmp, " CP")    ||!strcmp(pTmp, " C/P") ||
                !strcmp(pTmp, " SP")    || !strcmp(pTmp, " TC")))
      *pTmp = 0;

   if ((pTmp=strstr(acName1, " S/S")))
      *pTmp = 0;
   if ((pTmp=strstr(acName1, " J/T")))
      *pTmp = 0;
   if ((pTmp=strstr(acName1, " T/C")) || (pTmp=strstr(acName1, " C/B")))
      *pTmp = 0;

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&TEHRON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
         *pTmp1++ = 0;
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = -1;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      if (myOwner.acVest[0] > ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // If name is swapable, use it
      if (iRet >= 0)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   }

   if (iRet == -1)
   {
      // Couldn't split names
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME1);
   }

   // Save Name2 if exist
   if (acName2[0] > ' ')
      vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);

   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);
}


/******************************** Tuo_MergeOwner *****************************
 *
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Tuo_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf, char *pDba)
{
   int   iTmp, iRet, iNameCnt;
   char  acOwner[128], acName1[128], acNames[128], *pTmp, *p2;
   OWNER myOwner;
   bool  bHW=false;

   // Clear output buffer if needed
   removeNames(pOutbuf, true, true);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001040007000", 10))
   //   iTmp = 0;
#endif

   // Init names
   iNameCnt=0;
   strcpy(acName1, pNames);
   //remCharEx(acName1, ".,");
   //remChar(acName1, '.');
   myTrim(acName1);

   // Check DBA
   if (*pDba > ' ')
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

   // CareOf
   if (*pCareOf > ' ')
      updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST);

   // Merge H/W
   if (pTmp = strchr(acName1, '&'))
   {
      *pTmp = 0;
      // Point to name2
      p2 = pTmp + 2;
      iNameCnt = MergeName1(acName1, p2, acOwner);
      if (!iNameCnt)
      {
         // Cannot merge
         *pTmp = '&';
         strcpy(acOwner, acName1);
      }
   } else
      strcpy(acOwner, acName1);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = -1;
   if (strchr(acOwner, ' '))
   {
      // Remove extra blank
      blankRem(acOwner);

      // Remove notes
      if (pTmp = strchr(acOwner, '('))
         *pTmp = 0;

      // Make a copy of owner and remove unwanted trailing words before swap
      strcpy(acNames, acOwner);
      strcat(acNames, " ");
      if (pTmp = strstr(acNames, " JT "))
         *pTmp = 0;
      if ((pTmp = strstr(acNames, " TR ")) || (pTmp = strstr(acNames, " TRUST")))
         *pTmp = 0;

      iRet = splitOwner(acNames, &myOwner, 5);

      // If name is swapable, use it
      if (iRet >= 0)
      {
         iTmp = blankRem(myOwner.acSwapName);
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP, iTmp);
      }
   }

   // Keep assessee name as is, only remove comma & dot
   strcpy(acName1, pNames);
   remCharEx(acName1, ",");
   iTmp = blankRem(acName1);
   if (acName1[iTmp-1] == '&')
      acName1[iTmp-1] = 0;
   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);

   if (iRet == -1)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
}

/******************************** Tuo_CleanName ******************************
 *
 * Remove all garbage not belong to owner name
 * Return 0 if no vesting, update vesting if found
 *
 *****************************************************************************/

int Tuo_CleanName(char *pSrcName, char *pDstName, char *pVesting)
{
   char  acTmp[128], *pTmp;
   int   iCnt;

   strcpy(acTmp, pSrcName);
   blankRem((char *)&acTmp[0]);

   // Remove 1/2 or 25% 
   if ((pTmp=strchr(acTmp, '/')) || (pTmp=strchr(acTmp, '%')))
   {
      pTmp--;
      while ((pTmp > acTmp) && (isdigit(*pTmp) || *pTmp == '.'))
         *pTmp-- = 0;
   }
   
   // PALMA, SALVATORE T 27.5
   if (pTmp=strchr(acTmp, '.'))
   {
      if (isdigit(*(pTmp+1)))
      {
         while (pTmp > acTmp && *pTmp > ' ') pTmp--;

         if (pTmp > acTmp)
            *pTmp = 0;
      }
   }

   iCnt = 0;
   pTmp = findVesting(pSrcName, pVesting);
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

/**************************** Tuo_MergeOwnerFull *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Tuo_CombineOwner(char *pOutName1, char *pOutName2, char *pInName1, char *pInName2, char *pSwapName)
{
   int   iTmp, iMrgFlg;
   char  acTmp[64], acName1[64], acName2[64];
   char  *pTmp;

   OWNER myOwner;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "056083016000", 9))
   //   iTmp = 0;
#endif
   // Initialize
   *pOutName1 = 0;
   *pOutName2 = 0;
   strcpy(acName1, pInName1);
   strcpy(acName2, pInName2);

   // Merge name only if exist
   iMrgFlg = MergeName2(acName1, acName2, acTmp, ',');

   if (iMrgFlg == 1)
   {
      // Name merged OK - drop name2
      strcpy(acName1, acTmp);
      acName2[0] = 0;
   } else if (iMrgFlg == 2)
   {
      // Same name - drop name2
      acName2[0] = 0;
   } else if (iMrgFlg == 99 || !iMrgFlg)
   {
      // Cannot merge - Keep them as is
      if (!strcmp(acName1, acName2))
         acName2[0] = 0;
   }

   remChar(acName1, ',');
   pTmp = myTrim(acName1);

   // Now parse owners
   strcpy(acTmp, pTmp);
   iTmp = splitOwner(acTmp, &myOwner, 5);
   if (iTmp < 0)     
      strcpy(pSwapName, acName1);
   else
      strcpy(pSwapName, myOwner.acSwapName);

   strcpy(pOutName1, acName1);
   if (acName2[0] > ' ')
   {
      remChar(acName2, ',');
      strcpy(pOutName2, acName2);
   }
}

void Tuo_MergeOwnerFull(char *pOutbuf, char *pName1, char *pName2, char *pFullName)
{
   int   iTmp, iMrgFlg;
   char  acTmp[64], acName1[64], acName2[64], acVesting1[8], acVesting2[8];
   char  *pTmp;

   OWNER myOwner;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "056083016000", 9))
   //   iTmp = 0;
#endif
   // Initialize
   iMrgFlg = 0;
   acVesting1[0] = 0;
   acVesting2[0] = 0;
   acName2[0] = 0;

   iMrgFlg  = Tuo_CleanName(pName1, acName1, acVesting1);
   iMrgFlg |= Tuo_CleanName(pName2, acName2, acVesting2);

   // Merge name only if there is no vesting
   if (!iMrgFlg)
      iMrgFlg = MergeName2(acName1, acName2, acTmp, ',');

   if (iMrgFlg == 1)
   {
      // Name merged OK - drop name2
      strcpy(acName1, acTmp);
      acName2[0] = 0;
   } else if (iMrgFlg == 2)
   {
      // Same name - drop name2
      acName2[0] = 0;
   } else if (iMrgFlg == 99 || !iMrgFlg)
   {
      // Cannot merge - Keep them as is
      if (!strcmp(acName1, acName2))
         acName2[0] = 0;
      //else
      //   strcpy(acName2, pName2);
      //strcpy(acName1, pName1);
   } else
   {
      // Keep them as is
      strcpy(acName1, pName1);
      strcpy(acName2, pName2);
   }

   myTrim(acName2);
   pTmp = myTrim(acName1);

   // Remove Name2 if it's the same as Name1
   if (!strcmp(acName1, acName2))
      acName2[0] = 0;

   // Now parse owners
   strcpy(acTmp, pTmp);
   iTmp = splitOwner(acTmp, &myOwner, 3);
   if (iTmp < 0 || (acVesting1[0] > ' ' && isVestChk(acVesting1)))
   {
      remChar(acName1, ',');
      strcpy(myOwner.acSwapName, acName1);
   }
   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);

   replChar(acName1, ',', ' ');
   iTmp = blankRem(acName1, SIZ_NAME1);
   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);
   if (acName2[0] > ' ')
   {
      replChar(acName2, ',', ' ');
      iTmp = blankRem(acName2, SIZ_NAME2);
      vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);
      if (bDebug && iTmp < 5)
         LogMsg("***** Bad Name2 at %.10s: %s", pOutbuf, acName2);
   }

   // Check vesting
   if (acVesting1[0] > '0')
   {
      memcpy(pOutbuf+OFF_VEST, acVesting1, strlen(acVesting1));
      // Check EtAl
      if (!memcmp(acVesting1, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   }
   else if (acVesting2[0] > '0')
   {
      memcpy(pOutbuf+OFF_VEST, acVesting2, strlen(acVesting2));
      // Check EtAl
      if (!memcmp(acVesting2, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   }
}

/**************************** Tuo_MergeOwnerRec ******************************
 *
 * Merge owner from seperate owner file.
 *
 *****************************************************************************/

int Tuo_MergeOwnerRec(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     *pTmp, *apItems[20], sName1[64], sName2[64], sNames[128], sTitle1[32], sTitle2[32], 
            sNewName1[64], sNewName2[64], sTmp[1024];
   int      iLoop, iTmp, iItems;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdName);

#ifdef _DEBUG
   // 001051027    - Name2 is CareOf
   // 022240001520 - Name1 is CareOf
   //if (!memcmp(pOutbuf, "001101014000", 9))
   //   iTmp = 0;
#endif

StartLoop:

   do
   {
      if (!pRec)
      {
         fclose(fdName);
         fdName = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         //if (bDebug)
         //   LogMsg0("Skip Owner rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdName);
         lNameSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Remove NULL
   replStrAll(acRec, "NULL", "");

   // Parse owner record
   iItems = ParseStringIQ(acRec, cDelim, 20, apItems);
   if (iItems < TUO_OWNR_OWNERFULL)
   {
      LogMsg("***** Error: bad owner record for APN=%s (#tokens=%d)", acRec, iItems);
      return -1;
   }

   // Ignore CareOf
   if (*apItems[TUO_OWNR_OWNERLAST] == '%')
   {
      pRec = fgets(acRec, 1024, fdName);
      goto StartLoop;
   }

   lNameMatch++;

   // Initialize
   sNames[0] = 0;
   sNewName2[0] = 0;
   sTitle2[0] = 0;
   strcpy(sTitle1, apItems[TUO_OWNR_TITLE]);

   // Get next record and check for 2nd owner
   sTmp[0] = 0;
   if (!feof(fdName))
   {
      pRec = fgets(sTmp, 1024, fdName);
      replStrAll(sTmp, "NULL", "");
   }

   if (pRec && !memcmp(sTmp, acRec, iApnLen))
   {
      // Process name1
      if (*apItems[TUO_OWNR_OWNERFIRST] > ' ')
      {
         if (strchr(apItems[TUO_OWNR_OWNERLAST], ','))
            sprintf(sName1, "%s %s %s", apItems[TUO_OWNR_OWNERLAST], apItems[TUO_OWNR_OWNERFIRST], apItems[TUO_OWNR_OWNERMIDDLE]);
         else
            sprintf(sName1, "%s, %s %s", apItems[TUO_OWNR_OWNERLAST], apItems[TUO_OWNR_OWNERFIRST], apItems[TUO_OWNR_OWNERMIDDLE]);
      } else
         strcpy(sName1, apItems[TUO_OWNR_OWNERLAST]);

      // Process Name2
      iItems = ParseStringIQ(sTmp, cDelim, 20, apItems);
      if (*apItems[TUO_OWNR_OWNERFIRST] > ' ')
         sprintf(sName2, "%s, %s %s", apItems[TUO_OWNR_OWNERLAST], apItems[TUO_OWNR_OWNERFIRST], apItems[TUO_OWNR_OWNERMIDDLE]);
      else
         strcpy(sName2, apItems[TUO_OWNR_OWNERLAST]);
      strcpy(sTitle2, apItems[TUO_OWNR_TITLE]);

      // Drop Name2 if it's the same as Name1
      if (!strcmp(sName1, sName2))
      {
         sName2[0] = 0;
         sTitle2[0] = 0;
      }

      if (sName2[0] == '%')
      {
         sNewName2[0] = 0;
         strcpy(sNewName1, sName1);
         if (strchr(sName1, ',') && !strstr(sName1, " TR "))
            swapName(sName1, sNames, false);
         else
            strcpy(sNames, sName1);
      } else if (!strstr(sName1, " TR "))
         // Merge owner - Only handle record with 2 owners
         Tuo_CombineOwner(sNewName1, sNewName2, sName1, sName2, sNames);
      else
      {
         strcpy(sNewName1, sName1);
         strcpy(sNewName2, sName2);
         strcpy(sNames, sName1);
      }

      // If names can't combined, add title to name1
      // If both names has title, keep them separate
      if (sTitle1[0] > ' ' && sTitle2[0] > ' ')
      {
         sprintf(sNewName1, "%s %s", sName1, sTitle1);
         strcpy(sName1, sNewName1);
         sprintf(sNewName2, "%s %s", sName2, sTitle2);
         strcpy(sName2, sNewName2);
      } else if ((sName2[0] <= ' ' || sNewName2[0] <= ' ') && sTitle1[0] > ' ')
      {
         sprintf(sName1, "%s %s", sNewName1, sTitle1);
         sName2[0] = 0;
      } else
      {
         strcpy(sName1, sNewName1);
         strcpy(sName2, sNewName2);
      }

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdName);
      while (!memcmp(pOutbuf, pRec, iApnLen))
      {
         // More than 1 name records
         pRec = fgets(acRec, 1024, fdName);
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      }
   } else
   {
      sName2[0] = 0;

      // Process single name
      if (*apItems[TUO_OWNR_OWNERFIRST] > ' ')
      {
         remChar(apItems[TUO_OWNR_OWNERLAST], ',');
         if ((strlen(apItems[TUO_OWNR_OWNERLAST])+strlen(apItems[TUO_OWNR_OWNERFIRST])+strlen(apItems[TUO_OWNR_OWNERMIDDLE])) > 50)
         {
            strcpy(sName1, apItems[TUO_OWNR_OWNERLAST]);
            if (*apItems[TUO_OWNR_OWNERFIRST] == '&')
               strcpy(sName2, apItems[TUO_OWNR_OWNERFIRST]+2);
            else
               strcpy(sName2, apItems[TUO_OWNR_OWNERFIRST]);
            strcpy(sNames, apItems[TUO_OWNR_OWNERLAST]);
         } else
         {
            if (sTitle1[0] > '9')
               sprintf(sName1, "%s %s %s %s", apItems[TUO_OWNR_OWNERLAST], apItems[TUO_OWNR_OWNERFIRST], 
                  apItems[TUO_OWNR_OWNERMIDDLE], apItems[TUO_OWNR_TITLE]);
            else
               sprintf(sName1, "%s %s %s", apItems[TUO_OWNR_OWNERLAST], apItems[TUO_OWNR_OWNERFIRST], apItems[TUO_OWNR_OWNERMIDDLE]);
            sprintf(sNames, "%s %s %s", apItems[TUO_OWNR_OWNERFIRST], apItems[TUO_OWNR_OWNERMIDDLE], apItems[TUO_OWNR_OWNERLAST]);
         }
      } else
      { 
         OWNER myOwner;
         pTmp = strchr(apItems[TUO_OWNR_OWNERLAST], ',');
         if (pTmp)
         {
            remChar(apItems[TUO_OWNR_OWNERLAST], ',');
            sprintf(sName1, "%s %s", apItems[TUO_OWNR_OWNERLAST], apItems[TUO_OWNR_TITLE]);
            iTmp = splitOwner(apItems[TUO_OWNR_OWNERLAST], &myOwner, 5);
            if (iTmp >= 0)
               strcpy(sNames, myOwner.acSwapName);
            else
               strcpy(sNames, sName1);
            //pTmp = swapName(apItems[TUO_OWNR_OWNERLAST], sNames, false);
         } else
         {
            sprintf(sName1, "%s %s", apItems[TUO_OWNR_OWNERLAST], apItems[TUO_OWNR_TITLE]);
            strcpy(sNames, sName1);
         }
      }

      iTmp = blankRem(sName1);

      // Save current record
      strcpy(acRec, sTmp);
   }

   // Remove old names
   removeNames(pOutbuf, false, false);

   // Upate owners
   remChar(sName1, ',');
   iTmp = blankRem(sName1);
   vmemcpy(pOutbuf+OFF_NAME1, sName1, SIZ_NAME1, iTmp);
   if (sName2[0] > ' ')
   {
      remChar(sName2, ',');
      vmemcpy(pOutbuf+OFF_NAME2, sName2, SIZ_NAME2);
   }

   // Update swapname
   char *p1, *p2, *p3, *p4;
   if (sTitle1[0] < ' ' && 
      ((p1=strstr(sName1, " TR ")) || 
       (p2=strstr(sName1, " LLC")) ||
       (p3=strstr(sName1, " ESTATE")) ||
       (p4=strstr(sName1, " INC")) ))
   {
      if (p1)
         memcpy(pOutbuf+OFF_VEST, "TR", 2);
      else if (p2)
         memcpy(pOutbuf+OFF_VEST, "CO", 2);
      else if (p3)
         memcpy(pOutbuf+OFF_VEST, "ES", 2);
      else
         memcpy(pOutbuf+OFF_VEST, "CO", 2);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, sName1, SIZ_NAME1);
   } else if (!memcmp(sNames, "U.S.A.", 6))
   {
      memcpy(pOutbuf+OFF_VEST, "GV", 2);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames, SIZ_NAME1);
   } else
   {
      remChar(sNames, ',');
      iTmp = blankRem(sNames);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames, SIZ_NAME1, iTmp);
   }

   // Update Vesting
   if (sTitle1[0] > '9')
   {
      sprintf(sTmp, " %s", sTitle1);
      pTmp = findVesting(sTmp, sTitle1);
      if (pTmp)
         vmemcpy(pOutbuf+OFF_VEST, sTitle1, SIZ_VEST);
      else
         iTmp = 0;
   }

   return 0;
}

int Tuo_MergeOwnerRec_20170623(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     *apItems[20];
   int      iLoop, iItems;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdName);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "032520035000", 9))
   //   iLoop = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdName);
         fdName = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         //if (bDebug)
         //   LogMsg0("Skip Owner rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdName);
         lNameSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Remove NULL
   replStrAll(acRec, "NULL", "");

   // Parse owner record
   iItems = ParseStringIQ(acRec, cDelim, 20, apItems);
   if (iItems < TUO_OWNR_OWNERFULL)
   {
      LogMsg("***** Error: bad owner record for APN=%s (#tokens=%d)", acRec, iItems);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "056083016000", 10))
   //   iLoop = 0;
#endif

   // Merge owner - Only handle record with 2 owners
   if (*apItems[TUO_OWNR_OWNERFIRST] == '&')
   {
      char  sName1[128], sName2[128], sOwner[128];

      removeNames(pOutbuf, false, false);

      strcpy(sName1, apItems[TUO_OWNR_OWNERLAST]);
      strcpy(sName2, apItems[TUO_OWNR_OWNERFIRST]+2);
      strcpy(sOwner, apItems[TUO_OWNR_OWNERFULL]);
      Tuo_MergeOwnerFull(pOutbuf, sName1, sName2, sOwner);
   }
   lNameMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdName);
   if (!memcmp(pOutbuf, pRec, iApnLen))
   {
      // More than 1 name records
      pRec = fgets(acRec, 1024, fdName);
      *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   }

   return 0;
}

/******************************** Tuo_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Tuo_MergeMAdr(char *pOutbuf)
{
   char    acTmp[256], acAddr1[64], *pTmp;
   int     iTmp;
   ADR_REC sMailAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "056083016000", 10))
   //   iTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, false, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }

   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
      sMailAdr.strName[SIZ_M_STREET] = 0;
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      // Remove comma
      iTmp = remChar(apTokens[MB_ROLL_M_CITY], ',');
      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], SIZ_M_CITY, iTmp);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         // Take out '-' in zipcode if present
         strcpy(acTmp, apTokens[MB_ROLL_M_ZIP]);
         iTmp = strlen(acTmp);
         if (acTmp[5] == '-' && iTmp == 10)
         {
            strcpy(&acTmp[5], &acTmp[6]);
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         } else if (iTmp > 9)
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         else
            vmemcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
      } else
         iTmp = 0;

      if (iTmp == 9)
         sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP], apTokens[MB_ROLL_M_ZIP]+5);
      else
         sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

void Tuo_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2, *pDba;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

   // Initialize
   removeMailing(pOutbuf, true, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   pDba = p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }

      if (!_memicmp(pLine1, "DBA ", 4) )
         pDba = pLine1;
      else if (!_memicmp(pLine2, "DBA ", 4))
         pDba = pLine2;
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }

      if (!_memicmp(pLine1, "DBA ", 4) )
         pDba = pLine1;
   } else if (*pLine2 >= 'A')
   {
      if (*pLine1 == '%' && *(pLine1+2) < 'A')
         p1 = pLine1+2;
      else
         p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
   }

   // Update DBA
   if (pDba)
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001115009000", 9)  )
   //   iTmp = 0;
#endif

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Special case
      strcpy(acTmp, acAddr1);
      iTmp = replStr(acAddr1, "\"J' ", "\"J\" ");
      if (iTmp > 0)
         LogMsg("--> Rename M_STREET from <%s> to <%s> [%.12s]", acTmp, acAddr1, pOutbuf);

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));

         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   strcpy(acAddr2, p2);
   replChar(acAddr2, ',', ' ');
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);

      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}


/******************************* Tuo_MergeSitus ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Tuo_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acAddr2[64], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do {
         pRec = fgets(acRec, 512, fdSitus);
      } while (*pRec < ' ' || *pRec > '9');
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      pTmp = pRec;
      if (*pTmp == '"')
         pTmp++;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         //if (bDebug)
         //   LogMsg0("Skip Situs rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Remove NULL
   replStrAll(acRec, "NULL", "");

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   else
      iRet = ParseStringIQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   if (*apTokens[MB_SITUS_STRNAME] == ' ' && *apTokens[MB_SITUS_STRNUM] == ' ')
      return 0;

   // Merge data
   acAddr1[0] = 0;
   removeSitus(pOutbuf);
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);

   if (lTmp > 0)
   {
      // Remove spaces
      myBTrim(apTokens[MB_SITUS_STRNUM]);

      // Save original StrNum
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         vmemcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], SIZ_S_DIR);
      }
   }

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);
      vmemcpy(pOutbuf+OFF_S_STREET, apTokens[MB_SITUS_STRNAME], SIZ_S_STREET, strlen(apTokens[MB_SITUS_STRNAME]));

      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
      {
         Sfx2Code(acTmp, acCode);
         vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      } else
      {
         LogMsg0("*** Invalid suffix: %s [%s]", apTokens[MB_SITUS_STRTYPE], apTokens[MB_SITUS_ASMT]);
         iBadSuffix++;
      }
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apTokens[MB_SITUS_STRNAME]);
      vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      vmemcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, SIZ_S_SUFF);

      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004090016520", 12))
   //   iTmp = 0;
#endif
   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNO);
   }

   iTmp = blankRem(acAddr1);
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D, iTmp);
   memset(acAddr2, ' ', SIZ_S_CTY_ST_D);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr2, pOutbuf);
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr2[0] > ' ')
      {
         // Zip
         if (*apTokens[MB_SITUS_ZIP] == '9')
         {
            lTmp = atol(apTokens[MB_SITUS_ZIP]);
            if (lTmp > 90000 && lTmp < 99999)
               memcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], 5);

            iTmp = sprintf(acTmp, "%s, CA %.5s", acAddr2, apTokens[MB_SITUS_ZIP]);
         } else if (!memcmp(acAddr2, pOutbuf+OFF_M_CITY, strlen(acAddr2)) && *(pOutbuf+OFF_M_ZIP) == '9')
         {
            memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, 5);
            iTmp = sprintf(acTmp, "%s, CA %.5s", acAddr2, pOutbuf+OFF_M_ZIP);
         } else
         {
            iTmp = sprintf(acTmp, "%s, CA", acAddr2);
         }
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
      }
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);
   if (!pRec)
   {
      fclose(fdSitus);
      fdSitus = NULL;
   }
   return 0;
}

/***************************** Tuo_MergeStdChar ******************************
 *
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the one that has value.
 *
 *****************************************************************************/

int Tuo_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft;
   int      iLoop, iBeds, iFBath, iHBath, iBldgNum, iTmp;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001051007", 9))
   //   iTmp = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         //if (bDebug)
         //   LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   //while (!iLoop)
   {
      // Quality Class
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
      *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;

      // Yrblt
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

      // YrEff
      iTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
      if (iTmp > 1600 && iTmp <= lToyear)
         memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

      // BldgSqft
      lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
      if (lBldgSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      } else
         memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

      // Garage Sqft
      lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
      if (lGarSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      } else
         memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);

      // Parking type
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

      // ParkSpace
      iTmp = atoin(pChar->ParkSpace, SIZ_CHAR_SIZE4);
      if (iTmp > 0)
         memcpy(pOutbuf+OFF_PARK_SPACE, pChar->ParkSpace, SIZ_CHAR_SIZE4);
      else
         memset(pOutbuf+OFF_PARK_SPACE, ' ', SIZ_PARK_SPACE);

      // Heating
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];

      // Cooling
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

      // Beds
      iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      } else
         memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

      // Bath
      iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      } else
         memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

      // Half bath
      iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
      if (iHBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      } else
         memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

      // Rooms
      iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      } else
         memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

      // Units
      iTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
      if (pChar->Units[0] > ' ')
      {
         sprintf(acTmp, "%*d", SIZ_UNITS, iTmp);
         memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
      } else
         memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

      // Stories
      if (pChar->Stories[0] > ' ')
         memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
      else
         memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

      // Fireplace
      if (pChar->Fireplace[0] > ' ')
         memcpy(pOutbuf+OFF_FIRE_PL, pChar->Fireplace, SIZ_FIRE_PL);
      else
         memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);

      // HasSeptic or HasSewer
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

      // HasWell
      *(pOutbuf+OFF_WATER) = pChar->HasWater;

      // Pools
      *(pOutbuf+OFF_POOL) = pChar->Pool[0];

      // BldgNum
      iBldgNum = atoin(pChar->BldgSeqNo, SIZ_CHAR_SIZE2);

      // Neighborhood code
      if (pChar->Nbh_Code[0] > ' ')
         vmemcpy(pOutbuf+OFF_NBH_CODE, pChar->Nbh_Code, SIZ_NBH_CODE);

      lCharMatch++;

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      //if (!pRec)
      //   break;
      //iLoop = memcmp(pOutbuf, pRec, iApnLen);
      //if (!iLoop && (iBeds > 0 && iBldgNum > 0))
      //   break;
   }

   return 0;
}

/********************************* Tuo_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Tuo_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Tuo_MergeRoll(): bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN starts with 100-905 (906=mineral, 910=MH
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp > 99 && iTmp < 906))
      return 1;
#ifdef _DEBUG
   //if (iTmp > 910)
   //   iTmp = 0;
#endif

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // PREV_APN - Only apply to physical parcels
      //if (memcmp(pOutbuf, "100", 3) < 0)
      //   vmemcpy(pOutbuf+OFF_PREV_APN, apTokens[iApnFld], 10);

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "55TUO", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   //} else
   //{
      //memcpy(pOutbuf+OFF_CO_NUM, "55TUO", 5);

      // Format APN
      //iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      //memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Copy ALT_APN
      //memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Create MapLink and output new record
      //iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      //memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
   if (*apTokens[MB_ROLL_ZONING] > ' ' && memcmp(apTokens[MB_ROLL_ZONING], "0000", 4))
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   acTmp[SIZ_USE_CO] = 0;
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Neighborhood code
   if (*apTokens[MB_ROLL_NBHCODE] > ' ')
      vmemcpy(pOutbuf+OFF_NBH_CODE, apTokens[MB_ROLL_NBHCODE], SIZ_NBH_CODE);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "097320001000", 9))
   //   iTmp = 0;
#endif

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      // Ignore internal DocNum
      if (*(apTokens[MB_ROLL_DOCNUM]+4) == 'R' && isdigit(*(apTokens[MB_ROLL_DOCNUM]+5)))
      {
         pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp && !memcmp(acTmp, apTokens[MB_ROLL_DOCNUM], 4))
         {
            memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);

            lTmp = atol(apTokens[MB_ROLL_DOCNUM]+5);
            memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);

            iTmp = sprintf(acTmp, "%.5s%.7d", apTokens[MB_ROLL_DOCNUM], lTmp);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, iTmp);
         }
      }
   }

   // Owner
   try {
      Tuo_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER], apTokens[MB_ROLL_CAREOF], apTokens[MB_ROLL_DBA]);
   } catch(...) {
      LogMsg("***** Exeception occured in Tuo_MergeOwner()");
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "990018287", 9) )
   //   iTmp = 0;
#endif
   // Mailing
   try {
      Tuo_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Tuo_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************** Tuo_Load_Roll *****************************
 *
 ******************************************************************************/

int Tuo_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") DEL(124) ");
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") DEL(124) ");
   else
      lRet = sortFile(acSitusFile, acTmpFile, "S(#1,C,A) ");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acExeFile, acTmpFile, "S(#2,C,A) ");
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acTaxFile, acTmpFile, "S(#1,C,A) ");
   fdTax = fopen(acTmpFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
      return -2;
   }

   // Open Owner file
   LogMsg("Open Owner file %s", acNameFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Owner.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acNameFile, acTmpFile, "S(#1,C,A,#2,C,D) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acNameFile, acTmpFile, "S(#1,C,A,#2,C,D) ");

   fdName = fopen(acTmpFile, "r");
   if (fdName == NULL)
   {
      LogMsg("***** Error opening Owner file: %s\n", acTmpFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (acRollRec[0] > '9' || acRollRec[0] < '0');

   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "007280027000", 9))
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Tuo_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Tuo_MergeSitus(acBuf);

            // Merge Owner
            if (fdName)
               lRet = Tuo_MergeOwnerRec(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Tuo_MergeStdChar(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acBuf, 0);

            iRollUpd++;
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp || acRollRec[0] > '9')
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         //if (bDebug)
         //   LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Tuo_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Tuo_MergeSitus(acRec);

            // Merge Owner
            if (fdName)
               lRet = Tuo_MergeOwnerRec(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe2(acRec, 0);

            // Merge Char
            if (fdChar)
               lRet = Tuo_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acRec, iHdrRows);

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp || acRollRec[0] > '9')
         {
            bEof = true;    // Signal to stop
            break;
         } else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.12s < Roll->%.12s (%d) ***", acBuf, (char *)&acRollRec[0], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      //if (bDebug)
      //   LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Tuo_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Tuo_MergeSitus(acRec);

         // Merge Owner
         if (fdName)
            lRet = Tuo_MergeOwnerRec(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe2(acRec, 0);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Tuo_MergeStdChar(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, iHdrRows);

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fdName)
      fclose(fdName);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Owner matched:    %u", lNameMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Owner skiped:     %u", lNameSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   return 0;
}

/**************************** Tuo_ConvStdChar ********************************
 *
 * Convert tuo_char.csv to standard CHAR format.
 * This version parsing new CHAR format from TUO 02/13/2024 which is similar
 * to Phys_char.txt with some extra fields at the end (ASMT, ...)
 *
 *****************************************************************************/

int Tuo_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[32], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsgD("Converting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   pRec = fgets(acBuf, 4096, fdIn);
   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);

      if (!pRec)
         break;
      if (acBuf[1] < '0' || acBuf[1] > '9')
         continue;

      // Parse string
      replStrAll(acBuf, "NULL", "");
      if (cDelim == '|')
         iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

      if (iFldCnt < TUO_PCHAR_FLDS)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));

      // Format APN
      if (*apTokens[TUO_PCHAR_ASMT] >= '0')
      {
         memcpy(myCharRec.Apn, apTokens[TUO_PCHAR_ASMT], strlen(apTokens[TUO_PCHAR_ASMT]));
         iRet = formatApn(apTokens[TUO_PCHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
         memcpy(myCharRec.FeeParcel, apTokens[TUO_PCHAR_FEEPARCEL], strlen(apTokens[TUO_PCHAR_FEEPARCEL]));
      } else
      {
         if (bDebug)
            LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[TUO_PCHAR_FEEPARCEL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[TUO_PCHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Bldg Type
      if (*apTokens[TUO_PCHAR_BUILDINGTYPE] > ' ')
         myCharRec.BldgType[0] = *apTokens[TUO_PCHAR_BUILDINGTYPE];

      // BldgSize
      int iBldgSize = atoi(apTokens[TUO_PCHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[TUO_PCHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      }

      // Rooms
      iTmp = atoi(apTokens[TUO_PCHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[TUO_PCHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[TUO_PCHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
         memcpy(myCharRec.Bath_4Q, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[TUO_PCHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         myCharRec.HBaths[0] = *apTokens[TUO_PCHAR_HALFBATHS];
         myCharRec.Bath_2Q[0] = *apTokens[TUO_PCHAR_HALFBATHS];
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "059200022000", 9))
      //   iRet = 0;
#endif
      // Pool
      iTmp = blankRem(apTokens[TUO_PCHAR_POOLSPA]);
      if (*apTokens[TUO_PCHAR_POOLSPA] > ' ')
      {
         pRec = findXlatCode(apTokens[TUO_PCHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "097070017000", 9))
      //   iRet = 0;
#endif
      // QualityClass 
      if (*apTokens[TUO_PCHAR_QUALITYCLASS] >= '1')
      {
         strcpy(acTmp, apTokens[TUO_PCHAR_QUALITYCLASS]);
         iTmp = iTrim(acTmp);
         memcpy(myCharRec.QualityClass, acTmp, iTmp);
         iRet = atol(acTmp);
         if (iRet > 0 && iRet < 100)
         {  // 3.5
            acCode[0] = 0;
            iRet = Quality2Code((char *)&acTmp[0], acCode, NULL);

            if (acCode[0] > ' ')
               myCharRec.BldgQual  = acCode[0];
            else if (iRet < 0 && acTmp[0] > ' ')
               LogMsg("*** Bad Quality P2: %s (APN=%s)", acTmp, apTokens[0]);
         } else if (acTmp[0] >= 'A')
         {  // D6.5B
            myCharRec.BldgClass = acTmp[0];
            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            if (acCode[0] > ' ')
               myCharRec.BldgQual  = acCode[0];
            else if (iRet < 0 && acTmp[0] > ' ')
               LogMsg("*** Bad Quality P2: %s (APN=%s)", acTmp, apTokens[0]);
         } else
            LogMsg("*** Bad Quality: %s (APN=%s)", acTmp, apTokens[0]);
      }

      // Improved Condition

      // YrBlt
      int iYrBlt = atoi(apTokens[TUO_PCHAR_YEARBUILT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[TUO_PCHAR_EFFECTIVEYEAR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // Garage Type

      // Attached SF
      int iAttGar = atoi(apTokens[TUO_PCHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[TUO_PCHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ' || myCharRec.ParkType[0] == 'H' || iAttGar == 0)
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport SF
      int iCarport = atoi(apTokens[TUO_PCHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ' || myCharRec.ParkType[0] == 'H' || (iAttGar == 0 && iDetGar == 0))
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // LandSqft is not populated, use Acres (same in AMA)
      double dAcres = atof(apTokens[TUO_PCHAR_ACRES]);
      if (dAcres > 0.0)
      {
         iRet = sprintf(acTmp, "%d", (unsigned long)(dAcres*1000.0));
         memcpy(myCharRec.LotAcre, acTmp, iRet);

         iRet = sprintf(acTmp, "%d", (long)(dAcres * SQFT_PER_ACRE));
         memcpy(myCharRec.LotSqft, acTmp, iRet);
      }

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[TUO_PCHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCodeA(apTokens[TUO_PCHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      }

      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[TUO_PCHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[TUO_PCHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[TUO_PCHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[TUO_PCHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // FirePlace
      if (*(apTokens[TUO_PCHAR_FIREPLACE]) > '0')
      {
         blankRem(apTokens[TUO_PCHAR_FIREPLACE]);
         pRec = findXlatCodeA(apTokens[TUO_PCHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
         {
            myCharRec.Fireplace[0] = *pRec;
            if (*pRec > '9')
               myCharRec.Misc.sExtra.FirePlaceType[0] = *pRec;
         }
      }

      // Sewer
      blankRem(apTokens[TUO_PCHAR_SEWERCODE]);
      if (*apTokens[TUO_PCHAR_SEWERCODE] > '0')
      {
         myCharRec.HasSewer = 'Y';
         pRec = findXlatCode(apTokens[TUO_PCHAR_SEWERCODE], &asSewer[0]);
         if (pRec)
            myCharRec.Sewer = *pRec;
      }

      // Water
      if (*apTokens[TUO_PCHAR_WATERSOURCE] > '0')
      {
         blankRem(apTokens[TUO_PCHAR_WATERSOURCE]);
         pRec = findXlatCode(apTokens[TUO_PCHAR_WATERSOURCE], &asWaterSrc[0]);
         if (pRec)
         {
            myCharRec.HasWater = *pRec;
            if (*pRec == 'W')
               myCharRec.HasWell = 'Y';
         }
      } else 
      if (*apTokens[TUO_PCHAR_HASWELL] == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // OfficeSpaceSF
      iTmp = atoi(apTokens[TUO_PCHAR_OFFICESPACESF]);
      if (iTmp > 10)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Misc.sExtra.OfficeSqft, acTmp, iRet);
      }

      // PatioSF
      iTmp = atoi(apTokens[TUO_PCHAR_PATIOSF]);
      if (iTmp > 10)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // ViewCode
      if (*apTokens[TUO_PCHAR_VIEWCODE] > '0')
      {
         pRec = findXlatCode(apTokens[TUO_PCHAR_VIEWCODE], &asView[0]);
         if (pRec)
            myCharRec.View[0] = *pRec;
      }

      // Stories/NumFloors 1, 15, 2, 25, 3
      iTmp = atoi(apTokens[TUO_PCHAR_STORIESCNT]);
      if (iTmp > 0)
      {
         if (iTmp < 10)
            iRet = sprintf(acTmp, "%d.0", iTmp);
         else
            iRet = sprintf(acTmp, "%.1f", iTmp/10.0);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Zoning - bad data
      //if (*apTokens[TUO_PCHAR_ZONING] >= '0')
      //   memcpy(myCharRec.Zoning, apTokens[TUO_PCHAR_ZONING], strlen(apTokens[TUO_PCHAR_ZONING]));

      // SubDiv Name
      if (*apTokens[TUO_PCHAR_SUBDIVNAME] >= '0')
         vmemcpy(myCharRec.Misc.SubDiv, _strupr(apTokens[TUO_PCHAR_SUBDIVNAME]), SIZ_CHAR_SUBDIV);

      // Neighborhood code
      if (*apTokens[TUO_PCHAR_NEIGHBORHOODCODE] >= '0')
         vmemcpy(myCharRec.Nbh_Code, apTokens[TUO_PCHAR_NEIGHBORHOODCODE], SIZ_CHAR_NBH_CODE);

      // Roof cover
      if (*apTokens[TUO_PCHAR_ROOFCOVER] >= 'A')
      {
         pRec = findXlatCodeA(apTokens[TUO_PCHAR_ROOFCOVER], &asRoofMat[0]);
         if (pRec)
            myCharRec.RoofMat[0] = *pRec;
      }

      // Roof type
      if (*apTokens[TUO_PCHAR_ROOFTYPEFLAT] == 'F')
         myCharRec.RoofType[0] = 'F';
      else if (*apTokens[TUO_PCHAR_ROOFTYPEGABLE] == 'G')
         myCharRec.RoofType[0] = 'G';
      else if (*apTokens[TUO_PCHAR_ROOFTYPEHIP] == 'H')
         myCharRec.RoofType[0] = 'H';
      else if (*apTokens[TUO_PCHAR_ROOFTYPESHED] == 'S')
         myCharRec.RoofType[0] = 'K';

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/**************************** Tuo_Phys2StdChar *******************************
 *
 * Convert phys_char.txt to standard CHAR format
 *
 *****************************************************************************/

int Tuo_Phys2StdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[32], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsgD("Tuo_Phys2StdChar - Converting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   pRec = fgets(acBuf, 4096, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);

      if (!pRec)
         break;
      if (acBuf[1] < '0' || acBuf[1] > '9')
         continue;

      // Parse string
      replStrAll(acBuf, "NULL", "");
      iFldCnt = ParseStringNQ(pRec, ',', MAX_FLD_TOKEN, apTokens);

      if (iFldCnt < TUO_PCHAR_PERIMETERLF)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[TUO_PCHAR_FEEPARCEL], strlen(apTokens[TUO_PCHAR_FEEPARCEL]));
      memcpy(myCharRec.FeeParcel, apTokens[TUO_PCHAR_FEEPARCEL], strlen(apTokens[TUO_PCHAR_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[TUO_PCHAR_FEEPARCEL], acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      // Bldg#
      iTmp = atoi(apTokens[TUO_PCHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Bldg Type
      if (*apTokens[TUO_PCHAR_BUILDINGTYPE] > ' ')
         myCharRec.BldgType[0] = *apTokens[TUO_PCHAR_BUILDINGTYPE];

      // BldgSize
      int iBldgSize = atoi(apTokens[TUO_PCHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[TUO_PCHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      }

      // Rooms
      iTmp = atoi(apTokens[TUO_PCHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[TUO_PCHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[TUO_PCHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
         memcpy(myCharRec.Bath_4Q, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[TUO_PCHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         myCharRec.HBaths[0] = *apTokens[TUO_PCHAR_HALFBATHS];
         myCharRec.Bath_2Q[0] = *apTokens[TUO_PCHAR_HALFBATHS];
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "059200022000", 9))
      //   iRet = 0;
#endif
      // Pool
      iTmp = blankRem(apTokens[TUO_PCHAR_POOLSPA]);
      if (*apTokens[TUO_PCHAR_POOLSPA] > ' ')
      {
         pRec = findXlatCode(apTokens[TUO_PCHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "097070017000", 9))
      //   iRet = 0;
#endif
      // QualityClass 
      if (*apTokens[TUO_PCHAR_QUALITYCLASS] >= '1')
      {
         strcpy(acTmp, apTokens[TUO_PCHAR_QUALITYCLASS]);
         iTmp = iTrim(acTmp);
         memcpy(myCharRec.QualityClass, acTmp, iTmp);
         iRet = atol(acTmp);
         if (iRet > 0 && iRet < 100)
         {  // 3.5
            acCode[0] = 0;
            iRet = Quality2Code((char *)&acTmp[0], acCode, NULL);

            if (acCode[0] > ' ')
               myCharRec.BldgQual  = acCode[0];
            else if (iRet < 0 && acTmp[0] > ' ')
               LogMsg("*** Bad Quality P2: %s (APN=%s)", acTmp, apTokens[0]);
         } else if (acTmp[0] >= 'A')
         {  // D6.5B
            myCharRec.BldgClass = acTmp[0];
            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            if (acCode[0] > ' ')
               myCharRec.BldgQual  = acCode[0];
            else if (iRet < 0 && acTmp[0] > ' ')
               LogMsg("*** Bad Quality P2: %s (APN=%s)", acTmp, apTokens[0]);
         } else
            LogMsg("*** Bad Quality: %s (APN=%s)", acTmp, apTokens[0]);
      }

      // Improved Condition

      // YrBlt
      int iYrBlt = atoi(apTokens[TUO_PCHAR_YEARBUILT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[TUO_PCHAR_EFFECTIVEYEAR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // Garage Type

      // Attached SF
      int iAttGar = atoi(apTokens[TUO_PCHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[TUO_PCHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ' || myCharRec.ParkType[0] == 'H' || iAttGar == 0)
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport SF
      int iCarport = atoi(apTokens[TUO_PCHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ' || myCharRec.ParkType[0] == 'H' || (iAttGar == 0 && iDetGar == 0))
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // LandSqft is not populated, use Acres (same in AMA)
      double dAcres = atof(apTokens[TUO_PCHAR_ACRES]);
      if (dAcres > 0.0)
      {
         iRet = sprintf(acTmp, "%d", (unsigned long)(dAcres*1000.0));
         memcpy(myCharRec.LotAcre, acTmp, iRet);

         iRet = sprintf(acTmp, "%d", (long)(dAcres * SQFT_PER_ACRE));
         memcpy(myCharRec.LotSqft, acTmp, iRet);
      }

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[TUO_PCHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCodeA(apTokens[TUO_PCHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      }

      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[TUO_PCHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[TUO_PCHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[TUO_PCHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[TUO_PCHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // FirePlace
      if (*(apTokens[TUO_PCHAR_FIREPLACE]) > '0')
      {
         blankRem(apTokens[TUO_PCHAR_FIREPLACE]);
         pRec = findXlatCodeA(apTokens[TUO_PCHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
         {
            myCharRec.Fireplace[0] = *pRec;
            if (*pRec > '9')
               myCharRec.Misc.sExtra.FirePlaceType[0] = *pRec;
         }
      }

      // Sewer
      blankRem(apTokens[TUO_PCHAR_SEWERCODE]);
      if (*apTokens[TUO_PCHAR_SEWERCODE] > '0')
      {
         myCharRec.HasSewer = 'Y';
         pRec = findXlatCode(apTokens[TUO_PCHAR_SEWERCODE], &asSewer[0]);
         if (pRec)
            myCharRec.Sewer = *pRec;
      }

      // Water
      if (*apTokens[TUO_PCHAR_WATERSOURCE] > '0')
      {
         blankRem(apTokens[TUO_PCHAR_WATERSOURCE]);
         pRec = findXlatCode(apTokens[TUO_PCHAR_WATERSOURCE], &asWaterSrc[0]);
         if (pRec)
         {
            myCharRec.HasWater = *pRec;
            if (*pRec == 'W')
               myCharRec.HasWell = 'Y';
         }
      } else 
      if (*apTokens[TUO_PCHAR_HASWELL] == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // OfficeSpaceSF
      iTmp = atoi(apTokens[TUO_PCHAR_OFFICESPACESF]);
      if (iTmp > 10)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Misc.sExtra.OfficeSqft, acTmp, iRet);
      }

      // PatioSF
      iTmp = atoi(apTokens[TUO_PCHAR_PATIOSF]);
      if (iTmp > 10)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // ViewCode
      if (*apTokens[TUO_PCHAR_VIEWCODE] > '0')
      {
         pRec = findXlatCode(apTokens[TUO_PCHAR_VIEWCODE], &asView[0]);
         if (pRec)
            myCharRec.View[0] = *pRec;
      }

      // Stories/NumFloors 1, 15, 2, 25, 3
      iTmp = atoi(apTokens[TUO_PCHAR_STORIESCNT]);
      if (iTmp > 0)
      {
         if (iTmp < 10)
            iRet = sprintf(acTmp, "%d.0", iTmp);
         else
            iRet = sprintf(acTmp, "%.1f", iTmp/10.0);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Zoning - bad data
      //if (*apTokens[TUO_PCHAR_ZONING] >= '0')
      //   memcpy(myCharRec.Zoning, apTokens[TUO_PCHAR_ZONING], strlen(apTokens[TUO_PCHAR_ZONING]));

      // SubDiv Name
      if (*apTokens[TUO_PCHAR_SUBDIVNAME] >= '0')
         vmemcpy(myCharRec.Misc.SubDiv, _strupr(apTokens[TUO_PCHAR_SUBDIVNAME]), SIZ_CHAR_SUBDIV);

      // Neighborhood code
      if (*apTokens[TUO_PCHAR_NEIGHBORHOODCODE] >= '0')
         vmemcpy(myCharRec.Nbh_Code, apTokens[TUO_PCHAR_NEIGHBORHOODCODE], SIZ_CHAR_NBH_CODE);

      // Roof cover
      if (*apTokens[TUO_PCHAR_ROOFCOVER] >= 'A')
      {
         pRec = findXlatCodeA(apTokens[TUO_PCHAR_ROOFCOVER], &asRoofMat[0]);
         if (pRec)
            myCharRec.RoofMat[0] = *pRec;
      }

      // Roof type
      if (*apTokens[TUO_PCHAR_ROOFTYPEFLAT] == 'F')
         myCharRec.RoofType[0] = 'F';
      else if (*apTokens[TUO_PCHAR_ROOFTYPEGABLE] == 'G')
         myCharRec.RoofType[0] = 'G';
      else if (*apTokens[TUO_PCHAR_ROOFTYPEHIP] == 'H')
         myCharRec.RoofType[0] = 'H';
      else if (*apTokens[TUO_PCHAR_ROOFTYPESHED] == 'S')
         myCharRec.RoofType[0] = 'K';

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/*************************** Tuo_ConvertApnRoll *****************************
 *
 * Convert APN format from Cres to MB.  Keep old APN in PREV_APN.
 *
 ****************************************************************************/

int Tuo_ConvertApnRoll(char *pInfile, char *pOutfile, int iRecordLen)
{
   FILE *fdIn, *fdOut;
   int   iTmp, iCnt=0, iOut=0;
   char  acBuf[2048], acTmp[32], acApn[32];

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "rb");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "wb");

   fread(acBuf, 1, iRecordLen, fdIn);
   fwrite(acBuf, 1, iRecordLen, fdOut);

   while (!feof(fdIn))
   {
      iTmp = fread(acBuf, 1, iRecordLen, fdIn);
      if (iTmp < iRecordLen)
         break;

      if (acBuf[0] == '0')
      {
			if (acBuf[9] > '0')
				LogMsg("*** Ignore APN: %.10s", acBuf);
			else
			{
				// Format new APN - ignore last 2 digits of old APN
				sprintf(acApn, "%.6s0%.2s000", acBuf, &acBuf[6]);

				// Save old APN to previous APN
				memcpy(&acBuf[OFF_PREV_APN], &acBuf[0], 10);

            // Reformat APN
				sprintf(acTmp, "%.3s-%.3s-%.3s-000", acApn, &acApn[3], &acApn[6]);
				memcpy(&acBuf[OFF_APN_D], acTmp, 15);
				memcpy(acBuf, acApn, 12);

            // Clear sale info
            ClearOldSale(acBuf, true);

				fwrite(acBuf, 1, iRecordLen, fdOut);
				iOut++;
			}
      } else if (acBuf[OFF_STATUS] == 'R')
         LogMsg("Drop retired parcel: %.12s", acBuf);
      else
         LogMsg("Drop old parcel: %.12s", acBuf);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Total records processed: %d", iCnt);
   LogMsg("Total records output:    %d", iOut);

   return 0;
}

int Tuo_ConvertApn(char *pInfile, char *pOutfile)
{
   FILE *fdIn, *fdOut;
   int   iCnt=0, iOut=0;
   char  acBuf[2048], acApn[32];
   LIENEXTR *pLdrExt = (LIENEXTR *)acBuf;

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");

   while (!feof(fdIn) && fgets(acBuf, 2048, fdIn))
   {
      if (acBuf[0] >= '0' && acBuf[0] <= '9')
      {
			// Format new APN - ignore last 2 digits of old APN
			sprintf(acApn, "%.6s0%.2s000", acBuf, &acBuf[6]);

			// Save old APN to previous APN
			memcpy(pLdrExt->acPrevApn, &acBuf[0], 10);

         // Save new APN
			memcpy(acBuf, acApn, 12);
			iOut++;
      }

      fputs(acBuf, fdOut);
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Total records processed: %d", iCnt);
   LogMsg("Total records output:    %d", iOut);

   return 0;
}

int Tuo_ConvertApnSale(char *pInfile, char *pOutfile)
{
   FILE     *fdIn, *fdOut;
   int      iCnt=0, iConvert=0;
   double   dTmp;
   char     acBuf[1024], acApn[32], acTmp[32], *pBuf;

   SCSAL_REC *pSale = (SCSAL_REC *)&acBuf[0];

   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");

   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");

   while (!feof(fdIn))
   {
      pBuf = fgets(acBuf, 1024, fdIn);
      if (!pBuf)
         break;

      // Format new APN - ignore last 2 digits of old APN
      sprintf(acApn, "%.6s0%.2s000", acBuf, &acBuf[6]);
      memcpy(acBuf, acApn, 12);

      // Clean up stamp amt
      memcpy(acTmp, pSale->StampAmt, SALE_SIZ_STAMPAMT);
      acTmp[SALE_SIZ_STAMPAMT] = 0;
      dTmp = atof(acTmp);
      if (dTmp > 0.0)
      {
         sprintf(acTmp, "%.2f        ", dTmp);
         memcpy(pSale->StampAmt, acTmp,  SALE_SIZ_STAMPAMT);
         memset(pSale->NumOfPrclXfer, ' ', SALE_SIZ_NOPRCLXFR);

         // Fix DocNum
         if (pSale->DocNum[6] == ' ' && pSale->DocNum[0] > ' ')
            sprintf(acTmp, "%.4sR%.7d", pSale->DocDate, atol(pSale->DocNum));
         else
            sprintf(acTmp, "%.12s", pSale->DocNum);
         memcpy(pSale->DocNum, acTmp, 12);
         fputs(acBuf, fdOut);
         iConvert++;
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Number of records processed: ", iCnt);
   LogMsg("Number of records output:    ", iConvert);

   return 0;
}

/***************************** MB_CreateSCSale *******************************
 *
 * Extract sale data from ???_Sales.csv and output to ???_SALE.DAT
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * DateFmt:
 *    0 : Default. Auto format by checking input date
 *    MM_DD_YYYY_1 (MON)
 *    YYYY_MM_DD   (HUM)
 *
 * DocTypeFmt:
 *    0 : Default. Set DocType=1 (GD) if DocCode=1 or sale price > 0
 *    1 : AMA, BUT, MAD, SON, SBT
 *    2 : PLA
 *    3 : SHA, STA
 *
 * DocNumFmt:
 *    0 : Default. Copy DocNum as is
 *    1 : Format DocNum second part of DocNum to 7 digits (i.e. 2010R1234 = 2010R0001234)
 *    2 : Format DocNum second part of DocNum to n digits (i.e. 2010R0034 = 2010R34) (COL)
 *    3 : Remove all nonnumeric after 5th character and format to 6 digits (SON)
 *    4 : Format to yyyyR9999999 if R is in pos 4 or 5 of original DocNum (SBT)
 *    5 : TEH
 *    6 : MOD - format as yyyy9999 (i.e. 2012-0001 = 20120001)
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Tuo_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend, IDX_TBL5 *pDocTbl)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], acDocNum[32], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;
   int      iYear, iLen, iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp, lDocYear;

   LogMsg0("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      if (strchr(apTokens[MB_SALES_DOCNUM], 'I') || strchr(apTokens[MB_SALES_DOCNUM], 'A'))
      {
         LogMsg("*** Bad DocNum: %s [%s] ---> ignore record", apTokens[MB_SALES_DOCNUM], apTokens[MB_SALES_ASMT]);
         continue;
      }

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "052202011000", 9))
      //   iTmp = 0;
#endif
      // Doc date
      if (iDateFmt > 0)
      {
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      } else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
      {
         lDocYear = atoin(acTmp, 4);
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      } else
         lDocYear = 0;

      // Docnum
      myTrim(apTokens[MB_SALES_DOCNUM]);
      iYear = 0;
      iLen = strlen(apTokens[MB_SALES_DOCNUM]);
      if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R' || *(apTokens[MB_SALES_DOCNUM]+4) == 'r')
      {
         iTmp = atol(apTokens[MB_SALES_DOCNUM]+5);
         iLen = sprintf(acDocNum, "%.4sR%.7d", apTokens[MB_SALES_DOCNUM], iTmp);
      } else if (isdigit(*(apTokens[MB_SALES_DOCNUM]+4)) && !memcmp(apTokens[MB_SALES_DOCNUM], SaleRec.DocDate, 4))
      {
         iTmp = atol(apTokens[MB_SALES_DOCNUM]+4);
         iLen = sprintf(acDocNum, "%.4sR%.7d", apTokens[MB_SALES_DOCNUM], iTmp);
      } else if (isdigit(*(apTokens[MB_SALES_DOCNUM]+4)) && !memcmp(apTokens[MB_SALES_DOCNUM], &SaleRec.DocDate[2], 2))
      {
         // Some typo we want to correct
         iTmp = atol(apTokens[MB_SALES_DOCNUM]+4);
         iLen = sprintf(acDocNum, "%.4sR%.7d", SaleRec.DocDate, iTmp);
         if (bDebug)
            LogMsg("*** Reformat DocNum %s to %s for APN: %s", apTokens[MB_SALES_DOCNUM], acDocNum, apTokens[MB_SALES_ASMT]);
      } else if (isdigit(*(apTokens[MB_SALES_DOCNUM]+4)) && !memcmp(apTokens[MB_SALES_DOCNUM], &SaleRec.DocDate[0], 2))
      {
         // Special case
         if (!memcmp(apTokens[MB_SALES_DOCNUM], "20170000011", 11))
         {
            memcpy(SaleRec.DocDate, "20170104", 8);
            SaleRec.DocType[0] = '6';
            SaleRec.NoneSale_Flg = 'Y';
            iLen = 12;
            memcpy(acDocNum, "2017R0000011", iLen);
         } else
         {
            iTmp = atol(apTokens[MB_SALES_DOCNUM]+4);
            iLen = sprintf(acDocNum, "%.4sR%.7d", SaleRec.DocDate, iTmp);
         }
         if (bDebug)
            LogMsg("*** Reformat DocNum %s to %s for APN: %s", apTokens[MB_SALES_DOCNUM], acDocNum, apTokens[MB_SALES_ASMT]);
      } else
      {
         strcpy(acDocNum, apTokens[MB_SALES_DOCNUM]);
         iLen = 0;
         LogMsg("*** Invalid DocNum: %s DocDate: %s for APN: %s", acDocNum, apTokens[MB_SALES_DOCDATE], apTokens[MB_SALES_ASMT]);
      }

      memcpy(SaleRec.DocNum, acDocNum, iLen);

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

      // Confirmed sale price
      dTmp = atof(apTokens[MB_SALES_PRICE]);
      if (dTmp > 0.0)
      {
         lPrice = (long)((dTmp*100)/100.0);
         iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      } 

      // Do not use confirmed sale price
      if (!bUseConfSalePrice)
         lPrice = 0;

      // Tax
      dTmp = atof(apTokens[MB_SALES_TAXAMT]);
      if (dTmp > 0.0)
      {
         // Save DocTax
         iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);

         // Check for bad DocTax
         if (lPrice == 0)
         {
            // Calculate sale price
            lPrice = (long)(dTmp * SALE_FACTOR);
         }

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Ignore sale price.",
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE]);
               lPrice = 0;
            }
         }
      } 

      // Ignore sale price if less than 1000
      if (lPrice > 1000)
         sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
      else
         memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
      memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);

      // Doc code - accept following code only
      int iDocCode = 0;
      if (SaleRec.DocType[0] == ' ' && isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         // If no DocCode and has sale price, assume GD
         if (!memcmp(apTokens[MB_SALES_DOCCODE], "00", 2) && lPrice > 1000)
         {
            SaleRec.DocType[0] = '1';
         } else if (pDocTbl)
         {
            iTmp = findDocType(apTokens[MB_SALES_DOCCODE], pDocTbl);
            if (iTmp >= 0)
            {
               memcpy(SaleRec.DocType, pDocTbl[iTmp].pCode, pDocTbl[iTmp].iCodeLen);
               SaleRec.NoneSale_Flg = pDocTbl[iTmp].flag;
               if (lPrice > 1000)
               {
                  if (iTmp == 1)                         // Full sale
                     SaleRec.SaleCode[0] = 'F';
                  else if (iTmp == 57)                   // Partial interest
                     SaleRec.SaleCode[0] = 'P';
               }
            } else if (bDebug)
               LogMsg("*** Unknown DocCode: %s", apTokens[MB_SALES_DOCCODE]);
         }
      }

      // Save original DocCode
      vmemcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);

      // Transfer Type
      //if (*apTokens[MB_SALES_XFERTYPE] > ' ' && SaleRec.SaleCode[0] == ' ')
      //{
      //   iTmp = 0;
      //   while (iTmp < MAX_SALETYPE && *asXferTypes[iTmp].pName)
      //   {
      //      if (!memcmp(apTokens[MB_SALES_XFERTYPE], asXferTypes[iTmp].pName, asXferTypes[iTmp].iNameLen))
      //      {
      //         SaleRec.SaleCode[0] = *asXferTypes[iTmp].pCode;
      //         break;
      //      }
      //      iTmp++;
      //   }
      //}

      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         if (pTmp = strstr(apTokens[MB_SALES_SELLER], "C/O"))
            *pTmp = 0;
         if (pTmp = strstr(apTokens[MB_SALES_SELLER], "J/T"))
            *pTmp = 0;
         if (pTmp = strstr(apTokens[MB_SALES_SELLER], "C/P"))
            *pTmp = 0;
         if (pTmp = strstr(apTokens[MB_SALES_SELLER], "S/S"))
            *pTmp = 0;

         iTmp = replStr(apTokens[MB_SALES_SELLER], " % ", " ");
         if (iTmp > SIZ_SELLER)
         {
            // Chop it down to fit seller
            if (pTmp = strrchr(apTokens[MB_SALES_SELLER], '&'))
               *pTmp = 0;
         }
         vmemcpy(SaleRec.Seller1, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);

         // Buyer
         iTmp = replStr(apTokens[MB_SALES_BUYER], " % ", " ");
         vmemcpy(SaleRec.Name1, apTokens[MB_SALES_BUYER], SALE_SIZ_BUYER, iTmp);

         // Skip if DocNum not available
         if (SaleRec.DocNum[0] > ' ')
         {
            SaleRec.CRLF[0] = 10;
            SaleRec.CRLF[1] = 0;
            fputs((char *)&SaleRec,fdOut);
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);

   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(27,2,C,EQ,\"  \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -2;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            // Save old cumsale file
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);
            LogMsg("Rename %s to %s.", acCSalFile, acTmpFile);
            iTmp = rename(acCSalFile, acTmpFile);

            // Rename srt to SLS file
            LogMsg("Rename %s to %s.", acSrtFile, acCSalFile);
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -2;
      } else
         iTmp = rename(acOutFile, acCSalFile);

      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acCSalFile, acTmpFile);
      }
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp == -2)
      LogMsg("***** Error sorting output file");
   else if (iTmp)
      LogMsg("***** Error renaming to %s", acCSalFile);

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                          output: %d.", lTmp);
   LogMsg("         Latetest recording date: %d.", lLastRecDate);
   return iTmp;
}

///********************************* Tuo_MergeLien *****************************
// *
// * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
// * Return 0 if successful, < 0 if error
// *        1 retired record, not use
// *
// *****************************************************************************/
//
//int Tuo_MergeLien3x(char *pOutbuf, char *pRollRec)
//{
//   char     acTmp[256], acTmp1[64], *pTmp;
//   long     lTmp;
//   double   dTmp;
//   int      iRet=0, iTmp;
//
//   // Replace null char with space
//   iRet = replNull(pRollRec, ' ', 0);
//
//   // Parse input rec
//   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
//   if (iRet < L3_CURRENTDOCDATE)
//   {
//      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
//      return -1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//
//   // Remove hyphen from APN
//   remChar(apTokens[L3_ASMT], '-');
//
//   // Start copying data
//   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));
//
//   // Copy ALT_APN
//   iTmp = strlen(apTokens[L3_FEEPARCEL]);
//   if (iTmp < iApnLen)
//   {
//      iRet = iApnLen-iTmp;
//      memcpy(pOutbuf+OFF_ALT_APN, "0000", iRet);
//      memcpy(pOutbuf+OFF_ALT_APN+iRet, apTokens[L3_FEEPARCEL], iTmp);
//   } else
//      memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], iTmp);
//
//   // Format APN
//   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//   // Create MapLink and output new record
//   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // Create index map link
//   if (getIndexPage(acTmp, acTmp1, &myCounty))
//      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//   // County code
//   memcpy(pOutbuf+OFF_CO_NUM, "25MOD", 5);
//
//   // status
//   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];
//
//   // TRA
//   lTmp = atol(apTokens[L3_TRA]);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%0.6d", lTmp);
//      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
//   }
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // Land
//   long lLand = atoi(apTokens[L3_LANDVALUE]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: Growing Impr, Fixture, PersProp, PPMH
//   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
//   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
//   long lGrow  = atoi(apTokens[L3_GROWING]);
//   long lPers  = atoi(apTokens[L3_PPVALUE]);
//   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
//   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//
//      if (lFixtr > 0)
//      {
//         sprintf(acTmp, "%d         ", lFixtr);
//         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
//      }
//      if (lFixtRP > 0)
//      {
//         sprintf(acTmp, "%d         ", lFixtRP);
//         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
//      }
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%d         ", lPers);
//         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
//      }
//      if (lGrow > 0)
//      {
//         sprintf(acTmp, "%d         ", lGrow);
//         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
//      }
//      if (lPP_MH > 0)
//      {
//         sprintf(acTmp, "%d         ", lPP_MH);
//         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
//      }
//   }
//
//   // Gross total
//   lTmp += (lLand+lImpr);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//   // Exemption
//   long lExe1 = atol(apTokens[L3_HOX]);
//   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
//   lTmp = lExe1+lExe2;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
//      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
//   }
//
//   iTmp = OFF_EXE_CD1;
//   if (lExe1 > 0)
//   {
//      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
//      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
//      iTmp = OFF_EXE_CD2;
//   } else
//      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
//
//   // Save exemption code
//   if (lExe2 > 0 && *apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
//      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));
//
//   // Legal
//   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);
//
//   // UseCode
//   if (*apTokens[L3_LANDUSE1] > ' ')
//   {
//      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
//
//      // Std Usecode
//      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
//   } else
//      memcpy(pOutbuf+OFF_USE_STD, "999", 3);
//
//   // Acres
//   dTmp = atof(apTokens[L3_ACRES]);
//   lTmp = atol(apTokens[L3_LANDSIZE]);
//   if (dTmp > 0.0)
//   {
//      // Lot Sqft
//      lTmp = (long)(dTmp * SQFT_PER_ACRE);
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      // Format Acres
//      lTmp = (long)(dTmp * ACRES_FACTOR);
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   } else if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      lTmp = (long)(lTmp*SQFT_MF_1000);
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   // AgPreserved
//   //if (*apTokens[L3_ISAGPRESERVE] == '1')
//   //   *(pOutbuf+OFF_AG_PRE) = 'Y';
//
//   // Owner
//   if (*apTokens[L3_MAILADDRESS1] == '%')
//      Tuo_MergeOwner(pOutbuf, apTokens[L3_OWNER], apTokens[L3_MAILADDRESS1]);
//   else
//      Tuo_MergeOwner(pOutbuf, apTokens[L3_OWNER], NULL);
//
//   // Situs
//   //Tuo_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);
//
//   // Mailing
//   Tuo_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);
//
//   // SetTaxcode, Prop8 flag, FullExe flag
//   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);
//
//   // Recorded Doc - 2016
//   if (*apTokens[L3_CURRENTDOCNUM] > '0')
//   {
//      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
//      if (pTmp)
//      {
//         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
//         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
//      }
//   }
//
//   //// Garage size
//   //dTmp = atof(apTokens[L3_GARAGESIZE]);
//   //if (dTmp > 0.0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_GAR_SQFT, dTmp);
//   //   memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
//   //   *(pOutbuf+OFF_PARK_TYPE) = '2';                 // GARAGE/CARPORT
//   //}
//
//   //// Number of parking spaces
//   //if (*apTokens[L3_GARAGE] == '0' || *apTokens[L3_GARAGE] == 'N')
//   //   *(pOutbuf+OFF_PARK_TYPE) = 'H';                 // None
//   //else if (*apTokens[L3_GARAGE] > '0' && *apTokens[L3_GARAGE] <= '9')
//   //{
//   //   iTmp = atol(apTokens[L3_GARAGE]);
//   //   sprintf(acTmp, "%d", iTmp);
//   //   vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
//   //   if (dTmp > 100)
//   //      *(pOutbuf+OFF_PARK_TYPE) = 'Z';              // Garage
//   //   else
//   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Garage/Carport
//   //} else
//   //{
//   //   if (*(apTokens[L3_GARAGE]) == 'C')
//   //      *(pOutbuf+OFF_PARK_TYPE) = 'C';              // Carport
//   //   else if (*(apTokens[L3_GARAGE]) == 'A')
//   //   {
//   //      if (dTmp > 100)
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
//   //      else
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
//   //   } else if (!_memicmp(apTokens[L3_GARAGE], "DOU", 3))
//   //   {
//   //      *(pOutbuf+OFF_PARK_SPACE) = '2';
//   //      if (dTmp > 100)
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
//   //      else
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
//   //   } else if (!_memicmp(apTokens[L3_GARAGE], "TRI", 3))
//   //   {
//   //      *(pOutbuf+OFF_PARK_SPACE) = '3';
//   //      if (dTmp > 100)
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
//   //      else
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
//   //   } else if (!_memicmp(apTokens[L3_GARAGE], "SIN", 3))
//   //   {
//   //      *(pOutbuf+OFF_PARK_SPACE) = '1';
//   //      if (dTmp > 100)
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'I';           // Attached garage
//   //      else
//   //         *(pOutbuf+OFF_PARK_TYPE) = 'A';           // Attached
//   //   } else if (!_memicmp(apTokens[L3_GARAGE], "GC", 2))
//   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT
//   //   else if (!_memicmp(apTokens[L3_GARAGE], "GS", 2))
//   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // GARAGE/CARPORT ?
//   //   else if (!_memicmp(apTokens[L3_GARAGE], "DE", 2))
//   //      *(pOutbuf+OFF_PARK_TYPE) = 'D';              // Detached
//   //   else if (*(apTokens[L3_GARAGE]) == 'S')
//   //      *(pOutbuf+OFF_PARK_TYPE) = '2';              // Space ?
//   //}
//
//   //// YearBlt
//   //lTmp = atol(apTokens[L3_YEARBUILT]);
//   //if (lTmp > 1800 && lTmp < lToyear)
//   //{
//   //   iTmp = sprintf(acTmp, "%d", lTmp);
//   //   memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
//   //}
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "006380014000", 9))
//   //   iTmp = 0;
//#endif
//
//   // Acres
//   dTmp = atof(apTokens[L3_ACRES]);
//   if (dTmp > 0.0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   // Lot size
//   lTmp = atol(apTokens[L3_LANDSIZE]);
//   if (lTmp > 10)
//   {
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//      if (!dTmp)
//      {
//         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(lTmp*SQFT_MF_1000));
//         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//      }
//   } else if (dTmp > 0.0)
//   {
//      //lTmp = (dTmp+0.0005)*SQFT_PER_ACRE;
//      lTmp = (long)(dTmp*SQFT_PER_ACRE);
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//   }
//
//   //// Total rooms
//   //iTmp = atol(apTokens[L3_TOTALROOMS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
//   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
//   //}
//
//   //// Stories
//   //iTmp = atol(apTokens[L3_STORIES]);
//   //if (iTmp > 0 && iTmp < 100)
//   //{
//   //   sprintf(acTmp, "%d.0", iTmp);
//   //   vmemcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
//   //}
//
//   //// Units
//   //iTmp = atol(apTokens[L3_UNITS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%d", iTmp);
//   //   vmemcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
//   //}
//
//   //// Beds
//   //iTmp = atol(apTokens[L3_BEDROOMS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_BEDS, iTmp);
//   //   memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
//   //}
//
//   //// Baths
//   //iTmp = atol(apTokens[L3_BATHS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_BATH_F, iTmp);
//   //   memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
//   //}
//
//   //// HBaths
//   //iTmp = atol(apTokens[L3_HALFBATHS]);
//   //if (iTmp > 0)
//   //{
//   //   sprintf(acTmp, "%*d", SIZ_BATH_H, iTmp);
//   //   memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
//   //}
//
//   //// Heating
//   //int iCmp;
//   //if (*apTokens[L3_HEATING] > ' ')
//   //{
//   //   iTmp = 0;
//   //   iCmp = -1;
//   //   while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_HEATING], asHeating_LDR[iTmp].acSrc, asHeating_LDR[iTmp].iLen)) > 0)
//   //      iTmp++;
//
//   //   if (!iCmp)
//   //      *(pOutbuf+OFF_HEAT) = asHeating_LDR[iTmp].acCode[0];
//   //}
//
//   //// Cooling
//   //if (*apTokens[L3_AC] == 'C')
//   //   *(pOutbuf+OFF_AIR_COND) = 'C';
//   //else if (*apTokens[L3_AC] > ' ')
//   //   LogMsg("*** Unknown A/C: %s", apTokens[L3_AC]);
//
//   //// Pool/Spa
//   //if (*apTokens[L3_POOLSPA] > ' ')
//   //{
//   //   iTmp = 0;
//   //   iCmp = -1;
//   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_POOLSPA], asPool_LDR[iTmp].acSrc, asPool_LDR[iTmp].iLen)) > 0)
//   //      iTmp++;
//
//   //   if (!iCmp)
//   //      *(pOutbuf+OFF_POOL) = asPool_LDR[iTmp].acCode[0];
//   //}
//
//   //// Fire place
//   //if (*apTokens[L3_FIREPLACE] > ' ')
//   //{
//   //   iTmp = 0;
//   //   iCmp = -1;
//   //   pTmp = _strupr(apTokens[L3_FIREPLACE]);
//   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_FIREPLACE], asFP_LDR[iTmp].acSrc, asFP_LDR[iTmp].iLen)) > 0)
//   //      iTmp++;
//
//   //   if (!iCmp)
//   //      *(pOutbuf+OFF_FIRE_PL) = asPool[iTmp].acCode[0];
//   //}
//
//   //// Quality Class
//   //acTmp1[0] = 0;
//   //if (*apTokens[L3_QUALITYCLASS] > '0' || strlen(apTokens[L3_QUALITYCLASS]) > 1)
//   //{
//   //   strcpy(acTmp, apTokens[L3_QUALITYCLASS]);
//   //   remCharEx(acTmp, " ,'?");
//   //   pTmp = _strupr(acTmp);
//
//   //   //memcpy(myCharRec.QualityClass, pTmp, strlen(pTmp));
//   //   if (acTmp[0] == 'A' && !isdigit(acTmp[1]))
//   //      acTmp1[0] = 'A';
//   //   else if (acTmp[0] == 'F' && !isdigit(acTmp[1]))
//   //      acTmp1[0] = 'F';
//   //   else if (!memcmp(pTmp, "POOR", 4) || !memcmp(pTmp, "LOW", 3))
//   //      acTmp1[0] = 'P';
//   //   else if (*pTmp == 'G')
//   //      acTmp1[0] = 'G';
//   //   else if (isalpha(*pTmp))
//   //   {
//   //      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
//   //      if (isdigit(acTmp[1]))
//   //         iRet = Quality2Code(&acTmp[1], acTmp1, NULL);
//   //      else if (isdigit(acTmp[2]))
//   //         iRet = Quality2Code(&acTmp[2], acTmp1, NULL);
//   //      else if (isalpha(acTmp[1]))
//   //      {
//   //         switch (acTmp[1])
//   //         {
//   //            case 'L':
//   //            case 'P':
//   //               acTmp1[0] = 'P';
//   //               break;
//   //            case 'A':
//   //               acTmp1[0] = 'A';
//   //               break;
//   //            case 'F':
//   //               acTmp1[0] = 'F';
//   //               break;
//   //            case 'G':
//   //               acTmp1[0] = 'G';
//   //               break;
//   //         }
//   //      }
//   //   } else if (isdigit(*pTmp))
//   //   {
//   //      iTmp = atol(pTmp);
//   //      if (iTmp < 100)
//   //         iRet = Quality2Code(pTmp, acTmp1, NULL);
//   //   }
//
//   //   if (acTmp1[0] > '0')
//   //      *(pOutbuf+OFF_BLDG_QUAL) = acTmp1[0];
//   //}
//
//   return 0;
//}
//
///******************************** Tuo_Load_LDR3 *****************************
// *
// * Load LDR 2016
// *
// ****************************************************************************/
//
//int Tuo_Load_LDR3x(int iFirstRec /* 1=create header rec */)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
//   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
//
//   HANDLE   fhOut;
//   FILE     *fdRoll;
//
//   int      iRet;
//   DWORD    nBytesWritten;
//   BOOL     bRet, bEof;
//   long     lRet=0, lCnt=0, lTmp;
//
//   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
//   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//
//   // Sort roll file on ASMT
//   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   lTmp = getFileDate(acRollFile);
//   if (lTmp < lToday)
//   {
//      iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
//      if (!iRet)
//         return -1;
//   }
//
//   // Open roll file
//   LogMsg("Open Roll file %s", acRollFile);
//   fdRoll = fopen(acRollFile, "r");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acRollFile);
//      return -1;
//   }
//
//   // Open Char file
//   LogMsg("Open Char file %s", acCChrFile);
//   fdChar = fopen(acCChrFile, "r");
//   if (fdChar == NULL)
//   {
//      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
//      return -2;
//   }
//
//   // Open Situs file
//   LogMsg("Open Situs file %s", acSitusFile);
//   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   lTmp = getFileDate(acTmpFile);
//   if (lTmp < lToday)
//   {
//      strcpy(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") ");
//      if (cDelim == '|')
//         strcat(acRec, "DEL(124) ");
//      lRet = sortFile(acSitusFile, acTmpFile, acRec);
//   }
//   fdSitus = fopen(acTmpFile, "r");
//   if (fdSitus == NULL)
//   {
//      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
//      return -2;
//   }
//
//   // Open Output file
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error opening output file: %s\n", acOutFile);
//      return -4;
//   }
//
//   // Output first header record
//   if (iFirstRec > 0)
//   {
//      memset(acBuf, '9', iRecLen);
//      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//   }
//
//   // Get 1st rec
//   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
//   if (*pTmp > '9')
//      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
//
//   bEof = (pTmp ? false:true);
//
//   // Init variables
//   iNoMatch=iBadCity=iBadSuffix=0;
//
//   // Merge loop
//   while (!feof(fdRoll))
//   {
//      lLDRRecCount++;
//      // Create new R01 record
//      iRet = Tuo_MergeLien3(acBuf, acRec);
//      if (!iRet)
//      {
//         // Merge Situs
//         if (fdSitus)
//            lRet = Tuo_MergeSitus(acBuf);
//
//         // Merge Char
//         if (fdChar)
//            lRet = Tuo_MergeStdChar(acBuf);
//
//         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//         if (!(++lCnt % 1000))
//            printf("\r%u", lCnt);
//
//         if (!bRet)
//         {
//            LogMsg("***** Error writing to output file at record %d\n", lCnt);
//            lRet = WRITE_ERR;
//            break;
//         }
//      }
//
//      // Get next roll record
//      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
//      if (!pTmp)
//         break;
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fdChar)
//      fclose(fdChar);
//   if (fdSitus)
//      fclose(fdSitus);
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   LogMsg("Total input records:        %u", lLDRRecCount);
//   LogMsg("Total output records:       %u", lCnt);
//   LogMsg("Total bad-city records:     %u", iBadCity);
//   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
//   LogMsg("Number of Char matched:     %u", lCharMatch);
//   LogMsg("Number of Char skiped:      %u", lCharSkip);
//   LogMsg("Number of Situs matched:    %u", lSitusMatch);
//   LogMsg("Number of Situs skiped:     %u\n", lSitusSkip);
//   printf("\nTotal output records: %u\n", lCnt);
//
//   lRecCnt = lCnt;
//   return 0;
//}

/********************************* Tuo_MergeLien *****************************
 *
 * For 2017 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Tuo_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_ISAGPRESERVE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "55TUO", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

      // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&TUO_Exemption);

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Mailing
   Tuo_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "084140023000", 9))
   //   iTmp = 0;
#endif

   // Recorded Doc - 2016
   if (*apTokens[L3_CURRENTDOCNUM] > '0' && isdigit(*(apTokens[L3_CURRENTDOCNUM]+5)) > '0' && !strchr(apTokens[L3_CURRENTDOCNUM], 'I'))
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   }

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Lot size
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      if (!dTmp)
      {
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(lTmp*SQFT_MF_1000));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   } else if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   return 0;
}

/******************************** Tuo_Load_LDR ******************************
 *
 * Load LDR 2017
 *
 ****************************************************************************/

int Tuo_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acX01[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll, *fdX01;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acRollFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acTmpFile, acRollFile, "S(#3,C,A) DEL(9)");  // 2016
   if (!iRet)
      return -1;

   // Open roll file
   LogMsg("Open LDR file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acRollFile);
      return -1;
   }

   // Open old X01 file for PREV_APN
   sprintf(acX01, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "X01");
   if (!_access(acX01, 0))
   {
      LogMsg("Open X01 file %s", acX01);
      if (!(fdX01 = fopen(acX01, "rb")))
      {
         LogMsg("***** Error opening %s", acX01);
         return -1;
      }
   } else
      fdX01 = NULL;

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Value file
   //sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (!_access(acTmpFile, 0))
   //{
   //   LogMsg("Open Lien extract file %s", acTmpFile);
   //   fdLienExt = fopen(acTmpFile, "r");
   //   if (fdLienExt == NULL)
   //   {
   //      LogMsg("***** Error opening lien file: %s\n", acTmpFile);
   //      return -7;
   //   }
   //} else
   //   fdLienExt = NULL;

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(1,13,C,A) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Owner file
   LogMsg("Open Owner file %s", acNameFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Owner.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (cDelim == '|')
      lRet = sortFile(acNameFile, acTmpFile, "S(#1,C,A,#2,C,D) OMIT(1,1,C,LT,\"0\") DEL(124) ");
   else
      lRet = sortFile(acNameFile, acTmpFile, "S(#1,C,A,#2,C,D) ");

   fdName = fopen(acTmpFile, "r");
   if (fdName == NULL)
   {
      LogMsg("***** Error opening Owner file: %s\n", acTmpFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!feof(fdRoll))
   {
      // Create new R01 record
      iRet = Tuo_MergeLien(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         //if (fdLienExt)
         //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge PREV_APN
         if (fdX01)
            lRet = PQ_MergePrevApn(acBuf, 10, fdX01);

         // Merge Situs
         if (fdSitus)
            lRet = Tuo_MergeSitus(acBuf);

         // Merge Owner
         if (fdName)
            lRet = Tuo_MergeOwnerRec(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Tuo_MergeStdChar(acBuf);

         // Save last recording date
         //lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         //if (lRet > lLastRecDate && lRet < lToday)
         //   lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp || *pTmp > '9')
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdX01)
      fclose(fdX01);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdName)
      fclose(fdName);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/****************************** Tuo_CreatePublR01 ****************************
 *
 * Create public record in R01 format
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Tuo_CreatePublR01(char *pOutbuf, char *pRollRec, bool bCreate)
{
   char     acTmp[256], acTmp1[256];
   int      iRet, iCnt;

   iRet = ParseStringIQ(pRollRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iRet < TUO_UA_REM3)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[0]);
      return -1;
   }

   if (bCreate)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Remove hyphen from APN
      strcpy(acTmp1, apTokens[TUO_UA_APN]);
      remChar(acTmp1, '-');

      // Start copying data
      memcpy(pOutbuf, acTmp1, strlen(acTmp1));

      // Formatted APN
      memcpy(pOutbuf+OFF_APN_D, apTokens[TUO_UA_APN], strlen(apTokens[TUO_UA_APN]));

      // Create MapLink and output new record
      iRet = formatMapLink(acTmp1, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      memcpy(pOutbuf+OFF_CO_NUM, "55TUOA", 6);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Entity
      if (*apTokens[TUO_UA_ENTITY] > ' ')
      {
         if (!strcmp(apTokens[TUO_UA_ENTITY], apTokens[TUO_UA_NAME]))
            strcpy(acTmp, apTokens[TUO_UA_ENTITY]);
         else
            sprintf(acTmp, "%s %s", apTokens[TUO_UA_ENTITY], apTokens[TUO_UA_NAME]);
         iCnt = remChar(acTmp, 39);        // Remove single quote
         iCnt = blankRem(acTmp);
         vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1, iCnt);
      }
   }

   long lSqFtLand=0;
   double dAcreAge = atof(apTokens[TUO_UA_ACRES]);
   if (dAcreAge > 0.0)
   {
      dAcreAge *= 1000.0;
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dAcreAge));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lSqFtLand = (long)((dAcreAge * SQFT_FACTOR_1000));
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lSqFtLand);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "002-110-02", 10))
   //   iRet = 0;
#endif

   // Legal
   if (*apTokens[TUO_UA_LOCATION] > ' ')
      updateLegal(pOutbuf, apTokens[TUO_UA_LOCATION]);

   // Set public parcel flag
   *(pOutbuf+OFF_PUBL_FLG) = 'Y';

   // HO Exempt
   *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   return 0;
}

/********************************* Tuo_MergePubl ****************************
 *
 * Merge public parcel file to roll file
 *
 ****************************************************************************/

int Tuo_MergePubl(char *pPubParcelFile, int iSkip)
{
   char     acRec[2048], acBuf[2048], acRollRec[512];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], *pRoll;

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iNewRec=0, iChgRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   // Preparing output file
   sprintf(acRawFile, acRawTmpl, "Tuo", "Tuo", "R01");
   sprintf(acOutFile, acRawTmpl, "Tuo", "Tuo", "TMP");

   // Make sure R01 file is avail.
   if (_access(acRawFile, 0))
   {
      LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
      return -1;
   }

   // Open unassessed file
   LogMsg("Open unassessed file %s", pPubParcelFile);
   fdRoll = fopen(pPubParcelFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening unassessed file: %s\n", pPubParcelFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   for (iTmp = 0; iTmp < iSkip; iTmp++)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Get first rec
   pRoll = fgets(acRollRec, 512, fdRoll);

   // Merge loop
   while (pRoll &&  *pRoll < 'A')
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (iRecLen != nBytesRead)
         break;

NextRollRec:
      iTmp = memcmp(&acBuf[OFF_APN_D], &acRollRec[0], 15);
      if (iTmp <= 0)
      {
         if (!iTmp)
         {
            LogMsg("*** Update existing unassessed record: %.12s", acBuf);
            iRet = Tuo_CreatePublR01(acBuf, acRollRec, false);
            iChgRec++;

            // Get next record
            pRoll = fgets(acRollRec, 512, fdRoll);
         }

         // Write existing rec out
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else if (iTmp > 0)       // Insert new roll record
      {
         // Create new R01 record from unassessed record
         iRet = Tuo_CreatePublR01(acRec, acRollRec, true);
         iNewRec++;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         // Get next record
         pRoll = fgets(acRollRec, 512, fdRoll);
         if (!pRoll)
            break;
         else
            goto NextRollRec;
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   while (bRet && (iRecLen == nBytesRead))
   {
      // Write existing rec out
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   DeleteFile(acRawFile);
   rename(acOutFile, acRawFile);

   printf("\n");
   LogMsgD("Total output records:       %u", lCnt);
   LogMsg("Total unassessed records:   %u", iNewRec);
   LogMsg("Records status changed  :   %u", iChgRec);

   lRecCnt = lCnt;

   return lRet;
}


/******************************* Tuo_MakeDocLink *******************************
 *
 * Format DocLink
 *
 ******************************************************************************/

void Tuo_MakeDocLink(char *pDocLink, char *pDoc, char *pDate)
{
   int   iDocNum;
   char  acTmp[256], acDocName[256], acDocNum[32];

   *pDocLink = 0;
   if (*pDoc > ' ' && *pDate > ' ' && *(pDoc+4) == 'R')
   {
      iDocNum = atoin((char *)pDoc+5, 7);       // DocNum can be 6 or 7 digits
      sprintf(acDocNum, "%.6d", iDocNum);       // Reformat DocNum
      sprintf(acDocName, "%.4s\\%.3s\\%.4s%s", pDate, acDocNum, pDate, acDocNum);
      sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocName);

      try
      {
         if (!_access(acTmp, 0))
            strcpy(pDocLink, &acDocName[0]);
         else
            *pDocLink = 0;
      } catch (...)
      {
         LogMsg("*** Bad file name: %s", acTmp);
      }
   }
}

/********************************** MergeGrGr ********************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update owner(false)/seller(true) if requested.
 * If owner is updated, transfer will be updated as well
 * This is the modified version of MergeGrGr() which will not update sale in 
 * the same date.
 *
 *****************************************************************************/

int Tuo_MergeGrGrExp(char *pExpRec, char *pOutbuf, bool bUpdtSeller, bool bUpdtOwner, bool bUpdtPrice)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lTmp;
   char  *pTmp, acTmp[32];

   SCSAL_REC *pSaleRec = (SCSAL_REC *)pExpRec;

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt > lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }

   // Ignore non-sale document
   if (pSaleRec->NoneSale_Flg == 'Y')
      return 0;

   // If same date and ccurrent has sale price, use it
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   // Only update if current sale is newer
   if (lCurSaleDt < lLstSaleDt)
      return 0;

   lPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
   if (lCurSaleDt == lLstSaleDt)
   {
      lTmp = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (bUpdtPrice && lPrice > 5000 && lTmp == 0)
         goto UpdateCurrentSale;

      return 0;
   }

   // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);

UpdateCurrentSale:

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);
   if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, 3);
   else if (pTmp=findDocType(pSaleRec->DocType, acTmp))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pTmp, strlen(pTmp));

   // Remove sale code - 
   // Notes: SALE_SIZ_SALECODE is smaller than  SIZ_SALE1_CODE.  Don't swap them.
   if (pSaleRec->SaleCode[0] >= '0')
      memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, SALE_SIZ_SALECODE);
   else
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);

   if (bUpdtSeller && pSaleRec->Seller1[0] > ' ')
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);

   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

   // Update owner
   if (bUpdtOwner)
   {
      if (pSaleRec->Name1[0] > ' ')
      {
         memset(pOutbuf+OFF_NAME1, ' ', SIZ_NAME1);
         memcpy(pOutbuf+OFF_NAME1, pSaleRec->Name1, SIZ_NAME1);
         memcpy(pOutbuf+OFF_NAME2, pSaleRec->Name2, SIZ_NAME2);
      }
   }

   *(pOutbuf+OFF_AR_CODE1) = 'R';
   return 1;
}

/********************************* MergeGrGrFile *****************************
 *
 * Merge GrGr_Exp data to R01 file - TUO
 *
 *****************************************************************************/

int Tuo_MergeGrGrFile(char *pCnty, bool bUpdtSeller, bool bUpdtOwner, bool bUpdtPrice)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acSaleRec[1024];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acGrGrFile[_MAX_PATH];

   HANDLE    fhIn, fhOut;
   SCSAL_REC *pSaleRec = (SCSAL_REC *)&acSaleRec[0];

   int      iTmp, iSaleUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   if (_access(acRawFile, 0))
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "N01");
   sprintf(acGrGrFile, acEGrGrTmpl, pCnty, "DAT");        // GrGr_Exp.dat

   if (!iGrGrApnLen)
      iGrGrApnLen = myCounty.iApnLen;

   LogMsg0("Merge GrGr file %s to %s", acGrGrFile, acRawFile);

   if (_access(acGrGrFile, 0))
   {
      LogMsg("*** %s not available.", acGrGrFile);
      return 1;
   }

   // Open Sale file
   fdSale = fopen(acGrGrFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", acGrGrFile);
      return -2;
   }

   // Get first record
   pTmp = fgets(acSaleRec, 1024, fdSale);
   if (!pTmp || feof(fdSale))
   {
      LogMsg("*** No GrGr data available");
      return 0;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

      // Update sale
GrGr_ReLoad:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "015060025000", 12) )
      //   iTmp = 0;
#endif
      iTmp = memcmp(acBuf, pSaleRec->Apn, iGrGrApnLen);
      if (!iTmp)
      {
         // Merge sale data
         iTmp = Tuo_MergeGrGrExp(acSaleRec, acBuf, bUpdtSeller, bUpdtOwner, bUpdtPrice);
         if (iTmp > 0)
         {
            iSaleUpd++;
            iTmp = atoin((char *)&acBuf[OFF_SALE1_DT], SIZ_SALE1_DT);
            if (iTmp > lLastRecDate && iTmp < lToday)
               lLastRecDate = iTmp;
         } else
            iNoMatch++;

         // Read next sale record
         pTmp = fgets(acSaleRec, 1024, fdSale);
         if (!pTmp)
            bEof = true;    // Signal to stop sale update
         else
            goto GrGr_ReLoad;
      } else
      {
         if (iTmp > 0)        // Sale not match, advance to next sale record
         {
            if (bDebug)
               LogMsg0("*** Sale not match : %.12s > %.12s (%d) ", acBuf, pSaleRec->Apn, lCnt);
            pTmp = fgets(acSaleRec, 1024, fdSale);
            if (!pTmp)
               bEof = true;    // Signal to stop sale update
            else
            {
               iNoMatch++;
               goto GrGr_ReLoad;
            }
         }
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = -1;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename outfile
   LogMsg("Rename output file");
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   if (!_access(acRawFile, 0))
   {
      sprintf(acBuf, acRawTmpl, pCnty, pCnty, "T01");
      if (!_access(acBuf, 0))
         remove(acBuf);
      iTmp = rename(acRawFile, acBuf);
   }
   iTmp = rename(acOutFile, acRawFile);

   LogMsg("Total output records:     %u", lCnt);
   LogMsg("      records updated:    %u", iSaleUpd);
   LogMsg("      skipped records:    %u", iNoMatch);
   LogMsg("Latest recording date:    %u\n", lLastRecDate);
   lRecCnt = lCnt;
   return 0;
}


/**************************** Tuo_ExtrSaleMatched ***************************
 *
 * Extract data to GrGr_Exp.dat or GrGr_Exp.sls.  This output is to be merged
 * into R01 file.  Extract only records with ApnMatch=Y. with known DocType, 
 * and has sale price >= 5000.
 -
 * Convert to new DocNum format YYYYR9999999
 *
 * Input: pMode can be "w" or "a+" (overwrite or append)
 * Output: GrGr_Exp.dat or GrGr_Exp.sls in SALE_REC1 format
 *
 * Return 0 if successful, otherwise error
 *
 ****************************************************************************/

int Tuo_ExtrSaleMatched(char *pGrGrFile, char *pExSaleFile, char *pMode, bool bOwner=false)
{
   char     *pTmp, acGrGrRec[2048], acSaleRec[1024], acTmp[256];
   long     lOut=0, lCnt=0, lPrice, iTmp;

   SCSAL_REC *pSale = (SCSAL_REC *)&acSaleRec[0];
   GRGR_DEF  *pGrGr = (GRGR_DEF *)&acGrGrRec[0];

   LogMsg("Extract matched grgr from %s to %s", pGrGrFile, pExSaleFile);

   // Open output file
   LogMsg("Open output %s in [%s]", pExSaleFile, pMode);
   if (!(fdSale = fopen(pExSaleFile, pMode)))
   {
      LogMsg("***** Error creating %s file", pExSaleFile);
      return -1;
   }

   // Open input file
   LogMsg("Open input %s", pGrGrFile);
   if (!(fdGrGr = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error creating %s file", pGrGrFile);
      return -2;
   }

   // Loop through
   while (!feof(fdGrGr))
   {
      // Get input rec
      pTmp = fgets(acGrGrRec, 2048, fdGrGr);
      if (!pTmp)
         break;         // EOF

      // Drop all record without APN matched
      //if (pGrGr->APN_Matched != 'Y')
      //   continue;

      // Drop records without sale price
      lPrice = atoin(pGrGr->SalePrice, SIZ_GR_SALE);
      if (lPrice < 5000)
         continue;

      memset(acSaleRec, 32, sizeof(SCSAL_REC));

      // Doc type 
      iTmp = findDocType(pGrGr->DocTitle, (IDX_TBL5 *)&TUO_DocTitle[0]);
      if (iTmp >= 0)
      {
         memcpy(pSale->DocType, TUO_DocTitle[iTmp].pCode, TUO_DocTitle[iTmp].iCodeLen);
         pSale->NoneSale_Flg = TUO_DocTitle[iTmp].flag;

         memcpy(pSale->Apn, pGrGr->APN, iApnLen);
         memcpy(pSale->DocDate, pGrGr->DocDate, SALE_SIZ_DOCDATE);
         iTmp = sprintf(acTmp, "%.4sR%.7d", pGrGr->DocDate, atoin(pGrGr->DocNum, 7));
         memcpy(pSale->DocNum, acTmp, iTmp);
         memcpy(pSale->StampAmt, pGrGr->Tax, SIZ_GR_TAX);
         memcpy(pSale->SalePrice, pGrGr->SalePrice, SIZ_GR_SALE);
         pSale->MultiSale_Flg = pGrGr->MultiApn;

         // Get grantors grantees
         memcpy(pSale->Seller1, pGrGr->Grantors[0].Name, SALE_SIZ_SELLER);

         // Copy Owner name
         if (bOwner)
         {
            memcpy(pSale->Name1, pGrGr->Grantees[0].Name, SALE_SIZ_NAME);
            memcpy(pSale->Name2, pGrGr->Grantees[1].Name, SALE_SIZ_NAME);
         }

         pSale->CRLF[0] = '\n';
         pSale->CRLF[1] = 0;
         fputs(acSaleRec, fdSale);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdGrGr);
   fclose(fdSale);

   LogMsg("Number of records processed: %ld", lCnt);
   LogMsg("Number of output records:    %ld", lOut);
   return 0;
}

/******************************* Tuo_MatchRoll ******************************
 *
 * Match GrGr file against R01 and populate APNMatch and Owner match field.
 *
 ****************************************************************************/

int Tuo_MatchRoll(LPCSTR pGrGrFile)
{
   char     acBuf[2048], acRoll[2048], acTmpFile[_MAX_PATH], acTmp[256];
   char     *pTmp;
   FILE     *fdIn, *fdOut;
   GRGR_DEF *pGrGr = (GRGR_DEF *)&acBuf[0];
   int      iRet, iTmp, iNoApn, iApnMatch, iApnUnmatch, iOwnerMatch;
   HANDLE   hRoll;
   DWORD    nBytesRead;
   bool     bEof;

   // Open input file
   if (!(fdIn = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error opening %s", pGrGrFile);
      return -1;
   }

   // Create output temp file
   strcpy(acTmpFile, pGrGrFile);
   pTmp = strrchr(acTmpFile, '.');
   strcpy(pTmp, ".tmp");
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating %s", acTmpFile);
      fclose(fdIn);
      return -2;
   }

   // Open roll file
   sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   if (_access(acTmp, 0))
   {
      sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (_access(acTmp, 0))
      {
         LogMsg("***** Missing input file %s.  Please check!", acTmp);
         fclose(fdIn);
         fclose(fdOut);
         return -3;
      }
   }

   LogMsg("Open input file %s", acTmp);
   hRoll = CreateFile(acTmp, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (hRoll == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening %s in Mon_MatchRoll().", acTmp);
      fclose(fdIn);
      fclose(fdOut);
      return -3;
   }

   // Skip first record
   iRet = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);
   iRet = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);

   // Initialize counters
   iNoApn=iApnMatch=iApnUnmatch=iOwnerMatch = 0;
   bEof = false;

   // Loop through input file
   while (!bEof)
   {
      // Read input record
      if (!(pTmp = fgets(acBuf, 2048, fdIn)))
         break;

      pGrGr->APN_Matched = 'N';
      pGrGr->Owner_Matched = 'N';

      // If no APN, skip
      if (pGrGr->APN[0] == ' ')
      {
         iNoApn++;
         fputs(acBuf, fdOut);    // No APN, output anyway for data entry
         continue;
      }
#ifdef _DEBUG
      //if (!memcmp(pGrGr->APN, "001072004000", 9))
      //   iRet = 0;
#endif
      // Match APN
      do
      {
         // Compare
         //iRet = memcmp(pGrGr->APN, &acRoll[OFF_PREV_APN], iGrGrApnLen);
         iRet = memcmp(pGrGr->APN, acRoll, iApnLen);
         if (!iRet)
         {
            pGrGr->APN_Matched = 'Y';
            iApnMatch++;

            // Match owner - match last name or the first 10 bytes of grantors
            int   iIdx, iLen;
            char  acOwner[64];

            memcpy(acOwner, (char *)&acRoll[OFF_NAME1], SIZ_NAME1);
            acOwner[SIZ_NAME1] = 0;
            if (pTmp = strchr(acOwner, ' '))
            {
               iLen = pTmp - (char *)&acOwner[0];
               if (iLen < 3) iLen = 10;
            }

            for (iIdx = 0; iIdx < atoin(pGrGr->NameCnt, SIZ_GR_NAMECNT); iIdx++)
            {
               // Compare grantors only
               if (!memcmp((char *)&pGrGr->Grantors[iIdx].Name[0], acOwner, iLen))
               {
                  pGrGr->Owner_Matched = 'Y';
                  iOwnerMatch++;
                  break;
               }
            }

            // Populate with roll data - to be done when needed

            break;
         } else
         { 
            if (iRet == 1)
            {
               // Read R01 record
               iTmp = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);
               // Check for EOF
               if (!iTmp)
               {
                  LogMsg("***** Error reading roll file (%f)", GetLastError());
                  bEof = true;
                  break;
               }

               // EOF ?
               if (!nBytesRead)
               {
                  bEof = true;
                  break;
               }
            }
         }
      } while (iRet > 0);
      
      // Output record
      fputs(acBuf, fdOut);
   }

   // Check for leftover
   while (!feof(fdIn))
   {
      // Read input record
      if (!(pTmp = fgets(acBuf, 2048, fdIn)))
         break;

      // If no APN, skip
      if (pGrGr->APN[0] == ' ')
         iNoApn++;

      pGrGr->APN_Matched = 'N';
      pGrGr->Owner_Matched = 'N';

      // Output record
      fputs(acBuf, fdOut);
   }

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);
   if (hRoll)
      CloseHandle(hRoll);

   // If everything OK, rename output file to original file
   if (remove(pGrGrFile))
      LogMsg("***** Error removing temp file: %s (%d)", pGrGrFile, errno);
   else if (rename(acTmpFile, pGrGrFile))
      LogMsg("***** Error renaming temp file: %s --> %s (%d)", acTmpFile, pGrGrFile, errno);

   LogMsg("                     APN matched: %d", iApnMatch);
   LogMsg("                   Owner matched: %d", iOwnerMatch);
   LogMsg("                         No APN : %d", iNoApn);

   return 0;
}

/****************************** Tuo_LoadGrGr1 *******************************
 *
 * 07/01/2024
 *
 ****************************************************************************/

int Tuo_FormatApn(char *pInbuf, char *pOutbuf)
{
   int   iRet;

   iRet = strlen(pInbuf);
   if (iRet == 8 || iRet == 10)  // Old format
      sprintf(pOutbuf, "%.6s0%.2s000", pInbuf, pInbuf+6);
   else if (iRet == 7)
      sprintf(pOutbuf, "0%.5s0%.2s000", pInbuf, pInbuf+5);
   else if (iRet == 11)
   {
      sprintf(pOutbuf, "%.6s0%.2s000", pInbuf, pInbuf+6);
   } else if (iRet == 9)
      sprintf(pOutbuf, "%s000", pInbuf);
   else if (iRet == 12)
      strcpy(pOutbuf, pInbuf);
   else
   {
      *pOutbuf = 0;
      iRet = 0;
   }

   return iRet;
}

int Tuo_LoadGrGr1(char *pInfile, FILE *fdOut)
{
   FILE     *fdIn;
   char     acApn[32], *apApn[100], acInbuf[2048], acOutbuf[2048], acTmp[256], *pRec;
   int      iRet, iCnt=0, iRecCnt=0, iRecSkip=0, iApn, iIdx;
   int		ReadGrGr=0;
   long		lTax, lPrice;

   GRGR_DEF *pGrGr = (GRGR_DEF *)&acOutbuf[0];

   LogMsg("Processing GrGr file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening GrGr file %s", pInfile);
      return -1;
   }

   // Skip header
   pRec = fgets(acInbuf, 2048, fdIn);

   // Processing loop
   while (!feof(fdIn))
   {
      pRec = fgets(acInbuf, 2048, fdIn);
      if (!pRec)
         break;

      iRet = ParseStringNQ(acInbuf, ',', TUO_GR1_FLDS+1, apTokens);
      if (iRet < TUO_GR1_FLDS)
      {
         LogMsg("*** Skip rec #%d", iCnt);
         iRecSkip++;
         continue;
      }
      memset((void *)&acOutbuf, ' ', sizeof(GRGR_DEF));

      strcpy(acTmp, apTokens[TUO_GR1_APN]);
      iRet = strlen(acTmp);
      acApn[0] = 0;
      iIdx=iApn = 0;

ReformatAPN:
      if (iRet <= 12 && iRet >= 7)
         iRet = Tuo_FormatApn(acTmp, acApn);
      else if (iRet > 12)
      {
         iApn = ParseStringNQ(apTokens[TUO_GR1_APN], ',', TUO_GR1_FLDS+1, apApn);
         iIdx = 0;
         if (iApn > 1)
         {
            LogMsg0("*** Multi APN: %s DocNum=%s (%s)", acTmp, apTokens[TUO_GR1_DOCNUM], pInfile);
            strcpy(acTmp, apApn[iIdx++]);
            iRet = strlen(acTmp);
            pGrGr->MultiApn = 'Y';
            goto ReformatAPN;
         } else
         {
            LogMsg0("*** Weird APN: %s DocNum=%s (%s)", acTmp, apTokens[TUO_GR1_DOCNUM], pInfile);
         }
      } else if (iRet > 3)
      {
         LogMsg0("*** Short APN: %s DocNum=%s (%s)", acTmp, apTokens[TUO_GR1_DOCNUM], pInfile);
         sprintf(acApn, "%s000000000", acTmp);
         acApn[12] = 0;
      } else
      {
         LogMsg0("*** BAD APN: %s DocNum=%s (%s)", acTmp, apTokens[TUO_GR1_DOCNUM], pInfile);
         continue;
      }
      vmemcpy(pGrGr->APN, acApn, 12);
      
#ifdef _DEBUG
      //if (!memcmp(apTokens[TUO_GR1_DOCNUM], "2024-008819", 11))
      //   iRet = 0;
#endif
      if (isdigit(acApn[0]) && memcmp(acApn, "0000000000", 9))
      {
         memcpy(pGrGr->Grantors[0].Name, apTokens[TUO_GR1_GRANTOR], strlen(apTokens[TUO_GR1_GRANTOR]));
         memcpy(pGrGr->Grantees[0].Name, apTokens[TUO_GR1_GRANTEE], strlen(apTokens[TUO_GR1_GRANTEE]));
         pGrGr->NameCnt[0] = '1';

         memcpy(pGrGr->DocTitle, apTokens[TUO_GR1_DOCTYPE], strlen(apTokens[TUO_GR1_DOCTYPE]));

         if (strlen(apTokens[TUO_GR1_DOCDATE]) > 8)
         {
            dateConversion(apTokens[TUO_GR1_DOCDATE], acTmp, MM_DD_YYYY_1);
            memcpy(pGrGr->DocDate, acTmp, 8);
            iRet = atol(acTmp);
            if (iRet > lLastRecDate)
               lLastRecDate = iRet;
         } 

         // skip 4 digit year in Doc#
         strcpy(acTmp, apTokens[TUO_GR1_DOCNUM]);
         if (acTmp[4] == '-')
         {
            iRet = atol(&acTmp[5]);
            sprintf(acTmp, "%.7d", iRet);
            memcpy(pGrGr->DocNum, acTmp, 7);
         } else
         {
            if (bDebug)
               LogMsg("*** Bad DocNum: %s", acTmp);
            continue;
         }

         // AW_DTT -> Tax
         double dTax;
         strcpy(acTmp, apTokens[TUO_GR1_DOCTAX]);
         dTax = atof(acTmp);
         lTax = (long)(dTax*100.0);
         lPrice = (long)(dTax*SALE_FACTOR);
         if (lPrice > 0)
         {
            sprintf(acTmp, "%*d", SIZ_GR_TAX, lTax);
            memcpy(pGrGr->Tax, acTmp, SIZ_GR_TAX);

            sprintf(acTmp, "%*d", SIZ_GR_SALE, lPrice);
            memcpy(pGrGr->SalePrice, acTmp, SIZ_GR_SALE);
         }

         pGrGr->CRLF[0] = '\n';
         pGrGr->CRLF[1] = '\0';
         iRet = fputs(acOutbuf, fdOut);
         if (iApn > iIdx)
         {
            while (iApn > iIdx && strlen(apApn[iIdx]) >= 8)
            {
               strcpy(acTmp, apApn[iIdx++]);
               iRet = Tuo_FormatApn(acTmp, acApn);
               vmemcpy(pGrGr->APN, acApn, 12);
               iRet = fputs(acOutbuf, fdOut);
               iRecCnt++;
            }
         } else
            iRecCnt++;
      } else
         iRecSkip++;

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) 
      fclose(fdIn);

   LogMsgD("\nNumber of GrGr record in:    %d", iCnt);
   LogMsgD("Number of GrGr record out:   %d", iRecCnt);
   LogMsgD("Number of GrGr record Skip:  %d\n", iRecSkip);

   return iRecCnt;
}

/****************************** Tuo_LoadGrGrCsv *****************************
 *
 * Remove two digit year from Doc# - spn 05/23/2007
 *
 *
 ****************************************************************************/

int Tuo_LoadGrGrCsv(char *pInfile, FILE *fdOut)
{
   FILE     *fdIn;
   char     acApn[32], acInbuf[2048], acOutbuf[2048], acTmp[256], *pRec, *pTmp;
   int      iRet, iCnt=0, iRecCnt=0, iRecSkip=0;
   int		ReadGrGr=0;
   long		lTax, lPrice;
   bool     bTabIncl = false;

   GRGR_DEF *pGrGr = (GRGR_DEF *)&acOutbuf[0];

   LogMsg("Processing GrGr file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening GrGr file %s", pInfile);
      return -1;
   }

   // Skip header
   pRec = fgets(acInbuf, 2048, fdIn);

   // Check for type of input file
   if (pRec = isTabIncluded(acInbuf))
      bTabIncl = true;

   // Processing loop
   while (!feof(fdIn))
   {
      pRec = fgets(acInbuf, 2048, fdIn);
      if (!pRec)
         break;

      if (bTabIncl)
         iRet = ParseStringIQ(acInbuf, 9, TUO_GR_COMMENTS+1, apTokens);
      else
         iRet = ParseStringNQ(acInbuf, ',', TUO_GR_COMMENTS+1, apTokens);

      if (iRet < TUO_GR_COMMENTS)
      {
         iRecSkip++;
         continue;
      }
      memset((void *)&acOutbuf, ' ', sizeof(GRGR_DEF));

      strcpy(acTmp, apTokens[TUO_GR_APN]);
      iRet = strlen(acTmp);
      acApn[0] = 0;

      ReformatAPN:
      if (iRet == 8 || iRet == 10)  // Old format
         sprintf(acApn, "%.6s0%.2s000", acTmp, &acTmp[6]);
      else if (iRet == 7)
      {
         LogMsg0("*** 7-digit APN: %s DocNum=%s (%s)", acTmp, apTokens[TUO_GR_DOCNUM], pInfile);
         sprintf(acApn, "0%.5s0%.2s000", acTmp, &acTmp[5]);
      } else if (iRet == 11)
      {
         LogMsg0("*** 11-digit APN: %s DocNum=%s (%s)", acTmp, apTokens[TUO_GR_DOCNUM], pInfile);
         sprintf(acApn, "%.6s0%.2s000", acTmp, &acTmp[6]);
      } else if (iRet == 9)           // New format
         sprintf(acApn, "%s000", acTmp);
      else if (iRet == 12)          // New format
         strcpy(acApn, acTmp);
      else if (iRet > 12)
      {
         if ((pTmp = strchr(acTmp, ',')) || (pTmp = strchr(acTmp, '-')) ||
             (pTmp = strstr(acTmp, " THRU")) ||
             (pTmp = strstr(acTmp, " &")))
         {
            LogMsg0("*** Multi APN: %s DocNum=%s (%s)", acTmp, apTokens[TUO_GR_DOCNUM], pInfile);
            *pTmp = 0;
            iRet = strlen(acTmp);
            pGrGr->MultiApn = 'Y';
            goto ReformatAPN;
         } else
         {
            LogMsg0("*** Weird APN: %s DocNum=%s (%s)", acTmp, apTokens[TUO_GR_DOCNUM], pInfile);
         }
      } else if (iRet >3)
      {
         LogMsg0("*** Short APN: %s DocNum=%s (%s)", acTmp, apTokens[TUO_GR_DOCNUM], pInfile);
         sprintf(acApn, "%s000000000", acTmp);
         acApn[12] = 0;
      }
      memcpy(pGrGr->APN, acApn, 12);
      
#ifdef _DEBUG
      //if (!memcmp(acTmp, "08529001", 8))
      //   iRet = 0;
#endif
      if (isdigit(acApn[0]) && memcmp(acApn, "0000000000", 9))
      {
         memcpy(pGrGr->SourceTable, apTokens[TUO_GR_PRDOC], strlen(apTokens[TUO_GR_PRDOC]));

         memcpy(pGrGr->Grantors[0].Name, apTokens[TUO_GR_GRANTOR], strlen(apTokens[TUO_GR_GRANTOR]));
         memcpy(pGrGr->Grantees[0].Name, apTokens[TUO_GR_GRANTEE], strlen(apTokens[TUO_GR_GRANTEE]));
         pGrGr->NameCnt[0] = '1';

         memcpy(pGrGr->DocTitle, apTokens[TUO_GR_DOCTYPE], strlen(apTokens[TUO_GR_DOCTYPE]));

         if (strlen(apTokens[TUO_GR_DOCDATE]) > 8)
         {
            strcpy(acTmp, apTokens[TUO_GR_DOCDATE]);
            remChar(acTmp, '-');		
            memcpy(pGrGr->DocDate, acTmp, 8);
            iRet = atol(acTmp);
            if (iRet > lLastRecDate)
               lLastRecDate = iRet;
         } 

         // skip 4 digit year in Doc#
         if (strlen(apTokens[TUO_GR_DOCNUM]) > 0)
         {
            strcpy(acTmp, apTokens[TUO_GR_DOCNUM]);
            iRet = atol(&acTmp[4]);
            sprintf(acTmp, "%.7d", iRet);
            memcpy(pGrGr->DocNum, acTmp, 7);
         }

         // AW_DTT -> Tax
         double dTax;
         strcpy(acTmp, apTokens[TUO_GR_DOCTAX]);
         dTax = atof(acTmp);
         lTax = (long)(dTax*100.0);
         lPrice = (long)(dTax*SALE_FACTOR);
         if (lPrice > 0)
         {
            sprintf(acTmp, "%*d", SIZ_GR_TAX, lTax);
            memcpy(pGrGr->Tax, acTmp, SIZ_GR_TAX);

            sprintf(acTmp, "%*d", SIZ_GR_SALE, lPrice);
            memcpy(pGrGr->SalePrice, acTmp, SIZ_GR_SALE);
         }

         pGrGr->CRLF[0] = '\n';
         pGrGr->CRLF[1] = '\0';
         iRet = fputs(acOutbuf, fdOut);
         iRecCnt++;
      } else
         iRecSkip++;

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) 
      fclose(fdIn);

   LogMsgD("\nNumber of GrGr record in:    %d", iCnt);
   LogMsgD("Number of GrGr record out:   %d", iRecCnt);
   LogMsgD("Number of GrGr record Skip:  %d\n", iRecSkip);

   return iRecCnt;
}

/********************************* Tuo_LoadGrGr *****************************
 *
 * After processed, the zip file is copied to storage folder if defined and then 
 * moved to backup folder.
 *
 * 1. Process GRGR file
 * 2. Match against roll file, set APN_Match & Owner_Match flags
 * 3. Append to cummulative file Tuo_GrGr.sls
 * 4. Resort and extract to GrGr_Exp.dat for import. The output is used to merge
 *    into R01 via MergeGrGrFile().
 *
 * If successful, return 0.  Otherwise error.
 *
 * Output GrGr_Exp.dat and TUO_GrGr.sls
 *
 ****************************************************************************/

int Tuo_LoadGrGr(LPCSTR pCnty) 
{
   char     *pTmp;
   char     acTmp[256];
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH], acGrGrCopy[_MAX_PATH];
   char     acCsvFile[64], acGrGrFile[_MAX_PATH], acZipFile[_MAX_PATH], acUnzipFolder[_MAX_PATH];
   struct   _finddata_t  sFileInfo;

   FILE     *fdOut;

   int      iCnt, iRet;
   long     lCnt, lHandle;

   // GrGr file we want to process in zip file
   GetIniString(pCnty, "GrGrCsv", "TCROR.HFW", acCsvFile, _MAX_PATH, acIniFile);

   // Prepare backup folder
   sprintf(acGrGrBak, acGrGrBakTmpl, pCnty, acToday);
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   // Get copy to folder
   GetIniString(pCnty, "GrGrCopy", "", acGrGrCopy, _MAX_PATH, acIniFile);

   // Form GrGr input path
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   if (strstr(acGrGrIn, "$date$"))
      replStr(acGrGrIn, "$date$", acToday);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
   } else
   {
      LogMsg("*** No new file avail for processing: %s", acGrGrIn);
      return -1;
   }

   // Create Output file - Tuo_GrGr.dat
   sprintf(acGrGrOut, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      _findclose(lHandle);
      return -2;
   }

   // Initialize Zip server
   doZipInit();

   // Set unzip folder
   GetIniString(myCounty.acCntyCode, "UnzipFolder", "", acUnzipFolder, _MAX_PATH, acIniFile);
   if (!acUnzipFolder[0])
      sprintf(acUnzipFolder, "%s\\%s", acTmpPath, pCnty);
   setUnzipToFolder(acUnzipFolder);

   // Set to replace file if exist
   setReplaceIfExist(true);

   lLastRecDate = iCnt = lCnt = iRet = 0;
   while (!iRet)
   {
      //sprintf(acCsvFile, "TCROR.HFW");
      sprintf(acZipFile, "%s\\%s", acGrGrIn, sFileInfo.name);

      // Unzip input file
      LogMsg("Unzip input file %s", acZipFile);
      iRet = startUnzip(acZipFile, acCsvFile);
      if (!iRet)
      {
         // Rename the unzipped file - getting file date from zip file name
         strcpy(acTmp, sFileInfo.name);
         if (pTmp = strrchr(acTmp, '.'))
            *pTmp = 0;
         sprintf(acGrGrFile, "%s\\%.5s%s.HFW", acUnzipFolder, acCsvFile, acTmp);
         sprintf(acTmp, "%s\\%s", acUnzipFolder, acCsvFile);

         // Update GrGr file date
         iRet = getFileDate(acTmp);
         if (iRet > lLastGrGrDate)
            lLastGrGrDate = iRet;

         if (!_access(acGrGrFile, 0))
            remove(acGrGrFile);
         rename(acTmp, acGrGrFile);

         // Load GrGr file
         iRet = Tuo_LoadGrGrCsv(acGrGrFile, fdOut);
         if (iRet < 0)
            LogMsg("*** Skip %s", acGrGrFile);
         else
            lCnt += iRet;

         // Copy file to storage
         if (acGrGrCopy[0] > 'A')
         {
            BOOL bTmp;
            sprintf(acTmp, "%s\\%s", acGrGrCopy, sFileInfo.name);
            bTmp = CopyFile(acZipFile, acTmp, true);
         }

         // Move input file to backup folder
         sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
         rename(acZipFile, acTmp);

         iCnt++;
      } else
         LogMsg("*** Skip %s", acZipFile);

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }


   // Close handle
   _findclose(lHandle);
   iRet = 0;

   // Close files
   if (fdOut)
      fclose(fdOut);

   // Shut down Zip server
   doZipShutdown();

   LogMsg("Total processed records  : %u", lCnt);

   // Sort output
   if (lCnt > 0)
   {
      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, RecDate asc, DocNum asc, Data source
      sprintf(acTmp,"S(17,13,C,A,37,8,C,A,1,10,C,A,1109,2,C,A) F(TXT) DUPO(B%d, 1,44) ", sizeof(GRGR_DEF)+64);
      strcpy(acGrGrIn, acGrGrOut);
      sprintf(acGrGrOut, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");

      // Sort Tuo_GrGr.dat to Tuo_GrGr.srt
      lCnt = sortFile(acGrGrIn, acGrGrOut, acTmp);

      // Match with roll file
      iRet = Tuo_MatchRoll(acGrGrOut);

      // Update cumulative sale file
      if (lCnt > 0)
      {
         char  acSlsFile[_MAX_PATH];

         // Append Tuo_GrGr.srt to Tuo_GrGr.sls for backup
         sprintf(acSlsFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
         iRet = appendTxtFile(acGrGrOut, acSlsFile);

         // Resort SLS file
         lCnt = sortFile(acSlsFile, acGrGrOut, acTmp);
         if (lCnt > 0)
         {
            remove(acSlsFile);
            rename(acGrGrOut, acSlsFile);
         }

         // Extract Tuo_GrGr.sls to GrGr_Exp.dat 
         //sprintf(acTmp, acEGrGrTmpl, myCounty.acCntyCode, "dat");
         //iRet = Tuo_ExtrSaleMatched(acSlsFile, acTmp, "w", false);    
      } else
         iRet = 1;
   } else
      iRet = 1;

   LogMsgD("\nTotal output records after dedup: %u", lCnt);
   LogMsg("              Last recording date: %d.", lLastRecDate);

   return iRet;
}

int Tuo_LoadGrGrNoZip(LPCSTR pCnty) 
{
   char     *pTmp;
   char     acTmp[256];
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   char     acCsvFile[_MAX_PATH], acToday[16];
   struct   _finddata_t  sFileInfo;

   FILE     *fdOut;

   int      iCnt, iRet;
   long     lCnt, lHandle;

   // Get raw file name
   GetIniString("System", "GrGrBak", "", acTmp, _MAX_PATH, acIniFile);
   iRet = sprintf(acToday, "%d", lToday);
   if (iRet > 1)
   {
      sprintf(acGrGrBak, acTmp, pCnty, acToday);
   } else
   {
      strcpy(acGrGrBak, acGrGrIn);
      pTmp = strrchr(acGrGrBak, '\\');
      if (pTmp)
         *++pTmp = 0;
      sprintf(acTmp, "GrGr_%s", acToday);
      strcat(acGrGrBak, acTmp);
   }

   // Form GrGr input path
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   if (strstr(acGrGrIn, "$date$"))
      replStr(acGrGrIn, "$date$", acToday);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;

      // Create backup folder if not exist
      if (_access(acGrGrBak, 0))
         _mkdir(acGrGrBak);
      LogMsg("Set GRGR Backup folder: %s", acGrGrBak);
   } else
   {
      LogMsg("*** No new file avail for processing: %s", acGrGrIn);
      return 0;
   }

   // Create Output file - Tuo_GrGr.dat
   sprintf(acGrGrOut, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      _findclose(lHandle);
      return -2;
   }

   iCnt = lCnt = 0;
   while (!iRet)
   {
      sprintf(acCsvFile, "%s\\%s", acGrGrIn, sFileInfo.name);

      // Parse input file
      //iRet = Tuo_LoadGrGrCsv(acCsvFile, fdOut);
      iRet = Tuo_LoadGrGr1(acCsvFile, fdOut);
      if (iRet < 0)
         LogMsg("*** Skip %s", acCsvFile);
      else
         lCnt += iRet;

      // Move input file to backup folder
      sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
      iRet = rename(acCsvFile, acTmp);
      if (iRet)
         LogMsg("***** ERROR: Renaming failed: %s to %s (%d)", acCsvFile, acTmp, _errno);

      iCnt++;
      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }

   // Close handle
   _findclose(lHandle);
   iRet = 0;

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total processed records  : %u", lCnt);

   // Sort output
   if (lCnt > 0)
   {
      char sSortCmd[256];

      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, RecDate asc, DocNum asc, Data source
      sprintf(acTmp, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
      sprintf(sSortCmd,"S(17,13,C,A,37,8,C,A,1,10,C,A,1109,2,C,A) F(TXT) DUPO(B%d, 1,44) ", sizeof(GRGR_DEF)+64);
      sprintf(acGrGrIn, "%s+%s", acGrGrOut, acTmp);
      sprintf(acGrGrOut, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");

      // Sort Tuo_GrGr.dat to Tuo_GrGr.srt
      lCnt = sortFile(acGrGrIn, acGrGrOut, sSortCmd);

      // Match with roll file
      iRet = Tuo_MatchRoll(acGrGrOut);

      // Update cumulative sale file
      if (lCnt > 0)
      {
          // Rename Tuo_GrGr.srt to Tuo_GrGr.sls 
          if (!_access(acTmp, 0))
             remove(acTmp);
          iRet = rename(acGrGrOut, acTmp);
      } else
         iRet = 1;
   } else
      iRet = 1;

   LogMsg("Total output records after dedup: %u", lCnt);
   printf("\nTotal output records after dedup: %u\n", lCnt);

   return iRet;
}

/*********************************** loadTuo ********************************
 *
 * Options:
 *
 *    -CTUO -U -Xsi -T [-Xa] -O   (Normal update)
 *    -CTUO -L -Xl -Ms|Xsi [-Xa] [-Up] -O (LDR)
 *
 ****************************************************************************/

int loadTuo(int iSkip)
{
   int   iRet=0;
   char  sInfile[_MAX_PATH], sOutfile[_MAX_PATH];

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;
   iApnLen = myCounty.iApnLen;

   //sprintf(sInfile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acCntyCode);
   //sprintf(sOutfile, acLienTmpl, myCounty.acCntyCode, "New");
   //iRet = Tuo_ConvertApn(sInfile, sOutfile);
   //sprintf(sInfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   //sprintf(sOutfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   //iRet = Tuo_ConvertApnRoll(sInfile, sOutfile, 1900);
   //sprintf(sInfile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Org");
   //strcpy(sInfile, "H:\\CO_PROD\\STUO_CD\\raw\\TUO_Sale_2016.sls");
   //sprintf(sOutfile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   //iRet = Tuo_ConvertApnSale(sInfile, sOutfile);

   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      // Load tax base
      iRet = MB_Load_TaxBase(bTaxImport, true, 2, 5);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet = MB_Load_TaxCodeMstr(bTaxImport, 5);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport, 2);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport, 5);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
      if (!iLoadFlag)
         return iRet;
   }

   // Extract lien file
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", sInfile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(sInfile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, sInfile, 0);      // 2016
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode);
   }

   // Load Value file - from AMA
   if (iLoadFlag & EXTR_VALUE)                     // -Xv
   {
      char sDbName[64], sBYVFile[_MAX_PATH], sCVFile[_MAX_PATH], acTmp[256];

      sprintf(sBYVFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Value");
      sprintf(acTmp, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = MB_ExtrValues(myCounty.acCntyCode, acValueFile, acExeFile, acTmp, sBYVFile, iHdrRows);
      
      // Reset reason and VST
      if (iRet > 0)
      {
         sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Vmp");
         iRet = MB_ResetPVReason(sBYVFile, acTmp);
      }
      if (iRet > 0)
      {
         sprintf(sDbName, "LDR%d", lLienYear);
         GetIniString("Data", "SqlValueFile", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(sCVFile, acTmp, sDbName, myCounty.acCntyCode);
         lRecCnt = createValueImport(sBYVFile, sCVFile, false);
         if (lRecCnt > 0)
         {
            // Save output file for import
            strcpy(acValueFile, sCVFile);
            iRet = 0;
         } else
            iLoadFlag ^= EXTR_IVAL;      // Turn off import
      }
   }

   if (bUpdPrevApn)                                // -Up
   {
      // If not defined, use current apn length
      iRet = GetPrivateProfileInt(myCounty.acCntyCode, "PrevApnLen", iApnLen, acIniFile);
      iRet = updatePrevApn(myCounty.acCntyCode, iRet, iSkip);
   }

   // Extract Sale file from Tuo_Sales.txt to Tuo_Sale.sls
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Do not use confirmed sale price
      //bUseConfSalePrice = false;

      // Doc date - input format MM/DD/YYYY
      if (iSaleDateFmt > 0)
         iRet = Tuo_CreateSCSale(iSaleDateFmt, 0, 6, true, &TUO_DocCode[0]);
      else
         iRet = Tuo_CreateSCSale(MM_DD_YYYY_1, 0, 6, true, &TUO_DocCode[0]);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Check file size, skip if it is too small
      if ((iRet = getFileSize(acCharFile)) > 100000)
      {
         if (strstr(acCharFile, "phys"))
            iRet = Tuo_Phys2StdChar(acCharFile);
         else
            iRet = Tuo_ConvStdChar(acCharFile);
         if (iRet <= 0)
         {
            LogMsg("***** Error extracting attributes data from %s", acCharFile);
            return -1;
         }
      } else if (iRet < 0)
         LogMsg0("***** Missing CHAR file %s.  Old file will be used.", acCharFile);
      else
         LogMsg0("***** Bad CHAR file %s (size=%d).  Please check this out.", acCharFile, iRet);
   }

   // Load GrGr data
   if (iLoadFlag & LOAD_GRGR)                      // -G
   {
      // Create Tuo_GrGr.dat, Tuo_GrGr.sls, Sale_Exp.dat and append to Tuo_sale_cum.dat
      LogMsg0("Load %s GrGr file", myCounty.acCntyCode);

      GetIniString(myCounty.acCntyCode, "GrGrIn", "", sInfile, _MAX_PATH, acIniFile);
      if (strstr(sInfile, ".zip") )
      {
         iRet = Tuo_LoadGrGr(myCounty.acCntyCode);
         if (iRet < 0 && bSendMail)
         {
            bGrGrAvail = false;
            if (bSendMail &&  !(iLoadFlag & (LOAD_LIEN|LOAD_UPDT)))
               return iRet;
         }
      } else
         // Use following function to process GrGr not in the zip file
         iRet = Tuo_LoadGrGrNoZip(myCounty.acCntyCode);

      if (!iRet && !(iLoadFlag & LOAD_LIEN))
         iLoadFlag |= MERG_GRGR;       // Trigger merge grgr
      iRet = 0;
   }

   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      if (iLoadFlag & LOAD_LIEN)
      {
         // Create Lien file
         LogMsg0("Load %s Lien file", myCounty.acCntyCode);
         iRet = Tuo_Load_LDR(iSkip);
      } else if (iLoadFlag & LOAD_UPDT)
      {
         LogMsg0("Load %s roll update file", myCounty.acCntyCode);
         iRet = Tuo_Load_Roll(iSkip);
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Tuo_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_UPD_SALE);
      iLoadFlag |= LOAD_UPDT;
   }

   // Merge sale & GrGr data
   if (!iRet && (iLoadFlag & MERG_GRGR) )          // -Mg
   {
      // Extract Tuo_GrGr.sls to GrGr_Exp.dat 
      sprintf(sInfile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
      sprintf(sOutfile, acEGrGrTmpl, myCounty.acCntyCode, "DAT");
      iRet = Tuo_ExtrSaleMatched(sInfile, sOutfile, "w");

      // Merge sale data (GrGr_Exp.dat).  
      iRet = Tuo_MergeGrGrFile(myCounty.acCntyCode, true, false, false);
      if (!iRet)
      {
         iLoadFlag |= LOAD_UPDT;
      }
   }

   // Format DocLinks - Doclinks are concatenate fields separated by comma 
   if (!iRet && (iLoadFlag & (LOAD_UPDT|LOAD_LIEN|MERG_GRGR)) )
      iRet = updateDocLinks(Tuo_MakeDocLink, myCounty.acCntyCode, iSkip, 0);

   return iRet;
}
