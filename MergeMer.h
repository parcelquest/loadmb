#ifndef  _MERGEMER_H_
#define  _MERGEMER_H_

// MER 2016 - Value file definition
//#define V_ASMT                         0
//#define V_TAXYEAR                      1
//#define V_ROLLCHGNUM                   2
//#define V_CURRENTMARKETLANDVALUE       3
//#define V_CURRENTFIXEDIMPRVALUE        4
//#define V_CURRENTGROWINGIMPRVALUE      5
//#define V_CURRENTSTRUCTURALIMPRVALUE   6
//#define V_CURRENTPERSONALPROPVALUE     7
//#define V_CURRENTPERSONALPROPMHVALUE   8
//#define V_CURRENTEXEMPTIONCODE1        9
//#define V_CURRENTEXEMPTIONAMT1         10
//#define V_CURRENTEXEMPTIONCODE2        11
//#define V_CURRENTEXEMPTIONAMT2         12
//#define V_CURRENTEXEMPTIONCODE3        13
//#define V_CURRENTEXEMPTIONAMT3         14
//#define V_CURRENTNETVALUE              15
//#define V_BILLEDMARKETLANDVALUE        16
//#define V_BILLEDFIXEDIMPRVALUE         17
//#define V_BILLEDGROWINGIMPRVALUE       18
//#define V_BILLEDSTRUCTURALIMPRVALUE    19
//#define V_BILLEDPERSONALPROPVALUE      20
//#define V_BILLEDPERSONALPROPMHVALUE    21
//#define V_BILLEDEXEMPTIONCODE1         22
//#define V_BILLEDEXEMPTIONAMT1          23
//#define V_BILLEDEXEMPTIONCODE2         24
//#define V_BILLEDEXEMPTIONAMT2          25
//#define V_BILLEDEXEMPTIONCODE3         26
//#define V_BILLEDEXEMPTIONAMT3          27
//#define V_BILLEDNETVALUE               28
//#define V_PRIORMARKETLANDVALUE         29
//#define V_PRIORFIXEDIMPRVALUE          30
//#define V_PRIORGROWINGIMPRVALUE        31
//#define V_PRIORSTRUCTURALIMPRVALUE     32
//#define V_PRIORPERSONALPROPVALUE       33
//#define V_PRIORPERSONALPROPMHVALUE     34
//#define V_PRIOREXEMPTIONCODE1          35
//#define V_PRIOREXEMPTIONAMT1           36
//#define V_PRIOREXEMPTIONCODE2          37
//#define V_PRIOREXEMPTIONAMT2           38
//#define V_PRIOREXEMPTIONCODE3          39
//#define V_PRIOREXEMPTIONAMT3           40
//#define V_PRIORNETVALUE                41

#define  MER_CHAR_FEEPARCEL            0
#define  MER_CHAR_POOLSPA              1
#define  MER_CHAR_CATTYPE              2
#define  MER_CHAR_QUALITYCLASS         3
#define  MER_CHAR_YRBLT                4
#define  MER_CHAR_BUILDINGSIZE         5
#define  MER_CHAR_ATTACHGARAGESF       6
#define  MER_CHAR_DETACHGARAGESF       7
#define  MER_CHAR_CARPORTSF            8
#define  MER_CHAR_HEATING              9
#define  MER_CHAR_COOLINGCENTRALAC     10
#define  MER_CHAR_COOLINGEVAPORATIVE   11
#define  MER_CHAR_COOLINGROOMWALL      12
#define  MER_CHAR_COOLINGWINDOW        13
#define  MER_CHAR_STORIESCNT           14
#define  MER_CHAR_UNITSCNT             15
#define  MER_CHAR_TOTALROOMS           16
#define  MER_CHAR_EFFYR                17
#define  MER_CHAR_PATIOSF              18
#define  MER_CHAR_BEDROOMS             19
#define  MER_CHAR_BATHROOMS            20
#define  MER_CHAR_HALFBATHS            21
#define  MER_CHAR_FIREPLACE            22
#define  MER_CHAR_ASMT                 23
#define  MER_CHAR_BLDGSEQNUM           24
#define  MER_CHAR_HASWELL              25

/* Not used
#define  MER_CHAR_FEEPRCL                 0
#define  MER_CHAR_DOCNUM                  1
#define  MER_CHAR_CATTYPE                 2
#define  MER_CHAR_BLDGSEQNO               3
#define  MER_CHAR_UNITSEQNO               4
#define  MER_CHAR_YRBLT                   5
#define  MER_CHAR_BUILDINGTYPE            6
#define  MER_CHAR_EFFYR                   7
#define  MER_CHAR_BUILDINGSIZE            8
#define  MER_CHAR_BUILDINGUSEDFOR         9
#define  MER_CHAR_STORIESCNT              10
#define  MER_CHAR_UNITSCNT                11
#define  MER_CHAR_CONDITION               12
#define  MER_CHAR_BEDROOMS                13
#define  MER_CHAR_BATHROOMS               14
#define  MER_CHAR_QUALITYCLASS            15
#define  MER_CHAR_HALFBATHS               16
#define  MER_CHAR_CONSTRUCTION            17
#define  MER_CHAR_FOUNDATION              18
#define  MER_CHAR_STRUCTURALFRAME         19
#define  MER_CHAR_STRUCTURALFLOOR         20
#define  MER_CHAR_EXTERIORTYPE            21
#define  MER_CHAR_ROOFCOVER               22
#define  MER_CHAR_ROOFTYPEFLAT            23
#define  MER_CHAR_ROOFTYPEHIP             24
#define  MER_CHAR_ROOFTYPEGABLE           25
#define  MER_CHAR_ROOFTYPESHED            26
#define  MER_CHAR_INSULATIONCEILINGS      27
#define  MER_CHAR_INSULATIONWALLS         28
#define  MER_CHAR_INSULATIONFLOORS        29
#define  MER_CHAR_WINDOWPANESINGLE        30
#define  MER_CHAR_WINDOWPANEDOUBLE        31
#define  MER_CHAR_WINDOWPANETRIPLE        32
#define  MER_CHAR_WINDOWTYPE              33
#define  MER_CHAR_LIGHTING                34
#define  MER_CHAR_COOLINGCENTRALAC        35
#define  MER_CHAR_COOLINGEVAPORATIVE      36
#define  MER_CHAR_COOLINGROOMWALL         37
#define  MER_CHAR_COOLINGWINDOW           38
#define  MER_CHAR_HEATING                 39
#define  MER_CHAR_FIREPLACE               40
#define  MER_CHAR_GARAGE                  41
#define  MER_CHAR_PLUMBING                42
#define  MER_CHAR_SOLAR                   43
#define  MER_CHAR_BUILDER                 44
#define  MER_CHAR_BLDGDESIGNEDFOR         45
#define  MER_CHAR_MODELDESC               46
#define  MER_CHAR_UNFINAREASSF            47
#define  MER_CHAR_ATTACHGARAGESF          48
#define  MER_CHAR_DETACHGARAGESF          49
#define  MER_CHAR_CARPORTSF               50
#define  MER_CHAR_DECKSSF                 51
#define  MER_CHAR_PATIOSF                 52
#define  MER_CHAR_CEILINGHEIGHT           53
#define  MER_CHAR_FIRESPINKLERS           54
#define  MER_CHAR_AVGWALLHEIGHT           55
#define  MER_CHAR_BAY                     56
#define  MER_CHAR_DOCK                    57
#define  MER_CHAR_ELEVATOR                58
#define  MER_CHAR_ESCALATOR               59
#define  MER_CHAR_ROLLUPDOOR              60
#define  MER_CHAR_FIELD1                  61
#define  MER_CHAR_FIELD2                  62
#define  MER_CHAR_FIELD3                  63
#define  MER_CHAR_FIELD4                  64
#define  MER_CHAR_FIELD5                  65
#define  MER_CHAR_FIELD6                  66
#define  MER_CHAR_FIELD7                  67
#define  MER_CHAR_FIELD8                  68
#define  MER_CHAR_FIELD9                  69
#define  MER_CHAR_FIELD10                 70
#define  MER_CHAR_FIELD11                 71
#define  MER_CHAR_FIELD12                 72
#define  MER_CHAR_FIELD13                 73
#define  MER_CHAR_FIELD14                 74
#define  MER_CHAR_FIELD15                 75
#define  MER_CHAR_FIELD16                 76
#define  MER_CHAR_FIELD17                 77
#define  MER_CHAR_FIELD18                 78
#define  MER_CHAR_FIELD19                 79
#define  MER_CHAR_FIELD20                 80
#define  MER_CHAR_LANDFIELD1              81
#define  MER_CHAR_LANDFIELD2              82
#define  MER_CHAR_LANDFIELD3              83
#define  MER_CHAR_LANDFIELD4              84
#define  MER_CHAR_LANDFIELD5              85
#define  MER_CHAR_LANDFIELD6              86
#define  MER_CHAR_LANDFIELD7              87
#define  MER_CHAR_LANDFIELD8              88
#define  MER_CHAR_ACRES                   89
#define  MER_CHAR_NEIGHBORHOODCODE        90
#define  MER_CHAR_ZONING                  91
#define  MER_CHAR_TOPOGRAPHY              92
#define  MER_CHAR_VIEWCODE                93
#define  MER_CHAR_POOLSPA                 94
#define  MER_CHAR_WATERSOURCE             95
#define  MER_CHAR_SUBDIVNAME              96
#define  MER_CHAR_SEWERCODE               97
#define  MER_CHAR_UTILITIESCODE           98
#define  MER_CHAR_ACCESSCODE              99
#define  MER_CHAR_LANDSCAPE               100
#define  MER_CHAR_PROBLEMCODE             101
#define  MER_CHAR_FRONTAGE                102
#define  MER_CHAR_LOCATION                103
#define  MER_CHAR_PLANTEDACRES            104
#define  MER_CHAR_ACRESUNUSEABLE          105
#define  MER_CHAR_WATERSOURCEDOMESTIC     106
#define  MER_CHAR_WATERSOURCEIRRIGATION   107
#define  MER_CHAR_HOMESITES               108
#define  MER_CHAR_PROPERTYCONDITIONCODE   109
#define  MER_CHAR_ROADTYPE                110
#define  MER_CHAR_HASWELL                 111
#define  MER_CHAR_HASCOUNTYROAD           112
#define  MER_CHAR_HASVINEYARD             113
#define  MER_CHAR_ISUNSECUREDBUILDING     114
#define  MER_CHAR_HASORCHARD              115
#define  MER_CHAR_HASGROWINGIMPRV         116
#define  MER_CHAR_SITECOVERAGE            117
#define  MER_CHAR_PARKINGSPACES           118
#define  MER_CHAR_EXCESSLANDSF            119
#define  MER_CHAR_FRONTFOOTAGESF          120
#define  MER_CHAR_MULTIPARCELECON         121
#define  MER_CHAR_LANDUSECODE1            122
#define  MER_CHAR_LANDUSECODE2            123
#define  MER_CHAR_LANDSQFT                124
#define  MER_CHAR_TOTALROOMS              125
#define  MER_CHAR_NETLEASABLESF           126
#define  MER_CHAR_BLDGFOOTPRINTSF         127
#define  MER_CHAR_OFFICESPACESF           128
#define  MER_CHAR_NONCONDITIONSF          129
#define  MER_CHAR_MEZZANINESF             130
#define  MER_CHAR_PERIMETERLF             131
#define  MER_CHAR_ASMT                    132
#define  MER_CHAR_ASMTCATEGORY            133
#define  MER_CHAR_EVENTDATE               134
#define  MER_CHAR_SALESPRICE              135      // Confirmed
#define  MER_CHAR_CONFIRMATIONCODE        136
*/

static XLAT_CODE  asHeating[] = 
{
   // Value, lookup code, value length
   "B",  "F", 1,               // Baseboard
   "C",  "Z", 1,               // Central
   "E",  "F", 1,               // Electric Baseboard
   "F",  "C", 1,               // Floor Furnace
   "N",  "L", 1,               // None
   "P",  "Y", 1,               // Heated
   "W",  "D", 1,               // Wall Furnace
   "Y",  "Z", 1,               // Central
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "0",  "N", 1,               // None
   "1",  "P", 1,               // Pool
   "2",  "P", 1,               // Pool
   "3",  "P", 1,               // Pool
   "99", "P", 2,               // ?
   "DB", "J", 2,               // Doughboy
   "FG", "F", 2,               // Fiberglass
   "GU", "G", 2,               // Gunite
   "P&S","C", 3,               // Gunite
   "VI", "V", 2,               // Vinyl
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "1",  "1", 1,               // 1 fp
   "2",  "2", 1,               // 2 fl
   "3",  "3", 1,               // 3+
   "99", "O", 2,               // OTHER
   "N",  "N", 1,               // No
   "Y",  "Y", 1,               // Yes
   "",   "",  0
};

static XLAT_CODE  asView[] =
{
   "09","Y", 2,               // VIEW OF OWN PROPERTY
   "1", "Y", 1,               // View Code
   "2", "N", 1,               // NO VIEW
   "3", "1", 1,               // MINIMAL VIEW
   "4", "4", 1,               // GOOD-LOCAL AREA
   "5", "M", 1,               // Golfcourse
   "6", "R", 1,               // VALLEY VIEW
   "7", "T", 1,               // MOUNTAIN VIEW
   "8", "O", 1,               // PANORAMIC VIEW
   "9", "Y", 1,               // VIEW OF OWN PROPERTY
   "11","Y", 2,               // FILTERED
   "12","Y", 2,               // POSSIBLE
   "",  "",  0
};

static XLAT_CODE  asSewer[] =
{
   // Value, lookup code, value length
   "01", "S", 2,               // SEPTIC TANK
   "02", "L", 2,               // COMM LEACHFIELD
   "03", "A", 2,               // COMMUNITY SEWER
   "04", "S", 2,               // ENG SEPTIC
   "05", "S", 2,               // ALT. SEPTIC
   "09", "X", 2,               // NEEDS SEPTIC
   "17", "U", 2,               // UNKNOWN
   "19", "Y", 2,               // OTHER
   "99", "Y", 2,               // OTHER
   "",   "",  0
};

static XLAT_CODE  asCond[] =
{
   // Value, lookup code, value length
   "AV", "A", 2,               // Average
   "E",  "E", 1,               // Excellent
   "F",  "F", 1,               // Fair
   "G",  "G", 1,               // Good
   "P",  "P", 1,               // Poor
   "",   "",  0
};

static XLAT_CODE  asWaterSrc[] = 
{
   // Value, lookup code, value length
   "01", "P", 2,               // Community
   "02", "W", 2,               // Well
   "03", "S", 2,               // Spring
   "04", "Y", 2,               // Other
   "05", "R", 2,               // Shared
   "09", "F", 2,               // Canal
   "99", "Y", 2,               // Rural land water (irrigation water)
   "",   "",  0
};

IDX_TBL5 MER_DocCode[] =
{// DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN
   "01","1 ",'N',2,2,  // Sale-Reappraise
   "02","75",'Y',2,2,  // Transfer-Reappraise (can be trustees sale and sale price is not FV)
   "03","1 ",'N',2,2,  // Sale-No Reappraisal
   "04","75",'Y',2,2,  // Transfer-No Reappraisal
   "05","74",'Y',2,2,  // New Mobile Home
   "08","13",'N',2,2,  // Transfer prior year-Reappraise
   "09","19",'Y',2,2,  // Split/Combo
   "10","13",'N',2,2,  // Split/Combo w Sale - Reappraise
   "11","75",'Y',2,2,  // Split/Combo w Transfer - No Reappraise
   "14","74",'Y',2,2,  // New Garage/Carport
   "19","13",'N',2,2,  // Sale - Price not market
   "20","13",'N',2,2,  // Sale - House damaged
   "21","13",'N',2,2,  // Sale - House Upgraded
   "22","13",'Y',2,2,  // Partial Transfer-Reappraise
   "32","13",'Y',2,2,  // Partial Transfer-No Reappraise
   "33","74",'Y',2,2,  // New rural Bldg
   "52","74",'Y',2,2,  // Misc Comm
   "58","74",'Y',2,2,  // Prop 58-Non Reappraisal
   "60","74",'Y',2,2,  // Prop 60-Replacement Dwelling
   "66","74",'Y',2,2,  // PI Lien Update
   "70","74",'Y',2,2,  // Lien Date Update
   "81","13",'Y',2,2,  // Govt Land Outside
   "92","74",'Y',2,2,  // Prop 58 excl
   "93","74",'Y',2,2,  // Prop 60 Excl
   "96","74",'Y',2,2,  // Demo/Removal
   "99","74",'Y',2,2,  // Format Record-End Of Doc Codes
   "","",0,0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 MER_Exemption[] = 
{
   "E01", "H", 3,1,     // HOMEOWNERS EXEMPTION
   "E02", "V", 3,1,     // CHURCH EXEMPTION
   "E03", "D", 3,1,     // WELFARE - OTHER RELIGIOUS
   "E04", "D", 3,1,     // MUSEUM EXEMPTION
   "E05", "X", 3,1,     // WELFARE - PRIVATE & PAROCHIAL SCHOOLS
   "E06", "W", 3,1,     // WELFARE - HOSPITALS
   "E07", "X", 3,1,     // DUCK CLUB
   "E08", "X", 3,1,     // DISABLED VETERAN
   "E09", "C", 3,1,     // HISTORICAL AIRCRAFT
   "E10", "R", 3,1,     // LIBRARY EXEMPTION
   "E11", "U", 3,1,     // LESSORS FORM 263
   "E12", "U", 3,1,     // SCHOOL EXEMPTION
   "E13", "U", 3,1,     // OTHER VETS PROPERTY
   "E14", "U", 3,1,     // CEMETERY FORM 265
   "E15", "P", 3,1,     // COLLEGES (PRIVATELY OWNED)
   "E16", "P", 3,1,     // DISABLED VETERAN
   "E18", "M", 3,1,     // DISABLED VETERAN - LOW INCOME
   "E20", "P", 3,1,     // CHURCH - PARTIAL LAND
   "E21", "P", 3,1,     // CHURCH - PARTIAL STRUCTURE
   "E22", "P", 3,1,     // CHURCH - PARTIAL
   "E28", "X", 3,1,     // DISABLED VETERAN
   "E30", "E", 3,1,     // RELIGIOUS FORM 267S
   "E32", "X", 3,1,     // RELIGIOUS - GROUP A
   "E33", "E", 3,1,     // RELIGIOUS - GROUP B
   "E34", "E", 3,1,     // RELIGIOUS - GROUP B
   "E35", "E", 3,1,     // RELIGIOUS - PARTIAL LAND
   "E36", "W", 3,1,     // RELIGIOUS - PARTIAL STRUCTURE
   "E37", "W", 3,1,     // RELIGIOUS - PARTIAL
   "E42", "W", 3,1,     // WELFARE - OTHER RELIGIOUS
   "E43", "X", 3,1,     // WELFARE - OTHER RELIGIOUS
   "E44", "X", 3,1,     // WELFARE - OTHER RELIGIOUS
   "E50", "X", 3,1,     // LATE FILING R & T CODE
   "E52", "X", 3,1,     // WELFARE - PRIVATE & PAROCHIAL SCHOOLS
   "E53", "X", 3,1,     // WELFARE - PRIVATE & PAROCHIAL SCHOOLS
   "E54", "X", 3,1,     // WELFARE - PRIVATE & PAROCHIAL SCHOOLS
   "E62", "C", 3,1,     // WELFARE - HOSPITALS
   "E63", "C", 3,1,     // WELFARE - HOSPITALS
   "E64", "C", 3,1,     // WELFARE - HOSPITALS
   "E75", "W", 3,1,     // WELFARE LOW-INCOME BUILDERS-BOE 231-AH
   "E76", "X", 3,1,     // ?
   "E77", "W", 3,1,     // WELFARE GROUP A  AUDIT YRS 19, 22, 25, 28
   "E78", "X", 3,1,     // ?
   "E79", "W", 3,1,     // WELFARE GROUP B AUDIT YRS 20, 23, 26, 29
   "E80", "X", 3,1,     // ?
   "E81", "X", 3,1,     // ?
   "E82", "X", 3,1,     // ?
   "E83", "V", 3,1,     // OTHER VETS PROPERTIES - UNSECURED( SEC.3O, P, Q, R)
   "E84", "X", 3,1,     // ?
   "E85", "U", 3,1,     // PRIVATELY OWNED COLLEGES - UNSECURED (SEC. 3E)
   "E86", "W", 3,1,     // WELFARE GROUP C  AUDIT YRS 18, 21, 24, 27
   "E87", "X", 3,1,     // HOUSING - LOW INC HSEHLD FORM 267L
   "E88", "X", 3,1,     // HOUSING - ELDERLY OR HANDICAP FAMILIES  FORM 267H
   "E89", "X", 3,1,     // REHAB LIVING QTRS  FORM 267R
   "E91", "X", 3,1,     // HOUSING - LOW INC HSEHLD 267L1 (ENTITY HAS SCC)
   "E92", "X", 3,1,     // PENDING APPROVAL
   "E93", "M", 3,1,     // MUSEUM PARTIAL LAND
   "E94", "M", 3,1,     // MUSEUM PARTIAL STRUCTURE
   "E95", "X", 3,1,     // MULTIPLE ENTITY RECVG EXEMPTION -267O
   "E96", "X", 3,1,     // HOUSING - OVER INCOME TENANT DATA FORM 267-L3
   "E97", "X", 3,1,     // HOUSING LOW INCOME TENANT DATA 267L2
   "E98", "X", 3,1,     // OTHER
   "E99", "X", 3,1,     // LOW VALUE ORDINANCE EXEMPTION
   "P19", "X", 3,1,     // PROP 19 - FOR BYVT 19 REPORTING ONLY
   "","",0,0
};
   
#endif
