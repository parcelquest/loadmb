/**************************************************************************
 *
 * Notes:
 *    1) Review Yol_ExtrLien() before loading LDR.
 *    2) Currently output from -Xs has not been used.
 *    3) Use MergeYolRoll() instead of Yol_LoadRoll() until county corrects
 *       their data problem
 *
 * Options:
 *    -L   (load lien)
 *    -U   (load update)
 *    -Xa  (extract char data)
 *    -Xl  (extract lien value)
 *    -Xs  (extract sale data)
 *    -Xsi (extract sale data and import into SQL)
 *    -O   (overwrite log file)
 *    -V   (load value)
 *
 * LDR load:      -CYOL -L -Xl -Xsi -Xa -O
 * Normal update: -CYOL -U -Xsi -Xa -O [-V] [-T]
 *
 * Revision
 * 12/02/2008 8.5.0    Copy from MergeSbt.cpp to create this file
 * 12/15/2008 8.5.2.2  Fix Yol_ConvertChar() and Yol_MergeChar() to support
 *                     new CHAR fields LotSqft, LotAcres, Stories, ParkSpaces, Rooms.
 * 03/04/2009 8.6.2    Fix LotAcres (use Acres from roll file as primary and LandSqft from
 *                     Chars file as secondary to populate This field) and turn ON ParkSpaces.
 * 03/05/2009 8.6.2.1  Translate GarageTypeCode and adding Other garage to PQLkup.txt
 * 03/06/2009 8.6.2.2  Fix M_Zip problem.
 * 03/30/2009 8.7.1    Use MB_MergeSale() to do sale update instead of MergeSale3() to fix 
 *                     problem that non-sale transaction overwrites sale transaction that  
 *                     occurs in the same day  since we only keep one transaction per day.
 * 04/08/2009 8.7.4    Remove ATTN and C/O from CARE_OF field in Yol_MergeMAdr().
 * 04/09/2009 8.7.4.1  Keep CARE_OF field as is due to request from Paul.  Field length will
 *                     be increased in the future.
 * 07/24/2009 9.1.3    Use replNull() instead of replChar() in Yol_MergeLien().
 * 11/19/2009 9.2.5    Remove unsecured parcels per Paul request.
 * 01/18/2010 9.3.0.3  Skip administrative parcels.  No need to remove header since the 
 *                     file has been sorted.
 * 04/21/2010 9.6.1    Add books 910, 980 & 990 back to roll by Paul request.
 * 05/05/2010 9.6.3    Remove 980 & 990.
 * 07/18/2010 10.1.0   Modify Yol_MergeLien() to store full length legal and Taxibility.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 06/07/2011 10.9.1   Exclude HomeSite from OtherVal.  Replace Yol_MergeExe() with MB_MergeExe()
 *                     and Yol_MergeTax() with MB_MergeTax().  Add -Xs option.
 * 06/16/2011 10.11.0  Modify Yol_MergeSitus() to save full situs HseNo. Remove Unit#
 *                     from HseNo if found.
 * 07/15/2011 11.2.0   Add S_HSENO to LDR version of Yol_MergeSitus()
 * 06/08/2012 11.15.3  Fix bug in Yol_MergeSitus() that didn't copy correct situs zip.
 * 03/12/2012 12.7.1   Modify Yol_MergeRoll() to fix mail addr problem when there is no 
 *                     formatted addr, use unformatted addr instead.
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 10/01/2013 13.13.2  If DocNum from roll file looks funky, copy as is to TRANSFER_DOC.
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 01/01/2014 13.19.0  Add DocCode[] table and use FixDocType() to cleanup cumsale file.
 *                     Remove sale/lien update from Yol_Load_Roll() and use ApplyCumSale()
 *                     to update sale history.
 * 04/17/2014 13.23.0  Add option to load value file.
 * 07/15/2014 14.0.3   Modify ConvertChar() to output cumchar file to RawFile folder. Also modify
 *                     Load_LDR() and Load_Roll() to use cumchar file.
 * 08/29/2014 14.3.3   Copy parseMAdr1() to Yol_ParseMAdr1() and customize for YOL.
 *                     Skip check for long direction.
 * 09/09/2014 14.3.4   Add Yol_FixMAddr() & Yol_FixSAddr() to correct street names for
 *                     W CAPITOL, S RIVER, and N HARBOR.
 * 11/06/2014 14.9.1   Fix memory issue in Yol_ConvertChar()
 * 05/15/2015 14.15.7  Adding Yol_ConvStdChar() and Yol_MergeStdChar() to support new char file format
 * 05/18/2015 14.15.8  Check for input char file before process.
 * 05/26/2015 14.16.0  Add MB_Value.h
 * 07/14/2015 15.0.4   Modify Yol_MergeLien() to add LotSqft & LotAcres.  Add MergeZone() from roll file
 *                     when loading LDR since new CHAR file doesn't include it.  Modify MergeMAdr() to add DBA.
 * 07/17/2015 15.0.6   Fix Zip4 in Yol_MergeMAdr().  Call updateSitusCity() in Yol_Load_Roll() and
 *                     Yol_MergeLien() to fill in missing situs city & zip from mailing.
 * 07/31/2015 15.2.0   Fix Yol_MergeMAdr() to remove old DBA before putting new one in.
 * 08/04/2015 15.2.1   Add LotSqft & LotAcres to Yol_ConvStdChar() and Yol_MergeStdChar().
 * 11/10/2015 15.6.2   Modify Yol_MergeRoll() to use address lines for mailing address.  Also modify
 *                     Yol_MergeMAdr() to pickup DBA and CAREOF from both mail addr and its own field.
 * 11/17/2015 15.6.3   Fix bug in Yol_MergeMAdr() where DBA might overwrite mail address.
 * 12/02/2015 15.7.3   Fix Yol_MergeOwner() to keep SWAPNAME=NAME1 if SWAPNAME has only one word or TRUST.
 * 02/14/2016 15.12.0  Modify Imp_MergeMAdr() to fix overwrite bug.
 * 04/04/2016 15.14.2  Use findXlatCodeA() on asHeating[].
 * 04/06/2016 15.14.3  Load TC data
 * 07/18/2016 16.0.5   Add Yol_Load_LDR3() to support new LDR format.
 * 05/23/2017 16.16.1  Add -Xv option
 * 07/19/2017 17.1.5   Modify Yol_MergeRoll() to format DocNum to match with sale record.
 *                     Modify Yol_MergeLien3() to support new 2017 LDR layout.
 * 03/27/2018 17.9.0   Add -Ut option to update tax.
 * 07/09/2018 18.1.1   Modify Yol_Load_LDR3() to add Zoning and clean up code.
 * 08/02/2015 18.2.2.2 Remove blank APN in SORT command before processing in Yol_Load_Roll().
 * 07/13/2019 19.0.4   Update Yol_MergeLien3() adding AgPreserve.
 * 10/02/2019 19.3.1   Modify Yol_ConvStdChar() to fix QualClass new format.
 * 04/27/2020 19.9.0   Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 07/19/2020 20.2.0   Modify Yol_ParseMAdr1() to verify direction.
 * 11/01/2020 20.4.2   Modify Yol_MergeRoll(), Yol_MergeStdChar() & Yol_MergeZone() to populate default PQZoning.
 * 04/28/2021 20.8.5   Remove LandSqft from Yol_MergeStdChar() and Yol_ConvStdChar() since data in this field
 *                     does not match Acres from ROll file.
 * 07/21/2021 21.1.1   Modify Yol_MergeZone() to update TRANSFER data if available.
 *                     Add Yol_MergeZone() & Yol_Load_LDR15(), Yol_ExtrLdrRec15(), Yol_ExtrLien()
 *                     to support new LDR layout.
 * 03/30/2022 21.7.3   Add -G option and supported functions.  Modify Yol_MergeSitus() to ignore suffix
 *                     if street name is not available.
 * 04/25/2022 21.9.0   Use acGrGrBakTmpl as backup folder for GrGr files. 
 * 06/05/2022 21.9.3   Modify Yol_CreateGrGrRec() to fix bug when parsing grantee.
 * 07/03/2022 22.0.2   Modify Yol_ConvStdChar() to add ParkSpace & LotSqft and using "CharSep"
 *                     as delimiter defined in INI file.
 * 07/26/2022 22.1.1   Fix bug in Yol_ConvStdChar() by checking EOF before validate data.
 * 07/31/2022 22.1.2   Modify Yol_MergeSitus(), Yol_MergeRoll(), Yol_ConvStdChar() to
 *                     handle new update files which include null in records.
 *                     When loading roll update, make sure all files are sorted.
 * 08/10/2022 22.1.4   Increase buffer size in  Yol_CreateGrGrRec() & Yol_LoadGrGrCsv() to
 *                     avoid crash in multi sales.
 * 12/28/2022 22.5.3   Replace NULL with blank in Yol_Load_Roll().
 * 01/03/2023 22.5.5   Modify Yol_LoadGrGr() to check for empty file.
 * 06/03/2023 22.9.2   Fix bug in Yol_Load_Roll() that relates to replNull() "\n" is not on input line.
 * 07/22/2023 23.0.6   Add Yol_Load_LDR17() & Yol_MergeLien17() to support new LDR record layout (LdrGrp 17).
 * 03/23/2024 23.7.3   Modify Yol_MergeStdChar() to update LotAcre/LotSqft if available.
 * 03/20/2024 23.8.1   Modify ConvStdChar() To fix LOT_ACRES when the value is too big.
 * 09/19/2024 24.1.4   Fix known bad char in Yol_MergeOwner().  Modify Yol_MergeLien3() to add ExeType.
 * 11/22/2024 24.4.1   Fix bad TRANSFER_DOC in Yol_MergeRoll().
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "PQ.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "UseCode.h"
#include "Update.h"
#include "doGrGr.h"

#define _YOLO_       1
#include "LoadMB.h"
#include "MBExtrn.h"
#include "LoadValue.h"
#include "MB_Value.h"
#include "CharRec.h"
#include "MergeYol.h"
#include "Tax.h"

static STRSFX Yol_SfxTbl[] =
{
   "DR",  "DR",  "5  ", 2,
   "RD",  "RD",  "2  ", 2,
   "ST",  "ST",  "1  ", 2,
   "CT",  "CT",  "14 ", 2,
   "AVE", "AVE", "3  ", 3,
   "ST",  "ST",  "1  ", 2,
   "HWY", "HWY", "20 ", 3,
   "LN",  "LN",  "21 ", 2,
   "WAY", "WAY", "38 ", 3,
   "CIR", "CIR", "10 ", 3,
   "","","",0
};

IDX_TBL5 YOL_DocCode[] =
{// DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN
   "01", "1  ", 'N', 2, 3,          // *SALE (DTT)
   "02", "13 ", 'Y', 2, 3,          // *TRANSFER (NO DTT)
   "03", "74 ", 'Y', 2, 3,          // *NON-REAPPRAISABLE EVENT
   //"04", "49 ", 'Y', 2, 3,          // MOBILEHOME LIEN UPDATE
   //"05", "74 ", 'Y', 2, 3,          // NEW MOBILE HOME INSTALLATION (SOLD)
   //"06", "74 ", 'Y', 2, 3,          // POSSESSORY INT NEW/RENEWAL
   //"07", "74 ", 'Y', 2, 3,          // POSSESSORY INT LIEN UPDATE
   "08", "74 ", 'Y', 2, 3,          // *CLERICAL FUNCTIONS
   "09", "19 ", 'Y', 2, 3,          // *SPLIT/COMBO
   "10", "57 ", 'N', 2, 3,          // *PARTIAL INTEREST SALE (DTT)
   "11", "57 ", 'Y', 2, 3,          // *PARTIAL INT TRANSFER (NO DTT)
   "12", "74 ", 'Y', 2, 3,          // *GOVERNMENT ENTITY/NO SUPL
   //"13", "74 ", 'Y', 2, 3,          // NEW SINGLE FAMILY DWELLING
   //"14", "74 ", 'Y', 2, 3,          // NEW GARAGE/CARPORT
   //"15", "74 ", 'Y', 2, 3,          // NEW SWIMMING POOL/SPA
   //"16", "74 ", 'Y', 2, 3,          // DWELLING ADD'N/CONVERSION
   //"17", "74 ", 'Y', 2, 3,          // GAR ADD'N/CONV/PATIO/DECK
   "18", "74 ", 'Y', 2, 3,          // *MISC ADD'N
   "19", "74 ", 'Y', 2, 3,          // *MISC RES
   "20", "13 ", 'Y', 2, 3,          // *DEED
   "22", "13 ", 'Y', 2, 3,          // *DEED
   //"23", "74 ", 'Y', 2, 3,          // NEW MULTIPLE BLDG
   //"24", "74 ", 'Y', 2, 3,          // ADD'N/CONV TO MULTIPLES
   //"25", "74 ", 'Y', 2, 3,          // MISC MULTIPLES
   "26", "13 ", 'Y', 2, 3,          // *DEED
   //"33", "74 ", 'Y', 2, 3,          // NEW RURAL BLDG
   //"34", "74 ", 'Y', 2, 3,          // RURAL BLDG ADD'N
   //"35", "74 ", 'Y', 2, 3,          // MISC RURAL ADD'N, N.L.I.
   //"36", "74 ", 'Y', 2, 3,          // VINEYARD ADD'N/DELETION
   //"37", "74 ", 'Y', 2, 3,          // LAND IMPROVEMENTS
   //"38", "74 ", 'Y', 2, 3,          // TREES ADD'S/DELETIONS
   //"44", "74 ", 'Y', 2, 3,          // NEW INDUSTRIAL BLDG
   //"45", "74 ", 'Y', 2, 3,          // ADD'N/CONV INDUSTRIAL
   //"46", "74 ", 'Y', 2, 3,          // MISC INDUSTRIAL
   //"53", "74 ", 'Y', 2, 3,          // NEW COMM'L BLDG
   //"54", "74 ", 'Y', 2, 3,          // ADD'N/CONV COMM'L
   //"55", "74 ", 'Y', 2, 3,          // MISC COMM'L
   "58", "75 ", 'Y', 2, 3,          // *PROP 58-NON REAPPRAISAL
   //"63", "74 ", 'Y', 2, 3,          // AG CONTRACT-NEW BLDG
   //"64", "74 ", 'Y', 2, 3,          // AG CONTRACT-ADD'N/CONV
   //"65", "74 ", 'Y', 2, 3,          // AG CONTRACT-MISC
   //"66", "74 ", 'Y', 2, 3,          // AG CONTRACT- VINES INC/DEC
   //"67", "74 ", 'Y', 2, 3,          // AG CONTRACT LAND IMPS
   //"68", "74 ", 'Y', 2, 3,          // AG CONTRACT- TREES INC/DEC
   //"70", "74 ", 'Y', 2, 3,          // LIEN DATE UPDATE
   //"71", "74 ", 'Y', 2, 3,          // PROP 8
   //"72", "74 ", 'Y', 2, 3,          // CALAMITY-APPLICATION
   //"73", "74 ", 'Y', 2, 3,          // CALAMITY-RESTORATION
   //"74", "74 ", 'Y', 2, 3,          // ASSESSMENT APPEALS
   //"75", "74 ", 'Y', 2, 3,          // SOLAR IMPROVEMENT
   "80", "74 ", 'Y', 2, 3,          // *NON-TAXABLE GOVERNMENT PROP
   //"81", "74 ", 'Y', 2, 3,          // TAXABLE GOVERNMENT LAND
   //"82", "74 ", 'Y', 2, 3,          // SAND/GRAVEL NEW
   //"83", "74 ", 'Y', 2, 3,          // SAND/GRAVEL ABANDONDED
   //"84", "74 ", 'Y', 2, 3,          // GAS WELLS NEW
   //"85", "74 ", 'Y', 2, 3,          // GAS WELLS ABANDONDED
   //"96", "74 ", 'Y', 2, 3,          // DEMO/REMOVAL
   //"98", "74 ", 'Y', 2, 3,          // HOX
   "", "", '\0', 0, 0
};

extern   bool  bEnCode;
extern   char  *asDir[];
static   long  lCityCorrected;
static   char  m_strLegal[128];

/******************************** Yol_FixMAddr *******************************
 *
 * Special cases for YOL where direction is spell out.
 *
 *****************************************************************************/

void Yol_FixMAddr(char *pOutbuf)
{
   char  acTmp[128], acTmp1[128], acDir[16], *pTmp;

   // Rename special Street as needed
   if ((!memcmp(pOutbuf+OFF_M_STREET, "CAPITOL  ", 9) ||
      !memcmp(pOutbuf+OFF_M_STREET, "HARBOR  ", 8) ||
      !memcmp(pOutbuf+OFF_M_STREET, "RIVER  ", 7) ) && 
      (*(pOutbuf+OFF_M_DIR) > ' ' && *(pOutbuf+OFF_M_DIR+1) == ' '))
   {
      if (pTmp = getNameDir(pOutbuf+OFF_M_DIR))
      {
         LogMsg("M ==> %.12s: %c %.50s", pOutbuf, *(pOutbuf+OFF_M_DIR), pOutbuf+OFF_M_STREET);

         sprintf(acTmp, "%s %.*s", pTmp, SIZ_M_STREET, pOutbuf+OFF_M_STREET);
         memcpy(pOutbuf+OFF_M_STREET, acTmp, SIZ_M_STREET);
         memset(pOutbuf+OFF_M_DIR, ' ', SIZ_M_DIR);

         sprintf(acDir, " %c ", *pTmp);
         sprintf(acTmp1, " %s ", pTmp);
         memcpy(acTmp, pOutbuf+OFF_M_ADDR_D, SIZ_M_ADDR_D);
         acTmp[SIZ_M_ADDR_D] = 0;

         replStr(acTmp, acDir, acTmp1);
         memcpy(pOutbuf+OFF_M_ADDR_D, acTmp, SIZ_M_ADDR_D);
      }
   }
}

void Yol_FixSAddr(char *pOutbuf)
{
   char  acTmp[128], acTmp1[128], acDir[16], *pTmp;

   // Rename special Street as needed
   if ((!memcmp(pOutbuf+OFF_S_STREET, "CAPITOL  ", 9) ||
      !memcmp(pOutbuf+OFF_S_STREET, "HARBOR  ", 8) ||
      !memcmp(pOutbuf+OFF_S_STREET, "RIVER  ", 7) ) && 
      (*(pOutbuf+OFF_S_DIR) > ' ' && *(pOutbuf+OFF_S_DIR+1) == ' '))
   {
      if (pTmp = getNameDir(pOutbuf+OFF_S_DIR))
      {
         LogMsg("S ==> %.12s: %c %.50s", pOutbuf, *(pOutbuf+OFF_S_DIR), pOutbuf+OFF_S_STREET);

         sprintf(acTmp, "%s %.*s", pTmp, SIZ_S_STREET, pOutbuf+OFF_S_STREET);
         memcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET);
         memset(pOutbuf+OFF_S_DIR, ' ', SIZ_S_DIR);

         sprintf(acDir, " %c ", *pTmp);
         sprintf(acTmp1, " %s ", pTmp);
         memcpy(acTmp, pOutbuf+OFF_S_ADDR_D, SIZ_S_ADDR_D);
         acTmp[SIZ_S_ADDR_D] = 0;

         replStr(acTmp, acDir, acTmp1);
         memcpy(pOutbuf+OFF_S_ADDR_D, acTmp, SIZ_S_ADDR_D);
      }
   }
}

/******************************** Yol_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Yol_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acTmp[128], *pTmp, *pTmp1;
   char  acName1[64];
   bool  bKeepName=false;
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "032531002000", 9))
   //   iTmp = 0;
#endif

   if (pTmp = strstr(pNames, "&  &"))
   {
      pTmp += 4;
      if (*pTmp == ' ') pTmp++;
      strcpy(acTmp, pTmp);
   } else
      strcpy(acTmp, pNames);

   if (pTmp = strchr(acTmp, '/'))
   {
      *pTmp = 0;
      sprintf(acName1, "%s & %s", acTmp, ++pTmp);
      strcpy(acTmp, acName1);
   }

   // Fix known bad char
   if (pTmp = strchr(acTmp, 0x8E))
      *pTmp = 'A';

   // Save original owner name
   blankRem(acTmp);
   strcpy(acName1, acTmp);

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Drop everything from these words
   if ((pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;


   if ((pTmp=strstr(acTmp, " CO-TR"))  || (pTmp=strstr(acTmp, " LIFE EST")) ||
       (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTATE OF")) )
      *pTmp = 0;

   // Save data to append later
   if (pTmp1=strstr(acTmp, "TRUST"))
   {
      bKeepName = true;
      if ((pTmp=strstr(acTmp, " FAM"))   || (pTmp=strstr(acTmp, " REVOC")) ||
          (pTmp=strstr(acTmp, " INDIV")) || (pTmp=strstr(acTmp, " AMENDED ")) ||
          (pTmp=strstr(acTmp, " LIV")) )
         *pTmp = 0;
      else
         *pTmp1 = 0;
   }

   // Now parse owners
   acTmp[63] = 0;
   iTmp = splitOwner(acTmp, &myOwner, 0);

   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);
   myTrim(myOwner.acSwapName);
   if (bKeepName)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
   else if (countChar(myOwner.acSwapName, ' ') > 0)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
}

/********************************* parseMAdr1() *****************************
 *
 * Parse address line 1 - special case for YOL
 *
 ****************************************************************************/

void Yol_ParseMAdr1(ADR_REC *pAdrRec, char *pAddr)
{
   char  acAdr[256], acStrName[256], acTmp[256], *apItems[16], *pTmp;
   char *pAdr;
   int   iCnt, iTmp, iIdx;
   bool  bRet, bDir=false;

   // Check for PO Box
   if (!memcmp(pAddr, "PO BOX", 6)   ||
       !memcmp(pAddr, "P.O. BOX", 8) ||
       !memcmp(pAddr, "P O ", 4)     ||
       !memcmp(pAddr, "PO BX", 5)    ||
       !memcmp(pAddr, "BOX ", 4)     ||
       !memcmp(pAddr, "POB ", 4) )
   {
      strcpy(pAdrRec->strName, pAddr);
      return;
   }

   acStrName[0] = 0;
   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return;        // bad address
   }

   // Remove leading space - SFX
   while (*pAdr && *pAdr == ' ') pAdr++;

   // Remove '.' and extra space
   iTmp = 0;
   while (*pAdr)
   {
      // Remove dot, comma, and single quote
      if (*pAdr != '.' && *pAdr != ',' && *pAdr != 39)
         acAdr[iTmp++] = *pAdr;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }
   acAdr[iTmp] = 0;

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if (iTmp == 0 || iCnt == 1)
   {
      strcpy(acStrName, pAddr);
      pAdrRec->lStrNum = 0;
   } else
   {
      if (apItems[0][strlen(apItems[0])-1] > '9')
      {
         // StrSub ?
         pTmp = apItems[0];
         while (*pTmp && isdigit(*pTmp) )
            pTmp++;
         if (*pTmp && (*pTmp == '-' || *pTmp == '/'))
            pTmp++;
         if (*pTmp && strlen(pTmp) <= SIZ_M_STR_SUB)
            strcpy(pAdrRec->strSub, pTmp);
      }

      strcpy(pAdrRec->HseNo, apItems[0]);
      pAdrRec->lStrNum = iTmp;
      sprintf(acTmp, "%d", iTmp);
      strcpy(pAdrRec->strNum, acTmp);
      iIdx = 1;

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
         strcpy(pAdrRec->strSub, apItems[iIdx++]);

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][0], SIZ_M_UNITNO);
         iIdx++;
      }

      if (iIdx >= iCnt)
      {
         // Bad address
         return;
      }

      // Dir
      if (strlen(apItems[iIdx]) < 3)
      {
         if (*(apItems[iIdx]+1) == '.')
            *(apItems[iIdx]+1) = 0;
         bDir = isDir(apItems[iIdx]);
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      //if (iTmp <= 7 && iCnt > 2)
      if (bDir && iCnt > 2)
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         if ((iTmp=GetSfxDev(apItems[iIdx+1])) &&
             memcmp(apItems[iIdx+1], "HWY", 3) && 
             memcmp(apItems[iIdx+1], "HIGHWAY", 7)) 
         {
            strncpy(acStrName, apItems[iIdx], SIZ_M_STREET);
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, apItems[iIdx]);
            iIdx++;
         }
      }

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNO);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
            || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT")
            || !strcmp(apItems[iCnt-1], "SP") || !strcmp(apItems[iCnt-1], "RM") )
            iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#")
         || !strcmp(apItems[iCnt-2], "APT")  || !strcmp(apItems[iCnt-2], "APTS") || !strcmp(apItems[iCnt-2], "APT#")
         || !strcmp(apItems[iCnt-2], "RM")   || !strcmp(apItems[iCnt-2], "SP")   || !strcmp(apItems[iCnt-2], "LOT") )
      {
         sprintf(acTmp, "#%s", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         iCnt -= 2;
      } else if ((pTmp=strchr(apItems[iCnt-1], '#')))
      {  // 15025 NE SACRAMENTO ST#54, 838 S GRAND AVE APT#611
         if (!memcmp(apItems[iCnt-1], "SP", 2))
         {
            *pTmp = ' ';
         } else
         {
            strncpy(pAdrRec->Unit, pTmp, SIZ_M_UNITNO);
            *pTmp = 0;
            if (!strcmp(apItems[iCnt-1], "APT"))
               iCnt--;
         }
      } else if (!memcmp(apItems[iCnt-1], "FL-", 3) || !memcmp(apItems[iCnt-1], "FLR-", 4))
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "LOT") && atoi(apItems[iCnt-1]) > 0)
      {  // 245 WILDWOOD DR LOT 183, 45055 E FLORIDA AVE SP 36
         sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         iCnt -= 2;
      } else if (iCnt > 3 && atoi(apItems[iCnt-1]) > 0)
      {  // 555 N DANEBO 33
         // Not 21051 STATE ROUTE 772, 5323 HERMITAGE AVE# 1, 3100-R WEST HWY 86
         if (strcmp(apItems[iCnt-2], "ROUTE") && strcmp(apItems[iCnt-2], "HWY")
            && strcmp(apItems[iCnt-2], "BOX") && strcmp(apItems[iCnt-2], "ROAD")
            && strcmp(apItems[iCnt-2], "SP")  && strcmp(apItems[iCnt-2], "HIGHWAY"))   // MER
         {
            // Check for 123 COUNTY RD 105 on SIS->001110050000
            if (!strstr(pAddr, "COUNTY RD"))
            {
               sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
               strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
               iCnt--;
            } else
            {
               sprintf(acStrName, "COUNTY ROAD %s", apItems[iCnt-1]);
               bRet = true;
            }

         }

         if (pTmp=strchr(apItems[iCnt-1], '#'))
            *pTmp = 0;
      }

      // Not direction, token is str name
      if (!bRet && iIdx < iCnt)
      {
         // Check suffix
         if (iTmp=GetSfxDev(apItems[iCnt-1]))
         {
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            while (iIdx < iCnt-1)
            {
               // If multiple address, take the first one.
               // This loop break after first suffix found, ignore the rest.
               // We also must avoid strname like 123 ST PAUL BLVD.  StrSfx must
               // be indexed > 1
               if ((iIdx > 1) &&
                   (iIdx < iCnt-2) &&
                   (iTmp=GetSfxDev(apItems[iIdx])) )
               {
                  strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
                  break;
               }
               strcat(acStrName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStrName, " ");
            }
         } else if (!strcmp(apItems[iCnt-2], "HWY"))
         {  // 123 HWY 70
            sprintf(acStrName, "%s %s", apItems[iCnt-2], apItems[iCnt-1]);
         } else
         {
            // Scan for suffix in multi address cases.  Keep the first one
            // 123 MAIN ST 540 N SANTA ANA BLVD
            while (iIdx < iCnt)
            {
               if (iIdx > 1)
               {
                  if (iTmp = GetSfxDev(apItems[iIdx]))
                  {
                     try
                     {
                        pTmp = GetSfxStr(iTmp);
                        if (pTmp)
                           strcpy(pAdrRec->strSfx, pTmp);
                     } catch (...)
                     {
                        LogMsg("***** Error in GetSfxStr() with sfx=%s, %d\n", apItems[iIdx], iTmp);
                     }

                     break;
                  }
               }

               strcat(acStrName, apItems[iIdx]);
               iIdx++;
               if (iIdx < iCnt)
                  strcat(acStrName, " ");
            }
         }
      }
   }
   strncpy(pAdrRec->strName, acStrName, SIZ_M_STREET);
}

/******************************** Yol_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Yol_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64];
   int      iTmp;
   char     *pTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
      vmemcpy(pOutbuf+OFF_DBA, apTokens[MB_ROLL_DBA], SIZ_DBA);

   // Skip test address
   if (!memcmp(apTokens[MB_ROLL_M_ADDR], "XXX", 3) || !memcmp(apTokens[MB_ROLL_M_ADDR], "000", 3))
      return;

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   Yol_ParseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
     sMailAdr.strName[SIZ_M_STREET] = 0;
     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003410023000", 9))
   //   iTmp = 0;
#endif

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      memcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], strlen(apTokens[MB_ROLL_M_CITY]));
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      int   iZip, iZip4;
      char  acZip[16];
      pTmp = strchr(apTokens[MB_ROLL_M_ZIP], '-');
      iZip = atol(apTokens[MB_ROLL_M_ZIP]);
      if (pTmp)
         iZip4 = atol(pTmp+1);
      else if (iZip > 99999)
      {
         iZip = atoin(apTokens[MB_ROLL_M_ZIP], 5);
         iZip4 = atol(apTokens[MB_ROLL_M_ZIP]+5);
      } else
         iZip4 = 0;

      if (iZip > 0)
      {
         sprintf(acZip, "%.5d", iZip);
         memcpy(pOutbuf+OFF_M_ZIP, acZip, 5);
         if (iZip4 > 0)
         {
            sprintf(&acZip[5], "-%.4d", iZip4);
            memcpy(pOutbuf+OFF_M_ZIP4, &acZip[6], 4);
         }
      } else
         acZip[0] = 0;

      sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], acZip);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

/********************************* Yol_MergeMAdr *****************************
 *
 * Notes:
 *    - Mail addr is provided in various form.  We try the best we can since
 *      there is no way to have 100% covered.
 *
 *    - Sample Input:
 *      AMERICAS SERVICING CO           3476 STATEVIEW BLVD             FORECLOSURE MAC#7801-013        FT MILL SC 29715
 *      ALFRED F & IRENE G DEMELO       DBA WALGREENS #6418-S-P         300 WILMOT RD MS#1435           DEERFIELD IL 60015-5121
 *      AD VALOREM TAX DEPT (UNIT 3505) DBA BEACON OIL COMPANY          ONE VALERO WAY                  SAN ANTONIO TX 78249-1616
 *      ATTN  DAVE ARNAIZ               PMB 330                         PO BOX 2818                     ALPHARETTA GA 30023
 *      C/O BANK OF AMERICA             ATTN: BURR WOLFF LP             505 CITY PARKWAY WEST STE 100   ALPHARETTA GA 30023
 *      C/O CITIMORTGAGE, INC.          MAILSTOP 02-25                  27555 FARMINGTON RD             FARMINGTON HILLS MI 48334-3357
 *      RE: LOAN# 0089214605/BALTAZAR   2300 BROOKSTONE CENTRE PKWY     PO BOX 84013                    PO BOX 84013
 *
 *****************************************************************************/

void Yol_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4, char *pCareOf, char *pDba)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[256];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030260041", 9))
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         // Update DBA
         vmemcpy(pOutbuf+OFF_DBA, pLine1+4, SIZ_DBA);
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else if (!_memicmp(pLine1, "DBA ", 4) )
      {
         // Update DBA
         strcpy(acTmp, pLine1+4);
         vmemcpy(pOutbuf+OFF_DBA, acTmp, SIZ_DBA);
         p1 = pLine2;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
   } else if (pCareOf && *pCareOf > ' ' && memcmp(pCareOf, "DBA", 3))
   {
      updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
   } 
   
   if (*(pOutbuf+OFF_DBA) == ' ' && pDba && *pDba > ' ')
      vmemcpy(pOutbuf+OFF_DBA, pDba+4, SIZ_DBA);

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      iTmp = blankRem(acAddr1, SIZ_M_ADDR_D);
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D, iTmp);

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
         if (sMailAdr.strSfx[0] > '0')
            vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      } else
      {
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
      }
   }

   strcpy(acAddr2, p2);
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
   }
}

/****************************** Yol_ParseAdr() ******************************
 *
 * Parse address line 1 that has no street number, so parse
 * street name, suffix, and/or community.  Suffix will be coded
 *
 * StrName may contain strType and city abbr
 * i.e.
      SAN JOSE ST-SJB
      SECOND & JEFFERSON STS
      TAHUALAMI & 4TH ST SJB
      3RD & MONTEREY SJB
      MONTEREY ST SJB
      CHURCH & THIRD ST.  SJB
      THIRD ST.  SJB.
      NORTH ST.  SJB
      9A-409B SIXTH
      & 702 4TH
      3RD & 27 SAN JOSE STS SJ
      3RD ST-SJB-PINK HOUSE
      9 THOMAS LN (OFF 1ST ST-
      NORTH/THIRD LOT 4
      RIVER OAKS PARK/CHITTEND
      NORTH ST LOT 3
      1ST ST-SAN JUAN BAUTISTA
      3RD ST SAN JUAN BAUTISTA
      7TH ST PCL 8
      7TH STREET REAR
      4TH ST-CORNER WASHINGTON
      SOUTH SIDE OF HWY 156
      ARPENTERIA E SIDE
      THE ALAMEDA-SAN JUAN INN
      1/2 THE ALAMEDA (GROSCUP
      SAN JUAN-HOLLISTER
      CHITTENDEN RD NO OF HWY
      FOREST & ANZAR RDS. AROM
      FOREST RD AROMAS
      SCHOOL ROAD-AROMAS
      ROSE AVE "SPRING LOT"
      "A" ROSE AVE-AROMAS
 *
 ****************************************************************************

void Yol_ParseAdr(ADR_REC *pAdrRec, ADR_REC *pSitus)
{
   char  acAdr[128], acStr[64], acTmp[64], *apItems[16], *pTmp, *pTmp1;
   int   iCnt, iIdx, iSfxCode;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));
   strcpy(acAdr, pSitus->strName);

   // Check for unit #
   //   1 VIA PADRE UNIT #23
   //   SAN FELIPE RD, UNITS 1 T
   //   CANAL ALLEY UNITS A & B
   if ((pTmp = strstr(acAdr, " UNIT")) || (pTmp = strstr(acAdr, "-UNIT")))
   {
      // Only process Unit# id strNum is present
      if (*(pTmp+5) == 'S' || *(pTmp+5) == ' ')
      {
         pTmp1 = pTmp+6;
         if (*(pTmp+5) == 'S')
            pTmp1++;
         strncpy(pAdrRec->Unit, pTmp1, SIZ_M_UNITNO);
         *pTmp = 0;
      }
   }

   // Check for trailing community
   if (pTmp=strrchr(acAdr, '-'))
   {
      pTmp1 = pTmp + 1;
      if (*pTmp1 == ' ')
         pTmp1++;

      if (!memcmp(pTmp1, "SAN JUAN", 8) || !memcmp(pTmp1, "SJ", 2))
      {
         // SJB
      } else if (!memcmp(pTmp1, "AROMA", 5))
      {
      }else if (!memcmp(pTmp1, "N SIDE", 6))
      {
      }else if (!memcmp(pTmp1, "S SIDE", 6))
      {
      } else
      {
      }
   }

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
      return;

   if (iCnt > 2 && (!strcmp(apItems[iCnt-2], "STE")))
   {
      strncpy(&pAdrRec->Unit[1], apItems[iCnt-1], SIZ_M_UNITNO-1);
      iCnt -= 2;
   } else if (iCnt > 2 && (!strcmp(apItems[iCnt-2], "SP")))
   {
      sprintf(acTmp, "SP %s", apItems[iCnt-1]);
      strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
      iCnt -= 2;
   }


   acTmp[0] = 0;

   if (iSfxCode = GetSfxDev(apItems[iCnt-1]))
   {
      // Multi-word street name with suffix
      if (bEnCode)
         sprintf(pAdrRec->strSfx, "%d", GetSfxCode(GetSfxStr(iSfxCode)));
      else
         strcpy(pAdrRec->strSfx, GetSfxStr(iSfxCode));

      while (iIdx < iCnt-1)
      {
         strcat(acStr, apItems[iIdx++]);
         if (iIdx < iCnt)
            strcat(acStr, " ");
      }
   } else
   {
      // Multi-word street name without suffix
      while (iIdx < iCnt)
      {
         strcat(acStr, apItems[iIdx++]);
         if (iIdx < iCnt)
            strcat(acStr, " ");
      }
   }

   // Copy street name
   acStr[SIZ_M_STREET] = 0;
   strcpy(pAdrRec->strName, acStr);
}

/******************************* Yol_CorrectCity *****************************
 *
 * Correct city code
 *
 *****************************************************************************/

int Yol_CorrectCity(char *pOutbuf)
{
   int   iTmp, iRet;
   char  acTmp[128], acCity[128];

   iTmp = atoin(pOutbuf+OFF_S_CITY, 2);
   switch (iTmp)
   {
      case 2:
         strcpy(acCity, "CAPAY CA");
         iRet = 1;
         break;
      case 3:
         strcpy(acCity, "CLARKSBURG CA");
         iRet = 2;
         break;
      case 4:
         strcpy(acCity, "DAVIS CA");
         iRet = 3;
         break;
      case 5:
         strcpy(acCity, "DUNNIGAN CA");
         iRet = 4;
         break;
      case 6:
         strcpy(acCity, "EL MACERO CA");
         iRet = 5;
         break;
      case 7:
         strcpy(acCity, "ESPARTO CA");
         iRet = 6;
         break;
      case 8:
         strcpy(acCity, "GUINDA CA");
         iRet = 7;
         break;
      case 9:
         strcpy(acCity, "KNIGHTS LANDING CA");
         iRet = 8;
         break;
      case 10:
         strcpy(acCity, "MADISON CA");
         iRet = 9;
         break;
      case 11:
         strcpy(acCity, "RUMSEY CA");
         iRet = 10;
         break;
      case 12:
         strcpy(acCity, "WEST SACRAMENTO CA");
         iRet = 11;
         break;
      case 13:
         strcpy(acCity, "WINTERS CA");
         iRet = 12;
         break;
      case 14:
         strcpy(acCity, "WOODLAND CA");
         iRet = 13;
         break;
      case 15:
         strcpy(acCity, "YOLO CA");
         iRet = 14;
         break;
      case 17:
         strcpy(acCity, "ZAMORA CA");
         iRet = 15;
         break;
      default:
         iRet = 0;
         break;
   }

   if (!iRet)
   {
      memset(pOutbuf+OFF_S_CITY, ' ', SIZ_S_CITY);
      memset(pOutbuf+OFF_S_CTY_ST_D, ' ', SIZ_S_CTY_ST_D);
   } else
   {
      LogMsg("*** Correct city [%.12s]: %d -> %d : %s", pOutbuf, iTmp, iRet, acCity);
      iTmp = sprintf(acTmp, "%d    ", iRet);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, iTmp);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, strlen(acTmp));
   }

   return iRet;
}

/******************************** Yol_MergeSitus *****************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Yol_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004130013", 9))
   //   acTmp[0] = 0;
#endif
   //
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   Yol_ParseMAdr1(&sSitusAdr, acAddr1);

   if (sSitusAdr.lStrNum > 0)
   {
      char *pTmp = strchr(acAddr1, ' ');
      *pTmp = 0;

      // Save original StrNum
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
      memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   }

   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

int Yol_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acLegal[64], acUnit[32], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSitus);

      // Get first rec
      pRec = fgets(acRec, 512, fdSitus);
   }

   do 
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF 
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Replace null with space
   replNull(pRec);

   // Parse input
   iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "058011015", 9))
   //   iRet = 0;
#endif

   // Clear old data
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   acLegal[0] = 0;
   acUnit[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      // Check for unit#
      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], '#'))
      {
         *pTmp++ = 0;
         blankRem(pTmp);
         if (*pTmp > ' ')
            sprintf(acUnit, "#%s", pTmp);
      } else if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
      {
         *pTmp++ = 0;
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp, strlen(pTmp));
      }

      // Save original house number
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));
      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
      }
   } else if (*apTokens[MB_SITUS_STRNUM] == 'T' ||
              *apTokens[MB_SITUS_STRNUM] == 'R' ||
              *apTokens[MB_SITUS_STRNUM] == 'L' ||
              !_memicmp(apTokens[MB_SITUS_STRNAME], "PARKING", 7) )
   {
      sprintf(acLegal, " %s %s", apTokens[MB_SITUS_STRNUM], apTokens[MB_SITUS_STRNAME]);
      strcat(m_strLegal, acLegal);
   }

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
         Sfx2Code(acTmp, acCode);
      else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }

      if (*apTokens[MB_SITUS_STRNAME] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
         strcat(acAddr1, " ");
         strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);

         memcpy(pOutbuf+OFF_S_STREET, apTokens[MB_SITUS_STRNAME], strlen(apTokens[MB_SITUS_STRNAME]));
         memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      }
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apTokens[MB_SITUS_STRNAME]);
      if (sAdr.strName[0] > ' ')
      {
         blankPad(sAdr.strName, SIZ_S_STREET);
         memcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      }
      if (sAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, strlen(sAdr.strSfx));

      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      if (*apTokens[MB_SITUS_UNIT] == '#')
         strcpy(acUnit, apTokens[MB_SITUS_UNIT]);
      else
         sprintf(acUnit, "#%s", apTokens[MB_SITUS_UNIT]);
   }

   if (acUnit[0] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, acUnit);
      memcpy(pOutbuf+OFF_S_UNITNO, acUnit, strlen(acUnit));
   }

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1, pOutbuf);   
      blankPadz(acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
      {
         sprintf(acTmp, "%s CA", acAddr1);
         iTmp = blankRem(acTmp);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
      }
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/******************************** Yol_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Yol_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   while (!iTmp)
   {
      // Replace tab char with 0
      if (pRec)
         iRet = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "003410023000", 9))
      //   iRet = 0;
#endif

      pTmp = apTokens[MB_SALES_DOCNUM];
      // Process recorded doc only
      if (*(pTmp+4) == 'R')
      {
         // Merge data - Only take sale rec that has both docnum and docdate
         if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
         {
            memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

            // Docnum
            iRet = atol(pTmp+5);
            sprintf(sCurSale.acDocNum, "%.5s%.6d", pTmp, iRet);
            sCurSale.acDocNum[11] = ' ';

            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
            if (pTmp)
            {
               // Doc date
               lCurSaleDt = atol(acTmp);
               memcpy(sCurSale.acDocDate, acTmp, 8);
            }

            // Confirmed sale price - YOL populates this field more often
            // than DOCTAX field.  So check this first
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            } else
            {
               // Tax
               dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
               lPrice = 0;
               if (acTmp[0] > '0')
               {
                  dTmp = atof(acTmp);
                  lPrice = (long)(dTmp * SALE_FACTOR);
                  if (lPrice > 0)
                  {
                     if (lPrice < 100)
                        sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
                     else
                        sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
                     memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
                  }
               }
            }

            // DocType - need translation before production
            // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
            //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

            // Transfer Type
            if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
            {
               while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
               {
                  if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
                  {
                     sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                     break;
                  }
                  iTmp++;
               }
            } else
               sCurSale.acSaleCode[0] = ' ';

            // Group sale?
            if (*apTokens[MB_SALES_GROUPSALE] > '0')
               *(pOutbuf+OFF_MULTI_APN) = 'Y';
            else
               *(pOutbuf+OFF_MULTI_APN) = ' ';

            // Seller
            strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
            blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

            MB_MergeSale(&sCurSale, pOutbuf, true);
            iRet = 0;
         }
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Yol_MergeChar ******************************
 *
 * Note: need code table for Heating and Merling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

int Yol_MergeChar(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32];
   long     lTmp, lBldgSqft, lGarSqft, lLotSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iFp;
   MB_CHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 512, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   pChar = (MB_CHAR *)pRec;

   // Quality Class
   memcpy(acTmp, pChar->QualityClass, MBSIZ_CHAR_QUALITY);
   acTmp[MBSIZ_CHAR_QUALITY] = 0;

   acCode[0] = 0;
   if (isalpha(acTmp[0]))
   {
      *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0] & 0x4F; // Force upper case

      iTmp = 0;
      while (acTmp[iTmp] && !isdigit(acTmp[iTmp]))
         iTmp++;

      if (acTmp[iTmp] > '0')
         iRet = Quality2Code((char *)&acTmp[iTmp], acCode, NULL);
   } else if (acTmp[0] > '0' && acTmp[0] <= '9')
      iRet = Quality2Code(acTmp, acCode, NULL);

   blankPad(acCode, SIZ_BLDG_QUAL);
   memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);

   // Yrblt
   lTmp = atoin(pChar->YearBuilt, MBSIZ_CHAR_YRBLT);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Heating
   int iCmp;
   if (pChar->Heating[0] > ' ')
   {
      iTmp = 0;
      iCmp = -1;
      while (asHeating[iTmp].iLen > 0)
      {
         if (pChar->Heating[0] == asHeating[iTmp].acSrc[0])
         {
            *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }

   // Cooling
   //if (pChar->Cooling[0] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asCooling[iTmp].iLen > 0 && (iCmp=memcmp(pChar->Cooling, asCooling[iTmp].acSrc, asCooling[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_AIR_COND) = asCooling[iTmp].acCode[0];
   //}

   // Pool
   if (pChar->NumPools[0] > ' ')
   {
      iTmp = 0;
      iCmp = -1;
      while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(pChar->NumPools, asPool[iTmp].acSrc, asPool[iTmp].iLen)) > 0)
         iTmp++;

      if (!iCmp)
         *(pOutbuf+OFF_POOL) = asPool[iTmp].acCode[0];
   }

   // Beds
   iBeds = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Fireplace
   iFp = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   } else
      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);

   // HasSeptic or HasSewer
   if (pChar->HasSeptic > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
   else if (pChar->HasSewer > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWell;

   // LotSqft
   lLotSqft = atoin(pChar->LotSqft, MBSIZ_CHAR_LANDSQFT);
   if (lLotSqft > 100)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lLotSqft);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Lot acres
      double dTmp;
      dTmp = (double)(lLotSqft*1000)/SQFT_PER_ACRE;
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp+0.5));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Stories
   iTmp = atoin(pChar->Stories, MBSIZ_CHAR_NUMFLOORS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d.0 ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   } else
      memcpy(pOutbuf+OFF_STORIES, BLANK32, SIZ_STORIES);

   // Rooms
   iTmp = atoin(pChar->TotalRooms, MBSIZ_CHAR_TOTALROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   } else
      memcpy(pOutbuf+OFF_ROOMS, BLANK32, SIZ_ROOMS);

   // Garage Sqft
   lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
   if (lGarSqft > 100)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
   } else
   {
      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = ' ';
   }

   // Garage spaces
   iTmp = atoin(pChar->ParkSpaces, MBSIZ_CHAR_PARKSPACES);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d      ", iTmp);
      memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
   }

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 512, fdChar);

   return 0;
}

int Yol_MergeStdChar(char *pOutbuf)
{
   static   char acRec[1200], *pRec=NULL;
   char     acTmp[256];
   long     lTmp;
   int      iRet, iLoop, iBeds, iFBath, iHBath, iTmp;
   STDCHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=0;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, acRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "044510005", 9))
   //   iRet = 0;
#endif

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)&acRec[0];

   // Quality Class
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;

   // Impr condition
   *(pOutbuf+OFF_IMPR_COND) = pChar->ImprCond[0];

   // Yrblt
   long lYrBlt = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lYrBlt > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
   else
      memset(pOutbuf+OFF_YR_BLT, ' ', SIZ_YR_BLT);

   // YrEff
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 1900 && lTmp >= lYrBlt)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_EFF);
   else
      memset(pOutbuf+OFF_YR_EFF, ' ', SIZ_YR_EFF);

   // LotSqft
   long lLotSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lLotSqft > 100)
   {
      memcpy(pOutbuf+OFF_LOT_SQFT, pChar->LotSqft, SIZ_LOT_SQFT);
      memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_ACRES);
   } 

   // BldgSqft
   long lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 100)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

   // Garage Sqft
   long lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 100)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   } else
      memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);

   // Parking type
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // Heating
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   
   // Cooling 
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

   // Total rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   } else
      memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);
   
   // #Fireplace
   memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);
   *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

   // HasSeptic or HasSewer
   if (pChar->HasSeptic > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
   else if (pChar->HasSewer > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;
   else
      *(pOutbuf+OFF_SEWER) = ' ';

   // Pool
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Stories
   iTmp = atoin(pChar->Stories, SIZ_CHAR_SIZE4);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d.0 ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   } else
      memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

   // Units
   if (pChar->Units[0] > '0')
      memcpy(pOutbuf+OFF_UNITS, pChar->Units, SIZ_CHAR_UNITS);

   // Zoning
   if (pChar->Zoning[0] > ' ' && *(pOutbuf+OFF_ZONE) == ' ')
   {
      memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         memcpy(pOutbuf+OFF_ZONE_X1, pChar->Zoning, SIZ_ZONE);
   }

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************** Yol_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************

int Yol_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, ',');
      pTmp += 2;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 0;
   }

   iRet = ParseStringNQ(pRec, ',', MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Yol_MergeTax ******************************
 *
 * Note:
 *
 ****************************************************************************

int Yol_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double   dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iRet;

   // Merge tax data
   while (!iTmp)
   {
      iTmp = ParseStringNQ(pRec, ',', MB_TAX_ROLLCAT+1, apTokens);
      if (iTmp < MB_TAX_ROLLCAT)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
         return -1;
      }

      // If same tax year, apply values
      //if (!memcmp(myCounty.acYearAssd, apTokens[MB_TAX_YEAR], 4))
      {
         dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
         dTax1 = atof(acTmp);
         dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
         dTax2 = atof(acTmp);
         dTmp = dTax1+dTax2;
         if (dTax1 == 0.0 || dTax2 == 0.0)
            dTmp *= 2;

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/********************************* Yol_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 * Notes: 
 *    04/21/2010 Paul request to include all books 000-799, 910, 980 & 990.
 *    05/05/2010 Another request to remove 980 & 990.
 *    07/19/2017 Format DocNum to YYYYR9999999 to match with sale record
 *
 *****************************************************************************/

int Yol_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "033270003", 9))
   //   iTmp = 0;
#endif

   // Parse string - Use ParseStringNQ1() to handle double quote within double quote
   if (cDelim == ',')
      iRet = ParseStringNQ1(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad input record for APN=%s Fields=%d", apTokens[iApnFld], iRet);
      return -1;
   }

   // Skip unsecured & administrative parcels
   iTmp = atoin(apTokens[iApnFld], 3);
   if (iTmp == 555 || (iTmp > 799 && iTmp != 910))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], iApnLen);
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "57YOL", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   strcpy(m_strLegal, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   acTmp[SIZ_USE_CO] = 0;
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Standard UseCode
   //if (acTmp[0] > ' ')
   //{
   //   _strupr(acTmp);
   //   iTmp = strlen(acTmp);
   //   memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
   //   if (iNumUseCodes > 0)
   //   {
   //      pTmp = Cnty2StdUse(acTmp);
   //      if (pTmp)
   //         memcpy(pOutbuf+OFF_USE_STD, pTmp, 3);
   //      else
   //      {
   //         memcpy(pOutbuf+OFF_USE_STD, USE_MISC, 3);
   //         logUsecode(acTmp, pOutbuf);
   //      }
   //   }
   //}

   // Acres
   double dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
   memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "035410015000", 9))
   //   iTmp = 0;
#endif

   if (*apTokens[MB_ROLL_DOCNUM] > '0' && *(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
   {
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp && !memcmp(acTmp, apTokens[MB_ROLL_DOCNUM], 4))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         if (*(apTokens[MB_ROLL_DOCNUM]+5) == '-')
            iRet = atol(apTokens[MB_ROLL_DOCNUM]+6);
         else
            iRet = atol(apTokens[MB_ROLL_DOCNUM]+5);

         sprintf(acTmp, "%.5s%.7d", apTokens[MB_ROLL_DOCNUM], iRet);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, SIZ_TRANSFER_DOC);
      }
   }
   // Clean up
   iTmp = atoin(pOutbuf+OFF_SALE1_DT, 8);
   if (iTmp > 0 && iTmp < 1900101)
      memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);

   //if (*(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
   //{
   //   pTmp = apTokens[MB_ROLL_DOCNUM];
   //   iRet = atol(pTmp+5);
   //   if (iRet > 0)
   //      sprintf(acTmp, "%.5s%.7d", pTmp, iRet);
   //   else
   //      strcpy(acTmp, apTokens[MB_ROLL_DOCNUM]);
   //   memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, strlen(acTmp));

   //   pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
   //   if (pTmp)
   //      memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   //}

   // Owner
   try {
      Yol_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   } catch(...) {
      LogMsg("***** Exeception occured in Yol_MergeOwner()");
   }

   // Mailing
   try {
      if (memcmp(apTokens[MB_ROLL_M_ADDR1], "   ", 3))
         Yol_MergeMAdr(pOutbuf, apTokens[MB_ROLL_M_ADDR1], apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3], apTokens[MB_ROLL_M_ADDR4], apTokens[MB_ROLL_CAREOF], apTokens[MB_ROLL_DBA]);
      else if (*apTokens[MB_ROLL_M_ADDR] > '0')
         Yol_MergeMAdr(pOutbuf);

      // Rename special Street as needed
      Yol_FixMAddr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Yol_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************* Yol_Load_Roll ******************************
 *
 * This function has not been tested.
 *
 ******************************************************************************/

int Yol_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdRoll;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsgD("Yol_Load_Roll - Starting ...");
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acRec, "S(#1,C,A) OMIT(%d,1,C,LT,\"0\",OR,%d,1,C,GT,\"9\") DEL(%c)", iSkipQuote+1, iSkipQuote+1, cDelim);
   lRet = sortFile(acRollFile, acTmpFile, acRec);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acRec, "S(#1,C,A) DEL(%c)", cDelim);
   lRet = sortFile(acSitusFile, acTmpFile, acRec);
   LogMsg("Open Situs file %s", acTmpFile);
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Sales file
   /*
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -2;
   }
   */

   // Open Exe file
   sprintf(acTmpFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acRec, "S(#1,C,A) DEL(%c)", cDelim);
   lRet = sortFile(acExeFile, acTmpFile, acRec);
   LogMsg("Open Exe file %s", acTmpFile);
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -2;
   }

   // Open Tax file
   sprintf(acTmpFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acRec, "S(#1,C,A) DEL(%c)", cDelim);
   lRet = sortFile(acTaxFile, acTmpFile, acRec);
   LogMsg("Open Tax file %s", acTmpFile);
   fdTax = fopen(acTmpFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
      return -2;
   }

   // Open lien file
   fdLienExt = NULL;
   /*
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Lien file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   }
   */

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record - no need since the file has been resorted
   //pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      // Skip unsecured & administrative parcels
      //if (acBuf[0] > '7' || !memcmp(apTokens[iApnFld], "555", 3))
      //   break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(&acRollRec[0], "032531002000", 9) )
      //   iTmp = 0;
#endif

      // Replace null with space
      replNull(acRollRec);
      // 20240919 - Remove this line so we can fix known bad char.
      // replCharEx(acRollRec, 31, 32, 0);

      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Yol_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
            {
               lRet = Yol_MergeSitus(acBuf);
               // Rename special Street as needed
               Yol_FixSAddr(acBuf);
            }

            // Merge legal
            updateLegal(acBuf, m_strLegal);

            // Merge Lien
            //lRet = 1;
            //if (fdLienExt)
            //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

            // Merge Exe if not found in LienExt
            //if (fdExe && lRet)
            //   lRet = MB_MergeExe(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Yol_MergeStdChar(acBuf);
               //lRet = Yol_MergeChar(acBuf);

            // Remove old sale data
            //if (bClearSales)
            //   ClearOldSale(acBuf);

            // Merge Sales
            //if (fdSale)
            //   lRet = Yol_MergeSale(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acBuf);

            // Update Situs city & zip using Mailing if matching strname & strnum
            updateSitusCity(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, (char *)&acRollRec[1], lCnt);

         // Create new R01 record
         iRet = Yol_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
            {
               lRet = Yol_MergeSitus(acRec);
               // Rename special Street as needed
               Yol_FixSAddr(acRec);
            }

            // Merge legal
            updateLegal(acRec, m_strLegal);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Yol_MergeStdChar(acBuf);
               //lRet = Yol_MergeChar(acRec);

            // Merge Sales
            //if (fdSale)
            //   lRet = Yol_MergeSale(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acRec);

            // Update Situs city & zip using Mailing if matching strname & strnum
            updateSitusCity(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            iNewRec++;
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         // Trick to avoid error.  Do not remove this line.
         strcat(acRollRec, "\n");

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof && isdigit(acRollRec[1]))
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

#ifdef _DEBUG
      //if (!memcmp(&acRollRec[0], "910011828000", 9) )
      //   iTmp = 0;
#endif
      replNull(acRollRec);

      // Create new R01 record
      iRet = Yol_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
         {
            lRet = Yol_MergeSitus(acRec);
            // Rename special Street as needed
            Yol_FixSAddr(acRec);
         }

         // Merge legal
         updateLegal(acRec, m_strLegal);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            //lRet = Yol_MergeChar(acRec);
            lRet = Yol_MergeStdChar(acBuf);

         // Merge Sales
         //if (fdSale)
         //   lRet = Yol_MergeSale(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTax(acRec);

         // Update Situs city & zip using Mailing if matching strname & strnum
         updateSitusCity(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         bEof = true;    // Signal to stop
      else
         replCharEx(acRollRec, 31, 32, 0);
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   //if (fdSale)
   //   fclose(fdSale);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   //LogMsg("Number of Sale matched:     %u", lSaleMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   //LogMsg("Number of Sale skiped:      %u", lSaleSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Number of city corrected:   %u", lCityCorrected);
   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/******************************** Yol_MergeZone ******************************
 *
 * Merge Zoning from roll file (copy from MergeBut.cpp)
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Yol_MergeZone(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, MAX_RECSIZE, fdZone);
      if (acRec[1] > '9' || acRec[1] < '0')
         pRec = fgets(acRec, MAX_RECSIZE, fdZone);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdZone);
         fdZone = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Roll rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, MAX_RECSIZE, fdZone);
         if (!pRec)
            LogMsg("***** File may be corrupted.  Too many tokens %.12s", acRec);
         
         lZoneSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   replNull(acRec);
   if (cDelim == '|')
      iRet = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringNQ1(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdZone);
         fdZone = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input roll record for APN=%s", apTokens[0]);
         iRet = -1;
      }

      return iRet;
   }

   // Merge data
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      char *pTmp;

      pTmp = strchr(apTokens[MB_ROLL_ZONING], ' ');
      if (pTmp)
         *pTmp = 0;

      iTmp = strlen(apTokens[MB_ROLL_ZONING]);
      if (iTmp > 0)
      {
         vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE, iTmp);
         if (*(pOutbuf+OFF_ZONE_X1) == ' ')
            vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
      }

      // Recorded Doc
      if (*(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
      {
         char acTmp[256];

         pTmp = apTokens[MB_ROLL_DOCNUM];
         iRet = atol(pTmp+5);
         if (iRet > 0)
            sprintf(acTmp, "%.5s%.7d", pTmp, iRet);
         else
            strcpy(acTmp, apTokens[MB_ROLL_DOCNUM]);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, strlen(acTmp));

         pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
            memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
      }

      lZoneMatch++;
   }

   // Get next record
   pRec = fgets(acRec, MAX_RECSIZE, fdZone);

   return 0;
}

/********************************** Yol_FixApn ********************************
 *
 * Reformat APN from 999-999-99-9 to 999-999-999-999.
 *
 ******************************************************************************/

int Yol_FixApn(char *pCnty, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acTmp[32];
   char     acOutFile[_MAX_PATH], acRawFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet;
   int      iRet;
   long     lCnt=0;

   // Setup file names
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "O01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "S01");

   LogMsgD("Fix R01 file for %s.  Correcting APN & TRA", pCnty);

   // Open Lien extract file
   LogMsg("Open R01 file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   bRet = true;
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      // Format APN
      iRet = sprintf(acTmp, "%.6s0%.2s000", acBuf, &acBuf[6]);
      memcpy(&acBuf[OFF_APN_S], acTmp, iRet);
      iRet = formatApn(acBuf, acTmp, &myCounty);
      memcpy(&acBuf[OFF_APN_D], acTmp, iRet);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   return 0;
}

/******************************* convertChar() ******************************
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

int Yol_ConvertChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acOutBuf[1024], acInBuf[1024], acTmpFile[256], acTmp[256], *pRec;
   int      iRet, iTmp, iCnt=0;
   MB_CHAR  *pCharRec = (MB_CHAR *)&acOutBuf;

   LogMsgD("\nConverting char file %s\n", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   pRec = fgets(acInBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acInBuf, 1024, fdIn);

      if (!pRec)
         break;

      iRet = ParseStringNQ(pRec, cDelim, MB_CHAR_PARKTYPE+1, apTokens);
      if (iRet < MB_CHAR_PARKTYPE)
      {
         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset(acOutBuf, ' ', sizeof(MB_CHAR));
      memcpy(pCharRec->Asmt, apTokens[MB_CHAR_ASMT], strlen(apTokens[MB_CHAR_ASMT]));

      iTmp = atoi(apTokens[MB_CHAR_POOLS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->NumPools, acTmp, iRet);
      } else if (*apTokens[MB_CHAR_POOLS] >= 'A')
      {
         iTmp = strlen(apTokens[MB_CHAR_POOLS]);
         if (iTmp > MBSIZ_CHAR_POOLS) iTmp = MBSIZ_CHAR_POOLS;
         memcpy(pCharRec->NumPools, apTokens[MB_CHAR_POOLS], iTmp);
      }

      memcpy(pCharRec->LandUseCat, apTokens[MB_CHAR_USECAT], strlen(apTokens[MB_CHAR_USECAT]));
      memcpy(pCharRec->QualityClass, apTokens[MB_CHAR_QUALITY], strlen(apTokens[MB_CHAR_QUALITY]));
      memcpy(pCharRec->YearBuilt, apTokens[MB_CHAR_YRBLT], strlen(apTokens[MB_CHAR_YRBLT]));
      memcpy(pCharRec->BuildingSize, apTokens[MB_CHAR_BLDGSQFT], strlen(apTokens[MB_CHAR_BLDGSQFT]));
      memcpy(pCharRec->Heating, apTokens[MB_CHAR_HEATING], strlen(apTokens[MB_CHAR_HEATING]));
      memcpy(pCharRec->Cooling, apTokens[MB_CHAR_COOLING], strlen(apTokens[MB_CHAR_COOLING]));
      memcpy(pCharRec->HeatingSource, apTokens[MB_CHAR_HEATING_SRC], strlen(apTokens[MB_CHAR_HEATING_SRC]));
      memcpy(pCharRec->CoolingSource, apTokens[MB_CHAR_COOLING_SRC], strlen(apTokens[MB_CHAR_COOLING_SRC]));

      iTmp = atoi(apTokens[MB_CHAR_BEDS]);
      if (iTmp > 0)
         memcpy(pCharRec->NumBedrooms, apTokens[MB_CHAR_BEDS], strlen(apTokens[MB_CHAR_BEDS]));

      iTmp = atoi(apTokens[MB_CHAR_FBATHS]);
      if (iTmp > 0)
         memcpy(pCharRec->NumFullBaths, apTokens[MB_CHAR_FBATHS], strlen(apTokens[MB_CHAR_FBATHS]));

      iTmp = atoi(apTokens[MB_CHAR_HBATHS]);
      if (iTmp > 0)
         memcpy(pCharRec->NumHalfBaths, apTokens[MB_CHAR_HBATHS], strlen(apTokens[MB_CHAR_HBATHS]));

      iTmp = atoi(apTokens[MB_CHAR_FP]);
      if (iTmp > 0)
         memcpy(pCharRec->NumFireplaces, apTokens[MB_CHAR_FP], strlen(apTokens[MB_CHAR_FP]));

      memcpy(pCharRec->FeeParcel, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));

      if (*(apTokens[MB_CHAR_HASSEPTIC]) > '0')
         pCharRec->HasSeptic = 'Y';

      if (*(apTokens[MB_CHAR_HASSEWER]) > '0')
         pCharRec->HasSewer = 'Y';

      if (*(apTokens[MB_CHAR_HASWELL]) > '0')
         pCharRec->HasWell = 'Y';

      // Stories
      iTmp = atoi(apTokens[MB_CHAR_NUMFLOORS]);
      if (iTmp > 0)
         memcpy(pCharRec->Stories, apTokens[MB_CHAR_NUMFLOORS], strlen(apTokens[MB_CHAR_NUMFLOORS]));

      // LotSqft
      ULONG lSqft = atol(apTokens[MB_CHAR_LANDSQFT]);
      if (lSqft > 0)
         memcpy(pCharRec->LotSqft, apTokens[MB_CHAR_LANDSQFT], strlen(apTokens[MB_CHAR_LANDSQFT]));

      // Rooms
      iTmp = atoi(apTokens[MB_CHAR_TOTALROOMS]);
      if (iTmp > 0)
         memcpy(pCharRec->TotalRooms, apTokens[MB_CHAR_TOTALROOMS], strlen(apTokens[MB_CHAR_TOTALROOMS]));

      // Garage Sqft
      iTmp = atoi(apTokens[MB_CHAR_GARSQFT]);
      if (iTmp > 0)
      {
         if (iTmp < 100)
         {
            memcpy(pCharRec->ParkSpaces, apTokens[MB_CHAR_GARSQFT], strlen(apTokens[MB_CHAR_GARSQFT]));
            pCharRec->ParkType[0] = '2';
         } else
         {
            memcpy(pCharRec->SqFTGarage, apTokens[MB_CHAR_GARSQFT], strlen(apTokens[MB_CHAR_GARSQFT]));
            pCharRec->ParkType[0] = 'Z';
         }
      }

      // Garage spaces
      iTmp = atoi(apTokens[MB_CHAR_PARKTYPE]);
      if (iTmp > 0)
         memcpy(pCharRec->ParkSpaces, apTokens[MB_CHAR_PARKTYPE], strlen(apTokens[MB_CHAR_PARKTYPE]));
      else 
      {
         /*
         F        1
         G        7
         N        76
         P        1
         S        1
         U        5
         X        1
         Y        4
         */
         switch(*(apTokens[MB_CHAR_PARKTYPE]))
         {
            case 'C':
               pCharRec->ParkType[0] = 'C';     // Carport
               break;
            case 'N':
               pCharRec->ParkType[0] = 'H';     // None
               break;
            case 'G':
               pCharRec->ParkType[0] = 'Z';     // Garage
               break;
            case 'O':
               pCharRec->ParkType[0] = 'O';     // Other
               break;
            default:
               break;
         }
      }

      pCharRec->CRLF[0] = '\n';
      pCharRec->CRLF[1] = '\0';
      fputs(acOutBuf, fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      //iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D,461,8,C,D) DUPO(B2000,)");
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A)");
   } else
   {
      printf("\n");
      iRet = 0;
   }

   return iRet;
}

/* not used
int Yol_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[4], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsgD("Yol_ConvStdChar - Converting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 4096, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);

      if (!pRec)
         break;

      // Parse string
      replStrAll(acBuf, "NULL", "");
      if (cDelim == '|')
         iFldCnt = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

      if (iFldCnt < YOL_CHAR_SALESPRICE)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      // Event Date
      if (*apTokens[YOL_CHAR_DOCNUM] > ' ' || *apTokens[YOL_CHAR_ASMT] == ' ')
         continue;

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[YOL_CHAR_ASMT], strlen(apTokens[YOL_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[YOL_CHAR_FEEPRCL], strlen(apTokens[YOL_CHAR_FEEPRCL]));
      // Format APN
      iRet = formatApn(apTokens[YOL_CHAR_ASMT], acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      // Bldg#
      iTmp = atoi(apTokens[YOL_CHAR_BLDGSEQNO]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Bldg Type
      //iTmp = blankRem(apTokens[YOL_CHAR_BLDGTYPE]);
      //if (iTmp > 1)
      //{
      //   pRec = findXlatCode(apTokens[YOL_CHAR_POOLSPA], &asBldgType[0]);
      //   if (pRec)
      //      myCharRec.BldgType[0] = *pRec;
      //}

      // BldgSize
      int iBldgSize = atoi(apTokens[YOL_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[YOL_CHAR_UNITSCNT]);
      if (iTmp > 0 && iBldgSize > iTmp*100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } else if (iTmp > 1)
         LogMsg("*** Questionable UnitsCnt: %d, BldgSqft=%d, Apn=%s", iTmp, iBldgSize, apTokens[YOL_CHAR_ASMT]);

      // Unit# - currently all 0
      iTmp = atoi(apTokens[YOL_CHAR_UNITSEQNO]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.UnitSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** UnitSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[YOL_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[YOL_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[YOL_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
         //memcpy(myCharRec.Bath_4Q, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[YOL_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
         //memcpy(myCharRec.Bath_2Q, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "035410015000", 9))
      //   iRet = 0;
#endif
      // Pool 
      iTmp = blankRem(apTokens[YOL_CHAR_POOLSPA]);
      if (*apTokens[YOL_CHAR_POOLSPA] > ' ')
      {
         pRec = findXlatCode(apTokens[YOL_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass
      iTmp = blankRem(apTokens[YOL_CHAR_QUALITYCLASS]);
      pRec = _strupr(apTokens[YOL_CHAR_QUALITYCLASS]);
      if (*apTokens[YOL_CHAR_QUALITYCLASS] > ' ' && *apTokens[YOL_CHAR_QUALITYCLASS] <= 'Z')
      {
         iTmp = strlen(apTokens[YOL_CHAR_QUALITYCLASS]);
         memcpy(myCharRec.QualityClass, apTokens[YOL_CHAR_QUALITYCLASS], iTmp);

         acCode[0] = ' ';
         strcpy(acTmp, apTokens[YOL_CHAR_QUALITYCLASS]);
         if (isalpha(acTmp[0])) 
         {
            myCharRec.BldgClass = acTmp[0];

            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
            else if (isdigit(acTmp[2]))
               iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
            else if (!_memicmp(acTmp, "ST", 2))
               memcpy(myCharRec.QualityClass, "ST", 2);     // Steel
            else
            {
               pRec = strchr(acTmp, ' ');
               if (pRec)
               {
                  pRec++;
                  if (*pRec > '0' && *pRec <= '9')
                     iRet = Quality2Code(pRec, acCode, NULL);
                  else
                     LogMsg("*** Unknown QUALITYCLASS: '%s' in [%s]", apTokens[YOL_CHAR_QUALITYCLASS], apTokens[YOL_CHAR_ASMT]);
               }
            }
         } else if (isdigit(acTmp[0]))
            iRet = Quality2Code(acTmp, acCode, NULL);
         else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[YOL_CHAR_QUALITYCLASS], apTokens[YOL_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (*apTokens[YOL_CHAR_QUALITYCLASS] > ' ' && *apTokens[YOL_CHAR_QUALITYCLASS] != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[YOL_CHAR_QUALITYCLASS], apTokens[YOL_CHAR_ASMT]);

      // Improved Condition
      if (*apTokens[YOL_CHAR_CONDITION] > '0')
      {
         pRec = findXlatCode(apTokens[YOL_CHAR_CONDITION], &asCond[0]);
         if (pRec)
            myCharRec.ImprCond[0] = *pRec;
      }

      // YrBlt
      int iYrBlt = atoi(apTokens[YOL_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[YOL_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[YOL_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
      }

      // Detached SF
      int iDetGar = atoi(apTokens[YOL_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
      }

      // Carport SF
      int iCarport = atoi(apTokens[YOL_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
      }

      // LandSqft is not populated, use Acres (same in AMA)
      double dAcres = atof(apTokens[YOL_CHAR_ACRES]);
      long lLotSqft = atol(apTokens[YOL_CHAR_LANDSQFT]);
      if (dAcres > 0.0)
      {
         iRet = sprintf(acTmp, "%d", (unsigned long)(dAcres*1000.0));
         memcpy(myCharRec.LotAcre, acTmp, iRet);

         iRet = sprintf(acTmp, "%d", (long)(dAcres * SQFT_PER_ACRE));
         memcpy(myCharRec.LotSqft, acTmp, iRet);
      } 
      
      if (lLotSqft > 100)
      {
         iRet = sprintf(acTmp, "%d", lLotSqft);
         memcpy(myCharRec.LotSqft, acTmp, iRet);

         if (dAcres == 0.0)
         {
            dAcres = lLotSqft * 1;
            iRet = sprintf(acTmp, "%d", (unsigned long)(lLotSqft*SQFT_MF_1000+0.5));
            memcpy(myCharRec.LotAcre, acTmp, iRet);
         }
      }

      // Park space - it's in Garage field
      //iTmp = atoi(apTokens[YOL_CHAR_PARKINGSPACES]);
      //if (iTmp > 0)
      //{
      //   iRet = sprintf(acTmp, "%d", iTmp);
      //   memcpy(myCharRec.ParkSpace, acTmp, iRet);
      //}

      // Garage Type
      if (*apTokens[YOL_CHAR_GARAGE] > ' ')
      {
         iTmp = atoi(apTokens[YOL_CHAR_GARAGE]);
         if (iTmp > 0)
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(myCharRec.ParkSpace, acTmp, iRet);
         } else 
         {
            _toupper(*apTokens[YOL_CHAR_GARAGE]);
            pRec = findXlatCode(apTokens[YOL_CHAR_GARAGE], &asParkType[0]);
            if (pRec)
               myCharRec.ParkType[0] = *pRec;
         }
      }

      if (myCharRec.ParkType[0] == ' ')
      {
         if (iAttGar > 100)
            myCharRec.ParkType[0] = 'A';
         else if (iDetGar > 100)
            myCharRec.ParkType[0] = 'D';
         else if (iCarport > 100)
            myCharRec.ParkType[0] = 'C';
      }

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[YOL_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[YOL_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[YOL_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[YOL_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[YOL_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[YOL_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // FirePlace - 1, 2, 3, 5
      if (*(apTokens[YOL_CHAR_FIREPLACE]) > '0')
      {
         blankRem(apTokens[YOL_CHAR_FIREPLACE]);
         pRec = findXlatCode(apTokens[YOL_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Misc.sExtra.FirePlaceType[0] = *pRec;
      }

      // #fireplaces - This field is temporary and may be removed 5/25/2015
      iTmp = atol(apTokens[YOL_CHAR_NUMFIREPLACE]);
      if (iTmp > 0 && iTmp < 9)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Fireplace, acTmp, iRet);
      }

      // Sewer
      blankRem(apTokens[YOL_CHAR_SEWERCODE]);
      if (*apTokens[YOL_CHAR_SEWERCODE] > '0')
      {
         myCharRec.HasSewer = 'Y';
         pRec = findXlatCode(apTokens[YOL_CHAR_SEWERCODE], &asSewer[0]);
         if (pRec)
            myCharRec.Sewer = *pRec;
      }
    
      // Water
      blankRem(apTokens[YOL_CHAR_HASWELL]);
      if (*(apTokens[YOL_CHAR_HASWELL]) == 'T')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      //} else if (*(apTokens[YOL_CHAR_WATERSOURCE]) > '0')
      //{
      //   blankRem(apTokens[YOL_CHAR_WATERSOURCE]);
      //   pRec = findXlatCode(apTokens[YOL_CHAR_WATERSOURCE], &asWaterSrc[0]);
      //   if (pRec)
      //   {
      //      myCharRec.HasWater = *pRec;
      //      if (*pRec == 'W')
      //         myCharRec.HasWell = 'Y';
      //   }
      }

      // OfficeSpaceSF
      iTmp = atoi(apTokens[YOL_CHAR_OFFICESPACESF]);
      if (iTmp > 10)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Misc.sExtra.OfficeSqft, acTmp, iRet);
      }

      // NonConditionSF

      // ViewCode
      if (*apTokens[YOL_CHAR_VIEWCODE] >= 'A')
      {
         pRec = findXlatCode(apTokens[YOL_CHAR_VIEWCODE], &asView[0]);
         if (pRec)
            myCharRec.View[0] = *pRec;
      }

      // Stories/NumFloors
      iTmp = atoi(apTokens[YOL_CHAR_STORIESCNT]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Zoning
      if (*apTokens[YOL_CHAR_ZONING] >= '0')
         memcpy(myCharRec.Zoning, apTokens[YOL_CHAR_ZONING], strlen(apTokens[YOL_CHAR_ZONING]));

      // SubDiv Name
      if (*apTokens[YOL_CHAR_SUBDIVNAME] >= '0')
      {
         iTmp = strlen(apTokens[YOL_CHAR_SUBDIVNAME]);
         if (iTmp > sizeof(myCharRec.Misc.SubDiv))
            iTmp = sizeof(myCharRec.Misc.SubDiv);
         memcpy(myCharRec.Misc.SubDiv, apTokens[YOL_CHAR_SUBDIVNAME], iTmp);
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ," ") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/**************************** Yol_ConvStdChar ********************************
 *
 * Copy from MergeMer.cpp to convert new char file. 07/14/2015
 * Other counties has similar format: MER, YOL, HUM, MAD, MNO, SBT
 *
 *****************************************************************************/

int Yol_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], acQualCls[16], *pRec, cCharSep;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Create output file %s", acTmpFile);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Pull CHAR delimiter from INI file
   GetIniString(myCounty.acCntyCode, "CharSep", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] > ' ')
      cCharSep = acTmp[0];
   else
      cCharSep = cDelim;

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec)
         break;
      if (acBuf[1] < '0' || acBuf[1] > '9')
         continue;
      replNull(acBuf);

      if (cCharSep == '|')
         iFldCnt = ParseStringIQ(pRec, cCharSep, MAX_FLD_TOKEN, apTokens);
      else
         iFldCnt = ParseStringNQ(pRec, cCharSep, MAX_FLD_TOKEN, apTokens);

      if (iFldCnt < YOL_CHAR_HASWELL)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[YOL_CHAR_ASMT], iTrim(apTokens[YOL_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[YOL_CHAR_FEEPARCEL], iTrim(apTokens[YOL_CHAR_FEEPARCEL]));

      // Format APN
      if (*apTokens[YOL_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[YOL_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[YOL_CHAR_FEEPARCEL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[YOL_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[YOL_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool - 01, 02, 03, 99
      iTmp = blankRem(apTokens[YOL_CHAR_POOLSPA]);
      if (iTmp > 1)
      {
         pRec = findXlatCode(apTokens[YOL_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass 
      strcpy(acTmp, _strupr(apTokens[YOL_CHAR_QUALITYCLASS]));
      iTmp = remChar(acTmp, ' ');    // Remove all blanks before process
      if (acTmp[0] == 'X')
      {
         if (iTmp > 4 && acTmp[iTmp-1] == 'X')
            acTmp[iTmp-1] = 0;
         strcpy(acQualCls, &acTmp[1]);
      } else
         strcpy(acQualCls, acTmp);

      if (acQualCls[0] > ' ' && acQualCls[0] <= 'Z')
      {
         acCode[0] = ' ';
         if (isalpha(acQualCls[0])) 
         {
            myCharRec.BldgClass = acQualCls[0];

            if (isdigit(acQualCls[1]))
               iRet = Quality2Code((char *)&acQualCls[1], acCode, NULL);
            else if (isdigit(acQualCls[2]))
               iRet = Quality2Code((char *)&acQualCls[2], acCode, NULL);
            else if (!_memicmp(acTmp, "ST", 2))
               memcpy(myCharRec.QualityClass, "ST", 2);     // Steel
         } else if (isdigit(acQualCls[0]))
            iRet = Quality2Code(acQualCls, acCode, NULL);
         else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", acQualCls, apTokens[YOL_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (acQualCls[0] > ' ' && acQualCls[0] != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[YOL_CHAR_QUALITYCLASS], apTokens[YOL_CHAR_ASMT]);

      int iYrBlt = atoi(apTokens[YOL_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[YOL_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[YOL_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[YOL_CHAR_UNITSCNT]);
      //if (iTmp > 0 && iBldgSize > iTmp*100)
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 
      //else if (iTmp > 1)
      //   LogMsg("*** Questionable UnitsCnt: %d, BldgSqft=%d, Apn=%s", iTmp, iBldgSize, apTokens[YOL_CHAR_ASMT]);

      // Stories/NumFloors
      iTmp = atoi(apTokens[YOL_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 20)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[YOL_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[YOL_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport SF
      int iCarport = atoi(apTokens[YOL_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[YOL_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCodeA(apTokens[YOL_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[YOL_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[YOL_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[YOL_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[YOL_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[YOL_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[YOL_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[YOL_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace - 1, 2, 99, y
      if (*apTokens[YOL_CHAR_FIREPLACE] > '0' && *apTokens[YOL_CHAR_FIREPLACE] < '9')
         myCharRec.Fireplace[0] = *apTokens[YOL_CHAR_FIREPLACE];

      // Patio SF
      iTmp = atoi(apTokens[YOL_CHAR_PATIOSF]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // Haswell - 1, 0
      blankRem(apTokens[YOL_CHAR_HASWELL]);
      if (*(apTokens[YOL_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "018330028000", 9))
      //   iRet = 0;
#endif

      // Land Sqft
      if (iFldCnt > YOL_CHAR_LANDSQFT)
      {
         ULONG    lSqft;
         lSqft = (ULONG)atol(apTokens[YOL_CHAR_LANDSQFT]);
         if (lSqft > 1)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(lSqft*SQFT_MF_1000);
            iTmp = sprintf(acTmp, "%u", (long)(dTmp+0.1));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      // ParkSpaces
      if (iFldCnt > YOL_CHAR_PARKSPACES)
      {
         iTmp = atoi(apTokens[YOL_CHAR_PARKSPACES]);
         if (iTmp > 1 && iTmp < 9999)
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(myCharRec.ParkSpace, acTmp, iRet);
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum, UnitSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/******************************** Yol_MergeLien3 *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * For 2019, add AgPreserve
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Yol_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L3_QUALITYCLASS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "57YOL", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&YOL_Exemption);

   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Yol_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   //Yol_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Yol_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4], NULL, NULL);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Set DocNum index
   if (iTokens > L3_CURRENTDOCNUM)
      iTmp = L3_CURRENTDOCNUM;         // 2017
   else
      iTmp = L3_ACRES+1;               // 2017

   // Recorded Doc
   if (*(apTokens[iTmp]+4) == 'R')
   {
      pTmp = dateConversion(apTokens[iTmp+1], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         lTmp = sprintf(acTmp, "%.5s%.7d", apTokens[iTmp], atol(apTokens[iTmp]+5));
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, lTmp);
      }
   } 

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // AgPreserved
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   return 0;
}

/******************************** Yol_Load_LDR3 *****************************
 *
 * Load LDR 2016
 *
 ****************************************************************************/

int Yol_Load_LDR3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acLDRFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acLDRFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acLDRFile);
   if (lTmp < lToday)
   {
      if (cLdrSep < ' ')
         sprintf(acRec, "S(#3,C,A) DEL(%d)", cLdrSep);
      else
         sprintf(acRec, "S(#3,C,A) DEL(%c)", cLdrSep);
      iRet = sortFile(acTmpFile, acLDRFile, acRec);  // 2016
      if (!iRet)
         return -1;
   }

   // Open LDR file
   LogMsg("Open LDR file %s", acLDRFile);
   if (!(fdLDR = fopen(acLDRFile, "r")))
   {
      LogMsg("***** Error opening LDR file: %s\n", acLDRFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Value file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Value file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   } else
      fdLienExt = NULL;

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(1,13,C,A) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   if (!(fdSitus = fopen(acTmpFile, "r")))
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Zoning file
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acTmpFile, acRec, myCounty.acCntyCode, myCounty.acCntyCode);
   if (acTmpFile[0] > ' ')
   {
      LogMsg("Open Zoning file %s", acTmpFile);
      fdZone = fopen(acTmpFile, "r");
      if (fdZone == NULL)
      {
         LogMsg("***** Error opening Zoning file: %s\n", acTmpFile);
         return -2;
      }
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      // Create new R01 record
      iRet = Yol_MergeLien3(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         if (fdLienExt)
            lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Situs
         if (fdSitus)
            lRet = Yol_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Yol_MergeStdChar(acBuf);

         // Merge Zoning
         if (fdZone)
            lRet = Yol_MergeZone(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdLDR)
      fclose(fdLDR);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdZone)
      fclose(fdZone);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Zoning matched:   %u", lZoneMatch);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Zoning skiped:    %u", lZoneSkip);
   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************* Yol_MergeLien15 *****************************
 *
 * For 2021 LDR file 2021_secured_tr.txt
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Yol_MergeLien15(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L15_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L15_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L15_ASMT], strlen(apTokens[L15_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L15_FEEPARCEL], strlen(apTokens[L15_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L15_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L15_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "57YOL", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L15_ASMTSTATUS];

   // TRA
   lTmp = atol(apTokens[L15_TRA]);
   iRet = sprintf(acTmp, "%.6d", lTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L15_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L15_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L15_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L15_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L15_GROWING]);
   long lPers  = atoi(apTokens[L15_PPVALUE]);
   long lPP_MH = atoi(apTokens[L15_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L15_HOX]);
   long lExe2 = atol(apTokens[L15_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L15_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L15_OTHEREXEMPTIONCODE], strlen(apTokens[L15_OTHEREXEMPTIONCODE]));

   // Legal
   updateLegal(pOutbuf, apTokens[L15_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L15_LANDUSE1] > ' ')
   {
      lTmp = atol(apTokens[L15_LANDUSE1]);
      sprintf(acTmp, "%.3d", lTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, 3);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 3, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L15_ACRES]);
   lTmp = atol(apTokens[L15_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Yol_MergeOwner(pOutbuf, apTokens[L15_OWNER]);

   // Situs

   // Mailing
   Yol_MergeMAdr(pOutbuf, apTokens[L15_MAILADDRESS1], apTokens[L15_MAILADDRESS2], apTokens[L15_MAILADDRESS3], apTokens[L15_MAILADDRESS4], NULL, NULL);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L15_TAXABILITYFULL], true, true);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // AgPreserved
   if (*apTokens[L15_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   return 0;
}

/******************************* Yol_Load_LDR15 *****************************
 *
 * Load LDR 2021
 *
 ****************************************************************************/

int Yol_Load_LDR15(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acLDRFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort roll file on ASMT
   sprintf(acLDRFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acLDRFile);
   if (lTmp < lToday)
   {
      if (cLdrSep < ' ')
         sprintf(acRec, "S(#3,C,A) DEL(%d)", cLdrSep);
      else
         sprintf(acRec, "S(#3,C,A) DEL(%c)", cLdrSep);
      iRet = sortFile(acTmpFile, acLDRFile, acRec);  // 2016
      if (!iRet)
         return -1;
   }

   // Open LDR file
   LogMsg("Open LDR file %s", acLDRFile);
   if (!(fdLDR = fopen(acLDRFile, "r")))
   {
      LogMsg("***** Error opening LDR file: %s\n", acLDRFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(1,13,C,A) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   if (!(fdSitus = fopen(acTmpFile, "r")))
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Zoning file
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acTmpFile, acRec, myCounty.acCntyCode, myCounty.acCntyCode);
   if (acTmpFile[0] > ' ')
   {
      LogMsg("Open Zoning file %s", acTmpFile);
      fdZone = fopen(acTmpFile, "r");
      if (fdZone == NULL)
      {
         LogMsg("***** Error opening Zoning file: %s\n", acTmpFile);
         return -2;
      }
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      // Create new R01 record
      iRet = Yol_MergeLien15(acBuf, acRec);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Yol_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Yol_MergeStdChar(acBuf);

         // Merge Zoning
         if (fdZone)
            lRet = Yol_MergeZone(acBuf);

         // Save last recording date - DocDate not populated
         //lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         //if (lRet > lLastRecDate && lRet < lToday)
         //   lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp || acRec[0] > '9')
         break;      // EOF

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdLDR)
      fclose(fdLDR);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdZone)
      fclose(fdZone);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Zoning matched:   %u", lZoneMatch);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Zoning skiped:    %u", lZoneSkip);
   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/****************************** Yol_ExtrLien ********************************
 *
 * Create lien extract file for LDR 2021 (copy from MergeDnx.cpp)
 *
 ****************************************************************************/

int Yol_ExtrLdrRec15(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L15_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L15_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L15_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[L15_ASMT], strlen(apTokens[L15_ASMT]));

   // TRA
   lTmp = atol(apTokens[L15_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L15_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L15_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixt  = atoi(apTokens[L15_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L15_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L15_GROWING]);
   long lPers  = atoi(apTokens[L15_PPVALUE]);
   long lPP_MH = atoi(apTokens[L15_MHPPVALUE]);
   lTmp = lFixt+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L15_HOX]);
   long lExe2 = atol(apTokens[L15_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  

   iRet = 1;
   if (lExe1 > 0)
   {
      pLienExtr->acHO[0] = '1';      // 'Y'
      memcpy(pLienExtr->extra.MB.ExeCode1, "E01", 3);
      iRet = 2;
   } else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L15_OTHEREXEMPTIONCODE] > ' ')
   {
      if (iRet == 1)
         vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L15_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
      else
         vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L15_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
   }

   // Taxability
   lTmp = atoin(apTokens[L15_TAXABILITYFULL], 3);
   if (lTmp > 0)
   {
      // Prop 8 
      if (lTmp > 799 && lTmp < 900)
         pLienExtr->SpclFlag = LX_PROP8_FLG;

      iRet = sprintf(acTmp, "%.3d", lTmp);
      memcpy(pLienExtr->acTaxCode, acTmp, iRet);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

int Yol_ExtrLien(char *pCnty)
{
   char     *pTmp, acBuf[256], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   long     iRet, lCnt=0;
   FILE     *fdLien;

   LogMsg0("Extract lien values");

   // Open roll file
   GetIniString(pCnty, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
   LogMsg("Extract lien data from Roll file %s", acBuf);
   fdRoll = fopen(acBuf, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acBuf);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (acRollRec[0] > '9' || acRollRec[0] < '0');

   bool bEof = (pTmp ? false:true);

   // Merge loop
   while (!feof(fdRoll))
   {
      iRet = Yol_ExtrLdrRec15(acBuf, acRollRec);
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLien);

         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      } else
         LogMsg("Skip rec: %.50s", acRollRec);

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp || acRollRec[0] > '9')
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("\nTotal lien records extracted:    %u", lCnt);

   return 0;
}

/******************************* Yol_MergeLien17 *****************************
 *
 * For 2023 LDR AGENCYCDCURRSEC_TR601.TAB
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Yol_MergeLien17(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L17_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L17_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L17_ASMT], strlen(apTokens[L17_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L17_FEEPARCEL], strlen(apTokens[L17_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L17_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L17_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "57YOL", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L17_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L17_TRA], strlen(apTokens[L17_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   ULONG lLand = atoi(apTokens[L17_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   ULONG lImpr = atoi(apTokens[L17_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   ULONG lFixtr = atoi(apTokens[L17_FIXTURESVALUE]);
   ULONG lFixtRP= atoi(apTokens[L17_FIXTURESRP]);
   ULONG lGrow  = atoi(apTokens[L17_GROWINGVALUE]);
   ULONG lPers  = atoi(apTokens[L17_PPVALUE]);
   ULONG lPP_MH = atoi(apTokens[L17_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%u         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%u         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%u         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   ULONG lExe1 = atol(apTokens[L17_HOX]);
   ULONG lExe2 = atol(apTokens[L17_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L17_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L17_OTHEREXEMPTIONCODE], strlen(apTokens[L17_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&YOL_Exemption);

   // Legal
   updateLegal(pOutbuf, apTokens[L17_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L17_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L17_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L17_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L17_ACRES]);
   lTmp = atol(apTokens[L17_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
      if (lTmp < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }

      // Format Acres
      lTmp = (ULONG)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      if (lTmp < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }
      lTmp = (ULONG)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Yol_MergeOwner(pOutbuf, apTokens[L17_OWNER]);

   // Situs
   //Yol_MergeSitus(pOutbuf, apTokens[L17_SITUS1], apTokens[L17_SITUS2]);

   // Mailing
   Yol_MergeMAdr(pOutbuf, apTokens[L17_MAILADDRESS1], apTokens[L17_MAILADDRESS2], apTokens[L17_MAILADDRESS3], apTokens[L17_MAILADDRESS4], NULL, NULL);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L17_TAXABILITYFULL], true, true);

   // Set DocNum index
   if (iTokens > L17_CURRENTDOCNUM)
      iTmp = L17_CURRENTDOCNUM;         // 2017
   else
      iTmp = L17_ACRES+1;               // 2017

   // Recorded Doc
   if (*(apTokens[iTmp]+4) == 'R')
   {
      pTmp = dateConversion(apTokens[iTmp+1], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         lTmp = sprintf(acTmp, "%.5s%.7d", apTokens[iTmp], atol(apTokens[iTmp]+5));
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, lTmp);
      }
   } 

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // AgPreserved
   if (*apTokens[L17_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   return 0;
}

/******************************* Yol_Load_LDR17 *****************************
 *
 * Load LDR 2023 - AGENCYCDCURRSEC_TR601.txt (ldrgrp 17)
 *
 ****************************************************************************/

int Yol_Load_LDR17(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acLDRFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);

   // Sort roll file on ASMT
   sprintf(acLDRFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acLDRFile);
   if (lTmp < lToday)
   {
      sprintf(acRec, "S(#3,C,A) DEL(%d)", cLdrSep);
      iRet = sortFile(acTmpFile, acLDRFile, acRec);  // 2016
      if (!iRet)
         return -1;
   }

   // Open LDR file
   LogMsg("Open LDR file %s", acLDRFile);
   if (!(fdLDR = fopen(acLDRFile, "r")))
   {
      LogMsg("***** Error opening LDR file: %s\n", acLDRFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Value file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Value file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   } else
      fdLienExt = NULL;

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acTmpFile);
   if (lTmp < lToday)
   {
      strcpy(acRec, "S(1,13,C,A) ");
      lRet = sortFile(acSitusFile, acTmpFile, acRec);
   }
   if (!(fdSitus = fopen(acTmpFile, "r")))
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Zoning file
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRollFile, _MAX_PATH, acIniFile);
   if (acRollFile[0] > ' ')
   {
      sprintf(acTmpFile, "%s\\%s\\%s_roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      sprintf(acRec, "S(#1,C,A) DEL(%d)", cDelim);
      iRet = sortFile(acRollFile, acTmpFile, acRec); 

      LogMsg("Open Zoning file %s", acTmpFile);
      fdZone = fopen(acTmpFile, "r");
      if (fdZone == NULL)
      {
         LogMsg("***** Error opening Zoning file: %s\n", acTmpFile);
         return -2;
      }
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      // Create new R01 record
      iRet = Yol_MergeLien17(acBuf, acRec);
      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Yol_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Yol_MergeStdChar(acBuf);

         // Merge Zoning
         if (fdZone)
            lRet = Yol_MergeZone(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdLDR)
      fclose(fdLDR);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdZone)
      fclose(fdZone);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Zoning matched:   %u", lZoneMatch);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Zoning skiped:    %u", lZoneSkip);
   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/****************************** Yol_CreateGrGrRec ****************************
 *
 * Create YOL GrGr record.
 * For multi-apn transaction, APN field can more than 1 APN
 * Grantor & Grantee fields may have more than 1 name
 *
 * Return >0 if successful, <0 if error
 *
 *****************************************************************************/

int Yol_CreateGrGrRec(char *pRec, FILE *fdGrOut)
{
   char     *pTmp, acTmp[MAX_RECSIZE*2], acDate[16], acOutbuf[1024], *apItems[1024], *apOwners[1024], *apApns[1024];
   int      iTmp, iRet, iApnCnt;
   ULONG    lSalePrice;
   double   dTax;

   GRGR_DOC *pGrGrRec = (GRGR_DOC *)&acOutbuf[0];

   //strcpy(acTmp, pRec);
   iTmp = ParseStringNQ1(pRec, '|', YOL_GRGR_FLDCNT+2, apItems);
   if (iTmp >= YOL_GRGR_FLDCNT)
   {
      if (*apItems[YOL_GRGR_APN] < '0')
         return 0;

      // Init new record
      memset(acOutbuf, ' ', sizeof(GRGR_DOC));
      pGrGrRec->NoneSale = 'Y';
      pGrGrRec->NoneXfer = 'Y';

      // Check for multi APN
      iApnCnt = ParseString(apItems[YOL_GRGR_APN], ',', 1024, apApns);
      if (iApnCnt > 1)
         memcpy(pGrGrRec->APN, apApns[0], strlen(apApns[0]));
      else
         memcpy(pGrGrRec->APN, apItems[YOL_GRGR_APN], strlen(apItems[YOL_GRGR_APN]));

      // Doc Tax
      dTax = atof(apItems[YOL_GRGR_DOCTAX]);
      if (dTax > 0)
      {
         iTmp = sprintf(acTmp, "%.2f", dTax);
         memcpy(pGrGrRec->DocTax, acTmp, iTmp);
         lSalePrice = (ULONG)(dTax * SALE_FACTOR);
      } else
         lSalePrice = 0;

#ifdef _DEBUG
      // 008221031, 038203012
      //if (!memcmp(pGrGrRec->APN, "038203012", 9) || !memcmp(pGrGrRec->APN, "008221031", 9))
      //   iTmp = 0;
#endif

      // DocType
      memcpy(pGrGrRec->DocTitle, apItems[YOL_GRGR_DOCTITLE], strlen(apItems[YOL_GRGR_DOCTITLE]));
      if (lSalePrice > 0 && !strcmp(apItems[YOL_GRGR_DOCTYPE], "DEED"))
         pGrGrRec->DocType[0] = '1';
      else
      {
         strcpy(acTmp, apItems[YOL_GRGR_DOCTITLE]);
         iRet = XrefNameIndex((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
         if (iRet >= 0)
         {
            pGrGrRec->NoneSale = asDeed[iRet].acFlags[0];
            pGrGrRec->NoneXfer = asDeed[iRet].acOther[0];
            iRet = sprintf(acTmp, "%d", asDeed[iRet].iIdxNum);
            memcpy(pGrGrRec->DocType, acTmp, iRet);
         } else if (acTmp[0] > ' ')
         {
            LogMsg("*** YOL GRGR - Unknown DocTitle: %s [%s]", acTmp, apItems[YOL_GRGR_APN]);
            memcpy(pGrGrRec->DocType, "19", 2);
         }
      }

      // DocDate, DocNum
      if (*(apItems[YOL_GRGR_DOCNUM]+4) == '-')
      {
         pTmp = dateConversion(apItems[YOL_GRGR_RECDATE], acDate, MM_DD_YYYY_1);
         if (pTmp)
         {
            memcpy(pGrGrRec->DocDate, pTmp, 8);
            iTmp = atol(pTmp);
            if (iTmp > lLastRecDate)
               lLastRecDate = iTmp;
         } else
            LogMsg("*** Invalid RecDate: %s - %s", apItems[YOL_GRGR_RECDATE], apApns[0]);

         iRet = sprintf(acTmp, "%.4sR%.7s", apItems[YOL_GRGR_DOCNUM], apItems[YOL_GRGR_DOCNUM]+5);
         memcpy(pGrGrRec->DocNum, acTmp, iRet);    
      } else
            LogMsg("*** Invalid DocNum: %s - %s", apItems[YOL_GRGR_DOCNUM], apApns[0]);

      // Grantor
      pTmp = strcpy(acTmp, apItems[YOL_GRGR_GRANTOR]);
      iRet = ParseString(acTmp, ',', 1024, apOwners);
      if (iRet > 0)
         for (iTmp = 0; iTmp < iRet && iTmp < 2; iTmp++)
            memcpy(pGrGrRec->Grantor[iTmp], apOwners[iTmp], strlen(apOwners[iTmp]));

      // Grantee
      pTmp = strcpy(acTmp, apItems[YOL_GRGR_GRANTEE]);
      iRet = ParseString(acTmp, ',', 1024, apOwners);
      if (iRet > 0)
         for (iTmp = 0; iTmp < iRet && iTmp < 2; iTmp++)
            memcpy(pGrGrRec->Grantee[iTmp], apOwners[iTmp], strlen(apOwners[iTmp]));
      iTmp = sprintf(acTmp, "%d", iRet);
      memcpy(pGrGrRec->NameCnt, acTmp, iTmp);

      if (iApnCnt > 1)
         pGrGrRec->MultiApn = 'Y';
      pGrGrRec->CRLF[0] = '\n';
      pGrGrRec->CRLF[1] = '\0';
      fputs(acOutbuf, fdGrOut);
      for (iTmp = 1; iTmp < iApnCnt; iTmp++)
      {
         memset(pGrGrRec->APN, ' ', sizeof(pGrGrRec->APN));
         memcpy(pGrGrRec->APN, apApns[iTmp], strlen(apApns[iTmp]));
         fputs(acOutbuf, fdGrOut);
      }

      iRet = iApnCnt;
   } else
   {
      LogMsg("*** Bad record: %s (tokens=%d)", acTmp, iTmp);
      iRet = -1;
   }

   return iRet;
}

/********************************* Yol_LoadGrGr *****************************
 *
 * Input record is very long sometimes.  To be safe, make it larger than normal.
 *
 * If successful, return 0.  Otherwise error.
 *
 ****************************************************************************/

int Yol_LoadGrGrCsv(char *pInfile, FILE *fdOut)
{
   char     *pTmp, acTmp[256], acRec[MAX_RECSIZE*4], acOutbuf[1024];
   int      iRet, iCnt, lCnt, iGrGrRecSize=MAX_RECSIZE*4;

   FILE     *fdIn;
   GRGR_DOC  *pGrGrRec = (GRGR_DOC *)&acOutbuf[0];

   LogMsg("Process GrGr file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("*** Error opening file: %s", pInfile);
      return 0;
   }

   // Remove header
   pTmp = fgets(acRec, iGrGrRecSize, fdIn);

   // Update GrGr file date
   iRet = getFileDate(acTmp);
   if (iRet > lLastGrGrDate)
      lLastGrGrDate = iRet;

   // Initialize pointers
   iCnt = lCnt = 0;
   while (fdIn && !feof(fdIn))
   {
      pTmp = fgets(acRec, iGrGrRecSize, fdIn);
      if (!pTmp) break;

      iRet = Yol_CreateGrGrRec(acRec, fdOut);
      if (iRet > 0)
         iCnt += iRet;

      lCnt++;
      if (!(lCnt % 1000))
         printf("\r%d", lCnt);
   }

   fclose(fdIn);
   LogMsg("Total processed records  : %u", lCnt);

   return lCnt;
}

int Yol_LoadGrGr(LPCSTR pCnty) 
{
   char     *pTmp, acTmp[256], acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   char     acInFile[_MAX_PATH];
   struct   _finddata_t  sFileInfo;

   FILE     *fdOut;

   int      iCnt, iRet;
   long     lCnt, lHandle;

   // Get raw file name
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);

   // Prepare backup folder
   sprintf(acGrGrBak, acGrGrBakTmpl, pCnty, acToday);
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
   } else
   {
      LogMsg("*** No new file avail for processing: %s", acGrGrIn);
      return -1;
   }

   // Create Output file - Son_GrGr.dat
   sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "Dat");

   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      _findclose(lHandle);
      return -2;
   }

   lLastRecDate = iCnt = lCnt = iRet = 0;
   __int64 iFileSize=0;
   while (!iRet)
   {
      sprintf(acInFile, "%s\\%s", acGrGrIn, sFileInfo.name);
      
      // Check file size
      iFileSize = getFileSize(acInFile);
      if (iFileSize < 100)
      {
         LogMsg("*** Skip %s (input file is empty)", acInFile);
      } else
      {
         // Parse input file
         iRet = Yol_LoadGrGrCsv(acInFile, fdOut);
         if (iRet < 0)
            LogMsg("*** Skip %s", acInFile);
         else
            lCnt += iRet;
      }

      // Move input file to backup folder
      sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
      rename(acInFile, acTmp);

      iCnt++;
      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }


   // Close handle
   _findclose(lHandle);
   iRet = 0;

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total processed records  : %u", lCnt);

   // Sort output
   if (lCnt > 0)
   {
      char acCGrGrFile[_MAX_PATH], acSortCmd[200];

      // Sort output file and dedup if same docdate and docnum
      // Sort by APN, RecDate, DocNum, DocTitle (to avoid duplicate), Sale price desc
      sprintf(acSortCmd, "S(%d,12,C,A,%d,8,C,A,%d,12,C,A,%d,20,C,A,%d,10,C,D) F(TXT) DUPO(B2048,1,130) ", 
         OFF_GD_APN, OFF_GD_DOCDATE, OFF_GD_DOCNUM, OFF_GD_TITLE, OFF_GD_SALE);
      strcpy(acGrGrIn, acGrGrOut);
      sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "Srt");

      // Sort Son_GrGr.dat to Son_GrGr.srt
      lCnt = sortFile(acGrGrIn, acGrGrOut, acSortCmd);

      // Update cumulative sale file
      sprintf(acCGrGrFile, acGrGrTmpl, pCnty, pCnty, "sls"); // Son_GrGr.sls
      if (lCnt > 0)
      {
         if (!_access(acCGrGrFile, 0))
         {
            // Append Son_GrGr.srt to Son_GrGr.sls for backup
            sprintf(acInFile, "%s+%s", acGrGrOut, acCGrGrFile);

            // Sort on APN asc, DocDate asc, DocNum asc
            sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "dat"); // Son_GrGr.dat
            lCnt = sortFile(acInFile, acGrGrOut, acSortCmd);
            if (lCnt > 0)
            {
               if (!_access(acCGrGrFile, 0))
               {
                  sprintf(acTmp, acGrGrTmpl, pCnty, pCnty, "sav"); // Son_GrGr.sav
                  if (!_access(acTmp, 0))
                     DeleteFile(acTmp);

                  LogMsg("Rename old cum GRGR file: %s to %s", acCGrGrFile, acTmp);
                  rename(acCGrGrFile, acTmp);
               }

               // Rename Dat to SLS file
               LogMsg("Rename cum GRGR file: %s --> %s", acGrGrOut, acCGrGrFile);
               iRet = rename(acGrGrOut, acCGrGrFile);
            } else
            {
               iRet = -3;
               LogMsg("***** Error sorting output file in Son_LoadGrGr()");
            }
         } else
         {
            // Rename Srt to SLS file
            LogMsg("Rename cum GRGR file: %s --> %s", acGrGrOut, acCGrGrFile);
            iRet = rename(acGrGrOut, acCGrGrFile);
         }
      } else
         iRet = 1;
   } else
      iRet = 1;

   LogMsgD("Total output records after dedup: %u\n", lCnt);
   LogMsg("              Last recording date: %d.", lLastRecDate);

   return iRet;
}

/******************************* Yol_MakeDocLink *******************************
 *
 * Format DocLink
 *
 ******************************************************************************/

void Yol_MakeDocLink(char *pDocLink, char *pDoc, char *pDate)
{
   int   iDocNum;
   char  acTmp[256], acDocName[256], acDocNum[32];

#ifdef _DEBUG
   //if (!memcmp(pDoc, "2021R013922", 11))
   //   iDocNum = 0;
#endif

   *pDocLink = 0;
   if (*pDoc > ' ' && *pDate > ' ' && *(pDoc+4) == 'R')
   {
      iDocNum = atoin((char *)pDoc+5, 7);       // DocNum 7 digits
      sprintf(acDocNum, "%.7d", iDocNum);       // Reformat DocNum
      sprintf(acDocName, "%.4s\\%.4s\\%.4s%s", pDate, acDocNum, pDate, acDocNum);
      sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocName);

      try
      {
         if (!_access(acTmp, 0))
            strcpy(pDocLink, &acDocName[0]);
         else
            *pDocLink = 0;
      } catch (...)
      {
         LogMsg("*** Bad file name: %s", acTmp);
      }
   }
}

/*********************************** loadYol ********************************
 *
 * Options:
 *    -L  (load lien)
 *    -U  (load update)
 *    -Xl (extract lien value)
 *    -Xs (extract sale data)
 *    -Xa (extract char data)
 *    -O  (overwrite log file)
 *
 * LDR load:      -CYOL -L -Xl -Xs [-Xa] -O
 * Normal update: -CYOL -U -Xs [-Xa] -O
 *
 ****************************************************************************/

int loadYol(int iSkip)
{
   int   iRet=0;
   char  acTmpFile[_MAX_PATH];

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;

   iApnLen = myCounty.iApnLen;

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      TC_SetDateFmt(MM_DD_YYYY_1);
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Fix Lien extract
   //iRet = Yol_FixApn(myCounty.acCntyCode, iSkip);

   // Load Grgr
   if (iLoadFlag & LOAD_GRGR)                      // -G
   {
      // Load special table for GrGr
      GetIniString(myCounty.acCntyCode, "Yol_GrGr_Xref", "", acTmpFile, _MAX_PATH, acIniFile);
      iNumDeeds = LoadXrefTable(acTmpFile, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

      LogMsg("Load %s GrGr file", myCounty.acCntyCode);
      iRet = Yol_LoadGrGr(myCounty.acCntyCode);
      if (!iRet)
         iLoadFlag |= MERG_GRGR;                   // Trigger merge grgr
      else if (iRet == -1 || iRet == 1)
      {
         bGrGrAvail = false;
         if (bSendMail && !(iLoadFlag & (LOAD_LIEN|LOAD_UPDT)))
            return iRet;
      }
   }

   // Extract lien file - LDR2016
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acTmpFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acTmpFile, 0);     // 2016 -2020
      else if (iLdrGrp == 15)
         iRet = Yol_ExtrLien(myCounty.acCntyCode);                   // 2021
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode);
   }

   // Fix DocType if needed
   if (iLoadFlag & FIX_CSTYP)
   {
      iRet = FixDocType(acCSalFile, (IDX_TBL5 *)&YOL_DocCode[0], true /* remove rec w/o DocNum */);
      if (iRet)
         return iRet;
   }

   // Load Value file
   if (iLoadFlag & EXTR_VALUE)                     // -Xv
   {
      char sDbName[64], sBYVFile[_MAX_PATH], sCVFile[_MAX_PATH], acTmp[256];

      sprintf(sBYVFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Value");
      sprintf(acTmp, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = MB_ExtrValues(myCounty.acCntyCode, acValueFile, acExeFile, acTmp, sBYVFile, iHdrRows);
      
      // Reset reason and VST
      if (iRet > 0)
      {
         sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Vmp");
         iRet = MB_ResetPVReason(sBYVFile, acTmp);
      }
      if (iRet > 0)
      {
         sprintf(sDbName, "LDR%d", lLienYear);
         GetIniString("Data", "SqlValueFile", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(sCVFile, acTmp, sDbName, myCounty.acCntyCode);
         lRecCnt = createValueImport(sBYVFile, sCVFile, false);
         if (lRecCnt > 0)
         {
            // Save output file for import
            strcpy(acValueFile, sCVFile);
            iRet = 0;
         } else
            iLoadFlag ^= EXTR_IVAL;                // Turn off import
      }
   }

   // Create/Update cum sale file
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // 01/02/2014
      iRet = MB_CreateSCSale(MM_DD_YYYY_1, 0, 0, false, (IDX_TBL5 *)&YOL_DocCode[0]);
      //iRet = MB_CreateSCSale(MM_DD_YYYY_1, 0, 0, false);
      if (iRet)
         return iRet;

      iLoadFlag |= MERG_CSAL;
   }

   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Old Char format
      //iRet = Yol_ConvertChar(acCharFile);
      //if (iRet <= 0)
      //{
      //   LogMsg("***** Error converting Char file %s", acCharFile);
      //   return -1;
      //}

      // Load Char file
      if (!_access(acCharFile, 0))
      {
         iRet = Yol_ConvStdChar(acCharFile);
         if (iRet <= 0)
         {
            LogMsg("***** Error converting Char file %s", acCharFile);
            return -1;
         }
      } else
      {
         LogMsg("*** WARNING: CHAR file does not exist: %s: ", acCharFile);
         LogMsg("    -Xa option is ignore.  Please verify input file");
      }

   }

   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      if (iLoadFlag & LOAD_LIEN)                   // -L
      {
         // Create Lien file
         LogMsg0("Load %s Lien roll", myCounty.acCntyCode);
         if (iLdrGrp == 3)
            iRet = Yol_Load_LDR3(iSkip);
         else if (iLdrGrp == 15)
            iRet = Yol_Load_LDR15(iSkip);          // 2021
         else if (iLdrGrp == 17)
            iRet = Yol_Load_LDR17(iSkip);          // 2023
         else
         {
            LogMsg0("***** Please set LdrGrp before running");
            return -1;
         }
      } else if (iLoadFlag & LOAD_UPDT)            // -U
      {
         if (acRollFile[0])
         {
            LogMsg0("Load %s roll update file", myCounty.acCntyCode);
            iRet = Yol_Load_Roll(iSkip);
         } else
         {
            LogMsg("***** Please specify Rollfile in [%s] section of %s", myCounty.acCntyCode, acIniFile);
            iRet = -1;
         }
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply YOL_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   // Merge GrGr
   if (!iRet && (iLoadFlag & MERG_GRGR))           // -Mg
   {
      iRet = MergeGrGrDoc(myCounty.acCntyCode, 2);
      if (iRet)
         iLoadFlag = 0;
      else
         iLoadFlag |= LOAD_UPDT;
   }

   // Format DocLinks - Doclinks are concatenate fields separated by comma 
   if (!iRet && (iLoadFlag & (LOAD_UPDT|LOAD_LIEN|MERG_GRGR)) )
      iRet = updateDocLinks(Yol_MakeDocLink, myCounty.acCntyCode, iSkip, 0);

   return iRet;
}