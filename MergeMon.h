#ifndef  _MERGEMON_H_
#define  _MERGEMON_H_
/*
// MON_LIEN.CSV
#define  L_ASMT                        0
#define  L_TAXYEAR                     1
#define  L_ROLLCHGNUM                  2
#define  L_MAPCATEGORY                 3
#define  L_ROLLCATEGORY                4
#define  L_ROLLTYPE                    5
#define  L_DESTINATIONROLL             6
#define  L_INSTALLMENTS                7
#define  L_BILLTYPE                    8
#define  L_FEEPARCEL                   9
#define  L_ORIGINATINGASMT             10
#define  L_XREFASMT                    11
#define  L_STATUS                      12
#define  L_TRA                         13
#define  L_TAXABILITY                  14
#define  L_ACRES                       15
#define  L_SIZEACRESFTTYPE             16
#define  L_INTDATEFROM                 17
#define  L_INTDATEFROM2                18
#define  L_INTDATETHRU                 19
#define  L_INTERESTTYPE                20
#define  L_USECODE                     21
#define  L_CURRENTMARKETLANDVALUE      22
#define  L_CURRENTFIXEDIMPRVALUE       23
#define  L_CURRENTGROWINGIMPRVALUE     24
#define  L_CURRENTSTRUCTURALIMPRVALUE  25
#define  L_CURRENTPERSONALPROPVALUE    26
#define  L_CURRENTPERSONALPROPMHVALUE  27
#define  L_CURRENTNETVALUE             28
#define  L_BILLEDMARKETLANDVALUE       29
#define  L_BILLEDFIXEDIMPRVALUE        30
#define  L_BILLEDGROWINGIMPRVALUE      31
#define  L_BILLEDSTRUCTURALIMPRVALUE   32
#define  L_BILLEDPERSONALPROPVALUE     33
#define  L_BILLEDPERSONALPROPMHVALUE   34
#define  L_BILLEDNETVALUE              35
#define  L_OWNER                       36
#define  L_ASSESSEE                    37
#define  L_MAILADDRESS1                38
#define  L_MAILADDRESS2                39
#define  L_MAILADDRESS3                40
#define  L_MAILADDRESS4                41
#define  L_ZIPMATCH                    42
#define  L_BARCODEZIP                  43
#define  L_EXEMPTIONCODE1              44
#define  L_EXEMPTIONAMT1               45
#define  L_EXEMPTIONCODE2              46
#define  L_EXEMPTIONAMT2               47
#define  L_EXEMPTIONCODE3              48
#define  L_EXEMPTIONAMT3               49
#define  L_BILLDATE                    50
#define  L_DUEDATE1                    51
#define  L_DUEDATE2                    52
#define  L_TAXAMT1                     53
#define  L_TAXAMT2                     54
#define  L_PENAMT1                     55
#define  L_PENAMT2                     56
#define  L_COST1                       57
#define  L_COST2                       58
#define  L_PENCHRGDATE1                59
#define  L_PENCHRGDATE2                60
#define  L_PAIDAMT1                    61
#define  L_PAIDAMT2                    62
#define  L_PAYMENTDATE1                63
#define  L_PAYMENTDATE2                64
#define  L_TRANSDATE1                  65
#define  L_TRANSDATE2                  66
#define  L_COLLECTIONNUM1              67
#define  L_COLLECTIONNUM2              68
#define  L_UNSDELINQPENAMTPAID1        69
#define  L_SECDELINQPENAMTPAID2        70
#define  L_TOTALFEESPAID1              71
#define  L_TOTALFEESPAID2              72
#define  L_TOTALFEES                   73
#define  L_PMTCNLDATE1                 74
#define  L_PMTCNLDATE2                 75
#define  L_TRANSFERDATE                76
#define  L_PRORATIONFACTOR             77
#define  L_PRORATIONPCT                78
#define  L_DAYSTOFISCALYEAREND         79
#define  L_DAYSOWNED                   80
#define  L_OWNFROM                     81
#define  L_OWNTHRU                     82
#define  L_SITUS1                      83
#define  L_SITUS2                      84
#define  L_ASMTROLLYEAR                85
#define  L_PARCELDESCRIPTION           86
#define  L_BILLCOMMENTSLINE1           87
#define  L_BILLCOMMENTSLINE2           88
#define  L_DEFAULTNUM                  89
#define  L_DEFAULTDATE                 90
#define  L_REDEEMEDDATE                91
#define  L_SECDELINQFISCALYEAR         92
#define  L_PRIORTAXPAID1               93
#define  L_PENINTERESTCODE             94
#define  L_FOURPAYPLANNUM              95
#define  L_EXISTSLIEN                  96
#define  L_EXISTSCORTAC                97
#define  L_EXISTSCOUNTYCORTAC          98
#define  L_EXISTSBANKRUPTCY            99
#define  L_EXISTSREFUND                100
#define  L_EXISTSDELINQUENTVESSEL      101
#define  L_EXISTSNOTES                 102
#define  L_EXISTSCRITICALNOTE          103
#define  L_EXISTSROLLCHG               104
#define  L_ISPENCHRGCANCELED1          105
#define  L_ISPENCHRGCANCELED2          106
#define  L_ISPERSONALPROPERTYPENALTY   107
#define  L_ISAGPRESERVE                108
#define  L_ISCARRYOVER1STPAID          109
#define  L_ISCARRYOVERRECORD           110
#define  L_ISFAILURETOFILE             111
#define  L_ISOWNERSHIPPENALTY          112
#define  L_ISELIGIBLEFOR4PAY           113
#define  L_ISNOTICE4SENT               114
#define  L_ISPARTPAY                   115
#define  L_ISFORMATTEDADDRESS          116
#define  L_ISADDRESSCONFIDENTIAL       117
#define  L_SUPLCNT                     118
#define  L_ORIGINALDUEDATE1            119
#define  L_ORIGINALDUEDATE2            120
#define  L_ISPEN1REFUND                121
#define  L_ISPEN2REFUND                122
#define  L_ISDELINQPENREFUND           123
#define  L_ISREFUNDAUTHORIZED          124
#define  L_ISNODISCHARGE               125
#define  L_ISAPPEALPENDING             126
#define  L_OTHERFEESPAID               127
#define  L_FOURPAYREASON               128
#define  L_DATEDISCHARGED              129
#define  L_ISONLYFIRSTPAID             130
#define  L_ISALLPAID                   131
#define  L_DTS                         132
#define  L_USERID                      133
*/

#define  MON_L_FEEPARCEL                  0
#define  MON_L_ASMT                       1
#define  MON_L_TRA                        2
#define  MON_L_LANDUSE1                   3
#define  MON_L_ACRES                      4
#define  MON_L_ASSESSEE                   5
#define  MON_L_INCAREOF                   6
#define  MON_L_DBA                        7
#define  MON_L_A_STREET                   8
#define  MON_L_A_CITY                     9
#define  MON_L_A_STATE                    10
#define  MON_L_A_ZIP                      11
#define  MON_L_ADDRESS1                   12
#define  MON_L_ADDRESS2                   13
#define  MON_L_ADDRESS3                   14
#define  MON_L_ADDRESS4                   15
#define  MON_L_F_SITUS1                   16
#define  MON_L_F_SITUS2                   17
#define  MON_L_SITUSNUM                   18
#define  MON_L_SITUS_DIRX                 19
#define  MON_L_SITUS_ST                   20
#define  MON_L_SITUSAPT                   21
#define  MON_L_CITY_ID                    22
#define  MON_L_SIT_ZIP                    23
#define  MON_L_LAND                       24
#define  MON_L_IMP                        25
#define  MON_L_PERSPROP                   26
#define  MON_L_CURRENTDOCNUM              27
#define  MON_L_CURRENTDOCDATE             28
#define  MON_L_TRANSTAX                   29

#define  MON_OFF_BOOKPAGE					0				
#define  MON_OFF_NCODE						1
#define  MON_OFF_APPRID						2
#define  MON_OFF_NCSUFFIX					3
#define  MON_OFF_AREADESIG					4
#define  MON_OFF_AREADESC					5
#define  MON_OFF_APPRNAME					6
#define  MON_OFF_DTS							7
#define  MON_OFF_USERID						8

#define  MON_SIZ_BOOKPAGE					5				
#define  MON_SIZ_NCODE						4
#define  MON_SIZ_APPRID						4
#define  MON_SIZ_NCSUFFIX					20
#define  MON_SIZ_AREADESIG					3
#define  MON_SIZ_AREADESC					50
#define  MON_SIZ_APPRNAME					50
#define  MON_SIZ_DTS							1
#define  MON_SIZ_USERID						8

typedef struct _tMonAppr
{
	char Bookpage[MON_SIZ_BOOKPAGE];									
	char Ncode[MON_SIZ_NCODE];					
	char Apprid[MON_SIZ_APPRID];						
	char Ncsuffix[MON_SIZ_NCSUFFIX];					
	char Areadesig[MON_SIZ_AREADESIG];					
	char Areadesc[MON_SIZ_AREADESC];					
	char Apprname[MON_SIZ_APPRNAME];					
	char Dts[MON_SIZ_DTS];							
	char Userid[MON_SIZ_USERID];						
} MON_APPR;

/*
// _ROLL
#define  MB_ROLL_ASMT                 0
#define  MB_ROLL_FEEPARCEL            1
#define  MB_ROLL_TRA                  2
#define  MB_ROLL_ASMTDESC             3
#define  MB_ROLL_ZONING               4
#define  MB_ROLL_USECODE              5
#define  MB_ROLL_NBHCODE              6
#define  MB_ROLL_ACRES                7
#define  MB_ROLL_DOCNUM               8
#define  MB_ROLL_DOCDATE              9
#define  MB_ROLL_TAXABILITY           10
#define  MB_ROLL_OWNER                11
#define  MB_ROLL_CAREOF               12
#define  MB_ROLL_DBA                  13
#define  MB_ROLL_M_ADDR               14
#define  MB_ROLL_M_CITY               15
#define  MB_ROLL_M_ST                 16
#define  MB_ROLL_M_ZIP                17
#define  MB_ROLL_LAND                 18
#define  MB_ROLL_HOMESITE             19
#define  MB_ROLL_IMPR                 20
#define  MB_ROLL_GROWING              21
#define  MB_ROLL_FIXTRS               22

#define  MB_ROLL_FIXTR_RP             23  // Old script
#define  MB_ROLL_FIXTR_BUS            24

#define  MB_ROLL_PERSPROP             23  // New script
#define  MB_ROLL_BUSPROP              24

#define  MB_ROLL_PPMOBILHOME          25
#define  MB_ROLL_M_ADDR1              26
#define  MB_ROLL_M_ADDR2              27
#define  MB_ROLL_M_ADDR3              28
#define  MB_ROLL_M_ADDR4              29

// _EXE
#define  MB_EXE_STATUS                0
#define  MB_EXE_ASMT                  1
#define  MB_EXE_CODE                  2
#define  MB_EXE_HOEXE                 3
#define  MB_EXE_EXEAMT                4
#define  MB_EXE_EXEPCT                5
*/

// _CHAR 10/25/2013
/*
#define  MON_CHAR_FEE_PRCL            0
#define  MON_CHAR_UNITS               1
#define  MON_CHAR_BLDGSEQNO           2
#define  MON_CHAR_UNITSEQNO           3
#define  MON_CHAR_ROOMS               4
#define  MON_CHAR_POOLS               5
#define  MON_CHAR_USECAT              6
#define  MON_CHAR_QUALITY             7
#define  MON_CHAR_YRBLT               8
#define  MON_CHAR_EFFYR               9
#define  MON_CHAR_BLDGSQFT            10
#define  MON_CHAR_GARSQFT             11
#define  MON_CHAR_HEATING             12
#define  MON_CHAR_COOLING             13
#define  MON_CHAR_HEATING_SRC         14
#define  MON_CHAR_COOLING_SRC         15
#define  MON_CHAR_BEDS                16
#define  MON_CHAR_FBATHS              17
#define  MON_CHAR_HBATHS              18
#define  MON_CHAR_FP                  19
#define  MON_CHAR_ASMT                20
#define  MON_CHAR_HASSEPTIC           21
#define  MON_CHAR_HASSEWER            22
#define  MON_CHAR_HASWELL             23
*/

// 3/24/2014
/*
#define  MON_CHAR_FEEPRCL                 0
#define  MON_CHAR_ASMT                    1
#define  MON_CHAR_SEQUENCE                2
#define  MON_CHAR_SITUSNUM                3
#define  MON_CHAR_SITUS_ST                4
#define  MON_CHAR_SITUS_DIRX              5
#define  MON_CHAR_SITUSAPT                6
#define  MON_CHAR_CITY_ID                 7
#define  MON_CHAR_F_SITUS1                8
#define  MON_CHAR_F_SITUS2                9
#define  MON_CHAR_DOCNUM                  10
#define  MON_CHAR_CATTYPE                 11
#define  MON_CHAR_BLDGSEQNO               12
#define  MON_CHAR_YRBLT                   13
#define  MON_CHAR_BUILDINGTYPE            14
#define  MON_CHAR_EFFYR                   15
#define  MON_CHAR_BUILDINGSIZE            16
#define  MON_CHAR_BUILDINGUSEDFOR         17
#define  MON_CHAR_STORIESCNT              18
#define  MON_CHAR_UNITSCNT                19
#define  MON_CHAR_CONDITION               20
#define  MON_CHAR_BEDROOMS                21
#define  MON_CHAR_BATHROOMS               22
#define  MON_CHAR_QUALITYCLASS            23
#define  MON_CHAR_HALFBATHS               24
#define  MON_CHAR_CONSTRUCTION            25
#define  MON_CHAR_FOUNDATION              26
#define  MON_CHAR_STRUCTURALFRAME         27
#define  MON_CHAR_STRUCTURALFLOOR         28
#define  MON_CHAR_EXTERIORTYPE            29
#define  MON_CHAR_ROOFCOVER               30
#define  MON_CHAR_ROOFTYPEFLAT            31
#define  MON_CHAR_ROOFTYPEHIP             32
#define  MON_CHAR_ROOFTYPEGABLE           33
#define  MON_CHAR_ROOFTYPESHED            34
#define  MON_CHAR_INSULATIONCEILINGS      35
#define  MON_CHAR_INSULATIONWALLS         36
#define  MON_CHAR_INSULATIONFLOORS        37
#define  MON_CHAR_WINDOWPANESINGLE        38
#define  MON_CHAR_WINDOWPANEDOUBLE        39
#define  MON_CHAR_WINDOWPANETRIPLE        40
#define  MON_CHAR_WINDOWTYPE              41
#define  MON_CHAR_LIGHTING                42
#define  MON_CHAR_COOLINGCENTRALAC        43
#define  MON_CHAR_COOLINGEVAPORATIVE      44
#define  MON_CHAR_COOLINGROOMWALL         45
#define  MON_CHAR_COOLINGWINDOW           46
#define  MON_CHAR_HEATING                 47
#define  MON_CHAR_FIREPLACE               48
#define  MON_CHAR_GARAGE                  49
#define  MON_CHAR_PLUMBING                50
#define  MON_CHAR_SOLAR                   51
#define  MON_CHAR_BUILDER                 52
#define  MON_CHAR_BLDGDESIGNEDFOR         53
#define  MON_CHAR_MODELDESC               54
#define  MON_CHAR_UNFINAREASSF            55
#define  MON_CHAR_ATTACHGARAGESF          56
#define  MON_CHAR_DETACHGARAGESF          57
#define  MON_CHAR_CARPORTSF               58
#define  MON_CHAR_DECKSSF                 59
#define  MON_CHAR_PATIOSF                 60
#define  MON_CHAR_CEILINGHEIGHT           61
#define  MON_CHAR_FIRESPINKLERS           62
#define  MON_CHAR_AVGWALLHEIGHT           63
#define  MON_CHAR_BAY                     64
#define  MON_CHAR_DOCK                    65
#define  MON_CHAR_ELEVATOR                66
#define  MON_CHAR_ESCALATOR               67
#define  MON_CHAR_ROLLUPDOOR              68
#define  MON_CHAR_FIELD1                  69
#define  MON_CHAR_FIELD2                  70
#define  MON_CHAR_FIELD3                  71
#define  MON_CHAR_FIELD4                  72
#define  MON_CHAR_FIELD5                  73
#define  MON_CHAR_FIELD6                  74
#define  MON_CHAR_FIELD7                  75
#define  MON_CHAR_FIELD8                  76
#define  MON_CHAR_FIELD9                  77
#define  MON_CHAR_FIELD10                 78
#define  MON_CHAR_FIELD11                 79
#define  MON_CHAR_FIELD12                 80
#define  MON_CHAR_FIELD13                 81
#define  MON_CHAR_FIELD14                 82
#define  MON_CHAR_FIELD15                 83
#define  MON_CHAR_FIELD16                 84
#define  MON_CHAR_FIELD17                 85
#define  MON_CHAR_FIELD18                 86
#define  MON_CHAR_FIELD19                 87
#define  MON_CHAR_FIELD20                 88
#define  MON_CHAR_LANDFIELD1              89
#define  MON_CHAR_LANDFIELD2              90
#define  MON_CHAR_LANDFIELD3              91
#define  MON_CHAR_LANDFIELD4              92
#define  MON_CHAR_LANDFIELD5              93
#define  MON_CHAR_LANDFIELD6              94
#define  MON_CHAR_LANDFIELD7              95
#define  MON_CHAR_LANDFIELD8              96
#define  MON_CHAR_ACRES                   97
#define  MON_CHAR_NEIGHBORHOODCODE        98
#define  MON_CHAR_ZONING                  99
#define  MON_CHAR_TOPOGRAPHY              100
#define  MON_CHAR_VIEWCODE                101
#define  MON_CHAR_POOLSPA                 102
#define  MON_CHAR_WATERSOURCE             103
#define  MON_CHAR_SUBDIVNAME              104
#define  MON_CHAR_SEWERCODE               105
#define  MON_CHAR_UTILITIESCODE           106
#define  MON_CHAR_ACCESSCODE              107
#define  MON_CHAR_LANDSCAPE               108
#define  MON_CHAR_PROBLEMCODE             109
#define  MON_CHAR_FRONTAGE                110
#define  MON_CHAR_LOCATION                111
#define  MON_CHAR_PLANTEDACRES            112
#define  MON_CHAR_ACRESUNUSEABLE          113
#define  MON_CHAR_WATERSOURCEDOMESTIC     114
#define  MON_CHAR_WATERSOURCEIRRIGATION   115
#define  MON_CHAR_HOMESITES               116
#define  MON_CHAR_PROPERTYCONDITIONCODE   117
#define  MON_CHAR_ROADTYPE                118
#define  MON_CHAR_HASWELL                 119
#define  MON_CHAR_HASCOUNTYROAD           120
#define  MON_CHAR_HASVINEYARD             121
#define  MON_CHAR_ISUNSECUREDBUILDING     122
#define  MON_CHAR_HASORCHARD              123
#define  MON_CHAR_HASGROWINGIMPRV         124
#define  MON_CHAR_SITECOVERAGE            125
#define  MON_CHAR_PARKINGSPACES           126
#define  MON_CHAR_EXCESSLANDSF            127
#define  MON_CHAR_FRONTFOOTAGESF          128
#define  MON_CHAR_MULTIPARCELECON         129
#define  MON_CHAR_LANDUSECODE1            130
#define  MON_CHAR_LANDUSECODE2            131
#define  MON_CHAR_LANDSQFT                132
#define  MON_CHAR_TOTALROOMS              133
#define  MON_CHAR_NETLEASABLESF           134
#define  MON_CHAR_BLDGFOOTPRINTSF         135
#define  MON_CHAR_OFFICESPACESF           136
#define  MON_CHAR_NONCONDITIONSF          137
#define  MON_CHAR_MEZZANINESF             138
#define  MON_CHAR_PERIMETERLF             139
#define  MON_CHAR_ASMTCATEGORY            140
#define  MON_CHAR_EVENTDATE               141
#define  MON_CHAR_SALESPRICE              142   // Confirmed
#define  MON_CHAR_PCASMT                  143
#define  MON_CHAR_PCFEEPARCEL             144
*/

// 08/25/2014 - CountyPhyChar.txt
#define  MON_CHAR_FEEPRCL                 0
#define  MON_CHAR_ASMT                    1
#define  MON_CHAR_SEQUENCE                2
#define  MON_CHAR_SITUSNUM                3
#define  MON_CHAR_SITUS_ST                4
#define  MON_CHAR_SITUS_DIRX              5
#define  MON_CHAR_SITUSAPT                6
#define  MON_CHAR_CITY_ID                 7
#define  MON_CHAR_F_SITUS1                8
#define  MON_CHAR_F_SITUS2                9
#define  MON_CHAR_BLDGSEQNO               10
#define  MON_CHAR_YRBLT                   11
#define  MON_CHAR_BUILDINGTYPE            12
#define  MON_CHAR_EFFYR                   13
#define  MON_CHAR_BUILDINGSIZE            14
#define  MON_CHAR_BEDROOMS                15
#define  MON_CHAR_BATHROOMS               16
#define  MON_CHAR_QUALITYCLASS            17
#define  MON_CHAR_HALFBATHS               18
#define  MON_CHAR_FIREPLACE               19
#define  MON_CHAR_UNFINAREASSF            20
#define  MON_CHAR_GARAGESF                21
#define  MON_CHAR_UNITSEQNO               22
#define  MON_CHAR_NEIGHBORHOODCODE        23
#define  MON_CHAR_VIEWCODE                24
#define  MON_CHAR_LANDUSECODE1            25
#define  MON_CHAR_LANDUSECODE2            26
#define  MON_CHAR_LANDSQFT                27
#define  MON_CHAR_TOTALROOMS              28
#define  MON_CHAR_UNITSIZE                29
#define  MON_CHAR_ACRES                   30
#define  MON_CHAR_STORIES                 31
#define  MON_CHAR_UNITSCNT                32
#define  MON_CHAR_CONDITION               33
#define  MON_CHAR_HEATING                 34
#define  MON_CHAR_COOLING                 35
#define  MON_CHAR_GARAGE                  36
#define  MON_CHAR_ZONING                  37
#define  MON_CHAR_WATERSOURCE             38
#define  MON_CHAR_SEWERCODE               39
#define  MON_CHAR_POOLSPA                 40

// 06/06/2019 MON_CHAR.CSV
//#define  MON_CHAR_FEEPRCL                 0
//#define  MON_CHAR_UNITSCNT                1
//#define  MON_CHAR_BLDGSEQNO               2
//#define  MON_CHAR_UNITSEQNO               3
//#define  MON_CHAR_TOTALROOMS              4
//#define  MON_CHAR_NUMPOOLS                5    // 0,1,2,3,99
//#define  MON_CHAR_LANDUSECAT              6
//#define  MON_CHAR_QUALITYCLASS            7
//#define  MON_CHAR_YRBLT                   8
//#define  MON_CHAR_EFFYR                   9
//#define  MON_CHAR_BUILDINGSIZE            10
//#define  MON_CHAR_GARAGESF                11
//#define  MON_CHAR_HEATING                 12
//#define  MON_CHAR_COOLING                 13
//#define  MON_CHAR_HEATSRC                 14
//#define  MON_CHAR_COOLSRC                 15
//#define  MON_CHAR_BEDROOMS                16
//#define  MON_CHAR_BATHROOMS               17
//#define  MON_CHAR_HALFBATHS               18
//#define  MON_CHAR_FIREPLACE               19
//#define  MON_CHAR_ASMT                    20
//#define  MON_CHAR_HASSEPTIC               21
//#define  MON_CHAR_HASSEWER                22
//#define  MON_CHAR_HASWELL                 23
//#define  MON_CHAR_COLS                    24

// EXTRACT342S4.txt
#define  MON_GR_DOCNUM                    0
#define  MON_GR_DOCDATE                   1
#define  MON_GR_IMGPATH                   2
#define  MON_GR_TRANSFEROR                3
#define  MON_GR_TRANSFEREE                4
#define  MON_GR_DOCTYPE                   5
#define  MON_GR_ASMT                      6

// EXTRACT346S19.txt
#define  MON_GR_DOCTRANTAX                7
#define  MON_GR_TOTALFEE                  8
#define  MON_GR_FLDCNT                    9

static XLAT_CODE  asHeating[] = 
{
   "01", "I", 2,               // Radiant
   "02", "F", 2,               // Baseboard
   "03", "A", 2,               // Gravity
   "04", "G", 2,               // Heat pump
   "05", "5", 2,               // Oil Burner
   "06", "R", 2,               // Fireplace
   "99", "Q", 2,               // Structure heating
   "C",  "Z", 1,               // Central
   //"CF", "Z", 2,               // Central Forced
   "FL", "C", 2,               // Floor Furnace
   "WL", "D", 2,               // Wall Furnace
   //"WL2","D", 3,               // Double Wall Furnace
   "Z",  "T", 1,               // Zone
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "01", "P", 2,               // Pool
   "02", "C", 2,               // Pool & Spa
   "03", "S", 2,               // Spa
   "99", "P", 2,               // Pool
   "",   "",  0
};

static XLAT_CODE  asCond[] =
{
   // Value, lookup code, value length
   "01", "P", 2,               // Poor
   "03", "A", 2,               // Average
   "2",  "G", 2,               // Good
   "CND1","E",4,               // New/Excellent
   "CN", "V", 2,               // Very Good
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "01", "Y", 2,               // Yes
   "02", "N", 2,               // No
   "03", "B", 2,               // BBQ
   "1",  "1", 1,               // # fireplace
   "2",  "2", 1,               // # fireplaces
   "99", "O", 2,               // OTHER
   "",   "",  0
};

static XLAT_CODE  asView[] =
{
   "99", "Y", 2,               // Other
   "CT", "S", 2,               // City
   "CY", "I", 2,               // Canyon
   "GC", "M", 2,               // Golf Court 
   "MT", "T", 2,               // Mountain
   "OC", "B", 2,               // Ocean
   "PR", "O", 2,               // Panorama view
   "RV", "E", 2,               // River
   "VL", "R", 2,               // Valley
   "",   "",  0
};

static IDX_TBL5 asGarage[] =
{
   // Value, Code, Park Space, Value len, Code len
   //"010","I", '2', 3, 1,       // Special case
   "01", "I", '1', 2, 1,       // Attached Single Car
   "02", "I", '2', 2, 1,       // Attached Two Car
   "03", "O", '0', 2, 1,       // Attached Carport
   "04", "N", '0', 2, 1,       // Detached Carport
   "05", "L", '1', 2, 1,       // Detached Single Car Garage
   "06", "L", '2', 2, 1,       // Detached Two Car Garage
   "07", "I", '0', 2, 1,       // Garage-Attached
   "08", "L", '0', 2, 1,       // Garage-Detached
   "09", "O", '0', 2, 1,       // Carport-Attached
   "10", "N", '0', 2, 1,       // Carport-Detached
   "A",  "I", '0', 1, 1,       // Attached Garage
   "DE", "L", '0', 2, 1,       // Detached Garage
   "","",' ',0,0  
};

// Ignore all doc types not shown in this list
IDX_TBL5 MON_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "01", "1 ", 'N', 2, 2,     // FULL STAMPS NORMAL TRANSACTION
   "02", "1 ", 'N', 2, 2,     // FULL STAMPS SPLIT
   "03", "1 ", 'N', 2, 2,     // FULL STAMPS SALES W.O.P
   "04", "1 ", 'N', 2, 2,     // FULL STAMPS INTRA-FAM TRANS
   "05", "1 ", 'N', 2, 2,     // MOBILEHOME
   "06", "74", 'Y', 2, 2,     // SPLIT-RETAINED PART-NO SALE
   "07", "75", 'Y', 2, 2,     // TRANSFER-NOT SALE-WELL LTS R/W
   "08", "8 ", 'Y', 2, 2,     // AGREEMENT OF SALE
   "09", "74", 'Y', 2, 2,     // NO STAMPS, BLANK STP
   "0A", "57", 'P', 2, 2,     // PARTIAL STAMPS NORMAL TRANSACT
   "0B", "57", 'P', 2, 2,     // PARTIAL STAMPS SPLIT
   "0C", "57", 'P', 2, 2,     // PARTIAL STAMPS SALES W.O.P
   "0D", "57", 'Y', 2, 2,     // PARTIAL/NO STAMPS INTRA FAMILY
   "0E", "1 ", 'N', 2, 2,     // PUBLICLY ACQ FULL/PRT STMPS INVOL TR
   "0F", "77", 'N', 2, 2,     // FORECLOSURE FULL/PRT STAMPS INVOL TR
   "0H", "57", 'P', 2, 2,     // TRANSFER OR PARTIAL INTEREST
   "0L", "44", 'Y', 2, 2,     // LEASE LONG TERM
   "0R", "74", 'Y', 2, 2,     // RELOCATION SERV ACQUISITION
   "0X", "74", 'Y', 2, 2,     // ACTIVITY IN CORPORATE STOCK
   "0Z", "74", 'Y', 2, 2,     // DEATH OF REAL PROP OWNER
   "15", "75", 'N', 2, 2,     // MOBILEHOME TRANSFER
   "1D", "74", 'Y', 2, 2,     // INTERNAL DOC THAT REQUIRES SUPP
   "1X", "74", 'Y', 2, 2,     // ACTIVITY IN CORP STOCK SUPP REQ
   "1Z", "74", 'Y', 2, 2,     // DEATH OF REAL PROP OWNER SUPP REQ
   "BP", "74", 'Y', 2, 2,     // BUILDING PERMIT
   "CT", "74", 'Y', 2, 2,     // CITIES & TOWNS
   "OH", "57", 'P', 2, 2,     // TRANSFER OR PARTIAL INTEREST
   "PM", "74", 'Y', 2, 2,     // PARCEL MAPS
   "RS", "74", 'Y', 2, 2,     // RECORD OF SURVEY
   "SC", "74", 'Y', 2, 2,     // SPLIT/COMB-SAME OWNER
   "SM", "74", 'Y', 2, 2,     // SUBD MAP
   "TM", "74", 'Y', 2, 2,     // TAXPAYER MAP
   "Z0", "74", 'Y', 2, 2,     // DEATH OF REAL PROP OWNER
   "Z1", "74", 'Y', 2, 2,     // DEATH OF REAL PROP OWNER SUPP REQ
   "","",0,0,0
};

IDX_TBL5 MON_DocType[] =
{  // DocType, Index, Non-sale, len1, len2
   "001", "  ", 'Y', 3, 2,     // ABSTRACT OF JUDGMENT
   "002", "6 ", 'Y', 3, 2,     // AFFIDAVIT
   "003", "  ", 'Y', 3, 2,     // AGREEMENT
   "004", "7 ", 'Y', 3, 2,     // ASSIGNMENT
   "005", "  ", 'Y', 3, 2,     // NOTICE OF PENDENCY
   "006", "1 ", 'N', 3, 2,     // DEED
   "007", "65", 'Y', 3, 2,     // TRUST DEED
   "008", "  ", 'Y', 3, 2,     // ATTACHMENT
   "009", "44", 'Y', 3, 2,     // LEASE
   "010", "54", 'Y', 3, 2,     // MAP
   "011", "46", 'Y', 3, 2,     // MECHANIC'S LIEN
   "012", "  ", 'Y', 3, 2,     // RQ. FOR NOTICE OF DELINQUENCY
   "013", "44", 'Y', 3, 2,     // AMEND OIL & GAS LEASE
   "015", "  ", 'Y', 3, 2,     // NOTICE OF COMPLETION
   "016", "52", 'Y', 3, 2,     // NOTICE OF DEFAULT
   "017", "74", 'Y', 3, 2,     // NOTICE MISCELLANEOUS
   "018", "  ", 'Y', 3, 2,     // PROOF OF LABOR
   "019", "37", 'Y', 3, 2,     // RECONVEYANCE
   "020", "  ", 'Y', 3, 2,     // RELEASE
   "021", "8 ", 'Y', 3, 2,     // MODIFICATION AGREEMENT
   "022", "27", 'N', 3, 2,     // TRUSTEE DEED
   "023", "46", 'Y', 3, 2,     // LIEN FEDERAL
   "024", "67", 'Y', 3, 2,     // TAX LIEN
   "025", "  ", 'Y', 3, 2,     // NOTICE OF WITHDRAWAL
   "026", "  ", 'Y', 3, 2,     // NOTICE OF PRESERVATION
   "027", "  ", 'Y', 3, 2,     // X HOMESTEAD
   "028", "  ", 'Y', 3, 2,     // ABATEMENT
   "029", "  ", 'Y', 3, 2,     // ADDITIONAL ADVANCE
   "030", "8 ", 'Y', 3, 2,     // AGREEMENT OF SALE
   "031", "65", 'Y', 3, 2,     // AMEND TRUST DEED
   "032", "  ", 'Y', 3, 2,     // AMEND PARTNERSHIP
   "033", "  ", 'Y', 3, 2,     // AMEND RESTRICTION
   "034", "  ", 'Y', 3, 2,     // ANNEXATION
   "035", "32", 'Y', 3, 2,     // BILL OF SALE
   "037", "19", 'Y', 3, 2,     // BOND
   "038", "19", 'Y', 3, 2,     // BOND CONTRACT
   "039", "19", 'Y', 3, 2,     // NOTARY BOND
   "041", "  ", 'Y', 3, 2,     // X DEFAULT
   "042", "  ", 'Y', 3, 2,     // X LIS PENDENS
   "043", "  ", 'Y', 3, 2,     // X PARTNERSHIP
   "044", "  ", 'Y', 3, 2,     // CERTIF MISCELLANEOUS
   "045", "  ", 'Y', 3, 2,     // CERTIFICATE OF SALE
   "046", "  ", 'Y', 3, 2,     // CONSENT
   "047", "  ", 'Y', 3, 2,     // CONSERVATION CONTRACT
   "048", "65", 'Y', 3, 2,     // TRUST DEED CONSTRUCTION LOAN
   "052", "11", 'Y', 3, 2,     // CONTRACT OF SALE
   "053", "  ", 'Y', 3, 2,     // NOTICE OF CESSATION
   "054", "12", 'Y', 3, 2,     // DECLARATION ESTABLISHING DEATH
   "055", "79", 'Y', 3, 2,     // DECLARATION OF DISTRIBUTION
   "057", "12", 'Y', 3, 2,     // DECLARATION OF HOMESTEAD
   "058", "12", 'Y', 3, 2,     // DECLARATION OF TRUST
   "059", "  ", 'Y', 3, 2,     // DISSOLUTION
   "060", "40", 'Y', 3, 2,     // EASEMENT
   "061", "  ", 'Y', 3, 2,     // EXECUTION
   "062", "  ", 'Y', 3, 2,     // INDENTURE
   "063", "46", 'Y', 3, 2,     // LIEN
   "064", "  ", 'Y', 3, 2,     // LIS PENDENS
   "065", "  ", 'Y', 3, 2,     // ASSIGNMENT OF RENTS
   "066", "  ", 'Y', 3, 2,     // MERGER
   "067", "  ", 'Y', 3, 2,     // NOTICE OF ASSESSMENT
   "068", "  ", 'Y', 3, 2,     // NOTICE OF BULK TRANSFER
   "069", "75", 'Y', 3, 2,     // INTENDED TRANSFER
   "070", "  ", 'Y', 3, 2,     // NOTICE OF NON RESPONSIBILI
   "071", "44", 'Y', 3, 2,     // OIL & GAS LEASE
   "072", "  ", 'Y', 3, 2,     // OPTION
   "073", "80", 'Y', 3, 2,     // ORDER
   "074", "80", 'Y', 3, 2,     // ORDER CONFIRMING SALE
   "075", "  ", 'Y', 3, 2,     // ORDINANCE
   "076", "37", 'Y', 3, 2,     // PARTIAL RECONVEYANCE
   "077", "21", 'Y', 3, 2,     // PARTNERSHIP
   "078", "  ", 'Y', 3, 2,     // PERMIT
   "079", "  ", 'Y', 3, 2,     // PARTIAL RELEASE
   "080", "  ", 'Y', 3, 2,     // POWER OF ATTORNEY
   "081", "  ", 'Y', 3, 2,     // REQUEST NOTICE OF DEFAULT
   "082", "  ", 'Y', 3, 2,     // RESOLUTION
   "083", "  ", 'Y', 3, 2,     // RESTRICTION
   "084", "  ", 'Y', 3, 2,     // X POWER OF ATTORNEY
   "085", "  ", 'Y', 3, 2,     // SUBSTITUTION OF TRUSTEE
   "086", "  ", 'Y', 3, 2,     // SUBORDINATION AGREEMENT
   "088", "  ", 'Y', 3, 2,     // X LEASE
   "090", "  ", 'Y', 3, 2,     // WAIVER
   "091", "  ", 'Y', 3, 2,     // NOTICE OF TRUSTEE SALE
   "100", "  ", 'Y', 3, 2,     // AMEND AGREEMENT
   "101", "  ", 'Y', 3, 2,     // AMEND LEASE
   "102", "  ", 'Y', 3, 2,     // AMEND ORDER
   "103", "  ", 'Y', 3, 2,     // AMEND TRUST
   "104", "  ", 'Y', 3, 2,     // BOND FOR LOST NOTE
   "105", "  ", 'Y', 3, 2,     // BOND TO RELEASE LIEN
   "106", "  ", 'Y', 3, 2,     // CONTRACT
   "107", "12", 'Y', 3, 2,     // DECLARATION
   "108", "46", 'Y', 3, 2,     // EXTEND LIEN
   "109", "65", 'Y', 3, 2,     // FICTITIOUS TRUST DEED
   "111", "  ", 'Y', 3, 2,     // JUDGMENT
   "112", "74", 'Y', 3, 2,     // LETTERS MISCELLANEOUS
   "113", "  ", 'Y', 3, 2,     // MEMORANDUM
   "114", "  ", 'Y', 3, 2,     // OFFER
   "115", "  ", 'Y', 3, 2,     // PARTIAL WITHDRAWAL
   "118", "  ", 'Y', 3, 2,     // RIGHT OF ENTRY
   "119", "46", 'Y', 3, 2,     // SUBORDINATION LIEN
   "120", "  ", 'Y', 3, 2,     // VOLUNTARY PETITION
   "121", "  ", 'Y', 3, 2,     // WRIT OF SALE
   "122", "  ", 'Y', 3, 2,     // X AGREEMENT
   "123", "  ", 'Y', 3, 2,     // X ASSIGNMENT
   "125", "  ", 'Y', 3, 2,     // X RECONVEYANCE
   "126", "  ", 'Y', 3, 2,     // X TRUST
   "127", "  ", 'Y', 3, 2,     // X TRUST DEED
   "128", "  ", 'Y', 3, 2,     // FIXTURE FILING
   "129", "  ", 'Y', 3, 2,     // SECURITY AGREEMENT
   "130", "  ", 'Y', 3, 2,     // ABANDONMENT
   "131", "  ", 'Y', 3, 2,     // RESCISSION
   "132", "  ", 'Y', 3, 2,     // CERTIFICATION OF TRUST
   "133", "  ", 'Y', 3, 2,     // MILITARY DISCHARGE
   "134", "  ", 'Y', 3, 2,     // BANKRUPTCY ORDER
   "139", "  ", 'Y', 3, 2,     // LIMITED LIABILITY COMPANY
   "140", "9 ", 'Y', 3, 2,     // CERTIFICATE OF CORRECTION
   "142", "  ", 'Y', 3, 2,     // LOT MERGER
   "143", "  ", 'Y', 3, 2,     // CERTIFICATE OF COMPLIANCE
   "144", "  ", 'Y', 3, 2,     // MODIFICATION TRUST DEED
   "147", "67", 'N', 3, 2,     // TAX DEED TO PURCHASE
   "148", "  ", 'Y', 3, 2,     // AMENDED LETTERS
   "149", "  ", 'Y', 3, 2,     // MORTGAGE
   "150", "  ", 'Y', 3, 2,     // DISCLAIMER
   "152", "  ", 'Y', 3, 2,     // WITHDRAWAL OF LIEN
   "153", "  ", 'Y', 3, 2,     // BY LAWS
   "154", "  ", 'Y', 3, 2,     // REQUEST FOR DISMISSAL
   "155", "  ", 'Y', 3, 2,     // SEVERANCE OF JOINT TENANCY
   "156", "  ", 'Y', 3, 2,     // TERMINATION AGREEMENT
   "157", "  ", 'Y', 3, 2,     // NOTICE OF ADVANCE
   "159", "  ", 'Y', 3, 2,     // RESIGNATION OF TRUSTEE
   "160", "  ", 'Y', 3, 2,     // APPOINTMENT OF TRUSTEE
   "161", "  ", 'Y', 3, 2,     // LICENSE
   "162", "  ", 'Y', 3, 2,     // AMENDMENT MISCELLANEOUS
   "163", "  ", 'Y', 3, 2,     // RIGHT OF REFUSAL
   "164", "  ", 'Y', 3, 2,     // SUBSTANDARD BUILDING
   "165", "  ", 'Y', 3, 2,     // CERTIFICATE OF COMPLETION
   "166", "  ", 'Y', 3, 2,     // NOTICE OF SALE
   "167", "  ", 'Y', 3, 2,     // SATISFACTION
   "168", "  ", 'Y', 3, 2,     // X OPTION
   "170", "  ", 'Y', 3, 2,     // DISCHARGE LIEN
   "172", "  ", 'Y', 3, 2,     // COVENANT
   "173", "  ", 'Y', 3, 2,     // STIPULATION
   "174", "  ", 'Y', 3, 2,     // AMEND BY LAWS
   "175", "  ", 'Y', 3, 2,     // AMEND ASSIGN RENT
   "176", "  ", 'Y', 3, 2,     // AMEND DECREE DISTRIBUTION
   "177", "  ", 'Y', 3, 2,     // WATER RIGHTS
   "179", "  ", 'Y', 3, 2,     // AMEND JUDGMENT
   "180", "  ", 'Y', 3, 2,     // ACCEPTANCE OF TRUSTEE
   "182", "  ", 'Y', 3, 2,     // ASSUMPTION AGREEMENT
   "183", "  ", 'Y', 3, 2,     // RENEW JUDGMENT
   "184", "  ", 'Y', 3, 2,     // AMEND EASEMENT
   "185", "  ", 'Y', 3, 2,     // DISMISSAL
   "186", "  ", 'Y', 3, 2,     // NOTICE OF COMPLIANCE
   "187", "  ", 'Y', 3, 2,     // TERMINATION
   "188", "9 ", 'Y', 3, 2,     // CORRECT DEED
   "190", "  ", 'Y', 3, 2,     // AMEND LIEN
   "192", "7 ", 'Y', 3, 2,     // ASSIGNMENT OF DEED OF TRUST
   "193", "  ", 'Y', 3, 2,     // TEMPORARY EASEMENT
   "194", "  ", 'Y', 3, 2,     // RESTRICTIVE COVENANT MODIF
   "196", "  ", 'Y', 3, 2,     // CONDITION COMPLIANCE
   "197", "  ", 'Y', 3, 2,     // CORRECT ASSIGNMENT
   "198", "  ", 'Y', 3, 2,     // REQUEST RECONVEYANCE
   "200", "  ", 'Y', 3, 2,     // MINERAL CONVEYANCE
   "202", "  ", 'Y', 3, 2,     // RESCISSION OF RESTRICTION
   "203", "  ", 'Y', 3, 2,     // CONDOMINIUM PLAN
   "204", "  ", 'Y', 3, 2,     // MEMORANDUM OF LEASE
   "205", "  ", 'Y', 3, 2,     // STATEMENT OF DISSOCIATION
   "206", "  ", 'Y', 3, 2,     // AMEND CONSTRUCTION TRUST DEED
   "207", "  ", 'Y', 3, 2,     // ADDENDUM
   "208", "  ", 'Y', 3, 2,     // STATEMENT
   "209", "  ", 'Y', 3, 2,     // ARTICLES OF INCORPORATION
   "211", "  ", 'Y', 3, 2,     // REQUEST NOTICE OF DELINQUENCY
   "212", "  ", 'Y', 3, 2,     // SITE PLAN REVIEW
   "213", "  ", 'Y', 3, 2,     // AMEND DEED
   "215", "  ", 'Y', 3, 2,     // RULING ON MOTION
   "219", "46", 'Y', 3, 2,     // GRANT LIEN
   "221", "  ", 'Y', 3, 2,     // CERT OF DELINQUENCY
   "223", "  ", 'Y', 3, 2,     // PARTIAL ASSIGNMENT
   "224", "  ", 'Y', 3, 2,     // DISCHARGE
   "225", "75", 'Y', 3, 2,     // TRUST TRANSFER
   "228", "  ", 'Y', 3, 2,     // ACCEPTANCE
   "230", "  ", 'Y', 3, 2,     // LIEN AGREEMENT
   "231", "  ", 'Y', 3, 2,     // X ASSIGNMENT OF RENTS
   "233", "  ", 'Y', 3, 2,     // AMEND LIMITED LIABILITY CO
   "235", "  ", 'Y', 3, 2,     // AMEND LIS PENDENS
   "236", "  ", 'Y', 3, 2,     // ASSIGN SECURITY AGREEMENT
   "238", "  ", 'Y', 3, 2,     // PARTIAL TERMINATION AGREEMENT
   "241", "  ", 'Y', 3, 2,     // REVOC RESTRICTION
   "242", "  ", 'Y', 3, 2,     // ???
   "243", "  ", 'Y', 3, 2,     // FINAL DECREE
   "244", "  ", 'Y', 3, 2,     // REQUEST FOR LOT MERGER
   "247", "  ", 'Y', 3, 2,     // AMENDMENT OF AFFIDAVIT
   "252", "  ", 'Y', 3, 2,     // AMEND LIMITED LIABILITY CO
   "253", "  ", 'Y', 3, 2,     // CONSERVATION EASEMENT
   "258", "  ", 'Y', 3, 2,     // MODIFICATION SECURITY AGREE
   "260", "  ", 'Y', 3, 2,     // X EASEMENT
   "261", "  ", 'Y', 3, 2,     // RELEASE OF BOND
   "264", "  ", 'Y', 3, 2,     // AMENDMENT OF CERTIFICATE
   "265", "  ", 'Y', 3, 2,     // ENVIRONMENTAL INDEMNITY
   "266", "  ", 'Y', 3, 2,     // NOTICE OF DELINQUENT ASMT
   "271", "  ", 'Y', 3, 2,     // REVOCATION OF DEED
   "272", "  ", 'Y', 3, 2,     // CORRECT DEED OF TRUST
   "273", "  ", 'Y', 3, 2,     // EXTENSION AGREEMENT
   "274", "  ", 'Y', 3, 2,     // CANCELLATION
   "277", "  ", 'Y', 3, 2,     // AMEND ASSIGNMENT
   "279", "  ", 'Y', 3, 2,     // NOTICE TO SELL
   "284", "  ", 'Y', 3, 2,     // ???
   "287", "  ", 'Y', 3, 2,     // NOTICE OF LEVY
   "288", "  ", 'Y', 3, 2,     // WRIT OF EXECUTION
   "289", "  ", 'Y', 3, 2,     // CONDITIONS OF PERMIT APPROVAL
   "293", "  ", 'Y', 3, 2,     // NOTICE OF VIOLATION
   "294", "  ", 'Y', 3, 2,     // REVOCATION
   "297", "  ", 'Y', 3, 2,     // CORRECT SUBSTITUTION TRUSTEE
   "298", "  ", 'Y', 3, 2,     // CORRECT RECONVEYANCE
   "300", "  ", 'Y', 3, 2,     // AMENDMENT TO OPTION
   "304", "  ", 'Y', 3, 2,     // ???
   "305", "  ", 'Y', 3, 2,     // RATIFICATION & JOINDER
   "307", "  ", 'Y', 3, 2,     // CORRECT AFFIDAVIT
   "309", "  ", 'Y', 3, 2,     // LIEN AGREEMENT
   "311", "  ", 'Y', 3, 2,     // CORRECT TRUST
   "312", "  ", 'Y', 3, 2,     // DEED NOTIFICATION
   "314", "  ", 'Y', 3, 2,     // AMEND SATISFACTION
   "315", "  ", 'Y', 3, 2,     // AMEND RELEASE
   "317", "  ", 'Y', 3, 2,     // PRESERVATION CONTRACT
   "318", "  ", 'Y', 3, 2,     // ASSIGNMENT OF LEASE
   "319", "  ", 'Y', 3, 2,     // REVOCATION OF RELEASE
   "320", "  ", 'Y', 3, 2,     // REMOVAL OF LIEN
   "321", "  ", 'Y', 3, 2,     // ROYALTY CONVEYANCE
   "322", "  ", 'Y', 3, 2,     // AMEND DECLARATION
   "323", "  ", 'Y', 3, 2,     // AMEND NOTICE
   "324", "  ", 'Y', 3, 2,     // DISCHARGE MORTGAGE
   "327", "  ", 'Y', 3, 2,     // SUBORDINATION OF TRUST DEED
   "328", "  ", 'Y', 3, 2,     // SUBORDINATION OF LEASE
   "329", "  ", 'Y', 3, 2,     // EXPUNGE ORDER
   "330", "  ", 'Y', 3, 2,     // NOTICE OF REASSESSMENT
   "331", "  ", 'Y', 3, 2,     // NOTICE OF NON RENEWAL
   "332", "  ", 'Y', 3, 2,     // RESTATED PARTNERSHIP
   "333", "  ", 'Y', 3, 2,     // NT OF PARTIAL NON RENEWAL
   "334", "  ", 'Y', 3, 2,     // AMEND CONDO PLAN
   "335", "  ", 'Y', 3, 2,     // WITHDRAWAL
   "336", "  ", 'Y', 3, 2,     // MODIFICATION OF EASEMENT
   "337", "  ", 'Y', 3, 2,     // DECREE
   "338", "  ", 'Y', 3, 2,     // CORRECTION OF AGREEMENT
   "340", "37", 'Y', 3, 2,     // CONVEYANCE
   "341", "4 ", 'Y', 3, 2,     // QUITCLAIM
   "342", "  ", 'Y', 3, 2,     // CONDITIONAL USE PERMIT
   "343", "  ", 'Y', 3, 2,     // MODIFY OIL & GAS LEASE
   "344", "75", 'Y', 3, 2,     // ???
   "346", "  ", 'Y', 3, 2,     // EXTENSION OF CREDIT
   "347", "  ", 'Y', 3, 2,     // VARIANCE
   "348", "  ", 'Y', 3, 2,     // EXEMPTION
   "349", "  ", 'Y', 3, 2,     // ASSIGNMENT OF RIGHTS
   "350", "  ", 'Y', 3, 2,     // RESTATED RESTRICTION
   "352", "  ", 'Y', 3, 2,     // RELINQUISHMENT
   "353", "  ", 'Y', 3, 2,     // AMEND CONSERVATION CONTRACT
   "355", "  ", 'Y', 3, 2,     // CORRECT STATEMENT
   "356", "  ", 'Y', 3, 2,     // RIGHT OF SPOUSE
   "357", "  ", 'Y', 3, 2,     // ASSIGNMENT OF INTEREST
   "358", "4 ", 'Y', 3, 2,     // QUIT CLAIM
   "377", "75", 'Y', 3, 2,     // ???
   "378", "  ", 'Y', 3, 2,     // ??? Mortgage related
   "384", "19", 'Y', 3, 2,     // ??? Tax related
   "411", "75", 'Y', 3, 2,     // ???
   "412", "75", 'Y', 3, 2,     // ???
   "413", "75", 'Y', 3, 2,     // ???

   "A0",  "  ", 'Y', 2, 2,     // FINANCING STATEMENT
   "A1",  "  ", 'Y', 2, 2,     // FINANCING STAMT. CONTINUATION
   "A3",  "  ", 'Y', 2, 2,     // FINANCING STMT. ASIGNMENT
   "A4",  "  ", 'Y', 2, 2,     // FINANCING STMT. TERMINATION
   "A5",  "  ", 'Y', 2, 2,     // FINANCING STATEMENT AMENDMENT
   "","",0,0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 MON_Exemption[] = 
{
   "E01", "H", 3,1,
   "E02", "R", 3,1,
   "E03", "W", 3,1,     // CHARITABLE EXEMP
   "E07", "D", 3,1,
   "E08", "I", 3,1,
   "E09", "S", 3,1,
   "E12", "C", 3,1, 
   "E14", "U", 3,1,     // PRIVATELY OWNED COLLEGE
   "E15", "E", 3,1,
   "E16", "L", 3,1,     // QUALIFIED LESSORS FREE PUBLIC LIBRARY/SCHOOL/MUSEUM
   "E17", "D", 3,1,
   "E34", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E35", "X", 3,1,     // NOT OVER 40,000
   "E36", "X", 3,1,     // NOT OVER 127,510
   "","",0,0
};

#endif