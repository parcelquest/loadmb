#ifndef  _MERGELAS_H_
#define  _MERGELAS_H_

// Copy from HUM
#define  LAS_CHAR_FEEPARCEL            0
#define  LAS_CHAR_POOLSPA              1
#define  LAS_CHAR_CATTYPE              2
#define  LAS_CHAR_QUALITYCLASS         3
#define  LAS_CHAR_YRBLT                4
#define  LAS_CHAR_BUILDINGSIZE         5
#define  LAS_CHAR_ATTACHGARAGESF       6
#define  LAS_CHAR_DETACHGARAGESF       7
#define  LAS_CHAR_CARPORTSF            8
#define  LAS_CHAR_HEATING              9
#define  LAS_CHAR_COOLINGCENTRALAC     10
#define  LAS_CHAR_COOLINGEVAPORATIVE   11
#define  LAS_CHAR_COOLINGROOMWALL      12
#define  LAS_CHAR_COOLINGWINDOW        13
#define  LAS_CHAR_STORIESCNT           14
#define  LAS_CHAR_UNITSCNT             15
#define  LAS_CHAR_TOTALROOMS           16
#define  LAS_CHAR_EFFYR                17
#define  LAS_CHAR_PATIOSF              18
#define  LAS_CHAR_BEDROOMS             19
#define  LAS_CHAR_BATHROOMS            20
#define  LAS_CHAR_HALFBATHS            21
#define  LAS_CHAR_FIREPLACE            22
#define  LAS_CHAR_ASMT                 23
#define  LAS_CHAR_BLDGSEQNUM           24
#define  LAS_CHAR_HASWELL              25
#define  LAS_CHAR_LOTSQFT              26
#define  LAS_CHAR_PARKINGSPACES        27
#define  LAS_CHAR_FLDS                 28

// NEV has no fireplace data - 20180618
static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "1",  "1", 1,               // 1
   "2",  "2", 1,               // 2
   "3",  "3", 1,               // 3+
   "GA", "G", 1,               // Gas
   "MA", "L", 1,               // Masonry
   "MU", "M", 1,               // Multiple
   "N",  "N", 1,               // None
   "PS", "S", 1,               // PelletStove
   "UN", "U", 1,               // Unknown Type
   "WS", "W", 1,               // Woodstove
   "ZC", "Z", 1,               // ZeroClearance
   "Z",  "U", 1,               // Unknown
   "",   "", 0
};

//static XLAT_CODE  asSewer[] =
//{
//   // Value, lookup code, value length
//   "E", "E", 1,               // Engineered system
//   "O", "Z", 1,               // Other than sewer or septic
//   "S", "Y", 1,               // Sewer
//   "T", "S", 1,               // Septic tank
//   "",   "",  0
//};

//static XLAT_CODE  asWaterSrc[] =
//{
//   // Value, lookup code, value length
//   "1", "P", 2,               // Community
//   "2", "W", 2,               // Well
//   "4", "Y", 2,               // Other
//   "",   "",  0
//};

static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "BB", "8", 2,              // Baseboard
   "EL", "7", 2,              // Electric
   "FH", "X", 2,              // FAH
   "FL", "C", 2,              // Floor
   "GR", "A", 2,              // Grav
   "HP", "G", 2,              // Heat Pump 
   "MO", "V", 2,              // Monitor
   "PE", "X", 2,              // Perim
   "WL", "D", 2,              // Wall Unit
   "WS", "S", 2,              // Woodstove
   "Z",  "9", 1,              // Unknown
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "1",  "C", 1,              // Pool/Spa
   "C",  "D", 1,              // Community
   "FG", "F", 2,              // Fiberglass
   "GN", "G", 2,              // Gunite
   "ID", "I", 2,              // Indoor
   "PAG","X", 3,              // Permanent Above Ground
   "PH", "C", 2,              // Pool & Hot Tub 
   "PO", "P", 2,              // Pool (any)
   "VY", "V", 1,              // Vinyl (In Ground)
   "",   "",  0
};


IDX_TBL5 LAS_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "28", "75", 'Y', 2, 2,     // Transfers to Individuals - Sales price "NOT" accepted. Start 4-19-11
   "29", "75", 'Y', 2, 2,     // Transfers that are "NOT" a sale (Date of Death, Gift, Trustee Deed Forclosure, etc). New for 2008 Roll. Start 5-7-08								
   "30", "1 ", 'N', 2, 2,     // Ownership Change:  100% "Reappraisable" Sale.
   "31", "57", 'N', 2, 2,     // Partial Ownership Change:  Less than 100% "Reappraisable" Sale or Lot Line Adjustment or C.I.O. on MH only when MH is combined to land.
   "32", "1 ", 'N', 2, 2,     // Ownership Change & Major New Construction in same Assessment year.
   "33", "1 ", 'N', 2, 2,     // Ownership Change & Minor New Construction in same Assessment year.
   "34", "74", 'Y', 2, 2,     // Major New Construction:  Above 25% of Improvement Taxable Value Prior to Factor.
   "35", "74", 'Y', 2, 2,     // Minor New Construction:  Below 25% of Improvement Taxable Value Prior to Factor.
   "37", "74", 'Y', 2, 2,     // Value Change Due to a Change in Use.
   "38", "74", 'Y', 2, 2,     // Restricted Properties:  Any Change; New Construction, Compatible Uses, Non-Renewals, Cancellations, etc.
   "39", "74", 'Y', 2, 2,     // No Change in Base Year Value:  Prorate Share of Base Year Value; Segregations, Splits & Combinations retained by Original Owner.
   "40", "75", 'Y', 2, 2,     // Mobilehomes:  Newly Enrolled Mobilehome and M.H. voluntarily converted to L.P.T.
   "41", "74", 'Y', 2, 2,     // Disaster Relief:  Sections 69, 70 & 170 of R & T Code.
   "42", "74", 'Y', 2, 2,     // Proposition 8:  Current Market Value Less Than Factored Base Year Value.
   "43", "74", 'Y', 2, 2,     // Appeal Board Changes:  Change of Local Board of Equalization.
   "44", "74", 'Y', 2, 2,     // Value Changes Involving Tax Deeded Properties:  Full & Partial Cancellations for Government Acquisitions & Annexation.								
   "45", "74", 'Y', 2, 2,     // Voluntary Removal of Taxable Property.
   "46", "74", 'Y', 2, 2,     // Newly Discovered Structure/Land Improvements:  Previously Unassessed; Done Without Permit or No Permit Required.
   "47", "74", 'Y', 2, 2,     // Non-Board Change Corrections, Low Value Escapes,  Assessor Errors & Other Misc. Changes. NEW FOR 2005 Roll - Start 4/19/04
   "48", "74", 'Y', 2, 2,     // Board Changes (Escaped Assessments, Assessor Errors & Corrections, Misc.). 
   "56", "74", 'Y', 2, 2,     // Williamson Act Values.
   "58", "74", 'Y', 2, 2,     // Timber Preserve Zone (TPZ) Values.
   "60", "75", 'Y', 2, 2,     // Prop. 60:  Intracounty Replacement Dwelling; Section 69.5.
   "","",0,0,0
};

//USEXREF  Las_UseTbl[] =
//{
//   "00","120",              //VACANT-RESIDENTIAL TO 2.5 AC
//   "01","115",              //MOBILEHOME ON UP TO 2.5 ACS.
//   "02","100",              //NON-RES IMPRVMNT TO 2.5 ACS.
//   "05","120",              //VACANT - MULTI-RESIDENTIAL
//   "11","100",              //RESIDENTIAL IMPRVD TO 2.5 AC
//   "12","100",              //MULTI-RESID. 2-3 UNITS
//   "13","100",              //MULTI-RESID. 4 PLUS UNITS
//   "14","100",              //CONDMINIUMS/TOWNHOUSES     '
//   "15","813",              //POSS. INT.-FOREST SER. CABIN
//   "16","115",              //MOBILEHOMES
//   "17","791",              //OPEN SPACE CONTRACTS
//   "21","175",              //RURAL VACANT 2.5-20 ACRES
//   "22","175",              //RURAL IMPRVD 2.5-20 ACRES
//   "23","175",              //RURAL SUB-ECONOMIC UNIT
//   "24","175",              //RURAL ECONOMIC UNIT 20+ACRES
//   "25","175",              //RURAL RESTRICTED - CLCA
//   "26","175",              //RURAL RESTRICTED - CLCA
//   "28","115",              //MOBILEHOME - 2.5+ ACRES
//   "29","100",              //NON-RES IMPRVMNT -2.5+ ACRES
//   "30","835",              //COMMERCIAL VACANT
//   "31","202",              //COMMERCIAL -MINOR IMPRVMNT
//   "32","219",              //COMMERCIAL -MAJOR IMPRVMNT
//   "33","110",              //MOTELS
//   "34","234",              //SERVICE STATIONS
//   "35","114",              //MOBILEHOME PARKS
//   "40","190",              //INDUSTRIAL VACANT
//   "41","400",              //INDUSTRIAL IMPROVED
//   "50","535",              //TIMBER PRESERVE ZONES
//   "51","536",              //TIMBER RIGHTS
//   "60","600",              //RECREATIONAL PROPERTIES
//   "70","783",              //MINERAL RIGHTS
//   "75","801",              //GRAZING RIGHTS
//   "80","100",              //RESIDENTIAL - TIMESHARE
//   "", ""
//};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

// This table needs updated info before make production
IDX_TBL4 LAS_Exemption[] = 
{
   "E01", "H", 3,1,     // HOMEOWNER'S EXEMPTION
   "E02", "D", 3,1,     // 100% DISABLED VETERAN
   "E04", "W", 3,1,     // WELFARE
   "E05", "R", 3,1,     // RELIGIOUS
   "E06", "C", 3,1,     // CHURCH
   "E07", "E", 3,1,     // CEMETERY
   "E08", "D", 3,1,     // 100% DISABLED VETERAN 
   "E09", "X", 3,1,     // VESSEL
   "E10", "P", 3,1,     // PUBLIC SCHOOLS
   "E11", "X", 3,1,     // LESSEE/LESSOR
   "E12", "I", 3,1,     // WELFARE: HOSPITAL/CLINIC
   "E13", "M", 3,1,     // FREE MUSEUM/FREE LIBRARY
   "E14", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E16", "W", 3,1,     // WELFARE (LI HOUSING)
   "E94", "X", 3,1,     // UNSECURED LOW VALUE
   "E95", "X", 3,1,     // LOW VALUE ORDINANCE
   "E96", "X", 3,1,     // LOW VALUE PROP AND FIXT
   "E98", "X", 3,1,     // OTHER
   "","",0,0
};


#endif
