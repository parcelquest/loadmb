
#if !defined(AFX_MERGESJX_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_)
#define AFX_MERGESJX_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_

// CHAR file has the same layout as HUM
#define  SJX_CHAR_FEEPARCEL            0
#define  SJX_CHAR_POOLSPA              1
#define  SJX_CHAR_CATTYPE              2
#define  SJX_CHAR_QUALITYCLASS         3
#define  SJX_CHAR_YRBLT                4
#define  SJX_CHAR_BUILDINGSIZE         5
#define  SJX_CHAR_ATTACHGARAGESF       6
#define  SJX_CHAR_DETACHGARAGESF       7
#define  SJX_CHAR_CARPORTSF            8
#define  SJX_CHAR_HEATING              9
#define  SJX_CHAR_COOLINGCENTRALAC     10
#define  SJX_CHAR_COOLINGEVAPORATIVE   11
#define  SJX_CHAR_COOLINGROOMWALL      12
#define  SJX_CHAR_COOLINGWINDOW        13
#define  SJX_CHAR_STORIESCNT           14
#define  SJX_CHAR_UNITSCNT             15
#define  SJX_CHAR_TOTALROOMS           16
#define  SJX_CHAR_EFFYR                17
#define  SJX_CHAR_PATIOSF              18
#define  SJX_CHAR_BEDROOMS             19
#define  SJX_CHAR_BATHROOMS            20
#define  SJX_CHAR_HALFBATHS            21
#define  SJX_CHAR_FIREPLACE            22
#define  SJX_CHAR_ASMT                 23
#define  SJX_CHAR_BLDGSEQNUM           24
#define  SJX_CHAR_HASWELL              25
#define  SJX_CHAR_LOTSQFT              26

static XLAT_CODE  asCond[] =
{
   // Value, lookup code, value length
   "A", "A", 1,               // Average
   "E", "E", 1,               // Excellent
   "F", "F", 1,               // Fair
   "G", "G", 1,               // Good
   "N", "N", 1,               // New
   "P", "P", 1,               // Poor
   "U", "U", 1,               // Unsound
   "",  "",  0
};

static XLAT_CODE  asFirePlace[] =
{  // 2/27/2018
   // Value, lookup code, value length
   "WS", "W", 2,             // Wood Stove
   "Y",  "Y", 1,             // Yes
   "",   "", 0
};

static XLAT_CODE  asHeating[] =
{  // 3/2/2018
   // Value, lookup code, value length
   "C",  "Z", 1,              // Central
   "FAU","B", 3,              // Force Air Unit
   "FAZ","T", 3,              // Force Air Zone
   "FU", "C", 2,              // Floor unit
   "N",  "L", 1,              // None
   "SP", "J", 2,              // Space heater
   "ST", "H", 2,              // Steam
   "WHO","S", 3,              // Wood heat only
   "WU", "M", 2,              // Wall unit
   "",   "",  0
};

static XLAT_CODE  asPool[] =
{  // 3/2/2018
   // Value, lookup code, value length
   "DB",  "X", 2,             // Above ground Doughboy
   "FBG", "F", 3,             // Fiberglass inground
   "GPS", "C", 3,             // Gunite Pool/Spa
   "GP",  "G", 2,             // Gunite pool
   "N",   "N", 1,             // None
   "PS",  "C", 2,             // Pool/Spa
   "P",   "P", 1,             // Pool
   "S",   "S", 1,             // Spa
   "VIP", "B", 3,             // Vinyl inground pool
   "VP",  "V", 2,             // Vinyl pool
   "VS",  "S", 2,             // Vinyl Spa
   "Y",   "P", 1,             // Pool
   "",   "", 0
};

static XLAT_CODE  asSewer[] =
{
   // Value, lookup code, value length
   "P", "P", 1,               // Public
   "S", "S", 1,               // Septic
   "",   "",  0
};

static XLAT_CODE  asWaterSrc[] =
{
   // Value, lookup code, value length
   "M", "P", 1,               // Municipal
   "W", "W", 1,               // Private Well
   "",  "",  0
};

#define MAX_XFER_TYPE   12
static XLAT_CODE  asTranferTypes[] =
{
   "F",  "F", 1,              // Full value
   "F%", "F", 2,              // Full value - Percentage transfer
   "FV", "F", 2,              // Full value
   "L%", "P", 2,              // Less lien - Percentage transfer
   "LL", "N", 2,              // Less lien
   "M",  "W", 1,              // Multi Interest Transfer
   "P",  "P", 1,              // Partial Interest Transfer
   "W%", "P", 2,              // Multi-parcel percentage transfer
   "WL", "I", 2,              // With other properties less lien
   "WP", "W", 2,              // With other properties
   "Y",  "U", 1,              // Unknown
   "",   "",  0
};

IDX_TBL5 SJX_DocCode[] =
{  // DocCode, Index, Non-sale, DocCodelen, Indexlen
   "01", "75", 'Y', 2, 2,     // ROLL CHANGE - TRANSFERS (DOD, ETC)
   "02", "13", 'N', 2, 2,     // REVALUATION OF 100% TRANSFER OTHER THAN SALE PRICE
   "03", "57", 'N', 2, 2,     // REVALUATION OF PARTIAL INTEREST TRANSFER
   "04", "13", 'N', 2, 2,     // DIRECT ENROLLMENT TRANSFER
   "05", "1 ", 'N', 2, 2,     // REVALUATION OF 100% TRANSFER AT SALE PRICE
   "06", "19", 'Y', 2, 2,     // REVALUATION DUE TO LEASE IN EXCESS OF 35 YEARS
   "07", "13", 'N', 2, 2,     // VALUE ADJUSTMENT (COMPUTER TRENDING)
   "08", "13", 'N', 2, 2,     // ROLL CHANGE - MISCELLANEOUS (OTHER THAN TRANSFER)
   "09", "13", 'N', 2, 2,     // DIRECT ENROLLMENT
   "10", "74", 'Y', 2, 2,     // SELF REPORTED RESIDENTIAL IMPROVEMENT
   "11", "19", 'N', 2, 2,     // NEW SINGLE FAMILY RESIDENCE
   "12", "74", 'Y', 2, 2,     // RESIDENTIAL ADD, ALTER AND REMODEL
   //"13", "19", 'Y', 2, 2,     // NEW MULTI FAMILY IMPROVEMENT
   //"14", "19", 'Y', 2, 2,     // NEW GARAGE/CARPORT
   //"15", "19", 'Y', 2, 2,     // NEW SWIMMING POOL/SPA
   //"16", "19", 'Y', 2, 2,     // DWELLING ADD'N/CONVERSION
   //"17", "19", 'Y', 2, 2,     // GAR ADD'N/CONV/PATIO/DECK
   //"18", "19", 'Y', 2, 2,     // MISC ADD'N
   "19", "74", 'Y', 2, 2,     // RESIDENTIAL DEMOLITION
   "20", "74", 'Y', 2, 2,     // SELF REPORTED COMMERCIAL IMPROVEMENT
   "21", "74", 'Y', 2, 2,     // NEW COMMERCIAL IMPROVEMENT
   "22", "74", 'Y', 2, 2,     // COMMERCIAL ADD, ALTER AND REMODEL
   //"23", "19", 'Y', 2, 2,     // NEW MULTIPLE BLDG
   //"24", "19", 'Y', 2, 2,     // ADD'N/CONV TO MULTIPLES
   //"25", "19", 'Y', 2, 2,     // MISC MULTIPLES
   //"26", "19", 'Y', 2, 2,     // HOX FUTURE CANX
   "29", "74", 'Y', 2, 2,     // COMMERCIAL DEMOLITION
   //"30", "19", 'Y', 2, 2,     // SELF REPORTED INDUSTRIAL IMPROVEMENT
   //"31", "19", 'Y', 2, 2,     // NEW INDUSTRIAL IMPROVEMENT
   //"32", "19", 'Y', 2, 2,     // INDUSTRIAL ADD, ALTER AND REMODEL
   //"33", "19", 'Y', 2, 2,     // NEW RURAL BLDG
   //"34", "19", 'Y', 2, 2,     // RURAL BLDG ADD'N
   //"35", "19", 'Y', 2, 2,     // MISC RURAL ADD'N
   //"37", "19", 'Y', 2, 2,     // LAND IMPROVEMENTS
   "39", "74", 'Y', 2, 2,     // INDUSTRIAL DEMOLITION
   "40", "74", 'Y', 2, 2,     // SELF REPORTED AGRICULTURAL IMPROVEMENT
   "41", "19", 'Y', 2, 2,     // NEW AGRICULTURAL BUILDINGS
   //"42", "19", 'Y', 2, 2,     // AGRICULTURAL BUILDINGS ADD, ALTER OR REMODEL
   "43", "74", 'Y', 2, 2,     // MISC AGRICULTURAL IMPR (STAKES, IRRIG SYS, ETC)
   "44", "74", 'Y', 2, 2,     // ADD LIVING IMPROVEMENTS
   "45", "74", 'Y', 2, 2,     // REMOVE LIVING IMPROVEMENTS
   //"46", "19", 'Y', 2, 2,     // MISC INDUSTRIAL
   "49", "74", 'Y', 2, 2,     // AGRICULTURAL DEMOLITION (NON-LIVING IMPROVEMENTS)
   "51", "19", 'Y', 2, 2,     // SPLIT
   "52", "19", 'Y', 2, 2,     // COMBINATION
   //"53", "19", 'Y', 2, 2,     // OFFSITE IMPROVEMENTS
   //"54", "19", 'Y', 2, 2,     // ADD'N/CONV COMM'L
   "55", "19", 'Y', 2, 2,     // NEW TAXABLE PARCEL
   //"56", "19", 'Y', 2, 2,     // MAP REDRAW
   "58", "75", 'Y', 2, 2,     // PROP 58 NO REAPPRAISAL - Parent/Child transfer
   "60", "19", 'Y', 2, 2,     // WILLIAMSON ACT - NON RENEWAL
   "61", "19", 'Y', 2, 2,     // WILLIAMSON ACT - MISCELLANEOUS ACTIVITY
   //"62", "19", 'Y', 2, 2,     // WILLIAMSON ACT - R 34 423.3
   //"63", "19", 'Y', 2, 2,     // FLOOD DAMAGE - PERIODIC REVIEW
   "64", "19", 'Y', 2, 2,     // FIRE DAMAGE - PERIODIC REVIEW
   "65", "19", 'Y', 2, 2,     // MARKET VALUE BELOW FACTORED BASE VALUE - P8 REVIEW
   //"66", "19", 'Y', 2, 2,     // LEASE CHANGE - PERIODIC REVIEW
   //"67", "19", 'Y', 2, 2,     // TR REVALUATION DUE TO CONDEMATION-PROP 3
   "68", "19", 'Y', 2, 2,     // MOBILEHOME ACTIVITY
   "69", "19", 'Y', 2, 2,     // MISCELLANEOUS ACTIVITY
   //"70", "19", 'Y', 2, 2,     // LIEN DATE UPDATE
   //"71", "19", 'Y', 2, 2,     // PROP 8
   "72", "19", 'Y', 2, 2,     // POSSESSORY INTEREST
   //"73", "19", 'Y', 2, 2,     // UNS. BUSINESS DIV.ONLY/NO RE
   "74", "19", 'Y', 2, 2,     // BOARD ORDER VALUE
   //"75", "19", 'Y', 2, 2,     // SOLAR IMPROVEMENT
   //"76", "19", 'Y', 2, 2,     // UNS RE IMPS WITH  PP OR NO(850)
   "78", "19", 'Y', 2, 2,     // ???
   //"79", "19", 'Y', 2, 2,     // Const. In Progress - CIP
   "80", "19", 'Y', 2, 2,     // APPEALS
   //"81", "19", 'Y', 2, 2,     // AB 1620 FOR COMMERCIAL PROPERTIES
   //"82", "19", 'Y', 2, 2,     // ROAD TAKE
   //"83", "19", 'Y', 2, 2,     // NEW MOBILE HOME
   //"84", "19", 'Y', 2, 2,     // POSSESSORY INTEREST
   //"85", "19", 'Y', 2, 2,     // IREO
   "86", "19", 'Y', 2, 2,     // NO CHANGE IN VALUE
   "87", "19", 'Y', 2, 2,     // ???
   "90", "19", 'Y', 2, 2,     // ???
   "92", "75", 'Y', 2, 2,     // PROP 58 EXCLUSION (Parent/child transfer)
   "93", "19", 'Y', 2, 2,     // PROP 60 EXCLUSION (Proposition 60 is a constitutional amendment that allows the transfer of the base year value of your current primary residence to your newly acquired property of primary residence.)
   //"94", "19", 'Y', 2, 2,     // FIXTURES (SUPL ONLY)
   "95", "13", 'N', 2, 2,     // PRIMARY DIRECT ENROLLMENT
   //"96", "19", 'Y', 2, 2,     // MRA DIRECT ENROLLMENT
   "97", "13", 'N', 2, 2,     // REGULAR DIRECT ENROLLMENT
   //"99", "19", 'Y', 2, 2,     // FORMAT RECORD-END OF DOC CODES 
   "","",0,0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SJX_Exemption[] = 
{
   "E01", "H", 3,1,
   "E03", "V", 3,1,     // VETERAN (ON OWN PROPERTY)
   "E04", "V", 3,1,     // VETERAN (HUSBAND ON WIFE'S PROPERTY)
   "E05", "V", 3,1,     // VETERAN (DOUBLE)
   "E06", "D", 3,1,     // VETERAN (DISABLED, BASIC)
   "E07", "D", 3,1,     // VETERAN (DISABLED, LOW INCOME)
   "E08", "W", 3,1,     // WELFARE (CHARITABLE)
   "E09", "S", 3,1,     // WELFARE (SCHOOL - EDUCATIONAL)
   "E10", "I", 3,1,     // WELFARE (HOSPITAL)
   "E11", "R", 3,1,     // WELFARE (RELIGIOUS)
   "E16", "E", 3,1,     // CEMETERY
   "E17", "C", 3,1,     // CHURCH
   "E18", "M", 3,1,     // FREE MUSEUM, FREE PUBLIC LIBRARY
   "E19", "U", 3,1,     // MISCELLANEOUS - COLLEGE
   "E20", "X", 3,1,     // COMBINATION OF 2 OR MORE OTHER EXEMPTION
   "E21", "P", 3,1,     // PUBLIC SCHOOL
   "E22", "R", 3,1,     // RELIGIOUS
   "E23", "X", 3,1,     // AIRCRAFT HISTORICAL EXEMPTIONS
   "E24", "X", 3,1,     // FOUR PERCENT VESSEL
   "E25", "V", 3,1,     // VETERANS ORG EXEMPTION
   "E26", "X", 3,1,     // QUALIFIED LESSOR
   "E27", "X", 3,1,     // LESSOR EXEMPTION
   "E28", "C", 3,1,     // CHURCH LESSORS EXEMPTION CLAIM
   "E29", "C", 3,1,     // CHURCH LESSORS PP
   "E31", "W", 3,1,     // WELFARE PP
   "E32", "R", 3,1,     // RELIGIOUS PP
   "E33", "C", 3,1,     // CHURCH PP
   "E34", "P", 3,1,     // PUBLIC SCHOOL PP
   "E40", "W", 3,1,     // W PARTIAL WEL-REL-LAND
   "E41", "W", 3,1,     // W PARTIAL WEL-REL-IMPRV
   "E42", "W", 3,1,     // W PARTIAL WELFARE-REL PP
   "E43", "W", 3,1,     // W PARTIAL WEL-SCHOOL LAND
   "E44", "W", 3,1,     // W PARTIAL WEL SCHOOL IMP
   "E45", "W", 3,1,     // W PARTIAL WEL SCHOOL PP
   "E46", "D", 3,1,     // D PARTIAL DISABLED VET (BASIC)
   "E47", "D", 3,1,     // D PARTIAL DISABLED VET (LOW INCOME)
   "E50", "C", 3,1,     // C PARTIAL CHURCH-LAND
   "E51", "C", 3,1,     // C PARTIAL CHURCH-IMPROVEMENT
   "E52", "C", 3,1,     // C PARTIAL CHURCH PP
   "E53", "C", 3,1,     // PARTIAL CHURCH LESSOR LAND
   "E54", "C", 3,1,     // PARTIAL CHURCH LESSOR IMPROVEMENTS
   "E60", "R", 3,1,     // R PARTIAL RELIGIOUS-LAND
   "E61", "R", 3,1,     // R PARTIAL RELIGIOUS-IMPR
   "E62", "R", 3,1,     // R PARTIAL RELIGIOUS PP
   "E63", "I", 3,1,     // W PARTIAL HOSPITAL - LAND
   "E64", "I", 3,1,     // W PARTIAL HOSPITAL - IMPRV
   "E65", "I", 3,1,     // W PARTIAL HOSPITAL -  PP
   "E66", "W", 3,1,     // W PARTIAL WEL CHARIT LAND
   "E67", "W", 3,1,     // W PARTIAL WEL CHARIT IMP
   "E68", "W", 3,1,     // W PARTIAL WEL CHARIT PP
   "E69", "E", 3,1,     // PARTIAL CEMETERY LAND
   "E70", "E", 3,1,     // PARTIAL CEMETERY IMPR
   "E71", "E", 3,1,     // PARTIAL CEMETERY PP
   "E72", "X", 3,1,     // DUMMY EXEMPTION FOR BOE REPORT < 40K
   "E73", "P", 3,1,     // PARTIAL PUBLIC SCHOOL LAND
   "E74", "P", 3,1,     // PARTIAL PUBLIC SCHOOL IMPRV
   "E75", "P", 3,1,     // PARTIAL PUBLIC SCHOOL PP
   "E76", "X", 3,1,     // PARTIAL QUALIFIED LESSOR LAND
   "E77", "X", 3,1,     // PARTIAL QUALIFIED LESSOR IMPS
   "E78", "X", 3,1,     // PARTIAL QUALIFIED LESSOR PP
   "E79", "X", 3,1,     // PARTIAL LESSOR EXEMPTION LAND
   "E80", "X", 3,1,     // PARTIAL LESSOR EXEMPTION IMPS
   "E81", "X", 3,1,     // PARTIAL LESSOR EXEMPTION PP
   "E82", "V", 3,1,     // PARTIAL VETERANS ORG LAND
   "E83", "V", 3,1,     // PARTIAL VETERANS ORG IMPS
   "E84", "V", 3,1,     // PARTIAL VETERANS ORG PP
   "E98", "X", 3,1,     // LOW VALUE EXEMPTION FOR SECURED
   "E99", "X", 3,1,     // LOW VALUE ORDINANCE EXEMPTION
   "","",0,0
};

#endif // !defined(AFX_MERGESJX_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_)
