#ifndef  _MERGETRI_H_
#define  _MERGETRI_H_

// TRI_ROLL.CSV 01/14/2021
#define  TRI_ROLL_ASMT            0
#define  TRI_ROLL_FEEPARCEL       1
#define  TRI_ROLL_TRA             2
#define  TRI_ROLL_USECODE         3
#define  TRI_ROLL_ACRES           4
#define  TRI_ROLL_OWNER           5
#define  TRI_ROLL_CAREOF          6
#define  TRI_ROLL_DBA             7
#define  TRI_ROLL_M_ADDR          8
#define  TRI_ROLL_M_CITY          9
#define  TRI_ROLL_M_ST            10
#define  TRI_ROLL_M_ZIP           11
#define  TRI_ROLL_LAND            12
#define  TRI_ROLL_HOMESITE        13
#define  TRI_ROLL_IMPR            14
#define  TRI_ROLL_GROWING         15
#define  TRI_ROLL_FIXTRS          16
#define  TRI_ROLL_FIXTR_RP        17
#define  TRI_ROLL_PPBUSINESS      18
#define  TRI_ROLL_PPMH            19
#define  TRI_ROLL_ADDRESS1        20
#define  TRI_ROLL_ADDRESS2        21
#define  TRI_ROLL_ADDRESS3        22
#define  TRI_ROLL_ADDRESS4        23

// TRI_EXE.CSV
#define  TRI_EXE_STATUS           0
#define  TRI_EXE_ASMT             1
#define  TRI_EXE_CODE             2
#define  TRI_EXE_HOEXE            3

// TRI_SITUS.CSV
#define  TRI_SITUS_ASMT           0
#define  TRI_SITUS_STRNAME        1
#define  TRI_SITUS_STRNUM         2
#define  TRI_SITUS_STRTYPE        3
#define  TRI_SITUS_STRDIR         4
#define  TRI_SITUS_COMMUNITY      5
#define  TRI_SITUS_ZIP            6
#define  TRI_SITUS_SEQ            7

// TRI_SALES
#define  TRI_SALES_ASMT           0
#define  TRI_SALES_DOCNUM         1
#define  TRI_SALES_DOCDATE        2
#define  TRI_SALES_SELLER         3
#define  TRI_SALES_BUYER          4
#define  TRI_SALES_PRICE          5
#define  TRI_SALES_TAXAMT         6
#define  TRI_SALES_ADJREASON      7

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 TRI_Exemption[] = 
{
   "E01", "H", 3,1,     // HOMEOWNERS EXEMPTION
   "E30", "V", 3,1,     // REGULAR VETERAN
   "E32", "D", 3,1,     // DISABLED VETERAN - FULL
   "E33", "D", 3,1,     // DISABLED VETERAN - PARTIAL
   "E35", "D", 3,1,     // DISABLED VETERAN - LOW INCOME - FULL
   "E36", "D", 3,1,     // DISABLED VETERAN - LOW INCOME - PARTIAL
   "E37", "X", 3,1,     // ?
   "E40", "W", 3,1,     // WELFARE - FULL
   "E41", "W", 3,1,     // WELFARE - PARTIAL LAND
   "E42", "W", 3,1,     // WELFARE - PARTIAL IMPROVEMENT
   "E50", "C", 3,1,     // CHURCH - FULL
   "E51", "C", 3,1,     // CHURCH - PARTIAL LAND
   "E52", "C", 3,1,     // CHURCH - PARTIAL IMPROVEMENT
   "E60", "R", 3,1,     // RELIGIOUS - FULL
   "E61", "R", 3,1,     // RELIGIOUS - PARTIAL LAND
   "E62", "R", 3,1,     // RELIGIOUS - PARTIAL IMPROVEMENT
   "E70", "E", 3,1,     // CEMETERY - FULL
   "E71", "E", 3,1,     // CEMETERY - PARTIAL
   "E74", "M", 3,1,     // MUSEUM - FULL
   "E75", "M", 3,1,     // MUSEUM - PARTIAL
   "E78", "S", 3,1,     // CA ACADEMY OF SCIENCE
   "E81", "X", 3,1,     // BANK/FINANCIAL INST
   "E82", "P", 3,1,     // PUBLIC SCHOOLS
   "E85", "X", 3,1,     // VOLUNTEER FIRE DEPT
   "E87", "X", 3,1,     // WATER CO
   "E90", "X", 3,1,     // HISTORICAL AIRCRAFT - FULL
   "E91", "X", 3,1,     // HISTORICAL AIRCRAFT - PARTIAL
   "E92", "X", 3,1,     // EXCLUSION - ASSESSED IN ANOTHER COUNTY
   "E97", "X", 3,1,     // LOW VALUE 5000 OR LESS
   "E98", "X", 3,1,     // OTHER
   "E99", "X", 3,1,     // LOW VALUE 2000 OR LESS
   "P19", "X", 3,1,     // Prop 19
   "","",0,0
};

#endif
