#ifndef _MERGENAP_H_
#define _MERGENAP_H_

#define NAP_CHAR_FEEPARCEL                0
#define NAP_CHAR_POOLSPA                  1
#define NAP_CHAR_CATTYPE                  2
#define NAP_CHAR_QUALITYCLASS             3
#define NAP_CHAR_YRBLT                    4
#define NAP_CHAR_BUILDINGSIZE             5
#define NAP_CHAR_ATTACHGARAGESF           6
#define NAP_CHAR_DETACHGARAGESF           7
#define NAP_CHAR_CARPORTSF                8
#define NAP_CHAR_HEATING                  9
#define NAP_CHAR_COOLINGCENTRALAC         10
#define NAP_CHAR_COOLINGEVAPORATIVE       11
#define NAP_CHAR_COOLINGROOMWALL          12
#define NAP_CHAR_COOLINGWINDOW            13
#define NAP_CHAR_STORIESCNT               14
#define NAP_CHAR_UNITSCNT                 15
#define NAP_CHAR_TOTALROOMS               16
#define NAP_CHAR_EFFYR                    17
#define NAP_CHAR_PATIOSF                  18
#define NAP_CHAR_BEDROOMS                 19
#define NAP_CHAR_BATHROOMS                20
#define NAP_CHAR_HALFBATHS                21
#define NAP_CHAR_FIREPLACE                22
#define NAP_CHAR_ASMT                     23
#define NAP_CHAR_BLDGSEQNUM               24
#define NAP_CHAR_HASWELL                  25
#define NAP_CHAR_LOTSQFT                  26

static XLAT_CODE  asPool[] =
{  // 12/28/2015
   // Value, lookup code, value length
   "0",  "N", 1,               // NONE
   "1",  "P", 1,               // Not heated
   "2",  "H", 1,               // Heated
   "3",  "C", 1,               // With Spa
   "4",  "P", 1,               // OTHER
   "99", "C", 2,               // POOL/SPA
   "",   "",  0
};

static XLAT_CODE  asCooling[] =
{
   // Value, lookup code, value length
   "1", "C", 1,               // Central
   "C", "C", 1,               // Central
   "O", "O", 1,               // Office
   "Y", "Y", 1,               // Building
   "",   "",  0
};

static XLAT_CODE  asHeating[] =
{  // 12/17/2015
   // Value, lookup code, value length
   "C", "Z", 1,               // Central
   "O", "X", 1,               // Other
   "1", "Z", 1,               // Central*
   "0", "L", 1,               // None
   "Y", "Y", 1,               // Yes - Heated
   "2", "D", 1,               // Wall
   "N", "L", 1,               // No
   "9", "X", 1,               // Other
   "6", "E", 1,               // Hydronic
   "4", "C", 1,               // Floor
   "3", "D", 1,               // Double Wall
   "W", "D", 1,               // Wall
   "7", "F", 1,               // Baseboard
   "R", "I", 1,               // Radiant
   "5", "S", 1,               // Wood/pellet stove
   "CF","Z", 2,               // Central Forced
   "V", "X", 1,               // Other
   "B", "F", 1,               // Baseboard
   "WL","D", 2,               // Wall
   "8", "J", 1,               // Space
   "H", "X", 1,               // 
   "A", "X", 1,               // 
   "X", "X", 1,               // 
   "F", "X", 1,               // 
   "S", "X", 1,               // 
   "",   "",  0
};

static XLAT_CODE  asFirePlace[] =
{  // 12/28/2015
   // Value, lookup code, value length
   "0", "N", 1,                // None
   "1", "L", 1,                // Masonry
   "2", "Z", 1,                // Zero Clearance
   "3", "W", 1,                // Wood Stove
   "4", "S", 1,                // Pellet Stove
   "5", "O", 1,                // Other
   "99","O", 1,                // Other
   "",   "", 0
};

IDX_TBL5 NAP_DocCode[] =
{  // DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN
   // * denotes active codes found in sale file
   "01", "1  ", 'N', 2, 3,          // *SALE (DTT)
   "02", "13 ", 'Y', 2, 3,          // *TRANSFER (NO DTT)
   "03", "74 ", 'Y', 2, 3,          // *NON-REAPPRAISABLE EVENT
   "04", "49 ", 'Y', 2, 3,          // *MOBILEHOME LIEN UPDATE
   "05", "74 ", 'Y', 2, 3,          // *NEW MOBILE HOME INSTALLATION (SOLD)
   //"06", "74 ", 'Y', 2, 3,          // POSSESSORY INT NEW/RENEWAL
   //"07", "74 ", 'Y', 2, 3,          // POSSESSORY INT LIEN UPDATE
   //"08", "74 ", 'Y', 2, 3,          // CLERICAL FUNCTIONS
   "09", "19 ", 'Y', 2, 3,          // *SPLIT/COMBO
   "10", "13 ", 'N', 2, 3,          // *PARTIAL INTEREST SALE (DTT)
   "11", "13 ", 'Y', 2, 3,          // *PARTIAL INT TRANSFER (NO DTT)
   "12", "74 ", 'Y', 2, 3,          // *GOVERNMENT ENTITY/NO SUPL
   //"13", "74 ", 'Y', 2, 3,          // NEW SINGLE FAMILY DWELLING
   //"14", "74 ", 'Y', 2, 3,          // NEW GARAGE/CARPORT
   //"15", "74 ", 'Y', 2, 3,          // NEW SWIMMING POOL/SPA
   //"16", "74 ", 'Y', 2, 3,          // DWELLING ADD'N/CONVERSION
   //"17", "74 ", 'Y', 2, 3,          // GAR ADD'N/CONV/PATIO/DECK
   //"18", "74 ", 'Y', 2, 3,          // MISC ADD'N
   //"19", "74 ", 'Y', 2, 3,          // MISC RES
   "20", "19 ", 'Y', 2, 3,          // * DEED/QUITCLAIM/AFFIDAVIT
   //"23", "74 ", 'Y', 2, 3,          // NEW MULTIPLE BLDG
   //"24", "74 ", 'Y', 2, 3,          // ADD'N/CONV TO MULTIPLES
   //"25", "74 ", 'Y', 2, 3,          // MISC MULTIPLES
   "3 ", "74 ", 'Y', 2, 3,          // *NON-REAPPRAISABLE EVENT
   //"33", "74 ", 'Y', 2, 3,          // NEW RURAL BLDG
   //"34", "74 ", 'Y', 2, 3,          // RURAL BLDG ADD'N
   //"35", "74 ", 'Y', 2, 3,          // MISC RURAL ADD'N, N.L.I.
   //"36", "74 ", 'Y', 2, 3,          // VINEYARD ADD'N/DELETION
   //"37", "74 ", 'Y', 2, 3,          // LAND IMPROVEMENTS
   //"38", "74 ", 'Y', 2, 3,          // TREES ADD'S/DELETIONS
   //"44", "74 ", 'Y', 2, 3,          // NEW INDUSTRIAL BLDG
   //"45", "74 ", 'Y', 2, 3,          // ADD'N/CONV INDUSTRIAL
   //"46", "74 ", 'Y', 2, 3,          // MISC INDUSTRIAL
   //"53", "74 ", 'Y', 2, 3,          // NEW COMM'L BLDG
   //"54", "74 ", 'Y', 2, 3,          // ADD'N/CONV COMM'L
   //"55", "74 ", 'Y', 2, 3,          // MISC COMM'L
   "58", "75 ", 'Y', 2, 3,          // *PROP 58-NON REAPPRAISAL
   //"63", "74 ", 'Y', 2, 3,          // AG CONTRACT-NEW BLDG
   //"64", "74 ", 'Y', 2, 3,          // AG CONTRACT-ADD'N/CONV
   //"65", "74 ", 'Y', 2, 3,          // AG CONTRACT-MISC
   //"66", "74 ", 'Y', 2, 3,          // AG CONTRACT- VINES INC/DEC
   //"67", "74 ", 'Y', 2, 3,          // AG CONTRACT LAND IMPS
   //"68", "74 ", 'Y', 2, 3,          // AG CONTRACT- TREES INC/DEC
   //"70", "74 ", 'Y', 2, 3,          // LIEN DATE UPDATE
   //"71", "74 ", 'Y', 2, 3,          // PROP 8
   //"72", "74 ", 'Y', 2, 3,          // CALAMITY-APPLICATION
   //"73", "74 ", 'Y', 2, 3,          // CALAMITY-RESTORATION
   //"74", "74 ", 'Y', 2, 3,          // ASSESSMENT APPEALS
   //"75", "74 ", 'Y', 2, 3,          // SOLAR IMPROVEMENT
   //"80", "74 ", 'Y', 2, 3,          // NON-TAXABLE GOVERNMENT PROP
   "81", "74 ", 'Y', 2, 3,          // *TAXABLE GOVERNMENT LAND
   //"82", "74 ", 'Y', 2, 3,          // SAND/GRAVEL NEW
   //"83", "74 ", 'Y', 2, 3,          // SAND/GRAVEL ABANDONDED
   //"84", "74 ", 'Y', 2, 3,          // GAS WELLS NEW
   //"85", "74 ", 'Y', 2, 3,          // GAS WELLS ABANDONDED
   //"96", "74 ", 'Y', 2, 3,          // DEMO/REMOVAL
   //"98", "74 ", 'Y', 2, 3,          // HOX
   "", "", '\0', 0, 0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 NAP_Exemption[] = 
{
   "E01", "H", 3,1,     // HOMEOWNERS EXEMPTION
   "E11", "V", 3,1,     // VETERANS EXEMPTION
   "E12", "D", 3,1,     // DISABLED VETERANS
   "E14", "D", 3,1,     // DISABLED VETERANS
   "E15", "D", 3,1,     // DISABLED VETERANS MULTIPLE VETERAN
   "E21", "W", 3,1,     // WELFARE EXEMPTION
   "E22", "I", 3,1,     // WELFARE HOSPITAL
   "E23", "S", 3,1,     // WELFARE SCHOOL
   "E31", "C", 3,1,     // CHURCH EXEMPTION
   "E32", "R", 3,1,     // RELIGIOUS EXEMPTION
   "E40", "U", 3,1,     // PARTIAL COLLEGE EXEMPTION
   "E41", "U", 3,1,     // COLLEGE EXEMPTION
   "E42", "U", 3,1,     // PARTIAL COLLEGE EXEMPTION
   "E43", "U", 3,1,     // PARTIAL COLLEGE EXEMPTION
   "E51", "P", 3,1,     // PARTIAL PUBLIC SCHOOL
   "E52", "P", 3,1,     // PUBLIC SCHOOL
   "E53", "M", 3,1,     // MUSEUM EXEMPTION
   "E54", "P", 3,1,     // PARTIAL PUBLIC SCHOOLS
   "E55", "P", 3,1,     // PARTIAL PUBLIC SCHOOL
   "E56", "P", 3,1,     // PARTIAL PUBLIC SCHOOL
   "E60", "X", 3,1,     // LOW VALUE EXEMPTION
   "E61", "E", 3,1,     // CEMETERY EXEMPTION
   "E62", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E63", "E", 3,1,     // PARTIAL CEMETERY EXEMPTION
   "E64", "E", 3,1,     // PARTIAL CEMETERY EXEMPTION
   "E65", "E", 3,1,     // PARTIAL CEMETERY EXEMPTION
   "E70", "W", 3,1,     // PARTIAL WELFARE EXEMPTION
   "E71", "W", 3,1,     // PARTIAL WELFARE EXEMPTION
   "E72", "W", 3,1,     // PARTIAL WELFARE EXEMPTION
   "E73", "I", 3,1,     // PARTIAL HOSPITAL WELFARE EXEMPTION
   "E74", "I", 3,1,     // PARTIAL HOSPITAL WELFARE EXEMPTION
   "E75", "I", 3,1,     // PARTIAL HOSPITAL WELFARE EXEMPTION
   "E76", "S", 3,1,     // PARTIAL SCHOOL WELFARE EXEMPTION
   "E77", "S", 3,1,     // PARTIAL SCHOOL WELFARE
   "E78", "S", 3,1,     // PARTIAL SCHOOL WELFARE EXEMPTION
   "E80", "C", 3,1,     // PARTIAL CHURCH EXEMPTION
   "E81", "C", 3,1,     // PARTIAL CHURCH EXEMPTION
   "E82", "C", 3,1,     // PARTIAL CHURCH EXEMPTION
   "E90", "R", 3,1,     // PARTIAL RELIGIOUS EXEMPTION
   "E91", "R", 3,1,     // PARTIAL RELIGIOUS EXEMPTION
   "E92", "R", 3,1,     // PARTIAL RELIGIOUS EXEMPTION
   "E93", "X", 3,1,     // 4% COMMCL VESSELS
   "E98", "X", 3,1,     // OTHER EXEMPTION
   "P19", "X", 3,1,     // PROP 19 - FOR BYVT 19 REPORTING ONLY
   "","",0,0
};
   
#endif