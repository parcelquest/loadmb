#if !defined(AFX_TAXMSTR_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_)
#define AFX_TAXMSTR_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_

#define  TAX_NAME          64
#define  TAX_M_ADDR        64
#define  TAX_AMT           14
#define  TAX_DATE          10
#define  TAX_RATE          20

#define  BILLTYPE_SECURED              '1'
#define  BILLTYPE_SECURED_SUPPL        '2'
#define  BILLTYPE_SECURED_ESCAPE       '3'
#define  BILLTYPE_UNSECURED            '4'
#define  BILLTYPE_UNSECURED_SUPPL      '5'
#define  BILLTYPE_UNSECURED_ESCAPE     '6'
#define  BILLTYPE_UNSECURED_INT_OWNER  '7'
#define  BILLTYPE_ROLL_CORRECTION      '8'
#define  BILLTYPE_UTILITY              '9'

#define  TAX_STAT_REDEEMED             '1'
#define  TAX_STAT_SOLD2AUCTION         '2'
#define  TAX_STAT_POWER2SELL           '3'
#define  TAX_STAT_ONPAYMENT            '4'
#define  TAX_STAT_CANCEL               '5'
#define  TAX_STAT_DFLTINST             '6'
#define  TAX_STAT_UNPAID               '7'
#define  TAX_STAT_BKRPCY               '8'
#define  TAX_STAT_BYVALUE              '9'
#define  TAX_STAT_CORRECTION           'A'

#define  TAX_BSTAT_NOTAX               'N'
#define  TAX_BSTAT_CANCEL              'C'
#define  TAX_BSTAT_PASTDUE             'D'
#define  TAX_BSTAT_PAID                'P'
#define  TAX_BSTAT_UNPAID              'U'
#define  TAX_BSTAT_INSTPMNT            'I'
#define  TAX_BSTAT_DFLTINST            'F'         // Defaulted installment acct
#define  TAX_BSTAT_REFUND              'R'         // Tax Refund
#define  TAX_BSTAT_PENDING             'E'         // Pending

#define  TAX_LOADING                   1
#define  TAX_UPDATING                  2
#define  TAX_EXTRACT                   4
#define  TAX_SUPP                      8

typedef struct _TaxOwner
{
   char Apn[25];
   char BillNum[25];
   char TaxYear[6];
   char Name1[TAX_NAME];
   char Name2[TAX_NAME];
   char CareOf[TAX_NAME];
   char Dba[TAX_NAME];
   char MailAdr[4][TAX_M_ADDR];
} TAXOWNER;

typedef struct _TaxBase
{  // 869 bytes
   char Apn[25];
   char isSecd[2];            // 1 = secured
   char isSupp[2];            // 1 = supplemental
   char isDelq[2];            // 1=is or has been delinquent
   char Assmnt_No[25];
   char BillNum[25];
   char TRA[8];
   char TaxYear[6];
   char DelqYear[6];          // For easy update to SQL
   char TaxAmt1[TAX_AMT];
   char TaxAmt2[TAX_AMT];
   char PenAmt1[TAX_AMT];
   char PenAmt2[TAX_AMT];
   char PaidAmt1[TAX_AMT];
   char PaidAmt2[TAX_AMT];
   char DueDate1[TAX_DATE];
   char DueDate2[TAX_DATE];
   char PaidDate1[TAX_DATE];
   char PaidDate2[TAX_DATE];
   char TotalTaxAmt[TAX_AMT];
   char TotalDue[TAX_AMT];
   char TotalFees[TAX_AMT];
   char TotalRate[TAX_RATE];
   char Upd_Date[TAX_DATE];
   char Def_Date[TAX_DATE];   // Default date or delinquent year
   char Def_Num[25];          // Only populate when Def_Date is valid
   //char Def_Amt[TAX_AMT];     // Redemption amount due
   char DelqStatus[4];        // For INY & TRI
   char PaidStatus[2];        // Bill status.  
                              // B = both paid
                              // U = both unpaid 
                              // 1 = Inst1 paid
                              // N = No tax for this parcel
                              // C = Canceled
                              // D = Past due
   char Inst1Status[2];       // P = Paid
                              // C = Cancel
                              // N = No tax 
                              // D = Past due
   char Inst2Status[2];
   char BillType[2];          // 1 = SECURED
                              // 2 = SECURED SUPPLEMENTAL
                              // 3 = SECURED ESCAPE
                              // 4 = UNSECURED
 				                  // 5 = UNSECURED SUPPLEMENTAL
 				                  // 6 = UNSECURED ESCAPE
                              // 7 = UNSECURED INTERRIM OWNER
                              // 8 = ROLL CORRECTION
   TAXOWNER OwnerInfo;
   double   dTotalTax;
   char CRLF[2];
} TAXBASE;

typedef struct _TaxDetail
{  
   char Apn[25];
   char Assmnt_No[25];
   char BillNum[25];
   char TaxYear[6];
   char TaxAmt[TAX_AMT];
   char TaxRate[TAX_RATE];
   char TaxCode[TAX_RATE];
   char TaxDesc[100];
   char Phone[25];
   char TC_Flag[2];           // 1=PQ generated tax code
                              // 0=County tax code
   double dTaxAmt;
} TAXDETAIL;

typedef struct _TaxSupplement
{  // Based on MEN
   char Apn[25];
   char Assmnt_No[25];
   char ApnSeq[4];
   char CurPri[2];            // Current/Prior
   char Prorate[2];           // Prorate
   char BillNum[25];
   char RollYear[6];          // 0910 = 2009-2010
   char RollFctr[4];
   char NetAmt[TAX_AMT];
   char PenAmt[TAX_AMT];
   char Status1[2];           // P= paid, N=not billed, R=Refund, " "=Unpaid, C=Canceled
   char TaxAmt1[TAX_AMT];
   char PenAmt1[TAX_AMT];
   char DelqDate1[TAX_DATE];
   char PaidDate1[TAX_DATE];
   char Status2[2];
   char TaxAmt2[TAX_AMT];
   char PenAmt2[TAX_AMT];
   char DelqDate2[TAX_DATE];
   char PaidDate2[TAX_DATE];
   char NoteDate[TAX_DATE];
   char EventDate[TAX_DATE];  // When the transfer or new construction occurred, or effective date
   char RollSupp[2];          // 'C'urrent, 'P'rior, or 'N'ext roll
   char Type[2];              // 'S'ecured or 'U'nsecured
   char Upd_Date[TAX_DATE];
   char CRLF[2];
} TAXSUPP;

typedef struct _TaxDelinquent
{
   char Apn[25];
   char isSecd[2];            // 1 = secured
   char isSupp[2];            // 1 = supplemental
   char isDelq[2];            // 1 = currently delinquent
   char Assmnt_No[25];
   char BillNum[25];
   char Default_No[25];
   char TaxYear[6];
   char Def_Date[TAX_DATE];
   char Def_Amt[TAX_AMT];
   char Red_Date[TAX_DATE];   // Redemption date when paid in full
   char Red_Amt[TAX_AMT];     // Redemption amount paid if Red_Date present, otherwise amt due.
   char Tax_Amt[TAX_AMT];
   char Pen_Amt[TAX_AMT];
   char Fee_Amt[TAX_AMT];     // Other cost
   char Upd_Date[TAX_DATE];
   char Pts_Date[TAX_DATE];   // Power to sale date/due date on multi-plan
   char PrclType[4];          // 10 = plain parcel with no installment plan
			                     // 60 = bankruptcy filed
			                     // 70 = Tax Collectors Power to Sell recorded eligible for sale
			                     // 80 = Installment plan in good standing
			                     // 81 = defaulted installment plan or partial payments applied
			                     // 89 = Paid in full with multiple payments or by installment plan
			                     // 99 = Paid in full with one payment
   char InstDel[4];           // 2 = Second installment was delinquent, 3 = both installments were delinquent
   char DelqStatus[4];        // J, S, Y, E? ... For INY & TRI
                              // 1=Redeemed
                              // 2=Sold to auction
                              // 3=Power to sell
                              // 4=On payment plan to redeem
                              // 5=Cancelled
                              // 6=Defaulted Installment Plan
                              // 7=Unpaid
                              // 8=Bankruptcy filed

   char CRLF[2];
} TAXDELQ;

typedef struct _TaxAgency
{  // 280 bytes
   char Agency[160];
   char Code[20];
   char CodeXL[20];
   char Phone[20];
   char TaxRate[20];
   char TaxAmt[20];      
   char TC_Flag[10];          // 1=PQ generated tax code
                              // 0=County tax code
   int  iNameLen;
   int  iCodeLen;
} TAXAGENCY;

typedef struct _TaxPaid
{
   char Apn[25];           // RIV: Assmnt_No
   char BillNum[25];
   char Default_No[25];
   char Dist_No[20];
   char TRA[20];
   char PaidDate[20];
   char PaidAmt[20]; 
   char TaxAmt[20]; 
   char PenAmt[20]; 
   char FeeAmt[20];  
   char PaidFlg[2];           // P=both paid, B=both unpaid, 1=1st paid, 2=2nd unpaid D=Delq paid
   char CRLF[2];
} TAXPAID;

#define MAX_TRA_DIST       80
typedef struct _TraDist
{  // 260 bytes
   char TRA[8];
   char TaxRate[8];
   char Code[MAX_TRA_DIST][8];   // Store up to 60 district code per TRA, normally less than 45 in use
   int  iCount;
} TRADIST;

#define  TAX_BASE          0x0001
#define  TAX_DETAIL        0x0002
#define  TAX_DELINQUENT    0x0004
#define  TAX_SUPPLEMENT    0x0008
#define  TAX_AGENCY        0x0010
#define  TAX_OWNER         0x0020
#define  TAX_PAID          0x0040

#define  TAX_UBASE         0x1000
#define  TAX_UDETAIL       0x2000
#define  TAX_UDELINQUENT   0x4000
#define  TAX_USUPPLEMENT   0x8000

#define  TC_APN_S          0
#define  TC_APN_D          1
#define  TC_CO_NUM         2
#define  TC_CO_ID          3
#define  TC_TAX_YR         4
#define  TC_BILL_NUM       5
#define  TC_S_ADDR         6
#define  TC_M_ADDR         7
#define  TC_ORIG_PCL       8
#define  TC_FEE_PCL        9
#define  TC_LOT_ACRES      10
#define  TC_TRA            11
#define  TC_ROLL_CAT       12
#define  TC_LEGAL          13
#define  TC_PAID_STAT1     14
#define  TC_PAID_STAT2     15
#define  TC_DUE_PAID_DT1   16
#define  TC_DUE_PAID_DT2   17
#define  TC_TAX_DUE1       18
#define  TC_TAX_DUE2       19
#define  TC_TOTAL_TAX      20
#define  TC_PEN_DUE1       21
#define  TC_PEN_DUE2       22
#define  TC_TOTAL_PEN      23
#define  TC_OTHER_FEES     24
#define  TC_TOTAL_DUE      25
#define  TC_AMT_PAID1      26
#define  TC_AMT_PAID2      27
#define  TC_TOTAL_PAID     28
#define  TC_BALANCE1       29
#define  TC_BALANCE2       30
#define  TC_TOTAL_BALANCE  31
#define  TC_LAND           32
#define  TC_IMPR           33
#define  TC_GROW_IMPR      34
#define  TC_FIX_IMPR       35
#define  TC_PERS_PROP      36
#define  TC_PP_MH          37
#define  TC_EXE_CD1        38
#define  TC_EXE_AMT1       39
#define  TC_EXE_CD2        40
#define  TC_EXE_AMT2       41
#define  TC_EXE_CD3        42
#define  TC_EXE_AMT3       43
#define  TC_NET_VAL        44
#define  TC_DEF_DATE       45
#define  TC_RED_DATE       46
#define  TC_PMT_PLAN       47
#define  TC_PMT_PLAN_AMT   48
#define  TC_UPD_DATE       49

#define  TC_TAX_DIST       1
#define  TC_TAX_PHONE      2
#define  TC_TAX_CODE       3
#define  TC_TAX_RATE       4
#define  TC_TAX_TOTAL      5
#define  TC_TAX_BILLNUM    6
#define  TC_TAX_ROLLCAT    7

char  *Tax_CreateTaxBaseCsv(char *pOutbuf, TAXBASE *pInrec);
char  *Tax_CreateDelqCsv(char *pOutbuf, TAXDELQ *pInrec);
char  *Tax_CreateDetailCsv(char *pOutbuf, TAXDETAIL *pInrec);
char  *Tax_CreateAgencyCsv(char *pOutbuf, TAXAGENCY *pInrec);
char  *Tax_CreateSuppCsv(char *pOutbuf, TAXSUPP *pInrec);
char  *Tax_CreateTaxOwnerCsv(char *pOutbuf, TAXOWNER *pInrec);
char  *Tax_CreatePaidCsv(char *pOutbuf, TAXPAID *pInrec);
int   doTaxImport(char *pCnty, int iType);
int   doTaxImpExt(char *pCnty, int iType, char *pServerName=NULL);
int   doTaxPrep(char *pCnty, int iType);
int   doTaxPrepExt(char *pCnty, int iType, char *pServerName=NULL);
int   doTaxMerge(char *pCnty, bool bMergeItems=true, bool bMergeDelq=true, bool bMergeSupp=false);
int   doTaxUpdate(char *pCnty, int iType);
int   doUpdateTotalRate(char *pCnty, char *pVersion=NULL);

int   TC_LoadTaxBase(char *pCnty, char *pTaxFile, bool bImport=true);
int   TC_UpdateTaxBase(char *pCnty, char *pTaxFile, bool bImport=true);
int   TC_LoadTax(char *pCnty, bool bImport=true, bool bLoadDetail=true);

int   TC_LoadTaxAgencyAndDetail(char *pCnty, char *pDistFile, char *pAgencyFile, char *pDetailFile, bool bImport, bool bIgnoreNoValue=false);
int   Load_TC(char *pCnty, bool bImport=true);
int   Load_TCC(char *pCnty, bool bImport=true);

void  TC_SetDateFmt(int iFmt=MM_DD_YYYY_1, bool bUseExtendedDueDate=false);
int   TC_ParseTaxBase(char *pOutbuf, char *pInbuf, char cDelim='|');
int   TC_CreateTaxRecord(char *pOutbuf);
int   Update_TC(char *pCnty, bool bImport, bool bLoadDetail=true);
int   Update_TCC(char *pCnty, bool bImport, bool bLoadDetail=true);

int   updateTaxAgency(char *pCnty);
int   update_TI_BillNum(char *pCnty, char *pVersion=NULL);
int   updateDelqFlag(char *pCnty, bool bChkRedeem=true, bool bChkDelq=true, int iSpecialCase=0);
int   updTaxDB(char *strCmd, char*pServerName);
void  addTaxAgency(char *pTaxCode, char *pTaxAgency, char *pTaxRate, char *pPhone);
int   updateTaxBase(char *pCnty, char *pBaseRec, char *pOutbuf=NULL);
void  SetTaxRateIncl(char *pCnty);
int   updateTotalTaxRate(char *pCnty, char *pCmdType);

int   Tax_CreateAgencyTable(char *pCnty, char *pXrefFile, int iCodeXL=0);

// Create R01 records from TCF file
int   TC_CreateR01(char *pCnty, char *pBook);

TAXAGENCY *findExactTaxDist(LPCSTR pDist, int iStart=0);
TAXAGENCY *findTaxDist(LPCSTR pDist, int iStart=0);
TAXAGENCY *findTaxDist(LPCSTR pDist, LPCSTR pCode, int iStart=0);
TAXAGENCY *findTaxAgency(LPCSTR pCode, int iStart=0);
TAXAGENCY *BSrchTaxDist(LPCSTR pDist);       // Compare based on table name length
TAXAGENCY *BSrchTaxDistName(LPCSTR pDist);   // Compare exact name
TRADIST   *findTRADist(LPCSTR pCode, int iStart=0);

int       LoadTaxCodeTable(char *pXrefFile, int iCodeXL=0);
int       LoadTRADistTable(char *pXrefFile);
int       LoadTaxRateTable(char *pXrefFile);

int       InstDueDate(char *pResult, int iInstNum, int iYear);
int       ChkDueDate(int iInstNum, int iRollYear);
int       ChkDueDate(int iInstNum, char *pDueDate);
int       isNewTaxFile(char *pInfile, char *pCnty, int iChkDate=0);
int       UpdateTRA(char *pOutbuf, FILE *fd, bool bResetBuffer=false);
FILE      *OpenR01(char *pCnty);
void      SetCounty(char *pCnty);
void      NameTaxCsvFile(char *pOutFileName, char *pCnty, char *pType);
int       OpenTaxExpSql(char *pCnty, char *pType, char *pMode);
void      CloseTaxExpSql();
int       ConvertTaxSqlFile(char *pFile1, char *pFile2, char *pRoot1, char *pRoot2, char *pSvrType);

#ifdef _TAX_CPP
   int   iNumTaxDist, iNumTRADist, iTaxYear, iDelqUpdtDate, iApnMatch, iNewAgency,
         iDateFmt, iNewCodeIdx, iTaxDelq;
   bool  bInclTaxRate;     // Flag in INI file to include tax rate in Agency table or not
   bool  bIgnoreZero;      // Flag to ignore tax items with zero value

   extern   hlAdo       hl;
   extern   hlAdoRs     m_AdoRs;
   extern   bool        m_bConnected;
#else
   extern int iNumTaxDist, iNumTRADist, iTaxYear, iDelqUpdtDate, iApnMatch, iNewAgency;
   extern bool bIgnoreZero, m_bUseFeePrcl;
#endif

#endif