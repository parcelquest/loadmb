/**************************************************************************
 *
 * Notes:
 *    - This county data is coming from multiple files:
 *    - LDR
 *         + Copy AGENCYCDCURRSEC_TR601_Header.TAB+AGENCYCDCURRSEC_TR601.TAB TR601.TAB
 *         + Import TR601.TAB to SecuredRoll table TR601.  In Advance dialog, select
 *           data type as US-ASCII and {tab} delimeter.  Make all field as Text (not Memo).
 *
 *         + Rename last SSTA.R01 to SSTA.O01
 *         + Load lien: -L -CSTA -O -Xs|-Ms -Xa -Xl
 *
 *    - STA doesn't have tax file
 *    - Sale file contains only transactions of the last two years
 *    - Monthly update
 *         + Load update: -CSTA -U -Xa -Xs -O [-T]
 *
 * 04/14/2010 9.6.0    Copy from MergeMon.cpp and modify for STA.  They start sending
 *                     us in MB format comma delimited UNICODE files.
 * 04/23/2010 9.6.1    County sending files in new format pipe delimited ANSI.  Replace
 *                     ParseStringNQ with ParseStringIQ to handle this.
 * 06/23/2010 10.0.0   Modify Sta_MergeSitus() to convert addr to upper case.  Modify 
 *                     Sta_MergeLien() and Sta_ExtrChar() to correct LotSqft & LotAcres.
 * 08/21/2010 10.3.2   Sort EXE file on second field. DO not set HO_FLG=2 when not match.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 06/05/2011 10.9.0   Exclude HomeSite from OtherVal. Replace Sta_MergeExe() with MB_MergeExe(),
 *                     and Sta_ExtrSale() with MB_CreateSCSale(). Chamge sort cmd for EXE file sort.
 * 06/08/2011 10.9.1   Change sort command for sorting EXE file.
 * 06/13/2011 10.9.2   Due to input layout is different, MB_CreateSCSale() cannot be used.
 *                     A copy is modified for STA as Sta_CreateSCSale().
 * 06/22/2011 11.0.0   Reorder code in loadSta(). Check for unicode on roll file only.
 *                     If this file is unicode, convert all other files.  Update Transfer
 *                     when applying cum sale. Save original House Number in S_HSENO. 
 * 02/26/2012 11.11.1  Initialize sort command before sorting Situs file.
 * 05/21/2012 11.14.3  Fix cum sale (reset NumOfPrclXfer and set MultiSale_Flg='Y'), Check for 
 *                     bad DocTax (>20000).
 * 06/27/2012 12.0.1   Add Sta_CreateSCSale2() to extract sale from MDB file.
 * 10/22/2012 12.4.1   Modify Sta_CreateSCSale() to clean up DocNum.
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 06/26/2013 13.0.1   Modify Sta_ExtrChar() to use LandSqft if Acres is 0.
 * 07/01/2013 13.1.3.1 Fix Sta_MergeSitus() for LDR processing when StrNum is not present.
 * 09/30/2013 13.13.1  Modify Sta_ExtrChar() to support new STA_CHAR layout. County
 *                     has remove AC, Heating, and HasSeptic from CHAR file.
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 11/08/2013 13.18.1  Add asHeatingSrc & asCoolingSrc to use in place of Heating/Cooling.
 *.                    Rename Sta_ExtrChar() to Sta_ConvStdChar().
 * 11/25/2013 13.18.2  Modify Sta_ConvStdChar() to fix Sewer, update HeatingSrc & CoolingSrc tables.
 * 12/31/2013 13.19.0  Add DocCode[] table and modify Sta_CreateSCSale() to translate
 *                     DocCode to DocType using new table. Use FixDocType() to cleanup cumsale file.
 * 01/20/2014 13.20.1  Modify DocCode[] to make Trustee's Deed non-sale event.
 * 06/30/2015 15.0.2   Modify Sta_Load_LDR() to skip header record from LDR file.
 * 07/31/2015 15.2.0   Add DBA to Sta_MergeMAdr()
 * 11/03/2015 15.5.0   Update Heating, Cooling, and Sewer.
 * 01/13/2016 15.10.1  Remove old situs in Cal_MergeSitus() before update to avoid remnant from old addr.
 * 02/15/2016 15.12.2  Modify Sta_MergeMAdr() to fix overwrite bug. Format DocNum in 
 *                     Sta_CreateSCSale() as YYYYR1234567 instead of 6 digit.
 * 04/06/2016 15.14.3  Load TC data
 * 06/29/2016 16.0.1   Modify Sta_MergeLien() and Sta_Load_LDR() to support new L3_* record.
 * 07/07/2016 16.0.2   Modify Sta_MergeChar() to update only when new data is not blank since existing values are from LDR file
 * 07/17/2016 16.0.4   Add option to use TC601 to update value in Load_LDR()
 * 06/27/2017 17.0.2   Fix bug in Sta_ConvStdChar() by removing extra space in COOLING field.
 * 03/27/2018 17.9.0   Add -Ut option to update tax.
 * 07/09/2018 18.1.1   Call MB_ExtrTC601() with new option not to sort input file.
 * 03/16/2019 18.10.7  Modify Sta_MergeRoll() to filter out bad DocNum before populate TRANSFER fields.
 * 06/26/2019 19.0.1.1 Modify Sta_MergeLien() to add AgPreserve flag.
 * 06/28/2019 19.0.1.2 Modify Sta_MergeLien() to hardcode a DocNum error.
 * 07/07/2019 19.0.2   Add '-' to zip4 on M_CTY_ST_D in Sta_MergeMAdr().
 * 02/20/2020 19.6.7   Modify Sta_MergeSitus() to hardcode some suffixes and cleanup code.
 * 04/27/2020 19.9.0   Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 11/01/2020 20.4.2   Modify Sta_MergeRoll() to populate default PQZoning.
 * 05/05/2021 20.8.6   Modify Sta_ConvStdChar() to fix BldgClass.
 * 07/02/2021 20.0.4   Add QualityClass to R01 in Sta_MergeChar(). 
 * 10/11/2021 21.2.7   Modify Sta_MergeRoll() to reset LOT_ACRES & LOT_SQFT if data is not available.
 * 02/13/2024 23.6.2   Use RebuildCsv() to fix broken records in roll file.  Modify Sta_ConvStdChar() to
 *                     fix some quality class values.
 * 07/09/2024 24.0.1   Modify Sta_MergeLien() to add ExeType.  Reorg Sta_Load_LDR().
 *                     Rebuild roll file using RebuildCsv_IQ() to fix broken records before loading LDR.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "MergeSta.h"
#include "PQ.h"
#include "CharRec.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "Tax.h"

/******************************** Sta_MergeOwner *****************************
 *
 * Special cases:
 *     ROBIE W JOHN ET UX TRS & ROBIE W JOHN ET UX TRS KA
 *     GRAUPNER KARL E TRS & ZETTIA L & GRAUPNER & ZETTIA
 *     MORAN CLARE DUNN LIFE ESTATE
 *     GALLO ROBERT J LIFE EST
 *     BENNETT KAREL HEIRS OF ET AL
 *     GLOR DONALD D & VSEIA-GLOR VALORIE ANN
 *     AIKEN RAYMOND E ET UX TRS & AIKEN RAYMOND E ET UX
 *     VAN VLIET JOHN M & VAN VLIET MARY S
 *     O MEARA JACK B TRS & O MEARA MARIANNE H
 *     LA ROSA MICHAEL J & LA ROSA NANCY B
 *
 *****************************************************************************/

void Sta_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iRet;
   char  acTmp1[128], acTmp2[128], acTmp[128], *pTmp, *pTmp1;
   char  acName1[64], acName2[64];

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   // Copy data
   strcpy(acTmp1, pNames);

   // Replace comma with space
   if (pTmp = strrchr(acTmp1, ','))
      *pTmp = ' ';

   // Remove multiple spaces
   pTmp = acTmp1;
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '*' too
      if (*pTmp == '*' || *pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   else if (acTmp[iTmp-1] == '&')
      iTmp -= 2;

   acTmp[iTmp] = 0;
   acName2[0] = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // If number appears at the beginning of name, do not parse
   if (isdigit(acTmp[0]) || !strchr(acTmp, ' '))
   {
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp);
      return;
   }

   // check to see if same name is repeated.  If so, remove the later part
   if (pTmp = strchr(acTmp, '&'))
   {
      *pTmp = 0;
      strcpy(acTmp1, acTmp);
      strcpy(acTmp2, pTmp+2);
      if (pTmp1 = strstr(acTmp1, " TRS"))
         *pTmp1 = 0;
      if (pTmp1 = strstr(acTmp2, " TRS"))
         *pTmp1 = 0;
      if (strcmp(acTmp1, acTmp2))
         *pTmp = '&';               // Different names
   }

   // Filter out words
   if ((pTmp=strstr(acTmp, " ETAL")) ||
       (pTmp=strstr(acTmp, " ET AL")) ||
       (pTmp=strstr(acTmp, " & ET UX")) ||
       (pTmp=strstr(acTmp, " ET UX")) )
      *pTmp = 0;

   // If more than one "&", put second part into name2
   if (pTmp = strchr(acTmp, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1 = 0;
         strcpy(acName2, pTmp1+2);
      }
   }

   if ((pTmp=strstr(acTmp, " LIFE EST")) || (pTmp=strstr(acTmp, " HEIRS OF")) )
   {
      *pTmp = 0;
   }
   strcpy(acName1, acTmp);

   // Now parse owners
   iRet = 0;
   // Only parse if owner has more than one word
   if (strchr(acName1, ' '))
   {
      if (strchr(acName1, '-'))
         iRet = splitOwner(acName1, &myOwner, 4);  // Handle different last name
      else
         iRet = splitOwner(acName1, &myOwner, 3);  // Handle same last name

      iTmp = strlen(myOwner.acName1);
      if (iTmp > SIZ_NAME1)
         iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, iTmp);

      if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
      {
         iTmp = strlen(myOwner.acName2);
         if (iTmp > SIZ_NAME2)
            iTmp = SIZ_NAME2;
         memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, iTmp);
      }

      // If name is not swapable, use Name1 for it
      if (iRet == -1)
         strcpy(myOwner.acSwapName, myOwner.acName1);

      iTmp = strlen(myOwner.acSwapName);
      if (iTmp > SIZ_NAME_SWAP)
         iTmp = SIZ_NAME_SWAP;
      memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
   } else
   {
      // Couldn't split names
      iTmp = strlen(pNames);
      memcpy(pOutbuf+OFF_NAME1, pNames, iTmp);
      memcpy(pOutbuf+OFF_NAME_SWAP, pNames, iTmp);
   }
}

/******************************** Sta_MergeMAdr ******************************
 *
 * Merge Mail address from roll file
 *
 *****************************************************************************/

void Sta_MergeMAdr(LPSTR pOutbuf, LPCSTR pAdr1, LPCSTR pAdr2, LPCSTR pAdr3, LPCSTR pAdr4)
{
   char     acTmp[256], acAddr1[64], acAddr2[64], acAddr3[64], acAddr4[64];
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   if (pAdr1 && pAdr2)
   {
      if (*pAdr1 < '1')
         return;
      strcpy(acAddr1, pAdr1);
      strcpy(acAddr2, pAdr2);
      strcpy(acAddr3, pAdr3);
      strcpy(acAddr4, pAdr4);
      blankRem(acAddr1);
      blankRem(acAddr2);
      blankRem(acAddr3);
      blankRem(acAddr4);
   } else
   {
      return;
   }

   // Care of
   if (!memcmp(acAddr1, "C/O", 3) || !memcmp(acAddr1, "ATTN", 4) )
      updateCareOf(pOutbuf, acAddr1, strlen(acAddr1));
   else if (!memcmp(acAddr2, "C/O", 3) || !memcmp(acAddr2, "ATTN", 4) )
      updateCareOf(pOutbuf, acAddr2, strlen(acAddr1));

   if (acAddr4[0] > ' ')
   {
      strcpy(acAddr1, acAddr3);
      strcpy(acAddr2, acAddr4);
   } else if (acAddr3[0] > ' ')
   {
      strcpy(acAddr1, acAddr2);
      strcpy(acAddr2, acAddr3);
   }

   // Parsing mail address
   if (memcmp(acAddr1, "     ", 5))
   {
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
      parseMAdr1(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      // Remove Zip4
      char *pTmp;
      if (pTmp = strrchr(acAddr2, '-'))
         *pTmp = 0;
      parseMAdr2(&sMailAdr, acAddr2);

      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, strlen(sMailAdr.State));

      iTmp = atoi(sMailAdr.Zip);
      if (iTmp > 400)
      {
         sprintf(sMailAdr.Zip, "%0.5d", iTmp);
         memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      }

      sprintf(acTmp, "%s %s %s", sMailAdr.City, sMailAdr.State, sMailAdr.Zip);
      iTmp = blankRem(acTmp);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

void Sta_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;

   // Verify address
   if (*apTokens[MB_ROLL_M_ADDR] < '1')
   {
      Sta_MergeMAdr(pOutbuf, apTokens[MB_ROLL_M_ADDR1], apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3], apTokens[MB_ROLL_M_ADDR4]);
      return;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002003020000", 9))
   //   iTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   if (*apTokens[MB_ROLL_CAREOF] > '0')
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));
   else if (!memcmp(apTokens[MB_ROLL_M_ADDR1], "C/O", 3) || !memcmp(apTokens[MB_ROLL_M_ADDR1], "ATTN", 4) )
      updateCareOf(pOutbuf, apTokens[MB_ROLL_M_ADDR1], strlen(apTokens[MB_ROLL_M_ADDR1]));
   else if (!memcmp(apTokens[MB_ROLL_M_ADDR2], "C/O", 3) || !memcmp(apTokens[MB_ROLL_M_ADDR2], "ATTN", 4) )
      updateCareOf(pOutbuf, apTokens[MB_ROLL_M_ADDR2], strlen(apTokens[MB_ROLL_M_ADDR2]));

   // Update DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   } 

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   iTmp = blankRem(acAddr1, SIZ_M_ADDR_D);
   vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D, iTmp);

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
   vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
   vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);

   // City/St - Zip
   char  acZip[16];
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], SIZ_M_CITY);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      acZip[0] = 0;
      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         int   iZip4=0, iZip;

         // Take out '-' in zipcode if present
         strcpy(acTmp, apTokens[MB_ROLL_M_ZIP]);
         iTmp = strlen(acTmp);
         if (acTmp[5] == '-' && iTmp == 10)
         {
            iZip = atoin(acTmp, 5);
            iZip4 = atoin(&acTmp[6], 4);
         } else if (iTmp == 9)
         {
            iZip = atoin(acTmp, 5);
            iZip4 = atoin(&acTmp[5], 4);
         } else if (iTmp == 5)
            iZip = atoin(acTmp, 5);
         else
         {
            iZip = 0;
            vmemcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);   
            strcpy(acZip, acTmp);
         }

         if (iZip > 0)
         {
            if (iZip4 > 0)
            {
               iTmp = sprintf(acZip, "%.5d-%.4d", iZip, iZip4);
               iTmp = sprintf(acTmp, "%.5d%.4d", iZip, iZip4);
            } else
            {
               iTmp = sprintf(acTmp, "%.5d", iZip);
               iTmp = sprintf(acZip, "%.5d", iZip);
            }
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, iTmp);   
         }
      } else
         iTmp = 0;

      sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], acZip);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

/******************************** Sta_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Sta_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acExt[32], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSitus);

      // Get first rec
      pRec = fgets(acRec, 512, fdSitus);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      if (*pRec == '"')
         iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      else
         iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006080015000", 9))
   //   iRet = 0;
#endif

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   else
      iRet = ParseStringIQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);

   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Remove old situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      // Save original StrNum
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      sprintf(acTmp, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
      pTmp = strchr(acTmp, ' ');
      *(pTmp+1) = 0;
      strcpy(acAddr1, acTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));
   }

   if (*apTokens[MB_SITUS_STRDIR] > ' ')
   {
      strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
      strcat(acAddr1, " ");
      memcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
   } else
      memset(pOutbuf+OFF_S_DIR, ' ', SIZ_S_DIR);

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");

      acExt[0] = 0;
      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
      {
         sprintf(acCode, "%d ", iTmp);
         strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);
      } else if (!memcmp(apTokens[MB_SITUS_STRTYPE], "AVE", 3) || !memcmp(apTokens[MB_SITUS_STRTYPE], "RIVE", 4))
      {
         strcpy(acCode, "3 ");
         strcat(acAddr1, "AVE");
      } else if (!memcmp(apTokens[MB_SITUS_STRTYPE], "CTE", 3) || !memcmp(apTokens[MB_SITUS_STRTYPE], "CTY", 3))
      {
         strcpy(acCode, "14 ");
         strcat(acAddr1, "CT");
      } else
      {
         strcpy(acExt, apTokens[MB_SITUS_STRTYPE]);
         strcpy(acCode, " ");
         strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);
         LogMsg0("*** Invalid suffix: %s [%s]", apTokens[MB_SITUS_STRTYPE], apTokens[MB_SITUS_ASMT]);
         iBadSuffix++;
      }

      if (acExt[0] > ' ')
         sprintf(acTmp, "%s %s", apTokens[MB_SITUS_STRNAME], acExt);
      else
         strcpy(acTmp, apTokens[MB_SITUS_STRNAME]);
      vmemcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET);
      vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apTokens[MB_SITUS_STRNAME]);
      if (sAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      if (sAdr.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, SIZ_S_SUFF);

      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNO);
   }

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      pTmp = _strupr(apTokens[MB_SITUS_COMMUNITY]);
      Abbr2Code(pTmp, acTmp, acAddr1);
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      // Zipcode
      lTmp = atol(apTokens[MB_SITUS_ZIP]);
      if (lTmp >= 90000)
      {
         iTmp = sprintf(acTmp, "%s CA %s", myTrim(acAddr1), apTokens[MB_SITUS_ZIP]);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
         memcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], SIZ_S_ZIP);
      } else if (acAddr1[0] >= 'A')
      {
         iTmp = sprintf(acTmp, "%s CA", myTrim(acAddr1));
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      }
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int Sta_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128], acAddr2[128];
   ADR_REC  sSitusAdr;
   int      iStrNo, iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006080015000", 9))
   //   acTmp[0] = 0;
#endif
   //
   strcpy(acAddr1, _strupr(pLine1));
   blankRem(acAddr1);
   iStrNo = atoi(acAddr1);
   if (iStrNo > 0)
   {
      sprintf(acTmp, "%d      ", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
   }

   if (acAddr1[0] > ' ')
   {
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
      parseAdr1C(&sSitusAdr, acAddr1);
      memcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.HseNo, strlen(sSitusAdr.HseNo));

      if (sSitusAdr.strDir[0] > '0')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

      if (sSitusAdr.strName[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
         if (sSitusAdr.strSfx[0] > ' ')
            memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
      }

   } else
      memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));

   // Situs city
   strcpy(acAddr2, _strupr(pLine2));
   memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, strlen(acAddr2));
   if (!memcmp(acAddr2, "CA ", 3))
   {
      iTmp = atol((char *)&acAddr2[3]);
      if (iTmp >= 90000)
         memcpy(pOutbuf+OFF_S_ZIP, (char *)&acAddr2[3], SIZ_S_ZIP);
   } else
   {
      parseAdr2_1(&sSitusAdr, acAddr2);
      City2Code(sSitusAdr.City, acTmp);
      if (acTmp[0] > ' ')
         memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
      if (sSitusAdr.Zip[0] > '0')
         memcpy(pOutbuf+OFF_S_ZIP, sSitusAdr.Zip, strlen(sSitusAdr.Zip));
   }
   memcpy(pOutbuf+OFF_S_ST, "CA", SIZ_S_ST);

   return 0;
}

/******************************** Sta_MergeChar ******************************
 *
 * Note: need code table for Heating and Colling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - A parcel may have multiple entries in CHAR file.  If LandUseCat > 1,
 *     Add all of them together.  Otherwise, use the first one.
 *     For now, just use the first one found.
 *
 *****************************************************************************/

int Sta_MergeChar(char *pOutbuf)
{
   static   char acRec[640], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft, lTmp;
   int      iLoop, iBeds, iFBath, iHBath, iFp;
   STDCHAR  *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 640, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   // Pools
   if (pChar->Pool[0] > ' ')
      *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Yrblt
   memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "026021011", 9))
   //   lTmp = 0;
#endif

   // LotSqft
   if (*(pOutbuf+OFF_LOT_SQFT+7) == ' ' && pChar->LotSqft[0] > ' ')
   {
      lTmp = atoin(pChar->LotSqft, sizeof(pChar->LotSqft));
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = atoin(pChar->LotAcre, sizeof(pChar->LotAcre));
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      if (*(pOutbuf+OFF_PARK_TYPE) == ' ' || *(pOutbuf+OFF_PARK_TYPE) == 'H')
         *(pOutbuf+OFF_PARK_TYPE) = '2';
   }

   // Heating
   if (pChar->Heating[0] > ' ')
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   
   // Cooling 
   if (pChar->Cooling[0] > ' ')
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } 

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } 

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   }

   // Fireplace
   iFp = atoin(pChar->Fireplace, SIZ_CHAR_SIZE2);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   }

   // HasSeptic or HasSewer
   *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 640, fdChar);

   return 0;
}

/******************************** Sta_MergeExe *******************************
 *
 * Only update HO_FLG if indicated in EXE file.
 *
 *****************************************************************************

int Sta_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdExe);

      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "063032017000", 9))
   //   iTmp = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         //acRec[OFF_HO_FL] = '2';
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      // Skip double quote if needed
      if (*pTmp == '"')
         pTmp++;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      //*(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   if (cDelim == ',')
      iTmp = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   else
      iTmp = ParseStringIQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);

   if (iTmp < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == 'T')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Sta_MergeTax ******************************
 *
 * Note: County currently not release tax file yet.  Need tested before use.
 *
 ****************************************************************************

int Sta_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double   dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "063032017000", 9))
   //   iTmp = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iRet;

   // Merge tax data
   while (!iTmp)
   {
      iTmp = ParseStringIQ(pRec, cDelim, MB_TAX_ROLLCAT+1, apTokens);
      if (iTmp < MB_TAX_ROLLCAT)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
         return -1;
      }

      // If same tax year, apply values
      //if (!memcmp(myCounty.acYearAssd, apTokens[MB_TAX_YEAR], 4))
      {
         dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
         dTax1 = atof(acTmp);
         dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
         dTax2 = atof(acTmp);
         dTmp = dTax1+dTax2;
         if (dTax1 == 0.0 || dTax2 == 0.0)
            dTmp *= 2;

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/********************************* Sta_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired/unsecured record, not use
 *
 *****************************************************************************/

int Sta_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet, iTmp;

#ifdef _DEBUG
   // 059005031000
   // 065024018000
   //if (!memcmp(pOutbuf, "001003007000", 12))
   //   iTmp = 0;
#endif

   // Parse input
   if (cDelim == ',')
      iRet = ParseStringNQ1(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);

   if (iRet != MB_ROLL_M_ADDR4+1)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // Ignore APN starts with 800-999 except 910 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "50STA", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: Fixture, Homesite, Growing, FixtureRealProperty, PPBusiness, PPMH
      long lFixtr    = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lHSite    = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow     = atoi(apTokens[MB_ROLL_GROWING]);
      long lFixtRP   = atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lPP_Bus   = atoi(apTokens[MB_ROLL_PP_BUS]);
      long lPP_MH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      lTmp = lFixtr+lGrow+lFixtRP+lPP_MH+lPP_Bus;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixtr > 0)
         {
            sprintf(acTmp, "%d         ", lFixtr);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lPP_Bus > 0)
         {
            sprintf(acTmp, "%d         ", lPP_Bus);
            memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
         }
         if (lPP_MH > 0)
         {
            sprintf(acTmp, "%d         ", lPP_MH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   if (*apTokens[MB_ROLL_LEGAL] > ' ')
      updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "026021011", 9))
   //   iRet = 0;
#endif

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   if (acTmp[0] > ' ')
   {
      // Make uppercase
      _strupr(acTmp);

      if (acTmp[2] > 'Z')
         acTmp[2] = 0;
      else
         acTmp[SIZ_USE_CO] = 0;

      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
   memset(pOutbuf+OFF_TRANSFER_DT,  ' ', SIZ_TRANSFER_DT);
   if (*apTokens[MB_ROLL_DOCNUM] > '0' && isNumber(apTokens[MB_ROLL_DOCNUM]+5, 7))
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], strlen(apTokens[MB_ROLL_DOCNUM]));
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   }

   // Owner
   try {
      Sta_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   } catch(...) {
      LogMsg("***** Execption occured in Sta_MergeOwner()");
   }

   // Mailing
   try {
      Sta_MergeMAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Execption occured in Sta_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************** Sta_Load_Roll *****************************
 *
 * This function has not been tested.
 *
 ******************************************************************************/

int Sta_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Load roll update for %s", myCounty.acCntyCode);
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open extracted Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Open Tax file
   /* Tax file is not available
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return 2;
   }
   */

   // Open lien file
   fdLienExt = NULL;
   sprintf(acRec, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acRec, 0))
   {
      LogMsg("Open Lien file %s", acRec);
      fdLienExt = fopen(acRec, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acRec);
         return -7;
      }
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
      iTmp = memcmp(acBuf, (char *)&acRollRec[0], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Sta_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Sta_MergeSitus(acBuf);

            // Merge Lien
            //lRet = 1;
            //if (fdLienExt)
            //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

            // Merge Exe if not found in LienExt
            //if (fdExe && lRet)
            //   lRet = MB_MergeExe(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Sta_MergeChar(acBuf);

            // Merge Taxes
            //if (fdTax)
            //   lRet = Sta_MergeTax(acBuf);

            iRollUpd++;
         } else
            iRetiredRec++;

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Sta_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Sta_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Sta_MergeChar(acRec);

            // Merge Taxes
            //if (fdTax)
            //   lRet = Sta_MergeTax(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_SALE1_DT], SIZ_SALE1_DT);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof && acRollRec[1] <= '9')
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Sta_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Sta_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         //else
         //   acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Sta_MergeChar(acRec);

         // Merge Taxes
         //if (fdTax)
         //   lRet = Sta_MergeTax(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_SALE1_DT], SIZ_SALE1_DT);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);

         iNewRec++;
      }
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   //if (fdTax)
   //   fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u\n", lExeMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u\n", lExeSkip);
   //LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   printf("\nTotal output records: %u\n", lCnt);
   return 0;
}

/********************************* Sta_MergeLien *****************************
 *
 * For 2016 LDR AGENCYCDCURRSEC_TR601.TAB
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sta_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_CURRENTDOCDATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], strlen(apTokens[L3_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "50STA", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      vmemcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], SIZ_EXE_CD1);

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&STA_Exemption);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030330020", 9))
   //   iTmp = 0;
#endif
   // Legal
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);
   
      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Sta_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   Sta_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Sta_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc - 2016
   if (*apTokens[L3_CURRENTDOCNUM] > '0')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         if (!memcmp(apTokens[L3_CURRENTDOCNUM], "2018R00R0040", 12))
            memcpy(pOutbuf+OFF_TRANSFER_DOC, "2018R0040722", 12);
         else            
            vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L3_CURRENTDOCNUM], SIZ_TRANSFER_DOC);
      }
   } 

   // Number of parking spaces
   if (*apTokens[L3_GARAGE] == '0' || *apTokens[L3_GARAGE] == 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'H';             // None
   else if (!memcmp(apTokens[L3_GARAGE], "GC", 2))
      *(pOutbuf+OFF_PARK_TYPE) = '2';             // Garage/Carport
   else if (!memcmp(apTokens[L3_GARAGE], "GD", 2))
      *(pOutbuf+OFF_PARK_TYPE) = 'L';             // Garage Detached
   else if (*apTokens[L3_GARAGE] > '0' && *apTokens[L3_GARAGE] <= '9')
   {
      iTmp = atol(apTokens[L3_GARAGE]);
      sprintf(acTmp, "%d", iTmp);
      vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      switch (*(apTokens[L3_GARAGE]+1))
      {
         case 'C':
         case 'c':
         case 'P':
            *(pOutbuf+OFF_PARK_TYPE) = 'C';       // Car port
            break;
         case 'D':
            *(pOutbuf+OFF_PARK_TYPE) = 'D';       // Detached
            break;
         case 'G':
            *(pOutbuf+OFF_PARK_TYPE) = 'Z';       // Garage
            break;
         case 'T':
            *(pOutbuf+OFF_PARK_TYPE) = 'T';       // Tandem Garage 
            break;
      }
   }

   // Garage size
   dTmp = atof(apTokens[L3_GARAGESIZE]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, dTmp);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      if (*(pOutbuf+OFF_PARK_TYPE) == ' ' || *(pOutbuf+OFF_PARK_TYPE) == 'H')
         *(pOutbuf+OFF_PARK_TYPE) = '2';           // GARAGE/CARPORT
   }

   // Total rooms - this data is not reliable

   return 0;
}

// 2015
//int Sta_MergeLien(char *pOutbuf, char *pRollRec)
//{
//   char     acTmp[256];
//   long     lTmp;
//   double   dTmp;
//   int      iRet=0, iTmp;
//
//#ifdef _DEBUG
////   if (!memcmp(pRollRec, "011061007000", 12))
////      iTmp = 0;
//#endif
//
//   // Replace null char with blank
//   iRet = replNull(pRollRec, ' ', 0);
//   //iRet = replChar(pRollRec, 34, 39, 0);
//
//   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
//   if (iRet < L_DTS)
//   {
//      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
//      return -1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//
//   // Start copying data
//   memcpy(pOutbuf, apTokens[L_ASMT], strlen(apTokens[L_ASMT]));
//
//   // Copy ALT_APN
//   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L_FEEPARCEL], strlen(apTokens[L_FEEPARCEL]));
//
//   // Format APN
//   iRet = formatApn(apTokens[L_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//   // Create MapLink and output new record
//   iRet = formatMapLink(apTokens[L_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // County code
//   memcpy(pOutbuf+OFF_CO_NUM, "50STA", 5);
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // Land
//   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: Growing Impr, Fixture, PersProp, PPMH
//   long lFixtr = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
//   long lGrow  = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);
//   long lPers  = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
//   long lPP_MH = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);
//   lTmp = lFixtr+lGrow+lPers+lPP_MH;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//
//      if (lFixtr > 0)
//      {
//         sprintf(acTmp, "%d         ", lFixtr);
//         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
//      }
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%d         ", lPers);
//         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
//      }
//      if (lGrow > 0)
//      {
//         sprintf(acTmp, "%d         ", lGrow);
//         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
//      }
//      if (lPP_MH > 0)
//      {
//         sprintf(acTmp, "%d         ", lPP_MH);
//         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
//      }
//   }
//
//   // Gross total
//   lTmp += (lLand+lImpr);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//   // Tax
//   double dTax1 = atof(apTokens[L_TAXAMT1]);
//   double dTax2 = atof(apTokens[L_TAXAMT2]);
//   dTmp = dTax1+dTax2;
//   if (dTax1 == 0.0 || dTax2 == 0.0)
//      dTmp *= 2;
//
//   if (dTmp > 0.0)
//   {
//      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
//      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
//   } else
//      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
//
//   // Exemption
//   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
//   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
//   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
//   lTmp = lExe1+lExe2+lExe3;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
//      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
//   }  
//   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
//      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
//   else
//      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
//
//   // TRA
//   memcpy(pOutbuf+OFF_TRA, apTokens[L_TRA], strlen(apTokens[L_TRA]));
//
//   // status
//   *(pOutbuf+OFF_STATUS) = *apTokens[L_STATUS];
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "030330020", 9))
//   //   iTmp = 0;
//#endif
//   // Legal
//   updateLegal(pOutbuf, apTokens[L_PARCELDESCRIPTION]);
//
//   // UseCode
//   strcpy(acTmp, apTokens[L_USECODE]);
//   acTmp[SIZ_USE_CO] = 0;
//
//   // Standard UseCode
//   if (acTmp[0] > ' ')
//   {
//      _strupr(acTmp);
//      iTmp = strlen(acTmp);
//      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
//      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
//   }
//
//   // Acres - if type=S and acres>1000, they are probably sqft
//   //         if type=A and acres>5000, they are probably sqft
//   // Anything < 1000 is probably acres
//   dTmp = atof(apTokens[L_ACRES]);
//   if ((dTmp > 1000.0 && *apTokens[L_SIZEACRESFTTYPE] == 'S') ||
//       (dTmp > 5000.0 && *apTokens[L_SIZEACRESFTTYPE] == 'A') )
//   {
//      // Lot Sqft
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, (long)dTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      // Lot Acres = (dTmp * 1000)/SQFT_PER_ACRE
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dTmp*SQFT_MF_1000));
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   } else if (dTmp > 0.0)
//   {
//      // Lot Sqft
//      lTmp = (long)(dTmp * SQFT_PER_ACRE);
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      // Format Acres
//      lTmp = (long)(dTmp * ACRES_FACTOR);
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   // Ag preserved
//   if (*apTokens[L_ISAGPRESERVE] == '1')
//      *(pOutbuf+OFF_AG_PRE) = 'Y';      
//      
//
//   // Owner
//   Sta_MergeOwner(pOutbuf, apTokens[L_OWNER]);
//
//   // Situs
//   Sta_MergeSitus(pOutbuf, apTokens[L_SITUS1], apTokens[L_SITUS2]);
//
//   // Mailing
//   Sta_MergeMAdr(pOutbuf, apTokens[L_MAILADDRESS1], apTokens[L_MAILADDRESS2], apTokens[L_MAILADDRESS3], apTokens[L_MAILADDRESS4]);
//
//   // SetTaxcode, Prop8 flag, FullExe flag
//   iTmp = updateTaxCode(pOutbuf, apTokens[L_TAXABILITY], true, true);
//
//   return 0;
//}

/******************************** Sta_Load_LDR ******************************
 *
 *
 ****************************************************************************/

int Sta_Load_LDR(int iFirstRec, char *pInfile)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acSrtFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   if (!pInfile)
      GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   else
      strcpy(acTmpFile, pInfile);

   // Sort roll file on ASMT
   sprintf(acSrtFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lTmp = getFileDate(acSrtFile);
   if (lTmp < lToday)
   {
      iRet = sortFile(acTmpFile, acSrtFile, "S(#3,C,A) DEL(9)");  // 2016
      if (!iRet)
         return -1;
   }

   // Open roll file
   LogMsg("Open Roll file %s", acSrtFile);
   fdRoll = fopen(acSrtFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Value file - only if LV_File is defined
   fdLienExt = NULL;
   iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
   if (iRet > 10 && !_access(acValueFile, 0))
   {
      sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmpFile, 0))
      {
         LogMsg("Open Lien file %s", acTmpFile);
         fdLienExt = fopen(acTmpFile, "r");
         if (fdLienExt == NULL)
         {
            LogMsg("***** Error opening lien file: %s\n", acTmpFile);
            return -7;
         }
      }
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
      // Create new R01 record
      iRet = Sta_MergeLien(acBuf, acRec);
      if (!iRet)
      {
         // Merge value from LDR extract
         if (fdLienExt)
            lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

         // Merge Char
         if (fdChar)
            lRet = Sta_MergeChar(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!pTmp || acRec[1] > '9')
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************* Sta_MatchRoll ******************************
 *
 * Match GrGr file against R01 and populate APNMatch and Owner match field.
 *
 ****************************************************************************/

int Sta_MatchRoll(LPCSTR pGrGrFile)
{
   char     acBuf[2048], acRoll[2048], acTmpFile[_MAX_PATH], acTmp[256];
   char     *pTmp;
   FILE     *fdIn, *fdOut;
   GRGR_DEF *pGrGr = (GRGR_DEF *)&acBuf[0];
   int      iRet, iTmp, iNoApn, iApnMatch, iApnUnmatch, iOwnerMatch;
   HANDLE   hRoll;
   DWORD    nBytesRead;
   bool     bEof;

   // Open input file
   if (!(fdIn = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error opening %s", pGrGrFile);
      return -1;
   }

   // Create output temp file
   strcpy(acTmpFile, pGrGrFile);
   pTmp = strrchr(acTmpFile, '.');
   strcpy(pTmp, ".tmp");
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating %s", acTmpFile);
      fclose(fdIn);
      return -2;
   }

   // Open roll file
   sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   if (_access(acTmp, 0))
   {
      sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (_access(acTmp, 0))
      {
         LogMsg("***** Missing input file %s.  Please check!", acTmp);
         fclose(fdIn);
         fclose(fdOut);
         return -3;
      }
   }

   LogMsg("Open input file %s", acTmp);
   hRoll = CreateFile(acTmp, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (hRoll == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening %s in Sta_MatchRoll().", acTmp);
      fclose(fdIn);
      fclose(fdOut);
      return -3;
   }

   // Skip first record
   iRet = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);
   iRet = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);

   // Initialize counters
   iNoApn=iApnMatch=iApnUnmatch=iOwnerMatch = 0;
   bEof = false;

   // Loop through input file
   while (!bEof)
   {
      // Read input record
      if (!(pTmp = fgets(acBuf, 2048, fdIn)))
         break;

      pGrGr->APN_Matched = 'N';
      pGrGr->Owner_Matched = 'N';

      // If no APN, skip
      if (pGrGr->APN[0] == ' ')
      {
         iNoApn++;
         fputs(acBuf, fdOut);    // No APN, output anyway for data entry
         continue;
      }
#ifdef _DEBUG
      //if (!memcmp(pGrGr->APN, "010222018", 9))
      //   iRet = 0;
#endif
      // Match APN
      do
      {
         // Compare
         iRet = memcmp(pGrGr->APN, acRoll, 9);
         if (!iRet)
         {
            pGrGr->APN_Matched = 'Y';
            iApnMatch++;

            // Match owner - match last name or the first 10 bytes of grantors
            int   iIdx, iLen;
            char  acOwner[64];

            memcpy(acOwner, (char *)&acRoll[OFF_NAME1], SIZ_NAME1);
            acOwner[SIZ_NAME1] = 0;
            if (pTmp = strchr(acOwner, ' '))
            {
               iLen = pTmp - (char *)&acOwner[0];
               if (iLen < 3) iLen = 10;
            }

            for (iIdx = 0; iIdx < atoin(pGrGr->NameCnt, SIZ_GR_NAMECNT); iIdx++)
            {
               // Compare grantors only
               if (pGrGr->Grantors[iIdx].NameType[0] == 'O')
               {
                  if (!memcmp((char *)&pGrGr->Grantors[iIdx].Name[0], acOwner, iLen))
                  {
                     pGrGr->Owner_Matched = 'Y';
                     iOwnerMatch++;
                     break;
                  }
               } else
                  break;
            }

            // Populate with roll data - to be done when needed

            break;
         } else
         {
            // Read R01 record
            iTmp = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);
            // Check for EOF
            if (!iTmp)
            {
               LogMsg("***** Error reading roll file (%f)", GetLastError());
               bEof = true;
               break;
            }

            // EOF ?
            if (!nBytesRead)
            {
               bEof = true;
               break;
            }
         }
      } while (iRet > 0);

      // Output record
      fputs(acBuf, fdOut);
   }

   // Check for leftover
   while (!feof(fdIn))
   {
      // Read input record
      if (!(pTmp = fgets(acBuf, 2048, fdIn)))
         break;

      // If no APN, skip
      if (pGrGr->APN[0] == ' ')
         iNoApn++;

      pGrGr->APN_Matched = 'N';
      pGrGr->Owner_Matched = 'N';

      // Output record
      fputs(acBuf, fdOut);
   }

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);
   if (hRoll)
      CloseHandle(hRoll);

   // If everything OK, rename output file to original file
   if (remove(pGrGrFile))
      LogMsg("***** Error removing temp file: %s (%d)", pGrGrFile, errno);
   else if (rename(acTmpFile, pGrGrFile))
      LogMsg("***** Error renaming temp file: %s --> %s (%d)", acTmpFile, pGrGrFile, errno);

   LogMsg("                     APN matched: %d", iApnMatch);
   LogMsg("                   Owner matched: %d", iOwnerMatch);
   LogMsg("                         No APN : %d", iNoApn);

   return 0;
}

/***************************** Sta_ConvStdChar() ****************************
 *
 * This version is to replace convertChar().
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

int Sta_ConvStdChar(void)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmpFile[256], acTmp[256], acCode[8], *pRec;
   int      iRet, iTmp, iCmp, iCnt=0;
   double   dTmp;
   STDCHAR  myCharRec;

   LogMsg0("\nExtracting char file %s\n", acCharFile);

   if (!(fdIn = fopen(acCharFile, "r")))
   {
      LogMsg("***** Error open input file %s.  Please check for file exist.", acCharFile);
      return -1;
   }

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   iTmp = 0;
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);

      if (!pRec)
         break;
#ifdef _DEBUG
      //if (!memcmp(acBuf, "004050042000", 9))
      //   iRet = 0;
#endif

      if (cDelim == ',')
         iRet = ParseStringNQ(pRec, cDelim, MB_CHAR_UNITSEQNO+1, apTokens);
      else
         iRet = ParseStringIQ(pRec, cDelim, MB_CHAR_UNITSEQNO+1, apTokens);
      if (iRet != MB_CHAR_UNITSEQNO+1)
      {
         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));

      // Apn
      memcpy(myCharRec.Apn, apTokens[MB_CHAR_ASMT], strlen(apTokens[MB_CHAR_ASMT]));
      
      // Fee parcel
      memcpy(myCharRec.FeeParcel, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));
      
      // AsmtStatus
      myCharRec.AsmtStatus = *apTokens[MB_CHAR_ASMT_STATUS];

      // Pool/Spa
      if (*apTokens[MB_CHAR_POOLS] >= 'A')
      {
         iTmp = 0;
         iCmp = -1;
         while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[MB_CHAR_POOLS], asPool[iTmp].acSrc, asPool[iTmp].iLen)) )
            iTmp++;

         if (!iCmp)
            myCharRec.Pool[0] = asPool[iTmp].acCode[0];
      }

      // Use cat
      blankRem(apTokens[MB_CHAR_USECAT]);
      memcpy(myCharRec.LandUseCat, apTokens[MB_CHAR_USECAT], strlen(apTokens[MB_CHAR_USECAT]));

      // Land Use
      blankRem(apTokens[MB_CHAR_LANDUSE]);
      memcpy(myCharRec.LandUse, apTokens[MB_CHAR_LANDUSE], strlen(apTokens[MB_CHAR_LANDUSE]));

      // Quality Class
      iTmp = remChar(_strupr(apTokens[MB_CHAR_QUALITY]), ' ');
      if (iTmp > SIZ_CHAR_QCLS)
         LogMsg("*** Long QualityClass [%s] on [%.12s]", apTokens[MB_CHAR_QUALITY], apTokens[MB_CHAR_ASMT]);
      vmemcpy(myCharRec.QualityClass, apTokens[MB_CHAR_QUALITY], SIZ_CHAR_QCLS,iTmp);

      strcpy(acTmp, apTokens[MB_CHAR_QUALITY]);
      acCode[0] = 0;
      if (isalpha(acTmp[0]))
      {
         if (!memcmp(acTmp, "SPEC", 4))
            myCharRec.BldgClass = 'M';
         else if (!memcmp(acTmp, "AVG", 4))
            acCode[0] = 'A';
         else
         {
            myCharRec.BldgClass = acTmp[0];
            if (acTmp[1] == 'A')
               acCode[0] = 'A';
            else if (acTmp[1] == 'G')
               acCode[0] = 'G';
         }
         // Quality
         if (isdigit(acTmp[1]))
            iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
         else if (isdigit(acTmp[2]))
            iRet = Quality2Code((char *)&acTmp[2], acCode, NULL);
      } else if (acTmp[0] >= '0')
         iRet = Quality2Code(acTmp, acCode, NULL);
      else if (*apTokens[MB_CHAR_QUALITY] > ' ' )
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[MB_CHAR_QUALITY], apTokens[MB_CHAR_ASMT]);

      if (acCode[0] > '0')
         myCharRec.BldgQual = acCode[0];
      else if (acTmp[0] > ' ')
         LogMsg("*** No QUALITY: '%s' in [%s]", apTokens[MB_CHAR_QUALITY], apTokens[MB_CHAR_ASMT]);

      // YrBlt
      iTmp = atoi(apTokens[MB_CHAR_YRBLT]);
      if (iTmp > 1600 && iTmp <= lLienYear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // BldgSqft
      long lBldgSqft = atoi(apTokens[MB_CHAR_BLDGSQFT]);
      if (lBldgSqft > 100)
      {
         iRet = sprintf(acTmp, "%d", lBldgSqft);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Acres - Don't use landsqft until it is verified by the county
      //         if acres>5000, they are probably sqft
      // Anything < 1000 is probably acres.  Thing in the middle are unsure.  However,
      // we can format it as acres to match with county data.
      dTmp = atof(apTokens[MB_CHAR_LOTACRES]);
      ULONG lSqft = atol(apTokens[MB_CHAR_LOTSQFT]);
      if (dTmp > 5000.0)
      {
         // Lot Sqft
         iRet = sprintf(acTmp, "%d", lSqft);
         memcpy(myCharRec.LotSqft, acTmp, iRet);
         
         // Lot acres
         iRet = sprintf(acTmp, "%d", (long)(dTmp*SQFT_MF_1000));
         memcpy(myCharRec.LotAcre, acTmp, iRet);
      } else if (dTmp > 0.0)
      {
         // Lot Sqft
         iRet = sprintf(acTmp, "%u", (long)((dTmp * SQFT_PER_ACRE) + 0.5));
         memcpy(myCharRec.LotSqft, acTmp, iRet);

         // Lot acres
         iRet = sprintf(acTmp, "%u", (long)((dTmp*1000.0)+0.5));
         memcpy(myCharRec.LotAcre, acTmp, iRet);
      } else if (lSqft > 0)
      {
         // Lot Sqft
         iRet = sprintf(acTmp, "%u", lSqft);
         memcpy(myCharRec.LotSqft, acTmp, iRet);

         // Lot acres
         iRet = sprintf(acTmp, "%u", (long)(lSqft*SQFT_MF_1000));
         memcpy(myCharRec.LotAcre, acTmp, iRet);
      } 

      // GarSqft
      iTmp = atoi(apTokens[MB_CHAR_GARSQFT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
      }

      // Heating
      if (*apTokens[MB_CHAR_HEATING] > ' ')
      {
         iTmp = 0;
         iCmp = -1;
         remChar(apTokens[MB_CHAR_HEATING], ' ');
         while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[MB_CHAR_HEATING], asHeating[iTmp].acSrc, asHeating[iTmp].iLen)) )
            iTmp++;

         if (!iCmp)
            myCharRec.Heating[0] = asHeating[iTmp].acCode[0];
      }

      // Cooling
      remChar(apTokens[MB_CHAR_COOLING], ' ');
      if (*apTokens[MB_CHAR_COOLING] > ' ')
      {
         iTmp = 0;
         iCmp = -1;
         remChar(apTokens[MB_CHAR_COOLING], ' ');
         while (asCooling[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[MB_CHAR_COOLING], asCooling[iTmp].acSrc, asCooling[iTmp].iLen)) )
            iTmp++;

         if (!iCmp)
            myCharRec.Cooling[0] = asCooling[iTmp].acCode[0];
      }

      // Beds
      iTmp = atoi(apTokens[MB_CHAR_BEDS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Baths
      iTmp = atoi(apTokens[MB_CHAR_FBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half baths
      iTmp = atoi(apTokens[MB_CHAR_HBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // Fireplace
      iTmp = atoi(apTokens[MB_CHAR_FP]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Fireplace, acTmp, iRet);
      }

      // Sewer
      blankRem(apTokens[MB_CHAR_HASSEWER]);
      switch (*(apTokens[MB_CHAR_HASSEWER]))
      {
         case 'S':
            myCharRec.HasSewer = 'Y';
            break;
         case 'T':
            myCharRec.HasSewer = 'S';
            myCharRec.HasSeptic = 'Y';
            break;
         case 'E':
            myCharRec.HasSewer = 'E';
            break;
         case 'M':
            myCharRec.HasSewer = 'P';
            break;
         case 'O':
            myCharRec.HasSewer = 'Y';
            break;
      }

      blankRem(apTokens[MB_CHAR_HASWELL]);
      if (*(apTokens[MB_CHAR_HASWELL]) > '0')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // BldgSeqNum
      iTmp = atoi(apTokens[MB_CHAR_BLDGSEQNO]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      }

      // UnitSeqNum
      iTmp = atoi(apTokens[MB_CHAR_UNITSEQNO]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.UnitSeqNo, acTmp, iRet);
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      try {
         fputs((char *)&myCharRec.Apn[0], fdOut);
      } catch (...)
      {
         LogMsg("??? Exception occurs in Sta_ConvStdChar(),  APN=%s", apTokens[MB_CHAR_ASMT]);
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Total CHAR records extracted: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
   } else
      iRet = 0;

   LogMsgD("\nTotal CHAR records after sorted: %d", iRet);

   return iRet;
}

/******************************* Sta_ExtrSale() *****************************
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************

int Sta_ExtrSale()
{
   SCSAL_REC SaleRec;
   char      acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   char      acTmp[256], acRec[512], *pRec;

   FILE      *fdOut;

   int       iTmp;
   double    dTmp;
   long      lCnt=0, lPrice, lTmp;

   LogMsg("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return 2;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acRec, 512, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      pRec = fgets(acRec, 512, fdSale);
      if (!pRec)
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTmp = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTmp = ParseStringIQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);

      if (iTmp < MB_SALES_CONFCODE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTmp);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

      // Docnum
      lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%.5s%0.7ld", apTokens[MB_SALES_DOCNUM], lTmp);
         memcpy(SaleRec.DocNum, acTmp, SALE_SIZ_DOCNUM);

         // Doc date - input format YYYY-MM-DD
         if (dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, YYYY_MM_DD))
            memcpy(SaleRec.DocDate, acTmp, 8);
         else
            continue;
      } else
         continue;

      // Tax
      dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
      lPrice = 0;
      if (acTmp[0] > '0')
      {
         memcpy(SaleRec.StampAmt, acTmp, strlen(acTmp));

         dTmp = atof(acTmp);
         lPrice = (dTmp * SALE_FACTOR);
         if (lPrice < 100)
            sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         else
            sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         memcpy(SaleRec.SalePrice, acTmp, SIZ_SALE1_AMT);
      }

      // Doc code - accept following code only
      // 1 (Sale-Reappraise), 8 (Split/Combo w Sale-Reappraise), 12 (Direct Enrollment Transfer) 
      // 10 (mobilehome transfer)
      memcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], strlen(apTokens[MB_SALES_DOCCODE]));
      if (isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         iTmp = atoi(apTokens[MB_SALES_DOCCODE]);
         if (iTmp ==1 || iTmp == 12 || (iTmp == 8 && lPrice > 0))
            SaleRec.DocType[0] = '1';
         else if (iTmp == 10)
            memcpy(SaleRec.DocType, "75", 2);
         else 
         {
            SaleRec.NoneSale_Flg = 'Y';
            if (iTmp == 52)                        // Transfer - Default
               memcpy(SaleRec.DocType, "52", 2);
            else if (iTmp == 2 || iTmp == 4)       // Transfer
               memcpy(SaleRec.DocType, "75", 2);
            else if (iTmp == 5 || iTmp == 6)       // Partial Transfer
               memcpy(SaleRec.DocType, "75", 2);
         }
      } else if (!memcmp(apTokens[MB_SALES_DOCCODE], "GD", 2))
      {
         SaleRec.DocType[0] = '1';
      } else if (lPrice == 0)
         SaleRec.NoneSale_Flg = 'Y';

      // Transfer Type
      if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      // Group sale?
      if (*apTokens[MB_SALES_GROUPSALE] == '1')
      {
         SaleRec.NumOfPrclXfer[0] = 'M';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

      // Seller
      strcpy(acTmp, apTokens[MB_SALES_SELLER]);
      iTmp = blankRem(acTmp);
      if (iTmp > SALE_SIZ_SELLER)
         iTmp = SALE_SIZ_SELLER;
      memcpy(SaleRec.Seller1, acTmp, iTmp);

      // Buyer
      strcpy(acTmp, apTokens[MB_SALES_BUYER]);
      iTmp = blankRem(acTmp);
      if (iTmp > SALE_SIZ_BUYER)
         iTmp = SALE_SIZ_BUYER;
      memcpy(SaleRec.Name1, acTmp, iTmp);

      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   // Sort output file and dedup
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "DAT");
   LogMsg("Sorting %s to %s.", acTmpFile, acOutFile);

   // Sort on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A) F(TXT) DUPO(1,34) ");
   lCnt = sortFile(acTmpFile, acOutFile, acTmp);

   // Update cumulative sale file
   if (lCnt > 0)
   {
      //sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");      // accummulated sale file
      LogMsg("Append %s to %s.", acCSalFile, acTmpFile);
      iTmp = appendTxtFile(acCSalFile, acTmpFile);
      if (!iTmp)
      {
         // Resort SLS file
         sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         lCnt = sortFile(acTmpFile, acOutFile, acTmp);
         if (lCnt > 0)
         {
            iTmp = remove(acCSalFile);
            if (!iTmp)
               iTmp = rename(acOutFile, acCSalFile);
            else
               LogMsg("***** Error removing %s.  Please rerun with -Xs option", acCSalFile);
         }
      }

   } else
      iTmp = -1;

   LogMsgD("\nTotal Sale records exported: %d.", lCnt);
   return iTmp;
}

/***************************** MB_CreateSCSale *******************************
 *
 * Extract sale data from ???_Sales.csv and output to ???_SALE.DAT
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Sta_CreateSCSale(int iDateFmt, int iDocTypeFmt, int iDocNumFmt, bool bAppend)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp;

   LogMsg0("Extracting Sale file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;
      lCnt++;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

      // Docnum
      if (iDocNumFmt == 3)
      {  // STA
         if (*(apTokens[MB_SALES_DOCNUM]+6) == '-')
            *(apTokens[MB_SALES_DOCNUM]+6) = ' ';
         replCharEx(apTokens[MB_SALES_DOCNUM], "+*`./{}&%$", ' ', 0, true);
         remChar(apTokens[MB_SALES_DOCNUM], ' ');
         lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%.5s%0.7ld  ", apTokens[MB_SALES_DOCNUM], lTmp);
            memcpy(SaleRec.DocNum, acTmp, SALE_SIZ_DOCNUM);
         } else
            continue;         // Ignore record with bad DocNum
      }

      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
         memcpy(SaleRec.DocDate, acTmp, 8);

      // Tax
      if (*apTokens[MB_SALES_TAXAMT] > '0')
      {
         dTmp = atof(apTokens[MB_SALES_TAXAMT]);
         lPrice = (long)(dTmp * SALE_FACTOR);

         // Check for bad DocTax
         if (dTmp > 20000.0)
         {
            if (lPrice != ((lPrice/100)*100.0) )
            {
               lPrice = (long)dTmp;
               if (dTmp != ((lPrice/100)*100.0) )
               {
                  LogMsg("*** Questionable DocTax Amt APN=%s DocTax=%.2f", apTokens[MB_SALES_ASMT], dTmp);
                  lPrice = 0;
               }
            }
         } 

         if (dTmp > 1.0)
         {
            iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
            memcpy(SaleRec.StampAmt, acTmp, iTmp);
         }

         if (lPrice > 100)
         {
            sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
            memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);
         }
      } else
         lPrice = 0;

      // Save original DocCode
      if (isdigit(*apTokens[MB_SALES_DOCCODE]))
      {
         memcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], strlen(apTokens[MB_SALES_DOCCODE]));

         iTmp = findDocType(apTokens[MB_SALES_DOCCODE], (IDX_TBL5 *)&STA_DocCode[0]);
         if (iTmp >= 0)
         {
            memcpy(SaleRec.DocType, STA_DocCode[iTmp].pCode, STA_DocCode[iTmp].iCodeLen);
            if (lPrice < 100)
               SaleRec.NoneSale_Flg = STA_DocCode[iTmp].flag;
         } else if (lPrice < 100)
            SaleRec.NoneSale_Flg = 'Y';
         else
            LogMsg("*** Unknown DocCode %s [%s]", apTokens[MB_SALES_DOCCODE], apTokens[MB_SALES_ASMT]);
      }

      // Transfer Type
      if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1' || *apTokens[MB_SALES_GROUPSALE] == 'T')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

      // Seller
      iTmp = blankRem(apTokens[MB_SALES_SELLER]);
      vmemcpy(SaleRec.Seller1, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER, iTmp);

      // Buyer
      iTmp = blankRem(apTokens[MB_SALES_BUYER]);
      vmemcpy(SaleRec.Name1, apTokens[MB_SALES_BUYER], SALE_SIZ_BUYER, iTmp);

      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A) F(TXT) DUPO(1,34) ");
   lCnt = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lCnt)
      iTmp = -1;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lCnt = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lCnt > 0)
         {
            DeleteFile(acCSalFile);
   
            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
            if (iTmp)
               LogMsg("***** Error renaming %s to %s (%d)", acSrtFile, acCSalFile, _errno);
         } else
            iTmp = -1;
      } else
         iTmp = rename(acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
         DeleteFile(acCSalFile);
      iTmp = rename(acOutFile, acCSalFile);
   }

   LogMsg("Number of Sale records output: %d.", lCnt);
   return iTmp;
}

/***************************** Sta_CreateSCSale *******************************
 *
 * Extract sale data from CumSales.mdb and output to ???_SALE.DAT
 * Input:  CumSales.mdb in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Sta_CreateSCSale2(char *pSalesMdb)
{
   SCSAL_REC SaleRec;
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   char     acTmp[256], acTmp1[32], *pTmp;
   char     acMdbProvider[_MAX_PATH], acSaleTbl[32], acSaleKey[32];

   FILE     *fdOut;
   int      iTmp;
   long     lCnt=0, lTmp, lPrice;
   CString  strFld;
   hlAdoRs  rsStaSale;
   hlAdo    hSaleDb;

   LogMsg("Creating Sale export file for %s", myCounty.acCntyCode);

    // Open database
   try 
   {
      GetIniString("Database", "MdbProvider", "", acMdbProvider, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "SaleTbl", "", acSaleTbl, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "SaleKey", "", acSaleKey, _MAX_PATH, acIniFile);

      strcpy(acTmpFile, acMdbProvider);
      strcat(acTmpFile, pSalesMdb);
      if (!hSaleDb.Connect(acTmpFile))
      {
         LogMsg("***** ERROR accessing %s", pSalesMdb);
         return -1;
      }
   }
   AdoCatch(e)
   {
      LogMsg("***** Error open %s db: %s", pSalesMdb, ComError(e));
      return -1;
   }

   // Open sale file
   LogMsg("Open Sale table %s", acSaleTbl);
   try
   {
      sprintf(acTmp, "SELECT * FROM %s", acSaleTbl);
      LogMsg("%s", acTmp);
      rsStaSale.Open(hSaleDb, acTmp);
   }
   AdoCatch(e)
   {
      LogMsg("***** Error open %s db: %s", acSaleTbl, ComError(e));
      return -2;
   }

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating GrGr output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (rsStaSale.next())
   {  
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      strFld = rsStaSale.GetItem(acSaleKey);
      memcpy(SaleRec.Apn, strFld, strFld.GetLength());

      // Sale date
      lTmp = 0;
      strcpy(acTmp, rsStaSale.GetItem("RECORD_DT"));
      pTmp = dateConversion(acTmp, acTmp1, MM_DD_YYYY_1);

      if (pTmp)
      {
         memcpy(SaleRec.DocDate, acTmp1, 8);
         lTmp = atol(acTmp1);
      }

      if (lTmp > lLastRecDate && lTmp < lToday)
         lLastRecDate = lTmp;

      // Doc# 
      strFld = rsStaSale.GetItem("INSTRU_NO");
      memcpy(SaleRec.DocNum, strFld, strFld.GetLength());

      // Stamp Amt
      strFld = rsStaSale.GetItem("STAMP_TAX");
      memcpy(SaleRec.StampAmt, strFld, strFld.GetLength());

      // Sale price
      strFld = rsStaSale.GetItem("ESTSALESPRICE");
      lPrice = atol(strFld);
      if (lPrice > 0)
      {
         iTmp = sprintf(acTmp, "%*ld", SALE_SIZ_SALEPRICE, lPrice);
         memcpy(SaleRec.SalePrice, acTmp, iTmp);
      }

      // New owner
      strFld = rsStaSale.GetItem("NEWOWNER");
      if (strFld.GetLength() > 5)
         memcpy(SaleRec.Name1, strFld, strFld.GetLength());

      // Seller or previous owner
      strFld = rsStaSale.GetItem("OLDOWNER");
      if (strFld.GetLength() > 5)
         memcpy(SaleRec.Seller1, strFld, strFld.GetLength());

      // Doc Code: take 1 (Sale-Reappraise), 8 (Split/Combo w Sale-Reappraise)
      // and 12 (Direct Enrollment Transfer) only
      strFld = rsStaSale.GetItem("DOCCODE");
      iTmp = atoi(strFld);
      if (iTmp == 1 || iTmp == 3 || iTmp == 12 || (iTmp == 8 && lPrice > 0))
         SaleRec.DocType[0] = '1';
      else if (iTmp == 10)                      // Mobile home transfer
         memcpy(SaleRec.DocType, "75", 2);

      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   rsStaSale.Close();
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   // Sort output file and dedup
   char acSrtFile[_MAX_PATH];
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");      // accummulated sale file
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   LogMsg("Sorting %s+%s to %s.", acCSalFile, acTmpFile, acOutFile);
   sprintf(acSrtFile, "%s+%s", acCSalFile, acTmpFile, acOutFile);
   sprintf(acTmp, "S(1,12,C,A,27,8,C,A,15,12,C,A) F(TXT) DUPO(1,34) ");

   // Sort on APN asc, DocDate asc, DocNum asc
   lCnt = sortFile(acSrtFile, acOutFile, acTmp);
   if (lCnt > 0)
   {
      DeleteFile(acCSalFile);

      // Rename Dat to SLS file
      iTmp = rename(acOutFile, acCSalFile);
      LogMsg("Create Sale export completed: %d", lCnt);
   } else
   {
      iTmp = -1;
      LogMsg("***** Sale export completed with error");
   }

   return iTmp;
}

/*********************************** loadSta ********************************
 *
 * Options:
 *    -CSTA -L -Xs|-Ms -Xa -Xl      load lien
 *    -CSTA -U -Xs|-Ms [-Xa]        load update
 *
 *
 ****************************************************************************/

int loadSta(int iSkip)
{
   int   iRet=0;
   char  acTmp[_MAX_PATH], acSrtCmd[_MAX_PATH], acFixFile[_MAX_PATH], acLienFile[_MAX_PATH];

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;

   iApnLen = myCounty.iApnLen;

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      TC_SetDateFmt(MM_DD_YYYY_1);
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Reload old cumsale files
   /*
   char acTmpFile[_MAX_PATH];
   GetIniString(myCounty.acCntyCode, "CumSaleFile", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acTmpFile, acTmp, 2009);
   iRet = Sta_CreateSCSale2(acTmpFile);
   sprintf(acTmpFile, acTmp, 2010);
   iRet = Sta_CreateSCSale2(acTmpFile);
   sprintf(acTmpFile, acTmp, 2011);
   iRet = Sta_CreateSCSale2(acTmpFile);
   sprintf(acTmpFile, acTmp, 2012);
   iRet = Sta_CreateSCSale2(acTmpFile);
   */

   // Fix cumsale - replace 6 digit with 7 digit
   //iRet = FixSCSal(acCSalFile, SALE_FIX_DOCNUM, CNTY_STA);

   if (iLoadFlag & FIX_CSTYP)
   {
      iRet = FixDocType(acCSalFile, (IDX_TBL5 *)&STA_DocCode[0], true /* remove rec w/o DocNum */);
      if (iRet)
         return iRet;
   }

   // Extract lien file
   //if (iLoadFlag & EXTR_LIEN)    // -Xl
   //{
   //   iRet = MB_ExtrTR601(myCounty.acCntyCode);
   //   printf("\n");
   //}

   // 2024 - Fix input file
   if ((iLoadFlag & EXTR_LIEN) || (iLoadFlag & LOAD_LIEN))
   {
      sprintf(acFixFile, "%s\\%s\\%s_Lien.fix", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = RebuildCsv_IQ(acRollFile, acFixFile, cLdrSep, L3_COLS);
      if (iRet > 0)
      {
         strcpy(acRollFile, acFixFile);
      } else
      {
         LogMsg("***** Bad input file: %s", acRollFile);
         return -1;
      }
   }

   // Extract lien file - LDR2016
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      iRet = GetIniString(myCounty.acCntyCode, "LV_File", "", acValueFile, _MAX_PATH, acIniFile);
      if (iRet > 10 && !_access(acValueFile, 0))
         iRet = MB_ExtrTC601(myCounty.acCntyCode, acValueFile, 0, true);      // 2016 
      else
         iRet = MB_ExtrTR601(myCounty.acCntyCode, acRollFile);
   }

   // Extract characteristics
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Check for Unicode file
      //sprintf(acTmp, "%s\\%s\\Charfile.txt", acTmpPath, myCounty.acCntyCode);
      //if (!UnicodeToAnsi(acCharFile, acTmp))
      //   strcpy(acCharFile, acTmp);

      iRet = Sta_ConvStdChar();
      if (iRet <= 0)
      {
         LogMsg("***** Error extracting attributes data from %s", acCharFile);
         return -1;
      }
   }

   // Create/Update cum sale file
   // Extract Sale file from Sta_Sales.txt to Sta_Sale.dat and append to Sta_Sale.sls
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Check for Unicode file
      //sprintf(acTmp, "%s\\%s\\Salefile.txt", acTmpPath, myCounty.acCntyCode);
      //if (!UnicodeToAnsi(acSalesFile, acTmp))
      //   strcpy(acSalesFile, acTmp);

      // 12/30/2013
      iRet = Sta_CreateSCSale(YYYY_MM_DD, 1, 3, true);
      if (iRet)
         return iRet;

      iLoadFlag |= MERG_CSAL;
   }

   iRet = 0;
   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      //sprintf(acLienFile, "%s\\%s\\%s_LIEN.CSV", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      if (iLoadFlag & LOAD_LIEN)
      {
         // Create Lien file
         LogMsg0("Load %s Lien file", myCounty.acCntyCode);
         iRet = Sta_Load_LDR(iSkip, acRollFile);
         if (!iRet)
            iLoadFlag |= MERG_CSAL;
      }

      if (!iRet && (iLoadFlag & LOAD_UPDT))
      {
         LogMsg0("Prepare roll update files");

         // Special case when reloading LDR with roll update at the same time
         if (iLoadFlag & LOAD_LIEN)
         {
            // Rename S01 file to O01 if exist
            sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
            if (!_access(acTmp, 0))
            {
               sprintf(acLienFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "O01");
               if (!_access(acLienFile, 0))
                  remove(acLienFile);
               rename(acTmp, acLienFile);
            }

            // Restore roll file for updating.
            GetIniString(myCounty.acCntyCode, "RollFile", "", acRollFile, _MAX_PATH, acIniFile);
         }

         // Check for Unicode file
         /*
         sprintf(acTmp, "%s\\%s\\Rollfile.txt", acTmpPath, myCounty.acCntyCode);
         if (!UnicodeToAnsi(acRollFile, acTmp))
         {
            strcpy(acRollFile, acTmp);

            // Check Situs
            sprintf(acTmp, "%s\\%s\\Situs.txt", acTmpPath, myCounty.acCntyCode);
            if (!UnicodeToAnsi(acSitusFile, acTmp))
               strcpy(acSitusFile, acTmp);

            // Check Exe
            sprintf(acTmp, "%s\\%s\\Exe.txt", acTmpPath, myCounty.acCntyCode);
            if (!UnicodeToAnsi(acExeFile, acTmp))
               strcpy(acExeFile, acTmp);
         }
         */

         // Sort Situs file
         sprintf(acSrtCmd, "S(#1,C,A) F(TXT) DEL(%d) ", cDelim);
         sprintf(acTmp, "%s\\%s\\Situs.srt", acTmpPath, myCounty.acCntyCode);
         iRet = sortFile(acSitusFile, acTmp, acSrtCmd);
         strcpy(acSitusFile, acTmp);

         // Sort Exe file
         sprintf(acSrtCmd, "S(#2,C,A) B(66,R) DEL(%d) ", cDelim);
         sprintf(acTmp, "%s\\%s\\Exe.srt", acTmpPath, myCounty.acCntyCode);
         iRet = sortFile(acExeFile, acTmp, acSrtCmd);
         strcpy(acExeFile, acTmp);

         // Fix broken records in roll file
         sprintf(acFixFile, "%s\\%s\\%s_roll.fix", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
         iRet = RebuildCsv(acRollFile, acFixFile, cDelim, STA_ROLL_COLS);
         lLastFileDate = getFileDate(acRollFile);

         // Sort input
         sprintf(acTmp, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
         // Sort on APN ASC, LOTACRES DESC and omit APN > 999 or unsec parcels
         sprintf(acSrtCmd, "S(#1,C,A) DEL(%d) F(TXT)", cDelim);
         iRet = sortFile(acFixFile, acTmp, acSrtCmd);
         if (iRet > 0)
         {
            strcpy(acRollFile, acTmp);
            iRet = Sta_Load_Roll(iSkip);
         } else
            return -1;
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Sta_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
   }

   if (!iRet && bMergeOthers)
   {
      char acTmpFile[_MAX_PATH];

      // Merge other values
      sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmpFile, 0))
         iRet = PQ_MergeOthers(myCounty.acCntyCode, acTmpFile, GRP_MB, iSkip);
      else
         LogMsg("***** Lien extract is missing: %s.  Please locate the file and retry", acTmpFile);
   }

   return iRet;
}

