#ifndef  _PQ_H_
#define  _PQ_H_

#define  DEFAULT_TRA_LEN   6
#define  PQ_FIX_APN        0x00000001
#define  PQ_FIX_TRA        0x00000010
#define  PQ_CLR_CHARS      0x00000100
#define  PQ_REM_AMT9       0x00001000
#define  PQ_FIX_FULLEXE    0x00010000
#define  PQ_FIX_SALEPRICE  0x00100000
#define  PQ_FIX_NSALEFLG   0x01000000
#define  PQ_REM_SALES      0x10000000
#define  PQ_REM_XFER       0x00000002
#define  PQ_FIX_FP         0x00000020

#define  PQ_UPD_LEGAL      0x00000001
#define  PQ_UPD_USECODE    0x00000002
#define  PQ_UPD_ZONING     0x00000004

void PQ_MergeLienRec(char *pOutbuf, char *pLienRec, int iGrp=0, bool bSetFlg=true);
int  PQ_MergeLien(char *pOutbuf, FILE *fd, int iGrp=0, bool bSetFlg=false);
int  PQ_MergeLienExt(char *pCnty, int iGrp, int iSkip=1);
int  PQ_MergeValueExt(char *pCnty, int iGrp, int iSkip=1);
int  PQ_FixTRA(char *pCnty);
int  PQ_FixName(LPSTR pBuf, int iLen);
int  PQ_FixLienExt(char *pCnty, int iFixTRA, int iFixAPN);
int  PQ_ConvLienExt(char *pCnty);
int  PQ_FixR01(char *pCnty, int iFixOpts, int iFixLen=0, bool bRemDash=false, int iSkip=1);
int  PQ_MergeOthers(char *pCnty, char *pLienExt, int iGrp, int iSkip=1);
void PQ_MergeOtherRec(char *pOutbuf, char *pLienRec, int iGrp);
int  PQ_ChkBadR01(char *pCnty, char *pRawTmpl, int iRecordLen, char cRepl);
int  PQ_FixBadChar(char *pCnty, char cReplChar, int iRecLen);
int  PQ_MergePrevApn(char *pOutbuf, int iPrevApnLen, FILE *fd);
int  PQ_MergeLotArea(char *pCnty, bool bOverWrite=false);
int  PQ_CopyOldR01(char *pOutbuf, HANDLE fhIn, int iType, int iSkip=1);
int  PQ_SetDefaultPQZoning(char *pCnty);
int  PQ_RemovePQZoning(char *pCnty, int iSkip=1);
// Update PP values using data extracted from BPP records in roll update
int  PQ_UpdateBpp(char *pCnty, int iGrp, int iSkip=1);
#endif