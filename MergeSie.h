
#if !defined(AFX_MERGESIE_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_)
#define AFX_MERGESIE_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_

#define  SIE_CHAR_FEEPARCEL            0
#define  SIE_CHAR_POOLSPA              1
#define  SIE_CHAR_CATTYPE              2
#define  SIE_CHAR_QUALITYCLASS         3
#define  SIE_CHAR_YRBLT                4
#define  SIE_CHAR_BUILDINGSIZE         5
#define  SIE_CHAR_ATTACHGARAGESF       6
#define  SIE_CHAR_DETACHGARAGESF       7
#define  SIE_CHAR_CARPORTSF            8
#define  SIE_CHAR_HEATING              9
#define  SIE_CHAR_COOLINGCENTRALAC     10
#define  SIE_CHAR_COOLINGEVAPORATIVE   11
#define  SIE_CHAR_COOLINGROOMWALL      12
#define  SIE_CHAR_COOLINGWINDOW        13
#define  SIE_CHAR_STORIESCNT           14
#define  SIE_CHAR_UNITSCNT             15
#define  SIE_CHAR_TOTALROOMS           16
#define  SIE_CHAR_EFFYR                17
#define  SIE_CHAR_PATIOSF              18
#define  SIE_CHAR_BEDROOMS             19
#define  SIE_CHAR_BATHROOMS            20
#define  SIE_CHAR_HALFBATHS            21
#define  SIE_CHAR_FIREPLACE            22
#define  SIE_CHAR_ASMT                 23
#define  SIE_CHAR_BLDGSEQNUM           24
#define  SIE_CHAR_HASWELL              25
#define  SIE_CHAR_LOTSQFT              26
#define  SIE_CHAR_COLS                 27

//static XLAT_CODE  asBldgType[] =
//{
//   // Value, lookup code, value length
//   "C", " ", 1,               // Concrete
//   "M", " ", 1,               // Masonry
//   "S", " ", 1,               // Steel
//   "T", " ", 1,               // Tilt up
//   "W", " ", 1,               // Wood
//   "",   "",  0
//};

static XLAT_CODE  asCond[] =
{
   // Value, lookup code, value length
   "A", "A", 1,               // Average
   "E", "E", 1,               // Excellent
   "F", "F", 1,               // Fair
   "G", "G", 1,               // Good
   "N", "N", 1,               // New
   "P", "P", 1,               // Poor
   "U", "U", 1,               // Unsound
   "",  "",  0
};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "1", "1", 1,             // 1 Fireplace
   "2", "2", 1,             // 2 Fireplaces
   "3", "3", 1,             // 3 Fireplaces
   "8", "W", 1,             // Wood Stove - INSERT
   "9", "W", 1,             // Wood Stove
   "N", "N", 1,             // None
   "Y", "Y", 1,             // FirePlacee
   "",   "", 0
};

static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "B",  "F", 1,              // Baseboard
   "BA", "F", 2,              // Baseboard
   "FC", "Z", 2,              // Forced/Central
   "FL", "C", 2,              // Floor
   "PO", "6", 2,              // Portable
   "WA", "D", 2,              // Wall
   "",   "",  0
};

//static XLAT_CODE  asPool[] =
//{
//   // Value, lookup code, value length
//   "ABV", "X", 3,             // Above gound pool
//   "BLT", "B", 3,             // Built-in pool
//   "SPA", "S", 3,             // Spa
//   "",   "", 0
//};

//static XLAT_CODE  asSewer[] =
//{
//   // Value, lookup code, value length
//   "P", "P", 1,               // Public
//   "S", "S", 1,               // Septic
//   "",   "",  0
//};

static XLAT_CODE  asWaterSrc[] =
{
   // Value, lookup code, value length
   "M", "P", 1,               // Municipal
   "W", "W", 1,               // Private Well
   "",  "",  0
};

IDX_TBL5 SIE_DocCode[] =
{  // DocCode, Index, Non-sale, DocCodelen, Indexlen
   "01", "74", 'Y', 2, 2,       // New Taxable Possessory
   "02", "74", 'Y', 2, 2,       // Relocation - No ClO
   "03", "74", 'Y', 2, 2,       // Relocation - with
   "04", "74", 'Y', 2, 2,       // 100% CIO from Asmt Wk
   "05", "1 ", 'N', 2, 2,       // 100% UNS CIO Reappraisable
   "06", "57", 'N', 2, 2,       // Partial ClO Unsecured Roll
   "07", "45", 'N', 2, 2,       // Amended Location WITH VAL Change
   "08", "45", 'Y', 2, 2,       // Amended Location No Value Change
   "09", "74", 'Y', 2, 2,       // Partial ClO from Asmt Wk NT
   "10", "74", 'Y', 2, 2,       // Proposition 8 Removed
   "11", "74", 'Y', 2, 2,       // Proposition 8 Granted
   "12", "74", 'Y', 2, 2,       // Calamity Relief Enroll
   "13", "74", 'Y', 2, 2,       // Calamity Relief Restored (Removed)
   "14", "74", 'Y', 2, 2,       // Calamity Restore with New Construction
   "15", "74", 'Y', 2, 2,       // Review Prop 8/Calamity NOT ENROLLED
   "16", "1 ", 'N', 2, 2,       // New Parcel W/CIO
   "17", "74", 'Y', 2, 2,       // New Parcel No/ClO
   "18", "74", 'Y', 2, 2,       // New Parcel Not Subject to XIIA
   "19", "74", 'Y', 2, 2,       // New Parcel W/New Construction
   "20", "74", 'Y', 2, 2,       // Correction to Title Only - No ClO
   "21", "1 ", 'N', 2, 2,       // Sale 100% Reappraisable
   "22", "1 ", 'N', 2, 2,       // Non Sale 100% Reappraisable
   "23", "57", 'N', 2, 2,       // Partial Interest Sale
   "24", "57", 'N', 2, 2,       // Partial Interest Non Sale
   "25", "77", 'Y', 2, 2,       // Foreclosure/Default
   "26", "44", 'Y', 2, 2,       // Lease Fee 35+
   "27", "44", 'N', 2, 2,       // LEOP ClO orCliCc (Legal Entity Ownership Program)
   "28", "75", 'Y', 2, 2,       // Transfer on Death (Newly Recorded-No CIO)
   "29", "74", 'Y', 2, 2,       // Release of CP Rights - No CIO
   "30", "74", 'Y', 2, 2,       // New Construction Residential
   "31", "74", 'Y', 2, 2,       // New Construction Commercial
   "32", "74", 'Y', 2, 2,       // New Construction Industrial
   "33", "74", 'Y', 2, 2,       // New Construction Land
   "34", "74", 'Y', 2, 2,       // New Residential Improvement
   "35", "74", 'Y', 2, 2,       // Manufactured Home New Construction
   "36", "19", 'Y', 2, 2,       // New Commercial/Industrial Improvement
   "37", "19", 'Y', 2, 2,       // New Construction Partially Complete
   "38", "19", 'Y', 2, 2,       // New Construction Review NO VAL CHANGE
   "40", "19", 'Y', 2, 2,       // Demolition Residential
   "41", "19", 'Y', 2, 2,       // Demolition - Commercial
   "42", "19", 'Y', 2, 2,       // Demolition - Industrial
   "43", "19", 'Y', 2, 2,       // Demolition - Land
   "44", "19", 'Y', 2, 2,       // Relocated Residence
   "45", "19", 'Y', 2, 2,       // Relocated Manufactured Home
   "46", "19", 'Y', 2, 2,       // Relocated Commercial Improvement
   "47", "19", 'Y', 2, 2,       // Relocated Industrial improvement
   "48", "19", 'Y', 2, 2,       // Relocated Improvement becomes Personalty
   "50", "19", 'Y', 2, 2,       // Municipal Corporation
   "51", "74", 'Y', 2, 2,       // Non-Renewal Term Ended/Cancellation
   "52", "74", 'X', 2, 2,       // TPZ Non-Renewal Annual Review
   "53", "74", 'Y', 2, 2,       // WA Non-Renewal Annual Review
   "54", "74", 'Y', 2, 2,       // Factored Base Year Value Enrolled
   "55", "74", 'Y', 2, 2,       // Farmland Security Zone Contract (FSZ)
   "58", "74", 'Y', 2, 2,       // Restricted Parcel Value Change
   "60", "74", 'Y', 2, 2,       // New Possessory Interest
   "61", "44", 'Y', 2, 2,       // PI Lease Renewal
   "62", "74", 'Y', 2, 2,       // PI Terminate
   "63", "74", 'Y', 2, 2,       // Amended Location with Value Change
   "64", "44", 'Y', 2, 2,       // Mineral Lease - Creation/Sub Lease/Assignment
   "65", "44", 'Y', 2, 2,       // Termination of Mineral Lease
   "66", "74", 'Y', 2, 2,       // Removal of PI Structure
   "67", "74", 'Y', 2, 2,       // Annual Mining Review Completed
   "70", "19", 'Y', 2, 2,       // Misc/Admin Activity
   "72", "19", 'Y', 2, 2,       // Legal Change
   "74", "19", 'Y', 2, 2,       // Annual PPOP Review (MH, Boats, Water Co, etc.
   "75", "74", 'Y', 2, 2,       // BBSS
   "77", "57", 'Y', 2, 2,       // Non Reappraisable Transfer - Partial Interest
   "78", "75", 'Y', 2, 2,       // Non Reappraisable Transfer - 100% Interest
   "79", "44", 'Y', 2, 2,       // LEOP CIC Non Reappraisable
   "82", "19", 'Y', 2, 2,       // Clearing Title Only - No CIO
   "","",0,0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SIE_Exemption[] = 
{
   "E01", "H", 3,1,
   "E02", "D", 3,1,     // DISABLED VET (LOW INCOME)
   "E04", "V", 3,1,     // VETERAN
   "E05", "R", 3,1,     // RELIGIOUS/CHURCH
   "E06", "U", 3,1,     // COLLEGE
   "E07", "E", 3,1,     // CEMETARY
   "E08", "D", 3,1,     // DISABLED VETERAN
   "E09", "X", 3,1,     // VESSEL
   "E10", "P", 3,1,
   "E11", "X", 3,1,     // LESSEE/LESSOR
   "E12", "I", 3,1,     // WELFARE: HOSPITAL/CLINIC
   "E13", "M", 3,1,     // FREE MUSEUM/FREE LIBRARY
   "E14", "X", 3,1,     // HISTORICAL AIRCRAFT
   "E16", "W", 3,1,     // WELFARE (LOW INCOME HOUSING)
   "E18", "D", 3,1,     // LOW INCOME DISABLED VETERAN
   "","",0,0
};

#endif // !defined(AFX_MERGESIE_H__CEF47DA5_7684_407F_83DB_A2CCBB477F__INCLUDED_)
