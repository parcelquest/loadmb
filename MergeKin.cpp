/******************************************************************************
 *
 * Notes:
 *    - Update files are similar to ALP
 *
 * Revision
 * 08/18/2021 21.1.9    Copy from MergeMon.cpp
 *                      Modify for MB file format.
 * 08/26/2021 21.2.0    Default bUseConfSalePrice=false so we don't use confidential sale price.
 * 10/04/2021 21.2.6    Add code for loading tax using MB data files.
 * 08/17/2022 22.1.4    Update S_HSENO in Kin_MergeSitus() for LDR version.
 * 08/01/2023 23.1.0    Add Kin_MergeOwner3(), Kin_MergeLienL3() and Kin_MergeLienL3() for new LDR file.
 *                      Modify Kin_Load_LDR() to turn ON situs update.
 * 08/03/2023 23.1.3    Fix bad char in Kin_MergeOwner() and APN_D in Kin_ConvStdChar().
 * 10/18/2023 23.4.0    Update skip header value in all tax loading functions in loadKin().
 * 07/24/2024 24.0.3    Modify Kin_MergeLien3() to add ExeType.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "CharRec.h"
#include "doGrgr.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "LoadMB.h"
#include "Markup.h"
#include "PQ.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "Tax.h"

#include "MergeKin.h"

bool Kin_FormatDocNum(char *pResult, char *pDocNum, char *pDocDate, char *pApn);
void Kin_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf, char *pTaxability);

/******************************** Kin_MergeMisc ******************************
 *
 * Copy various fields from roll file since LDR file doesn't have them.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Kin_MergeMisc(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     *apItems[64], *pTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header
      pRec = fgets(acRec, 1024, fdRoll);
      // Get first rec
      pRec = fgets(acRec, 1024, fdRoll);
   }
         
   pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Zone rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 1024, fdRoll);
         lZoneSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringNQ(acRec, cDelim, 64, apItems);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input Zone record for APN=%s", apItems[0]);
         iRet = -1;
      }

      return iRet;
   }

   // Merge data
   if (*apItems[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apItems[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apItems[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // Update Transfer
   iTmp = atol(apItems[MB_ROLL_DOCDATE]);
   char acTmp[32];
   if (dateConversion(apItems[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1))
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, apItems[MB_ROLL_DOCNUM], strlen(apItems[MB_ROLL_DOCNUM]));
   }
                
   // Taxability - SetTaxcode, Prop8 flag, FullExe flag
   //iTmp = updateTaxCode(pOutbuf, apItems[MB_ROLL_TAXABILITY], true, true);

   // Owner
   //Kin_MergeOwner(pOutbuf, apItems[MB_ROLL_OWNER], apItems[MB_ROLL_CAREOF], apItems[MB_ROLL_TAXABILITY]);

   // Legal
   //updateLegal(pOutbuf, apItems[MB_ROLL_LEGAL]);

   lZoneMatch++;

   // Get next record
   pRec = fgets(acRec, 1024, fdRoll);

   return 0;
}

/******************************** Kin_MergeExe *******************************
 *
 * Merge exemption value
 *
 *****************************************************************************/

int Kin_MergeExe(char *pOutbuf, int iSkipHdr)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp, *apItems[16];
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iSkipHdr; iTmp++)
         pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
      if (acRec[0] == '"')
         iTmp = 1;
      else
         iTmp = 0;
      if (iTmp != iSkipQuote)
      {
         LogMsg("*** Overwrite INI SkipQuote value to %d (from %d)", iTmp, iSkipQuote);
         iSkipQuote = iTmp;
      }
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      iTmp = memcmp(pOutbuf, pTmp+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   iRet = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apItems);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "125132011000", 9))
   //   iTmp = 0;
#endif

   // EXE code
   if (*apItems[MB_EXE_CODE] > ' ')
      memcpy(pOutbuf+OFF_EXE_CD1, apItems[MB_EXE_CODE], strlen(apItems[MB_EXE_CODE]));

   // HO Exe
   myLTrim(apItems[MB_EXE_HOEXE]);
   if (*apItems[MB_EXE_HOEXE] == '1' || *apItems[MB_EXE_HOEXE] == 'T')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apItems[MB_EXE_EXEAMT]);
   if (lTmp > 0 && lTmp < 999999999)
   {
      long lGross = atoin(pOutbuf+OFF_GROSS, SIZ_GROSS);

      if (lTmp > lGross)
      {
         if (bDebug)
            LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Kin_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 * Notes: CareOf can be expanded portion of owner name.  Default parsing method
 * to 3.  If suspect it has different last name in CareOf, use parsing method 4.
 *
 *****************************************************************************/

void Kin_MergeOwner(char *pOutbuf, char *pNames, char *pCareOf, char *pTaxability)
{
   int   iTmp, iRet, iParseMethod=3;
   char  acTmp1[128], acTmp2[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[128], acName2[128], acOwner[128];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);

   memset(acTmp, 0, 128);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002585009", 9))
   //   iTmp = 0;
#endif

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      // Change special char
      if ((UCHAR)*pTmp == 0x90)
         *pTmp = 'E';

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' and single quote too
      if (*pTmp == '.' || *pTmp == 39)
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;
   acName2[0] = 0;
   acSave1[0] = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   if (*pCareOf == ' ' && *(pCareOf+1) > ' ')
      pCareOf++;
   if (*pCareOf > ' ')
   {
      if (!memcmp(pTaxability, "003", 3) || !memcmp(pTaxability, "040", 3))
      {
         // Check for ATTN or C/O
         if (!memcmp(pCareOf, "ATTN", 4) || !memcmp(pCareOf, "C/O", 3) || *pCareOf == '%')
         {
            updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
         } else
         {  // append to Name1 or keep it as Name2
            strcpy(acTmp2, pCareOf);

            if (pTmp = strchr(acTmp2, '%'))
            {
               *(pTmp-2) = 0;
               memcpy(pOutbuf+OFF_CARE_OF, pTmp, strlen(pTmp));
               strcat(acTmp, " ");
               strcat(acTmp, acTmp2);
            } else
            {
               // Not CareOf, append to Name1
               strcat(acTmp, " ");
               strcat(acTmp, acTmp2);
            }
            iTmp = strlen(acTmp);
         }

         // Public parcel
         if (acTmp[iTmp-1] == '&') 
            acTmp[iTmp-1] = 0;
         vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME1);
         *(pOutbuf+OFF_PUBL_FLG) = 'Y';
         return;
      } else if (acTmp[iTmp-1] == '&')
      {
         // Put it in Name2
         acTmp[iTmp] = ' ';
         strcpy(acTmp2, pCareOf);
         if (pTmp = strchr(acTmp2, '('))
         {
            if (*(pTmp-1) == ' ') pTmp--;
            *pTmp = 0;
         }

         // If acTmp2 is a single word, append to acTmp
         if (!(pTmp = strchr(acTmp2, ' ')))
         {
            strcat(acTmp, acTmp2);
            acTmp2[0] = 0;
         } else
         {
            strcpy(acName2, acTmp2);
         }        
      } else
      {
         // CareOf
         updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
      }
   }

   // Save original owner name
   strcpy(acOwner, acTmp);
   strcpy(acName1, acTmp);

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp || !memcmp(acTmp, "BOARD OF", 8))
   {
      vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME1);
      return;
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
   }

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
         strcpy(acName2, pTmp);
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;
   if ((pTmp=strstr(acName2, " AS ")) || (pTmp=strstr(acName2, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Eliminate last '&' in name
   iTmp = strlen(acName1)-1;
   if (acName1[iTmp] == '&')
      acName1[iTmp] = 0;

   // Save Name1
   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);

   // Now parse owners to swap name
   iRet = 0;
   memset((void *)&myOwner, 0, sizeof(OWNER));
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, iParseMethod);
      pTmp = strchr(acOwner, ' ');

      // If name is not swapable, use Name1 instead
      if (iRet == -1 || *(pOutbuf+OFF_ETAL_FLG) == 'Y' || !memcmp(acOwner, myOwner.acSwapName, pTmp - &acOwner[0]))
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
      else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
   {
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
   }

   // Process Name2 when there is more than one word
   if (acName2[0] && strchr((char *)&acName2[1], ' ') )
      vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);
}

void Kin_MergeOwner3(char *pOutbuf, char *pNames)
{
   int   iTmp, iRet, iParseMethod=3;
   char  acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[128], acName2[128], acOwner[128];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   memset(acTmp, 0, 128);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002585009", 9))
   //   iTmp = 0;
#endif

   // Check special case
   pTmp = strcpy(acTmp, pNames);
   if (pTmp = strchr(acTmp, 0x90))
      *pTmp = 'E';

   // Remove multiple spaces
   iTmp = blankRem(acTmp, ' ');
   acName2[0] = 0;
   acSave1[0] = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Save original owner name
   strcpy(acOwner, acTmp);
   strcpy(acName1, acTmp);

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp || !memcmp(acTmp, "BOARD OF", 8))
   {
      vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME1);
      return;
   }

   // Save Name1
   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Eliminate last '&' in name
   iTmp = strlen(acName1)-1;
   if (acName1[iTmp] == '&')
      acName1[iTmp] = 0;

   // Now parse owners to swap name
   iRet = 0;
   memset((void *)&myOwner, 0, sizeof(OWNER));
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, iParseMethod);
      pTmp = strchr(acOwner, ' ');

      // If name is not swapable, use Name1 instead
      if (iRet == -1 || *(pOutbuf+OFF_ETAL_FLG) == 'Y' || !memcmp(acOwner, myOwner.acSwapName, pTmp - &acOwner[0]))
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
      else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
   {
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
   }
}

/******************************** Kin_MergeMAdr ******************************
 *
 * Merge Mail address from roll file
 *
 *****************************************************************************/

void Kin_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // CareOf - parse and populate in Kin_MergeOwner()

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
   } 

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001463020", 9) )
   //   iTmp = 0;
#endif

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], SIZ_M_CITY);
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         // Take out '-' in zipcode if present
         strcpy(acTmp, apTokens[MB_ROLL_M_ZIP]);
         iTmp = strlen(acTmp);
         if (acTmp[5] == '-' && iTmp == 10)
         {
            strcpy(&acTmp[5], &acTmp[6]);
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);
         } else if (iTmp > 9)
            memcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);         
         else
            vmemcpy(pOutbuf+OFF_M_ZIP, acTmp, 9);   
      } else
         iTmp = 0;

      if (iTmp == 9)
         sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP], apTokens[MB_ROLL_M_ZIP]+5);
      else
         sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);
      iTmp = blankRem(acTmp);
      if (iTmp > SIZ_M_CTY_ST_D && acTmp[iTmp-5] == '-')
         acTmp[iTmp-5] = 0;
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D);
   }
}

void Kin_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001463020", 9) )
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      return;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      iTmp = blankRem(acAddr1, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, iTmp);

      // 07/18/2016
      parseMAdr1(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      } else
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   strcpy(acAddr2, p2);
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);

      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}

/******************************** Kin_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Kin_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSitus);

      // Get first rec
      pRec = fgets(acRec, 512, fdSitus);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   replNull(acRec, ' ');

   // Parse input
   iRet = ParseStringIQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Ignore situs starts with LOT#
   if (*apTokens[MB_SITUS_STRNUM] == 'L')
      return 1;

   // Clear old Situs
   removeSitus(pOutbuf);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "020087005000", 9))
   //   lTmp = 0;
#endif

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);

      // Special case - remove garbage
      if (pTmp = strstr(apTokens[MB_SITUS_STRNUM], " 1-"))
      {
          LogMsg("*** Drop trailing str number(1): %s [%s]", apTokens[MB_SITUS_STRNUM], apTokens[MB_SITUS_ASMT]);
         *pTmp = 0;
      }
      strcpy(acAddr1, apTokens[MB_SITUS_STRNUM]);
      iTmp = blankRem(acAddr1);

      // Save original StrNum
      vmemcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_HSENO, iTmp);

      strcat(acAddr1, " ");
      if (*apTokens[MB_SITUS_STRDIR] > ' ' && isDir(apTokens[MB_SITUS_STRDIR]))
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         vmemcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], SIZ_S_DIR);
      }
   }

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);
      vmemcpy(pOutbuf+OFF_S_STREET, apTokens[MB_SITUS_STRNAME], SIZ_S_STREET);

      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
         sprintf(acCode, "%d      ", iTmp);
      else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apTokens[MB_SITUS_STRNAME]);
      if (sAdr.strName[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
         if (sAdr.strSfx[0] > ' ')
            vmemcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, SIZ_S_SUFF);

         strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      }
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNO);
   }

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1, pOutbuf);   
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      // Zip
      lTmp = atol(apTokens[MB_SITUS_ZIP]);
      if (lTmp > 90000 && lTmp < 99999)
         memcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], 5);

      if (acAddr1[0] > ' ')
         sprintf(acTmp, "%s, CA %.5s", myTrim(acAddr1), pOutbuf+OFF_S_ZIP);
      else
         acTmp[0] = 0;

      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int Kin_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "020087005000", 9))
   //   acTmp[0] = 0;
#endif
   // 
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   // Save original StrNum
   vmemcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.HseNo, SIZ_S_HSENO);

   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/******************************** Kin_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Kin_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg("Skip Sale rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   while (!iTmp)
   {
      // Replace tab char with 0
      if (pRec)
         iRet = ParseStringNQ(pRec, ',', MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "001015008", 9))
      //   iRet = 0;
#endif

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         /* Do not use Confidential Sale Price
         } else
         {
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            }
         */
         }

         // DocType - need translation before production
         // 01(grant deed), 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         // 0F (Trustee deed)
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         MB_MergeSale(&sCurSale, pOutbuf, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Kin_MergeChar ******************************
 *
 * Note: need code table for Heating and Colling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - A parcel may have multiple entries in CHAR file.  If LandUseCat > 1,
 *     Add all of them together.  Otherwise, use the first one.
 *     For now, just use the first one found.
 *
 *****************************************************************************/

int Kin_MergeStdChar(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft, lTmp;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath;
   STDCHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 2048, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 2048, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
   {
      *(pOutbuf+OFF_BLDG_CLASS) = ' ';
      *(pOutbuf+OFF_BLDG_QUAL)  = ' ';
      return 1;
   }

   pChar = (STDCHAR *)pRec;

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Yrblt
   if (pChar->YrBlt[0] > '0')
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // YrEff
   if (pChar->YrEff[0] > '0')
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_EFF, BLANK32, SIZ_YR_BLT);

   // LotSqft - value from LotSqft is more accurate than calculate from LotAcres
   long lLotSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lLotSqft > 100)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      lTmp = atoin(pChar->LotAcre, SIZ_CHAR_SQFT);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      } else if (*(pOutbuf+OFF_LOT_ACRES+8) = ' ')
      {
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(double)(lLotSqft*SQFT_MF_1000+0.5));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   } 

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (pChar->Misc.sExtra.DetGarSqft[0] > ' ')
      lGarSqft += atoin(pChar->Misc.sExtra.DetGarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = '2';
   } else
   {
      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = ' ';
   }

   // ParkType
   if (pChar->ParkType[0] > ' ')
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "119161020000", 9))
   //   iTmp = 0;
#endif

   // Heating
   if (pChar->Heating[0] > ' ')
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];

   // Cooling 
   if (pChar->Cooling[0] > ' ')
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Pool - translation table has not been verified
   if (pChar->Pool[0] > ' ')
      *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Fireplace
   memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);
   if (pChar->Fireplace[0] > ' ')
      *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];
   
   // HasSeptic or HasSewer
   if (pChar->HasSewer > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // HasWater/HasWell
   if (pChar->HasWater > '0')
      *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Units
   iTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (iTmp > 0 && iTmp < 1000)
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   } else
      memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

   // Rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Impr cond
   if (pChar->ImprCond[0] > ' ')
      *(pOutbuf+OFF_IMPR_COND) = pChar->ImprCond[0];
   else
      *(pOutbuf+OFF_IMPR_COND) = ' ';

   // Stories
   iTmp = atoin(pChar->Stories, SIZ_CHAR_SIZE4);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d.0 ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   } else
      memcpy(pOutbuf+OFF_STORIES, BLANK32, SIZ_STORIES);

   // Get next Char rec
   pRec = fgets(acRec, 2048, fdChar);
   lCharMatch++;

   return 0;
}

/********************************* Kin_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired/unsecured record, not use
 *
 *****************************************************************************/

int Kin_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

#ifdef _DEBUG
   //f (!memcmp(pOutbuf, "001016008000", 12))
   //   iTmp = 0;
#endif

   // Remove NULL char
   replNull(pRollRec, ' ');

   // Replace tab char with 0
   iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_PPMOBILHOME)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   // If book >= 800, only keep 907, 910, & 915
   iTmp = atoin(apTokens[iApnFld], 3);
   if (iTmp >= 800 && iTmp != 910)
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "16KIN", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   _strupr(apTokens[MB_ROLL_LEGAL]);
   remUnPrtChar(apTokens[MB_ROLL_LEGAL]);
   quoteRem(apTokens[MB_ROLL_LEGAL]);
   updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);

   // Zoning
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   if (acTmp[0] > ' ')
   {
      // Make uppercase
      _strupr(acTmp);
      if (acTmp[2] > 'Z')
         acTmp[2] = 0;
      else
         acTmp[SIZ_USE_CO] = 0;

      iTmp = strlen(acTmp);
      vmemcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001144001000", 12))
   //   lTmp = 0;
#endif

   // Recorded Doc
   // Not consistent with sale file - use only when matched
   if (*apTokens[MB_ROLL_DOCNUM] > '0' && *apTokens[MB_ROLL_DOCDATE] > ' ' && *(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
   {
      char *pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp && isNumber(apTokens[MB_ROLL_DOCNUM]+5, 7))
      {
         memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM]+5, 7);
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
      }
   } 

   // Owner
   try {
      Kin_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER], apTokens[MB_ROLL_CAREOF], apTokens[MB_ROLL_TAXABILITY]);
   } catch(...) {
      LogMsg("***** Execption occured in Kin_MergeOwner()");
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "010271017000", 10))
   //   iRet = 0;
#endif

   // Mailing
   try {
      if (*apTokens[MB_ROLL_M_ADDR] > ' ')
         Kin_MergeMAdr(pOutbuf);
      else if (*apTokens[MB_ROLL_M_ADDR1] > ' ')
         Kin_MergeMAdr(pOutbuf, apTokens[MB_ROLL_M_ADDR1], apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3], apTokens[MB_ROLL_M_ADDR4]);

   } catch(...) {
      LogMsg("***** Execption occured in Kin_MergeMAdr()");
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************* Kin_Load_Roll ******************************
 *
 * Loading roll file.
 *
 ******************************************************************************/

int Kin_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   lLastFileDate = getFileDate(acRollFile);

   // Sort roll file
   sprintf(acRec, "%s\\%s\\Roll.srt", acTmpPath, myCounty.acCntyCode);
   iRet = sortFile(acRollFile, acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\")");
   if (!iRet)
      return -2;
   strcpy(acRollFile, acRec);

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }
   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Exe file
   LogMsg("Open Exe file %s", acExeFile);
   fdExe = fopen(acExeFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
      return -2;
   }

   // Sort tax file
   sprintf(acRec, "%s\\%s\\Tax.srt", acTmpPath, myCounty.acCntyCode);
   iRet = sortFile(acTaxFile, acRec, "S(#1,C,A) B(213,R)");
   if (!iRet)
      return -9;
   strcpy(acTaxFile, acRec);

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }

   // Open lien file
   fdLienExt = NULL;
   //sprintf(acRec, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //if (!_access(acRec, 0))
   //{
   //   LogMsg("Open Lien file %s", acRec);
   //   fdLienExt = fopen(acRec, "r");
   //   if (fdLienExt == NULL)
   //   {
   //      LogMsg("***** Error opening lien file: %s\n", acRec);
   //      return -7;
   //   }
   //}

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
      iTmp = memcmp(acBuf, (char *)&acRollRec[0], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Kin_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Kin_MergeSitus(acBuf);

            // Merge Lien
            //lRet = 1;
            //if (fdLienExt)
            //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

            // Merge Exe if not found in LienExt
            //if (fdExe && lRet)
            //   lRet = MB_MergeExe(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Kin_MergeStdChar(acBuf);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Kin_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Kin_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Kin_MergeStdChar(acRec);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTax(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Kin_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Kin_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);
         else
            acRec[OFF_HO_FL] = '2';

         // Merge Char
         if (fdChar)
            lRet = Kin_MergeStdChar(acRec);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTax(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);

         iNewRec++;
      }
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u\n", lCnt);
   return 0;
}

/********************************* Kin_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Kin_MergeLienL1(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256];
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

#ifdef _DEBUG
//   if (!memcmp(pRollRec, "011061007000", 12))
//      iTmp = 0;
#endif

   // Replace null char with blank
   iRet = replNull(pRollRec, ' ', 0);

   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L1_USERID)
   {
      LogMsg("***** Kin_MergeLienL1: bad input record for APN=%s", apTokens[L1_ASMT]);
      return -1;
   }

   // If book >= 800, only keep 906 & 910
   iTmp = atoin(apTokens[L1_ASMT], 3);
   if (iTmp >= 800 && iTmp != 906 && iTmp != 910)
      return 1;

   // Check tax year
   lTmp = atol(apTokens[L1_TAXYEAR]);
   if (lTmp != lLienYear)
      return 1;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L1_ASMT], strlen(apTokens[L1_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L1_FEEPARCEL], strlen(apTokens[L1_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L1_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L1_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "16KINA", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values - Land
   ULONG lLand = atoi(apTokens[L1_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   ULONG lImpr = atoi(apTokens[L1_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow = atoi(apTokens[L1_CURRENTGROWINGIMPRVALUE]);
   long lFixt   = atoi(apTokens[L1_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L1_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[L1_CURRENTPERSONALPROPMHVALUE]);
   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         iTmp = sprintf(acTmp, "%u", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, iTmp);
      }
      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%u", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, iTmp);
      }
      if (lPP > 0)
      {
         iTmp = sprintf(acTmp, "%u", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, iTmp);
      }
      if (lMH > 0)
      {
         iTmp = sprintf(acTmp, "%u", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L1_TAXAMT1]);
   double dTax2 = atof(apTokens[L1_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } 

   // Exemption
   long lExe1 = atol(apTokens[L1_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L1_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L1_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L1_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L1_EXEMPTIONCODE1], strlen(apTokens[L1_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L1_EXEMPTIONCODE2], strlen(apTokens[L1_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L1_EXEMPTIONCODE3], strlen(apTokens[L1_EXEMPTIONCODE3]));

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L1_TRA], strlen(apTokens[L1_TRA]));

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L1_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "119161020", 9))
   //   iTmp = 0;
   //strcpy(acTmp, apTokens[L1_PARCELDESCRIPTION]);
#endif
   // Legal
   _strupr(apTokens[L1_PARCELDESCRIPTION]);
   iTmp = updateLegal(pOutbuf, apTokens[L1_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   strcpy(acTmp, apTokens[L1_USECODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   }

   // Acres - do not use sizeacresfttype since it's not reliable
   dTmp = atof(apTokens[L1_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Kin_MergeOwner(pOutbuf, apTokens[L1_OWNER], "", "");

   // Situs
   Kin_MergeSitus(pOutbuf, apTokens[L1_SITUS1], apTokens[L1_SITUS2]);

   // Mailing
   Kin_MergeMAdr(pOutbuf, apTokens[L1_MAILADDRESS1], apTokens[L1_MAILADDRESS2], apTokens[L1_MAILADDRESS3], apTokens[L1_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L1_TAXABILITY], true, true);

   return 0;
}

int Kin_MergeLienL2(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

#ifdef _DEBUG
//   if (!memcmp(pRollRec, "011061007000", 12))
//      iTmp = 0;
#endif

   // Replace null char with blank
   iRet = replNull(pRollRec, ' ', 0);

   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L2_DTS)
   {
      LogMsg("***** Kin_MergeLienL2: bad input record for APN=%s", apTokens[L2_ASMT]);
      return -1;
   }

   // If book >= 800, only keep 907, 910, & 915
   iTmp = atoin(apTokens[L2_ASMT], 3);
   if (iTmp >= 800 && iTmp != 907 && iTmp != 910 && iTmp != 915 && iTmp != 935 && iTmp != 940)
      return 1;

   // Check tax year
   lTmp = atol(apTokens[L2_TAXYEAR]);
   if (lTmp != lLienYear)
      return 1;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L2_ASMT], strlen(apTokens[L2_ASMT]));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L2_FEEPARCEL], strlen(apTokens[L2_FEEPARCEL]));

   // Format APN
   iRet = formatApn(apTokens[L2_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L2_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "16KIN", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values - Land
   long lLand = atoi(apTokens[L2_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L2_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow = atoi(apTokens[L2_CURRENTGROWINGIMPRVALUE]);
   long lFixt   = atoi(apTokens[L2_CURRENTFIXEDIMPRVALUE]);
   long lPP   = atoi(apTokens[L2_CURRENTPERSONALPROPVALUE]);
   long lMH   = atoi(apTokens[L2_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   double dTax1 = atof(apTokens[L2_TAXAMT1]);
   double dTax2 = atof(apTokens[L2_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Exemption
   long lExe1 = atol(apTokens[L2_EXEMPTIONAMT1]);
   long lExe2 = atol(apTokens[L2_EXEMPTIONAMT2]);
   long lExe3 = atol(apTokens[L2_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  
   if (!memcmp(apTokens[L2_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L2_EXEMPTIONCODE1], strlen(apTokens[L2_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L2_EXEMPTIONCODE2], strlen(apTokens[L2_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L2_EXEMPTIONCODE3], strlen(apTokens[L2_EXEMPTIONCODE3]));

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L2_TRA], strlen(apTokens[L2_TRA]));

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L2_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030330020", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L2_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   strcpy(acTmp, apTokens[L2_USECODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres - do not use sizeacresfttype since it's not reliable
   dTmp = atof(apTokens[L2_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Kin_MergeOwner(pOutbuf, apTokens[L2_OWNER], "", "");

   // Situs
   //Kin_MergeSitus(pOutbuf, apTokens[L2_SITUS1], apTokens[L2_SITUS2]);

   // Mailing
   Kin_MergeMAdr(pOutbuf, apTokens[L2_MAILADDRESS1], apTokens[L2_MAILADDRESS2], apTokens[L2_MAILADDRESS3], apTokens[L2_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L2_TAXABILITY], true, true);

   return 0;
}

/******************************* Kin_MergeLienL3 *****************************
 *
 * For 2023 LDR AGENCYCDCURRSEC_TR601.TAB
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Kin_MergeLienL3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_ISAGPRESERVE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // APN
   vmemcpy(pOutbuf, apTokens[L3_ASMT], iApnLen);

   // Format APN
   iRet = formatApn(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Copy ALT_APN
   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], SIZ_ALT_APN);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L3_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "16KIN", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L3_TRA], strlen(apTokens[L3_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing Impr, Fixture, PersProp, PPMH
   long lFixtr = atoi(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= atoi(apTokens[L3_FIXTURESRP]);
   long lGrow  = atoi(apTokens[L3_GROWING]);
   long lPers  = atoi(apTokens[L3_PPVALUE]);
   long lPP_MH = atoi(apTokens[L3_MHPPVALUE]);
   lTmp = lFixtr+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L3_HOX]);
   long lExe2 = atol(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      *(pOutbuf+OFF_EXE_CD1) = 'H';
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
      vmemcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], SIZ_EXE_CD1);

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&KIN_Exemption);

   // Legal
   remChar(apTokens[L3_PARCELDESCRIPTION], '"');
   updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);

   // UseCode
   if (*apTokens[L3_LANDUSE1] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO);

      // Std Usecode
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   lTmp = atol(apTokens[L3_LANDSIZE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(lTmp*SQFT_MF_1000);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*apTokens[L3_ISAGPRESERVE] == '1')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   remChar(apTokens[L3_OWNER], '"');
   Kin_MergeOwner3(pOutbuf, apTokens[L3_OWNER]);

   // Mailing
   remChar(apTokens[L3_MAILADDRESS1], '"');
   remChar(apTokens[L3_MAILADDRESS2], '"');
   remChar(apTokens[L3_MAILADDRESS3], '"');
   Kin_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Recorded Doc
   if (*apTokens[L3_CURRENTDOCNUM] > '0' && *(apTokens[L3_CURRENTDOCNUM]+4) == 'R')
   {
      pTmp = dateConversion(apTokens[L3_CURRENTDOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp && !memcmp(apTokens[L3_CURRENTDOCNUM], pTmp, 4))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         iTmp = sprintf(acTmp, "%.5s%.7d", apTokens[L3_CURRENTDOCNUM], atol(apTokens[L3_CURRENTDOCNUM]+5));
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, iTmp);
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // Garage size
   dTmp = atof(apTokens[L3_GARAGESIZE]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, dTmp);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';                 // GARAGE
   }

   // Number of parking spaces
   if (*apTokens[L3_GARAGE] > '0' && *apTokens[L3_GARAGE] <= '9')
   {
      iTmp = atol(apTokens[L3_GARAGE]);
      sprintf(acTmp, "%d", iTmp);
      vmemcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';                 // Garage
   } else
   {
      if (*(apTokens[L3_GARAGE]) == 'C')
         *(pOutbuf+OFF_PARK_TYPE) = 'C';              // Carport
      else if (*(apTokens[L3_GARAGE]) == 'A')
      {
         *(pOutbuf+OFF_PARK_TYPE) = 'A';              // Attached
         if (*(apTokens[L3_GARAGE]+1) > '0' && *(apTokens[L3_GARAGE]+1) <= '9')
            *(pOutbuf+OFF_PARK_SPACE) = *(apTokens[L3_GARAGE]+1);
      } else if (*(apTokens[L3_GARAGE]) == 'D')
      {
         *(pOutbuf+OFF_PARK_TYPE) = 'D';              // Dettached
         if (*(apTokens[L3_GARAGE]+1) > '0' && *(apTokens[L3_GARAGE]+1) <= '9')
            *(pOutbuf+OFF_PARK_SPACE) = *(apTokens[L3_GARAGE]+1);
      } else if (*(apTokens[L3_GARAGE]) == 'Z')
         *(pOutbuf+OFF_PARK_TYPE) = '4';              // Other
      else if (*(apTokens[L3_GARAGE]) == '0')
         *(pOutbuf+OFF_PARK_TYPE) = 'H';              // None
   }

   // YearBlt
   lTmp = atol(apTokens[L3_YEARBUILT]);
   if (lTmp > 1800 && lTmp <= lToyear)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, iTmp);
   }

   // Total rooms
   iTmp = atol(apTokens[L3_TOTALROOMS]);
   if (iTmp > 0 && iTmp < 1000)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Stories
   dTmp = atof(apTokens[L3_STORIES]);
   if (dTmp > 0 && dTmp < 10)
   {
      sprintf(acTmp, "%d.0", dTmp);
      vmemcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   // Units
   iTmp = atol(apTokens[L3_UNITS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d", iTmp);
      vmemcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Beds
   iTmp = atol(apTokens[L3_BEDROOMS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths
   iTmp = atol(apTokens[L3_BATHS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iTmp);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   }

   // HBaths
   iTmp = atol(apTokens[L3_HALFBATHS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iTmp);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   }

   //// Heating
   //int iCmp;
   //if (*apTokens[L3_HEATING] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asHeating[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_HEATING], asHeating_LDR[iTmp].acSrc, asHeating_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_HEAT) = asHeating_LDR[iTmp].acCode[0];
   //}

   //// Cooling
   //if (*apTokens[L3_AC] == 'C')
   //   *(pOutbuf+OFF_AIR_COND) = 'C';
   //else if (*apTokens[L3_AC] > ' ')
   //   LogMsg("*** Unknown A/C: %s", apTokens[L3_AC]);

   //// Pool/Spa
   //if (*apTokens[L3_POOLSPA] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_POOLSPA], asPool_LDR[iTmp].acSrc, asPool_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_POOL) = asPool_LDR[iTmp].acCode[0];
   //}

   //// Fire place
   //if (*apTokens[L3_FIREPLACE] > ' ')
   //{
   //   iTmp = 0;
   //   iCmp = -1;
   //   pTmp = _strupr(apTokens[L3_FIREPLACE]);
   //   while (asPool[iTmp].iLen > 0 && (iCmp=memcmp(apTokens[L3_FIREPLACE], asFP_LDR[iTmp].acSrc, asFP_LDR[iTmp].iLen)) > 0)
   //      iTmp++;

   //   if (!iCmp)
   //      *(pOutbuf+OFF_FIRE_PL) = asPool[iTmp].acCode[0];
   //}

   //// Quality Class
   //acTmp1[0] = 0;
   //if (*apTokens[L3_QUALITYCLASS] > '0' || strlen(apTokens[L3_QUALITYCLASS]) > 1)
   //{
   //   strcpy(acTmp, apTokens[L3_QUALITYCLASS]);
   //   remCharEx(acTmp, " ,'?");
   //   pTmp = _strupr(acTmp);

   //   //memcpy(myCharRec.QualityClass, pTmp, strlen(pTmp));
   //   if (acTmp[0] == 'A' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'A';
   //   else if (acTmp[0] == 'F' && !isdigit(acTmp[1]))
   //      acTmp1[0] = 'F';
   //   else if (!memcmp(pTmp, "POOR", 4) || !memcmp(pTmp, "LOW", 3))
   //      acTmp1[0] = 'P';
   //   else if (*pTmp == 'G')
   //      acTmp1[0] = 'G';
   //   else if (isalpha(*pTmp))
   //   {
   //      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
   //      if (isdigit(acTmp[1]))
   //         iRet = Quality2Code(&acTmp[1], acTmp1, NULL);
   //      else if (isdigit(acTmp[2]))
   //         iRet = Quality2Code(&acTmp[2], acTmp1, NULL);
   //      else if (isalpha(acTmp[1]))
   //      {
   //         switch (acTmp[1])
   //         {
   //            case 'L':
   //            case 'P':
   //               acTmp1[0] = 'P';
   //               break;
   //            case 'A':
   //               acTmp1[0] = 'A';
   //               break;
   //            case 'F':
   //               acTmp1[0] = 'F';
   //               break;
   //            case 'G':
   //               acTmp1[0] = 'G';
   //               break;
   //         }
   //      }
   //   } else if (isdigit(*pTmp))
   //   {
   //      iTmp = atol(pTmp);
   //      if (iTmp < 100)
   //         iRet = Quality2Code(pTmp, acTmp1, NULL);
   //   }

   //   if (acTmp1[0] > '0')
   //      *(pOutbuf+OFF_BLDG_QUAL) = acTmp1[0];
   //}


   return 0;
}

/***************************** Kin_Load_LDR() *******************************
 *
 * Load LDR into 1900-byte record.
 *
 ****************************************************************************/

int Kin_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;
   HANDLE   fhOut;

   // Open roll file
   LogMsg("Open LDR file %s", acRollFile);
   fdLDR = fopen(acRollFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acRollFile);
      return -2;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   //LogMsg("Open Situs file %s", acSitusFile);
   //fdSitus = fopen(acSitusFile, "r");
   //if (fdSitus == NULL)
   //{
   //   LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
   //   return -2;
   //}
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acOutFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acRec, "S(#1,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\") DEL(%d) ", cDelim);
   lRet = sortFile(acSitusFile, acOutFile, acRec);
   fdSitus = fopen(acOutFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acOutFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLDR))
   {
      // Create new R01 record
      switch (iLdrGrp)
      {
         case 1:
            iRet = Kin_MergeLienL1(acBuf, acRec);
            break;
         case 2:
            iRet = Kin_MergeLienL2(acBuf, acRec);
            break;
         case 3:
            iRet = Kin_MergeLienL3(acBuf, acRec);
            break;
         default:
            LogMsg("***** Invalid LDR group %s.  Please check INI file.", iLdrGrp);
            iRet = -1;
            break;
      }

      if (!iRet)
      {
#ifdef _DEBUG
         //if (!memcmp(acBuf, "117333018000", 9))
         //   iRet = 0;
#endif

         // Merge Char
         if (fdChar)
            lRet = Kin_MergeStdChar(acBuf);

         // Merge Situs
         if (fdSitus)
            lRet = Kin_MergeSitus(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else if (bDebug)
         LogMsg("*** Skip %s", acRec);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp || acRec[1] > '9')
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************** Kin_MergeLien1 *****************************
 *
 * Use this function to load 2014 LDR roll.
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

//int Kin_MergeLien1(char *pOutbuf, char *pRollRec)
//{
//   char     acTmp[256], acTmp1[256];
//   long     lTmp;
//   double   dTmp;
//   int      iRet=0, iTmp;
//
//#ifdef _DEBUG
////   if (!memcmp(pRollRec, "011061007000", 12))
////      iTmp = 0;
//#endif
//
//   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
//   if (iRet < KIN_L_TRANSTAX)
//   {
//      LogMsg("***** Error: bad input record for APN=%s", pRollRec);
//      return -1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//
//   // Start copying data
//   memcpy(pOutbuf, apTokens[KIN_L_ASMT], strlen(apTokens[KIN_L_ASMT]));
//
//   // Copy ALT_APN
//   memcpy(pOutbuf+OFF_ALT_APN, apTokens[KIN_L_FEEPARCEL], strlen(apTokens[KIN_L_FEEPARCEL]));
//
//   // Format APN
//   iRet = formatApn(apTokens[KIN_L_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//   // Create MapLink and output new record
//   iRet = formatMapLink(apTokens[KIN_L_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // Create index map link
//   if (getIndexPage(acTmp, acTmp1, &myCounty))
//      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//   // County code
//   memcpy(pOutbuf+OFF_CO_NUM, "27MON", 5);
//
//   // Status
//   *(pOutbuf+OFF_STATUS) = 'A';
//
//   // TRA
//   memcpy(pOutbuf+OFF_TRA, apTokens[KIN_L_TRA], strlen(apTokens[KIN_L_TRA]));
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // Lien values - Land
//   long lLand = atoi(apTokens[KIN_L_LAND]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = atoi(apTokens[KIN_L_IMP]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: Growing, Fixture, PP, PPMH
//   long lPP   = atoi(apTokens[KIN_L_PERSPROP]);
//   if (lPP > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_OTHER, lPP);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//      iTmp = sprintf(acTmp, "%d", lPP);
//      memcpy(pOutbuf+OFF_PERSPROP, acTmp, iTmp);
//   }
//
//   // Gross total
//   lTmp = lPP + lLand + lImpr;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "030330020", 9))
//   //   iTmp = 0;
//#endif
//
//   // UseCode
//   strcpy(acTmp, apTokens[KIN_L_LANDUSE1]);
//   acTmp[SIZ_USE_CO] = 0;
//
//   // Standard UseCode
//   if (acTmp[0] > ' ')
//   {
//      _strupr(acTmp);
//      iTmp = strlen(acTmp);
//      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
//      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
//   }
//
//   // Acres - do not use sizeacresfttype since it's not reliable
//   dTmp = atof(apTokens[KIN_L_ACRES]);
//   if (dTmp > 0.0)
//   {
//      // Lot Sqft
//      lTmp = (long)(dTmp * SQFT_PER_ACRE);
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      // Format Acres
//      lTmp = (long)(dTmp * ACRES_FACTOR);
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   // Owner
//   //Kin_MergeOwner(pOutbuf, apTokens[KIN_L_ASSESSEE], "");
//
//   // Mailing
//   Kin_MergeMAdr(pOutbuf, apTokens[KIN_L_ADDRESS1], apTokens[KIN_L_ADDRESS2], apTokens[KIN_L_ADDRESS3], apTokens[KIN_L_ADDRESS4]);
//
//   return 0;
//}
//
//int Kin_Load_LDR1(int iFirstRec /* 1=create header rec */)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
//   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
//
//   HANDLE   fhOut;
//
//   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
//   DWORD    nBytesWritten;
//   BOOL     bRet, bEof;
//   long     lRet=0, lCnt=0;
//
//   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//
//   // Open LDR roll file
//   LogMsg("Open LDR Roll file %s", acRollFile);
//   fdLDR = fopen(acRollFile, "r");
//   if (fdLDR == NULL)
//   {
//      LogMsg("***** Error opening LDR roll file: %s\n", acRollFile);
//      return -1;
//   }  
//
//   // Use update roll to populate Taxability, Prop8, and Zoning
//   GetIniString(myCounty.acCntyCode, "RollFile", "", acTmpFile, _MAX_PATH, acIniFile);
//   LogMsg("Open update roll file %s", acTmpFile);
//   fdRoll = fopen(acTmpFile, "r");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
//      return -2;
//   }  
//
//   // Open Char file
//   LogMsg("Open Char file %s", acCChrFile);
//   fdChar = fopen(acCChrFile, "r");
//   if (fdChar == NULL)
//   {
//      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
//      return -2;
//   }
//
//   // Open Exe file
//   LogMsg("Open Exe file %s", acExeFile);
//   fdExe = fopen(acExeFile, "r");
//   if (fdExe == NULL)
//   {
//      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
//      return -2;
//   }
//
//   // Open Situs file
//   LogMsg("Open Situs file %s", acSitusFile);
//   fdSitus = fopen(acSitusFile, "r");
//   if (fdSitus == NULL)
//   {
//      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
//      return -2;
//   }
//
//   // Open Output file
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error opening output file: %s\n", acOutFile);
//      return -4;
//   }
//
//   // Output first header record
//   if (iFirstRec > 0)
//   {
//      memset(acBuf, '9', iRecLen);
//      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//   }
//
//   // Get 1st rec
//   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
//   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
//   bEof = (pTmp ? false:true);
//
//   // Init variables
//   iNoMatch=iBadCity=iBadSuffix=0;
//
//   // Merge loop 
//   while (!feof(fdLDR))
//   {
//      lLDRRecCount++;
//
//      // Create new R01 record
//      iRet = Kin_MergeLien1(acBuf, acRec);
//      if (!iRet)
//      {
//         // Merge Exe
//         if (fdExe)
//            lRet = Kin_MergeExe(acBuf, iHdrRows);
//
//         // Merge Char
//         if (fdChar)
//            lRet = Kin_MergeStdChar(acBuf);
//
//         // Merge Situs
//         if (fdSitus)
//            lRet = Kin_MergeSitus(acBuf);
//
//         // Merge misc fields from roll update
//         if (fdRoll)
//            lRet = Kin_MergeMisc(acBuf);
//
//         // Save last recording date
//         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
//         if (lRet > lLastRecDate && lRet < lToday)
//            lLastRecDate = lRet;
//
//         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//         if (!(++lCnt % 1000))
//            printf("\r%u", lCnt);
//
//         if (!bRet)
//         {
//            LogMsg("***** Error writing to output file at record %d\n", lCnt);
//            lRet = WRITE_ERR;
//            break;
//         }
//      }
//
//      // Get next roll record
//      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
//      if (!pTmp || acRec[1] > '9')
//         break;
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fdExe)
//      fclose(fdExe);
//   if (fdChar)
//      fclose(fdChar);
//   if (fdLDR)
//      fclose(fdLDR);
//   if (fdSitus)
//      fclose(fdSitus);
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   LogMsg("Total input records:        %u", lLDRRecCount);
//   LogMsg("Total output records:       %u", lCnt);
//   LogMsg("Total bad-city records:     %u", iBadCity);
//   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
//   LogMsg("Number of Situs matched:    %u", lSitusMatch);
//   LogMsg("Number of Char matched:     %u", lCharMatch);
//   LogMsg("Number of Zone matched:     %u", lZoneMatch);
//   LogMsg("Number of Exe matched:      %u\n", lExeMatch);
//
//   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
//   LogMsg("Number of Char skiped:      %u", lCharSkip);
//   LogMsg("Number of Zone skiped:      %u", lZoneSkip);
//   LogMsg("Number of Exe skiped:       %u\n", lExeSkip);
//   printf("\nTotal output records: %u\n", lCnt);
//
//   lRecCnt = lCnt;
//   return 0;
//}

/***************************** Kin_FormatDocNum ******************************
 *
 *  Before 07/01/1997:       199733840743  --> 199733840743
 *  09/01/1997 - 12/31/1999: 20009940022   --> 1999040022
 *  01/01/2000 - 12/31/2003: 200403129137  --> 20030129137
 *  01/01/2004 - present:    2004052194    --> 20040052194
 *
 * Return true if DocNum formatted ok
 *
 *****************************************************************************/

bool Kin_FormatDocNum(char *pResult, char *pDocNum, char *pDocDate, char *pApn)
{
   char  acTmp[32], acDocNum[32];
   int   iLen, lTmp;
   bool  bRet = true;

#ifdef _DEBUG
   //if (!memcmp(pDocNum, "20021081138", 11))
   //   lTmp = 0;
   //if (!memcmp(pApn, "012622060000", 10))
   //   lTmp = 0;
   
#endif

   // Ignore internal doc
   if ((*(pDocNum+4) == 'I') || isalpha(*(pDocNum+5)) )
      return false;

   memset(acDocNum, ' ', SALE_SIZ_DOCNUM);
   myTrim(pDocNum);
   iLen = strlen(pDocNum);
   lTmp = atoin(pDocDate, 6);
   if (lTmp > 200312)
   {
      // 2006070517     --> 2006070517
      lTmp = atol(pDocNum+4);
      sprintf(acDocNum, "%.4s%.6d   ", pDocNum, lTmp);
   } else if (lTmp > 199912)
   {
      // 2002R1026159   --> 2001026159
      // 200100077852   --> 2000077852
      if (iLen > 10)
      {
         lTmp = atol(pDocNum+6);
         if (!memcmp(pDocNum+4, pDocDate+2, 2) || (*(pDocNum+4) == 'R' && *(pDocNum+5) == *(pDocDate+3)) )
            sprintf(acDocNum, "%.4s%.6d   ", pDocDate, lTmp);
         else if (memcmp(pDocNum, pDocDate, 4))
            memcpy(acDocNum, pDocNum, iLen);
         else
            sprintf(acDocNum, "%.4s%.6d   ", pDocDate, lTmp);
      } else 
      {
         lTmp = atol(pDocNum+4);
         if (lTmp > 0)
            sprintf(acDocNum, "%.4s%.6d   ", pDocNum, lTmp);
         else
            bRet = false;
      }
      if (memcmp(pDocNum, pDocDate, 3))
         LogMsg("*** DocNum=%s, DocDate=%s", pDocNum, pDocDate);
   } else if (lTmp > 199708)
   {
      // ? 2004023247 - 19981205
      // 20009940022  - 19990525
      // 199835651501 - 19970829
      if (iLen == 12)
      {
         lTmp = atol(pDocNum+7);    // 199909854296
      } else if (iLen == 11)
         lTmp = atol(pDocNum+6);
      else if (iLen == 7)
         lTmp = atol(pDocNum+2);    // 9856650-19980825
      else
         lTmp = atol(pDocNum+4);    // 999808809-19980218

      if (*pDocNum > '2' || 
         !memcmp(pDocNum+4, pDocDate+2, 2) ||
         (iLen == 12 && !memcmp(pDocNum+5, pDocDate+2, 2)) )
         sprintf(acDocNum, "%.4s%.6d   ", pDocDate, lTmp);     // 9998123456
      else
         sprintf(acDocNum, "%.4s%.6d   ", pDocNum, lTmp);
   } else if (lTmp > 199612)
   {
      // 199835651501 - 19970829
      if (iLen == 12)
      {
         lTmp = 1;
         memcpy(acDocNum, pDocNum, iLen);
      } else if (iLen == 11)
         lTmp = atol(pDocNum+6);
      else if (iLen == 7)
         lTmp = atol(pDocNum+2);    // 9856650-19980825
      else
         lTmp = atol(pDocNum+4);    // 999808809-19980218

      if (*pDocNum > '2' || 
         !memcmp(pDocNum+4, pDocDate+2, 2) ||
         (iLen == 12 && !memcmp(pDocNum+5, pDocDate+2, 2)) )
         sprintf(acDocNum, "%.4s%.6d   ", pDocDate, lTmp);     // 9998123456
      else if (lTmp != 1)
         sprintf(acDocNum, "%.4s%.6d   ", pDocNum, lTmp);
   } else if (*(pDocNum+4) == 'R')
   {
      // 1968R5520695   --> 196805520695
      // 1972R763 146   --> 197207630146
      strcpy(acTmp, pDocNum);
      myTrim(acTmp);
      replChar(acTmp, ' ', '0');
      lTmp = atol(&acTmp[5]);
      sprintf(acDocNum, "%.4s%.8d ", pDocNum, lTmp);      
      //sprintf(acDocNum, "%.4s%.8d ", pDocDate, lTmp);      
   } else if (isdigit(*(pDocNum+5)) && isdigit(*(pDocNum+6)))
   {
      // 194513367      --> 19450013367
      // 198012650087   --> 198012650087
      strcpy(acTmp, pDocNum);
      myTrim(acTmp);
      lTmp = atol(&acTmp[4]);
      sprintf(acDocNum, "%.4s%.6d   ", pDocNum, lTmp);      
      //sprintf(acDocNum, "%.4s%.6d   ", pDocDate, lTmp);      
   } else
   {
      // These are internal doc, can be ignored
      // 198215PM0058
      // 1994IL062593
      // 1989DOD 1/91
      bRet = false;
   }
   memcpy(pResult, acDocNum, SALE_SIZ_DOCNUM);

   if (bRet && memcmp(acDocNum, pDocDate, 4))
#ifdef _DEBUG
      LogMsg("Questionable Transaction: DocNum=%.12s DocDate=%.8s APN=%.12s, DocCode=%s, XferType=%s", acDocNum, pDocDate, pApn, apTokens[MB_SALES_DOCCODE], apTokens[MB_SALES_XFERTYPE]);
#else
      LogMsg("Questionable Transaction: DocNum=%.12s DocDate=%.8s APN=%.12s", acDocNum, pDocDate, pApn);
#endif
   return bRet;
}

/***************************** Kin_CreateSCSale ******************************
 *
 * Extract sale data from ???_Sales.csv and output to ???_SALE.DAT
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * - DocNum: Drop the first 5 characters and assign the rest to DocNum to make 
 *   it compatible to old sales.
 *
 * DateFmt: 
 *    MM_DD_YYYY_1 (MON)
 *
 * - Copy from Alp_CreateSCSale()
 * - Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Kin_CreateSCSale(int iDateFmt, bool bAppend)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acDocNum[32], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   ULONG    lCnt=0, lPrice, lTmp;

   LogMsg0("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Replace null char with space
      replNull(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MB_SALES_FLDS, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MB_SALES_FLDS, apTokens);
      if (iTokens < MB_SALES_FLDS)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[MB_SALES_ASMT], "004062005000", 9))
      //   iTmp = 0;
#endif

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] == ' ' || *apTokens[MB_SALES_DOCDATE] == ' ')
         continue;

      // Remove YEAR in DocNum
      if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R')
         strcpy(acDocNum, apTokens[MB_SALES_DOCNUM]+5);
      else
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

      // Docnum
      vmemcpy(SaleRec.DocNum, acDocNum, SALE_SIZ_DOCNUM);

      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
      {
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1' || *apTokens[MB_SALES_GROUPSALE] == 'T')
      {
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      }

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "014760037", 9))
      //   iTmp = 0;
#endif
      // Confirmed sale price
      dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
      if (acTmp[0] > '0')
      {
         lPrice = atol(acTmp);
         if (lPrice < 1000)
         {
            // This doesn't look normal, but keep it for reference only
            iTmp = sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
            lPrice = 0;
         } else
            iTmp = sprintf(acTmp, "%*u00", SIZ_SALE1_AMT-2, lPrice/100);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, iTmp);
      } 

      // Do not use confirmed sale price
      if (!bUseConfSalePrice)
         lPrice = 0;

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001160016000", 9))
      //   iTmp = 0;
#endif
      // Tax
      dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
      if (acTmp[0] > '0')
      {
         dTmp = atof(acTmp);

         // Save DocTax
         iTmp = sprintf(acTmp, "%.2f", dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);

         // Calculate sale price
         lTmp = (long)(dTmp * SALE_FACTOR);
         iTmp = ((int)dTmp/100)*100;

         // Check for bad DocTax
         if (dTmp > 100000)
         {
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else if (lPrice > 100000 && lPrice == (lPrice/100)*100)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTmp);
            } else
            {
               LogMsg("??? (1) Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f, CP=%d.  Need investigation.  Ignore price.", SaleRec.Apn, SaleRec.DocNum, dTmp, lPrice);
               lPrice = 0;
            }
         } else if (lPrice > 0 && (long)dTmp >= lPrice+5000)
         {
            // Even though sale tax is questionable, we still rely on it if it is reasonable
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** (2) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               if (lPrice == (lPrice/100)*100)
                  LogMsg("*** (2) Questionable Sale Tax Amt for %.12s (cp=%d \ttax=%.2f).  Use sale price.", SaleRec.Apn, lPrice, dTmp);
               else
               {
                  LogMsg("??? (2) Questionable Sale Tax Amt for %.12s: DocNum=%.12s, Price=%d, \ttax=%.2f).  Need investigation.", SaleRec.Apn, SaleRec.DocNum, lPrice, dTmp);
                  lPrice = lTmp;
               }
            }
         } else if (iTmp == (int)dTmp && iTmp > 10000)
         {
            if (lTmp != (lTmp/100)*100)
            {
               LogMsg("*** (3) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
               lPrice = lTmp;
         } else if (lTmp == (lTmp/100)*100)
            lPrice = lTmp;
         else if (lTmp > 1000 && lPrice == 0)
            lPrice = lTmp;

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Ignore sale price.", 
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE]);
               lPrice = 0;
            }
         }  
      } 

      // Ignore sale price if less than 1000
      if (lPrice >= 10000)
         sprintf(acTmp, "%*u00", SALE_SIZ_SALEPRICE-2, lPrice/100);
      else if (lPrice >= 1000)
         sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
      else
         memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
      memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);

      // Doc code - accept following code only
      if (*apTokens[MB_SALES_DOCCODE] > ' ')
      {
         iTmp = findDocType(apTokens[MB_SALES_DOCCODE], (IDX_TBL5 *)&KIN_DocCode);
         if (iTmp >= 0)
         {
            memcpy(SaleRec.DocType, KIN_DocCode[iTmp].pCode, KIN_DocCode[iTmp].iCodeLen);
            SaleRec.NoneSale_Flg = KIN_DocCode[iTmp].flag;
         } else if (bDebug)
            LogMsg("*** Unknown DocCode: %s", apTokens[MB_SALES_DOCCODE]);
      } else if (lPrice > 1000)
      {
         SaleRec.DocType[0] = '1';
         SaleRec.NoneSale_Flg = 'N';
         SaleRec.SaleCode[0] = 'F';
      } 

      // Save original DocCode
      vmemcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);
      
      // Transfer Type: FV & F%
      if (*apTokens[MB_SALES_XFERTYPE] == 'F' && lPrice > 1000)
         SaleRec.SaleCode[0] = 'F';

      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         strcpy(acTmp, apTokens[MB_SALES_SELLER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Seller1, acTmp, SALE_SIZ_SELLER, iTmp);

         // Buyer
         strcpy(acTmp, apTokens[MB_SALES_BUYER]);
         iTmp = blankRem(acTmp);
         vmemcpy(SaleRec.Name1, acTmp, SALE_SIZ_BUYER, iTmp);

         SaleRec.ARCode  = 'A';
         SaleRec.CRLF[0] = 10;
         SaleRec.CRLF[1] = 0;
         fputs((char *)&SaleRec,fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -2;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            // Save old cumsale file
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);
            iTmp = rename(acCSalFile, acTmpFile);
   
            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -2;
      } else
         iTmp = rename(acOutFile, acCSalFile);

      if (iTmp)
         LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acCSalFile, acTmpFile);
      }
      iTmp = rename(acOutFile, acCSalFile);
   }

   if (iTmp)
   {
      iErrorCnt++;
      if (iTmp == -2)
         LogMsg("***** Error sorting output file");
      else
         LogMsg("***** Error renaming to %s", acCSalFile);
   }

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                          output: %d.", lTmp);
   LogMsg("         Latetest recording date: %d.", lLastRecDate);
   return iTmp;
}

/**************************** Kin_ConvStdChar ********************************
 *
 * Other counties has similar format: MER, MNO, YOL, HUM, MAD, SBT, ALP, EDX, KIN
 *
 *****************************************************************************/

int Kin_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[16], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);

   // Sort input file on ASMT
   sprintf(acTmpFile, "%s\\%s\\%s_char.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort char file %s to %s", pInfile, acTmpFile);
   iRet = sortFile(pInfile, acTmpFile, "S(#23,C,A)");
   if (iRet < 500)
   {
      LogMsg("***** Input file is too small.");
      return 1;
   }

   if (!(fdIn = fopen(acTmpFile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Start loop
   while (!feof(fdIn))
   {
      // Get next record
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec || acBuf[0] > '9')
         break;

      replNull(acBuf);
      iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < MB3_CHAR_FLDS)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[MB3_CHAR_ASMT], strlen(apTokens[MB3_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[MB3_CHAR_FEEPARCEL], strlen(apTokens[MB3_CHAR_FEEPARCEL]));

      // Format APN
      if (*apTokens[MB3_CHAR_ASMT] >= '0')
      {
         iRet = formatApn(apTokens[MB3_CHAR_ASMT], acTmp, &myCounty);
         memcpy(myCharRec.Apn_D, acTmp, iRet);
      } else
      {
         LogMsg("--- No ASMT.  FeeParcel=%s", apTokens[MB3_CHAR_FEEPARCEL]);
         continue;
      }

      // Bldg#
      iTmp = atoi(apTokens[MB3_CHAR_BLDGSEQNUM]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%2d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      } else if (iTmp >= 100)
         LogMsg("*** BldgSeqNo too big: %d", iTmp);

      // Rooms
      iTmp = atoi(apTokens[MB3_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Pool 
      iTmp = blankRem(apTokens[MB3_CHAR_POOLSPA]);
      if (iTmp > 1)
      {
         pRec = findXlatCode(apTokens[MB3_CHAR_POOLSPA], &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "006122003000", 9) || !memcmp(myCharRec.Apn, "002430002000", 9))
      //   iRet = 0;
#endif

      // QualityClass 
      if (*apTokens[MB3_CHAR_QUALITYCLASS] == '=' || !memcmp(apTokens[MB3_CHAR_QUALITYCLASS], "00", 2))
         *apTokens[MB3_CHAR_QUALITYCLASS] = 0;
      else
         vmemcpy(myCharRec.QualityClass, _strupr(apTokens[MB3_CHAR_QUALITYCLASS]), SIZ_CHAR_QCLS);

      int iIdx=0;
      if (*apTokens[MB3_CHAR_QUALITYCLASS] >= '0' && *apTokens[MB3_CHAR_QUALITYCLASS] <= 'Z')
      {
         acCode[0] = ' ';
         strcpy(acTmp, apTokens[MB3_CHAR_QUALITYCLASS]);
         if (isalpha(acTmp[0])) 
         {
            if (acTmp[0] == 'M' || acTmp[0] == 'X')
               iIdx = 1;
            myCharRec.BldgClass = acTmp[iIdx];

            if (isdigit(acTmp[iIdx+1]))
               iRet = Quality2Code((char *)&acTmp[iIdx+1], acCode, NULL);
            else if (isdigit(acTmp[iIdx+2]))
               iRet = Quality2Code((char *)&acTmp[iIdx+2], acCode, NULL);
         } else if (isdigit(acTmp[0]))
            iRet = Quality2Code(acTmp, acCode, NULL);
         else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[MB3_CHAR_QUALITYCLASS], apTokens[MB3_CHAR_ASMT]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      } else if (*apTokens[MB3_CHAR_QUALITYCLASS] > ' ' && *apTokens[MB3_CHAR_QUALITYCLASS] != 'U')
         LogMsg("*** Ignore QUALITYCLASS: '%s' in [%s]", apTokens[MB3_CHAR_QUALITYCLASS], apTokens[MB3_CHAR_ASMT]);

      // YrBlt
      int iYrBlt = atoi(apTokens[MB3_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[MB3_CHAR_EFFYR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[MB3_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[MB3_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // Stories/NumFloors
      iTmp = atoi(apTokens[MB3_CHAR_STORIESCNT]);
      if (iTmp > 0 && iTmp < 99)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[MB3_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[MB3_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport Sqft
      int iCarport = atoi(apTokens[MB3_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Parking spaces - no data
      iTmp = atoi(apTokens[MB3_CHAR_PARKINGSPACES]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.ParkSpace, acTmp, iRet);
      }

      // Patio SF
      iTmp = atoi(apTokens[MB3_CHAR_PATIOSF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // Heating 
      iTmp = blankRem(apTokens[MB3_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[MB3_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 
      
      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[MB3_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[MB3_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[MB3_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[MB3_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Beds
      iTmp = atoi(apTokens[MB3_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[MB3_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
         memcpy(myCharRec.Bath_4Q, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[MB3_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // FirePlace 
      if (*apTokens[MB3_CHAR_FIREPLACE] >= '0' && *apTokens[MB3_CHAR_FIREPLACE] <= '9')
      {
         pRec = findXlatCode(apTokens[MB3_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      } 

      // Haswell - not avail
      blankRem(apTokens[MB3_CHAR_HASWELL]);
      if (*(apTokens[MB3_CHAR_HASWELL]) == '1')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Lot Sqft
      if (iFldCnt >= MB3_CHAR_LOTSQFT)
      {
         iTmp = atoi(apTokens[MB3_CHAR_LOTSQFT]);
         if (iTmp > 1)
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(myCharRec.LotSqft, acTmp, iRet);

            // Lot acres
            double dTmp;
            dTmp = (double)(iTmp*1000)/SQFT_PER_ACRE;
            iTmp = sprintf(acTmp, "%d", (long)(dTmp+0.5));
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Asmt, BldgSeqNum, UnitSeqNum, EffYr desc
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/*********************************** loadMon ********************************
 *
 * Options:
 *    -CKIN -L  load lien
 *    -CKIN -La create assessor data set using lien file
 *    -CKIN -U  load update
 *    -CKIN -Ua update assessor data
 *    -CKIN -G  load GrGr (load grgr files from the county and extract 
 *              Sale_exp.dat and Kin_GrGr.dat for DataEntry)
 *    -CKIN -U -Xsi -G (daily update) 
 *          -Fs clear old sale
 *
 * Notes: This is new version to handle 2007 LDR file.
 *
 ****************************************************************************/

int loadKin(int iSkip)
{
   int   iRet=0;
   char  acLienFile[_MAX_PATH];

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;

   iApnLen = myCounty.iApnLen;

   // Fix GrGr file - one time
   //iRet = FixGrGrDef(acCumGrGr, 0, false);

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      // Load tax base
      iRet = MB_Load_TaxBase(bTaxImport, false, 2, 0);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet = MB_Load_TaxCodeMstr(bTaxImport, 0);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport, 0);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport, 0);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }

      // Exit if load tax only
      if (!iLoadFlag)
         return iRet;
   }

   // Extract lien file
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = MB_ExtrTR601(myCounty.acCntyCode);    

   // Create/Update cum sale file
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Do not use confirmed sale price
      //bUseConfSalePrice = false;

      iRet = Kin_CreateSCSale(MM_DD_YYYY_2, true);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Load CHAR file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      iRet = Kin_ConvStdChar(acCharFile);
      if (iRet < 0)
      {
         LogMsg("***** Error converting Char file %s", acCharFile);
         iRet = -1;
      } else if (!iRet)
         LogMsg("*** WARNING: Char file is empty %s", acCharFile);
      else
         iRet = 0;
   }

   iRet = 0;
   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Get LDR file name
      GetIniString(myCounty.acCntyCode, "LienFile", "", acRollFile, _MAX_PATH, acIniFile);
      sprintf(acLienFile, "%s\\%s\\%s_LIEN.CSV", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      iRet = sortFile(acRollFile, acLienFile, "S(1,12,C,A) ");
      if (iRet > 0)
      {
         iRet = 0;
         strcpy(acRollFile, acLienFile);
         iRet = Kin_Load_LDR(iSkip);
      } else
      {
         iRet = -1;
         LogMsg("***** Error sorting %s", acRollFile);
      }
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Kin_Load_Roll(iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Kin_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   return iRet;
}
