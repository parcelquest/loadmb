/**************************************************************************
 *
 * Notes: 
 *    - Dave request that we should only display sale price based on DocTax, not on
 *      Conf Sale price.  See 133330008, 133360001
 *
 * Revision
 * 01/27/2008 1.10.3.1 Add code to support standard usecode
 * 03/05/2008 1.10.5.1 Add code to output update records.  
 * 08/12/2008 8.3.1    Add function Plu_Load_LDR(), Plu_MergeLien(), Plu_MergeSitus(),
 *                     Plu_MergeMAdr() for TR601 input format.  Rename MergePluRoll()
 *                     to Plu_LoadRoll().
 * 03/30/2009 8.7.1    Use MB_MergeSale() to do sale update instead of MergeSale3() to fix 
 *                     problem that non-sale transaction overwrites sale transaction that  
 *                     occurs in the same day  since we only keep one transaction per day.
 * 05/21/2009 8.7.6    Remove reference to OldApn & NewApn files.  We no longer need them.
 * 07/24/2009 9.1.3    Modify all merge functions to support multiple input format based
 *                     on definition in INI file.
 * 08/03/2009 9.1.4.1  Fix bug in Plu_MergeMAdr() & Plu_MergeRoll() due to new record format.
 * 08/30/2009 9.2.0    Use new ParseStringNQ() that remove leading spaces on returned tokens.
 *                     Also ignore exemption amount >= 99999999.
 * 05/03/2010 9.6.2    Sort input files before processing Plu_LoadRoll(). Data from PLU
 *                     is no longer sorted.
 * 05/26/2010 9.6.4    Modify Plu_MergeSitus() to merge situs zip into R01 file.
 * 05/28/2010 9.6.5    Work around data problem in Situs file where zipcode is in UNIT position
 *                     and state is in zipcode position.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 06/09/2011 10.9.1   Exclude HomeSite from OtherVal.  Replace Plu_MergeExe() with MB_MergeExe(),
 *                     Plu_MergeTax() with MB_MergeTax(), and MB_CreateSale() with MB_CreateSCSale().
 * 07/29/2011 11.2.4   Add S_HSENO.  Use regular situs file to update LDR.
 * 08/15/2011 11.2.6   Modify MergeRoll() and Plu_Load_Roll() to filter out unsecured parcels.
 * 11/11/2011 11.8.5   Add option to load tax file, start with TaxRollInfo.
 * 05/22/2012 11.15.0  Add option to load tax file into tax database. Fix standard UseCode
 *                     by using numeric search instead of string search.
 * 07/18/2012 12.2.2   Fix USE_STD bug in Plu_MergeLien(). Add Plu_MergeZone() to merge Zoning 
 *                     from updated roll for LDR processing. Modify Plu_Load_LDR() to merge Zoning
 *                     and remove merge sale, use ApplyCumSale instead.
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 09/30/2013 13.13.0  Rewrite MergeOwner() to populate ETAL flag and Vesting.
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 10/09/2013 13.16.0  Use MB1_StdChar() to convert new char file format and MB_MergeStdChar()
 *                     to merge it to R01 file. Waiting for heating/cooling xlat tables
 * 10/31/2013 13.18.0  Use MB1_ConvStdChar() instead of Plu_ConvStdChar() since it's the same.
 * 11/06/2013 13.18.1  Add asQual[] and use new versio of MB1_ConvStdChar() and MB_MergeStdChar().
 * 07/04/2015 15.0.3   Add MergePlu.h to support new LDR record layout.  Add Plu_Load_LDR1(), 
 *                     Plu_MergeLien1(), Plu_ExtrLien.  Modify Plu_MergeZone() to include other fields.
 * 07/31/2015 15.2.0   Add DBA to Plu_MergeMAdr()
 * 10/13/2015 15.4.0   Use MB_Load_TaxBase() instead of MB_Load_TaxRollInfo() to import 
 *                     data into Tax_Base & Tax_Owner tables.
 * 11/05/2015 15.5.1   Modify Plu_ConvStdChar() to support new CHAR file.  Clean up Plu_Load_Roll()
 *                     to remove unused code. Add -Xa to load CHAR file.
 * 01/13/2016 15.10.1  Remove old situs in Cal_MergeSitus() before update to avoid remnant from old addr.
 * 08/14/2016 16.3.0   Replace MB_MergeTax() with MB_MergeTaxG2() to load PLU_Tax.csv
 *                     Remove unused function CreatePluRoll()
 * 12/01/2016 16.7.2   Add PatioSqft in Plu_ConvStdChar() and remove unused function Plu_MergeChar()
 * 12/02/2016 16.7.3   Modify Plu_ConvStdChar() to remove extra records with CatType 0.
 * 08/18/2018 18.3.0   Add Plu_LoadZoning & Plu_MergeZoneEx() to add zoning to R01.
 * 11/29/2018 18.7.6   Modify Plu_ConvStdChar() to support new CHAR file. Replace findXlatCode() with
 *                     findXlatCodeA() to search on unsorted list.
 * 02/09/2019 18.10.1  Replace Plu_LoadZoning() with MB_LoadZoning()
 * 07/09/2019 19.0.3   Modify Plu_MergeLien1() to check for APN & FEE_PARCEL without leading '0'.
 * 08/04/2019 19.1.2   Call MB_CreateSCSale() with new PLU_DocCode[] to translate all known DocType.
 * 08/28/2019 19.2.1   Modify Plu_MergeRoll(), Plu_MergeLien(), Plu_MergeLien1() to fix UseCode problem.
 * 03/26/2020 19.8.0.1 Add MergeZoning.h and call MergeZoming() instead of MB_LoadZoning()
 * 06/26/2020 20.0.1   Rename Plu_MergeZone() to Plu_MergeOthers().  Add findExeCode() and  
 *                     modify Plu_MergeLien1() to use EXE_CODE from Plu_Exe.csv by calling findExeCode().  
 * 06/30/2020          Cleanup Plu_MergeMAdr() & Plu_MergeSitus().  Modify Plu_MergeRoll() to call
 *                     Plu_MergeMAdr() based on which mail address is available.
 * 11/01/2020 20.4.2   Modify Plu_MergeRoll() to populate default PQZoning.
 * 06/25/2021 21.0.2   Modify Plu_MergeMAdr() to ignore addr2 if it's smaller than 4 chars.
 * 07/16/2023 23.0.5   Modify Plu_Load_LDR1(), Plu_CreateLienRec(), Plu_ExtrLien() due to new LDR format.
 * 09/10/2024 24.1.2   Modify Plu_MergeLien1() to add ExeType.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "CharRec.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"

#include "LoadMB.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "PQ.h"
#include "Tax.h"
#include "MergePlu.h"
//#include "MergeZoning.h"

/****************************** Plu_MergeOthers ******************************
 *
 * Merge Legal, Transfer using data from Plu_Roll.csv file
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Plu_MergeOthers(char *pOutbuf, bool bInclOther = false)
{
   static   char acRec[2048], *pRec=NULL;
   int      iRet=0, iTmp;
   char     acTmp[32], *pTmp;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 2048, fdZone);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdZone);
         fdZone = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Roll rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 2048, fdZone);
         lZoneSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_M_ADDR4)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdZone);
         fdZone = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input roll record for APN=%s", apTokens[0]);
         iRet = -1;
      }

      return iRet;
   }

   lZoneMatch++;

   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   if (bInclOther)
   {
      // Merge Acreage
      //double dTmp = atof(apTokens[MB_ROLL_ACRES]);
      //long   lTmp;
      //if (dTmp > 0.0)
      //{
      //   // Lot Sqft
      //   lTmp = (long)(dTmp * SQFT_PER_ACRE);
      //   sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      //   memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      //   // Format Acres
      //   lTmp = (long)(dTmp * ACRES_FACTOR);
      //   sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      //   memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      //}

      // Recorded Doc
      if (*(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
      {
         pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
            vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], SIZ_TRANSFER_DOC);
         }
      }
   }

   // Get next record
   pRec = fgets(acRec, 2048, fdZone);

   return 0;
}

/****************************** Plu_MergeZoneEx ******************************
 *
 * Merge Zoning from Plu_Zone.txt
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

//int Plu_MergeZoneEx(char *pOutbuf)
//{
//   static   char acRec[1024], *pRec=NULL;
//   char     *apItems[16];
//   int      iRet=0, iTmp;
//
//   // Get rec
//   if (!pRec)
//   {
//      // Get first rec
//      pRec = fgets(acRec, 1024, fdZone);
//   }
//
//   do
//   {
//      if (!pRec)
//      {
//         fclose(fdZone);
//         fdZone = NULL;
//         return 1;      // EOF
//      }
//
//      iTmp = memcmp(pOutbuf, pRec, iApnLen);
//      if (iTmp > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Zoning rec %.*s", iApnLen, pRec);
//         pRec = fgets(acRec, 1024, fdZone);
//         lZoneSkip++;
//      }
//   } while (iTmp > 0);
//
//   // If not match, return
//   if (iTmp)
//      return 1;
//
//   iRet = ParseStringIQ(acRec, cDelim, 16, apItems);
//   if (iRet < PLU_ZONE_CODE+1)
//   {
//      if (*pRec == 13 || *pRec == 10)
//      {
//         fclose(fdZone);
//         fdZone = NULL;
//         iRet = 1;      // EOF
//      } else
//      {
//         LogMsg("***** Error: bad input roll record for APN=%s", apItems[0]);
//         iRet = -1;
//      }
//
//      return iRet;
//   }
//
//   lZoneMatch++;
//   // Zoning
//   if (*apItems[PLU_ZONE_CODE] > ' ')
//   {
//      char *pTmp;
//
//      iTmp = iTrim(apItems[PLU_ZONE_CODE]);
//      while (iTmp > SIZ_ZONE)
//      {
//         if (pTmp = strrchr(apItems[PLU_ZONE_CODE], ','))
//            *pTmp = 0;
//         else
//            *(apItems[PLU_ZONE_CODE]+10) = 0;
//         iTmp = strlen(apItems[PLU_ZONE_CODE]);
//      }
//
//      vmemcpy(pOutbuf+OFF_ZONE, apItems[PLU_ZONE_CODE], SIZ_ZONE, iTmp);
//   }
//
//   // Get next record
//   pRec = fgets(acRec, 1024, fdZone);
//
//   return 0;
//}

/***************************** Plu_ConvStdChar *******************************
 *
 * Convert MB1 chars layout to STDCHAR format.
 * PLU may have multiple buildings with the same APN (i.e. 001251007000)
 *
 * Plu_ConvStdChar_Old() is used for older format prior to 11/2018
 * If CatType 4 is present, drop CatType 0 of the same parcel
 *
 *****************************************************************************/

int Plu_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmpFile[256], acTmp[256], acCode[32], acApn[20], *pRec, *pTmp;
   int      iRet, iTmp, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg0("\nConverting char file %s\n", pInfile);

   // Sort input file - APN, 
   sprintf(acTmpFile, "%s\\%s\\%s_char.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acTmp, "S(#27,C,A,#2,C,D,#3,N,A) DEL(%d) F(TXT)", cDelim);
   iRet = sortFile(pInfile, acTmpFile, acTmp);
   if (iRet <= 0)
      return -1;

   if (!(fdIn = fopen(acTmpFile, "r")))
      return -1;

   // Create output file
   if (!(fdOut = fopen(acCChrFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acCChrFile);
      return -2;
   }

   // Skip first record - header
   //iTmp = 0;
   //for (iTmp = 0; iTmp < iHdrRows; iTmp++)
   //   pRec = fgets(acBuf, 1024, fdIn);

   acApn[0] = 0;
   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);

      if (!pRec || *pRec > '9')
         break;

      if (cDelim == '|')
         iRet = ParseStringIQ(pRec, cDelim, PLU_CHAR_ASMT+1, apTokens);
      else
         iRet = ParseStringNQ(pRec, cDelim, PLU_CHAR_ASMT+1, apTokens);
      if (iRet < PLU_CHAR_ASMT)
      {
         if (iRet > 1)
            LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      if (*apTokens[PLU_CHAR_CATTYPE] == '0' && !strcmp(apTokens[PLU_CHAR_ASMT], acApn))
         continue;

      iRet = iTrim(apTokens[PLU_CHAR_ASMT]);
      strcpy(acApn, apTokens[PLU_CHAR_ASMT]);
      memcpy(myCharRec.Apn, apTokens[PLU_CHAR_ASMT], iRet);
      memcpy(myCharRec.FeeParcel, apTokens[PLU_CHAR_FEEPARCEL], strlen(apTokens[PLU_CHAR_FEEPARCEL]));

      // PoolSpa - Pl, PS, Sp
      iRet = blankRem(apTokens[PLU_CHAR_POOLSPA]);
      if (iRet > 0)
      {
         pTmp = findXlatCode(_strupr(apTokens[PLU_CHAR_POOLSPA]), &asPool[0]);
         if (pTmp)
            myCharRec.Pool[0] = *pTmp;
      }

      // Use category
      if (*apTokens[PLU_CHAR_CATTYPE] > ' ')
         myCharRec.LandUseCat[0] = *apTokens[PLU_CHAR_CATTYPE];

      // QualityClass
      if (*apTokens[PLU_CHAR_QUALITYCLASS] > ' ')
      {
         pTmp = _strupr(apTokens[PLU_CHAR_QUALITYCLASS]);
         if (*pTmp == 'X')
            strcpy(acTmp, apTokens[PLU_CHAR_QUALITYCLASS]+1);
         else
            strcpy(acTmp, apTokens[PLU_CHAR_QUALITYCLASS]);

         iRet = iTrim(acTmp);
         memcpy(myCharRec.QualityClass, acTmp, iRet);        
         myCharRec.BldgClass = acTmp[0];

         // Parse it
         if (isdigit(acTmp[2]))
            iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
         else if (!memcmp(&acTmp[1], "AVG", 3))
            acCode[0] = 'A';
         else if (!memcmp(&acTmp[2], "GD", 2))
            acCode[0] = 'G';
         else if (acTmp[2] == ',')
            iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
         else
            acCode[0] = 0;

         if (acCode[0] > 0)
            myCharRec.BldgQual = acCode[0];
      }

      // YrBlt
      iTmp = atoi(apTokens[PLU_CHAR_YEARBUILT]);
      if (iTmp > 1600 && iTmp <= lLienYear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[PLU_CHAR_EFFECTIVEYEAR]);
      if (iTmp > 1600 && iTmp <= lLienYear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSqft
      int iBldgSize = atoi(apTokens[PLU_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 1)
      {
         iTmp = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iTmp);
      }

      // Patio Sqft
      int iTmp = atoi(apTokens[PLU_CHAR_PATIOSF]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[PLU_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[PLU_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport SF
      int iCarport = atoi(apTokens[PLU_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

#ifdef _DEBUG
      //if (!memcmp(acBuf, "102111015000", 9))
      //   iRet = 0;
#endif
      // Fireplace
      if (*apTokens[PLU_CHAR_FIREPLACE] >= '9')
      {
         pRec = findXlatCodeA(apTokens[PLU_CHAR_FIREPLACE], &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      } 

      // Heating
      if (*apTokens[PLU_CHAR_HEATING] > ' ')
      {
         pTmp = findXlatCode(apTokens[PLU_CHAR_HEATING], &asHeating[0]);
         if (pTmp)
            myCharRec.Heating[0] = *pTmp;
      }

      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[PLU_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[PLU_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[PLU_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[PLU_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Rooms
      iTmp = atoi(apTokens[PLU_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[PLU_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[PLU_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half Baths
      iTmp = atoi(apTokens[PLU_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // Water
      if (*(apTokens[PLU_CHAR_HASWELL]) > '0')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Stories
      iTmp = atoi(apTokens[PLU_CHAR_STORIESCNT]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[PLU_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // BldgSeqNum
      iTmp = atoi(apTokens[PLU_CHAR_BLDGSEQNUM]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = 0;
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);
   return iRet;
}

int Plu_ConvStdChar_Old(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmpFile[256], acTmp[256], acCode[32], acApn[20], *pRec, *pTmp;
   int      iRet, iTmp, iCmp, iCmp1, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg0("\nConverting char file %s\n", pInfile);

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_char.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acTmp, "S(#27,C,A,#2,C,D,#3,N,A) DEL(%d) F(TXT)", cDelim);
   iRet = sortFile(pInfile, acTmpFile, acTmp);
   if (iRet <= 0)
      return -1;

   if (!(fdIn = fopen(acTmpFile, "r")))
      return -1;

   // Create output file
   if (!(fdOut = fopen(acCChrFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acCChrFile);
      return -2;
   }

   // Skip first record - header
   //iTmp = 0;
   //for (iTmp = 0; iTmp < iHdrRows; iTmp++)
   //   pRec = fgets(acBuf, 1024, fdIn);

   acApn[0] = 0;
   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);

      if (!pRec || *pRec > '9')
         break;
#ifdef _DEBUG
      //if (!memcmp(acBuf, "133280021000", 9))
      //   iRet = 0;
#endif

      if (cDelim == '|')
         iRet = ParseStringIQ(pRec, cDelim, PLU_CHAR_ASMT+1, apTokens);
      else
         iRet = ParseStringNQ(pRec, cDelim, PLU_CHAR_ASMT+1, apTokens);
      if (iRet < PLU_CHAR_ASMT)
      {
         if (iRet > 1)
            LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      iTmp = blankRem(apTokens[PLU_CHAR_ASMT]);

      if (*apTokens[PLU_CHAR_CATTYPE] == '0' && !strcmp(apTokens[PLU_CHAR_ASMT], acApn))
         continue;

      strcpy(acApn, apTokens[PLU_CHAR_ASMT]);
      memcpy(myCharRec.Apn, apTokens[PLU_CHAR_ASMT], iTmp);
      memcpy(myCharRec.FeeParcel, apTokens[PLU_CHAR_FEEPARCEL], strlen(apTokens[PLU_CHAR_FEEPARCEL]));

      // PoolSpa - 11/04/2015: no data avail
      //iTmp = atoi(apTokens[PLU_CHAR_POOLSPA]);
      //if (iTmp > 0)
      //{
      //   iRet = sprintf(acTmp, "%d", iTmp);
      //   memcpy(myCharRec.Pool, acTmp, iRet);
      //}

      // Use category
      if (*apTokens[PLU_CHAR_CATTYPE] > ' ')
         myCharRec.LandUseCat[0] = *apTokens[PLU_CHAR_CATTYPE];

      // QualityClass
      if (*apTokens[PLU_CHAR_QUALITYCLASS] > ' ')
      {
         pTmp = _strupr(apTokens[PLU_CHAR_QUALITYCLASS]);
         if (*pTmp == 'X')
            strcpy(acTmp, apTokens[PLU_CHAR_QUALITYCLASS]+1);
         else
            strcpy(acTmp, apTokens[PLU_CHAR_QUALITYCLASS]+1);

         memcpy(myCharRec.QualityClass, acTmp, iTmp);        
         iTmp = 0;
         iCmp = iCmp1 = -1;
         while (asQual[iTmp].iLen > 0 && 
            (iCmp=memcmp(acTmp, asQual[iTmp].acSrc, asQual[iTmp].iLen)) && 
            (iCmp1=memcmp(&acTmp[1], asQual[iTmp].acSrc, asQual[iTmp].iLen)) )
            iTmp++;

         if (!iCmp || !iCmp1)
         {
            acCode[0] = asQual[iTmp].acCode[0];
            if (!iCmp1)
               myCharRec.BldgClass = acTmp[0];
         } else
            acCode[0] = 0;

         // Parse it
         if (acCode[0] == 0 && isalpha(acTmp[0])) 
         {
            myCharRec.BldgClass = acTmp[0]; 
      
            iTmp = 0;
            while (acTmp[iTmp] && (!isdigit(acTmp[iTmp]) || acTmp[iTmp] < '1') )
               iTmp++;

            if (acTmp[iTmp] > '0')
               iRet = Quality2Code((char *)&acTmp[iTmp], acCode, NULL);
         } else if (acTmp[0] > '0' && acTmp[0] <= '9')
            iRet = Quality2Code(acTmp, acCode, NULL);

         if (acCode[0] > 0)
            myCharRec.BldgQual = acCode[0];
      }

      // YrBlt
      iTmp = atoi(apTokens[PLU_CHAR_YEARBUILT]);
      if (iTmp > 1600 && iTmp <= lLienYear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[PLU_CHAR_EFFECTIVEYEAR]);
      if (iTmp > 1600 && iTmp <= lLienYear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSqft
      int iBldgSize = atoi(apTokens[PLU_CHAR_BUILDINGSIZE]);
      if (iBldgSize > 1)
      {
         iTmp = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iTmp);
      }

      // Patio Sqft
      int iTmp = atoi(apTokens[PLU_CHAR_PATIOSF]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

      // Attached SF
      int iAttGar = atoi(apTokens[PLU_CHAR_ATTACHGARAGESF]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Detached SF
      int iDetGar = atoi(apTokens[PLU_CHAR_DETACHGARAGESF]);
      if (iDetGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iDetGar);
         memcpy(myCharRec.Misc.sExtra.DetGarSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'L';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Carport SF
      int iCarport = atoi(apTokens[PLU_CHAR_CARPORTSF]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Heating
      if (*apTokens[PLU_CHAR_HEATING] > ' ')
      {
         pTmp = findXlatCode(apTokens[PLU_CHAR_HEATING], &asHeating[0]);
         if (pTmp)
            myCharRec.Heating[0] = *pTmp;
      }

      // Cooling - CoolingCentralAC, CoolingEvaporative, CoolingRoomWall, CoolingWindow
      if (*apTokens[PLU_CHAR_COOLINGCENTRALAC] > ' ')
         myCharRec.Cooling[0] = 'C';
      else if (*apTokens[PLU_CHAR_COOLINGEVAPORATIVE] > ' ')
         myCharRec.Cooling[0] = 'E';
      else if (*apTokens[PLU_CHAR_COOLINGROOMWALL] > ' ')
         myCharRec.Cooling[0] = 'L';
      else if (*apTokens[PLU_CHAR_COOLINGWINDOW] > ' ')
         myCharRec.Cooling[0] = 'W';

      // Rooms
      iTmp = atoi(apTokens[PLU_CHAR_TOTALROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[PLU_CHAR_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[PLU_CHAR_BATHROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
      }

      // Half Baths
      iTmp = atoi(apTokens[PLU_CHAR_HALFBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
      }

      // Fireplace
      iTmp = atoi(apTokens[PLU_CHAR_FIREPLACE]);
      if (iTmp > 0)
         myCharRec.Fireplace[0] = 'Y';

      // Water
      if (*(apTokens[PLU_CHAR_HASWELL]) > '0')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      // Stories
      iTmp = atoi(apTokens[PLU_CHAR_STORIESCNT]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[PLU_CHAR_UNITSCNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // BldgSeqNum
      iTmp = atoi(apTokens[PLU_CHAR_BLDGSEQNUM]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.BldgSeqNo, acTmp, iRet);
      }

      // Lot Sqft - don't use - data is not matched with acreage in roll file
      // PatioSf - data is not usable

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = 0;
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);
   return iRet;
}

/******************************** Plu_CleanName ******************************
 *
 * Return 99 if found a vesting
 *
 *****************************************************************************/

int Plu_CleanName(char *pSrcName, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], *pTmp;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);

   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

/******************************** Plu_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Plu_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], *pTmp;
   char  acName1[64], acVesting[8];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);

   // Remove multiple spaces
   pTmp = strcpy(acTmp, pNames);
   iTmp = blankRem(acTmp);
   vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1, iTmp);
   strcpy(acName1, acTmp);

   iTmp = Plu_CleanName(acTmp, acTmp1, acVesting);
   if (iTmp == 99)
   {
      if (strchr(acTmp1, ' '))
         strcpy(acName1, acTmp1);
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

      // Check EtAl
      if (!memcmp(acVesting, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   }

   // Do not parse if there is numeric in Name
   iTmp =0;
   while (acName1[iTmp])
   {
      if (isdigit(acName1[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp1 = strlen(acName1);
      memcpy(pOutbuf+OFF_NAME_SWAP, acName1, iTmp1);
      return;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009210003000", 9))
   //   iTmp = 0;
#endif

   // Remove double double quote with single double quote
   //replStrAll(acName1, "\"\"", "\"");

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   if (strchr(acName1, ' ') && !(acVesting[0] > ' ' && isVestChk(acVesting)))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      if (iRet == -1)
      {
         memcpy(myOwner.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
         myOwner.acSwapName[SIZ_NAME1] = 0;
      }

   } else
   {
      memcpy(myOwner.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
      myOwner.acSwapName[SIZ_NAME1] = 0;
   }

   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
}

/******************************** Plu_MergeMAdr ******************************
 *
 * There are two places that hold Mail address in roll record.  If it's not parsable,
 * it is stored in M_ADDR1 through M_ADDR4.  We have two Plu_MergeMAdr() to handle
 * two different scenario.
 *
 *****************************************************************************/

void Plu_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
      updateCareOf(pOutbuf, apTokens[MB_ROLL_CAREOF], strlen(apTokens[MB_ROLL_CAREOF]));

   // DBA
   memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
   if (*apTokens[MB_ROLL_DBA] > ' ')
   {
      pTmp = apTokens[MB_ROLL_DBA];
      if (!memcmp(pTmp, "DBA ", 4))
         pTmp += 4;
      iTmp = strlen(pTmp);
      if (iTmp > SIZ_DBA)
         iTmp = SIZ_DBA;
      memcpy(pOutbuf+OFF_DBA, pTmp, iTmp);
   } 

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "129181001000", 9) )
   //   iTmp = 0;
#endif

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   iTmp = blankRem(acAddr1, SIZ_M_ADDR_D);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, iTmp);

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
     sMailAdr.strName[SIZ_M_STREET] = 0;
     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      iTmp = blankRem(apTokens[MB_ROLL_M_CITY]);
      memcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], iTmp);
      iTmp = blankRem(apTokens[MB_ROLL_M_ST]);
      if (2 == iTmp)
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         iTmp = blankRem(apTokens[MB_ROLL_M_ZIP]);
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], 9);
      }

      if (iTmp == 9)
      {
         pTmp = apTokens[MB_ROLL_M_ZIP];
         iTmp = sprintf(acTmp, "%s %s %.5s-%.4s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], pTmp, pTmp+5);
      } else
         iTmp = sprintf(acTmp, "%s %s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST], apTokens[MB_ROLL_M_ZIP]);

      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
   }
}

void Plu_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)
{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "130362016", 9) )
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Remove blank
      iTmp = blankRem(acAddr1);
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D, iTmp);

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         vmemcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            iTmp = sprintf(acTmp, "%s", sMailAdr.strSub);
            vmemcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB, iTmp);
         }
         if (sMailAdr.strDir[0] > '0')
            vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
         if (sMailAdr.strSfx[0] > '0')
            vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
         if (sMailAdr.Unit[0] > ' ')
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      } else
      {
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
      }
   }

   strcpy(acAddr2, p2);
   iTmp = blankRem(acAddr2);
   if (iTmp > 4)
   {
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D, iTmp);

      // Parse addr line 2
      parseAdr2(&sMailAdr, acAddr2);
      if (sMailAdr.City[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

         if (sMailAdr.State[0] > ' ')
            memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

         // Zipcode
         vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
         if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
            memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
      }
   }
}

/******************************** Plu_MergeSAdr ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Plu_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSitus);
      // Get first rec
      pRec = fgets(acRec, 512, fdSitus);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Replace tab char with 0
   if (pRec)
      iRet = ParseStringNQ(pRec, cDelim, MB_SITUS_SEQ+1, apTokens);
   if (iRet < MB_SITUS_SEQ)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old Situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
#ifdef _DEBUG
      //if (strchr(apTokens[MB_SITUS_STRNUM], '-'))
      //   iTmp = 0;
#endif

      // Save original StrNum
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[MB_SITUS_STRNUM], strlen(apTokens[MB_SITUS_STRNUM]));

      iTmp = sprintf(acAddr1, "%d ", lTmp);
      vmemcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM, iTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
      }
   }

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);

      strcpy(acTmp, apTokens[MB_SITUS_STRNAME]);
      vmemcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET);

      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
         Sfx2Code(acTmp, acCode);
      else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
      vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } else
   {
      ADR_REC sAdr;

      parseAdr1S(&sAdr, apTokens[MB_SITUS_STRNAME]);
      if (sAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);

      if (sAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, strlen(sAdr.strSfx));

      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], SIZ_S_UNITNO);
   }

   iTmp = blankRem(acAddr1, SIZ_S_ADDR_D); 
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D, iTmp);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      blankRem(apTokens[MB_SITUS_COMMUNITY]);
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1);   
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);

      if (*apTokens[MB_SITUS_ZIP] == '9')
         vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], SIZ_S_ZIP);      

      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
         sprintf(acTmp, "%s, CA %.5s", myTrim(acAddr1), pOutbuf+OFF_S_ZIP);
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

int Plu_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004130013", 9))
   //   acTmp[0] = 0;
#endif
   // 
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}

/******************************** Plu_MergeSale ******************************
 *
 * Note: need to figure out DocType and translate to our index table
 *
 *****************************************************************************/

int Plu_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp, iCnt;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "100210028000", 9))
   //   iRet = 0;
#endif

   if (iTmp)
      return 1;

   iCnt = 0;
   while (!iTmp)
   {
      iRet = ParseStringNQ(pRec, cDelim, MB_SALES_CONFCODE+1, apTokens);
      if (iRet < MB_SALES_CONFCODE)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }

      // Merge data - Only take sale rec that has both docnum and docdate
      // and recording document only.  Ignore internal document.
      if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         strcpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM]);
         blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);

         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;
         if (acTmp[0] > '0')
         {
            dTmp = atof(acTmp);
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         /* Do not use Confidential Sale Price
         } else
         {
            dollar2Num(apTokens[MB_SALES_PRICE], acTmp);
            if (acTmp[0] > '0')
            {
               lPrice = atol(acTmp);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
            }
         */
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         //strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         //MergeSale2(&sCurSale, pOutbuf, true);
         MB_MergeSale(&sCurSale, pOutbuf, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Plu_MergeChar ******************************
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************

int Plu_MergeChar(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32];
   long     lTmp, lBldgSqft, lGarSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iFp;
   MB_CHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 512, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   pChar = (MB_CHAR *)pRec;

   // Quality Class
   acCode[0] = 0;
   memcpy(acTmp, pChar->QualityClass, MBSIZ_CHAR_QUALITY);
   acTmp[MBSIZ_CHAR_QUALITY] = 0;
   if (isalpha(acTmp[0]))
   {
      *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0];

      iTmp = 0;
      while (acTmp[iTmp] && !isdigit(acTmp[iTmp]))
         iTmp++;

      if (acTmp[iTmp] > '0')
         iRet = Quality2Code((char *)&acTmp[iTmp], acCode, NULL);
   } else if (acTmp[0] > '0' && acTmp[0] <= '9')
      iRet = Quality2Code(acTmp, acCode, NULL);

   memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, strlen(acCode));

   // Yrblt
   lTmp = atoin(pChar->YearBuilt, MBSIZ_CHAR_YRBLT);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = '2';
   } else
   {
      memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = ' ';
   }

   // Heating
   // Cooling 
   // Pool

   // Beds
   iBeds = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Fireplace
   iFp = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   } else
      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);

   // HasSeptic or HasSewer
   if (pChar->HasSeptic > '0')
      *(pOutbuf+OFF_SEWER) = 'Y';
   else if (pChar->HasSewer > '0')
      *(pOutbuf+OFF_SEWER) = 'Y';

   // HasWell
   if (pChar->HasWell > ' ')
      *(pOutbuf+OFF_WATER) = 'Y';

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 512, fdChar);

   return 0;
}

/******************************** Plu_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************

int Plu_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      iTmp = memcmp(pOutbuf, pTmp+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp+iSkipQuote);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   iRet = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0 && lTmp < 99999999)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Plu_MergeTax ******************************
 *
 * Note:
 *
 ****************************************************************************

int Plu_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double   dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iRet;

   // Merge tax data
   while (!iTmp)
   {
      iTmp = ParseStringNQ(pRec, cDelim, MB_TAX_ROLLCAT+1, apTokens);
      if (iTmp < MB_TAX_ROLLCAT)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
         return -1;
      }

      // If same tax year, apply values
      //if (!memcmp(myCounty.acYearAssd, apTokens[MB_TAX_YEAR], 4))
      {
         dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
         dTax1 = atof(acTmp);
         dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
         dTax2 = atof(acTmp);
         dTmp = dTax1+dTax2;
         if (dTax1 == 0.0 || dTax2 == 0.0)
            dTmp *= 2;

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/********************************* Plu_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Plu_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse input
   iRet = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_M_ADDR4)
   {
      if (*pRollRec == 13 || *pRollRec == 10)
         return 1;      // EOF
      else
      {
         LogMsg("***** Error: bad input roll record for APN=%s", apTokens[iApnFld]);
         return -1;
      }
   }

   // Ignore APN starts with 800-999 except 910 (MH) or 920 (PUD/Common area)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910 && iTmp != 920))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "32PLU", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[MB_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[MB_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt  = atoi(apTokens[MB_ROLL_FIXTRS]);
      long lFixtRP= atoi(apTokens[MB_ROLL_FIXTR_RP]);
      long lMH    = atoi(apTokens[MB_ROLL_PPMOBILHOME]);
      long lHSite = atoi(apTokens[MB_ROLL_HOMESITE]);
      long lGrow  = atoi(apTokens[MB_ROLL_GROWING]);
      long lPP    = atoi(apTokens[MB_ROLL_PP_BUS]);
      lTmp = lFixt+lPP+lMH+lHSite+lGrow+lFixtRP;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMH > 0)
         {
            sprintf(acTmp, "%d         ", lMH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lHSite > 0)
         {
            sprintf(acTmp, "%d         ", lHSite);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
         if (lFixtRP > 0)
         {
            sprintf(acTmp, "%d         ", lFixtRP);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // Zoning
   memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
   if (*apTokens[MB_ROLL_ZONING] > ' ')
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   if (*apTokens[MB_ROLL_USECODE] > ' ')
   {
      strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
      myTrim(acTmp);
      updateStdUse1(pOutbuf+OFF_USE_STD, acTmp, pOutbuf);
      blankPad(acTmp, SIZ_USE_CO);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO);
   } else
   {
      memset(pOutbuf+OFF_USE_CO, ' ', SIZ_USE_CO);
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
   }

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Recorded Doc
   if (*(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
   {
      iTmp = strlen(apTokens[MB_ROLL_DOCNUM]);
      if (iTmp > SIZ_TRANSFER_DOC)
         iTmp = SIZ_TRANSFER_DOC;
      memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], iTmp);
      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   }

   // Owner
   try {
      Plu_MergeOwner(pOutbuf, apTokens[MB_ROLL_OWNER]);
   } catch(...) {
      LogMsg("***** Exeception occured in Plu_MergeOwner()");
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "129181001000", 9) )
   //   iTmp = 0;
#endif
   // Mailing
   if (*apTokens[MB_ROLL_M_ADDR] > '0')
      Plu_MergeMAdr(pOutbuf);
   else
      Plu_MergeMAdr(pOutbuf, apTokens[MB_ROLL_M_ADDR1], apTokens[MB_ROLL_M_ADDR2], apTokens[MB_ROLL_M_ADDR3], apTokens[MB_ROLL_M_ADDR4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************* Plu_Load_Roll ******************************
 *
 * This function has not been tested.
 *
 ******************************************************************************/

int Plu_Load_Roll(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE],
            acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], *pTmp;

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Reset header roll since we remove it during sort.
   iHdrRows = 0;

   // Open roll file
   lLastFileDate = getFileDate(acRollFile);
   sprintf(acTmpFile, "%s\\%s\\Roll.srt", acTmpPath, myCounty.acCntyCode);
   lRet = sortFile(acRollFile, acTmpFile, "S(1,12,C,A) F(TXT)  OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")"); 
   if (!lRet)
      return -2;

   LogMsg("Open Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -3;
   }

   // Open Situs file
   sprintf(acTmpFile, "%s\\%s\\Situs.srt", acTmpPath, myCounty.acCntyCode);
   lRet = sortFile(acSitusFile, acTmpFile, "S(1,12,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")"); 
   if (!lRet)
      return -4;
   LogMsg("Open Situs file %s", acTmpFile);
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -4;
   }

   // Open Exe file
   sprintf(acTmpFile, "%s\\%s\\Exe.srt", acTmpPath, myCounty.acCntyCode);
   //sprintf(acRec, "S(#2,C,A) B(154,R) DEL(%d) OMIT(1,1,C,LT,\"0\")", cDelim);
   sprintf(acRec, "S(#2,C,A) OMIT(12,1,C,LT,\"0\",OR,12,1,C,GT,\"9\")");
   lRet = sortFile(acExeFile, acTmpFile, acRec); 
   if (!lRet)
      return -5;
   LogMsg("Open Exe file %s", acTmpFile);
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -5;
   }

   // Open Tax file
   sprintf(acTmpFile, "%s\\%s\\Tax.srt", acTmpPath, myCounty.acCntyCode);
   sprintf(acRec, "S(#1,C,A) DEL(%d) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")", cDelim);
   lRet = sortFile(acTaxFile, acTmpFile, acRec); 
   if (!lRet)
      return -6;
   LogMsg("Open Tax file %s", acTmpFile);
   fdTax = fopen(acTmpFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmpFile);
      return -6;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return -8;

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return -9;

   // Drop header record
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "100210028000", 9) )
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, (char *)&acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Plu_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Plu_MergeSitus(acBuf);

            // Merge Char - Don't send xlat table since it's already coded
            if (fdChar)
               lRet = MB_MergeStdChar(acBuf, (XLAT_CODE *)NULL, (XLAT_CODE *)NULL);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acBuf, 0);
               //lRet = MB_MergeTax(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Plu_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
               lRet = Plu_MergeSitus(acRec);

            // Merge Exe
            if (fdExe)
               lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = MB_MergeStdChar(acRec, (XLAT_CODE *)NULL, (XLAT_CODE *)NULL);

            // Merge Taxes
            if (fdTax)
               lRet = MB_MergeTaxG2(acRec, 0);
               //lRet = MB_MergeTax(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

#ifdef _DEBUG
            if (replChar(acRec, 0, ' ', iRecLen))
               LogMsg("*** Null character found at %d on record %d", iRet+1, lCnt);
#endif
            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Plu_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            lRet = Plu_MergeSitus(acRec);

         // Merge Exe
         if (fdExe)
            lRet = MB_MergeExe(acRec);

         // Merge Char
         if (fdChar)
            lRet = MB_MergeStdChar(acRec, (XLAT_CODE *)NULL, (XLAT_CODE *)NULL);

         // Merge Taxes
         if (fdTax)
            lRet = MB_MergeTaxG2(acRec, 0);
            //lRet = MB_MergeTax(acRec);
      
         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }
   
   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Exe skiped:       %u", lExeSkip);
   LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u\n", lCnt);
   return 0;
}

/********************************* Plu_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

//int Plu_MergeLien(char *pOutbuf, char *pRollRec)
//{
//   char     acTmp[256], acTmp1[256];
//   long     lTmp;
//   double   dTmp;
//   int      iRet=0, iTmp;
//
//   // Replace null char with space
//   iRet = replNull(pRollRec, ' ', 0);
//
//   // Parse input rec
//   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
//   if (iRet < L_DTS)
//   {
//      LogMsg("***** Error: bad input lien record for APN=%s", apTokens[L_ASMT]);
//      return -1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//
//   // Start copying data
//   memcpy(pOutbuf, apTokens[L_ASMT], strlen(apTokens[L_ASMT]));
//
//   // Copy ALT_APN
//   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L_FEEPARCEL], strlen(apTokens[L_FEEPARCEL]));
//
//   // Format APN
//   iRet = formatApn(apTokens[L_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//   // Create MapLink and output new record
//   iRet = formatMapLink(apTokens[L_ASMT], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // Create index map link
//   if (getIndexPage(acTmp, acTmp1, &myCounty))
//      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//   // County code
//   memcpy(pOutbuf+OFF_CO_NUM, "32PLU", 5);
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // Lien values
//   // Land
//   long lLand = atoi(apTokens[L_CURRENTMARKETLANDVALUE]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = atoi(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: Growing, Fixture, PP, PPMH
//   long lGrow = atoi(apTokens[L_CURRENTGROWINGIMPRVALUE]);
//   long lFixt   = atoi(apTokens[L_CURRENTFIXEDIMPRVALUE]);
//   long lPP   = atoi(apTokens[L_CURRENTPERSONALPROPVALUE]);
//   long lMH   = atoi(apTokens[L_CURRENTPERSONALPROPMHVALUE]);
//
//   lTmp = lGrow+lFixt+lPP+lMH;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//
//      if (lGrow > 0)
//      {
//         sprintf(acTmp, "%d         ", lGrow);
//         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
//      }
//      if (lFixt > 0)
//      {
//         sprintf(acTmp, "%d         ", lFixt);
//         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
//      }
//      if (lPP > 0)
//      {
//         sprintf(acTmp, "%d         ", lPP);
//         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
//      }
//      if (lMH > 0)
//      {
//         sprintf(acTmp, "%d         ", lMH);
//         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
//      }
//   }
//
//   // Gross total
//   lTmp += (lLand+lImpr);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//   // Tax
//   double dTax1 = atof(apTokens[L_TAXAMT1]);
//   double dTax2 = atof(apTokens[L_TAXAMT2]);
//   dTmp = dTax1+dTax2;
//   if (dTax1 == 0.0 || dTax2 == 0.0)
//      dTmp *= 2;
//
//   if (dTmp > 0.0)
//   {
//      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
//      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
//   } else
//      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
//
//   // Exemption
//   long lExe1 = atol(apTokens[L_EXEMPTIONAMT1]);
//   long lExe2 = atol(apTokens[L_EXEMPTIONAMT2]);
//   long lExe3 = atol(apTokens[L_EXEMPTIONAMT3]);
//   lTmp = lExe1+lExe2+lExe3;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
//      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
//   }  
//   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
//      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
//   else
//      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
//
//   // Save exemption code
//   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
//   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
//   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));
//
//   // TRA
//   memcpy(pOutbuf+OFF_TRA, apTokens[L_TRA], strlen(apTokens[L_TRA]));
//
//   // status
//   *(pOutbuf+OFF_STATUS) = *apTokens[L_STATUS];
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "030330020", 9))
//   //   iTmp = 0;
//#endif
//   // Legal
//   iTmp = updateLegal(pOutbuf, apTokens[L_PARCELDESCRIPTION]);
//   if (iTmp > iMaxLegal)
//      iMaxLegal = iTmp;
//
//   // UseCode
//   if (*apTokens[L_USECODE] > ' ')
//   {
//      strcpy(acTmp, apTokens[L_USECODE]);
//      myTrim(acTmp);
//      updateStdUse1(pOutbuf+OFF_USE_STD, acTmp, pOutbuf);
//      memcpy(pOutbuf+OFF_USE_CO, acTmp, strlen(acTmp));
//   } else
//   {
//      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
//   }
//
//   // Acres
//   dTmp = atof(apTokens[L_ACRES]);
//   if (dTmp > 0.0)
//   {
//      // Lot Sqft
//      lTmp = (long)(dTmp * SQFT_PER_ACRE);
//      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      // Format Acres
//      lTmp = (long)(dTmp * ACRES_FACTOR);
//      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   // AgPreserved
//   if (*apTokens[L_ISAGPRESERVE] == '1')
//      *(pOutbuf+OFF_AG_PRE) = 'Y';
//
//   // Owner
//   Plu_MergeOwner(pOutbuf, apTokens[L_OWNER]);
//
//   // Situs
//   Plu_MergeSitus(pOutbuf, apTokens[L_SITUS1], apTokens[L_SITUS2]);
//
//   // Mailing
//   Plu_MergeMAdr(pOutbuf, apTokens[L_MAILADDRESS1], apTokens[L_MAILADDRESS2], apTokens[L_MAILADDRESS3], apTokens[L_MAILADDRESS4]);
//
//   // SetTaxcode, Prop8 flag, FullExe flag
//   iTmp = updateTaxCode(pOutbuf, apTokens[L_TAXABILITY], true, true);
//
//   return 0;
//}

/********************************* Plu_Load_LDR *****************************
 *
 * Load TR601 LDR into 1900-byte record.
 *
 ****************************************************************************/

//int Plu_Load_LDR(int iFirstRec /* 1=create header rec */)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
//   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
//
//   HANDLE   fhOut;
//
//   DWORD    nBytesWritten;
//   BOOL     bRet, bEof;
//   long     iRet, lRet=0, lCnt=0;
//
//   lLastFileDate = getFileDate(acRollFile);
//   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//
//   // Sort input file
//   sprintf(acTmpFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
//   iRet = sortFile(acRollFile, acTmpFile, "S(#1,C,A)");
//   if (iRet < 5000)
//   {
//      LogMsg("***** Input file is too small.");
//      return -1;
//   }
//
//   // Open roll file
//   LogMsg("Open Roll file %s", acTmpFile);
//   fdRoll = fopen(acTmpFile, "r");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
//      return -2;
//   }
//
//   // Open Char file
//   LogMsg("Open Char file %s", acCChrFile);
//   fdChar = fopen(acCChrFile, "r");
//   if (fdChar == NULL)
//   {
//      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
//      return -2;
//   }
//
//   // Open Zoning file
//   GetIniString(myCounty.acCntyCode, "RollFile", "", acRec, _MAX_PATH, acIniFile);
//   sprintf(acBuf, acRec, myCounty.acCntyCode, myCounty.acCntyCode);
//   if (acBuf[0] > ' ')
//   {
//      // Sort on ASMT
//      sprintf(acTmpFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//      iRet = sortFile(acBuf, acTmpFile, "S(#1,C,A)");
//
//      LogMsg("Open Zoning file %s", acTmpFile);
//      fdZone = fopen(acTmpFile, "r");
//      if (fdZone == NULL)
//      {
//         LogMsg("***** Error opening Zoning file: %s\n", acTmpFile);
//         return -2;
//      }
//   }
//
//   // Open Situs file
//   sprintf(acTmpFile, "%s\\%s\\Situs.srt", acTmpPath, myCounty.acCntyCode);
//   lRet = sortFile(acSitusFile, acTmpFile, "S(1,12,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")"); 
//   if (!lRet)
//      return -4;
//   LogMsg("Open Situs file %s", acTmpFile);
//   fdSitus = fopen(acTmpFile, "r");
//   if (fdSitus == NULL)
//   {
//      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
//      return -4;
//   }
//
//   // Open Output file
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error creating R01 file: %s\n", acTmpFile);
//      return 4;
//   }
//
//   // Output first header record
//   if (iFirstRec > 0)
//   {
//      memset(acBuf, '9', iRecLen);
//      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//   }
//
//   // Get 1st rec
//   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
//   bEof = (pTmp ? false:true);
//
//   // Init variables
//   iNoMatch=iBadCity=iBadSuffix=0;
//
//   // Merge loop
//   while (pTmp && !feof(fdRoll))
//   {
//      // Create new R01 record
//      iRet = Plu_MergeLien(acBuf, acRec);
//      if (!iRet)
//      {
//         // Merge Char
//         if (fdChar)
//            lRet = MB_MergeStdChar(acBuf, (XLAT_CODE *)NULL, (XLAT_CODE *)NULL);
//            //lRet = Plu_MergeChar(acBuf);
//
//         // Merge Zoning
//         if (fdZone)
//            lRet = Plu_MergeZone(acBuf);
//
//         // Merge Situs
//         if (fdSitus)
//            lRet = Plu_MergeSitus(acBuf);
//
//         // Save last recording date
//         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
//         if (lRet > lLastRecDate && lRet < lToday)
//            lLastRecDate = lRet;
//
//         lLDRRecCount++;
//         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//         if (!bRet)
//         {
//            LogMsg("***** Error writing to output file at record %d\n", lCnt);
//            lRet = WRITE_ERR;
//            break;
//         }
//      }
//
//      // Get next roll record
//      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
//      if (!isdigit(acRec[1]))
//         break;      // EOF
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fdChar)
//      fclose(fdChar);
//   if (fdZone)
//      fclose(fdZone);
//   if (fdSitus)
//      fclose(fdSitus);
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   LogMsg("Total input records:        %u", lCnt);
//   LogMsg("Total output records:       %u", lLDRRecCount);
//   LogMsg("Total bad-city records:     %u", iBadCity);
//   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
//
//   LogMsg("Number of Zoning matched:   %u", lZoneMatch);
//   LogMsg("Number of Situs matched:    %u", lSitusMatch);
//   LogMsg("Number of Char matched:     %u", lCharMatch);
//   LogMsg("Number of Sale matched:     %u\n", lSaleMatch);
//
//   LogMsg("Number of Zoning skiped:    %u", lZoneSkip);
//   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
//   LogMsg("Number of Char skiped:      %u", lCharSkip);
//   LogMsg("Number of Sale skiped:      %u\n", lSaleSkip);
//
//   LogMsg("Last recording date:        %u", lLastRecDate);
//   printf("\nTotal output records: %u\n", lCnt);
//
//   lRecCnt = lLDRRecCount;
//   return 0;
//}

/******************************** findExeCode ********************************
 *
 * Find EXE code
 *
 *****************************************************************************/

int findExeCode(char *pExeCode, char *pApn)
{
   static   char acRec[512], *pRec=NULL;
   char     *pTmp;
   int      iRet=0, iTmp;

   *pExeCode = 0;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
      if (acRec[0] > '9')
         pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, cDelim);
      pTmp++;
      iTmp = memcmp(pApn, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pApn, iRet);
      pRec = fgets(acRec, 512, fdExe);
      iErrorCnt++;
      return -1;
   }

   // EXE code
   if (*apTokens[MB_EXE_CODE] > ' ')
      strcpy(pExeCode, apTokens[MB_EXE_CODE]);

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

// Use this function when multiple codes are seen in EXE file.
int findExeCodes(char *pExeCode[], char *pApn)
{
   static   char acRec[512], *pRec=NULL;
   int      iRet=0, iTmp, iIdx=0;

   *pExeCode[0] = 0;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
      if (acRec[0] > '9')
         pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pApn, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   while (!feof(fdExe))
   {
      iRet = ParseStringNQ(pRec, cDelim, MB_EXE_EXEPCT+1, apTokens);
      if (iRet < MB_EXE_EXEPCT)
      {
         LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pApn, iRet);
         pRec = fgets(acRec, 512, fdExe);
         iErrorCnt++;
         return -1;
      }

      // EXE code
      if (*apTokens[MB_EXE_CODE] > ' ')
      {
         strcpy(pExeCode[iIdx++], apTokens[MB_EXE_CODE]);
      }

      // Get next record
      pRec = fgets(acRec, 512, fdExe);
      if (memcmp(pApn, pRec, iApnLen))
         break;
   }
   lExeMatch++;

   return 0;
}

/******************************************************************************
 *
 * Use Legal, Acreage, situs from update file
 *
 ******************************************************************************/

int Plu_MergeLien1(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256];
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < PLU_L_MAXCNT)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[PLU_L_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   iTmp = strlen(apTokens[PLU_L_ASMT]);
   if (iTmp < iApnLen)
      sprintf(acTmp, "%*s%s", iTmp, "000", apTokens[PLU_L_ASMT]);
   else
      strcpy(acTmp, apTokens[PLU_L_ASMT]);
   memcpy(pOutbuf, acTmp, iApnLen);

   // Format APN
   iRet = formatApn(acTmp, acTmp1, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp1, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acTmp, acTmp1, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp1, iRet);

   // Create index map link
   if (getIndexPage(acTmp1, acTmp, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp, iRet);

   // Copy ALT_APN
   iTmp = strlen(apTokens[PLU_L_FEEPARCEL]);
   if (iTmp < iApnLen)
      sprintf(acTmp, "%.*s%s", iTmp, "000", apTokens[PLU_L_ASMT]);
   else
      strcpy(acTmp, apTokens[PLU_L_ASMT]);
   memcpy(pOutbuf+OFF_ALT_APN, acTmp, iApnLen);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "32PLU", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[PLU_L_ELAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[PLU_L_EIMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   lTmp = atoi(apTokens[PLU_L_EPPROP]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      sprintf(acTmp, "%d         ", lTmp);
      memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[PLU_L_EHOE]);
   long lExe2 = atol(apTokens[PLU_L_ENONHOE]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002442001000", 9))
   //   iTmp = 0;
#endif
   *(pOutbuf+OFF_HO_FL) = '2';         // 'N'
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      lExeMatch++;
   } else
   if (lExe2 > 0)
   {
      iTmp = findExeCode(acTmp, apTokens[PLU_L_ASMT]);
      if (!iTmp)
         vmemcpy(pOutbuf+OFF_EXE_CD1, acTmp, 3);
   }

   // Populate ExeType
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&PLU_Exemption);

   // TRA
   iTmp = atol(apTokens[PLU_L_TRA]);
   sprintf(acTmp, "%.6d", iTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, 6);

   // status
   *(pOutbuf+OFF_STATUS) = 'A';

   // Legal
 
   // UseCode
   if (*apTokens[PLU_L_LANDUSE1] > ' ')
   {
      strcpy(acTmp, apTokens[PLU_L_LANDUSE1]);
      myTrim(acTmp);
      updateStdUse1(pOutbuf+OFF_USE_STD, acTmp, pOutbuf);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, strlen(acTmp));
   } else
   {
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
   }

   // Acres
   dTmp = atof(apTokens[PLU_L_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // AgPreserved
   if (*(apTokens[PLU_L_TAXABILITY]+1) == '5')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Plu_MergeOwner(pOutbuf, apTokens[PLU_L_ASSESSEENAME]);

   // Mailing
   Plu_MergeMAdr(pOutbuf, apTokens[PLU_L_ADDRESS1], apTokens[PLU_L_ADDRESS2], apTokens[PLU_L_ADDRESS3], apTokens[PLU_L_ADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[PLU_L_TAXABILITY], true, true);

   return 0;
}

/******************************************************************************
 *
 * 2015-2020 layout
 *
 ******************************************************************************/

int Plu_Load_LDR1(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;

   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     iRet, lRet=0, lCnt=0;

   lLastFileDate = getFileDate(acRollFile);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Sort input file - Sort on ASMT
   sprintf(acTmpFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort LDR file %s to %s", acRollFile, acTmpFile);
   sprintf(acRec, "S(#2,C,A) DEL(%d) ", cLdrSep);
   iRet = sortFile(acRollFile, acTmpFile, acRec);
   if (iRet < 5000)
   {
      LogMsg("***** Input file is too small.");
      return -1;
   }

   // Open roll file
   LogMsg("Open LDR file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acTmpFile);
      return -2;
   }

   // Open Exe file to get EXE code for nonHO
   sprintf(acTmpFile, "%s\\%s\\Exe.srt", acTmpPath, myCounty.acCntyCode);
   sprintf(acRec, "S(#2,C,A) DEL(%c) OMIT(12,1,C,LT,\"0\",OR,12,1,C,GT,\"9\")", cDelim);
   iRet = sortFile(acExeFile, acTmpFile, acRec); 
   if (!iRet)
      return -5;
   LogMsg("Open Exe file %s", acTmpFile);
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return -5;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open roll file to get Legal
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acBuf, acRec, myCounty.acCntyCode, myCounty.acCntyCode);
   if (acBuf[0] > ' ')
   {
      // Sort on ASMT
      sprintf(acTmpFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = sortFile(acBuf, acTmpFile, "S(#1,C,A)");

      LogMsg("Open roll file %s", acTmpFile);
      fdZone = fopen(acTmpFile, "r");
      if (fdZone == NULL)
      {
         LogMsg("***** Error opening roll file: %s\n", acTmpFile);
         return -2;
      }
   }

   // Open Situs file
   sprintf(acTmpFile, "%s\\%s\\Situs.srt", acTmpPath, myCounty.acCntyCode);
   lRet = sortFile(acSitusFile, acTmpFile, "S(1,12,C,A) OMIT(1,1,C,LT,\"0\",OR,1,1,C,GT,\"9\")"); 
   if (!lRet)
      return -4;
   LogMsg("Open Situs file %s", acTmpFile);
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -4;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL|FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating R01 file: %s\n", acTmpFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new R01 record
      iRet = Plu_MergeLien1(acBuf, acRec);
      if (!iRet)
      {
         // Merge Char
         if (fdChar)
            lRet = MB_MergeStdChar(acBuf, (XLAT_CODE *)NULL, (XLAT_CODE *)NULL);

         // Merge Zoning
         if (fdZone)
            lRet = Plu_MergeOthers(acBuf, true);

         // Merge Situs
         if (fdSitus)
            lRet = Plu_MergeSitus(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdZone)
      fclose(fdZone);
   if (fdSitus)
      fclose(fdSitus);
   if (fdExe)
      fclose(fdExe);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Exe matched:      %u", lExeMatch);
   LogMsg("Number of Legal matched:    %u", lZoneMatch);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u\n", lCharMatch);

   LogMsg("Number of Legal skiped:     %u", lZoneSkip);
   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lLDRRecCount;
   return 0;
}

/**************************** Plu_CreateLienRec *****************************
 *
 * Format lien extract record.  This function is for PLU only.
 *
 ****************************************************************************/

int Plu_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet;
   ULONG    lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   if (cLdrSep == 9)
      iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   else
      iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < PLU_L_MAXCNT)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[PLU_L_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   iRet = strlen(apTokens[PLU_L_ASMT]);
   if (iRet < iApnLen)
   {
      memcpy(pLienExtr->acApn, "0000", iApnLen - iRet);
      memcpy(&pLienExtr->acApn[iApnLen - iRet], apTokens[PLU_L_ASMT], iRet);
   } else
      memcpy(pLienExtr->acApn, apTokens[PLU_L_ASMT], iRet);

   // TRA
   lTmp = atol(apTokens[PLU_L_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[PLU_L_ELAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LIEN_FIXT);
   }

   // Improve
   long lImpr = atoi(apTokens[PLU_L_EIMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_LIEN_FIXT);
   }

   // Personal Property
   lTmp = atol(apTokens[PLU_L_EPPROP]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);
      memcpy(pLienExtr->acPP_Val, acTmp, SIZ_OTHER);
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[PLU_L_EHOE]);
   long lExe2 = atol(apTokens[PLU_L_ENONHOE]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  
   if (lExe1 > 0)
   {
      pLienExtr->acHO[0] = '1';      // 'Y'
      memcpy(pLienExtr->extra.MB.ExeCode1, "E01", 3);
   } else
   {
      pLienExtr->acHO[0] = '2';      // 'N'
   }

   // Prop 8 - correcting by spn 01/16/2010
   lTmp = atoin(apTokens[PLU_L_TAXABILITY], 3);
   if (lTmp > 799 && lTmp < 900)
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Taxability
   if (lTmp > 0)
      vmemcpy(pLienExtr->acTaxCode, apTokens[PLU_L_TAXABILITY], SIZ_LIEN_TAXCODE);

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************* Plu_ExtrLien *******************************
 *
 * Extract lien data from new layout 2015-2016_Secured.csv
 *
 ****************************************************************************/

int Plu_ExtrLien(LPCSTR pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg0("Extract lien roll for %s", pCnty);

   // Sort input file - Sort on ASMT
   sprintf(acBuf, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort roll file %s to %s", acRollFile, acBuf);
   sprintf(acRec, "S(#2,C,A) DEL(%d) ", cLdrSep);
   iRet = sortFile(acRollFile, acBuf, acRec);

   // Open lien file
   fdRoll = fopen(acBuf, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acBuf);
      return 2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Skip header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (!isdigit(acRec[3]))
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new record
      iRet = Plu_CreateLienRec(acBuf, acRec);
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (strlen(acRec) < 20)
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Plu_LoadZoning *****************************
 *
 * Load zoning file from Denise
 *
 ****************************************************************************/

//int Plu_LoadZoning(char *pCnty, int iSkip)
//{
//   char     acBuf[MAX_RECSIZE], acTmp[_MAX_PATH];
//   char     acOutFile[_MAX_PATH], acRawFile[_MAX_PATH];
//
//   HANDLE   fhIn, fhOut;
//   DWORD    nBytesRead, nBytesWritten;
//   BOOL     bRet;
//   int      iRet;
//   long     lCnt=0;
//
//   LogMsg0("Merge Zoning");
//
//   // Setup file names
//   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
//   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "TMP");
//
//   if (_access(acRawFile, 0))
//      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");
//
//   // Open Zoning file
//   GetIniString(myCounty.acCntyCode, "Zoning", "", acBuf, _MAX_PATH, acIniFile);
//   if (acBuf[0] > ' ')
//   {
//      // Sort on ASMT
//      sprintf(acTmp, "%s\\%s\\%s_Zoning.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//      iRet = sortFile(acBuf, acTmp, "S(#1,C,A)");
//
//      LogMsg("Open Zoning file %s", acTmp);
//      fdZone = fopen(acTmp, "r");
//      if (fdZone == NULL)
//      {
//         LogMsg("***** Error opening Zoning file: %s\n", acTmp);
//         return -2;
//      }
//   }
//
//   // Open R01 file
//   LogMsg("Open R01 file %s", acRawFile);
//   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhIn == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error opening input file: %s\n", acRawFile);
//      return -3;
//   }
//
//   // Open Output file
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error opening output file: %s\n", acOutFile);
//      return -4;
//   }
//
//   // Copy skip record
//   bRet = true;
//   memset(acBuf, ' ', iRecLen);
//   while (iSkip-- > 0)
//   {
//      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
//      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//   }
//
//   // Merge loop
//   while (bRet)
//   {
//      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
//
//      // Check for EOF
//      if (!bRet)
//      {
//         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
//         iRet = -1;
//         break;
//      }
//
//      // EOF ?
//      if (iRecLen != nBytesRead)
//         break;
//
//      // Merge Zoning
//      if (fdZone)
//         iRet = Plu_MergeZoneEx(acBuf);
//
//      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//   printf("\n");
//
//   // Close files
//   if (fdZone)
//      fclose(fdZone);
//   if (fhOut)
//      CloseHandle(fhOut);
//   if (fhIn)
//      CloseHandle(fhIn);
//
//   // Rename output file
//   sprintf(acTmp, acRawTmpl, pCnty, pCnty, "O01");
//   if (!_access(acTmp, 0))
//      remove(acTmp);
//   LogMsg("Rename %s to %s", acRawFile, acTmp);
//   iRet = rename(acRawFile, acTmp);
//   LogMsg("Rename %s to %s", acOutFile, acRawFile);
//   iRet = rename(acOutFile, acRawFile);
//
//   LogMsg("Total output records:       %u", lCnt);
//   LogMsg("Number of Zone matched:     %u", lZoneMatch);
//   LogMsg("Number of Zone skiped:      %u", lZoneSkip);
//
//   return iRet;
//}

/*********************************** loadPlu ********************************
 *
 * Options:
 *    -CPLU -L -Xl -Xs[i]   (load lien)
 *    -CPLU -U -Xs[i] -T    (load update)
 *
 ****************************************************************************/

int loadPlu(int iSkip)
{
   int   iRet=0;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = MB_ROLL_ASMT;

   iApnLen = myCounty.iApnLen;

   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      // Load taxrollinfo into SQL
      //iRet = MB_Load_TaxRollInfo(bTaxImport);

      // Load Plu_Tax.csv
      iRet = MB_Load_TaxBase(bTaxImport, true, 2, iHdrRows);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load taxcodemstr
         iRet = MB_Load_TaxCodeMstr(bTaxImport);

         // Load taxcodes
         if (!iRet)
            iRet |= MB_Load_TaxCode(bTaxImport);

         // Load Redemption
         if (!iRet)
            iRet |= MB_Load_TaxRedemption(bTaxImport);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
      if (!iLoadFlag)
         return iRet;
   }

   // Extract lien file
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      //iRet = MB_ExtrTR601(myCounty.acCntyCode);
      
      // LDR layout changed in 2015
      iRet = Plu_ExtrLien(myCounty.acCntyCode);
   }

   // Create/Update cum sale file
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      iRet = MB_CreateSCSale(MM_DD_YYYY_1, 0, 0, FALSE, &PLU_DocCode[0]);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      iRet = Plu_ConvStdChar(acCharFile);
      if (iRet <= 0)
      {
         LogMsg("***** Error extracting attributes data from %s", acCharFile);
         return -1;
      }
   }

   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      if (iLoadFlag & LOAD_LIEN)                   // -L
      {
         // Create Lien file
         LogMsg0("Load %s Lien file", myCounty.acCntyCode);
         //iRet = Plu_Load_LDR(iSkip);
         // 2015
         iRet = Plu_Load_LDR1(iSkip);
      } else if (iLoadFlag & LOAD_UPDT)            // -U
      {
         if (acRollFile[0])
         {
            LogMsg0("Load %s roll update file", myCounty.acCntyCode);
            iRet = Plu_Load_Roll(iSkip);      
         } else
         {
            LogMsg("***** Please specify Rollfile in [%s] section of %s", myCounty.acCntyCode, acIniFile);
            iRet = -1;
         }
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Do county specific case here to apply DocCode correctly
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
   }

   // Merge zoning
   //if (!iRet && (iLoadFlag & MERG_ZONE) )          // -Mz
   //{
   //   iRet = MergeZoning(myCounty.acCntyCode, iSkip);
   //}

   return iRet;
}