#ifndef  _R01_H_
#define  _R01_H_

#define  MAX_SUFFIX            256
#define  MAX_RECSIZE           6400
#define  MAX_FLD_TOKEN         256
#define  MAX_REC_CNT           8
#define  MAX_LOTACRES          22001.00   // Limit on LOT_SQFT is 9 digits or 999,999,999

#define  SIZ_APN_S             14
#define  SIZ_APN_D             17
#define  SIZ_CO_NUM            2
#define  SIZ_CO_ID             3
#define  SIZ_STATUS            8
#define  SIZ_TRA               8
#define  SIZ_NAME1             52
#define  SIZ_NAME2             46
#define  SIZ_NAME_TYPE1        1
#define  SIZ_NAME_TYPE2        1
#define  SIZ_VEST              4
#define  SIZ_ZONE              10
#define  SIZ_ZONE_X1           16
#define  SIZ_ZONE_X2           19
#define  SIZ_ZONE_X3           19
#define  SIZ_ZONE_X4           10
#define  SIZ_USE_STD           3
#define  SIZ_XCAREOF           20
#define  SIZ_USE_CO            8
#define  SIZ_USE_COX           19
#define  SIZ_S_HSENO           10
#define  SIZ_S_STRNUM          7
#define  SIZ_S_STR_SUB         3
#define  SIZ_S_DIR             2
#define  SIZ_S_STREET          24
#define  SIZ_S_SUFF            5
#define  SIZ_S_UNITNO          6
#define  SIZ_S_UNITNOX         12
#define  SIZ_S_CITY            24
#define  SIZ_S_ST              2
#define  SIZ_S_ZIP             5
#define  SIZ_S_ZIP4            4

#define  SIZ_M_STRNUM          7
#define  SIZ_M_STR_SUB         3
#define  SIZ_M_DIR             2
#define  SIZ_M_STREET          24
#define  SIZ_M_SUFF            5
#define  SIZ_M_UNITNO          6
#define  SIZ_M_UNITNOX         11
#define  SIZ_M_CITY            17
#define  SIZ_M_CITYX           3
#define  SIZ_M_ST              2
#define  SIZ_M_ZIP             5
#define  SIZ_M_ZIP4            4
#define  SIZ_TRACT             6
#define  SIZ_BLOCK             6
#define  SIZ_LOT               6
#define  SIZ_IMAPLINK          16
#define  SIZ_EXE_CD1           4
#define  SIZ_RATIO             3
#define  SIZ_LAND              10
#define  SIZ_IMPR              10
#define  SIZ_OTHER             10
#define  SIZ_EXE_TOTAL         10
#define  SIZ_GROSS             10
#define  SIZ_HO_FL             1
#define  SIZ_TAX_CODE          3
#define  SIZ_TAX_AMT           12
#define  SIZ_DEL_YR            4
#define  SIZ_TIMBER            11
#define  SIZ_AG_PRE            7
#define  SIZ_ALT_APN           14
#define  SIZ_SALE1_AMT         10
#define  SIZ_SALE1_CODE        2
#define  SIZ_SALE1_DOCTYPE     3
#define  SIZ_MULTI_APN         1
#define  SIZ_TRS               12
#define  SIZ_TD1_CODE          13
#define  SIZ_TD2_CODE          13
#define  SIZ_SELLER            24
#define  SIZ_SALE2_AMT         10
#define  SIZ_SALE2_CODE        2
#define  SIZ_SALE2_DOCTYPE     3
#define  SIZ_SALE3_AMT         10
#define  SIZ_SALE3_CODE        2
#define  SIZ_SALE3_DOCTYPE     3
#define  SIZ_TRANSFER_DT       8
#define  SIZ_TRANSFER_DOC      12
#define  SIZ_SALE1_DT          8
#define  SIZ_SALE1_DOC         12
#define  SIZ_SALE2_DT          8
#define  SIZ_SALE2_DOC         12
#define  SIZ_SALE3_DT          8
#define  SIZ_SALE3_DOC         12
#define  SIZ_LOT_ACRES         9
#define  SIZ_LOT_SQFT          9
#define  SIZ_YR_BLT            4
#define  SIZ_YR_EFF            4
#define  SIZ_BLDG_SF           9
#define  SIZ_UNITS             6
#define  SIZ_STORIES           4
#define  SIZ_BEDS              2
#define  SIZ_BATH_F            2
#define  SIZ_BATH_H            2
#define  SIZ_ROOMS             3
#define  SIZ_BLDG_CLASS        1
#define  SIZ_BLDG_QUAL         4
#define  SIZ_QUALITYCLASS      6
#define  SIZ_YR_ASSD           4
#define  SIZ_UNS_FLG           1
#define  SIZ_IMPR_COND         1
#define  SIZ_FIRE_PL           2
#define  SIZ_BLDGS             2
#define  SIZ_AIR_COND          1
#define  SIZ_HEAT              1
#define  SIZ_GAR_SQFT          6
#define  SIZ_PARK_TYPE         1
#define  SIZ_PARK_SPACE        4
#define  SIZ_VIEW              2
#define  SIZ_STYLE             1
#define  SIZ_IMPR_TYPE         1
#define  SIZ_CONS_TYPE         1
#define  SIZ_FNDN_TYPE         1
#define  SIZ_ROOF_TYPE         1
#define  SIZ_ROOF_MAT          1
#define  SIZ_FRAME_TYPE        1
#define  SIZ_EX_WALL_TYPE      1
#define  SIZ_WATER             1
#define  SIZ_SEWER             1
#define  SIZ_POOL              1
#define  SIZ_LEGAL             40
//#define  SIZ_OTHER_ROOM        40
#define  SIZ_MAPDIV_LA         1
#define  SIZ_PUBL_FLG          1
#define  SIZ_CARE_OF           30
#define  SIZ_LEGAL1            166
#define  SIZ_LEGAL2            34
#define  SIZ_BSMT_SF           6
#define  SIZ_YEAR              4
#define  SIZ_DBA               40

//#ifdef _DOC_LINK
#define  SIZ_AMENITIES         8
#define  SIZ_DOCLINKS          80
#define  SIZ_DOCLINKX          22
//#else
//#define  SIZ_AMENITIES         42
//#define  SIZ_EQUIPMENT         46
//#endif

#define  SIZ_MAPLINK           20
#define  SIZ_S_ADDR_D          52
#define  SIZ_S_CTY_ST_D        38
#define  SIZ_M_ADDR_D          52
#define  SIZ_M_CTY_ST_D        31
#define  SIZ_NAME_SWAP         52
#define  SIZ_TD1_AMT           10
#define  SIZ_TD2_AMT           10
#define  SIZ_N_SCR             4
#define  SIZ_N_LON             10
#define  SIZ_N_LAT             9
#define  SIZ_N_TRACT           9
#define  SIZ_N_DPBC            2

#define  SIZ_S_ADDR            82
#define  SIZ_M_ADDR            75
#define  SIZ_M_ADDR1           47
#define  SIZ_M_ADDR2           19
#define  SIZ_S_ADDRB_D         90
#define  SIZ_M_ADDRB_D         83

#define  SIZ_CLCA_LAND         10
#define  SIZ_CLCA_IMPR         10
#define  SIZ_FIXTR             10
#define  SIZ_PERSPROP          10
#define  SIZ_PP_MH             10
#define  SIZ_HOMESITE          10
#define  SIZ_TREEVINES         10
#define  SIZ_MINERAL           10
#define  SIZ_TIMBER_VAL        10
#define  SIZ_BUSINV            10
#define  SIZ_GR_IMPR           10
#define  SIZ_OTH_IMPR          9
#define  SIZ_FIXTR_RP          9
#define  SIZ_HOUSEHOLD_PP      9
#define  SIZ_EXE_CD            4
#define  SIZ_PORCH_SF          5
#define  SIZ_FLOOR_SF          5
#define  SIZ_FLOOR1_SF         6
#define  SIZ_FLOOR2_SF         5
#define  SIZ_FLOOR3_SF         5
#define  SIZ_DECK_SF           5
#define  SIZ_PATIO_SF          5
#define  SIZ_FLATWORK_SF       5
#define  SIZ_GUESTHSE_SF       5
#define  SIZ_MISCIMPR_SF       5
#define  SIZ_RENTAL_SF         7
#define  SIZ_NBH_CODE          6
#define  SIZ_LATE_PENALTY      9
#define  SIZ_ZONECAT           20
#define  SIZ_EXE_TYPE          10
#define  SIZ_ZONE_JURIS        3
#define  SIZ_ZONE_DESC         80

#define  OFF_APN_S             1-1
#define  OFF_APN_D             15-1
#define  OFF_CO_NUM            32-1
#define  OFF_CO_ID             34-1
#define  OFF_STATUS            37-1    // 1 byte used - [Parcel Status]
#define  OFF_NBH_CODE          38-1    // Neighborhood code (EDX,SAC,TUO)
#define  OFF_ADDREC            44-1    // Indicate new record added by GrGr process (ORG)

#define  OFF_TRA               45-1
#define  OFF_NAME1             53-1
#define  OFF_NAME2             105-1   // Extend Name2 to 46 bytes 10/14/2013
#define  OFF_NAME_TYPE1        151-1   // These fields never used and can be overlaid
#define  OFF_NAME_TYPE2        152-1   // Not used
#define  OFF_ZONE_X1           153-1   // 16 bytes - PQZoning 1

#define  OFF_VEST              169-1
#define  OFF_S_UNITNOX         173-1   // 12 bytes
#define  OFF_ZONE              185-1
#define  OFF_USE_STD           195-1
#define  OFF_XCAREOF           198-1   // CareOf extension
#define  OFF_ETAL_FLG          218-1   // Y/N
#define  OFF_ROOF_TYPE         219-1   // 1 byte coded - [Roof Type]
#define  OFF_ROOF_MAT          220-1   // 1 byte coded - [Roof Material]
#define  OFF_PATIO_SF          221-1   // 5 bytes
#define  OFF_DECK_SF           226-1   // 5 bytes 
#define  OFF_USE_CO            231-1
#define  OFF_S_STRNUM          239-1
#define  OFF_S_STR_SUB         246-1
#define  OFF_S_DIR             249-1
#define  OFF_S_STREET          251-1
#define  OFF_S_SUFF            275-1
#define  OFF_S_UNITNO          280-1
#define  OFF_S_CITY            286-1
#define  OFF_S_ST              310-1
#define  OFF_S_ZIP             312-1
#define  OFF_S_ZIP4            317-1
#define  OFF_M_STRNUM          321-1
#define  OFF_M_STR_SUB         328-1
#define  OFF_M_DIR             331-1
#define  OFF_M_STREET          333-1
#define  OFF_M_SUFF            357-1
#define  OFF_M_UNITNO          362-1
#define  OFF_M_CITY            368-1
#define  OFF_M_ST              385-1
#define  OFF_M_ZIP             387-1
#define  OFF_M_ZIP4            392-1
#define  OFF_TRACT             396-1
#define  OFF_BLOCK             402-1
#define  OFF_LOT               408-1
#define  OFF_IMAPLINK          414-1

#define  OFF_FIXTR             430-1   // Fixture/Equiptment
#define  OFF_EXE_CD1           440-1
#define  OFF_EXE_CD2           444-1
#define  OFF_EXE_CD3           448-1
#define  OFF_OTHER_APN         452-1   // TE_NO for KER
#define  OFF_TE_NO             452-1   

#define  OFF_RATIO             466-1
#define  OFF_LAND              469-1
#define  OFF_IMPR              479-1
#define  OFF_OTHER             489-1
#define  OFF_EXE_TOTAL         499-1
#define  OFF_GROSS             509-1
#define  OFF_HO_FL             519-1
#define  OFF_MINERAL           520-1   // Mineral Rights
#define  OFF_PARCEL_FLG1       530-1   // For SCL
#define  OFF_PARCEL_FLG2       531-1
#define  OFF_TAX_CODE          532-1
#define  OFF_TAX_AMT           535-1
#define  OFF_DEL_YR            547-1
#define  OFF_TIMBER            551-1   
#define  OFF_AG_PRE            562-1   // Y/N
#define  OFF_ELEVATOR          563-1   // Y/N  (ALA,SCL)
#define  OFF_TOPO              564-1   // ALA, SCR, NEV
#define  OFF_BATH_1Q           565-1   // 1/4 baths
#define  OFF_BATH_3Q           567-1   // 3/4 baths

#define  OFF_ALT_APN           569-1
#define  OFF_SALE1_AMT         583-1
#define  OFF_SALE1_CODE        593-1   // Up to 2 bytes used
#define  OFF_FILLER_2          595-1   // 11 bytes avail

#define  OFF_SALE1_DOCTYPE     606-1   // Only 3 bytes used
#define  OFF_ZONE_X2           609-1   // 19 bytes - PQZoning 2
                                       
#define  OFF_MULTI_APN         628-1
#define  OFF_TRS               629-1   // T-R-S
#define  OFF_FILLER_3          641-1   // 2 bytes available

#define  OFF_TD1CODE           643-1   // 20 bytes - Do not use
#define  OFF_HOMESITE          653-1   // Homesite
#define  OFF_ZONECAT           663-1   // 20 bytes - Zone category or PQZoningType
#define  OFF_SELLER            683-1
#define  OFF_PREV_APN          707-1   // Previous APN
#define  OFF_SALE2_AMT         721-1
#define  OFF_SALE2_CODE        731-1
#define  OFF_ZONE_JURIS        733-1   // 3 bytes index
#define  OFF_FILLER_4          736-1   // 1 byte avail

#define  OFF_RENTAL_SF         737-1   // SAC
#define  OFF_SALE2_DOCTYPE     744-1   // 3 bytes used
#define  OFF_ZONE_X3           747-1   // 19 bytes - PQZoning 3

#define  OFF_TIMBER_VAL        766-1   // Timber values
#define  OFF_S_POSTDIR         776-1   // Situs post dir
#define  OFF_M_POSTDIR         778-1   // Mail post dir
#define  OFF_SALE3_AMT         780-1
#define  OFF_SALE3_CODE        790-1
#define  OFF_M_UNITNOX         792-1   // 11-byte extended unit number, longer version of M_UNITNO                                   
#define  OFF_SALE3_DOCTYPE     803-1   // Only 3 bytes used
#define  OFF_LATE_PENALTY      806-1   // SOL, RIV
#define  OFF_ZONE_X4           815-1   // 10 bytes - PQZoning 4

#define  OFF_TRANSFER_DT       825-1
#define  OFF_TREEVINES         833-1   // Tree/Vines
#define  OFF_TRANSFER_DOC      843-1
#define  OFF_SALE1_DT          855-1
#define  OFF_PERSPROP          863-1   // Personal Property
#define  OFF_SALE1_DOC         873-1
#define  OFF_SALE2_DT          885-1
#define  OFF_BUSINV            893-1   // BusInv or BusProp
#define  OFF_SALE2_DOC         903-1
#define  OFF_SALE3_DT          915-1
#define  OFF_PP_MH             923-1   // PPMH
#define  OFF_SALE3_DOC         933-1
#define  OFF_GR_IMPR           945-1   // Growing Impr/Lease val
#define  OFF_FULL_EXEMPT       955-1   // Total exempt flag
#define  OFF_LOT_ACRES         956-1
#define  OFF_CLCA_LAND         965-1   // also called Williamson Act
#define  OFF_ROLLCHG_FLG       975-1   // Y/N
#define  OFF_LOT_SQFT          976-1
#define  OFF_YR_BLT            985-1
#define  OFF_YR_EFF            989-1
#define  OFF_CLCA_IMPR         993-1
#define  OFF_PROP8_FLG         1003-1  // Flag to indicate prop 8 property
#define  OFF_BLDG_SF           1004-1
#define  OFF_UNITS             1013-1  // Number of units
#define  OFF_STORIES           1019-1
#define  OFF_BEDS              1023-1
#define  OFF_BATH_F            1025-1
#define  OFF_BATH_H            1027-1
#define  OFF_ROOMS             1029-1
#define  OFF_BLDG_CLASS        1032-1
#define  OFF_BLDG_QUAL         1033-1  // 1 byte coded - [Quality]

#define  OFF_M_CITYX           1034-1  // 3 bytes - extended mail city name

#define  OFF_YR_ASSD           1037-1
#define  OFF_UNS_FLG           1041-1  // Unsecured flag - Y/N
#define  OFF_IMPR_COND         1042-1  // 1 byte coded - [Condition]

#define  OFF_QUALITYCLASS      1043-1  // 6 bytes.
#define  OFF_FILLER_6          1049-1  // 2 bytes avail
#define  OFF_FIRE_PL           1051-1  // 1 byte coded - [Fire Place]
#define  OFF_BLDGS             1053-1
#define  OFF_AIR_COND          1055-1  // 1 byte coded - [A/C]
#define  OFF_DOCLINKX          1056-1  // 22 bytes - over flow for DOCLINKS
#define  OFF_HEAT              1078-1  // 1 byte coded - [Heating]
#define  OFF_FLATWORK_SF       1079-1  // NEV
#define  OFF_GUESTHSE_SF       1084-1  // NEV
#define  OFF_MISCIMPR_SF       1089-1  // NEV, SAC, ALA
#define  OFF_PORCH_SF          1094-1  // NEV, VEN
#define  OFF_GAR_SQFT          1099-1
#define  OFF_PARK_TYPE         1105-1  // 1 byte coded - [Parking Type]
                                       
#define  OFF_FLOOR1_SF         1106-1  // VEN,SAC,SOL
#define  OFF_FLOOR2_SF         1112-1  // VEN,SAC,SOL
#define  OFF_FLOOR3_SF         1117-1  // VEN,SOL
#define  OFF_PARK_SPACE        1122-1  // Reduce park space from 6 bytes to 4 to match with STDCHAR
#define  OFF_FILLER_7          1126-1  // 2 bytes
#define  OFF_VIEW              1128-1
#define  OFF_STYLE             1130-1  // Reserved
#define  OFF_IMPR_TYPE         1131-1  // Reserved
#define  OFF_CONS_TYPE         1132-1  // Reserved

#define  OFF_USE_COX           1133-1  // 19 bytes Temporary used for RIV long UseCode
#define  OFF_DBA               1152-1  // DBA name
#define  OFF_BSMT_SF           1192-1
#define  OFF_S_HSENO           1198-1
#define  OFF_LEGAL2            1208-1  // 34 bytes
#define  OFF_OTH_IMPR          1242-1  // Other improve or value (ORG & SBX -Unit Val)/Living impr (SCR)
#define  OFF_FIXTR_RP          1251-1  // Fixture RP/Pers fixt (MNO, SIS, SON, TRI & TUO)
#define  OFF_HOUSEHOLD_PP      1260-1  // Household PP  (ALA)
#define  OFF_WATER             1269-1  // 1 byte used - 11-bytes avail
#define  OFF_SEWER             1281-1  // 1 byte used - 13-bytes avail
#define  OFF_POOL              1295-1  // 1 byte used - 11-bytes avail
#define  OFF_EXE_TYPE          1296-1  // H=Homeowners,D=Disabled Veterans,V=Veterans,C=Church,L=Library,M=Museums
                                       // R=Religious,T=Tribal Housing,W=Welfare,U=College,P=Public School,E=Cemetery
                                       // I=Hospital,S=School,
                                       
#define  OFF_LEGAL             1307-1  
#define  OFF_MAPDIV_LA         1347-1
#define  OFF_PUBL_FLG          1348-1
#define  OFF_CARE_OF           1349-1
#define  OFF_LEGAL1            1379-1  // 166 bytes
#define  OFF_MISC_IMPR         1545-1  // Y/N

#define  OFF_TAX_STAT          1546-1  // SBD (1=assd by county, 2=exempt, 3=assd by SBE, 4=common area, not assd)
#define  OFF_AMENITIES         1547-1  // ALA: (T)ennis court, (H)ot tub, (S)ecurity, (R)ecreation/exercise room
#define  OFF_FILLER_8          1548    // 1 byte available
#define  OFF_BATH_2Q           1549-1  // half baths
#define  OFF_BATH_4Q           1551-1  // full baths
#define  OFF_ZONE_DESC         1553-1  // Use 80 bytes
#define  OFF_DOCLINKS          1553-1
#define  OFF_MAPLINK           1633-1
#define  OFF_S_ADDR_D          1653-1
#define  OFF_S_CTY_ST_D        1705-1
#define  OFF_M_ADDR_D          1743-1
#define  OFF_M_CTY_ST_D        1795-1
#define  OFF_NAME_SWAP         1826-1
#define  OFF_TD1_AMT           1878-1  // Reserved
#define  OFF_TD2_AMT           1888-1  // Reserved
#define  OFF_AR_CODE1          1898-1
#define  OFF_AR_CODE2          1899-1
#define  OFF_AR_CODE3          1900-1
#define  OFF_N_SCR             1901-1
#define  OFF_N_LON             1905-1
#define  OFF_N_LAT             1915-1
#define  OFF_N_TRACT           1924-1
#define  OFF_N_DPBC            1933-1

#define  ACRES_FACTOR          1000
#define  SQFT_PER_ACRE         43560
#define  SQFT_FACTOR_10        4356
#define  SQFT_FACTOR_100       435.60
#define  SQFT_FACTOR_1000      43.560
#define  SQFT_MF_1             0.000022957
#define  SQFT_MF_1000          0.022957

#define  SALE_FACTOR           909.09091
#define  SALE_FACTOR_100       9.0909091

#define  R01_LEN               1900
#define  G01_LEN               1934
#define  R01_OPEN              1
#define  R01_CLOSE             2
#define  R01_SRCHAPN           4

#define  MAX_DEED_ENTRIES      512
#define  MAX_TRA_ENTRIES       100
#define  MAX_INST_ENTRIES      64

#define  SIZ_XTRAS             25

typedef struct _tAdr_Rec
{  // 180 bytes
   char  strNum[SIZ_M_STRNUM+1];    // 1
   long  lStrNum;                   // 9
   char  strSub[SIZ_M_STR_SUB+1];   // 13
   char  strDir[SIZ_M_DIR+1];       // 17
   char  strName[SIZ_M_STREET+1];   // 20
   char  strSfx[SIZ_M_SUFF+1];      // 45
   char  Unit[SIZ_M_UNITNO+1];      // 51
   char  City[SIZ_S_CITY+1];        // 58
   char  State[SIZ_M_ST+1];         // 83
   char  Zip[SIZ_M_ZIP+1];          // 86
   char  Zip4[SIZ_M_ZIP4+1];        // 92
   char  SfxCode[SIZ_M_SUFF+1];     // 97
   char  HseNo[SIZ_S_HSENO+1];      // 103
   char  Xtras[SIZ_XTRAS+1];        // 114
   char  SfxName[SIZ_M_SUFF+1];     // 140
   char  CityIdx[4];                // 146
   char  UnitNox[SIZ_S_UNITNOX+1];  // 150
   char  filler[17];                // 163
} ADR_REC;

typedef struct _tAdrRec1
{  // 80 chars
   char  strNum[SIZ_S_STRNUM];   // 1  - 432
   char  strSub[SIZ_S_STR_SUB];  // 8  - 438
   char  strDir[SIZ_S_DIR];      // 11 - 441
   char  strName[SIZ_S_STREET];  // 13 - 443
   char  strSfx[SIZ_S_SUFF];     // 37 - 467
   char  PostDir[SIZ_S_DIR];     // 42 - 472
   char  Unit[SIZ_S_UNITNO];     // 44 - 474
   char  City[SIZ_S_CITY];       // 50 - 480
   char  State[SIZ_S_ST];        // 74 - 504
   char  Zip[SIZ_S_ZIP];         // 76 - 506
} ADR_REC1;

typedef struct _tExtrAdrRec
{
   char  Apn[SIZ_APN_S];         // 1
   char  strNum[SIZ_S_STRNUM];   // 15
   char  strSub[SIZ_S_STR_SUB];  // 22
   char  strDir[SIZ_S_DIR];      // 25
   char  strName[SIZ_S_STREET];  // 27
   char  strSfx[SIZ_S_SUFF];     // 51
   char  Unit[SIZ_S_UNITNO];     // 56
   char  City[SIZ_S_CITY];       // 62
   char  State[SIZ_S_ST];        // 86
   char  Zip[SIZ_S_ZIP];         // 88
   char  Zip4[SIZ_S_ZIP4];       // 93
   char  Addr1[SIZ_S_ADDR_D];    // 97
   char  Addr2[SIZ_S_CTY_ST_D];  // 149
   char  FromSrc;                // 187
   char  HseNo[SIZ_S_HSENO];     // 188
   char  Xtras[SIZ_XTRAS];       // 198
   char  filler[32];             // 223
   char  CRLF[2];                // 255
} XADR_REC;

void parseOwners(char *pOwners, char *pOwner1, char *pOwner2, char *pSwapName);
void parseAdr1(ADR_REC *pAdrRec, char *pAdr);
void parseAdr1_1(ADR_REC *pAdrRec, char *pAdr);
void parseAdr1_2(ADR_REC *pAdrRec, char *pAdr);
void parseAdr1_3(ADR_REC *pAdrRec, char *pAdr);
void parseAdr1_4(ADR_REC *pAdrRec, char *pAdr, bool bReset);
void parseAdr1_5(ADR_REC *pAdrRec, char *pAdr, char *pApn);
void parseAdr1C(ADR_REC *pAdrRec, char *pAdr);
void parseAdr1C_1(ADR_REC *pAdrRec, char *pAdr);
void parseAdr1C_2(ADR_REC *pAdrRec, char *pAdr);   // SFX
void parseAdr1N(ADR_REC *pAdrRec, char *pAdr);
void parseAdr1N_1(ADR_REC *pAdrRec, char *pAdr);
void parseAdr1S(ADR_REC *pAdrRec, char *pAdr);
void parseAdr1S_1(ADR_REC *pAdrRec, char *pAdr);
void parseAdr1U(ADR_REC *pAdrRec, char *pAddr, bool bFlg=true);
void parseAdr2(ADR_REC *pAdrRec, char *pAdr);
void parseAdr2_1(ADR_REC *pAdrRec, char *pAdr);
void parseMAdr1(ADR_REC *pAdrRec, char *pAddr);
int  parseMAdr1_1(ADR_REC *pAdrRec, char *pAddr);
void parseMAdr1_2(ADR_REC *pAdrRec, char *pAddr);
void parseMAdr1_3(ADR_REC *pAdrRec, char *pAddr, bool bAddSign=true);
void parseMAdr1_4(ADR_REC *pAdrRec, char *pAddr);
void parseMAdr1_5(ADR_REC *pAdrRec, char *pAddr);
void parseMAdr2(ADR_REC *pAdrRec, char *pAddr);
void parseAdrND(ADR_REC *pAdrRec, char *pAdr);
void parseAdrNSD(ADR_REC *pAdrRec, char *pAdr);
void parseAdrDNS(ADR_REC *pAdrRec, char *pAdr);

void parseStreet(char *pInbuf, char *pStrName, char *pSuffix, char *pSfxCode=NULL);
void parseCitySt(char *pInbuf, char *pCity, char *pState, char *pCityCode=NULL);

void UpdateBlkLot(char *pOutbuf);
int  locateRecInR01(LPSTR pBuf, LPCSTR pSrchStr, int iSrchLen, int iCmd);
long extrSitus(char *pInfile, char *pOutfile, int iRecSize);
long extrZoning(char *pCnty, char *pInfile);
int  mergeXSitusFile(char *pInfile, char *pOutfile, char *pExtrfile);
void CopySitusToMailing(char *pOutbuf);
BOOL writeR01(HANDLE fh, LPSTR pBuf, int iLen);

// Return legal length
int  updateLegal(LPSTR pOutbuf, LPSTR pLegal);
int  updateCareOf(LPSTR pOutbuf, LPSTR pCareOf, int iMaxLen=0);
void removeNames(char *pRec, bool bRemCareof=true, bool bRemDBA=true);
void removeSitus(char *pOutbuf);
void removeMailing(char *pRec, bool bRemCareof=true, bool bRemDBA=false);
void removeLegal(char *pOutbuf);
void removeZoning(char *pOutbuf);
void removeDocLink(char *pOutbuf);
void removeLdrValue(char *pOutbuf);
void removeUseCode(char *pOutbuf);

long fixSitus(char *pInfile, char *pOutfile, int iSkip=1);
long fixOwners(char *pCnty, int iSkip=1);
long UpdDocDesc(char *pCnty, int iSkip);
int  updateDocLinks(void (*pfMakeDocLink)(char *, char *, char *), char *pCnty, int iSkip, int iUpdFlg=0, int iIdx = 1);
int  updatePrevApn(char *pCnty, int iPrevApnLen, int iSkip);

void updateSitusCity(char *pOutbuf);
char *getNameDir(char *pDir);
int  MergeArea(char *pCnty, char *pAcreage, int iSkip, bool bOverWrite=false);
bool isDir(char *pInbuf, char *pDir=NULL);
bool isDir1(char *pInbuf, char *pDir=NULL);
bool isDir2(char *pInbuf, char *pDir=NULL);
bool isDirx(char *pInbuf, char *pDir=NULL);

#endif