#ifndef  _MERGECOL_H_
#define  _MERGECOL_H_

#define  L6_ASMT                          0
#define  L6_FMT_APN                       1
#define  L6_TAXYEAR                       2
#define  L6_STATUS                        3
#define  L6_TRA                           4
#define  L6_FMT_TRA                       5
#define  L6_TAXABILITY                    6
#define  L6_OWNER                         7
#define  L6_ASSESSEE                      8
#define  L6_MAILADDRESS1                  9
#define  L6_MAILADDRESS2                  10
#define  L6_MAILADDRESS3                  11
#define  L6_MAILADDRESS4                  12
#define  L6_BARCODEZIP                    13
#define  L6_SITUS1                        14
#define  L6_FILLER1                       15
#define  L6_FILLER2                       16
#define  L6_LANDVALUE                     17
#define  L6_STRUCTUREVALUE                18
#define  L6_FIXTURESVALUE                 19
#define  L6_GROWING                       20
#define  L6_FIXTURESRP                    21
#define  L6_MHPPVALUE                     22
#define  L6_PPVALUE                       23
#define  L6_HOX                           24
#define  L6_OTHEREXEMPTION                25
#define  L6_OTHEREXEMPTIONCODE            26
#define  L6_LANDUSE1                      27
#define  L6_LANDSIZE                      28
#define  L6_ACRES                         29
#define  L6_BUILDINGTYPE                  30
#define  L6_QUALITYCLASS                  31
#define  L6_BUILDINGSIZE                  32
#define  L6_YEARBUILT                     33
#define  L6_TOTALAREA                     34
#define  L6_BEDROOMS                      35
#define  L6_BATHS                         36
#define  L6_HALFBATHS                     37
#define  L6_TOTALROOMS                    38
#define  L6_GARAGE                        39
#define  L6_GARAGESIZE                    40
#define  L6_HEATING                       41
#define  L6_AC                            42
#define  L6_FIREPLACE                     43
#define  L6_POOLSPA                       44
#define  L6_STORIES                       45
#define  L6_UNITS                         46
#define  L6_CURRENTDOCNUM                 47
#define  L6_CURRENTDOCDATE                48
#define  L6_CURRENTEXEMPTIONCODE2         49
#define  L6_CURRENTEXEMPTIONAMT2          50
#define  L6_CURRENTEXEMPTIONCODE3         51
#define  L6_CURRENTEXEMPTIONAMT3          52
#define  L6_CURRENTNETVALUE               53
#define  L6_BILLEDMARKETLANDVALUE         54
#define  L6_BILLEDFIXEDIMPRVALUE          55
#define  L6_BILLEDGROWINGIMPRVALUE        56
#define  L6_BILLEDSTRUCTURALIMPRVALUE     57
#define  L6_BILLEDPERSONALPROPVALUE       58
#define  L6_PROPMHVALUE                   59
#define  L6_BILLEDEXEMPTIONCODE1          60
#define  L6_BILLEDEXEMPTIONAMT1           61
#define  L6_BILLEDEXEMPTIONCODE2          62
#define  L6_BILLEDEXEMPTIONAMT2           63
#define  L6_BILLEDEXEMPTIONCODE3          64
#define  L6_BILLEDEXEMPTIONAMT3           65
#define  L6_BILLEDNETVALUE                66
#define  L6_PRIORMARKETLANDVALUE          67
#define  L6_PRIORFIXEDIMPRVALUE           68
#define  L6_PRIORGROWINGIMPRVALUE         69
#define  L6_PRIORSTRUCTURALIMPRVALUE      70
#define  L6_PRIORPERSONALPROPVALUE        71
#define  L6_PRIORPERSONALPROPMHVALUE      72
#define  L6_PRIOREXEMPTIONCODE1           73
#define  L6_PRIOREXEMPTIONAMT1            74
#define  L6_PRIOREXEMPTIONCODE2           75
#define  L6_PRIOREXEMPTIONAMT2            76
#define  L6_PRIOREXEMPTIONCODE3           77
#define  L6_PRIOREXEMPTIONAMT3            78
#define  L6_PRIORNETVALUE                 79

#define  COL_CHAR_FEEPRCL                 0
#define  COL_CHAR_DOCNUM                  1
#define  COL_CHAR_CATTYPE                 2
#define  COL_CHAR_BLDGSEQNO               3
#define  COL_CHAR_UNITSEQNO               4
#define  COL_CHAR_YRBLT                   5
#define  COL_CHAR_BUILDINGTYPE            6
#define  COL_CHAR_EFFYR                   7
#define  COL_CHAR_BUILDINGSIZE            8
#define  COL_CHAR_BUILDINGUSEDFOR         9
#define  COL_CHAR_STORIESCNT              10
#define  COL_CHAR_UNITSCNT                11
#define  COL_CHAR_CONDITION               12
#define  COL_CHAR_BEDROOMS                13
#define  COL_CHAR_BATHROOMS               14
#define  COL_CHAR_QUALITYCLASS            15
#define  COL_CHAR_HALFBATHS               16
#define  COL_CHAR_CONSTRUCTION            17
#define  COL_CHAR_FOUNDATION              18
#define  COL_CHAR_STRUCTURALFRAME         19
#define  COL_CHAR_STRUCTURALFLOOR         20
#define  COL_CHAR_EXTERIORTYPE            21
#define  COL_CHAR_ROOFCOVER               22
#define  COL_CHAR_ROOFTYPEFLAT            23
#define  COL_CHAR_ROOFTYPEHIP             24
#define  COL_CHAR_ROOFTYPEGABLE           25
#define  COL_CHAR_ROOFTYPESHED            26
#define  COL_CHAR_INSULATIONCEILINGS      27
#define  COL_CHAR_INSULATIONWALLS         28
#define  COL_CHAR_INSULATIONFLOORS        29
#define  COL_CHAR_WINDOWPANESINGLE        30
#define  COL_CHAR_WINDOWPANEDOUBLE        31
#define  COL_CHAR_WINDOWPANETRIPLE        32
#define  COL_CHAR_WINDOWTYPE              33
#define  COL_CHAR_LIGHTING                34
#define  COL_CHAR_COOLINGCENTRALAC        35
#define  COL_CHAR_COOLINGEVAPORATIVE      36
#define  COL_CHAR_COOLINGROOMWALL         37
#define  COL_CHAR_COOLINGWINDOW           38
#define  COL_CHAR_HEATING                 39
#define  COL_CHAR_FIREPLACE               40
#define  COL_CHAR_GARAGE                  41
#define  COL_CHAR_PLUMBING                42
#define  COL_CHAR_SOLAR                   43
#define  COL_CHAR_BUILDER                 44
#define  COL_CHAR_BLDGDESIGNEDFOR         45
#define  COL_CHAR_MODELDESC               46
#define  COL_CHAR_UNFINAREASSF            47
#define  COL_CHAR_ATTACHGARAGESF          48
#define  COL_CHAR_DETACHGARAGESF          49
#define  COL_CHAR_CARPORTSF               50
#define  COL_CHAR_DECKSSF                 51
#define  COL_CHAR_PATIOSF                 52
#define  COL_CHAR_CEILINGHEIGHT           53
#define  COL_CHAR_FIRESPINKLERS           54
#define  COL_CHAR_AVGWALLHEIGHT           55
#define  COL_CHAR_BAY                     56
#define  COL_CHAR_DOCK                    57
#define  COL_CHAR_ELEVATOR                58
#define  COL_CHAR_ESCALATOR               59
#define  COL_CHAR_ROLLUPDOOR              60
#define  COL_CHAR_FIELD1                  61
#define  COL_CHAR_FIELD2                  62
#define  COL_CHAR_FIELD3                  63
#define  COL_CHAR_FIELD4                  64
#define  COL_CHAR_FIELD5                  65
#define  COL_CHAR_FIELD6                  66
#define  COL_CHAR_FIELD7                  67
#define  COL_CHAR_FIELD8                  68
#define  COL_CHAR_FIELD9                  69
#define  COL_CHAR_FIELD10                 70
#define  COL_CHAR_FIELD11                 71
#define  COL_CHAR_FIELD12                 72
#define  COL_CHAR_FIELD13                 73
#define  COL_CHAR_FIELD14                 74
#define  COL_CHAR_FIELD15                 75
#define  COL_CHAR_FIELD16                 76
#define  COL_CHAR_FIELD17                 77
#define  COL_CHAR_FIELD18                 78
#define  COL_CHAR_FIELD19                 79
#define  COL_CHAR_FIELD20                 80
#define  COL_CHAR_LANDFIELD1              81
#define  COL_CHAR_LANDFIELD2              82
#define  COL_CHAR_LANDFIELD3              83
#define  COL_CHAR_LANDFIELD4              84
#define  COL_CHAR_LANDFIELD5              85
#define  COL_CHAR_LANDFIELD6              86
#define  COL_CHAR_LANDFIELD7              87
#define  COL_CHAR_LANDFIELD8              88
#define  COL_CHAR_ACRES                   89
#define  COL_CHAR_NEIGHBORHOODCODE        90
#define  COL_CHAR_ZONING                  91
#define  COL_CHAR_TOPOGRAPHY              92
#define  COL_CHAR_VIEWCODE                93
#define  COL_CHAR_POOLSPA                 94
#define  COL_CHAR_WATERSOURCE             95
#define  COL_CHAR_SUBDIVNAME              96
#define  COL_CHAR_SEWERCODE               97
#define  COL_CHAR_UTILITIESCODE           98
#define  COL_CHAR_ACCESSCODE              99
#define  COL_CHAR_LANDSCAPE               100
#define  COL_CHAR_PROBLEMCODE             101
#define  COL_CHAR_FRONTAGE                102
#define  COL_CHAR_LOCATION                103
#define  COL_CHAR_PLANTEDACRES            104
#define  COL_CHAR_ACRESUNUSEABLE          105
#define  COL_CHAR_WATERSOURCEDOMESTIC     106
#define  COL_CHAR_WATERSOURCEIRRIGATION   107
#define  COL_CHAR_HOMESITES               108
#define  COL_CHAR_PROPERTYCONDITIONCODE   109
#define  COL_CHAR_ROADTYPE                110
#define  COL_CHAR_HASWELL                 111
#define  COL_CHAR_HASCOUNTYROAD           112
#define  COL_CHAR_HASVINEYARD             113
#define  COL_CHAR_ISUNSECUREDBUILDING     114
#define  COL_CHAR_HASORCHARD              115
#define  COL_CHAR_HASGROWINGIMPRV         116
#define  COL_CHAR_SITECOVERAGE            117
#define  COL_CHAR_PARKINGSPACES           118
#define  COL_CHAR_EXCESSLANDSF            119
#define  COL_CHAR_FRONTFOOTAGESF          120
#define  COL_CHAR_MULTIPARCELECON         121
#define  COL_CHAR_LANDUSECODE1            122
#define  COL_CHAR_LANDUSECODE2            123
#define  COL_CHAR_LANDSQFT                124
#define  COL_CHAR_TOTALROOMS              125
#define  COL_CHAR_NETLEASABLESF           126
#define  COL_CHAR_BLDGFOOTPRINTSF         127
#define  COL_CHAR_OFFICESPACESF           128
#define  COL_CHAR_NONCONDITIONSF          129
#define  COL_CHAR_MEZZANINESF             130
#define  COL_CHAR_PERIMETERLF             131
#define  COL_CHAR_ASMT                    132
#define  COL_CHAR_ASMTCATEGORY            133
#define  COL_CHAR_EVENTDATE               134
#define  COL_CHAR_SALESPRICE              135      // Confirmed
#define  COL_CHAR_CONFCODE                136      // Confirmed code
#define  COL_CHAR_DISTANCE                137      // Distance
#define  COL_CHAR_DOCCODE                 138      // DocCode
#define  COL_CHAR_VALIDCOMP               139
#define  COL_CHAR_FLDS                    140

static XLAT_CODE  asHeating[] = 
{
// Value, lookup code, value length
   "B",  "8", 1,               // Baseboard
   "C",  "Z", 1,               // Central
   "E",  "7", 1,               // Electric 
   "FC", "Z", 2,               // Central Forced Air
   "F",  "C", 1,               // Floor
   "P",  "6", 1,               // Portable
   "S" , "K", 1,               // Solar
   "W" , "D", 1,               // Wall
   "Y" , "Y", 1,               // Yes
   "",   "",  0
};

//static XLAT_CODE  asCooling[] = 
//{
//// Value, lookup code, value length
//   "01", "C", 2,               // Central
//   "02", "E", 2,               // Evaporative Cooler
//   "03", "F", 2,               // Attic Fan
//   "04", "L", 2,               // Wall Unit
//   "05", "X", 2,               // Unknown
//   "06", "N", 2,               // None
//   "07", "F", 2,               // Whole house Fan 
//   "08", "X", 2,               // Multiple type
//   "99", "Y", 2,               // Building
//   "",   "",  0
//};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "99", "P", 2,               // Other
   "DB", "J", 2,               // Doughboy
   "FG", "F", 2,               // Fiberglass
   "GU", "G", 2,               // Gunite
   "UK", "U", 2,               // Unknown
   "VI", "V", 2,               // Vinyl
   "",   "",  0
};

//static XLAT_CODE  asParkType[] =
//{
//   // Value, lookup code, value length
//   "01", "A", 2,               // ATTACHED
//   "02", "D", 2,               // DETACHED
//   "03", "E", 2,               // BASEMENT
//   "04", "C", 2,               // CARPORT
//   "05", "2", 2,               // Multiple
//   "06", "Z", 2,               // Other
//   "07", "B", 2,               // BUILT-IN
//   "99", "Z", 2,               // Other
//   "AT", "A", 2,               // ATTACHED
//   "BA", "E", 2,               // BASEMENT
//   "D",  "D", 1,               // DETACHED
//   "N",  "N", 1,               // None
//   "",   "",  0
//};

static XLAT_CODE  asFirePlace[] =
{
   // Value, lookup code, value length
   "1",  "1", 1,               // 
   "2",  "2", 1,               // 
   "3",  "3", 1,               // 
   "4",  "4", 1,               // 
   "5",  "5", 1,               // 
   "99", "O", 2,               // OTHER
   "G",  "G", 1,               // GAS 
   "WP", "O", 2,               // 
   "",   "",  0
};

static XLAT_CODE  asView[] =
{
   //"09","Y", 2,               // VIEW OF OWN PROPERTY
   //"1", "Y", 1,               // View Code
   //"2", "N", 1,               // NO VIEW
   //"3", "1", 1,               // MINIMAL VIEW
   //"4", "4", 1,               // GOOD-LOCAL AREA
   //"5", "M", 1,               // Golfcourse
   //"6", "R", 1,               // VALLEY VIEW
   //"7", "T", 1,               // MOUNTAIN VIEW
   //"8", "O", 1,               // PANORAMIC VIEW
   //"9", "Y", 1,               // VIEW OF OWN PROPERTY
   //"11","Y", 2,               // FILTERED
   //"12","Y", 2,               // POSSIBLE
   "",  "",  0
};

static XLAT_CODE  asSewer[] =
{
   // Value, lookup code, value length
   //"01", "S", 2,               // SEPTIC TANK
   //"02", "L", 2,               // COMM LEACHFIELD
   //"03", "A", 2,               // COMMUNITY SEWER
   //"04", "S", 2,               // ENG SEPTIC
   //"05", "S", 2,               // ALT. SEPTIC
   //"09", "X", 2,               // NEEDS SEPTIC
   //"17", "U", 2,               // UNKNOWN
   //"19", "Y", 2,               // OTHER
   //"99", "Y", 2,               // OTHER
   "",   "",  0
};

// 10/31/2023
static XLAT_CODE  asCond[] =
{
   // Value, lookup code, value length
   "GD", "G", 2,               // Good
   "AV", "A", 2,               // Average
   "F",  "F", 1,               // Fair
   "P",  "P", 1,               // Poor
   "E",  "E", 1,               // Excellent
   "",   "",  0
};

static XLAT_CODE  asWaterSrc[] = 
{
   // Value, lookup code, value length
   //"01", "P", 2,               // Community
   //"02", "W", 2,               // Well
   //"03", "S", 2,               // Spring
   //"04", "Y", 2,               // Other
   //"05", "R", 2,               // Shared
   //"09", "F", 2,               // Canal
   //"99", "Y", 2,               // Rural land water (irrigation water)
   "",   "",  0
};

IDX_TBL5 COL_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "01", "19", 'N', 2, 2,     // Fed-State-County Property
   "04", "74", 'Y', 2, 2,     // Lot Line Adjustmt No Reappraisal
   "05", "1 ", 'N', 2, 2,     // New Mobile Home
   "09", "58", 'N', 2, 2,     // Possessory Interest
   "10", "19", 'Y', 2, 2,     // Split/Combo
   "11", "1 ", 'N', 2, 2,     // Transfer-100%
   "12", "57", 'N', 2, 2,     // Partial Interest Transfer
   "14", "61", 'Y', 2, 2,     // Trust/Irrevocable
   "15", "74", 'Y', 2, 2,     // Lot Line Adjustment/No Reappraisal
   "16", "74", 'Y', 2, 2,     // Dwelling Add'n/Conversion
   "17", "74", 'Y', 2, 2,     // Gar Add'n/Conv/Patio/Deck
   "18", "74", 'Y', 2, 2,     // Misc Add'n
   "20", "1 ", 'N', 2, 2,     // New Single Family Dwelling
   "21", "74", 'Y', 2, 2,     // New Garage/Carport
   "22", "74", 'Y', 2, 2,     // New Swimming Pool/spa
   "23", "1 ", 'N', 2, 2,     // New Multiple Bldg
   "24", "74", 'Y', 2, 2,     // Add'n/Conv To Multiples
   "25", "74", 'Y', 2, 2,     // Misc Multiples
   "33", "1 ", 'N', 2, 2,     // New Rural Bldg
   "34", "74", 'Y', 2, 2,     // Rural Bldg Add'n
   "35", "74", 'Y', 2, 2,     // Misc Rural Add'n, N.L.I.
   "36", "74", 'Y', 2, 2,     // Vineyard Add'n/Deletion
   "37", "74", 'Y', 2, 2,     // Land Improvements
   "38", "74", 'Y', 2, 2,     // Misc Comm'l
   "40", "1 ", 'N', 2, 2,     // New Comm'l Bldg
   "41", "74", 'Y', 2, 2,     // Add'n/Conv Comm'l
   "44", "1 ", 'N', 2, 2,     // New Industrial Bldg
   "45", "74", 'Y', 2, 2,     // Add'n/Conv Industrial
   "46", "74", 'Y', 2, 2,     // Misc Industrial
   "50", "74", 'Y', 2, 2,     // Non-Reappraisable Event
   "51", "1 ", 'N', 2, 2,     // Same Month Transfer-No Reappraisal
   "52", "75", 'Y', 2, 2,     // Trust Revocable No Reappraisal
   "58", "74", 'Y', 2, 2,     // Prop 58 Exclusion
   "63", "1 ", 'N', 2, 2,     // Ag Pres-New Bldg
   "64", "74", 'Y', 2, 2,     // Ag Pres-Add'n/Conv
   "65", "74", 'Y', 2, 2,     // Ag Pres-Misc
   "66", "74", 'Y', 2, 2,     // Ag Pres Vines Inc/Dec
   "70", "74", 'Y', 2, 2,     // Lien Date Update
   "72", "74", 'Y', 2, 2,     // Calamity
   "74", "80", 'Y', 2, 2,     // Board Order Value
   "75", "74", 'Y', 2, 2,     // Solar Improvement
   "80", "74", 'Y', 2, 2,     // Demo/Removal
   "86", "74", 'Y', 2, 2,     // No Change In Value/Permit
   "93", "74", 'Y', 2, 2,     // Prop 60 Exclusion
   "95", "74", 'Y', 2, 2,     // Bus Prop State Review
   "","",0,0,0
};


// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 COL_Exemption[] = 
{
   "E01", "H", 3,1,     // HOMEOWNERS EXEMPTION
   "E02", "D", 3,1,     // DISABLED VETERAN - BASIC EXEMPTION
   "E03", "W", 3,1,     // WELFARE EXEMPTION - UNSECURED
   "E04", "P", 3,1,     // PUBLIC SCHOOLS
   "E05", "X", 3,1,     // HISTORICAL  AIRCRAFT EXEMPTION
   "E06", "D", 3,1,     // DISABLED VETERAN - LOW INCOME EXEMPTION
   "E09", "R", 3,1,     // PARTIAL RELIGIOUS EXEMPTION
   "E10", "R", 3,1,     // RELIGIOUS EXEMPTION
   "E11", "W", 3,1,     // WELFARE EXEMPTION
   "E12", "C", 3,1,     // CHURCH EXEMPTION
   "E13", "X", 3,1,     // LESSOR EXEMPTION
   "E14", "S", 3,1,     // WELFARE EXEMPTION - PRIVATE PAROCHIAL SCHOOL
   "E16", "I", 3,1,     // WELFARE EXEMPTION - HOSPITAL - UNSECURED
   "E50", "X", 3,1,     // LOW VALUE EXEMPTION
   "E70", "W", 3,1,     // PARTIAL WELFARE EXEMPTION
   "E71", "W", 3,1,     // PARTIAL WELFARE EXEMPTION
   "E72", "W", 3,1,     // PARTIAL WELFARE EXEMPTION
   "E80", "C", 3,1,     // PARTIAL CHURCH EXEMPTION
   "E81", "C", 3,1,     // PARTIAL CHURCH EXEMPTION
   "E82", "C", 3,1,     // PARTIAL CHURCH EXEMPTION
   "E88", "D", 3,1,     // DUMMY - DISABLED VETERAN UNDER 40K
   "E89", "D", 3,1,     // DUMMY - DISABALE VETERAN NOT OVER 126,380
   "E90", "R", 3,1,     // PARTIAL RELIGIOUS EXEMPTION
   "E91", "R", 3,1,     // PARTIAL RELIGIOUS EXEMPTION
   "E92", "R", 3,1,     // PARTIAL RELIGIOUS EXEMPTION
   "E98", "X", 3,1,     // OTHER EXEMPTION
   "E99", "Y", 3,1,     // SOLDIERS & SAILORS RELIEF ACT
   "","",0,0
};

#endif
