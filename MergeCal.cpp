/**************************************************************************
 *
 * 2013: LDR file is AgencyCDSecured.xls.  We have to import this file into SQL
 *       then export it to delimited file AGENCYCDCURRSEC_TR601.txt using | as delimiter.
 *
 * Revision
 * 11/16/2005 1.0.0    First version by Tung Bui
 * 01/29/2007 1.8.2.2  Fix OtherVal algorithm to include Fixtures.
 * 01/30/2007 1.8.2.3  Fix loadCal() so when -L is run, everything is populated
 *                     not just lien data.
 * 08/03/2007 1.9.12   Skip header record if needed in Cal_Load_LDR()
 * 09/27/2007 1.9.17.1 Fix Cal_ConvertChar() and Cal_MergeChar() to fix Heating,
 *                     Cooling, and Pool problem.
 * 01/24/2008 1.10.3.1 Add code to support standard usecode
 * 02/29/2008 1.10.5   Add code to output update records.
 * 09/05/2008 8.1.1    CAL is now sending TR601 file format for LDR.  New function
 *                     MB_ExtrTR601() is created in LoadMB.cpp to extract lien values.
 * 09/05/2008 8.1.1    CAL is now sending TR601 file format for LDR.  Add function
 *                     Cal_Load_LDR(), Cal_MergeLien(), Cal_MergeSitus(),
 *                     Cal_MergeMAdr() for TR601 input format.
 * 09/07/2008 8.3.4    Modify Cal_MergeOwner() to keep original owner in Name1, 
 *                     Cal_Load_LDR() to remove the sort after processing, and 
 *                     Cal_MergeLien() to reformat UseCode & TRA - Sony
 * 09/08/2008 8.3.4.1  Add Exemption data to Cal_MergeLien()
 * 09/08/2008 8.3.4.2  Fix CareOf name format in Cal_MergeName()
 * 03/02/2009 8.6      Use MB_MergeSale() to do sale update instead of MergeSale3()
 *                     to fix problem that non-sale transaction overwrites sale 
 *                     transaction that occurs in the same day  since we only keep
 *                     one transaction per day.  Remove CreateCalRoll().
 * 03/11/2009 8.6.4    Add YrBlt and TotalRooms.
 * 03/27/2009 8.7      Drop DocType 4 (Stamp) and not using sale price on DocType 17 (Prop 58).
 *                     and adding option to clear old sales in Cal_LoadRoll().
 * 03/30/2009 8.7.1    Drop DocType 3 (non-appraisal event) too.
 * 05/21/2009 8.7.6    Remove reference to OldApn & NewApn files.  We no longer need them.
 * 07/30/2009 9.1.4    Modify Cal_Load_LDR(), Cal_MergeLien() and loadCal() to accept 
 *                     new LDR input format.
 * 11/23/2009 9.2.6    Remove all quotes from M_STR.
 * 11/30/2009 9.2.7.1  Remove all quotes from S_STR and use parseAdr1S_1() to parse
 *                     situs street name instead of parseAdr1S()
 * 09/02/2010 10.3.4   Overwrite EXE_TOTAL with GROSS if it is greater than GROSS.
 * 11/29/2010 10.5.0   Use updateTaxCode() to update TaxCode, Prop8, and Full Exempt flags
 * 04/21/2011 10.7.1   Add -Xs option to create cum sale file.
 * 06/01/2011 10.9.0   Exclude HomeSite from OtherVal.  Replace Cal_MergeExe() with MB_MergeExe()
 *                     and Cal_MergeTax() with MB_MergeTax().
 * 06/13/2011 10.10.1  Fix Cal_MergeChar() to handle multi record for a single APN. Change 
 *                     sort order in Cal_ConvertChar() to order YrBlt descending. Change sort 
 *                     order of Tax file in Cal_Load_Roll() to handle multi tax record of
 *                     an APN in a single year.
 * 06/14/2011 10.10.1  Fix Cal_ConvertChar() to ignore ROOMS if it's smaller than BEDS,
 *                     change sort order on ASMT, YRBLT, and BEDS to pick latest one.
 * 09/24/2011 11.5.5   Modify Cal_MergeSale() to allow update XFER. Modify Cal_MergeLien()
 *                     due to new delimiter used in TR601 file (tab instead of comma).
 *                     Use MB_ExtrTR601() instead of MB_ExtrLien() to extract LDR value.
 * 12/17/2011 11.8.5   Modify for new data files from CAL.
 * 12/20/2011 11.8.5.1 Add Units. Delete old file before moving to production folder for processing.
 *            11.8.5.4 Use input file as is, no sorting.  Fix bug in Cal_Load_Roll().
 * 03/16/2012 11.12.1  Check for corrupted CHAR file.
 * 05/21/2012 11.14.3  Fix cum sale (reset NumOfPrclXfer and set MultiSale_Flg='Y'), Check for 
 *                     bad DocTax (>20000).  Need review of DocType and correct translation.
 * 10/14/2012 12.3.4   Fix CHAR file bug.
 * 04/12/2013 12.8.0   Remove EXEAMT and update records with LDR value.
 * 08/19/2013 13.9.13  Modify Cal_Load_LDR() and add Cal_ExtrLien() to support new LDR format
 * 10/02/2013 13.14.0  Use updateVesting() to update Vesting and Etal flag.
 * 10/15/2013 13.17.1  Fix CareOf bug in Cal_MergeName(). Fix DocNum format in Cal_MergeRoll().
 * 01/03/2013 13.19.0  Populate S_HSENO.
 * 01/12/2014 13.19.0  Add DocCode[] table and modify Cal_CreateSCSale() to use this table
 *                     to update DocType.
 * 01/20/2014 13.20.1  Modify DocCode[] to make FORCLOSURE non-sale event.
 * 08/21/2014 14.3.1   Add MailZip to OFF_M_CTY_ST_D if available.
 * 09/15/2014 14.4.1   Modify Cal_MergeLien() & Cal_ExtrLienRec() to support new LDR layout.
 * 05/08/2015 14.15.6  Fix bug that get wrong file size in CHAR file.
 * 07/31/2015 15.2.0   Add DBA to Cal_MergeName()
 * 08/12/2015 15.2.3   Fix bug in Cal_MergeName()
 * 01/04/2016 15.9.0   Add option to load TC file by calling Load_TC()
 * 01/13/2016 15.10.1  Remove old situs in Cal_MergeSitus() before update to avoid remnant from old addr.
 * 08/20/2016 16.4.2   Move all tables to .H file.  Modify Cal_MergeLien(), Cal_ExtrLienRec()
 *                     to support new LDR layout.  Add LOTACRES, LOTSQFT, UNITS, & PARKTYPE using LDR file.
 * 07/04/2017 17.1.2   Modify Cal_Load_Roll() to dedup roll file before processing
 * 07/18/2017 17.1.4   Add Cal_MergeLien3() & Cal_ExtrLienRec3() to support new LDR records
 *                     Add -Xa option to extract CHAR independently from load update or LDR
 * 03/27/2018 17.9.0   Add -Ut option to update tax.
 * 08/01/2018 18.2.2   Add Cal_MergeLien7() & Cal_ExtrLienRec7() to support new 2018 LDR file.
 * 01/07/2019 18.9.4   Add Cal_ConvStdChar() and Cal_MergeStdChar() to support MI extract.
 *                     Modify Cal_Load_Roll() to use MergeStdChar() instead of MergeChar().
 *            18.9.4.1 Fix Sewer code in Cal_ConvStdChar()
 * 07/30/2019 19.1.1   Add Cal_ExtrLienRec12(), Cal_MergeLien12(), Cal_MergeZone(). 
 *                     Modify Cal_ExtrLien() & Cal_Load_LDR() to support new layout 2019. Use
 *                     Cal_MergeZone() to update Acres, Zoning, legal and Transfer info in R01.
 * 04/27/2020 19.9.0   Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 08/04/2020 20.2.5   Modify Cal_MergeMAdr() to fix special char 0xA5.  Modify Cal_MergeLien12()
 *                     to support new layout that includes characteristics.
 * 08/06/2020 20.2.6   Modify Cal_Load_Roll() to remove merge EXE since the file no longer available.
 * 10/31/2020 20.4.2   Modify Cal_MergeRoll(), Cal_MergeZone(), & Cal_MergeStdChar() to populate default PQZoning.
 * 01/11/2021 20.5.1   Modify Cal_CreateSCSale() to ignore records that is not 'R' type.
 * 03/17/2021 20.8.1   Cleanup known bad char in Cal_MergeName().
 * 08/05/2021 21.1.5   Add iRollAdj.  Modify Cal_MergeStdChar() to populate QualityClass.
 *                     Modify Cal_MergeLien12() & Cal_ExtrLienRec12() to make adjustment for 2021.
 *                     Modify Cal_Load_LDR() to use situs from LDR file instead of MergeSitus().
 * 09/28/2021 21.2.5   Fix Unzip problem when the zip file no longer in "\\Delivery" folder.
 * 07/29/2022 22.1.2   Modify Cal_Load_LDR() & loadCal() to support LDR layout change.
 * 06/07/2023 22.9.3   Fix bug when there is quote within quote in Cal_MergeRoll().
 * 07/25/2023 23.0.7   Add Cal_MergeLien32() and modify Cal_Load_LDR() to support new LDR layout.
 * 09/19/2023 23.1.11  Modify Cal_ConvStdChar() to update new format of QUALITYCLASS.
 * 03/20/2024 23.7.1   Fix APN_D in Cal_MergeLien32().
 * 07/02/2024 24.0.0   Modify Cal_MergeLien3() to add ExeType.  Modify Cal_Load_LDR() to support 2024 layout.
 *
 ***************************************************************************/

#include "stdafx.h"

#include "doZip.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "doSort.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "MergeCal.h"
#include "UseCode.h"
#include "Update.h"
#include "MBExtrn.h"
#include "PQ.h"
#include "Tax.h"
#include "CharRec.h"

extern int MB_MergeExe(char *pOutbuf);
extern int MB_MergeTax(char *pOutbuf);

// LDR roll adjustment, default 0
static int iRollAdj; 

/***************************** Cal_CreateSCSale ******************************
 *
 * Extract sale data from ???_Sales.csv and output to ???_SALE.DAT
 * Input:  ???_Sales.csv in acSaleFile
 * Output: ???_Sale.dat (SCSAL_REC format).  It is also appended to acCSalFile
 *
 * DateFmt: 
 *    0 : Default. Auto format by checking input date
 *    MM_DD_YYYY_1 (MON)
 *    YYYY_MM_DD   (HUM)
 *
 * DocTypeFmt: 
 *    0 : Default. Set DocType=1 (GD) if DocCode=1 or sale price > 0
 *    4 : CAL
 *
 * DocNumFmt: 
 *    0 : Default. Copy DocNum as is
 *    1 : Format DocNum second part of DocNum to 7 digits (i.e. 2010R1234 = 2010R0001234)
 *    
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Cal_CreateSCSale(int iDateFmt, bool bAppend)
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec;

   int      iTmp;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp;
   bool     bGrpSale;

   LogMsg("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSalesFile);
   fdSale = fopen(acSalesFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSalesFile);
      return -1;
   }

   // Skip header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets(acRec, 1024, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (!iSkipQuote)
         quoteRem(acRec);

      // Parse input rec
      if (cDelim == ',')
         iTokens = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      else
         iTokens = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens <= MB_SALES_XFERTYPE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[MB_SALES_ASMT], "006014025000", 9))
      //   iTmp = 0;
#endif

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] <= ' ' || *apTokens[MB_SALES_DOCDATE] <= ' ')
         continue;

      // 01/11/2021 - spn
      if (*(4+apTokens[MB_SALES_DOCNUM]) != 'R')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(SaleRec.Apn, apTokens[MB_SALES_ASMT], strlen(apTokens[MB_SALES_ASMT]));

      // Docnum
      lTmp = atoin(apTokens[MB_SALES_DOCNUM]+5, 7);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%.5s%0.7ld", apTokens[MB_SALES_DOCNUM], lTmp);
         memcpy(SaleRec.DocNum, acTmp, SALE_SIZ_DOCNUM);
      } else
         memcpy(SaleRec.DocNum, apTokens[MB_SALES_DOCNUM], strlen(apTokens[MB_SALES_DOCNUM]));

      // Doc date
      if (iDateFmt > 0)
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
      else
      {
         // Detect date format
         strcpy(acTmp, apTokens[MB_SALES_DOCDATE]);
         if (acTmp[4] == '-')
         {
            iDateFmt = YYYY_MM_DD;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else if (strchr(acTmp, '/'))
         {
            iDateFmt = MM_DD_YYYY_1;
            pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, iDateFmt);
         } else
         {
            LogMsg("*** Unknown date format %s", acTmp);
            pTmp = NULL;
         }
      }

      if (pTmp)
      {
         memcpy(SaleRec.DocDate, acTmp, 8);
         lTmp = atoin(acTmp, 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }

      // Group sale?
      myLTrim(apTokens[MB_SALES_GROUPSALE]);
      if (*apTokens[MB_SALES_GROUPSALE] == '1' || *apTokens[MB_SALES_GROUPSALE] == 'T')
      {
         bGrpSale = true;
         SaleRec.MultiSale_Flg = 'Y';
         if (*apTokens[MB_SALES_GROUPASMT] > ' ')
            memcpy(SaleRec.PrimaryApn, apTokens[MB_SALES_GROUPASMT], strlen(apTokens[MB_SALES_GROUPASMT]));
      } else
         bGrpSale = false;

      lPrice=0;

      // Doc code 
      if (*apTokens[MB_SALES_DOCCODE] > ' ')
      {
         iTmp = findDocType(apTokens[MB_SALES_DOCCODE], &CAL_DocCode[0]);
         if (iTmp >= 0)
         {
            memcpy(SaleRec.DocType, CAL_DocCode[iTmp].pCode, CAL_DocCode[iTmp].iCodeLen);
            if (lPrice < 100)
               SaleRec.NoneSale_Flg = CAL_DocCode[iTmp].flag;
         }

         // Save original DocCode
         vmemcpy(SaleRec.DocCode, apTokens[MB_SALES_DOCCODE], SALE_SIZ_DOCCODE);
      }

#ifdef _DEBUG
      //if (!memcmp(SaleRec.Apn, "001160016000", 9))
      //   iTmp = 0;
#endif
      // Tax
      dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
      if (acTmp[0] > '0')
      {
         // Save DocTax
         dTmp = atof(acTmp);
         iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);

         // Calculate sale price
         lTmp = (long)(dTmp * SALE_FACTOR);
         iTmp = ((int)dTmp/100)*100;

         // Check for bad DocTax
         if (dTmp > 100000)
         {
            if (iTmp == (int)dTmp)
            {
               LogMsg("*** (1) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
            {
               LogMsg("??? (1) Questionable Sale Tax Amt for %.12s: DocNum: %.12s, tax=%.2f, CP=%d.  Need investigation.  Ignore price.", SaleRec.Apn, SaleRec.DocNum, dTmp, lPrice);
               lPrice = 0;
            }
         } else if (iTmp == (int)dTmp && iTmp > 10000)
         {
            if (lTmp != (lTmp/100)*100)
            {
               LogMsg("*** (3) Questionable Sale Tax Amt for %.12s: tax=%.2f.  Use tax for sale price.", SaleRec.Apn, dTmp);
               lPrice = iTmp;
            } else
               lPrice = lTmp;
         } else
            lPrice = lTmp;

         // Check for questionable sale price
         if (lPrice > 5000000 && *apTokens[MB_SALES_GROUPSALE] != '1')
         {
            lTmp = (lPrice / 100)*100;
            if (lPrice != lTmp)
            {
               LogMsg("--> Questionable trans APN=%.12s, DocNum=%.12s, Date=%.8s, Price=%d \tTax=%.2f \tDOCCODE=%s: Ignore sale price.", 
                  SaleRec.Apn, SaleRec.DocNum, SaleRec.DocDate, lPrice, dTmp, apTokens[MB_SALES_DOCCODE]);
               lPrice = 0;
            }
         }  

         // Ignore sale price if less than 1000
         if (lPrice >= 10000)
            sprintf(acTmp, "%*d00", SALE_SIZ_SALEPRICE-2, lPrice/100);
         else if (lPrice >= 1000)
            sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lPrice);
         else
            memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
         memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);
      }

      // Transfer Type
      if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
            {
               SaleRec.SaleCode[0] = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      if (SaleRec.DocDate[0] > ' ')
      {
         // Seller
         iTmp = blankRem(apTokens[MB_SALES_SELLER]);
         vmemcpy(SaleRec.Seller1, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER, iTmp);

         // Buyer
         iTmp = blankRem(apTokens[MB_SALES_BUYER]);
         vmemcpy(SaleRec.Name1, apTokens[MB_SALES_BUYER], SALE_SIZ_BUYER, iTmp);

         SaleRec.CRLF[0] = 10;
         SaleRec.CRLF[1] = 0;
         fputs((char *)&SaleRec,fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH];
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(27,2,C,EQ,\"  \") F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);

   if (!lTmp)
      iTmp = -1;
   else if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acOutFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acOutFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acSrtFile, acTmp);
         if (lTmp > 0)
         {
            DeleteFile(acCSalFile);
   
            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -1;
      } else
         iTmp = rename(acOutFile, acCSalFile);
   } else
   {
      if (!_access(acCSalFile, 0))
         DeleteFile(acCSalFile);
      iTmp = rename(acOutFile, acCSalFile);
   }

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                          output: %d.", lTmp);
   LogMsg("         Latetest recording date: %d.", lLastRecDate);

   return iTmp;
}

/******************************** Cal_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Cal_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1, acName1[64], acName2[64], acOwners[64];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002003001000", 9))
   //   iTmp = 0;
#endif

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Save original owners
   strcpy(acOwners, acTmp);

   acName2[0] = 0;
   acSave1[0] = 0;

   if (pTmp=strstr(acTmp, " 1/2 INT"))
      *pTmp = 0;

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP);
      return;
   }

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
     if ( (pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " TR")))
        *pTmp = 0;

      // If TRUST appears after number, save from number forward
      if (strstr((char *)&acTmp[iTmp], " TRUST") || strstr((char *)&acTmp[iTmp], " LIVING") ||
        strstr((char *)&acTmp[iTmp], " REVOCABLE") )
      {
         iTmp--;
         strcpy(acSave1, (char *)&acTmp[iTmp]);
         acTmp[iTmp] = 0;
      }
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
      if ((pTmp=strstr(acName2, " ETAL")) || (pTmp=strstr(acName2, " ET AL")) )
         *pTmp = 0;
   } else if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Filter out words, things in parenthesis
   // MONDANI NELLIE M ESTATE OF

   if ( (pTmp = strchr(acTmp, '('))  || (pTmp=strstr(acTmp, " CO TR")) || (pTmp=strstr(acTmp, " CO-TR")) ||
      (pTmp=strstr(acTmp, " COTR")) ||
      (pTmp=strstr(acTmp, " LIFE ESTATE")) ||
      (pTmp=strstr(acTmp, " TRUSTEE")) ||
      (pTmp=strstr(acTmp, " TR ETAL")) ||
      (pTmp=strstr(acTmp, " TR")) ||
      (pTmp=strstr(acTmp, " TRES")) || (pTmp=strstr(acTmp, " TTEE")) ||
      (pTmp=strstr(acTmp, " ESTATE OF")) ||
      (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTS OF")) ||
      (pTmp=strstr(acTmp, " ETAL"))  || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " JT")) || (pTmp=strstr(acTmp, " CP")) ||
     (pTmp=strstr(acTmp, " LIVING")))
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " SUCCS-TR")) || (pTmp=strstr(acTmp, " SUCCESSOR")) ||
      (pTmp=strstr(acTmp, " SURVIVOR")) )
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);

   } else if (pTmp=strstr(acTmp, " FAMILY TRUST &"))
   {  // RUMAN FAMILY TRUST & RUMAN RICKY L & DANNY L
      // RUMAN RICKY L & RUMAN FAMILY TRUST &
      // RUMAN RICKY L & NANCY FAMILY TRUST & MARIE
      // VERMETTE JAMES & THERESA A & ZENDER ROBERT A &
      strcpy(acSave1, " FAMILY TRUST");
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " LIVING TRUST &"))
   {  // BOWERS CARLA LIVING TRUST & GRASS JAMES R TRUST
      strcpy(acSave1, " LIVING TRUST");
      strcpy(acName2, pTmp+16);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " REVOCABLE TRUST &"))
   {  // JENNINGS STEPHEN C REVOCABLE TRUST & SCARRONE CARO
      strcpy(acSave1, " REV TRUST");
      strcpy(acName2, pTmp+19);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST &"))
   {  // VAN VLECK STANLEY L TRUST & VAN VLECK VALERIE
      // ZIMMERMAN FAMILY 1990 TRUST & SWAYNE FAMILY 1998 T
      *(pTmp+6) = 0;
      pTmp1 = strstr(acTmp, " FAMILY");
      if (pTmp1)
      {
         strcpy(acSave1, pTmp1);
         *pTmp1 = 0;
      } else
      {
         strcpy(acSave1, pTmp);
         *pTmp = 0;
      }

      strcpy(acName1, acTmp);
      strcpy(acName2, pTmp+9);

   } else if (pTmp=strstr(acTmp, " LAND TRUST"))
   {
      strcpy(acSave1, " LAND TRUST");
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if ((pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " FAMILY "))
             || (pTmp=strstr(acTmp, " INCOME TR")) || (pTmp=strstr(acTmp, " LIVING TR")) )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST
      // Retrieve year as in KOBRIN EDWARD GEORGE & SHIRLEY JUNE 1991 LIVING TR
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " TRUST"))
   {  // FAGG DOUGLAS W & NORVA S TRUST
      // JAUCH CATHLEEN & DUANE 2003 TRUST
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " ESTATE OF"))
   {  // MONDANI NELLIE M ESTATE OF
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   if (pTmp = strstr(acSave1, " THE"))
      *pTmp = 0;

   // Split name into two if '/' presents
   if (pTmp = strchr(acName1, '/'))
   {  // CARRASCO HERBERT&DIANE/MOORE EVERETT&SHARON
      // Avoid 1/2 INT
      if (!isdigit(*(pTmp-1)))
      {
         *pTmp++ = 0;
         if (pTmp1 = strchr(pTmp, '/'))
            *pTmp1 = 0;
         strcpy(acName2, pTmp);
      }
   }

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;
   if ((pTmp=strstr(acName2, " AS ")) || (pTmp=strstr(acName2, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      if (myOwner.acVest[0] > ' ')
         vmemcpy(pOutbuf+OFF_VEST, myOwner.acVest, SIZ_VEST);

      // Concat what in saved buffer
      if (acSave1[0])
      {
         if (acTmp1[strlen(acTmp1)-1] == ' ' && acSave1[0] == ' ')
            strcat(acTmp1, (char *)&acSave1[1]);      // avoid double blank
         else
            strcat(acTmp1, acSave1);
      }

      // If name is not swapable, use Name1 instead
      if (iRet == -1)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, SIZ_NAME_SWAP);
      else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwners, SIZ_NAME_SWAP);

   vmemcpy(pOutbuf+OFF_NAME1, acOwners, SIZ_NAME1);
}

/******************************** Cal_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************

void Cal_MergeMAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64];
   int      iTmp;
   char *    pTmp;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   memset(pOutbuf+OFF_M_STRNUM, ' ', SIZ_M_ADDR);
   memset(pOutbuf+OFF_M_ADDR_D, ' ', SIZ_M_ADDR_D);
   memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // CareOf
   if (*apTokens[MB_ROLL_CAREOF] > ' ')
   {
      iTmp = strlen(apTokens[MB_ROLL_CAREOF]);
      if (iTmp > SIZ_CARE_OF)
         iTmp = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, apTokens[MB_ROLL_CAREOF], iTmp);
   }

   // Mail address
   strcpy(acAddr1, apTokens[MB_ROLL_M_ADDR]);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   iTmp = strlen(sMailAdr.strName);
   if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
   {
      // Skip the first 9 bytes and search for next space after box #
      pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
      if (pTmp)
         *pTmp = 0;

     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     sMailAdr.strSfx[SIZ_M_SUFF] = 0;
   } else
   {
     sMailAdr.strName[SIZ_M_STREET] = 0;
     memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
     memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   }

   // Unit #
   if (sMailAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   // City/St - Zip
   if (*apTokens[MB_ROLL_M_CITY] > ' ')
   {
      memcpy(pOutbuf+OFF_M_CITY, apTokens[MB_ROLL_M_CITY], strlen(apTokens[MB_ROLL_M_CITY]));
      if (2 == strlen(apTokens[MB_ROLL_M_ST]))
         memcpy(pOutbuf+OFF_M_ST, apTokens[MB_ROLL_M_ST], 2);

      if (*apTokens[MB_ROLL_M_ZIP] >= '0')
      {
         iTmp = strlen(apTokens[MB_ROLL_M_ZIP]);
         if (iTmp > 9)
            iTmp = 9;
         memcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_ROLL_M_ZIP], iTmp);
         //strcpy(sMailAdr.Zip, apTokens[MB_ROLL_M_ZIP]);
      }

      iTmp = sprintf(acTmp, "%s %s", apTokens[MB_ROLL_M_CITY], apTokens[MB_ROLL_M_ST]);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, iTmp);
   }

}
*/

/******************************** Cal_MergeName ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Cal_MergeName(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdName);

      // Get first rec
      pRec = fgets(acRec, 512, fdName);
      if (acRec[2] == ' ')
         pRec = fgets(acRec, 512, fdName);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdName);
         fdName = NULL;
         return 1;      // EOF
      }

      // Add 1 to Name rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Name rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdName);
         lNameSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 0;

   // Replace tab char with 0
   iRet = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_NAME_ISFMT_ADDR)
   {
      LogMsg("***** Error: bad name record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   Cal_MergeOwner(pOutbuf, apTokens[MB_NAME_OWNER]);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "034003113000", 9))
   //   iTmp = 0;
#endif

   // Mail address
   ADR_REC sMailAdr;

   // Clear old Mailing    
   removeMailing(pOutbuf, true);

   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   if (*apTokens[MB_NAME_ISFMT_ADDR] == '1' || *apTokens[MB_NAME_ISFMT_ADDR] == 'T') 
   {
      // CareOf
      if (*apTokens[MB_NAME_CAREOF] > ' ')
         updateCareOf(pOutbuf, apTokens[MB_NAME_CAREOF], strlen(apTokens[MB_NAME_CAREOF]));

      // DBA
      memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
      if (*apTokens[MB_NAME_DBA] > ' ')
      {
         pTmp = apTokens[MB_NAME_DBA];
         if (!memcmp(pTmp, "DBA ", 4))
            pTmp += 4;
         vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA);
      } 

      // Mail address
      strcpy(acAddr1, apTokens[MB_NAME_M_ADDR]);

      // Fix known char
      if (pTmp = strchr(acAddr1, 0xD1))
         *pTmp = 'N';

      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

      // Parse mail address
      parseMAdr1(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      iTmp = strlen(sMailAdr.strName);
      if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
      {
         // Skip the first 9 bytes and search for next space after box #
         pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
         if (pTmp)
            *pTmp = 0;

        vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
        sMailAdr.strSfx[SIZ_M_SUFF] = 0;
      } else
      {
        vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
        vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      }

      // Unit #
      if (sMailAdr.Unit[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);

      // City/St - Zip
      if (*apTokens[MB_NAME_M_CITY] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_CITY, apTokens[MB_NAME_M_CITY], SIZ_M_CITY);
         if (2 == strlen(apTokens[MB_NAME_M_ST]))
            memcpy(pOutbuf+OFF_M_ST, apTokens[MB_NAME_M_ST], 2);

         if (*apTokens[MB_NAME_M_ZIP] >= '0')
         {
            iTmp = strlen(apTokens[MB_NAME_M_ZIP]);
            if (iTmp > 9)
               iTmp = 9;
            memcpy(pOutbuf+OFF_M_ZIP, apTokens[MB_NAME_M_ZIP], iTmp);
         }

         sprintf(acTmp, "%s %s %.5s", apTokens[MB_NAME_M_CITY], apTokens[MB_NAME_M_ST], pOutbuf+OFF_M_ZIP);
         iTmp = blankRem(acTmp);
         vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
      }
   } else if (*apTokens[MB_NAME_M_ADDR1] > ' ' && *apTokens[MB_NAME_M_ADDR2] > ' ')
   {
      int   iAdr1, iAdr2;

      if (*apTokens[MB_NAME_M_ADDR4] > ' ')
         iAdr2 = MB_NAME_M_ADDR4;
      else  if (*apTokens[MB_NAME_M_ADDR3] > ' ')
         iAdr2 = MB_NAME_M_ADDR3;
      else  if (*apTokens[MB_NAME_M_ADDR2] > ' ')
         iAdr2 = MB_NAME_M_ADDR2;
      iAdr1 = iAdr2 - 1;

      // Fix known char
      if (pTmp = strchr(apTokens[iAdr1], 0xD1))
         *pTmp = 'N';

      vmemcpy(pOutbuf+OFF_M_ADDR_D, apTokens[iAdr1], SIZ_M_ADDR_D);
      pTmp = strrchr(apTokens[iAdr2], ' ');
      if (pTmp && isdigit(*(pTmp-1)))
         *pTmp = '-';                  // 92029 1234
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, apTokens[iAdr2], SIZ_M_CTY_ST_D);

      // Parse address
      parseMAdr1(&sMailAdr, apTokens[iAdr1]);
      parseMAdr2(&sMailAdr, apTokens[iAdr2]);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
      }

      if (sMailAdr.strSub[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, SIZ_M_STR_SUB);
      if (sMailAdr.strDir[0] > ' ')
         *(pOutbuf+OFF_M_DIR) = sMailAdr.strDir[0];
      if (sMailAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      if (sMailAdr.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);

      // Copy unit#
      if (sMailAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));


      // city state
      if (sMailAdr.City[0] > ' ')
      {
         memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, strlen(sMailAdr.City));
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
      }

      // Zipcode
      lTmp = atoin(sMailAdr.Zip, SIZ_M_ZIP);
      if (lTmp > 400)
      {
         sprintf(acTmp, "%.5d", lTmp);
         memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
         if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
            memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
      }
   }

   // Get next record
   pRec = fgets(acRec, 512, fdName);
   lNameMatch++;

   return 0;
}

/******************************** Cal_MergeValue *****************************
 *
 * Merge lien value into roll record
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************

int Cal_MergeValue(char *pOutbuf, bool bVerb)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record - no need since input already been resorted
      //for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      //   pRec = fgets(acRec, 512, fdValue);
      // Get first rec
      pRec = fgets(acRec, 512, fdValue);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         return 1;      // EOF
      }

      // Add 1 to Value rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug && bVerb)
            LogMsg0("Skip value rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdValue);
         lValueSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input record
   iRet = ParseStringNQ(pRec, cDelim, MB_VALU_USERID+1, apTokens);
   if (iRet < MB_VALU_USERID)
   {
      LogMsg("***** Error: bad value record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Land
   long lLand = atoi(apTokens[MB_VALU_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[MB_VALU_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   // HomeSite is not avail. in CAL
   long lFixt  = atoi(apTokens[MB_VALU_FIXTR]);
   long lPP    = atoi(apTokens[MB_VALU_PERSPROP]);
   long lMH    = atoi(apTokens[MB_VALU_PP_MH]);
   long lHSite = atoi(apTokens[MB_VALU_HOMESITE]);
   long lGrow  = atoi(apTokens[MB_VALU_GROWING]);
   long lBusInv= atoi(apTokens[MB_VALU_BUSPROP]);
   lTmp = lFixt+lPP+lMH+lGrow+lBusInv;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
      if (lHSite > 0)
      {
         sprintf(acTmp, "%d         ", lHSite);
         memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lBusInv > 0)
      {
         sprintf(acTmp, "%d         ", lBusInv);
         memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
      }
   }

   // Gross total
   long lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdValue);
   lValueMatch++;

   return 0;
}

/******************************** Cal_MergeSitus *****************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Cal_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdSitus);

      // Get first rec
      pRec = fgets(acRec, 512, fdSitus);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 0;

   // Replace tab char with 0
   if (pRec)
      iRet = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_SITUS_ZIP+1)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old Situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[MB_SITUS_STRNUM]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%d         ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_S_HSENO, acTmp, SIZ_S_HSENO);
      pTmp = strchr(acTmp, ' ');
      *(pTmp+1) = 0;
      strcpy(acAddr1, acTmp);

      if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));

      if (*apTokens[MB_SITUS_STRDIR] > ' ')
      {
         strcat(acAddr1, apTokens[MB_SITUS_STRDIR]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[MB_SITUS_STRDIR], strlen(apTokens[MB_SITUS_STRDIR]));
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "014009019000", 9))
   //   iTmp = 0;
#endif

   char acStr[64];
   strcpy(acStr, apTokens[MB_SITUS_STRNAME]);
   quoteRem(acStr);

   if (*apTokens[MB_SITUS_STRTYPE] > ' ')
   {
      strcat(acAddr1, apTokens[MB_SITUS_STRNAME]);
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_STRTYPE]);

      strcpy(acTmp, acStr);
      blankPad(acTmp, SIZ_S_STREET);
      memcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET);

      iTmp = GetSfxCodeX(apTokens[MB_SITUS_STRTYPE], acTmp);
      if (iTmp > 0)
         Sfx2Code(acTmp, acCode);
      else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } else
   {
      ADR_REC sAdr;

      parseAdr1S_1(&sAdr, acStr);
      if (sAdr.strName[0] > ' ')
      {
         blankPad(sAdr.strName, SIZ_S_STREET);
         memcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      }
      if (sAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, strlen(sAdr.strSfx));

      if (sAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sAdr.strDir, strlen(sAdr.strDir));

      strcat(acAddr1, acStr);
   }

   if (*apTokens[MB_SITUS_UNIT] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
      memcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], strlen(apTokens[MB_SITUS_UNIT]));
   }

   blankRem(acAddr1, SIZ_S_ADDR_D);
   blankPad(acAddr1, SIZ_S_ADDR_D);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
   {
      Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1);
      blankPadz(acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      if (acAddr1[0] > ' ')
         sprintf(acTmp, "%s CA", myTrim(acAddr1));
      blankPad(acTmp, SIZ_S_CTY_ST_D);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/******************************** Cal_MergeSale ******************************
 *
 * Note:
 * 1) need to figure out DocType and translate to our index table
 * 2) cal_sales.csv needs to be sorted before processing
 *
 *****************************************************************************

int Cal_MergeSale(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp, iIdx, iDocType;
   long     lCurSaleDt, lPrice;
   double   dTmp;
   SALE_REC sCurSale;


   // Get rec
   if (!pRec)
   {
      // Skip header record
      //pRec = fgets(acRec, 512, fdSale);
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   do
   {
      if (!pRec)
         return 1;      // EOF

      // Add 1 to Sale rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdSale);
         lSaleSkip++;
      }
   } while (iTmp > 0);

   if (iTmp)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "098047002", 9))
   //   iRet = 0;
#endif

   while (!iTmp)
   {
      // Replace tab char with 0
      if (pRec)
         iRet = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iRet < MB_SALES_XFERTYPE+1)
      {
         LogMsg("***** Error: bad sale record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         iRet =  -1;
         break;
      }

      // Merge data - Only take sale rec that has both docnum and docdate
      if (*apTokens[MB_SALES_DOCNUM] > ' ' && *apTokens[MB_SALES_DOCDATE] > ' ')
      {
         memset((SALE_REC *)&sCurSale, ' ', sizeof(SALE_REC));

         // Docnum
         if (*(apTokens[MB_SALES_DOCNUM]+4) == 'R')
         {
            iIdx = atoi(apTokens[MB_SALES_DOCNUM]+5);
            sprintf(acTmp, "%.5s%.7d", apTokens[MB_SALES_DOCNUM], iIdx);
            memcpy(sCurSale.acDocNum, acTmp, SALE_SIZ_DOCNUM);
         } else
         {
            strncpy(sCurSale.acDocNum, apTokens[MB_SALES_DOCNUM], SALE_SIZ_DOCNUM);
            blankPad(sCurSale.acDocNum, SALE_SIZ_DOCNUM);
         }

         // DocDate & Sale price - if no docdate, ignore everything else
         pTmp = dateConversion(apTokens[MB_SALES_DOCDATE], acTmp, YYYY_MM_DD);
         if (pTmp)
         {
            // Doc date
            lCurSaleDt = atol(acTmp);
            memcpy(sCurSale.acDocDate, acTmp, 8);
         }

         iDocType = atol(apTokens[MB_SALES_DOCCODE]);

         // Tax
         dollar2Num(apTokens[MB_SALES_TAXAMT], acTmp);
         lPrice = 0;

         // Do not use sale price on Doccode 17
         if (acTmp[0] > '0' && iDocType != 17)
         {
            dTmp = atof(acTmp);
            lPrice = (dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(sCurSale.acSalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // DocType - need translation before production
         // 01, 02, 03, 04, 05, 06, 07, 08, 09, 15, 57, 92
         strcpy(sCurSale.acDocType, apTokens[MB_SALES_DOCCODE]);

         // Transfer Type
         if (lPrice > 0 && *apTokens[MB_SALES_XFERTYPE] > ' ')
         {
            while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
            {
               if (!memcmp(apTokens[MB_SALES_XFERTYPE], asSaleTypes[iTmp].pName, 2))
               {
                  sCurSale.acSaleCode[0] = *asSaleTypes[iTmp].pCode;
                  break;
               }
               iTmp++;
            }
         } else
            sCurSale.acSaleCode[0] = ' ';

         // Group sale?
         if (*apTokens[MB_SALES_GROUPSALE] > '0')
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';

         // sale code
         //acTmp[0] = toupper(*apTokens[MB_SALES_XFERTYPE]);
         //if (acTmp[0] == 'F')
         //   sCurSale.acSaleCode[0] = 'F';
         //else if (acTmp[0] == 'L')
         //   sCurSale.acSaleCode[0] = 'L';

         // Seller
         strncpy(sCurSale.acSeller, apTokens[MB_SALES_SELLER], SALE_SIZ_SELLER);
         blankPad(sCurSale.acSeller, SALE_SIZ_SELLER);

         // Skip Prop 58 document -> non-appraisal event

         // Drop Doccodes 
         // 4 Stamp
         // 3 non-reappraisal
         if (iDocType != 4 && iDocType != 3)
            MB_MergeSale(&sCurSale, pOutbuf, true, true);
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (pRec)
         iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      else
         break;
   }

   lSaleMatch++;

   // Update flag
   if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   if (*(pOutbuf+OFF_SALE2_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';
   if (*(pOutbuf+OFF_SALE3_DOC) > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';

   return iRet;
}

/******************************** Cal_MergeChar ******************************
 *
 * Note: need code table for Heating and Cooling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

int Cal_MergeChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acCode[32];
   long     lBldgSqft, lGarSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iFp, iRooms, iYrBlt;
   MB_CHAR  *pChar;

#ifdef _DEBUG
   char     *pTmp = &acRec[0];
#endif

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 1024, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.12s", pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (MB_CHAR *)pRec;

   while (!iLoop)
   {
      // Quality Class
      memcpy(acTmp, pChar->QualityClass, MBSIZ_CHAR_QUALITY);
      acTmp[MBSIZ_CHAR_QUALITY] = 0;
      if (isalpha(acTmp[0]))
      {
         *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0];

         iTmp = 0;
         while (acTmp[iTmp] && !isdigit(acTmp[iTmp]))
            iTmp++;

         if (acTmp[iTmp] > '0')
            iRet = Quality2Code((char *)&acTmp[iTmp], acCode, NULL);
         else
            acCode[0] = 0;
      } else if (acTmp[0] > '0' && acTmp[0] <= '9')
         iRet = Quality2Code(acTmp, acCode, NULL);
      else
         acCode[0] = 0;

      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, strlen(acCode));

      // Yrblt
      iYrBlt = atoin(pChar->YearBuilt, MBSIZ_CHAR_YRBLT);
      if (iYrBlt > 1700)
         memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);

      // BldgSqft
      lBldgSqft = atoin(pChar->BuildingSize, MBSIZ_CHAR_BLDGSQFT);
      if (lBldgSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      } else
         memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

      // Garage Sqft
      lGarSqft = atoin(pChar->SqFTGarage, MBSIZ_CHAR_GARSQFT);
      if (lGarSqft > 10)
      {
         sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = '2';
      } else
      {
         memcpy(pOutbuf+OFF_GAR_SQFT, BLANK32, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = ' ';
      }

      // Heating
      if (pChar->Heating[0] > ' ')
      {
         iTmp = 0;
         while (asHeating[iTmp].iLen > 0)
         {
            if (!memcmp(pChar->Heating, asHeating[iTmp].acSrc, asHeating[iTmp].iLen))
            {
               *(pOutbuf+OFF_HEAT) = asHeating[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }
      // Cooling
      if (pChar->Cooling[0] > ' ')
      {
         iTmp = 0;
         while (asCooling[iTmp].iLen > 0)
         {
            if (!memcmp(pChar->Cooling, asCooling[iTmp].acSrc, asCooling[iTmp].iLen) )
            {
               *(pOutbuf+OFF_AIR_COND) = asCooling[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }
      // Pool
      if (pChar->NumPools[0] > ' ')
      {
         iTmp = 0;
         while (asPool[iTmp].iLen > 0)
         {
            if (!memcmp(pChar->NumPools, asPool[iTmp].acSrc, asPool[iTmp].iLen) )
            {
               *(pOutbuf+OFF_POOL) = asPool[iTmp].acCode[0];
               break;
            }
            iTmp++;
         }
      }

      // Beds
      iBeds = atoin(pChar->NumBedrooms, MBSIZ_CHAR_BEDS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      } else
         memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

      // Bath
      iFBath = atoin(pChar->NumFullBaths, MBSIZ_CHAR_FBATHS);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      } else
         memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

      // Half bath
      iHBath = atoin(pChar->NumHalfBaths, MBSIZ_CHAR_HBATHS);
      if (iHBath > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      } else
         memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

      // Total rooms
      iRooms = atoin(pChar->TotalRooms, MBSIZ_CHAR_ROOMS);
      if (iRooms > 0)
      {
         sprintf(acTmp, "%*d", SIZ_ROOMS, iRooms);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      }
      // Take this off since new file doesn't include this field
      //else
      //   memcpy(pOutbuf+OFF_ROOMS, BLANK32, SIZ_ROOMS);

      // Fireplace
      iFp = atoin(pChar->NumFireplaces, MBSIZ_CHAR_FP);
      if (iFp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
         memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
      } else
         memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SIZ_FIRE_PL);

      // HasSeptic or HasSewer
      if (pChar->HasSeptic > '0')
         *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
      else if (pChar->HasSewer > '0')
         *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

      // HasWell
      *(pOutbuf+OFF_WATER) = pChar->HasWell;

      // Pool - ignore
      // There are 35 parcels that has NUMPOOLS=99.  This is invalid data and we
      // cannot use it without investigation


      lCharMatch++;

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (pRec)
         iLoop = memcmp(pOutbuf, pRec, iApnLen);
      else
         break;

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "020027186", 9))
      //   iTmp = 0;
#endif
      // Skip new record if YrBlt is older.
      if (!iLoop && memcmp(pOutbuf+OFF_YR_BLT, &pChar->YearBuilt[0], SIZ_YR_BLT) > 0)
         break;
   }

   return 0;
}

/******************************** Cal_MergeChar ******************************
 *
 * Note: need updated code table for FirePlace, Heating and Colling
 *   - CHAR file needs resort on ASMT for better matched on condo.
 *   - If parcel has more than one CHAR record, use the first one.
 *
 *****************************************************************************/

int Cal_MergeStdChar(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   long     lTmp;
   int      iRet, iLoop, iBeds, iFBath, iHBath, iTmp;
   STDCHAR  *pChar;

   iRet=iBeds=iFBath=iHBath=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 1024, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "044510005", 9))
   //   iRet = 0;
#endif

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)pRec;

   // Quality Class
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Impr condition
   *(pOutbuf+OFF_IMPR_COND) = pChar->ImprCond[0];

   // Yrblt
   long lYrBlt = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lYrBlt > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
   else
      memcpy(pOutbuf+OFF_YR_BLT, BLANK32, SIZ_YR_BLT);

   // YrEff
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 1900 && lTmp >= lYrBlt)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_EFF);
   else
      memcpy(pOutbuf+OFF_YR_EFF, BLANK32, SIZ_YR_EFF);

   // LotSqft
   long lLotSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lLotSqft > 100)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_ACRES);
   } 

   // BldgSqft
   long lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 100)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memcpy(pOutbuf+OFF_BLDG_SF, BLANK32, SIZ_BLDG_SF);

   // Garage Sqft
   long lSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   } else
   {
      memset(pOutbuf+OFF_GAR_SQFT, ' ', SIZ_GAR_SQFT);
   }

   // Park type
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // Heating
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   
   // Cooling 
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memcpy(pOutbuf+OFF_BEDS, BLANK32, SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memcpy(pOutbuf+OFF_BATH_F, BLANK32, SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memcpy(pOutbuf+OFF_BATH_H, BLANK32, SIZ_BATH_H);

   // Total rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   } else
      memcpy(pOutbuf+OFF_ROOMS, BLANK32, SIZ_ROOMS);
   
   // Fireplace
   memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);
   *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

   // HasSeptic or HasSewer   
   if (pChar->HasSewer > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSewer;
   else if (pChar->HasSeptic > '0')
      *(pOutbuf+OFF_SEWER) = pChar->HasSeptic;
   else
      *(pOutbuf+OFF_SEWER) = ' ';

   // Pool
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // HasWell
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Stories
   iTmp = atoin(pChar->Stories, SIZ_CHAR_SIZE4);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d.0 ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   } else
      memcpy(pOutbuf+OFF_STORIES, BLANK32, SIZ_STORIES);

   // Units
   if (pChar->Units[0] > '0')
      memcpy(pOutbuf+OFF_UNITS, pChar->Units, SIZ_CHAR_UNITS);

   // Zoning
   if (pChar->Zoning[0] > ' ')
   {
      memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         memcpy(pOutbuf+OFF_ZONE_X1, pChar->Zoning, SIZ_ZONE);
   }
   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************** Cal_MergeExe *******************************
 *
 * Merge Situs address
 *
 *****************************************************************************

int Cal_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      //pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';
         return 1;      // EOF
      }

      // Asmt is on 2nd token
      pTmp = strchr(acRec, ',');
      pTmp += 2;
      iTmp = memcmp(pOutbuf, pTmp, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pTmp);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 0;
   }

   iRet = ParseStringNQ(pRec, ',', MB_EXE_EXEPCT+1, apTokens);
   if (iRet < MB_EXE_EXEPCT)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe
   if (*apTokens[MB_EXE_HOEXE] == '1')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[MB_EXE_EXEAMT]);
   if (lTmp > 0)
   {
      long lGross = atoin(pOutbuf+OFF_GROSS, SIZ_GROSS);

      if (lTmp > lGross)
      {
         LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/******************************** Cal_MergeTax *******************************
 *
 * Note:
 *
 *****************************************************************************

int Cal_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   int      iRet=0, iTmp;
   double   dTmp, dTax1, dTax2;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      //pRec = fgets(acRec, 512, fdTax);
      // Get first rec
      pRec = fgets(acRec, 512, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec   %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Merge tax data
   while (!iTmp)
   {
      iTmp = ParseStringNQ(pRec, ',', MB_TAX_ROLLCAT+1, apTokens);
      if (iTmp < MB_TAX_ROLLCAT)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
         return -1;
      }

      // If same tax year, apply values
      //if (!memcmp(myCounty.acYearAssd, apTokens[MB_TAX_YEAR], 4))
      {
         dollar2Num(apTokens[MB_TAX_TAXAMT1], acTmp);
         dTax1 = atof(acTmp);
         dollar2Num(apTokens[MB_TAX_TAXAMT2], acTmp);
         dTax2 = atof(acTmp);
         dTmp = dTax1+dTax2;
         if (dTax1 == 0.0 || dTax2 == 0.0)
            dTmp *= 2;

         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
            memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
         } else
            memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
      }

      // Get next tax record
      pRec = fgets(acRec, 512, fdTax);
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         break;         // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
   }

   lTaxMatch++;
   return iRet;
}

/********************************* Cal_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Cal_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet, iTmp;

   // Parse input row
   iRet = ParseStringNQ1(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   //if (iRet < MB_ROLL_TAXABILITY)
   if (iRet < MB_ROLL_M_ADDR4)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   //if (*apTokens[MB_ROLL_STATUS] == 'D')
   //{
   //   LogMsg0("** Skip deleted roll record for APN=%s", apTokens[iApnFld]);
   //   return 1;
   //}

   // Ignore APN starts with 800-999 except 910 or 915 (MH)
   iTmp = atoin(apTokens[iApnFld], 3);
   if (!iTmp || (iTmp >= 800 && iTmp != 910 && iTmp != 915))
      return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));

      // Copy ALT_APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[MB_ROLL_FEEPARCEL], strlen(apTokens[MB_ROLL_FEEPARCEL]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "05CAL", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[MB_ROLL_TRA], strlen(apTokens[MB_ROLL_TRA]));

   // status
   //*(pOutbuf+OFF_STATUS) = *apTokens[MB_ROLL_STATUS];
   *(pOutbuf+OFF_STATUS) = 'A';

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002016016000", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // Zoning
   if (memcmp(apTokens[MB_ROLL_ZONING], "000000", 6) > 0)
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   // UseCode
   strcpy(acTmp, apTokens[MB_ROLL_USECODE]);
   acTmp[SIZ_USE_CO] = 0;
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Megabyte:
   // Standard UseCode
   //if (acTmp[0] > ' ')
   //{
   //   _strupr(acTmp);
   //   iTmp = strlen(acTmp);
   //   memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
   //   if (iNumUseCodes > 0)
   //   {
   //      pTmp = Cnty2StdUse(acTmp);
   //      if (pTmp)
   //         memcpy(pOutbuf+OFF_USE_STD, pTmp, 3);
   //      else
   //      {
   //         memcpy(pOutbuf+OFF_USE_STD, USE_MISC, 3);
   //         logUsecode(acTmp, pOutbuf);
   //      }
   //   }
   //}

   // Acres
   dTmp = atof(apTokens[MB_ROLL_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Units
   iTmp = atol(apTokens[MB_ROLL_UNITS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Recorded Doc
   if (*apTokens[MB_ROLL_DOCNUM] > '0')
   {
      strcpy(acTmp, apTokens[MB_ROLL_DOCNUM]);
      lTmp = atol(&acTmp[5]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%.5s%0.7d", acTmp, lTmp);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, 12);
      } else
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], strlen(apTokens[MB_ROLL_DOCNUM]));

      pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[MB_ROLL_TAXABILITY], true, true);

   return 0;
}

/********************************* Cal_Load_Roll ******************************
 *
 * Roll file sometimes has duplicate entries.  It's better dedup it before processing.
 *
 ******************************************************************************/

int Cal_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Load %s roll update file", myCounty.acCntyCode);
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open updated roll file - Sort on field #1 (ASMT)
   strcpy(acTmpFile, acRollFile);
   lLastFileDate = getFileDate(acRollFile);
 
   // Sort roll file to dedup
   sprintf(acRollFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sorting %s to %s", acTmpFile, acRollFile);
   iRet = sortFile(acTmpFile, acRollFile, "S(#1,C,A) F(TXT) DUPO(#1)");
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting VALUE file %s to %s", acTmpFile, acRollFile);
      return -2;
   }
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open name file
   /* No need
   strcpy(acTmpFile, acNameFile);
   sprintf(acNameFile, "%s\\%s\\%s_Name.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sorting %s to %s", acTmpFile, acNameFile);
   printf("Sorting %s to %s\n", acTmpFile, acNameFile);
   iRet = sortFile(acTmpFile, acNameFile, "S(#1,C,A) F(TXT)");
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting Name & Addr file %s to %s", acTmpFile, acNameFile);
      return -2;
   }
   */
   LogMsg("Open Name file %s", acNameFile);
   fdName = fopen(acNameFile, "r");
   if (fdName == NULL)
   {
      LogMsg("***** Error opening Name file: %s\n", acNameFile);
      return -2;
   }

   // Open Situs file - need sorting
   /* No need
   strcpy(acTmpFile, acSitusFile);
   sprintf(acSitusFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sorting %s to %s", acTmpFile, acSitusFile);
   printf("Sorting %s to %s\n", acTmpFile, acSitusFile);
   iRet = sortFile(acTmpFile, acSitusFile, "S(#1,C,A) F(TXT)");
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting Situs file %s to %s", acTmpFile, acSitusFile);
      return -2;
   }
   */
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Exe file - Sort on 2nd field
   /* No need since the is already sorted
   strcpy(acTmpFile, acExeFile);
   sprintf(acExeFile, "%s\\%s\\%s_Exe.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sorting %s to %s", acTmpFile, acExeFile);
   printf("Sorting %s to %s\n", acTmpFile, acExeFile);
   iRet = sortFile(acTmpFile, acExeFile, "S(#2,C,A) F(TXT)");
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting EXE file %s to %s", acTmpFile, acExeFile);
      return -2;
   }
   */

   // EXE file is not available on daily update 8/6/2020
   //if (!_access(acExeFile, 0))
   //{
   //   LogMsg("Open Exe file %s", acExeFile);
   //   fdExe = fopen(acExeFile, "r");
   //   if (fdExe == NULL)
   //   {
   //      LogMsg("***** Error opening Exe file: %s\n", acExeFile);
   //      return -2;
   //   }
   //} else
   //{
   //   LogMsg("*** Missing EXE file: %s", acExeFile);
   //   fdExe = NULL;
   //}

   // Open Tax file - Sort on Asmt, TaxYear, ChrgDate1 & 2, PaidDate1 & 2
   /*
   strcpy(acTmpFile, acTaxFile);
   sprintf(acTaxFile, "%s\\%s\\%s_Tax.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sorting %s to %s", acTmpFile, acTaxFile);
   printf("Sorting %s to %s\n\n", acTmpFile, acTaxFile);
   iRet = sortFile(acTmpFile, acTaxFile, "S(#1,C,A,#16,C,D,#7,DAT,A,#15,DAT,A)");
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting Tax file %s to %s", acTmpFile, acTaxFile);
      return -2;
   }

   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }
   */

   // Open lien file
   /*
   fdLienExt = NULL;
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Lien file %s", acTmpFile);
      fdLienExt = fopen(acTmpFile, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien file: %s\n", acTmpFile);
         return -7;
      }
   }
   */

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header record 
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      
      //if (memcmp(&acRollRec[1], "820", 1) && (!memcmp(acBuf, "910000001000", 9) || !memcmp(acBuf, "915000050000", 9)))
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, (char *)&acRollRec[1], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Cal_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge name and mail addr
            if (fdName)
               lRet = Cal_MergeName(acBuf);

            // Merge Situs
            if (fdSitus)
               lRet = Cal_MergeSitus(acBuf);

            // Merge Lien
            //lRet = 1;
            //if (fdLienExt)
            //   lRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB, true);

            // Merge Exe if not found in LienExt
            //if (fdExe && lRet)
            //   lRet = MB_MergeExe(acBuf);

            // Merge Char
            if (fdChar)
               lRet = Cal_MergeStdChar(acBuf);
               //lRet = Cal_MergeChar(acBuf);

            // Remove old sale data
            //if (bClearSales)
            //   ClearOldSale(acBuf);

            // Merge Sales
            //if (fdSale)
            //   lRet = Cal_MergeSale(acBuf);

            // Merge Taxes
            //if (fdTax)
            //   lRet = MB_MergeTax(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp || acRollRec[1] > '9')
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Cal_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge value
            //if (fdValue)
            //   lRet = Cal_MergeValue(acRec, false);

            // Merge name and mail addr
            if (fdName)
               lRet = Cal_MergeName(acRec);

            // Merge Situs
            if (fdSitus)
               lRet = Cal_MergeSitus(acRec);

            // Merge Exe
            //if (fdExe)
            //   lRet = MB_MergeExe(acRec);

            // Merge Char
            if (fdChar)
               lRet = Cal_MergeStdChar(acRec);
               //lRet = Cal_MergeChar(acRec);

            // Merge Sales
            //if (fdSale)
            //   lRet = Cal_MergeSale(acRec);

            // Merge Taxes
            //if (fdTax)
            //   lRet = MB_MergeTax(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            iNewRec++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }

         // Get next roll record
         lCnt++;
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp || acRollRec[1] > '9')
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            break;
         }
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Cal_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge value
         //if (fdValue)
         //   lRet = Cal_MergeValue(acRec, false);

         // Merge name and mail addr
         if (fdName)
            lRet = Cal_MergeName(acRec);

         // Merge Situs
         if (fdSitus)
            lRet = Cal_MergeSitus(acRec);

         // Merge Exe
         //if (fdExe)
         //   lRet = MB_MergeExe(acRec);

         // Merge Char 
         if (fdChar)
            lRet = Cal_MergeStdChar(acRec);

         // Merge Taxes
         //if (fdTax)
         //   lRet = MB_MergeTax(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }

      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp || acRollRec[1] > '9')
         break;
   }

   // Close files
   //if (fdLienExt)
   //   fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   //if (fdSale)
   //   fclose(fdSale);
   //if (fdExe)
   //   fclose(fdExe);
   //if (fdTax)
   //   fclose(fdTax);
   //if (fdValue)
   //   fclose(fdValue);
   if (fdName)
      fclose(fdName);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iRollUpd+iNewRec;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Name matched:     %u", lNameMatch);
   //LogMsg("Number of Exe matched:      %u\n", lExeMatch);
   //LogMsg("Number of Sale matched:     %u\n", lSaleMatch);
   //LogMsg("Number of Value matched:    %u", lValueMatch);
   //LogMsg("Number of Tax matched:      %u\n", lTaxMatch);

   LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Name skiped:      %u", lNameSkip);
   //LogMsg("Number of Exe skiped:       %u\n", lExeSkip);
   //LogMsg("Number of Sale skiped:      %u\n", lSaleSkip);
   //LogMsg("Number of Value skiped:     %u", lValueSkip);
   //LogMsg("Number of Tax skiped:       %u\n", lTaxSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u", lRecCnt);

   return 0;
}

/******************************** Cal_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Cal_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pLine4)

{
   ADR_REC  sMailAdr;
   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[64];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "012007026000", 9))
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 == ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;
   p0 = NULL;
   if (*pLine4 > ' ')
   {
      p2 = pLine4;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;

         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pLine3;
            p1 = pLine2;
         } else if (isdigit(*pLine2))
         {
            p1 = pLine2;      // line3 is more likely mail stop
         } else
         {
            if (!memcmp(pLine2, "PMB", 3) || !memcmp(pLine2, "STE", 3))
            {
               sprintf(acAddr1, "%s %s", pLine3, pLine2);
               p1 = acAddr1;
            } else
               p1 = pLine3;
         }
      } else if (!_memicmp(pLine2, "C/O", 3)  ||
                 !_memicmp(pLine2, "ATTN", 4) ||
                 *pLine2 == '%')
      {
         p0 = pLine2;
         p1 = pLine3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pLine3;

         // Use line1 if it start with a digit
         if (isdigit(*pLine1))
            p1 = pLine1;
         else if (isdigit(*pLine2))
            p1 = pLine2;
         else
            p1 = pLine3;
      }
   } else if (*pLine3 > ' ')
   {
      p2 = pLine3;
      if (!_memicmp(pLine1, "C/O", 3)  ||
          !_memicmp(pLine1, "ATTN", 4) ||
          *pLine1 == '%')
      {
         p0 = pLine1;
         p1 = pLine2;
      } else if (!memcmp(pLine1, "PMB", 3))
      {
         if (strstr(pLine2, "STE") || strchr(pLine2, '#'))
            p1 = pLine2;
         else
         {
            sprintf(acAddr1, "%s %s", pLine2, pLine1);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "PMB", 3))
      {
         if (strstr(pLine1, "STE"))
            p1 = pLine1;
         else
         {
            sprintf(acAddr1, "%s %s", pLine1, pLine2);
            p1 = acAddr1;
         }
      } else if (!memcmp(pLine2, "STE", 3))
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         p1 = acAddr1;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pLine3);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p1 = pLine1;
            p2 = pLine2;
         } else if (isdigit(*pLine1))
            p1 = pLine1;
         else
            p1 = pLine2;
      }
   } else if (*pLine2 >= 'A')
   {
      p1 = pLine1;
      p2 = pLine2;
   } else
   {
      p2 = pLine1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      acTmp[0] = 0;
      updateCareOf(pOutbuf, p0, strlen(p0));
      /*
      if (!_memicmp(p0, "C/O", 3))
         pTmp = p0+4;
      else if (!_memicmp(p0, "ATTN", 4))
         pTmp = p0+5;
      else if (*p0 == '%')
         pTmp = p0+1;

      while (*pTmp == ' ')
         pTmp++;
      iTmp = strlen(pTmp);
      if (iTmp > SIZ_CARE_OF)
         iTmp = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, pTmp, iTmp);
      */
   }

   if (p1)
   {
      if (!acAddr1[0])
         strncpy(acAddr1, p1, SIZ_M_ADDR_D);
      if (pTmp = strchr(acAddr1, ','))
      {
         char *pTmp1;

         if (pTmp1 = strstr(pTmp, "FOR"))
            *pTmp = 0;
         else
            *pTmp = ' ';
      }

      // Special case
      if (pTmp = strchr(acAddr1, 0xA5))
         *pTmp = 'N';

      // Remove blank
      blankRem(acAddr1, SIZ_M_ADDR_D);
      quoteRem(acAddr1);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      parseMAdr1_3(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         if (sMailAdr.strDir[0] > '0')
            memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         if (sMailAdr.strSfx[0] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         if (pTmp = strstr(sMailAdr.strName, " PMB"))
            *pTmp = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      } else
      {
         iTmp = strlen(acAddr1);
         if (iTmp > SIZ_M_STREET)
            iTmp = SIZ_M_STREET;
         memcpy(pOutbuf+OFF_M_STREET, acAddr1, iTmp);
      }
   }

   strcpy(acAddr2, p2);
   blankRem(acAddr2);

   iTmp = strlen(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D)
      iTmp = SIZ_M_CTY_ST_D;
   memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      iTmp = strlen(sMailAdr.City);
      if (iTmp > SIZ_M_CITY) iTmp = SIZ_M_CITY;
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, iTmp);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      iTmp = strlen(sMailAdr.Zip);
      if (iTmp > SIZ_M_ZIP)
         iTmp = SIZ_M_ZIP;
      memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, iTmp);

      if (strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);

   }
}

int Cal_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2)
{
   char     acTmp[256], acAddr1[128];
   ADR_REC  sSitusAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002032009000", 9))
   //   acTmp[0] = 0;
#endif
   //
   strcpy(acAddr1, pLine1);
   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   memset(&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   memcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pLine2, strlen(pLine2));
   parseAdr2(&sSitusAdr, pLine2);
   if (sSitusAdr.City[0] > ' ')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }

   return 0;
}


/********************************* Cal_MergeLien *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Cal_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], sApn[20], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet, iTmp, iTmp1;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "002018027000", 12))
   //   iTmp = 0;
#endif

   //iRet = replNull(pRollRec, 32, 0);
   replStrAll(pRollRec, "|NULL|", "||");

   // Parse data
   //iRet = ParseString(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   // 2009 format
   //iRet = ParseString(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   // 2011 format
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   iTmp = remChar(apTokens[L_ASMT], '-');
   iTmp1 = iApnLen-iTmp;
   strcpy(sApn, "000000");
   strcpy(&sApn[iTmp1], apTokens[L_ASMT]);
   //iTmp = atoin(sApn, 3);
   //if (!iTmp || (iTmp >= 800 && iTmp != 910 && iTmp != 915))
   //   return 1;

   memcpy(pOutbuf, sApn, iApnLen);

   // Copy ALT_APN
   iTmp = remChar(apTokens[L_FEEPARCEL], '-');
   iTmp1 = iApnLen-iTmp;
   strcpy(acTmp, "000000");
   strcpy(&acTmp[iTmp1], apTokens[L_FEEPARCEL]);
   memcpy(pOutbuf+OFF_ALT_APN, acTmp, iApnLen);

   // Format APN
   iRet = formatApn(sApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(sApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
		memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "05CAL", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values
   // Land
   long lLand = dollar2Num(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow = dollar2Num(apTokens[L_CURRENTGROWINGIMPRVALUE]);
   long lFixt = dollar2Num(apTokens[L_CURRENTFIXEDIMPRVALUE]);
   long lPP   = dollar2Num(apTokens[L_CURRENTPERSONALPROPVALUE]);
   long lMH   = dollar2Num(apTokens[L_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lMH;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross total
   long lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Tax
   /*
   double dTax1 = atof(apTokens[L_TAXAMT1]);
   double dTax2 = atof(apTokens[L_TAXAMT2]);
   dTmp = dTax1+dTax2;
   if (dTax1 == 0.0 || dTax2 == 0.0)
      dTmp *= 2;

   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTmp*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);
   */

   // Exemption
   long lExe1 = dollar2Num(apTokens[L_EXEMPTIONAMT1]);
   long lExe2 = dollar2Num(apTokens[L_EXEMPTIONAMT2]);
   long lExe3 = dollar2Num(apTokens[L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      if (lTmp > lGross)
      {
         LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   // H/O Exemption
   myLTrim(apTokens[L_EXEMPTIONCODE1]);
   if (!memcmp(apTokens[L_EXEMPTIONCODE1], "E01", 3))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   myLTrim(apTokens[L_EXEMPTIONCODE2]);
   myLTrim(apTokens[L_EXEMPTIONCODE3]);
   memcpy(pOutbuf+OFF_EXE_CD1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
   memcpy(pOutbuf+OFF_EXE_CD2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
   memcpy(pOutbuf+OFF_EXE_CD3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));

   // TRA
   remChar(apTokens[L_TRA], '-');
   iTmp = atol(apTokens[L_TRA]);
   if (iTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d  ", iTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L_STATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "012007026000", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   iTmp = strlen(apTokens[L_USECODE]);
   if (iTmp > 0)
   {
      // Std Usecode
      if (strlen(apTokens[L_USECODE]) == 1)
         iTmp = sprintf(acTmp, "000%s", apTokens[L_USECODE]);
      else if (strlen(apTokens[L_USECODE]) == 2)
         iTmp = sprintf(acTmp, "00%s", apTokens[L_USECODE]);
      else if (strlen(apTokens[L_USECODE]) == 3)
         iTmp = sprintf(acTmp, "0%s", apTokens[L_USECODE]);
      else 
         strcpy(acTmp, apTokens[L_USECODE]);

      vmemcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   Cal_MergeOwner(pOutbuf, apTokens[L_OWNER]);

   // Situs
   Cal_MergeSitus(pOutbuf, apTokens[L_SITUS1], apTokens[L_SITUS2]);

   // Mailing
   Cal_MergeMAdr(pOutbuf, apTokens[L_MAILADDRESS1], apTokens[L_MAILADDRESS2], apTokens[L_MAILADDRESS3], apTokens[L_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   // Data not available in 2016
   //iTmp = updateTaxCode(pOutbuf, apTokens[L_TAXABILITY], true, true);

   // Chars - 2016 LDR includes some char fields which are not in CHAR file
   // Acres
   dTmp = atof(apTokens[L_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Rooms & Stories info is questionable
   //// Total rooms
   //iTmp = atol(apTokens[L_TOTALROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   //}

   //// Stories
   //lTmp = atol(apTokens[L_STORIES]);
   //if (lTmp > 0 && lTmp < 100)
   //{
   //   iTmp = sprintf(acTmp, "%d.0", lTmp);
   //   memcpy(pOutbuf+OFF_STORIES, acTmp, iTmp);
   //}

   // Units
   lTmp = atol(apTokens[L_UNITS]);
   if (lTmp > 1)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, iTmp);
   }

   // Garage
   iTmp = atol(apTokens[L_GARAGE]);
   if (iTmp > 0 && iTmp < 6)
   {
      pTmp = findXlatCode(apTokens[L_GARAGE], &asParkType[0]);
      if (pTmp)
         *(pOutbuf+OFF_PARK_TYPE) = *pTmp;
   }

   return 0;
}

int Cal_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], sApn[20], *pTmp;
   ULONG    lTmp;
   double   dTmp;
   int      iRet, iTmp;

   if (cLdrSep == '|')
      replStrAll(pRollRec, "|NULL|", "||");

   // Parse data
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L3_UNITS)
   {
      LogMsg("***** Error: bad input record for APN=%s (tokens=%d)", apTokens[L3_ASMT], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   vmemcpy(pOutbuf+OFF_APN_D, apTokens[L3_ASMT], 15);
   remChar(apTokens[L3_ASMT], '-');
   strcpy(sApn, apTokens[L3_ASMT]);
   vmemcpy(pOutbuf, sApn, iApnLen);

   // Copy ALT_APN
   remChar(apTokens[L3_FEEPARCEL], '-');
   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[L3_FEEPARCEL], iApnLen);

   // Create MapLink and output new record
   iRet = formatMapLink(sApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
		memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "05CAL", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   ULONG lLand = dollar2Num(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   ULONG lImpr = dollar2Num(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   ULONG lGrow  = dollar2Num(apTokens[L3_GROWING]);
   ULONG lFixt  = dollar2Num(apTokens[L3_FIXTURESVALUE]);
   ULONG lPP    = dollar2Num(apTokens[L3_PPVALUE]);
   ULONG lMH    = dollar2Num(apTokens[L3_MHPPVALUE]);
   ULONG lFixtRP= dollar2Num(apTokens[L3_FIXTURESRP]);

   lTmp = lGrow+lFixt+lPP+lMH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%u         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%u         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%u         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%u         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%u         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
   }

   // Gross total
   ULONG lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   ULONG lExe1 = dollar2Num(apTokens[L3_HOX]);
   ULONG lExe2 = dollar2Num(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      if (lTmp > lGross)
      {
         LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

      iTmp = OFF_EXE_CD1;
      if (lExe1 > 0)
      {
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
         memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
         iTmp = OFF_EXE_CD2;
      } else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Save exemption code
      if (*apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
         memcpy(pOutbuf+iTmp, apTokens[L3_OTHEREXEMPTIONCODE], strlen(apTokens[L3_OTHEREXEMPTIONCODE]));

      // Create exemption type
      makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&CAL_Exemption);
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // TRA
   remChar(apTokens[L3_TRA], '-');
   iTmp = atol(apTokens[L3_TRA]);
   if (iTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d  ", iTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L3_ASMTSTATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "012007026000", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L3_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   iTmp = strlen(apTokens[L3_LANDUSE1]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L3_LANDUSE1], SIZ_USE_CO, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L3_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   Cal_MergeOwner(pOutbuf, apTokens[L3_OWNER]);

   // Situs
   Cal_MergeSitus(pOutbuf, apTokens[L3_SITUS1], apTokens[L3_SITUS2]);

   // Mailing
   Cal_MergeMAdr(pOutbuf, apTokens[L3_MAILADDRESS1], apTokens[L3_MAILADDRESS2], apTokens[L3_MAILADDRESS3], apTokens[L3_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L3_TAXABILITYFULL], true, true);

   // Acres
   dTmp = atof(apTokens[L3_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Total rooms - value is questionable
   //iTmp = atol(apTokens[L3_TOTALROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   //}

   // Stories
   lTmp = atol(apTokens[L3_STORIES]);
   if (lTmp > 0 && lTmp < 100)
   {
      iTmp = sprintf(acTmp, "%d.0", lTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, iTmp);
   }

   // Units
   lTmp = atol(apTokens[L3_UNITS]);
   if (lTmp > 1)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, iTmp);
   }

   // Garage
   iTmp = atol(apTokens[L3_GARAGE]);
   if (iTmp > 0 && iTmp < 6)
   {
      pTmp = findXlatCode(apTokens[L3_GARAGE], &asParkType[0]);
      if (pTmp)
         *(pOutbuf+OFF_PARK_TYPE) = *pTmp;
   }

   // Garage size
   iTmp = atol(apTokens[L3_GARAGESIZE]);
   if (iTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, iTmp);
   }

   return 0;
}

int Cal_MergeLien7(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], sApn[20];
   long     lTmp;
   double   dTmp;
   int      iRet, iTmp;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "002018027000", 12))
   //   iTmp = 0;
#endif

   // Parse data
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L7_ACRES)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L7_ASMT]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   vmemcpy(pOutbuf+OFF_APN_D, apTokens[L7_ASMT], 15);
   remChar(apTokens[L7_ASMT], '-');
   strcpy(sApn, apTokens[L7_ASMT]);
   vmemcpy(pOutbuf, sApn, iApnLen);

   // Copy ALT_APN
   remChar(apTokens[L7_FEEPARCEL], '-');
   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[L7_FEEPARCEL], iApnLen);

   // Create MapLink and output new record
   iRet = formatMapLink(sApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
		memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "05CAL", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien values
   // Land
   long lLand = dollar2Num(apTokens[L7_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[L7_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow  = dollar2Num(apTokens[L7_GROWING]);
   long lFixt  = dollar2Num(apTokens[L7_FIXTURESVALUE]);
   long lPP    = dollar2Num(apTokens[L7_PPVALUE]);
   long lMH    = dollar2Num(apTokens[L7_MHPPVALUE]);
   long lFixtRP= dollar2Num(apTokens[L7_FIXTURESRP]);

   lTmp = lGrow+lFixt+lPP+lMH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
   }

   // Gross total
   long lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = dollar2Num(apTokens[L7_HOX]);
   long lExe2 = dollar2Num(apTokens[L7_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      if (lTmp > lGross)
      {
         LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

      if (lExe1 > 0)
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
      {
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         vmemcpy(pOutbuf+OFF_EXE_CD1, apTokens[L7_OTHEREXEMPTIONCODE], SIZ_EXE_CD1);
      }
   }  

   // TRA
   remChar(apTokens[L7_TRA], '-');
   iTmp = atol(apTokens[L7_TRA]);
   if (iTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d  ", iTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L7_ASMTSTATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "012007026000", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L7_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   iTmp = strlen(apTokens[L7_LANDUSE1]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L7_LANDUSE1], SIZ_USE_CO, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L7_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   Cal_MergeOwner(pOutbuf, apTokens[L7_OWNER]);

   // Situs
   Cal_MergeSitus(pOutbuf, apTokens[L7_SITUS1], apTokens[L7_SITUS2]);

   // Mailing
   Cal_MergeMAdr(pOutbuf, apTokens[L7_MAILADDRESS1], apTokens[L7_MAILADDRESS2], apTokens[L7_MAILADDRESS3], apTokens[L7_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L7_TAXABILITYFULL], true, true);

   // Acres
   dTmp = atof(apTokens[L7_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   return 0;
}

int Cal_MergeLien12(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   double   dTmp;
   int      iRet, iTmp, lTmp;

   // Parse data
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L12_CURRENTDOCNUM)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L12_ASMT], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[L12_ASMT], iApnLen);

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L12_FEEPARCEL], iApnLen);

   // Format APN
   iRet = formatApn(apTokens[L12_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[L12_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
		memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "05CAL", 5);

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L12_STATUS];

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, apTokens[L12_TAXYEAR], 4);

   // TRA
   iTmp = atol(apTokens[L12_TRA]);
   if (iTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d  ", iTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L12_TAXABILITY], true, true);

   // Owner
   Cal_MergeOwner(pOutbuf, apTokens[L12_OWNER]);

   // Mailing
   Cal_MergeMAdr(pOutbuf, apTokens[L12_MAILADDRESS1], apTokens[L12_MAILADDRESS2], apTokens[L12_MAILADDRESS3], apTokens[L12_MAILADDRESS4]);

   // Situs - Use situs from Calaveras_Situs.csv
   Cal_MergeSitus(pOutbuf, apTokens[L12_SITUS1+iRollAdj], apTokens[L12_SITUS2+iRollAdj]);

   // Lien values
   // Land
   long lLand = atol(apTokens[L12_LANDVALUE+iRollAdj]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[L12_STRUCTUREVALUE+iRollAdj]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   long lGrow  = atol(apTokens[L12_GROWINGVALUE+iRollAdj]);
   long lFixt  = atol(apTokens[L12_FIXTURESVALUE+iRollAdj]);
   long lPP    = atol(apTokens[L12_PPVALUE+iRollAdj]);
   long lMH    = atol(apTokens[L12_MHPPVALUE+iRollAdj]);
   long lFixtRP= atol(apTokens[L12_FIXTURESRPVALUE+iRollAdj]);

   lTmp = lGrow+lFixt+lPP+lMH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%d         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
   }

   // Gross total
   long lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[L12_HOX+iRollAdj]);
   long lExe2 = atol(apTokens[L12_OTHEREXEMPTION+iRollAdj]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      if (lTmp > lGross)
      {
         LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

      if (lExe1 > 0)
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
      {
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         vmemcpy(pOutbuf+OFF_EXE_CD1, apTokens[L12_OTHEREXEMPTIONCODE+iRollAdj], SIZ_EXE_CD1);
      }
   }  

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "012007026000", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L12_PARCELDESCRIPTION+iRollAdj]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   iTmp = strlen(apTokens[L12_LANDUSE1+iRollAdj]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L12_LANDUSE1+iRollAdj], SIZ_USE_CO, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L12_LANDUSE1+iRollAdj], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   dTmp = atof(apTokens[L12_ACRES+iRollAdj]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // 2020
   if (iTokens > L12_STORIES+iRollAdj)
   {
      // Stories
      lTmp = atol(apTokens[L12_STORIES+iRollAdj]);
      if (lTmp > 0 && lTmp < 100)
      {
         iTmp = sprintf(acTmp, "%d.0", lTmp);
         memcpy(pOutbuf+OFF_STORIES, acTmp, iTmp);
      }

      // Rooms
      lTmp = atol(apTokens[L12_TOTALROOMS+iRollAdj]);
      if (lTmp > 0 && lTmp < 1000)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, iTmp);
      }

      // Units
      lTmp = atol(apTokens[L12_UNITS+iRollAdj]);
      if (lTmp > 1)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pOutbuf+OFF_UNITS, acTmp, iTmp);
      }

      // Garage
      iTmp = atol(apTokens[L12_GARAGE+iRollAdj]);
      if (iTmp > 0 && iTmp < 6)
      {
         pTmp = findXlatCode(apTokens[L12_GARAGE+iRollAdj], &asParkType[0]);
         if (pTmp)
            *(pOutbuf+OFF_PARK_TYPE) = *pTmp;
      }

      // AgPreserved
      if (iTokens > (L3_ISAGPRESERVE+iRollAdj) && *apTokens[L3_ISAGPRESERVE+iRollAdj] == '1')
         *(pOutbuf+OFF_AG_PRE) = 'Y';
   }

   return 0;
}

// 2023 - Similar to grp 3, no BuildingType & IsAgPreserve
int Cal_MergeLien32(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], sApn[20], *pTmp;
   ULONG    lTmp;
   double   dTmp;
   int      iRet, iTmp;

   // Parse data
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L32_UNITS)
   {
      LogMsg("***** Error: bad input record for APN=%s (tokens=%d)", apTokens[L32_ASMT], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Format APN
   iRet = formatApn(apTokens[L32_ASMT], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
   strcpy(sApn, apTokens[L32_ASMT]);
   vmemcpy(pOutbuf, sApn, iApnLen);

   // Copy ALT_APN
   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[L32_FEEPARCEL], iApnLen);

   // Create MapLink and output new record
   iRet = formatMapLink(sApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
		memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "05CAL", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   ULONG lLand = dollar2Num(apTokens[L32_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   ULONG lImpr = dollar2Num(apTokens[L32_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Growing, Fixture, PP, PPMH
   ULONG lGrow  = dollar2Num(apTokens[L32_GROWING]);
   ULONG lFixt  = dollar2Num(apTokens[L32_FIXTURESVALUE]);
   ULONG lPP    = dollar2Num(apTokens[L32_PPVALUE]);
   ULONG lMH    = dollar2Num(apTokens[L32_MHPPVALUE]);
   ULONG lFixtRP= dollar2Num(apTokens[L32_FIXTURESRP]);

   lTmp = lGrow+lFixt+lPP+lMH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%u         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%u         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%u         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%u         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%u         ", lFixtRP);
         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
      }
   }

   // Gross total
   ULONG lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   ULONG lExe1 = dollar2Num(apTokens[L32_HOX]);
   ULONG lExe2 = dollar2Num(apTokens[L32_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      if (lTmp > lGross)
      {
         LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
         lTmp = lGross;
      }

      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   iTmp = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "E01", 3);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save exemption code
   if (*apTokens[L32_OTHEREXEMPTIONCODE] > ' ')
      memcpy(pOutbuf+iTmp, apTokens[L32_OTHEREXEMPTIONCODE], strlen(apTokens[L32_OTHEREXEMPTIONCODE]));

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&CAL_Exemption);

   // TRA
   iTmp = atol(apTokens[L32_TRA]);
   if (iTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d  ", iTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // status
   *(pOutbuf+OFF_STATUS) = *apTokens[L32_ASMTSTATUS];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "012007026000", 9))
   //   iTmp = 0;
#endif
   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[L32_PARCELDESCRIPTION]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode
   iTmp = strlen(apTokens[L32_LANDUSE1]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[L32_LANDUSE1], SIZ_USE_CO, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[L32_LANDUSE1], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   Cal_MergeOwner(pOutbuf, apTokens[L32_OWNER]);

   // Situs
   Cal_MergeSitus(pOutbuf, apTokens[L32_SITUS1], apTokens[L32_SITUS2]);

   // Mailing
   Cal_MergeMAdr(pOutbuf, apTokens[L32_MAILADDRESS1], apTokens[L32_MAILADDRESS2], apTokens[L32_MAILADDRESS3], apTokens[L32_MAILADDRESS4]);

   // SetTaxcode, Prop8 flag, FullExe flag
   iTmp = updateTaxCode(pOutbuf, apTokens[L32_TAXABILITYFULL], true, true);

   // Acres
   dTmp = atof(apTokens[L32_ACRES]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
      if (lTmp < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Total rooms - value is questionable
   //iTmp = atol(apTokens[L32_TOTALROOMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   //}

   // Stories
   lTmp = atol(apTokens[L32_STORIES]);
   if (lTmp > 0 && lTmp < 100)
   {
      iTmp = sprintf(acTmp, "%d.0", lTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, iTmp);
   }

   // Units
   lTmp = atol(apTokens[L32_UNITS]);
   if (lTmp > 1)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, iTmp);
   }

   // Garage
   iTmp = atol(apTokens[L32_GARAGE]);
   if (iTmp > 0 && iTmp < 6)
   {
      pTmp = findXlatCode(apTokens[L32_GARAGE], &asParkType[0]);
      if (pTmp)
         *(pOutbuf+OFF_PARK_TYPE) = *pTmp;
   }

   // AgPreserved
   //if (iTokens > L32_ISAGPRESERVE && *apTokens[L32_ISAGPRESERVE] == '1')
   //   *(pOutbuf+OFF_AG_PRE) = 'Y';

   return 0;
}

/******************************** Cal_MergeZone ******************************
 *
 * Merge Zoning, Acres, Legal, DocNum, DocDate from roll file
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Cal_MergeZone(char *pOutbuf, bool bInclOther = false)
{
   static   char acRec[2048], *pRec=NULL;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 2048, fdZone);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdZone);
         fdZone = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec+iSkipQuote, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Roll rec %.*s", iApnLen, pRec+iSkipQuote);
         pRec = fgets(acRec, 2048, fdZone);
         lZoneSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringNQ1(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < MB_ROLL_M_ADDR4)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdZone);
         fdZone = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input roll record for APN=%s", apTokens[0]);
         iRet = -1;
      }

      return iRet;
   }

   lZoneMatch++;
   // Zoning
   if (memcmp(apTokens[MB_ROLL_ZONING], "000000", 6) > 0)
   {
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[MB_ROLL_ZONING], SIZ_ZONE);
      vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[MB_ROLL_ZONING], SIZ_ZONE_X1);
   }

   if (bInclOther)
   {
      // Legal
      if (*(pOutbuf+OFF_LEGAL) == ' ')
      {
         iTmp = updateLegal(pOutbuf, apTokens[MB_ROLL_LEGAL]);
         if (iTmp > iMaxLegal)
            iMaxLegal = iTmp;
      }

      // Merge Acreage
      char  acTmp[32], *pTmp;
      double dTmp = atof(apTokens[MB_ROLL_ACRES]);
      if (dTmp > 0.0 && *(pOutbuf+OFF_LOT_SQFT+8) == ' ')
      {
         ULONG   lTmp;

         // Lot Sqft
         lTmp = (ULONG)(dTmp * SQFT_PER_ACRE);
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         vmemcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

         // Format Acres
         lTmp = (long)(dTmp * ACRES_FACTOR);
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
         vmemcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }

      // Recorded Doc
      if (*(apTokens[MB_ROLL_DOCNUM]+4) == 'R')
      {
         pTmp = dateConversion(apTokens[MB_ROLL_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
         {
            memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
            vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[MB_ROLL_DOCNUM], SIZ_TRANSFER_DOC);
         }
      }
   }

   // Get next record
   pRec = fgets(acRec, 2048, fdZone);

   return 0;
}

/***************************** Cal_Load_LDR() *******************************
 *
 * Load LDR into 1900-byte record.
 *
 ****************************************************************************/

int Cal_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Load LDR roll for CAL");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Remove NULL from lien file as needed
   if (strstr(acRollFile, ".TAB"))
   {
      LogMsg("Remove NULL from input file");
      sprintf(acTmpFile, "%s\\%s\\%s_Lien.Tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = replByte_EQ(acRollFile, acTmpFile, 0);
      if (iRet > 0)
         strcpy(acRollFile, acTmpFile);
      else
         return iRet;
   }

   // Open lien file
   LogMsg("Open Lien file %s", acRollFile);
   fdLDR = fopen(acRollFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open situs
   //LogMsg("Open Situs file %s", acSitusFile);
   //fdSitus = fopen(acSitusFile, "r");
   //if (fdSitus == NULL)
   //{
   //   LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
   //   return -2;
   //}

   // Open roll file to get zoning, acreage, transfer
   GetIniString(myCounty.acCntyCode, "RollFile", "", acTmpFile, _MAX_PATH, acIniFile);
   if (acTmpFile[0] > ' ')
   {
      LogMsg("Open roll file %s", acTmpFile);
      fdZone = fopen(acTmpFile, "r");
      if (fdZone == NULL)
      {
         LogMsg("***** Error opening roll file: %s\n", acTmpFile);
         return -2;
      }
   }

   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);
   if (!isdigit(acRec[0]))
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLDR);    // Skip header

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!feof(fdLDR))
   {
      // Create new R01 record
      if (lLienYear == 2019 || lLienYear == 2020)
         iRet = Cal_MergeLien12(acBuf, acRec);
      else if (lLienYear == 2018)
         iRet = Cal_MergeLien7(acBuf, acRec);
      else if (lLienYear == 2017 || 
               lLienYear == 2021 || 
               lLienYear == 2022 || 
               lLienYear >= 2024)
         iRet = Cal_MergeLien3(acBuf, acRec);
      else if (lLienYear == 2023)
         iRet = Cal_MergeLien32(acBuf, acRec);
      else
         iRet = Cal_MergeLien(acBuf, acRec);
      if (iRet != 0)
      {
         if (!(pTmp=fgets(acRec, MAX_RECSIZE, fdRoll)) || !isdigit(*pTmp) )
            break;
         continue;
      }

      // Merge Char
      if (fdChar)
         lRet = Cal_MergeStdChar(acBuf);

      // Merge Situs
      //if (fdSitus)
      //   lRet = Cal_MergeSitus(acBuf);

      // Merge Zoning, Acres, DocNum, DocDate
      if (fdZone)
         lRet = Cal_MergeZone(acBuf, true);

      if (!iRet)
      {
			lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
			if (lRet > lLastRecDate && lRet < lToday)
				lLastRecDate = lRet;
			
         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLDR);
      if (!pTmp || !isdigit(*pTmp))
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLDR)
      fclose(fdLDR);
   if (fdZone)
      fclose(fdZone);
   //if (fdSitus)
   //   fclose(fdSitus);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   //LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Zone matched:     %u\n", lZoneMatch);

   //LogMsg("Number of Situs skiped:     %u", lSitusSkip);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Zone skiped:      %u\n", lZoneSkip);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************* convertChar() ******************************
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

int Cal_ConvertChar(char *pInfile, char *pOutfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmpFile[256], acTmp[256], *pRec;
   int      iBeds, iRet, iTmp, iCnt=0;
   MB_CHAR  myCharRec;

   // Open input file
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   LogMsg("Convert %s to fixed length", pInfile);

   strcpy(acTmpFile, pInfile);
   pRec = strrchr(acTmpFile, '.');
   if (pRec)
      strcpy(pRec, ".tmp");

   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   pRec = fgets(acBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);

      if (!pRec)
         break;

      iRet = ParseStringNQ(pRec, cDelim, MB_CHAR_HASWELL+1, apTokens);
      if (iRet < MB_CHAR_HASSEWER)
         break;

      memset((void *)&myCharRec, ' ', sizeof(MB_CHAR));
      memcpy(myCharRec.Asmt, apTokens[MB_CHAR_ASMT], strlen(apTokens[MB_CHAR_ASMT]));

      iTmp = atoi(apTokens[MB_CHAR_POOLS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%.2d", iTmp);
         memcpy(myCharRec.NumPools, acTmp, iRet);
      } else if (*apTokens[MB_CHAR_POOLS] >= 'A')
      {
         iTmp = strlen(apTokens[MB_CHAR_POOLS]);
         if (iTmp > MBSIZ_CHAR_POOLS) iTmp = MBSIZ_CHAR_POOLS;
         memcpy(myCharRec.NumPools, apTokens[MB_CHAR_POOLS], iTmp);
      }

      memcpy(myCharRec.LandUse1, apTokens[MB_CHAR_USECAT], strlen(apTokens[MB_CHAR_USECAT]));
      memcpy(myCharRec.QualityClass, apTokens[MB_CHAR_QUALITY], strlen(apTokens[MB_CHAR_QUALITY]));
      memcpy(myCharRec.YearBuilt, apTokens[MB_CHAR_YRBLT], strlen(apTokens[MB_CHAR_YRBLT]));
      memcpy(myCharRec.BuildingSize, apTokens[MB_CHAR_BLDGSQFT], strlen(apTokens[MB_CHAR_BLDGSQFT]));
      memcpy(myCharRec.SqFTGarage, apTokens[MB_CHAR_GARSQFT], strlen(apTokens[MB_CHAR_GARSQFT]));
      memcpy(myCharRec.Heating, apTokens[MB_CHAR_HEATING], strlen(apTokens[MB_CHAR_HEATING]));
      memcpy(myCharRec.Cooling, apTokens[MB_CHAR_COOLING], strlen(apTokens[MB_CHAR_COOLING]));
      //memcpy(myCharRec.HeatingSource, apTokens[MB_CHAR_HEATING_SRC], strlen(apTokens[MB_CHAR_HEATING_SRC]));
      //memcpy(myCharRec.CoolingSource, apTokens[MB_CHAR_COOLING_SRC], strlen(apTokens[MB_CHAR_COOLING_SRC]));

      iBeds = atoi(apTokens[MB_CHAR_BEDS]);
      if (iBeds > 0)
         memcpy(myCharRec.NumBedrooms, apTokens[MB_CHAR_BEDS], strlen(apTokens[MB_CHAR_BEDS]));

      iTmp = atoi(apTokens[MB_CHAR_FBATHS]);
      if (iTmp > 0)
         memcpy(myCharRec.NumFullBaths, apTokens[MB_CHAR_FBATHS], strlen(apTokens[MB_CHAR_FBATHS]));

      iTmp = atoi(apTokens[MB_CHAR_HBATHS]);
      if (iTmp > 0)
         memcpy(myCharRec.NumHalfBaths, apTokens[MB_CHAR_HBATHS], strlen(apTokens[MB_CHAR_HBATHS]));

      //iTmp = atoi(apTokens[MB_CHAR_ROOMS]);
      //if (iTmp > iBeds)
      //   memcpy(myCharRec.TotalRooms, apTokens[MB_CHAR_ROOMS], strlen(apTokens[MB_CHAR_ROOMS]));

      iTmp = atoi(apTokens[MB_CHAR_FP]);
      if (iTmp > 0)
         memcpy(myCharRec.NumFireplaces, apTokens[MB_CHAR_FP], strlen(apTokens[MB_CHAR_FP]));

      memcpy(myCharRec.FeeParcel, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));

      //if (*(apTokens[MB_CHAR_HASSEPTIC]) > '0')
      if (*(apTokens[MB_CHAR_HASSEPTIC]) == 'T')
         myCharRec.HasSeptic = 'Y';

      //if (*(apTokens[MB_CHAR_HASSEWER]) > '0')
      if (*(apTokens[MB_CHAR_HASSEWER]) == 'T')
         myCharRec.HasSewer = 'Y';

      //if (*(apTokens[MB_CHAR_HASWELL]) > '0')
      if (*(apTokens[MB_CHAR_HASWELL]) == 'T')
         myCharRec.HasWell = 'Y';

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Asmt[0], fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT, YrBlt, Beds
   if (iCnt > 100)
      iRet = sortFile(acTmpFile, pOutfile, "S(1,12,C,A,23,4,C,D,53,3,I,A) F(TXT)");

   printf("\n");
   return iRet;
}

/**************************** Cal_ConvStdChar ********************************
 *
 * Copy from MergeAma.cpp to convert char file to STDCHAR.
 *
 *****************************************************************************/

int Cal_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[4], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg("\nConverting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec)
         break;

      // Replace NULL with empty string
      replStrAll(acBuf, "NULL", "");
      iFldCnt = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < MB_CHAR_HASSEWER)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      if (*apTokens[MB_CHAR_ASMT] < '0')
      {
         LogMsg("Drop CHAR at %d [%s]", ++iCnt, pRec);
         continue;
      }

      memcpy(myCharRec.Apn, apTokens[MB_CHAR_ASMT], strlen(apTokens[MB_CHAR_ASMT]));
      memcpy(myCharRec.FeeParcel, apTokens[MB_CHAR_FEE_PRCL], strlen(apTokens[MB_CHAR_FEE_PRCL]));

      // Format APN
      iRet = formatApn(apTokens[MB_CHAR_ASMT], acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

#ifdef _DEBUG
      //iRet = sizeof(STDCHAR);
      //if (!memcmp(myCharRec.Apn, "002013007000", 9))
      //   iRet = 0;
#endif

      // Pool - 01, 02, 03, 99
      iTmp = blankRem(apTokens[MB_CHAR_POOLS]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%.2d", iTmp);
         pRec = findXlatCode(acTmp, &asPool[0]);
         if (pRec)
            myCharRec.Pool[0] = *pRec;
      }

      // QualityClass
      _strupr(apTokens[MB_CHAR_QUALITY]);
      iTmp = blankRem(apTokens[MB_CHAR_QUALITY]);
      memcpy(myCharRec.QualityClass, apTokens[MB_CHAR_QUALITY], iTmp);
      strcpy(acTmp, apTokens[MB_CHAR_QUALITY]);
      acCode[0] = ' ';
      if (isalpha(acTmp[0]))
      {
         if (!memcmp(acTmp, "MH", 2) || isdigit(acTmp[1]))
            myCharRec.BldgClass = acTmp[0];
         else if (iTmp > 5 && isalpha(acTmp[1]) && isdigit(acTmp[2]))
            myCharRec.BldgClass = acTmp[1];

         iTmp = 0;
         while (acTmp[iTmp] && !isdigit(acTmp[iTmp]))
            iTmp++;

         if (acTmp[iTmp] >= '0')
            iRet = Quality2Code((char *)&acTmp[iTmp], acCode, NULL);
      } else if (acTmp[0] >= '0' && acTmp[0] <= '9')
         iRet = Quality2Code(acTmp, acCode, NULL);

      myCharRec.BldgQual = acCode[0];

      // YrBlt
      int iYrBlt = atoi(apTokens[MB_CHAR_YRBLT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[MB_CHAR_BLDGSQFT]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Garage SF
      int iAttGar = atoi(apTokens[MB_CHAR_GARSQFT]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = '2';              // GARAGE/CARPORT
      }

      // Heating - translation table has not been verified
      iTmp = blankRem(apTokens[MB_CHAR_HEATING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[MB_CHAR_HEATING], &asHeating[0]);
         if (pRec)
            myCharRec.Heating[0] = *pRec;
      } 

      // Cooling
      iTmp = blankRem(apTokens[MB_CHAR_COOLING]);
      if (iTmp > 0)
      {
         pRec = findXlatCode(apTokens[MB_CHAR_COOLING], &asCooling[0]);
         if (pRec)
            myCharRec.Cooling[0] = *pRec;
      } 

      // Beds
      iTmp = atoi(apTokens[MB_CHAR_BEDS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[MB_CHAR_FBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
         memcpy(myCharRec.Bath_4Q, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[MB_CHAR_HBATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
         memcpy(myCharRec.Bath_2Q, acTmp, iRet);
      }

      // FirePlace
      if (*apTokens[MB_CHAR_FP] > ' ')
      {
         sprintf(acTmp, "%.2d", atol(apTokens[MB_CHAR_FP]));
         pRec = findXlatCode(acTmp, &asFirePlace[0]);
         if (pRec)
            myCharRec.Fireplace[0] = *pRec;
      }

      // Sewer
      if (*(apTokens[MB_CHAR_HASSEWER]) == 'T')
         myCharRec.HasSewer = 'Y';
      else if (*(apTokens[MB_CHAR_HASSEPTIC]) == 'T')
      {
         myCharRec.HasSewer = 'S';
         myCharRec.HasSeptic = 'Y';
      }
      if (*(apTokens[MB_CHAR_HASSEPTIC]) == 'T')
         myCharRec.HasSeptic = 'Y';

      // Water
      if (*(apTokens[MB_CHAR_HASWELL]) == 'T')
      {
         myCharRec.HasWell = 'Y';
         myCharRec.HasWater = 'W';
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Sort on APN, YrBlt with newest one on top
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,53,4,C,D) DUPO(B4096,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/***************************** Cal_ExtrLienRec ******************************
 *
 *
 ****************************************************************************/

// 2019-2021 
int Cal_ExtrLienRec12(char *pOutbuf, char *pRollRec)
{
   int      iRet;
   ULONG    lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L12_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%.12s (%d)", apTokens[L12_ASMT], iRet);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L12_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[L12_ASMT], strlen(apTokens[L12_ASMT]));

   // TRA
   lTmp = atol(apTokens[L12_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Taxability
   lTmp = atol(apTokens[L12_TAXABILITY]);
   if (lTmp > 0)
   {
      // Prop 8 
      if (lTmp > 799 && lTmp < 900)
         pLienExtr->SpclFlag = LX_PROP8_FLG;
      vmemcpy(pLienExtr->acTaxCode, apTokens[L12_TAXABILITY], SIZ_LIEN_TAXCODE);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, apTokens[L12_TAXYEAR], 4);

   // Land
   long lLand = atol(apTokens[L12_LANDVALUE+iRollAdj]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[L12_STRUCTUREVALUE+iRollAdj]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixt  = atol(apTokens[L12_FIXTURESVALUE+iRollAdj]);
   long lFixtRP= atol(apTokens[L12_FIXTURESRPVALUE+iRollAdj]);
   long lGrow  = atol(apTokens[L12_GROWINGVALUE+iRollAdj]);
   long lPers  = atol(apTokens[L12_PPVALUE+iRollAdj]);
   long lPP_MH = atol(apTokens[L12_MHPPVALUE+iRollAdj]);
   lTmp = lFixt+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.FixtureRP, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = dollar2Num(apTokens[L12_HOX+iRollAdj]);
   long lExe2 = dollar2Num(apTokens[L12_OTHEREXEMPTION+iRollAdj]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  

   iRet = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      pLienExtr->acHO[0] = '1';      // 'Y'
      memcpy(pLienExtr->extra.MB.ExeCode1, "E01", 3);
      iRet = OFF_EXE_CD2;
   } else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L12_OTHEREXEMPTIONCODE+iRollAdj] > ' ')
   {
      if (iRet == OFF_EXE_CD1)
         vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L12_OTHEREXEMPTIONCODE+iRollAdj], SIZ_LIEN_TAXCODE);
      else
         vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L12_OTHEREXEMPTIONCODE+iRollAdj], SIZ_LIEN_TAXCODE);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

// 2018 version
int Cal_ExtrLienRec7(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L7_ACRES)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L7_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L7_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Remove hyphen from APN
   remChar(apTokens[L7_TRA], '-');
   remChar(apTokens[L7_ASMT], '-');

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[L7_ASMT], strlen(apTokens[L7_ASMT]));

   // TRA
   lTmp = atol(apTokens[L7_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = dollar2Num(apTokens[L7_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[L7_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixt  = dollar2Num(apTokens[L7_FIXTURESVALUE]);
   long lFixtRP= dollar2Num(apTokens[L7_FIXTURESRP]);
   long lGrow  = dollar2Num(apTokens[L7_GROWING]);
   long lPers  = dollar2Num(apTokens[L7_PPVALUE]);
   long lPP_MH = dollar2Num(apTokens[L7_MHPPVALUE]);
   lTmp = lFixt+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = dollar2Num(apTokens[L7_HOX]);
   long lExe2 = dollar2Num(apTokens[L7_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  

   iRet = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      pLienExtr->acHO[0] = '1';      // 'Y'
      memcpy(pLienExtr->extra.MB.ExeCode1, "E01", 3);
      iRet = OFF_EXE_CD2;
   } else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L7_OTHEREXEMPTIONCODE] > ' ')
   {
      if (iRet == OFF_EXE_CD1)
         vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L7_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
      else
         vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L7_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
   }

   // Taxability
   lTmp = atoin(apTokens[L7_TAXABILITYFULL], 3);
   if (lTmp > 0)
   {
      // Prop 8 
      if (lTmp > 799 && lTmp < 900)
         pLienExtr->SpclFlag = LX_PROP8_FLG;
      vmemcpy(pLienExtr->acTaxCode, apTokens[L7_TAXABILITYFULL], SIZ_LIEN_TAXCODE);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

// 2017 version
int Cal_ExtrLienRec3(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   replStrAll(pRollRec, "|NULL|", "||");
   iRet = ParseStringIQ(pRollRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iRet < L3_UNITS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L3_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L3_ASMT], "002006058", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Remove hyphen from APN
   remChar(apTokens[L3_TRA], '-');
   remChar(apTokens[L3_ASMT], '-');

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[L3_ASMT], strlen(apTokens[L3_ASMT]));

   // TRA
   lTmp = atol(apTokens[L3_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = dollar2Num(apTokens[L3_LANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[L3_STRUCTUREVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   long lFixt  = dollar2Num(apTokens[L3_FIXTURESVALUE]);
   long lFixtRP= dollar2Num(apTokens[L3_FIXTURESRP]);
   long lGrow  = dollar2Num(apTokens[L3_GROWING]);
   long lPers  = dollar2Num(apTokens[L3_PPVALUE]);
   long lPP_MH = dollar2Num(apTokens[L3_MHPPVALUE]);
   lTmp = lFixt+lGrow+lPers+lPP_MH+lFixtRP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLienExtr->extra.MB.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixtRP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtRP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = dollar2Num(apTokens[L3_HOX]);
   long lExe2 = dollar2Num(apTokens[L3_OTHEREXEMPTION]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  

   iRet = OFF_EXE_CD1;
   if (lExe1 > 0)
   {
      pLienExtr->acHO[0] = '1';      // 'Y'
      memcpy(pLienExtr->extra.MB.ExeCode1, "E01", 3);
      iRet = OFF_EXE_CD2;
   } else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Save exemption code
   if (lExe2 > 0 && *apTokens[L3_OTHEREXEMPTIONCODE] > ' ')
   {
      if (iRet == OFF_EXE_CD1)
         vmemcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L3_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
      else
         vmemcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L3_OTHEREXEMPTIONCODE], SIZ_LIEN_TAXCODE);
   }

   // Taxability
   lTmp = atoin(apTokens[L3_TAXABILITYFULL], 3);
   if (lTmp > 0)
   {
      // Prop 8 
      if (lTmp > 799 && lTmp < 900)
         pLienExtr->SpclFlag = LX_PROP8_FLG;
      vmemcpy(pLienExtr->acTaxCode, apTokens[L3_TAXABILITYFULL], SIZ_LIEN_TAXCODE);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

// 2016 version
int Cal_ExtrLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp, iTmp, iTmp1;
   char     acTmp[64], sApn[20];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_DTS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[L_ASMT]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L_ASMT], "002018027000", 9))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   iTmp = remChar(apTokens[L_ASMT], '-');
   iTmp1 = iApnLen-iTmp;
   strcpy(sApn, "000000");
   strcpy(&sApn[iTmp1], apTokens[L_ASMT]);

   // Start copying data
   memcpy(pLienExtr->acApn, sApn, iApnLen);

   // TRA
   iTmp = remChar(apTokens[L_TRA], '-');
   lTmp = atol(apTokens[L_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand, lImpr, lGrow, lFixt, lBP, lPP;
   lLand = dollar2Num(apTokens[L_CURRENTMARKETLANDVALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   lImpr = dollar2Num(apTokens[L_CURRENTSTRUCTURALIMPRVALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: FixtureRealProperty, PPBusiness, PPMH
   lGrow = dollar2Num(apTokens[L_CURRENTGROWINGIMPRVALUE]);
   lFixt = dollar2Num(apTokens[L_CURRENTFIXEDIMPRVALUE]);
   lBP   = dollar2Num(apTokens[L_CURRENTPERSONALPROPVALUE]);
   lPP   = dollar2Num(apTokens[L_CURRENTPERSONALPROPMHVALUE]);

   lTmp = lGrow+lFixt+lPP+lBP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lGrow > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lBP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lBP);
         memcpy(pLienExtr->extra.MB.BusProp, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = dollar2Num(apTokens[L_EXEMPTIONAMT1]);
   long lExe2 = dollar2Num(apTokens[L_EXEMPTIONAMT2]);
   long lExe3 = dollar2Num(apTokens[L_EXEMPTIONAMT3]);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }  
   if (!memcmp(myLTrim(apTokens[L_EXEMPTIONCODE1]), "E01", 3))
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Prop 8 - correcting by spn 01/16/2010
   //lTmp = atoin(apTokens[L_TAXABILITY], 3);
   //if (lTmp > 799 && lTmp < 900)
   //   pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Taxability - Not available in LDR 2016
   //if (lTmp > 0)
   //{
   //   iRet = strlen(apTokens[L_TAXABILITY]);
   //   if (iRet > SIZ_LIEN_TAXCODE)
   //      iRet = SIZ_LIEN_TAXCODE;
   //   memcpy(pLienExtr->acTaxCode, apTokens[L_TAXABILITY], iRet);
   //}

   // Save Exe code
   myLTrim(apTokens[L_EXEMPTIONCODE2]);
   myLTrim(apTokens[L_EXEMPTIONCODE3]);
   memcpy(pLienExtr->extra.MB.ExeCode1, apTokens[L_EXEMPTIONCODE1], strlen(apTokens[L_EXEMPTIONCODE1]));
   memcpy(pLienExtr->extra.MB.ExeCode2, apTokens[L_EXEMPTIONCODE2], strlen(apTokens[L_EXEMPTIONCODE2]));
   memcpy(pLienExtr->extra.MB.ExeCode3, apTokens[L_EXEMPTIONCODE3], strlen(apTokens[L_EXEMPTIONCODE3]));

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************* Cal_ExtrLien *******************************
 *
 * Extract lien data from ???_lien.csv
 *
 ****************************************************************************/

int Cal_ExtrLien(LPCSTR pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg0("Extract lien roll for %s", pCnty);

   // Open LDR file
   LogMsg("Open Lien Date Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acRollFile);
      return -1;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (!isdigit(acRec[0]))
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);    // Skip header

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new record
      if (lLienYear >= 2019)
         iRet = Cal_ExtrLienRec12(acBuf, acRec);
      else if (lLienYear == 2018)
         iRet = Cal_ExtrLienRec7(acBuf, acRec);
      else if (lLienYear == 2017)
         iRet = Cal_ExtrLienRec3(acBuf, acRec);
      else
         iRet = Cal_ExtrLienRec(acBuf, acRec);
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[0]))
         break;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/*********************************** loadCal ********************************
 *
 * Options:
 *    -CCAL -L -Xl (load lien)
 *    -CCAL -U (load update)
 *
 * Notes:
 *    1) In 2009, we received LDR file the same format as AMA.
 *
 ****************************************************************************/

int loadCal(int iSkip)
{
   int   iRet=0;
   char  acTmp[_MAX_PATH], acZipFile[_MAX_PATH], *pTmp;

   // Set default APN field
   iApnFld = MB_ROLL_ASMT;
   iApnLen = myCounty.iApnLen;

   if (lLienYear == 2021)
      iRollAdj= 1;
   else
      iRollAdj= 0;

   //iRet = FixCumSale(acCSalFile, SALE_FLD_NOPRCLXFR, false);

   // Load tax file
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Extract lien file
   if (iLoadFlag & EXTR_LIEN)                            // -Xl
      //iRet = Cal_ExtrLien(myCounty.acCntyCode);        // 2013
      iRet = MB_ExtrTR601(myCounty.acCntyCode);          // Grp=3, 2021, 2022
      //iRet = MB_ExtrLien(myCounty.acCntyCode, NULL);   // 2011

   // Move production files
   if (iLoadFlag & LOAD_UPDT)                            // -U
   {
      GetIniString("CAL", "ZipFile", "", acZipFile, _MAX_PATH, acIniFile);
      if (!_access(acZipFile, 0))
      {
         strcpy(acTmp, acZipFile);
         pTmp = strrchr(acTmp, '\\');
         *pTmp = 0;

         // Unzip input files
         doZipInit();  
         setUnzipToFolder(acTmp);
         setReplaceIfExist(true);
         iRet = startUnzip(acZipFile);
         doZipShutdown();

         if (iRet)
         {
            LogMsg("***** Error unzipping %s.  Program terminated", acZipFile);
            return -1;
         }
      }
   }

   // Create/Update cum sale file from Cal_Sales.csv
   if (iLoadFlag & EXTR_SALE)                            // -Xs
   {
      iRet = getFileSize(acSalesFile);
      if (iRet > 1000)
      {
         iRet = Cal_CreateSCSale(YYYY_MM_DD, false);
         if (!iRet)
            iLoadFlag |= MERG_CSAL;
      } else
         LogMsg("***** Error: Bad sale file: %s", acSalesFile);
   }

   // Extract new CHARS
   if (iLoadFlag & EXTR_ATTR)                            // -Xa
   {
      iRet = getFileSize(acCharFile);
      if (iRet > 1000)
      {
         iRet = Cal_ConvStdChar(acCharFile);
         if (iRet <= 0)
         {
            LogMsg("***** Error extracting attributes data from %s", acCharFile);
            return -1;
         }
      } else
         LogMsg("***** Error: Bad CHAR file: %s", acCharFile);
   }

   if (iLoadFlag & LOAD_LIEN)                            // -L
   {
      // Create Lien file
      iRet = Cal_Load_LDR(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)
   {
      GetIniString(myCounty.acCntyCode, "RollFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acRollFile, acTmp, myCounty.acCntyCode);

      iRet = Cal_Load_Roll(iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )                // -Ms
   {
      // Apply Cal_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   return iRet;
}