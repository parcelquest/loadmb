
#if !defined(AFX_LOADMB_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_)
#define AFX_LOADMB_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"
#include "hlAdo.h"

#define  LAST_CHAR_N    1              // Last char is a digit
#define  LAST_CHAR_DQ   2              // Last char is double quote

#define  OPEN_ERR       0xF0000001
#define  READ_ERR       0xF0000002
#define  WRITE_ERR      0xF0000004

#define  UPDATE_R01     0
#define  CREATE_R01     1
#define  CREATE_LIEN    2
#define  CLEAR_R01      4

#define  LOAD_LIEN      0x10000000
#define  LOAD_UPDT      0x01000000
#define  LOAD_GRGR      0x00100000
#define  LOAD_SALE      0x00010000
#define  LOAD_ATTR      0x00001000
#define  LOAD_ASSR      0x00000100
#define  LOAD_DAILY     0x00000010
#define  LOAD_ZIP       0x00000001

#define  EXTR_LIEN      0x20000000
#define  EXTR_CSAL      0x02000000
#define  EXTR_VALUE     0x00200000
#define  EXTR_SALE      0x00020000
#define  EXTR_ATTR      0x00002000
#define  EXTR_REGN      0x00000200
#define  EXTR_PRP8      0x00000020
#define  EXTR_FVAL      0x00000002

#define  MERG_LIEN      0x40000000
#define  MERG_CSAL      0x04000000
#define  MERG_GRGR      0x00400000
#define  MERG_GISA      0x00040000
#define  MERG_ATTR      0x00004000
#define  MERG_SADR      0x00000400
#define  MERG_PUBL      0x00000040
//#define  MERG_XFER      0x00000004
#define  MERG_ZONE      0x00000004

// lOptMisc
#define  M_OPT_ROLLCOR  0x02000000		// Load roll correction
#define  M_OPT_MERGVAL  0x00020000		// Merge value 

#define  EXTR_IVAL      0x80000000
#define  EXTR_ISAL      0x08000000
#define  EXTR_IGRGR     0x00800000
#define  UPDT_XSAL      0x00080000
#define  UPDT_ASSR      0x00008000
#define  EXTR_NRSAL     0x00000800     // Extract sale from NDC Recorder data
#define  FIX_CSDOC      0x00000080
#define  FIX_CSTYP      0x00000008

// MON_LIEN.CSV and CAL_LIEN.CSV
#define  L_ASMT                        0
#define  L_TAXYEAR                     1
#define  L_ROLLCHGNUM                  2
#define  L_MAPCATEGORY                 3
#define  L_ROLLCATEGORY                4
#define  L_ROLLTYPE                    5
#define  L_DESTINATIONROLL             6
#define  L_INSTALLMENTS                7
#define  L_BILLTYPE                    8
#define  L_FEEPARCEL                   9
#define  L_ORIGINATINGASMT             10
#define  L_XREFASMT                    11
#define  L_STATUS                      12
#define  L_TRA                         13
#define  L_TAXABILITY                  14
#define  L_ACRES                       15
#define  L_SIZEACRESFTTYPE             16
#define  L_INTDATEFROM                 17
#define  L_INTDATEFROM2                18
#define  L_INTDATETHRU                 19
#define  L_INTERESTTYPE                20
#define  L_USECODE                     21
#define  L_CURRENTMARKETLANDVALUE      22
#define  L_CURRENTFIXEDIMPRVALUE       23
#define  L_CURRENTGROWINGIMPRVALUE     24
#define  L_CURRENTSTRUCTURALIMPRVALUE  25
#define  L_CURRENTPERSONALPROPVALUE    26
#define  L_CURRENTPERSONALPROPMHVALUE  27
#define  L_CURRENTNETVALUE             28
#define  L_BILLEDMARKETLANDVALUE       29
#define  L_BILLEDFIXEDIMPRVALUE        30
#define  L_BILLEDGROWINGIMPRVALUE      31
#define  L_BILLEDSTRUCTURALIMPRVALUE   32
#define  L_BILLEDPERSONALPROPVALUE     33
#define  L_BILLEDPERSONALPROPMHVALUE   34
#define  L_BILLEDNETVALUE              35
#define  L_OWNER                       36
#define  L_ASSESSEE                    37
#define  L_MAILADDRESS1                38
#define  L_MAILADDRESS2                39
#define  L_MAILADDRESS3                40
#define  L_MAILADDRESS4                41
#define  L_ZIPMATCH                    42
#define  L_BARCODEZIP                  43
#define  L_EXEMPTIONCODE1              44
#define  L_EXEMPTIONAMT1               45
#define  L_EXEMPTIONCODE2              46
#define  L_EXEMPTIONAMT2               47
#define  L_EXEMPTIONCODE3              48
#define  L_EXEMPTIONAMT3               49
#define  L_BILLDATE                    50
#define  L_DUEDATE1                    51
#define  L_DUEDATE2                    52
#define  L_TAXAMT1                     53
#define  L_TAXAMT2                     54
#define  L_PENAMT1                     55
#define  L_PENAMT2                     56
#define  L_COST1                       57
#define  L_COST2                       58
#define  L_PENCHRGDATE1                59
#define  L_PENCHRGDATE2                60
#define  L_PAIDAMT1                    61
#define  L_PAIDAMT2                    62
#define  L_PAYMENTDATE1                63
#define  L_PAYMENTDATE2                64
#define  L_TRANSDATE1                  65
#define  L_TRANSDATE2                  66
#define  L_COLLECTIONNUM1              67
#define  L_COLLECTIONNUM2              68
#define  L_UNSDELINQPENAMTPAID1        69
#define  L_SECDELINQPENAMTPAID2        70
#define  L_TOTALFEESPAID1              71
#define  L_TOTALFEESPAID2              72
#define  L_TOTALFEES                   73
#define  L_PMTCNLDATE1                 74
#define  L_PMTCNLDATE2                 75
#define  L_TRANSFERDATE                76
#define  L_PRORATIONFACTOR             77
#define  L_PRORATIONPCT                78
#define  L_DAYSTOFISCALYEAREND         79
#define  L_DAYSOWNED                   80
#define  L_OWNFROM                     81
#define  L_OWNTHRU                     82
#define  L_SITUS1                      83
#define  L_SITUS2                      84
#define  L_ASMTROLLYEAR                85
#define  L_PARCELDESCRIPTION           86
#define  L_BILLCOMMENTSLINE1           87
#define  L_BILLCOMMENTSLINE2           88
#define  L_DEFAULTNUM                  89
#define  L_DEFAULTDATE                 90
#define  L_REDEEMEDDATE                91
#define  L_SECDELINQFISCALYEAR         92
#define  L_PRIORTAXPAID1               93
#define  L_PENINTERESTCODE             94
#define  L_FOURPAYPLANNUM              95
#define  L_EXISTSLIEN                  96
#define  L_EXISTSCORTAC                97
#define  L_EXISTSCOUNTYCORTAC          98
#define  L_EXISTSBANKRUPTCY            99
#define  L_EXISTSREFUND                100
#define  L_EXISTSDELINQUENTVESSEL      101
#define  L_EXISTSNOTES                 102
#define  L_EXISTSCRITICALNOTE          103
#define  L_EXISTSROLLCHG               104
#define  L_ISPENCHRGCANCELED1          105
#define  L_ISPENCHRGCANCELED2          106
#define  L_ISPERSONALPROPERTYPENALTY   107
#define  L_ISAGPRESERVE                108
#define  L_ISCARRYOVER1STPAID          109
#define  L_ISCARRYOVERRECORD           110
#define  L_ISFAILURETOFILE             111
#define  L_ISOWNERSHIPPENALTY          112
#define  L_ISELIGIBLEFOR4PAY           113
#define  L_ISNOTICE4SENT               114
#define  L_ISPARTPAY                   115
#define  L_ISFORMATTEDADDRESS          116
#define  L_ISADDRESSCONFIDENTIAL       117
#define  L_SUPLCNT                     118
#define  L_ORIGINALDUEDATE1            119
#define  L_ORIGINALDUEDATE2            120
#define  L_ISPEN1REFUND                121
#define  L_ISPEN2REFUND                122
#define  L_ISDELINQPENREFUND           123
#define  L_ISREFUNDAUTHORIZED          124
#define  L_ISNODISCHARGE               125
#define  L_ISAPPEALPENDING             126
#define  L_OTHERFEESPAID               127
#define  L_FOURPAYREASON               128
#define  L_DATEDISCHARGED              129
#define  L_ISONLYFIRSTPAID             130
#define  L_ISALLPAID                   131
#define  L_DTS                         132
#define  L_USERID                      133

// Group MB1
#define  L1_ASMT                        0
#define  L1_TAXYEAR                     1
#define  L1_MAPCATEGORY                 2
#define  L1_ROLLCATEGORY                3
#define  L1_ROLLTYPE                    4
#define  L1_DESTINATIONROLL             5
#define  L1_INSTALLMENTS                6
#define  L1_BILLTYPE                    7
#define  L1_FEEPARCEL                   8
#define  L1_ORIGINATINGASMT             9
#define  L1_XREFASMT                    10
#define  L1_STATUS                      11
#define  L1_TRA                         12
#define  L1_TAXABILITY                  13
#define  L1_ACRES                       14
#define  L1_SIZEACRESFTTYPE             15
#define  L1_INTDATEFROM                 16
#define  L1_INTDATEFROM2                17
#define  L1_INTDATETHRU                 18
#define  L1_INTERESTTYPE                19
#define  L1_USECODE                     20
#define  L1_CURRENTMARKETLANDVALUE      21
#define  L1_CURRENTFIXEDIMPRVALUE       22
#define  L1_CURRENTGROWINGIMPRVALUE     23
#define  L1_CURRENTSTRUCTURALIMPRVALUE  24
#define  L1_CURRENTPERSONALPROPVALUE    25
#define  L1_CURRENTPERSONALPROPMHVALUE  26
#define  L1_CURRENTNETVALUE             27
#define  L1_BILLEDMARKETLANDVALUE       28
#define  L1_BILLEDFIXEDIMPRVALUE        29
#define  L1_BILLEDGROWINGIMPRVALUE      30
#define  L1_BILLEDSTRUCTURALIMPRVALUE   31
#define  L1_BILLEDPERSONALPROPVALUE     32
#define  L1_BILLEDPERSONALPROPMHVALUE   33
#define  L1_BILLEDNETVALUE              34
#define  L1_OWNER                       35
#define  L1_ASSESSEE                    36
#define  L1_MAILADDRESS1                37
#define  L1_MAILADDRESS2                38
#define  L1_MAILADDRESS3                39
#define  L1_MAILADDRESS4                40
#define  L1_ZIPMATCH                    41
#define  L1_BARCODEZIP                  42
#define  L1_EXEMPTIONCODE1              43
#define  L1_EXEMPTIONAMT1               44
#define  L1_EXEMPTIONCODE2              45
#define  L1_EXEMPTIONAMT2               46
#define  L1_EXEMPTIONCODE3              47
#define  L1_EXEMPTIONAMT3               48
#define  L1_BILLDATE                    49
#define  L1_DUEDATE1                    50
#define  L1_DUEDATE2                    51
#define  L1_TAXAMT1                     52
#define  L1_TAXAMT2                     53
#define  L1_PENAMT1                     54
#define  L1_PENAMT2                     55
#define  L1_COST1                       56
#define  L1_COST2                       57
#define  L1_PENCHRGDATE1                58
#define  L1_PENCHRGDATE2                59
#define  L1_PAIDAMT1                    60
#define  L1_PAIDAMT2                    61
#define  L1_PAYMENTDATE1                62   // date9
#define  L1_PAYMENTDATE2                63
#define  L1_TRANSDATE1                  64
#define  L1_TRANSDATE2                  65
#define  L1_COLLECTIONNUM1              66
#define  L1_COLLECTIONNUM2              67
#define  L1_UNSDELINQPENAMTPAID1        68
#define  L1_SECDELINQPENAMTPAID2        69
#define  L1_TOTALFEESPAID1              70
#define  L1_TOTALFEESPAID2              71
#define  L1_TOTALFEES                   72
#define  L1_PMTCNLDATE1                 73
#define  L1_PMTCNLDATE2                 74
#define  L1_TRANSFERDATE                75   // date15
#define  L1_PRORATIONFACTOR             76
#define  L1_PRORATIONPCT                77
#define  L1_DAYSTOFISCALYEAREND         78
#define  L1_DAYSOWNED                   79
#define  L1_OWNFROM                     80
#define  L1_OWNTHRU                     81
#define  L1_SITUS1                      82
#define  L1_SITUS2                      83
#define  L1_ASMTROLLYEAR                84
#define  L1_PARCELDESCRIPTION           85
#define  L1_BILLCOMMENTSLINE1           86
#define  L1_BILLCOMMENTSLINE2           87
#define  L1_DEFAULTNUM                  88
#define  L1_DEFAULTDATE                 89   // date18
#define  L1_REDEEMEDDATE                90
#define  L1_SECDELINQFISCALYEAR         91
#define  L1_PRIORTAXPAID1               92
#define  L1_PENINTERESTCODE             93
#define  L1_FOURPAYPLANNUM              94
#define  L1_EXISTSLIEN                  95
#define  L1_EXISTSCORTAC                96
#define  L1_EXISTSCOUNTYCORTAC          97
#define  L1_EXISTSBANKRUPTCY            98
#define  L1_EXISTSREFUND                99
#define  L1_EXISTSDELINQUENTVESSEL      100
#define  L1_EXISTSNOTES                 101
#define  L1_EXISTSCRITICALNOTE          102
#define  L1_EXISTSROLLCHG               103
#define  L1_ISPENCHRGCANCELED1          104
#define  L1_ISPENCHRGCANCELED2          105
#define  L1_ISPERSONALPROPERTYPENALTY   106
#define  L1_ISAGPRESERVE                107
#define  L1_ISCARRYOVER1STPAID          108
#define  L1_ISCARRYOVERRECORD           109
#define  L1_ISFAILURETOFILE             110
#define  L1_ISOWNERSHIPPENALTY          111
#define  L1_ISELIGIBLEFOR4PAY           112
#define  L1_ISNOTICE4SENT               113
#define  L1_ISPARTPAY                   114
#define  L1_ISFORMATTEDADDRESS          115
#define  L1_ISADDRESSCONFIDENTIAL       116
#define  L1_SUPLCNT                     117
#define  L1_ORIGINALDUEDATE1            118
#define  L1_ORIGINALDUEDATE2            119  // date21
#define  L1_ISPEN1REFUND                120
#define  L1_ISPEN2REFUND                121
#define  L1_ISDELINQPENREFUND           122
#define  L1_ISREFUNDAUTHORIZED          123
#define  L1_ISNODISCHARGE               124
#define  L1_ISAPPEALPENDING             125
#define  L1_OTHERFEESPAID               126
#define  L1_FOURPAYREASON               127
#define  L1_DATEDISCHARGED              128
#define  L1_ISONLYFIRSTPAID             129
#define  L1_ISALLPAID                   131
#define  L1_DTS                         132
#define  L1_USERID                      133
#define  L1_CORTAC                      134

#define  L2_ASMT                        0
#define  L2_TAXYEAR                     1
#define  L2_ROLLCHGNUM                  2
#define  L2_MAPCATEGORY                 3
#define  L2_ROLLCATEGORY                4
#define  L2_ROLLTYPE                    5
#define  L2_DESTINATIONROLL             6
#define  L2_INSTALLMENTS                7
#define  L2_BILLTYPE                    8
#define  L2_FEEPARCEL                   9
#define  L2_ORIGINATINGASMT             10
#define  L2_XREFASMT                    11
#define  L2_STATUS                      12
#define  L2_TRA                         13
#define  L2_TAXABILITY                  14
#define  L2_ACRES                       15
#define  L2_SIZEACRESFTTYPE             16
#define  L2_INTDATEFROM                 17
#define  L2_INTDATEFROM2                18
#define  L2_INTDATETHRU                 19
#define  L2_INTERESTTYPE                20
#define  L2_USECODE                     21
#define  L2_CURRENTMARKETLANDVALUE      22
#define  L2_CURRENTFIXEDIMPRVALUE       23
#define  L2_CURRENTGROWINGIMPRVALUE     24
#define  L2_CURRENTSTRUCTURALIMPRVALUE  25
#define  L2_CURRENTPERSONALPROPVALUE    26
#define  L2_CURRENTPERSONALPROPMHVALUE  27
#define  L2_CURRENTNETVALUE             28
#define  L2_BILLEDMARKETLANDVALUE       29
#define  L2_BILLEDFIXEDIMPRVALUE        30
#define  L2_BILLEDGROWINGIMPRVALUE      31
#define  L2_BILLEDSTRUCTURALIMPRVALUE   32
#define  L2_BILLEDPERSONALPROPVALUE     33
#define  L2_BILLEDPERSONALPROPMHVALUE   34
#define  L2_BILLEDNETVALUE              35
#define  L2_OWNER                       36
#define  L2_ASSESSEE                    37
#define  L2_MAILADDRESS1                38
#define  L2_MAILADDRESS2                39
#define  L2_MAILADDRESS3                40
#define  L2_MAILADDRESS4                41
#define  L2_ZIPMATCH                    42
#define  L2_BARCODEZIP                  43
#define  L2_EXEMPTIONCODE1              44
#define  L2_EXEMPTIONAMT1               45
#define  L2_EXEMPTIONCODE2              46
#define  L2_EXEMPTIONAMT2               47
#define  L2_EXEMPTIONCODE3              48
#define  L2_EXEMPTIONAMT3               49
#define  L2_BILLDATE                    50
#define  L2_DUEDATE1                    51
#define  L2_DUEDATE2                    52
#define  L2_TAXAMT1                     53
#define  L2_TAXAMT2                     54
#define  L2_PENAMT1                     55
#define  L2_PENAMT2                     56
#define  L2_COST1                       57
#define  L2_COST2                       58
#define  L2_PENCHRGDATE1                59
#define  L2_PENCHRGDATE2                60
#define  L2_PAIDAMT1                    61
#define  L2_PAIDAMT2                    62
#define  L2_PAYMENTDATE1                63
#define  L2_PAYMENTDATE2                64
#define  L2_TRANSDATE1                  65
#define  L2_TRANSDATE2                  66
#define  L2_COLLECTIONNUM1              67
#define  L2_COLLECTIONNUM2              68
#define  L2_UNSDELINQPENAMTPAID1        69
#define  L2_SECDELINQPENAMTPAID2        70
#define  L2_TOTALFEESPAID1              71
#define  L2_TOTALFEESPAID2              72
#define  L2_TOTALFEES                   73
#define  L2_PMTCNLDATE1                 74
#define  L2_PMTCNLDATE2                 75
#define  L2_TRANSFERDATE                76
#define  L2_PRORATIONFACTOR             77
#define  L2_PRORATIONPCT                78
#define  L2_DAYSTOFISCALYEAREND         79
#define  L2_DAYSOWNED                   80
#define  L2_OWNFROM                     81
#define  L2_OWNTHRU                     82
#define  L2_SITUS1                      83
#define  L2_SITUS2                      84
#define  L2_ASMTROLLYEAR                85
#define  L2_PARCELDESCRIPTION           86
#define  L2_BILLCOMMENTSLINE1           87
#define  L2_BILLCOMMENTSLINE2           88
#define  L2_DEFAULTNUM                  89
#define  L2_DEFAULTDATE                 90
#define  L2_REDEEMEDDATE                91
#define  L2_SECDELINQFISCALYEAR         92
#define  L2_PRIORTAXPAID1               93
#define  L2_PENINTERESTCODE             94
#define  L2_FOURPAYPLANNUM              95
#define  L2_EXISTSLIEN                  96
#define  L2_EXISTSCORTAC                97   //
#define  L2_EXISTSCOUNTYCORTAC          98   //
#define  L2_EXISTSBANKRUPTCY            99   //
#define  L2_EXISTSREFUND                100
#define  L2_EXISTSDELINQUENTVESSEL      101
#define  L2_EXISTSNOTES                 102  // 
#define  L2_EXISTSCRITICALNOTE          103  //
#define  L2_EXISTSROLLCHG               104
#define  L2_ISPENCHRGCANCELED1          105
#define  L2_ISPENCHRGCANCELED2          106
#define  L2_ISPERSONALPROPERTYPENALTY   107
#define  L2_ISAGPRESERVE                108
#define  L2_ISCARRYOVER1STPAID          109
#define  L2_ISCARRYOVERRECORD           110
#define  L2_ISFAILURETOFILE             111
#define  L2_ISOWNERSHIPPENALTY          112
#define  L2_ISELIGIBLEFOR4PAY           113
#define  L2_ISNOTICE4SENT               114
#define  L2_ISPARTPAY                   115
#define  L2_ISFORMATTEDADDRESS          116
#define  L2_ISADDRESSCONFIDENTIAL       117
#define  L2_SUPLCNT                     118
#define  L2_ORIGINALDUEDATE1            119
#define  L2_ORIGINALDUEDATE2            120
#define  L2_ISPEN1REFUND                121
#define  L2_ISPEN2REFUND                122
#define  L2_ISDELINQPENREFUND           123
#define  L2_ISREFUNDAUTHORIZED          124
#define  L2_ISNODISCHARGE               125
#define  L2_ISAPPEALPENDING             126
#define  L2_OTHERFEESPAID               127
#define  L2_FOURPAYREASON               128
#define  L2_DATEDISCHARGED              129
#define  L2_ISONLYFIRSTPAID             130
#define  L2_ISALLPAID                   131
#define  L2_DTS                         132
#define  L2_USERID                      133
// Not all group 2 counties have following two fields
#define  L2_TAX_RATEUSED                134
#define  L2_TAX_ROLLCHGNUM              135

// 2016 AMA,BUT,GLE,HUM,LAK,MAD,MER,MNO,SHA,STA,TEH,YOL,YUB
// 2017 SON,SIS,CAL,SBT,SJX
// 2019 EDX,COL
// 2021 DNX
// 2022 MOD, MON
#define  L3_TAXYEAR                    0
#define  L3_ROLLCATEGORY               1
#define  L3_ASMT                       2
#define  L3_FEEPARCEL                  3
#define  L3_ASMTSTATUS                 4
#define  L3_TRA                        5
#define  L3_TAXABILITYFULL             6
#define  L3_OWNER                      7
#define  L3_ASSESSEE                   8
#define  L3_MAILADDRESS1               9
#define  L3_MAILADDRESS2               10
#define  L3_MAILADDRESS3               11
#define  L3_MAILADDRESS4               12
#define  L3_BARCODEZIP                 13
#define  L3_SITUS1                     14
#define  L3_SITUS2                     15
#define  L3_PARCELDESCRIPTION          16
#define  L3_LANDVALUE                  17
#define  L3_STRUCTUREVALUE             18
#define  L3_FIXTURESVALUE              19
#define  L3_GROWING                    20
#define  L3_FIXTURESRP                 21
#define  L3_MHPPVALUE                  22
#define  L3_PPVALUE                    23
#define  L3_HOX                        24
#define  L3_OTHEREXEMPTION             25
#define  L3_OTHEREXEMPTIONCODE         26
#define  L3_LANDUSE1                   27
#define  L3_LANDSIZE                   28
#define  L3_ACRES                      29
#define  L3_BUILDINGTYPE               30
#define  L3_QUALITYCLASS               31
#define  L3_BUILDINGSIZE               32
#define  L3_YEARBUILT                  33
#define  L3_TOTALAREA                  34
#define  L3_BEDROOMS                   35
#define  L3_BATHS                      36
#define  L3_HALFBATHS                  37
#define  L3_TOTALROOMS                 38
#define  L3_GARAGE                     39
#define  L3_GARAGESIZE                 40
#define  L3_HEATING                    41
#define  L3_AC                         42
#define  L3_FIREPLACE                  43
#define  L3_POOLSPA                    44
#define  L3_STORIES                    45
#define  L3_UNITS                      46
#define  L3_CURRENTDOCNUM              47
#define  L3_CURRENTDOCDATE             48
#define  L3_LANDUSE2                   49    // EDX
#define  L3_ISAGPRESERVE               49    // MER,NEV,TUO 2019, ALP 2020,DNX 2021-2022, YOL 2024
#define  L3_FLDS                       50

// 2023 AMA
#define  L31_TAXYEAR                   0
#define  L31_ROLLCATEGORY              1
#define  L31_ASMT                      2
#define  L31_FEEPARCEL                 3
#define  L31_ASMTSTATUS                4
#define  L31_TRA                       5
#define  L31_TAXABILITYFULL            6
#define  L31_OWNER                     7
#define  L31_ASSESSEE                  8
#define  L31_MAILADDRESS1              9
#define  L31_MAILADDRESS2              10
#define  L31_MAILADDRESS3              11
#define  L31_MAILADDRESS4              12
#define  L31_BARCODEZIP                13
#define  L31_SITUS1                    14
#define  L31_SITUS2                    15
#define  L31_PARCELDESCRIPTION         16
#define  L31_LANDVALUE                 17
#define  L31_STRUCTUREVALUE            18
#define  L31_FIXTURESVALUE             19
#define  L31_GROWING                   20
#define  L31_FIXTURESRP                21
#define  L31_MHPPVALUE                 22
#define  L31_PPVALUE                   23
#define  L31_HOX                       24
#define  L31_OTHEREXEMPTION            25
#define  L31_OTHEREXEMPTIONCODE        26
#define  L31_LANDUSE1                  27
#define  L31_LANDSIZE                  28
#define  L31_ACRES                     29
#define  L31_BUILDINGTYPE              30
#define  L31_QUALITYCLASS              31
#define  L31_BUILDINGSIZE              32
#define  L31_YEARBUILT                 33
#define  L31_TOTALAREA                 34
#define  L31_BEDROOMS                  35
#define  L31_BATHS                     36
#define  L31_HALFBATHS                 37
#define  L31_TOTALROOMS                38
#define  L31_GARAGE                    39
#define  L31_GARAGESIZE                40
#define  L31_HEATING                   41
#define  L31_AC                        42
#define  L31_FIREPLACE                 43
#define  L31_POOLSPA                   44
#define  L31_STORIES                   45
#define  L31_UNITS                     46
#define  L31_CURRENTDOCNUM             47
#define  L31_ISAGPRESERVE              48

// 2016: SON, SBT 
#define  L4_TAXYEAR                    0
#define  L4_ROLLCATEGORY               1
#define  L4_ASMT                       2
#define  L4_FEEPARCEL                  3
#define  L4_ASMTSTATUS                 4
#define  L4_TRA                        5
#define  L4_MAPCATEGORY                6
#define  L4_OWNER                      7
#define  L4_ASSESSEE                   8
#define  L4_MAILADDRESS1               9
#define  L4_MAILADDRESS2               10
#define  L4_MAILADDRESS3               11
#define  L4_MAILADDRESS4               12
#define  L4_ZIPMATCH                   13
#define  L4_BARCODEZIP                 14
#define  L4_SITUS1                     15
#define  L4_SITUS2                     16
#define  L4_PARCELDESCRIPTION          17
#define  L4_LANDVALUE                  18
#define  L4_FIXTURESVALUE              19
#define  L4_GROWING                    20
#define  L4_STRUCTUREVALUE             21
#define  L4_PPVALUE                    22
#define  L4_MHPPVALUE                  23
#define  L4_NETVALUE                   24
#define  L4_BILLEDMARKETLANDVALUE      25
#define  L4_BILLEDFIXEDIMPRVALUE       26
#define  L4_BILLEDGROWINGIMPRVALUE     27
#define  L4_BILLEDSTRUCTURALIMPRVALUE  28
#define  L4_BILLEDPERSONALPROPVALUE    29
#define  L4_BILLEDPERSONALPROPMHVALUE  30
#define  L4_BILLEDNETVALUE             31
#define  L4_EXEMPTIONCODE1             32
#define  L4_EXEMPTIONAMT1              33
#define  L4_EXEMPTIONCODE2             34
#define  L4_EXEMPTIONAMT2              35
#define  L4_EXEMPTIONCODE3             36
#define  L4_EXEMPTIONAMT3              37
#define  L4_LANDUSE1                   38
#define  L4_LANDSIZE                   39
#define  L4_ACRES                      40
#define  L4_BUILDINGTYPE               41
#define  L4_QUALITYCLASS               42
#define  L4_BUILDINGSIZE               43
#define  L4_YEARBUILT                  44
#define  L4_TOTALAREA                  45
#define  L4_BEDROOMS                   46
#define  L4_BATHS                      47
#define  L4_HALFBATHS                  48
#define  L4_TOTALROOMS                 49
#define  L4_GARAGE                     50
#define  L4_GARAGESIZE                 51
#define  L4_HEATING                    52
#define  L4_AC                         53
#define  L4_FIREPLACE                  54
#define  L4_POOLSPA                    55
#define  L4_STORIES                    56
#define  L4_UNITS                      57
#define  L4_DOCNUM                     58
#define  L4_DOCDATE                    59

// 2017 MNO 
#define  L5_TAXYEAR                    0
#define  L5_ROLLCATEGORY               1
#define  L5_ASMT                       2
#define  L5_FEEPARCEL                  3
#define  L5_ASMTSTATUS                 4
#define  L5_TRA                        5
#define  L5_TAXABILITYFULL             6
#define  L5_OWNER                      7
#define  L5_ASSESSEE                   8
#define  L5_MAILADDRESS1               9
#define  L5_MAILADDRESS2               10
#define  L5_MAILADDRESS3               11
#define  L5_MAILADDRESS4               12
#define  L5_BARCODEZIP                 13
#define  L5_SITUS1                     14
#define  L5_SITUS2                     15
#define  L5_PARCELDESCRIPTION          16
#define  L5_LANDVALUE                  17
#define  L5_STRUCTUREVALUE             18
#define  L5_FIXTURESVALUE              19
#define  L5_FIXTURESRP                 20
#define  L5_MHPPVALUE                  21
#define  L5_PPVALUE                    22
#define  L5_HOX                        23
#define  L5_OTHEREXEMPTION             24
#define  L5_OTHEREXEMPTIONCODE         25
#define  L5_LANDUSE1                   26
#define  L5_LANDSIZE                   27
#define  L5_ACRES                      28
#define  L5_BUILDINGTYPE               29
#define  L5_QUALITYCLASS               30
#define  L5_BUILDINGSIZE               31
#define  L5_YEARBUILT                  32
#define  L5_TOTALAREA                  33
#define  L5_BEDROOMS                   34
#define  L5_BATHS                      35
#define  L5_HALFBATHS                  36
#define  L5_TOTALROOMS                 37
#define  L5_GARAGE                     38
#define  L5_GARAGESIZE                 39
#define  L5_HEATING                    40
#define  L5_AC                         41
#define  L5_FIREPLACE                  42
#define  L5_POOLSPA                    43
#define  L5_STORIES                    44
#define  L5_UNITS                      45
#define  L5_CURRENTDOCNUM              46
#define  L5_CURRENTDOCDATE             47

// 2018 COL L6_
// 2018 CAL L7_
// 2018 MNO L8_
#define  L8_TAXYEAR                    0
#define  L8_ROLLCATEGORY               1
#define  L8_ASMT                       2
#define  L8_FEEPARCEL                  3
#define  L8_ASMTSTATUS                 4
#define  L8_TRA                        5
#define  L8_TAXABILITYFULL             6
#define  L8_OWNER                      7
#define  L8_ASSESSEE                   8
#define  L8_MAILADDRESS1               9
#define  L8_MAILADDRESS2               10
#define  L8_MAILADDRESS3               11
#define  L8_MAILADDRESS4               12
#define  L8_SITUS1                     13
#define  L8_SITUS2                     14
#define  L8_PARCELDESCRIPTION          15
#define  L8_LANDVALUE                  16
#define  L8_STRUCTUREVALUE             17
#define  L8_FIXTURESVALUE              18
#define  L8_FIXTURESRP                 19
#define  L8_MHPPVALUE                  20
#define  L8_PPVALUE                    21
#define  L8_HOX                        22
#define  L8_OTHEREXEMPTION             23
#define  L8_OTHEREXEMPTIONCODE         24
#define  L8_LANDUSE1                   25
#define  L8_LANDSIZE                   26
#define  L8_ACRES                      27
#define  L8_BUILDINGTYPE               28
#define  L8_QUALITYCLASS               29
#define  L8_BUILDINGSIZE               30
#define  L8_YEARBUILT                  31
#define  L8_TOTALAREA                  32
#define  L8_BEDROOMS                   33
#define  L8_BATHS                      34
#define  L8_HALFBATHS                  35
#define  L8_TOTALROOMS                 36
#define  L8_GARAGE                     37
#define  L8_GARAGESIZE                 38
#define  L8_HEATING                    39
#define  L8_AC                         40
#define  L8_FIREPLACE                  41
#define  L8_POOLSPA                    42
#define  L8_STORIES                    43
#define  L8_UNITS                      44
#define  L8_CURRENTDOCNUM              45

// 2018 DNX L9_

// 2018 EDX
#define  L10_TAXYEAR                   0
#define  L10_ROLLCATEGORY              1
#define  L10_ASMT                      2
#define  L10_FEEPARCEL                 3
#define  L10_ASMTSTATUS                4
#define  L10_TRA                       5
#define  L10_TAXABILITYFULL            6
#define  L10_OWNER                     7
#define  L10_ASSESSEE                  8
#define  L10_MAILADDRESS1              9
#define  L10_MAILADDRESS2              10
#define  L10_MAILADDRESS3              11
#define  L10_MAILADDRESS4              12
#define  L10_BARCODEZIP                13
#define  L10_SITUS1                    14
#define  L10_SITUS2                    15
#define  L10_PARCELDESCRIPTION         16
#define  L10_LANDVALUE                 17
#define  L10_STRUCTUREVALUE            18
#define  L10_FIXTURESVALUE             19
#define  L10_GROWING                   20
#define  L10_FIXTURESRP                21
#define  L10_MHPPVALUE                 22
#define  L10_PPVALUE                   23
#define  L10_HOX                       24
#define  L10_OTHEREXEMPTION            25
#define  L10_OTHEREXEMPTIONCODE        26
#define  L10_LANDUSE1                  27
#define  L10_LANDUSE2                  28
#define  L10_LANDSIZE                  29
#define  L10_ACRES                     30
#define  L10_BUILDINGTYPE              31
#define  L10_QUALITYCLASS              32
#define  L10_BUILDINGSIZE              33
#define  L10_YEARBUILT                 34
#define  L10_TOTALAREA                 35
#define  L10_BEDROOMS                  36
#define  L10_BATHS                     37
#define  L10_HALFBATHS                 38
#define  L10_TOTALROOMS                39
#define  L10_GARAGE                    40
#define  L10_GARAGESIZE                41
#define  L10_HEATING                   42
#define  L10_AC                        43
#define  L10_FIREPLACE                 44
#define  L10_POOLSPA                   45
#define  L10_SEWER                     46      // Y/N
#define  L10_STORIES                   47
#define  L10_UNITS                     48
#define  L10_CURRENTDOCNUM             49
#define  L10_CURRENTDOCDATE            50
#define  L10_ISAGPRESERVE              51

// 2019 MNO
#define  L11_TAXYEAR                   0
#define  L11_ASMT                      1
#define  L11_FEEPARCEL                 2
#define  L11_ASMTSTATUS                3
#define  L11_TRA                       4
#define  L11_TAXABILITY                5
#define  L11_OWNER                     6
#define  L11_ASSESSEE                  7
#define  L11_MAILADDRESS1              8
#define  L11_MAILADDRESS2              9
#define  L11_MAILADDRESS3              10
#define  L11_MAILADDRESS4              11
#define  L11_BARCODEZIP                12
#define  L11_PARCELDESCRIPTION         13
#define  L11_LANDVALUE                 14
#define  L11_STRUCTUREVALUE            15
#define  L11_FIXTURESVALUE             16
#define  L11_GROWINGVALUE              17
#define  L11_FIXTURESRP                18
#define  L11_MHPPVALUE                 19
#define  L11_PPVALUE                   20
#define  L11_HOX                       21
#define  L11_OTHEREXEMPTION            22
#define  L11_OTHEREXEMPTIONCODE        23
#define  L11_LANDUSE1                  24
#define  L11_LANDSIZE                  25
#define  L11_ACRES                     26
#define  L11_BUILDINGTYPE              27
#define  L11_QUALITYCLASS              28
#define  L11_BUILDINGSIZE              29
#define  L11_YEARBUILT                 30
#define  L11_TOTALAREA                 31
#define  L11_BEDROOMS                  32
#define  L11_BATHS                     33
#define  L11_HALFBATHS                 34
#define  L11_TOTALROOMS                35
#define  L11_GARAGE                    36
#define  L11_GARAGESIZE                37
#define  L11_HEATING                   38
#define  L11_AC                        39
#define  L11_FIREPLACE                 40
#define  L11_POOLSPA                   41
#define  L11_STORIES                   42
#define  L11_UNITS                     43
#define  L11_CURRENTDOCNUM             44
#define  L11_CURRENTDOCDATE            45
#define  L11_ISAGPRESERVE              46
#define  L11_FLDS                      47

// 2019 CAL L12_

// 2020 MNO L13_
#define  L13_TAXYEAR                   0
#define  L13_ROLLCATEGORY              1
#define  L13_ASMT                      2
#define  L13_FEEPARCEL                 3
#define  L13_ASMTSTATUS                4
#define  L13_TRA                       5
#define  L13_TAXABILITYFULL            6
#define  L13_OWNER                     7
#define  L13_ASSESSEE                  8
#define  L13_MAILADDRESS1              9
#define  L13_MAILADDRESS2              10
#define  L13_MAILADDRESS3              11
#define  L13_MAILADDRESS4              12
#define  L13_BARCODEZIP                13
#define  L13_SITUS1                    14
#define  L13_SITUS2                    15
#define  L13_LANDVALUE                 16
#define  L13_STRUCTUREVALUE            17
#define  L13_FIXTURESVALUE             18
#define  L13_GROWINGVALUE              19
#define  L13_FIXTURESRP                20
#define  L13_MHPPVALUE                 21
#define  L13_PPVALUE                   22
#define  L13_HOX                       23
#define  L13_LANDUSE1                  24
#define  L13_ACRES                     25
#define  L13_BUILDINGTYPE              26
#define  L13_QUALITYCLASS              27
#define  L13_BUILDINGSIZE              28
#define  L13_YEARBUILT                 29
#define  L13_TOTALAREA                 30
#define  L13_BEDROOMS                  31
#define  L13_BATHS                     32
#define  L13_HALFBATHS                 33
#define  L13_TOTALROOMS                34
#define  L13_GARAGE                    35
#define  L13_GARAGESIZE                36
#define  L13_HEATING                   37
#define  L13_AC                        38
#define  L13_FIREPLACE                 39
#define  L13_POOLSPA                   40
#define  L13_STORIES                   41
#define  L13_UNITS                     42
#define  L13_CURRENTDOCNUM             43
#define  L13_CURRENTDOCDATE            44
#define  L13_ISAGPRESERVE              45
#define  L13_FLDS                      46

// 2020 DNX L14
#define  L14_TAXYEAR                   0
#define  L14_ASMT                      1
#define  L14_FEEPARCEL                 2
#define  L14_TRA                       3
#define  L14_OWNER                     4
#define  L14_ASSESSEENAME              5
#define  L14_MAILADDRESS1              6
#define  L14_MAILADDRESS2              7
#define  L14_MAILADDRESS3              8
#define  L14_MAILADDRESS4              9
#define  L14_SITUS1                    10
#define  L14_SITUS2                    11
#define  L14_LANDVALUE                 12
#define  L14_STRUCTUREVALUE            13
#define  L14_FIXTURESVALUE             14
#define  L14_MHPPVALUE                 15
#define  L14_PPVALUE                   16
#define  L14_HOX                       17
#define  L14_OTHEREXEMPTION            18
#define  L14_LANDUSE1                  19
#define  L14_ACRES                     20
#define  L14_FLDCOUNT                  21

// 2021 YOL L15_
// 2021 MNO L16_
#define  L16_TAXYEAR                   0
#define  L16_ROLLCATEGORY              1
#define  L16_ASMT                      2
#define  L16_FEEPARCEL                 3
#define  L16_ASMTSTATUS                4
#define  L16_TRA                       5
#define  L16_TAXABILITYFULL            6
#define  L16_OWNER                     7
#define  L16_ASSESSEE                  8
#define  L16_MAILADDRESS1              9
#define  L16_MAILADDRESS2              10
#define  L16_MAILADDRESS3              11
#define  L16_MAILADDRESS4              12
#define  L16_BARCODEZIP                13
#define  L16_SITUS1                    14
#define  L16_SITUS2                    15
#define  L16_PARCELDESCRIPTION         16
#define  L16_LANDVALUE                 17
#define  L16_STRUCTUREVALUE            18
#define  L16_FIXTURESVALUE             19
#define  L16_GROWINGVALUE              20
#define  L16_FIXTURESRP                21
#define  L16_MHPPVALUE                 22
#define  L16_PPVALUE                   23
#define  L16_HOX                       24
#define  L16_OTHEREXEMPTION            25
#define  L16_OTHEREXEMPTIONCODE        26
#define  L16_LANDUSE1                  27
#define  L16_LANDSIZE                  28
#define  L16_ACRES                     29
#define  L16_BUILDINGTYPE              10
#define  L16_QUALITYCLASS              31
#define  L16_BUILDINGSIZE              32
#define  L16_YEARBUILT                 33
#define  L16_TOTALAREA                 34
#define  L16_BEDROOMS                  35
#define  L16_BATHS                     36
#define  L16_HALFBATHS                 37
#define  L16_TOTALROOMS                38
#define  L16_GARAGE                    39
#define  L16_GARAGESIZE                40
#define  L16_HEATING                   41
#define  L16_AC                        42
#define  L16_FIREPLACE                 43
#define  L16_POOLSPA                   44
#define  L16_STORIES                   45
#define  L16_UNITS                     46
#define  L16_CURRENTDOCNUM             47
#define  L16_CURRENTDOCDATE            48
#define  L16_ISAGPRESERVE              49
#define  L16_FLDS                      50

// 2022 - BUT
// 2023 - YOL
#define  L17_TAXYEAR                   0
#define  L17_ROLLCATEGORY              1
#define  L17_ASMT                      2
#define  L17_FEEPARCEL                 3
#define  L17_ASMTSTATUS                4
#define  L17_TRA                       5
#define  L17_TAXABILITYFULL            6
#define  L17_OWNER                     7
#define  L17_ASSESSEE                  8
#define  L17_MAILADDRESS1              9
#define  L17_MAILADDRESS2              10
#define  L17_MAILADDRESS3              11
#define  L17_MAILADDRESS4              12
#define  L17_BARCODEZIP                13
#define  L17_SITUS1                    14
#define  L17_SITUS2                    15
#define  L17_PARCELDESCRIPTION         16
#define  L17_LANDVALUE                 17
#define  L17_STRUCTUREVALUE            18
#define  L17_FIXTURESVALUE             19
#define  L17_GROWINGVALUE              20
#define  L17_FIXTURESRP                21
#define  L17_MHPPVALUE                 22
#define  L17_PPVALUE                   23
#define  L17_HOX                       24
#define  L17_OTHEREXEMPTION            25
#define  L17_OTHEREXEMPTIONCODE        26
#define  L17_LANDUSE1                  27
#define  L17_LANDSIZE                  28
#define  L17_ACRES                     29
#define  L17_CURRENTDOCNUM             30
#define  L17_CURRENTDOCDATE            31
#define  L17_ISAGPRESERVE              32
#define  L17_FLDS                      33

// 2022 MNO
#define  L18_TAXYEAR                   0
#define  L18_ASMT                      1
#define  L18_FEEPARCEL                 2
#define  L18_TRA                       3
#define  L18_TAXABILITY                4
#define  L18_OWNER                     5
#define  L18_ASSESSEE                  6
#define  L18_MAILADDRESS1              7
#define  L18_MAILADDRESS2              8
#define  L18_MAILADDRESS3              9
#define  L18_MAILADDRESS4              10
#define  L18_SITUS1                    11
#define  L18_SITUS2                    12
#define  L18_PARCELDESCRIPTION         13
#define  L18_LANDVALUE                 14
#define  L18_STRUCTUREVALUE            15
#define  L18_FIXTURESVALUE             16
#define  L18_FIXTURESRP                17
#define  L18_MHPPVALUE                 18
#define  L18_PPVALUE                   19
#define  L18_HOX                       20
#define  L18_OTHEREXEMPTION            21
#define  L18_OTHEREXEMPTIONCODE        22
#define  L18_LANDUSE                   23
#define  L18_LANDSIZE                  24
#define  L18_ACRES                     25
#define  L18_BUILDINGTYPE              26
#define  L18_QUALITYCLASS              27
#define  L18_BUILDINGSIZE              28
#define  L18_YEARBUILT                 29
#define  L18_TOTALAREA                 30
#define  L18_BEDROOMS                  31
#define  L18_BATHS                     32
#define  L18_HALFBATHS                 33
#define  L18_TOTALROOMS                34
#define  L18_GARAGE                    35
#define  L18_GARAGESIZE                36
#define  L18_HEATING                   37
#define  L18_AC                        38
#define  L18_FIREPLACE                 39
#define  L18_POOLSPA                   40
#define  L18_STORIES                   41
#define  L18_UNITS                     42
#define  L18_CURRENTDOCNUM             43
#define  L18_FLDS                      44

// 2023 MNO
#define  L19_TAXYEAR                   0
#define  L19_ASMT                      1
#define  L19_FEEPARCEL                 2
#define  L19_TRA                       3
#define  L19_TAXABILITY                4
#define  L19_OWNER                     5
#define  L19_ASSESSEE                  6
#define  L19_MAILADDRESS1              7
#define  L19_MAILADDRESS2              8
#define  L19_MAILADDRESS3              9
#define  L19_MAILADDRESS4              10
#define  L19_SITUS1                    11
#define  L19_SITUS2                    12
#define  L19_PARCELDESCRIPTION         13
#define  L19_LANDVALUE                 14
#define  L19_STRUCTUREVALUE            15
#define  L19_FIXTURESVALUE             16
#define  L19_GROWINGVALUE              17
#define  L19_FIXTURESRP                18
#define  L19_MHPPVALUE                 19
#define  L19_PPVALUE                   20
#define  L19_HOX                       21
#define  L19_LANDSIZE                  22
#define  L19_ACRES                     23
#define  L19_BUILDINGTYPE              24
#define  L19_QUALITYCLASS              25
#define  L19_BUILDINGSIZE              26
#define  L19_YEARBUILT                 27
#define  L19_TOTALAREA                 28
#define  L19_BEDROOMS                  29
#define  L19_BATHS                     30
#define  L19_HALFBATHS                 31
#define  L19_TOTALROOMS                32
#define  L19_GARAGE                    33
#define  L19_GARAGESIZE                34
#define  L19_HEATING                   35
#define  L19_AC                        36
#define  L19_FIREPLACE                 37
#define  L19_POOLSPA                   38
#define  L19_STORIES                   39
#define  L19_UNITS                     40
#define  L19_CURRENTDOCNUM             41
#define  L19_CURRENTDOCDATE            42
#define  L19_FLDS                      43

// 2024 MNO
#define  L20_TAXYEAR                   0
#define  L20_ASMT                      1
#define  L20_FEEPARCEL                 2
#define  L20_TRA                       3
#define  L20_TAXABILITY                4
#define  L20_OWNER                     5
#define  L20_ASSESSEE                  6
#define  L20_MAILADDRESS1              7
#define  L20_MAILADDRESS2              8
#define  L20_MAILADDRESS3              9
#define  L20_MAILADDRESS4              10
#define  L20_SITUS1                    11
#define  L20_SITUS2                    12
#define  L20_PARCELDESCRIPTION         13
#define  L20_LANDVALUE                 14
#define  L20_STRUCTUREVALUE            15
#define  L20_FIXTURESVALUE             16
#define  L20_GROWINGVALUE              17
#define  L20_FIXTURESRP                18
#define  L20_MHPPVALUE                 19
#define  L20_PPVALUE                   20
#define  L20_HOX                       21
#define  L20_OTHEREXEMPTION            22
#define  L20_OTHEREXEMPTIONCODE        23
//#define  L20_LANDUSE                   23
#define  L20_LANDSIZE                  24
#define  L20_ACRES                     25
#define  L20_BUILDINGTYPE              26
#define  L20_QUALITYCLASS              27
#define  L20_BUILDINGSIZE              28
#define  L20_YEARBUILT                 29
#define  L20_TOTALAREA                 30
#define  L20_BEDROOMS                  31
#define  L20_BATHS                     32
#define  L20_HALFBATHS                 33
#define  L20_TOTALROOMS                34
#define  L20_GARAGE                    35
#define  L20_GARAGESIZE                36
#define  L20_HEATING                   37
#define  L20_AC                        38
#define  L20_FIREPLACE                 39
#define  L20_POOLSPA                   40
#define  L20_STORIES                   41
#define  L20_UNITS                     42
#define  L20_CURRENTDOCNUM             43
#define  L20_FLDS                      44


// _ROLL
#define  MB_ROLL_ASMT                 0
#define  MB_ROLL_FEEPARCEL            1
#define  MB_ROLL_TRA                  2
#define  MB_ROLL_LEGAL                3
#define  MB_ROLL_ZONING               4
#define  MB_ROLL_USECODE              5
#define  MB_ROLL_NBHCODE              6
#define  MB_ROLL_ACRES                7
#define  MB_ROLL_DOCNUM               8
#define  MB_ROLL_DOCDATE              9
#define  MB_ROLL_TAXABILITY           10
#define  MB_ROLL_OWNER                11
#define  MB_ROLL_CAREOF               12
#define  MB_ROLL_DBA                  13
#define  MB_ROLL_M_ADDR               14
#define  MB_ROLL_M_CITY               15
#define  MB_ROLL_M_ST                 16
#define  MB_ROLL_M_ZIP                17
#define  MB_ROLL_LAND                 18
#define  MB_ROLL_HOMESITE             19
#define  MB_ROLL_IMPR                 20
#define  MB_ROLL_GROWING              21
#define  MB_ROLL_FIXTRS               22

#define  MB_ROLL_FIXTR_RP             23  // Old script
//#define  MB_ROLL_FIXTR_BUS            24
#define  MB_ROLL_PP_BUS               24

//#define  MB_ROLL_PERSPROP             23  // New script
//#define  MB_ROLL_BUSPROP              24

#define  MB_ROLL_PPMOBILHOME          25
#define  MB_ROLL_M_ADDR1              26
#define  MB_ROLL_M_ADDR2              27
#define  MB_ROLL_M_ADDR3              28
#define  MB_ROLL_M_ADDR4              29

// _EXE
#define  MB_EXE_STATUS                0
#define  MB_EXE_ASMT                  1
#define  MB_EXE_CODE                  2
#define  MB_EXE_HOEXE                 3
#define  MB_EXE_EXEAMT                4
#define  MB_EXE_EXEPCT                5

// _CHAR
// Plu
#define  MB1_CHAR_FEE_PRCL            0
#define  MB1_CHAR_POOLS               1
#define  MB1_CHAR_USECAT              2
#define  MB1_CHAR_QUALITY             3
#define  MB1_CHAR_YRBLT               4
#define  MB1_CHAR_NUMFLOORS           5
#define  MB1_CHAR_BLDGSQFT            6
#define  MB1_CHAR_GARSQFT             7
#define  MB1_CHAR_HEATING             8
#define  MB1_CHAR_COOLING             9
#define  MB1_CHAR_HEATING_SRC         10
#define  MB1_CHAR_COOLING_SRC         11
#define  MB1_CHAR_BEDS                12
#define  MB1_CHAR_FBATHS              13
#define  MB1_CHAR_HBATHS              14
#define  MB1_CHAR_FP                  15
#define  MB1_CHAR_ASMT                16
#define  MB1_CHAR_HASSEPTIC           17
#define  MB1_CHAR_HASSEWER            18
#define  MB1_CHAR_HASWELL             19

// Ama, Sha, Lak, Hum
// This replaces UPDGRP1
#define  MB2_CHAR_FEE_PRCL            0
#define  MB2_CHAR_USECAT              1
#define  MB2_CHAR_BLDGSEQNO           2
#define  MB2_CHAR_BLDGUSECAT          3
#define  MB2_CHAR_UNITSEQNO           4  
#define  MB2_CHAR_UNITUSECAT          5  
#define  MB2_CHAR_POOLS               6
#define  MB2_CHAR_QUALITY             7
#define  MB2_CHAR_YRBLT               8
#define  MB2_CHAR_BLDGSQFT            9
#define  MB2_CHAR_GARSQFT             10
#define  MB2_CHAR_HEATING             11
#define  MB2_CHAR_COOLING             12
#define  MB2_CHAR_HEATING_SRC         13
#define  MB2_CHAR_COOLING_SRC         14
#define  MB2_CHAR_BEDS                15
#define  MB2_CHAR_FBATHS              16
#define  MB2_CHAR_HBATHS              17
#define  MB2_CHAR_FP                  18
#define  MB2_CHAR_ASMT                19
#define  MB2_CHAR_HASSEPTIC           20
#define  MB2_CHAR_HASSEWER            21
#define  MB2_CHAR_HASWELL             22
#define  MB2_CHAR_BLDGTYPE            23
#define  MB2_CHAR_NUMFLOORS           24
#define  MB2_CHAR_DOCNUM              25
#define  MB2_CHAR_ACRES               26
#define  MB2_CHAR_LOTSQFT             27
#define  MB2_CHAR_SIZETYPE            28

// ALP, KIN
#define  MB3_CHAR_FEEPARCEL            0
#define  MB3_CHAR_POOLSPA              1
#define  MB3_CHAR_CATTYPE              2
#define  MB3_CHAR_QUALITYCLASS         3
#define  MB3_CHAR_YRBLT                4
#define  MB3_CHAR_BUILDINGSIZE         5
#define  MB3_CHAR_ATTACHGARAGESF       6
#define  MB3_CHAR_DETACHGARAGESF       7
#define  MB3_CHAR_CARPORTSF            8
#define  MB3_CHAR_HEATING              9
#define  MB3_CHAR_COOLINGCENTRALAC     10
#define  MB3_CHAR_COOLINGEVAPORATIVE   11
#define  MB3_CHAR_COOLINGROOMWALL      12
#define  MB3_CHAR_COOLINGWINDOW        13
#define  MB3_CHAR_STORIESCNT           14
#define  MB3_CHAR_UNITSCNT             15
#define  MB3_CHAR_TOTALROOMS           16
#define  MB3_CHAR_EFFYR                17
#define  MB3_CHAR_PATIOSF              18
#define  MB3_CHAR_BEDROOMS             19
#define  MB3_CHAR_BATHROOMS            20
#define  MB3_CHAR_HALFBATHS            21
#define  MB3_CHAR_FIREPLACE            22
#define  MB3_CHAR_ASMT                 23
#define  MB3_CHAR_BLDGSEQNUM           24
#define  MB3_CHAR_HASWELL              25
#define  MB3_CHAR_LOTSQFT              26
#define  MB3_CHAR_PARKINGSPACES        27
#define  MB3_CHAR_FLDS                 28

#define  MB_CHAR_FEE_PRCL             0
#define  MB_CHAR_POOLS                1
#define  MB_CHAR_USECAT               2
#define  MB_CHAR_QUALITY              3
#define  MB_CHAR_YRBLT                4
#define  MB_CHAR_BLDGSQFT             5
#define  MB_CHAR_GARSQFT              6
#define  MB_CHAR_HEATING              7
#define  MB_CHAR_COOLING              8
#define  MB_CHAR_HEATING_SRC          9
#define  MB_CHAR_COOLING_SRC          10

// MER 9/12/2013
// YUB 9/27/2013
#ifdef _MB_201309 
#define  MB_CHAR_FLOORS               11
#define  MB_CHAR_UNITS                12
#define  MB_CHAR_ROOMS                13
#define  MB_CHAR_EFFYR                14
#define  MB_CHAR_PATIO                15
#define  MB_CHAR_HAS_ELEVATOR         15
#define  MB_CHAR_BEDS                 16
#define  MB_CHAR_FBATHS               17
#define  MB_CHAR_HBATHS               18
#define  MB_CHAR_FP                   19
#define  MB_CHAR_ASMT                 20
#define  MB_CHAR_HASSEPTIC            21
#define  MB_CHAR_HASSEWER             22
#define  MB_CHAR_HASWELL              23
#else
#define  MB_CHAR_BEDS                 11
#define  MB_CHAR_FBATHS               12
#define  MB_CHAR_HBATHS               13
#define  MB_CHAR_FP                   14
#define  MB_CHAR_ASMT                 15
#define  MB_CHAR_HASSEPTIC            16
#define  MB_CHAR_HASSEWER             17
#define  MB_CHAR_HASWELL              18
#endif

// MER before 9/12/2013
#ifdef _MERCED_
#define  MB_CHAR_BLDGSEQNO            19
#define  MB_CHAR_UNITSEQNO            20  
#endif

// Amador
#ifdef _AMADOR_
#define  MB_CHAR_ASMT_STATUS          19
#define  MB_CHAR_BLDGSEQNO            20
#define  MB_CHAR_UNITSEQNO            21  
#define  MB_CHAR_NUMFLOORS            22
#endif

// Sonoma
#ifdef _SONOMA_
#define  MB_CHAR_DOCNO                16
#define  MB_CHAR_BLDGSEQNO            17
#define  MB_CHAR_UNITSEQNO            18
#define  MB_CHAR_TOTALROOMS           19  
#define  MB_CHAR_NUMFLOORS            20
#endif

// Yolo
#ifdef _YOLO_
#define  MB_CHAR_NUMFLOORS            19
#define  MB_CHAR_LANDSQFT             20
#define  MB_CHAR_TOTALROOMS           21  
#define  MB_CHAR_PARKTYPE             22
#endif

#define  MBSIZ_CHAR_ASMT              12
#define  MBSIZ_CHAR_POOLS             2
#define  MBSIZ_CHAR_USECAT            2
#define  MBSIZ_CHAR_QUALITY           6
#define  MBSIZ_CHAR_YRBLT             4
#define  MBSIZ_CHAR_BLDGSQFT          9
#define  MBSIZ_CHAR_GARSQFT           9
#define  MBSIZ_CHAR_HEATING           2
#define  MBSIZ_CHAR_COOLING           2
#define  MBSIZ_CHAR_HEATSRC           2
#define  MBSIZ_CHAR_COOLSRC           2
#define  MBSIZ_CHAR_BEDS              3
#define  MBSIZ_CHAR_FBATHS            3
#define  MBSIZ_CHAR_HBATHS            3
#define  MBSIZ_CHAR_FP                2
#define  MBSIZ_CHAR_HASSEPTIC         1
#define  MBSIZ_CHAR_HASSEWER          1
#define  MBSIZ_CHAR_HASWELL           1
// Sonoma
#define  MBSIZ_CHAR_DOCNO             12
#define  MBSIZ_CHAR_BLDGSEQNO         2
#define  MBSIZ_CHAR_UNITSEQNO         2
// Yolo
#define  MBSIZ_CHAR_NUMFLOORS         3
#define  MBSIZ_CHAR_LANDSQFT          9
#define  MBSIZ_CHAR_TOTALROOMS        4  
#define  MBSIZ_CHAR_PARKTYPE          4
#define  MBSIZ_CHAR_PARKSPACES        4

#define  MBOFF_CHAR_ASMT              1
#define  MBOFF_CHAR_POOLS             13
#define  MBOFF_CHAR_USECAT            15
#define  MBOFF_CHAR_QUALITY           17
#define  MBOFF_CHAR_YRBLT             23
#define  MBOFF_CHAR_BLDGSQFT          27
#define  MBOFF_CHAR_GARSQFT           36
#define  MBOFF_CHAR_HEATING           45
#define  MBOFF_CHAR_COOLING           47
#define  MBOFF_CHAR_HEATSRC           49
#define  MBOFF_CHAR_COOLSRC           51
#define  MBOFF_CHAR_BEDS              53
#define  MBOFF_CHAR_FBATHS            56
#define  MBOFF_CHAR_HBATHS            59
#define  MBOFF_CHAR_FP                62
#define  MBOFF_CHAR_HASSEPTIC         64
#define  MBOFF_CHAR_HASSEWER          65
#define  MBOFF_CHAR_HASWELL           66
// Sonoma
#define  MBOFF_CHAR_DOCNO             67
#define  MBOFF_CHAR_BLDGSEQNO         79
#define  MBOFF_CHAR_UNITSEQNO         81

// _SITUS
#define  MB_SITUS_ASMT                0
#define  MB_SITUS_STRNAME             1
#define  MB_SITUS_STRNUM              2
#define  MB_SITUS_STRTYPE             3
#define  MB_SITUS_STRDIR              4
#define  MB_SITUS_UNIT                5
#define  MB_SITUS_COMMUNITY           6
#define  MB_SITUS_ZIP                 7
#define  MB_SITUS_SEQ                 8
#define  MB_SITUS_FLDS                9

// _SALES
#define  MB_SALES_ASMT                0
#define  MB_SALES_DOCNUM              1
#define  MB_SALES_DOCDATE             2
#define  MB_SALES_DOCCODE             3
#define  MB_SALES_SELLER              4
#define  MB_SALES_BUYER               5
#define  MB_SALES_PRICE               6
#define  MB_SALES_TAXAMT              7
#define  MB_SALES_GROUPSALE           8
#define  MB_SALES_GROUPASMT           9
#define  MB_SALES_XFERTYPE            10
#define  MB_SALES_ADJREASON           11
#define  MB_SALES_CONFCODE            12
#define  MB_SALES_FLDS                13

#define  MB1_SALES_ASMT               0
#define  MB1_SALES_DOCNUM             1
#define  MB1_SALES_DOCDATE            2
#define  MB1_SALES_DOCCODE            3
#define  MB1_SALES_SELLER             4
#define  MB1_SALES_BUYER              5
#define  MB1_SALES_TAXAMT             6
#define  MB1_SALES_GROUPSALE          7
#define  MB1_SALES_GROUPASMT          8
#define  MB1_SALES_XFERTYPE           9
#define  MB1_SALES_ADJREASON          10
#define  MB1_SALES_CONFCODE           11
#define  MB1_SALES_FLDS               12

// _TAX (old tax layout)
// Tax file layout group 0: BUT
#define  MB_TAX_ASMT                  0
#define  MB_TAX_FEEPARCEL             1
#define  MB_TAX_TAXAMT1               2
#define  MB_TAX_TAXAMT2               3
#define  MB_TAX_PENAMT1               4
#define  MB_TAX_PENAMT2               5
#define  MB_TAX_PENDATE1              6
#define  MB_TAX_PENDATE2              7
#define  MB_TAX_PAIDAMT1              8
#define  MB_TAX_PAIDAMT2              9
#define  MB_TAX_TOTALPAID1            10
#define  MB_TAX_TOTALPAID2            11
#define  MB_TAX_TOTALFEES             12
#define  MB_TAX_PAIDDATE1             13
#define  MB_TAX_PAIDDATE2             14
#define  MB_TAX_YEAR                  15
#define  MB_TAX_ROLLCAT               16
// Added 8/22/2013 for BUT
#define  MB_TAX_ROLLCHGNUM            17
#define  MB_TAX_LAND                  18
#define  MB_TAX_FIXEDIMPR             19
#define  MB_TAX_GROWIMPR              20
#define  MB_TAX_IMPR                  21
#define  MB_TAX_PERSPROP              22
#define  MB_TAX_PP_MH                 23
#define  MB_TAX_NETVAL                24
#define  MB_TAX_EXECODE1              25
#define  MB_TAX_EXEAMT1               26
#define  MB_TAX_EXECODE2              27
#define  MB_TAX_EXEAMT2               28
#define  MB_TAX_EXECODE3              29
#define  MB_TAX_EXEAMT3               30
// Added 9/04/2013 for BUT
#define  MB_TAX_TAXABILITY            31

// Tax file layout group 1: AMA, TUO
#define  T1_ASMT                        0
#define  T1_TAXYEAR                     1
#define  T1_ROLLCHGNUM                  2
#define  T1_MAPCATEGORY                 3
#define  T1_ROLLCATEGORY                4
#define  T1_ROLLTYPE                    5
#define  T1_DESTINATIONROLL             6
#define  T1_INSTALLMENTS                7
#define  T1_BILLTYPE                    8
#define  T1_FEEPARCEL                   9
#define  T1_ORIGINATINGASMT             10
#define  T1_XREFASMT                    11
#define  T1_STATUS                      12
#define  T1_TRA                         13
#define  T1_TAXABILITY                  14
#define  T1_ACRES                       15
#define  T1_SIZEACRESFTTYPE             16
#define  T1_INTDATEFROM                 17
#define  T1_INTDATEFROM2                18
#define  T1_INTDATETHRU                 19
#define  T1_INTERESTTYPE                20
#define  T1_USECODE                     21
#define  T1_CURRENTMARKETLANDVALUE      22
#define  T1_CURRENTFIXEDIMPRVALUE       23
#define  T1_CURRENTGROWINGIMPRVALUE     24
#define  T1_CURRENTSTRUCTURALIMPRVALUE  25
#define  T1_CURRENTPERSONALPROPVALUE    26
#define  T1_CURRENTPERSONALPROPMHVALUE  27
#define  T1_CURRENTNETVALUE             28
#define  T1_BILLEDMARKETLANDVALUE       29
#define  T1_BILLEDFIXEDIMPRVALUE        30
#define  T1_BILLEDGROWINGIMPRVALUE      31
#define  T1_BILLEDSTRUCTURALIMPRVALUE   32
#define  T1_BILLEDPERSONALPROPVALUE     33
#define  T1_BILLEDPERSONALPROPMHVALUE   34
#define  T1_BILLEDNETVALUE              35
#define  T1_OWNER                       36
#define  T1_ASSESSEE                    37
#define  T1_MAILADDRESS1                38
#define  T1_MAILADDRESS2                39
#define  T1_MAILADDRESS3                40
#define  T1_MAILADDRESS4                41
#define  T1_ZIPMATCH                    42
#define  T1_BARCODEZIP                  43
#define  T1_EXEMPTIONCODE1              44
#define  T1_EXEMPTIONAMT1               45
#define  T1_EXEMPTIONCODE2              46
#define  T1_EXEMPTIONAMT2               47
#define  T1_EXEMPTIONCODE3              48
#define  T1_EXEMPTIONAMT3               49
#define  T1_BILLDATE                    50
#define  T1_DUEDATE1                    51
#define  T1_DUEDATE2                    52
#define  T1_TAXAMT1                     53
#define  T1_TAXAMT2                     54
#define  T1_PENAMT1                     55
#define  T1_PENAMT2                     56
#define  T1_COST1                       57
#define  T1_COST2                       58
#define  T1_PENCHRGDATE1                59
#define  T1_PENCHRGDATE2                60
#define  T1_PAIDAMT1                    61
#define  T1_PAIDAMT2                    62
#define  T1_PAYMENTDATE1                63
#define  T1_PAYMENTDATE2                64
#define  T1_TRANSDATE1                  65
#define  T1_TRANSDATE2                  66
#define  T1_COLLECTIONNUM1              67
#define  T1_COLLECTIONNUM2              68
#define  T1_UNSDELINQPENAMTPAID1        69
#define  T1_SECDELINQPENAMTPAID2        70
#define  T1_TOTALFEESPAID1              71
#define  T1_TOTALFEESPAID2              72
#define  T1_TOTALFEES                   73
#define  T1_PMTCNLDATE1                 74
#define  T1_PMTCNLDATE2                 75
#define  T1_TRANSFERDATE                76
#define  T1_PRORATIONFACTOR             77
#define  T1_PRORATIONPCT                78
#define  T1_DAYSTOFISCALYEAREND         79
#define  T1_DAYSOWNED                   80
#define  T1_OWNFROM                     81
#define  T1_OWNTHRU                     82
#define  T1_SITUS1                      83
#define  T1_SITUS2                      84
#define  T1_ASMTROLLYEAR                85
#define  T1_PARCELDESCRIPTION           86
#define  T1_BILLCOMMENTSLINE1           87
#define  T1_BILLCOMMENTSLINE2           88
#define  T1_DEFAULTNUM                  89
#define  T1_DEFAULTDATE                 90
#define  T1_REDEEMEDDATE                91
#define  T1_SECDELINQFISCALYEAR         92
#define  T1_PRIORTAXPAID1               93
#define  T1_PENINTERESTCODE             94
#define  T1_FOURPAYPLANNUM              95
#define  T1_EXISTSLIEN                  96
#define  T1_EXISTSREFUND                97
#define  T1_EXISTSDELINQUENTVESSEL      98
#define  T1_EXISTSROLLCHG               99
#define  T1_ISPENCHRGCANCELED1          100
#define  T1_ISPENCHRGCANCELED2          101
#define  T1_ISPERSONALPROPERTYPENALTY   102
#define  T1_ISAGPRESERVE                103
#define  T1_ISCARRYOVER1STPAID          104
#define  T1_ISCARRYOVERRECORD           105
#define  T1_ISFAILURETOFILE             106
#define  T1_ISOWNERSHIPPENALTY          107
#define  T1_ISELIGIBLEFOR4PAY           108
#define  T1_ISNOTICE4SENT               109
#define  T1_ISPARTPAY                   110
#define  T1_ISFORMATTEDADDRESS          111
#define  T1_ISADDRESSCONFIDENTIAL       112
#define  T1_SUPLCNT                     113
#define  T1_ORIGINALDUEDATE1            114
#define  T1_ORIGINALDUEDATE2            115
#define  T1_ISPEN1REFUND                116
#define  T1_ISPEN2REFUND                117
#define  T1_ISDELINQPENREFUND           118
#define  T1_ISREFUNDAUTHORIZED          119
#define  T1_ISNODISCHARGE               120
#define  T1_ISAPPEALPENDING             121
#define  T1_OTHERFEESPAID               122
#define  T1_FOURPAYREASON               123
#define  T1_DATEDISCHARGED              124
#define  T1_ISONLYFIRSTPAID             125
#define  T1_ISALLPAID                   126
#define  T1_DTS                         127
#define  T1_USERID                      128
#define  T1_RATEUSED                    129
#define  T1_FLDS                        130

// Tax file layout group 2: HUM, SIS, LAK, PLU, SHA, SBT
#define  T2_ASMT                        0
#define  T2_TAXYEAR                     1
#define  T2_ROLLCHGNUM                  2
#define  T2_MAPCATEGORY                 3
#define  T2_ROLLCATEGORY                4
#define  T2_ROLLTYPE                    5
#define  T2_DESTINATIONROLL             6
#define  T2_INSTALLMENTS                7
#define  T2_BILLTYPE                    8
#define  T2_FEEPARCEL                   9
#define  T2_ORIGINATINGASMT             10
#define  T2_XREFASMT                    11
#define  T2_STATUS                      12
#define  T2_TRA                         13
#define  T2_TAXABILITY                  14
#define  T2_ACRES                       15
#define  T2_SIZEACRESFTTYPE             16
#define  T2_INTDATEFROM                 17
#define  T2_INTDATEFROM2                18
#define  T2_INTDATETHRU                 19
#define  T2_INTERESTTYPE                20
#define  T2_USECODE                     21
#define  T2_CURRENTMARKETLANDVALUE      22
#define  T2_CURRENTFIXEDIMPRVALUE       23
#define  T2_CURRENTGROWINGIMPRVALUE     24
#define  T2_CURRENTSTRUCTURALIMPRVALUE  25
#define  T2_CURRENTPERSONALPROPVALUE    26
#define  T2_CURRENTPERSONALPROPMHVALUE  27
#define  T2_CURRENTNETVALUE             28
#define  T2_BILLEDMARKETLANDVALUE       29
#define  T2_BILLEDFIXEDIMPRVALUE        30
#define  T2_BILLEDGROWINGIMPRVALUE      31
#define  T2_BILLEDSTRUCTURALIMPRVALUE   32
#define  T2_BILLEDPERSONALPROPVALUE     33
#define  T2_BILLEDPERSONALPROPMHVALUE   34
#define  T2_BILLEDNETVALUE              35
#define  T2_OWNER                       36
#define  T2_ASSESSEE                    37
#define  T2_MAILADDRESS1                38
#define  T2_MAILADDRESS2                39
#define  T2_MAILADDRESS3                40
#define  T2_MAILADDRESS4                41
#define  T2_ZIPMATCH                    42
#define  T2_BARCODEZIP                  43
#define  T2_EXEMPTIONCODE1              44
#define  T2_EXEMPTIONAMT1               45
#define  T2_EXEMPTIONCODE2              46
#define  T2_EXEMPTIONAMT2               47
#define  T2_EXEMPTIONCODE3              48
#define  T2_EXEMPTIONAMT3               49
#define  T2_BILLDATE                    50
#define  T2_DUEDATE1                    51
#define  T2_DUEDATE2                    52
#define  T2_TAXAMT1                     53
#define  T2_TAXAMT2                     54
#define  T2_PENAMT1                     55
#define  T2_PENAMT2                     56
#define  T2_COST1                       57
#define  T2_COST2                       58
#define  T2_PENCHRGDATE1                59
#define  T2_PENCHRGDATE2                60
#define  T2_PAIDAMT1                    61
#define  T2_PAIDAMT2                    62
#define  T2_PAYMENTDATE1                63
#define  T2_PAYMENTDATE2                64
#define  T2_TRANSDATE1                  65
#define  T2_TRANSDATE2                  66
#define  T2_COLLECTIONNUM1              67
#define  T2_COLLECTIONNUM2              68
#define  T2_UNSDELINQPENAMTPAID1        69
#define  T2_SECDELINQPENAMTPAID2        70
#define  T2_TOTALFEESPAID1              71
#define  T2_TOTALFEESPAID2              72
#define  T2_TOTALFEES                   73
#define  T2_PMTCNLDATE1                 74
#define  T2_PMTCNLDATE2                 75
#define  T2_TRANSFERDATE                76
#define  T2_PRORATIONFACTOR             77
#define  T2_PRORATIONPCT                78
#define  T2_DAYSTOFISCALYEAREND         79
#define  T2_DAYSOWNED                   80
#define  T2_OWNFROM                     81
#define  T2_OWNTHRU                     82
#define  T2_SITUS1                      83
#define  T2_SITUS2                      84
#define  T2_ASMTROLLYEAR                85
#define  T2_PARCELDESCRIPTION           86
#define  T2_BILLCOMMENTSLINE1           87
#define  T2_BILLCOMMENTSLINE2           88
#define  T2_DEFAULTNUM                  89
#define  T2_DEFAULTDATE                 90
#define  T2_REDEEMEDDATE                91
#define  T2_SECDELINQFISCALYEAR         92
#define  T2_PRIORTAXPAID1               93
#define  T2_PENINTERESTCODE             94
#define  T2_FOURPAYPLANNUM              95
#define  T2_EXISTSLIEN                  96
#define  T2_EXISTSCORTAC                97   //
#define  T2_EXISTSCOUNTYCORTAC          98   //
#define  T2_EXISTSBANKRUPTCY            99   //
#define  T2_EXISTSREFUND                100
#define  T2_EXISTSDELINQUENTVESSEL      101
#define  T2_EXISTSNOTES                 102  // 
#define  T2_EXISTSCRITICALNOTE          103  //
#define  T2_EXISTSROLLCHG               104
#define  T2_ISPENCHRGCANCELED1          105
#define  T2_ISPENCHRGCANCELED2          106
#define  T2_ISPERSONALPROPERTYPENALTY   107
#define  T2_ISAGPRESERVE                108
#define  T2_ISCARRYOVER1STPAID          109
#define  T2_ISCARRYOVERRECORD           110
#define  T2_ISFAILURETOFILE             111
#define  T2_ISOWNERSHIPPENALTY          112
#define  T2_ISELIGIBLEFOR4PAY           113
#define  T2_ISNOTICE4SENT               114
#define  T2_ISPARTPAY                   115
#define  T2_ISFORMATTEDADDRESS          116
#define  T2_ISADDRESSCONFIDENTIAL       117
#define  T2_SUPLCNT                     118
#define  T2_ORIGINALDUEDATE1            119
#define  T2_ORIGINALDUEDATE2            120
#define  T2_ISPEN1REFUND                121
#define  T2_ISPEN2REFUND                122
#define  T2_ISDELINQPENREFUND           123
#define  T2_ISREFUNDAUTHORIZED          124
#define  T2_ISNODISCHARGE               125
#define  T2_ISAPPEALPENDING             126
#define  T2_OTHERFEESPAID               127
#define  T2_FOURPAYREASON               128
#define  T2_DATEDISCHARGED              129
#define  T2_ISONLYFIRSTPAID             130
#define  T2_ISALLPAID                   131
#define  T2_DTS                         132
#define  T2_USERID                      133
#define  T2_RATEUSED                    134
#define  T2_FLDS                        135

// Final value: SON_TAX.CSV
#define  T3_ASMT                        0
#define  T3_FEEPARCEL                   1
#define  T3_TAXAMT1                     2
#define  T3_TAXAMT3                     3
#define  T3_PENAMT1                     4
#define  T3_PENAMT3                     5
#define  T3_PENCHRGDDATE1               6
#define  T3_PENCHRGDDATE2               7
#define  T3_PAIDAMT1                    8
#define  T3_PAIDAMT3                    9
#define  T3_TOTALFEESPAID1              10
#define  T3_TOTALFEESPAID2              11
#define  T3_TOTALFEES                   12
#define  T3_PAYMENTDATE1                13
#define  T3_PAYMENTDATE2                14
#define  T3_TAXYEAR                     15
#define  T3_ROLLCATEGORY                16
#define  T3_ROLLCHGNUM                  17
#define  T3_MAPCATEGORY                 18
#define  T3_ROLLTYPE                    19
#define  T3_DESTINATIONROLL             20
#define  T3_INSTALLMENTS                21
#define  T3_BILLTYPE                    22
#define  T3_ORIGINATINGASMT             23
#define  T3_XREFASMT                    24
#define  T3_STATUS                      25
#define  T3_TRA                         26
#define  T3_TAXABILITY                  27
#define  T3_ACRES                       28
#define  T3_SIZEACRESFTTYPE             29
#define  T3_INTERESTDATEFROM            30
#define  T3_INTERESTDATEFROM2           31
#define  T3_INTERESTDATETHRU            32
#define  T3_INTERESTTYPE                33
#define  T3_USECODE                     34
#define  T3_CURRENTMARKETLANDVALUE      35
#define  T3_CURRENTFIXEDIMPRVALUE       36
#define  T3_CURRENTGROWINGIMPRVALUE     37
#define  T3_CURRENTSTRUCTURALIMPRVALUE  38
#define  T3_CURRENTPERSONALPROPVALUE    39
#define  T3_CURRENTPERSONALPROPMHVALUE  40
#define  T3_CURRENTNETVALUE             41
#define  T3_BILLEDMARKETLANDVALUE       42
#define  T3_BILLEDFIXEDIMPRVALUE        43
#define  T3_BILLEDGROWINGIMPRVALUE      44
#define  T3_BILLEDSTRUCTURALIMPRVALUE   45
#define  T3_BILLEDPERSONALPROPVALUE     46
#define  T3_BILLEDPERSONALPROPMHVALUE   47
#define  T3_BILLEDNETVALUE              48
#define  T3_OWNER                       49
#define  T3_ASSESSEE                    50
#define  T3_MAILADDRESS1                51
#define  T3_MAILADDRESS2                52
#define  T3_MAILADDRESS3                53
#define  T3_MAILADDRESS4                54
#define  T3_ZIPMATCH                    55
#define  T3_BARCODEZIP                  56
#define  T3_EXEMPTIONCODE1              57
#define  T3_EXEMPTIONAMT1               58
#define  T3_EXEMPTIONCODE2              59
#define  T3_EXEMPTIONAMT2               60
#define  T3_EXEMPTIONCODE3              61
#define  T3_EXEMPTIONAMT3               62
#define  T3_BILLDATE                    63
#define  T3_DUEDATE1                    64
#define  T3_DUEDATE2                    65
#define  T3_COST1                       66
#define  T3_COST3                       67
#define  T3_TRANSDATE1                  68
#define  T3_TRANSDATE2                  69
#define  T3_COLLECTIONNUM1              70
#define  T3_COLLECTIONNUM2              71
#define  T3_UNSDELINQPENAMTPAID1        72
#define  T3_SECDELINQPENAMTPAID2        73
#define  T3_PAYMENTCANCELEDDATE1        74
#define  T3_PAYMENTCANCELEDDATE2        75
#define  T3_TRANSFERDATE                76
#define  T3_PRORATIONFACTOR             77
#define  T3_PRORATIONPCT                78
#define  T3_DAYSTOFISCALYEAREND         79
#define  T3_DAYSOWNED                   80
#define  T3_OWNERSHIPFROM               81
#define  T3_OWNERSHIPTHRU               82
#define  T3_SITUS1                      83
#define  T3_SITUS2                      84
#define  T3_ASMTROLLYEAR                85
#define  T3_PARCELDESCRIPTION           86
#define  T3_BILLCOMMENTSLINE1           87
#define  T3_BILLCOMMENTSLINE2           88
#define  T3_DEFAULTNUM                  89
#define  T3_DEFAULTDATE                 90
#define  T3_REDEEMEDDATE                91
#define  T3_SECDELINQFISCALYEAR         92
#define  T3_PRIORTAXPAID1               93
#define  T3_PENINTERESTCODE             94
#define  T3_FOURPAYPLANNUM              95
#define  T3_EXISTSLIEN                  96
#define  T3_EXISTSCORTAC                97 
#define  T3_EXISTSCOUNTYCORTAC          98 
#define  T3_EXISTSBANKRUPTCY            99 
#define  T3_EXISTSREFUND                100
#define  T3_EXISTSDELINQUENTVESSEL      101
#define  T3_EXISTSNOTES                 102
#define  T3_EXISTSCRITICALNOTE          103
#define  T3_EXISTSROLLCHG               104
#define  T3_ISPENCHRGCANCELED1          105
#define  T3_ISPENCHRGCANCELED2          106
#define  T3_ISPERSONALPROPERTYPENALTY   107
#define  T3_ISAGPRESERVE                108
#define  T3_ISCARRYOVER1STPAID          109
#define  T3_ISCARRYOVERRECORD           110
#define  T3_ISFAILURETOFILE             111
#define  T3_ISOWNERSHIPPENALTY          112
#define  T3_ISELIGIBLEFOR4PAY           113
#define  T3_ISNOTICE4SENT               114
#define  T3_ISPARTPAY                   115
#define  T3_ISFORMATTEDADDRESS          116
#define  T3_ISADDRESSCONFIDENTIAL       117
#define  T3_SUPLCNT                     118
#define  T3_ORIGINALDUEDATE1            119
#define  T3_ORIGINALDUEDATE2            120
#define  T3_ISPEN1REFUND                121
#define  T3_ISPEN2REFUND                122
#define  T3_ISDELINQPENREFUND           123
#define  T3_ISREFUNDAUTHORIZED          124
#define  T3_ISNODISCHARGE               125
#define  T3_ISAPPEALPENDING             126
#define  T3_OTHERFEESPAID               127
#define  T3_FOURPAYREASON               128
#define  T3_DATEDISCHARGED              129
#define  T3_ISONLYFIRSTPAID             130
#define  T3_ISALLPAID                   131
#define  T3_DTS                         132
#define  T3_USERID                      133
#define  T3_RATEUSED                    134
#define  T3_FLDS                        135

// 2016 - TC601 (MER,AMA,BUT,STA,TEH,GLE)
#define V_ASMT                         0
#define V_TAXYEAR                      1
#define V_ROLLCHGNUM                   2
#define V_CURRENTMARKETLANDVALUE       3
#define V_CURRENTFIXEDIMPRVALUE        4
#define V_CURRENTGROWINGIMPRVALUE      5
#define V_CURRENTSTRUCTURALIMPRVALUE   6
#define V_CURRENTPERSONALPROPVALUE     7
#define V_CURRENTPERSONALPROPMHVALUE   8
#define V_CURRENTEXEMPTIONCODE1        9
#define V_CURRENTEXEMPTIONAMT1         10
#define V_CURRENTEXEMPTIONCODE2        11
#define V_CURRENTEXEMPTIONAMT2         12
#define V_CURRENTEXEMPTIONCODE3        13
#define V_CURRENTEXEMPTIONAMT3         14
#define V_CURRENTNETVALUE              15
#define V_BILLEDMARKETLANDVALUE        16
#define V_BILLEDFIXEDIMPRVALUE         17
#define V_BILLEDGROWINGIMPRVALUE       18
#define V_BILLEDSTRUCTURALIMPRVALUE    19
#define V_BILLEDPERSONALPROPVALUE      20
#define V_BILLEDPERSONALPROPMHVALUE    21
#define V_BILLEDEXEMPTIONCODE1         22
#define V_BILLEDEXEMPTIONAMT1          23
#define V_BILLEDEXEMPTIONCODE2         24
#define V_BILLEDEXEMPTIONAMT2          25
#define V_BILLEDEXEMPTIONCODE3         26
#define V_BILLEDEXEMPTIONAMT3          27
#define V_BILLEDNETVALUE               28
#define V_PRIORMARKETLANDVALUE         29
#define V_PRIORFIXEDIMPRVALUE          30
#define V_PRIORGROWINGIMPRVALUE        31
#define V_PRIORSTRUCTURALIMPRVALUE     32
#define V_PRIORPERSONALPROPVALUE       33
#define V_PRIORPERSONALPROPMHVALUE     34
#define V_PRIOREXEMPTIONCODE1          35
#define V_PRIOREXEMPTIONAMT1           36
#define V_PRIOREXEMPTIONCODE2          37
#define V_PRIOREXEMPTIONAMT2           38
#define V_PRIOREXEMPTIONCODE3          39
#define V_PRIOREXEMPTIONAMT3           40
#define V_PRIORNETVALUE                41

typedef struct _tMB_Char
{
   char  Asmt[MBSIZ_CHAR_ASMT];              //  1
   char  NumPools[MBSIZ_CHAR_POOLS];         // 13
   char  LandUseCat[MBSIZ_CHAR_USECAT];      // 15
   char  QualityClass[MBSIZ_CHAR_QUALITY];   // 17
   char  YearBuilt[MBSIZ_CHAR_YRBLT];        // 23
   char  BuildingSize[MBSIZ_CHAR_BLDGSQFT];  // 27
   char  SqFTGarage[MBSIZ_CHAR_GARSQFT];     // 36
   char  Heating[MBSIZ_CHAR_HEATING];        // 45
   char  Cooling[MBSIZ_CHAR_COOLING];        // 47
   char  HeatingSource[MBSIZ_CHAR_HEATSRC];  // 49
   char  CoolingSource[MBSIZ_CHAR_COOLSRC];  // 51
   char  NumBedrooms[MBSIZ_CHAR_BEDS];       // 53
   char  NumFullBaths[MBSIZ_CHAR_FBATHS];    // 56
   char  NumHalfBaths[MBSIZ_CHAR_HBATHS];    // 59
   char  NumFireplaces[MBSIZ_CHAR_FP];       // 62
   char  FeeParcel[MBSIZ_CHAR_ASMT];         // 64
#ifdef _SONOMA_
   char  DocNo[MBSIZ_CHAR_DOCNO];
   char  BldgSeqNo[MBSIZ_CHAR_BLDGSEQNO];
   char  UnitSeqNo[MBSIZ_CHAR_BLDGSEQNO];
#else
   char  HasSeptic;                          // 76
   char  HasSewer;                           // 77
   char  HasWell;                            // 78
#endif
   char  HasMHRec;                           // 79 Shasta
   char  AsmtStatus;                         // 80
#ifdef _YOLO_
   char  Stories[MBSIZ_CHAR_NUMFLOORS];      // 81
   char  LotSqft[MBSIZ_CHAR_LANDSQFT];       // 84
   char  TotalRooms[MBSIZ_CHAR_TOTALROOMS];  // 93
   char  ParkType[MBSIZ_CHAR_PARKTYPE];      // 97
   char  ParkSpaces[MBSIZ_CHAR_PARKSPACES];  // 101
#endif
#ifdef _AMADOR_
   char  Stories[MBSIZ_CHAR_NUMFLOORS];
#endif
#ifdef _MERCED_
   char  BldgSeqNo[MBSIZ_CHAR_BLDGSEQNO];
   char  UnitSeqNo[MBSIZ_CHAR_BLDGSEQNO];
#endif
   char  CRLF[2];
} MB_CHAR;

typedef struct _tApnRange
{
   char  acApn[12];
   int   iLen;
} APN_RNG;

// Convert MB format file to MB_CHAR
int   MB_ConvChar(char *pInfile);

// Convert MB1 format file to STD_CHAR
int   MB1_StdChar(char *pInfile);

bool  sqlConnect(LPCSTR strDb, hlAdo *phDb);
//int   execSqlCmd(LPCSTR strCmd);
//int   execSqlCmd(LPCSTR strCmd, hlAdo *phDb);
//int   MB_CreateSale();
// Use MB_CreateSCSale() for counties with ConfirmedSalesPrice
// and MB1_CreateSCSale() for counties w/o ConfirmedSalesPrice
int   MB_CreateSCSale(int iDateFmt=0, int iDocTypeFmt=0, int iDocNumFmt=0, bool bAppend=false, IDX_TBL5 *pDocTbl=NULL);

// MB1_CreateSCSale() is similar to MB_CreateSCSale() except it handles
// file w/o ConfirmedSalesPrice (i.e. Hum_Sales.csv)
int   MB1_CreateSCSale(int iDateFmt=0, int iDocTypeFmt=0, int iDocNumFmt=0, bool bAppend=false, IDX_TBL5 *pDocTbl=NULL);
int   MB_CreateLienRec(char *pOutbuf, char *pRollRec);
int   MB_ExtrLien(LPCSTR pCnty, LPCSTR pLDRFile);
int   MB_ExtrTR601(LPCSTR pCnty, LPCSTR pLDRFile=NULL, int iChkLastChar=0);
int   MB_ExtrTC601(LPCSTR pCnty, LPCSTR pLDRFile, int iChkLastChar=0, bool bSortInput=true);

int   MB_MergeSale(SALE_REC *pSaleRec, char *pOutbuf, bool bSaleFlag, bool bUpdtXfer=true);
int   MB_ExtrProp8Ldr(char *pCnty, char *pLDRFile=NULL);
int   MB_ExtrProp8(char *pCnty, char *pLDRFile, char cDelim, int iTaxable);
int   MB_ExtrZoning(int iZoneToken);

int   MB_MergeExe(char *pOutbuf);
int   MB_MergeExe2(char *pOutbuf, int iSkipHdr);
int   MB_MergeTax(char *pOutbuf);
int   MB_MergeTaxRC(char *pOutbuf);
int   MB_MergeTaxG1(char *pOutbuf, int iSkipHdr);
int   MB_MergeTaxG2(char *pOutbuf, int iSkipHdr);
//int   MB_Load_TaxRollInfo(bool bImport, int iSkipHdr=-1);
int   MB_Load_TaxCode(bool bImport, int iSkipHdr=-1, bool bUpdBillNum=true);
int   MB_Load_TaxCodeMstr(bool bImport, int iSkipHdr=-1);
int   MB_Load_TaxRedemption(bool bImport, int iSkipHdr=-1);
int   MB_Load_TaxDelq(bool bImport, int iSkipHdr=1);
int   MB_Load_TaxBase(bool bImport, bool bInclOwner, int iMB_Group=1, int iSkipHdr=-1, int iDateFmt=YYYY_MM_DD);
int   MB_Update_TaxPaid(bool bImport, int iMB_Group=1, int iSkipHdr=-1, int iDateFmt=YYYY_MM_DD);

int   MB_Load_TR601Owner(bool bImport, int iMinToken=0);
int   MB_UpdateXfer(char *pCnty);
int   doTaxImport(char *pCnty, int iType);

// Merge STDCHAR to R01 file
int   MB_MergeStdChar(char *pOutbuf, XLAT_CODE *paHeating, XLAT_CODE *paCooling, XLAT_CODE *paPool=NULL);
int   MB1_ConvStdChar(char *pInfile, XLAT_CODE *paHeating, XLAT_CODE *paCooling, XLAT_CODE *paPool, XLAT_CODE *paQual=NULL);
int   MB_ConvStdChar(char *pInfile, XLAT_CODE *paHeating, XLAT_CODE *paCooling, XLAT_CODE *paPool, XLAT_CODE *paQual=NULL);

// Merge zoning
int   MB_LoadZoning(char *pCnty, int iSkip=1);

// DocLinks
//int   updateDocLinks(void (*pfMakeDocLink)(LPSTR, LPSTR, LPSTR), int iSkip=1, int iUpdFlg=0);

#endif // !defined(AFX_LOADMB_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_)
